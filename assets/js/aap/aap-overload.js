directory.showResultsDirectoryHtml = function (resultsDirectory, renderType, smartGrid, community, unique, data) {
    directory.resultKeys = [];
    directory.resultKeys = Object.keys(resultsDirectory);
    var str = '';
    mylog.log('directory.showResultsDirectoryHtml', resultsDirectory, renderType);
    if (unique) {
        var formatParamsList = [];
        $.each(resultsDirectory, function (i, params) {
            formatParamsList.push(directory.prepParamsHtml(params, smartGrid, community, data))
        });
        if (notNull(renderType) && typeof renderType == "string") {
            renderViewFunction = eval(renderType);
            if (typeof renderViewFunction == "function")
                str = renderViewFunction(formatParamsList, smartGrid);
            else
                str = directory.elementPanelHtml(formatParamsList, smartGrid);
        } else
            str = directory.elementPanelHtml(formatParamsList, smartGrid);
    } else if (typeof renderType === 'object' && renderType.view && renderType.single) {
        var formatParams = directory.prepParamsHtml(params, smartGrid, community, data);
        renderViewFunction = eval(renderType.view);
        str += renderViewFunction(formatParams, smartGrid, data, aapObj);
    } else {
        $.each(resultsDirectory, function (i, params) {
            var formatParams = directory.prepParamsHtml(params, smartGrid, community, data);
            if (notNull(renderType) && typeof renderType == "string") {
                renderViewFunction = eval(renderType);
                if (typeof renderViewFunction == "function")
                    str += renderViewFunction(formatParams, smartGrid, data, aapObj);
                else
                    str += directory.elementPanelHtml(formatParams, smartGrid);
            } else
                str += directory.elementPanelHtml(formatParams, smartGrid);
        });
    }
    return str;
};

smallMenu.content = function (content) {
    el = $("#openModal #openModalContent");
    if (content == null)
        return el;
    else
        el.html(content);
}

coInterface.bindLBHLinks = function () {
    mylog.log("coInterface.bindLBHLinks");
    $(".lbh").unbind("click").on("click", function (e) {
        e.preventDefault();
        if ($(this).hasClass('menu-name-profil') && typeof aapObj !== 'undefined' && typeof aapObj.form === 'object' && aapObj.form && typeof aapObj.form.type === 'string' && aapObj.form.type === 'aap' && typeof costum.type === 'string' && costum.type === 'aap' && typeof aapObj.initHTML === 'function') {
            aapObj.initHTML(null);
            $('.modal.dashboard-aap').show(200);
            return;
        }
        $("#openModal").modal("hide");
        mylog.warn("***************************************");
        mylog.warn("coInterface.bindLBHLinks", $(this).attr("href"));
        mylog.warn("***************************************");
        //searchObject.reset();
        var h = ($(this).data("hash")) ? $(this).data("hash") : $(this).attr("href");
        urlCtrl.closePreview();
        $("#modalExplain").modal("hide");
        dyFObj.closeForm();
        urlCtrl.loadByHash(h);
        aapObj.directory.menuRightDropdownHtml(aapObj);
        aapObj.common.session.initYear(aapObj);
    });
    $(".lbh-menu-app").unbind("click").on("click", function (e) {
        if (!$(this).hasClass("editing")) {
            e.preventDefault();
            coInterface.simpleScroll(0, 500);
            if (typeof mapCO != "undefined")
                mapCO.clearMap();
            contextData = null;
            historyReplace = true;
            var h = "";
            if ($(this).data("hash"))
                h = $(this).data("hash")
            else {
                try { h = new URL($(this).attr("href")).hash }
                catch { h = $(this).attr("href") }
            }
            urlCtrl.loadByHash(h);
            aapObj.directory.menuRightDropdownHtml(aapObj);
            aapObj.common.session.initYear(aapObj);
            $this = $(this);
        }
    });
    $(".lbh-anchor").unbind("click").on("click", function (event) {
        //prevent the default action for the click event
        event.preventDefault();

        //get the full url - like mysitecom/index.htm#home
        var full_url = this.href;

        //split the url by # and get the anchor target name - home in mysitecom/index.htm#home
        var parts = full_url.split("#");
        var trgt = parts[1];
        var scrollDOm = ($(".cmsbuilder-center-content").length > 0) ? ".cmsbuilder-center-content" : 'html, body';

        //get the top offset of the target anchor

        if ($("div[data-anchor-target='" + trgt + "']").length > 0) {
            var target_offset = $("div[data-anchor-target='" + trgt + "']").offset();
            var target_top = target_offset.top;
            if ($("#mainNav").length > 0)
                target_top = target_top - $("#mainNav").outerHeight();
            //goto that anchor by setting the body scroll top to anchor top
            $(scrollDOm).animate({ scrollTop: target_top }, 100, 'easeInSine');
        } else {
            urlCtrl.afterLoad = function () {
                if ($("div[data-anchor-target='" + trgt + "']").length > 0) {
                    var target_offset = $("div[data-anchor-target='" + trgt + "']").offset();
                    var target_top = target_offset.top;
                    if ($("#mainNav").length > 0)
                        target_top = target_top - $("#mainNav").outerHeight();
                    //goto that anchor by setting the body scroll top to anchor top
                    $(scrollDOm).animate({ scrollTop: target_top }, 100, 'easeInSine');
                }
            }
            historyReplace = true;
            urlCtrl.loadByHash("#");
        }
        $(".lbh-menu-app, .lbh-anchor").removeClass("active");
        $(this).addClass("active");
    });
    // Open an url with specific module in url
    $(".lbh-module").unbind("click").on("click", function (e) {
        e.preventDefault();
        coInterface.simpleScroll(0, 500);
        //searchObject.reset();
        //if(!empty(filtersObj))
        if (typeof mapCO != "undefined")
            mapCO.clearMap();
        contextData = null;
        historyReplace = true;
        var h = ($(this).data("hash")) ? $(this).data("hash") : $(this).attr("href");
        urlCtrl.loadByHash(h, "survey/");
    });
    //open any url in a modal window
    $(".lbhp").unbind("click").on("click", function (e) {
        e.preventDefault();
        $("#openModal").modal("hide");
        mylog.warn("***************************************");
        mylog.warn("!coInterface.bindLBHLinks Preview", $(this).attr("href"), $(this).data("modalshow"));
        mylog.warn("***************************************");
        var h = ($(this).data("hash")) ? $(this).data("hash") : $(this).attr("href");
        if ($(this).data("modalshow")) {
            mylog.log("coInterface.bindLBHLinks Preview if");
            url = (h.indexOf("#") == 0) ? urlCtrl.convertToPath(h) : h;
            if (h.indexOf("#page") >= 0)
                url = "app/" + url
            smallMenu.openAjaxHTML(baseUrl + '/' + moduleId + "/" + url);

        }
        else {
            mylog.log("coInterface.bindLBHLinks Preview else");
            url = (h.indexOf("#") == 0) ? urlCtrl.convertToPath(h) : h;
            smallMenu.openAjaxHTML(baseUrl + '/' + moduleId + "/" + url);
        }
    });
    //open any url in a preview window
    $(".lbh-preview-element").unbind("click").on("click", function (e) {
        e.preventDefault();
        mylog.warn("***************************************");
        mylog.warn("coInterface.bindLBHLinks Preview ELEMENT", $(this).attr("href"), $(this).data("modalshow"));
        mylog.warn("***************************************");
        onchangeClick = false;
        link = (typeof $(this).data("hash") != "undefined" && notNull($(this).data("hash"))) ? $(this).data("hash") : $(this).attr("href");
        previewHash = link.split(".");
        if ($.inArray(previewHash[2], ["proposals", "proposal", "actions", "resolutions"]) >= 0) {
            ddaT = (previewHash[2] == "proposals") ? "proposal" : previewHash[2];
            uiCoop.getCoopDataPreview(ddaT, previewHash[4]);
        } else {
            hashT = location.hash.split("?");
            getStatus = urlCtrl.getUrlSearchParams();

            hashNav = (hashT[0].indexOf("#") < 0) ? "#" + hashT[0] : hashT[0];
            //hashT[0].substring(0) substring ne sert à rien ... why ???    	
            if (previewHash.length > 1) {
                urlHistoric = hashNav + "?preview=" + previewHash[2] + "." + previewHash[4];
                if ($("#entity" + previewHash[4]).length > 0) setTimeout(function () { $("#entity" + previewHash[4]).addClass("active"); }, 200);
            }
            else {
                mylog.log("hashNav", hashNav);
                pageName = (previewHash[0].indexOf("#") == 0) ? previewHash[0].substr(1) : previewHash[0];
                urlHistoric = hashNav + "?preview=page.costum.view." + pageName;
                // Voir intégration dans history.replaceState !!
                if (typeof costum.app[link] != "undefined" && typeof (costum.app[link].hash != "undefined") && costum.app[link].hash.indexOf("view") > -1 && typeof (costum.app[link].urlExtra != "undefined")) {
                    link = "view" + costum.app[link].urlExtra;
                }

            }
            if (getStatus != "") urlHistoric += "&" + getStatus;
            history.replaceState({}, null, urlHistoric);
            urlCtrl.openPreview(link);

            //permet d'ajouter le hash en paramètre get
            if (!costum || !costum.editMode)
                history.replaceState({}, null, urlCtrl.getUrlWithHashInSearchParams().href)
        }
    });
    $(".open-xs-menu").off().on("click", function () {
        menuToShow = $(this).data("target");
        iconClass = $(this).data("icon");
        labelButton = $(this).data("label");
        if ($(this).hasClass("close-menu")) {
            urlCtrl.closeXsMenu();

        } else {
            $(this).addClass("close-menu");
            $(this).find("i").removeClass(iconClass).addClass("fa-times");
            if ($(this).find(".labelMenu").length)
                $(this).find(".labelMenu").text(trad.close);
            $(".menu-xs-container." + menuToShow).show(400);
        }
    });
}

commentObj.openPreview = function (type, id, path, title, coformKey, formId, options) {
    if (typeof options == "undefined" || (typeof options != "undefined" && typeof options.notCloseOpenModal == "undefined"))
        $("#openModal").modal("hide");
    $("#openModal").removeAttr("tabindex");
    mylog.warn("***************************************");
    mylog.warn("***************************************");
    title = decodeURI(title);
    hashPreview = "?preview=comments." + type + "." + id;
    urlPreview = baseUrl + '/' + moduleId + "/comment/index/type/" + type + "/id/" + id;
    if (notNull(path)) {
        urlPreview += "/path/" + path;
        hashPreview += "." + path;
    }
    //urlCtopenPreviewElement( baseUrl+'/'+moduleId+"/"+url); 
    if (notNull(title))
        hashPreview += "." + encodeURI(title);
    else
        title = "";
    hashT = location.hash.split("?");
    getStatus = searchInterface.getUrlSearchParams();
    urlHistoric =/*hashT[0].substring(0)+hashPreview*/"";
    if (getStatus != "") urlHistoric += "&" + getStatus;
    if (typeof options == "undefined" || (typeof options != "undefined" && typeof options.notCloseOpenModal == "undefined"))
        history.replaceState({}, null, urlHistoric);
    $("#modal-preview-comment .title-comment").html(title);
    coInterface.showLoader("#modal-preview-comment .comment-tree");
    $("#modal-preview-comment").show(200);

    getAjax('#modal-preview-comment .comment-tree', urlPreview, function () {
        commentObj.bindModalPreview();
    }, "html");
    //reload coform input after added comment
    if (coformKey && formId) {
        commentObj.coformKey = coformKey;
        commentObj.formId = formId;
        commentObj.afterSaveReload = function () {
            //alert("afterSaveReload"+commentObj.coformKey+","+commentObj.formId)
            //commentObj.closePreview();
            if (typeof reloadInput == "function") {
                reloadInput(commentObj.coformKey, commentObj.formId);
            }

        }
    }
    if ($("#btn-comment-" + id).length != 0) {
        commentObj.afterSaveReload = function () {
            ajaxPost(
                null,
                baseUrl + '/' + moduleId + "/comment/countcommentsfrom",
                {
                    "type": type,
                    "id": id,
                    "path": path
                },
                function (data) {
                    $("#btn-comment-" + id).html(data.count + " <i class='fa fa-2xx fa-commenting'></i>")
                }
            );
        }
    }
}