var aapObj = {
	file: null,
	form: null,
	context: null,
	answer: null,
	project: null,
	defaultAnswer: null,
	defaultProject: null,
	defaultProposal: null,
	myDefaultProject: null,
	myDefaultProposal: null,
	page: null,
	pageDetail: null,
	canEdit: false,
	canParticipate: false,
	canSee: false,
	aapview: null,
	inputs: null,
	requiredInputs: {},
	session: null,
	aapSession: null,
	myRoles: [],
	subOrganizations: null,
	hasFilter: false,
	showMap: false,
	showMyMap: true,
	fonds: [],
	quartiers: [],
	associations: [],
	hasInviteInputs: false,
	dateStringOptions: {
		day: 'numeric',
		month: 'numeric',
		year: 'numeric',
		hour: 'numeric',
		minute: 'numeric'
	},
	avancementProjectData: {
		data: {
			"idea": "idea",
			"concept": "concept",
			"started": "started",
			"development": "development",
			"testing": "testing",
			"mature": "mature",
			"finished": "finished",
			"abandoned": "abandoned"
		}, icon: {
			"idea": "fa fa-lightbulb-o",
			"concept": "fa fa-bolt",
			"started": "fa fa-star",
			"development": "fa fa-legal",
			"testing": "fa fa-flag",
			"mature": "fa fa-leaf",
			"finished": "fa fa-send",
			"abandoned": "fa fa-ban"
		}, badgeColor: {
			"idea": "in-idea",
			"concept": "in-concept",
			"started": "in-started",
			"development": "in-development",
			"testing": "in-testing",
			"mature": "in-mature",
			"finished": "in-finished",
			"abandoned": "in-abandoned"
		}
	},
	dataFromSocket: {},
	init: function (pInit) {
		var copyAapObj = {};
		if (pInit != null && pInit.form != null && pInit.context != null) {
			copyAapObj = jQuery.extend(true, {}, aapObj);
			copyAapObj.initVar(pInit);
			copyAapObj.initHTML(pInit);
		}
		return copyAapObj;
	},
	initVar: function (pInit) {
		this.form = exists(pInit.form) ? pInit.form : null;
		this.context = exists(pInit.context) ? pInit.context : null;
		this.answer = exists(pInit.answer) ? pInit.answer : null;
		this.defaultAnswer = exists(pInit.defaultAnswer) ? pInit.defaultAnswer : null;
		this.project = exists(pInit.project) ? pInit.project : null;
		this.defaultProject = exists(pInit.defaultProject) ? pInit.defaultProject : null;
		this.defaultProposal = exists(pInit.defaultProposal) ? pInit.defaultProposal : null;
		this.myDefaultProject = exists(pInit.myDefaultProject) ? pInit.myDefaultProject : null;
		this.myDefaultProposal = exists(pInit.myDefaultProposal) ? pInit.myDefaultProposal : null;
		this.page = exists(pInit.page) ? pInit.page : null;
		this.canEdit = exists(pInit.canEdit) ? pInit.canEdit : null;
		this.canParticipate = exists(pInit.canParticipate) ? pInit.canParticipate : null;
		this.canSee = exists(pInit.canSee) ? pInit.canSee : null;
		this.aapview = exists(pInit.aapview) ? pInit.aapview : null;
		this.status = exists(pInit.status) ? pInit.status : null;
		this.inputs = exists(pInit.inputs) ? pInit.inputs : null;
		this.requiredInputs = exists(pInit.requiredInputs) ? pInit.requiredInputs : {};
		this.subOrganizations = exists(pInit.subOrganizations) ? pInit.subOrganizations : null;
		this.pageDetail = exists(pInit.pageDetail) ? pInit.pageDetail : null;
		this.session = exists(pInit.session) ? pInit.session : null;
		this.aapSession = exists(pInit.aapSession) ? pInit.aapSession : null;
		this.showMap = exists(pInit.showMap) ? pInit.showMap : false;
		this.showMyMap = this.showMyMap && this.showMap;

		this.fonds = pInit?.fonds ?? [];
		this.quartiers = pInit?.quartiers ?? [];
		this.associations = pInit?.associations ?? [];
		this.hasInviteInputs = pInit?.hasInviteInputs ?? false;
		this.haveAnswer = typeof pInit.haveAnswer !== 'undefined' ? parseBool(pInit.haveAnswer) : false;
		if (notEmpty(this.context)) {
			var key = "members";
			if (this.context.collection == "projects")
				key = "contributors";
			this.myRoles = this.context?.links?.[key]?.[userId]?.roles ?? []
		}
	},
	initHTML: function (pInit) {

	},
	directory: {
		aapProposalDetailed: function (params, smartGrid, data, aapObj) {
			var answer = aapObj.prepareData.proposal(aapObj, params, data);
			return aapObj.directory.proposition_template_html_detailed(aapObj, answer);
		}, aapProposalCompact: function (params, smartGrid, data, aapObj) {
			var answer = aapObj.prepareData.proposal(aapObj, params, data);
			return aapObj.directory.proposition_template_html_compact(aapObj, answer);
		}, aapProposalMini: function (params, smartGrid, data, aapObj) {
			var answer = aapObj.prepareData.proposal(aapObj, params, data);
			return aapObj.directory.proposition_template_html_mini(aapObj, answer);
		}, aapProjectMini: function (params, smartGrid, data, aapObj) {
			var project = aapObj.prepareData.project(aapObj, params, data);
			return aapObj.directory.project_template_html_mini(aapObj, project);
		}, projectCompact: function (params, smartGrid, data, aapObj) {
			var project = aapObj.prepareData.project(aapObj, params, data);
			return aapObj.directory.project_template_html_compact(aapObj, project);
		}, projectDetailed: function (params, smartGrid, data, aapObj) {
			var project = aapObj.prepareData.project(aapObj, params, data);
			return aapObj.directory.projectTemplateHtmlDetailed(aapObj, project);
		}, coformAnswerCompact: function (params, smartGrid, data, aapObj) {
			var answer = aapObj.prepareData.proposal(aapObj, params, data);
			return aapObj.directory.coformCompactHtml(aapObj, answer);
		}, proposition_template_html_detailed: function (aapObj, answer) {
			var dropdownAap = aapObj.directory.proposalDropdownHtml(aapObj, answer);
			var contributor_view_limit = 7;
			var contributors = answer.contributors;
			var contributor_html = contributors.slice(0, contributor_view_limit).map(function (__map, __) {
				return dataHelper.printf('' +
					'<div class="contributor-image" style="left: {{left}}px">' +
					'   <img src="{{imageSource}}" alt="{{imageName}}">' +
					'</div>', {
					left: __ * 16 + 2,
					imageSource: __map.image ? __map.image : defaultImage,
					imageName: __map.name
				});
			}).join('');
			const title = answer.title ? answer.title : '(Sans nom)';
			var html = `
                <div class="proposition-container proposition-detailed" id="proposal-container-detailed-${answer.id}">
                    ${aapObj.directory.propositionSelectionFieldStar(aapObj, answer)}
				    <div class="proposition-image-container">
						<img src="${answer.image}" alt="${(answer.title ? answer.title : answer.id)}" />
				    </div>
                    <div class="proposition-info-container">
                        <h2 class="proposition-name ${answer.title ? "cursor-pointer" : ""}" data-target="${answer.id}" data-project-id="${notEmpty(answer.projectId) ? answer.projectId : ''}">
                            ${aapObj.directory.propositionNotViewedDotMarker(aapObj, answer)}
                            <span class="no-padding title-text">${title}</span>
                        </h2>
                        <span><b>Parent : </b> <a href="#page.type.${answer.parent.collection}.id.${answer.parent.id}" class="parent-link lbh-preview-element">${(answer.parent.name)}</a></span><br>
                        <span><b>${trad["Deposited by"]} : </b>
                                <a href="#page.type.${answer.user.collection}.id.${answer.user.id}" class="lbh-preview-element">
                                    <span class="depositor"><img
                                            src="${answer.user.profilImageUrl}"
                                            alt="${answer.user.name}" class="depositor-image mr-2">${answer.user.name}</span>
                                    </span>
                                </a>
                                <b class="ml-2">${ucfirst(trad['on '])} : </b> ${new Date(answer.created * 1000).toLocaleString('fr', {
				day: 'numeric', month: 'numeric', year: 'numeric'
			})} <br>
                        </span>
                        <span class="proposition-description">
                        ${notEmpty(answer.description) ? `<b>${tradDynForm.description}</b><br>
                                ${aapObj.directory.proposalDescription(aapObj, answer)}
                            ` : ''}
                        </span>
                        <div class="no-padding proposal-badge">
                            ${aapObj.directory.proposalThematique(aapObj, answer)}
                        </div>
                        <div class="col-xs-12 pt-2 mt-4 no-padding row">
                            <span>
                                ${trad["Evaluation status"]} :
                                    <span class="statusView" id="status-${answer.id}">
                                        ${aapObj.directory.buildStatus_template_html(aapObj, answer.status, answer.id, answer.statusInfo, answer.usersStatus)}
                                    </span>
                            </span>
                        </div>
                    </div>
				    <div class="proposition-actions-container">
				        <div class="alert-actions">
				            ${answer.urgence != false && answer.urgence != '' ? '<div class="proposition-alert"><span class="fa fa-exclamation-triangle"></span> Urgent</div>' : ''}
				            ${dropdownAap}
				        </div>
				        <div class="contributors">
                            ${aapObj.hasInviteInputs ? `
                                ${(contributors.length ? (`<div class="contributor-images dropdown">
                                            <div class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                                ${contributor_html}` + (contributors.length > contributor_view_limit ? ('<div class="more-contributor-image">+' + (contributors.length - contributor_view_limit) + '</div>') : '') + `${aapObj.directory.contributeur_template_html(aapObj, contributors, {
				answerId: answer.id,
				projectId: answer?.projectId
			})}
                                            </div>
                                        </div>`) : ` <b class="${aapObj.canEdit ? 'cursor-pointer btn-invite-proposal dropup' : ''}" ${aapObj.canEdit ? `data-answerid="${answer.id}" data-projectid="${answer?.projectId}"` : ''}> ${trad.noone} ${trad.contributor} </b>`)}` : ''
				}
				        </div>
				        <div class="zone-column"></div>
                        ${answer.title ?
					`<div class="btn-access-column">
                                ${notEmpty(answer.projectId) ? `<button type="button" class="btn btn-aap-primary proposition-detail tooltips" data-project="true" data-project-id="${answer.projectId}" data-target="${answer.id}" data-toggle="tooltip" data-placement="bottom" data-original-title="${trad.projectstate}">
                                        <span class="fa fa-lightbulb-o"></span>
                                    </button>` : ''}
                                <button type="button" class="btn btn-aap-primary proposition-detail" data-target="${answer.id}" data-project-id="${notEmpty(answer.projectId) ? answer.projectId : ''}" data-title="${answer.title}">
                                    ${trad.Access} <span class="fa fa-long-arrow-right"></span>
                                </button>
                            </div>` : ''
				}
				    </div>
                    <div class="proposition-collapse-container">
                        <a href="#collapse-${answer.id}-${answer.random}" class="collapse-toggle collapsed proposition-collapse-container-toogler" data-toggle="collapse">
                            <span class="caret"></span>
                        </a>
                        
                        <div id="collapse-${answer.id}-${answer.random}" class="collapse proposition-collapse-item" data-id="${answer.id}">
                            <a href="javascript:;" class="btn btn-default border-none">
                                
                            </a>
                        </div>
                    </div>
				</div>`;
			return html;
		}, proposition_template_html_compact: function (aapObj, answer) {
			var dropdownAap = aapObj.directory.proposalDropdownHtml(aapObj, answer);
			var contributor_view_limit = 7;
			var contributors = answer.contributors;
			var contributor_html = contributors.slice(0, contributor_view_limit).map(function (__map, __) {
				return `<div class="contributor-image" style="left:  ${(__ * 16)}px"><img
                            src="${__map.image ? __map.image : defaultImage}"
                            alt="${__map.name}"></div>`;
			}).join('');
			var title = answer.title ? answer.title : '(' + trad["No name"] + ')';
			var html = '' + `<div class="proposition-container-compact" id="proposal-container-compact-${answer.id}">
                    ${aapObj.directory.propositionSelectionFieldStar(aapObj, answer)}
                    <div class="proposition-container">
                        <div class="proposition-image-container">
                			 <img src="${answer.image}" alt="${(answer.title ? answer.title : answer.id)}" />
                        </div>
                        <div class="proposition-info-container" style="overflow: unset">
                        <h2 class="proposition-name ${answer.title ? "cursor-pointer" : ""}" data-target="${answer.id}" data-project-id="${notEmpty(answer.projectId) ? answer.projectId : ''}">
                            ${aapObj.directory.propositionNotViewedDotMarker(aapObj, answer)}
                            <span class="no-padding title-text">${title}</span>
                		</h2>
                            <span><b>Parent : </b> <a href="#page.type.${answer.parent.collection}.id.${answer.parent.id}" class="parent-link lbh-preview-element">${(answer.parent.name)}</a></span><br>
                            <span><b>${trad["Deposited by"]} : </b>
                				<a href="#page.type.${answer.user.collection}.id.${answer.user.id}" class="lbh-preview-element">
                				<span class="depositor"><img
                                        src="${answer.user.profilImageUrl}"
                                        alt="${answer.user.name}" class="depositor-image mr-2">${answer.user.name}</span>
                				</span><br>
                				</a>
                            <span><b>${ucfirst(trad['on '])} : </b> ${new Date(answer.created * 1000).toLocaleString('fr', {
				day: 'numeric', month: 'numeric', year: 'numeric'
			})}</span><br>
                        <div class="contributors">
                        ${aapObj.hasInviteInputs ?
					`${(contributors.length ? (`
                                <div class="contributor-images dropdown">
                                    <div class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">` + contributor_html + (contributors.length > contributor_view_limit ? ('<div class="more-contributor-image">+' + (contributors.length - contributor_view_limit) + '</div>') : '') + `${aapObj.directory.contributeur_template_html(aapObj, contributors, {
						answerId: answer.id, projectId: answer?.projectId
					})}` + '</div></div>') : ` <b class="${aapObj.canEdit ? 'cursor-pointer btn-invite-proposal dropup' : ''}" ${aapObj.canEdit ? `data-answerid="${answer.id}" data-projectid="${answer?.projectId}"` : ''}> ${trad.noone} ${trad.contributor} </b>`)}`
					: ''
				}
                        </div>
                        <div class="no-padding proposal-badge">
                            ${aapObj.directory.proposalThematique(aapObj, answer)}
                        </div>
                            ${answer.title ?
					`<div class="col-xs-12 btn-access-column">
                                    ${notEmpty(answer.projectId) ? `<button type="button" class="btn btn-aap-primary proposition-detail tooltips" data-project="true" data-project-id="${answer.projectId}" data-target="${answer.id}" data-toggle="tooltip" data-placement="bottom" data-original-title="${trad.projectstate}">
                                            <span class="fa fa-lightbulb-o"></span>
                                        </button>` : ''}
                                    <button type="button" class="btn btn-aap-primary proposition-detail" data-target="${answer.id}" data-project-id="${notEmpty(answer.projectId) ? answer.projectId : ''}" data-title="${title}">
                                        ${trad.Access} <span class="fa fa-long-arrow-right"></span>
                                    </button>
                                </div>` : ""
				}
                        </div>
                        <div class="proposition-actions-container">
                            <div class="alert-actions">
                                ${answer.urgence != false && answer.urgence != '' ? '<div class="proposition-alert"><span class="fa fa-exclamation-triangle"></span> Urgent</div>' : ''}
                                ${dropdownAap}
                            </div>
                            <div class="zone-column"></div>
                        </div>
                        <div class="proposition-collapse-container">
                            <a href="#collapse-${answer.id}" class="collapse-toggle collapsed" data-toggle="collapse">
                                <span class="caret"></span>
                            </a>
                            
                            <div id="collapse-${answer.id}" class="collapse proposition-collapse-item" data-id="${answer.id}">
                                <a href="javascript:;" class="btn btn-default border-none">
                                    
                                </a>
                            </div>
                        </div>
                    </div>
                </div>`;
			return html;
		}, proposition_template_html_mini: function (aapObj, answer) {
			const title = answer.title ? answer.title : '(Sans nom)';
			const isMe = typeof userId != undefined && answer?.user?.id == userId ? true : false;
			const projectId = answer?.projectId ? answer?.projectId : '';
			const financingProgression = ((answer?.financements?.financed * 100) / (answer?.financements?.depense > 0 ? answer?.financements?.depense : 1)).toFixed(2);
			let evaluationsHtml = answer.evaluations.voters > 0 ? `
                    <span class="p-3">
                        <span>${trad["Notes"]} : </span>${Number(answer.evaluations.rate).toFixed(1)}
                    </span> <br>
                    <span class="p-3">
                        <span>${trad["Number of votes"]} : </span>
                        ${answer.evaluations.voters}
                    </span> <br>
            ` : ''
			evaluationsHtml += `
                <span class="p-3">
                    <span>${trad["Financed at"]} : </span>${financingProgression} %
                </span> 
				${answer.requiredInputsAnswer && Object.keys(answer.requiredInputsAnswer).length > 0 ? `
						<br> <span class="p-3"> 
								<span>${trad["Required fields not filled"]} : ${Object.keys(answer.requiredInputsAnswer).length} </span>
							 </span>
					` : ''
				}
				${
					answer?.notSeenComments ? `
						<br> <span class="p-3">
								<span>${ trad["New comments"] } : ${ answer.notSeenComments } </span>
							 </span>
					` : ''
				}
            `
			let financingProgressionHtml = `
                <div class="progress col-xs-12 no-padding mt-3" style="height: 3px; margin-bottom: 0.1rem">
                    <div class="progress-bar financing-progression" role="progressbar" style="width: ${financingProgression}%" aria-valuenow="${financingProgression}" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
            `
			const notSeenCommentsHtml = `
				${
					answer?.notSeenComments ? `
						<span class="mr-3">
							<span class="fa fa-comment"></span>
							<span class="badge badge-info new-comments-badge">
								${ answer.notSeenComments }
							</span>
						</span>
					` : ''
				}
			`
			var html = `
                <a class="col-xs-12 props-item set-p0 ${answer.requiredInputsAnswer && Object.keys(answer.requiredInputsAnswer).length > 0 ? 'has-empty-required-input' : ''}" data-custom-toggle="tooltip" data-content='${evaluationsHtml}' id="propItem${answer.id}" href="javascript:;" data-title="${title}" data-project-id="${projectId}" data-target="${answer.id}">
                    <span class="d-flex justify-content-between">
                        <span class="text-ellipsis hover-marquee">${title}</span> 
						<span class="d-flex">
							${ notSeenCommentsHtml } ${isMe ? `<i class="fa fa-user pull-right pr-3 pt-1"></i>` : ''}
						</span>
                    </span>
                    ${financingProgressionHtml}
                </a>
            `;
			return html
		}, project_template_html_mini: function (aapObj, project) {
			var title = project.title ? project.title : '(Sans nom)';
			var isMe = userId && project.answer && project.answer.user && project.answer.user === userId;
			var answerId = project.answer && project.answer._id && project.answer._id.$id ? project.answer._id.$id : "";
			var badge_html = '';
			var count_status = 'totest';
			if (project.action_count > 0)
				badge_html = '<span class="badge aap-action-count ' + count_status + '">' + project.action_count + '</span>';
			var html = `
                <a class="col-xs-12 props-item d-flex justify-content-between" id="propItem${project.id}" href="javascript:;" data-title="${title}" data-target="${project.id}" data-answer-id="${answerId}">
                    <span class="text-ellipsis hover-marquee">${title}</span>
                    ${isMe ? `<i class="fa fa-user pull-right pr-3 pt-1"></i>` : ''}
					${badge_html}
                </a>
            `;
			return (html);
		}, project_template_html_compact: function (aapObj, project) {
			var dropdownProject = aapObj.directory.projectDropdownHtml(aapObj, project);
			const progress_value = project['task_status']['total'] === 0 ? 0 : Math.ceil(project['task_status']['done'] * 100 / project['task_status']['total'])
			let background;
			if (progress_value === 100) {
				background = '#9bc125';
			} else {
				background = `conic-gradient(#9bc125 ${progress_value * 3.6}deg, #dedede 0deg)`;
			}
			var doneSvg = `
							<svg width="30" height="28" viewBox="0 0 30 28" fill="none" xmlns="http://www.w3.org/2000/svg">
								<path d="M29.6667 13.9999L26.4134 10.2932L26.8667 5.38656L22.0534 4.29322L19.5334 0.0532227L15 1.99989L10.4667 0.0532227L7.94671 4.29322L3.13337 5.37322L3.58671 10.2799L0.333374 13.9999L3.58671 17.7066L3.13337 22.6266L7.94671 23.7199L10.4667 27.9599L15 25.9999L19.5334 27.9465L22.0534 23.7066L26.8667 22.6132L26.4134 17.7066L29.6667 13.9999ZM12.3334 20.6666L7.00004 15.3332L8.88004 13.4532L12.3334 16.8932L21.12 8.10655L23 9.99989L12.3334 20.6666Z" fill="#9bc125"/>
							</svg>
							`;

			var contributorsViewLimit = 7;
			var contributors = project.contributors;
			var contributorHtml = contributors.slice(0, contributorsViewLimit).map(function (oneContributorMap, contributorIndex) {
				return `<div class="contributor-image" style="left:  ${(contributorIndex * 16)}px"><img
                            src="${oneContributorMap.image ? oneContributorMap.image : defaultImage}"
                            alt="${oneContributorMap.name}"></div>`;
			}).join('');

			var newHtml = `<div class="proposition-container-compact" id="proposal-container-compact-${project.id}">
                    <div class="proposition-container">
                        <div class="proposition-image-container">
                			 <img src="${project.image}" alt="${project.title}" />
                        </div>
                        <div class="proposition-info-container" style="overflow: unset">
                            <h2 class="proposition-name project-name cursor-pointer" data-id="${project.id}" ${notEmpty(project.answer) ? 'data-answer="' + project.answer?.['_id']?.['$id'] + '"' : ''} data-title="${project.title.replace(/"/g, '\'')}">
                                ${project['title']}
                            </h2>
                            <span><b>${ucfirst(trad['parent'])} : </b> <a href="#page.type.${project.parent.type}.id.${project.parent.id}" class="parent-link lbh-preview-element">${project.parent.name}</a></span><br>
                            <span><b>${trad["Deposited by"]} : </b><a href="#page.type.${project.creator.type}.id.${project.creator.id}" class="lbh-preview-element parent-link">${project.creator.name}</a></span><br>
							<span><b>${ucfirst(trad['on '])} : </b>${project.date.format('LL')}</span><br>
                            <div class="contributors">
                                ${aapObj.hasInviteInputs ?
					`${(contributors.length ? (`<div class="contributor-images dropdown">
                                        <div class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                            ${contributorHtml}${(contributors.length > contributorsViewLimit ? (`<div class="more-contributor-image">+${contributors.length - contributorsViewLimit}</div>`) : '')}
                                            ${aapObj.directory.contributeur_template_html(aapObj, contributors, {
						answerId: project?.answer?.["_id"]?.["$id"], projectId: project.id
					})}
                                        </div>
                                    </div>`) : ` <b class="${aapObj.canEdit && notEmpty(project.answer) ? 'cursor-pointer btn-invite-proposal dropup' : ''}" ${aapObj.canEdit && notEmpty(project.answer) ? `data-answerid="${project?.answer?.["_id"]?.["$id"]}" data-projectid="${project.id}"` : ''}> ${trad.noone} ${trad.contributor} </b>`)}`
					: ''
				}
                            </div>
                        </div>
                        <div class="proposition-actions-container">
                            <div class="alert-actions">
                                ${project['is_done'] ? doneSvg : ''}
                                <div id="projectstatus-${project.id}" class="project-status no-padding">
                                    ${aapObj.directory.buildProjectStatus_template_html(aapObj, project.avancement, project.id)}
                                </div>
                                ${dropdownProject}
                            </div>
                            <div class="be-circular-progress be-circular-progress-primary" style="background: ${background}" data-value="${progress_value}">
								<div class="be-circular-progress-label">${progress_value}%</div>
							</div>
							${ucfirst(trad['advancement'])}
							<div class="zone-column"></div>
                            <div class="btn-access-column">
                                <button class="btn btn-aap-primary project-access" data-target="${project.id}" ${notEmpty(project.answer) ? 'data-answer="' + project.answer?.['_id']?.['$id'] + '"' : ''} data-title="${project.title.replace(/"/g, '\'')}">
                                    ${trad.Access} <span class="fa fa-long-arrow-right"></span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>`;
			return newHtml;
		}, projectTemplateHtmlDetailed(aapObj, project) {
			var dropdownProject = aapObj.directory.projectDropdownHtml(aapObj, project);
			const progressValue = project['task_status']['total'] === 0 ? 0 : Math.ceil(project['task_status']['done'] * 100 / project['task_status']['total'])
			let background;
			if (progressValue === 100) {
				background = '#9bc125';
			} else {
				background = `conic-gradient(#9bc125 ${progressValue * 3.6}deg, #dedede 0deg)`;
			}
			var doneSvg = `
							<svg width="30" height="28" viewBox="0 0 30 28" fill="none" xmlns="http://www.w3.org/2000/svg">
								<path d="M29.6667 13.9999L26.4134 10.2932L26.8667 5.38656L22.0534 4.29322L19.5334 0.0532227L15 1.99989L10.4667 0.0532227L7.94671 4.29322L3.13337 5.37322L3.58671 10.2799L0.333374 13.9999L3.58671 17.7066L3.13337 22.6266L7.94671 23.7199L10.4667 27.9599L15 25.9999L19.5334 27.9465L22.0534 23.7066L26.8667 22.6132L26.4134 17.7066L29.6667 13.9999ZM12.3334 20.6666L7.00004 15.3332L8.88004 13.4532L12.3334 16.8932L21.12 8.10655L23 9.99989L12.3334 20.6666Z" fill="#9bc125"/>
							</svg>
							`;

			var contributorViewLimit = 7;
			var contributors = project.contributors;
			var contributorHtml = contributors.slice(0, contributorViewLimit).map(function (oneContributorMap, contributorIndex) {
				return `<div class="contributor-image" style="left:  ${(contributorIndex * 16)}px"><img
                            src="${oneContributorMap.image ? oneContributorMap.image : defaultImage}"
                            alt="${oneContributorMap.name}"></div>`;
			}).join('');
			var projectAnswer = notEmpty(project.answer) ? project.answer : null;

			var html = `
                <div class="proposition-container proposition-detailed" id="proposal-container-detailed-${project.id}">
				    <div class="proposition-image-container">
                         <img src="${project.image}" alt="${project.title}" />
                    </div>
                    <div class="proposition-info-container">
                        <h2 class="proposition-name project-name cursor-pointer" data-id="${project.id}" ${notEmpty(project.answer) ? 'data-answer="' + project.answer?.['_id']?.['$id'] + '"' : ''} data-title="${project.title.replace(/"/g, '\'')}">
                            ${project.title}
                        </h2>
                        <span><b>${ucfirst(trad['parent'])} : </b> <a href="#page.type.${project.parent.type}.id.${project.parent.id}" class="parent-link lbh-preview-element">${project.parent.name}</a></span><br>
                        <span><b>${trad["Deposited by"]} : </b><a href="#page.type.${project.creator.type}.id.${project.creator.id}" class="lbh-preview-element parent-link">${project.creator.name}</a></span><br>
                        <span><b>${ucfirst(trad['on '])} : </b>${project.date.format('LL')}</span><br>
                        ${notEmpty(project.description) ? `<span class="proposition-description"><b>Description</b><br>${aapObj.directory.proposalDescription(aapObj, project)}</span>` : ''}
                    </div>
				    <div class="proposition-actions-container">
                        <div class="alert-actions">
                            <div id="projectstatus-${project.id}" class="project-status no-padding">
                                ${aapObj.directory.buildProjectStatus_template_html(aapObj, project.avancement, project.id)}
                            </div>
                            ${dropdownProject}
                        </div>
                        <div class="contributors">
                            ${aapObj.hasInviteInputs ?
					`${(contributors.length ? (`<div class="contributor-images dropdown">
                                    <div class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        ${contributorHtml}` + (contributors.length > contributorViewLimit ? (`<div class="more-contributor-image">+${(contributors.length - contributorViewLimit)}</div>`) : '') + `${aapObj.directory.contributeur_template_html(aapObj, contributors, {
						answerId: project?.answer?.["_id"]?.["$id"], projectId: project.id
					})}
                                    </div>
                                </div>`) : ` <b class="${aapObj.canEdit && notEmpty(project.answer) ? 'cursor-pointer btn-invite-proposal dropup' : ''}" ${aapObj.canEdit && notEmpty(project.answer) ? `data-answerid="${project?.answer?.["_id"]?.["$id"]}" data-projectid="${project.id}"` : ''}> ${trad.noone} ${trad.contributor} </b>`)}`
					: ''
				}
                        </div>
				        <div class="zone-column"></div>
				        <div class="be-circular-progress be-circular-progress-primary" style="background: ${background}" data-value="${progressValue}">
                            <div class="be-circular-progress-label">${progressValue}%</div>
                        </div>
                        ${ucfirst(trad['advancement'])}
                        <div class="btn-access-column">
                            ${notEmpty(projectAnswer) && 0 ? `<button type="button" class="btn btn-aap-primary proposition-detail tooltips" data-project="true" data-answer-id="${projectAnswer._id.$id}" data-target="${project.id}" data-toggle="tooltip" data-placement="bottom" data-original-title="${trad.projectstate}">
                                    <span class="fa fa-lightbulb-o"></span>
                                </button>` : ''}
                                <div class="zone-column"></div>
                                <div class="col-xs-12 btn-access-column">
                                    <button class="btn btn-aap-primary project-access" data-target="${project.id}" ${notEmpty(projectAnswer) ? 'data-answer="' + projectAnswer._id.$id + '"' : ''} data-title="${project.title.replace(/"/g, '\'')}">
                                        ${trad.Access} <span class="fa fa-long-arrow-right"></span>
                                    </button>
                                </div>
                        </div>
				    </div>
				</div>`;
			return html;
		}, proposalDescription(aapObj, answer) {
			var html = trad.nodescription;
			if (notEmpty(answer.description)) {
				if (answer.description.length < 200) {
					html = dataHelper.markdownToHtml(answer.description.substring(0, 200), {
						parseImgDimensions: true, simplifiedAutoLink: true, strikethrough: true, tables: true, openLinksInNewWindow: true
					})
				} else {
					html = `<span id="less-${answer.id}">` + answer.description.substring(0, 200) + `<b>. . .</b>
                                    <button type="button" onclick="document.getElementById('less-${answer.id}').style.display = 'none';document.getElementById('more-${answer.id}').style.display = 'table';" class="moreLinkaap btn-xs bg-transparent"> ${trad.readmore} </button>
                            </span>
                            <span id="more-${answer.id}" style="display: none">
                                ${dataHelper.markdownToHtml(answer.description, {
						parseImgDimensions: true, simplifiedAutoLink: true, strikethrough: true, tables: true, openLinksInNewWindow: true
					})}
                                <button type="button" onclick="document.getElementById('more-${answer.id}').style.display = 'none';document.getElementById('less-${answer.id}').style.display = 'table';" class="moreLinkaap btn-xs bg-transparent"> ${trad.readless} </button>
                            </span>
                        `
				}
			}
			return html;
		}, proposalDropdownHtml: function (aapObj, params) {
			var preparedData = aapObj.prepareData.proposalDropdown(aapObj, params);
			const hisProposal = params?.user?.id == userId ? true : false;
			$.each(preparedData, function (key, value) {
				if (!aapObj.common.checkRules(aapObj, value.rules, value.user) && !hisProposal) {
					delete preparedData[key];
				}
			});

			var dropdownHtml = '';
			if (Object.keys(preparedData).length > 0) {
				dropdownHtml = '' +
					'<ul class="dropdown-menu" aria-labelledby="dropdownMenu1" style="right:0;left: -217px;">' +
					$.map(preparedData, function (value) {
						return dataHelper.printf('' +
							'<li>' +
							'   <a href="{{href}}" class="{{className}}" {{dataId}} {{title}} {{dataProject}} data-hisproposal="{{hisproposal}}">' +
							'       <i class="fa fa-{{icon}}"></i> {{label}}' +
							'   </a>' +
							'</li>', {
							className: value.class,
							href: value.href,
							dataId: value.dataId,
							title: value["data-title"] ? value["data-title"] : "",
							hisproposal: hisProposal,
							dataProject: value["data-project-id"] ? value["data-project-id"] : "",
							icon: value.icon,
							label: value.label
						})
					}).join('') +
					'</ul>';
				return '' +
					'<div class="dropdown">' +
					'   <button class="btn btn-default dropdown-toggle border-none" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">' +
					'       <i class="fa fa-ellipsis-v"></i>' +
					'   </button>' +
					dropdownHtml +
					'</div>';
			}
			return '';
		}, projectDropdownHtml: function (aapObj, params) {
			var preparedData = aapObj.prepareData.projectDropdown(aapObj, params);
			$.each(preparedData, function (key, value) {
				if (!aapObj.common.checkRules(aapObj, value.rules, value.user)) {
					delete preparedData[key];
				}
			});

			var dropdownHtml = '';
			if (Object.keys(preparedData).length > 0) {
				dropdownHtml = '' +
					'<ul class="dropdown-menu" aria-labelledby="dropdownMenu1" style="right:0;left: -217px;">' +
					$.map(preparedData, function (value) {
						return dataHelper.printf('' +
							'<li>' +
							'   <a href="{{href}}" class="{{className}}" {{dataId}} {{title}} {{answerId}}>' +
							'       <i class="fa fa-{{icon}}"></i> {{label}}' +
							'   </a>' +
							'</li>', {
							className: value.class,
							href: value.href,
							dataId: value.dataId,
							title: value["data-title"] ? value["data-title"] : "",
							answerId: value["data-answer-id"] ? value["data-answer-id"] : "",
							icon: value.icon,
							label: value.label
						}, true);
					}).join('') +
					'</ul>';
				return '' +
					'<div class="dropdown">' +
					'   <button class="btn btn-default dropdown-toggle border-none" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">' +
					'       <i class="fa fa-ellipsis-v"></i>' +
					'   </button>' +
					dropdownHtml +
					'</div>';
			}
			return '';
		}, menuRightDropdownHtml: function (aapObj) {
			if (location.hash.indexOf('context') != -1 && location.hash.indexOf('formid') != -1) {
				$('#menuTopRight .dropdown-menu-top ul.dropdown-menu .aap-menu-right-item-dropdown').remove();
				var config = aapObj.prepareData.menuRightDropdown(aapObj);
				var html = "";

				$.each(config, function (k, v) {
					var target = v?.target ? "target='_blank'" : ""
					if (aapObj.common.checkRules(aapObj, v?.rules)) {
						html += `<li class="menu-button btn-menu btn-menu-tooltips menu-btn-top aap-menu-right-item-dropdown">
                            <a href="${v.href}" class="${v?.class} cosDyn-buttonList" ${target} id="${v?.id}">
                                <i class="fa fa-${v.icon}"></i> ${v.label}
                            </a>
                        </li>
                        `;
					}
				})

				$('#menuTopRight .dropdown-menu-top ul.dropdown-menu').prepend(html);
				coInterface.bindLBHLinks();
				aapObj.events.menuRightDropdown(aapObj);
			} else {
				$('#menuTopRight .dropdown-menu-top ul.dropdown-menu .aap-menu-right-item-dropdown').remove();
			}
		}, propositionCollapseHtml: function (aapObj, answerId) {
			var answer = aapObj.common.getOneAnswer(aapObj, answerId);
			var html = ``;
			if (exists(answer['results']) && exists(answer['results'][answerId])) {
				answer = aapObj.prepareData.proposal(aapObj, answer['results'][answerId], answer, true);
				html = `
                    <div class="clps-container text-center">
                        ${aapObj.directory.propositionFinancingStats(aapObj, answer)}
                        ${aapObj.directory.propositionFinancingStats(aapObj, answer, true)}
                        ${aapObj.directory.propositionFinancingStats(aapObj, answer, false, "budget")}
                        ${aapObj.directory.proposalDossiersCompletion(aapObj, answer)}
                        ${aapObj.directory.propositionFinancingCounter(aapObj, answer)}

                        ${aapObj.directory.propositionComment(aapObj, answer)}
                        ${aapObj.directory.propositionFavoris(aapObj, answer)}
                        ${aapObj.directory.propositionViewerCounter(aapObj, answer)}
                        ${aapObj.directory.propositionEvaluatorCounter(aapObj, answer)}
                        ${aapObj.directory.propositionActionsStatus(aapObj, answer)}
                    </div>
                `;
				return html;
			}
		}, propositionComment: function (aapObj, answer) {
			if (notEmpty(userConnected)) {
				return `
                <a href="javascript:;" id="btn-comment-${answer.id}" class="btn btn-default border-none tooltips" onclick="commentObj.openPreview('answers','${answer.id}','','${answer.title ? answer.title.replace(/'/g, "\\'") : "Commentaires"}')" data-toggle="tooltip" data-placement="bottom" data-original-title="${trad.comment}">
                    ${notEmpty(answer.commentCount) ? answer.commentCount : ""} <i class='fa fa-2xx fa-commenting-o'></i>
                </a>
                `
			} else {
				return `
                <a href="javascript:;" id="btn-comment-${answer.id}" class="btn btn-default border-none "  data-toggle="modal" data-target="#modalLogin">
                    ${notEmpty(answer.commentCount) ? answer.commentCount : ""} <i class='fa fa-2xx fa-commenting-o tooltips' data-toggle="tooltip" data-placement="bottom" data-original-title="${trad.comment}"></i>
                </a>
                `
			}
		}, propositionFavoris: function (aapObj, answer) {
			var colorClass = " text-aap-primary";
			var dataValue = "set";
			var voteCount = answer.voteCount != 0 ? answer.voteCount : "";
			var icon = "fa-heart-o";

			if (answer.vote) {
				colorClass = " text-aap-primary";
				dataValue = "unset"
				voteCount = answer.voteCount
				icon = "fa-heart"
			}

			if (notEmpty(userConnected)) {
				html = `
                <a href="javascript:;" id="btn-favoris-${answer.id}" class="btn btn-default border-none btn-proposal-favorite tooltips" data-random="${answer.random}" data-id="${answer.id}" data-value="${dataValue}"  data-toggle="tooltip" data-placement="bottom" data-original-title="${trad.Favorites}">
                    ${voteCount} <i class='fa fa-2xx ${icon} ${colorClass}'></i>
                </a>`
			} else {
				html = `
                <a href="javascript:;" id="btn-favoris-${answer.id}" class="btn btn-default border-none" data-toggle="modal" data-target="#modalLogin" data-random="${answer.random}" data-id="${answer.id}" data-value="${dataValue}" >
                    ${voteCount} <i class='fa fa-2xx ${icon} ${colorClass} tooltips' data-toggle="tooltip" data-placement="bottom" data-original-title="${trad.Favorites}"></i>
                </a>`
			}
			return html;
		}, propositionViewerCounter: function (aapObj, answer) {
			var colorClass = "";
			var dataValue = "set";
			var viewsCount = "";
			var icon = "fa-eye";
			var html = "";
			if (answer.views) {
				dataValue = "unset";
				viewsCount = Object.keys(answer.views).length;
				var html = `
                <a href="javascript:;" id="btn-views-${answer.id}" disabled class="btn btn-default border-none btn-proposal-view tooltips" data-id="${answer.id}" data-value="${dataValue}"  data-toggle="tooltip" data-placement="bottom" data-original-title="${trad.Seen} ${trad.by} ${viewsCount} ${trad.person}(s)" style="opacity: 1;">
                    ${viewsCount} <i class='fa fa-2xx ${icon} ${colorClass}'></i>
                </a>`
			}
			return html;
		}, propositionNotViewedDotMarker: function (aapObj, answer) {
			var colorClass = "text-green";
			var icon = "fa-circle";
			var html = "";
			if (notNull(userConnected) && (!notEmpty(answer.views) || notEmpty(answer.views) && !exists(answer.views[userId]))) {
				//html = `<i class='fa ${icon} ${colorClass} tooltips' data-toggle="tooltip" data-placement="bottom"
				// data-original-title="${trad.notSeen}" style="font-size: 13px;"></i>`;
				html = `
                    <div class="ribbon">
                        <span class="aap-new-ribbon2">N<br>e<br>w</span>
                    </div>
                `;
			} else {
				html = "";
			}
			return html;
		}, proposalNotSeenCounter: function (aapObj) {
			var filterPrms = aapObj.paramsFilters.proposal(aapObj, "#ccccc", true, false);
			filterPrms.container = "#ccccc";
			filterPrms.header.dom = ".ccccc";
			filterPrms.aapFooter = "";
			filterPrms.subHeader = "";
			filterPrms.footerDom = {};
			filterPrms.results = {};
			filterPrms.defaults?.onResize ? delete filterPrms.defaults.onResize : "";
			filterPrms.defaults?.appendToXs ? delete filterPrms.defaults.appendToXs : "";
			filterPrms.defaults?.appendToLg ? delete filterPrms.defaults.appendToLg : "";
			filterPrms.defaults?.onResizeFunc ? delete filterPrms.defaults.onResizeFunc : "";


			aapObj.filterSearch.proposalNewCounter = searchObj.init(filterPrms);
			var params = aapObj.filterSearch.proposalNewCounter.search.obj;
			params.searchType = ["answers"];
			params.fields = [];
			ajaxPost(null, baseUrl + "/co2/aap/directoryproposal/source/" + (notEmpty(costum) ? costum.slug : contextData.slug) + "/form/" + aapObj.form._id.$id + "/newcounter/true", params, function (data) {
				$(".cosDyn-buttonList span:contains('Propositions')").empty().html(`
                        <span>Propositions</span>
                        <span class="aap-new-counter small badge tooltips ${data.newCounter > 0 ? '' : 'hide'}" data-toggle="tooltip" data-placement="bottom" data-original-title="${trad.newaction}">
                            ${data.newCounter}
                        </span>
                    `);
				$('.tooltips').tooltip();

				$('.aap-new-counter').on('click', function (e) {
					e.stopImmediatePropagation();
					if ($('#filterContainerL .btn-filters-select.seen[data-value="notSeen"]').length > 0) {
						$('#filterContainerL .btn-filters-select.seen[data-value="notSeen"]').trigger('click');
					} else {
						urlCtrl.loadByHash(`#proposalAap.context.${aapObj.context.slug}.formid.${aapObj.form._id.$id}`)
						htmlExists('.btn-filters-select.seen[data-value="notSeen"]', function () {
							$('.btn-filters-select.seen[data-value="notSeen"]').trigger('click');
						});
					}
				})
			})
		}, answerNotViewMarker: function (aapObj, answer) {
			var html = "";
			if (answer.user.id != userId || (notEmpty(answer.views) && !exists(answer.views[userId]))) {
				html = `unseen-proposition bdgdiv ` + answer.user.id;
			} else {
				html = "";
			}
			return html;
		}, propositionEvaluatorCounter: function (aapObj, answer) {
			var html = "";
			if (notEmpty(answer.selection)) {
				var colorClass = "";
				var icon = "fa-gavel";
				var html = "";

				var evaluatorCount = Object.keys(answer.selection).length;
				var html = `
                <a href="javascript:;" id="btn-evaluator-${answer.id}" disabled class="btn btn-default border-none btn-proposal-view tooltips" data-id="${answer.id}"  data-toggle="tooltip" data-placement="bottom" data-original-title="${evaluatorCount} ${trad.evaluator}(s)" style="opacity: 1;">
                    ${evaluatorCount} <i class='fa fa-2xx ${icon} ${colorClass}'></i>
                </a>`
			}
			return html;
		}, propositionSelectionFieldStar: function (aapObj, answer) {
			var html = "";
			if (notEmpty(answer?.selection) && notEmpty(answer?.allVotes)) {
				if (answer.allVotes > 5) {
					answer.allVotes = answer.allVotes / Object.keys(answer?.selection).length;
				}
				//html = `<div class="evaluator-rating text-center tooltips" data-rating="${answer?.allVotes}" data-toggle="tooltip" data-html="true"
				// data-placement="bottom" data-original-title=""></div>`;
				html = `<div class="star-indicator active tooltips" data-toggle="tooltip" data-html="true" data-placement="right" data-original-title="${ucfirst(trad["evaluators' rating"])}">
                    <span class="fa fa-star fa-3x"></span>
                    <span class="star-indicator-value">${answer.allVotes.toString()}</span>
                </div>`;
			}
			return html;
		}, propositionFinancingCounter: function (aapObj, answer) {
			var html = "";
			if (notEmpty(answer.depense)) {
				var colorClass = "";
				var icon = "fa-money";
				var financingCount = 0;


				var depenseCount = Object.keys(answer.depense).length;
				$.each(answer.depense, function (kd, vd) {
					if (vd?.financer && Object.keys(vd.financer).length != 0) {
						financingCount++;
					}
				})
				var dataOriginalTitle = `${ucfirst(trad.financing)} : ${financingCount}/${depenseCount}`;
				var html = `
                <a href="javascript:;" id="btn-financing-${answer.id}" disabled class="btn btn-default border-none btn-proposal-view tooltips" data-id="${answer.id}"  data-toggle="tooltip" data-placement="bottom" data-original-title="${dataOriginalTitle}" style="opacity: 1;">
                <span class="fraction"><b><sup>${financingCount}</sup><sub>${depenseCount}</sub></b></span> <i class='fa fa-2xx ${icon} ${colorClass}'></i>
                </a>`
			}
			return html
		}, propositionFinancingStats: function (aapObj, answer, isSubvention = false, key = "depense") {
			var html = "";
			if (notNull(answer[key])) {
				var total = 0;
				var financingTotal = 0;
				var spentTotal = 0;

				var subvention = 0;
				var financedSubvention = 0;
				var spentTotalSubvention = 0;
				var hasPostSubvention = false;

				var icon = " fa-money"
				var dataOriginalTitle = "";
				$.each(answer[key], function (k, v) {
					if (v?.poste && v.poste.toLowerCase().includes('Subvention AAP Politique de la Ville'.toLowerCase())) {
						hasPostSubvention = true;
						v?.price ? (subvention = v?.price) : "";
						if (v?.financer && Object.keys(v.financer).length != 0) {
							$.each(v.financer, function (k, v) {
								if (v?.amount) {
									financedSubvention += parseInt(v?.amount);
								}
							})
						}
						if (v?.payement && Object.keys(v.payement).length != 0) {
							$.each(v.payement, function (k, v) {
								if (v?.amount) {
									spentTotalSubvention += parseInt(v?.amount);
								}
							})
						}
					} else {
						if (v?.price) {
							total += parseInt(v?.price);
						}
						if (v?.financer && Object.keys(v.financer).length != 0) {
							$.each(v.financer, function (k, v) {
								if (v?.amount) {
									financingTotal += parseInt(v?.amount);
								}
							})
						}
						if (v?.payement && Object.keys(v.payement).length != 0) {
							$.each(v.payement, function (k, v) {
								if (v?.amount) {
									spentTotal += parseInt(v?.amount);
								}
							})
						}
					}
				})

				if (isSubvention && hasPostSubvention) {
					subvention = addSeparatorMillier(subvention) + " <i class='fa fa-euro'></i>";
					financedSubvention = addSeparatorMillier(financedSubvention) + " <i class='fa fa-euro'></i>";
					spentTotalSubvention = addSeparatorMillier(spentTotalSubvention) + " <i class='fa fa-euro'></i>";
					dataOriginalTitle = `<span class='text-left'>
                        ${ucfirst("Subvention")} : ${subvention} </br>
                            Subvention ${trad.financed} : ${financedSubvention} </br>
                        </span>`;
					html = `
                    <a href="javascript:;" id="btn-financing-2-${answer.id}" ${notEmpty(userConnected) ? "disabled" : ""} class="btn btn-default border-none btn-proposal-view btn-proposal-view-financement flex-column tooltips" data-id="${answer.id}"  data-toggle="tooltip" data-placement="bottom" data-original-title="${dataOriginalTitle}" data-html="true" style="opacity: 1;">
                        <span class="truncate">Subvention</span>
                        <span class="small"><b>${financedSubvention} / ${subvention}</b></span>
                    </a>`
				} else if (!isSubvention) {
					total = addSeparatorMillier(total) + " <i class='fa fa-euro'></i>";
					financingTotal = addSeparatorMillier(financingTotal) + " <i class='fa fa-euro'></i>";
					spentTotal = addSeparatorMillier(spentTotal) + " <i class='fa fa-euro'></i>";

					if (key == "budget") {
						dataOriginalTitle = `<span class='text-left'>
                        ${ucfirst(trad["Estimated budget"])} : ${total} </br>
                        </span>`;

						html = `
                        
                        <a href="javascript:;" id="btn-financing-2-${answer.id}" ${notEmpty(userConnected) ? "disabled" : ""} class="btn btn-default border-none btn-proposal-view btn-proposal-view-financement flex-column tooltips" data-id="${answer.id}"  data-toggle="tooltip" data-placement="bottom" data-original-title="${dataOriginalTitle}" data-html="true" style="opacity: 1;">
                            <span class="truncate"> ${ucfirst(trad["Estimated budget"])}</span>
                            <span class="small"><b>${total}</b></span>
                        </a>`
					} else {
						dataOriginalTitle = `<span class='text-left'>
                        ${ucfirst(trad.expenses)} : ${total} </br>
                        ${ucfirst(trad.financed)} : ${financingTotal} </br>
                        ${ucfirst(trad.spent)} : ${spentTotal}
                        </span>`;
						html = `
                        <a href="javascript:;" id="btn-financing-2-${answer.id}" ${notEmpty(userConnected) ? "disabled" : ""} class="btn btn-default border-none btn-proposal-view btn-proposal-view-financement flex-column tooltips" data-id="${answer.id}"  data-toggle="tooltip" data-placement="bottom" data-original-title="${dataOriginalTitle}" data-html="true" style="opacity: 1;">
                            <span class="truncate">${trad["Financing plan"]}</span>
                            <span class="small"><b>${financingTotal} / ${total} (${spentTotal})</b></span>
                        </a>`
					}
				}
			}
			return html
		}, propositionActionsStatus: function (aapObj, answer) {
			var html = "";
			if (notEmpty(answer.actions)) {
				var colorClass = "";
				var icon = "fa-thumb-tack";
				var actionCount = Object.keys(answer.actions).length;

				var dataOriginalTitle = `Actions : ${actionCount}`;
				var html = `
                <a href="javascript:;" id="btn-action-${answer.id}" disabled class="btn btn-default border-none btn-proposal-view tooltips" data-id="${answer.id}"  data-toggle="tooltip" data-placement="bottom" data-original-title="${dataOriginalTitle}" style="opacity: 1;">
                    ${actionCount} <i class='fa fa-2xx ${icon} ${colorClass}'></i>
                </a>`
			}
			return html;
		}, proposalDossiersCompletion: function (aapObj, answer) {
			var completeFields = 0;
			var fieldCount = 0;
			var percentage = 0;
			var color = "";
			var html = ``;
			if (notEmpty(answer.aapStep1) && notEmpty(aapObj.inputs)) {
				$.each(answer.aapStep1, function (k, v) {
					if (notEmpty(v)) {
						completeFields++;
					}
				})
				$.each(aapObj.inputs, function (k, v) {
					if (v?.step == "aapStep1" && exists(Object.values(aapObj.inputs)[0])) {
						var field = Object.values(aapObj.inputs)[0];
						fieldCount = Object.keys(field.inputs).length;
					}
				})
			}
			percentage = Math.round((completeFields * 100) / fieldCount);
			if (percentage < 50) {
				color = "text-red";
			}
			html = `
                <a href="javascript:;" id="completion-${answer.id}" disabled class="btn btn-default border-none btn-proposal-view flex-column tooltips" data-id="${answer.id}"  data-toggle="tooltip" data-placement="bottom" data-original-title="Dossiers" data-html="true" style="opacity: 1;">
                    <span class="truncate">Dossiers</span>
                    <span class="small"><b> <span class="${color}"> ${completeFields}  / ${fieldCount}(${percentage}%)</span></b></span>
                </a>`;
			return html;
		}, contributeur_template_html: function (aapObj, contributeurs = [], extraParams = {}) {
			return `
            <ul class="dropdown-menu contibutors-dropdownmenu">
            ${contributeurs.map(function (__map) {
				return `
                    <li>
                        <a class="col-xs-12 no-padding lbh-preview-element" href="#page.type.citoyens.id.${__map.id}">
                            <div class="col-xs-2">
                                <img src="${__map.image ? __map.image : defaultImage}" alt=""  width="25" height="25" style="border-radius: 100%;object-fit:cover"/>
                            </div>
                            <div class="col-xs-9 text-ellipsis">
                                <span><small>${__map.name}</small></span><br/>
                            </div>
                        </a>
                    </li>
                `;
			}).join('')}
            ${aapObj.canEdit && notEmpty(extraParams?.answerId) ? `
                <li>
                    <a class="col-xs-12 no-padding btn-invite-proposal dropup btn-inside-dropdown" href="javascript:;" data-answerid="${extraParams?.answerId}" data-projectid="${extraParams?.projectId}">
                        <div class="col-xs-2">
                            <i class="fa fa-user-plus a-icon-banner"></i>
                        </div>
                        <div class="col-xs-9 text-ellipsis">
                            <span><small>${trad["Invite"]}</small></span><br/>
                        </div>
                    </a>
                </li>` : ``}
            </ul>
            `;
		}, buildStatus_template_html: function (aapObj, proposalStatus, answerId = null, statusInfo = {}, usersStatus = {}) {
			var proposition_status = "";
			const answer_status_type = typeof proposalStatus;
			switch (answer_status_type) {
				case 'undefined':
					proposition_status = `(${trad["No status"]})`
					break;
				case 'object':
					if (proposalStatus[proposalStatus.length - 1]) {
						let tempUser = userConnected && userConnected?.["_id"]?.["$id"] ? { [userConnected?.["_id"]?.["$id"]]: userConnected } : {};
						proposalStatus.forEach((statVal, statIndex) => {
							let statusInfoObj = {
								html: ``, date: ''
							};
							if (aapObj.status?.[statVal]) {
								if (statusInfo && typeof statusInfo[statVal] != 'undefined') {
									const statValDateType = typeof statusInfo[statVal].updated;
									switch (statValDateType) {
										case 'number':
											statusInfoObj.date = new Date(statusInfo[statVal].updated * 1000).toLocaleString('fr', aapObj.dateStringOptions)
											break;
										case 'string':
											break;
										case 'object':
											if (typeof statusInfo[statVal].updated.sec != 'undefined' && typeof statusInfo[statVal].updated.usec != 'undefined') {
												statusInfoObj.date = new Date((statusInfo[statVal].updated.sec * 1 + (statusInfo[statVal].updated.usec * 1)) * 1000).toLocaleString('fr', aapObj.dateStringOptions)
											}
											break;
										default:
											statusInfoObj.date = '(Non renseigné)'
											break;
									}
								}

								if (statusInfo[statVal]) {
									if (typeof statusInfo?.[statVal]?.user != "undefined" && notEmpty(statusInfo?.[statVal]?.user) && !tempUser[statusInfo[statVal].user] && !usersStatus[statusInfo[statVal].user]) {
										ajaxPost(null, baseUrl + "/co2/person/getusermininfo/type/citoyens/id/" + statusInfo?.[statVal]?.user, {}, function (citoyenRes) {
											if (citoyenRes?.["_id"]?.["$id"]) {
												tempUser[citoyenRes?.["_id"]?.["$id"]] = citoyenRes
												usersStatus[citoyenRes?.["_id"]?.["$id"]] = citoyenRes
											}
										}, null, null, { async: false })
									}
									if (typeof statusInfo?.[statVal]?.user != "undefined" && notEmpty(statusInfo?.[statVal]?.user) && tempUser[statusInfo[statVal].user]) {
										usersStatus[statusInfo?.[statVal]?.user] = tempUser[statusInfo[statVal].user]
									}
									statusInfoObj.html += `<small>Fait par : ${statusInfo[statVal].user && usersStatus[statusInfo[statVal].user] ? usersStatus[statusInfo[statVal].user].name : '(Non renseigné)'}</small></br>`;
									statusInfoObj.html += `<small>${ucfirst(trad['on '])} : ${statusInfoObj.date}</small>`
								}
								proposition_status += `<span class="tooltips" data-toggle="tooltip" data-html="true" data-placement="top" data-original-title="${statusInfoObj.html}">` + (trad[statVal] ? trad[statVal] : statVal) + '</span> ';
								if (statIndex < proposalStatus.length - 1 && proposalStatus?.[statIndex + 1] && aapObj.status?.[proposalStatus?.[statIndex + 1]]) {
									proposition_status += ', '
								}
							}
						})
					} else if (proposalStatus.length == 0) {
						proposition_status = `(${trad["No status"]})`
					} else {
						proposition_status = `(${trad["No status"]}).`
					}
					break;
				default:
					proposition_status = trad[proposalStatus] ? trad[proposalStatus] : proposalStatus
					break;
			}
			return html = `
                <b>  ${proposition_status} </b>
                ${aapObj.canEdit ? (`<span class="changeStatus dropup">
                        <a href="javascript:;" class="ml-1 dropdown-toggle" data-toggle="dropdown" data-id="${answerId}" data-actualstatus="${typeof proposalStatus != 'undefined' && proposalStatus[proposalStatus.length - 1] ? proposalStatus.join(',') : ''}" role="button" data-users="${JSON.stringify(usersStatus).replace(/"/g, '\'')}" aria-haspopup="true" aria-expended="false" data-toggle-second="tooltip" data-placement="top" data-original-title="Modifier status"><i class="fa fa-plus-circle"></i></a>
                    </span>`) : ''}
            `;
		}, buildProjectStatus_template_html: function (aapObj, avancement, projectId = null) {
			return html = `
                <div class="proposition-alert ${aapObj.avancementProjectData.badgeColor?.[avancement] ? aapObj.avancementProjectData.badgeColor?.[avancement] : "in-progress"} ${aapObj.canEdit == true ? "cursor-pointer avancement-dropdown" : ""}" ${aapObj.canEdit ? "data-toggle='dropup' data-avancement='" + avancement + "' data-id='" + projectId + "'" : ""}>
                    <span class="${aapObj.avancementProjectData.icon?.[avancement] ? aapObj.avancementProjectData.icon?.[avancement] : "fa fa-cogs"}"></span> ${avancement ? trad[avancement] ? trad[avancement] : avancement : trad["In progress"]}
                </div>
            `;
		}, inviteToProposalDropdown_template_html: function (aapObj, answerId, container = "", callBack = () => { }, projectId = "") {
			// coInterface.showLoader(container, trad.currentlyloading);
			coInterface.showCostumLoader(container)
			var url = "";
			let allFormInputs = {};
			let inviteInputObj = {
				step: "", input: ""
			}
			let hasInviteInput = false;
			if (aapObj?.inputs) {
				for ([inputIndex, inputVal] of Object.entries(aapObj.inputs)) {
					if (inputVal.inputs) {
						for ([index, value] of Object.entries(inputVal.inputs)) {
							if (value.type && value.type == "tpls.forms.aap.invite") {
								inviteInputObj.step = inputVal.step ? inputVal.step : "aapStep1";
								inviteInputObj.input = index
								hasInviteInput = true
							}
						}
						allFormInputs = { ...allFormInputs, ...inputVal.inputs }
					}
				}
			}
			allFormInputs.invite ? hasInviteInput = true : ""
			var postPayload = null;
			if (notEmpty(projectId)) {
				if (notEmpty(answerId)) {
					url = baseUrl + `/survey/answer/answer/project/${projectId}/step/${(inviteInputObj.step != '' ? inviteInputObj.step : 'aapStep1')}/isinsideform/false/input/${(inviteInputObj.input != "" ? inviteInputObj.input : "invite")}`;
				} else {
					url = baseUrl + '/costum/project/action/request/invite_to_project_html/';
					postPayload = {
						project: projectId
					}
				}
			} else {
				url = baseUrl + `/survey/answer/answer/id/${answerId}/step/${inviteInputObj.step != "" ? inviteInputObj.step : "aapStep1"}/isinsideform/false/input/${inviteInputObj.input != "" ? inviteInputObj.input : "invite"}`;
			}
			setTimeout(function () {
				if (hasInviteInput) {
					ajaxPost(null, url, postPayload, function (data) {
						$(container).empty().append(`<h4 class="title"> ${trad["Invite people to contribute"]} </h4>`).append(data)
						callBack()
					}, null, null);
				} else {
					$(container).empty().append(`<h4 class="title text-center"> ${trad["The form does not have an assigned a contributor input"]} </h4>`)
				}
			});
		}, statusDropdown_template_html: function (aapObj, actualStatus, status = [], answerId = null, usersStatus = {}) {
			var html = `<ul class="statusDropdown dropdown-menu custom-scroll-bar row" style="max-height: 250px; overflow-y: auto">`;
			html += `<li class='col-xs-12 dropdown-header'>Status actuel</li>`, actualStatus.forEach((actuStatItem, actuStatIndex) => {

				if (actuStatItem != '' && actuStatItem != null) {
					if (aapObj.status?.[actuStatItem]) {
						html += `
                            <li class="col-xs-12 no-padding">
                                <a href="javascript:;" class="editStatus" data-action="remove" data-users="${JSON.stringify(usersStatus).replace(/"/g, '\'')}" data-id="${answerId}" data-value="${actuStatItem}" data-path="${actuStatIndex}">
                                    <span class="d-flex justify-content-between">
                                        <small class="text-ellipsis">${trad[actuStatItem] ? trad[actuStatItem] : actuStatItem}</small>
                                        <small class="text-red"><i class="fa fa-times-circle-o"></i></small>
                                    </span>
                                </a>
                            </li>
                        `;
					}
				} else if (actualStatus.length == 0) {
					html += `
                        <li class="text-center no-padding">
                            <small>(${trad["No status"]})</small>
                        </li>
                    `
				}

			});
			html += `<li class="col-xs-12 divider"> </li>`;
			html += `<li class="col-xs-12 dropdown-header">Status possible</li>`;
			status.forEach(statusItem => {
				if (actualStatus.indexOf(statusItem) < 0) {
					html += `
                        <li class='col-xs-12 no-padding'>
                            <a href="javascript:;" class="editStatus" data-users="${JSON.stringify(usersStatus).replace(/"/g, '\'')}" data-action="add" data-id='${answerId}' data-value='${statusItem}' data-path="">
                                <span class="d-flex justify-content-between">
                                    <small class="text-ellipsis">${trad[statusItem] ? trad[statusItem] : statusItem}</small>
                                    <small class="parent-link"><i class="fa fa-plus-circle"></i></small>
                                </span>
                            </a>
                        </li>
                    `;
				}
			})
			return html += '</ul>';
		}, avancementDropdown_template_html: function (aapObj, actualAvancement, avancements, projectId) {
			var html = `<ul class="avancementDropdown dropdown-menu custom-scroll-bar row" style="max-height: 250px; overflow-y: auto">`;
			html += `<li class='col-xs-12 dropdown-header'>Changer status</li>`, avancements.forEach((avancementItem, avancementIndex) => {
				if (avancementItem != '' && avancementItem != null && avancementItem != actualAvancement) {
					html += `
                        <li class="col-xs-12 no-padding">
                            <a href="javascript:;" class="editProjectStatus" data-action="remove" data-id="${projectId}" data-value="${avancementItem}" data-path="${avancementIndex}">
                                <span class="d-flex justify-content-between">
                                    <small class="text-ellipsis">
                                        ${aapObj.avancementProjectData.icon?.[avancementItem] ? `<i class="${aapObj.avancementProjectData.icon?.[avancementItem]} mr-2 pr-1"></i>` : "<i class='fa fa-cogs mr-2 pr-1'></i>"}
                                        ${trad[avancementItem] ? trad[avancementItem] : avancementItem}
                                    </small>
                                    <small class="parent-link"><i class="fa fa-plus-circle"></i></small>
                                </span>
                            </a>
                        </li>
                    `;
				}

			});
			return html += '</ul>';
		}, updateStatus: function (aapObject, action = 'add', statusObj = { id: '', value: '', path: 0 }, usersStatus = {}) {
			var objToSend = {};
			objToSend.id = statusObj.id;
			objToSend.collection = "answers";
			objToSend.value = statusObj.value;
			if (action == 'add') {
				objToSend.path = "status";
				objToSend.arrayForm = true
			} else {
				objToSend.path = "status." + statusObj.path;
				objToSend.pull = "status";
				objToSend.value = null;
			}
			$(`#status-${statusObj.id}.statusView`).empty().animate(600);
			$(`#status-${statusObj.id}.statusView`).append(`<span class='mx-4'><i class="fa fa-spin fa-circle-o-notch"></i></span>`);
			dataHelper.path2Value(objToSend, function (statusParams) {
				if (action == 'add') {
					ajaxPost("", baseUrl + '/survey/answer/rcnotification/action/newstatus/answerid/' + objToSend.id, {
						status: [objToSend.value],
					}, function (data) {
					}, "html");
					aapObject.bellNotification.proposal.updateStatus(objToSend.id);
				}

				objToSend.arrayForm ? delete objToSend.arrayForm : "";
				objToSend.path = "statusInfo." + statusObj.value;
				objToSend.value = {
					user: userConnected['_id']['$id'],
					action: action,
					updated: new Date().toLocaleString('fr', { ...aapObject.dateStringOptions, timeZone: 'UTC', timeZoneName: 'short', })
				};
				objToSend.setType = [{
					"path": "updated", "type": "isoDate"
				}]
				dataHelper.path2Value(objToSend, function (params) {
					$(`#status-${statusObj.id}.statusView`).empty()
					if (userConnected && userConnected["_id"] && userConnected["_id"]["$id"] && !usersStatus[userConnected["_id"]["$id"]]) {
						usersStatus[userConnected["_id"]["$id"]] = {
							name: userConnected["name"],
							collection: "citoyens",
							slug: userConnected.slug,
							'_id': { '$id': userConnected["_id"]["$id"] }
						}
					}
					$(`#status-${statusObj.id}.statusView`).append(aapObj.directory.buildStatus_template_html(aapObj, params && params.elt && params.elt.status ? params.elt.status : undefined, objToSend.id, params && params.elt && params.elt.statusInfo ? params.elt.statusInfo : {}, usersStatus)).animate(300);
					aapObj.events.proposalStatus(aapObj);
					toastr.success(trad.saved);
					params.usersStatus = usersStatus;
					if (statusParams?.elt?.status && statusParams?.elt?.status[statusParams?.elt?.status.length - 1] && statusParams?.elt?.status[statusParams?.elt?.status.length - 1] == "projectstate") {
						ajaxPost(null, typeof coWsConfig != "undefined" && coWsConfig.pingRefreshViewUrl ? coWsConfig.pingRefreshViewUrl : null, {
							action: "reloadProposal",
							answerId: objToSend.id, userSocketId: aapObj.userSocketId, responses: params,
						}, null, null, { contentType: 'application/json' });
					} else {
						ajaxPost(null, typeof coWsConfig != "undefined" && coWsConfig.pingRefreshViewUrl ? coWsConfig.pingRefreshViewUrl : null, {
							action: "statusUpdated",
							answerId: objToSend.id, userSocketId: aapObj.userSocketId, responses: params,
						}, null, null, { contentType: 'application/json' });
					}
				})
			});
		}, updateProjectStatus: function (aapObject, statusObj = { id: '', value: '', path: 0 }) {
			var objToSend = {};
			objToSend.id = statusObj.id;
			objToSend.collection = "projects";
			objToSend.value = statusObj.value;
			objToSend.path = statusObj.path;
			$(`#projectstatus-${statusObj.id}.project-status`).empty().animate(600);
			$(`#projectstatus-${statusObj.id}.project-status`).append(`<span class='mx-4'><i class="fa fa-spin fa-circle-o-notch"></i></span>`);
			dataHelper.path2Value(objToSend, function (params) {
				// ajaxPost("", baseUrl + '/survey/answer/rcnotification/action/newstatus/answerid/' + objToSend.id, {
				//     status : [objToSend.value],
				// }, function (data) {
				// }, "html");
				$(`#projectstatus-${statusObj.id}.project-status`).empty().append(aapObj.directory.buildProjectStatus_template_html(aapObj, params && params.elt && params.elt?.properties?.avancement ? params.elt.properties?.avancement : undefined, objToSend.id,)).animate(300);
				aapObj.events.projectAvancement(aapObj);
				toastr.success(trad.saved);
			});
		}, reloadAapProposalItem: function (aapObj, answerId) {
			var html = "";
			var answer = aapObj.common.getOneAnswer(aapObj, answerId);
			answer = aapObj.prepareData.proposal(aapObj, answer["results"][answerId], answer);

			if ($(`#proposal-container-detailed-${answer.id}`).length != -1) {
				html = aapObj.directory.proposition_template_html_detailed(aapObj, answer);
				$(`#proposal-container-detailed-${answer.id}`).replaceWith(html);
			}
			if ($(`#proposal-container-compact-${answer.id}`).length != -1) {
				html = aapObj.directory.proposition_template_html_compact(aapObj, answer);
				$(`#proposal-container-compact-${answer.id}`).replaceWith(html);
			}

			aapObj.events.proposal(aapObj);
			aapObj.events.proposalStatus(aapObj);
			aapObj.events.proposalDropdown(aapObj);
			coInterface.bindLBHLinks();
		}, reloadAapProjectItem: function (aapObj, projectId) {
			var html = "";
			var project = aapObj.common.getOneProject(aapObj, projectId);
			project = aapObj.prepareData.project(aapObj, project["results"][projectId], project);

			if ($(`#project-item-compact-${project.id}`).length != -1) {
				html = aapObj.directory.project_template_html_compact(aapObj, project);
				$(`#project-item-compact-${project.id}`).replaceWith(html);
			}
			aapObj.events.projects(aapObj);
			coInterface.bindLBHLinks();
		}, reloadAapMiniItem: function (aapObj, answerId) {
			var html = "";
			var prop_item_dom = $(`#propItem${answerId}`);

			if (prop_item_dom.length > 0) {
				var answer = aapObj.common.getOneAnswer(aapObj, answerId);
				answer = aapObj.prepareData.proposal(aapObj, answer["results"][answerId], answer);
				html = aapObj.directory.proposition_template_html_mini(aapObj, answer);
				prop_item_dom.replaceWith(html);

				var queryParams = aapObj?.common?.getQuery();
				var findInterval = null;
				const answerItemId = queryParams?.['answerId'] ? queryParams?.['answerId'] : null;
				$('.props-item.active').removeClass('active');
				$("#dropdown_search_detail").find("#propItem" + answerItemId).addClass("active");
				aapObj.events.customTooltip()
				aapObj.events.bindDetailProposalClickEvent(aapObj)
			}
		}, aapExceltable: function (params, smartGrid, data, aapObj) {
			const url = `${baseUrl}/co2/aap/exceltable`;
			const post = {
				inputs: Object.keys(data.inputs), answers: Object.keys(data.results), form: params.form._id.$id
			};
			const hot_interval = setInterval(function () {
				if (typeof Handsontable !== 'undefined') {
					clearInterval(hot_interval);
					ajaxPost(null, url, post, function (_response) {
						setTimeout(function () {
							$('#dropdown_search').empty().html(_response);
						}, 1000);
					});
				}
			});
			return ``;
		}, listItemCoFormButtons: function (aapObj, answerId, useranswer = false) {
			socialBtn = '';
			var p_active = false, adminRight = aapObj.canEdit, canEditEachotherAnswer = aapObj?.canParticipate,
				canReadEachOtherAnswer = aapObj?.canSee;
			switch (aapObj?.session?.status) {
				case "include":
					p_active = true;
					break;
				case "late":
					p_active = false;
					break;
				case "past":
					p_active = false;
					break;
				default:
					p_active = false;
					break;
			}

			if (userId && (p_active || adminRight)) {
				var btnclass = "getanswer", btndataw = `data-mode='w' data-ansid='${answerId}'`,
					btndatar = `data-mode='r' data-ansid='${answerId}'`,
					btnpdfclass = "exportanswer", btndeleteclass = "deleteanswer", btndatatype = "";
				socialBtn = `
                  <div class="col-md-12">
                    <div class="social-links justify-content-center">
                      ${adminRight || canEditEachotherAnswer || (!canEditEachotherAnswer && useranswer) ? (`<div class="social-btn flex-center ${btnclass}" ${btndataw} ${btndatatype}>
                            <i class="fa fa-pencil-square-o editdeleteicon"></i>
                            <span style="font-size: 14px;"> ${trad.edit} </span>
                          </div>`) : ''}
    
                        ${adminRight || canReadEachOtherAnswer || (!canReadEachOtherAnswer && useranswer) ? (`<div class="social-btn flex-center ${btnclass}" ${btndatar}>
                                <i class="fa fa-sticky-note-o editdeleteicon"></i><span style="font-size: 14px;">${trad.read}</span>
                            </div> 
                            ${adminRight ? (`<div class="social-btn flex-center ${btndeleteclass} " ${btndataw}>
                                    <i class="fa fa-trash-o editdeleteicon" style="color: #ff5722"></i><span style="font-size: 14px;">${trad.delete}</span>
                                </div>`) : ''}
                            `) : ''


					}
                      </div>
                    </div>`
			}
			return socialBtn;
		}, coformCompactHtml: function (aapObj, answer) {
			answerNameTitle = '';
			if (aapObj.form?.params?.inputForAnswerName?.title) {
				const answerName = aapObj.form?.params?.inputForAnswerName?.title;
				if (answer.answers) {
					Object.values(answer["answers"]).forEach((itemOAnswer, indexOAnswer) => {
						if (itemOAnswer[answerName]) {
							answerNameTitle = itemOAnswer[answerName];
						}
					})
				}
			}
			var html = `
                <div class="col-xs-12 compact-parent col-md-4 col-sm-6" id="list-${answer.id}" data-id="${answer.id}">
                    <div class="div compact-container padding-compact-coform ${aapObj.directory.answerNotViewMarker(aapObj, answer)}" data-label="nouv.">
                        ${answerNameTitle != "" ? `<h6>${answerNameTitle}</h6>` : ''}
                        <small>${ucfirst(trad.by)}
                            <a href="#page.type.${answer.user.collection}.id.${answer.user.id}" class="lbh-preview-element">
                                <span class="depositor">
                                    <img src="${answer.user.profilImageUrl}" alt="${answer.user.name}" class="depositor-image mr-2">
                                        <b>${answer.user.name}</b>
                                </span>
                            </a>
                        </small>
                        <small>
                            Le  <i class="fa fa-calendar-o" style="color: #7da53d"></i> 
                            <b>
                                <i>
                                    ${new Date(answer.created * 1000).toLocaleString('fr', {
				day: 'numeric', month: 'numeric', year: 'numeric'
			})}
                                </i>
                            </b>
                        </small>
                        <div class="margin-top-10" style="position: relative;display: flex;justify-content: center;width: 100%;font-size: 23px;">
                            ${aapObj.directory.listItemCoFormButtons(aapObj, answer.id, userId && answer.user.id != userId)}
                        </div>
                        <div class="margin-top-10" style="position: relative;display: flex;justify-content: center;width: 100%;font-size: 23px;">
                            ${userId ? aapObj.directory.propositionFavoris(aapObj, answer) + `<a href="javascript:;" id="btn-comment-${answer.id}" class="btn margin-right-5 openAnswersComment tooltips btn-custom" onclick="commentObj.openPreview('answers','${answer.id}','','${answer.title ? answer.title.replace(/'/g, "\\'") : 'Commentaires'}')" data-toggle="tooltip" data-placement="bottom" data-original-title="Ajouter un commentaire" style="font-size :23px;border:0">
                                    ${notEmpty(answer.commentCount) ? answer.commentCount : ""} <i class='fa fa-commenting fa-2x'></i>
                                </a>` : ''}
                        </div>
                    </div>
                </div>
            `;
			return html
		}, proposalObservatory: function (aapObj, answers) {
			return "<h1>En cours de Dev</h1>";
		}, proposalThematique: function (aapObj, answer) {
			let themeHtml = ""
			if (typeof answer.tags == 'object' && answer.tags != null) {
				themeHtml = `<div class="col-xs-12 d-flex flex-xs-wrap flex-sm-wrap">`;
				answer.tags.forEach(tagItem => {
					themeHtml += `<small class="badge-tag tag-theme m-1"> ${tagItem} </small>`
				})
				themeHtml += '</div>'
			}
			return themeHtml
		}
	},
	paramsFilters: {
		proposal: function (aapObj, elmtContainer = "#filterContainerL", isAap = true, toBeResized = true, isInsideModal = false) {
			if (typeof userId != 'undefined' && userId) {
				ajaxPost("", baseUrl + "/co2/person/getusersavedpref/type/citoyens/id/" + userId, { "appKey": "savedFilters" }, function (res) {
					aapObj.userSavedFilters = {};
					if (res[aapObj.form._id['$id']]) {
						aapObj.userSavedFilters = res[aapObj.form._id['$id']];
					}
				}, null, null, {
					async: false
				});
			}
			var paramsFilterL = {
				container: elmtContainer,
				urlData: baseUrl + "/co2/aap/directoryproposal/source/" + (notEmpty(costum) ? costum.slug : contextData.slug) + "/form/" + aapObj.form._id.$id,
				interface: {
					events: {
						page: true, //scroll: true,
						//scrollOne : true
					}
				}, /*loadEvent: {
				 default: "scroll"
				 },*/
				filters: isAap ? ({
					text: {
						view: "text",
						field: "answers.aapStep1.titre",
						event: "text",
						placeholder: trad.searchBy + " " + trad.projectName.toLocaleLowerCase(),
						icon: "fa-search",
						customClass: "set-width visible-xs to-sticky to-set-value",
						inputAlias: ".filter-alias .main-search-bar-addon"
					}, usersFilter: {}, theme: {
						view: "megaMenuAccordion",
						type: "filters",
						field: "answers.aapStep1.tags",
						remove0: true,
						countResults: true,
						activateCounter: true,
						countFieldPath: "answers.aapStep1.tags",
						classDom: "scrollable-row",
						classList: "label-text-to-ellipsis",
						remove0: false,
						name: ucfirst(trad["search by theme"]),
						event: "filters",
						list: exists(aapObj.form["params"]?.["tags"]?.["list"]) ? aapObj.form["params"]?.["tags"]?.["list"] : {}
					}, acceptation: {
						view: "accordionList",
						type: "filters",
						field: "acceptation",
						name: `${trad.retained} ${trad.or} ${trad.rejected}`,
						event: "filters",
						classDom: "scrollable-row",
						classList: "label-text-to-ellipsis",
						keyValue: false,
						list: {
							"retained": trad.retained, "rejected": trad.rejected, "pending": trad.pending
						}
					}, choosen: {
						view: "accordionList",
						type: "filters",
						field: "answers.aapStep2.choose." + costum?.contextId + ".value",
						name: `Sélectionnée / Non sélectionnée`,
						event: "filters",
						classDom: "scrollable-row",
						classList: "label-text-to-ellipsis",
						keyValue: false,
						list: {
							"selected": "Séléctionnée", "notselected": "No séléctionnée"
						}
					}, favorite: {
						view: "accordionList",
						type: "filters",
						field: "vote",
						name: trad.Favorites,
						event: "filters",
						classDom: "scrollable-row",
						classList: "label-text-to-ellipsis",
						keyValue: false,
						list: {
							[userId]: trad.Favorites,
						}
					}, seen: {
						view: "accordionList",
						type: "filters",
						field: "views",
						name: trad.notSeen + "/" + trad.Seen,
						event: "filters",
						classDom: "scrollable-row",
						classList: "label-text-to-ellipsis",
						keyValue: false,
						list: {
							"notSeen": trad.notSeen, "seen": trad.Seen,
						}
					}, urgency: {
						view: "accordionList",
						type: "filters",
						field: "answers.aapStep1.urgency",
						name: "Urgence",
						event: "filters",
						classDom: "scrollable-row",
						classList: "label-text-to-ellipsis",
						list: {
							"Urgent": "Urgent"
						}
					}, status: {
						view: "accordionList",
						type: "filters",
						field: "status",
						name: trad.Status,
						event: "filters",
						keyValue: false,
						classDom: "scrollable-row",
						classList: "label-text-to-ellipsis",
						list: aapObj.status
					}, inproject: {
						view: "accordionList",
						type: "filters",
						field: "inproject",
						name: trad.projectstate + "/" + trad.proposal,
						event: "filters",
						keyValue: false,
						classDom: "scrollable-row",
						classList: "label-text-to-ellipsis",
						list: {
							inproject: trad.projectstate, inproposal: trad.proposal
						}
					}, sortBy: {
						view: "accordionList",
						type: "sortBy",
						name: trad.sortby,
						event: "sortBy",
						classDom: "scrollable-row",
						classList: "label-text-to-ellipsis",
						isRadioButton: {
							"answers.aapStep1.titre": ["answers.aapStep2.allVotes", "created", "updated"],
							"answers.aapStep2.allVotes": ["answers.aapStep1.titre", "answers.aapStep2.allVotes", "created", "updated"],
							"created": ["answers.aapStep1.titre", "answers.aapStep2.allVotes", "created", "updated"],
							"updated": ["answers.aapStep1.titre", "answers.aapStep2.allVotes", "created", "updated"]
						},
						list: {
							"Ordre alphabetique": {
								"answers.aapStep1.titre": 1,
							}, "Plus de votes": {
								"answers.aapStep2.allVotes": -1,
							}, "Moins de votes": {
								"answers.aapStep2.allVotes": 1,
							}, "Date de création croissant": {
								"created": 1,
							}, "Date de création décroissant": {
								"created": -1,
							}, "Date de mise à jour croissant": {
								"updated": 1,
							}, "Date de mise à jour décroissant": {
								"updated": -1,
							},
						}
					}
				}) : ({
					sortBy: {
						view: "accordionList",
						type: "sortBy",
						name: trad.sortBy,
						event: "sortBy",
						classDom: "scrollable-row",
						classList: "label-text-to-ellipsis",
						isRadioButton: {
							"created": ["created", "updated"],
							"updated": ["created", "updated"]
						},
						list: {
							"Date de création croissant": {
								"created": 1,
							}, "Date de création décroissant": {
								"created": -1,
							}, "Date de mise à jour croissant": {
								"updated": 1,
							}, "Date de mise à jour décroissant": {
								"updated": -1,
							},
						}
					}
				}),
				notSourceKey: true,
				defaults: {
					notSourceKey: true,
					indexStep: 20,
					types: ["answers"],
					forced: {
						filters: {}
					},
					filters: isAap ? ({
						'form': aapObj.form._id.$id, /*'updated' : {
						 '$exists' : true
						 },*/
						'answers.aapStep1.titre': {
							'$exists': true
						}
					}) : ({
						'form': aapObj.form._id.$id, /*'updated' : {
						 '$exists' : true
						 },*/
						'answers': {
							'$exists': true
						}
					}),
					sortBy: {
						"updated": -1
					},
					types: ["answers"],
					fields: [],
					tagsPath: isAap ? "answers.aapStep1.tags" : null,
					onResize: true,
					onResizeFunc: () => aapObj.common.switchFilter(),
					appendToXs: ".filterXs",
					appendToLg: "#filterContainerAapOceco",
				},
				aapFooter: {
					subdom: 'aapSaveFilters', class: 'd-flex justify-content-center mt-2', views: {
						fav: {
							view: "savedFilters",
							tooltips: "Filtre enregisté",
							app: "search",
							event: "saveActiveFilters",
							appKey: "savedFilters",
							class: "btn btn-aap-tertiary",
							concernedForm: aapObj.form["_id"]["$id"]
						}
					}
				},
				subHeader: {
					dom: 'aapResetActiveFilters', class: 'd-flex justify-content-end mt-1 mb-3', views: {
						resetActiveFilter: {
							view: "resetFilter",
							tooltips: trad["Reset search"],
							app: "search",
							label: trad["Reset search"],
							event: "resetFilter",
							class: "moreLinkaap btn-xs bg-transparent"
						}
					}
				},
				header: {
					dom: ".headerSearchContainerL", options: {
						left: {
							classes: "col-xs-12 no-padding", group: {
								count: true, //sendNotification : true
							}
						}
					}, views: {
						map: function (fObj, v) {
							/*return  '<button class="btn-show-map-search pull-right" style="" title="'+trad.showmap+'" alt="'+trad.showmap+'">'+
							 '<i class="fa fa-map-marker"></i> '+trad.map+
							 '</button>';*/
							return "";
						}, sendNotification: function (fObj, v) {
							return `<button class="btn btn-default padding-left-15 communication-tools" style="" >
                                        <i class="fa fa-email"></i> Envoyer notification
                                    </button>`;
						}

					}
				},
				footerDom: ".footerSearchContainerL",
				lastSearchDone: {},
				events: {},
				results: {
					dom: "#dropdown_search", smartGrid: false, renderView: "aapObj.directory.aapProposalDetailed", map: {
						sameAsDirectory: true, showMapWithDirectory: false
						//active : true,
					}, events: function () {
						$("#dropdown_search .processingLoader").remove();
						if (isAap) {
							aapObj.events.proposal(aapObj);
							aapObj.events.proposalStatus(aapObj);
							aapObj.events.proposalDropdown(aapObj);
							aapObj.events.exceltable(aapObj);
						} else {
							aapObj.events.proposalCollapseItems(aapObj, false);
						}
						coInterface.bindLBHLinks();
					}
				}
			};

			if (aapObj.userSavedFilters && Object.keys(aapObj.userSavedFilters).length > 1) {
				Object.assign(paramsFilterL.filters, {
					usersFilter: {
						view: "accordionListSavedFilter",
						name: trad.savedFilter,
						event: "activeSavedFilters",
						list: aapObj.userSavedFilters,
						typeList: "arrayOfObjects",
						tooltip: "bottom",
						appKey: "savedFilters",
						concernedForm: aapObj.form["_id"]["$id"]
					}
				})
			} else {
				if (paramsFilterL.filters.usersFilter) {
					delete paramsFilterL.filters.usersFilter
				}
			}

			if (notNull(costum) && typeof window[costum.slug + "Obj"] != "undefined") {
				var scecificConfig = window[costum.slug + "Obj"];
				paramsFilterL.filters = scecificConfig.paramsFilters(scecificConfig, aapObj.form);
				paramsFilterL.defaults = scecificConfig.defaultFilters(scecificConfig);
			}

			if (notEmpty(aapObj.aapview)) {
				switch (aapObj.aapview) {
					case "detailed":
						paramsFilterL.results.renderView = "aapObj.directory.aapProposalDetailed"
						break;
					case "minimalist":
						paramsFilterL.results.renderView = "aapObj.directory.aapProposalCompact"
						break;
					case 'table':
						paramsFilterL.results.renderView = {
							single: true, view: 'aapObj.directory.aapExceltable'
						};
						paramsFilterL.defaults.indexStep = 'all';
						break;
					case 'coform-compact':
						paramsFilterL.results.renderView = "aapObj.directory.coformAnswerCompact"
						break;
					default:
						paramsFilterL.results.renderView = "aapObj.directory.aapProposalDetailed"
						break;
				}
			}

			if (toBeResized) {
				$(".filterXs").remove();
				if ($("#menu-top-profil-social").is(':visible') && !isInsideModal) {
					if ($("#menu-top-btn-group").is(":visible")) {
						$("#menu-top-profil-social #menu-top-btn-group").append(`<div class="filterXs visible-xs menu-xs-cplx pull-left"><span class="btnCloseMenuFilter" style="width : 99vw; position : absolute; top: 9rem; left: 0; z-index : 3; font-size: 2.5rem; display: none"><i class="fa fa-times pull-right mr-5"></i></span></div>`)
					}
				} else {
					if (isInsideModal) {
						$("#dialogContent .modal-custom-dialog .modal-content").prepend(`<div class="filterXs visible-xs menu-xs-cplx pull-left"><span class="btnCloseMenuFilter" style="width : 99vw; position : absolute; top: 9rem; left: 0; z-index : 3; font-size: 2.5rem; display: none"><i class="fa fa-times pull-right mr-5"></i></span></div>`);
					} else {
						$("#menuTopLeft").append(`<div class="filterXs visible-xs menu-xs-cplx pull-left"><span class="btnCloseMenuFilter" style="width : 99vw; position : absolute; top: 9rem; left: 0; z-index : 3; font-size: 2.5rem; display: none"><i class="fa fa-times pull-right mr-5"></i></span></div>`);
					}
				}
			}
			aapObj.hasFilter = true;
			return paramsFilterL;
		}, proposalDetail: function (aapObj, isInsideModal = false) {
			var paramsFilterL = aapObj.paramsFilters.proposal(aapObj, "#filterContainerD", true, true, isInsideModal);
			// if (paramsFilterL.filters?.text) {
			//     paramsFilterL.filters.text.customClass ? paramsFilterL.filters.text.customClass += ' display-none' :
			// paramsFilterL.filters.text.customClass = 'd-none' }
			$('.pdf-download:not(.disable)').off('click').on('click', function () {
				var self = $(this);
				smallMenu.openAjaxHTML(baseUrl + '/co2/aap/getviewbypath/path/costum.views.custom.coSindniSmarterre.aap.exportation-pdf-cosindnismarterre/params/' + self.data('answer'));
				if (isInsideModal) {
					var onShownInterv = setInterval(() => {
						if ($(".portfolio-modal.modal").is(":visible")) {
							$(".portfolio-modal.modal").css("z-index", 100002)
							clearInterval(onShownInterv);
							onShownInterv = null
						}
					}, 100);
				}
			});
			$(".filterXs.visible-xs").append(`
                <div class="col-xs-12 propsAndFilter no-padding">
                    <div class="visible-xs">
                        <button type="button" class="btn btn-default showHide-filters-xs" id="show-list-xs" style="width: 3rem">
                            <i class="fa fa-sliders"></i>
                        </button>
                    </div>
                    <div id="propsFilterTab" class="xs-dropdown-tab">
                        <span class="btnCloseMenuList" style="position : absolute; top: 0.5rem; right: 1rem; z-index : 3; font-size: 2.5rem; display: none"><i class="fa fa-times pull-right mr-2"></i></span>
                        <ul class="nav nav-tabs nav-panel-border" role="tablist">
                            <li class="active">
                                <a href="#proposition-menu-list" role="tab" data-toggle="tab">${trad["The proposals"]}</a>
                            </li>
                            <li class="">
                                <a href="#proposition-list-filter" id="filterXsTab" class="showHide-filters-xs" role="tab" data-toggle="tab">
                                    ${trad["Filters"]}
                                </a>
                            </li>
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div class="tab-pane aap-list-container active" id="proposition-menu-list">
                            </div>
                    
                            <div class="tab-pane aap-list-container padding-top-10" id="proposition-list-filter">
                    
                            </div>
                        </div>
                    </div>
                </div>
            `)
			paramsFilterL.defaults.indexStep = 20;
			paramsFilterL.defaults.scrollDom = "#listPropsPanel,#dropdown_search_detail";
			paramsFilterL.container = "#filterContainerD";
			paramsFilterL.header.dom = ".headerSearchContainerD";
			paramsFilterL.footerDom = ".footerSearchContainerD";
			paramsFilterL.defaults.appendToXs = "#proposition-list-filter";
			paramsFilterL.defaults.appendToLg = "#filterLCollapse";
			paramsFilterL.defaults.onResizeFunc = () => aapObj.common.switchList()
			paramsFilterL.interface = {
				events: {
					scroll: true, scrollOne: true
				}
			}
			paramsFilterL.loadEvent = {
				default: "scroll"
			}
			paramsFilterL.results = {
				dom: "#dropdown_search_detail", smartGrid: false, renderView: "aapObj.directory.aapProposalMini", map: {
					show: false
					//active : true,
				}, events: function () {
					aapObj.events.proposalDetail(aapObj);
					$("#proposalResultCount").html(aapObj.filterSearch?.proposalDetail?.results?.count?.answers && aapObj.filterSearch?.proposalDetail?.results?.count?.answers > 0 ? aapObj.filterSearch.proposalDetail.results.count.answers : "")
				}
			}
			// paramsFilterL.defaults.filters["answers.aapStep1.titre"] ? delete paramsFilterL.defaults.filters["answers.aapStep1.titre"] : "";
			return paramsFilterL;
		}, project: function (aapObj, elmtContainer = "#filterContainerL", isInsideModal = false) {
			if (typeof userId != 'undefined' && userId && userId != '') {
				ajaxPost("", baseUrl + "/co2/person/getusersavedpref/type/citoyens/id/" + userId, { "appKey": "savedFilters" }, function (res) {
					aapObj.userSavedFilters = {};
					if (res[aapObj.form._id['$id']]) {
						aapObj.userSavedFilters = res[aapObj.form._id['$id']];
					}
				}, null, null, {
					async: false
				});
			}
			var paramsFilterL = {
				container: elmtContainer,
				urlData: baseUrl + "/co2/aap/directoryproject/form/" + aapObj.form._id.$id + "/context/" + aapObj.context._id.$id,
				interface: {
					events: {
						page: true, //scroll: true,
						//scrollOne : true
					}
				}, /*loadEvent: {
				 default: "scroll"
				 },*/
				filters: {
					text: {
						view: "text",
						event: "text",
						placeholder: trad.searchBy + " " + trad.projectName.toLocaleLowerCase(),
						icon: "fa-search",
						customClass: "set-width visible-xs to-sticky to-set-value",
						inputAlias: ".filter-alias .main-search-bar-addon"
					}, avancement: {
						view: "accordionList",
						type: "filters",
						field: "properties.avancement",
						name: trad.Status,
						event: "filters",
						keyValue: false,
						list: avancementProject
					}, seen: {
						view: "accordionList",
						type: "filters",
						field: "views",
						name: trad.notSeen + "/" + trad.Seen,
						event: "filters",
						keyValue: false,
						list: {
							"notSeen": trad.notSeen, "seen": trad.Seen,
						}
					}, usersFilter: {}, sortBy: {
						view: "accordionList", type: "sortBy", name: trad.sortBy, event: "sortBy", list: {
							"Ordre alphabetique": {
								"name": 1,
							}, "Plus de votes": {
								"answers.aapStep2.allVotes": -1,
							}, "Moins de votes": {
								"answers.aapStep2.allVotes": 1,
							}, "Date de création croissant": {
								"created": 1,
							}, "Date de création décroissant": {
								"created": -1,
							}, "Date de mise à jour croissant": {
								"updated": 1,
							}, "Date de mise à jour décroissant": {
								"updated": -1,
							},
						}
					}
				},
				notSourceKey: true,
				defaults: {
					notSourceKey: true,
					indexStep: 20,
					types: ["projects"],
					forced: {
						filters: {}
					},
					filters: {},
					sortBy: {
						"updated": -1
					},
					types: ["projects"],
					fields: [],
					onResize: true,
					onResizeFunc: () => aapObj.common.switchFilter(),
					appendToXs: ".filterXs",
					appendToLg: "#filterContainerAapOceco",
					tagsPath: "answers.aapStep1.tags"
				},
				aapFooter: {
					subdom: 'aapSaveFilters', class: 'd-flex justify-content-center mt-2', views: {
						fav: {
							view: "savedFilters",
							tooltips: "Filtre enregisté",
							app: "search",
							event: "saveActiveFilters",
							appKey: "savedFilters",
							class: "btn btn-aap-tertiary",
							concernedForm: aapObj.form["_id"]["$id"]
						}
					}
				},
				header: {
					dom: ".headerSearchContainerL", options: {
						left: {
							classes: "col-xs-12 no-padding", group: {
								count: true, //map: true
							}
						}
					}, views: {
						map: function (fObj, v) {
							/*return  '<button class="btn-show-map-search pull-right" style="" title="'+trad.showmap+'" alt="'+trad.showmap+'">'+
							 '<i class="fa fa-map-marker"></i> '+trad.map+
							 '</button>';*/
							return "";
						}
					}
				},
				subHeader: {
					dom: 'aapResetActiveFilters',
					class: 'd-flex justify-content-end mt-1 mb-3',
					views: {
						resetActiveFilter: {
							view: "resetFilter",
							tooltips: trad["Reset search"],
							app: "search",
							label: trad["Reset search"],
							event: "resetFilter",
							class: "moreLinkaap btn-xs bg-transparent"
						}
					}
				},
				footerDom: ".footerSearchContainerL",
				lastSearchDone: {},
				events: {},
				results: {
					dom: "#dropdown_search", smartGrid: false, renderView: "aapObj.directory.projectDetailed", map: {
						sameAsDirectory: true,
						showMapWithDirectory: false
						//active : true,
					},
					events: function () {
						$("#dropdown_search .processingLoader").remove();
						aapObj.events.projects(aapObj);
						aapObj.events.projectDropdown(aapObj);
						coInterface.bindLBHLinks();
					}
				}
			};
			if (aapObj.userSavedFilters && Object.keys(aapObj.userSavedFilters).length > 1) {
				Object.assign(paramsFilterL.filters, {
					usersFilter: {
						view: "accordionListSavedFilter",
						name: trad.savedFilter,
						event: "activeSavedFilters",
						list: aapObj.userSavedFilters,
						typeList: "arrayOfObjects",
						tooltip: "bottom",
						appKey: "savedFilters",
						concernedForm: aapObj.form["_id"]["$id"]
					}
				})
			} else {
				if (paramsFilterL.filters.usersFilter) {
					delete paramsFilterL.filters.usersFilter
				}
			}
			if (notEmpty(aapObj.aapview)) {
				switch (aapObj.aapview) {
					case "detailed":
						paramsFilterL.results.renderView = "aapObj.directory.projectDetailed";
						break;
					case "minimalist":
						paramsFilterL.results.renderView = "aapObj.directory.projectCompact";
						break;
					default:
						paramsFilterL.results.renderView = "aapObj.directory.projectDetailed";
						break;
				}
			}
			$(".filterXs").remove()
			if (isInsideModal) {
				$("#dialogContent .modal-custom-dialog .modal-content").prepend(`<div class="filterXs visible-xs menu-xs-cplx pull-left"><span class="btnCloseMenuFilter" style="width : 99vw; position : absolute; top: 9rem; left: 0; z-index : 3; font-size: 2.5rem; display: none"><i class="fa fa-times pull-right mr-5"></i></span></div>`);
			} else {
				$("#menuTopLeft").append(`<div class="filterXs visible-xs menu-xs-cplx pull-left"><span class="btnCloseMenuFilter" style="width : 99vw; position : absolute; top: 9rem; left: 0; z-index : 3; font-size: 2.5rem; display: none"><i class="fa fa-times pull-right mr-5"></i></span></div>`);
			}
			aapObj.hasFilter = true;
			return paramsFilterL;
		}, projectDetail: function (aapObj, isInsideModal = false) {
			var paramsFilterL = aapObj.paramsFilters.project(aapObj, "#filterContainerD", isInsideModal);
			$(".filterXs.visible-xs").append(`
                <div class="col-xs-12 propsAndFilter no-padding">
                    <div class="visible-xs">
                        <button type="button" class="btn btn-default showHide-filters-xs" id="show-list-xs" style="width: 3rem">
                            <i class="fa fa-sliders"></i>
                        </button>
                    </div>
                    <div id="propsFilterTab" class="xs-dropdown-tab">
                        <span class="btnCloseMenuList" style="position : absolute; top: 0.5rem; right: 1rem; z-index : 3; font-size: 2.5rem; display: none"><i class="fa fa-times pull-right mr-2"></i></span>
                        <ul class="nav nav-tabs nav-panel-border" role="tablist">
                            <li class="active">
                                <a href="#proposition-menu-list" role="tab" data-toggle="tab">${ucfirst(trad["projects"])}</a>
                            </li>
                            <li class="">
                                <a href="#proposition-list-filter" id="filterXsTab" class="showHide-filters-xs" role="tab" data-toggle="tab">${trad["Filters"]}</a>
                            </li>
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div class="tab-pane aap-list-container active" id="proposition-menu-list">
                            </div>
                    
                            <div class="tab-pane aap-list-container padding-top-10" id="proposition-list-filter">
                    
                            </div>
                        </div>
                    </div>
                </div>
            `)
			paramsFilterL.defaults.indexStep = 20;
			paramsFilterL.defaults.scrollDom = "#listPropsPanel,#dropdown_search_detail";
			paramsFilterL.container = "#filterContainerD";
			paramsFilterL.header.dom = ".headerSearchContainerD";
			paramsFilterL.footerDom = ".footerSearchContainerD";
			// if(isInsideModal == true) {
			//     paramsFilterL.defaults.onResize = false;
			//     paramsFilterL.defaults.appendToXs ? delete paramsFilterL.defaults.appendToXs : ""
			//     paramsFilterL.defaults.appendToLg ? delete paramsFilterL.defaults.appendToLg : "";
			//     paramsFilterL.defaults.onResizeFunc = () => {}
			// } else {
			paramsFilterL.defaults.appendToXs = "#proposition-list-filter";
			paramsFilterL.defaults.appendToLg = "#filterLCollapse";
			paramsFilterL.defaults.onResizeFunc = () => aapObj.common.switchList()
			// }
			paramsFilterL.interface = {
				events: {
					scroll: true,
					scrollOne: true
				}
			}
			paramsFilterL.loadEvent = {
				default: "scroll"
			}
			paramsFilterL.results = {
				dom: "#dropdown_search_detail", smartGrid: false, renderView: "aapObj.directory.aapProjectMini", map: {
					show: false
					//active : true,
				}, events: function () {
					$("#proposalResultCount").html(aapObj.filterSearch?.projectDetail?.results?.count?.projects && aapObj.filterSearch?.projectDetail?.results?.count?.projects > 0 ? aapObj.filterSearch.projectDetail.results.count.projects : "")
					aapObj.events.projectDetail(aapObj)
				}
			}
			return paramsFilterL;
		},
	},
	filterSearch: {
		proposal: {},
		project: {},
		proposalNewCounter: null,
		activeFilters: {},
		proposalDetail: null,
		projectDetail: null,
		lastSearchObjProposal: {
			search: {
				obj: {}
			}
		},
		lastSearchObjProject: {
			search: {
				obj: {}
			}
		},
		proposalInitialized: null,
		proposalDetailInitialized: null,
		projectInitialized: null,
		projectDetailInitialized: null
	},
	events: {
		common: function (aapObj) {
			$("#show-filters-lg.showHide-filters-xs,#btnHideFilter.showHide-filters-xs").off().on("click", function (event) {
				event.stopImmediatePropagation();
				if ($(".open-xs-menu-collapse").children('i').hasClass("fa-times")) {
					$(".open-xs-menu-collapse").children('i').toggleClass("fa-times");
					$(".open-xs-menu-collapse").children('i').toggleClass("fa-bars");
					$("#menuTopCenter").toggleClass("in")
				}
				if ($(window).width() > 767) {
					if ($(".menu-filters-lg").is(":visible")) {
						$("#show-filters-lg.showHide-filters-xs").find("i").removeClass("fa-times").addClass("fa-sliders");
						$("#filter-list-container").removeClass("filter-shown");
						$(".menu-filters-lg").hide(300, function () {
							$.each(aapObj.common.range(1, 12), function (__, __col) {
								$('.filter-container').removeClass(`col-xs-${__col} col-md-${__col}`);
							});
							if (aapObj.common.canShowMap(aapObj)) {
								$(".filter-container").addClass('col-xs-9 col-md-6');
							} else {
								$(".filter-container").addClass('col-xs-12 col-md-12');
							}
						});
						$(".filterHidden").show('slide', {}, 300);
						// $(".menu-filters-lg").hide('slide', {
						//     direction: 'left'
						// }, 600);
						$(".openFilterParams").hide('fade', {}, 600)
					} else {
						$("#show-filters-lg.showHide-filters-xs").find("i").removeClass("fa-sliders").addClass("fa-times");
						$("#filter-list-container").addClass("filter-shown");
						$(".filterHidden").hide();
						$.each(aapObj.common.range(1, 12), function (__, __col) {
							$('.filter-container').removeClass(`col-xs-${__col} col-md-${__col}`);
						});
						if (aapObj.common.canShowMap(aapObj)) {
							$(".filter-container").addClass('col-xs-8 col-md-8');
						} else {
							$(".filter-container").addClass('col-xs-9 col-md-9');
						}
						// $(".filter-container").removeClass('col-xs-10').addClass('col-xs-8')
						$(".menu-filters-lg").show(300);
						if (!$("#filterContainerInside").is(":visible")) {
							$("#filterContainerInside").show()
						}
						// $(".menu-filters-lg").show('slide', {
						//     direction: 'right'
						// }, 600);
						$(".openFilterParams").show('fade', {}, 600);
					}
				}
			});

			$('.aap-organism-chooser').off().on('click', function () {
				if (notNull(costum) && exists(costum.contextId) && $(this).hasClass("active-subOrga")) {
					aapObj.common.loadAapOrganismPanel();
				}
			})
			$(`[href$='#${typeof page != "undefined" ? page : ""}']`).addClass('active');

			$('.propsDetailPanel a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
				const queryParams = aapObj.common.getQuery();

				if (location && location?.hash.indexOf("#detailProposalAap") > -1) {
					aapObj.aapview = $(this).attr('href').replace("#proposition-", "");
				}
			});
			if (aapObj.common.getQuery()?.aapview) {
				$(`[data-view=${aapObj.common.getQuery()?.aapview}]`).addClass('active');
			} else {
				$(`[data-view=detailed]`).addClass('active');
			}
			if (notNull(localStorage.getItem("showMyMap")) && (localStorage.getItem("showMyMap") === 'true' && aapObj.showMap)) {
				$(`[data-view=map]`).addClass('active');
			}

			if (
				(typeof aapObj.common.getQuery().standalone !== "undefined" && (aapObj.common.getQuery().standalone == "true" || aapObj.common.getQuery().standalone === true)) ||
				(typeof aapObj.common.getQuery().custom !== "undefined" && (aapObj.common.getQuery().custom == "true" || aapObj.common.getQuery().custom === true))
			) {
				$('#menuTopLeft').remove();
				$('.cosDyn-app').remove();
			}

			htmlExists('a[href^="#newproposalAap"]', function (sel) {
				if (!notEmpty(aapObj?.form?.onlymemberaccess) || aapObj?.form?.onlymemberaccess == false) {
					if (typeof aapObj.session !== 'undefined' && notEmpty(aapObj.session) && typeof aapObj.session.status === 'string' && aapObj.session.status === "include" && (!aapObj.form.oneAnswerPerPers || !aapObj.haveAnswer)) {
						sel.show();
					} else {
						sel.hide();
					}
				} else {
					if (aapObj.canEdit || aapObj.context?.links?.members?.[userId]) {
						if (typeof aapObj.session !== 'undefined' && notEmpty(aapObj.session) && typeof aapObj.session.status === 'string' && aapObj.session.status === "include" && (!aapObj.form.oneAnswerPerPers || !aapObj.haveAnswer)) {
							sel.show();
						} else {
							sel.hide();
						}
					} else {
						sel.hide();
					}
				}
			});
			if (!notEmpty(userConnected)) {
				$('.proposalDropdown,' +
					'.projectDropdown,' +
					'.projectDropdown+a,' +
					'.organismAap a.lbh-menu-app:nth-child(2),' +
					'.organismAap a.lbh-menu-app:nth-child(3)'
					// '#menuTopRight .cosDyn-app'
				).remove();
			}

			// trigger click the first element in the dropdown-menu when the dropdown is open
			$(".proposalDropdown, .projectDropdown").off("show.bs.dropdown").on("show.bs.dropdown", function (e) {
				e.stopImmediatePropagation();
				$(this).find(".dropdown-menu .sous-menu-full-width a.lbh-menu-app").first().trigger("click");
			});
			/*if(typeof aapObj.form.coremu === "undefined" || aapObj.form.coremu === "false" || aapObj.form.coremu === false){
			 $('.cosDyn-app a[href*="contributionAap"]').remove();
			 }*/

			/*$(".copylinkfinancer").off().on("click", function(){
				var thisbtn = $(this);
				var sampleTextarea = document.createElement("textarea");
				document.body.appendChild(sampleTextarea);
				sampleTextarea.value = baseUrl + "/costum/co/index/slug/" + costum.slug + "#answer.answer.id." +thisbtn.data("id")+ ".form." + aapObj.form["_id"]["$id"] + ".view.offline.step.aapStep3.input.financer"; //save main text in it
				sampleTextarea.select();
				document.execCommand("copy");
				document.body.removeChild(sampleTextarea);
				toastr.success(trad.copied);
			});*/
		}, configuration: function (aapObj) {
			$('.aap-community').off().on('click', function () {
				smallMenu.openAjaxHTML(baseUrl + '/co2/aap/getviewbypath/path/co2.views.aap.partials.community');
			});
		}, statusDropdown: function (aapObj) {
			$('a.editStatus').off().on('click', function () {
				const usersStatus = JSON.parse($(this).attr("data-users").replace(/'/g, '\"'));
				aapObj.directory.updateStatus(aapObj, $(this).attr("data-action"), {
					id: $(this).attr('data-id'), value: $(this).attr('data-value'), path: $(this).attr("data-path")
				}, usersStatus);
			})
		}, projectStatusDropdown: function (aapObj) {
			$('a.editProjectStatus').off().on('click', function () {
				aapObj.directory.updateProjectStatus(aapObj, {
					id: $(this).attr('data-id'), value: $(this).attr('data-value'), path: "properties.avancement"
				});
			})
		}, proposalMenuIcon: function () {
			htmlExists('.menu-detail-proposal', function (sel) {
				sel.off().on('click', function (e) {
					e.stopPropagation();
					setTimeout(() => {
						const parameters = {
							context: aapObj.context.slug, formid: aapObj.form._id.$id
						};
						if (notEmpty(aapObj.defaultProposal)) {
							parameters.answerId = aapObj.defaultProposal._id.$id;
							urlCtrl.loadByHash(aapObj.common.buildUrlQuery('#detailProposalAap', parameters, aapObj.common.getQueryStringObject()));
						}
					}, 100);
				})
			});

			htmlExists('.menu-detail-my-proposal', function (sel) {
				sel.off().on('click', function (e) {
					e.stopPropagation();
					setTimeout(() => {
						const parameters = {
							context: aapObj.context.slug,
							formid: aapObj.form._id.$id
						};
						if (notEmpty(aapObj.myDefaultProposal)) {
							parameters.answerId = aapObj.myDefaultProposal._id.$id;
							parameters.userId = userId;
							urlCtrl.loadByHash(aapObj.common.buildUrlQuery('#detailProposalAap', parameters, aapObj.common.getQueryStringObject()));
						}
					}, 100);
				})
			});
		}, proposal: function (aapObj) {
			$('.tooltips').tooltip();
			changeListView();
			$('.proposition-detail,.proposition-name.cursor-pointer').off('click').on('click', function () {
				const self = $(this);
				const target = self.data('target');
				const title = self.data('title');
				const project_id = self.data('project-id');
				const project = self.data('project');
				var href = document.location.href;
				var urlQuery = {
					context: aapObj.context.slug, formid: aapObj.form._id.$id, answerId: target, aapview: 'summary'
				};
				const currentQuery = aapObj.common.getQuery()
				if (notEmpty(currentQuery?.["userId"])) {
					urlQuery.userId = currentQuery?.userId
				}
				if (notEmpty(project_id)) {
					urlQuery.projectId = project_id;
				}
				aapObj.filterSearch.activeFilters = aapObj.common.getActivateFilters(aapObj, "#filterContainerL", "#activeFilters .filters-activate")
				history.pushState(null, title, aapObj.common.buildUrlQuery(notEmpty(project) ? '#detailProjectAap' : '#detailProposalAap', urlQuery, aapObj.common.getQueryStringObject()));
				sessionStorage.setItem("aapview", "kanban")
				urlCtrl.loadByHash(location.hash);
			});

			aapObj.events.inviteToProposalDropdown(aapObj)

			$('.add-proposition').off('click').on('click', function (e) {
				e.stopImmediatePropagation();
				e.preventDefault();
				const self = $(this);
				const target = self.data('target');
				const title = self.data('title');
				// const new_hash =
				// `coformAap.context.${aapObj.context.slug}.formid.${aapObj.form._id.$id}.answerId.new${document.location.href.indexOf('?') > -1 ?
				// '?' + document.location.href.split('?')[1] : ''}`;
				eventScrollAlreadyCalled = false;
				const query = aapObj.common.getQuery(aapObj);
				var substitutes = {
					context: query.context,
					formid: query.formid,
					answerId: "new"
				};
				if (notEmpty(query?.["userId"])) {
					substitutes["userId"] = query?.["userId"]
				}
				if (notEmpty(query?.["projectUserId"])) {
					substitutes["userId"] = query?.["projectUserId"]
				}

				var hash = aapObj.common.buildUrlQuery("#coformAap", substitutes, aapObj.common.getQueryStringObject());
				history.pushState(null, null, document.location.origin + document.location.pathname + hash);
				urlCtrl.loadByHash(location.hash);
			});

			$("#dropdown_search .proposition-collapse-container .collapse,.proposal-compact .proposition-collapse-container .collapse").on("show.bs.collapse", function () {
				var id = $(this).data("id");
				var html = aapObj.directory.propositionCollapseHtml(aapObj, id);
				$(this).empty().html(html);
				aapObj.events.proposalCollapseItems(aapObj);
			})

			/*$('.proposition-collapse-container-toogler').on('click',function(){
			 $('.proposition-collapse-item.collapse').collapse();
			 })*/
			if (aapObj.context?.slug !== "coSindniSmarterre") {
				$('.pdf-proposal').hide();
			}
			$('.pdf-proposal').off().on('click', function () {
				var id = $(this).data("id");
				smallMenu.openAjaxHTML(baseUrl + '/co2/aap/getviewbypath/path/co2.views.aap.partials.pdf_exporter/params/' + id);
			})
			$('[aria-labelledby=proposition-actions]').on('click', '[role=menuitem]', function (__e) {
				const actions_events = {
					'csv-export': function () {
						$('#csv-exportation').modal('show');
					}
				};
				const self = $(this);
				__e.preventDefault();
				actions_events[self.attr('href').substring(1)]();
			});

			/*$('.proposition-name').off().on('click', function () {
			 const answerId = $(this).data("id");
			 const title = $(this).data("title");
			 const query = aapObj.common.getQuery(aapObj);
			 const new_hash = `proposalAap.context.${query.context}.formid.${query.formid}.answerId.${answerId}${document.location.href.indexOf('?')> -1 ? '?' + document.location.href.split('?')[1] : ''}`;
			 history.pushState({}, title, `${document.location.origin}${document.location.pathname}#${new_hash}`);
			 document.title = title;
			 aapObj.common.modalProposalDetail(aapObj, answerId);
			 })*/
			$(".badge-tag.tag-theme").off().on('click', function () {
				const val = $(this).text().trim();
				$(`#filterCollapse-theme .panel-body [data-value="${val}"]`).trigger('click');
			})
			$(".filterContainerL.filter-alias .alias-main-search-bar").off().on("keyup", function (e) {
				$(".filter-alias .alias-main-search-bar").val($(this).val())
				if (e.keyCode != 13) {
					aapObj.common.filterItems($(this), true, null, null, "#filterContainerL");
				}
			})

			aapObj.events.proposalMenuIcon();
			// aapObj.events.projectMenuIcon();
			htmlExists('a[href^="#newproposalAap"]', function (sel) {
				// if (typeof aapObj.session !== 'undefined' && notEmpty(aapObj.session) && typeof aapObj.session.status === 'string' &&
				// aapObj.session.status === "include" && (!aapObj.form.oneAnswerPerPers || !aapObj.haveAnswer)) { sel.show(); } else { sel.hide(); }
				if (!notEmpty(aapObj?.form?.onlymemberaccess) || aapObj?.form?.onlymemberaccess == false) {
					if (typeof aapObj.session !== 'undefined' && notEmpty(aapObj.session) && typeof aapObj.session.status === 'string' && aapObj.session.status === "include" && (!aapObj.form.oneAnswerPerPers || !aapObj.haveAnswer)) {
						sel.show();
					} else {
						sel.hide();
					}
				} else {
					if (aapObj.canEdit || aapObj.context?.links?.members?.[userId]) {
						if (typeof aapObj.session !== 'undefined' && notEmpty(aapObj.session) && typeof aapObj.session.status === 'string' && aapObj.session.status === "include" && (!aapObj.form.oneAnswerPerPers || !aapObj.haveAnswer)) {
							sel.show();
						} else {
							sel.hide();
						}
					} else {
						sel.hide();
					}
				}
			});

			if (typeof aapObj.common.getQuery()["preconfiguration"] != "undefined" && aapObj.canEdit && $(".co-popup-preconfig-container").length <= 0) {
				aapObj.preconfiguration.actions.open(aapObj);
				aapObj.preconfiguration.initialized = true;
			}

			/*$(".copylinkfinancer").off().on("click", function(){
				var thisbtn = $(this);
				var sampleTextarea = document.createElement("textarea");
				document.body.appendChild(sampleTextarea);
				sampleTextarea.value = baseUrl + "/costum/co/index/slug/" + costum.slug + "#answer.answer.id." +thisbtn.data("id")+ ".form." + aapObj.form["_id"]["$id"] + ".view.offline.step.aapStep3.input.financer"; //save main text in it
				sampleTextarea.select();
				document.execCommand("copy");
				document.body.removeChild(sampleTextarea);
				toastr.success(trad.copied);
			});*/

		}, proposalCollapseItems: function (aapObj, isAap = true) {
			$('.btn-proposal-favorite').off().on('click', function () {
				$btn = $(this);
				$val = $btn.data('value');
				var tplCtx = {
					id: $btn.data("id"),
					value: $val
				}
				ajaxPost(null, baseUrl + '/co2/aap/publicrate', tplCtx, function (res) {
					if (res.result) {
						var id = $btn.data("id")
						if (!isAap) {
							answer = aapObj.common.getOneAnswer(aapObj, id)
							if (exists(answer['results']) && exists(answer['results'][id])) {
								answer = aapObj.prepareData.proposal(aapObj, answer['results'][id], answer);
							}
							const parentElem = $("#btn-favoris-" + id).parent();
							parentElem.find('div.tooltip.in').remove()
							$("#btn-favoris-" + id).remove();
							const html = aapObj.directory.propositionFavoris(aapObj, answer);
							parentElem.prepend(html)
							aapObj.events.proposalCollapseItems(aapObj, false);
						} else {
							var html = aapObj.directory.propositionCollapseHtml(aapObj, id);
							var iddiv = $btn.parent().parent().attr("id");
							$('#' + iddiv).empty().html(html);
							aapObj.events.proposalCollapseItems(aapObj);
						}
					}
				}, null);
			})
			$('.tooltips').tooltip();
			$(".btn-proposal-view-financement").off().on('click', function () {
				const answerId = $(this).data("id");
				aapObj.common.modalProposalDetail(aapObj, answerId, true);
				htmlExists("[href='#proposition-funding']", function (sel) {
					sel.trigger('click');
				});
			});
		}, proposalStatus: function () {
			$('.tooltips').tooltip();
			$('[data-toggle-second="tooltip"]').tooltip();
			$('.changeStatus.dropup .dropdown-toggle').off().on('click', function (e) {
				var self = this;
				$('[data-toggle-second="tooltip"]').tooltip();
				const usersStatus = JSON.parse($(this).attr('data-users').replace(/'/g, '\"'));
				let statusObj = {
					statusDropdown: $(aapObj.directory.statusDropdown_template_html(aapObj, $(self).attr('data-actualstatus').split(','), aapObj.status ? Object.keys(aapObj.status) : [], $(self).attr('data-id'), usersStatus)),
					dropdownHeight: 0
				}
				$('.statusDropdown.dropdown-menu').remove()
				$('body').append(statusObj.statusDropdown);
				statusObj.dropdownHeight = $('.dropdown-menu.custom-scroll-bar').height();
				statusObj.statusDropdown.css({
					display: 'block', left: e.pageX + 'px', top: e.pageY - statusObj.dropdownHeight + 'px', // right : '22vw',
					height: '0px', width: '15%', transition: 'height .3s ease'
				});
				statusObj.statusDropdown.animate({
					// display: 'block',
					// position: 'absolute',
					// left: e.pageX +'px',
					height: statusObj.dropdownHeight + 'px'
				}, 300);
				aapObj.events.statusDropdown(aapObj)
			});
			$(document).on('click', function (e) {
				$('.statusDropdown.dropdown-menu').remove()
			});
			$('.dropdown').on('show.bs.dropdown', function () {
				$('body .statusDropdown.dropdown-menu').remove()
			})
		}, myproposals: function (aapObj) {
		}, inviteToProposalDropdown: function (aapObj) {
			$('.btn-invite-proposal.dropup').off().on('click', function (e) {
				e.stopImmediatePropagation();
				$('.invite-container').empty();
				var self = this;
				$('[data-toggle-second="tooltip"]').tooltip();
				const target = $(this).attr("data-answerid")
				const projectId = $(this).attr('data-projectid')
				// const usersStatus = JSON.parse($(this).attr('data-users').replace(/'/g, '\"'));
				let invitationObj = {
					invitationDropdown: $(`<div class="inviteToProposalDropdown dropdown-menu custom-scroll-bar" style="max-height: 650px; overflow-y: auto" data-answerid="${target}" data-projectid="${projectId}"></div>`),
					dropdownHeight: 0
				}
				$('.inviteToProposalDropdown.dropdown-menu').remove()
				$('body').append(invitationObj.invitationDropdown);

				aapObj.directory.inviteToProposalDropdown_template_html(aapObj, $(self).attr("data-answerid"), ".inviteToProposalDropdown", function (params) {

				}, projectId)
				invitationObj.dropdownHeight = $('.inviteToProposalDropdown.dropdown-menu.custom-scroll-bar').height();
				let left = $(e.currentTarget).offset()?.left + invitationObj.invitationDropdown.width() > $(window).width() ? $(e.currentTarget).offset()?.left - invitationObj.invitationDropdown.width() + 30 : $(e.currentTarget).offset()?.left
				invitationObj.invitationDropdown.css({
					display: 'block',
					left: left + "px",
					top: $(e.currentTarget).offset()?.top + 40 + 'px', // left : e.pageX - invitationObj.invitationDropdown.width() + 'px',
					// top : e.pageY + 15 + 'px',
					// right : '22vw',
					height: 'auto',
					transition: 'height .3s ease'
				});
				invitationObj.invitationDropdown.animate({
					// display: 'block',
					// position: 'absolute',
					// left: e.pageX +'px',
					height: 'auto'
				}, 300);
				// aapObj.events.statusDropdown(aapObj)
				$('.inviteToProposalDropdown.dropdown-menu').off('hide.my.dropdown').on('hide.my.dropdown', function (e) {
					e.stopImmediatePropagation();
					const self = this;
					$(this).remove();
					const query = aapObj?.common?.getQuery();
					switch (page) {
						case "projectsAap":
							aapObj.directory.reloadAapProjectItem(aapObj, $(self).attr("data-projectid"));
							break;
						case "detailProjectAap":
							aapObj.common.loadProjectDetail(aapObj, query?.projectId, query?.aapview ? query.aapview : 'summary')
							break;
						case "proposalAap":
							aapObj.directory.reloadAapProposalItem(aapObj, $(self).attr("data-answerid"));
							break;
						case "detailProposalAap":
							aapObj.common.loadProposalDetail(aapObj, query?.answerId, query?.aapview ? query.aapview : 'summary');
							break;
						case "observatoryAap":
							if (query?.projectId) {
								aapObj.common.loadProjectDetail(aapObj, $(self).attr("data-projectid"), query?.aapview ? query.aapview : 'summary')
							} else if (query?.answerId) {
								aapObj.common.loadProposalDetail(aapObj, $(self).attr("data-answerid"), query?.aapview ? query.aapview : 'summary');
							}
							break;
						default:
							break;
					}
				})
			});
			$(document).on('click', function (e) {
				$('.inviteToProposalDropdown.dropdown-menu').hide().trigger('hide.my.dropdown')
			});
			$('.dropdown').on('show.bs.dropdown', function () {
				$('body .inviteToProposalDropdown.dropdown-menu').remove()
			})
		}, customTooltip: function () {
			$(`[data-custom-toggle="tooltip"]`).on("mouseenter", function (e) {
				e.stopImmediatePropagation();
				var self = this;
				const content = $(this).attr("data-content")
				const position = $(this).attr('data-position')
				let tooltipObj = {
					tooltipHtml: $(`
                        <div class="custom-tooltip tooltip-content evaluate-info custom-scroll-bar" style="max-height: 650px; overflow-y: auto">
							<div class="tooltip-arrow" style="top: 50%;"></div>
                            <div class="custom-tooltip-body">
								${content}
							</div>
                        </div>
                    `), tooltipHeight: 0
				}
				$('.custom-tooltip.tooltip-content').remove()
				$('body').append(tooltipObj.tooltipHtml);

				tooltipObj.tooltipHeight = tooltipObj.tooltipHtml.height();
				tooltipObj.tooltipHtml.css({
					position: 'absolute',
					display: 'block',
					left: (($(e.currentTarget).width() + tooltipObj.tooltipHtml.width()) > $('body').width() ? $(e.currentTarget).offset()?.left + $(e.currentTarget).width() - tooltipObj.tooltipHtml.width() + 18 + "px" : $(e.currentTarget).offset()?.left + $(e.currentTarget).width() + 18 + "px"),
					top: (($(e.currentTarget).width() + tooltipObj.tooltipHtml.width()) > $('body').width() ? $(e.currentTarget).offset()?.top + $(e.currentTarget).height() + 'px' : $(e.currentTarget).offset()?.top - ($(e.currentTarget).height() + 20 < tooltipObj.tooltipHtml.height() ? ((tooltipObj.tooltipHtml.height() + $(e.currentTarget).height()) / 4) : 5) + 'px'), // right : '22vw',
					height: 'auto',
					width: 'fit-content',
					"padding": '5px',
					"z-index": 100001,
					overflow: 'hidden'
				});
				tooltipObj.tooltipHtml.animate({
					height: 'auto'
				}, 300);
				if (($(e.currentTarget).width() + tooltipObj.tooltipHtml.width()) > $('body').width()) {
					tooltipObj.tooltipHtml.find(".tooltip-arrow").css({
						cssText: `
							top: 4px !important;
							left: 50% !important;
							transform: rotate(90deg);
						`
					})
				}
			})
			$(`[data-custom-toggle="tooltip"]`).on("mouseleave", function (e) {
				e.stopImmediatePropagation()
				$('body .custom-tooltip.tooltip-content').remove()
			})
		}, bindDetailProposalClickEvent: function (aapObj) {
			$('.props-item').off('click').on('click', function () {
				if ($(window).width() < 768) {
					if ($("#propsFilterTab").is(":visible")) {
						$("#show-list-xs").find("i").removeClass("fa-times").addClass("fa-sliders");
						$(".btnCloseMenuList").hide();
					}
					$("#propsFilterTab").hide(300);
				}
				queryParams = aapObj.common.getQuery();

				const self = $(this);
				const target = self.attr('data-target');
				const title = self.attr('data-title');
				const project = self.data('project-id');
				var hash = location.hash;
				var hashParams = {
					context: aapObj.context.slug,
					formid: aapObj.form._id.$id,
					answerId: target
				}
				$('.props-item.active').removeClass('active');
				self.addClass('active');
				$('h3.title span.main').text(title);
				if (typeof queryParams.detailProposalAap !== 'undefined') {
					hash = '#detailProposalAap';
					if (notEmpty(queryParams.aapview)) {
						hashParams.aapview = queryParams.aapview;
					}
				} else if (typeof queryParams.proposalAap !== 'undefined') {
					hash = '#proposalAap';
				}

				if (notEmpty(queryParams?.["userId"])) {
					hashParams["userId"] = queryParams?.["userId"];
				}

				if (notEmpty(queryParams.answerId)) {
					$('.propsDetailPanel .aap-nav-tabs>li>[role=tab]').attr('data-answer', target);
				}
				if (notEmpty(project)) {
					$('.propsDetailPanel .aap-nav-tabs>li>[role=tab]').attr('data-project', project);
					hashParams.projectId = project;
				} else {
					hashParams.projectId = ""
				}
				hash = aapObj.common.buildUrlQuery(hash, hashParams, aapObj.common.getQueryStringObject());

				$('.detail-breadcrumb').attr('href', hash).text(title);
				if ($(".detail-breadcrumb").is(":visible") && title) {
					const currentTitle = document.title;
					const newTitle = currentTitle.includes('|') ? currentTitle.split('|')[1].trim() : currentTitle;
					document.title = `${ucfirst(title)} | ${newTitle}`;
				}
				history.pushState(null, title, document.location.origin + document.location.pathname + hash);
				queryParams = aapObj.common.getQuery();

				$('.nav.nav-tabs.aap-nav-tabs a[role=tab][data-toggle=tab]').filter(function () {
					return $(this).attr('href') !== '#proposition-' + queryParams.aapview;
				}).addClass('need-reload');
				aapObj.common.loadProposalDetail(aapObj, target, queryParams.aapview ? queryParams.aapview : 'summary');
				$('.btn-invite-proposal').attr('data-projectid', hashParams.projectId)
				$('.btn-invite-proposal').attr('data-answerid', target)
				// $('.btn-invite-proposal').attr('href', '#element.invite.type.answers.id.' + target);
				$('.btn-comment-proposal').off('click').on('click', function () {
					// commentObj.openPreview('answers', target, '', $('.aap-page-info-left h3 .main').text().replace(/'/g, "\\'"));
					aapObj.common.openProposalDiscussionSpace(target)
				})
				aapObj.common.switchProposalToProjectDetail(aapObj, queryParams);
			});

			$(".props-item .text-ellipsis").each(function () {
				if ($(this)[0] && $(this)[0].scrollWidth && $(this).width()) {
					if ($(this)[0].scrollWidth - $(this).width() == 0) {
						$(this).removeClass("hover-marquee")
					} else {
						$(this).removeClass("hover-marquee").addClass("hover-marquee")
					}
				}
			})
		}, bindJoinBtnEvent: function () {
			$(".btn-join-proposal").off("click").on("click", function () {
				const thisBtn = $(this);
				thisBtn.remove();
				if (userId && userConnected)
					aapObj.common.joinThisProposal(thisBtn.attr("data-answerid"), thisBtn.attr("data-projectid") ? thisBtn.attr("data-projectid") : null, userId, userConnected)
			});
		}, proposalDetail: function (aapObj) {
			$("#dropdown_search_detail .processingLoader").remove();
			$("#dropdown_search_detail .divEndOfresults").hide();
			var queryParams = aapObj?.common?.getQuery();
			var findInterval = null;
			const answerItemId = queryParams?.['answerId'] ? queryParams?.['answerId'] : null;
			$('.props-item.active').removeClass('active');
			aapObj.events.customTooltip()

			$("#dropdown_search_detail").find("#propItem" + answerItemId).addClass("active");
			$('#listPropsPanel').on('shown.bs.collapse', function () {
				$(".panel-heading .panel-title i").addClass('fa-caret-up').removeClass('fa-caret-down');
			}).on('hidden.bs.collapse', function () {
				$(".panel-heading .panel-title i").addClass('fa-caret-down').removeClass('fa-caret-up');
			});

			if (typeof discussionComments.notVisibleConnections == "undefined") {
				discussionComments.notVisibleConnections = {};
			}
			$.each($("#dropdown_search_detail .props-item"), function (i, elt) {
				const thisCurrentAnsId = $(this).attr("data-target");
				if (thisCurrentAnsId && typeof discussionComments.notVisibleConnections[thisCurrentAnsId] == "undefined") {
					if (typeof discussionSocket != "undefined" && thisCurrentAnsId && userId && currentUser && currentUser.name) {
						const mongoIdRegex = /^[a-f\d]{24}$/i;
						if (mongoIdRegex.test(thisCurrentAnsId)) {
							discussionComments.notVisibleConnections[thisCurrentAnsId] = true;
							discussionSocket.emitEvent(wsCO, "join_discussion_room", {
								userId: userId,
								userName: currentUser.name,
								discuAnsId: thisCurrentAnsId,
								disableStatus: true
							});
						}
					}
				}
			})
			var connectedFromHereInterval = setInterval(() => {
				if ($(".propsDetailPanelContainer").length < 1) {
					clearInterval(connectedFromHereInterval);
					if (typeof discussionComments.notVisibleConnections != "undefined") {
						$.each(discussionComments.notVisibleConnections, function (index, val) {
							discussionSocket.emitEvent(wsCO, "user_leave_discussion", {
								userId: userId,
								userName: currentUser.name,
								discuAnsId: index
							});
							delete discussionComments.notVisibleConnections[index];
						})
					}
				}
			}, 1000);
			// $('a.btn-invite-proposal').attr("onclick", "return aapObj.common.addLinkToCommunityAttr()")
			if (aapObj.hasInviteInputs && aapObj.canEdit) {
				$('.invite-tab a.btn-invite-proposal').show()
				$('li.invite-tab').next().css("margin-right", "inherit")
			} else {
				$('.invite-tab a.btn-invite-proposal').remove();
				if (aapObj.hasInviteInputs && userId && answerItemId) {
					if (functionCurrentlyLoading && !functionCurrentlyLoading["alreadyContributors"]) {
						aapObj.common.alreadyContributors(userId, answerItemId, (data) => {
							if (typeof data.result != "undefined" && data.result == false) {
								$('.invite-tab').empty();
								$('.invite-tab').append(`
									<a href="javascript:;" class="pull-right text-center btn-join-proposal link-banner text-ellipsis"
									data-placement="bottom" data-original-title="Rejoindre comme contributeur à cette proposition" data-answerid="${answerItemId}"
									${data.elt && data.elt.project && data.elt.project.id ? `data-projectid="${data.elt.project.id}"` : ""}>
										<i class="fa fa-user-plus a-icon-banner"></i>
										<span class="title-link-banner hidden-sm">
											${trad.Join}
										</span>
									</a>
								`)
								aapObj.events.bindJoinBtnEvent()
							} else if (data.result == true) $('.invite-tab a.btn-join-proposal').remove();
						});
					}
				} else $('li.invite-tab').next().css("margin-right", "50px")
			}
			if (aapObj.context?.links?.members?.[userId] || aapObj.canEdit) {
				$('li a.btn-comment-proposal').show()
			} else {
				$('li a.btn-comment-proposal').hide()
			}
			$('.btn-comment-proposal').off('click').on('click', function () {
				// commentObj.openPreview('answers', answerItemId, '', $('.aap-page-info-left h3 .main').text().replace(/'/g, "\\'"));
				aapObj.common.openProposalDiscussionSpace(answerItemId)
			})
			aapObj.events.inviteToProposalDropdown(aapObj)
			aapObj.events.bindDetailProposalClickEvent(aapObj)

			var tabsDom = $('.nav.nav-tabs.aap-nav-tabs a[role=tab][data-toggle=tab]');
			var query = aapObj.common.getQuery();
			tabsDom.filter(function () {
				return $(this).attr('href') !== '#proposition-' + query.aapview;
			}).addClass('need-reload');
			tabsDom.off("shown.bs.tab").on('shown.bs.tab', function () {
				const viewTarget = $(this).attr('href').substring(13);
				var self = $(this);
				const view = self.attr('href').replace("#proposition-", "");
				var dataset = this.dataset;
				const project = dataset.project;
				const answer = dataset.answer;
				const query = aapObj.common.getQuery(aapObj);
				var substitutes = {
					context: query.context, formid: query.formid, aapview: view, answerId: answer
				};
				project ? substitutes.projectId = project : ""
				if (query.userId)
					substitutes.userId = query.userId;
				var new_hash = aapObj.common.buildUrlQuery(document.location.href, substitutes, aapObj.common.getQueryStringObject());

				history.pushState({}, {}, new_hash);
				if (self.hasClass('need-reload')) {
					aapObj.common.loadProposalDetail(aapObj, answer, viewTarget);
					self.removeClass('need-reload');
				}
			});
			var tabView = query.aapview ? query.aapview : 'action';
			var activeTabs = tabsDom.filter(function () {
				return $(this).hasClass('active');
			});
			if (activeTabs.length === 0) {
				tabsDom.filter(function () {
					return $(this).attr('href').match(new RegExp(tabView));
				}).tab('show');
			}

			$("#propsDetail.filterContainerD.filter-alias .alias-main-search-bar").off("keyup").on("keyup", function (e) {
				if (e.keyCode != 13) {
					aapObj.common.filterItems($(this), true, "#dropdown_search_detail", "a.props-item", "#filterContainerD");
				}
			})
			if ($("#dropdown_search_detail a.props-item").length == 0) {
				$("#dropdown_search_detail .divEndOfresults").show();
			}

			$("#btnHideFilterProps").on('click', function (e) {
				e.stopImmediatePropagation();
				let selector = ".allPropsPanel";
				$(".allPropsPanel").is(":visible") && $(".allFilterPanel").is(":visible") ? selector = ".aap-nav-panel" : "";
				// $(".openPropsList").attr("data-last-title", $(".openPropsList").attr("data-original-title"))
				// $(".openPropsList").attr("data-original-title", "Cacher filtre et propositions");
				$(selector).hide(300, function () {
					$(".propsDetailPanel").removeClass('col-xs-10 col-xs-8 col-sm-10 col-sm-8').addClass('col-xs-12');
					$("#detailFilterCountInBtn").removeClass("d-none").addClass("d-none");
					$(".openPropsList i").removeClass('fa-sliders fa-times').addClass('fa-list')
				});
			});
			$("#btnHideFilterOnly").on('click', function (e) {
				e.stopImmediatePropagation();
				$(".allFilterPanel").hide(300, function () {
					$(".propsDetailPanel").removeClass('col-xs-12 col-sm-12 col-sm-8').addClass('col-xs-10 col-sm-10');
					$(".openPropsList i").removeClass('fa-times').addClass('fa-sliders')
					$("#detailFilterCountInBtn").removeClass("d-none")
				});
			});
			$(".openPropsList").on('click', function (e) {
				e.stopImmediatePropagation();
				$(".tooltip").remove();
				$(this).removeAttr("aria-describedby")
				if ($(".allPropsPanel").is(":visible") && $(".allFilterPanel").is(":visible")) {
					$(".aap-nav-panel").hide(300, function () {
						$(".propsDetailPanel").removeClass('col-xs-12 col-xs-8 col-sm-12 col-sm-8').addClass('col-xs-12');
						$(".openPropsList i").removeClass('fa-sliders fa-times').addClass('fa-list')
					});
				} else if ($(".allPropsPanel").is(":visible")) {
					$(".propsDetailPanel").removeClass('col-xs-10 col-sm-10').addClass('col-xs-8 col-sm-8')
					$(".openPropsList i").removeClass('fa-sliders').addClass('fa-times');
					$("#detailFilterCountInBtn").removeClass("d-none").addClass("d-none");
					$(".allFilterPanel").show(300);
				} else {
					$(".propsDetailPanel").removeClass('col-xs-12 col-sm-12').addClass('col-xs-10 col-sm-10')
					$(".openPropsList i").removeClass('fa-list').addClass('fa-sliders');
					$("#detailFilterCountInBtn").removeClass("d-none");
					$(".allPropsPanel").show(300);
				}
			})

			htmlExists(".proposition-collapse-container .collapse", function () {
				$("#dropdown_search .proposition-collapse-container .collapse,.proposal-compact .proposition-collapse-container .collapse").on("show.bs.collapse", function () {
					var id = $(this).data("id");
					var html = aapObj.directory.propositionCollapseHtml(aapObj, id);
					$(this).empty().html(html);
					aapObj.events.proposalCollapseItems(aapObj);
				})
			})

			coInterface.bindLBHLinks();
		}, proposalDropdown: function (aapObj) {
			$(".delete-proposal").off().on('click', function (e) {
				const hisProposal = $(this).attr("data-hisproposal");
				if (aapObj.canEdit || hisProposal == "true") {
					e.stopPropagation();
					$this = $(this);
					id = $(this).data("id");
					var proposalTitle = $(this).data("title");
					if (notEmpty(proposalTitle) == "") { proposalTitle = `(${trad["No name"]})`; }
					projectId = typeof $(this).data("project-id") != "undefined" ? $(this).data("project-id") : "";
					bootbox.dialog({
						title: trad.confirm,
						message: "<span class='bold'><i class='fa fa-warning'></i>" + trad["Are you sure you want to delete the proposal"] + " \"" + proposalTitle + "\"?</span><br><span class='text-red bold'>" + trad.actionirreversible + "</span>",
						buttons: [{
							label: "Ok", className: "btn btn-primary pull-left", callback: function () {
								getAjax("", baseUrl + "/survey/co/delete/id/" + id, function (data) {
									if (data.result) {
										toastr.success(trad["Proposal successfully deleted"] + " !");
										$("#list-" + id + `, #propItem${id}`).remove();
										$(`.aap-results-container #dropdown_search_detail .props-item:first`).trigger('click');
										ajaxPost("", baseUrl + '/survey/answer/rcnotification/action/deleteproposition/answerid/' + $this.data('id'), {
											proposition: $this.data('proposition'), url: window.location.href
										}, function (data) {
											ajaxPost(null, typeof coWsConfig != "undefined" && coWsConfig.pingRefreshViewUrl ? coWsConfig.pingRefreshViewUrl : null, {
												action: "proposalDeleted",
												answerId: $this.data("id"), userSocketId: aapObj.userSocketId, responses: {
													...data, ...{
														context: {
															[aapObj?.context?.id ? aapObj?.context?.id : "sjqhgdghjeb"]: aapObj?.context
														}
													}
												}
											}, null, null, { contentType: 'application/json' });
										})
										if (projectId != "") {
											bootbox.dialog({
												title: trad["Delete the associated project"] + " ?",
												message: "<span class='text-red bold'><i class='fa fa-warning'></i> " + trad.actionirreversible + "</span>",
												buttons: [{
													label: "Ok", className: "btn btn-primary pull-left", callback: function () {
														var url = baseUrl + "/" + moduleId + "/element/delete/id/" + projectId + "/type/projects";
														var param = new Object;
														param.reason = ""
														ajaxPost(null, url, param, function (data) {
															if (data.result) {
																toastr.success(data.msg);
															} else {
																toastr.error(data.msg);
															}
														});
													}
												}, {
													label: "Annuler", className: "btn btn-default pull-left", callback: function () {
														urlCtrl.loadByHash(location.hash);
													}
												}]
											});
										} else {
											urlCtrl.loadByHash(location.hash);
										}
									}
								}, "html");


							}
						}, {
							label: "Annuler", className: "btn btn-default pull-left", callback: function () {
							}
						}]
					});
				}
			})
			$('.overview-proposal').off().on('click', function () {
				const answerId = $(this).data("id");
				const query = aapObj.common.getQuery(aapObj);
				const new_hash = `proposalAap.context.${query.context}.formid.${query.formid}.answerId.${answerId}${document.location.href.indexOf('?') > -1 ? '?' + document.location.href.split('?')[1] : ''}`;
				history.pushState({}, null, `${document.location.origin}${document.location.pathname}#${new_hash}`);
				aapObj.common.modalProposalDetail(aapObj, answerId, true);
			});
			$('.form-proposal').off().on('click', function () {
				const answerId = $(this).data("id");
				const query = aapObj.common.getQuery(aapObj);
				// const new_hash =
				// `editCoformAap.context.${query.context}.formid.${query.formid}.answerId.${answerId}${document.location.href.indexOf('?') > -1 ?
				// '?' + document.location.href.split('?')[1] : ''}`;
				var substitutes = {
					context: query.context,
					formid: query.formid,
					answerId: notEmpty(answerId) ? answerId : ""
				};
				if (notEmpty(query?.["userId"])) {
					substitutes["userId"] = query?.["userId"]
				}
				var hash = aapObj.common.buildUrlQuery("#editCoformAap", substitutes, aapObj.common.getQueryStringObject());
				history.pushState(null, null, document.location.origin + document.location.pathname + hash);
				// history.pushState({}, document.title, `${document.location.origin}${document.location.pathname}#${new_hash}`);
				urlCtrl.loadByHash(location.hash);
			})
		}, projectAvancement: function (aapObj) {
			$(`.avancement-dropdown[data-toggle="dropup"]`).off().on('click', function (e) {
				var self = this;
				let statusObj = {
					statusDropdown: $(aapObj.directory.avancementDropdown_template_html(aapObj, $(self).attr('data-avancement'), Object.keys(aapObj.avancementProjectData.data), $(self).attr('data-id'))),
					dropdownHeight: 0
				}
				$('.avancementDropdown.dropdown-menu').remove()
				$('body').append(statusObj.statusDropdown);
				statusObj.dropdownHeight = $('.dropdown-menu.custom-scroll-bar').height();
				statusObj.statusDropdown.css({
					display: 'block', left: e.pageX - 120 + 'px', top: e.pageY - statusObj.dropdownHeight - 5 + 'px', // right : '22vw',
					height: '0px', width: '15%', transition: 'height .3s ease'
				});
				statusObj.statusDropdown.animate({
					// display: 'block',
					// position: 'absolute',
					// left: e.pageX +'px',
					height: statusObj.dropdownHeight + 'px'
				}, 300);
				aapObj.events.projectStatusDropdown(aapObj)
			});
			$(document).on('click', function (e) {
				if (!$(e.target).is(`.avancement-dropdown[data-toggle="dropup"]`)) {
					$('.avancementDropdown.dropdown-menu').remove()
				}
			});
			$('.dropdown').on('show.bs.dropdown', function () {
				$('body .avancementDropdown.dropdown-menu').remove()
			})
		}, projectDropdown: function (aapObj) {
			$('.overview-project').off().on('click', function () {
				const projectId = $(this).data("id");
				const query = aapObj.common.getQuery(aapObj);
				const new_hash = `projectsAap.context.${query.context}.formid.${query.formid}.projectId.${projectId}${document.location.href.indexOf('?') > -1 ? '?' + document.location.href.split('?')[1] : ''}`;
				history.pushState({}, null, `${document.location.origin}${document.location.pathname}#${new_hash}`);
				aapObj.common.modalProjectDetail(aapObj, projectId);
			});
			$(".delete-project").off().on('click', function (e) {
				const hisProposal = $(this).attr("data-hisproposal");
				if (aapObj.canEdit || hisProposal == "true") {
					e.stopPropagation();
					$this = $(this);
					id = $(this).data("id");
					var projectTitle = $(this).data("title");
					if (notEmpty(projectTitle) == "") { projectTitle = `(${trad["No name"]})`; }
					answerId = typeof $(this).data("answer-id") != "undefined" ? $(this).data("answer-id") : "";
					bootbox.dialog({
						title: trad.confirm,
						message: "<span class='bold'><i class='fa fa-warning'></i>" + trad["Are you sure you want to delete the project"] + " \"" + projectTitle + "\"?</span><br><span class='text-red bold'>" + trad.actionirreversible + "</span>",
						buttons: [{
							label: "Ok", className: "btn btn-primary pull-left", callback: function () {
								var thisPost = {};
								thisPost.reason = ""
								ajaxPost("", baseUrl + "/" + moduleId + "/element/delete/id/" + id + "/type/projects", thisPost, function (data) {
									if (data.result) {
										toastr.success(trad["Project successfully deleted"] + " !");
										// ajaxPost(null, typeof coWsConfig != "undefined" && coWsConfig.pingRefreshViewUrl ? coWsConfig.pingRefreshViewUrl : null, {
										// 	action : "projectDeleted",
										// 	answerId : $this.data("id"), userSocketId : aapObj.userSocketId, responses : {
										// 		...data, ...{
										// 			context : {
										// 				[aapObj?.context?.id ? aapObj?.context?.id : "sjqhgdghjeb"] : aapObj?.context
										// 			}
										// 		}
										// 	}
										// }, null, null, {contentType : 'application/json'});
										if (answerId != "") {
											dataHelper.path2Value({
												id: answerId,
												collection: "answers",
												value: null,
												pull: "project",
												path: "project"
											}, function (responses) {

											})
											bootbox.dialog({
												title: trad["Delete the associated proposal"] + " ?",
												message: "<span class='text-red bold'><i class='fa fa-warning'></i> " + trad.actionirreversible + "</span>",
												buttons: [{
													label: "Ok", className: "btn btn-primary pull-left", callback: function () {
														var url = baseUrl + "/survey/co/delete/id/" + answerId
														getAjax(null, url, function (data) {
															if (data.result) {
																toastr.success(data.msg);
															} else {
																toastr.error(data.msg);
															}
															urlCtrl.loadByHash(location.hash);
														});
													}
												}, {
													label: "Annuler", className: "btn btn-default pull-left", callback: function () {
														urlCtrl.loadByHash(location.hash);
													}
												}]
											});
										} else {
											urlCtrl.loadByHash(location.hash);
										}
									}
								}, "html");


							}
						}, {
							label: "Annuler", className: "btn btn-default pull-left", callback: function () {
							}
						}]
					});
				}
			})
		}, projectMenuIcon: function () {
			htmlExists('.menu-detail-project', function (sel) {
				sel.off().on('click', function (e) {
					e.stopPropagation();
					setTimeout(() => {
						const parameters = {
							context: aapObj.context.slug,
							formid: aapObj.form._id.$id
						};
						if (notEmpty(aapObj.defaultProject)) {
							parameters.projectId = aapObj.defaultProject._id.$id;
							parameters.answerId = notEmpty(aapObj.defaultAnswer) ? aapObj.defaultAnswer._id.$id : '';
							urlCtrl.loadByHash(aapObj.common.buildUrlQuery('#detailProjectAap', parameters, aapObj.common.getQueryStringObject()));
						}
					}, 100);
				})
			});

			htmlExists('.menu-detail-my-project', function (sel) {
				sel.off().on('click', function (e) {
					e.stopPropagation();
					setTimeout(() => {
						const parameters = {
							context: aapObj.context.slug,
							formid: aapObj.form._id.$id
						};
						if (notEmpty(aapObj.myDefaultProject)) {
							parameters.projectId = aapObj.myDefaultProject._id.$id;
							parameters.answerId = notEmpty(aapObj.myDefaultProposal) ? aapObj.myDefaultProposal._id.$id : '';
							parameters.userId = notEmpty(aapObj.userId) ? aapObj.userId : '';
							parameters.projectUserId = typeof userId != "undefined" && userId ? userId : '';
							urlCtrl.loadByHash(aapObj.common.buildUrlQuery('#detailProjectAap', parameters, aapObj.common.getQueryStringObject()));
						}
					}, 100);
				})
			});
		}, projects: function (aapObj) {
			$('.project-access').off('click').on('click', function () {
				const dataset = this.dataset;
				const query = aapObj.common.getQuery(aapObj);
				const title = dataset.title;
				const answer = dataset.answer;
				aapObj.filterSearch.activeFilters = aapObj.common.getActivateFilters(aapObj, "#filterContainerL", "#activeFilters .filters-activate")
				var hashParams = {
					context: query.context,
					formid: query.formid,
					projectId: dataset.target,
					aapview: ''
				};
				if (notEmpty(query?.["projectUserId"])) {
					hashParams.projectUserId = query?.projectUserId
				}
				if (answer) {
					hashParams.answerId = answer
				}
				history.pushState({}, null, document.location.origin + document.location.pathname + aapObj.common.buildUrlQuery('#detailProjectAap', hashParams, aapObj.common.getQueryStringObject()));
				// document.title = title;
				sessionStorage.setItem("aapview", "kanban");
				urlCtrl.loadByHash(location.hash);
			});
			aapObj.events.inviteToProposalDropdown(aapObj)

			$('.project-name').off().on('click', function () {
				const projectId = $(this).data("id");
				const associatedAnswerId = $(this).attr("data-answer");
				const title = $(this).data("title");
				const query = aapObj.common.getQuery(aapObj);
				// const new_hash =
				// `projectsAap.context.${query.context}.formid.${query.formid}.projectId.${projectId}${document.location.href.indexOf('?') > -1 ?
				// '?' + document.location.href.split('?')[1] : ''}`;

				var urlQuery = {
					context: query.context,
					formid: query.formid,
					projectId: projectId
				};
				if (notEmpty(associatedAnswerId)) {
					urlQuery.answerId = associatedAnswerId;
				}
				if (notEmpty(query?.["projectUserId"]) && typeof userId != "undefined" && userId) {
					urlQuery["projectUserId"] = query?.["projectUserId"]
				}
				var newHash = aapObj.common.buildUrlQuery('#detailProjectAap', urlQuery, aapObj.common.getQueryStringObject());
				history.pushState({}, null, document.location.origin + document.location.pathname + newHash);
				// document.title = title;
				// aapObj.common.modalProjectDetail(aapObj, projectId, aswerId);
				urlCtrl.loadByHash(newHash);
			});
			$(".filterContainerL.filter-alias .alias-main-search-bar").off().on("keyup", function (e) {
				$(".filter-alias .alias-main-search-bar").val($(this).val())
				if (e.keyCode != 13) {
					aapObj.common.filterItems($(this), true, null, null, "#filterContainerL");
				}

			})

			aapObj.events.projectMenuIcon();
			aapObj.events.projectAvancement(aapObj)
		}, projectDetail: function (aapObj) {
			changeListView();
			$("#dropdown_search_detail .processingLoader").remove();
			$("#dropdown_search_detail .divEndOfresults").hide();
			var queryParams = aapObj.common.getQuery();
			const projectItemId = queryParams['projectId'] ? queryParams['projectId'] : null;
			const answerItemId = queryParams['answerId'] ? queryParams['answerId'] : null;
			var findInterval = null;
			$('.props-item.active').removeClass('active');

			$("#dropdown_search_detail").find("#propItem" + projectItemId).addClass("active");
			if (notEmpty($("#dropdown_search_detail").find("#propItem" + projectItemId).attr("data-answer-id"))) {
				$(".has-proposal").show();
				$(".has-proposal a").attr("data-answer", $("#propItem" + projectItemId).attr("data-answer-id"));
			} else {
				$(".has-proposal").hide();
			}
			$('#listPropsPanel').on('shown.bs.collapse', function () {
				$(".panel-heading .panel-title i").addClass('fa-caret-up').removeClass('fa-caret-down');
			}).on('hidden.bs.collapse', function () {
				$(".panel-heading .panel-title i").addClass('fa-caret-down').removeClass('fa-caret-up');
			});
			if (aapObj.hasInviteInputs && aapObj.canEdit) {
				$('.invite-tab a.btn-invite-proposal').show()
				$('li.invite-tab').next().css("margin-right", "inherit")
			} else {
				$('.invite-tab a.btn-invite-proposal').remove();
				// if (aapObj.hasInviteInputs && userId && answerItemId) {
				if (aapObj.hasInviteInputs && userId) {
					if (functionCurrentlyLoading && !functionCurrentlyLoading["alreadyContributors"]) {
						aapObj.common.alreadyContributors(userId, answerItemId, (data) => {
							if (typeof data.result != "undefined" && data.result == false) {
								$('.invite-tab').empty();
								$('.invite-tab').append(`
									<a href="javascript:;" class="pull-right text-center btn-join-proposal link-banner text-ellipsis"
									data-placement="bottom" data-original-title="Rejoindre comme contributeur à cette proposition" data-answerid="${notEmpty(answerItemId) ? answerItemId : ""}"
									${data.elt && data.elt.project && data.elt.project.id ? `data-projectid="${data.elt.project.id}"` : (!notEmpty(answerItemId) && data.elt && data.elt._id ? `data-projectid="${data.elt._id.$id}"` : "")}>
										<i class="fa fa-user-plus a-icon-banner"></i>
										<span class="title-link-banner hidden-sm">
											${trad.Join}
										</span>
									</a>
								`)
								aapObj.events.bindJoinBtnEvent()
							} else if (data.result == true) $('.invite-tab a.btn-join-proposal').remove();
						}, {
							projectId: projectItemId
						});
					}
				} else {
					$('li.invite-tab').next().css("margin-right", "50px");
					$('.invite-tab a.btn-join-proposal').remove()
				}
			}
			if (notEmpty(answerItemId) && (aapObj.context?.links?.members?.[userId] || aapObj.canEdit))
				$('li a.btn-comment-proposal').show()
			else
				$('li a.btn-comment-proposal').hide()
			$('.btn-comment-proposal').off('click').on('click', function () {
				if (answerItemId) {
					commentObj.openPreview('answers', answerItemId, '', $('.aap-page-info-left h3 .main').text().replace(/'/g, "\\'"));
				} else {
					commentObj.openPreview('projects', projectItemId, '', $('.aap-page-info-left h3 .main').text().replace(/'/g, "\\'"));
				}
			})
			aapObj.events.inviteToProposalDropdown(aapObj)
			$('.props-item').off('click').on('click', function () {
				if ($(window).width() < 768) {
					if ($("#propsFilterTab").is(":visible")) {
						$("#show-list-xs").find("i").removeClass("fa-times").addClass("fa-sliders");
						$(".btnCloseMenuList").hide();
					}
					$("#propsFilterTab").hide(300);
				}
				const self = $(this);
				const target = self.attr('data-target');
				const answer = self.data('answer-id');
				const title = self.attr('data-title');
				const urlQuery = aapObj.common.getQuery();
				var view = urlQuery.aapview ? urlQuery.aapview : 'action';
				$('.props-item.active').removeClass('active');
				self.addClass('active');
				$('h3.title span.main').text(title);
				if (!notEmpty(answer)) {
					$(".has-proposal").hide();
					view == "summary" || view == "evaluation" || view == "funding" ? view = "action" : ""
				} else {
					$(".has-proposal").show();
				}

				var substitutes = {
					context: query.context,
					formid: query.formid,
					projectId: target,
					aapview: view,
					answerId: notEmpty(answer) ? answer : ""
				};
				if (notEmpty(urlQuery?.["projectUserId"])) {
					substitutes["projectUserId"] = urlQuery?.["projectUserId"]
				}
				var hash = aapObj.common.buildUrlQuery(document.location.hash, substitutes, aapObj.common.getQueryStringObject());
				$('.detail-breadcrumb').attr('href', hash).text(title);
				if ($(".detail-breadcrumb").is(":visible") && title) {
					const currentTitle = document.title;
					const newTitle = currentTitle.includes('|') ? currentTitle.split('|')[1].trim() : currentTitle;
					document.title = `${ucfirst(title)} | ${newTitle}`;
				}
				history.pushState(null, title, document.location.origin + document.location.pathname + hash);
				queryParams = aapObj.common.getQuery();

				var tab_doms = $('.propsDetailPanel .aap-nav-tabs>li>[role=tab][data-project]');
				tab_doms.filter(function () {
					return $(this).attr('href') !== '#' + view;
				}).addClass('need-reload');
				if (aapObj.hasInviteInputs && notEmpty(answer) && aapObj.canEdit) {
					$('.invite-tab a.btn-invite-proposal').show()
					$('li.invite-tab').next().css("margin-right", "inherit")
				} else {
					$('.invite-tab a.btn-invite-proposal').hide()
					$('li.invite-tab').next().css("margin-right", "50px")
				}
				if (notEmpty(answer) && (aapObj.context?.links?.members?.[userId] || aapObj.canEdit))
					$('li a.btn-comment-proposal').show()
				else
					$('li a.btn-comment-proposal').hide()
				$('.btn-invite-proposal').attr('data-projectid', target)
				$('.btn-invite-proposal').attr('data-answerid', answer)
				aapObj.common.loadProjectDetail(aapObj, target, answer, view);

				const plan_content_dom = $('#plan');
				if (plan_content_dom.length && !plan_content_dom.hasClass('active'))
					plan_content_dom.addClass('need-reload');
				$('.aap-nav-tabs [data-project]').attr('data-project', target).attr('data-answer', answer);

				/*if(!notEmpty(queryParams?.answerId)){
				 $("#project-detailProposition").attr("disabled","disabled");
				 }else{
				 $("#project-detailProposition").removeAttr("disabled","disabled");
				 $("#project-detailProposition").off().on('click',function(){
				 if(!$(this).is('[disabled=disabled]')){
				 var new_hash = `#detailProposalAap.context.${aapObj.context.slug}.formid.${aapObj.form._id.$id}.answerId.${queryParams?.answerId}.projectId.${queryParams.projectId}`;
				 urlCtrl.loadByHash(new_hash);
				 }
				 })
				 }*/
				$('.btn-comment-proposal').off('click').on('click', function () {
					if (answer) {
						commentObj.openPreview('answers', answer, '', $('.aap-page-info-left h3 .main').text().replace(/'/g, "\\'"));
					} else {
						commentObj.openPreview('answers', target, '', $('.aap-page-info-left h3 .main').text().replace(/'/g, "\\'"));
					}
				})
				aapObj.common.switchProjectToProposalDetail(aapObj, queryParams);
			});

			$(".props-item .text-ellipsis").each(function () {
				if ($(this)[0] && $(this)[0].scrollWidth && $(this).width()) {
					if ($(this)[0].scrollWidth - $(this).width() == 0) {
						$(this).removeClass("hover-marquee")
					} else {
						$(this).removeClass("hover-marquee").addClass("hover-marquee")
					}
				}
			})

			var tabs = $('.propsDetailPanel .aap-nav-tabs>li>[role=tab][data-project]');
			tabs.off("shown.bs.tab").on('shown.bs.tab', function () {
				var self = $(this);
				var view = self.attr('href').substring(1);
				var dataset = this.dataset;
				var project = dataset.project;
				var answer = dataset.answer;
				var query = aapObj.common.getQuery(aapObj);
				var substitutes = {
					context: query.context,
					formid: query.formid,
					projectId: project,
					aapview: view,
					answerId: answer
				};
				if (query.userId) {
					substitutes.userId = query.userId;
				}
				var new_hash = aapObj.common.buildUrlQuery(document.location.href, substitutes, aapObj.common.getQueryStringObject());
				history.pushState({}, {}, new_hash);
				$('.contributor-project-filter').css({
					justifySelf: '',
					width: ''
				}).empty().html('');
				if (self.hasClass('need-reload'))
					aapObj.common.loadProjectDetail(aapObj, project, answer, view);
				if ($(".portfolio-modal.modal #aboutelem").is(":visible"))
					$(".portfolio-modal.modal.vertical").css("top", 0);
			});
			var query = aapObj.common.getQuery();
			var tabView = query.aapview ? query.aapview : 'action';
			var activeTabs = tabs.filter(function () {
				return $(this).hasClass('active');
			});
			if (activeTabs.length === 0) {
				tabs.filter(function () {
					return $(this).attr('href').indexOf(tabView) === 1;
				}).tab('show');
			}

			$("#propsDetail.filterContainerD .alias-main-search-bar").off().on("keyup", function (e) {
				if (e.keyCode != 13) {
					aapObj.common.filterItems($(this), true, "#dropdown_search_detail", "a.props-item", "#filterContainerD");
				}

			})
			if ($("#dropdown_search_detail a.props-item").length == 0) {
				$("#dropdown_search_detail .divEndOfresults").show();
			}

			$("#btnHideFilterProps").off('click').on('click', function (e) {
				e.stopImmediatePropagation();
				let selector = ".allPropsPanel";
				$(".allPropsPanel").is(":visible") && $(".allFilterPanel").is(":visible") ? selector = ".aap-nav-panel" : "";
				$(selector).hide(300, function () {
					$(".propsDetailPanel").removeClass('col-xs-10 col-xs-8 col-sm-10 col-sm-8').addClass('col-xs-12');
					$("#detailFilterCountInBtn").removeClass("d-none").addClass("d-none");
					$(".openPropsList i").removeClass('fa-sliders fa-times').addClass('fa-list')
				});
			});
			$("#btnHideFilterOnly").off('click').on('click', function (e) {
				e.stopImmediatePropagation();
				$(".allFilterPanel").hide(300, function () {
					$(".propsDetailPanel").removeClass('col-xs-12 col-sm-12 col-sm-8').addClass('col-xs-10 col-sm-10');
					$(".openPropsList i").removeClass('fa-times').addClass('fa-sliders');
					$("#detailFilterCountInBtn").removeClass("d-none")
				});
			});
			$(".openPropsList").off('click').on('click', function (e) {
				e.stopImmediatePropagation();
				$(".tooltip").remove();
				$(this).removeAttr("aria-describedby")
				if ($(".allPropsPanel").is(":visible") && $(".allFilterPanel").is(":visible")) {
					$(".aap-nav-panel").hide(300, function () {
						$(".propsDetailPanel").removeClass('col-xs-12 col-xs-8 col-sm-12 col-sm-8').addClass('col-xs-12');
						$(".openPropsList i").removeClass('fa-sliders fa-times').addClass('fa-list')
					});
				} else if ($(".allPropsPanel").is(":visible")) {
					$(".propsDetailPanel").removeClass('col-xs-10 col-sm-10').addClass('col-xs-8 col-sm-8');
					$(".openPropsList i").removeClass('fa-sliders').addClass('fa-times');
					$("#detailFilterCountInBtn").removeClass("d-none").addClass("d-none");
					$(".allFilterPanel").show(300);
				} else {
					$(".propsDetailPanel").removeClass('col-xs-12 col-sm-12').addClass('col-xs-10 col-sm-10')
					$(".openPropsList i").removeClass('fa-list').addClass('fa-sliders')
					$("#detailFilterCountInBtn").removeClass("d-none");
					$(".allPropsPanel").show(300);
				}
			});
			var inputTimeout;
			$('#multi-view-filter-input').off('input').on('input', function () {
				var query = aapObj.common.getQuery();
				var self = $(this);
				clearTimeout(inputTimeout);
				inputTimeout = setTimeout(function () {
					aapObj.common.load_project_detail_html(aapObj, query.projectId, '#plan', {
						action: self.val(),
						contributor: $('#multi-view-filter-contributor').val() !== 'all' ? $('#multi-view-filter-contributor').val() : ''
					});
				}, 500);
			});
			$('#multi-view-filter-contributor').off('change').on('change', function () {
				var self = $(this);
				var query = aapObj.common.getQuery();
				history.pushState({}, null, aapObj.common.buildUrlQuery(document.location.href, { userId: self.val() !== 'all' ? self.val() : '' }, aapObj.common.getQueryStringObject()));
				aapObj.common.load_project_detail_html(aapObj, query.projectId, '#plan', {
					action: $('#multi-view-filter-input').val(), contributor: $(this).val() !== 'all' ? $(this).val() : ''
				});
			});
		}, exceltable: function (aapObj) {

		}, menuRightDropdown: function (aapObj) {
			$('.communication-tools').off().on('click', function () {
				smallMenu.openAjaxHTML(baseUrl + '/co2/aap/getviewbypath/path/co2.views.aap.partials.communicationTools/params/' + aapObj.form._id.$id);
			});
			$('.dropdown-menu .aap-menu-right-item-dropdown a.invite-member').attr("onclick", "return aapObj.common.addLinkToCommunityAttr()")

			$(".copylinkstandalone").off().on("click", function () {
				var sampleTextarea = document.createElement("textarea");
				document.body.appendChild(sampleTextarea);
				sampleTextarea.value = baseUrl + "/costum/co/index/slug/" + costum.slug + "#answer.answer.id.new.form." + aapObj.form["_id"]["$id"] + ".mode.w.standalone.true.ask.true"; //save main text in it
				sampleTextarea.select();
				document.execCommand("copy");
				document.body.removeChild(sampleTextarea);
				toastr.success(trad.copied);
			});

			$('.email-lists').off().on('click', function () {
				smallMenu.openAjaxHTML(baseUrl + '/co2/aap/getviewbypath/path/co2.views.aap.partials.maillist./params/' + aapObj.form["_id"]["$id"]);
			})

			$('.export-csv').off().on('click', function () {
				$('#csv-exportation').modal('show');
			})

			$(".btn-download-csv-financer").off().on("click", function () {
				var thisbtnpdf = $(this);
				thisbtnpdf.addClass("pdfloading");

				ajaxPost("", baseUrl + '/co2/aap/exportcsvfinancer', {
					champs: [], query: aapObj.filterSearch.proposal.search.obj
				}, function (response) {
					const { Parser } = json2csv;
					const fields = response.heads;
					const json2csvParser = new Parser({
						fields, delimiter: ";"
					});
					const csv = json2csvParser.parse(response.bodies);
					var exportedFilenmae = 'export-financer.csv';
					//var universalBOM = "\uFEFF";
					var blob = new Blob([new Uint8Array([0xEF, 0xBB, 0xBF]), csv], {
						type: 'text/csv;charset=utf-8;'
					});
					if (navigator.msSaveBlob) {
						// IE 10+
						navigator.msSaveBlob(blob, exportedFilenmae);
					} else {
						var link = document.createElement("a");
						if (link.download !== undefined) {
							// feature detection
							// Browsers that support HTML5 download attribute
							var url = URL.createObjectURL(blob);
							link.setAttribute("href", url);
							link.setAttribute("download", exportedFilenmae);
							link.addEventListener('click', function () {
								setTimeout(function () {
									document.body.removeChild(link);
									URL.revokeObjectURL(url);
								}, 500);
							})
							link.style.visibility = 'hidden';
							document.body.appendChild(link);
							link.click();
						}
					}
				}, "html");
				return false;
			});

			$(".manage-notification").off().on("click", function () {
				tplCtx.id = aapObj.form["_id"]["$id"];
				tplCtx.collection = "Forms";
				tplCtx.path = "paramsNotification";
				var prms = aapObj.dynform.configNotification(aapObj);
				dyFObj.openForm(prms, null, aapObj.form?.paramsNotification);
			});

			$('.duplicate-prop-tools').off().on('click', function () {
				smallMenu.openAjaxHTML(baseUrl + '/co2/aap/getviewbypath/path/co2.views.aap.partials.duplicatePropTools/params/' + aapObj.form["_id"]["$id"]);
			})

			$('.import-csv').click(function () {
				var content = `
                <div class="form-group col-xs-12 addfromcsvdiv">
                    <div>
                        <label class="info text-center text-dark-blue"> <h4> ${trad["Importer à partir d'un fichier csv"]} </h4></label>
                    </div>
					<div id="file-drop-area" style="border: 2px dashed #ccc;border-radius: 20px;width: 100%;padding: 20px;text-align: center;margin: 10px auto;">
						<label for="inputcsvdata" style="cursor: pointer;color: #00b1ca;font-weight: bold;">
						<b>Déposer un fichiers <b/> ou 
							<input type="file"  id="inputcsvdata" class="inputcsvdata btn btn-default" name="inputcsvdata" accept=".csv">
						</label>
					</div>
                    <div class="col-md-12">
                        <div class="form-group col-md-4">
                            <label for="delimiterinput">Delimiteur</label>
                                <select name="delimiterinput" class="form-control delimiterinput">
                                    <option value=","> ${trad.Comma} </option>
                                    <option value=";"> ${trad.Semicolon} </option>
                                </select>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="delimiterendlineinput">Delimiteur fin de ligne</label>
                            <select name="delimiterendlineinput" class="form-control delimiterendlineinput">
                                <option value="ral"> ${trad["Back to line"]} </option>
                                <option value="bs"> ${trad["Backslash"]} </option>
                            </select>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="headerinput">${trad["With a title in the header"]}</label>
                            <select name="headerinput" class="form-control headerinput" >
                                <option value="true"> ${trad.yes} </option>
                                <option value="false">${trad.no} </option>
                            </select>
                        </div>
                        <div class="form-group col-md-4 hidden">
                            <a href="" class="button save-uploade-csv btn-aap-primary">${trad.save}</a>
                        </div>
                    </div>

                    <div class="col-md-12 containertablecss">
                        <div class="addfromcsvdivrs csvdivcontainer">  </div>
                    </div>
                </div>
                `;

				prioModal = bootbox.dialog({
					message: content, show: false, size: "large", className: 'csvdialog', buttons: {
						success: {
							label: trad.save, className: "btn-primary", callback: function () {
								var ext = $('.csvdialog .inputcsvdata').val().split(".").pop();

								if ($.inArray(ext, ["csv"]) == -1) {
									toastr.error(tradDynForm.youMustUseACSVFormat);
									return false;
								}

								if ($('.csvdialog .inputcsvdata').prop('files')[0] != undefined) {

									var reader = new FileReader();

									reader.onload = function (e) {
										var csvval, csvdata = []
										allData = {};

										if ($(".csvdialog .delimiterendlineinput").val() == "ral") {
											var csvval = e.target.result.split("\n");
										} else if ($(".csvdialog .delimiterendlineinput").val() == "bs") {
											var csvval = e.target.result.split("\\");
										}

										var collumnnumber = 0;
										var collummax = 0;

										$.each(csvval, function (ind, val) {
											var thh = val.split($(".csvdialog .delimiterinput").val());
											csvdata[ind] = thh;

											collumnnumber = 0;
											$.each(thh, function (thhid, thhhval) {
												collumnnumber++;
											});
											if (collumnnumber > collummax) {
												collummax = collumnnumber;
											}
										});

										var csvparams = [];

										var ii = 0

										while (ii < collummax) {
											var pscsv = {};
											pscsv["path"] = $(".csvdialog th[data-id='" + ii + "'] .inputpath").val();
											pscsv["type"] = $(".csvdialog th[data-id='" + ii + "'] .inputtype").val();
											if ($(".csvdialog th[data-id='" + ii + "'] .inputtitlepath").val() != "") {
												pscsv["tittle"] = $(".csvdialog th[data-id='" + ii + "'] .inputtitlepath").val();
											}
											csvparams.push(pscsv);
											ii++;
										}

										allData = {
											form: aapObj.form["_id"]["$id"], parent: {}, params: csvparams, data: csvdata
										};


										if (typeof contextId != "undefined") {
											allData.parent[contextId] = {
												type: contextData.type, name: contextData.name,
											}
										}

										tot = allData["data"].length;
										totData = allData["data"];
										chunk = 50
										ix = 0;

										for (i = 0; i - 1 < tot / chunk; i++) {

											allData["data"] = totData.slice(ix, i * chunk);

											ajaxPost('', baseUrl + "/survey/answer/importanswer", allData, function (data) {
											}, "html");

											ix = i * chunk
										}

									};

									reader.readAsText($('.csvdialog .inputcsvdata').prop('files')[0]);

								} else {
									toastr.error(tradDynForm.weWereUnableToReadYourFile);
								}
								return false;
							}
						}, cancel: {
							label: trad.cancel, className: "btn-secondary", callback: function () {
								prioModal.modal('hide');
							}
						}
					}, onEscape: function () {
						prioModal.modal('hide');
					}
				});

				prioModal.on('shown.bs.modal', function (e) {

					$(".csvdialog select").change(function (e) {
						//$(".inputcsvdata").trigger('change');
						handleFiles();
					});

					$(".inputcsvdata").change(function (e) {
						var file = e.target.files;
						aapObj.file = file;
						handleFiles();
					});
				});

				prioModal.modal("show");
				var dropArea = document.getElementById('file-drop-area');
				dropArea.addEventListener('dragover', function (e) {
					e.preventDefault();
					dropArea.classList.add('highlight');
				});

				dropArea.addEventListener('dragleave', function (e) {
					e.preventDefault();
					dropArea.classList.remove('highlight');
				});

				dropArea.addEventListener('drop', function (e) {
					e.preventDefault();
					dropArea.classList.remove('highlight');
					var files = e.dataTransfer.files;
					aapObj.file = files;
					handleFiles();
				});
			});
		}, summaryView: function () {
			$(`[data-schu-toggle="collapse"]`).off("click").on("click", function () {
				const btnToggle = $(this);
				const collapseParent = btnToggle.parent().parent();
				const collapseBody = $(collapseParent.find(btnToggle.attr("data-target")));
				if (collapseBody.is(":visible")) {
					collapseBody.hide(300);
				} else {
					collapseBody.show(300);
				}
			})
		},
	},
	preconfiguration: {
		initialized: false,
		slide: {
			activationstep: {
				timeline: "Ouverture aux réponses",
				callback: function (aapObj) {
					if ($('.inputpreconfig[data-actualstep="activationstep"]:checked').length > 0) {
						aapObj.preconfiguration.actions.save(aapObj, "active", true);
					} else {
						aapObj.preconfiguration.actions.save(aapObj, "active", false);
					}
				},
				view: function (aapObj, number) {
					var isActive = [
						{
							label: 'Ouvert au réponses',
							value: 'isActive',
							inputCallback: function () { },
							checked: 'checked'
						}
					];
					return aapObj.preconfiguration.views.createSlide([
						aapObj.preconfiguration.views.createLegend(1, `Ouverture aux réponses`, `La plateforme sera-elle ouverte à de nouvelles propositions`),
						aapObj.preconfiguration.views.createRadiobutton('activationstep', 'prestepname', isActive, ''),
						aapObj.preconfiguration.views.createNavButton('activationstep', 'datestep', 2, false, 'activation')
					]);
				}
			},
			datestep: {
				timeline: "Période d'ouverture",
				callback: function (aapObj) {
					if ($('.form-control[id="preconfig-startdate"][data-actualstep="datestep"]').val() != "") {
						aapObj.preconfiguration.actions.save(aapObj, "startDate", $('.form-control[id="preconfig-startdate"][data-actualstep="datestep"]').val(), "isoDate");
					}
					if ($('.form-control[id="preconfig-enddate"][data-actualstep="datestep"]').val() != "") {
						aapObj.preconfiguration.actions.save(aapObj, "endDate", $('.form-control[id="preconfig-enddate"][data-actualstep="datestep"]').val(), "isoDate");
					}
				},
				view: function (aapObj, number) {
					return aapObj.preconfiguration.views.createSlide([
						aapObj.preconfiguration.views.createLegend(number, `Période d'ouverture`, ``),
						`<div class="" >
							<div class="row createActivationStepRow">
								<div class="col-md-6">
									<label > Date de debut </label>
									<input type="datetime-local" id="preconfig-startdate" class="form-control" data-actualstep="datestep">
								</div>
								<div class="col-md-6">
									<label > Date de fin </label>
									<input type="datetime-local" id="preconfig-enddate" class="form-control" data-actualstep="datestep">
								</div>
					  		</div>
					  	</div>`,
						aapObj.preconfiguration.views.createInfo("Laisser vide la date d'ouverture si déjà ouvert , laisser vide la date de fin si toujours ouvert une fois debuté"),
						aapObj.preconfiguration.views.createNavButton('datestep', 'step0', number + 1, false, 'activation')
					]);
				}
			},
			step0: {
				timeline: "Type d'usage",
				callback: function (aapObj) {
					if ($('.inputpreconfig').val() == "coremuslide") {
						aapObj.preconfiguration.actions.save(aapObj, "coremu", true);
					}
				},
				view: function (aapObj, number) {
					var radiolistStep0 = [
						{
							label: 'Système de proposition',
							value: 'procecoslide',
							tooltip: 'When the proton wobbles for nowhere, all queens yearn interstellar, virtual sonic showers. ' +
								'Teleporters view with mineral! When the phenomenan trembles for nowhere, all particles teleport calm, photonic girls.',
							tooltipimg: defaultImage,
							inputCallback: function () { }
						},
						{
							label: "Système de coremuneration",
							value: 'coremuslide',
							tooltip: 'When the proton wobbles for nowhere, all queens yearn interstellar, virtual sonic showers. ' +
								'Teleporters view with mineral! When the phenomenan trembles for nowhere, all particles teleport calm, photonic girls.',
							tooltipimg: defaultImage,
							inputCallback: function () { }
						},
						{
							label: "Appel à projet pour le monde associatif , QPV, Quartier et indicateur INSEE",
							value: 'quartierslide',
							tooltip: 'When the proton wobbles for nowhere, all queens yearn interstellar, virtual sonic showers. ' +
								'Teleporters view with mineral! When the phenomenan trembles for nowhere, all particles teleport calm, photonic girls.',
							tooltipimg: defaultImage,
							inputCallback: function () { }
						}
					];

					return aapObj.preconfiguration.views.createSlide([
						aapObj.preconfiguration.views.createLegend(number, `Choisir un type usage de l'oceco`, `L’outil d’appel à projet peut servir à différents usages, Sélectionnez le votre dans la liste ci-dessous :`),
						aapObj.preconfiguration.views.createRadiobutton('step0', 'ocecotype', radiolistStep0, ''),
						aapObj.preconfiguration.views.createNavButton('step0', 'step1', number + 1, true, 'ocecotype')
					]);
				}
			},
			step1: {
				quartierslide: {
					timeline: "Quartier",
					callback: function (aapObj) {
						if ($('#quartierselect2').val() != "") {
							aapObj.preconfiguration.actions.save(aapObj, "coremu", $('#quartierselect2').val().split(' , '));
						}
					},
					view: {
						quartierslide: function (number) {
							var radiolistQSlide = [];

							return aapObj.preconfiguration.views.createSlide([
								aapObj.preconfiguration.views.createLegend(number, 'Configurer les quartiers', ''),
								`<div class="col-sm-12 col-md-12 margin-20">
									<label > Lister ci-dessous vos quuartier(s) </label>
									<input type="text" id="quartierselect2" class="">
								</div>`,
								aapObj.preconfiguration.views.createNavButton('step1', 'step2', number + 1)
							]);
						},
						procecoslide: function (number) {
							return aapObj.preconfiguration.slide.step2.view(number + 1);
						},
						coremuslide: function (number) {
							return aapObj.preconfiguration.slide.step2.view(number + 1);
						}
					}
				},
				procecoslide: function (number) {
					return aapObj.preconfiguration.slide.step2.view(number + 1);
				},
				coremuslide: function (number) {
					return aapObj.preconfiguration.slide.step2.view(number + 1);
				}
			},
			step1: {
				timeline: {
					quartierslide: 'Quartier',
					procecoslide: "Accéssibilité",
					coremuslide: "Accéssibilité"
				},
				callback: {
					quartierslide: function (number) {
						if ($('#quartierselect2').val() != "") {
							aapObj.preconfiguration.actions.save(aapObj, "coremu", $('#quartierselect2').val().split(','));
						}
					},
					procecoslide: function (number) {
						return aapObj.preconfiguration.slide.step2.callback(aapObj);
					},
					coremuslide: function (number) {
						return aapObj.preconfiguration.slide.step2.callback(aapObj);
					}
				},
				view: {
					quartierslide: function (aapObj, number) {
						var radiolistQSlide = [];

						return aapObj.preconfiguration.views.createSlide([
							aapObj.preconfiguration.views.createLegend(number, 'Configurer les quartiers', ''),
							`<div class="col-sm-12 col-md-12 margin-20">
									<label > Lister ci-dessous vos quuartier(s) </label>
									<input type="text" id="quartierselect2" class="">
								</div>`,
							aapObj.preconfiguration.views.createNavButton('step1', 'step2', number + 1)
						]);
					},
					procecoslide: function (aapObj, number) {
						return aapObj.preconfiguration.slide.step2.view(aapObj, number);
					},
					coremuslide: function (aapObj, number) {
						return aapObj.preconfiguration.slide.step2.view(aapObj, number);
					}
				}
			},
			step2: {
				timeline: "Accéssibilité",
				callback: function (aapObj) {
					if ($('.inputpreconfig[data-actualstep="step2"]').val() != "") {
						aapObj.preconfiguration.actions.save(aapObj, "onlymemberaccess", $('.inputpreconfig').val());
					}
				},
				view: function (aapObj, number) {
					var radiolistStep2 = [
						{
							label: 'Toute personne possedant un compte communecter',
							value: 'false',
							inputCallback: function () { }
						},
						{
							label: `Seuls les membres de la communauté de l'organisation`,
							value: 'true',
							inputCallback: function () { }
						}
					];

					return aapObj.preconfiguration.views.createSlide([
						aapObj.preconfiguration.views.createLegend(number, `Accéssibilité`, 'Qui peut ajouter une proposition'),
						aapObj.preconfiguration.views.createRadiobutton('step2', 'step2radio', radiolistStep2, ''),
						aapObj.preconfiguration.views.createInfo(`Pour plus de paramètre sur l'accès , rendez-vous sur le menu <i class="fa fa-cog"></i> "Configuration" et ensuite dans le sous-onglet "accès"`, ''),
						aapObj.preconfiguration.views.createNavButton('step2', 'step3', number + 1)
					]);
				}
			},
			step3: {
				timeline: "Droits de lecture et modification",
				callback: function (aapObj) {
					if ($('.inputpreconfig[data-actualstep="step3"]').val() != "") {
						var valonlyMembersCanSeeListAnswer = "";
						var valueonlyusercanedit = "";
						if ($('.inputpreconfig[data-actualstep="step3"]').val() == "choiceone") {
							valueonlyusercanedit = "true";
							valonlyMembersCanSeeListAnswer = "false";
						} else if ($('.inputpreconfig[data-actualstep="step3"]').val() == "choicetwo") {
							valueonlyusercanedit = "true";
							valonlyMembersCanSeeListAnswer = "true";
						} else if ($('.inputpreconfig[data-actualstep="step3"]').val() == "choicethree") {
							valueonlyusercanedit = "false";
							valonlyMembersCanSeeListAnswer = "true";
						}
						aapObj.preconfiguration.actions.save(aapObj, "onlyusercanedit", valueonlyusercanedit);
						aapObj.preconfiguration.actions.save(aapObj, "onlyMembersCanSeeListAnswer", valonlyMembersCanSeeListAnswer);
					}
				},
				view: function (aapObj, number) {
					var radiolistQSlide = [
						{
							label: 'Toute personne possedant un compte communecter peut voir le résumé',
							value: 'choiceone',
							inputCallback: function () { }
						},
						{
							label: `Seuls les membres de la communauté de l'organisation peuvent voir la résumé sans pouvoir modifier`,
							value: 'choicetwo',
							inputCallback: function () { }
						},
						{
							label: `Les membres de la communauté de l'organisation peuvent voir la résumé et modifier`,
							value: 'choicethree',
							inputCallback: function () { }
						}
					];

					return aapObj.preconfiguration.views.createSlide([
						aapObj.preconfiguration.views.createLegend(number, "Configurer la lecture et modification", 'Qui peut lire et ecrire une proposition dans la liste'),
						aapObj.preconfiguration.views.createRadiobutton('step3', 'step3name', radiolistQSlide, ''),
						aapObj.preconfiguration.views.createInfo(`Pour plus de paramètre sur l'accès , rendez-vous sur le menu <i class="fa fa-cog"></i> "Configuration" et ensuite dans le sous-onglet "accès"`, ''),
						aapObj.preconfiguration.views.createNavButton('step3', 'step4', number + 1)
					]);
				}
			},
			step4: {
				timeline: "Reponse autonome",
				callback: function (aapObj) {
					if ($('.inputpreconfig[data-actualstep="step4"]').val() != "") {
						var valtemporarymembercanreply = "";
						var valwithconfirmation = "";
						if ($('.inputpreconfig[data-actualstep="step4"]').val() == "choiceone") {
							valtemporarymembercanreply = "false";
							valwithconfirmation = "false";
						} else if ($('.inputpreconfig[data-actualstep="step4"]').val() == "choicetwo") {
							valtemporarymembercanreply = "true";
							valwithconfirmation = "false";
						} else if ($('.inputpreconfig[data-actualstep="step4"]').val() == "choicethree") {
							valtemporarymembercanreply = "true";
							valwithconfirmation = "true";
						}
						aapObj.preconfiguration.actions.save(aapObj, "temporarymembercanreply", valtemporarymembercanreply);
						aapObj.preconfiguration.actions.save(aapObj, "withconfirmation", valwithconfirmation);
					}
				},
				view: function (aapObj, number) {
					var radiolistQSlide = [
						{
							label: 'Disponible seulement au membre',
							value: 'choiceone',
							inputCallback: function () { }
						},
						{
							label: `Disponible en fournissant un email de préinscription`,
							value: 'choicetwo',
							inputCallback: function () { }
						},
						{
							label: `Disponible avec une vérification d'email`,
							value: 'choicethree',
							inputCallback: function () { }
						}
					];

					return aapObj.preconfiguration.views.createSlide([
						aapObj.preconfiguration.views.createLegend(number, "Configurer la page de reponse autonome", 'Qui peut acceder au page de reponse autonome'),
						aapObj.preconfiguration.views.createRadiobutton('step4', 'step4name', radiolistQSlide, ''),
						aapObj.preconfiguration.views.createInfo('', ''),
						aapObj.preconfiguration.views.createNavButton('step4', 'step5', number + 1)
					]);
				}
			},
			step5: {
				timeline: "Les etapes d'une proposition",
				callback: function (aapObj) {
					var tablestep5 = [];
					if ($('.inputpreconfig[data-actualstep="step5"]').val() != "") {
						$('.inputpreconfig[data-actualstep="step5"]:checked').each(function (item) {
							tablestep5.push($(this).val());
						});
					}
					aapObj.preconfiguration.actions.save(aapObj, "subForms", tablestep5);
				},
				view: function (aapObj, number) {
					var cklistQSlide = [
						{
							label: 'Formulaire de proposition',
							value: 'aapStep1',
							inputCallback: function () { },
							checked: 'checked'
						},
						{
							label: `Formulaire d'evaluation`,
							value: 'aapStep2',
							inputCallback: function () { },
							checked: 'checked'
						},
						{
							label: `Formulaire de financement`,
							value: 'aapStep3',
							inputCallback: function () { },
							checked: 'checked'
						},
						{
							label: `Formulaire de suivi`,
							value: 'aapStep4',
							inputCallback: function () { },
							checked: 'checked'
						}
					];

					return aapObj.preconfiguration.views.createSlide([
						aapObj.preconfiguration.views.createLegend(number, "Configurer les etapes", `Les etapes active du formulaire`),
						aapObj.preconfiguration.views.createCheckboxbutton('step5', 'step5name', cklistQSlide, ''),
						aapObj.preconfiguration.views.createInfo('', ''),
						aapObj.preconfiguration.views.createNavButton('step5', 'step6', number + 1)
					]);
				}
			},
			step6: {
				timeline: "Evaluation",
				callback: function (aapObj) {
					if ($('.inputpreconfig[data-actualstep="step6"]').val() != "") {
						var valmultiDecide = "tpls.forms.ocecoform.pourContre";
						if ($('.inputpreconfig[data-actualstep="step6"]').val() == "choiceone") {
							valmultiDecide = "tpls.forms.ocecoform.pourContre";
						} else if ($('.inputpreconfig[data-actualstep="step6"]').val() == "choicetwo") {
							valmultiDecide = "tpls.forms.ocecoform.decideFromBudget";
						} else if ($('.inputpreconfig[data-actualstep="step6"]').val() == "choicethree") {
							valmultiDecide = "tpls.forms.ocecoform.evaluation";
						} else if ($('.inputpreconfig[data-actualstep="step6"]').val() == "choicefour") {
							valmultiDecide = "tpls.forms.aap.selection";
						}
						aapObj.preconfiguration.actions.save(aapObj, "inputConfig.multiDecide", valmultiDecide);
					}
				},
				view: function (aapObj, number) {
					var radiolistQSlide = [
						{
							label: 'Vote simple pour ou contre',
							value: 'choiceone',
							inputCallback: function () { }
						},
						{
							label: `Votez chaque ligne du projet`,
							value: 'choicetwo',
							inputCallback: function () { }
						},
						{
							label: `Evaluation Ariane`,
							value: 'choicethree',
							inputCallback: function () { }
						},
						{
							label: `Evaluation à tableau 2D`,
							value: 'choicefour',
							inputCallback: function () { }
						}
					];

					return aapObj.preconfiguration.views.createSlide([
						aapObj.preconfiguration.views.createLegend(number, `Configurer l'evaluation`, `Methode d'evaluation`),
						aapObj.preconfiguration.views.createRadiobutton('step6', 'step6name', radiolistQSlide, ''),
						aapObj.preconfiguration.views.createInfo('', ''),
						aapObj.preconfiguration.views.createNavButton('step6', 'step7', number + 1)
					]);
				}
			},
			step7: {
				timeline: "Financement",
				callback: function (aapObj) {
					if ($('.inputpreconfig[data-actualstep="step5"]').val() != "") {
						$('.inputpreconfig[data-actualstep="step5"]:checked').each(function (item) {
							aapObj.preconfiguration.actions.save(aapObj, $(this).data("path"), $(this).val());
						});
					}
				},
				view: function (aapObj, number) {
					var cklistQSlide = [
						{
							label: `Activer l'enveloppe`,
							value: 'true',
							path: "envelope",
							inputCallback: function () { },
							checked: ''

						},
						{
							label: `Pouvoir dépaser le montant de depense`,
							value: 'true',
							path: "params.unlimitedFinancing",
							inputCallback: function () { },
							checked: ''
						},
					];

					return aapObj.preconfiguration.views.createSlide([
						aapObj.preconfiguration.views.createLegend(number, `Configurer le financement`, `Methode d'evaluation`),
						aapObj.preconfiguration.views.createCheckboxbutton('step7', 'step6name', cklistQSlide, ''),
						aapObj.preconfiguration.views.createInfo('', ''),
						aapObj.preconfiguration.views.createNavButton('step7', '', null)
					]);
				}
			}
		},
		views: {
			init: function () {
				return new Promise(function (resolve) {
					getAjax("", baseUrl + '/co2/aap/getviewbypath/path/co2.views.aap.preconfiguration', function (res) {
						$("body").append(res);
						resolve(true);
					});
				})
			},
			createNavButton: function (actualstep, nextstep, number, isObl = false, radioname = '') {
				var htmlNv = "";
				var isDisabled = "";
				var label = "";
				var evtclass = "preconfig-btn-next";
				if (isObl) {
					isDisabled = "disabled";
					label = `Suivant <span class="fa fa-long-arrow-right"></span>`;
				} else {
					label = `Passer cette etape <span class="fa fa-long-arrow-right"></span>`;
				}
				if (nextstep == '') {
					evtclass = "ap-starter-btn-finish";
					label = `Finir <span class="fa fa-check"></span>`;
				}
				return `<div>
							<button class="btn ap-starter-btn-close mr-4">Terminer</button>
							<button class="btn btn-aap-primary ${evtclass}" data-radioname="${radioname}" data-nextstep="${nextstep}" data-actualstep="${actualstep}" data-next="${number}" ${isDisabled}>${label}</button>
						</div>`;
			},
			createLegend: function (number, legend, label) {
				return `	<legend> ${number} - ${legend} </legend>
				        	<div class="">
				            	<label class="padding-20">${label}</label>
				        	</div>`;
			},
			createInfo: function (label, tooltips) {
				if (label != "") {
					return `  <div class="">
					          <label class="padding-20"> <i class="fa fa-exclamation-triangle"></i> ${label} </label>
					     </div>`;
				} else {
					return '';
				}
			},
			createRadiobutton: function (actualstep, name, lists, path) {
				var renderhtml = '';
				renderhtml += `<div class="wrapper">`;
				$.each(lists, function (id, list) {
					var pcTooltip = '';
					if (typeof list.tooltip != "undefined") {
						var pcTooltipimg = '';
						if (typeof list.tooltipimg != "undefined") {
							pcTooltipimg = `<img src="${baseUrl + list.tooltipimg}" />`;
						}
						pcTooltip = `<span class="fa fa-info-circle ml-3 preconfig-info-has-tooltip"> <span class="preconfig-info-tooltip"><p>${list.tooltip}</p>${pcTooltipimg}</span> </span>`;
					}
					renderhtml += `<div class="grid-row">
						              <label>
						                  <input class="inputpreconfig" type="radio" data-actualstep="${actualstep}" value="${list.value}" path="${path}" name="${name}"> <span> ${list.label} ${pcTooltip} </span>
						              </label>
						          </div>`;
					//list.inputCallback();
				});
				renderhtml += `</div>`;
				return renderhtml;
			},
			createCheckboxbutton: function (actualstep, name, lists, path) {
				var renderhtml = '';
				renderhtml += `<div class="wrapper">`;
				$.each(lists, function (id, list) {
					renderhtml += `<div class="grid-row">
						              <label>
						                  <input class="inputpreconfig" type="checkbox" data-actualstep="${actualstep}" value="${list.value}" path="${path}" name="${name}" ${list.checked}> <span> ${list.label} </span>
						              </label>
						          </div>`;
					//list.inputCallback();
				});
				renderhtml += `</div>`;
				return renderhtml;
			},
			createSlide: function (component) {
				return `<div class="swiper-slide" >
							${component.join(' ')}
					  </div>`;
			},
			createEntete: function () {
				return `<img class="logo-info" src="${typeof aapObj.context.profilThumbImageUrl != "undefined" ? baseUrl + aapObj.context.profilThumbImageUrl : baseUrl + defaultImage}">
                        <div class="title-info">${aapObj.context.name}</div>`;
			},
			updateTimeline: function (actualstep, branchestep) {
				var timeline = "";
				if (branchestep != "") {
					timeline = aapObj.preconfiguration.slide[actualstep].timeline[branchestep]
				} else {
					timeline = aapObj.preconfiguration.slide[actualstep].timeline
				}
				$('.timeline.aap-preconfigListStepSwipping li').removeClass('active');
				$(`
						<li class="active" data-actualstep="${actualstep}">
                            <div class="name-step">${timeline}</div>
                        </li>
				`).appendTo('.timeline.aap-preconfigListStepSwipping').hide().slideDown(500);
			},
		},
		events: {
			init: function () {
				$(".ap-starter-btn-start-preconfig").off().click(function () {
					$(".checkbox-preconfig-start").trigger('click');
					setTimeout(function () {
						$('.swiper-wrapper').append(aapObj.preconfiguration.slide.activationstep.view(aapObj));
						$('.aap-preconfigcontain-info-costum').append(aapObj.preconfiguration.views.createEntete);
						aapObj.preconfiguration.events.btnEvent();
						$('.co-popup-preconfig-content').hide();
						$('.co-popup-preconfig-question').show();

						swiper = new Swiper('.swiper#swiper', {
							loop: false,
							slidesPerView: 1,
							observer: true,
							observeParents: true,
							observeSlideChildren: true,
							watchSlidesVisibility: false,
							autoplayDisableOnInteraction: false,
							keyboardControl: false,
							mousewheelControl: false,
							allowSlidePrev: false,
							allowTouchMove: false,
						});
					}, "700");
				});
				aapObj.preconfiguration.events.btnEvent();
			},
			btnEvent: function () {
				$(`input.inputpreconfig[type="radio"] , input.inputpreconfig[type="checkbox"]`).on("change", function () {
					thisinput = $(this);
					$(`.preconfig-btn-next[data-actualstep="${thisinput.attr('data-actualstep')}"]`).prop('disabled', false);
					$(`.preconfig-btn-next[data-actualstep="${thisinput.attr('data-actualstep')}"]`).html('Suivant <span class="fa fa-long-arrow-right"></span>');
					//$(`.co-popup-preconfig-menu ul li[data-actualstep="${ thisinput.attr('data-actualstep') }"] p`).html(`${ thisinput.parent().text().trim() }`);
				});

				$(".preconfig-btn-next").off().click(function (e) {
					e.preventDefault();
					var actualstep = "activationstep";
					var nextpoint = 2;
					var nextstep = "datestep";
					var radioname = "radio";
					if ($(this).data('actualstep')) {
						actualstep = $(this).data('actualstep');
					}
					if ($(this).data('next')) {
						nextpoint = $(this).data('next');
					}
					if ($(this).data('nextstep')) {
						nextstep = $(this).data('nextstep');
					}
					if ($(this).data('radioname') && $(this).data('radioname') != '') {
						radioname = $(this).data('radioname');
					}

					if (typeof aapObj.preconfiguration.slide[actualstep].callback == "function") {
						aapObj.preconfiguration.slide[actualstep].callback(aapObj);
					}

					const swiperEl = document.querySelector('.swiper');
					const thisswiper = $(this);
					var branchestep = '';

					if ($('input[name="' + radioname + '"][step="' + actualstep + '"]:checked').val() != "") {
						if (typeof aapObj.preconfiguration.slide[nextstep].view[$('input[name="' + radioname + '"][data-actualstep="' + actualstep + '"]:checked').val()] != "undefined") {
							swiperEl.swiper.appendSlide(aapObj.preconfiguration.slide[nextstep].view[$('input[name="' + radioname + '"][data-actualstep="' + actualstep + '"]:checked').val()](aapObj, nextpoint));
							branchestep = $('input[name="' + radioname + '"][data-actualstep="' + actualstep + '"]:checked').val();
						} else if (typeof aapObj.preconfiguration.slide[nextstep].view != "undefined") {
							swiperEl.swiper.appendSlide(aapObj.preconfiguration.slide[nextstep].view(aapObj, nextpoint));
						}
						aapObj.preconfiguration.views.updateTimeline(nextstep, branchestep);
						swiperEl.swiper.update();
						swiperEl.swiper.slideNext();
						aapObj.preconfiguration.events.btnEvent();
						swiperEl.swiper.removeSlide(0);
						swiperEl.swiper.update();
					}

					if ($('#quartierselect2').length > 0) {
						$.getScript(baseUrl + "/plugins/bootstrap-tagsinput/src/bootstrap-tagsinput.js", function (data, textStatus, jqxhr) {
							$('#quartierselect2').tagsinput('items');
						});
					}

					if ($('#preconfig-startdate').length > 0) {
						/*$.getScript( "https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.15.1/moment.min.js", function( data, textStatus, jqxhr ) {
								$.getScript( baseUrl+"/plugins/bootstrap-datetimepicker/xdsoft/jquery.datetimepicker.full.min.js", function( data, textStatus, jqxhr ) {
								$('#preconfig-startdate').datetimepicker({
									format: 'Y-m-d H:i'
								});
								$('#preconfig-enddate').datetimepicker({
									format: 'Y-m-d H:i'
								});
							});
						});*/
					}

				});

				$(".ap-starter-btn-close").click(function () {
					aapObj.preconfiguration.actions.close();
					$(location).prop('href', baseUrl + '/costum/co/index/slug/' + costum.slug + '#proposalAap.context.' + aapObj.context.slug + '.formid.' + aapObj.form._id.$id);
				});

				$(".ap-starter-btn-finish").click(function () {
					$('.co-popup-preconfig-question').hide();
					$('.co-popup-preconfig-finish').show();
				});

				$(".ap-starter-btn-community").click(function () {
					aapObj.preconfiguration.actions.close();
					$(location).prop('href', baseUrl + '/costum/co/index/slug/' + costum.slug + '#configurationAap.context.' + aapObj.context.slug + '.formid.' + aapObj.form._id.$id + '.aapview.community');
				})

			},
		},
		actions: {
			open: function (aapObj) {
				mylog.log("aapObj.preconfiguration.initialized", aapObj.canEdit, aapObj.preconfiguration.initialized);
				if (aapObj.canEdit && !aapObj.preconfiguration.initialized) {
					aapObj.preconfiguration.views.init().then(function () {
						$('.co-popup-preconfig-header-logo').append(aapObj.preconfiguration.views.createEntete);
						aapObj.preconfiguration.events.init();
					})
				}
			},
			validStep: function (aapObj, toUpdate, callback = function () { }) {
				$.each(ttoUpdate, function (index, value) {
					aapObj.preconfiguration.actions.save(aapObj, value.path, value);
				});
			},
			close: function () {

				if ($(".co-popup-preconfig-container").length > 0)
					$(".co-popup-preconfig-container").remove();
			},
			finished: function () {
				if ($(".co-popup-preconfig-container").length > 0)
					$(".co-popup-preconfig-container").remove();
			},
			save: function (aapObj, path, value, setType = "") {
				var tplCtx = {
					id: aapObj.form._id.$id,
					path: path,
					value: value,
					collection: "forms"
				};
				if (setType != "") {
					tplCtx.setType = setType;
				}
				dataHelper.path2Value(tplCtx, function (params) {
					toastr.success('Configuration mis à jour');
				})
			}
		}
	},
	common: {
		initWebSocket: function (coWsConfig, actionToDo = "test_action") {
			var socketConfigurationEnabled = coWsConfig.enable && coWsConfig.serverUrl && coWsConfig.pingRefreshViewUrl;
			if (!notNull(wsCO)) {
				let wsCO = null

				if (socketConfigurationEnabled) {
					wsCO = io.connect(coWsConfig.serverUrl, {
						query: {
							action: actionToDo,
						}
					});
					wsCO.on('connect', function (socket) {
						aapObj.userSocketId = wsCO.id
					});
					wsCO.on('refresh-view', function (data) {
						if (aapObj.userSocketId && data.userSocketId && aapObj.userSocketId != data.userSocketId) {
							if (data.action && aapObj.common.makeRefresh[data.action]) {
								aapObj.common.makeRefresh[data.action](aapObj, data.answerId, data.responses)
							}
						}
					});
				}
			} else if (wsCO.connected == false) {
				if (socketConfigurationEnabled) {
					wsCO = io.connect(coWsConfig.serverUrl, {
						query: {
							action: actionToDo,
						}
					});
					wsCO.on('connect', function (socket) {
						aapObj.userSocketId = wsCO.id
					});
					wsCO.on('refresh-view', function (data) {
						if (aapObj.userSocketId && data.userSocketId && aapObj.userSocketId != data.userSocketId) {
							if (data.action && aapObj.common.makeRefresh[data.action]) {
								aapObj.common.makeRefresh[data.action](aapObj, data.answerId, data.responses)
							}
						}
					});
				}
			} else {
				aapObj.userSocketId = wsCO.id
				wsCO.on('refresh-view', function (data) {
					if (aapObj.userSocketId && data.userSocketId && aapObj.userSocketId != data.userSocketId) {
						if (data.action && aapObj.common.makeRefresh[data.action]) {
							aapObj.common.makeRefresh[data.action](aapObj, data.answerId, data.responses)
						}
					}
				});
			}

		},
		makeRefresh: {
			reloadThisInput: function (aapObj, answerId, responses) {
				const inputUpdated = responses?.editedInput ? responses?.editedInput : "default";
				if (typeof reloadInput != "undefined" && responses.editedInput != null && $(`#question_${responses?.editedInput}`).is(":visible")) {
					if ($(`div#question_${inputUpdated}`).attr("data-step"))
						reloadInput(responses.editedInput, $(`div#question_${inputUpdated}`).attr("data-step"))
				}
			},
			statusUpdated: function (aapObj, answerId, responses) {
				$(`#status-${answerId}.statusView`).empty();
				$(`#status-${answerId}.statusView`).append(aapObj.directory.buildStatus_template_html(aapObj, responses && responses?.elt?.status ? responses.elt.status : undefined, answerId, responses && responses?.elt?.statusInfo ? responses.elt.statusInfo : {}, responses && responses?.usersStatus ? responses.usersStatus : {})).animate(300);
				aapObj.events.proposalStatus(aapObj);
			}, removeUnusedUI: function (aapObj) {
				$('.proposal-compact .proposition-collapse-container .collapse').collapse("hide");
				$(".proposal-compact .proposition-detail").remove();
				$('.proposition-name').off();
				$('#openModal .propsDetailPanelContainer .propsDetailPanel').removeClass("col-sm-10");
			}, proposalEditedByForm: function (aapObj, answerId, responses) {
				let viewType = "proposal";
				let activeView = "detailed";
				const makeChangeAnyway = location.href.indexOf("#detailProposalAap") > -1 && $(`#propItem${answerId}.props-item`).hasClass("active") ? true : false
				let aapViewCopy = '' + aapObj.aapview;
				aapViewCopy = makeChangeAnyway ? "summary" : activeView;
				switch (aapViewCopy) {
					case 'minimalist':
						activeView = "compact"
						break;
					case 'table':
						activeView = "table"
						break;
					case 'summary':
						activeView = "compact";
						break;
					default:
						activeView = "detailed"
						break;
				}
				if (aapObj.filterSearch?.proposalDetail?.filters?.dom != undefined) {
					$(aapObj.filterSearch?.proposalDetail?.filters?.dom).length > 0 ? viewType = "proposalDetail" : "";
				}
				let filterSearchInstance = aapObj.filterSearch?.[viewType] ? { ...aapObj.filterSearch[viewType] } : {};
				const filterDomKey = filterSearchInstance?.filters?.dom ? filterSearchInstance?.filters?.dom.replace(/\#|\./g, '') : "noExist";
				const isSameContext = aapObj?.context?.id && responses?.context?.[aapObj?.context?.id] ? true : false;
				const isProposalView = location.href.indexOf("#proposal") > -1 || location.href.indexOf("#detailProposal") > -1 ? true : false;
				if (responses?.created && isSameContext) {
					if (filterSearchInstance?.lastSearchDone?.[filterDomKey] && filterSearchInstance?.lastSearchDone?.[filterDomKey] <= responses?.created * 1000) {
						if (aapObj.dataFromSocket[answerId]) {
							aapObj.dataFromSocket[answerId].title = responses?.answers?.aapStep1?.titre ? responses?.answers?.aapStep1?.titre : null;
							if (!aapObj.dataFromSocket[answerId]?.alreadyNotified && aapObj.dataFromSocket[answerId]?.title != null && aapObj.dataFromSocket[answerId]?.title != "") {
								let thisNotif = null;
								if (typeof aapObj?.form?.params.onlyAdminCanSeeList != "undefined" && aapObj.form?.params.onlyAdminCanSeeList == true) {
									if (aapObj.canEdit && isProposalView) {
										thisNotif = myPNotify.pingNotification(trad["New proposal"], `<span class="mb-2">
                                                <b>${responses.nameUser}</b> ${trad["has added a new proposal"]} "
                                                <a href="javascript:;" class="notif-btn-aap-link proposition-detail" data-target="${answerId}" data-title="${aapObj.dataFromSocket[answerId].title}" data-project-id="">${aapObj.dataFromSocket[answerId].title}</a> " <br>
                                            </span><br>
                                            <button class="btn notif-btn-aap-primary refresh-filter pull-right p-2" style="font-size: 1.2rem" data-type="${viewType}">${trad["Refresh list"]}</button>
                                            `)
										aapObj.dataFromSocket[answerId].alreadyNotified = true;
									} else if (responses.user == userConnected?.["_id"]?.["$id"] && aapObj?.directory?.proposalNotSeenCounter) {
										// aapObj.directory.proposalNotSeenCounter(aapObj);
										aapObj.dataFromSocket?.[answerId]?.alreadyNotified ? aapObj.dataFromSocket[answerId].alreadyNotified = true : "";
										// if(aapObj.filterSearch?.[viewType+"Initialized"] && aapObj.filterSearch?.[viewType]?.search?.obj) {
										//     aapObj.filterSearch[viewType].search.obj =
										// JSON.parse(JSON.stringify(aapObj.filterSearch[viewType+"Initialized"]));
										// aapObj.filterSearch[viewType]?.filters?.manage?.resetFilter(aapObj.filterSearch[viewType],
										// ".btn-filters-select") }
									}
								} else if (notNull(userConnected) && isProposalView) {
									thisNotif = myPNotify.pingNotification(trad["New proposal"], `<span class="mb-2">
                                            <b>${responses.nameUser}</b> ${trad["has added a new proposal"]} "
                                            <a href="javascript:;" class="notif-btn-aap-link proposition-detail" data-target="${answerId}" data-title="${aapObj.dataFromSocket[answerId].title}" data-project-id="">${aapObj.dataFromSocket[answerId].title}</a> " <br>
                                        </span><br>
                                        <button class="btn notif-btn-aap-primary refresh-filter pull-right p-2" style="font-size: 1.2rem" data-type="${viewType}">${trad["Refresh list"]}</button>
                                        `);
									aapObj.dataFromSocket[answerId].alreadyNotified = true;
								}
								$(".refresh-filter").on("click", function () {
									if (aapObj.filterSearch?.[viewType + "Initialized"] && aapObj.filterSearch?.[viewType]?.search?.obj) {
										aapObj.filterSearch[viewType].search.obj = JSON.parse(JSON.stringify(aapObj.filterSearch[viewType + "Initialized"]));
										aapObj.filterSearch[viewType]?.filters?.manage?.resetFilter(aapObj.filterSearch[viewType], ".btn-filters-select")
									}
									thisNotif.close({ destroy: true });
								});
								$(".pnotify .proposition-detail").on("click", function () {
									const self = $(this);
									const target = self.data('target');
									const title = self.data('title');
									const project_id = self.data('project-id');
									const project = self.data('project');
									var urlQuery = {
										context: aapObj.context.slug, formid: aapObj.form._id.$id, answerId: target, aapview: 'summary'
									};
									if (notEmpty(project_id)) {
										urlQuery.projectId = project_id;
									}
									// aapObj.filterSearch.activeFilters = aapObj.common.getActivateFilters(aapObj, "#filterContainerL",
									// "#activeFilters .filters-activate")
									history.pushState(null, title, aapObj.common.buildUrlQuery(notEmpty(project) ? '#detailProjectAap' : '#detailProposalAap', urlQuery, aapObj.common.getQueryStringObject()));
									thisNotif.close({ destroy: true });
									sessionStorage.setItem("aapview", "kanban")
									urlCtrl.loadByHash(location.hash);
								})
								if (notNull(thisNotif)) {
									thisNotif.on("pnotify:beforeClose", function () {
										aapObj.directory.proposalNotSeenCounter(aapObj);
										aapObj.dataFromSocket[answerId].alreadyNotified = true;
									});
								}
							}
						} else {
							aapObj.dataFromSocket[answerId] = {
								alreadyNotified: false,
								trackCreated: new Date().getTime(),
								lastUpdate: new Date().getTime(),
								title: responses?.answers?.aapStep1?.titre ? responses?.aapStep1?.titre : null
							}
						}
						aapObj.dataFromSocket[answerId].lastUpdate = new Date().getTime()
					} else {
						if (typeof aapObj.dataFromSocket[answerId] != "undefined") {
							aapObj.dataFromSocket[answerId].lastUpdate = new Date().getTime()
						}
						const inputUpdated = responses?.editedInput ? responses?.editedInput : "default";
						if (responses?.answers?.aapStep1?.titre && inputUpdated == "titre") {
							$(`#propItem${answerId}.props-item`).html(ucfirst(responses?.answers?.aapStep1?.titre)).attr("data-title", ucfirst(responses?.answers?.aapStep1?.titre))
						}
						if (makeChangeAnyway || $(`#proposal-container-${activeView}-${answerId}`).is(":visible")) {
							let isVisibleBefore = $(`#proposal-container-${activeView}-${answerId} .proposition-collapse-item[data-id="${answerId}"]`).is(":visible") ? true : false;
							switch (inputUpdated) {
								case "image":
									aapObj.directory.reloadAapProposalItem(aapObj, answerId);
									if (aapObj.aapview == "summary") {
										aapObj.common.makeRefresh.removeUnusedUI(aapObj)
									}
									if (isVisibleBefore) {
										$(`#proposal-container-${activeView}-${answerId} .proposition-collapse-container a.collapse-toggle[data-toggle="collapse"]`).trigger("click")
									}
									break;
								case "titre":
									if (responses?.answers?.aapStep1?.titre) {
										$(`#proposal-container-${activeView}-${answerId} .proposition-name .title-text`).html(ucfirst(responses?.answers?.aapStep1?.titre));
										if ($(`#propItem${answerId}.props-item`).hasClass("active")) {
											$(".aap-page-info-left .title .main").html(ucfirst(responses?.answers?.aapStep1?.titre));
											aapObj?.context && aapObj?.form?.["_id"]?.["$id"] && answerId ? $(`.breadcrumb.aap-breadcrumb li a.lbh.detail-breadcrumb`).html(ucfirst(responses?.answers?.aapStep1?.titre)) : ""
										}
									}
									break;
								case "principalDomaine":
									$(`#proposal-container-${activeView}-${answerId} .proposal-badge`).empty().html(aapObj.directory.proposalThematique(aapObj, responses?.answers?.aapStep1));
									if ($(`#propItem${answerId}.props-item`).hasClass("active")) {
										$(`.card-body .tag-body`).empty();
										$(`.card-body .tag-body`).html(aapObj.directory.proposalThematique(aapObj, responses?.answers?.aapStep1));
									}
									break;
								case "description":
									$(`#proposal-container-${activeView}-${answerId} .proposition-description`).empty().html(`
                                        ${typeof responses?.answers?.aapStep1?.description != "undefined" && notEmpty(responses?.answers?.aapStep1?.description) ? `
                                            <b>${tradDynForm.description}</b><br>
                                            ${aapObj.directory.proposalDescription(aapObj, responses?.answers?.aapStep1)}
                                        ` : ''}
                                    `);
									if (aapObj.aapview == "summary") {
										typeof responses?.answers?.aapStep1?.description != "undefined" && notEmpty(responses?.answers?.aapStep1?.description) ? $(".card.card-show-divider.detail-description .card-body .description-body").html('<div class="p-3">' + aapObj.directory.proposalDescription(aapObj, responses?.answers?.aapStep1) + '</div>').removeClass("detail-content").addClass("detail-content") : $(".card.card-show-divider.detail-description .card-body .description-body").html(`<p class="list-group-item">${trad.nodescription}</p>`).removeClass("detail-content")
									}
									break;
								default:
									// aapObj.common.makeRefresh.reloadThisInput(aapObj, answerId, responses)
									if (inputUpdated == "budgetTable" || inputUpdated == "depense" || inputUpdated == "financer" || inputUpdated == "annualGlobalBudget") {
										if ($(`#proposal-container-${activeView}-${answerId} .proposition-collapse-item[data-id="${answerId}"]`).is(":visible")) {
											const html = aapObj.directory.propositionCollapseHtml(aapObj, answerId);
											$(`.proposition-collapse-item[data-id="${answerId}"]`).empty().html(html);
											aapObj.events.proposalCollapseItems(aapObj);
										}
									} else {
										if (inputUpdated == "decide" || inputUpdated == "typologie" || inputUpdated == "invite") {
											aapObj.directory.reloadAapProposalItem(aapObj, answerId);
											if (aapObj.aapview == "summary") {
												aapObj.common.makeRefresh.removeUnusedUI(aapObj)
											}
											if (isVisibleBefore) {
												$(`#proposal-container-${activeView}-${answerId} .proposition-collapse-container a.collapse-toggle[data-toggle="collapse"]`).trigger("click")
											}
										} else if ($(`#proposal-container-${activeView}-${answerId} .proposition-collapse-item[data-id="${answerId}"]`).is(":visible")) {
											const html = aapObj.directory.propositionCollapseHtml(aapObj, answerId);
											$(`.proposition-collapse-item[data-id="${answerId}"]`).empty().html(html);
											aapObj.events.proposalCollapseItems(aapObj);
										}
									}

									break;
							}
						}
						if ($(`div#question_${inputUpdated}`).is(":visible")) {
							if (typeof reloadInput != "undefined" && $(`div#question_${inputUpdated}`).attr("data-step")) {
								reloadInput(inputUpdated, $(`div#question_${inputUpdated}`).attr("data-step"))
							}
						}
						if ($(`div#listPropsPanel a#propItem${answerId}`).is(":visible")) {
							aapObj.directory.reloadAapMiniItem(aapObj, answerId)
						}
					}
				}
				if (!isProposalView && isSameContext && aapObj?.directory?.proposalNotSeenCounter) {
					aapObj.directory.proposalNotSeenCounter(aapObj);
					aapObj?.dataFromSocket?.[answerId] && notEmpty(aapObj?.dataFromSocket?.[answerId]?.title) ? aapObj.dataFromSocket[answerId].alreadyNotified = true : "";
				}
			}, proposalDeleted: function (aapObj, answerId, responses) {
				let activeView = "detailed";
				const isSameContext = aapObj?.context?.id && responses?.context?.[aapObj?.context?.id] ? true : false;
				const isProposalView = location.href.indexOf("#proposal") > -1 ? true : false;
				switch (aapObj.aapview) {
					case 'minimalist':
						activeView = "compact"
						break;
					case 'table':
						activeView = "table"
						break;
					case 'summary':
						activeView = "compact"
						break;
					default:
						activeView = "detailed"
						break;
				}
				if ($(`#proposal-container-${activeView}-${answerId}`).is(":visible") && isSameContext && isProposalView) {
					$(`#proposal-container-${activeView}-${answerId}`).remove();
					aapObj?.directory?.proposalNotSeenCounter ? aapObj.directory.proposalNotSeenCounter(aapObj) : ""
				}
			}, commentUpdated: function (aapObj, answerId, responses) {
				let activeView = "detailed";
				switch (aapObj.aapview) {
					case 'minimalist':
						activeView = "compact"
						break;
					case 'table':
						activeView = "table"
						break;
					case 'summary':
						activeView = "compact"
						break;
					default:
						activeView = "detailed"
						break;
				}
				if ($(`#proposal-container-${activeView}-${answerId} .proposition-collapse-item[data-id="${answerId}"]`).is(":visible")) {
					const html = aapObj.directory.propositionCollapseHtml(aapObj, answerId);
					$(`.proposition-collapse-item[data-id="${answerId}"]`).empty().html(html);
					aapObj.events.proposalCollapseItems(aapObj);
				}
			}, contributorUpdated: function (aapObj, answerId, responses) {
				let activeView = "detailed";
				switch (aapObj.aapview) {
					case 'minimalist':
						activeView = "compact"
						break;
					case 'table':
						activeView = "table"
						break;
					case 'summary':
						activeView = "compact"
						break;
					default:
						activeView = "detailed"
						break;
				}
				let isVisibleBefore = $(`#proposal-container-${activeView}-${answerId} .proposition-collapse-item[data-id="${answerId}"]`).is(":visible") ? true : false;
				if ($(`#proposal-container-${activeView}-${answerId}`).is(":visible")) {
					aapObj.directory.reloadAapProposalItem(aapObj, answerId);
					if (aapObj.aapview == "summary") {
						aapObj.common.makeRefresh.removeUnusedUI(aapObj)
					}
					if (isVisibleBefore) {
						$(`#proposal-container-${activeView}-${answerId} .proposition-collapse-container a.collapse-toggle[data-toggle="collapse"]`).trigger("click")
					}
				}
			}, newProjectGenerated: function (aapObj, answerId, responses) {
				let viewType = "project";
				let activeView = "detailed";
				let aapViewCopy = '' + aapObj.aapview;
				const makeChangeAnyway = location.href.indexOf("#detailProjectAap") > -1 && $(`#propItem${answerId}.props-item`).hasClass("active") ? true : false
				aapViewCopy = makeChangeAnyway ? "summary" : activeView;
				switch (aapViewCopy) {
					case 'minimalist':
						activeView = "compact"
						break;
					case 'table':
						activeView = "table"
						break;
					case 'summary':
						activeView = "compact";
						break;
					default:
						activeView = "detailed"
						break;
				}
				if (aapObj.filterSearch?.projectDetail?.filters?.dom != undefined) {
					$(aapObj.filterSearch?.projectDetail?.filters?.dom).length > 0 ? viewType = "projectDetail" : "";
				}
				let filterSearchInstance = aapObj.filterSearch?.[viewType] ? { ...aapObj.filterSearch[viewType] } : {};
				const filterDomKey = filterSearchInstance?.filters?.dom ? filterSearchInstance?.filters?.dom.replace(/\#|\./g, '') : "noExist";
				const isSameContext = aapObj?.context?.id && responses?.context?.[aapObj?.context?.id] ? true : false;
				const isProposalView = location.href.indexOf("#projectsAap") > -1 || location.href.indexOf("#detailProjectAap") > -1 ? true : false;
				mylog.log("edited call", viewType, filterDomKey, responses, isSameContext, isProposalView)
				if (filterSearchInstance?.lastSearchDone?.[filterDomKey] && responses?.created && isSameContext) {
					if (filterSearchInstance?.lastSearchDone?.[filterDomKey] <= responses?.created * 1000) {
						// if(aapObj.dataFromSocket['project_'+answerId]) {
						aapObj.dataFromSocket['project_' + answerId] = {
							alreadyNotified: false,
							trackCreated: new Date().getTime(),
							lastUpdate: new Date().getTime(),
							title: responses?.answers?.aapStep1?.titre ? responses?.aapStep1?.titre : null
						}
						aapObj.dataFromSocket['project_' + answerId].title = responses?.answers?.aapStep1?.titre ? responses?.answers?.aapStep1?.titre : null;
						if (!aapObj.dataFromSocket['project_' + answerId]?.alreadyNotified && aapObj.dataFromSocket['project_' + answerId]?.title != null && aapObj.dataFromSocket['project_' + answerId]?.title != "") {
							let thisNotif = null;
							if (typeof aapObj?.form?.params.onlyAdminCanSeeList != "undefined" && aapObj.form?.params.onlyAdminCanSeeList == true) {
								if (aapObj.canEdit && isProposalView) {
									thisNotif = myPNotify.pingNotification(trad["New project"], `<span class="mb-2">
                                                <b>${responses.nameUser}</b> ${trad['has generated new project']} "
                                                <a href="javascript:;" class="notif-btn-aap-link project-access" data-target="${responses?.project.id}" data-answer="${answerId}" data-title="${aapObj.dataFromSocket['project_' + answerId].title}">${aapObj.dataFromSocket['project_' + answerId].title}</a> " <br>
                                            </span><br>
                                            <button class="btn notif-btn-aap-primary refresh-filter pull-right p-2" style="font-size: 1.2rem" data-type="${viewType}">${trad["Refresh list"]}</button>
                                            `)
									aapObj.dataFromSocket['project_' + answerId].alreadyNotified = true;
								} else if (responses.user == userConnected?.["_id"]?.["$id"] && aapObj?.directory?.proposalNotSeenCounter) {
									aapObj.dataFromSocket?.['project_' + answerId]?.alreadyNotified ? aapObj.dataFromSocket['project_' + answerId].alreadyNotified = true : "";
								}
							} else if (notNull(userConnected) && isProposalView) {
								thisNotif = myPNotify.pingNotification(trad["New project"], `<span class="mb-2">
                                            <b>${responses.nameUser}</b> ${trad['has generated new project']} "
                                            <a href="javascript:;" class="notif-btn-aap-link project-access" data-target="${responses?.project.id}" data-answer="${answerId}" data-title="${aapObj.dataFromSocket['project_' + answerId].title}">${aapObj.dataFromSocket['project_' + answerId].title}</a> " <br>
                                        </span><br>
                                        <button class="btn notif-btn-aap-primary refresh-filter pull-right p-2" style="font-size: 1.2rem" data-type="${viewType}">${trad["Refresh list"]}</button>
                                        `)
								aapObj.dataFromSocket['project_' + answerId].alreadyNotified = true;
							}
							$(".refresh-filter").on("click", function () {
								if (aapObj.filterSearch?.[viewType + "Initialized"] && aapObj.filterSearch?.[viewType]?.search?.obj) {
									aapObj.filterSearch[viewType].search.obj = JSON.parse(JSON.stringify(aapObj.filterSearch[viewType + "Initialized"]));
									aapObj.filterSearch[viewType]?.filters?.manage?.resetFilter(aapObj.filterSearch[viewType], ".btn-filters-select")
								}
								thisNotif.close({ destroy: true });
							});
							$('.pnotify .project-access').off('click').on('click', function () {
								const dataset = this.dataset;
								const query = aapObj.common.getQuery(aapObj);
								const title = dataset.title;
								const answer = dataset.answer;
								const new_hash = `detailProjectAap.context.${query.context}.formid.${query.formid}${answer ? `.answerId.${answer}` : ''}.projectId.${dataset.target}${document.location.href.indexOf('?') > -1 ? '?' + document.location.href.split('?')[1] : ''}`;
								history.pushState({}, null, `${document.location.origin}${document.location.pathname}#${new_hash}`);
								// document.title = title;
								thisNotif.close({ destroy: true });
								urlCtrl.loadByHash(location.hash);
							});
							if (notNull(thisNotif)) {
								thisNotif.on("pnotify:beforeClose", function () {
									// aapObj.directory.proposalNotSeenCounter(aapObj);
									aapObj.dataFromSocket['project_' + answerId].alreadyNotified = true;
								})
							}
						}
						// } else {

						// }
						aapObj.dataFromSocket['project_' + answerId].lastUpdate = new Date().getTime()
					}
				}
			}, reloadProposal: function (aapObj, answerId, responses) {
				let viewType = "proposal";
				let activeView = "detailed";
				if (typeof responses.context == "undefined" && responses?.elt?.context) {
					responses.context = responses.elt.context
				}
				const makeChangeAnyway = location.href.indexOf("#detailProposalAap") > -1 && $(`#propItem${answerId}.props-item`).hasClass("active") ? true : false
				let aapViewCopy = '' + aapObj.aapview;
				aapViewCopy = makeChangeAnyway ? "summary" : activeView;
				switch (aapViewCopy) {
					case 'minimalist':
						activeView = "compact"
						break;
					case 'table':
						activeView = "table"
						break;
					case 'summary':
						activeView = "compact";
						break;
					default:
						activeView = "detailed"
						break;
				}
				if (aapObj.filterSearch?.proposalDetail?.filters?.dom != undefined) {
					$(aapObj.filterSearch?.proposalDetail?.filters?.dom).length > 0 ? viewType = "proposalDetail" : "";
				}
				const isSameContext = aapObj?.context?.id && responses?.context?.[aapObj?.context?.id] ? true : false;
				const isProposalView = location.href.indexOf("#proposal") > -1 || location.href.indexOf("#detailProposal") > -1 ? true : false;
				mylog.log("edited call", activeView, isSameContext, isProposalView, responses)
				if (isProposalView && isSameContext && $(`#proposal-container-${activeView}-${answerId}`).is(":visible")) {
					let isVisibleBefore = $(`#proposal-container-${activeView}-${answerId} .proposition-collapse-item[data-id="${answerId}"]`).is(":visible") ? true : false;
					aapObj.directory.reloadAapProposalItem(aapObj, answerId);
					if (isVisibleBefore) {
						$(`#proposal-container-${activeView}-${answerId} .proposition-collapse-container a.collapse-toggle[data-toggle="collapse"]`).trigger("click")
					}
				}
			}
		},
		alreadyContributors: function (userId, answerId, callBack = () => { }, extraParams = {}) {
			var objToSend = {
				userId: userId, answerId: answerId
			};
			if (extraParams && typeof extraParams.projectId != "undefined" && notEmpty(extraParams.projectId) && !notEmpty(answerId)) {
				objToSend.projectId = extraParams.projectId;
			}
			if (!notEmpty(answerId)) {
				delete objToSend.answerId;
			}
			functionCurrentlyLoading["alreadyContributors"] = true;
			ajaxPost(
				"",
				baseUrl + "/co2/aap/commonaap/action/already_contributors",
				objToSend,
				function (data) {
					functionCurrentlyLoading["alreadyContributors"] = false;
					callBack(data);
				},
				function (error) {
					functionCurrentlyLoading["alreadyContributors"] = false;
					mylog.log("ajaxPost error", error)
				},
				"json"
			)
		},
		joinThisProposal: function (answerId, projectId, userId, userInfo) {
			var params = {
				parentId: answerId,
				parentType: "answers",
				listInvite: {
					citoyens: {},
					organizations: {}
				}
			}
			if (userId && userInfo) {
				params.listInvite.citoyens[userId] = userInfo.name;
				if (projectId && projectId != null) {
					ajaxPost("", baseUrl + '/' + moduleId + "/link/multiconnect", {
						parentId: projectId,
						parentType: "projects",
						listInvite: { ...params.listInvite }
					}, function (data) {
						if (!notEmpty(answerId)) {
							toastr.success(trad.saved);
						}
					})
				}
				if (notEmpty(answerId)) {
					ajaxPost("", baseUrl + '/' + moduleId + "/link/multiconnect", params, function (data) {
						toastr.success(trad.saved);
						ajaxPost("", baseUrl + '/survey/answer/rcnotification/action/addperson/answerid/' + params.parentId,
							{
								pers: params?.listInvite?.citoyens,
								url: window.location.href
							},
							function (data) {

							}, "html");
					})
				}
			}
		},
		/**
		 * Constructeur adaptatif d'url à partir d'éxistant
		 * On peut tout à fait en construire un nouveau
		 * @param {string} hash Peut être un lien direct (http://domaine.com) ou un hash (#tag)
		 * @param {Object} substitutes Elements qui vont être changé dans le lien sous la forme (http://domaine.com/page#clé.valeur => {clé: valeur})
		 * @param {Object} get Les requêtes get de la forme (clé1=valeur&clé2=valeur2 => {clé1: valeur1, clé2: valeur2})
		 * @return {string}
		 */
		buildUrlQuery: function (hash, substitutes, get) {
			if (typeof substitutes === "undefined" || substitutes === null) {
				substitutes = {};
			}
			if (typeof get === "undefined" || get === null) {
				get = {};
			}

			var output = hash;
			var firstPart = hash.split('?')[0];
			for (var key in substitutes) {
				var pattern = "\\.(" + key + ")\\.[\\w\\d]*(\\.)?";
				var regex = new RegExp(pattern);
				if (firstPart.match(regex)) {
					firstPart = firstPart.replace(regex, function (match, key, dot) {
						return ((typeof substitutes[key] === "undefined" || substitutes[key] !== "" ? "." + key + "." + substitutes[key] : "") + (dot ? dot : ""));
					});
				} else if (substitutes[key] != "") {
					firstPart += "." + key + "." + substitutes[key];
				}
			}
			output = firstPart;
			if (Object.keys(get).length > 0) {
				var queryString;
				if (hash.indexOf("?") > 0) {
					queryString = hash.substring(hash.indexOf("?") + 1);
					for (var getParam in get) {
						var regexString;
						if (['number', 'string'].includes(typeof get[getParam])) {
							regexString = '(&|^)(' + getParam + ')=[\\w\\d_\\+\\.%]+';
						} else if (Array.isArray(get[getParam])) {
							regexString = '(&|^)(' + getParam + ')\\[\\]=[\\w\\d_\\+\\.%]+';
						}
						var regex = new RegExp(regexString, "g");
						queryString = queryString.replace(regex, function (match, begining, key) {
							if (Array.isArray(get[key])) {
								return '';
							} else {
								return get[key] !== '' ? ((begining ? begining : '') + key + '=' + get[key]) : '';
							}
						});
					}
					for (var getParam in get) {
						if (Array.isArray(get[getParam])) {
							get[getParam].forEach(function (getQuery) {
								queryString += '&' + getParam + '[]=' + getQuery;
							});
						} else if (['number', 'string'].includes(typeof get[getParam]) && !queryString.match(new RegExp('(&|^)(' + getParam + ')=[\\w\\d_\\+\\.%]+', 'g'))) {
							queryString += '&' + getParam + '=' + get[getParam];
						}
					}
					output = firstPart + '?' + queryString;
				} else {
					queryString = '';
					for (var getParam in get) {
						if (Array.isArray(get[getParam])) {
							get[getParam].forEach(function (getQuery) {
								queryString += '&' + getParam + '[]=' + getQuery;
							});
						} else if (['number', 'string'].includes(typeof get[getParam]) && !queryString.match(new RegExp('(&|^)(' + getParam + ')=[\\w\\d_\\+\\.%]+', 'g'))) {
							queryString += '&' + getParam + '=' + get[getParam];
						}
					}
					output = firstPart + '?' + queryString.substring(1);
				}
			}
			return output;
		},
		getQueryStringObject: function () {
			var url = document.location.href;
			var output = {};
			url = url.split('?');
			if (url.length > 1) {
				url = url.slice(1).join('');
				url = url.split('&');
				url.forEach(function (query) {
					var split = query.split('=');
					output[split[0]] = split.slice(1).join('');
				});
			}
			return output;
		},
		canShowMap(aapObj) {
			var mapContainerDom = $('#mapOfResultsAnswL');
			var showMapStorage = localStorage.getItem('showMyMap');
			var canShowMap = notEmpty(showMapStorage) ? JSON.parse(showMapStorage) : false;
			return mapContainerDom.length && canShowMap && aapObj.showMap;
		},
		checkRules: function (aapObj, rules, user) {
			var currentPage = [];
			if (aapObj && aapObj.pageDetail)
				currentPage = Object.keys(aapObj.pageDetail);
			var myRoles = aapObj.myRoles;
			if (rules?.and) {
				var valid = true;
				if (rules?.and?.admin && !aapObj.canEdit) {
					valid = false;
				}
				if (rules?.and?.pages && !currentPage.some(p => rules?.and.pages.includes(p))) {
					valid = false;
				}
				if (rules?.and?.roles && !myRoles.some(r => rules?.and.roles.includes(r))) {
					valid = false;
				}
				//check the rest rules
				delete rules?.and;
				if (notEmpty(rules)) {
					return valid && aapObj.common.checkRules(aapObj, rules);
				} else {
					return valid
				}
			} else if (rules?.or) {
				var valid = false;
				if (rules?.or?.admin && aapObj.canEdit) {
					valid = true;
				}
				if (rules?.or?.pages && currentPage.some(p => rules?.or.pages.includes(p))) {
					valid = true;
				}
				if (rules?.or?.roles && myRoles.some(r => rules?.or.roles.includes(r))) {
					valid = true;
				}
				if (rules?.or?.roles && notNull(user) && user == userId) {
					valid = true;
				}
				//check the rest of rules
				delete rules?.or;
				if (notEmpty(rules)) {
					return valid && aapObj.common.checkRules(aapObj, rules);
				} else {
					return valid
				}
			} else if (notEmpty(rules) && !notEmpty(rules?.and) && !notEmpty(rules?.or)) {
				var obj = { 'rules': {} };
				obj.rules.and = rules;
				return aapObj.common.checkRules(aapObj, obj.rules);
			} else if (!notEmpty(rules)) {
				return true
			}
		},
		getQuery: function (aapObj) {
			var urlQuery = document.location.href;
			var qs = '';
			if (urlQuery.indexOf('?') > -1 && urlQuery.indexOf('#') > -1) {
				qSplit = urlQuery.split('?');
				qs = qSplit[1].substring(qSplit[1].indexOf('?') + 1).split("&").concat(qSplit[0].substring(qSplit[0].indexOf('#') + 1).split("."));
				for (var i = 0, result = {}; i < qs.length; i++) {
					if (qs[i].indexOf('=') > -1) {
						qs[i] = qs[i].split('=');
					} else if (qs[i + 1]) {
						qs[i] = [qs[i], qs[i + 1]];
					}
					result[qs[i][0]] = qs[i][1];
				}
			} else if (urlQuery.indexOf('?') > -1) {
				qs = urlQuery.substring(urlQuery.indexOf('?') + 1).split("&");
				for (var i = 0, result = {}; i < qs.length; i++) {
					qs[i] = qs[i].split('=');
					result[qs[i][0]] = qs[i][1];
				}
			} else {
				qs = urlQuery.substring(urlQuery.indexOf('#') + 1).split(".");
				for (var i = 0, result = {}; i < qs.length; i++) {
					if (qs[i + 1]) {
						qs[i] = [qs[i], qs[i + 1]];
						result[qs[i][0]] = qs[i][1];
					}
				}
			}

			return result;
		},
		checkVisible: function (elm) {
			var rect = elm.getBoundingClientRect();
			var viewHeight = Math.max(document.documentElement.clientHeight, window.innerHeight);
			return !(rect.top - viewHeight / 2 >= 0);
		},
		breadcrumbHtml(aapObj) {
			var key = Object.keys(aapObj.pageDetail)[0];
			if (location.hash.indexOf(".") > -1) {
				const currentHash = location.hash.substring(0, location.hash.indexOf("."))
				if (key != currentHash) {
					key = currentHash
				}
			}
			var outputHtml = "";
			var aapview = sessionStorage.getItem('aapview');
			aapview = aapview ? aapview : '';
			const currentQuery = aapObj.common.getQuery();
			var contextTexts = {
				'#proposalAap': ucfirst(trad.proposal),
				'#projectsAap': ucfirst(trad.project),
				'#presentationAap': ucfirst(tradCategory.presentation),
				'#dashboardAap': ucfirst(trad.Dashboard),
				'#myproposalsAap': ucfirst(trad["My proposals"]),
				'#myprojectsAap': ucfirst(trad.myprojects),
				'#configurationAap': ucfirst(trad.Configuration),
				'#contributionAap': ucfirst(tradCategory.contribution),
				'#coformAap': ucfirst(trad["Submit a dossier"]),
				'#editCoformAap': ucfirst(trad["EditProposal"])
			}
			switch (key) {
				case "#proposalAap":
				case "#projectsAap":
					outputHtml += dataHelper.printf('' +
						'<li><a href="javascript:void(0);" style="pointer-events: none;" >{{level1}}</a></li>' +
						'<li><a href="{{hash}}" class="active lbh">{{level2}}</a></li>', {
						level1: aapObj.context.name,
						level2: contextTexts[key],
						hash: aapObj.common.buildUrlQuery(key, {
							context: aapObj.context.slug,
							formid: aapObj.form._id.$id,
							aapview: aapview
						}, aapObj.common.getQueryStringObject())
					});
					break;
				case "#presentationAap":
				case "#dashboardAap":
				case "#myproposalsAap":
				case "#myprojectsAap":
				case "#configurationAap":
				case "#contributionAap":
					outputHtml += dataHelper.printf('' +
						'<li><a href="javascript:void(0);" style="pointer-events: none;" >{{level1}}</a></li>' +
						'<li><a href="{{hash}}" class="active lbh">{{level2}}</a></li>', {
						level1: aapObj.context.name,
						level2: costum.app[key] && costum.language &&
							costum.app[key].name && costum.app[key].name[costum.language] ?
							costum.app[key].name[costum.language] : contextTexts[key],
						hash: aapObj.common.buildUrlQuery(key, {
							context: aapObj.context.slug,
							formid: aapObj.form._id.$id,
							aapview: aapview,
							userId: userId ? userId : ""
						}, aapObj.common.getQueryStringObject())
					});
					break;
				case "#detailProposalAap":
					var title = aapObj.answer && aapObj.answer.answers && aapObj.answer.answers.aapStep1 && aapObj.answer.answers.aapStep1 && aapObj.answer.answers.aapStep1.titre ? aapObj.answer.answers.aapStep1.titre : '';
					var realTitle = title ? title : coTranslate('No title');
					$('h3.title span.main').text(realTitle);
					if (realTitle != coTranslate('No title')) {
						const currentTitle = document.title;
						const newTitle = currentTitle.includes('|') ? currentTitle.split('|')[1].trim() : currentTitle;
						document.title = `${ucfirst(realTitle)} | ${newTitle}`;
					}
					outputHtml += dataHelper.printf('' +
						'<li><a href="javascript:void(0);" style="pointer-events: none;" >{{level1}}</a></li>' +
						'<li><a href="{{hash}}" class="active lbh">{{level2}}</a></li>' +
						'<li><a href="javascript:void(0);" style="pointer-events: none;" class="detail-breadcrumb">{{level3}}</a></li>', {
						level1: aapObj.context.name,
						level2: currentQuery?.userId ? ucfirst(trad["My proposals"]) : ucfirst(trad.proposal),
						level3: realTitle.toLocaleLowerCase(),
						hash: notEmpty(aapview) && aapview == "kanban" && aapObj.defaultProposal != null && aapObj.defaultProposal._id ?
							aapObj.common.buildUrlQuery('#detailProposalAap', currentQuery?.userId ? {
								context: aapObj.context.slug,
								formid: aapObj.form._id.$id,
								aapview: notEmpty(aapview) && aapview != "kanban" ? aapview : '',
								answerId: aapObj.myDefaultProposal != null && aapObj.myDefaultProposal._id ? aapObj.myDefaultProposal._id.$id : '',
								projectId: aapObj.myDefaultProposal != null && aapObj.myDefaultProposal.project && aapObj.myDefaultProposal.project.id ? aapObj.myDefaultProposal.project.id : '',
								userId: currentQuery?.userId ? currentQuery?.userId : ""
							} : {
								context: aapObj.context.slug,
								formid: aapObj.form._id.$id,
								aapview: notEmpty(aapview) && aapview != "kanban" ? aapview : '',
								projectId: aapObj.defaultProposal != null && aapObj.defaultProposal.project && aapObj.defaultProposal.project.id ? aapObj.defaultProposal.project.id : '',
								answerId: aapObj.defaultProposal != null && aapObj.defaultProposal._id ? aapObj.defaultProposal._id.$id : '',
							}, aapObj.common.getQueryStringObject())
							: aapObj.common.buildUrlQuery(currentQuery?.userId ? "#myproposalsAap" : '#proposalAap', currentQuery?.userId ? {
								context: aapObj.context.slug,
								formid: aapObj.form._id.$id,
								aapview: aapview,
								userId: currentQuery?.userId ? currentQuery?.userId : ""
							} : {
								context: aapObj.context.slug,
								formid: aapObj.form._id.$id,
								aapview: aapview
							}, aapObj.common.getQueryStringObject())
					});
					break;
				case "#detailProjectAap":
					const projectIdFromQuery = aapObj.common.getQuery().projectId;
					var hash1 = `#projectsAap.context.${aapObj.context.slug}.formid.${aapObj.form._id.$id}`;
					var title = "";
					var loadProjectDetailBreadcrumb = function () {
						outputHtml += dataHelper.printf('' +
							'<li><a href="javascript:void(0);" style="pointer-events: none;" >{{level1}}</a></li>' +
							'<li><a href="{{hash}}" class="active lbh">{{level2}}</a></li>' +
							'<li><a href="javascript:void(0);" style="pointer-events: none;" class="detail-breadcrumb">{{level3}}</a></li>', {
							level1: aapObj.context.name,
							level2: currentQuery?.projectUserId ? ucfirst(trad["myprojects"]) : ucfirst(trad.project),
							level3: aapObj.project.name.toLocaleLowerCase(),
							hash: notEmpty(aapview) && aapview == "kanban" && aapObj.defaultProject != null && aapObj.defaultProject._id ?
								aapObj.common.buildUrlQuery('#detailProjectAap', currentQuery?.projectUserId ? {
									context: aapObj.context.slug,
									formid: aapObj.form._id.$id,
									aapview: notEmpty(aapview) && aapview != "kanban" ? aapview : 'action',
									answerId: aapObj.myDefaultProject != null && notEmpty(aapObj.myDefaultProject.answer) && aapObj.myDefaultProject.answer.$id ? aapObj.myDefaultProject.answer.$id : '',
									projectId: aapObj.myDefaultProject != null && aapObj.myDefaultProject._id ? aapObj.myDefaultProject._id.$id : '',
									projectUserId: currentQuery?.projectUserId ? currentQuery?.projectUserId : ""
								} : {
									context: aapObj.context.slug,
									formid: aapObj.form._id.$id,
									aapview: notEmpty(aapview) && aapview != "kanban" ? aapview : 'action',
									projectId: aapObj.defaultProject != null && aapObj.defaultProject._id ? aapObj.defaultProject._id.$id : '',
									answerId: aapObj.defaultProject != null && notEmpty(aapObj.defaultProject.answer) && aapObj.defaultProject.answer.$id ? aapObj.defaultProject.answer.$id : '',
								}, aapObj.common.getQueryStringObject())
								: aapObj.common.buildUrlQuery(currentQuery?.projectUserId ? '#myprojectsAap' : '#projectsAap', currentQuery?.projectUserId ? {
									context: aapObj.context.slug,
									formid: aapObj.form._id.$id,
									projectUserId: currentQuery?.projectUserId ? currentQuery?.projectUserId : "",
									aapview: aapview
								} : {
									context: aapObj.context.slug,
									formid: aapObj.form._id.$id,
									aapview: aapview
								}, aapObj.common.getQueryStringObject())
						});
						$('h3.title span.main').text(aapObj.project.name);
						if (aapObj.project.name) {
							const currentTitle = document.title;
							const newTitle = currentTitle.includes('|') ? currentTitle.split('|')[1].trim() : currentTitle;
							document.title = `${ucfirst(aapObj.project.name)} | ${newTitle}`;
						}
					}

					if (typeof aapObj.project?.name == "undefined" && projectIdFromQuery) {
						aapObj.common.getOneProject(aapObj, projectIdFromQuery, () => {
							loadProjectDetailBreadcrumb();
						})
					} else {
						loadProjectDetailBreadcrumb();
					}
					break;
				case "#coformAap":
					var title = costum.app[key] && costum.language &&
						costum.app[key].name && costum.app[key].name[costum.language] && trad[costum.app[key].name[costum.language]] ?
						trad[costum.app[key].name[costum.language]] : contextTexts[key];
					outputHtml += dataHelper.printf('' +
						'<li><a href="javascript:void(0);" style="pointer-events: none;" >{{level1}}</a></li>' +
						'<li><a href="{{hash}}" class="active lbh">{{level2}}</a></li>' +
						'<li><a href="javascript:void(0);" style="pointer-events: none;" class="detail-breadcrumb">{{level3}}</a></li>', {
						level1: aapObj.context.name,
						level2: currentQuery?.userId ? ucfirst(trad["My proposals"]) : ucfirst(trad.proposal),
						level3: title,
						hash: aapObj.common.buildUrlQuery(currentQuery?.userId ? "#myproposalsAap" : '#proposalAap', currentQuery?.userId ? {
							context: aapObj.context.slug,
							formid: aapObj.form._id.$id,
							aapview: aapview,
							userId: currentQuery?.userId ? currentQuery?.userId : ""
						} : {
							context: aapObj.context.slug,
							formid: aapObj.form._id.$id,
							aapview: aapview
						}, aapObj.common.getQueryStringObject())
					});
					break;
				case "#editCoformAap":
					var editCoformAap = costum.app["#editCoformAap"];
					var title = costum.app[key] && costum.language &&
						costum.app[key].name && costum.app[key].name[costum.language] && trad[costum.app[key].name[costum.language]] ?
						trad[costum.app[key].name[costum.language]] : contextTexts[key];
					outputHtml += dataHelper.printf('' +
						'<li><a href="javascript:void(0);" style="pointer-events: none;" >{{level1}}</a></li>' +
						'<li><a href="{{hash}}" class="active lbh">{{level2}}</a></li>' +
						'<li><a href="javascript:void(0);" style="pointer-events: none;" class="">{{level3}}</a></li>', {
						level1: aapObj.context.name,
						level2: currentQuery?.userId ? ucfirst(trad["My proposals"]) : ucfirst(trad.proposal),
						level3: title,
						hash: aapObj.common.buildUrlQuery(currentQuery?.userId ? "#myproposalsAap" : '#proposalAap', currentQuery?.userId ? {
							context: aapObj.context.slug,
							formid: aapObj.form._id.$id,
							aapview: aapview,
							userId: currentQuery?.userId ? currentQuery?.userId : ""
						} : {
							context: aapObj.context.slug,
							formid: aapObj.form._id.$id,
							aapview: aapview
						}, aapObj.common.getQueryStringObject())
					});
					break;
				default:
					break;
			}
			$('.aap-breadcrumb').html(outputHtml);
		},
		addLinkToCommunityAttr() {
			var linktoCommunityInterval = setInterval(() => {
				if ($("#modal-invite #stepResult .link-to-community").is(":visible") || !$("#modal-invite").is(":visible")) {
					$("#modal-invite #stepResult .link-to-community").attr("data-link", `${baseUrl}/costum/co/index/slug/${costum?.slug}#communityAap.context.${aapObj.context?.slug}.formid.${aapObj.form?.["_id"]?.["$id"]}`)
					clearInterval(linktoCommunityInterval)
					linktoCommunityInterval = null
				}
			}, 350);
		},
		getOneAnswer(aapObj, answerId) {
			var url = baseUrl + '/co2/aap/proposition/request/get_one_answer';
			var post = {
				id: answerId
			};
			ajaxPost(null, url, post, function (data) {
				answer = data;
				aapObj.answer = data;
			}, null, null, {
				async: false
			});
			return aapObj.answer;
		},
		getOneProject(aapObj, projectId, callBack = () => {
		}) {
			var url = baseUrl + '/co2/aap/project/request/get_one_project';
			var post = {
				id: projectId
			};
			ajaxPost(null, url, post, function (data) {
				answer = data;
				aapObj.project = data;
				callBack;
			}, null, null, {
				async: false
			});
			return aapObj.project;
		},
		loadProposalDetail(aapObj, answerId, defaultView) {
			if (typeof defaultView !== 'string') {
				defaultView = 'summary';
			}
			switch (defaultView) {
				case 'funding':
					aapObj.common.load_financement_html(aapObj, answerId);
					break;
				case 'evaluation':
					aapObj.common.load_evaluation_view(aapObj, answerId);
					break;
				case 'contribution':
					aapObj.common.load_contribution_view(aapObj, answerId);
					break;
				default:
					aapObj.common.load_summary_view(aapObj, answerId);
					break;
			}
			aapObj.common.viewedProposal(aapObj, answerId);
			aapObj.events.proposalDetail(aapObj);
			aapObj.common.authorizationAction(aapObj, { answer: answerId }).then(function (authorization) {
				var switchResumeDom = $('.btn-switch-resume');
				if (!authorization.canEdit || defaultView !== 'summary') {
					switchResumeDom.hide();
					switchResumeDom.find('.edit-proposal').removeAttr('onclick');
				} else {
					switchResumeDom.show();
				}
			});
			aapObj.directory.proposalNotSeenCounter(aapObj);
		},
		loadProjectDetail(aapObj, projectId, answerId, view = 'action') {
			switch (view) {
				case 'action':
					aapObj.common.loadKanban(aapObj, projectId, '#kanban-container1');
					break;
				case 'plan':
					aapObj.common.load_project_detail_html(aapObj, projectId);
					break;
				case 'funding':
					aapObj.common.load_financement_html(aapObj, answerId, "#funding", projectId);
					break;
				case 'summary':
					aapObj.common.load_summary_view(aapObj, answerId, '#summary');
					break;
				case 'evaluation':
					aapObj.common.load_evaluation_view(aapObj, answerId, '#evaluation', { readOnly: true });
					break;
				case 'aboutelem':
					aapObj.common.loadAboutElement(aapObj, 'projects', projectId, "#about-element-container")
					break;
			}
			if (typeof aapObj.form.coremu != "undefined" && aapObj.form.coremu == true)
				aapObj.common.load_contribution_view(aapObj, answerId, "#contribution");
			aapObj.common.viewedProject(aapObj, projectId);
			aapObj.events.projectDetail(aapObj);


			io = typeof io === 'undefined' ? null : io;
			detailProject($, wsCO, io, coWsConfig, projectId);
		},
		load_summary_view(aapObj, answerId, container = '#proposition-summary-container') {
			// coInterface.showLoader(container);
			coInterface.showCostumLoader(container);
			const url = `${baseUrl}/co2/aap/proposition/request/get_summary`;
			const post = {
				id: answerId
			};
			setTimeout(function () {
				ajaxPost(null, url, post, function (data) {
					$(container).empty().html(data);
					$('#proposition-summary-span').html(trad["Modify proposal"]);
					$('#proposition-summary-i').addClass("fa-pencil-square-o");
					$('#proposition-summary-i').removeClass("fa-file");
					$('.edit-proposal').attr('onClick', 'aapObj.common.load_proposal_edit(aapObj,"' + answerId + '")');
				});
			});
		},
		load_proposal_edit(aapObj, answerId, container = '#proposition-summary-container') {
			eventScrollAlreadyCalled = false;
			const url = baseUrl + '/survey/answer/answer/id/' + answerId + '/step/aapStep1';
			// coInterface.showLoader(container);
			coInterface.showCostumLoader(container);
			setTimeout(function () {
				ajaxPost(null, url, null, function (data) {
					$(container).empty().html(data);
					$('#proposition-summary-span').html(trad["Access the summary"]);
					$('#proposition-summary-i').removeClass("fa-pencil-square-o");
					$('#proposition-summary-i').addClass("fa-file");
					$('.edit-proposal').attr('onClick', 'aapObj.common.load_summary_view(aapObj,"' + answerId + '")');
					aapObj.events.summaryView()
				}, null, null);
			});
		},
		load_financement_html(aapObj, answerId, container = '#proposition-funding', projectId = null) {
			// coInterface.showLoader(container, trad.currentlyloading);
			coInterface.showCostumLoader(container);
			var url = "";
			var offlineUrl = "";
			if (isUserConnected != "logged") {
				offlineUrl = "/view/offline"
			}
			if (notNull(projectId)) {
				url = baseUrl + '/survey/answer/answer/project/' + projectId + '/step/aapStep3/isinsideform/false/input/financer' + offlineUrl;
			} else {
				url = baseUrl + '/survey/answer/answer/id/' + answerId + '/step/aapStep3/isinsideform/false/input/financer' + offlineUrl;
			}
			setTimeout(function () {
				ajaxPost(null, url, null, function (data) {
					$(container).empty().html(data);
				}, null, null);
			});
		},
		load_contribution_view(aapObj, answerId, container = '#proposition-contribution') {
			// coInterface.showLoader(container, trad.currentlyloading);
			coInterface.showCostumLoader(container);
			var url = baseUrl + '/survey/answer/answer/id/' + answerId + '/step/aapStep1/isinsideform/false/input/depense';
			setTimeout(function () {
				ajaxPost(null, url, null, function (data) {
					$(container).empty().html(data);
				}, null, null);
			});
		},
		load_evaluation_view(aapObj, answerId, container = '#proposition-evaluation', options) {
			var defaultOption = {
				readOnly: false
			};
			if (typeof options === 'undefined') {
				options = defaultOption;
			} else {
				options = $.extend({}, defaultOption, options);
			}
			// coInterface.showLoader(container, trad.currentlyloading);
			coInterface.showCostumLoader(container);
			var url = baseUrl + '/survey/answer/answer/id/' + answerId + '/step/aapStep2/isinsideform/false';
			if (options.readOnly) {
				url += '/mode/r';
			}
			setTimeout(function () {
				ajaxPost(null, url, null, function (data) {
					$(container).empty().html(data);
				}, null, null);
			});
		},
		load_project_detail_html(aapObj, projectId, container = '#plan', options) {
			var defaultOption = {};
			if (typeof options === 'undefined')
				options = defaultOption;
			else
				options = $.extend({}, defaultOption, options);
			coInterface.showCostumLoader(container);
			const url = `${baseUrl}/co2/aap/project/request/project_detail`;
			const post = { project_id: projectId };
			if (typeof options.action === 'string' && options.action !== '')
				post.name = options.action;
			if (typeof options.contributor === 'string' && options.contributor !== '')
				post.user = options.contributor;
			const query = aapObj.common.getQuery();
			if (query.userId)
				post.user = query.userId;
			setTimeout(function () {
				ajaxPost(null, url, post, function (response) {
					var contributors = response.contributors;
					$(container).empty().html(response.html);
					$(container).find('.form-inline').remove();
					$(container).prepend(dataHelper.printf('' +
						'<div class="form-inline">' +
						'    <div class="form-group">' +
						'        <input type="text" class="form-control input-sm" id="multi-view-filter-input" placeholder="Rechercher une action" value="{{inputFilter}}">' +
						'    </div>' +
						'    <div class="form-group">' +
						'        <select id="multi-view-filter-contributor">' +
						'            <option value="all" data-image="{{defaultImage}}" {{selectedWhenNoUser}}>{{everybodyTrad}}</option>' +
						'            {{mapContributors}}' +
						'        </select>' +
						'    </div>' +
						'</div>', {
						inputFilter: response.inputFilter.replace(/"/g, '&quot;'),
						selectedWhenNoUser: typeof query.userId === 'undefined' || typeof contributors[query.userId] === 'undefined' ? 'selected="selected"' : '',
						defaultImage: defaultImage,
						everybodyTrad: trad.Everyone,
						mapContributors: Object.keys(contributors).map(function (contributorKey) {
							return dataHelper.printf('<option value="{{id}}" data-image="{{image}}" {{selectedWhen}}>{{name}}</option>', {
								id: contributorKey,
								name: contributors[contributorKey].name,
								image: contributors[contributorKey].image,
								selectedWhen: typeof query.userId !== 'undefined' && query.userId === contributorKey ? 'selected="selected"' : ''
							});
						}).join('')
					}));
					$('#multi-view-filter-contributor').select2({
						width: '250px', formatResult(state) {
							var elementDom = $(state.element[0]);
							return dataHelper.printf('' +
								'<div class="avatar-container">' +
								'    <div class="image"><img src="{{image}}" alt="{{name}}"></div>' +
								'    <span class="name">{{name}}</span>' +
								'</div>', {
								image: elementDom.data('image'), name: state.text
							});
						}
					});
					aapObj.events.projectDetail(aapObj);
				});
			});
		},
		loadKanban(aapObj, projectId, container = '#kanban-container1', filter = {}) {
			coInterface.showCostumLoader(container);
			const url = `${baseUrl}/costum/agenda/projects/request/all/reorder/1`;
			const post = {
				'ids': [projectId]
			};
			if (filter.name)
				post.agendaContentText = filter.name;
			if (filter.contributor)
				post.members = [filter.contributor];
			var contributorFilter;
			setTimeout(function () {
				ajaxPost(null, url, post, function (projects) {
					function getHeaderLabel(index) {
						return aapObj.context && aapObj.context.oceco && aapObj.context.oceco.kanbanColumns && aapObj.context.oceco.kanbanColumns[mainLanguage] && aapObj.context.oceco.kanbanColumns[mainLanguage][index] ? aapObj.context.oceco.kanbanColumns[mainLanguage][index] : '';
					}

					var headers = [
						{
							id: 'discuter',
							label: getHeaderLabel('discuter') ? getHeaderLabel('discuter') : coTranslate("act.discuter"),
							classNameList: ['discuter-column']
						},
						{
							id: 'todo',
							label: getHeaderLabel('todo') ? getHeaderLabel('todo') : coTranslate("act.todo"),
							classNameList: ['todo-column']
						},
						{
							id: 'next',
							label: getHeaderLabel('next') ? getHeaderLabel('next') : coTranslate("Next"),
							classNameList: ['next-column']
						},
						{
							id: 'tracking',
							label: getHeaderLabel('tracking') ? getHeaderLabel('tracking') : coTranslate("act.tracking"),
							classNameList: ['tracking-column']
						},
						{
							id: 'totest',
							label: getHeaderLabel('totest') ? getHeaderLabel('totest') : coTranslate("act.totest"),
							classNameList: ['totest-column']
						},
						{
							id: 'done',
							label: getHeaderLabel('done') ? getHeaderLabel('done') : coTranslate("act.done"),
							classNameList: ['done-column']
						}
					];
					var has_next = false;
					var data = [];
					var actionContributors = {};
					var projectContributors = {};
					var isContributor = false;
					var milestones = [];
					var existing_tags = [];
					$.each(projects.data, function (_, oneProject) {
						projectContributors = $.extend({}, oneProject.contributors);
						isContributor = isContributor ? isContributor : userId && (userConnected.roles.superAdmin || typeof projectContributors[userId] === 'object');
						var position_by_status = {};
						milestones = milestones.concat(milestones, oneProject.milestones);
						$.each(oneProject.subs, function (_, oneAction) {
							var doneTaskLength = oneAction.subs.filter(function (task) {
								return task.checked;
							}).length;
							var taskSummary = doneTaskLength + '/' + oneAction.subs.length;
							var datum;
							var i;

							// if (!has_next && coInterface.actions.get_action_status(oneAction) === "next")
								has_next = true;
							if (oneAction.tags) {
								i = 0;
								while (i < oneAction.tags.length) {
									if (!existing_tags.includes(oneAction.tags[i]))
										existing_tags.push(oneAction.tags[i]);
									i++;
								}
							}
							if (notEmpty(userConnected)) {
								datum = {
									id: oneAction.id.substr(3),
									title: oneAction.name,
									tags: oneAction.tags,
									header: coInterface.actions.get_action_status(oneAction),
									milestone: oneAction.milestone && oneAction.milestone.milestoneId ? oneAction.milestone.milestoneId : null,
									position: _,
									contributors: Object.keys(oneAction.contributors).map(function (contributorKey) {
										var contributor = oneProject.contributors[contributorKey];
										contributor.id = contributorKey;
										contributor.data = {
											id: contributorKey,
										};
										actionContributors[contributorKey] = {
											name: contributor.name,
											image: contributor.image
										};
										return contributor;
									}),
									actions: [{
										icon: 'fa fa-archive',
										action: 'onCardArchiveClick',
										bstooltip: { text: trad['Archive'], position: 'top' }
									}, {
										icon: 'fa fa-commenting',
										badge: oneAction.commentCount.toString(),
										bstooltip: { text: trad.comments, position: 'top' },
										action: 'onCardCommentClick'
									}, {
										icon: 'fa fa-picture-o',
										badge: oneAction.images.length.toString(),
										bstooltip: { text: trad.images, position: 'top' },
										action: 'onCardPictureClick',
										hideCondition: { badge: '0' }
									}, {
										icon: 'fa fa-file',
										badge: oneAction.documents.length.toString(),
										action: 'onCardFileClick',
										bstooltip: { text: trad.files, position: 'top' },
										hideCondition: { badge: '0' },
									}, {
										badge: taskSummary,
										bstooltip: {
											text: trad.task, position: 'top'
										},
										className: taskSummary !== '0/0' ? (doneTaskLength === oneAction.subs.length ? 'kanban-done-task-action' : 'kanban-doing-task-action') : 'kanban-no-task-action'
									}, {
										icon: typeof oneAction.contributors[userId] !== 'undefined' ? 'fa fa-unlink' : 'fa fa-link',
										bstooltip: {
											text: typeof oneAction.contributors[userId] !== 'undefined' ? coTranslate('Quit') : coTranslate('Participate'),
											position: 'top'
										},
										action: 'onParticipate'
									}],
									images: oneAction.images
								};
							} else {
								datum = {
									id: oneAction.id.substr(3),
									title: oneAction.name,
									tags: oneAction.tags,
									header: coInterface.actions.get_action_status(oneAction),
									milestone: oneAction.milestone && oneAction.milestone.milestoneId ? oneAction.milestone.milestoneId : null,
									position: _,
									contributors: Object.keys(oneAction.contributors).map(function (contributorKey) {
										var contributor = oneProject.contributors[contributorKey];
										contributor.id = contributorKey;
										contributor.data = {
											id: contributorKey,
										};
										actionContributors[contributorKey] = {
											name: contributor.name,
											image: contributor.image
										};
										return contributor;
									}),
									actions: [{
										icon: 'fa fa-picture-o',
										badge: oneAction.images.length.toString(),
										bstooltip: { text: trad.images, position: 'top' },
										action: 'onCardPictureClick',
										hideCondition: { badge: '0' }
									}],
									images: oneAction.images
								};
							}
							if (typeof position_by_status[datum.header] !== 'number')
								position_by_status[datum.header] = -1;
							datum.position = ++position_by_status[datum.header];
							if (oneAction.name.match(/Subvention AAP Politique de la Ville 2022(.*)Montant Global incluant Ville, Etat, Bailleurs/i)) {
								datum.html = true;
								datum.editable = false;
							} else
								datum.editable = userConnected !== null && typeof oneAction.creator === 'string' && (oneAction.creator === userConnected._id.$id || (typeof oneAction.links != "undefined" && typeof oneAction.links.canEdit != "undefined" && typeof oneAction.links.canEdit[userConnected._id.$id] != "undefined"));
							data.push(datum);
						});
					});
					if (!has_next)
						headers.splice(1, 1);
					data = data.map(function (dataMap) {
						dataMap.canMoveCard = isContributor;
						if (!isContributor) {
							dataMap.actions = dataMap.actions.map(function (actionMap) {
								if (['onCardCommentClick', 'onCardArchiveClick'].includes(actionMap.action)) {
									delete actionMap.action;
								}
								return actionMap;
							});
						}
						return dataMap;
					});
					var query = aapObj.common.getQuery();
					existing_tags = existing_tags.sort((a, b) => a.toLowerCase() < b.toLowerCase() ? -1 : (a == b ? 0 : 1));
					$(container).prev('.kanb-filter-contaienr').remove();
					$(container).before(dataHelper.printf('' +
						'<div class="kanb-filter-contaienr">' +
						'   <div class="form-inline">' +
						'       <div class="form-group">' +
						'           <input type="text" class="form-control input-sm" id="kanban-filter-input" placeholder="Rechercher une action">' +
						'       </div>' +
						'       <div class="form-group">' +
						'           <select class="form-control input-sm" id="kanb-filter-milestone">' +
						'           	<option value="" selected>{{jalon_label}}</option>' +
						'           	{{jalons}}' +
						'           </select>' +
						'       </div>' +
						'   </div>' +
						'   <div class="label-group">' +
						'   	{{tags}}' +
						'   </div>' +
						'</div>', {
						jalons: milestones.map(function (j) {
							return ('<option value="' + j.milestoneId + '">' + j.name + '</option>')
						}).join(""),
						tags: existing_tags.map(function (tag) {
							return ('<label class="kanb-filter-tags label label-default" type="button" data-target="' + tag + '">#' + tag + '</label>')
						}).join(""),
						jalon_label: milestones.length === 0 ? "(Aucun jalon)" : "(Aucun jalon sélectionné)",
					}));
					contributorFilter = topContributorFilter('#contributor-filter-container');
					contributorFilter.build(Object.keys(actionContributors).map(function (contributorKey) {
						return {
							id: contributorKey,
							name: projectContributors[contributorKey].name,
							image: projectContributors[contributorKey].image
						}
					}), query.projectUserId ? query.projectUserId : query.userId);
					contributorFilter.onFilter = function () {
						var getQuery = aapObj.common.getQuery();
						var putQuery = {};
						var value = this.selected === null ? '' : this.selected;
						if (typeof getQuery.userId === 'undefined' && typeof getQuery.projectUserId === 'undefined') {
							putQuery.userId = value;
						} else if (typeof getQuery.projectUserId === 'string') {
							putQuery.projectUserId = value;
						} else {
							putQuery.userId = value;
						}
						var href = aapObj.common.buildUrlQuery(document.location.href, putQuery, aapObj.common.getQueryStringObject());
						history.pushState({}, null, href);
						filterKanban();
					}
					$('#kanban-filter-contributor').select2({
						width: '250px',
						formatResult: function (state) {
							var elementDom = $(state.element[0]);
							return dataHelper.printf('' +
								'<div class="avatar-container">' +
								'    <div class="image"><img src="{{image}}" alt="{{name}}"></div>' +
								'    <span class="name">{{name}}</span>' +
								'</div>', {
								image: elementDom.data('image'), name: state.text
							});
						}
					});

					var onFirstLoad = true;
					var kanbanContainerDom = $(container)
						.kanban('destroy')
						.kanban({
							headers,
							data,
							showCardNumber: true,
							language: mainLanguage,
							endpoint: `${baseUrl}/plugins/kanban/`,
							actionConditionEnabled: true,
							showContributors: true,
							canAddCard: isContributor,
							canEditHeader: isContributor,
							editable: isContributor,
							enableMention: true,
							canContributes: Object.keys(projectContributors).map(key => {
								const contributor = projectContributors[key];
								return {
									id: key,
									name: contributor.name,
									alias: contributor.username,
									image: contributor.image
								};
							}),
							onParticipate: function (data) {
								var contributors = data.contributors.map(contributor => contributor.id);
								var index = contributors.indexOf(userId);
								if (index >= 0)
									contributors.splice(index, 1);
								else
									contributors.push(userId);
								var params = {
									action: data.id,
									contributor: userId,
									participate: index >= 0 ? 0 : 1
								}
								coInterface.actions.request_participation(params).then(function (contribs) {
									data.contributors = contribs;
									var is_contributor = contributors.indexOf(userId);
									for (var i = 0; i < data.actions.length; i++) {
										action = data.actions[i];
										if (action.action === 'onParticipate') {
											if (is_contributor >= 0) {
												action.icon = 'fa fa-unlink';
												action.bstooltip.text = coTranslate('Quit');
											} else {
												action.icon = 'fa fa-link';
												action.bstooltip.text = coTranslate('Participate');
											}
										}
										data.actions[i] = action;
									}
									kanbanContainerDom.kanban('setData', { id: data.id, data: data });
								});
								var url = baseUrl + '/costum/project/action/request/set_contributors';
								ajaxPost(null, url, {
									action: data.id,
									contributors: contributors,
									emiter: wsCO && wsCO.id ? wsCO.id : null
								}, function (contribs) {

								});
							},
							onRenderDone: function () {
								kanbanContainerDom.find('.kanban-list-cards').addClass('co-scroll');
								$('.tooltips').tooltip({
									container: 'body'
								});
								if (onFirstLoad) {
									onFirstLoad = false;
									filterKanban();
								}
							},
							onEditHeaderAction: function () {
								if (!isContributor) {
									toastr.error(coTranslate('You must be a contributor to make changes'));
								}
							},
							onInsertCardAction: function (column) {
								if (!isContributor) {
									toastr.error(coTranslate('You must be a contributor to make changes'));
								}
							},
							onCardClick: function (action) {
								if (notEmpty(userConnected)) {
									var self = $(this);
									var actionModalDom = $('#action-modal-preview').empty().html('');
									if (typeof action.isClickable === 'undefined' || action.isClickable) {
										action.isClickable = false;
										$('*').css('cursor', 'wait');
										self.data('datum', action);
										var url = baseUrl + '/costum/project/action/request/action_detail_html/mode/' + (isContributor ? 'w' : 'r');
										var id = action.id;
										var post = {
											id: id
										};
										setTimeout(function () {
											ajaxPost(null, url, post, function (html) {
												actionModalDom.off('shown.bs.modal').on('shown.bs.modal', '.modal', function () {
													action.isClickable = true;
													self.data('datum', action);
												});
												actionModalDom.html(html);
											}, null, 'text');
										});
									}
								}
							},
							onCardUpdate: function (action) {
								if (action.oldValue.title !== action.newValue.title) {
									var path2Value = {
										id: action.newValue.id,
										collection: 'actions',
										value: action.newValue.title,
										path: 'name'
									};
									dataHelper.path2Value(path2Value, function (response) {
										if (response.result) {
											ajaxPost(null, coWsConfig.pingActionManagement, {
												event: '.kanban-edit-card' + aapObj.common.getQuery().projectId, data: {
													id: action.newValue.id, title: action.newValue.title
												}
											}, null, null, { contentType: 'application/json' });
											toastr.success(response.msg);
										}
									});
								}
							},
							onCardDrop: function (dropped) {
								var action = dropped.data;
								var after = kanbanContainerDom.find("[data-id=" + action.id + "]").next(".kanban-list-card-detail");

								if (after.length === 0) {
									var next_column = dropped.columns.map(function (column) {
										return (column.id);
									}).indexOf(dropped.target) + 1;
									after = null;
									while (typeof dropped.columns[next_column] !== "undefined" && after === null) {
										after = kanbanContainerDom.find(".kanban-list-card-detail[data-column=" + dropped.columns[next_column].id + "]:first");
										if (after.length > 0)
											after = after.data("id");
										else
											after = null;
										next_column++;
									}
								}
								else
									after = after.data("id");
								coInterface.actions.update_action_position({
									action: action.id,
									after: after,
									group: action.header,
									emiter: wsCO && wsCO.id ? wsCO.id : null,
									component: 'kanban',
									server_url: baseUrl
								}).then();
								if (wsCO !== null && typeof wsCO.id === 'string') {
									ajaxPost(null, baseUrl + '/costum/project/action/request/aap-pnotify-project', {
										action: action.id,
										project: aapObj.common.getQuery().projectId,
										emiter: wsCO.id,
										target: dropped.target,
										position: action.position,
										component: 'kanban',
										after: after
									}, null, null, { contentType: 'application/json' });
								}
								if (dropped.origin === dropped.target)
									return (false);
								coInterface.actions.request_set_status({
									id: action.id,
									status: dropped.target,
									server_url: baseUrl,
									user: userId
								}).then(function (response) {
									action.tags = response.tags;
									toastr.success(coTranslate('Status change, success!'));
								}).catch(function (arg0) {
									console.error(arg0);
									toastr.error(coTranslate('Something went wrong. Please contact an administrator.'));
								});
							},
							onCardInsert: function (created) {
								created = $.extend({}, created);
								var post = {
									name: created.title,
									status: created.header,
									parentId: projectId,
									parentType: 'projects',
									component: 'kanban',
									mentions: created.contributors.map(c => c.alias),
								};
								if (wsCO && wsCO.id)
									post.emiter = wsCO.id;
								const is_contributor = created.contributors.some(s => s.id === userId);
								coInterface.actions.request_create_action(post).then(function (response) {
									// send create notification
									var id_at_kanban = created.id;
									created.id = response._id.$id;
									created.actions = [{
										icon: 'fa fa-archive',
										bstooltip: { text: trad.delete, position: 'top' },
										action: 'onCardArchiveClick',
										bstooltip: { text: trad['Archive'], position: 'top' }
									}, { icon: 'fa fa-commenting', badge: '0', action: 'onCardCommentClick' }, {
										icon: 'fa fa-picture-o',
										badge: '0',
										action: 'onCardPictureClick',
										hideCondition: { badge: '0' }
									},
									{
										icon: 'fa fa-file',
										badge: '0',
										action: 'onCardFileClick',
										hideCondition: { badge: '0' }
									},
									{
										badge: '0/0',
										className: 'kanban-no-task-action'
									}, {
										icon: `fa ${is_contributor ? "fa-unlink" : "fa-link"}`,
										bstooltip: {
											text: is_contributor ? coTranslate("Quit") : coTranslate("Participate"),
											position: 'top'
										},
										action: 'onParticipate'
									}];
									$(container).kanban('setData', { column: created.header, id: id_at_kanban, data: created });
									coInterface.actions.update_action_position({
										action: created.id,
										group: created.header,
										emiter: wsCO && wsCO.id ? wsCO.id : null,
										component: 'kanban',
										server_url: baseUrl
									}).then();
								}).catch(function (error) {
									mylog.error(error);
								});
							},
							onHeaderChange: function (changedHeader) {
								var { values } = changedHeader;
								if (values.old !== values.new) {
									var parent = aapObj.form.parent;
									var keys = Object.keys(parent);
									parent = parent[keys[0]];
									var id = keys[0];
									var path2Value = {
										id: id,
										collection: parent.type,
										value: values.new,
										path: `oceco.kanbanColumns.${mainLanguage}.${changedHeader.column}`
									}
									dataHelper.path2Value(path2Value, function (response) {
										if (response.result)
											toastr.success(response.msg);
									})
								}
							},
							onContributorClick: function (contributorData) {
								if (notEmpty(userConnected)) {
									var hrefDom = $('<a>', {
										href: `#page.type.citoyens.id.${contributorData.id}`,
										class: 'lbh-preview-element',
										hidden: true
									});
									$(document.body).append(hrefDom);
									coInterface.bindLBHLinks();
									hrefDom.trigger('click');
									hrefDom.remove();
								}
							},
							onCardPictureClick: function (action) {
								var imagesList = action.images;
								$(`a[data-lightbox=${action.id}]`).remove();
								if (imagesList.length > 0) {
									$.each(imagesList, function (_, oneImage) {
										$(document.body).append($('<a>', {
											href: oneImage, hidden: true,
										}).attr('data-lightbox', action.id));
									});
									$(`a[data-lightbox=${action.id}]:first`).trigger('click');
								}
							},
							onCardCommentClick: function (action) {
								if (notEmpty(userConnected)) {
									commentObj.openPreview("actions", action.id, action.id + ".comment", action.title.replace(/'/g, "\\'"));
								}
							},
							onCardArchiveClick: function (action, cardDom) {
								$.confirm({
									title: trad['Archive'], type: 'orange', content: trad['Please confirm archiving'], buttons: {
										no: {
											text: trad.no,
											btnClass: 'btn btn-default'
										}, yes: {
											text: trad.yes,
											btnClass: 'btn btn-warning',
											action: function () {
												coInterface.actions.request_archive(action.id).then(() => {
													kanbanContainerDom.kanban("deleteData", action)
												});
											}
										}
									}
								});
							},
							onCardFileClick: function (action) {
								var url = `${baseUrl}/costum/project/action/request/action_detail_html`;
								var id = action.id;
								var post = {
									id, activeTab: 'document'
								};
								ajaxPost(null, url, post, function (html) {
									$('#action-modal-preview').empty().html(html);
								}, null, 'text');
							},
							onContributorRemove: arg => {
								const was_contributor = arg.contributor.id === userId;
								const data = $.extend({}, arg.card, {actions: arg.card.actions.map(m => {
										if (m.action === "onParticipate") {
											m.icon = `fa ${was_contributor ? "fa-link" : "fa-unlink"}`;
											m.bstooltip.text = was_contributor ? coTranslate("Participate") : coTranslate("Quit");
										}
										return m;
									})});
								if (was_contributor) {
									kanbanContainerDom.kanban("setData", {
										id: arg.card.id,
										data: data,
									})
								}
								coInterface.actions.request_participation({
									action: arg.card.id,
									contributors: arg.card.contributors.map(c => c.id),
								}).then(r => toastr.success("La liste de contributeur a été mise à jour."));
							}
						});
					$(document.body).off('action.set-contributor').on('action.set-contributor', function (event, data) {
						var contributorKeys = Object.keys(data.contributors);
						var kanbanData = kanbanContainerDom.kanban('getData', { id: data.action });
						kanbanData.contributors = contributorKeys.map(function (contributorKey) {
							return {
								id: contributorKey,
								name: data.contributors[contributorKey].name,
								image: typeof data.contributors[contributorKey].image === 'string' ? data.contributors[contributorKey].image : modules.co2.url + '/images/thumb/default_citoyens.png'
							}
						});
						kanbanContainerDom.kanban('setData', { id: data.action, data: kanbanData });
					});
				});
			});
			var filterInputTimeout = null;

			function filterKanban() {
				var inputFilterDom = $('#kanban-filter-input');
				var query = aapObj.common.getQuery();
				var filter = {
					attributes: {}
				};
				if (inputFilterDom.val())
					filter.card = inputFilterDom.val();
				if (typeof query.userId !== 'undefined' && contributorFilter.selected !== null)
					filter.contributors = [contributorFilter.selected];
				if ($("#kanb-filter-milestone").val())
					filter.attributes.milestone = $("#kanb-filter-milestone").val();
				if ($(".kanb-filter-tags.label-primary").length) {
					filter.attributes.tags = [];
					$(".kanb-filter-tags.label-primary").each(function () {
						filter.attributes.tags.push(this.dataset.target);
					})
				};
				$(container).kanban('filter', filter);
			}

			$('#action').off('input').off('click').off("change")
				.on('input', '#kanban-filter-input', function () {
					var self = $(this);
					clearTimeout(filterInputTimeout);
					filterInputTimeout = setTimeout(filterKanban, 1000);
				}).on('change', '#kanban-filter-contributor', function () {
					filterKanban();
				}).on("change", "#kanb-filter-milestone", function () {
					filterKanban();
				}).on("click", ".kanb-filter-tags", function () {
					var self = $(this);

					self.toggleClass("label-primary");
					self.toggleClass("label-default");
					filterKanban();
				});
		},
		loadAboutElement(aapObj, elementType, elementId, container = '#about-element-container') {
			// coInterface.showLoader(container, trad.currentlyloading);
			coInterface.showCostumLoader(container);
			const url = `${baseUrl}/co2/aap/proposition/request/get_aboutelem`;
			setTimeout(function () {
				ajaxPost(container, url, {
					elementId: elementId,
					elementType: elementType
				}, function (element) {
				})
			});
		},
		modalProposalDetail: function (aapObj, answerId, isModal = false) {
			const url = baseUrl + '/co2/aap/getviewbypath/path/co2.views.aap.detailProposal';
			const post = {
				answerId: answerId, showFilter: false,
			};
			dialogContent.open({
				modalContentClass: "modal-custom-lg",
				isRemoteContent: true,
				shownCallback: () => {

				},
				hiddenCallback: () => {
					if (!isModal) {
						const query = aapObj.common.getQuery(aapObj);
						const new_hash = `proposalAap.context.${query.context}.formid.${query.formid}${document.location.href.indexOf('?') > -1 ? '?' + document.location.href.split('?')[1] : ''}`;
						history.pushState({}, null, `${document.location.origin}${document.location.pathname}#${new_hash}`);
						// document.title = title;
					}
					aapObj.directory.reloadAapProposalItem(aapObj, answerId, false);
					$("#dialogContent #dialogContentBody").empty()
					$("#openModal .modal-content").removeClass("aap-modal-md");
					$("#openModal .modal-content .close-modal").removeClass("aap-close-modal");
					$('#openModalContent').removeClass("container container-fluid").addClass("container");
				}
			});
			ajaxPost(null, url, post, function (data) {
				$("#dialogContent #dialogContentBody").empty().html(data)
				const view_target = $("#dialogContent .propsDetailPanel [role=tab]").attr('href').substring(13);
				const answer = $("#dialogContent .propsDetailPanel [role=tab]").data('answer');
				aapObj.common.loadProposalDetail(aapObj, answer, view_target);
				coInterface.bindLBHLinks();
			}, null, null);
			// ajaxPost(null, url, post, function(data){
			// 	$(' #openModal .propsDetailPanel [role=tab]').on('shown.bs.tab', function(){
			// 		$('body.index.modal-open').append(`
			//                 <div class="modal-backdrop my-backdrop fade in"></div>
			//             `)
			// 		const view_target = $(this).attr('href').substring(13);
			// 		const answer = $(this).data('answer');
			// 		aapObj.common.loadProposalDetail(aapObj, answer, view_target);
			// 	});
			// 	$('#openModal').on('hidden.bs.modal', function(){
			// 		$('#openModalContent').empty();
			// 		if(!isModal){
			// 			const query = aapObj.common.getQuery(aapObj);
			// 			const new_hash = `proposalAap.context.${query.context}.formid.${query.formid}${document.location.href.indexOf('?') > -1 ? '?' + document.location.href.split('?')[1] : ''}`;
			// 			const title = aapObj.pageDetail["#" + aapObj.page + "Aap"]?.subdomainName;
			// 			history.pushState({}, title, `${document.location.origin}${document.location.pathname}#${new_hash}`);
			// 			document.title = title;
			// 			$('body.index.modal-open .modal-backdrop.my-backdrop.fade.in').remove();
			// 		}
			// 		aapObj.directory.reloadAapProposalItem(aapObj, answerId,false);
			// 	})
			// }, null, null, {
			// 	async : false
			// });
		},
		modalProjectDetail: function (aapObj, projectId, answerId) {
			const url = baseUrl + '/co2/aap/getviewbypath/path/co2.views.aap.detailProject';
			const post = {
				projectId: projectId, answerId: answerId, showFilter: false,
			};
			dialogContent.open({
				modalContentClass: "modal-custom-lg",
				isRemoteContent: true,
				shownCallback: () => {

				},
				hiddenCallback: () => {
					$('#openModalContent').empty();
					$("#openModal").removeClass("fix-top")
					const newHash = aapObj.common.buildUrlQuery(document.location.href, {
						aapview: sessionStorage && sessionStorage.aapview && notEmpty(sessionStorage.aapview) ? sessionStorage.aapview : "action",
						"projectId": projectId
					}, aapObj.common.getQueryStringObject())
					history.pushState({}, null, newHash);
					aapObj.directory.reloadAapProjectItem(aapObj, projectId);
				}
			});
			ajaxPost(null, url, post, function (data) {
				// smallMenu.open(data)
				$("#dialogContent #dialogContentBody").empty().html(data)
				// $("#openModal").css({
				// 	cssText : `
				//         top: 0 !important;
				//         display: block !important;
				//         overflow: hidden !important;
				//         display: block !important;
				//         background-color: rgba(3, 3, 3, 0.62) !important;`
				// }).addClass("fix-top");
				// $("#openModal .modal-content").addClass("aap-modal-md");
				// $("#openModal .modal-content .close-modal").addClass("aap-close-modal");
				// $('#openModalContent').removeClass("container").addClass("container-fluid");
				// $('#openModal').on('hidden.bs.modal', function(){
				// 	$('#openModalContent').empty();
				// 	$("#openModal").removeClass("fix-top")
				// 	const query = aapObj.common.getQuery(aapObj);
				// 	const new_hash = `projectsAap.context.${query.context}.formid.${query.formid}${document.location.href.indexOf('?') > -1 ?
				// 	'?' + document.location.href.split('?')[1] : ''}`;
				// 	const newHash = aapObj.common.buildUrlQuery(document.location.href, {
				// 		aapview : sessionStorage && sessionStorage.aapview && notEmpty(sessionStorage.aapview) ? sessionStorage.aapview : "action",
				// 		"projectId" : projectId
				// 	}, aapObj.common.getQueryStringObject())
				// 	const title = aapObj.pageDetail["#" + aapObj.page + "Aap"].subdomainName;
				// 	history.pushState({}, null, newHash);
				// 	// document.title = title;
				// 	aapObj.directory.reloadAapProjectItem(aapObj, projectId);
				// })
			}, null, null);
		},
		switchFilter: function () {
			if ($(window).width() < 768) {
				// $(".menu-verticale.searchObjCSS").detach().appendTo("#menuTopLeft");
				// $("#filterContainerInside").hide();
				// $("#filterContainerAapOceco").hide()
				$.each(aapObj.common.range(1, 12), function (__, __col) {
					$('.filter-container').removeClass(`col-xs-${__col} col-md-${__col}`);
				});
				if (aapObj.common.canShowMap(aapObj)) {
					$(".filter-container").addClass('col-md-6');
				} else {
					$(".filter-container").addClass('col-md-12');
				}
			} else {
				$.each(aapObj.common.range(1, 12), function (__, __col) {
					$('.filter-container').removeClass(`col-xs-${__col} col-md-${__col}`);
				});
				if (aapObj.common.canShowMap(aapObj)) {
					$(".filter-container").addClass('col-md-6');
				} else {
					$(".filter-container").addClass('col-md-12');
				}
				// $(".menu-verticale.searchObjCSS").detach().appendTo("#filterContainerAapOceco");
				// $("#filterContainerInside").show()
			}
		},
		switchList: function () {
			if ($(window).width() < 768) {
				$(".propsDetailPanel").removeClass("col-xs-8 col-sm-8 col-xs-10 col-sm-10")
				$(".propsDetailPanel").removeClass("col-xs-8 col-sm-8 col")
				$("#show-list-xs i").removeClass("fa-times fa-sliders").addClass("fa-sliders")
				$("#detailFilterCount").detach().appendTo(".filterXs #filterXsTab");
				$(".aap-results-container").detach().appendTo("#proposition-menu-list");
				$("#propsFilterTab").hide();
				$("#filterContainerInside").show()
			} else {
				$(".propsDetailPanel").addClass("col-xs-10 col-sm-10")
				$(".openPropsList i").removeClass("fa-sliders fa-times").addClass("fa-sliders")
				$(".allPropsPanel").show();
				$(".allFilterPanel").hide();
				$("#detailFilterCount").detach().appendTo(".allFilterPanel i.showHide-filters-xs");
				if ($(".allFilterPanel i.showHide-filters-xs .filter-xs-count").length > 1) {
					$(".allFilterPanel .showHide-filters-xs").find(".filter-xs-count:last").remove()
				}
				$(".aap-results-container").detach().appendTo("#listPropsPanel");
				$("#dropdown_search_detail").show();
			}
			$('#show-list-xs,.btnCloseMenuList i').off().on('click', function () {
				// if ($(".filterXs .menu-filters-xs").is(":visible")) {
				//     $(".filterXs #show-filters-xs").find("i").removeClass("fa-times").addClass("fa-filter");
				//     $(".filterXs .menu-filters-xs").hide()
				// }
				if ($("#propsFilterTab").is(":visible")) {
					$("#show-list-xs").find("i").removeClass("fa-times").addClass("fa-sliders");
					$(".btnCloseMenuList").hide();
				} else {
					$(".btnCloseMenuList").show();
					$("#show-list-xs").find("i").removeClass("fa-sliders").addClass("fa-times");
				}
				$("#propsFilterTab").slideToggle(500);
			});
		},
		ucfirst: function (string = "vide") {
			return string.charAt(0).toUpperCase() + string.slice(1)
		},
		viewedProposal: function (aapObj, answerId) {
			var objToSend = {
				id: answerId, collection: "answers", value: "now", setType: "isoDate", path: "views." + userId + ".date"
			};
			if (userId)
				ajaxPost("", baseUrl + "/co2/aap/commonaap/action/update_seen_status", objToSend, function (data) { }, function (error) {
					mylog.log("ajaxPost error", error)
				}, "json")
			// dataHelper.path2Value(objToSend, function (params) {
			// })
		},
		viewedProject: function (aapObj, projectId) {
			if (typeof userId != "undefined") {
				var objToSend = {
					id: projectId, collection: "projects", value: "now", setType: "isoDate", path: "views." + userId + ".date"
				};
				if (userId)
					ajaxPost("", baseUrl + "/co2/aap/commonaap/action/update_seen_status", objToSend, function (data) { }, function (error) {
						mylog.log("ajaxPost error", error)
					}, "json")
				// dataHelper.path2Value(objToSend, function (params) {
				// })
			}
		},
		range: function (__start, __end) {
			const output = [];
			const alphabets = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'];
			if (typeof __start === 'number' && typeof __end === 'number' && !isNaN(__start) && !isNaN(__end)) {
				for (let i = __start; i <= __end; i++) output.push(i);
			} else if (typeof __start === 'string' && typeof __end === 'string' && __start.length === 1 && __end.length === 1) {
				let first_index_of = alphabets.findIndex(function (__alphabet) {
					return __alphabet === __start
				});
				let last_index_of = alphabets.findIndex(function (__alphabet) {
					return __alphabet.toLowerCase() === __end || __alphabet.toUpperCase() === __end
				});
				if (first_index_of >= 0) {
					if (last_index_of >= first_index_of) {
						for (let i = first_index_of; i <= last_index_of; i++) output.push(alphabets[i]);
					} else {
						output.push(alphabets[first_index_of]);
					}
				} else {
					first_index_of = alphabets.findIndex(function (__alphabet) {
						return __alphabet.toUpperCase() === __start
					});
					if (first_index_of >= 0) {
						if (last_index_of >= first_index_of) {
							for (let i = first_index_of; i <= last_index_of; i++) output.push(alphabets[i].toUpperCase());
						} else {
							output.push(alphabets[first_index_of].toUpperCase());
						}
					}
				}
			}
			return output;
		},
		loadAapOrganismPanel() {
			smallMenu.openAjaxHTML(baseUrl + '/co2/aap/getviewbypath/path/co2.views.aap.partials.organismChooser');
		},
		getCurrentOrganism(aapObj) {
			var p = aapObj.common.getQuery(aapObj);
			var slg = exists(p.context) ? p.context : (notEmpty(costum) ? costum.contextSlug : contextData.slug)
			ajaxPost(null, baseUrl + "/" + moduleId + "/search/globalautocomplete", {
				searchType: ["organizations", "projects"], filters: {
					slug: slg
				}, fields: ["name", "oceco"], notSourceKey: true,
			}, function (res) {
				if (notEmpty(res.results)) {
					$.each(res.results, function (k, v) {
						$('.aap-organism-chooser span.ellipsis-2').text(v.name);
						$('.aap-organism-chooser').addClass("active-subOrga");
						if (!v?.oceco?.subOrganization) {
							$('.aap-organism-chooser span .fa-chevron-down').remove();
							$('.aap-organism-chooser').css('cursor', "default").removeClass("active-subOrga");
						}
					})
				}
			}, null, null, { async: false })
		},
		filterItems: function (inputFilter, directToServer, parentSelector = "", itemToFilterSelector = "", domMainFilterSetelctor = "#filterContainerD") {
			if (inputFilter instanceof jQuery) {
				if (directToServer) {
					$(domMainFilterSetelctor + " .to-set-value input.main-search-bar").val(inputFilter.val()).trigger("keyup")
				} else {
					var foundedItem = 0;
					$(parentSelector + " " + ".divEndOfresults").hide();
					$(parentSelector).find(itemToFilterSelector).each(function () {
						if ($(this).text().toLocaleLowerCase().indexOf(inputFilter.val().toLocaleLowerCase()) > -1) {
							$(this).removeClass("d-none")
							foundedItem++;
						} else {
							$(this).removeClass("d-none").addClass("d-none")
						}
					});
					$(parentSelector + " .processingLoader").remove();
					if (foundedItem == 0 && inputFilter.val().length > 0) {
						$(domMainFilterSetelctor + " .to-set-value input.main-search-bar").val(inputFilter.val()).trigger("keyup")
					}
					if (inputFilter.val().length == 0) {
						$(domMainFilterSetelctor + " .to-set-value input.main-search-bar").val(inputFilter.val()).trigger("keyup")
					}
				}
			}
		},
		changeImageLoader: function (aapObj) {
			$('#firstLoader .center-body-loader .loader-spanne .loader-spanne-img').html(`
                <img src="${typeof costum != "undefined" && costum.logo ? costum.logo : aapObj.context.profilImageUrl}">
            `)
		},
		changeLogo: function (aapObj) {
			$('#menuTopLeft .image-menu').attr('src', (typeof costum != "undefined" && costum.logo ? costum.logo : aapObj.context.profilImageUrl));
		},
		switchProposalToProjectDetail: function (aapObj, queryParams) {
			var propositionDetailProjectDom = $("#proposition-detailProject");
			var project_link_dom = $('[name="associate-project"]');
			var generateProjectDom = $("#proposal-generate-project");
			project_link_dom.parent().show();
			if (!notEmpty(queryParams?.projectId)) {
				propositionDetailProjectDom.hide();
				generateProjectDom.show().off().on('click', function () {
					var self = $(this);
					if (self.prop('disabled'))
						return (true);
					self.prop('disabled', true);
					if (self.html('<i class="fa fa-spinner fa-2 fa-spin" aria-hidden="true"></i>')) {
						if (!project_link_dom.val())
							aapObj.common.generateProject(aapObj, queryParams?.answerId, function (data) {
								self.prop('disabled', false);
								if (notEmpty(data.projet)) {
									urlCtrl.loadByHash(aapObj.common.buildUrlQuery('#detailProjectAap', {
										context: aapObj.context.slug,
										formid: aapObj.form._id.$id,
										answerId: queryParams.answerId ? queryParams.answerId : '',
										projectId: data.projet
									}, aapObj.common.getQueryStringObject()));
								}
							});
						else {
							var path2value,
								aap,
								answer_id;

							aap = new aapv2();
							answer_id = aap.url().dot("answerId");
							aap.answer(answer_id);
							path2value = {
								id: answer_id,
								collection: "answers",
								path: "project",
								value: {
									id: project_link_dom.val(),
									startDate: moment().format("DD/MM/YYYY")
								}
							};
							dataHelper.path2Value(path2value, function () {
								aap.xhr().create_actions_from_expenditure().then(function (resp) {
									self.prop('disabled', false);
									urlCtrl.loadByHash(aap.url().hash("#detailProjectAap").dot({
										context: aapObj.context.slug,
										formid: aapObj.form._id.$id,
										answerId: answer_id,
										projectId: project_link_dom.val(),
										aapview: "action"
									}).toString());
								});
							});
						}
					}
				});
				project_link_dom.off("change").on("change", function () {
					var self;

					self = $(this);
					if (!self.val())
						generateProjectDom.text("Générer un projet");
					else
						generateProjectDom.text("Associer le projet");
				});
			} else {
				generateProjectDom.hide();
				project_link_dom.parent().hide();
				propositionDetailProjectDom.show().off().on('click', function () {
					if (!$(this).is('[disabled=disabled]')) {
						var new_hash = `#detailProjectAap.context.${aapObj.context.slug}.formid.${aapObj.form._id.$id}.answerId.${queryParams?.answerId}.projectId.${queryParams?.projectId}`;
						urlCtrl.loadByHash(new_hash);
					}
				});
			}
		},
		switchProjectToProposalDetail: function (aapObj, queryParams) {
			if (!notEmpty(queryParams?.answerId)) {
				$("#project-detailProposition").attr("disabled", "disabled");
			} else {
				$("#project-detailProposition").removeAttr("disabled", "disabled");
				$("#project-detailProposition").off().on('click', function () {
					if (!$(this).is('[disabled=disabled]')) {
						var new_hash = `#detailProposalAap.context.${aapObj.context.slug}.formid.${aapObj.form._id.$id}.answerId.${queryParams?.answerId}.projectId.${queryParams.projectId}`;
						urlCtrl.loadByHash(new_hash);
					}
				})
			}
		},
		openProposalDiscussionSpace: function (answerId) {
			dialogContent.open({
				modalContentClass: "modal-custom-lg",
				isRemoteContent: true,
				backdropClick: true,
				shownCallback: () => {
					$("#dialogContent .modal-content .modal-body#dialogContentBody").next().hide()
				},
				hiddenCallback: () => {
					$("#dialogContent #dialogContentBody").empty();
					$("#dialogContent > .modal-content").removeClass("col-xs-12 bs-px-0");
					$("#dialogContent #dialogContentBody").removeClass("col-lg-12 bs-py-0")
					$("#dialogContent .modal-content .modal-body#dialogContentBody").next().show();
					if (
						discussionComments.alreadyNotifyDiscussionMember && typeof discussionSocket != "undefined" && typeof discussionSocket.emitEvent != "undefined" &&
						userId && typeof userConnected != "undefined" && userConnected.name
					) {
						discussionSocket.emitEvent(
							wsCO,
							"change_user_activity", {
								userId: userId,
								onUpdate: "userWriting",
								commentId: answerId,
								userActivity: "stopWriting",
							}
						);
						discussionComments.alreadyNotifyDiscussionMember = false;
					}
					aapObj.directory.reloadAapMiniItem(aapObj, answerId)
				}
			})
			if (typeof aapv2 == "undefined") {
				lazyLoad(modules.co2.url + "/js/aap/aapv2.js");
			}
			var aap = new aapv2();
			aap.xhr()
				.get_discussion_space(answerId)
				.then(function(res) {
					$("#dialogContent #dialogContentBody").empty().html(res);
					$("#dialogContent > .modal-content").addClass("col-xs-12 bs-px-0");
					$("#dialogContent #dialogContentBody").addClass("col-lg-12 bs-py-0");
				})
				.catch(function(err) {
					if (err.code === 102) {
						toastr.error(err.msg ? err.msg : "Une erreuur c'est produit");
					}
				});
		},
		getActivateFilters: function (aapObj, filtersContainer, activateFiltersSelector) {
			let activateFilters = {};
			$(`${filtersContainer} ${activateFiltersSelector}`).each((k, item) => {
				let activeFilterObj = {
					key: "kunik"
				}
				if (typeof $(item).attr("data-key") != "undefined") {
					activeFilterObj.key = $(item).attr("data-key")
				}
				if (typeof $(item).attr("data-value") != "undefined") {
					activeFilterObj.value = $(item).attr("data-value")
				}
				if (typeof $(item).attr("data-type") != "undefined") {
					activeFilterObj.type = $(item).attr("data-type")
				}
				if (typeof $(item).attr("data-field") != "undefined") {
					activeFilterObj.field = $(item).attr("data-field")
				}
				if (activeFilterObj.type && activeFilterObj.type != "filters") {
					activeFilterObj.likeFilters = true
				}
				activateFilters[k + activeFilterObj.key] = activeFilterObj
			});
			return activateFilters
		},
		generateProject: function (aapObj, answerId, callback) {
			ajaxPost("", baseUrl + '/survey/form/generateproject/answerId/' + answerId + '/parentId/' + aapObj.context._id.$id + '/parentType/' + aapObj.context.collection, null, function (data) {
				if (notEmpty(data?.projet)) {
					var today = new Date();
					var tplCtx = {
						id: answerId,
						collection: "answers",
						path: "project",
						value: {
							"id": data.projet,
							"startDate": moment(today).format('DD/MM/YYYY')
						},
					}
					dataHelper.path2Value(tplCtx, function (params) {
						if (params && params.elt && params.elt.status && params.elt.status.indexOf("projectstate") < 0)
							aapObj.directory.updateStatus(aapObj, "add", {
								id: answerId, value: "projectstate",
							});
						params?.elt?.created ? params.elt.created = (new Date().getTime() / 1000).toFixed() : "";
						ajaxPost(null, typeof coWsConfig != "undefined" && coWsConfig.pingRefreshViewUrl ? coWsConfig.pingRefreshViewUrl : null, {
							action: "newProjectGenerated",
							answerId: answerId,
							userSocketId: aapObj?.userSocketId,
							responses: { ...params?.elt, ...{ nameUser: userConnected?.name ? userConnected?.name : '(No name)' } },
						}, null, null, { contentType: 'application/json' });
						if (typeof callback == "function") {
							callback(data);
						}
					});
				}
			}, "html");
		},
		session: {
			hasYearField: function (aapObj) {
				var hasYear = false;
				$.each(aapObj.inputs, function (k, v) {
					if (exists(v.inputs)) {
						$.each(v.inputs, function (ki, vi) {
							if (ki == "year") {
								hasYear = true;
								return false
							}

						})
					}
				})
				return hasYear;
			}, initYear: function (aapObj) {
				/*htmlExists('.aap-year-chooser select',function(sel){
				 if (location.hash.indexOf('context') != -1 && location.hash.indexOf('formid') != -1 && notNull(aapObj.aapSession)) {
				 var hasYear = aapObj.common.session.hasYearField(aapObj);
				 if (hasYear) {
				 sel.show();
				 sel.val(aapObj.aapSession);
				 aapObj.common.session.switchSession(aapObj);
				 }
				 } else {
				 $('.aap-year-chooser select').hide();
				 }
				 })*/

				htmlExists('.cosDyn-app .tools-dropdown.sessionAap .drop-down-sub-menu .sous-menu-full-width', function (sel) {
					if (location.hash.indexOf('context') != -1 && location.hash.indexOf('formid') != -1 && notNull(aapObj.aapSession)) {
						var hasYear = aapObj.common.session.hasYearField(aapObj);
						if (hasYear) {
							$('.cosDyn-app .tools-dropdown.sessionAap').show();
							$('.dropdown-app-full-width.sessionAap .dropdown-toggle .label-menu').text("Session " + aapObj.aapSession);
							var date = new Date();
							var startYear = date.getFullYear() - 2;
							var endDate = date.getFullYear();
							var menuYear = ''
							for (let index = startYear; index <= endDate; index++) {
								var activeclass = (index == aapObj.aapSession ? "active" : "");
								menuYear += `<a href="javascript:;" class="aap-year-chooser-2 ${activeclass}" data-value="${index}">${index}</a>`;
							}
							$('.cosDyn-xsMenu.sessionAap .drop-down-sub-menu').html(`
                                <div class="drop-down-sub-menu-content">
                                    <a href="#projectsAap.context.coSindniSmarterre.formid.620a5e534f94bf26f34c5be8.aapview.detailed" class="menu-app hidden-xs lbh-menu-app  cosDyn-xsMenu">
                                        ${menuYear}
                                    </a>
                                </div>
                            `)
							sel.html(menuYear);
							aapObj.common.session.switchSession(aapObj);
						}/*else
						 $('.tools-dropdown.sessionAap').hide();*/
					} else {
						$('.cosDyn-app .tools-dropdown.sessionAap').hide();
					}
				})
			}, switchSession: function (aapObj) {
				var sws = function (year) {
					var q = aapObj.common.getQuery();
					if (exists(q.formid)) {
						var url = baseUrl + '/co2/aap/changeaapsession';
						var post = {
							year: year, formid: q.formid
						};
						ajaxPost(null, url, post, function (data) {
							location.reload();
						});
					}
				}
				/*$('.aap-year-chooser select').off().on('change', function () {
				 sws($(this).val());
				 })*/
				$('.aap-year-chooser-2').off().on('click', function () {
					sws($(this).data("value"));
				})

			}
		},
		authorizationAction: function (aapObj, options) {
			if (typeof options === 'object') {
				if (typeof options.answer === 'string') {
					var url = baseUrl + '/co2/aap/proposition/request/get_authorization';
					var post = {
						answer: options.answer
					};
					return new Promise(function (resolve) {
						ajaxPost(null, url, post, resolve);
					})
				}
			} else {
				return new Promise(function (resolve) {
					resolve({
						canEdit: false,
						canRead: false
					});
				})
			}
		}
	},
	dynform: {
		configNotification: function (aapObj) {
			var paramsNotif = {
				"jsonSchema": {
					"title": trad["Manage notifications"], "icon": "fa-cog", "properties": {
						newproposition: {
							"inputType": "checkboxSimple", "label": trad.newaction, "params": checkboxSimpleParams
						}, deleteproposition: {
							"inputType": "checkboxSimple", "label": trad["Deleting a proposal"], "params": checkboxSimpleParams
						}, "newstatus": {
							"inputType": "checkboxSimple", "label": trad["Status change"], "params": checkboxSimpleParams
						}, "newdepense": {
							"inputType": "checkboxSimple", "label": trad["New expense"], "params": checkboxSimpleParams
						}, "deletedepense": {
							"inputType": "checkboxSimple", "label": trad["Deletion of expenses"], "params": checkboxSimpleParams
						}, "newfinancement": {
							"inputType": "checkboxSimple", "label": trad["New financing"], "params": checkboxSimpleParams
						}, "deletefinancement": {
							"inputType": "checkboxSimple", "label": trad["Deletion of financing"], "params": checkboxSimpleParams
						}, "newpayement": {
							"inputType": "checkboxSimple", "label": trad["New payment"], "params": checkboxSimpleParams
						}, "deletepayement": {
							"inputType": "checkboxSimple", "label": trad["Deletion of payment"], "params": checkboxSimpleParams
						}, "newtask": {
							"inputType": "checkboxSimple", "label": trad["New task"], "params": checkboxSimpleParams
						}, "deletetask": {
							"inputType": "checkboxSimple", "label": trad["Deleting a task"], "params": checkboxSimpleParams
						}, "checktask": {
							"inputType": "checkboxSimple", "label": trad["Closed a task"], "params": checkboxSimpleParams
						}, "addperson": {
							"inputType": "checkboxSimple", "label": trad["Add a contributor"], "params": checkboxSimpleParams
						}, "rmperson": {
							"inputType": "checkboxSimple", "label": trad["Deleting a contributor"], "params": checkboxSimpleParams
						},
					}, onLoads: {
						onload: function () {
							alignInput2(paramsNotif.jsonSchema.properties, "proposition", 6, 6, null, null, "Contributeur", "#3f4e58", "");
						}
					}, save: function (data) {
						tplCtx = {};
						tplCtx.id = aapObj.form["_id"]["$id"];
						tplCtx.collection = "forms";
						tplCtx.path = "paramsNotification";
						tplCtx.value = {};
						$.each(paramsNotif.jsonSchema.properties, function (k, val) {
							tplCtx.value[k] = $("#" + k).val();
						});

						if (typeof tplCtx.value == "undefined") {
							toastr.error('value cannot be empty!');
						} else {
							dataHelper.path2Value(tplCtx, function (params) {
								toastr.success(trad["Notification settings saved"]);
								dyFObj.closeForm();
								urlCtrl.loadByHash(location.hash);
							});
						}

					}
				}
			};
			return paramsNotif;
		}, createProject: function (aapObj) {
			const data = {
				parent: {
					[aapObj.context._id.$id]: {
						type: aapObj.context.collection, name: aapObj.context.name
					}
				}
			};
			const dynform_costum_in = {
				beforeBuild: {
					properties: {
						parent: {
							type: 'finder', value: {
								[aapObj.context._id.$id]: {
									type: aapObj.context.collection, name: aapObj.context.name
								}
							}
						}
					}
				}, onload: {
					actions: {
						hide: {
							parentfinder: 1
						}
					}
				}, afterSave: function (__data) {
					document.location.reload();
				}
			};
			const option = {
				type: 'bootbox'
			};
			typeObj.project.color = 'green';
			dyFObj.openForm('project', null, null, null, dynform_costum_in, option);
		}
	},
	prepareData: {
		proposal: function (aapObj, params, data, plus) {
			var db_answer_step1 = params?.answers && params?.answers?.aapStep1 ? params.answers.aapStep1 : {};
			var db_answer_step2 = params?.answers && params?.answers?.aapStep2 ? params.answers.aapStep2 : {};
			var contextId = params.context ? Object.keys(params.context)[0] : null;

			var answer = {
				random: generateRandomString(6),
				id: params._id && params._id.$id ? params._id.$id : null,
				title: db_answer_step1.titre ? ucfirst(db_answer_step1.titre) : null,
				description: db_answer_step1.description ? db_answer_step1.description : null,
				parent: {
					id: contextId,
					name: data?.elements?.[contextId]?.name,
					slug: data?.elements?.[contextId]?.slug,
					collection: data?.elements?.[contextId]?.collection,
				},
				user: {
					id: params.user,
					name: data?.users?.[params.user]?.name,
					slug: data?.users?.[params.user]?.slug,
					collection: data?.users?.[params.user]?.collection,
					profilImageUrl: data?.users?.[params.user]?.profilImageUrl,
				},
				created: params.created,
				updated: params.updated,
				status: params?.status,
				urgence: db_answer_step1?.urgency ? db_answer_step1?.urgency : false,
				image: defaultImage,
				contributors: [],
				actions: [],
				statusInfo: params?.statusInfo,
				usersStatus: data?.usersStatus,
				commentCount: params?.commentCount ? params?.commentCount : null,
				vote: params?.vote?.[userId]?.status && params?.vote?.[userId]?.status == "love",
				voteCount: params?.vote ? Object.keys(params?.vote).length : "",
				views: params?.views ? params?.views : null,
				tags: db_answer_step1 ? db_answer_step1?.tags : null,
				selection: db_answer_step2?.selection ? db_answer_step2?.selection : null,
				allVotes: db_answer_step2?.allVotes ? Number(db_answer_step2?.allVotes).toFixed(1) : 0,
				depense: db_answer_step1.depense ? db_answer_step1.depense : null,
				budget: db_answer_step1.budget ? db_answer_step1.budget : null,
				financements: {
					depense: 0, financed: 0
				},
				evaluations: {
					rate: db_answer_step2?.allVotes ? db_answer_step2?.allVotes : 0,
					voters: db_answer_step2?.selection ? Object.keys(db_answer_step2?.selection).length : 0
				},
				requiredInputsAnswer: {},
				notSeenComments: 0,
				projectId: params?.project?.id ? params?.project?.id : ""
			};
			if (notEmpty(data.users) && params.links && params.links.contributors) {
				const contributors = Object.keys(params.links.contributors);
				for (const key in data.users) {
					const user = data.users[key];
					if (!contributors.includes(key)) {
						continue;
					}
					answer.contributors.push({
						id: user._id.$id, name: user.name, image: user.profilImageUrl ? `${baseUrl}/${user.profilImageUrl}` : ''
					});
				}
			}
			if (notEmpty(data.allImages)) {
				$.each(data.allImages, function (__image_id, __image) {
					typeof __image.docPath == "undefined" ? __image.docPath = `/upload/${__image?.moduleId}/${__image?.folder}/${__image?.name}` : "";
					if (answer.image === defaultImage && answer.id === __image.id) {
						answer.image = __image.docPath;
					}
				});
			}
			if (notEmpty(data.allActions) && exists(params?.project?.id)) {
				$.each(data.allActions, function (idAct, vAct) {
					if (vAct.parentId == params.project.id) {
						answer.actions.push(vAct);
					}
				})
			}
			if (notEmpty(answer?.selection) && notEmpty(answer?.allVotes)) {
				if (answer.allVotes > 5) {
					answer.allVotes = answer.allVotes / Object.keys(answer?.selection).length;
				}
				answer.allVotes = Number(answer.allVotes).toFixed(1)
			}
			if (data?.allNotSeenComments?.[answer.id]) {
				answer.notSeenComments = data.allNotSeenComments[answer.id]
			}
			if (aapObj.requiredInputs && Object.keys(aapObj.requiredInputs).length > 0) {
				for ([stepKey, stepVal] of Object.entries(aapObj.requiredInputs)) {
					const formKey = stepVal.step ? stepVal.step : stepKey;
					if (stepVal.inputs && Object.keys(stepVal.inputs).length > 0) {
						for ([inputKey, inputVal] of Object.entries(stepVal.inputs)) {
							if (inputVal.isRequired && inputVal.isRequired == true && params?.answers?.[formKey]?.[inputKey]) {
								if (!notEmpty(params?.answers?.[formKey]?.[inputKey])) {
									answer.requiredInputsAnswer[formKey + '-' + inputKey] = "empty"
								}
							} else {
								answer.requiredInputsAnswer[formKey + '-' + inputKey] = params.answers
							}
						}
					}
				}
			}
			if (plus) {
				answer.aapStep1 = db_answer_step1;
			}
			if (!aapObj.form?.subType || !aapObj.form?.type || (aapObj.form?.subType && aapObj.form?.subType != "aap") || (aapObj.form?.type && aapObj.form?.type != "aap")) {
				answer.answers = params?.answers
			}
			let depenseObj = {
				depense: 0, financed: 0
			}
			if (db_answer_step1.depense) {
				const depenseLength = Object.keys(db_answer_step1.depense).length;
				for ([depenseIndex, depenseValue] of Object.entries(db_answer_step1.depense)) {
					if (notEmpty(depenseValue) && depenseValue?.price) {
						depenseObj.depense += depenseValue?.price * 1
					}
					if (notEmpty(depenseValue) && depenseValue?.financer && Array.isArray(depenseValue?.financer)) {
						depenseValue.financer.forEach(finValue => {
							if (finValue?.amount) {
								depenseObj.financed += finValue?.amount * 1
							}
						})
					}
					if (depenseIndex == depenseLength - 1) {
						answer.financements = { ...depenseObj }
					}
				}
			}
			return answer;
		}, project: function (aapObj, params, data) {
			const this_id = params._id.$id;
			const parent_keys = Object.keys(params.parent);
			var answer = null;
			if (notEmpty(data.data.answer)) {
				$.each(data.data.answer, function (answerId, oneAnswer) {
					// if(notEmpty(oneAnswer.project[this_id])) answer = oneAnswer
					if (oneAnswer?.project?.id && oneAnswer?.project?.id == this_id) {
						answer = oneAnswer;
					}
				});
			}
			const project = {
				id: this_id,
				title: params.name,
				image: params.profilImageUrl ? params.profilImageUrl : defaultImage,
				parent: {
					name: params.parent[parent_keys[0]].name ? params.parent[parent_keys[0]].name : data.data.organizations[parent_keys]?.name,
					type: params.parent[parent_keys[0]].type,
					id: parent_keys[0]
				},
				creator: {
					name: data.data.users[params.creator].name,
					type: 'citoyens',
					id: params.creator
				},
				date: moment.unix(params.created),
				task_status: {
					total: data.data.additional_data[this_id].total_task,
					done: data.data.additional_data[this_id].task_done
				},
				is_urgent: data.data.urgency,
				contributors: [],
				avancement: data.results?.[this_id]?.properties?.avancement && data.results?.[this_id]?.properties?.avancement != "" ? data.results?.[this_id]?.properties?.avancement : "In progress",
				answer,
				action_count: params.action_count
			};
			if (notEmpty(data.data.users) && params.links && params.links.contributors) {
				const contributors = Object.keys(params.links.contributors);
				for (const key in data.data.users) {
					const user = data.data.users[key];
					if (!contributors.includes(key)) {
						continue;
					}
					project.contributors.push({
						id: user._id.$id, name: user.name, image: user.profilImageUrl ? `${baseUrl}/${user.profilImageUrl}` : ''
					});
				}
			}
			project.is_done = project.task_status.total === project.task_status.done;
			return (project);
		}, proposalDropdown(aapObj, params) {
			var daraId = `data-id=${"'" + params.id + "'"}`
			var dropD = {
				/*detail   : {
				 label : "Détail",
				 href : "javascript:;",
				 icon : "file",
				 class : "detail-proposal",
				 dataId : daraId
				 },
				 toProject : {
				 label : "Mettre en projet",
				 href : "javascript:;",
				 icon : "refresh",
				 class : "to-project-proposal",
				 dataId : daraId,
				 rules : {
				 admin : true,
				 }
				 },
				 action : {
				 label : "Action",
				 href : "javascript:;",
				 icon : "pencil-square-o",
				 class : "action-proposal",
				 dataId : daraId,
				 rules : {
				 admin : true,
				 }
				 },
				 selection : {
				 label : "Sélection",
				 href : "javascript:;",
				 icon : "gavel",
				 class : "selection-proposal",
				 dataId : daraId,
				 rules : {
				 admin : true,
				 }
				 },*/
				pdf: {
					label: "Pdf", href: "javascript:;", icon: "file", class: "pdf-proposal", dataId: daraId, rules: {
						or: {
							admin: true, roles: ["Evaluateur", "Financeur"],
						}
					}
				}, overview: {
					label: trad.Overview, href: "javascript:;", icon: "info", class: "overview-proposal", dataId: daraId, rules: {
						or: {
							admin: true, roles: ["Evaluateur", "Financeur"],
						}
					}
				}, form: {
					label: trad.EditProposal, href: "javascript:;", icon: "file-text", class: "form-proposal", dataId: daraId, rules: {
						or: {
							admin: true, roles: ["Evaluateur", "Financeur"],
						}
					}, user: params.user
				}, delete: {
					label: trad.delete,
					href: "javascript:;",
					icon: "times",
					class: "delete-proposal",
					dataId: daraId,
					['data-title']: "data-title='" + (notEmpty(params.title) ? params.title : "") + "'",
					['data-project-id']: "data-project-id='" + (params.projectId ? params.projectId : "") + "'",
					rules: {
						admin: true,
					}
				}
				/*, financerstandalone : {
					label : "lien fiche financement", href : "javascript:;", icon : "info", class : "copylinkfinancer", dataId : daraId, rules : {

					}
				}*/
				//add here the dropdown button
			}
			return dropD;
		}, projectDropdown(aapObj, params) {
			var daraId = `data-id=${"'" + params.id + "'"}`;
			var dropD = {
				overview: {
					label: trad.Overview,
					href: "javascript:;",
					icon: "info",
					class: "overview-project",
					dataId: daraId,
					dataAnswer: params?.answer ?? null
				},
			}
			if (aapObj && aapObj.canEdit == true) {
				dropD.delete = {
					label: trad.delete,
					href: "javascript:;",
					icon: "times",
					class: "delete-project",
					dataId: daraId,
					['data-title']: "data-title='" + (notEmpty(params.title) ? params.title : "") + "'",
					["data-answer-id"]: "data-answer-id='" + (params.answer && params.answer._id && params.answer._id.$id ? params.answer._id.$id : "") + "'",
					rules: {
						admin: true,
					}
				}
			}
			return dropD;
		}, menuRightDropdown: function (aapObj) {
			var slug = aapObj && aapObj.context && aapObj.context.slug ? aapObj.context.slug : costum.slug,
				form = aapObj && aapObj.form ? aapObj.form._id.$id : null,
				collection = aapObj && aapObj.context ? aapObj.context.collection : costum.contextType,
				context_id = aapObj && aapObj.context ? aapObj.context._id.$id : costum.contextId;

			var params = {
				standaloneLink: {
					label: trad["Reply form"], href: `javascript:;`, icon: "link", class: "copylinkstandalone",
				}, elementHome: {
					label: `${trad["Go to"]} ${slug}`,
					href: `${document.location.origin}/#@${slug}`,
					target: `_blank`,
					icon: "home",
					class: "aap-goto-element",
				}, community: {
					label: trad["Manage the community"],
					href: `#communityAap.context.${slug}.formid.${form}`,
					icon: "users",
					class: "lbh",
					rules: {
						or: {
							admin: true, roles: ["Evaluateur", "Financeur"],
						}
					}
				}, inviteMembers: {
					label: tradDynForm["Invite members"],
					href: `#element.invite.type.${collection}.id.${context_id}`,
					icon: "user-plus",
					class: "lbhp invite-member",
					rules: {
						admin: true,
					}
				}, manageNotification: {
					label: trad["Manage notifications"], href: `javascript:;`, icon: "bell", class: "manage-notification", rules: {
						admin: true,
					}
				}, communicationTools: {
					label: trad["Communication tools"], href: `javascript:;`, icon: "at", class: "communication-tools", rules: {
						pages: ["#proposalAap", "#myproposalsAap"],
						roles: aapObj.form?.params?.communicationToolsRoles ? aapObj.form?.params?.communicationToolsRoles : []
					}
				}, emailList: {
					label: trad["Email lists"], href: `javascript:;`, icon: "at", class: "email-lists", rules: {
						roles: aapObj.form?.params?.communicationToolsRoles ? aapObj.form?.params?.communicationToolsRoles : []
					}
				}, importCsv: {
					label: `${trad["import"]} CSV`, href: `javascript:;`, icon: "upload", class: "import-csv", rules: {
						or: {
							admin: true, roles: ["Evaluateur", "Financeur"],
						}
					}
				}, exportCsv: {
					label: `${trad["Export as"]} CSV`, href: `javascript:;`, icon: "file-excel-o", class: "export-csv", rules: {
						or: {
							admin: true, roles: ["Evaluateur", "Financeur"],
						}, pages: ["#proposalAap", "#myproposalsAap"],
					}
				}, exportCsvFinancing: {
					label: `${trad["Export as"]} CSV (${trad.financing})`,
					href: `javascript:;`,
					icon: "file-excel-o",
					class: "btn-download-csv-financer",
					rules: {
						or: {
							admin: true, roles: ["Evaluateur", "Financeur"],
						}, pages: ["#proposalAap", "#myproposalsAap"],
					}
				}, duplicateProposal: {
					label: `${trad["Duplicate"]}  ${trad.proposal}`,
					href: `javascript:;`,
					icon: "copy",
					class: "duplicate-prop-tools",
					rules: {
						admin: true, pages: ["#proposalAap", "#myproposalsAap"],
					}
				}
			}
			return params;
		}
	},
	bellNotification: {
		proposal: {
			updateStatus: function (answerId) {
				ajaxPost("", baseUrl + "/co2/aap/commonaap/action/notifyUpdateStatus", { answerId: answerId }, function (data) {

				}, function (error) {
					mylog.log("ajaxPost error", error)
				}, "json")
			}
		}
	}
}
//call this functions when we are outside AAP page

aapObj.events.common(aapObj);
aapObj.events.proposal(aapObj);
aapObj.events.projects(aapObj);
aapObj.common.getCurrentOrganism(aapObj);
aapObj.common.session.initYear(aapObj);
aapObj.common.initWebSocket(coWsConfig, "socket_aap");

const formPromise = new Promise((resolve, reject) => {
	const loop = () => (typeof aapObj !== "undefined" && notNull(aapObj.form)) ? resolve(aapObj) : setTimeout(loop);
	loop();
});

formPromise.then((value) => {
	if (typeof value.form.coremu === "undefined" || value.form.coremu === "false" || value.form.coremu === false) {
		$('.cosDyn-app a[href*="contributionAap"]').remove();
	}
	if (userConnected?.roles?.superAdmin && userConnected?.roles?.superAdmin == false) {
		if (typeof value.context?.links?.members[userId]?.isAdmin == "undefined" || value.context?.links?.members[userId]?.isAdmin == false) {
			$('.cosDyn-app a[href*="configurationAap"]').remove();
		}
	} else if (userConnected?.roles?.superAdmin && userConnected?.roles?.superAdmin == true) {

	} else {
		if (typeof value.context?.links?.members[userId]?.isAdmin == "undefined" || value.context?.links?.members[userId]?.isAdmin == false) {
			$('.cosDyn-app a[href*="configurationAap"]').remove();
		}
	}
});

$(document).on('click', '.menu-hider button', function () {
	var self = $(this);
	self.toggleClass('show');
	$('.nav.nav-tabs.aap-nav-tabs, .row.aap-list-container.aap-page-info').slideToggle();
});

window.addEventListener('resize', function () {
	var hiddenCheckDom = $('.nav.nav-tabs.aap-nav-tabs, .row.aap-list-container.aap-page-info');
	if (hiddenCheckDom.length) {
		if ($(window).width() > 767) {
			hiddenCheckDom.css('display', '');
		}
	}
});

function changeListView() {
	$('.change-list-view:not(.active)').off().on('click', function () {
		var aapView = ["detailed", "minimalist", "compact", 'table'];
		var urlQuery = aapObj.common.getQuery();
		var currentHash = location.hash.split('.')[0];
		var isProposal = ['#proposalAap', '#detailProposalAap', '#myproposalsAap'].includes(currentHash)
		if (typeof aapObj?.filterSearch?.activeFilters != "undefined" && typeof aapObj.common?.getActivateFilters != "undefined") {
			aapObj.filterSearch.activeFilters = aapObj.common.getActivateFilters(aapObj, $("#filterContainerL").length > 0 ? "#filterContainerL" : "#filterContainerD", "#activeFilters .filters-activate")
			if (typeof aapObj.filterSearch.proposal?.search?.obj != "undefined" && typeof aapObj.filterSearch?.lastSearchObjProposal?.search?.obj != "undefined") {
				aapObj.filterSearch.proposal?.search?.obj ? aapObj.filterSearch.lastSearchObjProposal.search.obj = JSON.parse(JSON.stringify(aapObj.filterSearch.proposal.search.obj)) : {}
			}
			if (typeof aapObj.filterSearch.project?.search?.obj != "undefined" && typeof aapObj.filterSearch?.lastSearchObjProject?.search?.obj != "undefined") {
				aapObj.filterSearch.project?.search?.obj ? aapObj.filterSearch.lastSearchObjProject.search.obj = JSON.parse(JSON.stringify(aapObj.filterSearch.project.search.obj)) : {}
			}
		}
		var pageKey = Object.keys(aapObj.pageDetail)[0];
		if (location.hash.indexOf(".") > -1) {
			const actualCurrentHash = location.hash.substring(0, location.hash.indexOf("."))
			if (pageKey != actualCurrentHash) {
				pageKey = actualCurrentHash
			}
		}
		if (this.dataset.view === 'kanban') {
			var kanbanUrlParams = {
				context: urlQuery.context,
				formid: urlQuery.formid,
				aapview: isProposal ? 'summary' : 'action'
			};
			if (aapObj.defaultAnswer && aapObj.defaultAnswer._id)
				kanbanUrlParams.answerId = aapObj.defaultAnswer._id.$id;
			if (aapObj.defaultProject && aapObj.defaultProject._id)
				kanbanUrlParams.projectId = aapObj.defaultProject._id.$id;

			switch (pageKey) {
				case "#myproposalsAap":
					urlQuery.userId ? kanbanUrlParams.userId = urlQuery.userId : "";
					if (aapObj.myDefaultProposal && aapObj.myDefaultProposal._id && aapObj.myDefaultProposal._id.$id) {
						kanbanUrlParams.answerId = aapObj.myDefaultProposal._id.$id;
						if (aapObj.myDefaultProposal.project && aapObj.myDefaultProposal.project.id)
							kanbanUrlParams.projectId = aapObj.myDefaultProposal.project.id
						else if (kanbanUrlParams.projectId)
							delete kanbanUrlParams.projectId
					}
					break;
				case "#myprojectsAap":
					urlQuery.projectUserId ? kanbanUrlParams.projectUserId = urlQuery.projectUserId : "";
					if (aapObj.myDefaultProject && aapObj.myDefaultProject._id && aapObj.myDefaultProject._id.$id)
						kanbanUrlParams.projectId = aapObj.myDefaultProject._id.$id
					if (aapObj.myDefaultProject && notEmpty(aapObj.myDefaultProject.answer) && aapObj.myDefaultProject.answer.$id)
						kanbanUrlParams.answerId = aapObj.myDefaultProject.answer.$id
					break;

				default:
					break;
			}
			var kanbanUrl = aapObj.common.buildUrlQuery(isProposal ? '#detailProposalAap' : '#detailProjectAap', kanbanUrlParams, aapObj.common.getQueryStringObject());
			sessionStorage.setItem('aapview', 'kanban');
			history.pushState(null, null, kanbanUrl);
			urlCtrl.loadByHash(kanbanUrl);
			return true;
		}
		var view = $(this).data('view');
		var proposalUrlParams = {
			context: urlQuery.context,
			formid: urlQuery.formid,
			aapview: view
		};
		var hashNoParams = location.hash.split('?')[0];

		//var getParameter
		if (location.href.indexOf("aapview") != -1) {
			hashNoParams = hashNoParams.split(".");
			hashNoParams = removeFromArray(hashNoParams, "aapview");
			$.each(aapView, function (k, v) {
				hashNoParams = removeFromArray(hashNoParams, v);
			})
			hashNoParams = hashNoParams.join('.');
		}
		// update url
		var linkDom = $('#mainNav .lbh-menu-app[href]').filter(function () {
			var href = this.getAttribute('href');
			var pagesList = ['myprojectsAap', 'proposalAap', 'myproposalsAap', 'projectsAap'];
			return href.indexOf('context') >= 0 && pagesList.some(function (onePage) {
				return href.indexOf(onePage) >= 0;
			});
		});
		linkDom.each(function () {
			var self = $(this);
			var newHref = '';
			var currentHref = self.attr('href');
			var viewsList = ['detailed', 'minimalist', 'table'];
			var indexOfAapView = currentHref.indexOf('aapview');
			if (indexOfAapView >= 0) {
				var firstPart = currentHref.slice(0, indexOfAapView);
				var secondPart = currentHref.slice(indexOfAapView + 'aapview.'.length);
				var splited = secondPart.split('.');
				splited[0] = view;
				newHref = firstPart + 'aapview.' + splited.join('.');
			} else {
				newHref = currentHref += '.aapview.' + view;
			}
			self.attr('href', newHref);
		})

		var pageHash = isProposal ? '#proposalAap' : '#projectsAap';
		switch (pageKey) {
			case "#myproposalsAap":
				urlQuery.userId ? proposalUrlParams.userId = urlQuery.userId : "";
				pageHash = '#myproposalsAap';
				if (aapObj.myDefaultProposal && aapObj.myDefaultProposal._id) {
					proposalUrlParams.answerId = aapObj.myDefaultProposal._id.$id
				}
				break;
			case "#detailProposalAap":
				urlQuery.userId ? (proposalUrlParams.userId = urlQuery.userId, pageHash = '#myproposalsAap') : "";
				break;
			case "#myprojectsAap":
				urlQuery.projectUserId ? proposalUrlParams.projectUserId = urlQuery.projectUserId : "";
				pageHash = '#myprojectsAap';
				if (aapObj.myDefaultProject && aapObj.myDefaultProject._id && aapObj.myDefaultProject._id.$id)
					proposalUrlParams.projectId = aapObj.myDefaultProject._id.$id
				break;
			case "#detailProjectAap":
				urlQuery.projectUserId ? (proposalUrlParams.projectUserId = urlQuery.projectUserId, pageHash = '#myprojectsAap') : "";
				break;

			default:
				break;
		}

		var proposalUrl = aapObj.common.buildUrlQuery(pageHash, proposalUrlParams, aapObj.common.getQueryStringObject());
		sessionStorage.setItem('aapview', view);
		if (view == "map") {
			localStorage.setItem("showMyMap", !aapObj.showMyMap);
		} else {
			history.pushState('', null, proposalUrl);
		}

		urlCtrl.loadByHash(proposalUrl);
		$('.change-list-view').removeClass("active");
		$(this).addClass('active');
		if (aapObj.common.canShowMap(aapObj)) {
			$(`[data-view=map]`).addClass('active');
		}
	})
}

window.addEventListener('scroll', function () {
	var dropdownSubMenuDom = $('.dropdown-menu.drop-down-sub-menu').filter(function () {
		return $(this).is(':visible');
	})
	var stickTop = 0;
	if (dropdownSubMenuDom.length > 0) {
		dropdownSubMenuDom.each(function () {
			var bcr = this.getBoundingClientRect();
			stickTop = Math.max(bcr.bottom, stickTop);
		});
	} else {
		$('#mainNav').each(function () {
			var bcr = this.getBoundingClientRect();
			stickTop = Math.max(bcr.bottom, stickTop);
		});
	}
	if ($("#confignav").is(":visible"))
		$('.coformbuilder-side-content').css('top', $("#confignav").height() + 20 + stickTop);
	$('.nav.nav-tabs.aap-nav-tabs.pos-sticky').css('top', stickTop);
	$('.pos-sticky.allPropsPanel.aap-nav-panel').css('top', stickTop);
	$('.pos-sticky.allFilterPanel.aap-nav-panel').css('top', stickTop);
})


function handleFiles() {
	$(".addfromcsvdivrs").html('<span class="homestead"><i class="fa fa-spin fa-circle-o-noch"></i> ' + trad.currentlyloading + '...</span>')

	var ext = $(".inputcsvdata").val().split(".").pop();

	if (aapObj.file[0].name.split(".").pop() != "csv") {
		return false;
	}

	if (aapObj.file.length > 0) {

		var reader = new FileReader();

		reader.onload = function (e) {
			var csvval;
			if ($(".csvdialog .delimiterendlineinput").val() == "ral") {
				var csvval = csvdata = e.target.result.split("\n");
			} else if ($(".csvdialog .delimiterendlineinput").val() == "bs") {
				var csvval = csvdata = e.target.result.split("\\");
			}

			var collumnnumber = 0;
			var collummax = 0;


			var str = '<table class="table table-bordered "><tbody>';
			$.each(csvval, function (ind, val) {
				str += '<tr>';
				var thh = val.split($(".csvdialog .delimiterinput").val());
				collumnnumber = 0;
				$.each(thh, function (thhid, thhhval) {
					str += '<th>' + thhhval + '</th>';
					collumnnumber++;
				});
				if (collumnnumber > collummax) {
					collummax = collumnnumber;
				}

				str += '</tr>';
			});

			var str2 = '<tr class="inputtr">';
			var ii = 0
			while (ii < collummax) {
				str2 += '<th class="inputth" data-id="' + ii + '">';
				str2 += '   <div class="form-group">';
				str2 += '       <label> Path </label>';
				str2 += '       <input style="min-width: 150px;" class="form-control inputpath" data-id"' + ii + '">';
				str2 += '   </div>';
				str2 += '   <div class="form-group">';
				str2 += '       <label> Type </label>';
				str2 += '       <select class="form-control inputtype" data-id="' + ii + '">'
				str2 += '           <option selected value=""> Chaine de caratéres</option>';
				str2 += '           <option value="int"> Nombre entier</option>';
				str2 += '           <option value="float"> Nombre </option>';
				str2 += '           <option value="isoDate"> Date (isoDate)</option>';
				str2 += '           <option value="timestamp"> Date (timestamp)</option>';
				str2 += '           <option value="checkbox"> checkbox</option>';
				str2 += '           <option value="radiobutton"> radiobutton</option>';
				str2 += '           <option value="array"> array</option>';
				str2 += '           <option value="financement"> financement</option>';
				str2 += '       </select>';
				str2 += '   </div>';
				str2 += '   <div class="form-group inputtitlepathdiv" data-id="' + ii + '" style="display: none">';
				str2 += '       <label> titre </label>';
				str2 += '       <input style="min-width: 150px;" class="form-control inputtitlepath" data-id"' + ii + '">';
				str2 += '   </div>';
				str2 += '</th>';
				ii++
			}
			str2 += '</tr>';

			str += '</tbody></table>';

			$(".addfromcsvdivrs")
				.html(str)
				.animate({ scrollTop: 100000 });

			setTimeout(function () {
				$(".addfromcsvdivrs .inputtype").change(function (e) {
					var thiss = $(this)
					if (thiss.val() == "financement") {
						$(".addfromcsvdivrs .inputtitlepathdiv[data-id='" + thiss.data('id') + "']").show();
					} else {
						$(".addfromcsvdivrs .inputtitlepathdiv[data-id='" + thiss.data('id') + "']").hide();
					}
				});
			}, 2000);

			$(".addfromcsvdivrs table").prepend(str2);

		};
		reader.readAsText(aapObj.file.item(0));

	} else {
		toastr.error(tradDynForm.weWereUnableToReadYourFile);
	}
	return false;
}