(function (x, $) {
	function xhr(aapv2) {
		var that = this;
		this._aapv2 = aapv2;

		this.update_seen = function () {
			return new Promise(function (resolve, reject) {
				var aap;

				aap = that._aapv2;
				if (!aap.answer())
					reject({
						code: 101,
						msg: 'Missing answer id or bad format (string only)'
					});
				else if (!x.userId)
					reject({
						code: 102,
						msg: 'Missing user id, or user is not connected'
					});
				else {
					var post = {
						id: that._aapv2.answer(),
						collection: 'answers',
						value: 'now',
						setType: 'isoDate',
						path: "views." + x.userId + ".date"
					};
					var url = '/co2/aap/commonaap/action/update_seen_status';
					ajaxPost(null, url, post, resolve, function (arg0, arg1, arg2) {
						reject({
							code: 400,
							msg: [arg0, arg1, arg2]
						})
					});
				}
			});
		}
		// requests
		this.get_answer_data = function () {
			return new Promise(function (resolve, reject) {
				var aap;

				aap = that._aapv2;
				if (!aap.answer())
					reject({
						code: 101,
						msg: 'Missing answer id or bad format (string only)'
					});
				else {
					var post = {
						id: aap.answer()
					};
					var url = '/co2/aap/proposition/request/get_answer_v2';
					ajaxPost(null, url, post, resolve, function (arg0, arg1, arg2) {
						reject({
							code: 400,
							msg: [arg0, arg1, arg2]
						})
					});
				}
			});
		},
		this.get_element = function (id, type) {
			return new Promise(function (resolve, reject) {
				var post = {
					id: id,
					type: type
				};
				var url = '/co2/aap/proposition/request/checkElement';
				ajaxPost(null, url, post, resolve, function (arg0, arg1, arg2) {
					reject({
						code: 400,
						msg: [arg0, arg1, arg2]
					})
				});
				
			});
		},
		this.get_next_answer_id = function (criteria) {
			return new Promise(function (resolve, reject) {
				var aap,
					post;

				aap = that._aapv2;
				if (!aap.answer())
					reject({
						code: 11,
						msg: 'Missing answer id or bad format (string only)'
					});
				else {
					post = {
						id: aap.answer(),
						criteria: criteria
					};
					var url = '/co2/aap/proposition/request/get_answer_id_next';
					ajaxPost(null, url, post, function (resp) {
						if (resp.success)
							resolve(resp.content);
						else
							reject({
								code: 21,
								msg: resp.content
							});
					}, function (arg0, arg1, arg2) {
						reject({
							code: 22,
							msg: [arg0, arg1, arg2]
						})
					});
				}
			});
		}
		this.get_prev_answer_id = function (criteria) {
			return new Promise(function (resolve, reject) {
				var aap,
					post;

				aap = that._aapv2;
				if (!aap.answer())
					reject({
						code: 11,
						msg: 'Missing answer id or bad format (string only)'
					});
				else {
					post = {
						id: aap.answer(),
						criteria: criteria
					};
					var url = '/co2/aap/proposition/request/get_answer_id_prev';
					ajaxPost(null, url, post, function (resp) {
						if (resp.success)
							resolve(resp.content);
						else
							reject({
								code: 21,
								msg: resp.content
							});
					}, function (arg0, arg1, arg2) {
						reject({
							code: 22,
							msg: [arg0, arg1, arg2]
						})
					});
				}
			});
		}
		this.contribute = function (contrib, set) {
			return new Promise(function (resolve, reject) {
				var post = {
					answer: that._aapv2.answer(),
					contribs: []
				};
				if (!that._aapv2.answer())
					reject({
						code: 101,
						msg: 'Missing answer id or bad format (string only)'
					});
				else if (!contrib)
					reject({
						code: 102,
						msg: 'Missing contributor'
					});
				else if (!['boolean', 'number'].includes(typeof set))
					reject({
						code: 103,
						msg: 'Missing action or bad format (boolean or 0|1)'
					});
				else {
					if (typeof contrib === 'object' && Array.isArray(contrib))
						post.contribs = contrib;
					else
						post.contribs = [contrib];
					post.action = set;
					var url = x.baseUrl + '/co2/aap/proposition/request/contributev2'
					ajaxPost(null, url, post, resolve, function (arg0, arg1, arg2) {
						reject({
							code: 400,
							msg: [arg0, arg1, arg2]
						})
					});
				}
			});
		}
		this.rate = function (set) {
			return new Promise(function (resolve, reject) {
				var post, url;
				if (!['boolean', 'number'].includes(typeof set))
					reject({
						code: 101,
						msg: 'Missing action or bad format (boolean or 0|1)'
					});
				else if (!that._aapv2.answer())
					reject({
						code: 102,
						msg: 'Missing answer id or bad format (string only)'
					});
				else if (!x.userId) {
					reject({
						code: 103,
						msg: 'User is not connected'
					});
				}
				else {
					post = {
						id: that._aapv2.answer(),
						value: set ? 'set' : 'unset'
					};
					url = x.baseUrl + '/co2/aap/publicrate';
					ajaxPost(null, url, post, resolve, function (arg0, arg1, arg2) {
						reject({
							code: 400,
							msg: [arg0, arg1, arg2]
						})
					});
				}
			});
		}
		this.update_select_status = function (context, path) {
			return new Promise(function (resolve, reject) {
				var post, url;
				if (typeof context == "undefined" || typeof path == "undefined")
					reject({
						code: 101,
						msg: 'Missing action or bad format, required step, input'
					});
				else if (!that._aapv2.answer())
					reject({
						code: 102,
						msg: 'Missing answer id or bad format (string only)'
					});
				else if (!x.userId) {
					reject({
						code: 103,
						msg: 'User is not connected'
					});
				}
				else {
					post = {
						id: that._aapv2.answer(),
						collection: "answers",
						path: path,
						value: context
					};
					dataHelper.path2Value(post, resolve, function (arg0, arg1, arg2) {
						reject({
							code: 400,
							msg: [arg0, arg1, arg2]
						})
					})
				}
			});
		}
		this.get_discussion_space = function (answerId = null, form = null, tpl = null) {
			return new Promise(function (resolve, reject) {
				var post, url;
				if (!notEmpty(answerId) && !that._aapv2.answer())
					reject({
						code: 102,
						msg: 'Missing answer id or bad format (string only)'
					});
				// else if (!x.userId) {
				// 	reject({
				// 		code: 103,
				// 		msg: 'User is not connected'
				// 	});
				// }
				else {
					post = {
						answerId: answerId ? answerId : that._aapv2.answer(),
						formId: form
					};
					tpl ? post.tpl = tpl : "";
					url = x.baseUrl + '/co2/aap/commonaap/action/getDiscussionSpace';
					ajaxPost(null, url, post, resolve, function (arg0, arg1, arg2) {
						reject({
							code: 400,
							msg: [arg0, arg1, arg2]
						})
					});
				}
			});
		}
		this.get_tl_use = function (arg0) {
			var url, post;

			return (new Promise(function (resolve, reject) {
				if (!that._aapv2.answer())
					reject({
						code: 101,
						msg: 'Missing answer id or bad format (string only)'
					});
				else {
					post = {
						answer: that._aapv2.answer()
					};
					if (typeof arg0 === 'boolean' && arg0)
						post.user = x.userId;
					url = x.baseUrl + '/co2/aap/proposition/request/tl_user';
					ajaxPost(null, url, post, function (resp) {
						if (resp.success)
							resolve(resp.content);
						else
							reject({
								code: 400,
								msg: resp.content
							});
					}, function (arg0, arg1, arg2) {
						reject({
							code: 401,
							msg: [arg0, arg1, arg2]
						})
					});
				}
			}));
		}
		this.tl_use = function (arg0) {
			return (new Promise(function (resolve, reject) {
				if (!that._aapv2.answer())
					reject({
						code: 101,
						msg: 'Missing answer id or bad format (string only)'
					});
				else if (!x.userId) {
					reject({
						code: 102,
						msg: 'User is not connected'
					});
				} else if (!arg0) {
					reject({
						code: 103,
						msg: 'No argument is specified'
					});
				} else {
					post = {
						tls: arg0,
						answer: that._aapv2.answer()
					};
					url = x.baseUrl + '/co2/aap/proposition/request/tl_use';
					ajaxPost(null, url, post, function (resp) {
						if (resp.success)
							resolve(resp.content);
						else
							reject({
								code: 400,
								msg: resp.content
							});
					}, function (arg0, arg1, arg2) {
						reject({
							code: 401,
							msg: [arg0, arg1, arg2]
						})
					});
				}
			}));
		}
		this.fetch_aac = function() {
			return new Promise(function (resolve, reject) {
				var post = {
					"searchType" : ["forms"],
					"indexMin" : 0,
					"initType" : "", 
					"count" : true,
					"countType" : ["forms"],
					"indexStep" : 30,
					"filters": {
						"aapType": ["aac"]
					}
				};
				var url = baseUrl + '/co2/aap/aac/method/aac_directory';
				ajaxPost(null, url, post, resolve, function (arg0, arg1, arg2) {
					reject({
						code: 400,
						msg: [arg0, arg1, arg2]
					})
				});
			})
		},
		this.get_random_answer = function (limit, extraParams = {}) {
			return new Promise(function (resolve, reject) {
				var post = {
					form: that._aapv2.form()
				};
				extraParams.filters ? post.filters = extraParams.filters : ""
				if (typeof limit === 'undefined')
					post.limit = 1;
				if (!['number', 'undefined'].includes(typeof limit))
					reject({
						code: 101,
						msg: 'Limit bad format only number'
					});
				else if (!that._aapv2.form())
					reject({
						code: 102,
						msg: 'Missing form id or bad format (string only)'
					});
				else {
					post.limit = limit;
					var url = x.baseUrl + '/co2/aap/proposition/request/random';
					ajaxPost(null, url, post, resolve, function (arg0, arg1, arg2) {
						reject({
							code: 400,
							msg: [arg0, arg1, arg2]
						})
					});
				}
			});
		}
		this.depense_images = function (keys) {
			return new Promise(function (resolve, reject) {
				var post = {
					keys: keys
				};
				var url = x.baseUrl + '/co2/aap/depense-images';
				ajaxPost(null, url, post, function (resp) {
					if (resp.success)
						resolve(resp.content);
					else
						reject({
							code: 401,
							msg: resp.content
						});
				}, function (arg0, arg1, arg2) {
					reject({
						code: 402,
						msg: [arg0, arg1, arg2]
					})
				});
			});
		}
		this.create_actions_from_expenditure = function () {
			return new Promise(function (resolve, reject) {
				var url;

				url = x.baseUrl + "/survey/form/linkproject/answer/" + that._aapv2.answer();
				if (!that._aapv2.answer())
					reject({
						code: 11,
						msg: "Empty or invalid answerid format (string only)"
					});
				else
					x.ajaxPost(null, url, null, function (resp) {
						if (resp.success)
							resolve(resp.content);
						else
							reject({
								code: 21,
								msg: resp.content
							});
					}, function (arg0, arg1, arg2) {
						reject({
							code: 22,
							msg: [arg0, arg1, arg2]
						})
					});
			});
		}
	}
	function url() {
		var that = this;
		this._hash = null;
		this._dot = null;
		this._get = null;

		// constructor
		function construct() {
			var splited;
			var part_splited = [];
			var key_value_split;
			var key;
			var value;
			var i;

			this._hash = x.location.hash;
			splited = this._hash.split('?');
			this._dot = {};
			this._get = {};
			if (splited.length > 0) {
				part_splited = splited[0].split('.');
				i = 1;
				while (i < part_splited.length - 1) {
					that._dot[part_splited[i]] = part_splited[i + 1];
					i += 2;
				}
			}
			if (splited.length > 1) {
				part_splited = splited[1].split('&');
				i = 0;
				while (i < part_splited.length) {
					key_value_split = part_splited[i].split('=');
					key = key_value_split[0];
					value = key_value_split[1];
					if (key.substring(key.length - 2) === '[]') {
						key = key.substring(0, key.length - 2);
						if (typeof that._get[key] === 'undefined')
							value = [value];
						else {
							value = that._get[key].slice();
							value.push(key_value_split[1]);
						}
					}
					that._get[key] = value;
					i++;
				}
			}
		}

		construct.call(this);
		this.hash = function (arg0) {
			if (typeof arg0 === 'string') {
				that._hash = arg0;
				return (that);
			}
			if (typeof arg0 === 'undefined')
				return (this._hash);
		}
		this.dot = function (arg0, arg1) {
			if (typeof arg0 === 'object' && arg0 && notEmpty(arg0)) {
				Object.keys(arg0).forEach(function (key) {
					if (notEmpty(arg0[key]) && decodeURI(arg0[key]) === arg0[key].toString())
						that._dot[key] = encodeURI(arg0[key]);
					else
						that._dot[key] = arg0[key];
				});
				return (that);
			}
			if (typeof arg0 === 'string' && typeof arg1 === 'undefined')
				return (that._dot[arg0] ? that._dot[arg0] : null);
			if (typeof arg0 === 'string' && (['string', 'number'].includes(typeof arg1) || !arg1)) {
				if (arg1 && decodeURI(arg1) === arg1.toString())
					that._dot[arg0] = encodeURI(arg1);
				else
					that._dot[arg0] = arg1;
				return (that);
			}
			return (that._dot);
		}
		this.get = function (arg0, arg1) {
			if (typeof arg0 === 'object' && arg0) {
				Object.keys(arg0).forEach(function (key) {
					if (arg0[key] && decodeURI(arg0[key]) === arg0[key].toString())
						that._get[key] = encodeURI(arg0[key]);
					else
						that._get[key] = arg0[key];
				});
				return (that);
			}
			if (typeof arg0 === 'string' && typeof arg1 === 'undefined')
				return (that._get[arg0] ? that._get[arg0] : null);
			if (typeof arg0 === 'string' && ['string', 'number', 'object'].includes(typeof arg1)) {
				if (typeof arg1 === 'object' && Array.isArray(arg1)) {
					that._get[arg0] = [];
					arg1.forEach(function (arg) {
						if (decodeURI(arg) === arg.toString())
							that._get[arg0].push(encodeURI(arg));
						else
							that._get[arg0].push(arg);
					});
				} else if (arg1 && decodeURI(arg1) === arg1.toString())
					that._get[arg0] = encodeURI(arg1);
				else
					that._get[arg0] = arg1;
				return (that);
			}
			return (that._get);
		}
		this.reset = function () {
			construct.call(this);
			return (this);
		}
		this.toString = function () {
			var splited = this._hash.split('?');
			var dotstring = splited[0];
			var getstring = '';

			if (splited.length > 1)
				getstring = splited[1];
			Object.keys(this._dot).forEach(function (key) {
				var regex, value;

				regex = new RegExp('\\.(' + key + ')\\.[\\w\\d]*(\\.)?');
				value = that.dot(key);
				if (dotstring.match(regex)) {
					dotstring = dotstring.replace(regex, function (_, key, dot) {
						var replaced;

						replaced = '';
						if (value)
							replaced = '.' + key + '.' + that.dot(key)
						if (dot)
							replaced += dot;
						return (replaced);
					});
				} else if (value)
					dotstring += '.' + key + '.' + that.dot(key);
			});

			// replace existing value on query string
			// and exclude array form (put at second part)
			Object.keys(this._get).forEach(function (key) {
				var regex;

				if (typeof that.get(key) !== 'object')
					regex = '(&|^)(' + key + ')=[\\w\\d_\\+\\.%]+';
				else if (Array.isArray(that.get(key)))
					regex = '(&|^)(' + key + ')\\[\\]=[\\w\\d_\\+\\.%]+';
				if (regex) {
					regex = new RegExp(regex, 'g');
					getstring = getstring.replace(regex, function (_, begining, key) {
						var replaced;
						var value;

						replaced = '';
						value = that.get(key);
						if (value && !Array.isArray(value)) {
							if (begining)
								replaced += begining;
							replaced += (key + '=' + value);
						}
						return (replaced);
					});
				}
			});
			// parse remaining parameters
			Object.keys(this._get).forEach(function (key) {
				var regex, value;

				regex = new RegExp('(&|^)(' + key + ')=[\\w\\d_\\+\\.%]+', 'g');
				value = that.get(key);
				if (Array.isArray(value))
					value.forEach(function (value) {
						getstring += ('&' + key + '[]=' + value);
					});
				else if (value && !getstring.match(regex))
					getstring += ('&' + key + '=' + value);
			});
			if (getstring.charAt(0) === '&')
				getstring = getstring.substring(1);
			if (getstring)
				return (dotstring + '?' + getstring);
			return (dotstring);
		}
	}
	function html() {

	}
	if (typeof x.aapv2 === 'function')
		return;
	x.aapv2 = function (args) {
		var that = this;
		this._answer = null;
		this._form = null;
		this._context = null;
		this._xhr = null;
		this._url = null;
		this._html = null;
		// constructor
		if (typeof args === 'object' && args)
			this.set(args);

		// setters
		this.set = function (arg0, arg1) {
			if (typeof arg0 === 'object' && arg0 && typeof arg1 === 'undefined')
				$.each(arg0, function (key, val) {
					that['_' + key] = val;
					if (typeof that[key] !== 'function')
						that[key] = function (arg) {
							if (typeof arg === 'undefined') {
								if (typeof that['_' + key] === 'object') {
									if (Array.isArray(that['_' + key]))
										return (that['_' + key].slice());
									return (Object.assign({}, that['_' + key]));
								}
								return (that['_' + key]);
							}
							if (typeof arg !== 'undefined')
								that['_' + key] = arg;
							return (that);
						}
				});
			else if (typeof arg0 === 'string' && typeof arg1 !== 'undefined')
				that['_' + arg0] = arg1;
			return (this);
		}
		this.form = function (arg0) {
			if (typeof arg0 === 'undefined')
				return (that._form);
			if (typeof arg0 === 'string')
				that._form = arg0;
			return (that);
		}
		this.answer = function (arg0) {
			if (typeof arg0 === 'undefined')
				return (that._answer);
			if (typeof arg0 === 'string')
				that._answer = arg0;
			return (that);
		}
		// object instances
		this.xhr = function () {
			if (!that._xhr)
				that._xhr = new xhr(that);
			return (that._xhr);
		}
		this.url = function (hash) {
			if (!that._url)
				that._url = new url();
			if (typeof hash === 'string')
				return (that._url.hash(hash));
			return (that._url);
		}
		this.html = function () {
			if (!that._html)
				that._html = new html();
			return (that._html);
		};
	};
	x.g_aap = new x.aapv2();
})(window, jQuery);