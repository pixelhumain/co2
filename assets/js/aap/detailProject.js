function detailProject($, wsCO, io, coWsConfig, projectId) {
	var kanban_dom;
	var socketConfigurationEnabled = coWsConfig.enable && coWsConfig.serverUrl && coWsConfig.pingActionManagement;
	if (!socketConfigurationEnabled) {
		return true;
	}
	wsCO = wsCO && wsCO.connected ? wsCO : (io ? io.connect(coWsConfig.serverUrl) : null);
	if (!wsCO)
		return (true);
	function l_update_action_count(project) {
		var action_count_url = baseUrl + '/co2/aap/project/request/action_count';
		var post = {
			project: project
		};
		ajaxPost(null, action_count_url, post, function (c) {
			$('.props-item[data-target="' + project + '"] .badge').remove();
			if (c > 0) {
				var badge_dom = $('<span>', {
					text: c,
					class: 'badge'
				});
				$('.props-item[data-target="' + project + '"]').append(badge_dom);
			}
		});
	}
	if (!wsCO.hasListeners('.set-contributors' + projectId))
		wsCO
			.on('.set-contributors' + projectId, function (data) {
				var kanbanDom = $('#kanban-container1');
				if (kanbanDom.length) {
					var kanbanData = kanbanDom.kanban('getData', data.action);
					var i, action, is_contributor;
					kanbanData.contributors = data.contributors.slice();
					is_contributor = kanbanData.contributors.findIndex(contributor => (contributor.id === userId));
					for (i = 0; i < kanbanData.actions.length; i++) {
						action = kanbanData.actions[i];
						if (action.action === 'onParticipate') {
							if (is_contributor >= 0) {
								action.icon = 'fa fa-unlink';
								action.bstooltip.text = coTranslate('Quit');
							} else {
								action.icon = 'fa fa-link';
								action.bstooltip.text = coTranslate('Participate');
							}
						}
						kanbanData.actions[i] = action;
					}
					kanbanDom.kanban('setData', { id: data.action, data: kanbanData });
				}
			})
	if (!wsCO.hasListeners('.update-images' + projectId))
		wsCO
			.on('.update-images' + projectId, function (data) {
				var kanbanDom = $('#kanban-container1');
				if (kanbanDom.length) {
					var kanbanData = kanbanDom.kanban('getData', { id: data.action });
					kanbanData.images = data.images.map(function (imageMap) {
						return imageMap.url;
					});
					for (var i = 0; i < kanbanData.actions.length; i++) {
						var action = kanbanData.actions[i];
						if (action.action === 'onCardPictureClick') {
							action.badge = data.images.length;
						}
						kanbanData.actions[i] = action;
					}
					kanbanDom.kanban('setData', { id: data.action, data: kanbanData });
				}
			})
	if (!wsCO.hasListeners('.update-documents' + projectId))
		wsCO
			.on('.update-documents' + projectId, function (data) {
				var kanbanDom = $('#kanban-container1');
				if (kanbanDom.length) {
					var kanbanData = kanbanDom.kanban('getData', { id: data.action });
					for (var i = 0; i < kanbanData.actions.length; i++) {
						var action = kanbanData.actions[i];
						if (action.action === 'onCardFileClick') {
							action.badge = data.documents.length;
						}
						kanbanData.actions[i] = action;
					}
					kanbanDom.kanban('setData', { id: data.action, data: kanbanData });
				}
			})
	if (!wsCO.hasListeners('.update-task' + projectId))
		wsCO
			.on('.update-task' + projectId, function (data) {
				var kanbanDom = $('#kanban-container1');
				if (kanbanDom.length) {
					var kanbanData = kanbanDom.kanban('getData', { id: data.action });
					for (var i = 0; i < kanbanData.actions.length; i++) {
						var action = kanbanData.actions[i];
						if (typeof action.icon === 'undefined') {
							var taskSummary = data.recap.done + '/' + data.recap.total;
							action.badge = taskSummary;
							action.className = taskSummary !== '0/0' ? (data.recap.total === data.recap.done ? 'kanban-done-task-action' : 'kanban-doing-task-action') : 'kanban-no-task-action';
						}
						kanbanData.actions[i] = action;
					}
					kanbanDom.kanban('setData', { id: data.action, data: kanbanData });
				}
			})
	if (!wsCO.hasListeners('.aap-kanban-move' + projectId))
		wsCO
			.on('.aap-kanban-move' + projectId, function (data) {
				l_update_action_count(projectId);
				var kanbanDom = $('#kanban-container1');
				if (userConnected && userConnected._id.$id !== data.authorId && data.publics.includes(userConnected._id.$id)) {
					window.myPNotify.pingNotification(window.coTranslate('Action update status'), dataHelper.printf("<strong>{{authorName}}</strong> a changé le status de l'action <strong>{{actionName}}</strong> à <strong>{{newStatus}}</strong>", {
						authorName: data.authorName,
						actionName: data.actionName,
						newStatus: data.targetText
					}));
				}
				if (kanbanDom.length && data.emiter !== wsCO.id) {
					kanbanDom.kanban('moveCard', {
						id: data.action,
						target: data.target,
						position: parseInt(data.position, 10)
					});
				}
			})
	if (!wsCO.hasListeners('.archive' + projectId))
		wsCO
			.on('.archive' + projectId, function (data) {
				l_update_action_count(projectId);
				var kanbanDom = $('#kanban-container1');
				if (kanbanDom.length && data.emiter !== wsCO.id) {
					var param = { id: data.action };
					if (typeof data.column !== 'undefined' && data.column) {
						param.column = data.column;
					}
					kanbanDom.kanban('deleteData', param);
				}
			});
	if (!wsCO.hasListeners('.kanban-insert-card' + projectId))
		wsCO
			.on('.kanban-insert-card' + projectId, function (data) {
				l_update_action_count(projectId);
				var kanbanDom = $('#kanban-container1');
				if (kanbanDom.length && kanbanDom.find('.kanban-list-card-detail[data-id="' + data.id + '"]').length === 0) {
					kanbanDom.kanban('addData', data.action);
				}
			})
	if (!wsCO.hasListeners('.action-create-' + projectId))
		wsCO
			.on('.action-create-' + projectId, function (data) {
				l_update_action_count(projectId);
				var kanbanDom = $('#kanban-container1');
				if (kanbanDom.length && (data.component !== 'kanban' || data.emiter != wsCO.id)) {
					coInterface.actions.request_get_action(data.id).then(function (db_action) {
						data.is_contributor = typeof db_action.contributors.find(c => c.id === userId) !== "undefined";
						var k_data = {
							id: data.id,
							title: db_action.name,
							tags: db_action.tags,
							header: data.group,
							contributors: db_action.contributors.map(function (contributor) {
								return {
									id: contributor.id,
									name: contributor.name,
									image: contributor.image,
									data: {
										id: contributor.id
									}
								};
							}),
							actions: [{
								icon: 'fa fa-archive',
								bstooltip: { text: trad.delete, position: 'top' },
								action: 'onCardArchiveClick',
								bstooltip: { text: trad['Archive'], position: 'top' }
							}, { icon: 'fa fa-commenting', badge: '0', action: 'onCardCommentClick' }, {
								icon: 'fa fa-picture-o',
								badge: '0',
								action: 'onCardPictureClick',
								hideCondition: { badge: '0' }
							},
							{
								icon: 'fa fa-file',
								badge: '0',
								action: 'onCardFileClick',
								hideCondition: { badge: '0' }
							},
							{
								badge: '0/0',
								className: 'kanban-no-task-action'
							}, {
								icon: 'fa ' + (data.is_contributor ? 'fa-unlink' : 'fa-link'),
								bstooltip: {
									text: data.is_contributor ? coTranslate('Quit') : coTranslate('Participate'),
									position: 'top'
								},
								action: 'onParticipate'
							}]
						};
						kanbanDom.kanban('addData', k_data);
					});
				}
			})
	if (!wsCO.hasListeners('.kanban-edit-card' + projectId))
		wsCO
			.on('.kanban-edit-card' + projectId, function (data) {
				var kanbanDom = $('#kanban-container1');
				if (kanbanDom.length) {
					var action = kanbanDom.kanban('getData', { id: data.id });
					if (action.title !== data.title) {
						action.title = data.title;
						kanbanDom.kanban('setData', { id: data.id, data: action });
					}
				}
			});
	if (!wsCO.hasListeners('action-archive'))
		wsCO.on('action-archive', function (data) {
			l_update_action_count(projectId);
			var kanbanDom = $('#kanban-container1');
			if (kanbanDom.length && typeof $.fn.kanban === 'function') {
				var data = kanbanDom.kanban('getData', data.id);
				if (data)
					kanbanDom.kanban('deleteData', data.id);
			}
		});
	wsCO.on("action-full-update", function (data) {
		var kb_dom = $("#kanban-container1");
		var kb_data;

		if (kb_dom.length && typeof $.fn.kanban === "function") {
			kb_data = kb_dom.kanban("getData", data.id);
			if (kb_data) {
				kb_data.title = data.name;
				kb_dom.kanban("setData", { id: data.id, data: kb_data });
			}
		}
	});
	wsCO.on('.action-order', function (data) {
		l_update_action_count(projectId);
		kanban_dom = $('#kanban-container1');
		var card_dom = kanban_dom.find('.kanban-list-card-detail[data-id=' + data.moved + ']');
		var after_data;
		if (data.component === 'kanban')
			return (true);
		if (kanban_dom.length && card_dom.length) {
			if (data.after) {
				after_data = kanban_dom.kanban('getData', { id: data.after });
				if (after_data.header === data.group)
					kanban_dom.kanban('moveCard', {
						id: data.moved,
						target: data.group,
						position: after_data.position
					})
				else
					kanban_dom.kanban('moveCard', {
						id: data.moved,
						target: data.group
					});
			} else
				kanban_dom.kanban('moveCard', {
					id: data.moved,
					target: data.group
				});
		}
	});
}