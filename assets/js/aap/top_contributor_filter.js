function topContributorFilter(target) {
	var _contributors;
	var _active;
	if (typeof target === 'string') {
		target = $(target);
	}
	if (typeof target !== 'object' || target.length === 0) {
		return null;
	}
	target.css({
		justifySelf : 'normal'
	});
	
	function getInvisibleDom(domElements) {
		return domElements.filter(function (domElement) {
			return domElement.parent().width() - (domElement.position().left + domElement.width()) < 0;
		});
	}
	
	var _topContributorFilter = {
		build : function (contributors, activeId) {
			var itsWidth = target.empty().html('').css('width', 'auto').width();
			var activeIdIndex = -1;
			target.width(itsWidth);
			if (typeof contributors !== 'object' || !Array.isArray(contributors)) {
				contributors = [];
			}
			_contributors = contributors.slice();
			_active = activeId;
			if (typeof _active === 'string' && _active.length) {
				activeIdIndex = _contributors.findIndex(function (contributor) {
					return contributor.id === _active;
				});
				_active = activeIdIndex >= 0 ? _active : null;
				_topContributorFilter.selected = _active;
			}
			var unorderedListDom = $('<ul>').addClass('tab-contributor-panel').css({
				overflow : 'hidden',
				justifyContent : ''
			});
			var tabContributorsDom = _contributors.map(function (contributor) {
				var liDom = $('<li>')
					.addClass('tab-contributor')
					.attr('data-toggle', 'tooltip')
					.attr('data-placement', 'bottom')
					.attr('title', contributor.name)
					.attr('data-id', contributor.id)
					.append(
						$('<img>')
							.attr('src', contributor.image)
							.attr('alt', contributor.name)
					);
				if (typeof _active !== 'undefined' && contributor.id === _active) {
					liDom.addClass('active');
				}
				return liDom;
			});
			var dropdownContributorsDom = _contributors.map(function (contributor) {
				var avatarDom = $('<div>')
					.addClass('contributor-filter avatar-container')
					.append(
						$('<div>').addClass('image').append(
							$('<img>')
								.attr('src', contributor.image)
								.attr('alt', contributor.name)
						)
					)
					.append(
						$('<span>')
							.addClass('name')
							.text(contributor.name)
					);
				if (typeof _active !== 'undefined' && contributor.id === _active) {
					avatarDom.addClass('active');
				}
				return $('<li>')
					.attr('role', 'presentation')
					.attr('data-id', contributor.id)
					.data('contributor', contributor)
					.append(avatarDom);
			});
			
			var invisibleDom = [];
			do {
				target.empty().html('').append(unorderedListDom.empty().html(''));
				var tabContributorsDomCopy = tabContributorsDom.filter(function (tabContributorDom) {
					return !invisibleDom.includes(tabContributorDom);
				});
				var finalDropdownContributorsDom = dropdownContributorsDom.slice(unorderedListDom.children().length);
				$.each(tabContributorsDomCopy, function (_, tabContributorDom) {
					unorderedListDom.append(tabContributorDom);
					tabContributorDom.off('click').on('click', function () {
						var activated = tabContributorDom.hasClass('active');
						
						tabContributorsDomCopy.forEach(function (contributorDom) {
							contributorDom.removeClass('active');
						});
						finalDropdownContributorsDom.forEach(function (contributorDom) {
							contributorDom.find('.avatar-container').removeClass('active');
						});
						
						if (!activated) {
							tabContributorDom.addClass('active')
							_topContributorFilter.selected = tabContributorDom.data('id');
						} else {
							_topContributorFilter.selected = null;
						}
						
						if (typeof _topContributorFilter.onFilter === 'function') {
							_topContributorFilter.onFilter.call(_topContributorFilter, {
								id : tabContributorDom.data('id'),
								active : tabContributorDom.hasClass('active')
							});
						}
					})
				});
				invisibleDom = getInvisibleDom(tabContributorsDomCopy);
			} while (invisibleDom.length > 0);
			
			if (unorderedListDom.children().length < tabContributorsDom.length) {
				if (activeIdIndex >= 0) {
					var lastViewIndex = unorderedListDom.children().length - 2;
					if (activeIdIndex > lastViewIndex && lastViewIndex >= 0) {
						unorderedListDom.children().eq(lastViewIndex).before(tabContributorsDom[activeIdIndex]);
						tabContributorsDom.splice(lastViewIndex, 0, tabContributorsDom.splice(activeIdIndex, 1)[0]);
						dropdownContributorsDom.splice(lastViewIndex, 0, dropdownContributorsDom.splice(activeIdIndex, 1)[0]);
					}
				}
				unorderedListDom.children().last().detach();
				var finalDropdownContributorsDom = dropdownContributorsDom.slice(unorderedListDom.children().length);
				var searchContributorDom = $('<li>').addClass('filterContainerD filter-alias custom-filter searchBar-filters contributor-dropdown-search')
				                                    .html('' +
					                                    '<input type="text" class="form-control pull-left text-center alias-main-search-bar search-bar" data-field="text" placeholder="Recherche">' +
					                                    '<span class="text-white input-group-addon pull-left main-search-bar-addon " data-field="text" data-icon="fa-search">' +
					                                    '   <i class="fa fa-search"></i>' +
					                                    '</span>');
				finalDropdownContributorsDom.forEach(function (contributorDom) {
					contributorDom.off('click').on('click', function () {
						var activated = contributorDom.find('.avatar-container').hasClass('active');
						tabContributorsDomCopy.forEach(function (contributorDom) {
							contributorDom.removeClass('active');
						});
						finalDropdownContributorsDom.forEach(function (contributorDom) {
							contributorDom.find('.avatar-container').removeClass('active');
						});
						
						if (!activated) {
							contributorDom.find('.avatar-container').addClass('active')
							_topContributorFilter.selected = contributorDom.data('id');
						} else {
							_topContributorFilter.selected = null;
						}
						if (typeof _topContributorFilter.onFilter === 'function') {
							_topContributorFilter.onFilter.call(_topContributorFilter, {
								id : contributorDom.data('id'),
								active : true
							});
						}
						if (unorderedListDom.children().length > 1) {
							var reorderedContributors = _contributors.slice();
							var index = reorderedContributors.findIndex(function (reorderedContributor) {
								return reorderedContributor.id === _topContributorFilter.selected;
							});
							var lastUlIndex = unorderedListDom.children().length - 2;
							reorderedContributors.splice(lastUlIndex, 0, reorderedContributors.splice(index, 1)[0]);
							_topContributorFilter.build(reorderedContributors, _topContributorFilter.selected);
						}
					})
				})
				var childrenLength = unorderedListDom.children().length;
				var dropdownText = (childrenLength === 0 ? '' : '+') + (tabContributorsDom.length - childrenLength);
				var dropdownMenuDom = $('<ul>')
					.addClass('dropdown-menu')
					.attr('role', 'menu')
					.attr('aria-labelledby', 'contributor-expand-dropdown');
				unorderedListDom.append(
					$('<li>')
						.addClass('tab-contributor expander dropdown')
						.append(
							$('<span>')
								.addClass('dropdown-toggle')
								.attr('id', 'contributor-expand-dropdown')
								.attr('data-toggle', 'dropdown')
								.text(dropdownText)
						)
						.append(
							dropdownMenuDom.append(searchContributorDom, finalDropdownContributorsDom)
						)
				);
				searchContributorDom.on('input', '.search-bar', function() {
					var self = $(this);
					while(dropdownMenuDom.children().length > 1) {
						dropdownMenuDom.children().last().detach();
					}
					finalDropdownContributorsDom.forEach(function(element) {
						var regex = new RegExp(self.val(), 'ig');
						if(!self.val() || element.data('contributor').name.match(regex)) {
							dropdownMenuDom.append(element);
						}
					})
				});
			}
			unorderedListDom.css({overflow : '', justifyContent : 'end'});
			$('[data-toggle=tooltip]').tooltip();
			return _topContributorFilter;
		},
		selected : null
	};
	
	window.onresize = function() {
		if (typeof _contributors !== 'undefined') {
			_topContributorFilter.build(_contributors, _active);
		}
	};
	return _topContributorFilter;
}