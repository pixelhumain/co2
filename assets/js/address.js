var addressObj = {
	container : null,
	config : null,
	resultSearch : {},
	result : [],
	countryList : null,
	countryCodeList : {},
	map : {},
	main : null,
	init : function(pInit){
		mylog.log("addressObj.init", pInit);
		//Init variable
		var copyAdd = null;
		if (pInit != null &&
			typeof pInit.container != "undefined" &&
			pInit.container != null &&
			$(pInit.container).length > 0) {
			copyAdd = jQuery.extend(true, {}, addressObj);
			copyAdd.initVar(pInit);
			copyAdd.initHTML(pInit);
		}
		return copyAdd;

	},
	initVar: function (pInit) {
		mylog.log("addressObj.initVar", pInit);
		this.container = ((pInit != null && typeof pInit.container != "undefined") ? pInit.container : null );
		this.result = ((pInit != null && typeof pInit.result != "undefined" && pInit.result != null && pInit.result != "") ? pInit.result : [] );
		this.config = ((pInit != null && typeof pInit.config != "undefined" && pInit.config != null && pInit.config != "") ? pInit.config : {} );
		this.formDataToResult(this);
	},
	initHTML: function (pInit) {
		mylog.log("addressObj.initHTML", pInit);
		var str = '';
		str += '<div class="col-md-6 col-xs-12 no-padding">';
			str += '<div class="form-inline form-group no-padding selectCountry">'+
						'<label class="" for="selectCountry">'+tradDynForm.country+' : </label>'+
						'<select class="" name="selectCountry" multiple id="selectCountry">'+
							//'<option value="">'+trad.chooseCountry+'</option>'+
						'</select>'+
					'</div>';
			str += '<div class="col-xs-11 no-padding inputAddCountry">';
			var strC = '<input type="text" ';
			if( typeof pInit["input"] != "undefined" ){
				if(typeof pInit["input"]["id"] != "undefined" )
					strC += ' id="'+pInit["input"]["id"]+'"';

				if(typeof pInit["input"]["class"] != "undefined" ){
					mylog.log("addressObj.initHTML class", pInit["input"]["class"]);
					strC += ' class=" '+pInit["input"]["class"]+' " ';
				}

				if(typeof pInit["input"]["aria-describedby"] != "undefined" )
					strC += ' aria-describedby="'+pInit["input"]["aria-describedby"]+'"';

				if(typeof pInit["input"]["placeholder"] != "undefined" )
					strC += ' placeholder="'+pInit["input"]["placeholder"]+'"';

				strC += ( ( typeof pInit["input"]["value"] != "undefined" ) ? ' value="'+pInit["input"]["value"]+'"' :  ' value=""' );
			}
			strC +=  ' >' ;
			mylog.log("addressObj.initHTML strC", strC);
			str += strC+'</div>';

			str += '<a href="javascript:;" class="btn btn-primary btnSearchAddress col-xs-1"><i class="fa fa-search"></i></a>';

			str += '<div class="dropdown pull-left col-xs-12 no-padding">'+
						'<ul class="dropdown-menu dropdown-address" style="margin-top: -2px; width:100%;background-color : #ea9d13; max-height : 300px ; overflow-y: auto">'+
							'<li><a href="javascript:" class="disabled">'+trad.currentlyresearching+'</a></li>'+
						'</ul>'+
					'</div>';
			str += '<div class="resultAddress col-xs-12 no-padding"></div>';
		str += '</div>';
		str += '<div id="mapAddress" class="col-md-6 col-xs-12" style="height : 400px"></div>';
		mylog.log("addressObj.initHTML str", str);
		$(this.container).html(str);
		this.bindAddress(pInit, this);
		this.addResultInHTML(this);
		var aObj = this;
		mylog.log("addressObj.initHTML before ajax");
		if(Object.entries(addressObj.countryCodeList).length === 0){
			$.ajax({
				type: "POST",
				url: baseUrl+"/"+moduleId+"/opendata/getcountries/",
				dataType: "json",
				success: function(data){
					mylog.log("addressObj.initHTML getcountries data !",data, aObj);
					aObj.countryList = data;
					if(aObj.countryList!=null){
						var strCountry = "";
						$.each(aObj.countryList, function(key, v){
							if(aObj.config != null && typeof aObj.config.availableCountry != "undefined" && Array.isArray(aObj.config.availableCountry) && aObj.config.availableCountry.length != 0){
								if(aObj.config.availableCountry.includes(v.countryCode)){
									aObj.countryCodeList[v.countryCode] = v ;
									strCountry += "<option value='"+v.countryCode+"'>"+v.name+"</option>";
								}
							}else{
								//mylog.log("addressObj.initHTML countryList", key, v);
								aObj.countryCodeList[v.countryCode] = v ;
								strCountry += "<option value='"+v.countryCode+"'>"+v.name+"</option>";
							}
						});
						$(aObj.container+' #selectCountry').append(strCountry);
					}

					if(typeof aObj.config != "undefined" && aObj.config != null && typeof aObj.config.defaultCountry != "undefined" && aObj.config.defaultCountry != null){
						if(Array.isArray(aObj.config.defaultCountry)){
							$.each(aObj.config.defaultCountry, function(i,e){
								$(aObj.container+" #selectCountry option[value='" + e + "']").prop("selected", true);
							});

						}else
							$(aObj.container+' #selectCountry').val(aObj.config.defaultCountry)
					}
					$(aObj.container+' #selectCountry').select2();
					$(aObj.container+' #selectCountry').change(function(){
						mylog.log($(this).val(),selectCountry);
					})
					//$("#btn-submit-form").prop('disabled', false);
					
				},
				error: function(error){
					mylog.log("addressObj.initHTML error", error);
				}
			});
		}
		else{
			addressObj.countryCodeList.level= ["1"];
			ajaxPost(null,
			baseUrl + '/co2/search/getzone/', 
				addressObj.countryCodeList, 
				function(data){
					mylog.log("addressObj. get specific country !",data);
					aObj.countryList = data;
					if(aObj.countryList!=null){
						var strCountry = "";
						var selected = "";
						if(Object.keys(aObj.countryList).length==1)
							selected = "selected";
						$.each(aObj.countryList, function(key, v){
							//mylog.log("addressObj.initHTML countryList", key, v);
							aObj.countryCodeList[v.countryCode] = v ;
							strCountry += "<option "+ selected +" value='"+v.countryCode+"'>"+v.name+"</option>";
						});
						$(aObj.container+' #selectCountry').append(strCountry);
					}
				}
			);
		}
	},
	bindAddress: function (pInit, aObj) {

		var paramsMapAdd = {
			container : aObj.container+' #mapAddress',
			activePopUp : true,
			tile : "mapbox2",
			menuRight : false
	    };
	    mylog.log("addressObj.bindAddress", pInit, aObj);
	    aObj.map = mapObj.init(paramsMapAdd);
	    allMaps.maps[paramsMapAdd.container]=aObj.map;

	    $(aObj.container+" .btnSearchAddress").off().click(function () {
			aObj.search(aObj);
		});

	    $(aObj.container+" #inputAdd").on('keyup keypress', function(e) {
		  var keyCode = e.keyCode || e.which;
		  if (keyCode === 13) { 
		    e.preventDefault();
		    return aObj.search(aObj);
		  }
		});
	},
	search: function (aObj) {
		mylog.log("addressObj.search");
		var html = "<li><i class='fa fa-spinner fa-spin'></i> Recherche en cours</li>";
		aObj.setHtmlDropdown(aObj, html);
		var paramsAddress = {
			text : $(this.container+" #inputAdd").val(),
			countryCode : $(this.container+" #selectCountry").val()
		};
		mylog.log("addressObj.search paramsAddress",paramsAddress);
		var ajaxUrl = baseUrl + '/co2/search/address' ;
		ajaxPost("", ajaxUrl, paramsAddress, function(data){
			mylog.log('addressObj.search ajax success', data);
			aObj.resultSearch = data;
			aObj.addResultsInForm(aObj, data);
		}, function(data){
			mylog.log('addressObj.search ajax error', data);
		}, "json");
	},
	setHtmlDropdown : function(aObj, html){
		mylog.dir("addressObj.setHtmlDropdown", html);
		$(aObj.container +" .dropdown-address").html(html);
		$(aObj.container +" .dropdown-address").show();
	},
	addResultsInForm : function (aObj, res){
		mylog.log("addressObj.search addResultsInForm", aObj, res);
		var html = "";
		if(typeof res != "undefined" && res != null){
			$.each(res, function(key, value){ //mylog.log(allCities);
				html += "<li><a href='javascript:;' class='item-street-found' data-key='"+key+"'> "+
							value.address.name+
						"</a></li>";
			});
		}
		
		if(html == "") html = "<li><i class='fa fa-ban'></i> "+trad.noresult+"</li>";
		
		aObj.setHtmlDropdown(aObj, html);
		
		$(aObj.container+" .item-street-found").click(function(){
			var keyAdd = $(this).data("key");
			$(aObj.container +" .dropdown-address").hide();
			if (aObj.result.length == 0) {
				aObj.resultSearch[keyAdd].main = true;
				aObj.main = 0;
			}
			if(typeof aObj.resultSearch[keyAdd].new != "undefined" && aObj.resultSearch[keyAdd].new === true ){
				var ajaxUrl = baseUrl + '/co2/city/create' ;
				ajaxPost("", 
					ajaxUrl, 
					aObj.resultSearch[keyAdd],
					function(data){
						mylog.log('addressObj.search addResultsInForm item-street-found ajax success', data);
						aObj.result.push(aObj.resultSearch[keyAdd]);
						
						aObj.addResultInHTML(aObj);
						if(typeof aObj.saveAuto == "function")
							aObj.saveAuto(aObj);
					}, function(data){
						mylog.log('addressObj.search addResultsInForm item-street-found ajax error', data);
					}, "json");
			} else{
				aObj.result.push(aObj.resultSearch[keyAdd]);
				aObj.addResultInHTML(aObj);
				if(typeof aObj.saveAuto == "function")
					aObj.saveAuto(aObj);
			}
			

		});
	},
	addResultInHTML : function(aObj){
		mylog.log("addressObj.search addResultInHTML", aObj);
		aObj.map.clearMap();
		
		var str = "";
		var strS = "";
		var strP = "";
		var eltsMap = aObj.result;
		if(aObj.result.length > 0){
			$.each(aObj.result, function(keyR, valR){
				mylog.log("addressObj.search addResultInHTML each", keyR, valR);
				var nameAddress = "";
				if(typeof valR.address.name == "undefined")
					nameAddress = valR.address.streetAddress+", "+ valR.address.postalCode+", "+valR.address.addressLocality+", "+valR.address.level3Name;
				else	
					nameAddress = valR.address.name
				eltsMap[keyR]["name"] = nameAddress ;
				if(typeof valR.main != "undefined" && valR.main === true){
					strP += "<div class='col-xs-12 padding-5'><h4>Adresse Principale</h4></div>"+
								"<div class='col-xs-12 shadow2 padding-15'>"+
								
								"<span class='pull-left locel text-red bold'>"+
								nameAddress+ 
						          "</span> "+ 

						          "<a href='javascript:;' data-index='"+keyR+"' "+ 
						          	' data-toggle="tooltip" data-placement="top" title="'+tradDynForm.clear+'" '+
						            " class='deleteLocDynForm btn btn-sm btn-danger pull-right'> "+ 
						            "<i class='fa fa-trash'></i>"+
						          "</a>"+ 
							"</div>";
					} else {
						strS += "<div class='col-xs-12 shadow2 padding-15'>"+
							"<span class='pull-left locel text-red bold'>"+
							nameAddress+ 
					          "</span> "+ 

					          "<a href='javascript:;' data-index='"+keyR+"' "+ 
					          	' data-toggle="tooltip" data-placement="top" title="'+tradDynForm.clear+'" '+
					            " class='deleteLocDynForm btn btn-sm btn-danger pull-right'> "+ 
					            "<i class='fa fa-trash'></i>"+
					          "</a>"+ 
					 
					          "<a href='javascript:;' data-index='"+keyR+"'"+ 
					          	' data-toggle="tooltip" data-placement="top" title="Set as main"'+
					            " class='margin-right-5 setAsMain pull-right btn btn-sm btn-success' > "+ 
					            "<i class='fa fa-star'></i>"+ 
					          "</a>" + 
						"</div>";
					}
				
				
			});

			if(strS != "")
				strS = "<div class='col-xs-12 padding-5'><h4>Adresses Secondaires</h4></div>"+strS;
			str = strP + strS;
		}	    	
		
		$(aObj.container +" .resultAddress").html(str);
		aObj.map.addElts(eltsMap);

		$(aObj.container+" .deleteLocDynForm").off().click(function () {
			if(typeof $(this).data("index") != "undefined" && $(this).data("index") != null ){
				var index = $(this).data("index");

				aObj.result.splice($(this).data("index"),1);

				if(aObj.main == index)
					aObj.main = null;

				if(aObj.result.length > 0 && aObj.main == null){
					aObj.setAsMain(aObj, 0);
				} else{
					aObj.addResultInHTML(aObj);
					if(typeof aObj.saveAuto == "function")
						aObj.saveAuto(aObj);
				}
			}
		});

		$(aObj.container+" .setAsMain").off().click(function () {
			aObj.setAsMain(aObj, $(this).data("index"));
		});
	    $(aObj.container+' [data-toggle="tooltip"]').tooltip();
				
	},
	/*getFullAddress(obj){
		var strResult = "";
		if(typeof obj.street != "undefined") strResult += obj.street;
		if(typeof obj.cityName != "undefined") strResult += (strResult != "") ? ", " + obj.cityName : obj.cityName;
		//if(typeof obj.village != "undefined") strResult += (strResult != "") ? ", " + obj.village : obj.village;
		if(typeof obj.postalCode != "undefined") strResult += (strResult != "") ? ", " + obj.postalCode : obj.postalCode;
		if(typeof obj.country != "undefined") strResult += (strResult != "") ? ", " + obj.country : obj.country;
		return strResult;
	},*/
	setAsMain : function (aObj, index){
		mylog.log("addressObj.search setAsMain", aObj, index);
		if( aObj.main != null && aObj.result[aObj.main] )
			delete aObj.result[aObj.main].main ;
		aObj.main = index;
		aObj.result[aObj.main].main = true ;
		aObj.addResultInHTML(aObj);
		if(typeof aObj.saveAuto == "function")
			aObj.saveAuto(aObj);
	},
	getResultToFormData : function (aObj){
		var res = {}
		if(aObj.result.length > 0){
			$.each(aObj.result, function(keyR, valR){
				if(typeof valR.main != "undefined" && valR.main === true){
					res.address = valR.address;
					res.geo = valR.geo;
					res.geoPosition = valR.geoPosition;
				}else{
					if(typeof res.addresses == "undefined")
						res.addresses = [];
					res.addresses.push(valR);
				}
			});
		}
		return res;
	},
	formDataToResult : function (aObj){
		mylog.log("addressObj.formDataToResult", aObj);
		var res = [] ;
		if(typeof aObj.result.address != "undefined"){
			var main = {
				address : aObj.result.address,
				geo : aObj.result.geo,
				geoPosition : aObj.result.geoPosition,
				main : true
			};
			res.push(main);
		}

		if(typeof aObj.result.addresses != "undefined"){
			$.each(aObj.result.addresses, function(keyR, valR){
				res.push(valR);
			});
		}
		aObj.result = res;
	}
};