//$("#content-view-admin").empty();
var adminDirectory = {
	container : "adminDirectory",
	context: {},
	timeoutAddCity : null,
	paramsSearch : {
		text:null,
		page:"",
		types:[]
	},
	panelAdmin : {},
	results : {},
	//countTotal : 0,
	modals : {},
	actionsStr : false,
	//url : null,
	setType:[],
	//filtersObj : null,
	//searchObj : false,
	init : function(pInit){
		var pInit = (typeof pInit !== 'undefined') ? pInit : null;
		mylog.log("adminDirectory.init ",pInit);
		//Init variable
		var copyAdmin = jQuery.extend(true, {}, adminDirectory);
		copyAdmin.initVar(pInit);
		return copyAdmin;

	},
	initVar : function(pInit){
		var pInit = (typeof pInit !== 'undefined') ? pInit : null;
		mylog.log("adminDirectory.initVar", pInit);
		var aObj = this;
		aObj.container = ( ( pInit != null && typeof pInit.container != "undefined" ) ? pInit.container : "adminDirectory" );
		//aObj.results = ( ( pInit != null && typeof pInit.results != "undefined" ) ? pInit.results : {} );
		aObj.panelAdmin = ( ( pInit != null && typeof pInit.panelAdmin != "undefined" ) ? pInit.panelAdmin : {} );
		aObj.context = ( ( typeof aObj.panelAdmin.context != "undefined" ) ? aObj.panelAdmin.context : {} );
		//aObj.paramsSearch = aObj.panelAdmin;

		//aObj.searchObj = ( ( pInit != null && typeof pInit.searchObj != "undefined" ) ? pInit.searchObj : false );
		mylog.log("adminDirectory.initVar aObj.container", aObj.container);
		if( typeof pInit.panelAdmin.types != "undefined" ){
			aObj.paramsSearch.types =  pInit.panelAdmin.types;
			aObj.setType =  pInit.panelAdmin.types;
		}
		if(notEmpty(pInit.panelAdmin.forced)){
			$.each(pInit.panelAdmin.forced, function(e, v){
				aObj.paramsSearch[e]=v;
			});
		}
		/*aObj.countTotal = 0 ;
		if(typeof aObj.results != "undefined" && typeof aObj.results.count != "undefined"){
			$.each(aObj.results.count, function(key, values){
				aObj.countTotal += values;
			});
		}*/
		
		aObj.initView();
		//aObj.initViewTable();
		//aObj.initPageTable();
		aObj.initTable();
		aObj.initModals();
		//aObj.initFilters(aObj);
		if(typeof aObj.bindCostum == "function")
			aObj.bindCostum(aObj);

		// aObj.filtersObj = ( ( typeof aObj.panelAdmin.paramsFilter != "undefined" ) ? filterObj.init(aObj.panelAdmin.paramsFilter) : null );
		// if(typeof aObj.filtersObj != "undefined" && aObj.filtersObj != null){
		// 	aObj.filtersObj.search = function(){
		// 		aObj.search(0);
		// 	};
		// }
		
	},
	initView : function(){
		mylog.log("adminDirectory.initView");
		var aObj = this;
		var str = "";
		if(typeof aObj.panelAdmin.title != "undefined" && aObj.panelAdmin.title != null)
			str += '<div class="col-xs-12 padding-10 text-center"><h2>'+aObj.panelAdmin.title+'</h2></div>';

			str += '<div class="headerSearchContainerAdmin no-padding col-xs-12"></div>';
			str += '<div class="no-padding col-xs-12 searchObjCSS" id="filters-nav-admin"></div>';
		str += 	'<div class="pageTable col-xs-12 padding-10"></div>';
		
		str +=	'<div class="panel-body">'+
			'<div>'+
				'<table style="table-layout: fixed; word-wrap: break-word;" class="table table-striped table-bordered table-hover directoryTable" id="panelAdmin">'+
					'<thead>'+
						'<tr id="headerTable">'+
						'</tr>'+
					'</thead>'+
					'<tbody class="directoryLines">'+
						
					'</tbody>'+
				'</table>'+
			'</div>'+
		'</div>'+
		'<div class="pageTable col-md-12 col-sm-12 col-xs-12 padding-20"></div>';

		$("#"+this.container).html(str);
	},
	initTable : function(){
		var aObj = this ;
		var str = "" ;
		mylog.log("adminDirectory.initTable ", aObj.actionsStr, aObj.panelAdmin);
		if(	typeof aObj.panelAdmin.table != "undefined" && 
			aObj.panelAdmin.table != null ){
			//mylog.log("adminDirectory.initTable table ", aObj.panelAdmin.table);
			$.each(aObj.panelAdmin.table, function(key, value){ 
				mylog.log("adminDirectory.initTable each ", key, value, (key != "actions"));
				if(key != "actions"){
					mylog.log("adminDirectory.initTable if ");
					if(value === "true")
						str +="<th>"+key+"</th>";
					else if(typeof value != "undefined" && typeof value.name != "undefined"){
						var classCol = ((typeof value.class != "undefined") ? value.class : " text-center ");
						var sort = "";
						if(typeof value.sort != "undefined" && typeof value.sort.field != "undefined"){
							if(typeof value.sort.up != "undefined")
								sort += "<a href='javascript:;' data-sort='up' data-field='"+value.sort.field+"' class='sortAdmin' > <i class='fa fa-arrow-up'></i> </a>" ;
							if(typeof value.sort.down != "undefined")
								sort += "<a href='javascript:;' data-sort='down' data-field='"+value.sort.field+"' class='sortAdmin' > <i class='fa fa-arrow-down'></i> </a>" ;
						}
						str +="<th class='"+classCol+"'>"+value.name+" "+sort+"</th>";
					}
				}
				
			});
			
		}

		if(	typeof aObj.panelAdmin.table != "undefined" && aObj.panelAdmin.actions != null ) {
			var classCol = 'col-xs-1 text-center';
			if(typeof aObj.panelAdmin.table.actions != "undefined" ){
				if(typeof aObj.panelAdmin.table.actions.class != "undefined")
					classCol = aObj.panelAdmin.table.actions.class ;
			}
			str +="<th class='"+classCol+"'>Actions</th>";
		}
		$("#"+aObj.container+" #headerTable").append(str) ;
		coInterface.bindLBHLinks();
	},
	columTable : function(e, id, collection){
		mylog.log("adminDirectory.columTable ", id, collection);
		var str = "" ;
		var aObj = this ;
		if(	typeof aObj.panelAdmin.table != "undefined" && 
			aObj.panelAdmin.table != null ){

			$.each(aObj.panelAdmin.table, function(key, value){
				if(key != "actions"){
					str += `<td class='${key!="name"?"center":""} ${key}'>`;
					if(typeof aObj.values[key] != "undefined" && value === "true") {
						str += aObj.values[key](e, id, collection, aObj);
					} else if(typeof aObj.values[key] != "undefined") {
						str += aObj.values[key](e, id, collection, aObj, value);
					}
					str += '</td>';
				}
			});
		}
		return str ;
	},
	initModals : function(){
		mylog.log("adminDirectory.initModals");
		var aObj = this;
		var str = "";
		if(	typeof aObj.modals != "undefined" && 
			aObj.modals != null && 
			Object.keys( aObj.modals ).length > 0 ) {

			$.each(aObj.modals, function(kM, valM){
				str += aObj.modals[kM](aObj) ;
			});
		}
		$("#"+this.container).after(str);
	},
	initViewTable : function(aObj){
		//var aObj = this;
		mylog.log("adminDirectory.initViewTable", aObj.container);
		$("#"+aObj.container+" #panelAdmin .directoryLines").html("");
		mylog.log("adminDirectory.initViewTable !", aObj.results);
		var sumColumn="";
		var posSumColumn = 0;
		var sum=null;
		var tot="";
		var totalColumn=0;

		//vérifie si une entrée du tableau contribut l'attribut sum pour faire la somme
		$.each(aObj.panelAdmin.table, function(k, v){
			mylog.log("yiyi",Object.keys(v));
			if($.inArray("sum",Object.keys(v))>-1){
				sumColumn=k;
				posSumColumn=Object.keys(aObj.panelAdmin.table).indexOf(k);
			}
		});

		if(Object.keys(aObj.results).length > 0){
			$.each(aObj.results ,function(key, values){
				mylog.log("adminDirectory.initViewTable !!! ", key, values);
				var entry = aObj.buildDirectoryLine( values, values.collection);
				$("#"+aObj.container+" #panelAdmin .directoryLines").append(entry);
				// Addition des valeurs de la colonne
				if(sumColumn){
					if(typeof values[sumColumn]!="Number")
						sum = Number(values[sumColumn]);
					totalColumn = totalColumn + sum ;
				}	
			});	
			//Ligne supplémentaire pour faire la somme
			if(sumColumn){
				tot += '<tr style="height: 20px;"></tr>';
				tot += '<tr style="border-top:solid;border-bottom:solid;"><td style="border-left:none !important;border-right:none !important;font-weight:800;">TOTAL</td</tr>';


				for(var p=0;p<posSumColumn-1;p++){
					tot+='<td style="border-left:none !important;border-right:none !important;"></td>';
				}
				tot += '<td style="border-left:none !important;border-right:none !important;">'+totalColumn+'</td>';
				$("#"+aObj.container+" #panelAdmin .directoryLines").append(tot);
			}		
		}	

		aObj.bindAdminBtnEvents(aObj);
	},
	buildDirectoryLine : function( e, collection, icon ){
		mylog.log("adminDirectory.buildDirectoryLine",  e, collection, icon);
		var aObj = this;
		var strHTML="";
		if( typeof e._id == "undefined" || 
			( (typeof e.name == "undefined" || e.name == "") && 
			  (e.text == "undefined" || e.text == "") ) )
			return strHTML;
		var actions = "";
		var classes = "";
		var id = e._id.$id;
		var status=[];
		strHTML += '<tr id="'+e.collection+id+'" class="'+e.collection+' line">';
			strHTML += aObj.columTable(e, id, e.collection);
			var actionsStr = aObj.actions.init(e, id, e.collection, aObj);
			mylog.log("adminDirectory.buildDirectoryLine actionsStr",  actionsStr);
			if(actionsStr != ""){
				aObj.actionsStr = true;
				mylog.log("adminDirectory.buildDirectoryLine aObj.actionsStr",  aObj.actionsStr);
				strHTML += 	'<td class="center">'+ 
								'<div class="btn-group">'+
								actionsStr +
								'</div>'+
							'</td>';
			}
		strHTML += '</tr>';
		return strHTML;
	},
	getElt : function(id, type){
		mylog.log("adminDirectory.getElt", this, id, type);
		return this.results[id] ;
	},
	setElt : function(elt, id, type, attr){
		mylog.log("adminDirectory.setElt", id, type);
		if(notNull(attr) && typeof this.results[id][attr] !="undefined"){
			this.results[id].attr=elt;
		}else
			this.results[id] = elt;
	},
	values : {
		type : function(e, id, type, aObj, params){
			mylog.log("adminDirectory.values.type", id, type);
			//mylog.log(e);
			var img = "";
			if (e && typeof e.profilThumbImageUrl != "undefined" && e.profilThumbImageUrl!="")
				img = '<img width="50" height="50" alt="image" class="img-circle" src="'+baseUrl+e.profilThumbImageUrl+'"><br/><span class="uppercase bold">'+type+"</span>";
			else{
				var typology= (typeof e.type !="undefined") ? e.type : type ;
				img = '<i class="fa '+icon+' fa-2x"></i> <br/><span class="uppercase bold">'+typology+"</span>";
			}	
			var lbhN = "lbh" ;
			var urlH='#page.type.'+type+'.id.'+id;
			var targetLink="target='_blank'";
			if(typeof params.preview !="undefined" && params.preview){
				lbhN =  "lbh-preview-element";
				targetLink="";
			}
			if(typeof params.notLink !="undefined" && params.notLink){
				lbhN="";
				urlH="javascript:;";
				targetLink="";
			}
			var str = 	'<a href="'+urlH+'" class="col-xs-12 no-padding text-center '+lbhN+'" '+targetLink+'>'+
							img +
						'</a>';
			return str;
		},
		name : function(e, id, type, aObj, params){
			mylog.log("adminDirectory.values.name", id, type, params);
			var str = "";
			var userAvatar = `<i class="fa fa-user" style="border: solid ${"#"+e._id.$id.substring(13, 19)}c9; color:${"#"+e._id.$id.substring(13, 19)};border-radius: 50%;height: 30px;width: 30px;font-family: 'Fontawesome' !important;   font-size: 22px !important;text-align: center !important;"></i>`
			if(type == "answers"){
				var title="Réponse vide";
				if(typeof e.mappingValues != "undefined" && typeof e.mappingValues.name != "undefined" && typeof e.mappingValues.name != null)
					title=e.mappingValues.name;
				mylog.log("adminDirectory.values.name e.mappingValues",e.mappingValues);
				str = '<a href="#answer.index.id.'+id+'" class="" target="_blank">'+title+'</a>';
			} else if (type == "forms"){
				str = '<a href="#form.edit.id.'+id+'" class="" target="_blank">'+e.name+'</a>';
			} else {
				var title;
				if(typeof e.name != "undefined")
					title=e.name;
				else if(typeof e.text != "undefined")
					title=e.text;
				else if(typeof e.title != "undefined")
					title=e.title;
				
				//var lbhN = "lbh" ;
				var lbhN = "" ;
				var urlH='#page.type.'+type+'.id.'+id;
				var targetLink="target='_blank'";
				if(typeof params.preview !="undefined" && params.preview){
					targetLink="";
					lbhN =  "lbh-preview-element";
				}
				if(typeof params.notLink !="undefined" && params.notLink){
					lbhN="";
					targetLink="";
					urlH="javascript:;";
				}

				if (typeof e.profilImageUrl != "undefined" && e.profilImageUrl != "") {
					userAvatar = `<img style="display: inline;border: solid ${"#"+e._id.$id.substring(13, 19)}c9;border-radius: 50%;height: 30px;width: 30px;font-family: 'Fontawesome' !important;   font-size: 22px !important;text-align: center !important;" src="${e.profilImageUrl}" />`
				}
				
				str = '<a href="'+urlH+'" class="padding-left-10 padding-right-10 '+lbhN+'" '+targetLink+'>'+userAvatar+'<span class="padding-left-15">'+title+'</span></a>';
			}
			
			return str;
		},
		category : function(e, id, type, aObj){
			mylog.log("adminDirectory.values.name", id, type);
			//mylog.log(e);
			var category="";
			if(typeof e.category != "undefined")
				category=(typeof tradCategory[e.category] != "undefined") ? tradCategory[e.category] : e.category;
			var str="<span class=''>"+category+"</span>";
			return str;
		},
		section : function(e, id, type, aObj){
			mylog.log("adminDirectory.values.section", id, type);
			//mylog.log(e);
			var section="";
			if(typeof e.section != "undefined")
				section=(typeof tradCategory[e.section] != "undefined") ? tradCategory[e.section] : e.section;
			var str="<span class=''>"+section+"</span>";
			return str;
		},
		subtype : function(e, id, type, aObj){
			mylog.log("adminDirectory.values.subtype", id, type);
			//mylog.log(e);
			var subtype="";
			if(typeof e.subtype != "undefined")
				subtype=(typeof tradCategory[e.subtype] != "undefined") ? tradCategory[e.subtype] : e.subtype;
			var str="<span class=''>"+subtype+"</span>";
			return str;
		},
		creator : function(e, id, type, aObj){
			mylog.log("adminDirectory.values.name", id, type);
			//mylog.log(e);
			var str = '';
			if(typeof e.creator != "undefined"){
				var idCreator, nameCreator;
				if(typeof e.creator == "string"){
					idCreator=e.creator;
					nameCreator=e.creator;
				}else{
					idCreator=e.creator.id;
					nameCreator=e.creator.name;
				}
				str='<a href="#page.type.citoyens.id.'+idCreator+'" class="lbh-preview-element">'+nameCreator+'</a>';	
			}
			else
				str="<span class=''></span>";
			return str;
		},
		tags : function(e, id, type, aObj, className){
			mylog.log("adminDirectory.values.tags", id, type, aObj, className);
			//mylog.log(e);
			var tagsStr="";
			if(notEmpty(e.tags)){
				$.each(e.tags, function(e,v){
					tagsStr+="<span class='badge bg-white text-red shadow2'>"+v+"</span>";
				});
			}
			var str = '<div class="'+(typeof className == "string" ? className : "")+'">'+tagsStr+'</div>';
			return str;
		},
		created : function(e, id, type, aObj){
			mylog.log("adminDirectory.values.name", id, type);
			mylog.log("created date",e.created, typeof e.created);
			var created="";
			if(notEmpty(e.created)){
				if(typeof e.created== "number"){
					ts=new Date(e.created*1000);
				}else if(typeof e.created== "object"){
					ts=new Date(e.created.sec*1000);
				}
				var language=(notEmpty(mainLanguage)) ? mainLanguage :  "fr";
				created=ts.toLocaleDateString(language,{year: 'numeric',month: 'long',day: 'numeric',hour: '2-digit',minute:'2-digit'}) ;//moment(e.created).local().locale('fr').format('DD/MM/Y');
			}
			var str = '<span>'+created+'</span>';
			return str;
		},
		
		description : function(e, id, type, aObj){
			mylog.log("adminDirectory.values.name", id, type);
			//mylog.log(e);
			var description="";
			if(typeof e.description != "undefined")
				description=e.description;
			var str = '<span>'+description+'</span>';
			return str;
		},
		shortDescription : function(e, id, type, aObj){
			mylog.log("adminDirectory.values.name", id, type);
			//mylog.log(e);
			var shortDescription="";
			if(typeof e.shortDescription != "undefined")
				shortDescription=e.shortDescription;
			var str = '<span>'+shortDescription+'</span>';
			return str;
		},
		source : function(e, id, type, aObj){
			mylog.log("adminDirectory.values.source", id, type);
			//mylog.log(e);
			var origin="";
			var source="Communecter";
			if(typeof e.source != "undefined" && typeof e.source.insertOrign != "undefined")
				origin=e.source.insertOrign;
			if(typeof e.source != "undefined" && typeof e.source.key != "undefined")
				source=e.source.key;
				
			var str = '<span>'+origin+' '+source+'</span>';
			return str;
		},
		amount : function(e, id, type, aObj){
			mylog.log("adminDirectory.values.amount", e, id, type);
			var str = '<span >'+e.amount+'</span>';
			return str;
		
		},
		email : function(e, id, type, aObj){
			mylog.log("adminDirectory.values.email", id, type);
			//mylog.log(e);
			if(typeof e.mappingValues != "undefined" && typeof e.mappingValues.email != "undefined" && typeof e.mappingValues.email != null)
				var str=e.mappingValues.email;
			else
				var str = ( ( typeof e.email != "undefined" ) ? e.email : "" );
			return str;
		},
		pdf : function(e, id, type, aObj){
			var urlCostum =  "";
			if(typeof costum != "undefined" && costum != null && typeof costum.slug != "undefined")
				urlCostum = "/slug/"+costum.slug;
			return '<a href="'+baseUrl+'/co2/export/pdfelement/id/'+id+'/type/'+type+urlCostum+'" data-id="'+id+'" data-type="'+type+'" class="margin-right-5 btn btn-default text-red" target="_blank"> <i class="fa fa-file-pdf-o" ></i> PDF</a> ';
		},
		tobeactivated : function(e, id, type, aObj){
			mylog.log("adminDirectory.values.tobeactivated", id, type);
			//mylog.log(e);
			var tobeactivated=( typeof e.roles != "undefined" && typeof e.roles.tobeactivated != "undefined" && e.roles.tobeactivated == true) ? true : false;
			var str = "";
			if(tobeactivated)
				str = "<span class='badge bg-orange'><i class='fa fa-check'></i> "+trad.waiting+"</span>";
			else
				str = "<span class='badge bg-green-k'><i class='fa fa-check'></i> "+trad.validated+"</span>";
			return str;
		},
		validated : function(e, id, type, aObj){
			//mylog.log("adminDirectory.values.validated", id, type);
			//mylog.log(e);
			var isValidated=( typeof e.source != "undefined" && typeof e.preferences.toBeValidated != "undefined" && typeof e.preferences.toBeValidated[costum.slug] != "undefined" && e.preferences.toBeValidated[costum.slug] == true) ? false : true;
			var str = "";
			if(!isValidated)
				str = "<span class='badge bg-orange'><i class='fa fa-check'></i> En attente de validation</span>";
			else
				str = "<span class='badge bg-green-k'><i class='fa fa-check'></i> Groupe validé</span>";
			return str;
		},
		private : function(e, id, type, aObj){
			mylog.log("adminDirectory.values.private", id, type);
			//mylog.log(e);
			var str = "";
			if( typeof e.preferences != "undefined" && 
				typeof e.preferences.private != "undefined" && 
				e.preferences.private === true ){
				str = '<span id="private'+id+'" class="label label-danger"> Privé </span>';
			}else{
				str = '<span id="private'+id+'" class="label label-success"> Public </span>';
			}
			return str;
		},
		scope : function(e, id, type, aObj){
			mylog.log("adminDirectory.values.status", id, type);
			//mylog.log(e);
			var str = "";
			
			if(typeof e.scope != "undefined" && notEmpty(e.scope)){
				$.each(e.scope, function(i, v){
					var value=( typeof v.cityName != "undefined") ? v.cityName : "";
					str += "<span class='badge'>"+value+"</span>";
			
				});
			}else{
				str = "Zone géographique non renseignée";
			}
			return str;
		},
		address : function(e, id, type, aObj){
			var localityHtml="",streetAddress='', postalCode = '', city='';
			if (e.address != null) {
				streetAddress= e.address.streetAddress ? e.address.streetAddress : '';
				postalCode = e.address.postalCode ? e.address.postalCode : '';
				city = e.address.addressLocality ? e.address.addressLocality : '' ;
			}
			localityHtml = (e.streetAddress != '' || e.postalCode!='' || e.city !='') ? streetAddress+' '+postalCode+' '+city : trad.UnknownLocality;
			return localityHtml;
				

		},
		regionDep : function(e, id, type, aObj){
			mylog.log("adminDirectory.values regionDep", id, type, aObj);
			//mylog.log(e);
			var str = "";
			var scopeInjectedDep=[];
			if(typeof e.address != "undefined") 
			{
				if( typeof e.address.level3Name != "undefined" ){
					str += e.address.level3Name;
				}
				
				if(typeof e.address.level4Name != "undefined" ){
					scopeInjectedDep.push(e.address.level4Name);
					str += "<br/>"+e.address.level4Name;
				}
			}
			else if(typeof e.scope != "undefined" && notEmpty(e.scope)) 
			{
				$.each(e.scope, function(i, v){
					if( typeof v.level3Name != "undefined" && $.inArray(v.level3Name, scopeInjectedDep) <= -1){
						if(scopeInjectedDep.length > 0) str+="<br/>";
						str += v.level3Name;
						scopeInjectedDep.push(v.level3Name);
					}
					
					if(typeof v.level4Name != "undefined" && $.inArray(v.level4Name, scopeInjectedDep) <= -1){
						scopeInjectedDep.push(v.level4Name);
						str += "<br/>"+v.level4Name;
						}
				});
			}
			return "<span class=''>"+str+"</span>";
		},
		region : function(e, id, type, aObj){
			mylog.log("adminDirectory.values region", id, type, aObj);
			//mylog.log(e);
			var str = "";
			var scopeInjectedDep=[];
			if(typeof e.address != "undefined") 
			{
				if( typeof e.address.level3Name != "undefined" ){
					str += e.address.level3Name;
				} 
			}
			else if(typeof e.scope != "undefined" && notEmpty(e.scope)) 
			{
				$.each(e.scope, function(i, v){
					if( typeof v.level3Name != "undefined" && $.inArray(v.level3Name, scopeInjectedDep) <= -1){
						if(scopeInjectedDep.length > 0) str+="<br/>";
						str += v.level3Name;
						scopeInjectedDep.push(v.level3Name);
					}
				});
			}
			else if(typeof e.project != "undefined" && typeof e.project.address != "undefined" && typeof e.project.address.level3Name != "undefined" && notEmpty(e.project.address))
			{
				str += e.project.address.level3Name;
			}
			return str;
		},
		departement : function(e, id, type, aObj){
			mylog.log("adminDirectory.values Dep", id, type, aObj);
			//mylog.log(e);
			var str = "";
			var scopeInjectedDep=[];
			if(typeof e.address != "undefined") 
			{ 
				if(typeof e.address.level4Name != "undefined" ){
					scopeInjectedDep.push(e.address.level4Name);
					str += e.address.level4Name;
				}
			}
			else if(typeof e.scope != "undefined" && notEmpty(e.scope)) 
			{
				$.each(e.scope, function(i, v){
					 
					if(typeof v.level4Name != "undefined" && $.inArray(v.level4Name, scopeInjectedDep) <= -1){
						scopeInjectedDep.push(v.level4Name);
						str += v.level4Name;
						}
				});
			}
			else if(typeof e.project != "undefined" && typeof e.project.address != "undefined" && typeof e.project.address.level4Name != "undefined" && notEmpty(e.project.address))
			{
				str += e.project.address.level4Name;
			}
			return "<span class=''>"+str+"</span>";
		},
		status : function(e, id, type, aObj){
			mylog.log("adminDirectory.values.status", id, type);
			//mylog.log(e);
			var value=( typeof e.source != "undefined" && typeof e.source.status != "undefined" && typeof e.source.status[costum.slug] != "undefined" && e.source.status[costum.slug] != null) ? e.source.status[costum.slug] : "";
			mylog.log("adminDirectory.values.value", value);
			var str = "";
			str = "<span class=''>"+value+"</span>";
			return str;
		},
		adhesionDate : function(e, id, type, aObj){
			mylog.log("adminDirectory.values.adhesionDate", id, type);
			//mylog.log(e);
			var value = (typeof e.links != "undefined" && typeof e.links.memberOf !="undefined" && typeof e.links.memberOf[costum.contextId] != "undefined" && typeof e.links.memberOf[costum.contextId].adhesion!="undefined" && costum.communityLinks.members[id].adhesion.startDate) ? (moment(costum.communityLinks.members[id].adhesion.startDate).format("DD / MM / YYYY")): "";
			var str = "";
			str = "<span class=''>"+value+"</span>";
			return str;
		},
		adhesionDuration : function(e, id, type, aObj){
			mylog.log("adminDirectory.values.adhesionDuration", id, type);
			//mylog.log(e);
			//var value=( typeof e.source != "undefined" && typeof e.source.status != "undefined" && typeof e.source.status[costum.slug] != "undefined" && e.source.status[costum.slug] != null) ? e.source.status[costum.slug] : "";
			var value = (typeof e.links != "undefined" && typeof e.links.memberOf !="undefined" && typeof e.links.memberOf[costum.contextId] != "undefined" && typeof e.links.memberOf[costum.contextId].adhesion!="undefined" && costum.communityLinks.members[id].adhesion.duration) ? costum.communityLinks.members[id].adhesion.duration + " mois": "";
			var str = "";
			str = "<span class=''>"+value+"</span>";
			return str;
		},
		adhesionAmount : function(e, id, type, aObj){
			mylog.log("adminDirectory.values.adhesionAmount", id, type);
			//mylog.log(e);
			var value = (typeof e.links != "undefined" && typeof e.links.memberOf !="undefined" && typeof e.links.memberOf[costum.contextId] != "undefined" && typeof e.links.memberOf[costum.contextId].adhesion!="undefined" && costum.communityLinks.members[id].adhesion.amount) ? costum.communityLinks.members[id].adhesion.amount + " €": "";
			var str = "";
			str = "<span class=''>"+value+"</span>";
			return str;
		},
		roles : function(e, id, type, aObj){
			mylog.log("adminDirectory.values.roles", id, type, aObj);
			//mylog.log(e);

			if(typeof e.collection != "undefined" && typeof e.type == "undefined")
				type = e.collection;
			var value= "";
			if( typeof aObj.context != "undefined" &&
				typeof aObj.context.id != "undefined" &&
				typeof aObj.context.collection != "undefined" &&
				typeof e.links != "undefined" && 
				typeof e.links[ links.getConnect( type, aObj.context.collection ) ] != "undefined" && 
				typeof e.links[ links.getConnect( type, aObj.context.collection ) ][ aObj.context.id ] != "undefined" && 
				typeof e.links[ links.getConnect( type, aObj.context.collection ) ][ aObj.context.id ].roles != "undefined" &&
				notEmpty(e.links[ links.getConnect( type, aObj.context.collection ) ][ aObj.context.id ].roles)){
				var i = 0;
				$.each(e.links[ links.getConnect( type, aObj.context.collection ) ][aObj.context.id].roles, function(kR, valR){
					if(i > 0)
						value += ",<br>";
					value += valR;
					i++;
				});
			}
				
			mylog.log("adminDirectory.values.roles value", value);
			var str = "";
			str = "<span class=''>"+value+"</span>";
			return str;
		},
		isInviting : function(e, id, type, aObj){
			mylog.log("adminDirectory.values.isInviting", id, type, aObj);
			//mylog.log(e);
			var value= trad.yes;
			if( typeof aObj.context != "undefined" &&
				typeof aObj.context.id != "undefined" &&
				typeof aObj.context.collection != "undefined" &&
				typeof e.links != "undefined" &&
				typeof links != "undefined" && 
				typeof links.linksTypes != "undefined" &&
				typeof links.linksTypes["citoyens"] != "undefined" &&
				typeof links.linksTypes["citoyens"][aObj.context.collection] != "undefined" &&
				typeof e.links[links.linksTypes["citoyens"][aObj.context.collection] ] != "undefined" && 
				typeof e.links[links.linksTypes["citoyens"][aObj.context.collection] ][aObj.context.id] != "undefined" && 
				typeof e.links[links.linksTypes["citoyens"][aObj.context.collection] ][aObj.context.id].isInviting != "undefined"&& 
				e.links[links.linksTypes["citoyens"][aObj.context.collection] ][aObj.context.id].isInviting === true ) 
				value = trad.no ;
			mylog.log("adminDirectory.values.value", value);
			var str = "";
			str = "<span class=''>"+value+"</span>";
			return str;
		},
		admin : function(e, id, type, aObj){
			mylog.log("adminDirectory.values.admin", id, type, aObj);
			//mylog.log(e);
			var value= trad.no;
			if( typeof aObj.context != "undefined" &&
				typeof aObj.context.id != "undefined" &&
				typeof aObj.context.collection != "undefined" &&
				typeof e.links != "undefined" &&
				typeof links != "undefined" && 
				typeof links.linksTypes != "undefined" &&
				typeof links.linksTypes["citoyens"] != "undefined" &&
				typeof links.linksTypes["citoyens"][aObj.context.collection] != "undefined" &&
				typeof e.links[links.linksTypes["citoyens"][aObj.context.collection] ] != "undefined" && 
				typeof e.links[links.linksTypes["citoyens"][aObj.context.collection] ][aObj.context.id] != "undefined" && 
				typeof e.links[links.linksTypes["citoyens"][aObj.context.collection] ][aObj.context.id].isAdmin != "undefined"&& 
				e.links[links.linksTypes["citoyens"][aObj.context.collection] ][aObj.context.id].isAdmin === true) 
				value = trad.yes ;
			mylog.log("adminDirectory.values.value", value);
			var str = "";
			str = "<span class=''>"+value+"</span>";
			return str;
		},
		project : function(e, id, type, aObj){
			var str = "";
			if(typeof e.project != "undefined"){
				str = '<a href="#page.type.'+e.project.type+'.id.'+e.project.id+'" class="" target="_blank">'+e.project.name+'</a>';
			} 
			return str;		
		},
		organization : function(e, id, type, aObj){
			var str = "";
			if(typeof e.organization != "undefined"){
				str = '<a href="#page.type.'+e.organization.type+'.id.'+e.organization.id+'" class="lbh" target="_blank">'+e.organization.name+'</a>';
			} 
			return str;		
		},
		comment : function(e, id, type, aObj){
			var str = "";
			var nbComment = (typeof e.countComment != "undefined") ? e.countComment : "" ;
			str += 	'<center><a href="javascript:;" class="btn btn-primary openAnswersComment commentBtn" data-id="'+id+'" data-type="'+type+'" >'+
						nbComment+' <i class="fa fa-commenting"></i>'+
						//'<?php echo PHDB::count(Comment::COLLECTION, array("contextId"=>(string)$v['_id'],"contextType"=>Form::ANSWER_COLLECTION)); ?> <i class="fa fa-commenting"></i>'+
					'</a></center>';
			return str; 
		}
	},
	actions : {
		init : function(e, id, type, aObj){
			mylog.log("adminDirectory.actions.init", id, type, aObj);
			//mylog.log(e);
			var str = "";
			if( typeof aObj.panelAdmin.actions != "undefined" && 
				aObj.panelAdmin.actions != null &&
				Object.keys(aObj.panelAdmin.actions).length == 1 ) {
				mylog.log("adminDirectory.actions.init 1");
				 str = aObj.actions.get(e, id, type, false, aObj);
			}else{
				mylog.log("adminDirectory.actions.init 2");
				var actionsStr = aObj.actions.get(e, id, type, true, aObj);
				if(actionsStr != ""){
					str = '<a href="#" data-toggle="dropdown" class="btn btn-danger dropdown-toggle btn-sm">'+
							'<i class="fa fa-cog"></i> <span class="caret"></span>'+
						'</a>'+
						'<ul class="dropdown-menu pull-right dropdown-dark" role="menu">'+
							actionsStr +
						'</ul>';
				}
				
			}
			//mylog.log("adminDirectory.actions.init end ", str);
			return str ;
		},
		get : function(e, id, type, multiple, aObj){
			mylog.log("adminDirectory.actions.get", id, type, multiple, aObj);
			//mylog.log(e);
			var str = "";

			if( typeof aObj.panelAdmin.actions != "undefined" && 
				aObj.panelAdmin.actions != null &&
				Object.keys( aObj.panelAdmin.actions ).length > 0 ){
				$.each(aObj.panelAdmin.actions, function(key, value){

					if(multiple === "true")
						str += "<li>";
					mylog.log("adminDirectory.actions.get key value", key, value, id, type);
					if(key == "validated" && value === "true"){
						str += aObj.actions.validated(e, id, type, aObj);	
					} else if(key == "status" && value === "true"){
						str += aObj.actions.status(e, id, type, aObj);	
					} else if(typeof aObj.actions[key] != "undefined") {
						str += aObj.actions[key](e, id, type, aObj);
					}

					if(multiple === "true")
						str += "</li>";
				});
			}
			
			//mylog.log("adminDirectory.actions.get str ", str);
			return str ;
		},
		pdf : function(e, id, type, aObj){
			mylog.log("adminDirectory.actions.pdf ");
			var urlCostum =  "";
			if(typeof costum != "undefined" && costum != null && typeof costum.slug != "undefined")
				urlCostum = "/slug/"+costum.slug;
			return '<a href="'+baseUrl+'/co2/export/pdfelement/id/'+id+'/type/'+type+urlCostum+'/" data-id="'+id+'" data-type="'+type+'" class="col-xs-12 btn bg-green-k" target="_blank"> <i class="fa fa-file-pdf-o text-red" ></i> PDF </a> ';
		},
		validated : function(e, id, type, aObj){
			var str = "" ;
			var isValidated=( typeof e.preferences != "undefined" && typeof e.preferences.toBeValidated != "undefined" && typeof e.preferences.toBeValidated[costum.slug] != "undefined" && e.preferences.toBeValidated[costum.slug] == true) ? false : true;
			if(!isValidated){
				str ='<button data-id="'+id+'" data-type="'+type+'" data-valid="true" class="col-xs-12 validateSourceBtn btn bg-green-k text-white"><i class="fa fa-check"></i> Valider le groupe</button>';
			} else {
				str ='<button data-id="'+id+'" data-type="'+type+'" data-valid="false" class="col-xs-12 validateSourceBtn btn bg-red-k text-white"><i class="fa fa-trash"></i> Enlever la validation du groupe</button>';
			}
			return str ;
		},
		validatePledge : function(e, id, type, aObj){
		  	mylog.log("adminDirectory.actions validatePledge", e, id, type, aObj);
		  	var str="";
		  	if(typeof e.type != "undefined" && e.type == "pledge"){
		    	str = '<button data-id="'+id+'" data-type="'+type+'" class="margin-right-5 validatePledgeBtn btn bg-green-k text-white"><i class="fa fa-check"></i> Valider (reçu)</button>';
		  	}
		  	return str;
		},
		status : function(e, id, type, aObj){
			mylog.log("adminDirectory.actions.status", id, type);
			//mylog.log(e);
			var str ='<button data-id="'+id+'" data-type="'+type+'" class="col-xs-12 statusBtn btn bg-green-k text-white">Modifier le statut</button>';
			
			return str ;
		},
		private : function(e, id, type, aObj){
			mylog.log("adminDirectory.actions.private", id, type);
			//mylog.log(e);
			var val = false ;
			var labelAct="Rendre privé";
			if( typeof e.preferences != "undefined" && 
				typeof e.preferences.private != "undefined" && 
				e.preferences.private === true ){
				val = true;
				labelAct="Rendre public";
			}

			var str ='<button data-id="'+id+'" data-type="'+type+'" data-private="'+val+'" data-path="preferences.private" class="col-xs-12 privateBtn btn bg-green-k text-white">'+labelAct+'</button>';
			
			return str ;
		},
		roles : function(e, id, type, aObj){
			mylog.log("adminDirectory.actions.roles", id, type);
			//mylog.log(e);
			var str ='<button data-id="'+id+'" data-type="'+type+'" class="col-xs-12 rolesBtn btn bg-green-k text-white">Modifier les roles</button>';
			
			return str ;
		},
		admin : function(e, id, type, aObj){
			mylog.log("adminDirectory.actions.admin", id, type);
			//mylog.log(e);

			var isAdmin = false;
			var val = "Ajouté en tant qu'admin";

			if( typeof aObj.context != "undefined" &&
				typeof aObj.context.id != "undefined" &&
				typeof aObj.context.collection != "undefined" &&
				typeof e.links != "undefined" &&
				typeof links != "undefined" && 
				typeof links.linksTypes != "undefined" &&
				typeof links.linksTypes["citoyens"] != "undefined" &&
				typeof links.linksTypes["citoyens"][aObj.context.collection] != "undefined" &&
				typeof e.links[links.linksTypes["citoyens"][aObj.context.collection] ] != "undefined" && 
				typeof e.links[links.linksTypes["citoyens"][aObj.context.collection] ][aObj.context.id] != "undefined" && 
				typeof e.links[links.linksTypes["citoyens"][aObj.context.collection] ][aObj.context.id].isAdmin != "undefined"&& 
				e.links[links.linksTypes["citoyens"][aObj.context.collection] ][aObj.context.id].isAdmin === true){
				isAdmin = true ;
				val = "Supprimé en tant qu'admin";
			}
			var str ='<button data-id="'+id+'" data-type="'+type+'"  data-isadmin="'+isAdmin+'" class="adminBtn btn bg-green-k text-white">'+val+'</button>';
			
			return str ;
		},
		update : function(e, id, type, aObj){
			mylog.log("adminDirectory.actions.update", id, type, aObj);
			//mylog.log(e);
			var subtype = "";
			if( typeof aObj.panelAdmin != "undefined" && 
				typeof aObj.panelAdmin.actions != "undefined" && 
				typeof aObj.panelAdmin.actions.update != "undefined"){
				if(typeof aObj.panelAdmin.actions.update.subType != "undefined")
					subtype =  ' data-subtype="'+aObj.panelAdmin.actions.update.subType+'" ';
				else if(aObj.panelAdmin.actions.update.subTypeField && e[aObj.panelAdmin.actions.update.subTypeField])
					subtype =  ' data-subtype="'+e[aObj.panelAdmin.actions.update.subTypeField]+'" ';
			}
				
			mylog.log("adminDirectory.actions.update type", type);
			var str ='<button data-id="'+id+'" data-type="'+type+'" '+subtype+' class="col-xs-12 updateBtn btn bg-green-k text-white"><i class="fa fa-pencil"></i> Modifier</button>';
			
			return str ;
		},
		delete : function(e, id, type, aObj){
			mylog.log("adminDirectory.actions.update", id, type, aObj);
			//mylog.log(e);

			var str ='<button data-id="'+id+'" data-type="'+type+'" class="col-xs-12 deleteBtn btn bg-red text-white"><i class="fa fa-trash"></i> Supprimer</button>';
			
			return str ;
		},
		deleteUser : function(e, id, type, aObj){
			mylog.log("adminDirectory.actions.deleteUser", id, type, aObj);
			//mylog.log(e);

			var str ='<button data-id="'+id+'" data-type="'+type+'" class="col-xs-12 deleteUserBtn btn bg-red text-white"><i class="fa fa-trash"></i> Supprimer</button>';
			
			return str ;
		},
		deleteAnswer : function(e, id, type, aObj){
			mylog.log("adminDirectory.actions.deleteAnswer", e, id, type, aObj);

			var str = '<a href="javascript:;" data-id="'+id+'" data-type="'+type+'" '+
						'class="deleteAnswer col-xs-12 btn btn-error" >Supprimer l\'action '+ 
					'</a>';
			return str;
		},
		banUser : function(e, id, type, aObj){
			mylog.log("adminDirectory.actions.banUser", id, type, aObj);
			//mylog.log(e);
			var label = "Bannir l'utilisateur";
			if( typeof e.roles != "undefined" && typeof e.roles.isBanned != "undefined" )
				label = "Enlèver le bannissement";
			var str = '<button data-id="'+id+'" data-type="'+type+'" class="col-xs-12 banUserBtn btn bg-red text-white"><i class="fa fa-trash"></i> '+label+'</button>';
			return str ;
		},
		switchToUser : function(e, id, type, aObj){
			mylog.log("adminDirectory.actions.switch2User", id, type, aObj);
			//mylog.log(e);
			var str='<button data-id="'+id+'" data-type="'+type+'" class="switch2UserThisBtn"><span class="fa-stack"><i class="fa fa-user fa-stack-1x"></i><i class="fa fa-eye fa-stack-2x stack-right-bottom text-danger"></i></span> Switch to this user</button>';
			return str ;
		},
		disconnect : function(e, id, type, aObj){
			mylog.log("adminDirectory.actions.disconnect", id, type, aObj);
			//mylog.log(e);
			var str='<button data-id="'+id+'" data-type="'+type+'" class="btnDisconnect"><span class="fa fa-stack"></span> Supprimer le lien</button>';
			return str ;
		},
		relaunchInviation : function(e, id, type, aObj){
			mylog.log("adminDirectory.actions.disconnect", id, type, aObj);
			//mylog.log(e);
			var tobeactivated=( typeof e.roles != "undefined" && typeof e.roles.tobeactivated != "undefined" && e.roles.tobeactivated == true) ? true : false;
			var str="";
			if(tobeactivated == true)
				str='<button data-id="'+id+'" data-type="'+type+'" class="btnRelaunchInviation"><span class="fa fa-stack"></span> Relancer l\'invitation</button>';
			return str ;
		},
		creatorMailing : function(e, id, type, aObj){
			mylog.log("adminDirectory.actions.disconnect", id, type, aObj);
			//mylog.log(e);
			var str='<button data-id="'+id+'" data-type="'+type+'" class="btnCreatorMailing btn btn-default"><span class="fa fa-send"></span> Contacter le créateur</button>';
			return str ;
		},
		removeSource : function(e, id, type, aObj){
			var str='<button data-id="'+id+'" data-type="'+type+'" data-action="remove" data-setkey="source" class="margin-right-5 setSourceAdmin btn bg-red text-white"><i class="fa fa-ban"></i> Remove from source</button>';
			return str;
		},
		removeReference: function(e, id, type, aObj){
			var str='<button data-id="'+id+'" data-type="'+type+'" data-action="remove" data-setkey="reference" class="margin-right-5 setSourceAdmin btn bg-red text-white"><i class="fa fa-ban"></i> Remove from reference</button> ';
			return str;
		},
		addReference : function(e, id, type, aObj){
			var str='<button data-id="'+id+'" data-type="'+type+'" data-action="add" data-setkey="reference" class="margin-right-5 setSourceAdmin btn bg-green text-white"><i class="fa fa-plus"></i> Add as reference</button>';
			return str;
		}
	},
	events : {
		csv: function(aObj){
			mylog.log("adminDirectory.events.csv ");
			$("#"+aObj.container+" .btnCsv").off().on("click", function(){
				mylog.log("adminDirectory.events.csv.btnCsv ", $(this).data("url"));
				var paramsSearch = searchInterface.constructObjectAndUrl();
				paramsSearch = jQuery.extend(paramsSearch, aObj.paramsSearch);
				ajaxPost(
			        null,
			        $(this).data("url"),
			        paramsSearch,
			        function(data){
			        	var hiddenElement = document.createElement('a');
					    hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(data);
					    hiddenElement.target = '_blank';
					    hiddenElement.download = 'export.csv';
					    hiddenElement.click();
			  		},
			  		function(data){
			  			$("#searchResults").html("erreur");
			  		}
			  	);
			});
		},
		update: function(aObj){
			$("#"+aObj.container+" .updateBtn").off().on("click", function(){
				mylog.log("adminDirectory..updateBtn ", $(this).data("id"), $(this).data("type"));
				dyFObj.editElement($(this).data("type"), $(this).data("id"), $(this).data("subtype"));
			});
		},
		delete : function(aObj){
			$("#"+aObj.container+" .deleteBtn").off().on("click", function(){
				mylog.log("adminDirectory..delete ", $(this).data("id"), $(this).data("type"));
				directory.deleteElement($(this).data("type"), $(this).data("id"), $(this));
			});
		},
		deleteAnswer : function(aObj){
			$("#"+aObj.container+" .deleteAnswer").off().on("click", function(){
				var id = $(this).data("id");
				var type = $(this).data("type");

				bootbox.dialog({
				  title: "Confirmez la suppression de la candidature",
				  message: "<span class='text-red bold'><i class='fa fa-warning'></i> Cette action sera irréversible</span>",
				  buttons: [
				    {
				      label: "Ok",
				      className: "btn btn-primary pull-left",
				      callback: function() {
				      	
				        ajaxPost("",baseUrl+"/survey/co/delete/id/"+id,null,function(data){
				        	if(typeof data.result == "undefined" || !data.result){
				        		toastr.error(data.msg);
				        	}else{
					        	toastr.success("La candidature a bien été supprimée");
					        	 $("#"+type+id).fadeOut();
		                    
					        	//aObj.search.init(aObj);
					        }
				        },"html");
				      }
				    },
				    {
				      label: "Annuler",
				      className: "btn btn-default pull-left",
				      callback: function() {}
				    }
				  ]
				});
			});
		},
		private : function(aObj){
			$("#"+aObj.container+" .privateBtn").off().on("click", function(){
				mylog.log("adminDirectory..privateBtn ", $(this).data("id"), $(this).data("type"));
				var thisObj=$(this);
				var id = $(this).data("id");
				var type = $(this).data("type");
				var elt = aObj.getElt(id, type);
				mylog.log("privatePublicThisBtn click",$(this).data("private"));
				//if private doesn't exist then == not private == fasle > switch to true
				var value = ($(this).data("private") && $(this).data("private") != "undefined") ? null : true;
				if(value){
					$(this).text("Rendre public");
					$(this).data("private", true);
				}
				else{
					$(this).text("Rendre privé");
					$(this).data("private", false);
				}
				var params = {
					collection  : dyFInputs.get( type ).col,
					id    		: id, 
					type  		: type,
					path  		: $(this).data("path"), 
					value 		: value
				};
				globalCtx = params;

				dataHelper.path2Value( params, function(data) {
					if(typeof data.elt.preferences != "undefined"){
					 	elt.preferences = data.elt.preferences ;
					}
					aObj.setElt(elt, id, type) ;
					$("#"+aObj.container+" #"+type+id+" .private").html( aObj.values.private(elt, id, type, aObj));
				} );
			});
		},
		disconnect:function(aObj){
			if($("#"+aObj.container+" .btnDisconnect").length > 0){
				$("#"+aObj.container+" .btnDisconnect").off().on("click", function(){
					var childId = $(this).data("id");
					var childType = $(this).data("type");
					var connectType = links.getConnect( aObj.context.collection, childType);
					links.disconnect(aObj.context.collection,aObj.context.id, childId, childType,connectType, function(){
					$("#"+aObj.container+" #"+childType+childId).fadeOut();
					
						//aObj.search.init(aObj);
						//aObj.search(0);
					});
				});
			}
		},
		creatorMailing : function(aObj){
			$("#"+aObj.container+" .btnCreatorMailing").off().on("click", function(){
				var id = $(this).data("id");
				var type = $(this).data("type");
				var elt = aObj.getElt(id, type) ;
				aObj.mailTo.bootbox(aObj, elt, type, id);
			});
		},
		roles : function(aObj){
			$("#"+aObj.container+" .rolesBtn").off().on("click", function(){
				var id = $(this).data("id");
				var type = $(this).data("type");
				var elt = aObj.getElt(id, type) ;
				//var connectType = ( (typeof aObj.context != "undefined" && typeof aObj.context.collection != "undefined") ? "contributors" : "members" ) ;
				var connectType = links.linksExternalContext[aObj.context.collection][type] ; 
				var roles= [];
				if( typeof aObj.context != "undefined" &&
					typeof aObj.context.id != "undefined" &&
					typeof aObj.context.collection != "undefined" &&
					typeof elt.links != "undefined" && 
					typeof elt.links[connectType] != "undefined" && 
					typeof elt.links[connectType][aObj.context.id] != "undefined" && 
					typeof elt.links[connectType][aObj.context.id].roles != "undefined"){
					roles = elt.links[connectType][aObj.context.id].roles ;
				}
				var form = {
					saveUrl : baseUrl+"/"+moduleId+"/link/removerole/",
					dynForm : {
						jsonSchema : {
							title : tradDynForm.modifyoraddroles+"<br/>"+elt.name,// trad["Update network"],
							icon : "fa-key",
							onLoads : {
								sub : function(){
									$("#ajax-modal .modal-header").removeClass("bg-dark bg-purple bg-red bg-azure bg-green bg-green-poi bg-orange bg-yellow bg-blue bg-turq bg-url")
												  				  .addClass("bg-dark");
								}
							},
							beforeSave : function(){
								mylog.log("beforeSave");
						    },
							afterSave : function(data){
								if( typeof aObj.context != "undefined" &&
									typeof aObj.context.id != "undefined" &&
									typeof aObj.context.collection != "undefined" &&
									typeof elt.links != "undefined" && 
									typeof elt.links[connectType] != "undefined" && 
									typeof elt.links[connectType][aObj.context.id] != "undefined"){
									if(typeof elt.links[connectType][aObj.context.id].roles != "undefined")
										elt.links[connectType][aObj.context.id].roles = data.roles ;
									else
										elt.links[connectType][aObj.context.id]={"roles": data.roles};
								}
								aObj.setElt(elt, id, type) ;
								$("#"+type+id+" .roles").html( aObj.values.roles(elt, id, type, aObj));
								dyFObj.closeForm();
							},
							properties : {
								contextId : dyFInputs.inputHidden(),
								contextType : dyFInputs.inputHidden(), 
								//roles : dyFInputs.tags((costum && typeof costum.roles!="undefined")?costum.roles:rolesList, tradDynForm["addroles"] , tradDynForm["addroles"]),
								roles : dyFInputs.inputSelectGroup(tradDynForm["addroles"] , tradDynForm["addroles"], (costum && typeof costum.roles!="undefined")?costum.roles.reduce((a, v) => ({ ...a, [v]: v}), {}) :rolesList.reduce((a, v) => ({ ...a, [v]: v}), {}), [], {multiple:true}, null, true),
								childId : dyFInputs.inputHidden(), 
								childType : dyFInputs.inputHidden(),
								connectType : dyFInputs.inputHidden()
							}
						}
					}
				};

				var dataUpdate = {
			        contextId : aObj.context.id,
			        contextType : aObj.context.collection,
			        childId : id,
			        childType : type,
			        connectType : links.linksTypes[aObj.context.collection][type],
			        roles : roles
				};

				dyFObj.openForm(form, "sub", dataUpdate);
			});
		},
		admin : function(aObj){
			$("#"+aObj.container+" .adminBtn").off().click(function(e){
				var thisObj=$(this);
				var id = $(this).data("id");
				var type = $(this).data("type");
				var elt = aObj.getElt(id, type) ;
				var connectType = ( (typeof aObj.context != "undefined" && typeof aObj.context.collection != "undefined") ? links.linksTypes["citoyens"][aObj.context.collection] : "members" ) ;
				var isAdmin = $(this).data("isadmin");
				mylog.log("isAdmin", isAdmin);
				var params = {
			    	parentId : aObj.context.id,
			    	parentType : aObj.context.collection,
			   		childId : id,
					childType : type,
					connect : connectType,
					isAdmin : (isAdmin === true ? false : true )
				};
				ajaxPost(
			        null,
			        baseUrl+"/"+moduleId+"/link/updateadminlink/",
			        params,
			        function(data){ 
			          	mylog.log("success data", data);			        
						if(isAdmin === true)
							isAdmin = false ;
						else
							isAdmin = true ;

						if( typeof aObj.context != "undefined" &&
							typeof aObj.context.id != "undefined" &&
							typeof aObj.context.collection != "undefined" &&
							typeof elt.links != "undefined" && 
							typeof links != "undefined" && 
							typeof links.linksTypes != "undefined" &&
							typeof links.linksTypes["citoyens"] != "undefined" &&
							typeof links.linksTypes["citoyens"][aObj.context.collection] != "undefined" &&
							typeof elt.links[links.linksTypes["citoyens"][aObj.context.collection]] != "undefined" && 
							typeof elt.links[links.linksTypes["citoyens"][aObj.context.collection]][aObj.context.id] != "undefined" && 
							typeof elt.links[links.linksTypes["citoyens"][aObj.context.collection]][aObj.context.id]){
							elt.links[links.linksTypes["citoyens"][aObj.context.collection]][aObj.context.id].isAdmin = isAdmin ;
						}

				        aObj.setElt(elt, id, type) ;
						$("#"+type+id+" .admin").html( aObj.values.admin(elt, id, type, aObj));
						thisObj.replaceWith( aObj.actions.admin(elt, id, type, aObj) );
						aObj.bindAdminBtnEvents(aObj);
			  		},
			  		function(data){
			  			$("#searchResults").html("erreur");
			  		}
			  	); 
		    });
		},
		validatePledge : function(aObj){
			mylog.log("validatePledgeBtn");
		    $("#"+aObj.container+" .validatePledgeBtn").off().on("click", function(){
		      $(this).find("i").removeClass("fa-check").addClass("fa-spinner fa-spin");
		      var $this=$(this);
		      var params={
		        id:$(this).data("id"),
		        type:$(this).data("type")
		      };
		      ajaxPost(
		        null,
		        baseUrl+'/co2/crowdfunding/validatepledge/type/'+params.type+'/id/'+params.id,
		        params,
		        function(data){ 
		              mylog.log("validatepledge callback",data);
		              if(data.result==true){
		                toastr.success(data.msg);
		                urlCtrl.loadByHash(location.hash);
		              }
		              else{
		                toastr.error(trad.somethingwentwrong);
		              }
		        }
		      );
		  });
		},    
		status : function(aObj){
			$("#"+aObj.container+" .statusBtn").off().on("click", function(){
				mylog.log("adminDirectory..statusBtn ", $(this).data("id"), $(this).data("type"));
				var id = $(this).data("id");
				var type = $(this).data("type");
				var elt = aObj.getElt(id, type) ;
				var listStatus = {};
				var statusElt = "" ;

				if( typeof aObj.panelAdmin.actions.status.list != "undefined" ){
					$.each(aObj.panelAdmin.actions.status.list, function(key, value){
						listStatus[value] = value;
					});
				}

				if(	typeof elt != "undefined" && 
					typeof elt.source != "undefined" && 
					typeof elt.source.status != "undefined" &&
					typeof costum != "undefined" && 
					typeof costum.slug != "undefined" &&
					typeof elt.source.status[costum.slug] != "undefined" &&
					elt.source.status[costum.slug] ){
					statusElt = elt.source.status[costum.slug];
				}
					

				var form = {
					saveUrl : baseUrl+"/"+moduleId+"/element/updatepathvalue",
					dynForm : {
						jsonSchema : {
							title : "Modifier les Status",
							icon : "fa-key",
							onLoads : {
								sub : function(){
									$("#ajax-modal #collection").val(type);
								}
							},
							afterSave : function(data){
								mylog.dir(data);
								if(typeof data.elt.source != "undefined"){
									elt.source = data.elt.source ;
								}
								aObj.setElt(elt, id, type) ;
								$("#"+type+id+" .status").html( aObj.values.status(elt, id, type, aObj));
								dyFObj.closeForm();
							},
							properties : {
								//collection : dyFInputs.inputHidden(),
								id : dyFInputs.inputHidden(),
								path : dyFInputs.inputHidden(""),
								value : dyFInputs.inputSelect("Choisir un status", "Choisir un status", listStatus, {required : true}),
							}
						}
					}
				};
				var dataUpdate = {
					value : statusElt,
					collection : type,
					id : id,
					path : "source.status."+costum.slug
				};
				mylog.log("adminDirectory..statusBtn form", form);
				dyFObj.openForm(form, "sub", dataUpdate);
			});
		},
		relaunchInviation : function(aObj){
			if($("#"+aObj.container+" .btnRelaunchInviation").length > 0){
				$("#"+aObj.container+" .btnRelaunchInviation").off().on("click", function(){
					ajaxPost(
				        null,
				        baseUrl+"/"+moduleId+"/co2/mailmanagement/relaunchinvitation",
				        {
							id : $(this).data("id")
						},
				        function(data){ 
				          	if ( data && data.result ) {
								var elt = aObj.getElt(params.id, params.type) ;
								if(typeof data.elt.source != "undefined"){
									elt.source = data.elt.source ;
								}
								aObj.setElt(elt, params.id, params.type) ;

								//thisObj.replaceWith( aObj.actions.validated( elt, params.id, params.type, aObj) );
								$("#"+params.type+params.id+" .validated").html( aObj.values.validated( elt, params.id, params.type, aObj));
								//aObj.bindAdminBtnEvents(aObj);
								if(params.valid === "true")
									toastr.success("L'élément est validée");
								else 
									toastr.success("L'élément n'est plus validée");
								
							} else {
								toastr.error("Un problème est survenu lors de la validation");
							}
				  		}
				  	); 
				});
			}
		},
		switchToUser : function(aObj){
			$("#"+aObj.container+" .switch2UserThisBtn").off().on("click",function () 
			{
				//$(this).empty().html('<i class="fa fa-spinner fa-spin"></i>');
		        var btnClick = $(this);
		        var id = $(this).data("id");
		        var urlToSend = baseUrl+"/"+moduleId+"/admin/switchto/uid/"+id;
		        
		        bootbox.confirm("confirm please !!",
	        	function(result) 
	        	{
					if (!result) {
						btnClick.empty().html('<i class="fa fa-thumbs-down"></i>');
						return;
					} else {
						ajaxPost(
					        null,
					        urlToSend,
					        null,
					        function(data){ 
					          	if ( data && data.result ) {
						        	toastr.info("Switched user!!");
						        	location.hash='#page.type.citoyens.id.'+data.id;
								        				window.location.reload();
						        	//window.location.href = baseUrl+"/"+moduleId;
						        } else {
						           toastr.error("something went wrong!! please try again.");
						        }
					  		}
					  	); 
					}
				});

			});
		},
		banUser: function(aObj){
			$("#"+aObj.container+" .banUserBtn").off().on("click",function (){
				mylog.log("adminDirectory..banUserBtn ", $(this).data("id"), $(this).data("type"));
				var id = $(this).data("id");
				var type = $(this).data("type");
				var elt = aObj.getElt(id, type) ;
				var action = "addBannedUser";
				var label = "confirm please !!";
				if( typeof elt.roles != "undefined" && typeof elt.roles.isBanned != "undefined" ){
					action = "revokeBannedUser";
					label = "confirm to accept this user again !!";
				}
				var btnClick = $(this);
					bootbox.confirm(label, function(result) {
						if (result) {
							aObj.changeRole(btnClick, action, "banUser");
						}
				});
			});
		},
		deleteUser : function(aObj){
			$("#"+aObj.container+" .deleteUserBtn").off().on("click",function () {
				mylog.log("deleteThisBtn click");
				var id = $(this).data("id");
				var type = $(this).data("type");
				var url = baseUrl+"/"+moduleId+"/element/delete/id/"+id+"/type/"+type;
				bootbox.confirm("confirm please !!",
		    	function(result){
					if (!result) {
						btnClick.empty().html('<i class="fa fa-thumbs-down"></i>');
						return;
					} else {
						mylog.log("deleteElement", url);
					 	var param = new Object;
					 	ajaxPost(
					        null,
					        url,
					        param,
					        function(data){ 
					          	if(data.result){
									toastr.success(data.msg);
									mylog.log("Retour de delete : "+data.status);
									$("#"+type+id).remove();
                    
									//urlCtrl.loadByHash(location.hash);
						    	}else{
						    		toastr.error(data.msg);
						    	}
					  		},
					  		function(data){
						    	toastr.error("Something went really bad ! Please contact the administrator.");
						    }
					  	); 
					}
				});
			});
		},
		validated : function(aObj){
			$("#"+aObj.container+" .validateSourceBtn").off().on("click", function(){
				//$(this).find("i").removeClass("fa-check").addClass("fa-spinner fa-spin");
				var thisObj=$(this);
				var params={
					id:$(this).data("id"),
					type:$(this).data("type"),
					valid:$(this).data("valid")
				};
				if(!params.valid){
					$(this).removeClass("bg-red-k").addClass("bg-green-k").html("<i class='fa fa-check'></i> Valider");
					$(this).data("valid", true);
				}
				else{
					$(this).removeClass("bg-green-k").addClass("bg-red-k").html("<i class='fa fa-trash'></i> Enlever la validation");
					$(this).data("valid", false);
				}
				mylog.log("adminDirectory.params", params);
				ajaxPost(
			        null,
			        baseUrl+"/co2/admin/validategroup",
			        params,
			        function(data){ 
			          	if ( data && data.result ) {
							//mylog.log("validateSourceBtn data", data, params.id, params.type);

							var elt = aObj.getElt(params.id, params.type) ;
							if(typeof data.elt.source != "undefined"){
								elt.source = data.elt.source ;
							}
							aObj.setElt(elt, params.id, params.type) ;

							//thisObj.replaceWith( aObj.actions.validated( elt, params.id, params.type, aObj) );
							$("#"+params.type+params.id+" .validated").html( aObj.values.validated( elt, params.id, params.type, aObj));
//							aObj.bindAdminBtnEvents(aObj);
							if(params.valid === "true")
								toastr.success("L'élément est validée");
							else 
								toastr.success("L'élément n'est plus validée");
							
						} else {
							toastr.error("Un problème est survenu lors de la validation");
						}
				    }
			  	); 
			});
		},
		generateAAP : function(aObj){
			$("#"+aObj.container+" .generateAAPBtn").off().on("click", function(){
				mylog.log("adminDirectory..generateAAPBtn ", $(this).data("slug") );
				var slug = $(this).data("slug");
				ajaxPost(null, baseUrl+"/survey/co/index/id/"+slug+"/copy/ficheAction", null, function(){toastr.success("Le territoire peut désormais ajouter des actions")});
			});
		},
		privateanswer: function(aObj){
			$("#"+aObj.container+" .privateAnswerBtn").off().on("click", function(){
				mylog.log("adminDirectory..privateAnswerBtn ", $(this).data("id"), $(this).data("type"));
				var thisObj=$(this);
				var id = $(this).data("id");
				var type = $(this).data("type");
				var parentid = $(this).data("parentid");
				var parenttype = $(this).data("parenttype");
				var elt = aObj.getElt(parentid, parenttype) ;
				mylog.log("adminDirectory..privateAnswerBtn elt", elt);
				mylog.log("privatePublicThisBtn click",$(this).data("private"));
				//if private doesn't exist then == not private == fasle > switch to true
				var value = ($(this).data("private") && $(this).data("private") != "undefined") ? null : true;
				if(value){
					$(this).text("Rendre public");
					$(this).data("private", true);
				}
				else{
					$(this).text("Rendre privé");
					$(this).data("private", false);
				}
				var params = {
					collection  : dyFInputs.get( type ).col,
					id    		: id, 
					type  		: type,
					path  		: $(this).data("path"), 
					value 		: value
				};
				globalCtx = params;
				dataHelper.path2Value( params, function(data) { 
					if(typeof data.elt.preferences != "undefined"){
						elt.project.preferences = data.elt.preferences ;
					}
					aObj.setElt(elt, parentid, parenttype) ;
					$("#"+parenttype+parentid+" .privateanswer").html( aObj.values.privateanswer(elt, parentid, parenttype, aObj));
					//thisObj.replaceWith( aObj.actions.privateanswer(elt, parentid, parenttype, aObj) );
					//aObj.bindAdminBtnEvents(aObj);
				} );
			});
		},
		manageSourceData : function(aObj){
			mylog.log("bindReferenceBtnEvents");
			$(".setSourceAdmin").off().on("click", function(){
				var $btnClick=$(this);
				if($btnClick.data("setkey")=="source" && $btnClick.data("action")=="remove"){
					bootbox.confirm("BE carefull, remove a source from an element is not revokable !!<br/>Are you sure to continue ?", function(result) {
						if (result) {
							aObj.setSourceDataAction(aObj, $btnClick);
						}
					});
				}else{
					aObj.setSourceDataAction(aObj, $btnClick);
				}	
			});
		} 
	},
	setSourceDataAction : function(aObj, $btnClick){
		var action=$btnClick.data("action");
		var setKey=$btnClick.data("setkey");
		var params={
			id:$btnClick.data("id"),
			type:$btnClick.data("type")
		};
		if(typeof costum != "undefined" && notNull(costum)){
			params.origin="costum";
			params.sourceKey=(typeof costum.isTemplate !="undefined" && costum.isTemplate) ? costum.contextSlug : costum.slug;
		} 
		ajaxPost(
	        null,
	        baseUrl+"/"+moduleId+"/admin/setsource/action/"+action+"/set/"+setKey,
	        params,
	        function(data){ 
				if ( data && data.result ) {
		        	toastr.success(data.msg);
		        	$("#"+params.type+params.id).fadeOut();
		        	countB=parseInt($("#admin-count-"+setKey).text());
		        	if(action=="remove")
		        		countB--;
		        	else
		        		countB++;
		        	$("#admin-count-"+setKey).text(countB);
		        	//window.location.href = baseUrl+"/"+moduleId;
		        } else {
		           toastr.error("something went wrong!! please try again.");
		        }

	        }
		);
	},
	bindAdminBtnEvents : function(aObj){
		coInterface.bindLBHLinks();
		if( typeof aObj.panelAdmin.actions != "undefined" && 
				aObj.panelAdmin.actions != null &&
				Object.keys( aObj.panelAdmin.actions ).length > 0 ){
				mylog.log("adminDirectory init events",  aObj.panelAdmin.actions);
				$.each(aObj.panelAdmin.actions, function(key, value){
					if(typeof value.event != "undefined" && aObj.events[value.event] != "undefined")
						aObj.events[value.event](aObj);
					else if(typeof aObj.events[key] != "undefined")
						aObj.events[key](aObj);
				});
		}
		mylog.log("adminDirectory.bindAdminBtnEvents");
		if($("#"+aObj.container+" #btnInvite").length > 0){
			$("#"+aObj.container+" #btnInvite").off().on("click", function(){
				var id = $(this).data("id");
				var type = $(this).data("type");
				smallMenu.openAjaxHTML(baseUrl+'/co2/element/invite/type/'+type+'/id/'+id);
			});
		}

		if($("#"+aObj.container+" .commentBtn").length > 0){
			$("#"+aObj.container+" .commentBtn").off().on("click", function(){
				mylog.log("adminDirectory..updateBtn ", $(this).data("id"), $(this).data("type"));
				var id = $(this).data("id");
				var type = $(this).data("type");
				var elt = aObj.getElt(id, type) ;
				
				var modal = bootbox.dialog({
			        message: '<div class="content-risk-comment-tree"></div>',
			        title: "Fil de commentaire du projet",
			        buttons: [
			        
			          {
			            label: "Annuler",
			            className: "btn btn-default pull-left",
			            callback: function() {
			              
			            }
			          }
			        ],
			        onEscape: function() {
			          modal.modal("hide");
			        }
			    });
				modal.on("shown.bs.modal", function() {
				  $.unblockUI();
				  	getAjax(".content-risk-comment-tree",baseUrl+"/"+moduleId+"/comment/index/type/"+type+"/id/"+id,
					function(){  //$(".commentCount").html( $(".nbComments").html() ); 
					},"html");
				});
			});
		}

	},
	changeRole : function(button, action, fAction) {
		mylog.log(button," click");
		var aObj = this ;
	    var id = button.data("id");
		var type = button.data("type");
		var elt = aObj.getElt(id, type) ;
	    var params ={
	    	type:type,
	    	id:id,
	    	action:action
	    };

	    var urlToSend = baseUrl+"/"+moduleId+"/element/updatestatus";
	    var res = false;
	    ajaxPost(
	        null,
	        urlToSend,
	        params,
	        function(data){ 
	          	if ( data && data.result === true ) {
		        	toastr.success("Change has been done !!");
		        	if( typeof data.elt != "undefined" &&
						typeof data.elt.roles != "undefined"){
						elt.roles = data.elt.roles ;
					}
			        aObj.setElt(elt, id, type) ;
					//$("#"+type+id+" .admin").html( aObj.values.admin(elt, id, type, aObj));
					button.replaceWith( aObj.actions[fAction](elt, id, type, aObj) );
		        } else {
		           toastr.error("Something went wrong!! please try again. " + data.msg);
		        }
		    }
	  	); 
	},
	mailTo : {
		initMessage : true,
		initObject : true,
		initMail: function(elt){
			if(typeof elt.creator != "undefined" && elt.creator.email)
				return elt.creator.email;
			else
				return "";
		},
		defaultObject : function(elt){
			return "[co] Modération avant validation de "+elt.name;
		},
		defaultRedirect : function(elt, type, id){
			return "<a href='"+baseUrl+"/#page.type."+type+".id."+id+"' target='_blank'>Retrouvez la page "+elt.name+" en cliquant sur ce lien</a>";
		},
		defaultMessage : function(elt, type, id){
			var nameContact=(typeof elt.creator != "undefined" && elt.creator.name) ? elt.creator.name : "";
			var str="Bonjour"+((notEmpty(nameContact)) ? " "+nameContact : "")+",\n"+
				"Le contenu que vous avez publié demande d'être approfindi : \n"+
				" - Référencer l'adresse\n"+
				" - Ajouter la description, des mots clés, une image\n"+
				" - Les dates ne sont pas cohérentes\n"+
				"Encore un petit effort et vos points seront attribués";
			return str;
			//$("#send-mail-admin #message-email").text(str);
		},
		bootbox : function(aObj, elt, type, id){
			bootbox.dialog({
		        onEscape: function() {},
		        message: '<div id="send-mail-admin" class="row">  ' +
		            '<div class="col-xs-12"> ' +
		            	'<span>Email</span> ' +
		            	'<input type="text" id="contact-email" class="col-xs-12" value="'+aObj.mailTo.initMail(elt)+'"/>'+
		            '</div>'+
		            '<div class="col-xs-12"> ' +
		            	'<span>Object</span> ' +
		            	'<input type="text" id="object-email" class="col-xs-12" value="'+aObj.mailTo.defaultObject(elt)+'"/>'+
		            '</div>'+
		            '<div class="col-xs-12"> ' +
		            	'<span>Message</span> ' +
		            	'<textarea id="message-email" class="col-xs-12 text-dark" style="min-height:250px;">'+aObj.mailTo.defaultMessage(elt, type, id)+'</textarea>'+
		            '</div>'+
		            '</div>',
		        buttons: {
		            success: {
		                label: "Ok",
		                className: "btn-primary",
		                callback: function () {
		                	aObj.mailTo.sendMail(aObj, elt, type, id);
		                }
		            },
		            cancel: {
		            	label: trad["cancel"],
		            	className: "btn-secondary",
		            	callback: function() {}
		            }
		        }
		    });
		},
		sendMail : function(aObj, elt, type, id){
			var msg="<span style='white-space: pre-line;'>"+$("#send-mail-admin #message-email").text()+"<br/></span>"+
				aObj.mailTo.defaultRedirect(elt, type, id);
				
			var params={
				tplMail : $("#send-mail-admin #contact-email").val(),
				tpl : "basic",
				tplObject : $("#send-mail-admin #object-email").val(),
				html: msg
			};
			ajaxPost(
		        null,
		        baseUrl+"/co2/mailmanagement/createandsend",
		        params,
		        function(data){ 
		          	toastr.success("Le mail a été envoyé avec succès");
				}
		  	); 
		}
	}
};