(function ($) {
    $(function () {
        $(document).on('click', '.be-btn.be-btn-ripple', function (__e) {
            var bounding_rect = this.getBoundingClientRect();
            var x = __e.clientX - bounding_rect.left;
            var y = __e.clientY - bounding_rect.top;

            var rippler = document.createElement('span');
            rippler.classList.add('btn-ripple');
            rippler.style.left = x + 'px';
            rippler.style.top = y + 'px';
            this.appendChild(rippler);
            setTimeout(function () {
                rippler.remove();
            }, 300);
        });
    });
})(jQuery);