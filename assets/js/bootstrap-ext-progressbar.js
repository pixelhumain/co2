(function($) {
    $(function() {
        $('.be-circular-progress').each(function() {
            const self = $(this);
            const target = parseInt(self.data('value'));
            const colors = {
                'be-circular-progress-primary': '#9bc125'
            }
            for(const key in colors) {
                if(self.hasClass(key)) {
                    let calculator = 0;
                    const speed = 10;
                    const load_progress = setInterval(function() {
                        if(calculator === 100) {
                            self.css('background', colors[key]);
                            self.find('.be-circular-progress-label').text(`100%`);
                            clearInterval(load_progress);
                        } else {
                            self.css('background', `conic-gradient(${colors[key]} ${calculator * 3.6}deg, #dedede 0deg)`);
                            self.find('.be-circular-progress-label').text(`${calculator}%`);
                            calculator++;
                        }
                        if(calculator > target) clearInterval(load_progress);
                    }, speed);
                }
            }
        });
    });
})(jQuery)