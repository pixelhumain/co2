// cd-int = corner dev interactive
(function (X, $) {
	var socketConfigurationEnabled = X.coWsConfig && X.coWsConfig.enable && X.coWsConfig.serverUrl && X.coWsConfig.pingActionManagement;
	var ws;

	if (socketConfigurationEnabled) {
		ws = X.wsCO;
		if ((!X.wsCO || !wsCO.connected) && X.io)
			ws = X.io.connect(X.coWsConfig.serverUrl)
	}
	// handle drag&drop
	var offset = {
		x: 0,
		y: 0,
	};
	var substitute_dom = null;
	function getVerticalDragAfterElement(container, y) {
		var draggableElements = Array.from(container.querySelectorAll('.cd-int-action-list-container:not(.dragging):not(.substitute)'));
		return draggableElements.reduce(function (closest, child) {
			var box = child.getBoundingClientRect();
			var offset = y - box.top - box.height / 2;
			if (offset < 0 && offset > closest.offset) {
				return {
					offset: offset,
					element: child
				}
			} else {
				return closest
			}
		}, {
			offset: Number.NEGATIVE_INFINITY,
			element: null
		}).element;
	}
	if (typeof X.cd_int !== 'function')
		X.cd_int = function (php) {
			var remote_url,
				path2value_option;
			var INPUT_ACTION = {
				APPEND: 'append',
				CREATE: 'create'
			};
			var MODEL = {
				PERSON: 'citoyens',
				ACTION: 'actions',
			};

			php = $.extend({}, { server_url: X.baseUrl }, php);
			remote_url = php.server_url;
			path2value_option = {
				url: php.server_url
			};
			function handle_leave(e) {
				var container = $(this).parents('.cd-int-action-list-container');

				if (substitute_dom) {
					container.css({
						position: '',
						top: '',
						left: '',
						width: '',
						height: '',
						zIndex: '',
					}).removeClass('dragging');
					substitute_dom.before(container);
					substitute_dom.detach()
						.remove();
					var position_data = {
						action: container.data('id'),
						group: $('.cd-int-status-filter li.active .cd-int-status-item').data('target'),
						emiter: X.wsCO && X.wsCO.id ? X.wsCO.id : null,
						component: 'cd-int',
						server_url: php.server_url
					};
					var after = $('.cd-int-action-list-container');
					var container_index = after.index(container);
					var data_parent = JSON.stringify(container.data('parent'));
					after = after.filter(function (i, dom) {
						return (i > container_index
							&& JSON.stringify($(dom).data('parent')) === data_parent);
					});
					if (after.length)
						position_data.after = after.eq(0).data('id');
					coInterface.actions.update_action_position(position_data).then();
				}
				substitute_dom = null;
				offset = {
					x: 0,
					y: 0,
				};
			}
			$(document)
				.off('mousedown', '.cd-int-drag-drop')
				.off('mousemove', '.cd-int-drag-drop')
				.off('mouseleave', '.cd-int-drag-drop')
				.off('mouseup', '.cd-int-drag-drop')
				.on('mousedown', '.cd-int-drag-drop', function (e) {
					var container = $(this).parents('.cd-int-action-list-container');
					if (!container.length)
						return;
					var orig = e.originalEvent;
					var bcr = container.get(0).getBoundingClientRect();

					if (e.originalEvent.button === 2)
						return;
					offset = {
						x: orig.clientX - bcr.x,
						y: orig.clientY - bcr.y,
					};
					substitute_dom = container.clone();
					substitute_dom.addClass('substitute')
						.find('*')
						.css('color', 'transparent');
					substitute_dom.find('.cd-int-action-list')
						.css('background', '#777')
					container.before(substitute_dom.hide());
				}).on('mousemove', '.cd-int-drag-drop', function (e) {
					var orig = e.originalEvent;
					var container = $(this).parents('.cd-int-action-list-container');

					if (!substitute_dom)
						return;
					substitute_dom.show();
					container.css({
						position: 'fixed',
						top: orig.clientY - offset.y,
						left: orig.clientX - offset.x,
						width: container.outerWidth(true),
						height: container.outerHeight(true),
						zIndex: 20,
					}).addClass('dragging');
					var containerVanillaDom = document.querySelector('.cd-popover-content');
					var afterElementVanillaDom = getVerticalDragAfterElement(containerVanillaDom, orig.clientY);
					if (!afterElementVanillaDom)
						$(containerVanillaDom).append(substitute_dom);
					else
						$(afterElementVanillaDom).before(substitute_dom);
				}).on('mouseleave', '.cd-int-drag-drop', handle_leave)
				.on('mouseup', '.cd-int-drag-drop', handle_leave);
			// handle drag&drop

			function xhr_action_detail_modal(action) {
				return new Promise(function (resolve) {
					const url = remote_url + '/costum/project/action/request/action_detail_html';
					const post = {
						id: action,
						connectedUser: userId,
						server_url: remote_url
					};
					ajaxPost(null, url, post, resolve, null, 'text');
				});
			}
			function xhr_get_contributors(action) {
				return new Promise(function (resolve) {
					var url = remote_url + "/costum/project/action/request/all_contributors";
					ajaxPost(null, url, {
						id: action
					}, resolve);
				});
			}
			function html_content_loader() {
				return ('<div class="cd-int-content-loader"><i class="fa fa-spinner fa-spin"></i></div>');
			}
			function html_add_action() {
				return (
					'<div class="cd-int-create-action">' +
					'	<button class="cd-int-new-action">' +
					'	    <span class="fa fa-plus"></span> Ajouter une action' +
					'	</button>' +
					'	<button class="cd-int-close-prompt">' +
					'	    <span class="fa fa-times"></span>' +
					'	</button>' +
					'</div>'
				);
			}
			function clone_action_list(action, clone) {
				var container;
				var contributor_list = [];
				var participate_btn = '<span class="fa fa-link"></span>';
				var task_list = [];
				var status_list = {
					discuter: 'à discuter',
					todo: 'à faire',
					tracking: 'en cours',
					totest: 'à tester',
					done: 'terminé'
				};
				var status_dom = $('.cd-int-status-filter li.active .cd-int-status-item');
				function done_task(task) {
					return (typeof task.checked !== 'undefined' && task.checked);
				}
				if (typeof action.links !== 'undefined' && typeof action.links.contributors === 'object')
					$.each(action.links.contributors, function (id, contrib) {
						if (contrib.type === MODEL.PERSON)
							contributor_list.push(id);
					});
				if (contributor_list.includes(userId))
					participate_btn = '<span class="fa fa-unlink"></span>';
				if (typeof action.tasks === 'object' && Array.isArray(action.tasks))
					task_list = action.tasks;
				task_list = task_list.map(task => {
					if (typeof task.createdAt === 'object' && task.createdAt)
						task.createdAt = moment.unix(task.createdAt.sec).format();
					return (task);
				});
				container = clone.clone();
				container.data('parent', { id: action.parentId, type: action.parentType });
				container.find('.cd-int-title span.text').text(action.name);
				container.find('.cd-int-footer-action.comment')
					.attr('data-title', action.name.replace(/"/g, "'"))
					.empty()
					.html(
						typeof action.commentCount !== 'undefined' ? action.commentCount : 0 +
							' <span class="fa fa-commenting"></span>'
					);
				container.find('.cd-int-footer-action.task-list-resume')
					.text(task_list.filter(done_task).length + '/' + task_list.length);
				container.find('.cd-int-footer-action.contribution-action')
					.attr('data-contribution', contributor_list.includes(userId))
					.empty()
					.html(participate_btn)
					.next()
					.empty()
					.html(
						contributor_list.length +
						' <span class="fa fa-users"></span>'
					);
				container.find('.cd-int-contributor-list')
					.empty()
					.html(
						contributor_list.map(function (id) {
							return (
								'<li class="cd-int-contributor-info" data-id="' + id + '" data-status="loading">' +
								'    <div class="cd-int-contributor-image">' +
								'        <img src="' + remote_url + '/plugins/lightbox2/img/loading.gif" alt="Contributor">' +
								'    </div>' +
								'    <p class="cd-int-contributor-name">Chargement</p>' +
								'</li>'
							);
						}).join('')
					);
				if (container.find('.cd-int-contributors').css('display') === 'block')
					xhr_get_contributors(action.id).then(function (contributor_list) {
						$.each(contributor_list, function (_, contributor) {
							var info_container_dom = $('.cd-int-contributor-info[data-id="' + contributor.id + '"]');

							info_container_dom.removeAttr('data-status')
							info_container_dom.find('img')
								.attr('src', contributor.image)
								.attr('alt', contributor.name);
							info_container_dom.find('.cd-int-contributor-name')
								.text(contributor.name);
						});
					});
				container.find('.cd-int-statuses-list')
					.empty()
					.html(
						Object.keys(status_list).filter(function (status) {
							return (status !== status_dom.data('target'));
						}).map(function (status) {
							return (
								'<li class="cd-int-statuses-item"' +
								'	data-target="' + status + '"' +
								'	data-action="' + action.id + '">' +
								status_list[status] +
								'</li>'
							);
						}).join('')
					);
				container.find('.cd-int-task-list-resume')
					.empty()
					.html(
						task_list.map(function (task, i) {
							var is_contributor = false;
							var task_name_class = ['cd-int-task-name'];

							if (typeof task.contributors === 'object'
								&& typeof task.contributors[userId] === 'object'
								&& typeof task.contributors[userId].type === 'string'
								&& task.contributors[userId].type === MODEL.PERSON)
								is_contributor = true;
							if (typeof task.checked === 'boolean' && task.checked)
								task_name_class.push('checked');
							return (
								'<li class="cd-int-task-item" data-index="' + i + '" data-action="' + action.id + '">' +
								'	<span class="' + task_name_class.join(' ') + '" data-index="' + i + '">' +
							'		<input type="checkbox" name="' + i + '" ' + (task_name_class.includes('checked') ? 'checked' : '') + ' /> ' +
										task.task + '</span>' +
								'	<div class="cd-int-task-actions">' +
								'		<button class="cd-int-task-action" data-target="contribution" data-action="' +
								(is_contributor ? 'unset' : 'set') + '" data-index="' + i + '">' +
								'			<i class="fa fa-user-' +
								(is_contributor ? 'times' : 'plus') + '"></i>' +
								'		</button>' +
								'		<button class="cd-int-task-action" data-target="delete" data-index="' + i + '">' +
								'			<i class="fa fa-trash"></i>' +
								'		</button>' +
								'	</div>' +
								'</li>'
							);
						}).join('')
					);
				container.find('.cd-int-task-item').each(function () {
					var self = $(this);
					var index = parseInt(self.data('index'));

					self.data('storage', task_list[index]);
				});
				return (container);
			}
			function html_action_list(action) {
				var container;
				var contributor_list = [];
				var participate_btn = '<span class="fa fa-link"></span>';
				var task_list = [];
				var status_list = {
					discuter: 'à discuter',
					todo: 'à faire',
					tracking: 'en cours',
					totest: 'à tester',
					done: 'terminé'
				};
				var status_dom = $('.cd-int-status-filter li.active .cd-int-status-item');

				function done_task(task) {
					return (typeof task.checked !== 'undefined' && task.checked);
				}
				if (typeof action.links !== 'undefined' && typeof action.links.contributors === 'object')
					$.each(action.links.contributors, function (id, contrib) {
						if (contrib.type === MODEL.PERSON)
							contributor_list.push(id);
					});
				if (contributor_list.includes(userId))
					participate_btn = '<span class="fa fa-unlink"></span>';
				if (typeof action.tasks === 'object' && Array.isArray(action.tasks))
					task_list = action.tasks;
				task_list = task_list.map(task => {
					if (typeof task.createdAt === 'object' && task.createdAt)
						task.createdAt = moment.unix(task.createdAt.sec).format();
					return (task);
				});
				container = $('<div>', {
					class: "cd-int-action-list-container",
					data: {
						parent: {
							id: action.parentId,
							type: action.parentType
						}
					}
				});
				container.attr("data-id", action.id);
				container.append(
					'<div data-id="' + action.id + '" class="cd-int-action-list">' +
					'    <span class="cd-int-title" data-id="' + action.id + '">' +
					'		<i class="fa fa-arrows cd-int-drag-drop"></i>' +
					'		<span class="text cd-int-title-tag">' + action.name + '</span>' +
					'		<i class="fa fa-angle-down cd-int-more-less"></i>' +
					'	</span>' +
					'   <div class="cd-int-footer" style="display: none">' +
					'        <button' +
					'            class="cd-int-footer-action archive"' +
					'			data-id="' + action.id + '"' +
					'        >' +
					'            <span class="fa fa-archive"></span>' +
					'        </button>' +
					'        <button' +
					'            class="cd-int-footer-action status"' +
					'			data-id="' + action.id + '"' +
					'        >' +
					'            <span class="fa fa-tasks"></span>' +
					'        </button>' +
					'        <button' +
					'            class="cd-int-footer-action comment"' +
					'			data-id="' + action.id + '"' +
					'			data-title="' + action.name.replace(/"/g, "'") + '"' +
					'        >' + (typeof action.commentCount !== 'undefined' ? action.commentCount : 0) +
					'            <span class="fa fa-commenting"></span>' +
					'        </button>' +
					'        <button' +
					'            class="cd-int-footer-action task-list-resume"' +
					'			data-id="' + action.id + '"' +
					'        >' + task_list.filter(done_task).length + '/' + task_list.length + '</button>' +
					'        <button' +
					'            class="cd-int-footer-action contribution-action"' +
					'            data-contribution="' + contributor_list.includes(userId) + '"' +
					'            data-id="' + action.id + '"' +
					'        >' +
					participate_btn +
					'        </button>' +
					'        <button class="cd-int-footer-action contributors-preview" data-id="' + action.id + '">' + contributor_list.length +
					'            <span class="fa fa-users"></span>' +
					'        </button>' +
					'    </div>' +
					'    <div class="cd-int-contributors">' +
					'        <ul class="cd-int-contributor-list">' +
					contributor_list.map(function (id) {
						return (
							'<li class="cd-int-contributor-info" data-id="' + id + '" data-status="loading">' +
							'    <div class="cd-int-contributor-image">' +
							'        <img src="' + remote_url + '/plugins/lightbox2/img/loading.gif" alt="Contributor">' +
							'    </div>' +
							'    <p class="cd-int-contributor-name">Chargement</p>' +
							'</li>'
						);
					}).join('') +
					'		</ul>' +
					'	</div>' +
					'    <div class="cd-int-statuses">' +
					'        <ul class="cd-int-statuses-list">' +
					Object.keys(status_list).filter(function (status) {
						return (status !== status_dom.data('target'));
					}).map(function (status) {
						return (
							'<li class="cd-int-statuses-item"' +
							'	data-target="' + status + '"' +
							'	data-action="' + action.id + '">' +
							status_list[status] +
							'</li>'
						);
					}).join('') +
					'		</ul>' +
					'	</div>' +
					'    <div class="cd-int-tasks">' +
					'        <ul class="cd-int-task-list-resume">' +
					task_list.map(function (task, i) {
						var is_contributor = false;
						var task_name_class = ['cd-int-task-name'];

						if (typeof task.contributors === 'object'
							&& typeof task.contributors[userId] === 'object'
							&& typeof task.contributors[userId].type === 'string'
							&& task.contributors[userId].type === MODEL.PERSON)
							is_contributor = true;
						if (typeof task.checked === 'boolean' && task.checked)
							task_name_class.push('checked');
						return (
							'<li class="cd-int-task-item" data-index="' + i + '" data-action="' + action.id + '">' +
							'	<span class="' + task_name_class.join(' ') + '" data-index="' + i + '">' +
							'		<input type="checkbox" name="' + i + '" ' + (task_name_class.includes('checked') ? 'checked' : '') + ' /> ' +
										task.task + '</span>' +
							'	<div class="cd-int-task-actions">' +
							'		<button class="cd-int-task-action" data-target="contribution" data-action="' +
							(is_contributor ? 'unset' : 'set') + '" data-index="' + i + '">' +
							'			<i class="fa fa-user-' +
							(is_contributor ? 'times' : 'plus') + '"></i>' +
							'		</button>' +
							'		<button class="cd-int-task-action" data-target="delete" data-index="' + i + '">' +
							'			<i class="fa fa-trash"></i>' +
							'		</button>' +
							'	</div>' +
							'</li>'
						);
					}).join('') +
					'		</ul>' +
					'	</div>' +
					'</div>'
				);
				container.find('.cd-int-task-item').each(function () {
					var self = $(this);
					var index = parseInt(self.data('index'));

					self.data('storage', task_list[index]);
				});
				return (container);
			}
			function html_create_action(parent_list, mode) {
				var container,
					parent,
					input,
					storage;

				storage = X.cd_get_storage();
				if (typeof mode !== 'string')
					mode = INPUT_ACTION.CREATE;
				container = $('<div>', {
					class: 'cd-int-create-container'
				});
				parent = $('<select>');
				$.each(parent_list, function (id, content) {
					parent.append($('<option>', {
						value: id,
						text: content.name,
						data: {
							storage: content
						}
					}));
				});
				parent.val(storage && storage.active_project ? storage.active_project.id : null);
				input = $('<textarea>', {
					placeholder: 'Nom de votre action',
					class: 'cd-int-action-name',
					css: {
						marginTop: '10px'
					}
				});
				input.on('keydown', function (e) {
					var event = e.originalEvent;

					if (event.key && event.key.toLowerCase() === 'enter') {
						event.preventDefault();
						$('.cd-int-new-action').trigger('click');
					}
				});
				container.append(parent);
				container.append(input);
				container.append(html_add_action());
				parent.select2({
					width: '100%'
				});
				container.on('click', '.cd-int-new-action', function () {
					var create_container_dom = $('.cd-int-create-container');
					var post = {
						name: input.val(),
						parentId: parent.val(),
						status: $('.cd-int-status-filter li.active .cd-int-status-item').data('target'),
						urls: [
							X.location.href
						],
					};
					var storage_data = parent.find('option[value=' + parent.val() + ']').data('storage');
					for (var field in post) {
						if (!post[field])
							return;
					}
					X.cd_set_storage({
						active_project: {
							id: parent.val(),
							name: storage_data.name,
							slug: storage_data.slug,
						}
					});
					create_container_dom.empty()
						.html(html_content_loader());
					xhr_add_action(post).then(function (response) {
						X.dataHelper.path2Value({
							id: response._id.$id,
							collection: MODEL.ACTION,
							path: 'links.contributors.' + userId,
							value: {
								type: MODEL.PERSON
							}
						}, function () {
							$('.create-act, .cd-popover-action.add-action').css('display', '');
							do_load_action_list();
						}, path2value_option);
					});
				}).on('click', '.cd-int-close-prompt', function () {
					var title_dom = $('.cd-popover-head .cd-int-popover-title');

					$('.cd-int-add-section').empty();
					$('.create-act, .cd-popover-action.add-action').css('display', '');
					$('.cd-int-no-action').css('display', '');
					title_dom.text(title_dom.data('last-title'));
				});
				return (container);
			}

			function xhr_project_list() {
				var url,
					post;

				url = remote_url + '/costum/project/project/request/project_names';
				post = {
					connectedUser: userId
				};
				return (new Promise(function (resolve, reject) {
					ajaxPost(null, url, post, resolve, function (xhr, status, error) {
						reject({
							code: 41,
							msg: [xhr, status, error],
						});
					})
				}));
			}

			function xhr_add_action(args) {
				var url,
					post;

				url = remote_url + '/costum/project/action/request/new';
				post = {
					status: 'todo',
					parentType: 'projects'
				};
				return (new Promise(function (resolve, reject) {
					if (typeof args !== 'object') {
						reject({
							code: 21,
							msg: 'argument is null'
						})
						return;
					}
					post = $.extend({}, post, args);
					X.ajaxPost(null, url, post, function (response) {
						if (response.success)
							resolve(response.data);
						else
							reject({
								code: 41,
								msg: response.data
							});
					}, function (xhr, status, error) {
						reject({
							code: 42,
							msg: [xhr, status, error]
						});
					})
				}));
			}

			function xhr_count_action_by_link(args) {
				return (new Promise(function (resolve, reject) {
					var url = remote_url + "/co2/action/cornerdev/method/url-exists",
						post = {
							url: X.location.href
						};
					if (typeof args === 'object' && args)
						post = $.extend({}, post, args);
					ajaxPost(null, url, post, function (response) {
						if (response.success)
							resolve(response.content);
						else
							reject({
								code: 41,
								msg: response.content
							});
					}, function (arg0, arg1, arg2) {
						reject({
							code: 42,
							msg: [arg0, arg1, arg2]
						});
					})
				}));
			}

			function xhr_get_action_by_link(args) {
				return (new Promise(function (resolve, reject) {
					var url = remote_url + "/co2/action/cornerdev/method/get-by-url",
						post = {
							url: X.location.href,
						};
					if (typeof args === 'object')
						post = $.extend({}, post, args);
					ajaxPost(null, url, post, function (response) {
						if (response.success)
							resolve(response.content);
						else
							reject({
								code: 41,
								msg: response.content
							});
					}, function (arg0, arg1, arg2) {
						reject({
							code: 42,
							msg: [arg0, arg1, arg2]
						});
					})
				}));
			}

			function xhr_get_action(action) {
				return (new Promise(function (resolve, reject) {
					var url = remote_url + "/co2/action/cornerdev/method/get-action",
						post = {
							action: action
						};
					ajaxPost(null, url, post, function (response) {
						if (response.success)
							resolve(response.content);
						else
							reject({
								code: 41,
								msg: response.content
							});
					}, function (arg0, arg1, arg2) {
						reject({
							code: 42,
							msg: [arg0, arg1, arg2]
						});
					})
				}));
			}

			function get_storage() {
				var data = sessionStorage.getItem("cd-refresh");
				var default_refresh = {
					link: X.location.href,
					show: true,
					refresh: 0,
				};
				data = $.extend({}, default_refresh, JSON.parse(data));
				if (data.link === X.location.href)
					data.refresh++;
				else
					data = {
						link: X.location.href,
						refresh: 1
					};
				return (data);
			}

			function set_storage(data) {
				var save = $.extend({}, get_storage(), data);
				sessionStorage.setItem("cd-refresh", JSON.stringify(save));
			}

			function do_load_action_list(id) {
				var popover_dom = $(".cornerdev-button .cd-popover");
				var pastille_dom = $('.cd-int-pastille');
				var title_dom = $('.cd-popover-head .cd-int-popover-title');
				var status_dom = $('.cd-int-status-filter li.active .cd-int-status-item');

				if (typeof id === 'string')
					xhr_get_action(id).then(function (action) {
						var target_dom = $('.cd-int-action-list-container[data-id="' + id + '"]');
						action.id = id;
						target_dom.after(clone_action_list(action, target_dom));
						target_dom.remove();
					});
				else
					xhr_get_action_by_link({
						status: status_dom.data('target')
					})
						.then(function (action_object) {
							var keys = Object.keys(action_object);
							pastille_dom.text(keys.length);

							if (keys.length >= 3)
								$('.cd-int-status-filter').removeClass('dropup');
							else
								$('.cd-int-status-filter').addClass('dropup');
							title_dom.data('last-title', title_dom.text())
								.text(status_dom.text());
							popover_dom.find(".cd-popover-content")
								.empty();
							if (keys.length === 0)
								popover_dom.find(".cd-popover-content")
									.html('<span class="cd-int-no-action">Aucune action dans ce status</span>');
							$.each(action_object, function (id, action) {
								action.id = id;
								popover_dom.find(".cd-popover-content")
									.append(html_action_list(action));
							});
							popover_dom.find('.cd-popover-content')
								.append('<div class="cd-int-add-section" data-mode="' + INPUT_ACTION.APPEND + '"></div>');
						});
			}

			// start of instruction
			var storage = get_storage();
			set_storage(storage);
			var cd_get_storage = X.cd_get_storage();
			if (ws && cd_get_storage && cd_get_storage.active_project) {
				ws.on('.cd-int-contributors', function (data) {
					if ($('.cd-int-action-list-container[data-id=' + data.action + ']').length)
						do_load_action_list(data.action);
				}).on('.cd-int-task', function (data) {
					if ($('.cd-int-action-list-container[data-id=' + data.action + ']').length)
						do_load_action_list(data.action);
				}).on('.cd-int-archive', function (data) {
					var dom = $('.cd-int-action-list-container[data-id=' + data.action + ']');
					if (dom.length)
						dom.slideUp(100, function () {
							do_load_action_list();
						})
				}).on('.cd-int-move', function (data) {
					var dom = {
						target: $('.cd-int-action-list-container[data-id=' + data.action + ']'),
						after: $('.cd-int-action-list-container[data-id=' + data.after + ']'),
						active_filter: $('.cd-int-status-filter li.active .cd-int-status-item'),
					};
					if (data.target === dom.active_filter.data('target')) {
						if (dom.target.length && dom.after.length)
							dom.after.before(dom.target);
						else
							do_load_action_list();
					} else if (dom.target.length)
						dom.target.remove();
				});
			}
			xhr_count_action_by_link().then(function (exists) {
				var popover_dom = $(".cornerdev-button .cd-popover");
				var pastille_dom = $('.cd-int-pastille');
				var title_dom = $('.cd-popover-head .cd-int-popover-title');

				pastille_dom.on('click', function () {
					if (!popover_dom.is(':visible')) {
						popover_dom.css('display', '');
						set_storage({show: true});
					}
				});
				$('.cd-int-status-filter').off('click').on('click', '.cd-int-status-item', function (e) {
					var self = $(this);

					e.preventDefault();
					$('.cd-int-status-filter li').removeClass('active');
					self.parent().addClass('active');
					do_load_action_list();
				});
				popover_dom.off('click').on("click", ".create-act, .cd-popover-action.add-action", function () {
					var target_dom = $('.create-act, .cd-popover-action.add-action');
					var add_section_dom = $('.cd-int-add-section');

					$('.cd-int-no-action').hide();
					add_section_dom.empty()
						.html(html_content_loader());
					target_dom.hide();
					title_dom.data('last-title', title_dom.text())
						.text('Sur quoi travailles-tu ?');
					xhr_project_list().then(function (project_list) {
						add_section_dom.empty()
							.html(html_create_action(project_list, add_section_dom.data('mode')));
					}).catch(function () {
						target_dom.css('display', '');
						add_section_dom.empty();
					});
				}).on("click", ".hide-msg", function () {
					popover_dom.hide();
					set_storage({show: false});
				});
				pastille_dom.text(0);
				if (!exists) {
					title_dom.text('Oceco')
						.data('last-title', 'Oceco');
					popover_dom.css('display', storage.refresh >= 3 && storage.show ? 'block' : 'none')
						.find(".cd-popover-content")
						.empty()
						.html(
							'<span class="create-act">Sur quoi travailles-tu ?</span><br>' +
							'<div class="cd-int-add-section" data-mode="' + INPUT_ACTION.CREATE + '"></div>'
						);
				} else {
					popover_dom.css('display', storage.show ? 'block' : 'none')
					title_dom.data('last-title', title_dom.text())
						.text('En cours');
					do_load_action_list();
				}
			});
			// handle cards events
			$('.cd-popover-content').off().on('click', '.cd-int-more-less', function () {
				var self = $(this);
				var font = {
					MORE: 'fa-angle-down',
					LESS: 'fa-angle-up'
				};

				if (self.hasClass(font.MORE)) {
					self.parents('.cd-int-action-list')
						.find('.cd-int-footer, .cd-int-contributors, .cd-int-tasks')
						.css('display', '');
					self.removeClass(font.MORE);
					self.addClass(font.LESS);
				} else {
					self.parents('.cd-int-action-list')
						.find('.cd-int-footer, .cd-int-contributors, .cd-int-tasks')
						.css('display', 'none');
					self.addClass(font.MORE);
					self.removeClass(font.LESS);
				}
			}).on('click', '.contributors-preview', function () {
				var self = $(this);
				var container = self.parents('.cd-int-action-list-container');
				if (container.hasClass('dragging'))
					return;
				var id = $(this).data('id');
				container.find('.cd-int-tasks').hide();
				container.find('.cd-int-statuses').hide();
				container.find('.cd-int-contributors')
					.slideToggle(200, function () {
						var self = $(this);

						if (self.is(':visible') && container.find('.cd-int-contributor-info[data-status="loading"]').length) {
							xhr_get_contributors(id).then(function (contributor_list) {
								$.each(contributor_list, function (_, contributor) {
									var info_container_dom = $('.cd-int-contributor-info[data-id="' + contributor.id + '"]');

									info_container_dom.removeAttr('data-status')
									info_container_dom.find('img')
										.attr('src', contributor.image)
										.attr('alt', contributor.name);
									info_container_dom.find('.cd-int-contributor-name')
										.text(contributor.name);
								});
							});
						}
					});
			}).on('click', '.contribution-action', function () {
				var self = $(this);
				var post = {
					action: self.data('id'),
					contributor: userId,
					participate: self.data('contribution') ? 0 : 1
				};

				if (self.prop('disabled'))
					return;
				self.empty()
					.html('<i class="fa fa-spin fa-spinner"></i>');
				self.prop("disabled", true);
				coInterface.actions.request_participation(post).then(function () {
					do_load_action_list(self.data("id"));
					self.prop("disabled", false);
				});
			}).on('click', '.cd-int-title', function (e) {
				var self = $(this);
				var previewDom = $('#corner-action-preview');

				if ($(e.target).is($('.cd-int-more-less')))
					return;
				previewDom.empty();
				xhr_action_detail_modal(self.data('id')).then(function (html) {
					previewDom.html(html);
				});
			}).on('click', '.cd-int-footer-action.archive', function () {
				var self = $(this);
				var id = self.data('id');

				$.confirm({
					title: X.coTranslate('Archive'),
					type: 'orange',
					content: X.coTranslate('Please confirm archiving'),
					buttons: {
						no: {
							text: X.coTranslate('no'),
							btnClass: 'btn btn-default'
						},
						yes: {
							text: X.coTranslate('yes'),
							btnClass: 'btn btn-warning',
							action: function () {
								var params = {
									id: id,
									server_url: remote_url
								};
								X.coInterface.actions.request_archive(params).then(function () {
									toastr.success("Action archivée");
									ajaxPost(null, coWsConfig.pingActionManagement, {
										event: '.archive' + php.parent_id,
										data: {
											action: id,
											emiter: '',
										}
									}, null, null, { contentType: 'application/json' });
									do_load_action_list();
								});
							}
						}
					}
				});
			}).on('click', '.cd-int-footer-action.task-list-resume', function () {
				var container = $(this).parents('.cd-int-action-list-container');

				container.find('.cd-int-contributors').hide();
				container.find('.cd-int-statuses').hide();
				container.find('.cd-int-tasks').slideToggle(200);
			}).on('click', '.cd-int-task-name', function () {
				var self = $(this);
				var parent_dom = self.parents('.cd-int-task-item');
				var value = parent_dom.data('storage');
				var checked = !self.hasClass('checked');
				var id = parent_dom.data('action');
				var setType = [
					{ path: 'checked', type: 'boolean' },
					{ path: 'checkedAt', type: 'isoDate' },
					{ path: 'createdAt', type: 'isoDate' },
				];

				value = $.extend({}, value, {
					checkedAt: null,
					checked: false,
					checkedUserId: null,
				});
				if (checked) {
					value.checkedAt = moment().format();
					value.checkedUserId = userId;
					value.checked = true;
				}
				var params = {
					id: id,
					collection: MODEL.ACTION,
					path: 'tasks.' + self.data('index'),
					value: value,
					setType: setType,
				};
				X.dataHelper.path2Value(params, function () {
					do_load_action_list(id);
					X.coInterface.actions.ping_update_task({
						action: id,
						index: self.data('index'),
						content: value,
						operation: 'update'
					}).then();
				}, path2value_option);
			}).on('click', '.cd-int-task-action[data-target=contribution]', function () {
				var self = $(this);
				var parent_dom = self.parents('.cd-int-task-item');
				var value = parent_dom.data('storage');
				var contributors = value.contributors ? value.contributors : {};
				var setType = [
					{ path: 'checked', type: 'boolean' },
					{ path: 'checkedAt', type: 'isoDate' },
					{ path: 'createdAt', type: 'isoDate' },
				];

				value = $.extend({}, {
					checkedAt: null,
					checked: false,
					checkedUserId: null,
				}, value);
				if (self.data('action') === 'unset')
					delete contributors[userId];
				else if (self.data('action') === 'set')
					contributors[userId] = {
						type: MODEL.PERSON,
					};
				if (!contributors || !Object.keys(contributors).length)
					delete value.contributors;
				else
					value.contributors = contributors;
				var params = {
					id: parent_dom.data('action'),
					collection: MODEL.ACTION,
					path: 'tasks.' + self.data('index'),
					value: value,
					setType: setType,
				};
				X.dataHelper.path2Value(params, function () {
					do_load_action_list(parent_dom.data('action'));
					X.coInterface.actions.ping_update_task({
						action: parent_dom.data('action'),
						index: self.data('index'),
						content: value,
						operation: 'contribution'
					}).then();
				}, path2value_option);
			}).on('click', '.cd-int-task-action[data-target=delete]', function () {
				var self = $(this);
				var parent_dom = self.parents('.cd-int-task-item');

				$.confirm({
					title: 'Supprimer',
					content: 'Supprimer cette tâche',
					buttons: {
						no: {
							text: 'Non',
							btnClass: 'btn btn-default'
						},
						yes: {
							text: 'Oui',
							btnClass: 'btn btn-danger',
							action: function () {
								var params = {
									id: parent_dom.data('action'),
									collection: MODEL.ACTION,
									path: 'tasks.' + self.data('index'),
									pull: 'tasks',
									value: null
								};
								X.dataHelper.path2Value(params, function (response) {
									if (response.result && response.result === true) {
										do_load_action_list(parent_dom.data('action'));
										X.coInterface.actions.ping_update_task({
											action: parent_dom.data('action'),
											index: self.data('index'),
											content: null,
											operation: 'delete'
										}).then();
										toastr.success("Sous tache supprimé!");
									}
								}, path2value_option);
							}
						}
					}
				});
			}).on('click', '.cd-int-footer-action.status', function () {
				var container = $(this).parents('.cd-int-action-list-container');

				container.find('.cd-int-tasks').hide();
				container.find('.cd-int-contributors').hide();
				container.find('.cd-int-statuses').slideToggle(200);
			}).on('click', '.cd-int-statuses-item', function () {
				var self = $(this);
				var id = self.data('action');
				var status = self.data('target');

				X.coInterface.actions.request_set_status({
					id: id,
					status: status,
					server_url: remote_url,
					user: userId ? userId : null
				}).then(function () {
					var pastille_dom = $('.cd-int-pastille');
					do_load_action_list();
					pastille_dom.text(parseInt(pastille_dom.text()) - 1);
					coInterface.actions.update_action_position({
						action: id,
						group: status,
						emiter: X.wsCO && X.wsCO.id ? X.wsCO.id : null,
						component: 'cd-int',
						server_url: php.server_url
					}).then()
				});
			}).on('click', '.cd-int-footer-action.comment', function () {
				var self = $(this);

				if (typeof commentObj !== 'undefined' && userId)
					commentObj.openPreview("actions", self.data('id'), "", self.data('title'));
			});
			// handle cards events
		}
})(window, jQuery);