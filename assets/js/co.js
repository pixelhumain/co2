
var countPoll = 0;
var prevStep = 0;
var steps = ["explain1", "live", "explain2", "event", "explain3", "orga", "explain4", "project", "explain5", "person"];
var isBinding = false; // Store previous hash value
var slides = {
	explain1: function () { showDefinition("explainCommunectMe") },
	live: function () { urlCtrl.loadByHash("#default.live") },
	explain2: function () { showDefinition("explainCartographiedeReseau") },
	event: function () { urlCtrl.loadByHash("#event.detail.id.57bb4078f6ca47cb6c8b457d") },
	explain3: function () { showDefinition("explainDemoPart") },
	orga: function () { urlCtrl.loadByHash("#organization.detail.id.57553776f6ca47b37da93c2d") },
	explain4: function () { showDefinition("explainCommunecter") },
	project: function () { urlCtrl.loadByHash("#project.detail.id.56c1a474f6ca47a8378b45ef") },
	explain5: function () { showDefinition("explainProxicity") },
	person: function () { urlCtrl.loadByHash("#person.detail.id.54eda798f6b95cb404000903") }
};
function runslide(cmd) {
	if (cmd == 0) {
		prevStep = null;
		urlCtrl.loadByHash("#default.live");
	}

	if (prevStep != null) {
		slides[steps[prevStep]]();
		prevStep = (prevStep < steps.length - 1) ? prevStep + 1 : 0;
		setTimeout(function () {
			runslide();
		}, 8000);
	}
}

function checkPoll() {
	countPoll++;
	mylog.log("co.js checkPoll countPoll", countPoll, "currentUrl", currentUrl, " | userId : ", userId);
	//refactor check Log to use only one call with pollParams 
	//returning multple server checks in a unique ajax call
	if (userId) {
		_checkLoggued();
		var socketConnectionEtablish = Boolean(wsCO)
		if (typeof refreshNotifications != "undefined" && !socketConnectionEtablish) refreshNotifications(userId, "citoyens", "");
	}

	//according to the loaded page 
	//certain checks can be made  
	if (currentUrl.indexOf("#comment.index.type.actionRooms.id") >= 0)
		checkCommentCount();

	if (countPoll < 100) {
		setTimeout(function () { checkPoll() }, 300000); //every5min
		countPoll++;
	}
}
function linkMdToHtml(str) {
	descHtml = dataHelper.convertMardownToHtml(str);
	descHtml = descHtml.replace(/<a href="([^j][^\s"]+)">/gm,
		'<a href="$1" target="_blank">');
	descHtml = descHtml.replace(/(?!")(https[^j][^\s]+)\<\/li\>/gm,
		'<a href="$1" target="_blank">$1</a>');
	if (str.indexOf("@") > 0) {
		descHtml = descHtml.replace(/(?!#)1(@[^\s\)]+)/gm,
			'<a href="' + baseUrl + '/#$1" target="_blank">$1</a>');
	}
	if (str.indexOf("!@") > 0) {
		descHtml = descHtml.replace(/!(@[^\s\)]+)/gm,
			'<a href="' + baseUrl + '/#$1" target="_blank">$1</a>');
	}
	return descHtml;
}
var watchThis = null;
function bindRightClicks() {
	$.contextMenu({
		selector: ".add2fav",
		build: function ($trigger, e) {
			if (userId) {
				var validElems = ["#element", "#page", "#organization", "#project", "#event", "#person", "#element", "#survey", "#rooms"];
				href = $trigger[0].hash.split(".");
				if ($.inArray(href[0], validElems) >= 0) {
					if (href[0] == "#element") {
						var what = href[3];
						var id = href[5];
					} else if (href[0] == "#page") {
						var what = href[2];
						var id = href[4];
					} else {
						var what = typeObj[href[0].substring(1)].col;
						var id = href[3];
					}
				}
				/*
				watchThis = {
					t : $(this),
					tr : $trigger
				};*/
				var btns = {
					/*
					todo : how remove if can't edit 
					would like to use a class but can't find how to get the content of the class attribute
					ask TKA
					editThis : {
						name: trad.edit,
						icon: "fa-pencil", 
						callback: function(key, opt){ 
							dyFObj.editElement( what , id );
						}
					},*/
					openInNewTab: {
						name: "Ouvrir dans un nouvel onglet",
						icon: "fa-arrow-circle-right",
						callback: function (key, opt) {
							window.open($trigger[0].hash, '_blank');
						}
					}
				};
				$.each(userConnected.collections, function (col, list) {
					btns[col] = {
						name: function ($element, key, item) {
							nameCol = col;
							if (col == "favorites")
								nameCol = "mes favoris";
							var str = "Ajouter à " + nameCol;

							if (notNull(userConnected.collections[col]) && notNull(userConnected.collections[col][what]) && notNull(userConnected.collections[col][what][id]))
								str = "Retirer de " + nameCol;
							return str;
						},
						icon: "fa-folder-open",
						callback: function (key, opt) {
							if (notNull(what) && notNull(id)) {
								collection.add2fav(what, id, col);
							}
						}
					}
				});
				btns.newCollection = {
					name: "Créer une nouvelle Collection",
					icon: "fa-folder-open-o",
					callback: function (key, opt) {
						if (userId) {
							collection.crud();
						}
					}
				};
				return { items: btns }
			}
		}
	});
	$.contextMenu({
		selector: ".tag",
		build: function ($trigger, e) {
			var tag = $trigger[0].dataset.tagValue;
			var btns = {};
			btns.filterTag = {
				name: "Filtrer",
				icon: "fa-filter",
				callback: function (key, opt) {
					directory.toggleEmptyParentSection(".favSection", "." + slugify(tag), directory.elemClass, 1);
				}
			};
			btns.addToMultiTags = {
				name: "Ajouter au tags préférés",
				icon: "fa-tag",
				callback: function (key, opt) {
					addTagToMultitag(tag);
				}
			};
			return { items: btns }
		}
	});
}
function linkify(inputText) {
	if (typeof inputText != "undefined" && notNull(inputText)) {
		var replacedText, replacePattern1, replacePattern2, replacePattern3;

		//URLs starting with http://, https://, or ftp://
		replacePattern1 = /(\b(https?|ftp):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/gim;
		replacedText = inputText.replace(replacePattern1, '<a href="$1" target="_blank">$1</a>');

		//URLs starting with "www." (without // before it, or it'd re-link the ones done above).
		replacePattern2 = /(^|[^\/])(www\.[\S]+(\b|$))/gim;
		replacedText = replacedText.replace(replacePattern2, '$1<a href="http://$2" target="_blank">$2</a>');

		//Change email addresses to mailto:: links.
		replacePattern3 = /(([a-zA-Z0-9\-\_\.])+@[a-zA-Z\_]+?(\.[a-zA-Z]{2,6})+)/gim;
		replacedText = replacedText.replace(replacePattern3, '<a href="mailto:$1">$1</a>');

		return replacedText;
	}
}
function addslashes(str) {
	//  discuss at: http://phpjs.org/functions/addslashes/
	// original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	// improved by: Ates Goral (http://magnetiq.com)
	// improved by: marrtins
	// improved by: Nate
	// improved by: Onno Marsman
	// improved by: Brett Zamir (http://brett-zamir.me)
	// improved by: Oskar Larsson Högfeldt (http://oskar-lh.name/)
	//    input by: Denny Wardhana
	//   example 1: addslashes("kevin's birthday");
	//   returns 1: "kevin\\'s birthday"

	return (str + '')
		.replace(/[\\"']/g, '\\$&')
		.replace(/\u0000/g, '\\0');
}
/* *************************** */
/* instance du menu questionnaire*/
/* *************************** */
function DropDown(el) {
	this.dd = el;
	this.placeholder = this.dd.children('span');
	this.opts = this.dd.find('ul.dropdown > li');
	this.val = '';
	this.index = -1;
	this.initEvents();
}
DropDown.prototype = {
	initEvents: function () {
		var obj = this;

		obj.dd.on('click', function (event) {
			$(this).toggleClass('active');
			return false;
		});

		obj.opts.on('click', function () {
			var opt = $(this);
			obj.val = opt.text();
			obj.index = opt.index();
			obj.placeholder.text(obj.val);
			window.open($(this).find('a').slice(0, 1).attr('href'));
		});
	},
	getValue: function () {
		return this.val;
	},
	getIndex: function () {
		return this.index;
	}
}

function addslashes(str) {
	//  discuss at: http://phpjs.org/functions/addslashes/
	// original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	// improved by: Ates Goral (http://magnetiq.com)
	// improved by: marrtins
	// improved by: Nate
	// improved by: Onno Marsman
	// improved by: Brett Zamir (http://brett-zamir.me)
	// improved by: Oskar Larsson Högfeldt (http://oskar-lh.name/)
	//    input by: Denny Wardhana
	//   example 1: addslashes("kevin's birthday");
	//   returns 1: "kevin\\'s birthday"

	return (str + '')
		.replace(/[\\"']/g, '\\$&')
		.replace(/\u0000/g, '\\0');
}
/* *************************** */
/* global JS tools */
/* *************************** */
var mylog = (function () {

	return {
		log: function () {
			if (debug) {
				var args = Array.prototype.slice.call(arguments);
				console.log.apply(console, args);
			}
		},
		warn: function () {
			if (debug) {
				var args = Array.prototype.slice.call(arguments);
				console.warn.apply(console, args);
			}
		},
		debug: function () {
			if (debug) {
				var args = Array.prototype.slice.call(arguments);
				console.debug.apply(console, args);
			}
		},
		info: function () {
			if (debug) {
				var args = Array.prototype.slice.call(arguments);
				console.info.apply(console, args);
			}
		},
		dir: function () {
			if (debug) {
				var args = Array.prototype.slice.call(arguments);
				console.warn.apply(console, args);
			}
		},
		error: function () {
			if (debug) {
				var args = Array.prototype.slice.call(arguments);
				console.error.apply(console, args);
			}
		}
	}
}());
/* ------------------------------- */

function initSequence() {
	$.each(initT, function (k, v) {
		log(k, 'info');
		v();
	});
	initT = null;
}

/* DEPRACATED [06/05/2019] TODO : clean if not used
function showEvent(id){
	$("#"+id).click(function(){
		if($("#"+id).prop("checked"))
			$("#"+id+"What").removeClass("hidden");
		else
			$("#"+id+"What").addClass("hidden");
	});
}*/


function loadSettings(hash) {
	$("#modal-settings").show();
	var url = "settings/index";
	if (typeof hash != "undefined" && hash.indexOf("page") >= 0) {
		hashT = hash.split(".");
		url += "/page/" + hashT[2];
		if (hash.indexOf("to") >= 0) {
			url += "/to/" + hashT[4];
		}
	}
	showLoader('#modal-settings');
	ajaxPost('#modal-settings', baseUrl + '/' + moduleId + '/' + url,
		null,
		function () { });
}

var CoAllReadyLoad = false;
var navbarObserver;
var coInterface = {
	showMapOnLoad: false,
	init: function () {
		coInterface.initHtmlPosition();
		coInterface.scrollTo(".main-container");
		coInterface.bindEvents();
		coInterface.menu.set(location.hash);
		// Si dans un des menu de la maquette html il y a le bouton de dashboard user on charge la vue
		if ($(".open-modal-dashboard").length > 0 && $("#modal-dashboard-account").html() == "") {
			var url;
			var post;
			// conditionnement avec le type de costum aap
			if (costum && typeof costum.type === 'string' && costum.type === 'aap') {
				url = baseUrl + '/co2/aap/userdashboard';
				if (typeof costum.fUserDashboard === 'string')
					url += '/function/' + costum.fUserDashboard;
				l_rebuild_modal.call(aapObj);
				function l_rebuild_modal() {
					$('.dashboard-aap').remove();
					var dashboardAap = $('<div>')
						.addClass('modal dashboard-aap').css({
							display: 'none',
							overflowY: 'scroll',
							backgroundColor: 'rgba(0,0,0,0.9)',
							zIndex: 100000,
							position: 'fixed',
							top: $("#mainNav").outerHeight(),
							left: $("#menuApp.menuLeft").width()
						});
					$(document.body).append(dashboardAap);
					if (typeof costum.fUserDashboard === 'string')
						url += '/function/' + costum.fUserDashboard;
					ajaxPost(dashboardAap, url, post, null, null, 'html');
				}
				$('.menu-name-profil')
					.tooltip({
						container: 'body'
					})
					.removeClass('lbh')
					.addClass('open-modal-user-aap')
					.off('click')
					.on('click', function () {
						l_rebuild_modal.call(aapObj);
						$('.dashboard-aap').show(200);
					})
					.find(".tooltips-menu-btn")
					.text("Tableau de bord");
			} else {
				url = baseUrl + '/' + moduleId + '/default/render?url=co2.views.person.dashboard';
				post = {
					view: $(".open-modal-dashboard").data("view")
				};
				ajaxPost('#modal-dashboard-account', url, post, function () { });
			}
		}
	},
	infScroll: false,
	bindEvents: function () {
		coInterface.bindResizeEvent();
		// Active button to get dynform
		coInterface.bindButtonOpenForm();
		coInterface.bindLBHLinks();
		coInterface.bindTooltips();
		coInterface.bindCostumizer();
		$(".logoutBtn").off().on("click", function () {
			url = baseUrl + '/' + moduleId + '/person/logout';
			if (notNull(costum) && notNull(costum.slug)) {
				url += "?slug=" + costum.slug;
			}
			//alert(url);
			ajaxPost(null, url,
				null,
				function (data) {
					if (costum && typeof costum.htmlConstruct != "undefined" && typeof costum.htmlConstruct.redirect != "undefined" && typeof costum.htmlConstruct.redirect.unlogged != "undefined" && costum.htmlConstruct.redirect.unlogged) {
						window.location.hash = "#" + costum.htmlConstruct.redirect.unlogged;
					}
					location.reload();
					//alert(data.url);
				});
		});
		$('#ajax-modal').off().on('hidden.bs.modal', function () {
			dyFObj.closeForm();
		});
		/* empecher le dropdown de se fermer quand on clique */
		var heightSousMenu = $(".dropdown-app-full-width .drop-down-sub-menu").height();

		$(".dropdown-app-full-width .sous-menu-full-width a").on("click", function (e) {
			e.preventDefault();
			$(this).closest(".drop-down-sub-menu").addClass("show").trigger("on.shown.dropdown.aap");
			if (typeof heightSousMenu == "number")
				$(".pageContent").css("margin-top", `${heightSousMenu}px`);
		});

		$(".menu-not-have-sub-menu").on("click", function () {
			$(".dropdown-app-full-width .drop-down-sub-menu").removeClass("show").trigger("on.hidden.dropdown.aap");
			$(".pageContent").css("margin-top", "0");
		})

		$(".dropdown-app-full-width a[data-toggle='dropdown']").on("click", function () {
			$(".dropdown-app-full-width .drop-down-sub-menu").removeClass("show");
			const dropdownElem = $(this).parent().find(".dropdown-menu.drop-down-sub-menu");
			if (typeof $(this).attr("aria-expanded") == "undefined" || $(this).attr("aria-expanded") == "false") {
				if (typeof heightSousMenu == "number")
					$(".pageContent").css("margin-top", `${heightSousMenu}px`);
			} else {
				$(".pageContent").css("margin-top", "0");
			}
			if (!dropdownElem.is(":visible"))
				dropdownElem.trigger("on.shown.dropdown.aap")
			else
				dropdownElem.trigger("on.hidden.dropdown.aap")
			$(".pageContent,#menuTopLeft,#mainNav").on("click", function () {
				var toggleVisibility = setInterval(() => {
					if ($('.dropdown-app-full-width.open').length > 0 || $(".drop-down-sub-menu.show").length > 0) {
						$(".pageContent").css("margin-top", `${$(".dropdown-app-full-width .drop-down-sub-menu.show").height()}px`);
					} else {
						$(".pageContent").css("margin-top", "0");
					}
					clearInterval(toggleVisibility)
					toggleVisibility = null;
				}, 70);
			});
		})

		$(".dropdown-app-full-width .drop-down-sub-menu").off("on.shown.dropdown.aap").on("on.shown.dropdown.aap", function (e) {
			e.stopImmediatePropagation();
			const selfElem = this;
			var interv = setInterval(() => {
				clearInterval(interv);
				interv = null;
				if (typeof heightSousMenu == "number")
					$(".pageContent").css("margin-top", `${$(selfElem).height()}px`);
			}, 30);
		})
		$(".dropdown-app-full-width .drop-down-sub-menu").off("on.hidden.dropdown.aap").on("on.hidden.dropdown.aap", function (e) {
			e.stopImmediatePropagation();
			if (!$(this).is(":visible"))
				$(".pageContent").css("margin-top", "0");
		})

		if ($('.dropdown-app-full-width.open').length > 0) {
			const openedSousMenu = $('.dropdown-app-full-width.open').find(".drop-down-sub-menu")
			var sousMenuHeight = ($(".drop-down-sub-menu.show").length > 0 ? $(".drop-down-sub-menu.show").height() : ($(openedSousMenu).length > 0 ? $(openedSousMenu).height() : 0))
			$(".pageContent").css("margin-top", `${sousMenuHeight}px`);
		} else {
			$(".drop-down-sub-menu.show").length > 0 ? $(".drop-down-sub-menu.show").removeClass("show") : ""
			$(".pageContent").css("margin-top", "0");
		}

		/*****Evenement pour montrer le menu d'ajout d'élement si plusieurs*****/
		var count = 0;
		if (costum && costum.typeObj) {
			$.each(costum.typeObj, function (e, v) {
				if (typeof costum.typeObj[e]["add"] != "undefined" && costum.typeObj[e]["add"] == true) {
					count++;
				}
			});
		}

		if (typeof contextData != "undefined" && contextData != null && typeof contextData.costum != "undefined" && notEmpty(contextData.costum)) {
			$(".place-align .link-circle[data-original-title='Costum']").addClass("hasCostum");
		}


		var sameId = false;
		if (costum && costum.contextId && contextData && contextData.id) {
			if (contextData.id == costum.contextId) {
				sameId = true;
			}
		}

		if ($(".cosDyn-menuTop").find('.cosDyn-add, .bottom-add, .show-bottom-add').length > 0) {
			$(".cosDyn-menuTop").find('.cosDyn-add, .bottom-add, .show-bottom-add').find(".toolbar-bottom-fullwidth").hide()
		}
		if (((count > 1 || (sameId == true && count != 1)) || (count == 0 && (!notNull(contextData)))) && $("#show-bottom-add").parent().find(".cosDyn-add.btn-open-form").length != 1) {

			$('.cosDyn-add, .bottom-add, .show-bottom-add').off().click(function (e) {
				if ($(this).parent().attr("id") === "menuBottom") {
					$(this).find(".toolbar-bottom-fullwidth").css("display", "grid");
				}

				if (typeof $(this).parent().data("path") != "undefined" && $(this).parent().data("path").includes("menuTop")) {
					$(this).find(".toolbar-bottom-fullwidth").css("bottom", "auto").css("top", "75%").css("left", "auto");

				}

				let element = ($("[data-path='menuLeft']").find(".cosDyn-add, .bottom-add, .show-bottom-add").length > 0) ? $(this) : $(this).parent();
				if (!element.hasClass("opened")) {
					element.addClass("opened");
					element.find(".toolbar-bottom-adds").show();
					/*$('.toolbar-bottom-adds a').click(function(){
						$(this).find(".toolbar-bottom-adds").hide(200);
						$(this).removeClass("opened");
					});*/
				} else {
					element.find(".toolbar-bottom-adds").hide(200);
					element.removeClass("opened");
				}
			});
		}
		$('.openChatRC').off().click(function () {
			rcObj.loadChat("", "citoyens", true, true)
		});
		$('.toolbar-bottom-adds').unbind("mouseleave").mouseleave(function () {
			//console.log(".toolbar-bottom-adds mouseleave");
			$(this).parent().removeClass("opened");
			$(this).hide();

		});
		/*****Fin d'évenement sur le bouton show button create elt *****/

		//jQuery for page scrolling feature - requires jQuery Easing plugin
		// This button can be used to get scroll to the container indicated in href
		$('.page-scroll a').off().on('click', function (event) {
			var $anchor = $(this);
			$('html, body').stop().animate({
				scrollTop: ($($anchor.attr('href')).offset().top - 50)
			}, 1250, 'easeInOutExpo');
			event.preventDefault();
		});

		// jQuery for page scrolling feature - requires jQuery Easing plugin
		$('.btn-scroll').off().on('click', function (event) {
			var target = $(this).data('targetid');
			coInterface.scrollTo(target);
			event.preventDefault();
		});

		// Highlight the top nav as scrolling occurs
		// Closes the Responsive Menu on Menu Item Click
		/*$('.navbar-collapse ul li a').click(function(){ 
			$('.navbar-toggle:visible').click();
		});*/

		/*$(".openModalSelectCreate").click(function(){
			$("#selectCreate").modal("show");
			showFloopDrawer(false);
			showNotif(false);
		});*/

		//Floopdrawer is a div containing all links user did
		$(".btn-open-floopdrawer").off().click(function () {
			showNotif(false);
			if ($("#floopDrawerDirectory .floopHeader").length <= 0) {
				floopDrawer.init({
					dom: '#floopDrawerDirectory',
					header: {
						close: true
					},
					options: {
						status: true,
						answer: true,
						actions: {
							chat: true,
							disconnect: true
						}
					}

				});
			}
			//$("#dropdown-user").removeClass("open");
			showFloopDrawer(true);
		});


		// 2 events for notifications
		$('.btn-menu-notif').off().click(function () {
			if ($('#notificationPanelSearch').is(":visible"))
				showNotif(false);
			else
				showNotif();
		});
		$("#notificationPanelSearch").off().on("mouseleave", function () {
			showNotif(false);
		});

		$(".btn-dashboard-dda").off().click(function () {
			// showFloopDrawer(false);
			showNotif(false);
			dashboard.loadDashboardDDA();
			$("#dropdown-user, .dropdownApps-menuTop").removeClass("open");
			$("#dropdown-dda").addClass("open");
		});

		$(`.btnOpenInsideModal`).off("click").on("click", function () {
			const thisBtn = $(this);
			var currentLink = thisBtn.data("link");
			const title = thisBtn.data("title") ? thisBtn.data("title") : "";
			const btnLabel = thisBtn.attr("data-btn-label") ? thisBtn.attr("data-btn-label") : "";
			const btnRedirectionUrl = thisBtn.attr("data-btnredirectionurl") ? thisBtn.attr("data-btnredirectionurl") : "";
			if (currentLink && currentLink.indexOf("#") == 0) {
				if (
					urlCtrl.jsDialogController(currentLink, {
						title: title,
						btnLabel: btnLabel,
						btnRedirectionUrl: btnRedirectionUrl
					})
				) {
				} else {
					toastr.error("Error : unable to open this link");
				};
			} else {
				const getFirstStepOnly = thisBtn.attr("data-first-step-only");
				getFirstStepOnly && /true/.test(getFirstStepOnly) ? currentLink += "/step/aapStep1" : currentLink = currentLink.replace("/step/aapStep1", "");
				const finderMethode = ["searchAndPopulateFinder", "addSelectedToForm", "bindSelectItems", "populateFinder"];
				const lastFinderVal = {};
				if (typeof finder != "undefined") {
					$.each(finderMethode, (index, methode) => {
						if (typeof finder[methode] != "undefined") {
							lastFinderVal[methode] = finder[methode];
						}
					})
				}
				urlCtrl.openUrlInDialog(baseUrl + currentLink, null, {
					title: title,
					btnLabel: btnLabel,
					btnRedirectionUrl: btnRedirectionUrl,
					hiddenCallback: function () {
						if (typeof finder != "undefined") {
							$.each(lastFinderVal, (key, methode) => {
								finder[key] = methode;
							})
						}
						if ($(`[data-need-refreshblock]`).length > 0) {
							$(`[data-need-refreshblock]`).each((index, elem) => {
								const blockId = $(elem).attr("data-need-refreshblock")
								if (typeof refreshBlock == "function") {
									setTimeout(() => {
										refreshBlock(blockId, `.cmsbuilder-block[data-id="${blockId}"]`)
									}, 500);
								}
							})
						}
					}
				});
			}
		})

		$(".tooltips").tooltip();
	},
	initHtmlPosition: function () {
		$(window).off();

		$(".pageContent, #mapContent").css({ "min-height": ($(window).height() - ($("#headerBand").outerHeight() + $("#filters-nav").outerHeight() + $("#mainNav").outerHeight() + $("footer").outerHeight())) + "px" });
		$(".searchObjCSS .menu-filters-xs").css({ "min-height": ($(window).height() - ($("#headerBand").outerHeight() + $("#mainNav").outerHeight())) + "px" });

		if ($('#social-header #contentBanner').length > 0) {
			/* Fire your image resize code here */
			pageProfil.setPosition();
		}
		setTimeout(function () {
			//if($("#menu-top-costumizer").length > 0){
			//	coInterface.menu.initTopPos=$("#menu-top-costumizer").outerHeight();	
			//}
			//if($("#menu-left-costumizer").length > 0){
			//	coInterface.menu.initLeftPos=$("#menu-left-costumizer").outerWidth();		
			//}

			//coInterface.menu.initRightPos=($("#toolsBar-edit-block").length > 0 && $("#toolsBar-edit-block").is(":visible")) ? $("#toolsBar-edit-block").outerWidth(): 0;		

			//$("#mainNav .image-menu").on("load", function(){
			// INIT POSTION INTERFACE IN CASE WHERE COSTUM CONTAIN BANNER IN HEADER
			if ($("#headerBand").length > 0) {
				coInterface.menu.initTopPos = $("#headerBand").outerHeight();
				coInterface.setDomHtmlPosition();
				$("#affix-sub-menu, #mainNav, #filters-nav, #menuLeft, #subMenu").addClass("position-absolute");
			} else {
				coInterface.setDomHtmlPosition(); //heightTopMenu=$("#mainNav").outerHeight();
			}
			// BIND EVENT WHEN SCROLLING WITH INTERFACE INITIALIZE ON ABSOLUTE POSITION DUE TO BANNER IN HEADER
			$(window).bind("scroll", function () {
				if ($("#headerBand").length > 0) {
					if ($(this).scrollTop() > $("#headerBand").outerHeight() && coInterface.infScroll) {
						coInterface.menu.initTopPos = 0;
						$("#affix-sub-menu, #mainNav, #menuLeft.pos-after-menu-top, #filters-nav, #subMenu").removeClass("position-absolute");
						$(".main-container .headerSearchContainer").addClass("affix");
						coInterface.setDomHtmlPosition();
						coInterface.infScroll = false;
					} else if ($(this).scrollTop() <= $("#headerBand").outerHeight() && !coInterface.infScroll) {
						coInterface.menu.initTopPos = $("#headerBand").outerHeight();
						$(".main-container .headerSearchContainer").removeClass("affix");
						$("#affix-sub-menu, #mainNav, #filters-nav, #menuLeft.pos-after-menu-top, #subMenu").addClass("position-absolute");
						coInterface.setDomHtmlPosition();
						coInterface.infScroll = true;

					}
				}
				// INIT POSITION INTERFACE WITHOUT BANNER IN HEADER 
				else {
					if ($(this).scrollTop() <= 10) {
						coInterface.infScroll = true;
						$(".main-container .headerSearchContainer").removeClass("affix");
						coInterface.setTopPosition(true);
					}
					if ($(this).scrollTop() > 10 && !headerScaling && typeof coInterface.infScroll != "undefined" && coInterface.infScroll && (typeof networkJson == "undefined" || networkJson == null)) {
						$(".main-container .headerSearchContainer").addClass("affix");
						mylog.log("coInterface.initHtmlPosition", themeParams);
						if (typeof directory.appKeyParam != "undefined" &&
							notNull(directory.appKeyParam) &&
							typeof urlCtrl.loadableUrls[directory.appKeyParam] != "undefined" &&
							typeof urlCtrl.loadableUrls[directory.appKeyParam].hideFilterHtml != "undefined" &&
							urlCtrl.loadableUrls[directory.appKeyParam].hideFilterHtml === true) {
							$("#filter-scopes-menu, #filters-nav").hide(200);
						}

						$(".menu-btn-scope-filter").removeClass("active");
						setTimeout(function () { coInterface.setTopPosition(true) }, 250);
						if (typeof themeParams.numberOfApp != "undefined" && themeParams.numberOfApp <= 1)
							$("#mainNav").addClass("borderShadow");
						else
							$("#menuApp.subMenuTop").addClass("borderShadow");
						headerScaling = false;
						coInterface.infScroll = false;
					}
				}
			});

			coInterface.bindResizeEvent();
			$("#loadingModal").css({ "opacity": 0.95 });
			if (typeof costum != "undefined" &&
				costum != null &&
				typeof costum.initHtmlPosition == "function")
				costum.initHtmlPosition();
			//})
		}, 400);
	},
	initializeNavbarObserver: function () {
		if (navbarObserver) {
			navbarObserver.disconnect();
		}

		navbarObserver = new MutationObserver(function(mutations) {
			mutations.forEach(function(mutation) {
				coInterface.adjustNavbarHeight();
			});
		});

		var config = { childList: true, subtree: true };
		if (document.getElementById('menuTopLeft') && document.getElementById('menuTopCenter') && document.getElementById('menuTopRight')) {
			navbarObserver.observe(document.getElementById('menuTopLeft'), config);
			navbarObserver.observe(document.getElementById('menuTopCenter'), config);
			navbarObserver.observe(document.getElementById('menuTopRight'), config);
		}
	},
 	adjustNavbarHeight: function() {
        // var heightMenuPosition = Math.max($("#menuTopLeft").outerHeight(), $("#menuTopCenter").outerHeight(), $("#menuTopRight").outerHeight());
        // $("#menuTopLeft, #menuTopCenter, #menuTopRight").css({ "height": heightMenuPosition + "px" });
		const dontGoUp = $(window).height() / 3
		function getMaxHeight(selector) {
			var maxHeight = 0;
			$(selector).children().each(function() {
				if ($(this).is(":visible")) {
					var childHeight = $(this).outerHeight();
					if (childHeight > maxHeight && childHeight <= dontGoUp) { // Assuming 100 is the maximum height you want to consider
						maxHeight = childHeight;
					}
				}
			});
			return maxHeight;
		}
	
		// Get the maximum height of children elements for each section
		var maxHeightLeft = getMaxHeight("#menuTopLeft");
		var maxHeightCenter = getMaxHeight("#menuTopCenter");
		var maxHeightRight = getMaxHeight("#menuTopRight");
	
		// Determine the maximum height among the three sections
		var heightMenuPosition = Math.max(maxHeightLeft, maxHeightCenter, maxHeightRight);
	
		// Apply the maximum height to each section
		$("#menuTopLeft, #menuTopCenter, #menuTopRight").css({ "height": heightMenuPosition + "px" });
	
    },
	setDomHtmlPosition: function () {
		//mylog.log("coInterface.setDomHtmlPosition", coInterface.menu.initTopPostion);
		$("#mainNav").css({ "top": coInterface.menu.initTopPos, "left": coInterface.menu.initLeftPos, "right": coInterface.menu.initRightPos });

		//heightNav=$("#mainNav").outerHeight();
		heightTopMenu = $("#mainNav").outerHeight() + coInterface.menu.initTopPos;
		//mylog.log("coInterface.setDomHtmlPosition", heightTopMenu, $("#mainNav").outerHeight());
		$(".dropdownApps-menuTop .dropdown-menu, #menuLeft.pos-after-menu-top, #mainNav .menu-xs-container, #affix-sub-menu").css("top", heightTopMenu);
		$("#mainNav .menu-xs-container, #affix-sub-menu").css("left", coInterface.menu.initLeftPos);
		if (typeof costum != "undefined" && notEmpty(costum) && typeof costum.editMode != "undefined" && !costum.editMode)
			$("#notificationPanelSearch.horizontal.arrow_box, #notificationPanelSearch.vertical.arrow_box, #home-floop, #floopDrawerDirectory, #modal-preview-coop, .main-container .dropdown-main-menu, #mainNav .dropdown-result-global-search, .main-container .portfolio-modal.modal, .portfolio-modal.modal.vertical").css("top", heightTopMenu);

		$(" #affix-sub-menu .dropdown-result-global-search, .searchObjCSS .menu-filters-xs").css("top", (heightTopMenu + $("#text-search-menu").height()));
		coInterface.setTopPosition(false);
		/*if(heightNav > 70){
			marginTop=(heightNav-55);
			$("#mainNav .navbar-right, #mainNav .navbar-item-left, #mainNav .navbar-item-left .menu-app-top").css("margin-top", marginTop); 
		} */
	},
	setTopPosition: function (bool) {
		//mylog.log("coInterface.setTopPosition", bool);
		headerScaling = bool;
		heightPos = $("#mainNav").outerHeight();
		$("#menuLeft").css("left", coInterface.menu.initLeftPos);
		let leftPos = (notNull($("#menuLeft").outerWidth())) ? $("#menuLeft").outerWidth() + coInterface.menu.initLeftPos : coInterface.menu.initLeftPos;
		if ($("#mainNav").hasClass("pos-after-menu-left")) {
			leftMenuTop = ($("#menuLeft").is(":visible")) ? leftPos : coInterface.menu.initLeftPos;
			$("#mainNav").css("left", leftMenuTop);
		} else
			$("#mainNav").css("left", coInterface.menu.initLeftPos);
		if ($("#menuLeft").hasClass("pos-top-0")) {
			$("#menuLeft").css("top", 0);
		}
		if ($("#menuLeft").hasClass("transparent")) {
			$("#mapContent, #modal-calendar, #filters-nav.menuFilters-vertical, .headerSearchContainer.affix, .filters-affix-dropdown, #subMenu").css({ "left": coInterface.menu.initLeftPos, "right": coInterface.menu.initRightPos });
			// Decalage des boutons de control de la map zoom
			$("#mapContent .leaflet-left").css("left", ($("#menuLeft").outerWidth() + 10));
			$(".main-container").css({ "paddingLeft": coInterface.menu.initLeftPos, "paddingRight": coInterface.menu.initRightPos });
			//leftPosModal=($("#modalCoop").is(":visible")) ? $("#menuCoop").outerWidth() : leftPos;
			$(".main-container .portfolio-modal.modal").css({ "paddingLeft": coInterface.menu.initLeftPos, "paddingRight": coInterface.menu.initRightPos });
			morePaddingLeft = (leftPos > 0) ? (leftPos + 10) : coInterface.menu.initLeftPos;
			$(".central-section, #search-content, .live-container, .container-classified-app, .filters-affix-dropdown, .headerSearchContainer.affix, #content-view-admin").css({ "paddingLeft": morePaddingLeft, "paddingRight": coInterface.menu.initRightPos });
		} else {
			$("#mapContent, #modal-calendar, #filters-nav.menuFilters-vertical, .headerSearchContainer.affix, .filters-affix-dropdown, .searchObjCSS .menu-filters-xs, #subMenu").css({ "left": leftPos, "right": coInterface.menu.initRightPos });
			$(".main-container").css({ "paddingLeft": leftPos, "paddingRight": coInterface.menu.initRightPos });
			// leftPosModal=($("#modalCoop").is(":visible")) ? $("#menuCoop").outerWidth() : leftPos;
			$(".main-container .portfolio-modal.modal").css({ "paddingLeft": leftPos, "paddingRight": coInterface.menu.initRightPos });
		}
		if ($("#menuLeft").hasClass("align-middle")) {
			heightBtns = 0;
			$("#menuLeft").children().each(function () {
				heightBtns += $(this).height();
			});
			padTopMenu = ($("#menuLeft").height() - heightBtns) / 2;
			$("#menuLeft").css("paddingTop", padTopMenu);
		}
		if (coInterface.menu.initTopPos != 0)
			heightPos = heightPos + coInterface.menu.initTopPos;
		if ($("#affix-sub-menu").is(":visible"))
			heightPos = heightPos + $("#affix-sub-menu").outerHeight();
		$("#filter-scopes-menu").css("top", heightPos);
		if ($("#filter-scopes-menu").is(":visible"))
			heightPos = heightPos + $("#filter-scopes-menu").outerHeight();
		if ($("#menuApp").hasClass("subMenuTop")) {
			$("#menuApp").css("top", heightPos);
			heightPos = heightPos + $("#menuApp").outerHeight();
		}
		if ($("#subMenu").is(":visible")) {
			$("#subMenu").css({ "top": heightPos + "px" });
			heightPos = heightPos + $("#subMenu").outerHeight();
		}
		$("#filters-nav").css("top", heightPos);
		if ($("#filters-nav").is(":visible"))
			heightPos = heightPos + $("#filters-nav").outerHeight();
		$("#mapContent, #modal-calendar, .filters-affix-dropdown, .searchObjCSS .menu-filters-xs").css("top", heightPos);
		//si le menu top des elements est présent et fixe, on descent la carte en dessous 
		if ($("#menu-top-profil-social").length > 0 && $("#menu-top-profil-social").hasClass("affix")) {
			$("#mapContent").css("top", (heightPos + $("#menu-top-profil-social").outerHeight()));
		}
		if ($(".main-container .headerSearchContainer").hasClass("affix")) {
			mylog.log("coInterface.setTopPosition headerSearchContainer heightPos", heightPos);
			$(".main-container .headerSearchContainer").css("top", heightPos);
			heightPos = heightPos + $(".main-container .headerSearchContainer").outerHeight();
		} else {
			$(".main-container .headerSearchContainer").css({ "top": "inherit", 'left': 'inherit', 'right': 'inherit' });
		}
		if ($("#headerBand").length > 0 && coInterface.menu.initTopPos == 0)
			heightPos = heightPos + $("#headerBand").outerHeight();
		$(".main-container").css("padding-top", heightPos);
		if (notNull(costum) && typeof costum.setTopPosition == "function")
			costum.setTopPosition(leftPos, heightPos);
		setTimeout(function () {
			headerScaling = false;

			// add max height to menuTopLeft, menuTopCenter, menuTopRight
			// var heightMenuPosition = Math.max($("#menuTopLeft").outerHeight(), $("#menuTopCenter").outerHeight(), $("#menuTopRight").outerHeight());
			// $("#menuTopLeft, #menuTopCenter, #menuTopRight").css({ "height": heightMenuPosition + "px" });
			coInterface.adjustNavbarHeight();
		}, 300);

		// Initialize the navbar observer
		coInterface.initializeNavbarObserver();
		// Add event listeners for load and resize
		window.removeEventListener("load", coInterface.adjustNavbarHeight);
		window.removeEventListener("resize", coInterface.adjustNavbarHeight);
		window.addEventListener("load", coInterface.adjustNavbarHeight);
		window.addEventListener("resize", coInterface.adjustNavbarHeight);
	

		/* add style for dropdown full width */
		var heightMainNav = $("#mainNav").height();
		var initialPositionMainNav = $("#mainNav").offset();

		var positionForDropDownFullWidth = (typeof initialPositionMainNav != "undefined") ? (initialPositionMainNav.top + heightMainNav) : 0;

		if ($(".dropdown-app-full-width").length > 0) {
			if (costum.editMode) {
				var width = $(".cmsbuilder-content-wrapper").width();
				$(".dropdown-app-full-width .drop-down-sub-menu").css({ position: "fixed", width: `${width}px`, top: `${positionForDropDownFullWidth}px`, left: `${initialPositionMainNav.left}px` });
				$(".cmsbuilder-center-content").on("scroll", function () {
					var currentPositionMainNav = $("#mainNav").offset().top + heightMainNav;
					$(".dropdown-app-full-width .drop-down-sub-menu").css({ position: "fixed", width: `${width}px`, top: `${currentPositionMainNav}px`, left: `${initialPositionMainNav.left}px` });
				})
			} else {
				$(".dropdown-app-full-width .drop-down-sub-menu").css({ position: "fixed", width: "100%", top: `${heightMainNav}px` });
			}
			$(".dropdown-app-full-width .sous-menu-full-width .menu-has-submenu").hover(function (e) {
				e.stopPropagation();
				$(this).next(".sous-menu-btn-navigation-full-width").show();
			})
			$(".dropdown-app-full-width .sous-menu-full-width .sous-menu-btn-navigation-content").on("mouseleave", function () {
				$(".sous-menu-btn-navigation-full-width").css("display", "none");
			})

			if ($(".dropdown-app-full-width").hasClass("open") === false) {
				$(".sous-menu-btn-navigation-full-width").css("display", "none");
			}
		}
		/* end add style for dropdown full width */
	},
	bindResizeEvent: function () {
		$(window).resize(function () {
			//mylog.log("resize");
			var height = $("#mapCanvasBg").height() - 55;
			height = $("#mapCanvasBg").height() - 200;
			$("#scroll-dashboard-dda").css("maxHeight", height);
			coInterface.initHtmlPosition();
			if (notNull(costum) && typeof costum.bindResizeEvent == "function")
				costum.bindResizeEvent();
			//responsivité filters
			var winwidth = $(window).innerWidth();
			if (winwidth > 768) {
				$('.container-filters-menu').show();
			};
			//responsive menu communecter
			if (winwidth >= 768) {
				$('.menu-xs-container').hide();
				$('#show-filters').removeClass("show-icon");
			} else {
				$('.container-filters-menu').hide();
				$('#show-filters').removeClass("show-icon");
			};
			//responsivité Network on Dashboard
			$collapseNet = $("#collapseNet");
			$collapseNet.removeClass('collapse collapse in');
			if (winwidth < 768) {
				//alert("xs");
				return $collapseNet.addClass('collapse');
			} else if (winwidth >= 768) {
				//alert("lg");
				return $collapseNet.addClass('collapse in');
			};

		});
	},
	bindTooltips: function () {
		$(".btn-menu-vertical").mouseenter(function () {
			$(this).find(".tooltips-menu-btn").show();
		}).mouseleave(function () {
			$(this).find(".tooltips-menu-btn").hide();
		});
		$(".btn-menu-tooltips").mouseenter(function () {
			$(this).find(".tooltips-menu-btn").show();
		}).mouseleave(function () {
			$(this).find(".tooltips-menu-btn").hide();
		});
		$(".btn-sub-menu-vertical").mouseenter(function () {
			$(this).find(".tooltips-sub-menu-btn").show();
		}).mouseleave(function () {
			//if($(this).find("tooltips-sub-menu-btn:hover").length == 0 && $(this).length == 0){
			$(this).find(".tooltips-sub-menu-btn").hide();
			//}
		});
	},
	bindButtonOpenForm: function () {
		//window select open form type (selectCreate)


		$(".btn-open-form").off().on("click", function (e) {
			e.stopPropagation();
			var href = $(this).attr("href"),
				patternValidationURL = /^(http|https):\/\/[^ "]+$/;
			if (patternValidationURL.test(href)) {
				location.href = href;
				location.reload()
			} else {
				mylog.log("btn-open-form");
				$('.bootbox').remove();
				var typeForm = $(this).data("form-type");
				currentKFormType = ($(this).data("form-subtype")) ? $(this).data("form-subtype") : null;
				//dynFormCostum = (costum && costum.typeObj && costum.typeObj[typeForm]) ? costum.typeObj[typeForm]["dynFormCostum"] : null;
				if (contextData && contextData.type && contextData.id) {

					dyFObj.openForm(typeForm, "sub");
				}
				else {

					dyFObj.openForm(typeForm);
				}
			}
		});
		$(".btn-open-optionModal").off().on("click", function () {
			mylog.log("btn-open-optionModal");
			//coInterface.optionModalOpen($(this).data("form-type"));
			var typeForm = $(this).data("form-type");
			var modalHtml = "";
			var btnClass = '';
			var subText = '';
			var btnClasses = {};
			$.each(typeObj[typeForm].optionModal.list, function (k, v) {
				btnClass = (typeof v.btnClass != "undefined") ? v.btnClass : 'btn-open-form';
				subText = (typeof v.subText != "undefined") ? v.subText + "<br/>" : '';
				btnClasses[btnClass] = true;
				var btnLink = (typeof v.link != "undefined") ? v.link : 'javascript:;';
				modalHtml += "<a href='" + btnLink + "' class='" + btnClass + "' data-form-type='" + typeForm + "'> <i class='fa fa-" + v.icon + " fa-2x'></i> " + v.label + "</a><br/>" + subText;
			})

			bootbox.dialog({
				title: typeObj[typeForm].optionModal.title,
				message: modalHtml
			}).init(function () {
				coInterface.bindButtonOpenForm();
				$.each(btnClasses, function (btc, v) {
					//alert(typeof btc+":"+btc);
					//class name can be a callback function ex : crteOpenForm inside ctenat/ctenat_index.js
					if (btc != 'btn-open-form' && typeof eval("costumBindButton." + btc) == "function")
						eval("costumBindButton." + btc)();
				})

			});

		});
	},
	optionModalOpen: function (typeForm) {
		var modalHtml = "";
		var btnClass = '';
		var subText = '';
		var btnClasses = {};
		$.each(typeObj[typeForm].optionModal.list, function (k, v) {
			btnClass = (typeof v.btnClass != "undefined") ? v.btnClass : 'btn-open-form';
			subText = (typeof v.subText != "undefined") ? v.subText + "<br/>" : '';
			btnClasses[btnClass] = true;
			var btnLink = (typeof v.link != "undefined") ? v.link : 'javascript:;';
			modalHtml += "<a href='" + btnLink + "' class='" + btnClass + "' data-form-type='" + typeForm + "'> <i class='fa fa-" + v.icon + " fa-2x'></i> " + v.label + "</a><br/>" + subText;
		})

		bootbox.dialog({
			title: typeObj[typeForm].optionModal.title,
			message: modalHtml
		}).init(function () {
			coInterface.bindButtonOpenForm();
			$.each(btnClasses, function (btc, v) {
				//alert(typeof btc+":"+btc);
				//class name can be a callback function ex : crteOpenForm inside ctenat/ctenat_index.js
				if (btc != 'btn-open-form' && typeof eval("costumBindButton." + btc) == "function")
					eval("costumBindButton." + btc)();
			})
		});
	},
	eventTohideTooltip: function () {
		$(this).tooltip('hide')
	},
	bindLBHLinks: function () {
		mylog.log("bindLBHLinks");
		if (isBinding) return; // Exit if already binding to avoid recursive calls
    	isBinding = true;

		$(".lbh").unbind("click").on("click", function (e) {
			e.stopPropagation();
			e.preventDefault();
			$("#openModal").modal("hide");
			mylog.warn("***************************************");
			mylog.warn("coInterface.bindLBHLinks", $(this).attr("href"));
			mylog.warn("***************************************");
			//searchObject.reset();
			var h = ($(this).data("hash")) ? $(this).data("hash") : $(this).attr("href");
			urlCtrl.closePreview();
			$("#modalExplain").modal("hide");
			dyFObj.closeForm();
			urlCtrl.loadByHash(h);
		});
		$(".lbh-menu-app").unbind("click").on("click", function (e) {
			if (!$(this).hasClass("editing")) {
				e.preventDefault();
				coInterface.simpleScroll(0, 500);
				if (typeof mapCO != "undefined")
					mapCO.clearMap();
				contextData = null;
				historyReplace = true;
				var h = "";
				if ($(this).data("hash"))
					h = $(this).data("hash")
				else {
					try { h = new URL($(this).attr("href")).hash }
					catch { h = $(this).attr("href") }
				}
				urlCtrl.loadByHash(h);
				htmlExists(this, function () {
					$this = $(this);
					$this.addClass("active");
					$('.lbh-anchor,.lbh-dropdown').removeClass("active");
					if ($this.parent().hasClass("drop-down-sub-menu-content")) {
						$this.parent().parent().parent().find('.lbh-dropdown.cosDyn-buttonList').addClass('active');
					}
				})
			}
		});
		$(".lbh-anchor").unbind("click").on("click", function (event) {
			//prevent the default action for the click event
			event.preventDefault();

			//get the full url - like mysitecom/index.htm#home
			var full_url = this.href;

			//split the url by # and get the anchor target name - home in mysitecom/index.htm#home
			var parts = full_url.split("#");
			var trgt = parts[1];
			var scrollDOm = ($(".cmsbuilder-center-content").length > 0) ? ".cmsbuilder-center-content" : 'html, body';

			//get the top offset of the target anchor

			if ($("div[data-anchor-target='" + trgt + "']").length > 0) {
				var target_offset = $("div[data-anchor-target='" + trgt + "']").offset();
				var target_top = target_offset.top;
				if ($("#mainNav").length > 0)
					target_top = target_top - $("#mainNav").outerHeight();
				//goto that anchor by setting the body scroll top to anchor top
				$(scrollDOm).animate({ scrollTop: target_top }, 100, 'easeInSine');
			} else {
				urlCtrl.afterLoad = function () {
					if ($("div[data-anchor-target='" + trgt + "']").length > 0) {
						var target_offset = $("div[data-anchor-target='" + trgt + "']").offset();
						var target_top = target_offset.top;
						if ($("#mainNav").length > 0)
							target_top = target_top - $("#mainNav").outerHeight();
						//goto that anchor by setting the body scroll top to anchor top
						$(scrollDOm).animate({ scrollTop: target_top }, 100, 'easeInSine');
					}
					urlCtrl.afterLoad = null;
				}
				historyReplace = true;
				var mainHash = "#";
				if ($(this).closest(".drop-down-sub-menu").length > 0) {
					if ($(this).closest(".drop-down-sub-menu").data("upperhash") != location.hash) {
						mainHash = $(this).closest(".drop-down-sub-menu").data("upperhash");
					}
				}
				mylog.log("mainHash", mainHash, $(this));
				urlCtrl.loadByHash(mainHash);
			}
			$(".lbh-menu-app, .lbh-anchor").removeClass("active");
			$(this).addClass("active");
		});
		// Open an url with specific module in url
		$(".lbh-module").unbind("click").on("click", function (e) {
			e.preventDefault();
			coInterface.simpleScroll(0, 500);
			//searchObject.reset();
			//if(!empty(filtersObj))
			if (typeof mapCO != "undefined")
				mapCO.clearMap();
			contextData = null;
			historyReplace = true;
			var h = ($(this).data("hash")) ? $(this).data("hash") : $(this).attr("href");
			urlCtrl.loadByHash(h, "survey/");
		});
		//open any url in a modal window
		$(".lbhp").unbind("click").on("click", function (e) {
			e.preventDefault();
			$("#openModal").modal("hide");
			mylog.warn("***************************************");
			mylog.warn("!coInterface.bindLBHLinks Preview", $(this).attr("href"), $(this).data("modalshow"));
			mylog.warn("***************************************");
			var h = ($(this).data("hash")) ? $(this).data("hash") : $(this).attr("href");
			if ($(this).data("modalshow")) {
				mylog.log("coInterface.bindLBHLinks Preview if");
				url = (h.indexOf("#") == 0) ? urlCtrl.convertToPath(h) : h;
				if (h.indexOf("#page") >= 0)
					url = "app/" + url
				smallMenu.openAjaxHTML(baseUrl + '/' + moduleId + "/" + url);

			}
			else {
				mylog.log("coInterface.bindLBHLinks Preview else");
				url = (h.indexOf("#") == 0) ? urlCtrl.convertToPath(h) : h;
				smallMenu.openAjaxHTML(baseUrl + '/' + moduleId + "/" + url);
			}
		});
		//open any url in a preview window
		$(".lbh-preview-element").unbind("click").on("click", function (e) {
			e.preventDefault();
			mylog.warn("***************************************");
			mylog.warn("coInterface.bindLBHLinks Preview ELEMENT", $(this).attr("href"), $(this).data("modalshow"));
			mylog.warn("***************************************");
			onchangeClick = false;
			link = (typeof $(this).data("hash") != "undefined" && notNull($(this).data("hash"))) ? $(this).data("hash") : $(this).attr("href");
			previewHash = link.split(".");
			if ($.inArray(previewHash[2], ["proposals", "proposal", "actions", "resolutions"]) >= 0) {
				ddaT = (previewHash[2] == "proposals") ? "proposal" : previewHash[2];
				uiCoop.getCoopDataPreview(ddaT, previewHash[4]);
			} else {
				hashT = location.hash.split("?");
				getStatus = urlCtrl.getUrlSearchParams();

				hashNav = (hashT[0].indexOf("#") < 0) ? "#" + hashT[0] : hashT[0];
				//hashT[0].substring(0) substring ne sert à rien ... why ???    	
				if (previewHash.length > 1) {
					urlHistoric = hashNav + "?preview=" + previewHash[2] + "." + previewHash[4];
					if ($("#entity" + previewHash[4]).length > 0) setTimeout(function () { $("#entity" + previewHash[4]).addClass("active"); }, 200);
				}
				else {
					mylog.log("hashNav", hashNav);
					pageName = (previewHash[0].indexOf("#") == 0) ? previewHash[0].substr(1) : previewHash[0];
					urlHistoric = hashNav + "?preview=page.costum.view." + pageName;
					// Voir intégration dans history.replaceState !!
					if (typeof costum.app[link] != "undefined" && typeof (costum.app[link].hash != "undefined") && costum.app[link].hash.indexOf("view") > -1 && typeof (costum.app[link].urlExtra != "undefined")) {
						link = "view" + costum.app[link].urlExtra;
					}

				}
				if (getStatus != "") urlHistoric += "&" + getStatus;
				history.replaceState({}, null, urlHistoric);
				urlCtrl.openPreview(link);

				//permet d'ajouter le hash en paramètre get
				if (!costum || !costum.editMode)
					history.replaceState({}, null, urlCtrl.getUrlWithHashInSearchParams().href)
			}
		});
		$(".open-xs-menu").off().on("click", function () {
			menuToShow = $(this).data("target");
			iconClass = $(this).data("icon");
			labelButton = $(this).data("label");
			if ($(this).hasClass("close-menu")) {
				urlCtrl.closeXsMenu();

			} else {
				$(this).addClass("close-menu");
				$(this).find("i").removeClass(iconClass).addClass("fa-times");
				if ($(this).find(".labelMenu").length)
					$(this).find(".labelMenu").text(trad.close);
				$(".menu-xs-container." + menuToShow).show(400);
			}
		});
		$("#ctrlK .search-btn").off().on("click", function () {
			var event = new KeyboardEvent('keydown', {
				key: 'k',
				keyCode: 75,
				ctrlKey: true
			});
			document.dispatchEvent(event);
		})

		isBinding = false;
	},
	create_costum: function (args) {
		var path2value = {
			id: args.id,
			collection: args.collection,
			path: "allToRoot",
			value: {
				costum: {
					slug: "costumize",
					firstStepper: { step: 0 },
					htmlConstruct: {
						header: {
							menuTop: {
								activated: true,
								left: {
									buttonList: { logo: true, app: { label: true } }
								}
							}
						}
					},
					language: userConnected.language,
					css: {
						menuTop: {
							left: {
								app: {
									buttonList: {
										fontWeight: "800",
										textTransform: "capitalize",
										fontSize: "17px",
										paddingLeft: "10px"
									},
									paddingBottom: "5px",
									paddingLeft: "5px",
									paddingRight: "5px",
									paddingTop: "5px"
								}
							}
						}
					}
				}
			}
		};

		return new Promise(function (resolve) {
			dataHelper.path2Value(path2value, resolve);
		});
	},
	bindCostumizer: function () {
		$(".startCostumizer").off().on("click", function () {
			coInterface.create_costum(contextData).then(function (response) {
				if (response.result) {
					window.open(baseUrl + '/costum/co/index/slug/' + contextData.slug + '/edit/true', '_blank');
				}
			}).finally(function () {
				$("#modalFirstStepCostum").modal('hide');
			});
		});
		$(".startWithFeature").off().on("click",function(){
			console.log($("[type='checkbox']"))
		})
	},
	setLanguage(lang, changeByNavBtn = false, changeUserLangToo = false) {
		if (notNull(costum)) {
			if (changeByNavBtn) {
				var languagesInCookie = $.cookie("costumlanguages");
				var costumLanguages = (typeof languagesInCookie != "undefined") ? JSON.parse(languagesInCookie) : {};
				costumLanguages[costum.contextSlug] = lang;
				$.cookie("costumlanguages", JSON.stringify(costumLanguages), { expires: 365, path: "/"});
				if (changeUserLangToo && notEmpty(userId)) {
					$.cookie('lang', lang, { expires: 365, path: "/" });
					param = {
						name: "language",
						value: lang,
						pk: userId
					};
					ajaxPost(
						null,
						baseUrl + "/" + moduleId + "/element/updatefields/type/citoyens",
						param,
						function (data) {
							if (data.result) {
								toastr.success(data.msg);
								location.reload();
							}
						}
					);
				} else
					window.location.reload();
			}
		} else {
			$.cookie('lang', lang, { expires: 365, path: "/" });
			if (userId != "") {
				param = {
					name: "language",
					value: lang,
					pk: userId
				};
				ajaxPost(
					null,
					baseUrl + "/" + moduleId + "/element/updatefields/type/citoyens",
					param,
					function (data) {
						if (data.result) {
							toastr.success(data.msg);
							location.reload();
						}
					}
				);
			} else {
				location.reload();
			}
		}
	},
	scrollTo: function (target) {
		mylog.log("coInterface.scrollTo target", target);
		if ($(target).length >= 1) {
			var heightTopBar = $("#mainNav").height();
			if ($("#territorial-menu").length >= 1) heightTopBar = $("#mainNav").height() + $("#territorial-menu").height() + 20;
			if ($(".headerSearchContainer").length >= 1) heightTopBar += ($(".headerSearchContainer").outerHeight() + 20);
			$('html, body').stop().animate({
				scrollTop: $(target).offset().top - 60 - heightTopBar
			}, 500, '');
		}
	},
	simpleScroll: function (height, speed) {
		var scrollToPos = (notNull(height)) ? height : 71;
		var speedScroll = (notNull(speed)) ? speed : 100;
		$('html,body').animate({ scrollTop: scrollToPos }, speedScroll, '');
	},
	menu: {
		initTopPos: 0,
		initLeftPos: 0,
		initRightPos: 0,
		showFilters: false,
		showScope: false,
		set: function (hash) {
			mylog.log("coInterface.menu.set", hash);
			$(".filterXs").remove();
			if (!notEmpty(hash) || hash == "#") {
				redirK = (userId != "") ? "logged" : "unlogged";
				pageMenu = "#" + urlCtrl.loadableUrls["#app.index"]["redirect"][redirK];
			} else if (hash.indexOf(".") >= 0) {
				pageMenu = hash.split(".")[0];
			} else
				pageMenu = hash;
			if (pageMenu.indexOf("?") >= 0)
				pageMenu = pageMenu.split("?")[0];
			if (pageMenu.indexOf("@") == 1)
				pageMenu = "#page";
			mylog.log("coInterface.menu.set pageMenu", pageMenu);
			useFilter = (typeof urlCtrl.loadableUrls[pageMenu] != "undefined" && typeof urlCtrl.loadableUrls[pageMenu].useFilter != "undefined") ? urlCtrl.loadableUrls[pageMenu].useFilter : false;
			subdomainName = (typeof urlCtrl.loadableUrls[pageMenu] != "undefined" && typeof urlCtrl.loadableUrls[pageMenu].subdomainName != "undefined") ? urlCtrl.loadableUrls[pageMenu].subdomainName : "";
			icon = (typeof urlCtrl.loadableUrls[pageMenu] != "undefined" && typeof urlCtrl.loadableUrls[pageMenu].icon != "undefined") ? urlCtrl.loadableUrls[pageMenu].icon : "info";
			dropdownResult = (typeof urlCtrl.loadableUrls[pageMenu] != "undefined" && typeof urlCtrl.loadableUrls[pageMenu].dropdownResult != "undefined") ? urlCtrl.loadableUrls[pageMenu].dropdownResult : false;
			useFooter = (typeof urlCtrl.loadableUrls[pageMenu] != "undefined" && typeof urlCtrl.loadableUrls[pageMenu].useFooter != "undefined") ? urlCtrl.loadableUrls[pageMenu].useFooter : true;
			coInterface.showMapOnLoad = (typeof urlCtrl.loadableUrls[pageMenu] != "undefined" && typeof urlCtrl.loadableUrls[pageMenu].showMap != "undefined") ? urlCtrl.loadableUrls[pageMenu].showMap : false;
			if (typeof urlCtrl.loadableUrls[pageMenu] != "undefined"
				&& typeof urlCtrl.loadableUrls[pageMenu].useMapBtn != "undefined" && !urlCtrl.loadableUrls[pageMenu].useMapBtn)
				$("#mainNav .btn-show-map, #menuRight .btn-show-map").addClass("forcedHide");
			else
				$("#mainNav .btn-show-map, #menuRight .btn-show-map").removeClass("forcedHide");
			//mylog.dir(urlObj);
			$(".lbh-menu-app, .lbh-anchor").removeClass("active");
			$(".lbh-menu-app[data-hash='" + pageMenu + "']").addClass("active");
			$('[href*="' + pageMenu + '"].lbh-menu-app').addClass("active");
			$("#filter-scopes-menu").hide(200);
			if (notEmpty(useFilter)) {
				coInterface.menu.showScope = true;
				coInterface.menu.showFilters = true;
				if (typeof useFilter == "object") {
					coInterface.menu.showScope = (typeof useFilter.scope != "undefined" && useFilter.scope) ? true : false;
					coInterface.menu.showFilters = (typeof useFilter.filters != "undefined" && useFilter.filters) ? true : false;
				}
				classToShow = (coInterface.menu.showScope) ? ".menu-btn-scope-filter" : "";
				if (coInterface.menu.showFilters) {
					classToShow += (notEmpty(classToShow)) ? ", " : "";
					classToShow += (coInterface.menu.showFilters) ? ".btn-show-filters" : "";
				}
				$(classToShow).show().removeClass("forcedHide");
				if (coInterface.menu.showFilters)
					coInterface.menu.resetFilters();
			} else {
				coInterface.menu.showScope = false;
				coInterface.menu.showFilters = false;
				//$("#filters-nav").hide();
				//$(".menu-btn-scope-filter, .btn-show-filters").hide();
				$(".menu-btn-scope-filter.visible-xs, .btn-show-filters.visible-xs").addClass("forcedHide");
			}
			$("#second-search-bar, #main-search-bar, #input-search-map").val("");

			mylog.log("coInterface.menu.set dropdownResult", dropdownResult);
			if (!dropdownResult) {
				//Replace id event of app search bar by id event of search bar using dropdown result like in home, element page;
				$.each(["second-search-bar", "second-search-bar-addon", "second-search-xs-bar", "second-search-xs-bar-addon"], function (e, v) {
					changeId = v.replace("second", "main");
					//changeId+=" central-search-bar";
					$("#" + v).attr("id", changeId).removeClass(v).addClass(changeId);
				});
				$(".main-search-bar, .main-search-bar-addon").addClass("central-search-bar");
			} else {
				//Generate id event for app search bar instead of search bar using dropdown result like in home, element page;
				$.each(["main-search-bar", "main-search-bar-addon", "main-search-xs-bar", "main-search-xs-bar-addon"], function (e, v) {
					changeId = v.replace("main", "second");
					$("#" + v).attr("id", changeId).removeClass(v).addClass(changeId);
				});
				$("#second-search-bar").off().on("keyup", function (e) {
					$("#input-search-map").val($("#second-search-bar").val());
					$("#second-search-xs-bar").val($("#second-search-bar").val());
					if (e.keyCode == 13) {
						searchObject.text = $(this).val();
						myScopes.type = "open";
						myScopes.open = {};
						startGlobalSearch(0, indexStepGS, ".searchBarInMenu");
					}
				});
				$("#second-search-xs-bar").off().on("keyup", function (e) {
					$("#input-search-map").val($("#second-search-xs-bar").val());
					$("#second-search-bar").val($("#second-search-xs-bar").val());
					if (e.keyCode == 13) {
						searchObject.text = $(this).val();
						myScopes.type = "open";
						myScopes.open = {};
						startGlobalSearch(0, indexStepGS, ".searchBarInMenu");
					}
				});
				$("#second-search-bar-addon, #second-search-xs-bar-addon").off().on("click", function () {
					mylog.log("searchInterface.setSearchbar #second-search-bar-addon, #second-search-xs-bar-addon click");
					$("#input-search-map").val($("#second-search-bar").val());
					searchObject.text = $("#second-search-bar").val();
					myScopes.type = "open";
					myScopes.open = {};
					startGlobalSearch(0, indexStepGS, ".searchBarInMenu");
				});
			}
			if (useFooter)
				$("footer").show();
			else
				$("footer").hide();
			//searchInterface.setSearchbar();
		},
		resetFilters: function () {
			$("#filters-nav-list li .menu-button").each(function () {
				if ($(this).hasClass("active")) {
					$(this).removeClass("active");
					$(this).html($(this).attr("title") + " <i class='fa fa-angle-down'></i>");
				}
			});
		},
		renderTools: {

		},
		eventsTools: {

		}
	},
	showLoader: function (domContainer, msg, specClass) {
		color1 = "#354c57";
		color2 = "#9fbd38";
		if (typeof costum != "undefined" && notNull(costum)) {
			if (typeof costum.css != "undefined" && typeof costum.css.loader != "undefined") {
				if (typeof costum.css.loader.ring1 != "undefined" && costum.css.loader.ring1.color != "undefined")
					color1 = costum.css.loader.ring1.color;
				if (typeof costum.css.loader.ring2 != "undefined" && costum.css.loader.ring2.color != "undefined")
					color2 = costum.css.loader.ring2.color;
			}
		}
		loadingMsg = (!notEmpty(msg)) ? trad.currentlyloading : msg;
		classSpec = (notNull(specClass)) ? specClass : "";
		strLoading = "<div class='processingLoader col-xs-12 text-center margin-top-50 " + classSpec + "'>" +
			"<center>" +
			'<div class="lds-css ng-scope">' +
			'<div style="width:100%;height:100%" class="lds-doubl-ring">' +
			'<div style="border-color: transparent ' + color2 + ' transparent ' + color2 + ';"></div>' +
			'<div style="border-color: transparent ' + color1 + ' transparent ' + color1 + ';"></div></div>' +
			'</div><br/>' +
			'<span style="background: linear-gradient(to left,' + color1 + ' 0%,' + color2 + ' 100%);-webkit-background-clip: text;-webkit-text-fill-color: transparent;">' + loadingMsg + "</span>" +
			'</center>' +
			'</div>';
		if (notNull(domContainer))
			$(domContainer).html(strLoading);
		else
			return strLoading;
	},
	showCostumLoader: function (domContainer, msg = trad.currentlyloading) {
		var thisColor1 = "#354c57";
		var thisColor2 = "#9fbd38";
		var thisLoadingMsg = (!notEmpty(msg)) ? trad.currentlyloading : msg;
		if (typeof costum != "undefined" && notNull(costum)) {
			if (typeof costum.css != "undefined" && typeof costum.css.loader != "undefined") {
				if (typeof costum.css.loader.ring1 != "undefined" && costum.css.loader.ring1.color != "undefined")
					thisColor1 = costum.css.loader.ring1.color;
				if (typeof costum.css.loader.ring2 != "undefined" && costum.css.loader.ring2.color != "undefined")
					thisColor2 = costum.css.loader.ring2.color;
			}
		}
		var parentClass = "";
		var loaderCssVar = {
			border1: "2px",
			border2: "2px",
		}
		var loaderStr = `
			<div class="processingLoader">
				<center>
					<div class="lds-css ng-scope">
						<div style="width:100%; height:100%;" class="lds-doubl-ring">
							<div id="ring1" style="border-color: transparent ${thisColor1}; "></div>
							<div id="ring2" style="border-color: transparent ${thisColor2};"></div>
						</div>
					</div></br>
					<span style="background: linear-gradient(to left, ${color1} 0%, ${color2} 100%);-webkit-background-clip: text;-webkit-text-fill-color: transparent;">${thisLoadingMsg}</span>
				</center>
			</div>
		`;
		if (costum && (typeof costum.logo != "undefined" || typeof costum.loaderImg != "undefined") && typeof costum.css != "undefined" && typeof costum.css.loader != "undefined") {
			const objLoader = typeof costum.css != "undefined" && typeof costum.css.loader != "undefined" ? $.extend({}, true, costum.css.loader) : null;
			var ring1 = objLoader.ring1; var ring2 = objLoader.ring2;
			var loaderImg = (typeof costum.loaderImg != "undefined" && costum.loaderImg != "") ? costum.loaderImg : costum.logo
			const name = costum.css.loader.loaderUrl ? costum.css.loader.loaderUrl : "loading_modal";
			var costumLoaderType = {
				loading_modal: `
					<div class="processingLoader">
						<center>
							<div class="lds-css ng-scope">
								<div style="width:100%; height:100%;" class="lds-doubl-ring">
									<div id="ring1" style="border-color: transparent ${ring1.color}; "></div>
									<div id="ring2" style="border-color: transparent ${ring2.color};"></div>
									<img src="${loaderImg}">
								</div>
							</div><br>
							<span style="background: linear-gradient(to left, ${ring1.color} 0%, ${ring2.color} 100%);-webkit-background-clip: text;-webkit-text-fill-color: transparent;">${thisLoadingMsg}</span>
						</center>
					</div>
				`,
				loader_1: `
					<div class="loader_1">
						<span></span>
						<span></span>
						<span></span>
						<span></span>
					</div>
				`,
				loader_2: `
					<div class="dl">
					<div class="dl_container">
						<div class="dl_corner--top"></div>
						<div class="dl_corner--bottom"></div>
					</div>
					<div class="dl_square"><img class="dl_img" src="${loaderImg}"></div>
					</div>
				`,
				loader_3: `
					<div class="horizontal_bar">
						<div class="horizontal_bar_logo">
							<img src="${loaderImg}">
						</div>
						<div class="horizontal_bar_loader">
							<div class="horizontal_bar_loading"></div>
						</div>
					</div>
				`,
				loader_4: `
					<div class="nc">
						<img class="newtons-cradle-img" src="${loaderImg}">
						<div class="newtons-cradle">
							<div style="background-color: ${ring1.color};"></div>
							<div style="background-color: ${ring1.color};"></div>
							<div style="background-color: ${ring2.color};"></div>
						<div style="background-color: ${ring2.color};"></div>
					</div>
				`,
				loader_5: `
					<div class="loader-square">
						<img src="${loaderImg}">
					</div>
				`,
				loader_6: `
					<div class="loader-circle-2">
						<img src="${loaderImg}">
					</div>
				`,
				loader_7: `
					<div class="loader-spanne">
						<div class="loader-spanne-img">
							<img src="${loaderImg}">
						</div>
						<span></span>
						<span></span>
						<span></span>
						<span></span>
					</div>
				`,
				loader_8: `
					<div class="loader-ball-container">
						<img class="loader-ball-img" src="${loaderImg}">
						<div class="loader-ball">
							<div class="dot1"></div>
							<div class="dot2"></div>
							<div class="dot3"></div>
							<div class="dot4"></div>
						</div>
					</div>
				`,
			}
			switch (name) {
				case "loader_2":
					loaderCssVar.border1 = ring1.borderWidth;
					loaderCssVar.border2 = ring2.borderWidth;
					parentClass += "set-medium-height";
					break;
				case "loader_3":
					parentClass += "set-medium-height";
					break;
				case "loader_4":
					parentClass += "set-medium-height"
					break;
				case "loader_5":
					loaderCssVar.border1 = ring1.borderWidth;
					loaderCssVar.border2 = ring2.borderWidth;
					parentClass += "set-medium-height change-display";
					break;
				case "loader_7":
					loaderCssVar.border1 = ring1.borderWidth;
					loaderCssVar.border2 = ring2.borderWidth;
					parentClass += "set-medium-height change-display";
					break;
				case "loader_8":
					loaderCssVar.border1 = ring1.borderWidth;
					loaderCssVar.border2 = ring2.borderWidth;
					parentClass += "set-medium-height change-display set-width-max";
					break;
				default:
					break;
			}

			costumLoaderType[name] ? loaderStr = costumLoaderType[name] : "";
		}
		if (notNull(domContainer)) {
			$(domContainer).html(`
				<div class="call_loader processingLoader margin-top-50 margin-bottom-20 ${parentClass}" style="--loader-color1: ${thisColor1}; --loader-color2: ${thisColor2}; --loader-border1: ${loaderCssVar.border1}; --loader-border2: ${loaderCssVar.border2}">
					${loaderStr}
				</div>
			`);
		}
		else {
			return `
				<div class="call_loader ${parentClass}" style="--loader-color1: ${thisColor1}; --loader-color2: ${thisColor2}; --loader-border1: ${loaderCssVar.border1}; --loader-border2: ${loaderCssVar.border2}">
					${loaderStr}
				</div>
			`;
		}
	},
	counterAnimation: function (element, start, end, duration, formatRes = true) {
		end = end*1;
		start = start*1;
		const $element = element;
		const range = end - start;
		const increment = range / (duration / 16); // Incrément pour chaque frame (~60 FPS)
		let current = start;
		const startTime = performance.now();
  
		function updateCounter(time) {
		  const elapsedTime = time - startTime;
		  current = start + (range * (elapsedTime / duration));
  
		  if ((range > 0 && current >= end) || (range < 0 && current <= end)) {
			current = end; // S'assurer qu'on atteint exactement la valeur finale
		  }
  
		  $element.text(formatRes ? current.toLocaleString("fr-FR", {
			style: "decimal",
			minimumFractionDigits: 2,
			maximumFractionDigits: 2
		  }) : Math.round(current));
  
		  // Ajout de l'effet de zoom
		  $element.css("transform", "scale(1.2)");
		  setTimeout(() => $element.css("transform", "scale(1)"), 100);
  
		  if (current !== end) {
			requestAnimationFrame(updateCounter);
		  }
		}
  
		requestAnimationFrame(updateCounter);
	},
	actions: {
		get_action_status: function (action) {
			var has_tags = action && action.tags && Array.isArray(action.tags);
			var status = typeof action.status === 'string' ? action.status : '';
			var is_tracking = typeof action.tracking !== 'undefined' && JSON.parse(action.tracking);
			var lower_tags = has_tags ? action.tags.map(t => t.toLowerCase()) : [];
			if (status === "done") return ("done");
			if (lower_tags.includes('totest')) return ("totest");
			if (is_tracking) return ("tracking");
			if (lower_tags.includes('next')) return ("next");
			if (lower_tags.includes('discuter')) return ("discuter");
			return ("todo");
		},
		update_action_position: function (args) {
			var url = args.server_url + "/co2/action/reorder/action/{{action}}";
			return new Promise(function (resolve, reject) {
				if (typeof args === 'undefined')
					reject({ success: false, data: 'Parameter is empty' });
				if (typeof args === 'string')
					args = { action: args };
				if (typeof args.after !== 'undefined' && args.after) {
					if (typeof args.after !== 'string')
						reject({ success: false, data: "'After' argument must be an action id" });
					else
						url += '/after/' + args.after;
				}
				url = dataHelper.printf(url, args);
				ajaxPost(null, url, args, resolve, reject);
			});
		},
		request_set_status: function (args) {
			return new Promise(function (resolve, reject) {
				var url;

				if (typeof args !== 'object' || !args)
					reject({
						code: 10,
						msg: 'Vous devez passer un objet en paramètre'
					});
				else {
					if (typeof args.server_url === 'string')
						url = args.server_url;
					else
						url = baseUrl;
					url += '/costum/project/action/request/set_status';
					ajaxPost(null, url, {
						id: args.id,
						status: args.status,
						user: args.user ? args.user : null
					}, resolve, reject);
				}
			});
		},
		get_element_order: function (args) {
			var url = args.server_url + "/co2/action/get_orders/parent/{{parent}}/type/{{type}}";
			var post = { parent: null, type: null };
			return new Promise(function (resolve, reject) {
				post = $.extend(post, args);
				if ([post.parent, post.type].includes(null))
					reject("argument issue");
				url = dataHelper.printf(url, post);
				ajaxPost(null, url, {}, resolve, reject);
			});
		},
		request_create_action: function (post) {
			var url = "/costum/project/action/request/new";

			return new Promise(function (resolve, reject) {
				if (typeof post !== 'object' || !post)
					reject({
						side: "client",
						msg: 'Empty parameter',
						fn: "request_create_action",
					});
				else {
					if (post.server_url) {
						url = post.server_url + url;
						delete post.server_url;
					} else
						url = baseUrl + url;
					var key_list = ['name', 'status', 'parentId', 'parentType'];
					var ft = { success: true, data: [] };
					key_list.forEach(function (key_item) {
						if (typeof post[key_item] === 'undefined') {
							ft.success = false;
							ft.data.push(key_item);
						}
					});
					if (!ft.success)
						ft.data = 'Missing parameters : ' + ft.data.join(', ') + '.';
					if (!ft.success)
						reject({
							side: "client",
							msg: ft.data,
							fn: "request_create_action",
						});
					else
						ajaxPost(null, url, post, response => {
							if (response.success)
								resolve(response.content);
							else
								reject({
									side: "server",
									msg: response.content,
									fn: "request_create_action",
								});
						}, (xhr, status, error) => {
							reject({
								side: "server",
								msg: error,
								fn: "request_create_action",
							});
						});
				}
			});
		},
		request_get_action: function (args) {
			var id;
			var url;
			return new Promise(function (resolve, reject) {
				if (typeof args === 'undefiend')
					reject({ success: false, data: 'Argument is empty' });
				if (typeof args === 'string')
					id = args;
				else if (typeof args.id === 'string')
					id = args.id;
				if (typeof args.server_url === 'string')
					url = args.server_url;
				else
					url = baseUrl;
				url += '/costum/project/action/request/action_basic_info';
				ajaxPost(null, url, { id: id }, resolve, reject);
			});
		},
		request_archive: function (args) {
			var url, id;
			return new Promise(function (resolve, reject) {
				if (typeof args === 'undefiend')
					reject({ success: false, data: 'Argument is empty' });
				if (typeof args === 'string')
					id = args;
				else if (typeof args.id === 'string')
					id = args.id;
				if (typeof args.server_url === 'string')
					url = args.server_url;
				else
					url = baseUrl;
				url += '/costum/project/action/request/archive';
				if (id)
					ajaxPost(null, url, { id: id }, function(response) {
						resolve(response);
						ajaxPost(null, coWsConfig.pingActionManagement, {
							event: '.cd-int-archive', data: {
								action: id,
							}
						}, null, null, { contentType: 'application/json' });
					}, reject);
			});
		},
		request_cancel: function (args) {
			var url,
				id;

			return (new Promise(function (resolve, reject) {
				if (typeof args === 'undefiend')
					reject({
						code: 1,
						msg: 'Argument is empty'
					});
				else if (typeof args === 'string')
					id = args;
				else if (typeof args.id === 'string')
					id = args.id;
				if (typeof args.server_url === 'string')
					url = args.server_url;
				else
					url = baseUrl;
				url += '/costum/project/action/request/cancel';
				if (id)
					ajaxPost(null, url, { id: id }, function (resp) {
						if (resp.success)
							resolve(resp.content);
						else
							reject({
								code: 21,
								msg: resp.content
							});
					}, function (arg0, arg1, arg2) {
						reject({
							code: 22,
							msg: [arg0, arg1, arg2]
						});
					});
			}));
		},
		request_participation: function (args) {
			return new Promise(function (resolve, reject) {
				const fn = "request_participation";
				
				if (typeof args !== 'object' || !args)
					reject({
						side: "client",
						msg: "Argument format error",
						fn,
					});
				else if (typeof args.action === 'undefined')
					reject({
						side: "client",
						msg: "Undefined action id",
						fn,
					});
				else if (typeof args.contributor === "undefined" && typeof args.contributors === "undefined")
					reject({
						side: "client",
						msg: "Should provide contributor id(s)",
						fn,
					});
				else {
					var url = baseUrl;
					if (typeof args.server_url === 'string')
						url = args.server_url;
					url += '/costum/project/action/request/set_contributors';
					var post = {
						action: args.action,
						emiter: wsCO && wsCO.id ? wsCO.id : ''
					};
					if (typeof args.contributors === "object")
						post.contributors = args.contributors;
					else if (typeof args.contributor === "string") {
						post.contributor = args.contributor;
						if (typeof args.participate === 'number')
							post.participate = args.participate;
						else
							post.participate = 0;
					}
					ajaxPost(null, url, post, response => {
						if (response.success)
							resolve(response.content);
						else
							reject({
								side: "server",
								msg: response.content,
								fn,
							});
					}, (xhr, status, error) => {
						reject({
							side: "server",
							msg: error,
							fn,
						});
					});
				}
			});
		},
		ping_update_task: function(args) {
			return new Promise(function(resolve, reject){
				var url = baseUrl + '/costum/project/action/request/socket_task_update';
				var post = {};
				if (typeof args === 'string')
					post.action = args;
				else if (typeof args === 'object' && args)
					post = args;
				if (typeof post.action !== 'string')
					return (
						reject({
							code: 10,
							msg: "L'action est vide"
						})
					);
				if (typeof wsCO === 'object' && typeof wsCO.id === 'string' && wsCO.id.length)
					post.emiter = wsCO.id;
				ajaxPost(null, url, post, resolve, reject);
			})
		},
		xhrListByCostum: function ()
		{
			return (new Promise(
				function (resolve, reject)
				{
					var url, post;

					url = '/co2/action/cornerdev/method/get-by-costum';
					ajaxPost(null, url, post, function (response)
					{
						if (response.success)
							resolve(response.content);
						else
							reject({
								side: 'server',
								msg: response.content,
							});
					}, function (error, xhr)
					{
						reject({
							side: 'server',
							msg: [error, xhr]
						});
					});
				}
			));
			ajaxPost()
		}
	}
};

var urlCtrl = {
	afterLoad: null,
	loadableUrls: {
		"#modal.": { title: 'OPEN in Modal' },
		"#event.calendarview": { title: "EVENT CALENDAR ", icon: "calendar" },
		"#form": { title: "OPEN FORM ", icon: "calendar", "module": "survey" },
		"#answer": { title: "EVENT CALENDAR ", icon: "calendar", "module": "survey" },
		"#city.opendata": { title: 'STATISTICS ', icon: 'line-chart' },
		"#person.telegram": { title: 'CONTACT PERSON VIA TELEGRAM ', icon: 'send' },
		"#event.detail": { aliasParam: "#page.type.events.id.$id", params: ["id"], title: 'EVENT DETAIL ', icon: 'calendar' },
		"#organization.detail": { aliasParam: "#page.type.organizations.id.$id", params: ["id"], title: 'ORGANIZATION DETAIL ', icon: 'users' },
		"#project.detail": { aliasParam: "#page.type.projects.id.$id", params: ["id"], title: 'PROJECT DETAIL ', icon: 'lightbulb-o' },
		"#project.addchartsv": { title: 'EDIT CHART ', icon: 'puzzle-piece' },
		"#event.directory": { aliasParam: "#page.type.events.id.$id.view.directory.dir.attendees", params: ["id"], title: 'EVENT DETAIL ', icon: 'calendar' },
		"#organization.directory": { aliasParam: "#page.type.organizations.id.$id.view.directory.dir.members", params: ["id"], title: 'ORGANIZATION DETAIL ', icon: 'users' },
		"#project.directory": { aliasParam: "#page.type.projects.id.$id.view.directory.dir.contributors", params: ["id"], title: 'PROJECT DETAIL ', icon: 'lightbulb-o' },
		"#news.detail": { aliasParam: "#page.type.news.id.$id", params: ["id"], title: 'NEWS', icon: 'rss' },
		"#news.index": { aliasParam: "#page.type.$type.id.$id", params: ["type", "id"], title: 'NEWS', icon: 'rss' },
		"#project.addchartsv": { title: 'EDIT CHART ', icon: 'puzzle-piece' },
		"#chart.addchartsv": { title: 'EDIT CHART ', icon: 'puzzle-piece' },
		"#gantt.addtimesheetsv": { title: 'EDIT TIMELINE ', icon: 'tasks' },
		"#graph.viewer": { title: 'GRAPE VIEW', icon: 'share-alt', useHeader: true },
		//"#news.detail" : {title:'NEWS DETAIL ', icon : 'rss' },
		//"#news.index.type" : {title:'NEWS INDEX ', icon : 'rss', menuId:"menu-btn-news-network","urlExtraParam":"isFirst=1" },
		"#need.detail": { title: 'NEED DETAIL ', icon: 'cubes' },
		"#need.addneedsv": { title: 'NEED DETAIL ', icon: 'cubes' },
		"#city.creategraph": { title: 'CITY ', icon: 'university', menuId: "btn-geoloc-auto-menu" },
		"#city.graphcity": { title: 'CITY ', icon: 'university', menuId: "btn-geoloc-auto-menu" },
		"#city.statisticPopulation": { title: 'CITY ', icon: 'university' },
		"#rooms.index.type.cities": { title: 'ACTION ROOMS ', icon: 'cubes', menuId: "btn-citizen-council-commun" },
		"#rooms.editroom": { title: 'ADD A ROOM ', icon: 'plus', action: function () { editRoomSV(); } },
		"#element.aroundme": { title: "Around me", icon: 'crosshairs', menuId: "menu-btn-around-me" },
		"#element.notifications": { title: 'DETAIL ENTITY', icon: 'legal' },
		"#person.invite": { title: 'DETAIL ENTITY', icon: 'legal' },
		"#element": { title: 'DETAIL ENTITY', icon: 'legal' },
		"#gallery": { title: 'ACTION ROOMS ', icon: 'photo' },
		"#comment.": { title: 'DISCUSSION ROOMS ', icon: 'comments' },
		"#admin.cleantags": { title: 'CLEAN TAGS', icon: 'download' },
		"#stat.chartglobal": { title: 'STATISTICS ', icon: 'bar-chart' },
		"#settings.redirect": { title: 'Settings ', icon: 'cogs' },
		"#stat.chartlogs": { title: 'STATISTICS ', icon: 'bar-chart' },
		"#default.live": { title: "FLUX'Direct", icon: 'heartbeat', menuId: "menu-btn-live" },
		"#default.login": { title: 'COMMUNECTED AGENDA ', icon: 'calendar' },
		"#showTagOnMap.tag": { title: 'TAG MAP ', icon: 'map-marker', action: function (hash) { showTagOnMap(hash.split('.')[2]) } },
		"#define.": { title: 'TAG MAP ', icon: 'map-marker', action: function (hash) { showDefinition("explain" + hash.split('.')[1]) } },
		"#data.index": { title: 'OPEN DATA FOR ALL', icon: 'fa-folder-open-o' },
		"#opendata": { "alias": "#data.index" },
		"#interoperability.copedia": { title: 'COPEDIA', icon: 'fa-folder-open-o', useHeader: true },
		"#interoperability.co-osm": { title: 'COSM', icon: 'fa-folder-open-o', useHeader: true },
		"#chatAction": { title: 'CHAT', icon: 'comments', action: function () { rcObj.loadChat("", "citoyens", true, true) }, removeAfterLoad: true },
		"#map-network": { hash: "#app.map", subdomain: "map", subdomainName: "Map" },
		"#news-builder": { hash: "#app.newsbuilder" }
	},
	getAnchorPages: function () {
		var anchorList = [];
		$.each(urlCtrl.loadableUrls, function (e, v) {
			if (typeof v.lbhAnchor != "undefined")
				anchorList.push(e);
		})
		return anchorList;
	},
	shortVal: ["p", "poi", "s", "o", "e", "pr", "c", "cl"/* "s","v","a", "r",*/],
	shortKey: ["citoyens", "poi", "siteurl", "organizations", "events", "projects", "cities", "classifieds"/*"entry","vote" ,"action" ,"rooms" */],
	map: function (hash) {
		if (typeof hash == "undefined") return {
			hash: "#",
			type: "",
			id: ""
		};
		hashT = hash.split('.');
		return {
			hash: hash,
			type: hashT[2],
			id: hashT[4]
		};
	},
	convertToPath: function (hash) {
		return hash.substring(1).replace("#", "").replace(/\./g, "/");
	},
	//manages url short cuts like eve_xxxxx
	//warning : works with only 1 underscore 
	//can contain more variables eve_xxxxx.viewer.dsdsd
	checkAndConvert: function (hash) {
		hashT = hash.split('_');
		mylog.log("-------checkAndConvert : ", hash, hashT);
		pos = $.inArray(hashT[0].substring(1), urlCtrl.shortVal);
		if (pos >= 0) {
			type = urlCtrl.shortKey[pos];
			hash = "#page.type." + type + ".id." + hashT[1];
			mylog.log("converted hash : ", hash);
		}
		return hash;
	},
	firstLoad: true,
	jsController: function (hash) {
		mylog.log("jsController", hash);
		hash = urlCtrl.checkAndConvert(hash);
		//alert("jsController"+hash);
		mylog.log("jsController", hash);
		res = false;
		$(".menuShortcuts").addClass("hide");
		//mylog.log("urlCtrl.loadableUrls", urlCtrl.loadableUrls);
		$.each(urlCtrl.loadableUrls, function (urlIndex, urlObj) {
			//mylog.log("replaceAndShow2",urlIndex);
			if (hash.indexOf(urlIndex) >= 0) {
				if (typeof urlCtrl.loadableUrls[hash] != "undefined" && hash !== urlIndex)
					return true;
				if (urlObj.goto) {
					window.location.href = urlObj.goto;
					return false;
				}
				coInterface.menu.set(hash);

				endPoint = urlCtrl.loadableUrls[urlIndex];
				mylog.log("jsController 2", endPoint, "login", endPoint.login, endPoint.hash);

				if (typeof endPoint.login == undefined || !endPoint.login || (endPoint.login && userId)) {
					//alises are renaming of urls example default.home could be #home
					if (endPoint.alias) {
						endPoint = urlCtrl.jsController(endPoint.alias);
						return false;
					}
					if (endPoint.aliasParam) {
						hashT = hash.split(".");
						alias = endPoint.aliasParam;
						$.each(endPoint.params, function (i, v) {
							$.each(hashT, function (ui, e) {
								if (v == e) {
									paramId = hashT[ui + 1];
									alias = alias.replace("$" + v, paramId);
								}
							});
						});
						endPoint = urlCtrl.jsController(alias);
						return false;
					}
					// an action can be connected to a url, and executed
					if (endPoint.action && typeof endPoint.action == "function") {
						endPoint.action(hash);
					} else {
						//classic url management : converts urls by replacing dots to slashes and ajax retreiving and showing the content 
						extraParams = (endPoint.urlExtraParam) ? "?" + endPoint.urlExtraParam : "";
						urlExtra = (endPoint.urlExtra) ? endPoint.urlExtra : "";
						//execute actions before teh ajax request
						res = false;
						if (endPoint.preaction && typeof endPoint.preaction == "function")
							res = endPoint.preaction(hash);
						//hash can be iliased
						if (endPoint.hash) {
							hash = hash.replace(urlIndex, endPoint.hash);
						}
						if (hash.indexOf("?") >= 0) {
							hashT = hash.split("?");
							mylog.log(hashT);
							hash = hashT[0];
							extraParams = "?" + hashT[1];
						}
						if (extraParams.indexOf("#") >= 0) {
							extraParams = extraParams.replace("#", "%hash%");
						}
						path = urlCtrl.convertToPath(hash);
						pathT = path.split('/');
						//open path in a modal (#openModal)
						if (pathT[0] == "modal") {
							path = path.substring(5);
							//alert(baseUrl+'/'+moduleId+path);
							smallMenu.openAjaxHTML(baseUrl + '/' + moduleId + path);
						} else {

							mod = moduleId + '/';
							if (moduleId != activeModuleId && activeModuleId != "costum") {
								mod = '';
								//go get the path , module is given in the hash

							} else if (endPoint.module) {
								mod = (endPoint.module !== true) ? endPoint.module + "/" : "";
							}

							mylog.log("urlCtrl.loadByHash jsController before showAjaxPanel", { mod: mod, path: path, urlExtra: urlExtra, extraParams: extraParams });

							showAjaxPanel(baseUrl + '/' + mod + path + urlExtra + extraParams, endPoint.title, endPoint.icon, res, endPoint);

							/*if(path == "dda" && extraParams != "" ){
								urlCtrl.afterLoad = function() {
									
									extraParams = extraParams.substring(1);
									extraParamsT = extraParams.split(".");
									typeProp= (extraParamsT[0]=="proposals") ? "proposal": extraParamsT[0];
									
									//uiCoop.prepPreview(extraParamsT[0],extraParamsT[1],null,extraParamsT[2],extraParamsT[3],null);
								}
							}else */
							urlCtrl.checkUrlPreview(extraParams);
						}
						urlCtrl.firstLoad = false;

						if (endPoint.menu)
							$("." + endPoint.menu).removeClass("hide");

						if (endPoint.removeAfterLoad) {
							history.pushState('', document.title, window.location.pathname);
						}
					}
					res = true;
					return false;
				} else {
					mylog.warn("PRIVATE SECTION LOGIN FIRST", hash);
					Login.openLogin();
					resetUnlogguedTopBar();
					res = true;
				}
			} /*else 
				alert("hash not found");*/
		});
		return res;
	},
	checkUrlPreview: function (extraParams) {
		if (extraParams != "" && extraParams.indexOf("preview") >= 0) {
			//if(extraParams.indexOf("preview") > 0){									
			urlCtrl.afterLoad = function () {
				extraParamsT = extraParams.split("&")[0].substring(1).replace("preview=", "").split(".");
				if ($.inArray(extraParamsT[0], ["proposals", "proposal", "actions", "resolutions"]) >= 0) {
					ddaT = (extraParamsT[0] == "proposals") ? "proposal" : extraParamsT[0];
					uiCoop.getCoopDataPreview(ddaT, extraParamsT[1]);
				} else if (extraParamsT[0] == "comments") {
					path = (typeof extraParamsT[3] != "undefined") ? extraParamsT[3] : null;
					title = (typeof extraParamsT[4] != "undefined") ? extraParamsT[4] : null;
					commentObj.openPreview(extraParamsT[1], extraParamsT[2], path, title);
				} else {
					urlCtrl.openPreview("#page.type." + extraParamsT[0] + ".id." + extraParamsT[1]);
				}
			}
		}
	},
	openUrlInDialog: function (url, dataPost = null, moreParams = {}) {
		dialogContent.open({
			modalContentClass: "modal-custom-lg",
			isRemoteContent: true,
			backdropClick: true,
			shownCallback: () => {
				$("#dialogContent .modal-content .modal-body#dialogContentBody").next().hide()
				if (moreParams && typeof moreParams.shownCallback == "function")
					moreParams.shownCallback()
			},
			hiddenCallback: () => {
				$("#dialogContent .modal-content .modal-body#dialogContentBody").next().show()
				$("#dialogContent #dialogContentBody").removeClass("col-xs-12").empty()
				$("#dialogContent #currentModalHeader, #dialogContent #currentModalFooter").remove();
				if (moreParams && typeof moreParams.hiddenCallback == "function")
					moreParams.hiddenCallback()
			}
		});
		ajaxPost(null, url, dataPost, function (data) {
			const _htmlStr = `
				<div class="modal-body col-xs-12 bs-xs-0">
					${data}
				</div>
			`
			$("#dialogContent #dialogContentBody").addClass("col-xs-12").empty().html(_htmlStr)
			$("#dialogContent > .modal-dialog:first-child > .modal-content #dialogContentBody").before(`
				<div class="modal-header set-sticky" id="currentModalHeader">
					<h3 class="modal-title text-center">${(moreParams.title ? moreParams.title : "Titre du modal")}</h3>
				</div>
			`)
			$("#dialogContent > .modal-dialog:first-child > .modal-content").append(`
				<div class="custom-modal-footer col-xs-12" id="currentModalFooter">
					<button type="button" class="btn btn-default close-current-modal ${url.indexOf("survey/answer") > -1 || url.indexOf("#coformAap") > -1 ? "hide" : ""}">${moreParams.btnLabel}</button>
				</div>
			`)
			if (url.indexOf("survey/answer") > -1 || url.indexOf("#coformAap") > -1) {
				var btnSendInterval = setInterval(() => {
					if ($("#dialogContent .coformstep.standalonestep").length > 0) {
						clearInterval(btnSendInterval)
						$("#dialogContent .custom-modal-footer .close-current-modal").removeClass("hide")
					}
				}, 1000);
			}
			$("#dialogContent .close-current-modal").off("click").on("click", function () {
				$("#dialogContent .close-modal[data-dismiss=modal]").trigger("click")
				if (moreParams.btnRedirectionUrl && moreParams.btnRedirectionUrl.indexOf("#") == 0 && moreParams.btnRedirectionUrl != location.hash) {
					urlCtrl.loadByHash(moreParams.btnRedirectionUrl)
				}
			})
			const stickyElem = $('.modal-header.set-sticky');
			const modalCont = $('#dialogContent .modal-content');

			function checkStickyPos() {
				const scrollTop = modalCont.scrollTop() + window.scrollY;
				const removedHeight = stickyElem.height ? stickyElem.height() : 0
				const modalOffsetTop = modalCont.offset().top ? modalCont.offset().top - removedHeight : 0;
				const modalHeight = modalCont.outerHeight();
				var stickyMenu = $('#dialogContent .coform-nav .schu-sticky, #dialogContent .coform-nav .schu-btn-sticky');

				if (scrollTop >= modalOffsetTop) {
					stickyElem.css({
						position: 'fixed',
						width: $('#dialogContent .modal-content').width() - 20
					});
					if (scrollTop >= modalOffsetTop + removedHeight) {
						stickyMenu.css({
							position: 'fixed',
							"z-index": "100",
							top: stickyElem.offset().top - window.scrollY + stickyElem.outerHeight() + 10 + "px"
						});
						if ($("#dialogContent .coform-nav .schu-btn-sticky .btn").is(":visible")) {
							stickyMenu.css({
								left: "3%",
								width: "90vw"
							})
							$("#dialogContent .coform-nav .schu-sticky").css({
								top: stickyElem.offset().top - window.scrollY + stickyElem.outerHeight() + $("#dialogContent .coform-nav .schu-btn-sticky").outerHeight() + 10 + "px"
							})
						}
					}
				} else {
					stickyElem.css({
						position: 'relative',
						width: '100%'
					});
					if (scrollTop < modalOffsetTop + removedHeight) {
						stickyMenu.css({
							position: 'relative',
							top: "initial",
							left: "inherit"
						});
						if ($("#dialogContent .coform-nav .schu-btn-sticky .btn").is(":visible")) {
							stickyMenu.css({
								left: "inherit",
								width: "100%"
							})
						}
					}
				}
				if (typeof syncSection != "undefined" && stickyMenu.length > 0) {
					syncSection()
				}
			}
			typeof checkStickyPosition != "undefined" ? modalCont[0].removeEventListener('scroll', checkStickyPosition) : "";
			modalCont[0].addEventListener('scroll', checkStickyPos);
		})
	},
	jsDialogController: function (hash, moreParams = {}) {
		hash = urlCtrl.checkAndConvert(hash);
		res = false;
		$.each(urlCtrl.loadableUrls, function (urlIndex, urlObj) {
			if (hash.indexOf(urlIndex) >= 0) {
				if (typeof urlCtrl.loadableUrls[hash] != "undefined" && hash !== urlIndex)
					return true;
				if (urlObj.goto) {
					return false;
				}
				thisEndPoint = urlCtrl.loadableUrls[urlIndex];
				if (typeof thisEndPoint.login == undefined || !thisEndPoint.login || (thisEndPoint.login && userId)) {
					//alises are renaming of urls example default.home could be #home
					if (thisEndPoint.alias) {
						thisEndPoint = urlCtrl.jsDialogController(thisEndPoint.alias);
						return false;
					}
					if (thisEndPoint.aliasParam) {
						hashT = hash.split(".");
						alias = thisEndPoint.aliasParam;
						$.each(thisEndPoint.params, function (i, v) {
							$.each(hashT, function (ui, e) {
								if (v == e) {
									paramId = hashT[ui + 1];
									alias = alias.replace("$" + v, paramId);
								}
							});
						});
						thisEndPoint = urlCtrl.jsController(alias);
						return false;
					}
					if (thisEndPoint.action && typeof thisEndPoint.action == "function") {
						thisEndPoint.action(hash);
					} else {
						//classic url management : converts urls by replacing dots to slashes and ajax retreiving and showing the content 
						extraParams = (thisEndPoint.urlExtraParam) ? "?" + thisEndPoint.urlExtraParam : "";
						urlExtra = (thisEndPoint.urlExtra) ? thisEndPoint.urlExtra : "";
						//execute actions before teh ajax request
						res = false;
						if (thisEndPoint.preaction && typeof thisEndPoint.preaction == "function")
							res = thisEndPoint.preaction(hash);
						//hash can be iliased
						if (thisEndPoint.hash) {
							hash = hash.replace(urlIndex, thisEndPoint.hash);
						}
						if (hash.indexOf("?") >= 0) {
							hashT = hash.split("?");
							mylog.log(hashT);
							hash = hashT[0];
							extraParams = "?" + hashT[1];
						}
						if (extraParams.indexOf("#") >= 0) {
							extraParams = extraParams.replace("#", "%hash%");
						}
						path = urlCtrl.convertToPath(hash);
						pathT = path.split('/');
						//open path in a modal (#openModal)
						if (pathT[0] == "modal") {
							path = path.substring(5);
							smallMenu.openAjaxHTML(baseUrl + '/' + moduleId + path);
						} else {
							mod = moduleId + '/';
							if (moduleId != activeModuleId && activeModuleId != "costum") {
								mod = '';
								//go get the path , module is given in the hash

							} else if (thisEndPoint.module) {
								mod = (thisEndPoint.module !== true) ? thisEndPoint.module + "/" : "";
							}
							// showAjaxPanel(baseUrl + '/' + mod + path + urlExtra + extraParams, thisEndPoint.title, thisEndPoint.icon, res, thisEndPoint);
							const thisLink = '/' + mod + path + urlExtra + extraParams;
							urlCtrl.openUrlInDialog(baseUrl + thisLink, null, moreParams);
						}
					}
					res = true;
					return false;
				} else {
					Login.openLogin();
					resetUnlogguedTopBar();
					res = true;
				}
			}
		});
		return res;
	},
	checkSlugUrl: function (url, callback) {
		var result = url;
		if (url.indexOf("#@") >= 0) {
			splitHref = (url.indexOf("?") >= 0) ? url.split("?") : [url];
			if (splitHref[0] > 2 || splitHref[0].indexOf("#@") >= 0) {
				hashT = (splitHref[0].indexOf("#@") >= 0) ? splitHref[0].replace("#@", "") : splitHref[0].replace("#", "");
				hashT = hashT.split(".");
				if (typeof hashT == "string")
					slug = hashT;
				else
					slug = hashT[0];
				ajaxPost(
					null,
					baseUrl + "/" + moduleId + "/slug/getinfo/key/" + slug,
					param,
					function (data) {
						if (data.result) {
							viewPage = "";
							if (hashT.length > 1) {
								hashT.shift();
								viewPage = "/" + hashT.join("/");
							}

							var key = hashT[0];
							var get = "";
							if (key.indexOf("?") > -1) {
								get = key.substr(key.indexOf("?"), key.length);
								key = key.substr(0, key.indexOf("?"), key.length);
							}
							if ($.inArray(key, themeParams["onepageKey"]) > -1) viewPage = "/view/" + key;
							callback('page/type/' + data.contextType + '/id/' + data.contextId + viewPage + get);
						} else
							callback(result);
					}
				);
			} else
				callback(result);
		}
		else
			callback(result);
	},
	getUrlSearchParams: function () {
		var searchParamsUrl = "";
		if (notEmpty(hashT[1])) {
			if (hashT[1].indexOf("preview=") >= 0) {
				urlRequestT = hashT[1].split("&");
				if (urlRequestT.length > 1) {
					delEntry = urlRequestT.splice(0, 1);
					searchParamsUrl = urlRequestT.join("&");
				}
			} else
				searchParamsUrl = hashT[1];
		}
		return searchParamsUrl;
	},
	//back sert juste a differencier un load avec le back btn
	//ne sert plus, juste a savoir d'ou vient drait l'appel
	loadByHash: function (hash, mod, callBack) {
		mylog.log("urlCtrl.loadByHash", hash, mod);
		// Permet de remettre à 0 le scroll event des app de recherche lors d'un changement de vue
		//coInterface.initHtmlPosition();
		if (typeof allMaps != "undefined")
			allMaps.clear();
		if (typeof window.initedGraph != "undefined") {
			window.initedGraph = [];
		}
		$(".portfolio-modal").hide(200);
		onchangeClick = false;
		navInSlug = false;
		if (typeof pageProfil != "undefined")
			pageProfil.affixPageMenu = 0;
		if (typeof costum != "undefined" && notNull(costum) && typeof costum.loadByHash == "function") {
			mylog.log("urlCtrl.loadByHash costum if");
			hash = costum.loadByHash(hash);
		}
		mylog.warn("urlCtrl.loadByHash render", hash, mod);
		if (typeof globalTheme != "undefined" && globalTheme == "network") {
			mylog.log("globalTheme", globalTheme);
			if ( /*hash.indexOf("#network") < 0 &&
				location.hash.indexOf("#network") >= 0 &&*/ hash != "#" && hash != "") {
				mylog.log("urlCtrl.loadByHash network");
				//}
				//else{
				mylog.log("urlCtrl.loadByHash network2");
				count = $(".breadcrumAnchor").length;
				//case on reload view
				if (count == 0)
					count = 1;
				breadcrumGuide(count, hash);
			}
			return;
		}

		currentUrl = hash;
		allReadyLoad = true;
		CoAllReadyLoad = true;
		redirectHash = false;
		if (typeof urlCtrl.loadableUrls[hash] == "undefined" ||
			typeof urlCtrl.loadableUrls[hash].emptyContextData == "undefined" ||
			urlCtrl.loadableUrls[hash].emptyContextData == true) {
			//probably used for the add button, if Contextdata null then use userId 
			//mylog.log("emptyContextData",urlCtrl.loadableUrls[hash],urlCtrl.loadableUrls[hash].emptyContextData,)
			//contextData = null;
		}

		$(".my-main-container").off()
			.bind("scroll", function () { shadowOnHeader() })
			.scrollTop(0);

		//$(".searchIcon").removeClass("fa-file-text-o").addClass("fa-search");
		//searchPage = false;
		if (hash === "#" || hash === "") {
			keyConnect = (userId != "") ? "logged" : "unlogged";
			if (exists(urlCtrl.loadableUrls["#app.index"])
				&& exists(urlCtrl.loadableUrls["#app.index"]["redirect"])
				&& exists(urlCtrl.loadableUrls["#app.index"]["redirect"][keyConnect])) {
				hash = "#" + urlCtrl.loadableUrls["#app.index"]["redirect"][keyConnect];
				redirectHash = true;
				location.hash = "";
			}
		}
		mylog.warn("urlCtrl.loadByHash", hash, mod);
		if (notNull(mod)) {
			showAjaxPanel(baseUrl + '/' + hash.replace("#", "").replace(/\./g, "/"));
		}
		else if (urlCtrl.jsController(hash)) {
			mylog.log("urlCtrl.loadByHash >>> hash found", hash);
		}
		else if (hash.indexOf("#settings") >= 0 && hash.indexOf("redirect") < 0) {
			if (userId == "")
				$('#modalLogin').modal("show");
			else {
				loadSettings(hash);
				setTimeout(function () { $(".progressTop").val(100) }, 10);
				$(".progressTop").fadeOut(200);
			}

		} else if (hash.indexOf("#gallery.index.id") >= 0) {
			hashT = hash.split(".");
			showAjaxPanel(baseUrl + '/' + moduleId + '/' + hash.replace("#", "").replace(/\./g, "/"), 'ACTIONS in this ' + typesLabels[hashT[3]], 'rss');
		}

		else if (hash.indexOf("#city.directory") >= 0) {
			hashT = hash.split(".");
			showAjaxPanel(baseUrl + '/' + moduleId + '/' + hash.replace("#", "").replace(/\./g, "/"), 'KESS KISS PASS in this ' + typesLabels[hashT[3]], 'rss');
		}

		else if (hash.length > 2 || hash.indexOf("#@") >= 0) {
			splitHref = (hash.indexOf("?") >= 0) ? hash.split("?") : [hash];
			if (typeof splitHref[1] != "undefined") urlCtrl.checkUrlPreview("?" + splitHref[1]);
			if (splitHref[0] > 2 || splitHref[0].indexOf("#@") >= 0) {
				hashT = (splitHref[0].indexOf("#@") >= 0) ? splitHref[0].replace("#@", "") : splitHref[0].replace("#", "");
				hashT = hashT.split(".");
				if (typeof hashT == "string")
					slug = hashT;
				else
					slug = hashT[0];
				ajaxPost(
					null,
					baseUrl + "/" + moduleId + "/slug/getinfo/key/" + slug,
					null,
					function (data) {
						if (data.result) {
							viewPage = "";
							if (hashT.length > 1) {
								hashT.shift();
								viewPage = "/" + hashT.join("/");
							}

							var key = hashT[0];
							var get = "";

							if (key.indexOf("?") > -1) {
								get = key.substr(key.indexOf("?"), key.length);
								key = key.substr(0, key.indexOf("?"), key.length);
							}

							if ($.inArray(key, themeParams["costum"]) > -1)
								window.location.href = baseUrl + '/costum/co/index/id/' + data.contextId + '/type/' + data.contextType;
							else if ($.inArray(key, themeParams["onepageKey"]) > -1)
								viewPage = "/view/" + key;

							if ($.inArray(key, themeParams["costum"]) == -1) {
								coInterface.menu.set("#page");
								showAjaxPanel(baseUrl + '/' + moduleId + '/app/page/type/' + data.contextType + '/id/' + data.contextId + viewPage + get);
							}

						} else {
							//alert("index redir pour un element pas trouvé");
							//alert( baseUrl+'/'+ moduleId + '/app/index 1' );
							coInterface.menu.set("");
							showAjaxPanel(baseUrl + '/' + moduleId + '/app/index', 'Home', 'home');
						}
					}
				);
			} else {
				//alert("index redir pour une url ayant #@sans rien derriere");
				coInterface.menu.set("");
				//alert( baseUrl+'/'+ moduleId + '/app/index 2' );
				showAjaxPanel(baseUrl + '/' + moduleId + '/app/index', 'Home', 'home');
			}
		}
		else if (moduleId != activeModuleId && activeModuleId != "costum") {
			//alert( ctrlId +"/"+ actionId );
			showAjaxPanel(baseUrl + '/' + activeModuleId + '/' + ctrlId + "/" + actionId, 'Home', 'home');
		}
		else {
			//alert( baseUrl+'/'+ moduleId + '/app/index 3' );

			coInterface.menu.set();
			showAjaxPanel(baseUrl + '/' + moduleId + '/app/index', 'Home', 'home');
		}
		mylog.log("urlCtrl.loadByHash END hash:", hash);
		if (!redirectHash)
			location.hash = hash;

		if (location.hash.indexOf("#panel") >= 0) {
			panelName = location.hash.substr(7);
			mylog.log("urlCtrl.loadByHash panelName", panelName);
			if (userId == "") {
				if (panelName == "box-login")
					Login.openLogin();
			} else if (panelName == "box-register") {
				$('#modalRegister').modal("show");
			} else if (panelName == "modal-account") {
				if ($(".open-modal-dashboard").length > 0 && $("#modal-dashboard-account").html() == "") {
					view = $(".open-modal-dashboard").data("view");
					ajaxPost('#modal-dashboard-account', baseUrl + '/' + moduleId + '/default/render?url=co2.views.person.dashboard',
						{ view: view },
						function () {
							$(".open-modal-dashboard").trigger("click");
						});
				}
			}
		}

		if (notNull(costum) && notEmpty(costum.app)) {
			const hashIndex = location.hash.lastIndexOf("#");
			const hashValue = location.hash.substring(hashIndex + 1);
			if (location.hash.includes(hashValue)) {
				htmlExists(`#mainNav #menuTopLeft .lbh-menu-app[href*='${hashValue}']`, function (sel) {
					$(`#mainNav #menuTopLeft .lbh-menu-app,#mainNav #menuTopLeft .lbh-dropdown`).removeClass("active");
					if (notEmpty(hashValue)) {
						$(sel).addClass("active");
						if ($(sel).parent().hasClass("drop-down-sub-menu-content")) {
							$(sel).parent().parent().parent().find('.lbh-dropdown.cosDyn-buttonList').addClass('active');
						}
					}
				})
			}

			// Changer le titre de la page
			if (typeof costum.editMode != "undefined" && costum.editMode === false) {
				const title = (typeof costum.title != "undefined") ? costum.title : costum.contextSlug;
				if (notEmpty(subsectionPageName)) {
					document.title = `${subsectionPageName} | ${title}`;
					subsectionPageName = "";
				} else {
					const hashMatch = location.hash.replace("#", "").match(/^[\w\d\-_]{1,}/);
					const pageName = hashMatch ? hashMatch[0] : "welcome";
					const costumLangActive = costum.langCostumActive;
					const costumPage = (typeof costum.app[`#${pageName}`] != "undefined" && typeof costum.app[`#${pageName}`]["name"] != "undefined") ? costum.app[`#${pageName}`]["name"] : [];
					if (notEmpty(costumPage)) {
						const allNamePage = costumPage;
						let namePageActive = "";
						if (typeof allNamePage === "object" && notEmpty(allNamePage)) {
							namePageActive = allNamePage[costumLangActive] || allNamePage[Object.keys(allNamePage)[0]];
						}
						document.title = namePageActive ? `${namePageActive} | ${title}` : title;
					} else if (pageName == "welcome") {
						document.title = title;
					} else {
						document.title = `${pageName} | ${title}`;
					}
				}
			}
		}

		if (typeof callBack == "function")
			callBack();

		if (typeof activitypubStarter != "undefined" && activitypubStarter)
			activitypubStarter.actions.open();
	},
	/*loadByAnchor : function(){
		var getSections = function ($links) {
		  return $(
			$links
			  .map((i, el) => $(el).attr('href'))
			  .toArray()
			  .filter(href => href.length > 1 && href.indexOf('@') == -1 && href.indexOf('.') == -1 && href.charAt(0) === '#')
			  .join(','),
		  );
		}
		var activateLink = function ($sections, $links) {
		  var yPosition = $window.scrollTop();
		  for (let i = $sections.length - 1; i >= 0; i -= 1) {
			var $section = $sections.eq(i);

			if (yPosition >= $section.offset().top - 10 - $("#mainNav").height() ) {
			  return $links
				.removeClass('active')
				.filter(`[href="#${$section.attr('id')}"]`)
				.addClass('active');
			}
		  }
		}
		var onScrollHandler = function () {
		  activateLink($sections, $links);
		}
		var onClickHandler = function (e) {
			var href = (typeof $(e.target).parent().attr("href") != "undefined") ? $(e.target).parent().attr("href") : $(e.target).attr('href');
			if($.inArray(href, $anchorHashLinks) !== -1 ){
				if(location.hash != ''){
					urlCtrl.loadByHash("#",null,function(){
						setTimeout(function(){
							urlCtrl.loadByAnchor();
							window.location.hash="";
							if($(href).length){
								$root.animate(
									{ scrollTop: $(href).offset().top - $("#mainNav").height() },
									800,
									() => {},
								);
							};
						},2000)
					})
				}else{
					e.preventDefault();
					urlCtrl.closeXsMenu();
					if($(href).length){
						$root.animate(
							{ scrollTop: $(href).offset().top - $("#mainNav").height() },
							800,
							() => {
								if(window.location.hash)
									window.location.hash="";
							},
						);
					}
					return false;					
				}
			}
		}
		// Variables
		var $window = $(window);
		var $links = $('#menuTopLeft a,#menuTopRight a,#menuLeft a');
		var $sections = getSections($links);

		var $root = $('html, body');
		var $possibleHashLinks = [],$anchorHashLinks = [],$notAnchorHashLinks = [];
		if(notNull(costum) && exists(costum.app))
			$.each(costum.app,function(k,v){
				if(typeof v.lbhAnchor != "undefined" && v.lbhAnchor == true){
					$possibleHashLinks.push('a[href="'+k+'"]');
					$anchorHashLinks.push(k);
				}else
					$notAnchorHashLinks.push(k);
			})
		var $hashLinks = $($possibleHashLinks.join(','));
			  console.log("$sections $anchorHashLinks",$anchorHashLinks);
		console.log("$sections $notAnchorHashLinks",$notAnchorHashLinks);
		// Events
		//setTimeout(function(){
			window.addEventListener("scroll", onScrollHandler);
		//},800)

		$hashLinks.off().on('click', onClickHandler);
		// Body
		activateLink($sections, $links);
	},*/
	// Manage history est l'ancienne fonction du moteur de recherche
	// Elle est principalement utilisée aujourd'hui pour la fermeture des preview (annonces, poi, element, etc)
	// Elle permet de reconstruire l'url de manière à ne pas affecter relancer le location.hash
	manageHistory: function (setExtraP = true, setSearchP = true) {
		onchangeClick = false;
		hashT = location.hash.split("?");
		setUrl = hashT[0].substring(0);
		extraParamsArray = ["preview"];

		extraUrl = "";
		if (setExtraP && notEmpty(hashT[1])) {
			checkTableGet = hashT[1].split("&");
			$.each(checkTableGet, function (e, v) {
				nameLabel = v.split("=")[0];
				if ($.inArray(nameLabel, extraParamsArray) >= 0)
					extraUrl += (notEmpty(extraUrl)) ? "&" + v : v;
			});
			setUrl += (notEmpty(extraUrl)) ? "?" + extraUrl : "";
		}
		if (setSearchP) {
			searchParamsUrl = urlCtrl.getUrlSearchParams();
			if (notEmpty(searchParamsUrl)) {
				setUrl += (notEmpty(extraUrl)) ? "&" : "?";
				setUrl += searchParamsUrl;
			}
		}
		if (historyReplace)
			history.replaceState({}, null, setUrl);
		else if (notEmpty(setUrl) || setUrl == "#") {
			location.hash = setUrl;
		}
		historyReplace = false;

		//enlever les paramètres "h" et "hp" qui sont liée à la hash
		var url = new URL(location.href)
		if (url.searchParams.get("h") || url.searchParams.get("hp")) {
			url.searchParams.delete("h")
			url.searchParams.delete("hp")
			history.replaceState({}, null, url.href);
		}
	},
	bindCoNav: function () {
		mylog.log("urlCtrl.bindCoNav");
		document.onmouseover = function () {
			//User's mouse is inside the page.
			window.innerDocClick = true;
		}

		document.onmouseleave = function () {
			//User's mouse has left the page.
			window.innerDocClick = false;
		}
		window.onhashchange = function () {
			mylog.log("urlCtrl.bindCoNav window.onhashchang");
			if (!window.innerDocClick) {
				//Browser back button was clicked
				/*searchObject.reset();
				searchObject.text="",
				searchObject.tags=[];*/
				onchangeClick = true;
				//mylog.log("urlCtrl.bindCoNav search", searchObject);
			}
			mylog.warn("popstate history.state", history.state);
			if (lastWindowUrl && "onhashchange" in window) {

				if (allReadyLoadWindow == false && onchangeClick) {
					lastSplit = lastWindowUrl.split(".");
					currentSplit = location.hash.split(".");
					if (navInSlug || lastWindowUrl.indexOf("#page") >= 0 && location.hash.indexOf("#page") >= 0) {
						if (navInSlug) {
							if (lastSplit[0] == currentSplit[0] && typeof pageProfil != "undefined") {
								if (location.hash.indexOf("view") >= 0) {
									pageProfil.params.view = currentSplit[2];
									pageProfil.params.dir = "";
									if (typeof currentSplit[4] != "undefined") {
										if (currentSplit[3] == "dir")
											pageProfil.params.dir = currentSplit[4];
										else if (currentSplit[3] == "subview")
											pageProfil.params.subview = currentSplit[4];
									}

									if (currentSplit[2] != "coop")
										pageProfil.initView();
								} else {
									pageProfil.params.view = "";
									pageProfil.initView();
								}

							} else
								urlCtrl.loadByHash(location.hash);
						} else {
							if (lastSplit[2] == currentSplit[2] && lastSplit[4] == currentSplit[4] && typeof pageProfil != "undefined") {
								if (location.hash.indexOf("view") >= 0) {
									pageProfil.params.view = currentSplit[6];
									pageProfil.params.dir = "";
									if (typeof currentSplit[8] != "undefined") {
										if (currentSplit[7] == "dir")
											pageProfil.params.dir = currentSplit[8];
										else if (currentSplit[7] == "subview")
											pageProfil.params.subview = currentSplit[8];
									}
									if (currentSplit[6] != "coop")
										pageProfil.initView();
								}
								else {
									pageProfil.params.view = "";
									pageProfil.initView();
								}
							} else
								urlCtrl.loadByHash(location.hash);
						}
					} else if (lastWindowUrl.indexOf("#admin") >= 0 && location.hash.indexOf("#admin") >= 0) {
						if (lastSplit[0] == currentSplit[0]) {
							adminPanel.params.view = "";
							if (location.hash.indexOf("view") >= 0) {
								adminPanel.params.view = currentSplit[2];
								if (location.hash.indexOf("subview") >= 0) {
									adminPanel.params.view = currentSplit[4];
									adminPanel.params.subView = true;
								}
								//getAdminSubview(currentSplit[2]);
							}
							adminPanel.initView();
							//else
							//getAdminSubview("");


						} else
							urlCtrl.loadByHash(location.hash, true);
					} else if (lastWindowUrl.indexOf("#docs") >= 0 && location.hash.indexOf("#docs") >= 0) {
						if (lastSplit[0] == currentSplit[0]) {
							page = (location.hash.indexOf("page") >= 0) ? currentSplit[2] : "welcome";
							dir = (location.hash.indexOf("dir") >= 0) ? currentSplit[4] : mainLanguage;
							navInDocs(page, dir);
						} else
							urlCtrl.loadByHash(location.hash);
					} else {
						urlCtrl.loadByHash(location.hash);
					}
				}
				allReadyLoadWindow = false;
				onchangeClick = true;
			}
			urlBackHistory = lastWindowUrl;
			lastWindowUrl = location.hash;
		}
	},
	closeXsMenu: function () {
		$(".open-xs-menu").each(function () {
			if ($(this).hasClass("close-menu")) {
				$(this).removeClass("close-menu");
				$(this).find("i").removeClass("fa-times").addClass($(this).data("icon"));
				if ($(this).find(".labelMenu").length)
					$(this).find(".labelMenu").text($(this).data("label"));
				$(".menu-xs-container." + $(this).data("target")).hide(400);
			}
		});
	},
	previewCurrentlyLoading: false,
	openPreview: function (url, data ,withLoader=true) {
		$("#openModal").modal("hide");
		urlCtrl.previewCurrentlyLoading = true;
		$(".searchEntity").removeClass("active");
		if (withLoader){
			$("#modal-preview-coop").removeClass("hidden").show();
			coInterface.showLoader("#modal-preview-coop");
		}
		urlCtrl.checkSlugUrl(url, function (res) {
			urlPreview = baseUrl + '/' + moduleId + "/";
			urlPreview += (res.indexOf("#") == 0) ? "app/" + urlCtrl.convertToPath(res) : "app/" + res;
			//urlCtopenPreviewElement( baseUrl+'/'+moduleId+"/"+url);	
			$("#modal-preview-coop").show(200);
			if (!notNull(data))
				data = { "preview": true };
			else if (typeof data.preview == "undefined")
				data.preview = true;
			ajaxPost(
				"#modal-preview-coop",
				urlPreview,
				data,
				function (data) {
					urlCtrl.bindModalPreview();
					urlCtrl.resizePreview();
					coInterface.bindEvents();
				}
			);
		});
	},
	openAjax: function (url, data) {
		$("#openModal").modal("hide");
		urlCtrl.previewCurrentlyLoading = true;
		$(".searchEntity").removeClass("active");
		$("#modal-preview-coop").removeClass("hidden").show();
		coInterface.showLoader("#modal-preview-coop");
		$("#modal-preview-coop").show(200);
		if (!notNull(data))
			data = { "preview": true };
		else if (typeof data.preview == "undefined")
			data.preview = true;
		ajaxPost(
			"#modal-preview-coop",
			url,
			data,
			function (data) {
				urlCtrl.bindModalPreview();
				urlCtrl.resizePreview();
				coInterface.bindEvents();
			}
		);
	},
	resizePreview: function () {
		heightModal = $("#modal-preview-coop").outerHeight();
		heightToolsMenu = $("#modal-preview-coop .toolsMenu").outerHeight();
		if (heightModal != 0)
			$("#modal-preview-coop .container-preview").css({ "height": (heightModal - heightToolsMenu) + "px" });
		else
			setTimeout(function () { urlCtrl.resizePreview(); }, 200);
	},
	bindModalPreview: function () {
		urlCtrl.previewCurrentlyLoading = false;
		$(".btn-close-preview, .deleteThisBtn").click(function (e) {
			e.stopPropagation();
			urlCtrl.closePreview();
		});

		$(".main-container").off().on("click", function () {
			if ($("#modal-preview-coop").css("display") == "block") {
				urlCtrl.closePreview();
			}
		});

		$(".btn-share-link-preview").off("click").click(function (e) {
			e.stopPropagation();

			urlCtrl.shareCurrentUrl()
		})
	},
	closePreview: function () {
		if (!urlCtrl.previewCurrentlyLoading) {
			$(".main-container").off();
			$(".searchEntity").removeClass("active");
			$("#modal-preview-coop").css("display", "none");
			$("#modal-preview-coop").html("");
			if ($(".dashboard-account").is(":visible")) $(".dashboard-account").show();
			urlCtrl.manageHistory(false, true);
		}
	},
	modalExplain: function (options) {
		$("#modalExplainContainer").remove();
		var typeModal = (notNull(options.type)) ? options.type : "md";
		var textAlign = (notNull(options.align)) ? options.align : "center";
		$('body').append('<div id="modalExplainContainer"></div>');
		var modalConten = '<div class="explain-modal modal fade text-center" id="modalExplain" tabindex="-1" role="dialog"  aria-hidden="true">' +
			'<div class="modal-dialog modal-' + typeModal + '" role="document">' +
			'<div class="modal-content">' +
			'<div class="modal-body">' +
			'<div class="circle-effect">';
		if (typeof options.icon != "undefined")
			modalConten += '<i class="fa fa-' + options.icon + '"></i>';
		else if (typeof options.logo != "undefined")
			modalConten += '<img src="' + options.logo + '" class="img-responsive">';
		modalConten += '</div>';
		if (typeof options.name != "undefined")
			modalConten += '<h2 class="name">' + options.name + '</h2>';
		if (typeof options.title != "undefined")
			modalConten += '<h3 class="text-secondary">' + options.title + '</h3>';
		if (typeof options.html != "undefined")
			modalConten += options.html;
		else if (typeof options.text != "undefined")
			modalConten += '<p class="text-muted">' + options.text + '</p>';
		if (notNull(options.link)) {
			mylog.log("options link", typeof options.link);
			if (typeof options.link == "object") {
				var linksNumber = Object.keys(options.link).length;
				modalConten += '<div class="col-xs-12 padding-10">';
				$.each(options.link, function (k, v) {
					mylog.log("donationpledge", k, v);
					var label = (typeof options.link[k].label != "undefined") ? options.link[k].label : "";
					var icon = (typeof options.link[k].icon != "undefined") ? options.link[k].icon : "";
					// Allow to add params in openForm depending on the link
					if (typeof options.link[k].onclick != "undefined" && options.link[k].onclick.indexOf("dyFObj.openForm") > -1) {

						if (typeof options.link[k].dataExtend == "object") {
							var openFObj = {};
							openFObj = options.link[k].onclick.substring(
								options.link[k].onclick.lastIndexOf("{"),
								options.link[k].onclick.lastIndexOf("}") + 1
							);
							var addParam = {};
							mylog.log("addParam", openFObj);
							addParam = JSON.parse(openFObj);
							mylog.log("dataExtend", options.link[k].dataExtend);
							var dataExtended = $.extend(options.link[k].dataExtend, addParam);
							mylog.log("dataExtended", dataExtended);
							stringExtended = JSON.stringify(dataExtended);
							function escapeHtml(text) {
								var map = {
									'&': '&amp;',
									'<': '&lt;',
									'>': '&gt;',
									//'"': '&quot;',
									"'": '&#039;'
								};

								return text.replace(/[&<>']/g, function (m) { return map[m]; });
							}

							stringExtended = escapeHtml(stringExtended);
							mylog.log("stringExtended", stringExtended);

						}

					}
					var extendedOnclick = (typeof options.link[k].dataExtend != "undefined" && typeof options.link[k].onclick != "undefined") ? options.link[k].onclick.replaceAll(openFObj, stringExtended) : options.link[k].onclick;
					var classLBH = (typeof options.link[k].linkClass != "undefined") ? options.link[k].linkClass : "";
					var onclick = (typeof options.link[k].onclick != "undefined") ? "onclick='" + extendedOnclick + "'" : "";
					var link = (typeof options.link[k].link != "undefined") ? options.link[k].link : "";
					modalConten += '<a href="' + options.link[k].link + '" ' + onclick + ' class="btn btn-show-more ' + classLBH + '" data-dismiss="modal"><i class="fa fa-' + options.link[k].icon + '"></i> ' + label + '</a>';
				});
				modalConten += '</div>';
				modalConten += '<a href="javascript:;" class="btn btn-show-more" data-dismiss="modal" aria-label="Close"><i class="fa fa-remove"></i>&nbsp;' + trad.Close + '</a>';
			}
			else if (typeof options.link == "string") {
				var labelLink = (typeof options.linkLabel != "undefined") ? options.linkLabel : trad.knowmore;
				var targetB = (typeof options.targetB == "undefined") ? 'target="_blank"' : "";
				var classLBH = (typeof options.linkClass != "undefined") ? options.linkClass : "";
				modalConten += '<a href="javascript:;" class="btn btn-show-more" data-dismiss="modal" aria-label="Close"><i class="fa fa-remove"></i>&nbsp;' + trad.Close + '</a>';
				modalConten += "<a href='" + options.link + "' " + targetB + " class='btn btn-show-more " + classLBH + "'>" + labelLink + "</a>";
			}
		}
		modalConten += '</div>' +
			'</div>' +
			'</div>' +
			'</div>';
		$('#modalExplainContainer').html(modalConten);
		$("#modalExplain").modal();
		coInterface.bindLBHLinks();

	},
	getUrlWithHashInSearchParams: function (keepHash = true) {
		var url = new URL(location.href)
		var hash = url.hash.replace("#", "").trim();
		if (!keepHash)
			url.hash = "";

		if (hash == "") {
			url.searchParams.delete("h")
			url.searchParams.delete("hp")
		} else {
			var hash_parts = hash.split("?"),
				page = hash_parts[0],
				params = hash_parts[1] ? hash_parts[1] : "";

			url.searchParams.set("h", page);
			if (params) {
				url.searchParams.set("hp", btoa(params))
			} else {
				url.searchParams.delete("hp")
			}
		}

		return url;
	},
	shareCurrentUrl: function () {
		var el = document.createElement("input"),
			url = urlCtrl.getUrlWithHashInSearchParams(false);

		url.searchParams.set("st", Date.now())

		el.value = url.href;
		document.body.appendChild(el);
		el.select();
		document.execCommand('copy');
		document.body.removeChild(el);

		toastr.success("Le lien est copié.");
	},
	getAacList: function(params = {}, callBack) {
		var selectedList = {
		}
		var defaultSelection = params.selectedList ? $.extend(true, {}, params.selectedList) : {};
		if (params.selectedList) {
			selectedList = $.extend(true, {}, params.selectedList);
		}
		const fetchThisData = function (url, thisPost, thisCallback) {
			ajaxPost(
				null,
				url,
				thisPost,
				(res) => {
					thisCallback(res);
				}
			)
		}
		const buildThisFinderList = function (listData, thisSelectedList) {
			var thisHtml = "";
			$.each(listData, function (aacKey, aacValue) {
				const thisAacId = aacValue.id ? aacValue.id : aacKey
				thisHtml += `
					<div class="thisfinder-item elt-${thisAacId} col-xs-12" data-value="${thisAacId}">
						<div class="checkbox-content pull-left">
							<label>
								<input type="checkbox" ${ thisSelectedList[thisAacId] && thisSelectedList[thisAacId].value == "selected" ? "checked" : "" } class="check-population-finder checkbox-info" data-type="${aacValue.type}" data-name="${aacValue.contextName}" data-value="${thisAacId}">
								<span class="cr">
									<i class="cr-icon fa fa-check"></i>
								</span>
							</label>
						</div>
						<div class="element-finder element-finder-${thisAacId}">
							<img src="${aacValue.profilThumbImageUrl ? aacValue.profilThumbImageUrl : defaultImage}" class="thumb-send-to pull-left img-circle" height="40" width="40">
							<span class="info-contact pull-left margin-left-10">
								<span class="name-element text-dark text-bold" data-id="${thisAacId}">
									${ aacValue.name }
								</span><br>
								<span class="type-element text-light pull-left">
									${ trad[aacValue.type] ? ucfirst(trad[aacValue.type]) : aacValue.type }
								</span>
							</span>
						</div>
					</div>
				`
			});
			return thisHtml;
		}
		const bindThisFinderEvents = function() {
			if (params.closeBtnEvent) {
				$(".this-bootbox-cancel").off("click").on("click", function() {
					params.closeBtnEvent();
				});
			}
			if (params.acceptBtnEvent) {
				$(".this-bootbox-accept").off("click").on("click", function() {
					params.acceptBtnEvent(selectedList);
				});
			}
			$(".thisfinder-item input.check-population-finder").off("change").on("change", function() {
				var aacKey = $(this).data("value");
				const thisCheckBtn = $(this);
				if ($(this).is(":checked")) {
					selectedList[aacKey] = {
						name: thisCheckBtn.attr("data-name"),
						type: thisCheckBtn.attr("data-type"),
						value: "selected"
					}
				} else {
					if (selectedList[aacKey] && defaultSelection[aacKey]) {
						selectedList[aacKey].value = "notselected";
					} else if (selectedList[aacKey]) {
						delete selectedList[aacKey];
					}
				}
				if (Object.keys(selectedList).length < 1) {
					$(".this-bootbox-accept").prop("disabled", true).addClass("disabled");
				} else {
					$(".this-bootbox-accept").prop("disabled", false).removeClass("disabled");
				}
			})
			$(".this-modal-body #populateThisFinder").off("keyup").on("keyup", delay(function() {
				var searchVal = $(this).val().toLowerCase();
				fetchThisData(
					params.url,
					{
						name: searchVal,
					},
					(response) => {
						if (typeof response.results != "undefined") {
							$(".thisfinder-list-selection").html(buildThisFinderList(response.results, selectedList));
							bindThisFinderEvents();
						}
					}
				)
			}, 750))
		}
		var aacListHtml = `
			<div class="modal-header">
				<h5 class="modal-title">${ params.title ? params.title : "Sélectionner dans la liste"}</h5>
			</div>
			<div class="col-xs-12 this-modal-body bs-my-4">
				<input class="form-group form-control" type="text" id="populateThisFinder" placeholder="${ trad["Search an element"] }">
				<hr>
				<div class="col-xs-12 bs-px-0 bs-pt-3 shadow2 thisfinder-list-selection">`;
		if (params.url) {
			fetchThisData(
				params.url, 
				{}, 
				function (response) {
					if (typeof response.results != "undefined") {
						if (!notEmpty(response.results)) {
							selectedList = {};
						}
						aacListHtml += buildThisFinderList(response.results, selectedList);
						aacListHtml += `
									</div>
								</div>
							<div class="modal-footer col-xs-12">
								<button type="button" class="btn btn-default this-bootbox-cancel">
									${ trad.Close }
								</button>
								<button type="button" class="btn btn-success this-bootbox-accept ${ Object.keys(selectedList).length < 1 ? "disabled" : "" }">
									${ trad.save }
								</button>
							</div>
							`;
						if (typeof callBack === "function") {
							callBack(aacListHtml);
							bindThisFinderEvents();
						}
					}
				}
			)
		}
	}
}
var smallMenu = {
	destination: ".menuSmallBlockUI",
	inBlockUI: true,
	//smallMenu.openAjax(\''+baseUrl+'/'+moduleId+'/collections/list/col/'+obj.label+'\',\''+obj.label+'\',\'fa-folder-open\',\'yellow\')
	//the url must return a list like userConnected.list
	openAjax: function (url, title, icon, color, title1, params, callback) {
		/*if( typeof directory == "undefined" )
			lazyLoad( moduleUrl+'/js/default/directory.js', null, null );
		*/
		//processingBlockUi();
		//$(smallMenu.destination).html("<i class='fa fa-spin fa-refresh fa-4x'></i>");

		ajaxPost(null, url, params, function (data) {
			if (!title1 && notNull(title1) && data.context && data.context.name)
				title1 = data.context.name;
			var content = smallMenu.buildHeader(title, icon, color, title1);
			smallMenu.open(content);
			if (data.count == 0)
				$(".titleSmallMenu").html("<a class='text-white' href='javascript:smallMenu.open();'> <i class='fa fa-th'></i></a> " +
					' <i class="fa fa-angle-right"></i> ' +
					title + " vide <i class='fa " + icon + " text-" + color + "'></i>");
			else
				directory.buildList(data.list);

			$('.searchSmallMenu').off().on("keyup", function () {
				directory.search(".favSection", $(this).val());
			});
			//else collection.buildCollectionList( "linkList" ,"#listCollections",function(){ $("#listCollections").html("<h4 class=''>Collections</h4>"); });

			if (typeof callback == "function")
				callback(data);
		});
	},
	build: function (params, build_func, callback) {
		//processingBlockUi();
		if (typeof build_func == "function")
			content = build_func(params);
		smallMenu.open(content);
		if (typeof callback == "function")
			callback();
	},
	//ex : smallMenu.openSmall("Recherche","fa-search","green",function(){
	openSmall: function (title, icon, color, callback, title1) {
		if (typeof directory == "undefined")
			lazyLoad(moduleUrl + '/js/default/directory.js', null, null);

		var content = smallMenu.buildHeader(title, icon, color, title1);
		smallMenu.open(content);

		if (typeof callback == "function")
			callback();
	},
	buildHeader: function (title, icon, color, title1) {
		title1 = (typeof title1 != "undefined" && notNull(title1)) ? title1 : "<a class='text-white' href='javascript:smallMenu.open();'> <i class='fa fa-th'></i></a> ";
		content =
			"<div class='col-xs-12 padding-5'>" +

			"<h3 class='titleSmallMenu'> " +
			title1 + "<i class='fa " + icon + " text-" + color + "'></i> " + title +
			"</h3><hr>" +
			"<div class='col-md-12 bold sectionFilters'>" +
			"<a class='text-black bg-white btn btn-link favSectionBtn btn-default' " +
			"href='javascript:directory.toggleEmptyParentSection(\".favSection\",null,\".searchEntityContainer\",1)'>" +
			"<i class='fa fa-asterisk fa-2x'></i><br>Tout voir</a></span> </span>" +
			"</div>" +

			"<div class='col-md-12'><hr></div>" +

			"</div>" +

			"<div id='listDirectory' class='col-md-10 no-padding'></div>" +
			"<div class='hidden-xs col-sm-2 text-left'>" +
			"<input name='searchSmallMenu' style='border:1px solid red' class='form-control searchSmallMenu text-black' placeholder='rechercher' style=''><br/>" +
			"<h4 class=''><i class='fa fa-angle-down'></i> Filtres</h4>" +
			"<a class='btn btn-dark-blue btn-anc-color-blue btn-xs favElBtn favAllBtn text-left' href='javascript:directory.toggleEmptyParentSection(\".favSection\",null,\".searchEntityContainer\",1)'> <i class='fa fa-tags'></i> Tout voir </a><br/>" +

			"<div id='listTags'></div>" +
			"<div id='listScopes'><h4><i class='fa fa-angle-down'></i> Où</h4></div>" +
			"<div id='listCollections'></div>" +
			"</div> " +
			"<div class='col-xs-12 col-sm-10 center no-padding'>" +
			//"<a class='pull-right btn btn-xs btn-default' href='javascript:collection.newChild(\""+title+"\");'> <i class='fa fa-sitemap'></i></a> "+
			"<a class='pull-right btn btn-xs menuSmallTools hide text-red' href='javascript:collection.crud(\"del\",\"" + title + "\");'> <i class='fa fa-times'></i></a> " +
			"<a class='pull-right btn btn-xs menuSmallTools hide'  href='javascript:collection.crud(\"update\",\"" + title + "\");'> <i class='fa fa-pencil'></i></a> " +

			// "<h3 class='titleSmallMenu'> "+
			// 	title1+' <i class="fa fa-angle-right"></i> '+title+" <i class='fa "+icon+" text-"+color+"'></i>"+
			// "</h3>"+
			// "<input name='searchSmallMenu' class='searchSmallMenu text-black' placeholder='rechercher' style='margin-bottom:8px;width: 300px;font-size: x-large;'><br/>"+

			"</div>";
		return content;
	},
	ajaxHTML: function (url, title, type, nextPrev) {
		mylog.log("smallMenu.ajaxHTML", url, title, type, nextPrev);
		var dest = (type == "blockUI") ? ".blockContent" : "#openModal .modal-content .container";
		var ctxParams = {};
		if (costum && typeof costum.contextId != "undefined")
			ctxParams.costumId = costum.contextId;
		if (costum && typeof costum.contextType != "undefined")
			ctxParams.costumType = costum.contextType;
		if (costum && typeof costum.contextSlug != "undefined")
			ctxParams.costumSlug = costum.contextSlug;

		ajaxPost(dest, url, ctxParams, function () {
			mylog.log("smallMenu.ajaxHTML getAjax", url, title, type, nextPrev);
			//next and previous btn to nav from preview to preview
			if (nextPrev) {
				var p = 0;
				var n = 0;
				var found = false;
				var l = $('.searchEntityContainer .container-img-profil').length;
				$.each($('.searchEntityContainer .container-img-profil'), function (i, val) {
					if (found) {
						n = (i == l - 1) ? $($('.searchEntityContainer .container-img-profil')[0]).attr('href') : $(this).attr('href');
						return false;
					}
					if ($(this).attr('href') == nextPrev)
						found = true;
					else
						p = (i == 0) ? $($('.searchEntityContainer .container-img-profil')[$('.searchEntityContainer .container-img-profil').length]).attr('href') : $(this).attr('href');
				});
				html = "<div style='margin-bottom:50px'><a href='" + p + "' class='lbhp text-dark'><i class='fa fa-2x fa-arrow-circle-left'></i> PREV </a> " +
					" <a href='" + n + "' class='lbhp text-dark'> NEXT <i class='fa fa-2x fa-arrow-circle-right'></i></a></div>";
				$(dest).prepend(html);

			}
			coInterface.bindLBHLinks();
		}, "html");
	},
	//openSmallMenuAjaxBuild("",baseUrl+"/"+moduleId+"/favorites/list/tpl/directory2","FAvoris")
	//opens any html without post processing
	openAjaxHTML: function (url, title, type, nextPrev) {
		mylog.log("smallMenu.openAjaxHTML", url, title, type, nextPrev);
		smallMenu.open("", type);
		smallMenu.ajaxHTML(url, title, type, nextPrev);
	},
	//content Loader can go into a block
	//smallMenu.open("Recherche","blockUI")
	//smallMenu.open("Recherche","bootbox")
	open: function (content, type, color, callback) {
		mylog.log("smallMenu.open", content, type, color, callback);
		//add somewhere in page
		if (!smallMenu.inBlockUI) {
			mylog.log("smallMenu.open if");
			$(smallMenu.destination).html(content);
			$.unblockUI();
		}
		else {
			mylog.log("smallMenu.open else");
			//this uses blockUI
			if (type == "blockUI") {
				mylog.log("smallMenu.open else if blockUI");
				colorCSS = (color == "black") ? 'rgba(0,0,0,0.70)' : 'rgba(256,256,256,0.85)';
				colorText = (color == "black") ? '#fff' : '#000';
				$.blockUI({
					//title : 'Welcome to your page', 
					message: (content) ? content : "<div class='blockContent'></div>",
					onOverlayClick: $.unblockUI(),
					css: {
						backgroundColor: colorCSS,
						color: colorText
					}
				});
			} else if (type == "bootbox") {
				mylog.log("smallMenu.open else if bootbox");
				bb = bootbox.dialog({
					message: content
				});
				if (color == "black")
					bb.css({ 'background-color': '#000', 'opacity': '0.8' });
				bb.on('shown.bs.modal', function (e) {
					if (typeof callback == "function")
						callback();
				})
			} else {//open inside a boostrap modal
				mylog.log("smallMenu.open else else", $("#openModal").hasClass('in'));
				if (!$("#openModal").hasClass('in'))
					$("#openModal").modal("show");
				if (content)
					smallMenu.content(content);
				else
					smallMenu.content("<i class='fa fa-spin fa-refresh fa-4x'></i>");
			}

			$(".blockPage").addClass(smallMenu.destination.slice(1));
			// If network, check width of menu small
			if (typeof globalTheme != "undefined" && globalTheme == "network") {
				if ($("#ficheInfoDetail").is(":visible"))
					$(smallMenu.destination).css("cssText", "width: 100% !important;left: 0% !important;");
				else
					$(smallMenu.destination).css("cssText", "width: 83.5% !important;left: 16.5% !important;");
			}
			coInterface.bindLBHLinks();
			if (typeof callback == "function")
				callback();
		}
	},
	content: function (content) {
		el = $("#openModal div.modal-content div.container");
		if (content == null)
			return el;
		else
			el.html(content);
	}
};
var dialogContent = {
	customClass: "",
	open: function (
		params = {
			modalContentClass: "",
			isRemoteContent: false,
			backdropClick: false,
			shownCallback: () => { },
			keepOpenMsg: "",
			hiddenCallback: () => { },
		}
	) {
		dialogContent.customClass = params.modalContentClass;
		$("#dialogContent .modal-dialog").addClass(params.modalContentClass)
		if (!$("#dialogContent .modal-content").is(":visible"))
			$("#dialogContent .modal-content").show()

		//open modal
		$("#dialogContent").toggle("drop", { direction: "up" }, 300).show().addClass("in", function () {
			$(this).trigger("shown.custom.modal")
		})
		if (params.isRemoteContent) {
			if (typeof coInterface.showCostumLoader != "undefined") {
				coInterface.showCostumLoader("#dialogContent #dialogContentBody")
			} else {
				coInterface.showLoader("#dialogContent #dialogContentBody", trad["Loading, please wait"])
			}
		}

		//event
		$("#dialogContent").off("shown.custom.modal").on("shown.custom.modal", function (e) {
			e.stopImmediatePropagation();
			$("body.index").removeClass("hide-overflow-y").addClass("hide-overflow-y")
			params.shownCallback()
		});
		$("#dialogContent").off("hidden.custom.modal").on("hidden.custom.modal", function (e) {
			e.stopImmediatePropagation();
			$("body.index").removeClass("hide-overflow-y");
			$('.inviteToProposalDropdown').remove();
			if (typeof aapObj != "undefined" && aapObj.context && aapObj.context.name) {
				setTitle("", "", aapObj.context.name)
			}
			params.hiddenCallback()
		});
		$(`#dialogContent [data-dismiss="modal"]`).off("click").on("click", function (e) {
			e.stopImmediatePropagation();
			dialogContent.close()
		})
		document.removeEventListener("keydown", dialogContent.closeDocEvent)
		if (params.backdropClick) {
			document.addEventListener("keydown", dialogContent.closeDocEvent)
			$('#dialogContent').off('click').on('click', function (e) {
				if (params.backdropClick && params.backdropClick == true)
					if ($(e.target).is($('.modal-dialog')) || $(e.target).is($(".modal-custom-backdrop"))) {
						dialogContent.close()
					}
			})
		} else
			$('#dialogContent').off('click')
	},
	close: function (modalSelector = "#dialogContent") {
		const closeModalEvent = function () {
			if (!$(modalSelector).is(':visible'))
				return;
			$(modalSelector).toggle("drop", { direction: "up" }, 300).removeClass("in " + params.modalContentClass).hide(function () {
				$(modalSelector).trigger("hidden.custom.modal")
				$("#dialogContent .modal-dialog").removeClass(dialogContent.customClass);
			})
		}
		if (typeof dialogContent.keepOpen != "undefined" && dialogContent.keepOpen == true) {
			$.confirm({
				title: "Confirmer action",
				content: params.keepOpenMsg ? params.keepOpenMsg : "Vous êtes sur le point de quitter la fenêtre de configuration sans enregister les modifications. Voulez vous quitter cette fenêtre?",
				buttons: {
					no: {
						text: 'Non',
						btnClass: 'btn btn-default'
					},
					yes: {
						text: 'Oui',
						btnClass: 'btn btn-danger',
						action: function () {
							dialogContent.keepOpen = false
							closeModalEvent()
						}
					}
				}
			});
		} else {
			closeModalEvent()
		}
	},
	closeDocEvent: function (event) {
		// close on esc
		if (event.keyCode == 27) {
			dialogContent.close()
		}
	}
}

if (typeof discussionComments != "undefined") {
	discussionComments.common = {
		buildCommentedQuestionList: function (formSteps, toBounceIn = true, currentActive = "") {
			var questionListHtml = `
				<li class="discuLi ${notEmpty(currentActive) && currentActive == "allComments" ? "active" : ""} allComments ${toBounceIn ? "seen animated bounceInRight enable" : ""} col-md-12 col-sm-12 col-xs-12" ${notEmpty(discussionComments.params.toBeSelectedComment) && typeof discussionComments.params.toBeSelectedAll != "undefined" && discussionComments.params.toBeSelectedAll == true ? `data-toselect-comment="${discussionComments.params.toBeSelectedComment}"` : ""} data-is-allcomments="true" data-path="">
					<a href="javascript:;" class="question col-xs-12 no-padding">
						<div class="col-md-10 col-sm-10 col-xs-10 no-padding question-text-content">
							<span class="message pull-left">
								${ trad["Global comments"] }
							</span>
						</div>
					</a>
					<div class="pull-left col-xs-12 bs-px-0 rubric-connected-users bs-py-2">
					</div>
				</li>
			`;
			if (notEmpty(currentActive) && currentActive == "allComments" && typeof discussionComments.params.allDiverNotSeenComments != "undefined" && discussionComments.params.allDiverNotSeenComments > 0) {
				$(".discussions-content .refresh-comment-content").show(300)
			} else if ($(".discussions-content .refresh-comment-content").is(":visible")) {
				$(".discussions-content .refresh-comment-content").hide(300)
			}
			$.each(formSteps, function (stepIndex, stepVal) {
				if (stepVal.inputs && discussionComments.params.answer._id) {
					$.each(stepVal.inputs, function (inputIndex, inputVal) {
						if (inputVal.activeComments || (typeof inputVal.showAnyway != "undefined" && inputVal.showAnyway == true)) {
							if (
								notEmpty(currentActive) && currentActive == inputIndex &&
								typeof discussionComments.params.orderedQuestion != "undefined" && notEmpty(discussionComments.params.orderedQuestion) &&
								discussionComments.params.orderedQuestion[stepIndex] && discussionComments.params.orderedQuestion[stepIndex].inputs &&
								discussionComments.params.orderedQuestion[stepIndex].inputs[inputIndex] &&
								discussionComments.params.orderedQuestion[stepIndex].inputs[inputIndex].notSeenComments &&
								discussionComments.params.orderedQuestion[stepIndex].inputs[inputIndex].notSeenComments > 0
							) {
								$(".discussions-content .refresh-comment-content").show(300)
							} else if (
								notEmpty(currentActive) && currentActive == inputIndex &&
								$(".discussions-content .refresh-comment-content").is(":visible")
							) {
								$(".discussions-content .refresh-comment-content").hide(300)
							}
							var discussionCategory = "";
							var thisDepId = 0;
							var editDiscuBtn = "";
							if (notEmpty(inputVal.commentPath)) {
								if (inputVal.commentPath.indexOf(".privateComment") > -1) {
									editDiscuBtn = `
										<div class="btn-action-container">
											<span class="discu-action edit-discu bs-mr-3" data-title="${inputVal.label}" data-privilege="private" data-stepid="${stepIndex}" data-discu-path="moreDiscussions.${stepVal.id}"><i class="fa fa-pencil fa-2xx"></i></span>
											<span class="discu-action delete-discu" data-title="${inputVal.label}" data-stepid="${stepIndex}" data-discu-path="moreDiscussions.${stepVal.id}"><i class="fa fa-trash fa-2xx text-danger"></i></span>
										</div>
									`
									discussionCategory = `<span class="discussion-category-icon"><i class="fa fa-lock fa-2xx text-danger"></i></span>`
								}
								if (inputVal.commentPath.indexOf(".commentPublic") > -1 && inputVal.commentPath.indexOf(".discu_") > -1) {
									if (discussionComments.params.canEditDiscussion) {
										editDiscuBtn = `
											<div class="btn-action-container">
												<span class="discu-action edit-discu bs-mr-3" data-title="${inputVal.label}" data-privilege="authorAll" data-stepid="${stepIndex}" data-discu-path="moreDiscussions.${stepVal.id}"><i class="fa fa-pencil fa-2xx"></i></span>
												<span class="discu-action delete-discu" data-title="${inputVal.label}" data-stepid="${stepIndex}" data-discu-path="moreDiscussions.${stepVal.id}"><i class="fa fa-trash fa-2xx text-danger"></i></span>
											</div>
										`
									}
									discussionCategory = `<span class="discussion-category-icon"><i class="fa fa-unlock fa-2xx text-success"></i></span>`
								} else if (inputVal.commentPath.indexOf(".comment") > -1 && inputVal.commentPath.indexOf(".discu_") > -1) {
									editDiscuBtn = `
										<div class="btn-action-container">
											<span class="discu-action edit-discu bs-mr-3" data-title="${inputVal.label}" data-privilege="authorAdmin" data-stepid="${stepIndex}" data-discu-path="moreDiscussions.${stepVal.id}"><i class="fa fa-pencil fa-2xx"></i></span>
											<span class="discu-action delete-discu" data-title="${inputVal.label}" data-stepid="${stepIndex}" data-discu-path="moreDiscussions.${stepVal.id}"><i class="fa fa-trash fa-2xx text-danger"></i></span>
										</div>
									`
									discussionCategory = `<span class="discussion-category-icon"><i class="fa fa-lock fa-2xx text-primary"></i></span>`
								}
								const splittedPath = inputVal.commentPath.split(".");
								const depIndex = splittedPath.indexOf("depenseindex")
								thisDepId = depIndex > -1 && splittedPath[depIndex + 1] ? splittedPath[depIndex + 1] : 0;
							}
							questionListHtml += `
								<li class="discuLi cursor-pointer ${typeof inputVal.notSeenComments != "undefined" && inputVal.notSeenComments > 0 ? "non-lus" : ""} ${toBounceIn ? 'animated bounceInRight enable' : ""} col-md-12 col-sm-12 col-xs-12 ${notEmpty(currentActive) && currentActive == inputIndex ? "active" : ""} " ${typeof inputVal.notSeenComments != "undefined" && inputVal.notSeenComments > 0 ? `data-notseen="${inputVal.notSeenComments}"` : ""} ${notEmpty(discussionComments.params.toBeSelectedComment) && typeof inputVal.toBeSelected != "undefined" && inputVal.toBeSelected == true ? `data-toselect-comment="${discussionComments.params.toBeSelectedComment}"` : ""} data-id="${discussionComments.params.answer && discussionComments.params.answer._id ? discussionComments.params.answer._id.$id : ""}" data-stepkey="${stepVal.id}" data-inputkey="${inputIndex}" data-depenseid="${thisDepId}" data-path="${inputVal.commentPath ? inputVal.commentPath : `${discussionComments.params.parentForm && discussionComments.params.parentForm._id ? discussionComments.params.parentForm._id.$id : ""}.${stepIndex}.${discussionComments.params.answer._id.$id}.${inputIndex}.question_${inputIndex}.comment`}">
									${discussionCategory}
									${editDiscuBtn}
									<a href="javascript:;" class="question col-xs-12 no-padding">
										<div class="col-md-10 col-sm-10 col-xs-10 no-padding question-text-content">
											<span class="message pull-left">${inputVal.label}</span>
											<span class="time pull-left"></span>
										</div>
									</a>
									<div class="pull-left col-xs-12 bs-px-0 rubric-connected-users bs-py-2">
									</div>
								</li>
							`;
						}
					})
				}
			})
			return questionListHtml
		},
		buildProposalView: function (answer, titlePath) {
			const proposalTitle = notEmpty(jsonHelper.getValueByPath(answer, titlePath)) ? jsonHelper.getValueByPath(answer, titlePath) : `(${trad["No name"]})`;
			const parentId = answer.context ? Object.keys(answer.context)[0] : null;
			var proposalDetailLink = answer._id ? "#detail-un-commun.communId." + answer._id.$id : "";
			if (discussionComments.params.parentForm && discussionComments.params.parentForm.discussionsGlobalConfig && discussionComments.params.parentForm.discussionsGlobalConfig.proposalRedirectionLink) {
				proposalDetailLink = discussionComments.params.parentForm.discussionsGlobalConfig.proposalRedirectionLink.replace("{answerId}", answer._id ? answer._id.$id : "");
			}
			return `
				<div class="proposal-container bs-my-2">
					<div class="proposal-image-container">
							<img src="${answer.profilImageUrl ? answer.profilImageUrl : defaultImage}" alt="proposalImg">
					</div>
					<div class="proposal-info-container" style="overflow: unset">
						<h2 class="proposal-name ${proposalDetailLink ? "cursor-pointer" : ""}" ${proposalDetailLink ? `data-link="${proposalDetailLink}"` : ""} >    
							<span class="no-padding title-text">${proposalTitle}</span>
						</h2>
						<span><b>${ trad["Parent"] } : </b> <a href="${(parentId && answer.context && answer.context[parentId] && answer.context[parentId].type ? `#page.type.${answer && answer.context && answer.context[parentId] && answer.context[parentId].type}.id.${parentId}` : "")}" class="parent-link lbh-preview-element answer-parent-elt">${(parentId && answer && answer.context && answer.context[parentId] && answer.context[parentId].name ? answer.context[parentId].name : "")}</a></span><br>
						<span><b>${ trad["Deposited by"] } : </b>
							<a href="${(answer.user ? `#page.type.citoyens.id.${answer.user}` : "javascript:;")}" class="lbh-preview-element answer-parent-elt">
								<span class="depositor">
									<img src="${(discussionComments.params.userData && discussionComments.params.userData.profilImageUrl ? discussionComments.params.userData.profilImageUrl : defaultImage)}" alt="citoyensProfilImg" class="depositor-image bs-mr-2">
									${(discussionComments.params.userData && discussionComments.params.userData.name ? discussionComments.params.userData.name : "")}
								</span>
							</a>
							</span>
							<a href="${(answer.user ? `#page.type.citoyens.id.${answer.user}` : "javascript:;")}" class="lbh-preview-element answer-parent-elt"><br>
							</a>
						<span><b>${ucfirst(trad['on '])}  : </b> ${new Date(moment.unix(answer.created)).toLocaleString('fr', {
				day: 'numeric', month: 'numeric', year: 'numeric'
			})}</span><br>
					</div>
				</div>
			`
		},
		getCurrentComments: function (type, parentFormId, path, callBack = () => { }) {
			const thisUrl = baseUrl + `/co2/comment/index/type/${type}/id/${parentFormId}/path/${path}`;
			ajaxPost(null, thisUrl, null, function (response) {
				callBack(response);
				if (typeof discussionComments.params.toBeSelectedComment != "undefined" && !notEmpty(discussionComments.params.toBeSelectedComment)) {
					discussionComments.common.highlightAllNewComments(path).then(() => {
						$(".alreadyclicked").removeClass("alreadyclicked");
						if (notEmpty(path)) {
							const splittedPath = path.split(".");
							const depenseindex = splittedPath.indexOf("depenseindex");
							const inputIdSuffix = path.indexOf("comment.financement.") > -1 ? ("_financement" + `${depenseindex > -1 && splittedPath[depenseindex + 1] ? "_" + splittedPath[depenseindex + 1] : ""}`) : "";
							const inputId = splittedPath[3] ? splittedPath[3] + inputIdSuffix : null;
							$.each(discussionComments.params.orderedQuestion, function (stepKey, stepVal) {
								if (stepVal.inputs[inputId] && stepVal.inputs[inputId].notSeenComments > 0 && discussionComments.params.toHighlightComments && typeof discussionComments.params.toHighlightComments[inputId] != "undefined") {
									discussionComments.params.orderedQuestion[stepKey].inputs[inputId].notSeenComments = discussionComments.params.toHighlightComments[inputId].length;
								}
							})
						}
					})
				} else {
					discussionComments.common.highlightAllNewComments(path).then(() => {
						if (typeof discussionComments.params.toBeSelectedComment != "undefined" && notEmpty(discussionComments.params.toBeSelectedComment)) {
							discussionComments.common.highlightComment(discussionComments.params.toBeSelectedComment)
							if ($('.item-comment#item-comment-' + discussionComments.params.toBeSelectedComment).is(":visible")) {
								discussionComments.params.toBeSelectedComment = null;
								$(".alreadyclicked").removeClass("alreadyclicked");
								if (notEmpty(path)) {
									const splittedPath = path.split(".");
									const depenseindex = splittedPath.indexOf("depenseindex");
									const inputIdSuffix = path.indexOf("comment.financement.") > -1 ? ("_financement" + `${depenseindex > -1 && splittedPath[depenseindex + 1] ? "_" + splittedPath[depenseindex + 1] : ""}`) : "";
									const inputId = splittedPath[3] ? splittedPath[3] + inputIdSuffix : null;
									$.each(discussionComments.params.orderedQuestion, function (stepKey, stepVal) {
										if (stepVal.inputs[inputId] && stepVal.inputs[inputId].notSeenComments > 0 && discussionComments.params.toHighlightComments && typeof discussionComments.params.toHighlightComments[inputId] != "undefined") {
											discussionComments.params.orderedQuestion[stepKey].inputs[inputId].notSeenComments = discussionComments.params.toHighlightComments[inputId].length;
										}
									})
								}
							}
						}
					})
				}
				discussionComments.common.startIntervalIfNeeded(".date-com .dateUpdated", { createdTimeAttr: "created" })
			}, null, null)
		},
		getCurrentDepenseCard: function (answerId, depenseId, callBack) {
			const post = {
				answerId: answerId,
				depenseId: depenseId
			};
			const url = baseUrl + '/co2/aap/commonaap/action/getDepenseCard';
			ajaxPost(null, url, post, function (res) {
				callBack(res)
			}, function (arg0, arg1, arg2) {
			});
		},
		highlightComment: function (commentId, toBeSelectedComment = discussionComments.params.toBeSelectedComment, scrollOnLast = true, dotOnly = false) {
			if (!notEmpty(toBeSelectedComment)) {
				return
			}
			const commentItem = $(".item-comment#item-comment-" + commentId)
			if (commentItem.parent().hasClass("answerCommentContainer")) {
				const commentParentId = commentItem.parent().attr("id").replace("comments-list-", "");
				const answerBtn = $("#footer-comments-" + commentParentId + " a.lblComment").first();
				if (commentItem.prevAll(`[class*="link-show-more"]`).length > 0 && !commentItem.prevAll(`[class*="link-show-more"]`).first().find("a").hasClass("alreadyclicked")) {
					if (!dotOnly) {
						commentItem.prevAll(`[class*="link-show-more"]`).first().find("a").trigger("click").addClass("alreadyclicked");
					} else {
						commentItem.prevAll(`[class*="link-show-more"]`).first().find("a").addClass("has-newanswer");
					}
				}
				if (answerBtn.length > 0 && !answerBtn.hasClass("alreadyclicked") && !dotOnly) {
					answerBtn.addClass("alreadyclicked");
					eval(answerBtn.attr("href"))
				} else if (answerBtn.length > 0 && dotOnly) {
					answerBtn.addClass("has-newanswer");
				}
				return discussionComments.common.highlightComment(commentParentId, toBeSelectedComment, scrollOnLast, dotOnly);
			} else {
				if (commentItem.prevAll(`[class*="link-show-more"]`).length > 0 && !commentItem.prevAll(`[class*="link-show-more"]`).first().find("a").hasClass("alreadyclicked")) {
					if (!dotOnly) {
						commentItem.prevAll(`[class*="link-show-more"]`).first().find("a").trigger("click").addClass("alreadyclicked");
					} else {
						commentItem.prevAll(`[class*="link-show-more"]`).first().find("a").addClass("has-newanswer");
					};
				}
				const answerBtn = $("#footer-comments-" + commentId + " a.lblComment").first();

				$('.item-comment#item-comment-' + toBeSelectedComment).addClass("highlight-this")
				if (answerBtn.lenght > 0 && !answerBtn.hasClass("alreadyclicked") && !dotOnly) {
					answerBtn.addClass("alreadyclicked");
					eval(answerBtn.attr("href"));
				} else if (answerBtn.length > 0 && dotOnly) {
					answerBtn.addClass("has-newanswer");
				}

				if (scrollOnLast) {
					var scrollInterval = setInterval(() => {
						const targetElement = $('.item-comment#item-comment-' + toBeSelectedComment);
						if (targetElement.is(":visible")) {
							const scrollableDiv = $('.discussions-content .panel-right-content .comments-content .content-body');
							const scrollToPosition = targetElement.offset().top - (($(window).height() - targetElement.outerHeight(true)) / 2);
							scrollableDiv.animate({
								scrollTop: scrollToPosition
							}, 600);
							clearInterval(scrollInterval)
						}
					}, 600);
				}
				return false
			}
		},
		highlightAllNewComments: function (path) {
			return (new Promise(function (resolve, reject) {
				const splittedPath = path ? path.split(".") : [];
				const depenseindex = splittedPath.indexOf("depenseindex");
				const inputIdSuffix = path.indexOf("comment.financement.") > -1 ? ("_financement" + `${depenseindex > -1 && splittedPath[depenseindex + 1] ? "_" + splittedPath[depenseindex + 1] : ""}`) : "";
				const inputId = splittedPath[3] ? splittedPath[3] + inputIdSuffix : null;
				var newCommentsSelector = "";
				if (notEmpty(inputId) && discussionComments.params.toHighlightComments && typeof discussionComments.params.toHighlightComments[inputId] != "undefined" && notEmpty(discussionComments.params.toHighlightComments[inputId])) {
					$.each(discussionComments.params.toHighlightComments[inputId], function (index, commentId) {
						if ($("#item-comment-" + commentId).length > 0) {
							newCommentsSelector += `#item-comment-${commentId}`
							if (index < discussionComments.params.toHighlightComments[inputId].length - 1) {
								newCommentsSelector += ", "
							}
							if (index < discussionComments.params.toHighlightComments[inputId].length - 1) {
								discussionComments.common.highlightComment(commentId, commentId, false)
							} else {
								discussionComments.common.highlightComment(commentId, commentId, typeof discussionComments.params.toBeSelectedComment != "undefined" && notEmpty(discussionComments.params.toBeSelectedComment) ? false : true)
								var clearArrTimeout = setTimeout(() => {
									discussionComments.params.toHighlightComments[inputId].splice(0, discussionComments.params.toHighlightComments[inputId].length)
									$(newCommentsSelector).addClass("highlight-this")
									clearTimeout(clearArrTimeout)
								}, 300);
							}
						}
					})
				} else if (!notEmpty(path) && discussionComments.params.toHighlightComments && typeof discussionComments.params.toHighlightComments.allComments != "undefined" && notEmpty(discussionComments.params.toHighlightComments.allComments)) {
					$.each(discussionComments.params.toHighlightComments.allComments, function (index, commentId) {
						if ($("#item-comment-" + commentId).length > 0) {
							newCommentsSelector += `#item-comment-${commentId}`
							if (index < discussionComments.params.toHighlightComments.allComments.length - 1) {
								newCommentsSelector += ", "
							}
							if (index < discussionComments.params.toHighlightComments.allComments.length - 1) {
								discussionComments.common.highlightComment(commentId, commentId, false)
							} else {
								discussionComments.common.highlightComment(commentId, commentId, typeof discussionComments.params.toBeSelectedComment != "undefined" && notEmpty(discussionComments.params.toBeSelectedComment) ? false : true)
								var clearArrTimeout = setTimeout(() => {
									discussionComments.params.toHighlightComments.allComments.splice(0, discussionComments.params.toHighlightComments.allComments.length)
									$(newCommentsSelector).addClass("highlight-this")
									clearTimeout(clearArrTimeout)
								}, 300);
							}
						}
					})
				}
				resolve({ res: true });
				reject({
					code: 400
				})
			}))
		},
		getQuestionCurrentAnswer: function (answerId, stepKey, inputKey, callBack = () => { }) {
			const thisUrl = baseUrl + `/survey/answer/answer/id/${answerId}/step/${stepKey}/isinsideform/false/mode/r/view/costum/input/${inputKey}`;
			ajaxPost(null, thisUrl, null, function (response) {
				if (discussionComments.params.answer && notEmpty(jsonHelper.getValueByPath(discussionComments.params.answer, `answers.${stepKey}.${inputKey}`))) {
					callBack({ res: response, classesName: "move-mt-1" });
				} else {
					callBack({ res: `<span class="empty-answer bs-px-4">${ trad["No answer"] }</span>` });
				}
			}, null, null)
		},
		toggleLeftPanel: function () {
			if ($(".discussions-content .panel-left").is(":visible")) {
				$(".discussions-content .panel-left").toggle(400, function () {
					$(this).toggleClass("shown")
				});
			} else {
				$(".discussions-content .panel-left").toggleClass("shown").toggle(400);
			}
		},
		showEmptyPanelRight: function () {
			$(".discussions-content .panel-right-loader").html(`
				<div class="col-xs-12 bs-px-3 content-body text-center">
					Aucune question commentée
				</div>
			`).removeClass("hide").show(300);
		},
		showPanelRightLoader: function () {
			$(".discussions-content .panel-right-loader").empty().removeClass("hide").show(300);
			coInterface.showCostumLoader(".discussions-content .panel-right-loader");
		},
		hidePanelRightLoader: function () {
			$(".discussions-content .panel-right-loader").hide(300);
		},
		bindAnswerCardEvent: function () {
			$(".discussions-content .proposal-container .proposal-name.cursor-pointer").off("click").on("click", function () {
				if ($(this).attr("data-link")) {
					if (costum) {
						urlCtrl.loadByHash($(this).attr("data-link"))
					} else if (discussionComments.params && discussionComments.params.context && discussionComments.params.context.slug) {
						window.location.href = baseUrl + `/costum/co/index/slug/${discussionComments.params.context.slug}${$(this).attr("data-link")}`
					}
					if ($("#dialogContent").is(":visible")) {
						dialogContent.close()
					}
				}
			})
			const answerParentEltClick = function () {
				if ($("#dialogContent").is(":visible")) {
					dialogContent.close()
				}
			}
			$.each($(".answer-parent-elt"), function (index, elt) {
				if ($(elt)[0]) {
					$(elt)[0].removeEventListener("click", answerParentEltClick)
					$(elt)[0].addEventListener("click", answerParentEltClick)
				}
			})
			coInterface.bindLBHLinks();
		},
		handleQuestionClick: function (questionSelector, getCommentOnly = false) {
			const self = $(questionSelector);
			const formId = discussionComments.params.parentForm && discussionComments.params.parentForm._id ? discussionComments.params.parentForm._id.$id : null;
			const path = self.attr('data-path');
			const stepKey = self.attr('data-stepkey');
			const inputKey = self.attr('data-inputkey');
			const isAllComments = self.attr('data-is-allcomments');
			const depenseId = self.attr("data-depenseid");
			const commentIndex = self.attr("data-toselect-comment");
			$(".discussions-content .refresh-comment-content").hide();
			self.parent().closest(".panel-left-content").find("li.discuLi").removeClass("active")
			self.addClass("active");
			if ($(window).width() < 768 && discussionComments.click > 0) {
				discussionComments.common.toggleLeftPanel();
			}
			discussionComments.click++;
			if (discussionComments.params.answer && discussionComments.params.answer._id && stepKey && inputKey) {
				if (
					discussionComments.alreadyNotifyDiscussionMember && typeof discussionSocket != "undefined" && typeof discussionSocket.emitEvent != "undefined" &&
					userId && typeof userConnected != "undefined" && userConnected.name
				) {
					discussionSocket.emitEvent(
						wsCO,
						"change_user_activity",
						{
							userId: userId,
							onUpdate: "userWriting",
							commentId: discussionComments.params.answer._id,
							userActivity: "stopWriting",
						}
					);
					discussionComments.alreadyNotifyDiscussionMember = false;
				}
				const resetNotSeenCount = function (inputId, path) {
					if (!path) {
						return
					}
					const splittedPath = path.split(".");
					const thisStepId = splittedPath[1] ? splittedPath[1] : null;
					if (discussionComments.params.orderedQuestion[thisStepId] && discussionComments.params.orderedQuestion[thisStepId].inputs && discussionComments.params.orderedQuestion[thisStepId].inputs[inputId]) {
						discussionComments.params.orderedQuestion[thisStepId].inputs[inputId].notSeenComments = 0;
					}
				}
				if (!getCommentOnly) {
					discussionComments.common.showPanelRightLoader();
					if (path && path.indexOf(".discu_") > -1) {
						var discuTypeHint = trad["Discussions between"] + " " + trad["admin and bearer only"];
						if (path.indexOf(".privateComment") > -1) {
							discuTypeHint = trad["Discussions between"] + " " + trad["admins only"];
						}
						if (path.indexOf(".commentPublic") > -1) {
							discuTypeHint = ""
						}
						$(".discussions-content .panel-right .panel-right-content .answer-content").html(`
							<span class="content-header">
								<h4>
									${ trad["Context"] } - ${self.find(".question .message").text()}
									<small>
										</br>
										<span id="discussion-type">${discuTypeHint}</span>
									</small>
								</h4>
							</span>
							<div class="col-xs-12 bs-px-3 content-body cs-scroll xl-height">
								${discussionComments.common.buildProposalView(discussionComments.params.answer, discussionComments.params.answerTitlePath ? discussionComments.params.answerTitlePath : "answers.aapStep1.titre")}
							</div>
						`)
						discussionComments.common.bindAnswerCardEvent();
						discussionComments.common.hidePanelRightLoader()
						if (formId && path) {
							coInterface.showCostumLoader(".discussions-content .panel-right .panel-right-content .comments-content .html-container");
							discussionComments.common.getCurrentComments("forms", formId, path, function (resp) {
								discussionComments.common.hidePanelRightLoader();
								if (resp) {
									$(".footer-comments").remove()
									$(".discussions-content .panel-right .panel-right-content .comments-content .html-container").html(`
										<span class="content-header">
											<h4>${ trad.Comments }</h4>
										</span>
										<div class="col-xs-12 bs-px-3 content-body cs-scroll">
											${resp}
										</div>
									`);
								}
								self.removeClass("non-lus").removeAttr("data-notseen");
								resetNotSeenCount(inputKey, path);
							});
						}
					}
					else if (inputKey.indexOf("_financement") > -1) {
						discussionComments.common.hidePanelRightLoader()
						discussionComments.common.getCurrentDepenseCard(discussionComments.params.answer._id.$id, depenseId, function (response) {
							$(".discussions-content .panel-right .panel-right-content .answer-content").html(`
								<span class="content-header">
									<h4>${ trad["Answer to this question"] }</h4>
								</span>
								<div class="col-xs-12 bs-px-4 content-body cs-scroll xl-height">
									${response}
								</div>
							`);
						})
						if (formId && path) {
							coInterface.showCostumLoader(".discussions-content .panel-right .panel-right-content .comments-content .html-container");
							discussionComments.common.getCurrentComments("forms", formId, path, function (resp) {
								discussionComments.common.hidePanelRightLoader();
								if (resp) {
									$(".footer-comments").remove()
									$(".discussions-content .panel-right .panel-right-content .comments-content .html-container").html(`
										<span class="content-header">
											<h4>${ trad.Comments }</h4>
										</span>
										<div class="col-xs-12 bs-px-3 content-body cs-scroll">
											${resp}
										</div>
									`);
								}
								self.removeClass("non-lus").removeAttr("data-notseen");
								resetNotSeenCount(inputKey, path);
							});
						}
					} else {
						discussionComments.common.getQuestionCurrentAnswer(discussionComments.params.answer._id.$id, stepKey, inputKey, function (response) {
							if (response.res) {
								$(".discussions-content .panel-right .panel-right-content .answer-content").html(`
									<span class="content-header">
										<h4>${ trad["Answer to this question"] }</h4>
									</span>
									<div class="col-xs-12 bs-px-4 content-body cs-scroll">
										${response.res}
									</div>
								`);
							}
							if (response.classesName) {
								$(".discussions-content .panel-right .panel-right-content .answer-content .content-body #question_" + inputKey).addClass(response.classesName);
							}
							if (formId && path) {
								discussionComments.common.getCurrentComments("forms", formId, path, function (resp) {
									discussionComments.common.hidePanelRightLoader();
									if (resp) {
										$(".footer-comments").remove()
										$(".discussions-content .panel-right .panel-right-content .comments-content .html-container").html(`
											<span class="content-header">
												<h4>${ trad.Comments }</h4>
											</span>
											<div class="col-xs-12 bs-px-3 content-body with-question-comment cs-scroll">
												${resp}
											</div>
										`);
									}
									self.removeClass("non-lus").removeAttr("data-notseen");
									resetNotSeenCount(inputKey, path)
								});
							}
						});
					}
				} else {
					discussionComments.common.hidePanelRightLoader()
					if (inputKey.indexOf("_financement") > -1) {
						discussionComments.common.hidePanelRightLoader()
						discussionComments.common.getCurrentDepenseCard(discussionComments.params.answer._id.$id, depenseId, function (response) {
							$(".discussions-content .panel-right .panel-right-content .answer-content").html(`
								<span class="content-header">
									<h4>${ trad["Answer to this question"] }</h4>
								</span>
								<div class="col-xs-12 bs-px-4 content-body cs-scroll xl-height">
									${response}
								</div>
							`);
						})
						if (formId && path) {
							coInterface.showCostumLoader(".discussions-content .panel-right .panel-right-content .comments-content .html-container");
							discussionComments.common.getCurrentComments("forms", formId, path, function (resp) {
								discussionComments.common.hidePanelRightLoader();
								if (resp) {
									$(".footer-comments").remove()
									$(".discussions-content .panel-right .panel-right-content .comments-content .html-container").html(`
										<span class="content-header">
											<h4>${ trad.Comments }</h4>
										</span>
										<div class="col-xs-12 bs-px-3 content-body cs-scroll">
											${resp}
										</div>
									`);
								}
								self.removeClass("non-lus").removeAttr("data-notseen");
								resetNotSeenCount(inputKey, path);
							});
						}
					} else {
						if (formId && path) {
							coInterface.showCostumLoader(".discussions-content .panel-right .panel-right-content .comments-content .html-container");
							discussionComments.common.getCurrentComments("forms", formId, path, function (resp) {
								discussionComments.common.hidePanelRightLoader();
								if (resp) {
									$(".footer-comments").remove()
									$(".discussions-content .panel-right .panel-right-content .comments-content .html-container").html(`
										<span class="content-header">
											<h4>${ trad.Comments }</h4>
										</span>
										<div class="col-xs-12 bs-px-3 content-body with-question-comment cs-scroll">
											${resp}
										</div>
									`);
								}
								self.removeClass("non-lus").removeAttr("data-notseen");
								resetNotSeenCount(inputKey, path)
							});
						}
					}
				}
			} else if (discussionComments.params.answer && discussionComments.params.answer._id && (/true/).test(isAllComments)) {
				discussionComments.common.hidePanelRightLoader()
				$(".discussions-content .panel-right .panel-right-content .answer-content").html(`
					<span class="content-header">
						<h4>${ trad.Context }</h4>
					</span>
					<div class="col-xs-12 bs-px-3 content-body cs-scroll xl-height">
						${discussionComments.common.buildProposalView(discussionComments.params.answer, discussionComments.params.answerTitlePath ? discussionComments.params.answerTitlePath : "answers.aapStep1.titre")}
					</div>
				`)
				discussionComments.common.bindAnswerCardEvent();
				coInterface.showCostumLoader(".discussions-content .panel-right .panel-right-content .comments-content .html-container");
				discussionComments.common.getCurrentComments("answers", discussionComments.params.answer._id.$id, path, function (resp) {
					if (resp) {
						$(".footer-comments").remove()
						$(".discussions-content .panel-right .panel-right-content .comments-content .html-container").html(`
							<span class="content-header">
								<h4>${ trad.Comments }</h4>
							</span>
							<div class="col-xs-12 bs-px-3 content-body cs-scroll">
								${resp}
							</div>
						`);
					}
					discussionComments.params.allDiverNotSeenComments = 0;
					self.removeClass("non-lus").removeAttr("data-notseen");
				});
			} else {
				discussionComments.common.showEmptyPanelRight()
			}
		},
		bindDiscussionContentEvent: function () {
			var clickFromOtherBtn = false;
			if (typeof discussionComments.params.allDiverNotSeenComments != "undefined" && discussionComments.params.allDiverNotSeenComments > 0) {
				$(".discussions-content .panel-left-content li.discuLi.allComments").addClass("non-lus").attr("data-notseen", discussionComments.params.allDiverNotSeenComments)
			}
			$(".discussions-content .panel-left-content .questions-list li.discuLi, .discussions-content .panel-left-content li.discuLi.allComments").off("click").on("click", function () {
				discussionComments.common.handleQuestionClick(this, clickFromOtherBtn)
				clickFromOtherBtn = false;
			});
			$(".discussions-content .refresh-comment-content").off("click").on("click", function () {
				clickFromOtherBtn = true;
				$(".discussions-content .panel-left-content li.discuLi.active").trigger("click")
			})
			$(".discussions-content .create-new-discu").off("click").on("click", function () {
				$(".discussions-content .new-discussion-form").show(300)
				$("#discu-name").val("")
				$("#discu-privilege").val("authorAdmin")
			})
			$(".discussions-content .hideNewDiscuForm").off("click").on("click", function () {
				$(".discussions-content .new-discussion-form").hide(300, function () {
					$(".discussions-content .new-discussion-form .new-title").show()
					$(".discussions-content .new-discussion-form #discu-privilege").parent().show()
					$(".discussions-content .new-discussion-form .discussion-button.create-discu").show()
					$(".discussions-content .new-discussion-form .edit-title").addClass("hide").hide()
					$(".discussions-content .new-discussion-form .discussion-button.update-discu").addClass("hide").hide()
				})
			})
			$(".discussions-content .create-discu").off("click").on("click", function () {
				var discuName = $("#discu-name").val();
				var discuPrivilege = $("#discu-privilege").val();
				if (notEmpty(discuName)) {
					discussionComments.common.showPanelLeftLoader();
					const now = new Date();
					const newDiscussionId = "discu_" + now.getDate() + (now.getMonth() + 1) + now.getFullYear() + "" + now.getHours() + now.getMinutes() + now.getSeconds();
					const newDiscuStepId = `moreDiscussions_${"" + now.getHours() + now.getMinutes() + now.getSeconds() + discussionComments.common.genererChaineAleatoire()}`;
					var discussionPath = `${discussionComments.params.parentForm && discussionComments.params.parentForm._id ? discussionComments.params.parentForm._id.$id : "parentForm"}.${newDiscuStepId}.${discussionComments.params.answer && discussionComments.params.answer._id ? discussionComments.params.answer._id.$id : "answerId"}.${newDiscussionId}.comment`
					if (discuPrivilege == "authorAll") {
						discussionPath += "Public"
					}
					if (discuPrivilege == "private") {
						discussionPath = discussionPath.replace(".comment", ".privateComment")
					}
					const thisDiscuData = {
						name: discuName,
						privilege: discuPrivilege,
						path: discussionPath,
						stepId: newDiscuStepId,
						created: Math.round(now.getTime() / 1000),
						updated: Math.round(now.getTime() / 1000),
						creator: userId,
						status: "active"
					}
					discussionComments.common.createNewDiscussion(
						{
							id: discussionComments.params.answer._id.$id,
							path: "moreDiscussions." + newDiscussionId,
							collection: "answers",
							updatePartial: true,
							value: thisDiscuData
						},
						function (resp) {
							toastr.success("Discussion créée avec succès")
							thisDiscuData.id = newDiscussionId
							discussionComments.common.refreshDiscussionsList(thisDiscuData)
							$(".discussions-content .hideNewDiscuForm").trigger("click")
							discussionComments.common.hidePanelLeftLoader()
							const toClickElem = $(`.discussions-content .discuLi[data-path="${thisDiscuData.path}"]`)
							if (
								typeof discussionSocket != "undefined" && typeof discussionSocket.emitEvent != "undefined" &&
								userId
							) {
								discussionSocket.emitEvent(
									wsCO,
									"change_comment",
									{
										userId: userId,
										onUpdate: "newDiscussionGroup",
										newDiscuGroupData: thisDiscuData
									}
								);
							}
							var clickNewDiscu = setInterval(() => {
								if (toClickElem.is(":visible") || ($(window).width() < 768 && toClickElem.length > 0)) {
									clearInterval(clickNewDiscu)
									toClickElem.trigger("click")
								}
							}, 700);
						}
					)
				} else {
					toastr.error("Veuillez renseigner le nom de la discussion")
				}
			})
			$(".discussions-content .btn-action-container .edit-discu").off("click").on("click", function (e) {
				e.stopImmediatePropagation()
				$(".discussions-content .new-discussion-form .new-title").hide()
				$(".discussions-content .new-discussion-form .discussion-button.create-discu").hide()
				$(".discussions-content .new-discussion-form .edit-title").removeClass("hide").show()
				$(".discussions-content .new-discussion-form .discussion-button.update-discu").removeClass("hide").show()
				$("#discu-name").val($(this).attr("data-title"))
				$("#discu-privilege").parent().hide().val($(this).attr("data-privilege"))
				$(".discussions-content .new-discussion-form").show(300)
				$(".discussions-content .update-discu")
					.attr("data-discu-path", $(this).attr("data-discu-path"))
					.attr("data-stepid", $(this).attr("data-stepid"))
			});
			$(".discussions-content .btn-action-container .delete-discu").off("click").on("click", function (e) {
				e.stopImmediatePropagation()
				const thisElem = $(this);
				const currentStepId = thisElem.attr("data-stepid")
				const discuBDPath = thisElem.attr("data-discu-path");
				const discuPath = discuBDPath.replace("moreDiscussions.", "");
				var discuCommentPath = "currentCommentPath";
				if (discussionComments.params.orderedQuestion && discussionComments.params.orderedQuestion[currentStepId] && discussionComments.params.orderedQuestion[currentStepId].inputs && discussionComments.params.orderedQuestion[currentStepId].inputs[discuPath]) {
					discuCommentPath = discussionComments.params.orderedQuestion[currentStepId].inputs[discuPath].commentPath
				}
				$.confirm({
					title: trad["delete"],
					type: 'red',
					content: "<span class='bold'>" + trad["Are you sure you want to delete the discussion"] + " \"" + (thisElem.attr("data-title")) + "\"?</span><br><span class='text-red bold'>" + trad.actionirreversible + "</span>",
					buttons: {
						no: {
							text: trad.no,
							btnClass: 'btn btn-default'
						}, yes: {
							text: trad.yes,
							btnClass: 'btn btn-danger',
							action: function () {
								ajaxPost(
									null,
									baseUrl + "/co2/aap/commonaap/action/deleteDiscuGroup",
									{
										id: discussionComments.params.answer._id.$id,
										path: thisElem.attr("data-discu-path"),
										discucommentpath: discuCommentPath
									},
									function (response) {
										if (currentStepId && discussionComments.params.steps[currentStepId] && discussionComments.params.steps[currentStepId][discuPath]) {
											delete discussionComments.params.steps[currentStepId][discuPath];
										}
										if (discussionComments.params.orderedQuestion && discussionComments.params.orderedQuestion[currentStepId] && discussionComments.params.orderedQuestion[currentStepId].inputs && discussionComments.params.orderedQuestion[currentStepId].inputs[discuPath]) {
											delete discussionComments.params.orderedQuestion[currentStepId].inputs[discuPath];
										}

										if (thisElem.closest(".discuLi").hasClass("active")) {
											$(".discussions-content .panel-left-content .questions-list .discuLi").first().trigger("click");
										}
										if (
											typeof discussionSocket != "undefined" && typeof discussionSocket.emitEvent != "undefined" &&
											userId
										) {
											discussionSocket.emitEvent(
												wsCO,
												"change_comment",
												{
													userId: userId,
													onUpdate: "deleteDiscussionGroup",
													discuGroupData: {
														stepId: currentStepId,
														discuPath: discuPath,
														discuCommentPath: discuCommentPath
													}
												}
											);
										}
										thisElem.closest(".discuLi").remove();
										toastr.success("Discussion supprimée avec succès")
									},
									function (err) {
										mylog.error("Error creating new discussion", err)
									}
								)
							}
						}
					}
				});
			})
			$(".discussions-content .update-discu").off("click").on("click", function () {
				var discuName = $("#discu-name").val();
				const thisElem = $(this)
				const currentStepId = thisElem.attr("data-stepid")
				const discuBDPath = thisElem.attr("data-discu-path");
				const discuPath = discuBDPath.replace("moreDiscussions.", "");
				var discuPrivilege = $("#discu-privilege").val();

				if (notEmpty(discuName)) {
					discussionComments.common.showPanelLeftLoader();
					dataHelper.path2Value({
						id: discussionComments.params.answer._id.$id,
						collection: "answers",
						updatePartial: true,
						path: discuBDPath,
						value: {
							name: discuName,
						}
					}, function (res) {
						if (res.text && res.text.name) {
							if (currentStepId && discussionComments.params.steps[currentStepId] && discussionComments.params.steps[currentStepId][discuPath]) {
								discussionComments.params.steps[currentStepId][discuPath].name = res.text.name
							}
							if (discussionComments.params.orderedQuestion && discussionComments.params.orderedQuestion[currentStepId] && discussionComments.params.orderedQuestion[currentStepId].inputs && discussionComments.params.orderedQuestion[currentStepId].inputs[discuPath]) {
								discussionComments.params.orderedQuestion[currentStepId].inputs[discuPath].label = res.text.name
							}
							if (
								typeof discussionSocket != "undefined" && typeof discussionSocket.emitEvent != "undefined" &&
								userId
							) {
								discussionSocket.emitEvent(
									wsCO,
									"change_comment",
									{
										userId: userId,
										onUpdate: "updateDiscussionGroup",
										discuGroupData: {
											stepId: currentStepId,
											discuPath: discuPath,
											name: res.text.name,
										}
									}
								);
							}
							const discuListElem = $(".discussions-content .panel-left-content .questions-list")
							if (discuListElem.length > 0) {
								var currentActive = "";
								if (discuListElem.find(".discuLi.active").hasClass("allComments")) {
									currentActive = "allComments";
								} else if (discuListElem.find(".discuLi.active").attr("data-inputkey")) {
									currentActive = discuListElem.find(".discuLi.active").attr("data-inputkey");
								}
								discuListElem.html(discussionComments.common.buildCommentedQuestionList(typeof discussionComments.params.orderedQuestion != "undefined" && notEmpty(discussionComments.params.orderedQuestion) ? discussionComments.params.orderedQuestion : {}, false, currentActive));
								discussionComments.common.bindDiscussionContentEvent()
							}
						}
						toastr.success("Discussion modifiée avec succès")
					})
					$(".discussions-content .hideNewDiscuForm").trigger("click")
					discussionComments.common.hidePanelLeftLoader()
				} else {
					toastr.error("Veuillez renseigner le nom de la discussion")
				}
			});
		},
		updateAnswerBtnNb: function (containerSelector, parentCommentId) {
			const answerCommentsCount = $(`.answerCommentContainer#comments-list-${parentCommentId} > .item-comment`).length
			if (answerCommentsCount > 0) {
				$(containerSelector).find(".nbNewsComment").text(answerCommentsCount)
			} else {
				$(containerSelector).find(".nbNewsComment").text("")
			}
		},
		/**
		 * 
		 * @param {string} commentId id of the new comment added by socket 
		 */
		refreshLeftPanelSeenOneComment: function (commentId) {
			const thisElem = $(".discussions-content .panel-left-content .questions-list")
			if (thisElem.length > 0) {
				var currentActive = "";
				if (thisElem.find(".discuLi.active").hasClass("allComments")) {
					currentActive = "allComments";
				} else if (thisElem.find(".discuLi.active").attr("data-inputkey")) {
					currentActive = thisElem.find(".discuLi.active").attr("data-inputkey");
				}
				discussionComments.common.updateSeenComments(commentId)
				thisElem.html(discussionComments.common.buildCommentedQuestionList(typeof discussionComments.params.orderedQuestion != "undefined" && notEmpty(discussionComments.params.orderedQuestion) ? discussionComments.params.orderedQuestion : {}, false, currentActive));
				discussionComments.common.bindDiscussionContentEvent()
			}
		},
		refreshDiscussionCommentsParamsData: function (newCommentData, discussionId = null) {
			if (notEmpty(newCommentData) && discussionComments.params && discussionComments.params.steps) {
				var newRubric = {};
				discussionComments.params.newComment = newCommentData;
				// if (discussionId && discussionComments.members && discussionComments.members.active && discussionComments.members.active[discussionId] && newCommentData.author && discussionComments.members.active[discussionId][newCommentData.author]) {
				// 	discussionComments.params.newComment ? discussionComments.params.newComment.authorData = {
				// 		_id: {
				// 			'$id': newCommentData.author
				// 		},
				// 		collection: "citoyens",
				// 		name: discussionComments.members.active[discussionId][newCommentData.author].name,
				// 		profilThumbImageUrl: discussionComments.members.active[discussionId][newCommentData.author].profilThumbImageUrl
				// 	} : ""
				// } else {
				if (newCommentData.author && newCommentData.author.id && newCommentData.author.name) {
					discussionComments.params.newComment ? discussionComments.params.newComment.authorData = {
						_id: {
							'$id': newCommentData.author.id
						},
						collection: "citoyens",
						name: newCommentData.author.name,
						profilThumbImageUrl: newCommentData.author.profilThumbImageUrl ? newCommentData.author.profilThumbImageUrl : modules.co2.url + "/images/thumb/default_citoyens.png"
					} : ""
				}
				// }
				if (typeof newCommentData.path != "undefined" && notEmpty(newCommentData.path)) {
					const splittedPath = newCommentData.path.split(".");
					const depIndex = splittedPath.indexOf("depenseindex")
					const depKey = splittedPath.indexOf("depensekey")
					const thisDepId = depIndex > -1 && splittedPath[depIndex + 1] ? splittedPath[depIndex + 1] : 0;
					const thisDepKey = depKey > -1 && splittedPath[depKey + 1] ? splittedPath[depKey + 1] : "";
					const inputIdSuffix = newCommentData.path.indexOf("comment.financement.") > -1 ? ("_financement" + `${depIndex > -1 && splittedPath[depIndex + 1] ? "_" + splittedPath[depIndex + 1] : ""}`) : "";
					const inputKey = splittedPath[3] ? splittedPath[3] : null;
					const inputId = splittedPath[3] ? splittedPath[3] + inputIdSuffix : null;
					const stepId = splittedPath[1] ? splittedPath[1] : null;
					var toHighlightThisComment = false;
					if (stepId && discussionComments.params.orderedQuestion && discussionComments.params.orderedQuestion[stepId]) {
						toHighlightThisComment = true;
						typeof discussionComments.params.orderedQuestion[stepId].inputs[inputId].notSeenComments != "undefined" ? discussionComments.params.orderedQuestion[stepId].inputs[inputId].notSeenComments++ : discussionComments.params.orderedQuestion[stepId].inputs[inputId].notSeenComments = 1;
						var orderedQuestionKeys = Object.keys(discussionComments.params.orderedQuestion)
						const currentRubricIndex = orderedQuestionKeys.indexOf(stepId)
						var newOrderedQuestion = {};
						if (currentRubricIndex > 0) {
							const currentRubric = orderedQuestionKeys.splice(currentRubricIndex, 1)
							orderedQuestionKeys.unshift(currentRubric[0])
							$.each(orderedQuestionKeys, function (index, key) {
								newOrderedQuestion[key] = discussionComments.params.orderedQuestion[key]
							})
							discussionComments.params.orderedQuestion = $.extend({}, true, newOrderedQuestion)
						}
					} else {
						$.each(discussionComments.params.steps, function (stepKey, stepVal) {
							if (stepKey == stepId) {
								if (stepVal.inputs && stepVal.inputs[inputKey]) {
									toHighlightThisComment = true
									newRubric = $.extend({}, true, stepVal),
										newRubric.inputs = {
											[inputId]: $.extend({}, true, stepVal.inputs[inputKey])
										}
									newRubric.inputs[inputId].notSeenComments = 1;
									if (notEmpty(inputIdSuffix)) {
										newRubric.inputs[inputId].showAnyway = true
										newRubric.inputs[inputId].commentPath = newCommentData.path
										if (discussionComments.params.answer && discussionComments.params.answer.answers) {
											const splittedDepKey = thisDepKey.split("-")
											const depensePath = splittedDepKey.join(".") + "." + thisDepId
											const depenseVal = jsonHelper.getValueByPath(discussionComments.params.answer.answers, depensePath)
											if (depenseVal && typeof depenseVal.poste != "undefined") {
												newRubric.inputs[inputId].label = depenseVal.poste + " (Financement)";
											}
										}
									}
								}
							}
						})
						if (notEmpty(newRubric)) {
							var newOrderedQuestions = {
								[stepId]: newRubric
							}
							$.each(discussionComments.params.orderedQuestion, (questionKey, questionVal) => {
								newOrderedQuestions[questionKey] = $.extend({}, true, questionVal)
							})
						}
					}
					if (toHighlightThisComment) {
						if (discussionComments.params.toHighlightComments && discussionComments.params.toHighlightComments[inputId]) {
							discussionComments.params.toHighlightComments[inputId].push(newCommentData._id.$id);
						} else if (discussionComments.params.toHighlightComments) {
							discussionComments.params.toHighlightComments[inputId] = [newCommentData._id.$id];
						}
					}
				} else {
					if (discussionComments.params.toHighlightComments && typeof discussionComments.params.toHighlightComments.allComments != "undefined") {
						discussionComments.params.toHighlightComments.allComments.push(newCommentData._id.$id);
						typeof discussionComments.params.allDiverNotSeenComments != "undefined" ? discussionComments.params.allDiverNotSeenComments++ : discussionComments.params.allDiverNotSeenComments = 1;
					}
				}
			}
		},
		updateSeenComments: function (commentId) {
			if (discussionComments.params.toHighlightComments && typeof discussionComments.params.toHighlightComments.allComments != "undefined" && notEmpty(discussionComments.params.toHighlightComments.allComments)) {
				const commentIndex = discussionComments.params.toHighlightComments.allComments.indexOf(commentId);
				if (commentIndex > -1) {
					discussionComments.params.toHighlightComments.allComments.splice(commentIndex, 1);
					discussionComments.params.allDiverNotSeenComments--;
				}
			} else if (discussionComments.params.toHighlightComments) {
				const exludeKeys = ["allComments"];
				$.each(discussionComments.params.toHighlightComments, function (rubricKey, rubricVal) {
					if (exludeKeys.indexOf(rubricKey) == -1) {
						const commentIndex = rubricVal.indexOf(commentId);
						if (commentIndex > -1) {
							discussionComments.params.toHighlightComments[rubricKey].splice(commentIndex, 1);
							if (discussionComments.params.orderedQuestion) {
								$.each(discussionComments.params.orderedQuestion, function (stepKey, stepVal) {
									if (stepVal.inputs[rubricKey] && stepVal.inputs[rubricKey].notSeenComments > 0) {
										discussionComments.params.orderedQuestion[stepKey].inputs[rubricKey].notSeenComments--;
									}
								})
							}
						}
					}
				})
			}
		},
		/**
		 * Represents a DOM element.
		 * @typedef {HTMLElement} element
		 * @returns {boolean} - True if the element is in the viewport, false otherwise.
		 */
		isInViewport: function (element) {
			if (element && typeof element.getBoundingClientRect === 'function') {
				const rect = element.getBoundingClientRect();
				return (
					rect.top >= 0 &&
					rect.left >= 0 &&
					rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
					rect.right <= (window.innerWidth || document.documentElement.clientWidth)
				);
			} else {
				return false;
			}
		},
		/**
		 * Updates the visible times of elements based on their timestamps.
		 * @param {string} elementsSelector - The selector for the elements to update.
		 * @param {number} timeStampDate - The timestamp date to use for relative time calculation.
		 * @param {string} createdTimeAttr - The attribute name that contains the creation timestamp of the element if `timeStampDate` is empty. 
		*/
		updateVisibleTimes: function (elementsSelector, timeStampDate = null, createdTimeAttr = "time") {
			const timeRegex = /(\.\s\d{2}:\d{2}|[a-z]{3}\.\s\d{2}\s⋅\s\d{1,2}:\d{2})$/i;
			$(elementsSelector + ":visible").each(function () {
				const timeText = $(this).text();
				if (discussionComments.common.isInViewport(this) && !timeRegex.test(timeText)) {
					const timeData = timeStampDate ? timeStampDate : $(this).data(createdTimeAttr);
					const relativeTime = discussionComments.common.getRelativeTime(timeData);
					$(this).html(relativeTime);
				}
			});
		},
		/**
		 * Checks if an element is in the viewport.
		 * @param {HTMLElement} element - The element to check.
		 * @param {boolean} showDateOnly - Whether show the date only
		 * @returns {boolean} - True if the element is in the viewport, false otherwise. 
		*/
		getRelativeTime: function (timestamp, showDateOnly = null) {
			var str = '';
			formatDateView = "DD MMMM ⋅ HH:MM";
			var d = new Date();
			var current_seconde = Math.floor(new Date().getTime() / 1000);

			var seconde = current_seconde.valueOf() - timestamp.valueOf();
			var hours = Math.floor(seconde / 60 / 60);
			var minutes = Math.floor(seconde / 60);
			var day = Math.floor(hours / 24);

			if (notNull(showDateOnly) && showDateOnly == true) {
				d.setTime(timestamp * 1000);
				str += '<i class="fa fa-calendar"></i> ' + moment().day(d.getDay()).local().locale(mainLanguage).format('dddd') + ' ' + moment(d).local().locale(mainLanguage).format("DD MMMM YYYY");
				return '<div class=\'dateUpdated\'>' + str + '</div>';
			} else {
				if (seconde < 60) {
					str += '<i class="fa fa-clock-o"></i> ' + trad.justnow;
				}
				else if (seconde >= 60 && minutes < 60) {
					str += '<i class="fa fa-clock-o"></i> ' + trad.ago + ' ' + minutes + ' ' + trad.min
				}
				else if (minutes >= 60 && hours < 12) {
					str += '<i class="fa fa-clock-o"></i> ' + trad.ago + ' ' + hours + ' h';
				}
				else if (hours >= 12 && hours < 24) {
					var dd = new Date();
					dd.setTime(dd.getTime());
					d.setTime(timestamp * 1000);
					var today = dd.getDay();
					var day = d.getDay();
					if (today == day) {
						d.setTime(timestamp * 1000);
						str += '<i class="fa fa-clock-o"></i> ' + trad.tod + '. ' + d.getHours() + ':' + d.getMinutes();
					} else {
						d.setTime(timestamp * 1000)
						str += '<i class="fa fa-clock-o"></i> ' + trad.yest + '. ' + d.getHours() + ':' + d.getMinutes();
					}
				}// si timestamp >= 1jour et timestamp < 1semaine
				else if (hours >= 24 && hours < 168) {
					d.setTime(timestamp * 1000);
					var datedate = d.getDate() < 10 ? '0' + d.getDate() : d.getDate();
					str += '<i class="fa fa-clock-o"></i> ' + moment().day(d.getDay()).local().locale(mainLanguage).format('ddd') + ' ' + datedate + ' ⋅ ' + d.getHours() + ':' + d.getMinutes();
				}// si timestamp >= 1 semaine
				else if (hours >= 168) {
					var dd = new Date();
					dd.setTime(dd.getTime());
					d.setTime(timestamp * 1000)
					var now = dd.getFullYear();
					var creat = d.getFullYear();
					//si année craated = année actuel
					if (now == creat) {
						d.setTime(timestamp * 1000);
						str += '<i class="fa fa-clock-o"></i> ' + moment().day(d.getDay()).local().locale(mainLanguage).format('ddd') + ' ' + moment(d).local().locale(mainLanguage).format("DD MMMM ⋅ HH:MM");
					} else {
						d.setTime(timestamp * 1000);
						str += '<i class="fa fa-clock-o"></i> ' + moment().day(d.getDay()).local().locale(mainLanguage).format('ddd') + ' ' + moment().local().locale(mainLanguage).format("DD MMMM YYYY ⋅ HH:MM");
					}
				}
				return str;
			}
		},
		startIntervalIfNeeded: function (elementsToUpdateSelector, elementParams = {}) {
			if ($(elementsToUpdateSelector).length > 0 && !discussionComments.intervalId) {
				discussionComments.updateTimeParams.elementsSelector = elementsToUpdateSelector;
				elementParams.timeStampDate ? discussionComments.updateTimeParams.timeStamp = elementParams.timeStampDate : "";
				elementParams.createdTimeAttr ? discussionComments.updateTimeParams.createdTimeAttr = elementParams.createdTimeAttr : "";
				discussionComments.intervalId = setInterval(() => {
					discussionComments.common.updateVisibleTimes(
						elementsToUpdateSelector,
						elementParams.timeStampDate ? elementParams.timeStampDate : null,
						elementParams.createdTimeAttr ? elementParams.createdTimeAttr : "time"
					)
				}, 40000);
				discussionComments.common.updateVisibleTimes(
					elementsToUpdateSelector,
					elementParams.timeStampDate ? elementParams.timeStampDate : null,
					elementParams.createdTimeAttr ? elementParams.createdTimeAttr : "time"
				);
				window.removeEventListener("scroll", discussionComments.common.upadetTimeOnWindowScroll);
				window.addEventListener("scroll", discussionComments.common.upadetTimeOnWindow);
			}
		},
		upadetTimeOnWindowScroll: function () {
			discussionComments.common.updateVisibleTimes(
				discussionComments.updateTimeParams.elementsSelector,
				discussionComments.updateTimeParams.timeStamp ? discussionComments.updateTimeParams.timeStamp : null,
				discussionComments.updateTimeParams.createdTimeAttr ? discussionComments.updateTimeParams.createdTimeAttr : "time"
			)
		},
		showPanelLeftLoader: function () {
			$(".discussions-content .panel-left-loader").empty().removeClass("hide").show(300);
			coInterface.showCostumLoader(".discussions-content .panel-left-loader");
		},
		hidePanelLeftLoader: function () {
			$(".discussions-content .panel-left-loader").empty().removeClass("hide").hide(300);
		},
		createNewDiscussion: function (discuData, callBack) {
			if (discuData && discuData.id && discuData.path) {
				ajaxPost(
					null,
					baseUrl + "/co2/aap/commonaap/action/createNewDiscu",
					discuData,
					function (response) {
						callBack(response)
					},
					function (err) {
						mylog.error("Error creating new discussion", err)
					}
				)
			}
		},
		genererChaineAleatoire: function () {
			const alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
			let resultat = '';

			for (let i = 0; i < 4; i++) {
				const indexAleatoire = Math.floor(Math.random() * alphabet.length);
				resultat += alphabet[indexAleatoire];
			}

			return resultat;
		},
		/**
		 * Retrieves the value from the usersObj based on the specified userValPath.
		 * The resulting value, usersVal, can be in one of the following formats:
		 * - An array of user IDs.
		 * - An object where the keys are user IDs and the values are user details.
		 * - A string representing a single or more (separate by coma) user ID.
		 *
		 * @param {Object} usersObj - The object containing user data.
		 * @param {string} userValPath - The path used to retrieve the value from usersObj.
		 * @returns {(Array<string>|Object<string, Object>|string)} usersVal - The value obtained from userValPath.
		 */
		checkIfUserCanAccesPrivateDiscu: function (userValPath, usersObj, currentUserId = userId) {
			if (userValPath && usersObj && currentUserId) {
				const usersVal = jsonHelper.getValueByPath(usersObj, userValPath);
				if (usersVal && usersVal) {
					if (Array.isArray(usersVal) && usersVal.indexOf(currentUserId) > -1) {
						return true;
					}
					if (typeof usersVal == "object") {
						const userValKeys = Object.keys(usersVal);
						if (userValKeys.indexOf(currentUserId) > -1) {
							return true;
						}
					}
					if (typeof usersVal == "string" && usersVal.indexOf(currentUserId) > -1) {
						return true;
					}
					return false;
				}
			}
			return false;

		},
		getInitials: function (name) {
			var words = name.trim().split(" ");

			words = words.filter(word => word.length > 0);

			if (words.length === 1) {
				return words[0].length > 1 ? words[0].substring(0, 2).toUpperCase() : words[0].substring(0, 1).toUpperCase();
			}

			const firstInitial = words[0].length > 0 ? words[0].substring(0, 1) : '';
			const secondInitial = words[1].length > 0 ? words[1].substring(0, 1) : '';

			return (firstInitial + secondInitial).toUpperCase();
		},
		refreshDiscussionsList: function (newDiscuData) {
			if (discussionComments.params) {
				if (
					newDiscuData.stepId
				) {
					discussionComments.params.orderedQuestion[newDiscuData.stepId] = {
						id: newDiscuData.id,
						name: newDiscuData.name ? newDiscuData.name : "",
						formParent: discussionComments.params.parentForm && discussionComments.params.parentForm._id ? discussionComments.params.parentForm._id.$id : null,
						step: newDiscuData.stepId,
						"_id": { '$id': newDiscuData.id },
						inputs: {
							[newDiscuData.id]: {
								label: newDiscuData.name,
								showAnyway: true,
								commentPath: newDiscuData.path
							}
						}
					}
					discussionComments.params.steps[newDiscuData.stepId] = {
						[newDiscuData.id]: {
							id: newDiscuData.id,
							name: newDiscuData.name,
							formParent: discussionComments.params.parentForm && discussionComments.params.parentForm._id ? discussionComments.params.parentForm._id.$id : null,
							step: newDiscuData.stepId,
							"_id": { '$id': newDiscuData.id },
							inputs: {
								[newDiscuData.id]: {
									label: newDiscuData.name,
									showAnyway: true,
									commentPath: newDiscuData.path
								}
							}
						}
					}
				}
				const thisElem = $(".discussions-content .panel-left-content .questions-list");
				var currentActive = "";
				if (thisElem.find(".discuLi.active").hasClass("allComments")) {
					currentActive = "allComments";
				} else if (thisElem.find(".discuLi.active").attr("data-inputkey")) {
					currentActive = thisElem.find(".discuLi.active").attr("data-inputkey");
				}
				thisElem.html(discussionComments.common.buildCommentedQuestionList(typeof discussionComments.params.orderedQuestion != "undefined" && notEmpty(discussionComments.params.orderedQuestion) ? discussionComments.params.orderedQuestion : {}, false, currentActive));
				discussionComments.common.bindDiscussionContentEvent()
			}
		}
	}
}

function showAjaxPanel(url, title, icon, mapEnd, urlObj) {
	//alert("showAjaxPanel"+url);
	//$("#loadingModal").css({"opacity": 0.7});
	var dest = ".pageContent";
	if (typeof urlObj == "undefined" || typeof urlObj.useHeader != "undefined")
		dest = themeObj.mainContainer;
	if (notNull(costum) && typeof costum.htmlConstruct != "undefined" && notNull(costum.htmlConstruct)
		&& typeof costum.htmlConstruct.mainContainer != "undefined"
		&& $(costum.htmlConstruct.mainContainer).length)
		dest = costum.htmlConstruct.mainContainer;
	mylog.log("showAjaxPanel", url, urlObj, dest, urlCtrl.afterLoad);
	//var dest = themeObj.mainContainer;
	hideScrollTop = false;
	//alert("showAjaxPanel"+dest);
	showNotif(false);
	$(".hover-info,.hover-info2").hide();
	$(".box").hide(200);
	icon = (icon) ? " <i class='fa fa-" + icon + "'></i> " : "";
	$(".panelTitle").html(icon + title).fadeIn();
	mylog.log("GETAJAX", icon + title);
	//showTopMenu(true);
	urlCtrl.closeXsMenu();
	userIdBefore = userId;
	coInterface.simpleScroll(0, 300);
	$(".progressTop").show().val(30);
	if (coInterface.showMapOnLoad)
		showMap(true);
	else
		showMap(false);
	setTimeout(function () {
		if ($(dest).length) {
			setTimeout(function () { $('.progressTop').val(40) }, 500);
			setTimeout(function () { $('.progressTop').val(60) }, 1500);
			/*setTimeout(function(){$("#loadingModal").css({"opacity": 0.});}, 500);
			setTimeout(function(){$("#loadingModal").css({"opacity": 0.5});}, 1500);*/
			$("#loadingModal").css({ "opacity": 0.9 });

			params = {
				"clienturi": location.href
			};

			ajaxPost(dest, url, params, function (data) {

				if (dest != themeObj.mainContainer)
					$(".subModuleTitle").html("");
				//showMap(false);
				$(".modal-backdrop").hide();
				if (coInterface.menu.showFilters) $("#filters-nav").show();
				else $("#filters-nav").hide().empty();

				coInterface.setTopPosition();
				coInterface.bindEvents();
				//coInterface.bindLBHLinks();
				coInterface.initHtmlPosition();
				$(".progressTop").val(90);
				//$("#loadingModal").hide(1000);
				if ($("#firstLoader").is(":visible")) {
					setTimeout(function () { $("#loadingModal").css({ "opacity": 0.8 }); }, 250);
					setTimeout(function () { $("#loadingModal").css({ "opacity": 0.6 }); }, 250);
					setTimeout(function () { $("#loadingModal").css({ "opacity": 0.4 }); }, 250);
					setTimeout(function () { $("#loadingModal").css({ "opacity": 0.2 }); $("#firstLoader").hide(400); }, 250);
					//setTimeout(function(){}, 10);
				}
				setTimeout(function () { $(".progressTop").val(100) }, 10);
				$(".progressTop").fadeOut(300);
				$.unblockUI();
				if (!$("#page-top").is(":visible")) $("#page-top").show();
				if (mapEnd)
					showMap(true);

				//				if(userId!="")
				//					addBtnSwitch();

				if (typeof contextData != "undefined" && contextData != null && contextData.type && contextData.id) {
					// VERIFIER REGRESSION SUR CE COMMENTAIRE (02/10/2020) => FIX EDIT POI CLASSFIED IN CONTEXT DATA
					//uploadObj.set(contextData.type,contextData.id);
					mylog.log("seo meta data : ", contextData.name, contextData.shortDescription, contextData.tags);
					if (contextData.name) {
						$('meta[name=title]').attr('content', contextData.name);
						mylog.log("seo meta title", $('meta[name=title]').attr('content'));
					}
					if (contextData.shortDescription) {
						$('meta[name=description]').attr('content', contextData.shortDescription);
						mylog.log("seo meta description", $('meta[name=description]').attr('content'));
					}
					if (contextData.tags) {
						$('meta[name=keywords]').attr('content', contextData.tags.toString());
						mylog.log("seo meta keywords", $('meta[name=keywords]').attr('content'));
					}
					if (contextData.creator) {
						$('meta[name=author]').attr('content', contextData.creator);
						mylog.log("seo meta author", $('meta[name=author]').attr('content'));
					}
				}

				if (typeof urlCtrl.afterLoad == "function") {
					urlCtrl.afterLoad();
					urlCtrl.afterLoad = null;
				}

				if (location.hash.indexOf("#panel") >= 0) {
					panelName = location.hash.substr(7);
					mylog.log("panelName", panelName);
					if (userId == "") {
						if (panelName == "box-login")
							Login.openLogin();
						else if (panelName == "box-register")
							$('#modalRegister').modal("show");

					}
				}
			});
		} else
			console.error('showAjaxPanel', dest, "doesn't exist");
	}, 100);
}


function inMyContacts(type, id) {
	var res = false;
	//var type= (type=="citoyens") ? "people" : type;
	if (typeof myContacts != "undefined" && myContacts != null && myContacts[type]) {
		$.each(myContacts[type], function (key, val) {
			//mylog.log("val", val);
			if ((typeof val["_id"] != "undefined" && id == val["_id"]["$id"]) ||
				(typeof val["id"] != "undefined" && id == val["id"])) {
				res = true;
				return;
			}
		});
	}
	return res;
}
/* ****************
Generic non-ajax panel loading process 
**************/
function showPanel(box, callback) {
	$(".my-main-container").scrollTop(0);

	$(".box").hide(200);
	showNotif(false);

	if (isMapEnd) showMap(false);

	mylog.log("showPanel", box);
	//showTopMenu(false);
	$(themeObj.mainContainer).animate({ top: -1500, opacity: 0 }, 500);

	$("." + box).show(500);
	if (typeof callback == "function") {
		callback();
	}
}




/*prevDbAccessCount = 0; 
function clearDbAccess() { 
	getAjax(null, baseUrl+'/'+moduleId+"/log/clear", function(data){ 
		$(".dbAccessBtn").remove();
		prevDbAccessCount = 0; 
	});
}*/

function decodeHtml(str) {
	mylog.log("decodeHtml", str);
	var txt = document.createElement("textarea");
	txt.innerHTML = str;
	mylog.log("decodeHtml", txt.value);
	return txt.value;
}

function setTitle(str, icon, topTitle, keywords, shortDesc) {
	mylog.log("setTitle", str);
	if (typeof icon != "undefined" && icon != "")
		icon = (icon.indexOf("<i") >= 0) ? icon : "<i class='fa fa-" + icon + "'></i> ";

	//$(".moduleLabel").html( icon +" "+ str);

	if (topTitle)
		str = topTitle;
	defaultTitle = (typeof costum != "undefined" && costum != null && typeof costum.title != "undefined") ? costum.title : "Communecter, se connecter à sa commune";
	// $(document).prop('title', (str != "") ? str : defaultTitle);

	if (notNull(keywords))
		$('meta[name="keywords"]').attr("content", keywords);
	else if (typeof costum == "undefined" || !notNull(costum))
		$('meta[name="keywords"]').attr("content", "communecter,connecter, commun,commune, réseau, sociétal, citoyen, société, territoire, participatif, social, smarterre");

	if (notNull(shortDesc)) {
		$('meta[name="description"]').attr("content", shortDesc);
	}
	//	else if(typeof costum == "undefined" || !notNull(costum))
	//		$('meta[name="description"]').attr("content","Communecter : Connecter à sa commune, inter connecter les communs, un réseau sociétal pour un citoyen connecté et acteur au centre de sa société.");
}


var backUrl = null;
function checkIsLoggued(uId) {
	if (uId == "" || typeof uId == "undefined") {
		mylog.warn("");
		toastr.error("<h1>Section Sécuriser, Merci de vous connecter!</h1>");

		setTitle("Section Sécuriser", "user-secret");

		backUrl = location.hash;
		Login.openLogin();

		resetUnlogguedTopBar();
	} else
		return true;
}
function resetUnlogguedTopBar() {
	//put anything that needs to be reset 
	//replace the loggued toolBar nav by log buttons
	$('.topMenuButtons').html('<button class="btn-top btn btn-success  hidden-xs" onclick="showPanel(\'box-register\');"><i class="fa fa-plus-circle"></i> <span class="hidden-sm hidden-md hidden-xs">Sinscrire</span></button>' +
		' <button class="btn-top btn bg-red  hidden-xs" style="margin-right:10px;" onclick="showPanel(\'box-login\');"><i class="fa fa-sign-in"></i> <span class="hidden-sm hidden-md hidden-xs">Se connecter</span></button>');
}

function _checkLoggued() {
	ajaxPost(
		null,
		baseUrl + "/" + moduleId + "/person/logged",
		null,
		function (data) {
			if (typeof data === 'string') {
				data = JSON.parse(data);
			}
			if (!data.userId || data.userId == "" || typeof data.userId == "undefined")
				window.location.reload();
		}
	);
}


/* ****************
DEPRACATED 01/2020 BY BOUBOULE : @RAPHAELRIVIERE DELETE FOR SUR
visualize all tagged elements on a map
**************/
/*function showTagOnMap (tag) { 

	mylog.log("showTagOnMap",tag);

	var data = { 	 "name" : tag, 
						 "locality" : "",
						 "searchType" : [ "persons" ], 
						 //"searchBy" : "INSEE",
					 "indexMin" : 0, 
					 "indexMax" : 500  
					};

		//setTitle("", "");$(".moduleLabel").html("<i class='fa fa-spin fa-circle-o-notch'></i> Les acteurs locaux : <span class='text-red'>" + cityNameCommunexion + ", " + cpCommunexion + "</span>");
		
		$.blockUI({
			message : "<h1 class='homestead text-red'><i class='fa fa-spin fa-circle-o-notch'></i> Recherches des collaborateurs ...</h1>"
		});

		showMap(true);
		ajaxPost(
			null,
			baseUrl+"/"+moduleId+"/search/globalautocomplete",
			data,
			function(data){ 
					  if( !data.userId || data.userId == "" ||  typeof data.userId == "undefined" )
					window.location.reload();
			},
			function (data){
				 mylog.log("error"); mylog.dir(data);          
			},
		);
		({
		  type: "POST",
			  url: baseUrl+"/" + moduleId + "/search/globalautocomplete",
			  data: data,
			  dataType: "json",
			  error: function (data){
				 mylog.log("error"); mylog.dir(data);          
			  },
			  success: function(data){
				if(!data){ toastr.error(data.content); }
				else{
					mylog.dir(data);
					Sig.showMapElements(Sig.map, data);
					$.unblockUI();
				}
			  }
		  });

	//urlCtrl.loadByHash('#project.detail.id.56c1a474f6ca47a8378b45ef',null,true);
	//Sig.showFilterOnMap(tag);
}*/

/* ****************
show a definition in the focus menu panel
**************/
function showDefinition(id, copySection) {

	setTimeout(function () {
		mylog.log("showDefinition", id, copySection);

		//$( themeObj.mainContainer ).animate({ opacity:0.3 }, 400 );

		if (copySection) {
			contentHTML = $("." + id).html();
			if (copySection != true)
				contentHTML = copySection;
			smallMenu.open(contentHTML);
			bindExplainLinks()
		}
		else {
			$(".hover-info").css("display", "inline");
			toggle("." + id, ".explain");
			$("." + id + " .explainDesc").removeClass("hide");
		}
		return false;
	}, 500);
}

var timeoutHover = setTimeout(function () { }, 0);
var hoverPersist = false;
var positionMouseMenu = "out";

function activateHoverMenu() {
	//mylog.log("enter all");
	positionMouseMenu = "in";
	$(themeObj.mainContainer).animate({ opacity: 0.3 }, 400);
	$(".lbl-btn-menu-name").show(200);
	$(".lbl-btn-menu-name").css("display", "inline");
	$(".menu-button-title").addClass("large");

	showInputCommunexion();

	hoverPersist = false;
	clearTimeout(timeoutHover);
	timeoutHover = setTimeout(function () {
		hoverPersist = true;
	}, 1000);
}





var selection;
function bindHighlighter() {
	//mylog.clear();  
	mylog.log("bindHighlighter");
	mylog.dir(window.getSelection());
	$(".my-main-container").bind('mouseup', function (e) {
		if (window.getSelection) {
			selection = window.getSelection();
		} else if (document.selection) {
			selection = document.selection.createRange();
		}
		selTxt = selection.toString();
		if (selTxt) {
			//alert(selTxt);
			/*
			if($(".selBtn").length)
				$(".selBtn").remove();
			links = "<a href='javascript:;' onclick='fastAdd(\"/rooms/fastaddaction\")' class='selBtn text-bold btn btn-purple btn-xs'><i class='fa fa-cogs'></i> créer en action <i class='fa fa-plus'></i></a>"+
					" <a href='javascript:;'  onclick='fastAdd(\"/survey/fastaddentry\")' class='selBtn text-bold btn btn-purple btn-xs'><i class='fa fa-archive'></i> créer en proposition <i class='fa fa-plus'></i></a>";

			$(this).parent().find("div.bar_tools_post").append(links);
			*/
		}
	});
}

function bindTags() {
	mylog.log("bindTags");
	//var tagClasses = ".tag,.label tag_item_map_list"
	$(".tag,.label tag_item_map_list").off().on('click', function (e) {
		//if(userId){
		var tag = ($(this).data("val")) ? $(this).data("val") : $(this).html();
		//alert(tag);
		//showTagInMultitag(tag);
		//$('#btn-modal-multi-tag').trigger("click");
		//$('.tags-count').html( $(".item-tag-name").length );
		if (addTagToMultitag(tag))
			toastr.success("Le tag \"" + tag + "\" ajouté à vos tags préférés");
		else
			toastr.info("Le tag \"" + tag + "\" est déjà dans vos tags");

		//} else {
		//	toastr.error("must be loggued");
		//}
	});
}

function bindExplainLinks() {
	mylog.log("bindExplainLinks");
	$(".explainLink").click(function () {
		mylog.log("explainLink");
		showDefinition($(this).data("id"));
		return false;
	});
}


function mouseX(evt) {
	if (evt.pageX) {
		return evt.pageX;
	} else if (evt.clientX) {
		return evt.clientX + (document.documentElement.scrollLeft ?
			document.documentElement.scrollLeft :
			document.body.scrollLeft);
	} else {
		return null;
	}
}

function mouseY(evt) {
	if (evt.pageY) {
		return evt.pageY;
	} else if (evt.clientY) {
		return evt.clientY + (document.documentElement.scrollTop ?
			document.documentElement.scrollTop :
			document.body.scrollTop);
	} else {
		return null;
	}
}

/* DEPRACATED NEWS ENGINE => OLD
function reloadNewsSearch(){
	if(location.hash.indexOf("#default.live")==0)
		startSearch(false);
	else{
		dateLimit = 0;
		loadStream(0, 5);
	}
}
function bindRefreshBtns() { mylog.log("bindRefreshBtns");
	if( $("#dropdown_search").length || $(".newsTL").length)
	{
		var searchFeed = "#dropdown_search";
		var method = "startSearch(0, indexStepInit);"
		if( $(".newsTL").length){
			searchFeed = ".newsTL";
			method = "reloadNewsSearch();"
		}
		$('#scopeListContainer .item-scope-checker, #scopeListContainer .item-tag-checker, .btn-filter-type').click(function(e){
			  //mylog.warn( ">>>>>>>",$(this).data("scope-value"), $(this).data("tag-value"), $(this).attr("type"));
			  var str = getFooterScopeChanged(method);
			  if(location.hash.indexOf("#news.index")==0 || location.hash.indexOf("#city.detail")==0){  mylog.log("vide news stream perso");
				  $(".newsFeedNews, #backToTop, #footerDropdown").remove();
				  $(searchFeed).append( str );
			  }else { mylog.log("vide autre news stream perso", searchFeed);
				  $(searchFeed).html( str );
			  }
			  $(".search-loader").html("<i class='fa fa-ban'></i>");
		});
	}
}*/

function hideSearchResults() {
	var searchFeed = "#dropdown_search";
	var method = "startSearch(0, indexStepInit);"
	if ($(".newsTL").length) {
		searchFeed = ".newsTL";
		method = "reloadNewsSearch();"
	}
	//mylog.warn( ">>>>>>>",$(this).data("scope-value"), $(this).data("tag-value"), $(this).attr("type"));
	str = getFooterScopeChanged(method);
	if (location.hash.indexOf("#news.index") == 0 || location.hash.indexOf("#city.detail") == 0) {
		mylog.log("vide news stream perso");
		$(".newsFeedNews, #backToTop, #footerDropdown").remove();
		$(searchFeed).append(str);
	} else {
		mylog.log("vide autre news stream perso", searchFeed);
		$(searchFeed).html(str);
	}
	$(".search-loader").html("<i class='fa fa-ban'></i>");

}
function getFooterScopeChanged(method) {
	var str = '<div class="padding-5 text-center" id="footerDropdown">';
	//str += 		"<hr style='float:left; width:100%;'/>"
	str += '<button class="btn btn-default" onclick="' + method + '"><i class="fa fa-refresh"></i> Relancer la recherche</button>' +
		"<span style='' class='text-dark padding-10'><i class='fa fa-angle-right'></i> Les critères ont changés</span><br/>";
	str += "</div>";
	return str;
}


/* **************************************
maybe movebale into Element.js
***************************************** */

function buildQRCode(type, id) {

	$(".qrCode").qrcode({
		text: baseUrl + "/#" + dyFInputs.get(type).ctrl + ".detail.id." + id,//'{type:"'+type+'",_id:"'+id+'"}',
		render: 'image',
		minVersion: 8,
		maxVersion: 40,
		ecLevel: 'L',
		size: 150,
		radius: 0,
		quiet: 2,
		/*mode: 2,
		mSize: 0.1,
		mPosX: 0.5,
		mPosY: 0.5,

		label: name,
		fontname: 'Ubuntu',
		fontcolor: '#E33551',*/
	});
}

function activateSummernote(elem) {

	if (!$('script[src="' + baseUrl + '/plugins/summernote/dist/summernote.min.js"]').length) {
		$("<link/>", {
			rel: "stylesheet",
			type: "text/css",
			href: baseUrl + "/plugins/summernote/dist/summernote.css"
		}).appendTo("head");
		$.getScript(baseUrl + "/plugins/summernote/dist/summernote.min.js", function (data, textStatus, jqxhr) {
			//mylog.log( data ); // Data returned
			//mylog.log( textStatus ); // Success
			//mylog.log( jqxhr.status ); // 200
			//mylog.log( "Load was performed." );

			$(".btnEditAdv").hide();
			$(elem).summernote({
				toolbar: [
					['style', ['bold', 'italic', 'underline', 'clear']],
					['color', ['color']],
					['para', ['ul', 'ol', 'paragraph']],
				]
			});
		});
	} else {
		$(".btnEditAdv").hide();
		$(elem).summernote({
			toolbar: [
				['style', ['bold', 'italic', 'underline', 'clear']],
				['color', ['color']],
				['para', ['ul', 'ol', 'paragraph']],
			]
		});
	}
}


function firstOptions() {
	var res = {
		"dontKnow": tradDynForm["dontknow"],
	};
	res[userId] = tradDynForm["me"];
	return res;
}

function myAdminList(ctypes) {
	mylog.log("myAdminList", ctypes);
	var myList = {};
	if (userId) {
		//types in MyContacts
		var connectionTypes = {
			organizations: "members",
			projects: "contributors",
			events: "attendees"
		};
		$.each(ctypes, function (i, ctype) {
			var connectionType = connectionTypes[ctype];
			myList[ctype] = { label: ctype, options: {} };
			if (notNull(myContacts)) {
				mylog.log("myAdminList", ctype, connectionType, myContacts, myContacts[ctype]);
				$.each(myContacts[ctype], function (id, elemObj) {
					mylog.log("myAdminList", ctype, id, elemObj.name);
					if (elemObj.links && elemObj.links[connectionType] && elemObj.links[connectionType][userId] && elemObj.links[connectionType][userId].isAdmin) {
						mylog.warn("myAdminList2", ctype + "-" + id + "-" + elemObj.name, elemObj["_id"]["$id"]);
						myList[ctype]["options"][elemObj["_id"]["$id"]] = elemObj.name;
						mylog.log(myList);
					}
				});
			}
		});
		mylog.log("myAdminList return", myList);
	}
	return myList;
}

function parentList(ctypes, parentId, parentType) {
	mylog.log("parentList", ctypes, parentId, parentType);
	var myList = myAdminList(ctypes);

	mylog.log("parentList myList", myList);
	if (notEmpty(parentId) && notEmpty(parentType) &&
		notEmpty(myList) &&
		(!notEmpty(myList[parentType]) ||
			(notEmpty(myList[parentType]) && !notEmpty(myList[parentType]["options"][parentId])))) {

		if (!notEmpty(myList[parentType]))
			myList[parentType] = { label: parentType, options: {} };
		myList[parentType]["options"][parentId] = ((typeof contextData.parent != "undefined" && typeof contextData.parent.name != "undefined") ? contextData.parent.name : tradDynForm["dontknow"]);
	}
	return myList;
}


function escapeHtml(string) {
	var entityMap = {
		'"': '&quot;',
		"'": '&#39;',
	};
	return String(string).replace(/["']/g, function (s) {
		return entityMap[s];
	});
}

function fillContactFields(id) {
	name = cotmp[id].name;
	mylog.log("fillContactFields", id, name);
	$("#idContact").val(id);
	$("#listSameName").html("<i class='fa fa-check text-success'></i> Vous avez sélectionner : " + escapeHtml(name));
	$("#name").val(name);
}
var cotmp = {};


function activeMenuElement(page) {
	mylog.log("-----------------activeMenuElement----------------------");
	listBtnMenu = ['detail', 'news', 'directory', 'gallery', 'addmembers', 'calendar'];
	$.each(listBtnMenu, function (i, value) {
		$(".btn-menu-element-" + value).removeClass("active");
	});
	$(".btn-menu-element-" + page).addClass("active");
}

function shadowOnHeader() {
	var y = $(".my-main-container").scrollTop();
	if (y > 0) { $('.main-top-menu').addClass('shadow'); }////NOTIFICATIONS}
	else { $('.main-top-menu').removeClass('shadow'); }
}



function myContactLabel(type, id) {
	if (typeof myContacts != "undefined" && myContacts[type]) {
		$.each(myContacts[type], function (key, val) {
			if (id == val["_id"]["$id"]) {
				return val;
			}
		});
	}
	return null;
}

function autoCompleteInviteSearch(search) {
	mylog.log("autoCompleteInviteSearch2", search);
	if (search.length < 3) { return }
	tabObject = [];

	var data = {
		"search": search,
		"searchMode": "personOnly"
	};

	ajaxPost("", moduleId + '/search/searchmemberautocomplete', data,
		function (data) {
			mylog.log(data);
			var str = "<li class='li-dropdown-scope'><a href='javascript:newInvitation()'>Pas trouvé ? Lancer une invitation à rejoindre votre réseau !</li>";
			var compt = 0;
			var city, postalCode = "";
			if (data["citoyens"].length > 0) {
				$.each(data["citoyens"], function (k, v) {
					city = "";
					mylog.log(v);
					postalCode = "";
					var htmlIco = "<i class='fa fa-user fa-2x'></i>"
					if (v.id != userId) {
						tabObject.push(v);
						if (v.profilImageUrl != "") {
							var htmlIco = "<img width='50' height='50' alt='image' class='img-circle' src='" + baseUrl + v.profilImageUrl + "'/>"
						}
						if (v.address != null) {
							city = v.address.addressLocality;
							postalCode = v.address.postalCode;
						}
						str += "<li class='li-dropdown-scope'>" +
							"<a href='javascript:setInviteInput(" + compt + ")'>" + htmlIco + " " + v.name;

						if (typeof postalCode != "undefined")
							str += "<br/>" + postalCode + " " + city;
						//str += "<span class='city-search'> "+postalCode+" "+city+"</span>" ;
						str += "</a></li>";

						compt++;
					}
				});
			}

			$("#ajaxFormModal #dropdown_searchInvite").html(str);
			$("#ajaxFormModal #dropdown_searchInvite").css({ "display": "inline" });
		}
	);
}

function newInvitation() {
	$("#ajaxFormModal #step1").css({ "display": "none" });
	$("#ajaxFormModal #step3").css({ "display": "block" });

	$('#ajaxFormModal #inviteId').val("");
	var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
	if (emailReg.test($("#ajaxFormModal #inviteSearch").val())) {
		$('#ajaxFormModal #inviteEmail').val($("#ajaxFormModal #inviteSearch").val());
		$("#ajaxFormModal #inviteName").val("");
	} else {
		$("#ajaxFormModal #inviteName").val($("#ajaxFormModal #inviteSearch").val());
		$("#ajaxFormModal #inviteEmail").val("");
	}
	$("#inviteText").val('');
}

function communecterUser() {
	mylog.warn("communecterUser");
	if (typeof contextData == "undefined" || contextData == null || contextData.id != userId) {
		contextData = {
			id: userId,
			type: "citoyens"
		};
	}
	$.unblockUI();
	updateLocalityEntities();
}

function updateLocalityEntities(addressesIndex, addressesLocality) {
	mylog.log("updateLocalityEntities", addressesIndex, addressesLocality);


	var data = {
		address: contextData.address,
		geo: contextData.geo

	};
	// if(addressesLocality && addressesIndex){
	// 	locality.address = addressesLocality.address ;
	// 	locality.geo = addressesLocality.geo ;
	// }else if(addressesIndex) {
	// 	locality.address = null ;
	// 	locality.geo = null ;
	// 	locality = null;
	// }

	mylog.log("updateLocalityEntities data", data);
	var form = {
		saveUrl: baseUrl + "/" + moduleId + "/element/updateblock/",
		dynForm: {
			jsonSchema: {
				title: trad["Update Location"],
				icon: "fa-key",
				onLoads: {
					initUpdateWhen: function () {
						mylog.log("initUpdateInfo");
						$("#ajax-modal .modal-header").removeClass("bg-purple bg-red bg-azure bg-green bg-green-poi bg-orange bg-yellow bg-blue bg-turq bg-url")
							.addClass("bg-dark");
					}
				},
				beforeSave: function () {

				}, afterSave: function (data) {

					dyFObj.closeForm();
					urlCtrl.loadByHash(location.hash);
					// window.location.reload();
				},
				properties: {
					block: dyFInputs.inputHidden(),
					typeElement: dyFInputs.inputHidden(),
					isUpdate: dyFInputs.inputHidden(true),
					formLocality: dyFInputs.formLocality(tradDynForm.addLocality, tradDynForm.addLocality, null, data),
					location: dyFInputs.location,
				}
			}
		}
	};

	var dataUpdate = {
		block: "localities",
		id: contextData.id,
		typeElement: contextData.type,
		address: contextData.address,
		geo: contextData.geo,
		geoPosition: contextData.geoPosition,
		addresses: contextData.addresses,
	};

	dyFObj.openForm(form, "initUpdateWhen", dataUpdate);

}

function cityKeyPart(unikey, part) {
	var s = unikey.indexOf("_");
	var e = unikey.indexOf("-");
	var len = unikey.length;
	if (e < 0) e = len;
	if (part == "insee") return unikey.substr(s + 1, e - s - 1);
	if (part == "cp" && unikey.indexOf("-") < 0) return "";
	if (part == "cp") return unikey.substr(e + 1, len);
	if (part == "country") return unikey.substr(e + 1, len);
}

var list = {
	initList: function (dataList, action, subType) {
		var viewList = "";
		$.each(dataList, function (e, v) {
			if (action == "backup") {
				if (e == "services") {
					$.each(v, function (i, data) {
						$.each(data, function (key, service) {
							viewList += list.getListOf(key, service, action);
						});

					});
				} else if (e == "products") {
					$.each(v, function (i, data) {

						viewList += list.getListOf(e, data, action);
					});
				}
			}
			else {
				if (action != "history")
					viewList += "<h4 class='listSubtitle col-md-12 col-sm-12 col-xs-12 letter-orange'>" + Object.keys(v).length + " " + e + "</h4>";
				$.each(v, function (i, data) {

					viewList += list.getListOf(e, data, action);
				});
			}
			$("#listList").html(viewList);
		});
		if (action == "history") {
			$(".orderItemComment").click(function () {
				orderItem = listComponents["orderItems"][$(this).data("id")];
				commentRating(orderItem, $(this).data("action"));
			});
		}
	},
	getListOf: function (type, data, action) {
		data.imgProfil = "";
		btnAction = "";
		if (action == "manage")
			btnAction = "<a href='#page.type." + type + ".id." + data._id.$id + "' class='lbh btn bg-orange linkBtnList'>Manage it</a>";
		else if (action == "history") {
			btnAction = "save";
			labelAction = trad["Leave your comment"];
			if (typeof data.comment != "undefined") {
				btnAction = "show";
				labelAction = "Show your comment";
			}
			btnAction = "<button class='btn btn-link bg-green-k orderItemComment linkBtnList' data-id='" + data._id.$id + "' data-action='" + btnAction + "'>" +
				labelAction +
				"</button>";
		}
		if (!data.useMinSize)
			data.imgProfil = "<i class='fa fa-image fa-3x'></i>";
		if ("undefined" != typeof data.profilMediumImageUrl && data.profilMediumImageUrl != "")
			data.imgProfil = "<img class='img-responsive' src='" + baseUrl + data.profilMediumImageUrl + "'/>";
		str = "<div class='col-md-12 col-sm-12 contentListItem padding-5'>" +
			"<div class='col-md-2 col-sm-2 contentImg text-center no-padding'>" +
			data.imgProfil +
			"</div>" +
			"<div class='col-md-10 col-sm-10 listItemInfo'>" +
			"<div class='col-md-10 col-sm-10'>" +
			"<h4>" + data.name + "</h4>" +
			"<span>Price: " + data.price + "</span><br/>";
		if (action == "backup") {
			str += "<span>Quantity: " + data.countQuantity + "</span>";
		}
		if (action == "manage" && typeof data.toBeValidated != "undefined")
			str += "<i class='text-azul'>Waiting for validation</i>";
		str += "</div>" +
			"<div class='col-md-2 col-sm-2'>" +

			"</div>" +
			"</div>" +
			btnAction +
			"</div>";
		if (action == "history") {
			str += "<div id='content-comment-" + data._id.$id + "' class='col-xs-12 no-padding contentRatingComment'></div>";
		}
		if (action == "backup") {
			if (typeof data.reservations != "undefined") {
				str += "<div class='col-md-12 col-sm-12 col-xs-12'>";
				$.each(data.reservations, function (date, value) {
					s = (value.countQuantity > 1) ? "s" : "";
					dateStr = directory.getDateFormated({ startDate: date }, true);
					str += "<div class='col-md-12 col-sm-12 col-xs-12 bookDate margin-bottom-10'>" +
						"<div class='col-md-12 col-sm-12 col-xs-12 dateHeader'>" +
						"<h4 class='pull-left margin-bottom-5 no-margin col-md-5 col-sm-5 col-xs-5 no-padding'><i class='fa fa-calendar'></i> " + dateStr + "</h4>";
					incQtt = "";
					if (typeof value.hours == "undefined")
						incQtt = value.countQuantity + " reservation" + s;
					str += "<span class='pull-left text-center col-md-3 col-sm-3 col-xs-3'>" + incQtt + "</span>" +
						"</div>";

					if (typeof value.hours != "undefined") {
						$.each(value.hours, function (key, hours) {
							s = (hours.countQuantity > 1) ? "s" : "";
							incQtt = hours.countQuantity + " reservation" + s;
							str += "<div class='col-md-12 col-sm-12 col-xs-12 margin-bottom-5 padding-5 contentHoursSession'>" +
								"<h4 class='col-md-4 col-sm-4 col-xs-3 no-padding no-margin'><i class='fa fa-clock-o'></i> " + hours.start + " - " + hours.end + "</h4>" +
								"<span class='col-md-5 col-sm-5 col-xs-6 text-center'>" + incQtt + "</span>" +
								"</div>";
						});
					}
					str += "</div>";
				});
				str += "</div>";
			}
		}
		return str;
	}
}
/* *********************************
			COLLECTIONS
********************************** */

var collection = {
	crud: function (action, name, type, id) {
		if (userId) {
			var params = {};
			var sure = true;
			if (typeof type != "undefined")
				params.type = type;
			if (typeof id != "undefined")
				params.id = id;
			if (typeof action == "undefined")
				action = "new";
			if (action == "del") {
				params.name = name;
				sure = confirm("Vous êtes sûr ?");
			}
			else if (action == "new" || action == "update")
				params.name = prompt(tradDynForm.collectionname + ' ?', name);
			if (action == "update")
				params.oldname = name;

			if (sure) {
				ajaxPost(null, baseUrl + "/" + moduleId + "/collections/crud/action/" + action, params, function (data) {
					console.warn(params.action);
					if (data.result) {
						toastr.success(data.msg);
						if (location.hash.indexOf("#page") >= 0) {
							loadDataDirectory("collections", "star");
						}
						//if no type defined we are on user
						//TODO : else add on the contextMap
						if (typeof type == "undefined" && action == "new") {
							if (!userConnected.collections)
								userConnected.collections = {};
							if (!userConnected.collections[params.name])
								userConnected.collections[params.name] = {};
						} else if (action == "update") {
							smallMenu.openAjax(baseUrl + '/' + moduleId + '/collections/list/col/' + params.name, params.name, 'fa-folder-open', 'yellow');
							if (!userConnected.collections[params.name])
								userConnected.collections[params.name] = userConnected.collections[params.oldname];
							delete userConnected.collections[params.oldname];
						} else if (action == "del") {
							delete userConnected.collections[params.name];
							smallMenu.open();
						}
						collection.buildCollectionList("col_Link_Label_Count", ".menuSmallBtns", function () { $(".collection").remove() })
					}
					else
						toastr.error(data.msg);
				}, "none");
			}
		} else
			toastr.error(trad.LoginFirst);
	},
	applyColor: function (what, id, col) {
		var collection = (typeof col == "undefined") ? "favorites" : col;
		//console.log("applyColor",what,id)
		if (userConnected && userConnected.collections && userConnected.collections[collection] && userConnected.collections[collection][what] && userConnected.collections[collection][what][id]) {
			$(".star_" + what + "_" + id).children("i").removeClass("fa-star-o").addClass('fa-star text-red');
			//console.warn("applying Color",what,id)
		}
	},
	isFavorites: function (type, id) {
		res = false;
		if (userConnected && userConnected.collections) {
			$.each(userConnected.collections, function (name, listCol) {
				if (typeof listCol[type] != "undefined" && typeof listCol[type][id] != "undefined") {
					res = name;
					return false;
				}
			});
		}
		return res;
	},
	add2fav: function (what, id, col) {
		var collection = (typeof col == "undefined") ? "favorites" : col;
		if (userId) {
			var params = { id: id, type: what, collection: collection };
			var el = ".star_" + what + "_" + id;

			ajaxPost(null, baseUrl + "/" + moduleId + "/collections/add", params, function (data) {
				console.warn(params.action, collection, what, id);
				if (data.result) {
					if (data.list == '$unset') {
						/*if(location.hash.indexOf("#page") >=0){
							if(location.hash.indexOf("view.directory.dir.collections") >=0 && contextData.id==userId){ 
								loadDataDirectory("collections", "star"); 
								}else{ 
								$(".favorisMenu").removeClass("text-yellow"); 
								$(".favorisMenu").children("i").removeClass("fa-star").addClass('fa-star-o'); 
								} 
						}else{*/
						$(el).removeClass("letter-yellow-k");
						$(el).find("i").removeClass("fa-star letter-yellow-k").addClass('fa-star-o');
						delete userConnected.collections[collection][what][id];
						//}
					}
					else {
						/*if(location.hash.indexOf("#page") >=0){
							if(location.hash.indexOf("view.directory.dir.collections") >=0 && contextData.id==userId){ 
								loadDataDirectory("collections", "star"); 
								}else{ 
								$(".favorisMenu").addClass("text-yellow"); 
								$(".favorisMenu").children("i").removeClass("fa-star-o").addClass('fa-star'); 
								}
							}
						else*/
						$(el).addClass("letter-yellow-k");
						$(el).find("i").removeClass("fa-star-o").addClass('fa-star letter-yellow-k');

						if (!userConnected.collections)
							userConnected.collections = {};
						if (!userConnected.collections[collection])
							userConnected.collections[collection] = {};
						if (!userConnected.collections[collection][what])
							userConnected.collections[collection][what] = {};
						userConnected.collections[collection][what][id] = new Date();
					}
					toastr.success(data.msg);
				}
				else
					toastr.error(data.msg);
			}, "none");
		} else
			toastr.error(trad.LoginFirst);
	},
	buildCollectionList: function (tpl, appendTo, reset) {
		if (typeof reset == "function")
			reset();
		str = "";
		$.each(userConnected.collections, function (col, list) {
			var colcount = 0;
			$.each(list, function (type, entries) {
				colcount += Object.keys(entries).length;
			});
			str += js_templates[tpl]({
				label: col,
				labelCount: colcount
			});
		});
		$(appendTo).append(str);
	}
};

/* *********************************
			DYNFORM SPEC TYPE OBJ
********************************** */
var contextData = null;
var dynForm = null;
var mentionsInput = [];
var mentionsInit = {
	stopMention: false,
	isSearching: false,
	get: function (domElement) {
		mentionsInput = [];
		$(domElement).mentionsInput({
			allowRepeat: true,
			onDataRequest: function (mode, query, callback) {
				if (!mentionsInit.stopMention) {
					var data = mentionsContact;
					data = _.filter(data, function (item) { return item.name.toLowerCase().indexOf(query.toLowerCase()) > -1 });
					callback.call(this, data);
					mentionsInit.isSearching = true;
					var search = { "searchType": ["citoyens", "organizations", "projects"], "name": query };
					ajaxPost(
						null,
						baseUrl + "/" + moduleId + "/search/globalautocomplete",
						search,
						function (retdata) {
							if (!retdata) {
								toastr.error(retdata.content);
							} else {
								mylog.log(retdata);
								data = [];
								$.each(retdata.results, function (e, value) {
									avatar = "";
									if (typeof value.profilThumbImageUrl != "undefined" && value.profilThumbImageUrl != "")
										avatar = baseUrl + value.profilThumbImageUrl;
									object = new Object;
									object.id = e;
									object.name = value.name;
									object.slug = value.slug;
									object.avatar = avatar;
									object.type = value.type;
									var findInLocal = _.findWhere(mentionsContact, {
										name: value.name,
										type: value.type
									});
									if (typeof (findInLocal) == "undefined") {
										mentionsContact.push(object);
									}
								});
								data = mentionsContact;
								data = _.filter(data, function (item) { return item.name.toLowerCase().indexOf(query.toLowerCase()) > -1 });
								callback.call(this, data);
								mylog.log("callback", callback);
							}
						}
					);
				}
			}
		});
	},
	beforeSave: function (object, domElement) {
		$(domElement).mentionsInput('getMentions', function (data) {
			mentionsInput = data;
		});
		if (typeof mentionsInput != "undefined" && mentionsInput.length != 0) {
			var textMention = "";
			$(domElement).mentionsInput('val', function (text) {
				textMention = text;

				$.each(mentionsInput, function (e, v) {
					strRep = v.name;
					while (textMention.indexOf("@[" + v.name + "](" + v.type + ":" + v.id + ")") > -1) {
						if (typeof v.slug != "undefined")
							strRep = "@" + v.slug;
						textMention = textMention.replace("@[" + v.name + "](" + v.type + ":" + v.id + ")", strRep);
					}
				});
			});
			object.mentions = mentionsInput;
			object.text = textMention;
		}
		return object;
	},
	addMentionInText: function (text, mentions) {
		$.each(mentions, function (index, value) {
			if (typeof value.slug != "undefined") {
				while (text.indexOf("@" + value.slug) > -1) {
					str = "<span class='lbh' onclick='urlCtrl.loadByHash(\"#page.type." + value.type + ".id." + value.id + "\")' onmouseover='$(this).addClass(\"text-blue\");this.style.cursor=\"pointer\";' onmouseout='$(this).removeClass(\"text-blue\");' style='color: #719FAB;'>" +
						value.name +
						"</span>";
					text = text.replace("@" + value.slug, str);
				}
			} else {
				//Working on old news
				array = text.split(value.value);
				text = array[0] +
					"<span class='lbh' onclick='urlCtrl.loadByHash(\"#page.type." + value.type + ".id." + value.id + "\")' onmouseover='$(this).addClass(\"text-blue\");this.style.cursor=\"pointer\";' onmouseout='$(this).removeClass(\"text-blue\");' style='color: #719FAB;'>" +
					value.name +
					"</span>" +
					array[1];
			}
		});
		return text;
	},
	reset: function (domElement) {
		$(domElement).mentionsInput('reset');
	}
}


var documents = {
	objFile: {
		"pdf": { icon: "file-pdf-o", class: "text-red" },
		"text": { icon: "file-text-o", class: "text-blue" },
		"presentation": { icon: "file-powerpoint-o", class: "text-orange" },
		"spreadsheet": { icon: "file-excel-o", class: "text-green" }
	},
	getIcon: function (contentKey, name) {
		if (documents.objFile[contentKey] && documents.objFile[contentKey].icon && documents.objFile[contentKey].class)
			return '<i class="fa fa-' + documents.objFile[contentKey].icon + ' ' + documents.objFile[contentKey].class + '"></i>';
		else if (notNull(name) && documents.objFile[name.split('.')[1]] && documents.objFile[name.split('.')[1]].icon && documents.objFile[name.split('.')[1]].class) {
			return '<i class="fa fa-' + documents.objFile[name.split('.')[1]].icon + ' ' + documents.objFile[name.split('.')[1]].class + '"></i>';
		} else
			return "";
	}
}
/* ************************************
Keyboard Shortcuts
*************************************** */
var keyboardNav = {
	keycodeObj: {
		"backspace": 8, "tab": 9, "enter": 13, "shift": 16, "ctrl": 17, "alt": 18, "pause/break": 19, "capslock": 20, "escape": 27, "pageup": 33, "pagedown": 34, "end": 35,
		"home": 36, "left": 37, "up": 38, "right": 39, "down": 40, "insert": 45, "delete": 46, "0": 48, "1": 49, "2": 50, "3": 51, "4": 52, "5": 53, "6": 54, "7": 55, "8": 56, "9": 57,
		"a": 65, "b": 66, "c": 67, "d": 68, "e": 69, "f": 70, "g": 71, "h": 72, "i": 73, "j": 74, "k": 75, "l": 76, "m": 77, "n": 78, "o": 79, "p": 80, "q": 81, "r": 82, "s": 83, "t": 84, "u": 85, "v": 86, "w": 87,
		"x": 88, "y": 89, "z": 90, "left window key": 91, "right window key": 92, "select key": 93, "numpad 0": 96, "numpad 1": 97, "numpad 2": 98, "numpad 3": 99, "numpad 4": 100, "numpad 5": 101,
		"numpad 6": 102, "numpad 7": 103, "numpad 8": 104, "numpad 9": 105, "multiply": 106, "add": 107, "subtract": 109, "decimal point": 110, "divide": 111, "f1": 112, "f2": 113, "f3": 114,
		"f4": 115, "f5": 116, "f6": 117, "f7": 118, "f8": 119, "f9": 120, "f10": 121, "f11": 122, "f12": 123, "num lock": 144, "scroll lock": 145, "semi-colon": 186, "equal sign": 187,
		"comma": 188, "dash": 189, "period": 190, "forward slash": 191, "grave accent": 192, "open bracket": 219, "back slash": 220, "close braket": 221, "single quote": 222
	},

	keyMap: {
		//"112" : function(){ $('#modalMainMenu').modal("show"); },//f1
		"113": function () { if (userId) urlCtrl.loadByHash('#person.detail.id.' + userId); else alert("login first"); },//f2
		"114": function () { $('#openModal').modal('hide'); showMap(true); },//f3
		//"115" : function(){ dyFObj.openForm('themes') },//f4
		"117": function () { console.clear(); urlCtrl.loadByHash(location.hash) },//f6
	},
	keyMapCombo: {
		"13": function () {
			$('#openModal').modal('hide'); $('#selectCreate').modal('show');//dyFObj.openForm('addElement')
		},//enter : add elements
		"61": function () { $('#openModal').modal('hide'); $('#selectCreate').modal('show') },//= : add elements
		"65": function () { $('#openModal').modal('hide'); dyFObj.openForm('action') },//a : actions
		"66": function () { $('#openModal').modal('hide'); smallMenu.destination = "#openModal"; smallMenu.openAjax(baseUrl + '/' + moduleId + '/collections/list', 'Mes Favoris', 'fa-star', 'yellow') },//b best : favoris
		"67": function () { $('#openModal').modal('hide'); dyFObj.openForm('classified') },//c : classified
		"69": function () { $('#openModal').modal('hide'); dyFObj.openForm('event') }, //e : event
		"70": function () { $('#openModal').modal('hide'); $(".searchIcon").trigger("click") },//f : find
		"71": function () { co.graph() },//g : graph
		"72": function () { smallMenu.openAjaxHTML(baseUrl + '/' + moduleId + '/default/view/page/help') },//h : help
		"73": function () { $('#openModal').modal('hide'); dyFObj.openForm('person') },//i : invite
		"76": function () { smallMenu.openAjaxHTML(baseUrl + '/' + moduleId + '/default/view/page/links') },//l : links and infos
		"79": function () { $('#openModal').modal('hide'); dyFObj.openForm('organization') },//o : orga
		"80": function () { $('#openModal').modal('hide'); dyFObj.openForm('project') },//p : project
		"81": function () { co.finder() }, //q : finder
		"82": function () { $('#openModal').modal('hide'); smallMenu.openAjax(baseUrl + '/' + moduleId + '/person/directory?tpl=json', 'Mon répertoire', 'fa-book', 'red') },//r : annuaire
		"86": function () { $('#openModal').modal('hide'); dyFObj.openForm('entry') },//v : votes
	},
	checkKeycode: function (e) {
		e.preventDefault();
		var keycode;
		if (window.event) { keycode = window.event.keyCode; e = event; }
		else if (e) { keycode = e.which; }


		if (e.ctrlKey && e.altKey && notNull(keyboardNav.keyMapCombo) && keyboardNav.keyMapCombo[keycode]) {
			console.warn("keyMapCombo", keycode);//shiftKey ctrlKey altKey
			keyboardNav.keyMapCombo[keycode]();
		}
		else if (keyboardNav.keyMap[keycode]) {
			console.warn("keyMap", keycode);
			keyboardNav.keyMap[keycode]();
		}
	}
}

//*********************************************************************************
// Utility for events date
//*********************************************************************************
function manageTimestampOnDate() {
	$.each($(".date2format"), function (k, v) {
		var dates = "";
		var dateFormat = "DD-MM-YYYY HH:mm";
		if ($(this).data("allday") == true) {
			dateFormat = "DD-MM-YYYY";
		}
		dates = moment($(this).data("startdate")).local().format(dateFormat);
		dates += "</br>" + moment($(this).data("enddate")).local().format(dateFormat);
		$(this).html(dates);
	})
}

//Display event start and end date depending on allDay params
//Used on popup and right list on map
function displayStartAndEndDate(event) {
	var content = "";
	//si on a bien les dates
	mylog.log("event map", event);
	if ("undefined" != typeof event['startDateDB'] && "undefined" != typeof event['endDateDB']) {
		//var start = dateToStr(data['startDate'], "fr", true);
		//var end = dateToStr(data['endDate'], "fr", true);

		var startDateMoment = moment(event['startDateDB']).local();
		var endDateMoment = moment(event['endDateDB']).local();

		var startDate = startDateMoment.format("DD-MM-YYYY");
		var endDate = endDateMoment.format("DD-MM-YYYY");

		var hour1 = "Toute la journée";
		var hour2 = "Toute la journée";
		if (event["allDay"] == false || event["allDay"] == null) {
			hour1 = startDateMoment.format("HH:mm");
			hour2 = endDateMoment.format("HH:mm");
		}
		//si la date de debut == la date de fin
		if (startDate == endDate) {
			content += "<div class='info_item startDate_item_map_list double'><i class='fa fa-caret-right'></i> Le " + startDate;

			if (event["allDay"] == true) {
				content += "</br><i class='fa fa-caret-right'></i> " + hour1;
			} else {
				content += "</br><i class='fa fa-caret-right'></i> " + hour1 + " - " + hour2;
			}
			content += "</div>";
		} else {
			content += "<div class='info_item startDate_item_map_list double'><i class='fa fa-caret-right'></i> Du " +
				startDate + " - " + hour1 +
				"</div>" +
				"<div class='info_item startDate_item_map_list double'><i class='fa fa-caret-right'></i> Au " +
				endDate + " - " + hour2 +
				"</div></br>";
		}
	}
	return content;
}

//*********************************************************************************
// JS Template
//*********************************************************************************
var js_templates = {
	objectify: function (obj) {
		var tplObj = { label: obj.label };
		tplObj.lblCount = (notNull(obj.labelCount)) ? ' <span class="labelCount">(' + obj.labelCount + ')</span>' : '';
		tplObj.action = (notNull(obj.action)) ? obj.action : 'javascript:smallMenu.openAjax(\'' + baseUrl + '/' + moduleId + '/collections/list/col/' + obj.label + '\',\'' + obj.label + '\',\'fa-folder-open\',\'yellow\'})';
		tplObj.icon = (notNull(obj.icon)) ? obj.icon : "fa-question-circle-o";
		tplObj.classes = (notNull(obj.classes)) ? obj.classes : "";
		tplObj.parentClass = (notNull(obj.parentClass)) ? obj.parentClass : "";
		tplObj.key = (notNull(obj.key)) ? ' data-key="' + obj.key + '"' : "";
		tplObj.color = (notNull(obj.color)) ? obj.color : "white";
		tplObj.tooltip = (notNull(obj.tooltip)) ? 'data-toggle="tooltip" data-placement="left" title="' + tooltip + '"' : "";
		return tplObj;
	},

	//params :
	//obj : 
	//classes :: applies a class on each rendered element
	//open / close :: is a globale container
	//el_open/el_close :: is a container for each element of the list rendering
	loop: function (obj, tpl, tplparams) {
		var str = (notNull(tplparams) && notNull(tplparams.open)) ? tplparams.open : "";
		var cleanup = false;
		$.each(obj, function (k, v) {
			if (!notNull(v.classes) && notNull(tplparams) && notNull(tplparams.classes)) {
				v.classes = tplparams.classes;
				cleanup = true;
			}
			if (!notNull(v.parentClass) && notNull(tplparams) && notNull(tplparams.parentClass))
				v.parentClass = tplparams.parentClass;
			var opener = (notNull(tplparams) && notNull(tplparams.el_open)) ? tplparams.el_open : "";
			str += opener + js_templates[tpl](v);
			if (notNull(tplparams) && notNull(tplparams.el_close)) str += tplparams.el_close;
			if (cleanup)
				delete v.classes;
		});
		if (notNull(tplparams) && notNull(tplparams.close)) str += tplparams.close;
		return str;
	},

	col_Link_Label_Count: function (obj) {
		var tplObj = js_templates.objectify(obj);
		return ' <div class="' + tplObj.parentClass + ' center padding-5 ">' +
			'<a href="' + tplObj.action + '" ' +
			'class="' + tplObj.classes + ' btn tooltips text-' + tplObj.color + '" ' + tplObj.tooltip + ' ' + tplObj.key + '>' +
			'<i class="fa ' + tplObj.icon + ' text-' + tplObj.color + '"></i> ' +
			'<br/>' + tplObj.label + tplObj.lblCount +
			'</a>' +
			'</div>'
	},

	linkList: function (obj) {
		var tplObj = js_templates.objectify(obj);
		mylog.log("classes", tplObj.classes);
		return '<a href="' + tplObj.action + '" class="' + tplObj.classes + ' btn btn-xs btn-link text-white text-left w100p" ' + tplObj.key + '><i class="fa ' + tplObj.icon + '  text-' + tplObj.color + '"></i> ' + tplObj.label + tplObj.lblCount + '</a><br/>';
	},

	leftMenu_content: function (params) {
		//left menu section 
		var str = '<div class="menuSmallMenu"><div class="menuSmallLeftMenu col-sm-3 col-xs-12 center margin-top-15 margin-bottom-5">';
		str += js_templates.loop(params.menu, "linkList", { classes: "padding-5 bg-dark center  col-xs-12 ", el_open: '<div class="col-xs-12 center no-padding">', el_close: '</div>' });
		str += '</div>' +
			//right content section 
			'<div class="col-sm-9 col-xs-12 no-padding">';

		str += "<div class='homestead titleSmallMenu' style='font-size:45px'> " +
			params.title1 + ' <i class="fa fa-angle-right"></i> ' + params.title2 + "</div>";

		str += js_templates.loop(params.list, "col_Link_Label_Count", { classes: "bg-red kickerBtn", parentClass: "col-xs-12 col-sm-4 " });
		str += '</div></div>';
		return str;
	},

	album: function (obj) {
		isDoc = (obj.name.toLowerCase().indexOf(".pdf") > 0) ? true : false;
		target = (isDoc) ? " target='_blanck'" : "";
		str = ' <div class="col-xs-3 portfolio-item" id="' + obj.id + '">' +
			' <a class="thumb-info pull-left ' + obj.classes + '" ' + target + ' href="' + obj.path + '/' + obj.name + '" data-lightbox="all">';

		if (isDoc)
			str += '<i class="fa fa-file-text-o fa-x5"></i>';
		else
			str += ' <img src="' + obj.path + '/medium/' + obj.name + '" class="img-responsive" alt="' + obj.name + '">';

		str += ' </a>' +
			((notNull(userId) && obj.author == userId) ? ' <br/><a class="btnRemove" href="javascript:;" data-id="' + obj.id + '" data-key="" data-name="' + obj.name + '" ><i class="fa text-red fa-trash"></i> </a>' : '') +
			'</div>';
		return str;

	}

};
var documentManager = {
	render: function () {

	},
	bindEvents: function (callback) {
		$(".btn-remove-document").off().on("click", function () {
			documentManager.delete($(this).data("id"), callback);
		});
	},
	multipleDelete: function () {

	},
	delete: function (id, callback) {
		ajaxPost(
			null,
			baseUrl + "/" + moduleId + "/document/delete/id/" + id,
			{},
			function (data) {
				if (data.result) {
					if (notNull(callback))
						callback(data);
					else if ($("#" + id).length > 0) {
						toastr.success(data.msg);
						$("#" + id).remove();
					}
				} else {
					toastr.error(data.error)
				}
			}
		);
	}
}
//*********************************************************************************
// smallMenu Photo Albums
//*********************************************************************************
var album = {
	show: function (id, type) {
		uploadObj.set(type, id);
		getAjax(null, baseUrl + '/' + moduleId + "/document/list/id/" + id + "/type/" + type + "/tpl/json", function (data) {

			console.dir(data);
			smallMenu.build(
				data.list,
				function (params) {
					str = '<style>.thumb-info{height:200px; overflow: hidden; position: relative; } .thumb-info img{}</style><a class="pull-left btn bg-red addPhotoBtn" data-type="' + type + '" data-id="' + id + '" href="javascript:;"> Ajouter des Photos <i class="fa fa-plus"></i></a>' +
						"<div class='homestead titleSmallMenu' style='font-size:35px'> Album <i class='fa fa-angle-right'></i> " + data.element.name + " </div><br/>" +
						js_templates.loop(params, "album");
					return str;
				},
				function () {
					$(".addPhotoBtn").click(function () {
						uploadObj.set(type, id);
						dyFObj.openForm("addPhoto");
					});
					album.delete();
				});
		});
	},
	delete: function () {
		$(".portfolio-item .btnRemove").off().on("click", function (e) {
			e.preventDefault();
			var imageId = $(this).data("id");
			/*var params =  { 
				"parentId": uploadObj.id, 
				"parentType": uploadObj.type, 
				"docId" : imageId};*/
			console.dir(params);
			bootbox.confirm(trad.areyousuretodelete + "<span class='text-red'> " + $(this).data("name") + "</span> ?",
				function (result) {
					if (result) {
						ajaxPost(
							null,
							baseUrl + "/" + moduleId + "/document/delete/id/" + imageId,
							params,
							function (data) {
								if (data.result) {
									toastr.success(data.msg);
									$("#" + imageId).remove();
								} else {
									toastr.error(data.error)
								}
							}
						);
					}
				})
		})
	}
}

var CoAllReadyLoad = false;
var CoSigAllReadyLoad = false;
//back sert juste a differencier un load avec le back btn
//ne sert plus, juste a savoir d'ou vient drait l'appel


function showLoader(id) {
	$(id).html("<center><i class='fa fa-spin fa-refresh margin-top-50 fa-2x'></i></center>");
}

function updateSlug() {
	mylog.log("slugUnique updateSlug");
	/*var type=type;
	var canEdit=canEdit;
	var hasRc=hasRc;*/
	var form = {
		saveUrl: baseUrl + "/" + moduleId + "/element/updateblock/",
		timer: false,
		dynForm: {
			jsonSchema: {
				title: tradDynForm.updateslug,// trad["Update network"], 
				icon: "fa-key",
				onLoads: {
					sub: function () {
						$("#ajax-modal .modal-header").removeClass("bg-dark bg-purple bg-red bg-azure bg-green bg-green-poi bg-orange bg-yellow bg-blue bg-turq bg-url")
							.addClass("bg-dark");
						//bindDesc("#ajaxFormModal");
					}
				},
				beforeSave: function () {
					mylog.log("beforeSave");
					//removeFieldUpdateDynForm(contextData.type);
				},
				afterSave: function (data) {
					dyFObj.closeForm();
					toastr.success("Votre identifiant URL a bien été enregistré");
					strHash = "";
					if (location.hash.indexOf(".view") > 0) {
						hashPage = location.hash.split(".view");
						strHash = ".view" + hashPage[1];
					}
					location.hash = "@" + data.resultGoods.values.slug + strHash;
					hashUrlPage = "#@" + data.resultGoods.values.slug;
					contextData.slug = data.resultGoods.values.slug;
					//rcObj.loadChat(data.resultGoods.values.slug,type,canEdit,hasRc);
					//loadDataDirectory(connectType, "user", true);
					//changeHiddenFields();
					urlCtrl.loadByHash(location.hash);
				},
				properties: {
					info: {
						inputType: "custom",
						html: "<p class='text-dark'><i class='fa fa-info-circle'></i> " + tradDynForm.infoslug + "<hr></p>",
					},
					block: dyFInputs.inputHidden(),
					id: dyFInputs.inputHidden(),
					typeElement: dyFInputs.inputHidden(),
					slug: dyFInputs.slug(tradDynForm.slug, tradDynForm.slug, { minlength: 3/*, uniqueSlug:true*/ }),
				}
			}
		}
	};
	var dataUpdate = {
		block: "info",
		id: contextData.id,
		typeElement: contextData.type,
		slug: contextData.slug,
	};
	dyFObj.openForm(form, "sub", dataUpdate);
}

/*function bindButtonOpenForm(){ 
	//window select open form type (selectCreate)
	$(".btn-open-form").off().on("click",function(){
		mylog.log("btn-open-form");
		var typeForm = $(this).data("form-type");
		currentKFormType = ($(this).data("form-subtype")) ? $(this).data("form-subtype") : null;
		mylog.log("btn-open-form contextData", typeForm, currentKFormType, contextData);
		//alert(contextData.type+" && "+contextData.id+" : "+typeForm);
		if(contextData && contextData.type && contextData.id ){
			dyFObj.openForm(typeForm,"sub");
		}
		else{
			dyFObj.openForm(typeForm);
		}
	});
}*/

var timerCloseDropdownUser = false;


var dashboard = {
	ddaView: null,
	loadDashboardDDA: function () {
		mylog.log("loadDashboardDDA");
		$("#list-dashboard-dda").html("<span class='text-center col-xs-12 padding-25'><i class='fa fa-circle-o-notch fa-spin'></i></span>");
		lazyLoad(baseUrl + '/plugins/showdown/showdown.min.js', null, function () { });
		if (dashboard.ddaView != null) {
			$("#list-dashboard-dda").html(dashboard.ddaView);
		} else {
			ajaxPost(
				null,
				baseUrl + "/" + moduleId + "/dda/getmydashboardcoop/",
				null,
				function (data) {
					mylog.log("loadDashboardDDA ok");
					dashboard.ddaView = view;
					$("#list-dashboard-dda").html(view);
				}
			);
		}

	}
};

//MAPREMOVE : cette fct peut etre deplacer dans profilSocial.js 
// n'est utiliser que sur la page d'un element a priorie
function getRolesList(type, id) {
	mylog.log("getContextDataRoleList", type, id);
	ajaxPost(
		null,
		baseUrl + "/" + moduleId + "/element/getroleslist",
		{
			type: type,
			id: id
		},
		function (data) {
			rolesList = data;
		}
	);
}
function getContextDataLinks() {
	mylog.log("getContextDataLinks");
	ajaxPost(
		null,
		baseUrl + "/" + moduleId + "/element/getalllinks/type/" + contextData.type + "/id/" + contextData.id,
		null,
		function (data) {
			mylog.log("getContextDataLinks data", data);
			//Sig.restartMap();

			if (typeof mapCO != "undefined") {
				mapCO.clearMap();
				if (notNull(contextData)) {
					contextData.map = {
						data: data,
						icon: "link",
						title: trad.thecommunityof + " <b>" + contextData.name + "</b>"
					};
					mapCO.addElts(data)
					//Sig.showMapElements(Sig.map, data, "link", trad.thecommunityof+" <b>"+contextData.name+"</b>");
				}
			}
		},
		function (error) {
			mylog.log("getContextDataLinks error findGeoposByInsee", error);
			if (typeof mapCO != "undefined")
				mapCO.clearMap();
			callbackFindByInseeError(error);
		}
	);
}

function test(params, itemType) {
	var typeIco = i;
	params.size = size;
	params.id = getObjectId(params);
	params.name = notEmpty(params.name) ? params.name : "";
	params.description = notEmpty(params.shortDescription) ? params.shortDescription :
		(notEmpty(params.message)) ? params.message :
			(notEmpty(params.description)) ? params.description :
				"";

	//mapElements.push(params);
	//alert("TYPE ----------- "+contentType+":"+params.name);

	if (typeof (typeObj[itemType]) == "undefined")
		itemType = "poi";
	typeIco = itemType;
	if (directory.dirLog) mylog.warn("itemType", itemType, "typeIco", typeIco);
	if (typeof params.typeOrga != "undefined")
		typeIco = params.typeOrga;

	var obj = (dyFInputs.get(typeIco)) ? dyFInputs.get(typeIco) : typeObj["default"];
	params.ico = "fa-" + obj.icon;
	params.color = obj.color;
	if (params.parentType) {
		if (directory.dirLog) mylog.log("params.parentType", params.parentType);
		var parentObj = (dyFInputs.get(params.parentType)) ? dyFInputs.get(params.parentType) : typeObj["default"];
		params.parentIcon = "fa-" + parentObj.icon;
		params.parentColor = parentObj.color;
	}
	if (params.type == "classifieds" && typeof params.category != "undefined") {
		params.ico = typeof classifieds.filters[params.category] != "undefined" ?
			"fa-" + classifieds.filters[params.category]["icon"] : "";
	}

	params.htmlIco = "<i class='fa " + params.ico + " fa-2x bg-" + params.color + "'></i>";

	// var urlImg = "/upload/communecter/color.jpg";
	// params.profilImageUrl = urlImg;
	params.useMinSize = typeof size != "undefined" && size == "min";
	params.imgProfil = "";
	if (!params.useMinSize)
		params.imgProfil = "<i class='fa fa-image fa-2x'></i>";

	if ("undefined" != typeof params.profilMediumImageUrl && params.profilMediumImageUrl != "")
		params.imgProfil = "<img class='img-responsive' src='" + baseUrl + params.profilMediumImageUrl + "'/>";

	if (dyFInputs.get(itemType) &&
		dyFInputs.get(itemType).col == "poi" &&
		typeof params.medias != "undefined" && typeof params.medias[0].content.image != "undefined")
		params.imgProfil = "<img class='img-responsive' src='" + params.medias[0].content.image + "'/>";

	params.insee = params.insee ? params.insee : "";
	params.postalCode = "", params.city = "", params.cityName = "";
	if (params.address != null) {
		params.city = params.address.addressLocality;
		params.postalCode = params.cp ? params.cp : params.address.postalCode ? params.address.postalCode : "";
		params.cityName = params.address.addressLocality ? params.address.addressLocality : "";
	}
	params.fullLocality = params.postalCode + " " + params.cityName;

	params.type = dyFInputs.get(itemType).col;
	params.urlParent = (notEmpty(params.parentType) && notEmpty(params.parentId)) ?
		'#page.type.' + params.parentType + '.id.' + params.parentId : "";

	//params.url = '#page.type.'+params.type+'.id.' + params.id;
	params.hash = '#page.type.' + params.type + '.id.' + params.id;
	/* if(params.type == "poi")    
		 params.hash = '#element.detail.type.poi.id.' + id;
 
	 params.onclick = 'urlCtrl.loadByHash("' + params.hash + '");';*/

	params.elTagsList = "";
	var thisTags = "";
	if (typeof params.tags != "undefined" && params.tags != null) {
		$.each(params.tags, function (key, value) {
			if (typeof value != "undefined" && value != "" && value != "undefined") {
				thisTags += "<span class='badge bg-transparent text-red btn-tag tag' data-tag-value='" + slugify(value) + "'>#" + value + "</span> ";
				params.elTagsList += slugify(value) + " ";
			}
		});
		params.tagsLbl = thisTags;
	} else {
		params.tagsLbl = "";
	}

	params.updated = notEmpty(params.updatedLbl) ? params.updatedLbl : null;
}

$(document).ready(function () {
	mylog.warn("learn",
		"utiliser la console comme documentation",
		"taper 'learn' dans le filtre, c'est simple",
		"voir toutes l'informations de la page ou url chargé en cours.",
		{
			"url": "sur toutes urls chargé",
			"render": "sur chaque view ou template",
			"todo": "qlq chose qui reste à faire",
			"doc": "un element de documentation sur la page",
			"learn": "explique qlq chose d'utile pour un process d'apprentissage"
		}
	);

	if(notNull(costum)) {
		if(typeof contextId == "undefined" || typeof contextType == "undefined"){
			contextId = costum.contextId;
			contextType = costum.contextType;
		}
		if(!notNull(contextData)){
			contextData = getContextData();
		}
		if(notNull(contextData) && notNull(contextData.preferences) && 
		notNull(contextData.preferences.showQr) && contextData.preferences.showQr){
		}else{
			initJoinQrBtn();
		}
	}
	setTimeout(function () { checkPoll() }, 10000);
	document.onkeyup = keyboardNav.checkKeycode;
	if (notNull(userId) && userId != "")
		bindRightClicks();
});


var co = {
	ctrl: {
		lbh: function (url) {
			if (userId)
				urlCtrl.loadByHash(url);
			else co.nect();
		},
		open: function (url, type, target) {
			title = "test";

			if (type == "githubmd") {
				title = "<h1 class='text-red'>Github Markdown</h1>"
				let urlChange = url.replace("/blob/", "/")
				url = urlChange.replace("https://github.com", "https://raw.githubusercontent.com")
				callback = function () {
					getAjax('', url, function (data) {
						descHtml = dataHelper.markdownToHtml(data);
						smallMenu.content(title + descHtml);
					}
						, "html");
				}
			}
			if (type == "gitlabmd") {
				title = "<a href='" + url + "' target='_blank'>source</a>";
				var urlInterop = baseUrl + "/interop/gitlab/page?url=" + url;
				callback = function () {
					getAjax('', urlInterop, function (data) {
						//descHtml = dataHelper.convertMardownToHtml(data.content);
						descHtml = linkMdToHtml(data.content);
						smallMenu.content(title + descHtml);
					}
						, "html");
				}
			}
			if (type == "codimd") {
				title = "<h1 class='text-red'>CodiMD</h1>"
				let urlChange = url + "/download"
				callback = function () {
					getAjax('', urlChange, function (data) {
						descHtml = linkMdToHtml(data);
						// descHtml = dataHelper.convertMardownToHtml(data);
						smallMenu.content(title + descHtml);
					}
						, "html");
				}
			}
			if (type == "youtube") {
				title = "<h1 class='text-red'>Youtube</h1>"
				let urlChange = url.replace("/watch?v=", "/embed/")
				url = urlChange.split("&", 1)
				callback = function () { title + smallMenu.content('<iframe width="560" height="315" src="' + url + '" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>'); }
			}

			if (type == "json") {
				title = "Json";
				callback = function () {
					$("#openModal div.modal-content").css("text-align", "left");
					lazyLoad(baseUrl + "/plugins/jsonview/jquery.jsonview.js",
						baseUrl + "/plugins/jsonview/jquery.jsonview.css", function () {
							//alert();
							getAjax('', url, function (data) {
								urlT = url.split('/');
								title = url + "<br/>" + urlT[8];
								smallMenu.content().JSONView(data);
							}
								, "html");
						});

				}
			}

			if (title) {
				smallMenu.open(title, null, null, callback);
			} else
				toastr.error("Type not found!!");
		},
	},
	help: function () {
		url = urlCtrl.convertToPath("#default.view.page.links");
		smallMenu.openAjaxHTML(baseUrl + '/' + moduleId + "/" + url);
	},
	lang: function () {
		str = "";
		$.each(co, function (k, v) {
			str += "<a class='btn' href='javascript:;' onclick='co." + k + "()'>co." + k + "</a><br/>";
		})
		smallMenu.open("<h1>Talk to CO</h1>" + str);
	},
	//co.rss : function () {  },
	tools: function () {
		smallMenu.ajaxHTML(baseUrl + '/cotools');
	},
	nect: function () {
		if (!userId)
			Login.openLogin();
		else
			toastr.success("allready Logged in!!");
	},
	tribute: function () {
		window.open('https://www.helloasso.com/associations/open-atlas/collectes/communecter/don', '_blank');
	},
	graph: function () {
		lazyLoad("https://d3js.org/d3.v4.min.js", null, function () {
			var what = "id/" + userId + "/type/citoyens";
			if (contextData && contextData.id && contextData.type) {
				contextDataType = dyFInputs.get(contextData.type).ctrl;
				what = "id/" + contextData.id + "/type/" + contextDataType;
			}
			smallMenu.openAjaxHTML(baseUrl + '/graph/co/d3/' + what);
		}, true);
	},
	communityNode: function (id, type) {
		var what = "id/" + userId + "/type/citoyens";
		if (contextData && contextData.id && contextData.type) {
			what = "id/" + contextData.id + "/type/" + contextData.type;
		} else if (costum && costum.contextId && costum.contextType) {
			what = "id/" + costum.contextId + "/type/" + costum.contextType;
		}

		if (id && type) {
			what = `id/${id}/type/${type}`;
		}
		//return what;
		// smallMenu.openAjaxHTML( baseUrl+'/graph/co/community/'+what);
		urlCtrl.openAjax(baseUrl + '/graph/co/community/' + what);
		// $("#openModalContent").removeClass("container");
	},
	finder: function () {
		smallMenu.openAjaxHTML(baseUrl + '/graph/co/finder/slug/' + contextData.slug);
	},
	badge: function () {
		//c'est le kit de comm CO pour mettre un badge co sur 
		//https://github.com/pixelhumain/connect/blob/master/views/co/badge.php
		var what = "id/" + userId + "/type/citoyens";
		if (contextData && contextData.id && contextData.type) {
			contextDataType = dyFInputs.get(contextData.type).ctrl;
			what = "id/" + contextData.id + "/type/" + contextDataType;
		}
		smallMenu.openAjaxHTML(baseUrl + '/connect/co/badge/' + what);
	},
	gmenu: function () { },
	mind: function () {
		if (contextData && contextData.type == "citoyens")
			smallMenu.open("Mindmap", null, null, function () {
				mm = null;
				d3.json(baseUrl + '/api/person/get/id/' + contextData.id + "/format/tree", function (error, data) {
					if (error) throw error;

					smallMenu.content("<svg id='mindmap' style='width:100%;height:800px'></svg>");
					mylog.log(data);
					markmap('svg#mindmap', data, {
						preset: 'default', // or colorful
						linkShape: 'diagonal' // or bracket
					});
				});
			});
		else co.nect();
	},
	md: function (str) {
		if (contextData)
			co.ctrl.open(baseUrl + '/api/' + dyFInputs.get(contextData.type).ctrl + '/get/id/' + contextData.id + "/format/md", "md");
		else
			toastr.error("No context to build a markdown!!");
	},
	agenda: function () { urlCtrl.loadByHash("#agenda"); },
	search: function () { urlCtrl.loadByHash("#search"); },
	live: function () { urlCtrl.loadByHash("#live"); },
	web: function () { urlCtrl.loadByHash("#web"); },
	annonces: function () { urlCtrl.loadByHash("#annonces"); },
	add: function (str) {
		strT = str.split(".");
		type = {
			"org": "organization",
			"o": "organization",
			"pr": "project",
			"ev": "event",
			"e": "event",
			"p": "person",
			"poi": "poi"
		}
		if (type[strT[2]])
			dyFObj.openForm(strT[2]);
		//else todo
		// free add form 
		// set a type 

	},
	json: function (str) {
		if (contextData)
			co.ctrl.open(baseUrl + '/api/' + dyFInputs.get(contextData.type).ctrl + '/get/id/' + contextData.id, "json");
		else
			toastr.error("No context available!!");
	},
	importCostumContent : function (params, loadpage = false, progress = 0, progressBar = '', progressPercentage = '') {
		return new Promise((resolve, reject) => {
			ajaxPost(
				null,
				baseUrl + "/" + moduleId + "/cms/importbcms",
				params,
				function (data) {
					if (progress && progress > 0) {
						progressBar.style.width = progress + '%';
						progressPercentage.textContent = Math.round(progress) + '%';
					}
					if (loadpage) {
						window.location = baseUrl + "/costum/co/index/slug/" + costum.contextSlug;
					}
					resolve();
				},
				{ async: false }
			);
			return false;
		});
	},

	// *****************************************
	// Connected person stuff
	// ****************************************
	o: function () { co.ctrl.lbh("#" + userConnected.username + ".view.directory.dir.organizations"); },
	e: function () { co.ctrl.lbh("#" + userConnected.username + ".view.directory.dir.evens"); },
	pr: function () { co.ctrl.lbh("#" + userConnected.username + ".view.directory.dir.projects"); },
	p: function () { co.ctrl.lbh("#" + userConnected.username + ".view.directory.dir.follows"); },
	poi: function () { co.ctrl.lbh("#" + userConnected.username + ".view.directory.dir.poi"); },
	info: function () { co.ctrl.lbh("#" + userConnected.username + ".view.detail"); },
	// *****************************************
	// TODO
	// ****************************************

	/*
	ntre : function () { smallMenu.open("<h1>Toutes les propositions de lois et décisions sociétal pour lesquels on est contre</h1>"); } ,
	rd : function () { smallMenu.open("<h1> Visualisation de notre R&D</h1>"); } ,
	roadmap : function () { smallMenu.open("<h1> Visualisation de notre Roadmap</h1>"); } ,
	timeline : function () { smallMenu.open("<h1> Visualisation de notre Timeline</h1>"); } ,
	team : function () { smallMenu.open("<h1>Visualisation de notre Team</h1>"); } ,
	dda : function () { smallMenu.open("<h1> DashBoard Discuss, Decide, Act </h1>"); } ,
	social : function () { smallMenu.open("<h1> connecting to other social plateforms </h1>"); } ,
	city : function () { smallMenu.open("<h1> DashBoard City </h1>"); } ,
	*/
}

function initJoinQrBtn(){
	ajaxPost(
		null,
		baseUrl+"/"+moduleId+"/element/join_element",
		{
			contextId: contextId,
			contextType: contextType
		},
		function(data){
			mylog.log("Data from success", data);
			if(data.result){
				mylog.log("Data from success", "Okay");
				if($(`#joins-${contextId}`).length > 0){
					$(`#joins-${contextId}`).remove();
				}

				$("#page-top").append(data.view);
			}
		}
	);
}

function getContextData(){
	var context = {};
	ajaxPost('', baseUrl + "/co2/element/getcontext/contextId/" + contextId+"/contextType/"+contextType,
		null,
		function (data) {
			context = data;
		},
		null,
		"json",
		{async : false}
	);
	return context;
}