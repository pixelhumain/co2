/*(function(){
    if(typeof CoInput !== "function"){
        console.error("L'extension 'co-input-types-css' a besoin de l'object 'CoInput' pour fonctionner.");
        return;
    }*/

  

    var cssHelpers = {
        render: {
            generalCssStyle : function(obj, keyClass="", mainDom=""){
                var str="";
                var strSm="";
                var strXs="";
                var objClass=cssHelpers.render.getCssInCascade(obj, {});
                $.each(objClass, function(classes,style){
                    if (notEmpty(style)) {
                        tabClass=classes.split(".");
                        var classStr=cssHelpers.render.getDomByPathArray(tabClass, keyClass, mainDom);

                        var specClass = [":hover", ":active", ":focus"];
                        specClass.forEach(function (k) {
                            if (classStr.includes(k) && classStr.indexOf(k) < classStr.length - k.length) {
                                classStr = classStr.replace(k,"");
                                classStr = classStr+k;
                            }
                        });
                        if (classStr.includes(" .xs")) {
                            strXs += cssHelpers.render.clrDuplicatedClass(classStr.replace(" .xs", "")) + "{" + style.join('') + "}";
                        } else if (classStr.includes(" .sm")) {
                            strSm += cssHelpers.render.clrDuplicatedClass(classStr.replace(" .sm", "")) + "{" + style.join('') + "}";
                        } else {
                            str += classStr + "{" + style.join('') + "}";
                        }
                    }
                });
                xs = strXs !== "" ? "@media only screen and (max-width: 767px) {"+ strXs +"}" : "";
                sm = strSm !== "" ? "@media only screen and (max-width: 991px) {"+ strSm +"}" : "";
                str += sm + xs ;
                return str;
            },
            getCssInCascade : function(obj, objClass, path){
                obj=cssHelpers.render.checkBackgroundProperty(obj);
                $.each(obj,function(e,v){
                    if($.inArray(e, cssHelpers.rejectCssProperties) < 0 ){
                        if(typeof cssHelpers.json[e]== "undefined" || typeof v == "object"){
                            keypath=(notEmpty(path)) ? path+"."+e : e;
                            objClass[keypath]=[];
                        }
                        if(typeof cssHelpers.json[e]!= "undefined" && typeof v != "object"){
                            v = (e == "fontFamily") ? `'${v}' !important` : v;
                            objClass[path].push(cssHelpers.json[e].property+":"+v+";");
                        }
                        else if(typeof v == "object"){
                            objClass=cssHelpers.render.getCssInCascade(v, objClass, keypath);
                        }
                    }
                });
                return objClass;
            },
            checkBackgroundProperty : function(obj){
                if(typeof obj["backgroundType"] != "undefined"){
                    removeAttr=[];
                    if(obj["backgroundType"] == "backgroundColor"){
                        removeAttr=['backgroundImage', 'backgroundAttachment', 'backgroundPosition',"backgroundRepeat"];
                    }
                    
                    if(typeof removeAttr != "undefined" && removeAttr.length > 0){
                        $.each(removeAttr, function(e,v){
                            if(typeof obj[v] != "undefined") delete obj[v];
                        })
                    }
                    delete obj.backgroundType;
                }
                return obj;
                
            },
            getDomByPathArray : function(arrayDom, key, mainDom=""){
                var domStr = (notEmpty(mainDom)) ? mainDom : "";
                $.each(arrayDom,function(i,domTarget){
                    if($.inArray(domTarget, ["hover", "active", "focus"]) >=0) {
                        domStr+=":"+domTarget;
                    }
                    else 
                        domStr+=(notEmpty(key)) ? " ."+key+"-"+domTarget :  (domTarget.startsWith("#") ? " "+domTarget : " ."+domTarget );
                });
                return domStr;
            },
            removeStyleUI: function(arrayStyleToRemove, key=""){
                $.each(arrayStyleToRemove, function(e, v){
                    var classStr=cssHelpers.render.getDomByPathArray(v.split("."), key);
                    if($(classStr).length >=0)
                        $(classStr).removeAttr("style");
                });
            },
            addStyleUI : function(path, value, key="" , childSelector = ""){
                if(path.indexOf("css.") >= 0)
                    path=path.split("css.")[1];
                var tabClass=path.split(".");
                var currentProperty=tabClass.splice(-1)[0];

                var classStr=cssHelpers.render.getDomByPathArray(tabClass, key);

                if ( childSelector != ""){
                    var $elementSelected = $(classStr+childSelector)
                } else {
                    var $elementSelected = $(classStr)
                }
                

                if($elementSelected.length >=0 && typeof cssHelpers.json[currentProperty] != "undefined"&& typeof cssHelpers.json[currentProperty].property != "undefined"){
                    var styleElem = $elementSelected.attr("style");
                    var newStylesheet="";
                    if(notEmpty(styleElem)){
                        if(styleElem.indexOf(cssHelpers.json[currentProperty].property) >= 0){
                            $.each(styleElem.split(";"), function(e, v){
                                if(notEmpty(v) && v.indexOf(cssHelpers.json[currentProperty].property) < 0)
                                    newStylesheet+=v+";";           
                            });
                        }else
                            newStylesheet=styleElem;
                    }
                    if (path.indexOf("fontFamily") >= 0) {
                        value = `'${value}'`;
                    }
                    $elementSelected.attr('style', newStylesheet+cssHelpers.json[currentProperty].property+': '+value+' !important;');
                    // costumizer.focusComponentSocket("elementFocused", "[data-path='"+$elementSelected.data("path")+"']");
                }
            },
            addStyleInDomByPath : function(obj, dom, keyClass="", mainDom=""){
                tabCSSToGenerate={};
                $.each(obj, function(e, v){
                    pathCss=e.slice(0, e.lastIndexOf("."));
                    property= e.slice((e.lastIndexOf(".")+1), e.length);
                    if(typeof tabCSSToGenerate[pathCss] == "undefined")
                        tabCSSToGenerate[pathCss]=[];
                    if(typeof cssHelpers.json[property] != "undefined" && typeof cssHelpers.json[property].property != "undefined")
                        tabCSSToGenerate[pathCss].push(cssHelpers.json[property].property+":"+v+cssHelpers.render.getSpecPx(property, v)+"!important;");
                     
                    //var currentProperty=tabClass.splice(-1)[0];

                });
                var str = "";
                 $.each(tabCSSToGenerate, function(classes,style){
                    tabClass=classes.split(".");
                    var classStr=cssHelpers.render.getDomByPathArray(tabClass, keyClass, mainDom);
                    if(notEmpty(style))
                        str+=classStr+"{"+style.join('')+"}";
                });
                $(dom).append(str);
            },
            addClassDomByPath : function (obj,keyClass){
                 $.each(obj, function(e, v){
                     if ($.inArray(e, ["hideOnDesktop", "hideOnTablet", "hideOnMobil"])>= 0){
                         if (v == true) {
                            $("."+keyClass).addClass(cssHelpers.json[e].classValue);
                         } else {
                            $("."+keyClass).removeClass(cssHelpers.json[e].classValue);
                         }
                     }
                 })
            },
            replaceAllDomStyle : function(path, value, key=""){

            },
            generateSimpleStyle:function(cssObj){
                var cssAdd="";
                $.each(cssObj, function(e, v){
                    if(typeof cssHelpers.json[e] != "undefined" && typeof cssHelpers.json[e].property != "undefined")
                        cssAdd+=cssHelpers.json[e].property+":"+v+cssHelpers.getSpecPx(e, v)+";";
                });
               
                return cssAdd;
            },
            getSpecPx:function(k, v){
                if(typeof cssHelpers.json[k].input != "undefined"
                    && typeof cssHelpers.json[k].input.rules != "undefined"){
                    if(cssHelpers.json[k].input.rules=="cssHelpers.form.rules.checkLengthProperties"){
                        if(typeof v == "number"){
                            // alert("")
                            return "px";
                        } else if (v.toString().indexOf("%") >=0) {
                            return ""
                        } else if(v.toString().indexOf("px")>=0) {
                            return "";
                        } else if( parseInt(v) != NaN ) {
                            return "px";
                        } else if(v == "auto") {
                            return "";
                        } else if (v.toString().indexOf("%") >=0){
                            //alert("ici%");
                            return "";
                        } else if(v.toString().indexOf("px")>=0){
                            //alert("icipx");
                            return "";
                        } else
                            return "px";
                    
                    }else {
                        return "";
                    }   
                }
            },
            clrDuplicatedClass : function (classStr) {
                // var filteredStr = Array.from(new Set(classStr.split(' '))).toString();
                return classStr.replace(",","")
            },
            otherClassControl: function (otherCssData, classStr) {
                var mediaQuerySM = window.matchMedia('(min-width: 768px) and (max-width: 991px)');
                var mediaQueryXS = window.matchMedia('screen and (max-width: 767px)');
                var updateClasses = function () {
                  var otherClassToAdd = "";
                  var classToRemove = "";
                  var otherClass = typeof otherCssData["otherClass"] !== "undefined" ? otherCssData["otherClass"] : "";
                  var otherClassSm = (typeof otherCssData["sm"] !== "undefined" && typeof otherCssData["sm"]["otherClass"] !== "undefined") ? otherCssData["sm"]["otherClass"] : "";
                  var otherClassXs = (typeof otherCssData["xs"] !== "undefined" && typeof otherCssData["xs"]["otherClass"] !== "undefined") ? otherCssData["xs"]["otherClass"] : "";
                  
                  if (mediaQueryXS.matches) {
                    otherClassToAdd += " " + otherClassXs;
                    classToRemove = otherClassSm;
                  } else if (mediaQuerySM.matches) {
                    otherClassToAdd += " " + otherClassSm;
                    classToRemove = otherClassXs;
                  } else {
                    classToRemove = otherClassXs + " " + otherClassSm;
                    otherClassToAdd += " " + otherClass;
                  }
            
                  $("." + classStr).removeClass(classToRemove);
                  $("." + classStr).addClass(otherClassToAdd);
                };
            
                // Initial check
                updateClasses();
            
                // Add event listener for changes
                mediaQueryXS.addListener(updateClasses);
                mediaQuerySM.addListener(updateClasses);
            }
        },
        form : {
          /*  translateInput: function(key, obj){

                if(typeof obj.type != "undefined"){
                    typeInput = obj.type;
                    delete optionsInput.type;
                }
                if(typeof obj.rules == "string")
                    optionsInput.filterValue=eval(obj.rules);
                if(typeof obj.input.units != "undefined")
                    optionsInput.units=obj.input.units;
                if(typeof obj.input.linkValue != "undefined")
                    optionsInput.linkValue=obj.input.linkValue;
                    
                optionsInput.name=key;
                return options;
            },*/
            extendCoInput : function(obj){
                   // alert();
                 
                    $.each(cssHelpers.json, function(key, v){
                        if(notNull(v.label))
                            cssHelpers.translate[key] = v.label;
                        //var input = cssHelpers.json[key],
                        //    name = input.options?.name || input.type;
                        if(notNull(v.input)){

                            var optionsInput=$.extend(true,{},v.input);
                            var typeInput="inputSimple";
                            if(typeof v.input.type != "undefined"){
                                typeInput = v.input.type;
                                delete optionsInput.type;
                            }
                            if(typeof v.input.rules == "string")
                                optionsInput.filterValue=eval(v.input.rules);                         
                            optionsInput.name=key;
                            if($.inArray(typeInput, ["inputGroup"]) >=0){
                                var optionsInputGroup=$.extend(true,[],v.input.options);
                                optionsInput.inputs=optionsInputGroup;
                            }
                            if(typeof v.defaultZero != "undefined") {
                                optionsInput.defaultZero = v.defaultZero;
                            }
                            optionsInput.label=(typeof v.label != "undefined") ? v.label : "";
                            CoInput.extendType(typeInput, key, function(options){
                                var extendOptions = $.extend(true,{},optionsInput);
                                return $.extend(true, extendOptions, options);
                            });
                        }
                    });
                
            },
            getFormatData : function(){
              
            },
            launch:function(container, inputs, payload, defaultValues, translate, onchangeCallback=null, onblurCallback=null){
                buildTranslate = $.extend(true,cssHelpers.translate,translate);
                new CoInput ({
                    container : container,
                    inputs : inputs,
                    payload: payload,
                    defaultValues : defaultValues,
                    i18n: buildTranslate,
                    onchange: function(name,value, payload){
                        var path = '';
                        if (typeof payload != "undefined" && notEmpty(payload)) {
                            if (typeof payload.path != "undefined")
                                path=payload.path;
                            if(typeof payload.sectionPath != "undefined") {
                                if (typeof payload.pathExist != "undefined") {
                                    path = payload.sectionPath
                                } else {
                                    (path !== "") ? path += "." + payload.sectionPath : path += payload.sectionPath;
                                }
                            }
                        }

                        if (name == "backgroundImage") {
                            if (value != "")
                                value = `url('${value}')`;
                            else 
                                value = "url('')";
                        }
                        var valueToSet={};
                        valueToSet=value;
                        if(typeof cssHelpers.json[name] != "undefined" && 
                            typeof cssHelpers.json[name].input != "undefined" &&
                                (typeof cssHelpers.json[name].input.aggregate == "undefined" || cssHelpers.json[name].input.aggregate===true || typeof cssHelpers.json[name].input.aggregate == "object")){
                            path+="."+name;
                        }
                        if(notNull(onchangeCallback))
                            onchangeCallback(path,valueToSet,name,payload,value);
                    },
                    onblur: function(name, value, payload) {
                        var path = '';
                        if (typeof payload != "undefined" && notEmpty(payload)) {
                            if (typeof payload.path != "undefined")
                                path=payload.path;
                            if(typeof payload.sectionPath != "undefined") {
                                if (typeof payload.pathExist != "undefined") {
                                    path = payload.sectionPath
                                } else {
                                    (path !== "") ? path += "." + payload.sectionPath : path += payload.sectionPath;
                                }
                            }
                        }

                        if (name == "backgroundImage") {
                            if (value != "")
                                value = `url('${value}')`;
                            else 
                                value = "url('')";
                        }

                        var valueToSet={};
                        valueToSet=value;
                        
                        if(typeof cssHelpers.json[name] != "undefined" && 
                            typeof cssHelpers.json[name].input != "undefined" &&
                                (typeof cssHelpers.json[name].input.aggregate == "undefined" || cssHelpers.json[name].input.aggregate===true || typeof cssHelpers.json[name].input.aggregate == "object")){
                            path+="."+name;
                        }

                        if(notNull(onblurCallback))
                            onblurCallback(path,valueToSet,name,payload,value);
                    }
                });
            },
            generateCoInputForm: function(obj){

            },  
            rules : {
                checkLengthProperties : function(values, unit = ""){
                    if(typeof values =="object"){
                        $.each(values, function(e,v){
                            if(isNaN(v))
                                values[e]=v;
                            else if (v == "")
                                values[e]="";
                            else
                                values[e]=v+""+unit;
                            });//.join(" ");
                    }else{
                        if (isNaN(values))
                            values=values;
                        else if (values == "")
                            values="";
                        else
                            values=values+""+unit;
                    }
                    //if(!res.replace(/\s/g, ""))
                      //  return "";
                    return values;
                },
                checkLengthInparentProperty : function(values){
                   var res = ["top","right","bottom","left"].map(function(name){
                    if(isNaN(values[name]))
                        return values[name]
                    else if (values[name] == "")
                        return "0";
                    else
                        return values[name]+"px";
                }).join(" ");
                if(!res.replace(/\s/g, ""))
                         return "";
                    return res;
                }
            },
            getBorderOptions:function(position){
                var positionCapitalize = position[0].toUpperCase() + position.slice(1);
        
                return {
                    [`border${positionCapitalize}`]:{
                        type:"inputBorderSingle",
                        options:{
                            name:"border-"+position,
                            label:"Border "+position
                        }
                    },
                    [`border${positionCapitalize}Color`]:{
                        type:"colorPicker",
                        options:{
                            name:`border-${position}-color`,
                            label:`Border ${position} color`
                        }
                    },
                    [`border${positionCapitalize}Style`]:{
                        type:"select",
                        options:{
                            name:`border-${position}-style`,
                            label:`Border ${position} style`,
                            options:["none","hidden","dotted","dashed","solid","double","groove","ridge","inset","outset","initial","inherit"]
                        }
                    },
                    [`border${positionCapitalize}Width`]:{
                        type:"inputSimple",
                        options:{
                            name:`border-${position}-width`,
                            label:`Border ${position} width`
                        }
                    }
                }
            },
            getSpacingOptions:function(name, label){
                return {
                    type:"inputGroup",
                    options:{
                        name:name,
                        label:label,
                        inputs:[
                            { label:"Top", name:"top",icon:"toggle-up" },
                            { label:"Right", name:"right",icon:"toggle-right" },
                            { label:"Bottom", name:"bottom",icon:"toggle-down" },
                            { label:"Left", name:"left",icon:"toggle-left" }
                        ],
                        filterValue:function(values){
                            var res = ["top","right","bottom","left"].map(function(name){
                                if(isNaN(values[name]))
                                    return values[name]
                                else if (values[name] == "")
                                    return "0";
                                else
                                    return values[name]+"px";
                            }).join(" ");
                            if(!res.replace(/\s/g, ""))
                                return "";
                            return res;
                        }
                    }
                }
            },
            getWidthOptions:function(name, label){
                return {
                    type:"selectGroup",
                    options:{
                        name:name,
                        label:label,
                        inputs:[
                            {
                                name: "colMd",
                                label: "",
                                icon : "desktop",
                                options: ["1","2","3","4","5","6","7","8","9","10","11","12"]
                                
                            },
                            {
                                name: "colSm",
                                label: "",
                                icon : "tablet",
                                options: ["1","2","3","4","5","6","7","8","9","10","11","12"]
                                
                            },
                            {
                                name: "colXs",
                                label: "",
                                icon : "mobile",
                                options: ["1","2","3","4","5","6","7","8","9","10","11","12"]
                                
                            }
                        ]
                    }
                }
            },
            getAlignOptions:function(name, label){
                return {
                    type:"groupButtons",
                    options:{
                        name:name,
                        label:label,
                        options:[
                            {
                                value:"left",
                                icon:"align-left"
                            },
                            {
                                value:"center",
                                icon:"align-center"
                            },
                            {
                                value:"right",
                                icon:"align-right"
                            }
                        ]
                    }
                };
            },
            formatPaddingValue:function(value){
                if(value){
                    value = value.replace(/\s{2,}/g, " ")
                    var valueParts = value.split(" ")
                    if(valueParts.length === 1)
                        value = Array(4).fill(valueParts[0]).join(" ")
                    else if(valueParts.length === 2)
                        value = valueParts.concat(valueParts).join(" ")
                    else
                        value = valueParts.join(" ")
                }
                return value;
            }
        },
        rejectCssProperties:["urls", "font","hideOnDesktop","hideOnTablet","hideOnMobil","columnWidth","languages"],
        rejectFromCss:["languages","hideOnDesktop","hideOnTablet","hideOnMobil","otherClass","otherCss"],
        translate : {},
        json : {    
            accentColor:{
                label: tradCms.accentColor || "Accent color",
                property:"accent-color",
                input:{
                    type:"inputSimple"
                }
            },
            alignItems:{
                moz: false,
                webkit: true,
                property:"align-items",
                label: tradCms.alignItems || "Align items",
                input:{
                    type:"groupButtons",
                    responsive: true,
                    options:[
                        {
                            value:"stretch",
                            img: baseUrl+"/"+assetPath+"/cmsBuilder/img/display/stretch.png"
                        },
                        {
                            value:"center",
                            img: baseUrl+"/"+assetPath+"/cmsBuilder/img/display/align-content-center.png"
                        },
                        {
                            value:"flex-start",
                            img: baseUrl+"/"+assetPath+"/cmsBuilder/img/display/align-content-left.png"
                        },
                        {
                            value:"flex-end",
                            img: baseUrl+"/"+assetPath+"/cmsBuilder/img/display/align-content-right.png"
                        },
                        {
                            value:"baseline",
                            img: baseUrl+"/"+assetPath+"/cmsBuilder/img/display/baseline.png"
                        },
                        {
                            value:"initial",
                            img: baseUrl+"/"+assetPath+"/cmsBuilder/img/display/initial.png"
                        },
                        {
                            value:"inherit",
                            img: baseUrl+"/"+assetPath+"/cmsBuilder/img/display/inherit.png"
                        },
                    ]
                }
            },
            alignSelf:{
                label: tradCms.alignSelf || "Align self",
                property: "align-self",
                moz: false,
                webkit: true,
                input:{
                    type:"groupButtons",
                    responsive: true,
                    options:[
                        {
                            value:"auto",
                            img: baseUrl+"/"+assetPath+"/cmsBuilder/img/display/auto.png"
                        },
                        {
                            value:"stretch",
                            img: baseUrl+"/"+assetPath+"/cmsBuilder/img/display/stretch.png"
                        },
                        {
                            value:"flex-start",
                            img: baseUrl+"/"+assetPath+"/cmsBuilder/img/display/align-content-left.png"
                        },
                        {
                            value:"flex-end",
                            img: baseUrl+"/"+assetPath+"/cmsBuilder/img/display/align-content-right.png"
                        },
                        {
                            value:"center",
                            img: baseUrl+"/"+assetPath+"/cmsBuilder/img/display/align-content-center.png"
                        },
                        {
                            value:"baseline",
                            img: baseUrl+"/"+assetPath+"/cmsBuilder/img/display/baseline.png"
                        },
                        {
                            value:"initial",
                            img: baseUrl+"/"+assetPath+"/cmsBuilder/img/display/initial.png"
                        },
                        {
                            value:"inherit",
                            img: baseUrl+"/"+assetPath+"/cmsBuilder/img/display/inherit.png"
                        },
                    ]
                }
            },
            alignContent:{
                label: tradCms.alignContent || "Align content",
                property: "align-content",
                moz: false,
                webkit: true,
                input:{
                    type:"groupButtons",
                    responsive: true,
                    options:[
                        {
                            value:"flex-start",
                            img: baseUrl+"/"+assetPath+"/cmsBuilder/img/display/align-content-left.png"
                        },
                        {
                            value:"flex-end",
                            img: baseUrl+"/"+assetPath+"/cmsBuilder/img/display/align-content-right.png"
                        },
                        {
                            value:"center",
                            img: baseUrl+"/"+assetPath+"/cmsBuilder/img/display/align-content-center.png"
                        },
                        {
                            value:"space-between",
                            img: baseUrl+"/"+assetPath+"/cmsBuilder/img/display/align-content-arround.png"
                        },
                        {
                            value:"space-around",
                            img: baseUrl+"/"+assetPath+"/cmsBuilder/img/display/align-content-between.png"
                        },
                        {
                            value:"space-evenly",
                            img: baseUrl+"/"+assetPath+"/cmsBuilder/img/display/evenly.png"
                        },
                        {
                            value:"initial",
                            img: baseUrl+"/"+assetPath+"/cmsBuilder/img/display/initial.png"
                        },
                        {
                            value:"inherit",
                            img: baseUrl+"/"+assetPath+"/cmsBuilder/img/display/inherit.png"
                        },
                        {
                            value:"stretch",
                            img: baseUrl+"/"+assetPath+"/cmsBuilder/img/display/stretch.png"
                        }
                    ]
                }
            },
            justifyContent:{
                label: tradCms.justifyContent || "Justify content",
                property: "justify-content",
                moz: true,
                webkit: true,
                input:{
                    type:"groupButtons",
                    responsive: true,
                    options:[
                        {
                            value:"flex-start",
                            img: baseUrl+"/"+assetPath+"/cmsBuilder/img/display/align-content-left.png"
                        },
                        {
                            value:"flex-end",
                            img: baseUrl+"/"+assetPath+"/cmsBuilder/img/display/align-content-right.png"
                        },
                        {
                            value:"center",
                            img: baseUrl+"/"+assetPath+"/cmsBuilder/img/display/align-content-center.png"
                        },
                        {
                            value:"space-between",
                            img: baseUrl+"/"+assetPath+"/cmsBuilder/img/display/align-content-arround.png"
                        },
                        {
                            value:"space-around",
                            img: baseUrl+"/"+assetPath+"/cmsBuilder/img/display/align-content-between.png"
                        },
                        {
                            value:"space-evenly",
                            img: baseUrl+"/"+assetPath+"/cmsBuilder/img/display/evenly.png"
                        },
                        {
                            value:"initial",
                            img: baseUrl+"/"+assetPath+"/cmsBuilder/img/display/initial.png"
                        },
                        {
                            value:"inherit",
                            img: baseUrl+"/"+assetPath+"/cmsBuilder/img/display/inherit.png"
                        },
                        {
                            value:"stretch",
                            img: baseUrl+"/"+assetPath+"/cmsBuilder/img/display/stretch.png"
                        }
                    ]
                }
            },
            all:{
                moz: false,
                webkit: false,
                property:"all",
                label: tradCms.all || "All",
                input:{
                    type:"select", 
                    options:["initial","inherit","unset"]
                }
            },
            animationDelay: {
                moz: true,
                webkit: true,
                label: tradCms.animationDelay || "Animation delay",
                property:"animation-delay",
                input: {
                    type: "inputNumberRange",
                    minRange: -1000,
                    maxRange: 1000,
                    units: ["ms", "s"],
                    rules: "cssHelpers.form.rules.checkLengthProperties",
                } 
            },
            animationDirection: {
                moz: true,
                webkit: true,
                property: "animation-direction",
                label : tradCms.animationDirection || "Animation direction",  
                input: { 
                    type:"select",
                    options:[
                        "normal","reverse","alternate","alternate-reverse","initial","inherit"
                    ]
                }
            },
            animationDuration: {
                moz: true,
                webkit: true,
                label : tradCms.animationDuration || "Animation duration",
                property:" animation-duration",
                input: {
                    type: "inputNumberRange",
                    minRange: -1000,
                    maxRange: 1000,
                    units: ["ms", "s"],
                    rules: "cssHelpers.form.rules.checkLengthProperties",
                } 
            },
            animationFillMode: {
                moz: true,
                webkit: true,
                label : tradCms.animationFillMode || "Animation fill mode",
                property: "animation-fill-mode",
                input : { 
                    type : "select",
                    options : [
                        "none",
                        "forwards",
                        "backwards",
                        "both",
                        "initial",
                        "inherit"
                    ]
                }
            },
            animationIterationCount: {
                moz: true,
                webkit: true,
                property:"animation-iteration-count",
                label : tradCms.animationIterationCount || "Animation iteration count",
                input: {
                    type: "inputNumberRange",
                    minRange: -1000,
                    maxRange: 1000,
                    units: ["ms", "s"],
                    rules: "cssHelpers.form.rules.checkLengthProperties",
                } 
            },
            animationName: {
                moz: true,
                webkit: true,
                property : "animation-name",
                label : tradCms.animationName || "Animation name",
                input:{
                    type:"inputSimple"
                }
            },
            animationTimingFunction: {
                moz: true,
                webkit: true,
                property:"animation-timing-function",
                label: tradCms.animationTimingFunction || "Animation timing function",
                input:{
                    type:"select",
                    options: [
                        "linear",
                        "ease",
                        "ease-in",
                        "ease-out",
                        "ease-in-out",
                        "step-start",
                        "step-end",
                        "[steps]",
                        "cubic-bezier",
                        "initial",
                        "inherit"
                    ]
                }
            },
            backdropFilter:{
                property : "backdrop-filter",
                label: tradCms.backdropFilter || "Backdrop filter",
                input:{
                    type:"inputSimple"              
                }
            },
            backfaceVisibility:{
                moz:true,
                webkit:true,
                label: tradCms.backfaceVisibility || "Backface visibility",
                property: "backface-visibility",
                input:{
                    type:"select",
                    options:["visible","hidden","initial","inherit"]
                 }
            },
            backgroundType:{
                moz:false,
                webkit:false,
                label: tradCms.background || "Background",
                input:{
                    type: "inputSwitcher",
                    tabs :[
                        {
                            value: "backgroundColor",
                            label : tradCms.color || "Color",
                            inputs: [
                                "backgroundColor"
                            ]
                        },
                        {
                            value: "backgroundUpload",
                            label : tradCms.image || "Image",
                            inputs: [
                                "backgroundUpload"
                            ]
                        }
                    
                    ]
                }
            },
            backgroundAttachment:{
                moz : false,
                webkit : false,
                label: tradCms.backgroundAttachment || "Background attachment",
                property:"background-attachment",
                input: {
                    type:"select",
                    responsive: true,
                    options:["scroll","fixed","local","initial","inherit"]
                }
            },
            backgroundBlendMode:{
                moz : false,
                webkit : false,
                label: tradCms.backgroundBlendMode || "Background blend mode",
                property:"background-blend-mode",
                input:{
                    type:"select",
                    options:["normal","multiply","screen","overlay","darken","lighten","color-dodge","saturation","color","luminosity"]
                }
            },
            backgroundClip:{
                moz : false,
                webkit : false,
                property:"background-clip",
                label: tradCms.backgroundClip || "Background clip",
                input:{
                    type:"select",
                    options:["border-box","padding-box","content-box","initial","inherit"]
                }
            },
            background:{
                moz:false,
                webkit:false,
                property:"background",
                label: tradCms.background || "Background",
                input:{
                    type:"colorPicker"
                }
            },
            backgroundColor:{
                moz:false,
                webkit:false,
                property:"background-color",
                label: tradCms.backgroundColor || "Background color",
                input:{
                    type:"colorPicker"
                }
            },
            backgroundUpload : {
                moz:false,
                webkit:false,
                property:null,
                input: {
                    type:"inputGroup",
                    classes: "fullWidth",
                    options: [
                        "backgroundImage",
                        "backgroundPosition",
                        "backgroundRepeat",
                        "backgroundSize",
                        "backgroundAttachment",
                        "backgroundColor"
                    ]
                }
            },
            backgroundImage:{
                moz:false,
                webkit:false,
                property: "background-image",
                label: tradCms.backgroundImage || "Background image",
                input:{
                    type : "inputFileImage",
                    preview:true
                }
            },
            backgroundOrigin:{
                moz:false,
                webkit:false,
                property: "background-origin",
                label: tradCms.backgroundOrigin || "Background origin",
                input:{
                    type:"select",
                    options:["padding-box","border-box","content-box","initial","inherit"]
                }
            },
            backgroundPosition:{
                moz:false,
                webkit:false,
                property: "background-position",
                label: tradCms.backgroundPosition || "Background position",
                input:{
                    type:"select",
                    responsive: true,
                    options: [
                    "left top","left center","left bottom",
                    "right top","right center","right bottom",
                    "center top","center center","center bottom",
                    "initial","inherit"]
                }
            },
            backgroundRepeat:{
                moz:false,
                webkit:false,
                property:"background-repeat",
                label: tradCms.backgroundRepeat || "Background repeat",
                input:{
                    type:"select",
                    responsive: true,
                    options:["repeat","repeat-x","repeat-y","no-repeat","initial","inherit"]
                }
            },
            backgroundSize:{
                moz:true,
                webkit:true,
                property:"background-size",
                label: tradCms.backgroundSize || "Background size",    
                input:{
                    type:"select",
                    responsive: true,
                    options:["auto", "length", "cover", "contain", "initial", "inherit"]
                }
            },
            border:{
                moz:false,
                webkit:false,
                property:"border",
                label: tradCms.border || "Border",
                input:{
                    type:"inputBorderSingle",
                    responsive: true,
                    aggregate: ["borderWidth", "borderStyle", "borderColor"]
                }
            },
            borderLeft: {
                moz:false,
                webkit:false,
                property:"border-left",
                label: tradCms.border || "Border",
                input:{
                    type:"inputBorderSingle",
                    responsive: true,
                    aggregate: ["borderWidth", "borderStyle", "borderColor"]
                }
            },
            borderWidth:{
                label: tradCms.borderWidth || "Border width",
                property: "border-width",
                input:{
                    type:"number",
                    units: ["px", "%"],
                    rules:"cssHelpers.form.rules.checkLengthProperties"
                }
            },
            borderColor:{
                moz:false,
                webkit:false,
                property:"border-color",
                label: tradCms.borderColor || "Border color",
                input:{
                    type:"colorPicker"
                }
            },
            borderStyle: {
                moz: false,
                webkit: false,
                property:"border-style",
                label : tradCms.borderStyle || "Border style",
                input:{
                    type:"select",
                    options: [
                        "none","hidden","dotted","dashed","solid","double",
                        "groove","ridge","inset","outset","initial","inherit"
                    ]
                }
            },
            radius:{
                webkit: false,
                moz: false,
                label: tradCms.borderRadius || "Border radius",
                property:"border-radius",
                icon:"toggle-left", 
                input:{
                    type:"number",
                    responsive: true,
                    rules:"cssHelpers.form.rules.checkLengthProperties"
                }
            },
            borderRadius:{
                moz:false,
                webkit:false,
                property: "border-radius",
                label: tradCms.borderRadius || "Border radius",
                input:{
                    type:"inputGroup",
                    rules:"cssHelpers.form.rules.checkLengthProperties",
                    units:["px", "%"],
                    linkValue:true,
                    aggregate : false,
                    responsive: true,
                    options:[
                        "borderBottomLeftRadius",
                        "borderBottomRightRadius",
                        "borderTopLeftRadius",
                        "borderTopRightRadius"
                    ],
                }
            },
            borderBottomLeftRadius:{
                moz:false,
                webkit:false,
                property:"border-bottom-left-radius",
                label: tradCms.bottomLeft || "Bottom left",
                input:{
                    type:"number",
                    rules:"cssHelpers.form.rules.checkLengthProperties"
                }
            },
            borderBottomRightRadius:{
                moz:false,
                webkit:false,
                property:"border-bottom-right-radius",
                label: tradCms.bottomRight || "Bottom right",
                input:{
                    type:"number",
                    rules:"cssHelpers.form.rules.checkLengthProperties"
                }
            },
            borderTopLeftRadius:{
                moz:false,
                webkit:false,
                property:"border-top-left-radius",
                label: tradCms.topLeft || "Top left",
                input:{
                    type:"number",
                    rules:"cssHelpers.form.rules.checkLengthProperties"
                }
            },
            borderTopRightRadius:{
                moz:false,
                webkit:false,
                property:"border-top-right-radius",
                label: tradCms.topRight || "Top right",
                input:{
                    type:"number",
                    rules:"cssHelpers.form.rules.checkLengthProperties"
                }
            },
            bottom:{
                moz:false,
                webkit:false,
                property : "bottom",
                label: tradCms.bottom || "Bottom",
                input:{
                    type:"number",
                    units: ["px", "%"],
                    rules:"cssHelpers.form.rules.checkLengthProperties"
                 }
            },
            boxShadow:{
                moz:true,
                webkit:true,
                property : "box-shadow",
                label: tradCms.shadow || "Box shadow",
                input:{
                    responsive: true,
                    type: "inputShadow"
                }
            },
            boxSizing:{
                moz:true,
                webkit:true,
                property : "box-sizing",
                label: tradCms.boxSizing || "Box sizing",
                input:{
                    type:"select",
                    options:["content-box","border-box","initial","inherit"]
                }
            },
            breakAfter:{
                moz:false,
                webkit:false,
                property:"break-after",
                label: tradCms.breakAfter || "Break after",
                input:{
                    type:"select",
                    options:[
                        "auto","all","always","avoid","avoid-column","avoid-page",
                        "avoid-region","column","left","page","recto","region",
                        "right","verso","initial","inherit"
                    ]
                }
            },
            breakBefore:{
                moz:false,
                webkit:false,
                property: "break-before",
                label: tradCms.breakBefore || "Break before",
                input:{
                    type:"select",
                    options:[
                        "auto","all","always","avoid","avoid-column","avoid-page",
                        "avoid-region","column","left","page","recto","region",
                        "right","verso","initial","inherit"
                    ]
                }
            },
            breakInside:{
                moz:false,
                webkit:false,
                property:"break-inside",
                label: tradCms.breakInside || "Break inside",
                input:{
                    type:"select",
                    options:[
                        "auto","all","always","avoid","avoid-column","avoid-page",
                        "avoid-region","column","left","page","recto","region",
                        "right","verso","initial","inherit"
                    ]
                }
            },
            captionSide:{
                property:"caption-side",
                label: tradCms.captionSide || "Caption side",
                input:{
                    type:"select",
                    options:["top","bottom","initial","inherit"]
                }
            },
            caretColor:{
                property:"caret-color",
                label: tradCms.caretColor || "Caret color",
                input:{
                    type:"colorPicker"
                }
            },
            clear:{
                property:"clear",
                label: tradCms.clear || "Clear",
                input:{
                    type:"select",            
                    options:["none","left","right","both","initial","inherit"]
                }
            },
            color:{
                property:"color",
                label: tradCms.color || "Color",
                input:{
                    type:"colorPicker"
                }
            },
            columnWidth:{
                label: tradCms.columnWidth || "Column width",
                input: {
                    type: "selectGroup",
                    inputs:[
                        {
                            name: "colMd",
                            icon : "desktop",
                            options: ["1","2","3","4","5","6","7","8","9","10","11","12"]
                            
                        },
                        {
                            name: "colSm",
                            icon : "tablet",
                            options: ["1","2","3","4","5","6","7","8","9","10","11","12"]
                            
                        },
                        {
                            name: "colXs",
                            icon : "mobile",
                            options: ["1","2","3","4","5","6","7","8","9","10","11","12"]
                            
                        }
                    ]
                }
            },
            cursor:{
                moz: false,
                webkit: false,
                property:"cursor",
                label: tradCms.cursor || "Cursor",
                input:{
                    type:"select",
                    options:[
                        "alias","all-scroll","auto","cell","col-resize",
                        "context-menu","copy","crosshair","default","e-resize",
                        "ew-resize","grab","grabbing","help","move","n-resize",
                        "ne-resize","ns-resize","nw-resize","nwse-resize","nwse-resize",
                        "no-drop","none","not-allowed","pointer","progress","row-resize",
                        "s-resize","se-resize","sw-resize","text","URL","vertical-text",
                        "w-resize","wait","zoom-in","zoom-out","initial","inherit"
                    ]
                }
            },
            direction:{
                property:"direction",
                moz: false,
                webkit: false,
                label: tradCms.direction || "Direction",
                input:{
                    type:"select",
                    options:["ltr","rtl","initial","inherit"]
                }
            },
            display:{
                moz: false,
                webkit: false,
                property:"display",
                label: tradCms.display || "Display",
                input:{
                    type:"select",
                    options:[
                        "inline","block","contents","flex","grid","inline-block","inline-flex",
                        "inline-grid","inline-table","list-item","run-in","table",
                        "table-caption","table-column-group","table-header-group","table-footer-group","table-row-group","table-cell","table-column","table-row","none",
                        "initial","inherit"]
                }
            },
            fill:{
                property:"fill",
                label: tradCms.fill || "Fill",
                input:{
                    type:"colorPicker"
                }
            },
            filter:{
                property: "filter", 
                moz: false,
                webkit: true,
                label: tradCms.filtr || "Filter",
                input:{
                    type: "filterCssFunction",
                    options: [
                        "none",
                        "[fn:blur]",
                        "[fn:brightness]",
                        "[fn:contrast]",
                        "[fn:drop-shadow]",
                        "[fn:grayscale]",
                        "[fn:hue-rotate]",
                        "[fn:invert]",
                        "[fn:opacity]",
                        "[fn:saturate]",
                        "[fn:sepia]",
                        "[fn:url]",
                        "initial",
                        "inherit"
                    ]
                }
            },
            float: {
                moz: false,
                webkit: false,
                property:"float",
                label: tradCms.float || "Float",
                input:{
                    type:"select",
                    options: [
                        "none",
                        "left",
                        "right",
                        "initial",
                        "inherit"
                    ]
                }
            },
            flexDirection: {
                moz: true,
                webkit: true,
                property:"flex-direction",
                label: tradCms.flexDirection || "Flex direction",
                input:{
                    type:"select",
                    options: [
                        "row",
                        "row-reverse",
                        "column",
                        "column-reverse",
                        "initial",
                        "inherit"
                    ]
                }
            },
            flexWrap: {
                moz: true,
                webkit: true,
                property:"flex-wrap",
                label: tradCms.flexWrap || "Flex wrap",
                input:{
                    type:"select",
                    options: [
                        "nowrap",
                        "wrap",
                        "wrap-reverse",
                        "initial",
                        "inherit"
                    ]
                }
            },
            font : {
                moz: false,
                webkit: false,
                property:"font",
                label: tradCms.font || "Font",
                icon : "font",
                input : [
                    {
                        type: "selectFont",
                        options: {
                            name: "fontSelect",
                            label: tradCms.selectFont
                        }
                    },
                    {
                        type: "inputFileFont",
                        options: {
                            name: "font",
                            label: tradCms.fontUpload,
                            file: null,
                            accept: [".ttf"]
                        }
                    }
                ]
            },
            fontFamily:{
                moz: false,
                webkit: false,
                property:"font-family",
                label: tradCms.selectFont || "Font family",
                input: {
                    type: "selectFont",
                    options: "costum.fonts",
                    canEdit: true
                }
            },
            fontSize:{
                moz: false,
                webkit: false,
                label: tradCms.fontSize || "Font size",
                property:"font-size",
                input:{
                    responsive: true,
                    type:"number",
                    units: ["px", "%"],
                    rules:"cssHelpers.form.rules.checkLengthProperties"
                }
            },
            fontStyle:{
                moz: false,
                webkit: false,
                label: tradCms.fontStyle || "Font style",
                property:"font-style",
                input:{
                    type:"select",
                    options:["normal","italic","oblique","initial","inherit"]
                }
            },
            fontVariant:{
                moz: false,
                webkit: false,
                property:"font-variant",
                label: tradCms.fontVariant || "Font variant",    
                input:{
                    type:"select",
                    options:["normal","small-caps","initial","inherit"]
                }
            },
            fontWeight:{
                moz: false,
                webkit: false,
                label: tradCms.fontWeight || "Font weight",
                property:"font-weight",
                input:{
                    type:"select",
                    options:[
                        "normal","bold","bolder","lighter","100","200",
                        "300","400","500","600","700","800","900","initial","inherit"
                    ]
                }
            },
            height: {
                moz: false,
                webkit: false,
                label: tradCms.height || "Height",
                property: "height",
                defaultZero: "max-content",
                input: {
                    type: "inputNumberRange",
                    responsive:true,
                    units: ["px", "%", "vh"],
                    rules: "cssHelpers.form.rules.checkLengthProperties",
                } 
            },
            hideOnDesktop : {
                label: tradCms.hideOnDesktop || "hideOnDesktop",
                classValue : "hidden-md hidden-lg",
                input: {
                    type : "inputSimple",
                    inputType: "checkbox",
                    class: "switch"
                }
            },
            hideOnTablet : {
                label: tradCms.hideOnTablet || "hideOnTablet",
                classValue : "hidden-sm",
                input: {
                    type : "inputSimple",
                    inputType: "checkbox",
                    class: "switch"
                }
            },
            hideOnMobil : {
                label: tradCms.hideOnMobil || "hideOnMobil",
                classValue : "hidden-xs",
                input: {
                    type : "inputSimple",
                    inputType: "checkbox",
                    class: "switch"
                }
            },
            languages : {
                label: tradCms.languages || "languages",
                input: {
                    type: "select",
                    tradAuto: true,
                    options: $.map({default: "default", ...themeParams.languages}, function( key, val ) {
                        return { value: val,label: trad[key] }
                    })
                }
            },
            left:{ 
                moz:false,
                webkit: false,
                property:"left",
                label: tradCms.left || "Left",  
                input:{
                    type:"number",
                    units: ["px", "%"],
                    rules:"cssHelpers.form.rules.checkLengthProperties"
                }
            },
            letterSpacing:{
                moz:false,
                webkit: false,
                property:"letter-spacing",
                label: tradCms.letterSpacing || "Letter spacing",
                input:{
                    type:"number",
                    rules:"cssHelpers.form.rules.checkLengthProperties"
                }
            },
            lineHeight:{
                moz:false,
                webkit: false,
                label : tradCms.lineHeight || "Line height",
                property:"line-height",
                input:{
                    type:"number",
                    rules:"cssHelpers.form.rules.checkLengthProperties"
                }
            },
            margin:{
                webkit: false,
                moz: false,
                property:"margin",
                label: tradCms.margin || "Margin",
                input:{
                    rules:"cssHelpers.form.rules.checkLengthProperties",
                    type:"inputGroup",
                    aggregate : false,
                    units: ["px", "%"],
                    linkValue: true,
                    responsive:true,
                    options:[
                        "marginTop",
                        "marginLeft",
                        "marginRight",
                        "marginBottom"
                    ] 
                }
            },
           // margin:cssPropertiesUtils.getSpacingOptions("margin", "Margin"),
            marginBottom:{
                webkit: false,
                moz: false,
                label: tradCms.bottom || "Bottom",
                property:"margin-bottom",
                icon:"toggle-down", 
                input:{
                    type:"number",
                    rules:"cssHelpers.form.rules.checkLengthProperties"
                }
            },
            marginLeft:{
                webkit: false,
                moz: false,
                label: tradCms.left || "Left",
                property:"margin-left",
                icon:"toggle-left", 
                input:{
                    type:"number",
                    rules:"cssHelpers.form.rules.checkLengthProperties"
                }
            },
            marginRight:{
                webkit: false,
                moz: false,
                label: tradCms.right || "Right",
                property:"margin-right",
                icon:"toggle-right", 
                input:{
                    type:"number",
                    rules:"cssHelpers.form.rules.checkLengthProperties"
                }
              
            },
            marginTop:{
                webkit: false,
                moz: false,
                label: tradCms.top || "Top",
                property:"margin-top",
                icon:"toggle-up", 
                input:{
                    type:"number",
                    rules:"cssHelpers.form.rules.checkLengthProperties"
                }
            },
            maxHeight: {
                moz: false,
                webkit: false,
                property : "max-height",
                label: tradCms.maxHeight || "Max height",
                input:{
                    type :"number",
                    rules:"cssHelpers.form.rules.checkLengthProperties"
               }
            },
            minHeight: {
                moz: false,
                webkit: false,
                property : "min-height",
                label: tradCms.minHeight || "Min height",
                input:{
                    type :"number",
                    rules:"cssHelpers.form.rules.checkLengthProperties"
               }
            },
            minWidth: {
                moz: false,
                webkit: false,
                property : "min-width",
                label: tradCms.minWidth || "Min width",
                input:{
                    type :"number",
                    rules:"cssHelpers.form.rules.checkLengthProperties"
               }
            },
            maxWidth: {
                moz: false,
                webkit: false,
                property : "max-width",
                label: tradCms.maxWidth || "Max width",
                input:{
                    type :"number",
                    rules:"cssHelpers.form.rules.checkLengthProperties"
               }
            },
            objectFit:{
                moz: false,
                webkit: false,
                property:"object-fit",
                label: tradCms.objectFit || "Object fit",    
                input:{
                    type:"select",
                    options:["fill","contain","cover","scale-down","none","initial","inherit"]
                }
            },
            padding:{
                webkit: false,
                moz: false,
                property:"padding",
                label: tradCms.padding || "padding",
                input:{
                    type:"inputGroup",
                    rules:"cssHelpers.form.rules.checkLengthProperties",
                    units:["px", "%"],
                    linkValue:true,
                    aggregate : false,
                    responsive:true,
                    options: [
                        "paddingTop",
                        "paddingLeft",
                        "paddingRight",
                        "paddingBottom"
                    ]
                },

            },
            paddingBottom:{
                webkit: false,
                moz: false,
                label: tradCms.bottom || "Bottom",
                property:"padding-bottom",
                icon:"toggle-down", 
                input:{
                    type:"number",
                    rules:"cssHelpers.form.rules.checkLengthProperties"
                }
            },
            paddingLeft:{
                webkit: false,
                moz: false,
                label: tradCms.left || "Left",
                property:"padding-left",
                icon:"toggle-left", 
                input:{
                    type:"number",
                    rules:"cssHelpers.form.rules.checkLengthProperties"
                }
            },
            paddingRight:{
                webkit: false,
                moz: false,
                label: tradCms.right || "Right",
                property:"padding-right",
                icon:"toggle-right", 
                input:{
                    type:"number",
                    rules:"cssHelpers.form.rules.checkLengthProperties"
                }
              
            },
            paddingTop:{
                webkit: false,
                moz: false,
                label: tradCms.top || "Top",
                property:"padding-top",
                icon:"toggle-up", 
                input:{
                    type:"number",
                    rules:"cssHelpers.form.rules.checkLengthProperties"
                }
            },
            overflowX: {
                moz: false,
                webkit: false,
                property:"overflow-x",
                label: tradCms.overflowHorizontal || "Horizontal overflow",
                input:{
                    type:"select",
                    options: [
                        "visible",
                        "hidden",
                        "scroll",
                        "auto",
                        "initial",
                        "inherit"
                    ]
                }
            },
            overflowY: {
                moz: false,
                webkit: false,
                property:"overflow-y",
                label: tradCms.overflowVertical || "Vertical overflow",
                input:{
                    type:"select",
                    options: [
                        "visible",
                        "hidden",
                        "scroll",
                        "auto",
                        "initial",
                        "inherit"
                    ]
                }
            },
            position: {
                moz: false,
                webkit: false,
                property:"position",
                label: tradCms.position || "Position",
                input:{
                    type:"select",
                    options: [
                        "static",
                        "absolute",
                        "fixed",
                        "relative",
                        "sticky",
                        "initial",
                        "inherit"
                    ]
                }
            },
            // columnWidth:cssPropertiesUtils.getWidthOptions ("colomn", "Column width"),
            right:{   
                moz: false,
                webkit: false, 
                property:"right",
                label: tradCms.right || "Right",
                input:{
                    type:"number",
                    units: ["px", "%"],
                    rules:"cssHelpers.form.rules.checkLengthProperties"
                }
            },
            textAlign: {
                moz: false,
                webkit: false, 
                property:"text-align",
                label: tradCms.textalignment || "Text alignment",
                input:{
                    type:"groupButtons",
                    options:[
                        {
                            value:"left",
                            icon:"align-left"
                        },
                        {
                            value:"center",
                            icon:"align-center"
                        },
                        {
                            value:"right",
                            icon:"align-right"
                        }
                    ]
                }
            },
            //cssPropertiesUtils.getAlignOptions("textAlign", "Text align"),
            textDecorationStyle:{
                moz: false,
                webkit: false,
                property:"text-decoration-style",
                label: tradCms.textDecorationStyle || "Text decoration style",
                input:{
                    type:"select",
                    options:["solid", "double", "dotted", "dashed", "wavy", "initial", "inherit"]
                }
            },
            textDecorationLine:{
                property:"text-decoration-line",
                label: tradCms.textDecorationLine || "Text decoration line",
                input:{
                    type:"select",
                    options:["none","underline","overline","line-through","initial","inherit"]
                }
            },
            loaderUrl:{
                label: tradCms.typeLoader || "Type loader",
                input:{
                    type:"select",
                    options:["loading_modal","loader_1","loader_2","loader_3"]
                }
            },
            textDecoration:{
                moz:false,
                webkit:false,
                property:"text-decoration",
                label: tradCms.textDecoration || "Text decoration",
                input:{
                    type:"select",
                    options:["solid", "double", "dotted", "dashed", "wavy", "initial", "inherit"]
                }
            },
            textDecorationColor:{
                moz: true,
                webkit: true,
                property:"text-decoration-color",
                label: tradCms.textDecorationColor || "Text decoration color",
                input:{
                    type:"colorPicker"
                }
            },
            textJustify:{
                moz: true,
                webkit: true,
                label: tradCms.textJustify || "Text justify",
                property:"text-justify",
                input:{
                    type:"select",
                    options:["auto","inter-word","inter-character","none","initial", "inherit"]
                }
            },
            textTransform:{
                property:"text-transform",
                label: tradCms.textTransform || "Text transform",
                input:{
                    type:"select",
                    options:["none","capitalize","uppercase","lowercase","initial","inherit"]
                }
            },
            textShadow: {
                moz: false,
                webkit: false,
                property:"text-shadow",
                label : tradCms.textShadow || "Text shadow",
                input:{
                    type:"inputShadow"
                }
            },
            transitionDelay: {
                moz: true,
                webkit: true,
                property:"transition-delay",
                label: tradCms.transitionDelay || "transition delay",
                input:{
                    type:"number"
                }
            },
            transitionDuration: {
                moz: true,
                webkit: true,
                property:"transition-duration",
                label: tradCms.transitionDuration || "transition-duration",
                input:{
                    type:"number"
                }
            },
            transitionProperty: {
                moz: true,
                webkit: true,
                property:"transition-property",
                label: tradCms.tranistionProperty || "Transition property",
                input:{
                    type:"number",
                    options: [
                        "none",
                        "all",
                        "property",
                        "initial",
                        "inherit"
                    ]
                }
            },
            transitionTimingFunction: {
                moz: true,
                webkit: true,
                property:"transition-timing-function",
                label: tradCms.transitionTimingFunction || "Transition timing function",
                input:{
                    type:"select",
                    options: [
                        "ease",
                        "linear",
                        "ease-in",
                        "ease-out",
                        "ease-in-out",
                        "step-start",
                        "step-end",
                        "[fn:steps]",
                        "[fn:cubic-bezier]",
                        "initial",
                        "inherit"
                    ]
                }
            },
            top:{
                moz: false,
                webkit: false,
                label: tradCms.top || "Top",
                property:"top",
                input:{
                    type:"number",
                    units: ["px", "%"],
                    rules:"cssHelpers.form.rules.checkLengthProperties"
                }
            },
            verticalAlign:{
                moz : false, 
                webkit: false, 
                property : "vertical-align",
                label: tradCms.verticalAlign || "Vertical align",
                input:{
                    type:"select",
                    options:["baseline","length","sub","super","top","text-top","middle","bottom","text-bottom","initial","inherit"]
                }
            },
            visibility:{
                moz : false, 
                webkit: false, 
                property:"visibility",
                label: tradCms.visibility || "Visibility",
                input:{
                    type:"select",
                    options:[
                        "visible","hidden","collapse","initial","inherit"
                    ]
                }
            },
            width: {
                moz: false,
                webkit: false,
                label: tradCms.width || "Width",
                property: "width",
                defaultZero: "auto",
                input: {
                    type: "inputNumberRange",
                    responsive:true,
                    units: ["px", "%", "vw"],
                    rules: "cssHelpers.form.rules.checkLengthProperties",
                } 
            },
            whiteSpace:{
                moz : false, 
                webkit: false, 
                property:"white-space",
                label: tradCms.whiteSpace || "White space",
                input:{
                    type:"select",
                    options:[
                        "normal","nowrap","pre","pre-line","pre-wrap","initial","inherit"
                    ]
                }
            },
            clipPath:{
                type:"inputSimple",
                property:"clip-path",
                options:{
                    name:"clipPath",
                    label: tradCms.clipPath || "Clip-path"
                }
            },
            otherCss : {
                moz: false,
                webkit: false,
                label : tradCms.otherCss || "Other CSS",
                input:{
                    responsive:true,
                    type : "textarea",
                }
            },
            otherClass : {
                moz: false,
                webkit: false,
                label : tradCms.otherClass || "Other Class",
                input:{
                    responsive:false,
                    type : "textarea",
                }
            }
        }
    }