(function(){
    var coreTypes = {},
        extendTypes = {};

    this.CoInput = function(){
        //private properties
        var defaultSettings = {
            container:"",
            inputs:[],
            payload:{},
            customInputType:{},
            i18n:{},
            defaultValues:{},
            onchange: function(name, value, payload){},
            onblur: function(name, value, payload){}
        }

        /** public properties */
        this.settings = $.extend(true, defaultSettings, arguments[0]);
        this.form = [];

        init(this)
    }

    /* PRIVATE METHODS */
    function init(self){
        //destroy previous instance with the same DOM
        destroy(self)
        var settings = self.settings,
            $form = $(settings.container);

        var inputParams = settings.inputs.map(function(input){
            var params = getInputParams(self, input),
                name = params.options?.name || params.type;
            if(settings.defaultValues[name] && typeof params.options.defaultValue == "undefined")
                params.options = $.extend({}, params.options, { defaultValue:settings.defaultValues[name] })
            return params;
        })
        renderInputs(self, $form, inputParams);

        $form.on("valueChange", function(e, params){
            e.stopPropagation()
            settings.onchange(
                params.name, 
                params.value, 
                $.extend({}, settings.payload, params.payload)
            )
        })

        $form.on("inputBlur", function(e, params){
            e.stopPropagation();
            settings.onblur(
                params.name,
                params.value,
                $.extend({}, settings.payload, params.payload)
            )
        })
    }

    function destroy(self){
        $(self.settings.container).off("valueChange").html("")
        $(self.settings.container).off("inputBlur").html("")
    }

    function getInputParams(self, input){
        var settings = self.settings,
            inputParams = input;

        if(typeof inputParams === "string"){
            inputParams = { 
                type:inputParams,
                options:{
                    //transforme camel case to snake case
                    name: inputParams//inputParams.replace(/([a-z])([A-Z])/g, '$1-$2').toLowerCase()
                }
            }
        }

        var name = inputParams.options?.name || inputParams.type;
        if(settings.i18n[name])
            inputParams.options = $.extend({}, inputParams.options, { label:settings.i18n[name] })

        return inputParams;
    }

    function getInput(self, inputParams){
        var type = inputParams.type,
            options = inputParams.options || {};

        var cloneObject = function(o){
            return $.extend(true,{},o)
        }

        if(coreTypes[type])
            return coreTypes[type](self, cloneObject(options))
        else if(extendTypes[type] && coreTypes[extendTypes[type].baseType]){
            var extendType = extendTypes[type],
                baseType = extendType.baseType,
                baseOptions = extendType.getBaseTypeOptions(options)

            return coreTypes[baseType](self, cloneObject(baseOptions))
        }else if(self.settings.customInputType[type]){
            return self.settings.customInputType[type](self, cloneObject(options))
        }

        return null;
    }

    function renderInputs(self, $container, inputParams){
        var inputParams = [].concat(inputParams);

        inputParams.forEach(function(params){
            if(Array.isArray(params)){
                var $row = $(`<div></div>`)
                $row.css({
                    display: "grid",
                    gridTemplateColumns: `repeat(${params.length}, 1fr)`,
                    gridGap: "5px"
                })
                $container.append($row);

                params.forEach(function(childParams){
                    var $childInput = getInput(self, childParams)
                    if($childInput){
                        $row.append($childInput)
                        $childInput.on("valueChange", function(e, values){
                            e.stopPropagation()
                            $container.trigger("valueChange", values)
                        })
                        $childInput.on("inputBlur", function(e, values){
                            e.stopPropagation()
                            $container.trigger("inputBlur", values)
                        })
                    }
                })
            }else{
                var $input = getInput(self, params)
                if($input){
                    $container.append($input)
                    $input.on("valueChange", function(e, values){
                        e.stopPropagation()
                        $container.trigger("valueChange", values)
                    })
                    $input.on("inputBlur", function(e, values){
                        e.stopPropagation()
                        $container.trigger("inputBlur", values)
                    })
                }
            }
        })

        return $container;
    }

    function generateUUID(){
        return Math.random().toString(36).slice(-6)
    }

    function separateValueAndUnit(value) {
        if (typeof value == "number") {
            return { number: value, unit: "" }
        } else {
            const matches = value.match(/^(-?\d*\.?\d+)(\D+)?$/);
            if (matches) {
                return { number: parseFloat(matches[1]), unit: matches[2] };
            } else {
                return { number: parseFloat(value), unit: "" };
            }
        }
    }

    function isValidColorName(color) {
        var colorList = ["aliceblue","antiquewhite","aqua","aquamarine","azure","beige","bisque","black","blanchedalmond","blue","blueviolet","brown","burlywood","cadetblue","chartreuse","chocolate","coral","cornflowerblue","cornsilk","crimson","cyan","darkblue","darkcyan","darkgoldenrod","darkgray","darkgreen","darkgrey","darkkhaki","darkmagenta","darkolivegreen","darkorange","darkorchid","darkred","darksalmon","darkseagreen","darkslateblue","darkslategray","darkslategrey","darkturquoise","darkviolet","deeppink","deepskyblue","dimgray","dimgrey","dodgerblue","firebrick","floralwhite","forestgreen","fuchsia","gainsboro","ghostwhite","gold","goldenrod","gray","green","greenyellow","grey","honeydew","hotpink","indianred","indigo","ivory","khaki","lavender","lavenderblush","lawngreen","lemonchiffon","lightblue","lightcoral","lightcyan","lightgoldenrodyellow","lightgray","lightgreen","lightgrey","lightpink","lightsalmon","lightseagreen","lightskyblue","lightslategray","lightslategrey","lightsteelblue","lightyellow","lime","limegreen","linen","magenta","maroon","mediumaquamarine","mediumblue","mediumorchid","mediumpurple","mediumseagreen","mediumslateblue","mediumspringgreen","mediumturquoise","mediumvioletred","midnightblue","mintcream","mistyrose","moccasin","navajowhite","navy","oldlace","olive","olivedrab","orange","orangered","orchid","palegoldenrod","palegreen","paleturquoise","palevioletred","papayawhip","peachpuff","peru","pink","plum","powderblue","purple","red","rosybrown","royalblue","saddlebrown","salmon","sandybrown","seagreen","seashell","sienna","silver","skyblue","slateblue","slategray","slategrey","snow","springgreen","steelblue","tan","teal","thistle","tomato","turquoise","violet","wheat","white","whitesmoke","yellow","yellowgreen", "transparent"];
        return colorList.indexOf(color.toLowerCase()) !== -1;
    } 
    
    function isValidColor(color) {
        var hexColor = /^#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$/,
        hslColor = /^hsl\(\s*\d{1,3}(\.\d+)?\s*,\s*\d{1,3}%\s*,\s*\d{1,3}%\s*\)$/,
        rgbColor = /^rgb\(\s*\d{1,3}%?\s*,\s*\d{1,3}%?\s*,\s*\d{1,3}%?\s*\)$/,
        rgbaColor = /^rgba\(\s*\d{1,3}%?\s*,\s*\d{1,3}%?\s*,\s*\d{1,3}%?\s*,\s*(0\.\d+|1|0)\s*\)$/;
        return isValidColorName(color) ||  hexColor.test(color) || hslColor.test(color) || rgbColor.test(color) || rgbaColor.test(color);
    }   

    function getUnitsSelector($owner, options, value) {
        let optionsUnits = "", unit = ""; 
        Object.values(options.units).forEach(function(unitValue){
            optionsUnits += `<option value='${unitValue}' ${(value == unitValue) ? 'selected' : ''}>${unitValue}</option>`;
        });
        $unitsOptions = $(`
            <div class="units-form form-inline">
                <div class="form-group">
                    <select class="form-control select-units-value">
                        ${optionsUnits}
                    </select>
                </div>
            </div>
        `); 
        unit = $unitsOptions.find(".select-units-value").val();
        $unitsOptions.find("select").change(function(){
            $owner.trigger("unitChange", {
                value: $(this).val()
            })
        }) 
        return { view: $unitsOptions, unitValue: unit };
    }

    function autoTranslate(textTotranslate, langTotranslate) {
        return new Promise(function(resolve, reject) {
            if (textTotranslate != "" && langTotranslate != "") {
                dataHelper.autoTranslateDeepl(textTotranslate, langTotranslate, function(error, translatedText) {
                    if (error) {
                        reject(error);
                    } else {
                        resolve(translatedText);
                    }
                })
            }
        })
    }

    function traductionSelector($owner, inputForm, defaultInputValue = {}, inputWithoutTradAuto = true, nameInput, payloadInput) {
        let allLanguages = {},
            allLanguagesSelectedInDb = dataHelper.getAllLanguageInDb(),
            generateID = generateUUID(),
            tradAuto = false,
            langSelected = costum.langCostumActive || "fr", 
            isChangedByInput = false,
            isSelectLanguage = false,
            valueForInput = "",
            dropDownItem = "";
            
        for (var i = 0; i < allLanguagesSelectedInDb.length; i++) {
            var keyLang = allLanguagesSelectedInDb[i].toUpperCase();
            if (themeParams.DeeplLanguages.hasOwnProperty(keyLang)) {
                allLanguages[keyLang] = themeParams.DeeplLanguages[keyLang];
            }
        }

        if (typeof defaultInputValue == "object" && notEmpty(defaultInputValue)) {
            if (typeof defaultInputValue["languageSelected"] != "undefined") {
                langSelected = defaultInputValue["languageSelected"];
            }
            valueForInput = defaultInputValue.hasOwnProperty(langSelected) ? defaultInputValue[langSelected] : "";
            if (valueForInput != "") {
                inputForm.val(valueForInput);
            } else {
                inputForm.val(defaultInputValue[Object.keys(defaultInputValue)[0]]);
            }
        }

        $.each(allLanguages, function(key, value) {
            dropDownItem += `<button class="btn dropdown-item${ (langSelected === key.toLowerCase()) ? ' active': '' }" type="button" data-value="${key.toLowerCase()}"${ (langSelected === key.toLowerCase()) ? ' disabled': '' }><span style="font-size: 20px; margin-right: 10px">${value["icon"]}</span> ${tradCms[value["label"]]}</button>`;
        });

        var $html = $(`
            ${(Object.keys(allLanguages).length > 1) ? `
            <div class="dropdown co-input-translate-form">
                <button class="btn btn-primary dropdown-toggle btn-flag-translate-active" type="button" id="dropdownForTranslate" data-toggle="dropdown">
                    <span style="font-size: 20px; margin-right: 5px">${themeParams.DeeplLanguages[langSelected.toUpperCase()]["icon"]}</span> ${langSelected}
                </button>
                <div class="dropdown-menu${(!inputWithoutTradAuto) ? ' dropdown-lang-without-tradAuto' : ''}" aria-labelledby="dropdownForTranslate">
                    <div class="translate-form-select">
                        ${(inputWithoutTradAuto) ? 
                            `<div class="co-input-language-props switch">
                                <label for="languageProps_${generateID}" class="label-for-language-props_${generateID}">Traduction automatique</label>
                                <input id="languageProps_${generateID}" type="checkbox" name="inputInset" class="form-control language-props-input">
                                <label for="languageProps_${generateID}"></label>
                            </div>` : ''
                        }
                        ${dropDownItem}
                    </div>
                </div>
            </div>
            ` : ''}
        `);

        $html.find(`.co-input-language-props .label-for-language-props_${generateID}`).click(function() {
            $(this).closest(".co-input-translate-form").addClass("open");
        })

        $html.find(`.co-input-language-props #languageProps_${generateID}`).click(function() {
            $(this).closest(".co-input-translate-form").addClass("open");
            tradAuto = ($(this).is(":checked")) ? true : false;
        })
        
        $html.find(".translate-form-select button.dropdown-item").click(function() {
            langSelected = $(this).data("value");
            $html.find(".translate-form-select button.dropdown-item").removeClass("active").removeAttr("disabled");
            $(this).addClass("active").attr("disabled", "disabled");
            $html.find(".btn-flag-translate-active").html(`<span style="font-size: 20px; margin-right: 5px">${themeParams.DeeplLanguages[langSelected.toUpperCase()]["icon"]}</span> ${langSelected}`);
            isSelectLanguage = true;
            valueForInput = defaultInputValue.hasOwnProperty(langSelected) ? defaultInputValue[langSelected] : "";
            if (tradAuto && inputForm.val() != "") {
                $('<i class="loader-input"></i>').insertAfter(inputForm);
                inputForm.prop('disabled', true);
                autoTranslate(inputForm.val(), langSelected).then(textTranslated => {
                    inputForm.val(textTranslated);
                    $owner.trigger("selectLanguage");
                    triggerCustomEvents("inputBlur", nameInput, textTranslated, payloadInput, $owner);
                    inputForm.prop('disabled', false);
                    $('i.loader-input').remove();
                }).catch(error => {
                    mylog.log("translate Error", error);
                });
            } else {
                if (valueForInput != "") {
                    inputForm.val(valueForInput);
                } else {
                    inputForm.val(defaultInputValue[Object.keys(defaultInputValue)[0]]);
                }
                $owner.trigger("selectLanguage");
            }
        });

        inputForm.on("keydown", function () { 
            isChangedByInput = true;
        })

        $owner.on("valueChange", function(e, data) {
            if (isSelectLanguage) {
                data.value = { translate: true, text: data.value, lang: langSelected, tradAuto: tradAuto, inputForm: inputForm };
                if (isChangedByInput) {
                    data.value = { isChangedByInput: isChangedByInput, ...data.value };
                }
                isChangedByInput = false;
            }
            else {
                data.value = { translate: false, text: data.value, lang: langSelected };
                if (isChangedByInput) {
                    data.value = { isChangedByInput: isChangedByInput, ...data.value };
                }
                isChangedByInput = false;
            }
            if (!notEmpty(defaultInputValue))
                defaultInputValue = {};
            defaultInputValue[langSelected] = data.value["text"];
            if (tradAuto) {
                tradAuto = false;
                isSelectLanguage = false;
                $html.find(`.co-input-language-props #languageProps_${generateID}`).prop('checked', false);
            }
        })

        $owner.on("inputBlur", function(e, data) {
            if (isSelectLanguage)
                data.value = { translate: true, text: data.value, lang: langSelected, tradAuto: tradAuto, inputForm: inputForm };
            else 
                data.value = { translate: false, text: data.value, lang: langSelected };
            tradAuto = false;
            isSelectLanguage = false;
            $html.find(`.co-input-language-props #languageProps_${generateID}`).prop('checked', false);
        })
        
        return $html;
    }

    function selectViewMode($owner) {
        var generateID = generateUUID(),
            viewSelected = costumizer.mode,
            modeView = "",
            viewMode = {"md": "desktop", "sm": "tablet", "xs": "mobile"},
            dropdownItem = "";
            
        $.each(viewMode, function(key, value) {
            dropdownItem += `<button class="btn dropdown-item${ (viewSelected === key) ? ' active': '' }" type="button" data-value="${key}" data-view="${value}"${ (viewSelected === key) ? ' disabled': '' }><i class="fa fa-${value}"></i> ${value}</button>`;
        })

        var $html = $(`
            <div class="dropdown co-input-select-view-mode-form">
                <button class="btn btn-primary dropdown-toggle btn-view-mode-active" type="button" id="dropdownForViewMode_${generateID}" data-toggle="dropdown">
                    <i class="fa fa-${viewMode[viewSelected]} icon-mod-view"></i>
                    <i class="fa fa-chevron-down icon-select-down"></i> 
                </button>
                <div class="dropdown-menu" aria-labelledby="dropdownForViewMode_${generateID}">
                    <div class="view-mode-form-select">
                        ${dropdownItem}
                    </div>
                </div>
            </div>
        `);

        $html.find(".view-mode-form-select button.dropdown-item").click(function() {
            viewSelected = $(this).data("value");
            modeView = $(this).data("view");
            $html.find(".view-mode-form-select button.dropdown-item").removeClass("active").removeAttr("disabled");
            $(this).addClass("active").attr("disabled", "disabled");
            $html.find(".btn-view-mode-active").html(`<i class="fa fa-${modeView} icon-mod-view"></i><i class="fa fa-chevron-down icon-select-down"></i>`);
            parent.$('.btn-costumizer-viewMode[data-mode='+viewSelected+']').click();
        });

        return $html;
    }

    function previewLink(defaultValue) {

        var $html = $(`
            <div class="dropdown co-input-select-view-mode-form">
                <a href="${defaultValue}" class="previewMode-view" target="_blank" >
                    <i class="fa fa-eye icon-preview">  ${tradCms.showPage}</i>
                </a>     
            </div>
        `);

        return $html;
    }

    function getLinkValue($owner, options, linked = false){
        $linkValueOptions=$(`<button class='btn btn-primary btn-links ${ (linked) ? "linked" : "" }' data-linked='${linked}' type='button'><i class='fa fa-link'></i></button>`); 
        $linkValueOptions.click(function() {
            if ($(this).attr("data-linked") === "false") {
                $(this).attr("data-linked", "true");
                $(this).addClass("linked");
            } else {
                $(this).attr("data-linked", "false");
                $(this).removeClass("linked");
            }
            $owner.trigger("linkValueChange", {
                value:  $(this).attr("data-linked")
            })
        }) 
        return $linkValueOptions;
    }

    function getListImageDocuments() {
        let allImages = {};
        ajaxPost(
            null,
            baseUrl+"/"+moduleId+"/document/getlistdocumentswhere",
            {
                params : {
                    id: costum.contextId,
                    type: costum.contextType,
                    doctype : "image",
                },
                limit: 100,
                indexMin : 0
            },
            function(data){
                allImages = data;
            },
            null,
            "json",
            {
                async: false
            }
        )
        return allImages;
    }

    function getColorPalette() {
        return [
            dataHelper.getDefaultColorCostum(),
            ["#000000", "#444444", "#5b5b5b", "#999999", "#bcbcbc", "#eeeeee", "#f3f6f4", "#ffffff"],
            ["#f44336", "#744700", "#ce7e00", "#8fce00", "#2986cc", "#16537e", "#6a329f", "#c90076"],
            ["#f4cccc", "#fce5cd", "#fff2cc", "#d9ead3", "#d0e0e3", "#cfe2f3", "#d9d2e9", "#ead1dc"],
            ["#ea9999", "#f9cb9c", "#ffe599", "#b6d7a8", "#a2c4c9", "#9fc5e8", "#b4a7d6", "#d5a6bd"],
            ["#e06666", "#f6b26b", "#ffd966", "#93c47d", "#76a5af", "#6fa8dc", "#8e7cc3", "#c27ba0"],
            ["#cc0000", "#e69138", "#f1c232", "#6aa84f", "#45818e", "#3d85c6", "#674ea7", "#a64d79"],
            ["#990000", "#b45f06", "#bf9000", "#38761d", "#134f5c", "#0b5394", "#351c75", "#741b47"],
            ["#660000", "#783f04", "#7f6000", "#274e13", "#0c343d", "#073763", "#20124d", "#4c1130"]
        ];
    }

    /* STATIC METHODS */
    this.CoInput.extendType = function(baseType, typeName, getBaseTypeOptions){
        if(coreTypes[baseType] && typeof getBaseTypeOptions === "function"){
            extendTypes[typeName] = {
                baseType:baseType,
                getBaseTypeOptions:getBaseTypeOptions
            }
        }
    }

    /* PUBLIC METHODS */
    this.CoInput.prototype.destroy = function(){
        destroy(this)
    }
    
    /* TRIGGER EVENTS FUNCTION */
    function triggerCustomEvents(eventType, name, value, payload, $owner) {
        var eventData = {
            name: name,
            value: value,
            payload: payload
        }
        $owner.trigger(eventType, eventData);
    }

    /* SELECT SORTABLE */
    var selectSortableObj = {
        init: function(mySelect, config = {}, data, $owner) {
            $(mySelect).select2(config).on("change", function(evt) {
                var id = "";
                if (typeof evt.added != "undefined")
                    id = evt.added.id;
                else if (typeof evt.removed != "undefined")
                    id = evt.removed.id;
                var element = $(this).children("option[value='"+id+"']");
                selectSortableObj.moveElementToEndOfParent(element);
            })
            var eltChoise = $(mySelect).parent().find("ul.select2-choices");
            eltChoise.sortable({
                containment: 'parent',
                update: function() {
                    selectSortableObj.orderSortedValues(mySelect);
                    $owner.trigger("valueChange", {
                        name: data.name || $(mySelect).attr("name"),
                        value: $(mySelect).val(),
                        payload: data.payload || {},
                    })
                }
            }); 
        },
        moveElementToEndOfParent : function(element) {
            var parent = element.parent();
            element.detach();
            parent.append(element);
        },
        orderSortedValues : function(myselect) {
			var value = ''
			$(myselect).parent().find("ul.select2-choices").children("li").children("div").each(function(i, obj){
				var element = $(myselect).children('option').filter(function () { 
					return $(this).html() == $(obj).text() 
				});
				selectSortableObj.moveElementToEndOfParent(element)
			});
		}
    }

    /**
     * CORE TYPES DEFINITIONS
     */
    //INPUT SIMPLE
    coreTypes.inputSimple = function(context, options) {
        var name = options.name || "inputSimple",
            payload = options.payload,
            translate = options.translate,
            defaultValue = "",
            isViewPreview = options.viewPreview,
            tradAuto = true,
            inputType = "",
            placeholder = options.placeholder,
            generateID = generateUUID(),
            classInput = (typeof options.class != "undefined") ? options.class : "",
            classContainer = (typeof options.classContainer != "undefined") ? options.classContainer : "",
            valueCheckbox = "";
        if(options.defaultValue){
            defaultValue = options.defaultValue;
        } else if (context.settings && context.settings.defaultValues) {
            var values = [];
            if (typeof options.payload != "undefined" && typeof options.payload.sectionPath != "undefined") {
                values = jsonHelper.getValueByPath(context.settings.defaultValues, options.payload.sectionPath);
            } else if ( typeof context.settings.defaultValues.css != "undefined" && typeof cssHelpers.json[name] != "undefined" ){
                values = context.settings.defaultValues.css
            } else {
                values = context.settings.defaultValues;
            }
            defaultValue = typeof values != "undefined" && typeof values[name] != "undefined" ? values[name] : "";
        }

        if (typeof options.inputType != "undefined")
            inputType = options.inputType;
        else 
            inputType = options.type;

        var $html = $(`
            <div class="form-group input-group-sm ${(typeof translate != "undefined") ? 'contain-input-translate' : ""} ${classInput} ${classContainer}">
                ${ (typeof translate != "undefined") ? '<div class="co-input-with-label-translate">' : "" }
                    ${ options.label ? `<label class="switch-text-label" for="${options.name}_${generateID}">${options.label}</label>`:"" }
                ${ (typeof translate != "undefined") ? '</div>' : ""}
                <input 
                    type="${inputType || "text"}" 
                    name="${options.name}"
                    class="form-control ${classInput} ${(inputType === 'checkbox') ? 'co-input-inputSimple-input-checkbox' : 'co-input-inputSimple-input-text'}"
                    ${ typeof defaultValue == 'boolean' && defaultValue == true ? 'checked' : `value = '${escapeHtml(defaultValue)}'` }
                    ${ inputType==="checkbox" && classInput.includes("switch") ? `id="${options.name}_${generateID}"`:"" }
                    ${ typeof placeholder != "undefined" ? `placeholder="${placeholder}"`: ""}
                >
                ${ inputType==="checkbox" && classInput.includes("switch") ? `<label  for="${options.name}_${generateID}"></label>`:"" }
                ${ (isViewPreview) ? "<div class='co-input-preview'></div>" : "" }
            </div>
        `)

        if (isViewPreview) {
            $html.find(".co-input-preview").append(previewLink(defaultValue));
        }

        if ((typeof options.icon != "undefined") && options.icon != ""){
            $html.find("label").prepend(`
                <i class="fa fa-${options.icon}"></i> 
            `)
        }

        if (typeof translate != "undefined") {
            if(typeof translate === "object" && typeof translate.auto != "undefined") {
                tradAuto = translate.auto;
            }
            $html.find('.co-input-with-label-translate').append(traductionSelector($html, $html.find(".co-input-inputSimple-input-text"), defaultValue, tradAuto, name, payload));
        }

        //remove all inused events handlers
        ["keyup", "focusin", "focusout"].forEach(function(eventName){
            $html.find("input").off(eventName).on(eventName, function(e){ e.stopImmediatePropagation() })
        })

        $html.on("selectLanguage", function() {
            triggerCustomEvents("valueChange", name, $html.find(".co-input-inputSimple-input-text").val(), payload, $html);
        });

        $html.find(".co-input-inputSimple-input-checkbox").change(function() {
            valueCheckbox = ($(this).is(":checked")) ? true : false;
            triggerCustomEvents("valueChange", name, valueCheckbox, payload, $html);
            triggerCustomEvents("inputBlur", name, valueCheckbox, payload, $html);
        })

        $html.find(".co-input-inputSimple-input-text").on("input", function() {
            triggerCustomEvents("valueChange", name, $(this).val(), payload, $html);
        })

        let inputValue = defaultValue;
        $html.find(".co-input-inputSimple-input-text").on("blur", function() {
            if ($html.find(".co-input-inputSimple-input-text").val() !== "" && inputValue != $html.find(".co-input-inputSimple-input-text").val()) {
                triggerCustomEvents("inputBlur", name, $(this).val(), payload, $html);
                inputValue = $(this).val();
            }
            $html.find("a").attr("href",$(this).val());
        })

        $html.val = function(){
            if (inputType === "checkbox") {
                valueCheckbox = ($html.find(".co-input-inputSimple-input-checkbox").is(":checked")) ? true : false;
                return {
                    name:name, 
                    value: valueCheckbox
                }
            } else {
                return {
                    name:name, 
                    value: $html.find(".co-input-inputSimple-input-text").val()
                }
            }
        }

        return $html;
    }

    //INPUT NUMBER
     coreTypes.number = function(context, options){
        var name = options.name || "inputNumber",
            payload = options.payload,
            classInput = (typeof options.class != "undefined") ? options.class : "",
            unitValueSelected = "",
            inputValue = "",
            isResponsive = options.responsive,
            defaultValue = "";

        if (typeof options.filterValue == "undefined") 
            options.filterValue = cssHelpers.form.rules.checkLengthProperties;

        if(options.defaultValue){
            unitValueSelected = notEmpty(separateValueAndUnit(options.defaultValue)) ? separateValueAndUnit(options.defaultValue)["unit"] : "";
            defaultValue = notEmpty(separateValueAndUnit(options.defaultValue)) && Number(separateValueAndUnit(options.defaultValue)["number"]) != 0 ? separateValueAndUnit(options.defaultValue)["number"] : "";
        } else if (typeof options["desactivateEvent"] == "undefined" && context.settings && context.settings.defaultValues) {
            if (typeof options.payload != "undefined" && typeof options.payload.sectionPath != "undefined") {
                values = jsonHelper.getValueByPath(context.settings.defaultValues, options.payload.sectionPath);
            } else if ( typeof context.settings.defaultValues.css != "undefined"  && typeof cssHelpers.json[name] != "undefined" ){
                values = context.settings.defaultValues.css;
            } else {
                values = context.settings.defaultValues;
            }
            unitValueSelected = typeof values != "undefined" && typeof values[name] != "undefined" && notEmpty(separateValueAndUnit(values[name])) ? separateValueAndUnit(values[name])["unit"] : "";
            defaultValue = typeof values != "undefined" && typeof values[name] != "undefined" && notEmpty(separateValueAndUnit(values[name])) && Number(separateValueAndUnit(values[name])["number"]) != 0 ? separateValueAndUnit(values[name])["number"] : "";
        }

        var $html = $(`
            <div class="form-group input-group-sm ${classInput} co-input-number-content">
                <div class="co-input-number">
                    ${ (isResponsive) ? "<div class='co-input-label-with-mode'>" : "" }
                        ${ options.label ? `<label>${options.label}</label>`:"" }
                    ${ (isResponsive) ? "</div>" : "" }
                </div>
                <input 
                    type="${options.type || "number"}" 
                    name="${options.name}"
                    class="form-control ${classInput}"
                    min="${options.minValue || 0 }"
                    value="${defaultValue}"
                    ${ options.type=="checkbox" && classInput =="switch" ? `id="${options.name}"`:"" }
                >
                ${ options.type=="checkbox" && classInput=="switch" ? `<label  for="${options.name}"></label>`:"" }
            </div>
        `)

        if (isResponsive) {
            $html.find(".co-input-label-with-mode").append(selectViewMode($html))
        }

        if ((typeof options.icon != "undefined") && options.icon != ""){
            $html.find("label").prepend(`
                <i class="fa fa-${options.icon}"></i> 
            `)
        }

        options.units ? $html.find('.co-input-number').append(getUnitsSelector($html, options, unitValueSelected).view) : "";


        //remove all inused events handlers
        ["keyup", "focusin", "focusout"].forEach(function(eventName){
            $html.find("input").off(eventName).on(eventName, function(e){ e.stopImmediatePropagation() })
        })

        $html.on("unitChange", function(e, data){
            unitValueSelected = data.value;
            let hasValue = false, value = "";
            if ($html.find("input").val() != "") {
                hasValue = true;
                value = $html.find("input").val();
            }
            if (hasValue) {
                triggerCustomEvents("valueChange", name, options.filterValue(value, unitValueSelected), payload, $html);
                triggerCustomEvents("inputBlur", name, options.filterValue(value, unitValueSelected), payload, $html);
            }
        })

        inputValue = $html.find("input").val();
        if (typeof options["desactivateEvent"] == "undefined") {
            unitValueSelected = $html.find(".select-units-value").val();
            $html.find("input").on("input", function() {
                triggerCustomEvents("valueChange", name, options.filterValue($(this).val(), unitValueSelected), payload, $html);
            })
            $html.find("input").on("click", function() {
                $(this).focus();
            });
            $html.find("input").on("blur", function() {
                if (Number(inputValue) !== Number($(this).val())) {
                    triggerCustomEvents("inputBlur", name, options.filterValue($(this).val(), unitValueSelected), payload, $html);
                    inputValue = $(this).val();
                }
            })
        }

        $html.val = function(){
            return {
                name:name, 
                value: options.filterValue($html.find("input").val(), unitValueSelected)
            }
        }

        return $html;
    }

    //INPUT INPUT_NUMBER_RANGE
    coreTypes.inputNumberRange = function(context, options) {
        var name = options.name || 'inputNumberRange',
            payload = options.payload,
            classInput = options.class ? options.class : "",
            unitValueSelected = "",
            defaultValue = "",
            isResponsive = options.responsive,
            inputValue = "";

        if (typeof options.filterValue == "undefined") 
            options.filterValue = cssHelpers.form.rules.checkLengthProperties;

        if(options.defaultValue){
            unitValueSelected = notEmpty(separateValueAndUnit(options.defaultValue)) ? separateValueAndUnit(options.defaultValue)["unit"] : "";
            defaultValue = notEmpty(separateValueAndUnit(options.defaultValue)) ? separateValueAndUnit(options.defaultValue)["number"] : "";
        } else if (context.settings && context.settings.defaultValues) {
            if (typeof options.payload != "undefined" && typeof options.payload.sectionPath != "undefined") {
                values = jsonHelper.getValueByPath(context.settings.defaultValues, options.payload.sectionPath);
            } else if ( typeof context.settings.defaultValues.css != "undefined"  && typeof cssHelpers.json[name] != "undefined" ){
                values = context.settings.defaultValues.css
            } else {
                values = context.settings.defaultValues;
            }
            unitValueSelected = typeof values != "undefined" && typeof values[name] != "undefined" && notEmpty(separateValueAndUnit(values[name])) ? separateValueAndUnit(values[name])["unit"] : "";
            defaultValue = typeof values != "undefined" && typeof values[name] != "undefined" && notEmpty(separateValueAndUnit(values[name])) ? separateValueAndUnit(values[name])["number"] : "";
        }

        var $html = $(`
            <div class="form-group input-group-sm ${classInput}">
                <div class="co-input-number-range">
                    <div class="co-input-number-range-title co-input-label-with-mode">
                        ${ options.label ? `<label for="${options.name}">${options.label}</label>`:"" }
                    </div>
                </div>
                <div class="co-input-number-range-body">
                    <input type="range" class="range" ${options.minRange ? `min=${options.minRange}` : ""} ${options.maxRange ? `max=${options.maxRange}` : ""} name="rangeInputNumber" value="${ !isNaN(defaultValue) ? defaultValue : "" }">
                    <input type="number" class="form-control form-control-sm ${classInput}" name="${options.name}" value="${ !isNaN(defaultValue) ? defaultValue : "" }" ${typeof options.defaultZero != "undefined" ? `placeholder="${options.defaultZero}"` : ""}>
                </div>
            </div>
        `)

        if (isResponsive) {
            $html.find(".co-input-label-with-mode").append(selectViewMode($html))
        }

        if ($.inArray(defaultValue, ["0", ""]) >= 0 || isNaN(defaultValue)) {
            defaultValue = 0;
            $html.find("input[type='number']").val("");
            $html.find("input[type='range']").val(0).attr("value", 0);
        }

        var changeMinMax = function(value) {
            if (unitValueSelected === "px") {
                $html.find("input[type='range']").attr({"min": 0, "max": 1200});
            } else if (unitValueSelected === "%") {
                $html.find("input[type='range']").attr({"min": 0, "max": 100});
            } else {
                $html.find("input[type='range']").attr({"min": typeof options.minRange != "undefined" ? options.minRange : 0, "max": typeof options.maxRange != "undefined" ? options.maxRange : 1200});
            }
            if (value != "")
                $html.find("input[type='range']").val(value).attr("value", value);
        }

        var getValue = function(value) {
            if ($.inArray(value, ["0", ""]) >= 0 || isNaN(value)) {
                typeof options.defaultZero != "undefined" ? value = options.defaultZero : value;
            }
            return value;
        }

        if(!options.minRange && !options.maxRange){
            changeMinMax(defaultValue);
        }

        options.units ? $html.find('.co-input-number-range').append(getUnitsSelector($html, options, unitValueSelected).view) : "";

        $html.on("unitChange", function(e, data){
            unitValueSelected = data.value;
            let hasValue = false, value = "";
            if ($html.find("input[type='number']").val() != "") {
                hasValue = true;
                value = $html.find("input[type='number']").val();
                if (unitValueSelected == "%" && Number(value) > 100) {
                    $html.find("input[type='number']").val(100);
                    $html.find("input[type='range']").val(100).attr("value", 100);
                    value = 100;
                }
            }
            changeMinMax(value);
            if (hasValue) {
                triggerCustomEvents("valueChange", name, options.filterValue(getValue(value), unitValueSelected), payload, $html);
                triggerCustomEvents("inputBlur", name, options.filterValue(getValue(value), unitValueSelected), payload, $html);
            }
        })

        unitValueSelected = $html.find(".select-units-value").val();

        $html.find("input[type='range']").on("input", function() {
            ($(this).val() == 0) ? $html.find("input[type='number']").val("") : $html.find("input[type='number']").val($(this).val());
            $(this).attr("value", $(this).val());
            triggerCustomEvents("valueChange", name, options.filterValue(getValue($(this).val()), unitValueSelected), payload, $html);
        })
        $html.find("input[type='range']").change(function() {
            inputValue = $(this).val();
            triggerCustomEvents("inputBlur", name, options.filterValue(getValue($(this).val()), unitValueSelected), payload, $html);
        })

        inputValue = $html.find("input[type='number']").val();

        $html.find("input[type='number']").on("input", function() {
            if ($.inArray($(this).val(), ["0", ""]) >= 0 || isNaN($(this).val()))
                $html.find("input[type='range']").val(0).attr("value", 0);
            else
                $html.find("input[type='range']").val(Number($(this).val())).attr("value", $(this).val());
            
            triggerCustomEvents("valueChange", name, options.filterValue(getValue($(this).val()), unitValueSelected), payload, $html);
        })

        $html.find("input[type='number']").on("blur", function() {
            if (Number(inputValue) !== Number($(this).val())) {
                triggerCustomEvents("inputBlur", name, options.filterValue(getValue($(this).val()), unitValueSelected), payload, $html);
                inputValue = $(this).val();
            }
        })

        $html.val = function(){
            return {
                name: name, 
                value: options.filterValue(getValue($html.find("input[type='number']").val()), unitValueSelected)
            }
        }
    
        return $html;
    }

    //SELECT
    coreTypes.select = function(context, options){
        var name = options.name || "select",
            isSelect2 = options.isSelect2 || false,
            configForSelect2 = options.configForSelect2 || {},
            payload = options.payload, 
            defaultValue = "",
            tradAutoForLanguage = options.tradAuto || false,
            viewSelectLanguage = "",
            label = options.label,
            classInput = (typeof options.class != "undefined") ? options.class : "" ;

        if (tradAutoForLanguage) {
            viewSelectLanguage = `
                <div class="select-form-traduction">
                    <label for="traduction">Traduction</label>
                    <select class="form-control select-language-props" id="traduction">
                        <option value='manual' selected>Manuelle</option>
                        <option value='auto'>Automatique</option>
                    </select>
                </div>
            `;
        }

        var $html = $(`
            <div class="form-group input-group-sm ${classInput}">
                ${(tradAutoForLanguage) ? "<div class='co-input-with-select-lang-props'>" : ""}
                    ${ label ? `<label class="label-for-input-select">${label}</label>`:"" }
                ${(tradAutoForLanguage) ? "</div>" : ""}
                <select class="form-control select-value-input" name="${options.name}">
                    <option></option>
                </select>
            </div>
        `)

        if (viewSelectLanguage != "")
            $html.find(".co-input-with-select-lang-props").append(viewSelectLanguage);

        if(options.defaultValue){
            defaultValue = options.defaultValue;
        } else if (context.settings && context.settings.defaultValues) {
            if (typeof options.payload != "undefined" && typeof options.payload.sectionPath != "undefined") {
                values = jsonHelper.getValueByPath(context.settings.defaultValues, options.payload.sectionPath);
            } else if ( typeof context.settings.defaultValues.css != "undefined"  && typeof cssHelpers.json[name] != "undefined" ){
                values = context.settings.defaultValues.css
            }
            else {
                values = context.settings.defaultValues;
            }
            defaultValue = typeof values != "undefined" && typeof values[name] != "undefined" ? values[name] : "";
        }

        options.options.forEach(function(option){
            var label = option.label || option,
                value = option.value || option;
                
            $html.find(".select-value-input").append(`
                <option value="${value}" ${value===defaultValue ? "selected":""}>
                    ${label}
                </option>
            `)
        })

        if ((typeof options.icon != "undefined") && options.icon != ""){
            $html.find("label").prepend(`
                <i class="fa fa-${options.icon}"></i> 
            `)
        }

        if (tradAutoForLanguage && viewSelectLanguage != "") {
            $html.find(".select-language-props").change(function() {
                if ($(this).val() === "auto" && $html.find(".select-value-input").val() !== "default") {
                    triggerCustomEvents("valueChange", "traduction", $html.find(".select-value-input").val(), payload, $html);
                    triggerCustomEvents("inputBlur", "traduction", $html.find(".select-value-input").val(), payload, $html);
                }
            })
        }

        $html.find(".select-value-input").change(function(){
            triggerCustomEvents("valueChange", name, $(this).val(), payload, $html);
            triggerCustomEvents("inputBlur", name, $(this).val(), payload, $html);
        })

        $html.val = function(){
            return {
                name:name,
                value:$html.find(".select-value-input").val()
            }
        }

        if (isSelect2) {
            setTimeout(function () {
                $html.find("select").select2(configForSelect2);
            },100);
        }

        return $html;
    }

    //SELECT
    coreTypes.inputWithSelect = function(context, options) {
        var name = options.name || "select",
            configForSelect2 = options.configForSelect2 || {},
            payload = options.payload, 
            isViewPreview = options.viewPreview,
            generateID = generateUUID(),
            defaultValue = "",
            classInput = (typeof options.class != "undefined") ? options.class : "" ;

        if(options.defaultValue){
            defaultValue = options.defaultValue;
        } else if (context.settings && context.settings.defaultValues) {
            if (typeof options.payload != "undefined" && typeof options.payload.sectionPath != "undefined") {
                values = jsonHelper.getValueByPath(context.settings.defaultValues, options.payload.sectionPath);
            } else if ( typeof context.settings.defaultValues.css != "undefined"  && typeof cssHelpers.json[name] != "undefined" ){
                values = context.settings.defaultValues.css
            }
            else {
                values = context.settings.defaultValues;
            }
            defaultValue = typeof values != "undefined" && typeof values[name] != "undefined" ? values[name] : "";
        }

        var $html = $(`
            <div class="form-group input-group-sm ${classInput}">
                ${ options.label ? `<label>${options.label}</label>`:"" }
                <input type="text" class="form-control" id="inputWithSelect_${generateID}" value="${defaultValue}" name="${options.name}"> 
                ${ (isViewPreview) ? "<div class='co-input-preview'></div>" : "" }
            </div>
        `)

        if (isViewPreview) {
            $html.find(".co-input-preview").append(previewLink(defaultValue));
        }


        if ((typeof options.icon != "undefined") && options.icon != ""){
            $html.find("label").prepend(`
                <i class="fa fa-${options.icon}"></i> 
            `)
        }

        $html.find("input").change(function(){
            triggerCustomEvents("valueChange", name, $(this).val(), payload, $html);
            triggerCustomEvents("inputBlur", name, $(this).val(), payload, $html);
            $html.find("a").attr("href",$(this).val());
        })

        $html.val = function(){
            return {
                name:name,
                value:$html.find("input").val()
            }
        }

        setTimeout(function () {
            if (typeof configForSelect2["tags"] == "undefined")
                configForSelect2["tags"] = [];
            $html.find(`#inputWithSelect_${generateID}`).select2(configForSelect2);
        },300);

        return $html;
    }

    //SELECT MULTIPLE
    coreTypes.selectMultiple = function(context, options){
        var name = options.name || "select",
            payload = options.payload, 
            canDragAndDropChoise = options.canDragAndDropChoise || false,
            configForSelect2 = options.configForSelect2 || {},
            generateID = generateUUID();
            classInput = (typeof options.class != "undefined") ? options.class : "" ;

        var $html = $(`
            <div class="form-group input-group-sm ${classInput}">
                ${ options.label ? `<label>${options.label}</label>`:"" }
                <select class="form-control selectMultiple" id="selectMultiple_${generateID}" name="${options.name}"  multiple="multiple">
                    <option></option>
                </select>
            </div>
        `)
        options.options.forEach(function(option){
            var label = option.label || option,
                value = option.value || option;

            $html.find("select").append(`<option value="${value}" ${ (typeof options.defaultValue!= "undefined" ) &&  options.defaultValue.indexOf(value) > -1 ? "selected":""}>${label}</option>`);

        })
        if ((typeof options.icon != "undefined") && options.icon != ""){
            $html.find("label").prepend(`
                <i class="fa fa-${options.icon}"></i> 
            `)
        }

        $html.find("select").change(function(){
            var valueSelect = ($(this).val() == null) ? [] : $(this).val();
            triggerCustomEvents("valueChange", name, valueSelect, payload, $html);
            triggerCustomEvents("inputBlur", name, valueSelect, payload, $html);
        })

        $html.val = function(){
            return {
                name:name,
                value:$html.find("select").val()
            }
        }

        setTimeout(function () {
            if (canDragAndDropChoise) {
                var data = {
                    name: name,
                    payload: payload
                }
                selectSortableObj.init(`#selectMultiple_${generateID}`, configForSelect2, data, $html);
            } else {
                $(`#selectMultiple_${generateID}`).select2(configForSelect2)
            }
        },100);

        return $html;
    }

    //Tags
    coreTypes.tags = function(context, options){
        var name = options.name || "select",
            payload = options.payload,
            classInput = (typeof options.class != "undefined") ? options.class : "" ;

        var ref = generateUUID();

        var $html = $(`
            <div class="form-group input-group-sm ${classInput}">
                ${ options.label ? `<label>${options.label}</label>`:"" }
                <input type="text" class="form-control" id="inputTags${ref}" value="${options.defaultValue || ""}" name="${options.name}"> 
            </div>
        `)
        if ((typeof options.icon != "undefined") && options.icon != ""){
            $html.find("label").prepend(`
                <i class="fa fa-${options.icon}"></i> 
            `)
        }

        $html.find("#inputTags"+ref).change(function(){
            triggerCustomEvents("valueChange", name, $(this).val(), payload, $html);
            triggerCustomEvents("inputBlur", name, $(this).val(), payload, $html);
        })


        $html.val = function(){
            return {
                name:name,
                value:$html.find("#inputTags"+ref).val()
            }
        }

        setTimeout(function () {
            $("#inputTags"+ref).select2({tags : options.options})
        },100);

        return $html;
    }

    // SELECT SPECIAL FONT 
    coreTypes.selectFont = function(context, options){
        var name = options.name || "select",
            isCanEdit = options.canEdit || false,
            payload = options.payload;

        if(typeof options.options == "string" && options.options == "costum.fonts") {
            options.options = Object.values(costum.fonts || []).map(function(value) {
                return {
                    value: fontObj[value].replaceAll(" ", "_"),
                    label: fontObj[value]
                }
            }).sort((font1, font2) => font1.label.localeCompare(font2.label));
        }

        var $html = $(`
            <div class="form-group input-group-sm">
                ${ options.label ? `<label>${options.label}</label>`:"" }
                ${(isCanEdit) ? "<div class='select-font-with-btn-add'>" : ""}
                <select class="form-control select-input" name="${options.name}">
                    <option${((isCanEdit && typeof costum.fonts == "undefined") || (typeof costum.fonts != "undefined" && costum.fonts.length == 0)) ? " selected" : ""}>${(isCanEdit && typeof costum.fonts == "undefined") || (typeof costum.fonts != "undefined" && costum.fonts.length == 0) ? tradCms.nofontsSelectPleaseChoose || "Aucune police sélectionnée, veuillez ajouter des polices" : ""}</option>
                </select>
                ${(isCanEdit) ? `<button class="btn btn-font-manager btn-success"><i class="fa fa-plus"></i></button></div>` : ""}
            </div>
        `);

        options.options.forEach(function(option){
            var label = option.label || option,
                value = option.value || option;
                
            $html.find("select").append(`
                <option value="${value}" ${value===options.defaultValue ? "selected":""} style="font-family: '${label.split(' ').join('_')}'">
                    ${label}
                </option>
            `)
        })

        if (isCanEdit) {
            $html.find(".btn-font-manager").click(function() {
                $(".lbh-costumizer[data-space='fonts']").trigger("click");
            })
        }

        $html.find("select").change(function(){
            triggerCustomEvents("valueChange", name, $(this).val(), payload, $html);
            triggerCustomEvents("inputBlur", name, $(this).val(), payload, $html);
        })

        $html.val = function(){
            return {
                name:name,
                value:$html.find("select").val()
            }
        }

        return $html;
    }

    //COLOR PICKER
    coreTypes.colorPicker = function(context, options){
        var name = options.name || "colorPicker",
            defaultValue = "",
            classInput = (typeof options.class != "undefined") ? options.class : "",
            payload = options.payload;
            classInput = (typeof options.class != "undefined") ? options.class : "" ;

        if(options.defaultValue){
            defaultValue = options.defaultValue;
        } else if (context.settings && context.settings.defaultValues) {
            if (typeof options.payload != "undefined" && typeof options.payload.sectionPath != "undefined") {
                values = jsonHelper.getValueByPath(context.settings.defaultValues, options.payload.sectionPath);
            } else if ( typeof context.settings.defaultValues.css != "undefined"  && typeof cssHelpers.json[name] != "undefined" ){
                values = context.settings.defaultValues.css
            } else {
                values = context.settings.defaultValues;
            }
            defaultValue = typeof values != "undefined" && typeof values[name] != "undefined" ? values[name] : "";
        }

        var $html = $(`
            <div class="form-group input-group-sm ${classInput}">
                ${ options.label ? `<label>${options.label}</label>`:"" }
                <input 
                    type="text" 
                    name="${options.name}"
                    class="form-control"
                >
            </div>
        `)

        if ((typeof options.icon != "undefined") && options.icon != ""){
            $html.find("label").prepend(`
                <i class="fa fa-${options.icon}"></i> 
            `)
        }

        $html.find("input").spectrum({
            type: "text",
            color: defaultValue || null,
            appendTo: "parent",
            showInput: true,
            showInitial: true,
            palette: getColorPalette()
        })

        $html.find("input").change(function(){
            if ($(this).val() != "") {
                if (isValidColor($(this).val())) {
                    triggerCustomEvents("valueChange", name, $(this).val(), payload, $html);
                    triggerCustomEvents("inputBlur", name, $(this).val(), payload, $html);
                } else {
                    toastr.error("La couleur saisie n'est pas valide. Veuillez sélectionner une couleur à l'aide de l'outil de sélection de couleur.")
                    $(this).val("");
                    $(this).attr("style", "");
                }
            }
        })

        $html.val = function(){
            return {
                name:name,
                value: $html.find("input").val()
            }
        }

        return $html;
    }

    //INPUT GROUP
    coreTypes.inputGroup = function(context, options){
              var name = options.name || "inputGroup",
            payload = options.payload,
            isResponsive = options.responsive,
            checkSameValues = [],
            linked = false,
            values = {};

        if(options.defaultValue){
            if (typeof options.defaultValue === "object")
                values = options.defaultValue
            else {
                var valuesParts = options.defaultValue.replace(/\s{2,}/g, " ").split(" ");
                options.inputs.forEach(function(input, key){
                    var name = input.name || "input"+key;
                    values[name] = valuesParts[key] || ""
                })
            }
        } else if (context.settings && context.settings.defaultValues) {
            if (typeof options.payload != "undefined" && typeof options.payload.sectionPath != "undefined") {
                values = jsonHelper.getValueByPath(context.settings.defaultValues, options.payload.sectionPath);
            } else if ( typeof context.settings.defaultValues.css != "undefined"  && typeof cssHelpers.json[name] != "undefined" ){
                values = context.settings.defaultValues.css;
            } else {
                values = context.settings.defaultValues;
            }
        }

        var $html = $(`
            <div class="co-input-multiple ${options.classes ? options.classes : "" }">
                <div class="co-input-multiple-title">
                    ${ (isResponsive) ? "<div class='co-input-label-with-mode'>" : "" }
                        ${ options.label ? `<label>${options.label}</label>`:"" }
                    ${ (isResponsive) ? "</div>" : "" }
                </div>
                <div class="co-input-multiple-form"></div>
            </div>
        `);

        if (isResponsive) {
            $html.find(".co-input-label-with-mode").append(selectViewMode($html))
        }
        
        var unitValueSelected = "";
        options.inputs.forEach(function(input,key){
            if (typeof input=="string") {
                let optionsForInput = {};
                optionsForInput["desactivateEvent"] = true;
                if (typeof values != "undefined" && typeof values[input] != "undefined") {
                    if (typeof separateValueAndUnit(values[input])["unit"] != "undefined")
                        unitValueSelected = separateValueAndUnit(values[input])["unit"];
                    checkSameValues.push(values[input]);
                    optionsForInput["defaultValue"] = values[input];
                }
                $childInput = getInput(context, {type: input, options: optionsForInput});
                if($childInput){
                    $html.find(".co-input-multiple-form").append($childInput)
                }
            } else {
                var name = input.name || "input"+key;
                var icon = input.icon ? '<i class="fa fa-'+input.icon+'"></i>':"";
                var label = input.label;
                var defaultValue = "";
                if (typeof values != "undefined" && typeof values[name] != "undefined") {
                    defaultValue = separateValueAndUnit(values[name])["number"];
                    if (typeof separateValueAndUnit(values[name])["unit"] != "undefined")
                        unitValueSelected = separateValueAndUnit(values[name])["unit"];
                }
                $html.find(".co-input-multiple-form").addClass("co-input-multiple-not-string").append(`
                    <div class="form-group">
                        ${(typeof label != "undefined" || icon != "") ? `<span class="ellipsis">${(icon != "") ? icon+" " : ""}${(typeof label != "undefined") ? label : ""}</span>`: ""}
                        <input class="form-control" type="${ input.type || "text" }" name="${name}" value="${ defaultValue || "" }" style="text-align: center;">
                    </div>
                `)
            }
        })

        if (notEmpty(checkSameValues)) {
            linked = checkSameValues.every(element => element === checkSameValues[0]);
        }

        options.units ? $html.find('.co-input-multiple-title').append(getUnitsSelector($html, options, unitValueSelected).view) : "";

        options.linkValue ? $html.find(".co-input-multiple-form").append(getLinkValue($html, options, linked)) : "";

        var getValues = function(){
            unitValueSelected = $html.find(".select-units-value").val();
            var values = {}
            $html.find("input").each(function(){
                values[$(this).attr("name")] = $(this).val()
            })

            if(typeof options.filterValue === "function") {
                values = options.filterValue(values, unitValueSelected)
            }
            return values
        }
        
        $html.on("unitChange", function(e, data){
            unitValueSelected = data.value;
            let hasValue = false;
            $html.find("input").each(function() {
                if ($(this).val() != "") {
                    hasValue = true;
                }
            })
            if (hasValue) {
                triggerCustomEvents("valueChange", name, getValues(), payload, $html);
                triggerCustomEvents("inputBlur", name, getValues(), payload, $html);
            }
        })

        var btnLinkedActivated = linked.toString();
        $html.on("linkValueChange", function(e, data){
            btnLinkedActivated = data.value;
        })

        $html.find("input").on("input", function() {
            if (typeof $(this).data("changed") == "undefined") {
                $(this).data("changed", true);
            }
            let value = $(this).val();
            if (btnLinkedActivated === "true") {
                $html.find("input").each(function() {
                    $(this).val(value);
                })
            }
            triggerCustomEvents("valueChange", name, getValues(), payload, $html);
        })

        $html.find("input").on("click", function() {
            $(this).focus();
        });
        

        $html.find("input").on("blur", function() {
            if (typeof $(this).data("changed") != "undefined") {
                triggerCustomEvents("inputBlur", name, getValues(), payload, $html);
                $(this).removeData("changed");
            }
        })

        $html.val = function(){
            return {
                name:name,
                value:getValues()
            }
        }
        

        return $html;
    }

    //INPUT SWITCHER
    coreTypes.inputSwitcher = function(context, options) {
        var name = options.name || "inputSwitcher",
            payload = options.payload,
            allInputs = [],
            hasChanged = false,
            generateID = generateUUID();

        var $html = $(`
            <div class="co-input-switcher ${ options.class ? options.class : ""} ">
                ${ options.label ? `<label>${options.label}</label>`:"" }
                <div class="co-input-switcher-tab">
                    <form class="co-input-switcher-tab-nav" id="co-input-switcher-tab-nav_${generateID}"></form>
                    <div class="co-input-switcher-tab-content" id="co-input-switcher-tab-content_${generateID}"></div>
                </div>
            </div>
        `);

        options.tabs.forEach(function(tab, e) {
             $html.find(`#co-input-switcher-tab-nav_${generateID}`).append(`
                <input type="radio" name="${options.name}" value="${tab.value}" id="${options.name}_${tab.value}_${generateID}" class="${generateID}">
                <label for="${options.name}_${tab.value}_${generateID}" class="label-tab">${tab.label}</label>
            `);
            $html.find(`#co-input-switcher-tab-content_${generateID}`).append(`<div class="tab-content_${generateID} content_${options.name}_${tab.value}_${generateID}" style="display: none;"></div>`);
            tab.inputs.forEach(function(input, key) {
                if (typeof input == "string") {
                    var $input = getInput(context,{type: input});
                    allInputs.push(input);
                }else{
                    if (typeof options.payload != "undefined") {
                        input.options.payload = options.payload;
                    }
                    var $input=coreTypes[input.type](context, input.options);
                    if (typeof input.options != "undefined" && typeof input.options.name != "undefined") {
                        allInputs.push(input.options.name);
                    } else {
                        allInputs.push(input.type);
                    }
                }
                $html.find(`#co-input-switcher-tab-content_${generateID} .content_${options.name}_${tab.value}_${generateID}`).append($input);
            });
        });

        //INIT TAB SWITCHER
        if(notEmpty(options.defaultValue)){
            $html.find(`#co-input-switcher-tab-nav_${generateID} #${options.name}_${options.defaultValue}_${generateID}`).prop("checked", true);
            $html.find(`#co-input-switcher-tab-content_${generateID} .content_${options.name}_${options.defaultValue}_${generateID}`).show();
        }else{
            $html.find(`#co-input-switcher-tab-nav_${generateID} input.${generateID}[type='radio']`).first().prop("checked", true);
            $html.find(`#co-input-switcher-tab-content_${generateID} .tab-content_${generateID}`).first().show();
        }
    
        $html.find(`#co-input-switcher-tab-nav_${generateID} input.${generateID}[type='radio']`).change(function() {
            $html.find(`#co-input-switcher-tab-content_${generateID} .tab-content_${generateID}`).hide();
            $html.find(`#co-input-switcher-tab-content_${generateID} .content_${$(this).attr("id")}`).show();
            triggerCustomEvents("valueChange", $(this).attr("name"), $(this).val(), payload, $html);
            triggerCustomEvents("inputBlur", $(this).attr("name"), $(this).val(), payload, $html);
            hasChanged = true;
        })

        $html.on("valueChange", function(e, params) {
            if (notEmpty(params) && typeof params.name != "undefined" && params.name != name && $.inArray(params.name, allInputs) >= 0 && !hasChanged) {
                triggerCustomEvents("valueChange", name, $html.find(`#co-input-switcher-tab-nav_${generateID} input.${generateID}[type='radio']`).val(), payload, $html);
            }
        })

        $html.on("inputBlur", function(e, params) {
            if (notEmpty(params) && typeof params.name != "undefined" && params.name != name && $.inArray(params.name, allInputs) >= 0 && !hasChanged) {
                triggerCustomEvents("inputBlur", name, $html.find(`#co-input-switcher-tab-nav_${generateID} input.${generateID}[type='radio']`).val(), payload, $html);
                hasChanged = true;
            }
        })

        return $html;
    }

    //INPUT SELECT GROUP
    coreTypes.selectGroup = function(context, options){
        var name = options.name || "selectGroup",
            payload = options.payload,
            values = {};

        if(options.defaultValue){
            if(typeof options.defaultValue === "object")
                values = options.defaultValue
            else{
                var valuesParts = options.defaultValue.replace(/\s{2,}/g, " ").split(" ");
                options.inputs.forEach(function(input, key){
                    var name = input.name || "input"+key;
                    values[name] = valuesParts[key] || ""
                })
            }
        }

        var $html = $(`
            <div class="co-select-multiple">
                ${ options.label ? `<label>${options.label}</label>`:"" }
                <div class="co-select-multiple-form"></div>
            </div>
        `)
        options.inputs.forEach(function(input,key){
            var name = input.name || "input"+key;
            var inputs = "";
            var icon = input.icon ? '<i class="fa fa-'+input.icon+'"></i>' : "";
            var label = input.label ? input.label : "";
            Object.values(input.options).forEach(function(value) {
                if(typeof options.defaultValue === "object")
                    values = options.defaultValue[name]
                else
                    values = options.defaultValue;
                inputs += `<option value="${value}" ${value=== values ? "selected":""}>${value}</option>`;
            })
            $html.find(".co-select-multiple-form").append(`
                <div>
                    ${icon != "" || label != "" ? `<span class="elipsis">${icon != "" ? icon : "" +' '+ label != "" ? label : ""}</span>` : ""} 
                    <select name="${name}">
                        <option></option>
                        ${inputs}
                    </select>
                </div>
            `)
        })

        var getValues = function(){
            var valueToSend = {}
            $html.find("select").each(function() {
                valueToSend[$(this).attr("name")] = $(this).val();
            })
            if(typeof options.filterValue === "function")
                valueToSend = options.filterValue(valueToSend);

            return valueToSend;
        }

        $html.find("select").change(function() {
            triggerCustomEvents("valueChange", name, getValues(), payload, $html);
            triggerCustomEvents("inputBlur", name, getValues(), payload, $html);
        })

        $html.val = function(){
            return {
                name:name,
                value:getValues()
            }
        }

        return $html;
    }

    //INPUT SHADOW
    coreTypes.inputShadow = function(context, options){
        var name = options.name || "inputShadow",
            colorValue = "",
            payload = options.payload,
            isResponsive = options.responsive,
            defaultValue = "";
            
        if (name === "textShadow")
            defaultValue = "0px 0px 0px transparent";
        else 
            defaultValue = "0px 0px 0px 0px transparent";
            
        var ref = generateUUID(),
            value = (typeof options.defaultValue != "undefined") ? options.defaultValue.trim() : defaultValue;
            valueParts = value.replace(/\s{2,}/g, " ").replaceAll("px","").split(" ");
        
        Object.values(valueParts).forEach(function(value){
            if (isValidColor(value)) {
                colorValue = value;
                valueParts.splice(valueParts.indexOf(value), 1);
            }
        });

        var $html = $(`
            <div class="co-input-shadow">
                <div class="form-group input-group-sm">
                    ${ (isResponsive) ? "<div class='co-input-label-with-mode' style='margin-bottom: 5px'>" : "" }
                        ${ options.label ? `<label>${options.label}</label>`:"" }
                    ${ (isResponsive) ? "</div>" : "" }
                    <input 
                        data-toggle="collapse"
                        href="#collapseshadow-${ref}" 
                        aria-expanded="false" 
                        aria-controls="collapseshadow-${ref}" 
                        type="text" 
                        name="${options.name}" 
                        value="${value}" 
                        class="form-control dark-shadow" 
                        style="cursor: pointer;"
                    >
                </div>
                <div id="collapseshadow-${ref}" class="panel-collapse collapse" role="tabpanel">
                    <div class="panel-body co-input-shadow-customize">
                        <div class="wrapper-shadow">
                            ${ name !== "textShadow" ? `<div class="co-input-inset switch"><label>Inset</label><input id="inset" type="checkbox" name="inputInset" class="form-control" ${(typeof valueParts[4] != "undefined") ? "checked" : ""}><label for="inset"></label></div>` : "" }
                            ${ options.preview ? `<div class="co-input-shadow-preview" style="box-shadow: ${value}"></div>`:"" }
                            <div class="sliders-boxshadow">
                                <label>${ tradCms.horizontal }</label>
                                <div class="slider-form">
                                    <input type="range" name="shadow-sldr1-range"  min="-100" max="100" value="${valueParts[0] || 0 }" data-inputValue="${valueParts[0] || 0 }">
                                    <input type="number" name="shadow-sldr1-input" class="form-control form-control-sm" value="${valueParts[0] || 0 }">
                                </div>
                                <label>${ tradCms.vertical }</label>
                                <div class="slider-form">
                                    <input type="range" name="shadow-sldr2-range" min="-100" max="100" value="${valueParts[1] || 0}" data-inputValue="${valueParts[1] || 0 }">
                                    <input type="number" name="shadow-sldr2-input" class="form-control form-control-sm" value="${valueParts[1] || 0 }">
                                </div>
                                <label>${ tradCms.blurRadius }</label>
                                <div class="slider-form">
                                    <input type="range" name="shadow-sldr3-range" min="0" max="100" value="${valueParts[2] || 0}" data-inputValue="${valueParts[2] || 0 }">
                                    <input type="number" name="shadow-sldr3-input" class="form-control form-control-sm" value="${valueParts[2] || 0}">
                                </div>
                                ${ name !== 'textShadow' ? `<label>${ tradCms.spreadRadius }</label><div class="slider-form"><input type="range" name="shadow-sldr4-range" min="0" max="100" value="${valueParts[3] || 0 }" data-inputValue="${valueParts[3] || 0 }"><input type="number" name="shadow-sldr4-input" class="form-control form-control-sm" value="${valueParts[3] || 0 }"></div>` : "" }
                            </div>
                            <div class="co-input-colors-shadow">
                                <label>${ tradCms.color }</label>
                                <input type="text" name="shadowcolorgenerator" class="form-control form-control-sm">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        `)

        if (isResponsive) {
            $html.find(".co-input-label-with-mode").append(selectViewMode($html))
        }

        var getValue = function(){
            var value = ""
            $html.find(`#collapseshadow-${ref} .sliders-boxshadow input[type='number']`).each(function(){
                value += " " + $(this).val() + "px";
            })
            value += " " + $html.find(`#collapseshadow-${ref} .co-input-colors-shadow input`).val();
            if ($html.find(".co-input-inset input[type='checkbox']").is(":checked")) {
                value += " " + "inset";
            }
            return value.trim();
        }

        $html.find("input[name='shadowcolorgenerator']").spectrum({
            type: "text",
            color: colorValue != "" ? colorValue : null,
            showInput: true,
            showInitial: true,
            appendTo: "parent",
            palette: getColorPalette()
        })

        $html.find("input[type='range']").change(function() {
            switch ($(this).attr("name")) {
                case "shadow-sldr1-range":
                    parent.$("input[name='shadow-sldr1-input'").val($(this).val());
                    $(this).attr("data-inputValue", $(this).val());
                    triggerCustomEvents("inputBlur", name, getValue(), payload, $html);
                    break;
                case "shadow-sldr2-range":
                    parent.$("input[name='shadow-sldr2-input'").val($(this).val());
                    $(this).attr("data-inputValue", $(this).val());
                    triggerCustomEvents("inputBlur", name, getValue(), payload, $html);
                    break;
                case "shadow-sldr3-range":
                    parent.$("input[name='shadow-sldr3-input'").val($(this).val());
                    $(this).attr("data-inputValue", $(this).val());
                    triggerCustomEvents("inputBlur", name, getValue(), payload, $html);
                    break;
                case "shadow-sldr4-range":
                    parent.$("input[name='shadow-sldr4-input'").val($(this).val());
                    $(this).attr("data-inputValue", $(this).val());
                    triggerCustomEvents("inputBlur", name, getValue(), payload, $html);
                    break;
            }
        })

        var updateValue = function(){
            $html.find(`#collapseshadow-${ref} .co-input-shadow-preview`).css({ boxShadow: getValue()});
            $html.find(`input[name="${options.name}"]`).val(getValue());
            triggerCustomEvents("valueChange", name, getValue(), payload, $html);
            triggerCustomEvents("inputBlur", name, getValue(), payload, $html);
        }

        $html.find(`#collapseshadow-${ref} .sliders-boxshadow input`).change(function(){
            $html.find(`#collapseshadow-${ref} .co-input-shadow-preview`).css({ boxShadow: getValue()});
            $html.find(`input[name="${options.name}"]`).val(getValue());
            triggerCustomEvents("valueChange", name, getValue(), payload, $html);
        })

        $html.find(`#collapseshadow-${ref} .sliders-boxshadow input`).on("blur", function(){
            if ($(this).val() !== $html.find(`input[name='${$(this).attr("name").replaceAll("input", "range")}']`).attr("data-inputValue")) {
                triggerCustomEvents("inputBlur", name, getValue(), payload, $html);
                $html.find(`input[name='${$(this).attr("name").replaceAll("input", "range")}']`).attr("data-inputValue", $(this).val());
            }
        })

        $html.find(`#collapseshadow-${ref} .co-input-colors-shadow input`).change(function(){
            updateValue();
        })

        $html.find(".co-input-inset input[type='checkbox']").change(function() {
            updateValue();
        })

        $html.val = function(){
            return {
                name:name,
                value:getValue()
            }
        }

        return $html;
    }

    //INPUT GROUP BUTTONS
    coreTypes.groupButtons = function(context, options){
        var name = options.name || "groupButtons",
            payload = options.payload,
            isResponsive = options.responsive,
            classInput = (typeof options.class != "undefined") ? options.class : "" ;

        var $html = $(`
            <div class="form-group input-group-sm ${classInput}">
                <div>
                    ${ (isResponsive) ? "<div class='co-input-label-with-mode textarea-with-mode'>" : "" }
                        ${ options.label ? `<label>${options.label}</label>`:"" }
                    ${ (isResponsive) ? "</div>" : "" }
                </div>
                <div class="btn-group btn-group-content">
                </div>
            </div>
        `)

        if (isResponsive) {
            $html.find(".co-input-label-with-mode").append(selectViewMode($html))
        }

        options.options.forEach(function(option){
            $html.find(".btn-group").append(`
                <button type="button" data-value="${option.value}" class="btn ${ (options.defaultValue === option.value) ? "btn-primary btn-active":"btn-default" }" data-toggle="tooltip" data-placement="top" title="${ option.label ? option.label : option.value }">
                    ${ option.icon ? `<i class="fa fa-${option.icon}"></i>`:"" } ${ option.label ? `<span>${option.label}</span>`:"" }
                    ${ option.img ? `<img class="${ name == "alignContent" ? "transform-rotate-90" : "" }" src="${option.img}">` : "" }
                </button>
            `)
        })

        $html.find("button").click(function(){
            $html.find("button").removeClass("btn-primary btn-active").addClass("btn-default")
            $(this).addClass("btn-primary btn-active");
            triggerCustomEvents("valueChange", name, $(this).data("value"), payload, $html);
            triggerCustomEvents("inputBlur", name, $(this).data("value"), payload, $html);
        })

        $html.val = function(){
            return {
                name:name,
                value:$html.find("button.btn-primary").data("value")
            }
        }

        return $html;
    }

    //TEXTAREA
    coreTypes.textarea = function(context, options){
        var name = options.name || "textarea",
            payload = options.payload,
            classInput = (typeof options.class != "undefined") ? options.class : "",
            isMarkdown = options.markdown || false,
            tradAuto = true,
            defaultValue = "",
            translate = options.translate,
            isResponsive = options.responsive;

        if(options.defaultValue){
            defaultValue = options.defaultValue;
        } else if (context.settings && context.settings.defaultValues) {
            var values = [];
            if (typeof options.payload != "undefined" && typeof options.payload.sectionPath != "undefined") {
                values = jsonHelper.getValueByPath(context.settings.defaultValues, options.payload.sectionPath);
            } else if ( typeof context.settings.defaultValues.css != "undefined" && typeof cssHelpers.json[name] != "undefined" ){
                values = context.settings.defaultValues.css
            } else {
                values = context.settings.defaultValues;
            }
            defaultValue = typeof values != "undefined" && typeof values[name] != "undefined" ? values[name] : "";
        }

        var $html = $(`
            <div class="form-group ${(typeof translate != "undefined") ? 'contain-input-translate' : ""} ${classInput}">
                ${(isResponsive || typeof translate !== "undefined") ? `<div class='config-form ${isResponsive ? "co-input-label-with-mode textarea-with-mode" : ""} ${typeof translate !== "undefined" ? "co-input-with-label-translate" : ""}'>` : ""}
                    ${ options.label ? `<label>${options.label}</label>`:"" }
                ${ (isResponsive || typeof translate != "undefined") ? "</div>" : "" }
                <textarea name="${options.name}" rows="${options.rows || 5}" class="form-control">${options.defaultValue || ""}</textarea>
            </div>
        `)

        if (isResponsive) {
            $html.find(".co-input-label-with-mode").append(selectViewMode($html))
        }

        if (isMarkdown) {
            dataHelper.activateMarkdown($html.find("textarea"));
        }

        if (typeof translate != "undefined") {
            if(typeof translate === "object" && typeof translate.auto != "undefined") {
                tradAuto = translate.auto;
            }
            $html.find('.co-input-with-label-translate').append(traductionSelector($html, $html.find("textarea"), defaultValue, tradAuto, name, payload));
        }

        $html.find("textarea").on("input", function(){
            triggerCustomEvents("valueChange", name, $(this).val(), payload, $html);
        })

        $html.find("textarea").on("blur", function(){
            triggerCustomEvents("inputBlur", name, $(this).val(), payload, $html);
        })

        $html.val = function(){
            return {
                name:name,
                value:$html.find("textarea").val()
            }
        }

        if ((typeof options.icon != "undefined") && options.icon != ""){
            $html.find("label").prepend(`
                <i class="fa fa-${options.icon}"></i> 
            `)
        }

        return $html;
    }

    //INPUT BORDER
    coreTypes.inputBorder = function(context, options){
        var inputName = options.name || "inputBorder",
            payload = options.payload;
        
        var defaultValue = options.defaultValue || {}

        var $html = $(`
            <div class="cnb-input cnb-input-border">
                <label><span>${options.label || "Borders"}</span><button class="btn-add-border"><i class="fa fa-plus-circle"></i></button></label>
                <div class="cnb-input-border-list"></div>
            </div>
        `)

        var borderForms = {
            forms:{},
            getValues(){
                var borders = {},
                    forms = this.forms;
                Object.keys(forms).forEach(function(key){
                    var $form = forms[key],
                        borderName = $form.find(".cnb-input-border-name").val(),
                        borderSize = $form.find(".cnb-input-border-size").val(),
                        borderStyle = $form.find(".cnb-input-border-style").val(),
                        borderColor = $form.find(".cnb-input-border-color").val();

                    if(borderName !== ""){
                        borders[borderName] = `${borderSize} ${borderStyle} ${borderColor}`
                    }
                })

                return borders;
            },
            add(name="", value="1px solid black"){
                var valueParts = value.replace(/\s{2,}/g, value).split(" "),
                    size = valueParts[0],
                    style = valueParts[1],
                    color = valueParts[2];

                var key = generateUUID();
                var $form = $(`
                    <div class="cnb-input-border-form">
                        <div>
                            <select class="cnb-input-border-name">
                                <option ${name==="" ? "selected":""}></option>
                                <option value="border" ${name==="border" ? "selected":""}>Toutes</option>
                                <option value="border-top" ${name==="border-top" ? "selected":""}>Haut</option>
                                <option value="border-right" ${name==="border-right" ? "selected":""}>Droite</option>
                                <option value="border-bottom" ${name==="border-bottom" ? "selected":""}>Bas</option>
                                <option value="border-left" ${name==="border-left" ? "selected":""}>Gauche</option>
                            </select>
                        </div>
                        <div>
                            <input type="text" class="cnb-input-border-size" value="${size}">
                        </div>
                        <div>
                            <select class="cnb-input-border-style">
                                <option value="solid" ${style==="solid"?"selected":""}>Solid</option>
                                <option value="dotted" ${style==="dotted"?"selected":""}>Dotted</option>
                                <option value="none" ${style==="none"?"selected":""}>None</option>
                            </select>
                        </div>
                        <div>
                            <input type="text" class="cnb-input-border-color" value="${color}">
                        </div>
                        <div>
                            <button class="btn-remove-border" data-key="${key}"><i class="fa fa-minus-circle"></i></button>
                        </div>
                    </div>
                `)

                //event handlers
                $form.find(".cnb-input-border-name, .cnb-input-border-size, .cnb-input-border-style, .cnb-input-border-color").change(function(){
                    triggerCustomEvents("valueChange", inputName, borderForms.getValues(), payload, $html);
                    triggerCustomEvents("inputBlur", inputName, borderForms.getValues(), payload, $html);
                })

                $form.find(".btn-remove-border").click(function(){
                    $(this).closest(".cnb-input-border-form").remove()
                    delete borderForms.forms[$(this).data("key")]
                    triggerCustomEvents("valueChange", inputName, borderForms.getValues(), payload, $html);
                    triggerCustomEvents("inputBlur", inputName, borderForms.getValues(), payload, $html);
                })

                //init spectrum
                $form.find(".cnb-input-border-color").spectrum({ 
                    type:"color",
                    appendTo: "parent", 
                    palette: getColorPalette()
                })

                //add form
                this.forms[key] = $form;
                $html.find(".cnb-input-border-list").append($form)
            }
        }

        $html.find(".btn-add-border").click(function(e){
            e.preventDefault()
            e.stopPropagation();
            
            borderForms.add()
        })

        //init default value
        Object.keys(defaultValue).forEach(function(name){
            if(["border", "border-top", "border-bottom", "border-left", "border-right"].indexOf(name) > -1)
                borderForms.add(name, defaultValue[name])
        })

        $html.val = function(){
            return {
                name:inputName,
                value:borderForms.getValues()
            }
        }

        return $html;
    }

    //INPUT BORDER SINGLE
    coreTypes.inputBorderSingle = function(context, options){
        var inputName = options.name || "inputBorderSingle",
            payload = options.payload,
            defaultValue = options.defaultValue || "none",
            isResponsive = options.responsive,
            defaultValueParts = defaultValue.split(" ");

        var values = {}
        if(defaultValue === "none"){
            values = { size:"", style:"none", color:"" }
        }else if(defaultValueParts.length < 3){
            values = { size:"", style:"", color:"" }
        }else{
            values = {
                size:defaultValueParts[0],
                style:defaultValueParts[1],
                color:defaultValueParts[2]
            }
        }

        var $html = $(`
            <div class="form-group">
                ${ (isResponsive) ? "<div class='co-input-label-with-mode' style='margin-bottom: 5px'>" : "" }
                    ${ options.label ? `<label>${options.label}</label>`:"" }
                ${ (isResponsive) ? "</div>" : "" }
                <div class="cnb-input-border-simple widthBorderSingle">
                    <div><input type="text" class="border-input" name="size" value="${values.size}" /></div>
                    <div>
                        <select name="style" class="border-change">
                            <option></option>
                        </select>
                    </div>
                    <div>
                        <input type="text" class="border-change" name="color" value="${values.color}"/>
                    </div>
                </div>
            </div>
        `)

        if ( typeof costumizer.mode != "undefined" && costumizer.mode != "md"){
            var $html = $(`
                <div class="form-group">
                    ${ (isResponsive) ? "<div class='co-input-label-with-mode' style='margin-bottom: 5px'>" : "" }
                        ${ options.label ? `<label>${options.label}</label>`:"" }
                    ${ (isResponsive) ? "</div>" : "" }
                    <div class="cnb-input-border-simple widthBorderSingleOthers">
                        <div><input type="text" class="border-input" name="size" value="${values.size}" /></div>
                        <div>
                            <select name="style" class="border-change">
                                <option></option>
                            </select>
                        </div>
                    </div>
                    <div class="cnb-input-border-simple widthBorderSingleColor">
                        <div class="borderSingleColor">
                            <input type="text" class="border-change" name="color" value="${values.color}"/>
                        </div>
                    </div>
                </div>
            `)
        }

        if (isResponsive) {
            $html.find(".co-input-label-with-mode").append(selectViewMode($html))
        }

        var borderStyles = ["none","hidden","dotted","dashed","solid","double","groove","ridge","inset","outset","initial","inherit"]

        borderStyles.forEach(function(style){
            $html.find("select").append(`
                <option value="${style}" ${style===values.style ? "selected":""}>${style}</option>
            `)
        })

        var getValues = function(){
            var size = $html.find("input[name='size']").val()?.replace(/\s/g, ""),
                style = $html.find("select[name='style']").val()?.replace(/\s/g, ""),
                color = $html.find("input[name='color']").val()?.replace(/\s/g, "");

            return `${size || "1px"} ${style || "none"} ${color || "black"}`
        }

        $html.find("input[name='color']").spectrum({ 
            type:"text", 
            palette: getColorPalette()
        })

        if ( typeof costumizer.mode != "undefined" && costumizer.mode != "md"){
            $html.find("input[name='color']").spectrum({ type:"text" , appendTo: "parent" , showInput: true, showInitial: true, palette: getColorPalette()})
        }

        $html.find(".border-input").on("input", function(){
            triggerCustomEvents("valueChange", inputName, getValues(), payload, $html);
        })

        $html.find(".border-input").on("blur", function(){
            triggerCustomEvents("inputBlur", inputName, getValues(), payload, $html);
        })

        $html.find(".border-change").change(function() {
            triggerCustomEvents("valueChange", inputName, getValues(), payload, $html);
            triggerCustomEvents("inputBlur", inputName, getValues(), payload, $html);
        })

       $html.val = function(){
           return {
                name:inputName,
                value:getValues()
           }
       }

        return $html;
    }
 
    //INPUT MULTIPLE
    coreTypes.inputMultiple = function(context, options){
        var inputName = options.name || "inputMultiple",
            payload = options.payload,
            generateID = generateUUID(),
            maxInputs = options.max,
            single = options.single || false;

        var values = {
            _values:{},
            add(value={}){
                var key = generateUUID();
                this._values[key] = value;
                return key;
            },
            set(key, name, value, payload={}){
                if (notEmpty(payload)) {
                    var path = "";
                    if (typeof payload.path != "undefined")
                        path = payload.path;
                    if (typeof payload.sectionPath != "undefined") {
                        (path !== "") ? path += "."+payload.sectionPath : path += payload.sectionPath; 
                    } 
                }
                if (typeof path != "undefined" && path !== "") {
                    path += "."+name
                } else {
                    path = name
                }
                jsonHelper.setValueByPath(this._values, key+"."+path, value);
            },
            delete(key){
                delete this._values[key]
            },
            get(){
                var that = this;
                return Object.keys(that._values).map(function(key){
                    return that._values[key]
                })
            },
            getTotal(){
                return Object.keys(this._values).length;
            }
        };

        var $html = $(`
            <div class="form-group ${options.class ? options.class : ""}">
                ${options.label ? `<label class='inputmultiple-label'>${options.label}</label>` : ""}
                <div class="inputs-container inputs-container_${generateID}"></div>
                ${!single ? `<button type="button" class="btn btn-default btn-block btn-add-input_${generateID}">
                    <i class="fa fa-plus" aria-hidden="true"></i></button>` : ""}
            </div>
        `);



        var addInputs = function(defaultValue={}){
            //add new values
            var key = values.add();

            var baseHtml = `
                <div class="input-item" style="display:flex;align-items:center;justify-content:space-between">
                    <div class="inputs-row_${generateID}" style="flex: 0.98"></div>
                    ${!single ? `
                        <div class="btn-remove-input-content">
                            <button style="border-radius:100%; margin-left:5px;" class="btn btn-danger btn-xs btn-remove-input_${generateID}">
                                <i class="fa fa-times"></i>
                            </button>
                        </div>
                    ` : ""}
                </div>
            `;

            var $inputs = $(baseHtml);

            var inputsParams = []
            options.inputs.forEach(function(input){
                if (typeof input.options != "undefined" &&  typeof input.options.name != "undefined" && typeof defaultValue[input.options.name] == "undefined") {
                    if (typeof input.options != "undefined" && typeof input.options.defaultValue != "undefined")
                        delete input.options.defaultValue;
                }
                if(Array.isArray(input)){       
                    input = input.map(function(params){
                        if(typeof params !== "object")
                        params = { type:params, options:{ name:params } }
                        params.options = params.options || {};
                        if(defaultValue[params.options.name]){
                            params.options.defaultValue = defaultValue[params.options.name];
                            values.set(key, params.options.name, defaultValue[params.options.name])
                        }
                        return params
                    })
                }else{
                    if(typeof input !== "object")
                        input = { type:input, options:{ name:input } }
                    input.options = input.options || {}
                    if(defaultValue[input.options.name]){
                        input.options.defaultValue = defaultValue[input.options.name];
                        values.set(key, input.options.name, defaultValue[input.options.name])
                    }
                }
                inputsParams.push(getInputParams(context, input))
            })

            var $form = $inputs.find(`.inputs-row_${generateID}`)
            renderInputs(context, $form, inputsParams)
            $form.on("valueChange", function(e, params) {
                e.stopPropagation()
                values.set(key, params.name, params.value, params.payload);
                triggerCustomEvents("valueChange", inputName, values.get(), payload, $html);
            })

            $form.on("inputBlur", function(e, params) {
                e.stopPropagation();
                values.set(key, params.name, params.value, params.payload);
                triggerCustomEvents("inputBlur", inputName, values.get(), payload, $html);
            })

            $inputs.find(`.btn-remove-input_${generateID}`).click(function(e) {
                $inputs.remove();
                values.delete(key);
                triggerCustomEvents("valueChange", inputName, values.get(), payload, $html);
                triggerCustomEvents("inputBlur", inputName, values.get(), payload, $html);

                if (typeof maxInputs != "undefined") {
                    if(values.getTotal() < maxInputs){
                        $html.find(`.btn-add-input_${generateID}`).show();
                    }
                }
            })

            $html.find(`.inputs-container_${generateID}`).append($inputs);
        }

        //init default value;
        if(options.defaultValue){
            $.each(options.defaultValue, function(key, value){
                addInputs(value)
            })
        }

        if (typeof maxInputs != "undefined" && values.getTotal() === maxInputs) {
            $html.find(`.btn-add-input_${generateID}`).hide();
        }
        
        $html.find(`.btn-add-input_${generateID}`).click(function(){
            addInputs();
            if (typeof maxInputs != "undefined") {
                if(values.getTotal() === maxInputs){
                    $(this).hide();
                }
            }
            triggerCustomEvents("valueChange", inputName, values.get(), payload, $html);
            triggerCustomEvents("inputBlur", inputName, values.get(), payload, $html);
        })

        $html.val = function(){
            return {
                name:inputName,
                value:values.get()
            }
        }

        return $html
    }

    //INPUT FILE
    coreTypes.inputFile = function(context, options){
        var name = options.name || "textarea",
            payload = options.payload,
            file = null,
            accept = options.accept || ["*"];

        var $html = $(`
            <div class="form-group">
                ${ options.label ? `<label>${options.label}</label>`:"" }
                <div class="input-group">
                    <input type="text" class="form-control" name="${options.name}" placeholder="${options.placeholder || ""}" readonly>
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="button"><i class="fa fa-folder-open" aria-hidden="true"></i></button>
                    </span>
                </div>
            </div>
        `)

        $html.find("button").click(function(){
            var $input = $(`<input type="file" accept="${accept.join(", ")}"/>`)
            $input.change(function(){
                file = $input[0].files[0];
                $html.find("input").val(file.name)
                triggerCustomEvents("valueChange", name, file, payload, $html);
                triggerCustomEvents("inputBlur", name, file, payload, $html);
            })

            $input.trigger("click")
        })

        $html.val = function(){
            return {
                name:name,
                value:file
            }
        }

        return $html;
    }

    //INPUT FILE IMAGE
    coreTypes.inputFileImage = function(context, options){
        var name = options.name || "inputFileImage",
            contentKey = options.contentKey || "slider",
            uploadOnly = options.uploadOnly || false,
            uploaderInitialized = options.uploaderInitialized || false,
            payload = options.payload,
            bibliotheque = {},
            canSelectMultipleImage = options.canSelectMultipleImage || false,
            classInput = (typeof options.class != "undefined") ? options.class : "",
            endPoint = (typeof options.endPoint != "undefined") ? options.endPoint : "/subKey/coInput",
            domElement = (typeof options.domElement != "undefined") ? options.domElement : "image",
            filetype = (typeof options.filetype != "undefined") ? options.filetype : ["jpeg", "jpg", "gif", "png"],
            defaultPathImage = (options.canSelectMultipleImage) ? [] : (typeof translate != "undefined") ? {} : "",
            maxImageSelected = options.maxImageSelected,
            translate = options.translate,
            pathImage = (options.canSelectMultipleImage) ? [] : (typeof translate != "undefined") ? {} : "",
            tradAuto = true,
            langSelected = costum.langCostumActive || "fr",
            allLanguages = {},
            allLanguagesSelectedInDb = dataHelper.getAllLanguageInDb(),
            imageToShow = "",
            generateID = generateUUID(),
            values = {};
        if (typeof options.defaultValue != "undefined") {
            if (canSelectMultipleImage) {
                if (typeof options.defaultValue == "string" && options.defaultValue == "") 
                    options.defaultValue = [];
                defaultPathImage = options.defaultValue;
            } else {
                if (typeof options.defaultValue == "string") {
                    defaultPathImage = options.defaultValue.replaceAll("url('", "").replaceAll("')", "");
                } else {
                    if (typeof translate != "undefined") {
                        defaultPathImage = options.defaultValue;
                    } else {
                        defaultPathImage = typeof options.defaultValue[name] != "undefined" ? options.defaultValue[name].replaceAll("url('", "").replaceAll("')", "") : "";
                    }
                }
            }
        } else if (context.settings && context.settings.defaultValues) {
            if (typeof options.payload != "undefined" && typeof options.payload.sectionPath != "undefined") {
                values = jsonHelper.getValueByPath(context.settings.defaultValues, options.payload.sectionPath);
            } else if ( typeof context.settings.defaultValues.css != "undefined"  && typeof cssHelpers.json[name] != "undefined" ){
                values = context.settings.defaultValues.css;
            } else {
                values = context.settings.defaultValues;
            }

            if (canSelectMultipleImage)
                defaultPathImage = values;
            else {
                if (typeof translate != "undefined") {
                    defaultPathImage = typeof values != "undefined" && typeof values[name] != "undefined" ? values[name] : {};
                } else {
                    defaultPathImage = typeof values != "undefined" && typeof values[name] != "undefined" ? values[name].replaceAll("url('", "").replaceAll("')", "") : "";
                }
            }
        }
        var valueChange = function(path, langSelected = "") {
            if (canSelectMultipleImage == true) {
                if (typeof path == "object") {
                    Object.values(path).forEach(function(value) {
                        pathImage.push(value);
                    })
                } else {
                    (pathImage).push(path);
                }
            } else {
                if (typeof translate != "undefined" && langSelected != "") {
                    pathImage = { translate: true, pathImage: path, lang: langSelected };
                } else 
                    pathImage = path;
            }
            triggerCustomEvents("valueChange", name, pathImage, payload, $html);
            triggerCustomEvents("inputBlur", name, pathImage, payload, $html);
            if (typeof translate != "undefined") {
                defaultPathImage = $.extend({}, defaultPathImage, {[langSelected] : path});
            } else {
                defaultPathImage = pathImage;
            }
            pathImage = (canSelectMultipleImage) ? [] : (typeof translate != "undefined") ? {} : "";
        }

        var deleteImageDocument = function() {
            bootbox.confirm({message: trad.areyousuretodelete,
                buttons: {
                    confirm: {
                        label: trad.yes,
                        className: 'btn-success'
                    },
                    cancel: {
                        label: trad.no,
                        className: 'btn-danger'
                    }
                }, 
                callback: function(response) {
                    if (response) {
                        var langToSaveInDb = "";
                        path = (name == "backgroundImage") ? "" : `${modules.co2.url}/images/thumbnail-default.jpg`;
                        if (typeof translate != "undefined") {
                            langToSaveInDb = langSelected;
                        }
                        valueChange(path, langSelected);
                        viewImage();
                    }
                }
            });
        }

        var $html = $(`
            <div class="form-group co-input-file-image ${ classInput }">
                ${ (typeof translate != "undefined") ? '<div class="co-input-with-label-translate input-file-image-form">' : ""}
                    ${ options.label ? `<label>${options.label}</label>`:"" }
                ${ (typeof translate != "undefined") ? '</div>' : ""}
                <div class="text-center">
                    <div class="co-input-file-image-content">
                        <div class='${ (canSelectMultipleImage) ? "allMultipleImageView" : "singleImageView" }'></div>
                    </div>
                    <button class="btn btn-sm btn-choose-file" type="button"><i class="fa fa-upload"></i> ${ (canSelectMultipleImage) ? "Sélectionner les images" : tradCms.chooseFile }</button>
                </div>
            </div> 
        `)

        if ((typeof options.icon != "undefined") && options.icon != ""){
            $html.find("label").prepend(`
                <i class="fa fa-${options.icon}"></i> 
            `)
        }

        if (typeof translate != "undefined") {
            var dropDownItem = "";
            if (typeof defaultPathImage == "object" && typeof defaultPathImage["languageSelected"] != "undefined") {
                langSelected = defaultPathImage["languageSelected"];
            }
    
            for (var i = 0; i < allLanguagesSelectedInDb.length; i++) {
                var keyLang = allLanguagesSelectedInDb[i].toUpperCase();
                if (themeParams.DeeplLanguages.hasOwnProperty(keyLang)) {
                    allLanguages[keyLang] = themeParams.DeeplLanguages[keyLang];
                }
            }

            $.each(allLanguages, function(key, value) {
                dropDownItem += `<button class="btn dropdown-item${ (langSelected === key.toLowerCase()) ? ' active': '' }" type="button" data-value="${key.toLowerCase()}"${ (langSelected === key.toLowerCase()) ? ' disabled': '' }><span style="font-size: 20px; margin-right: 10px">${value["icon"]}</span> ${tradCms[value["label"]]}</button>`;
            });
    
            var $viewTranslateForm = $(`
                ${(Object.keys(allLanguages).length > 1) ? 
                    `<div class="dropdown co-input-translate-form">
                        <button class="btn btn-primary dropdown-toggle btn-flag-translate-active" type="button" id="dropdownForTranslate" data-toggle="dropdown">
                            <span style="font-size: 20px; margin-right: 5px">${themeParams.DeeplLanguages[langSelected.toUpperCase()]["icon"]}</span> ${langSelected}
                        </button>
                        <div class="dropdown-menu dropdown-lang-without-tradAuto" aria-labelledby="dropdownForTranslate">
                            <div class="translate-form-select">
                                ${dropDownItem}
                            </div>
                        </div>
                    </div>` : ''
                }
            `);

            $viewTranslateForm.find(".translate-form-select button.dropdown-item").click(function() {
                langSelected = $(this).data("value");
                var pathToSend = "";
                $viewTranslateForm.find(".translate-form-select button.dropdown-item").removeClass("active").removeAttr("disabled");
                $(this).addClass("active").attr("disabled", "disabled");
                $viewTranslateForm.find(".btn-flag-translate-active").html(`<span style="font-size: 20px; margin-right: 5px">${themeParams.DeeplLanguages[langSelected.toUpperCase()]["icon"]}</span> ${langSelected}`);
                if (typeof defaultPathImage == "object" && notEmpty(defaultPathImage)) {
                    pathToSend = (typeof defaultPathImage[langSelected] != "undefined") ? defaultPathImage[langSelected] : defaultPathImage[Object.keys(defaultPathImage)[0]]
                }
                pathImage = { translate: true, pathImage: pathToSend, lang: langSelected };
                triggerCustomEvents("valueChange", name, pathImage, payload, $html);
                viewImage();
            });

            $html.find('.co-input-with-label-translate').append($viewTranslateForm);
        }



        var viewImage = function() {
            let viewImages = "";
            if (translate) {
                if (!canSelectMultipleImage) {
                    $html.find(".singleImageView").html("");
                    if (typeof defaultPathImage == "object" && notEmpty(defaultPathImage)) {
                        if (typeof defaultPathImage[langSelected] != "undefined") {
                            imageToShow = defaultPathImage[langSelected]
                        } else {
                            imageToShow = defaultPathImage[Object.keys(defaultPathImage)[0]];
                        }
                        if (imageToShow != "" && imageToShow.includes("thumbnail-default.jpg") === false) {
                            $html.find(".co-input-file-image-content").prepend(`<button type="button" class="btn btn-sm btn-delete-img"><i class="fa fa-times"></i></button>`);
                            viewImages = `<img class="img-uploaded" src="${ imageToShow }">`;
                        } else {
                            $html.find(".btn-delete-img").remove();
                            viewImages = `<img class="img-uploaded" src="${modules.co2.url}/images/thumbnail-default.jpg">`
                        }
                    } else {
                        $html.find(".btn-delete-img").remove();
                        viewImages = `<img class="img-uploaded" src="${modules.co2.url}/images/thumbnail-default.jpg">`
                    }
                    $html.find(".singleImageView").append(viewImages);
                    $html.find(".btn-delete-img").click(function() {
                        deleteImageDocument();
                    })
                }
            } else {
                if (canSelectMultipleImage) {
                    $html.find(".allMultipleImageView").html("");
                    if (notEmpty(defaultPathImage)) {
                        Object.values(defaultPathImage).forEach(function(value) {
                            viewImages += `<div class="multiple-image-content"><img src='${value}'></div>`;
                        })
                    } else {
                        viewImages = `<img class="img-uploaded" src="${modules.co2.url}/images/thumbnail-default.jpg">`
                    }
                    $html.find(".allMultipleImageView").append(viewImages);
                } else {
                    $html.find(".singleImageView").html("");
                    if (defaultPathImage != "" && defaultPathImage.includes("thumbnail-default.jpg") === false) {
                        $html.find(".co-input-file-image-content").prepend(`<button type="button" class="btn btn-sm btn-delete-img"><i class="fa fa-times"></i></button>`);
                        viewImages = `<img class="img-uploaded" src="${ defaultPathImage }">`;
                    } else {
                        $html.find(".btn-delete-img").remove();
                        viewImages = `<img class="img-uploaded" src="${modules.co2.url}/images/thumbnail-default.jpg">`
                    }
                    $html.find(".singleImageView").append(viewImages);
                    $html.find(".btn-delete-img").click(function() {
                        deleteImageDocument();
                    })
                }
            }
        }

        viewImage();

        $html.find(".btn-choose-file, .allMultipleImageView, .singleImageView").click(function(){
            let path = (canSelectMultipleImage) ? [] : "";
            bibliotheque = getListImageDocuments();
            let html = ``;
            if (uploadOnly){
                html = `
                <div class='tab-co-input-view-image'>
                    <input class="radio upload-image-input" id="two" name="group" type="radio" checked />
                    <div class="">
                        <label class="tab two-tab" for="two"><i class="fa fa-laptop"></i><span>${ tradCms.computer }</span></label>
                    </div>
                    <div class="view-image-body panels">
                        <div class="panel-content two-panel">
                            <div class="upload-image">
                            </div>
                        </div>
                    </div>
                </div>
                `;
            } else {
                html = `
                    <div class='tab-co-input-view-image'>
                        <input class="radio" id="one" name="group" type="radio" ${ notEmpty(bibliotheque) ? "checked" : "" } />
                        <input class="radio upload-image-input" id="two" name="group" type="radio" ${ !notEmpty(bibliotheque) ? "checked" : "" } />
                        <div class="tabs">
                            <label class="tab one-tab" for="one"><i class="fa fa-book"></i><span>${ tradCms.library }</span></label>
                            <label class="tab two-tab" for="two"><i class="fa fa-laptop"></i><span>${ tradCms.computer }</span></label>
                        </div>
                        <div class="view-image-body panels">
                            <div class="panel-content one-panel">
                                ${ !notEmpty(bibliotheque) ? "<p class='empty-image-text'>"+tradCms.noImageInLibrary+"</p>" : "" }
                            </div>
                            <div class="panel-content two-panel">
                                <div class="upload-image">
                                </div>
                            </div>
                        </div>
                    </div>
                `;
            }

            bootbox.confirm({
                size: "large",
                className: "co-input-panel-image",
                message: html,
                closeButton: true,
                title: `${ tradCms.choose } ${options.label ? options.label : tradCms.image }`,
                callback: function(response) {
                    if (response) {
                        if ($(".upload-image-input").is(":checked")) {
                            dyFObj.commonAfterSave(params = null, function() {
                                toastr.success(tradCms.imageWellAdded);
                            });
                        }
                    }
                }
            })

            if (notEmpty(bibliotheque)) {
                let imageView = "";
                // boucle image bibliotheque and add checked si l'image est séléctionné
                $.each(bibliotheque, function(key, value) {
                    var hasChecked = false;
                    if (canSelectMultipleImage) {
                        $.each(defaultPathImage, function(defaultVal, defaultVal) {
                            if (defaultVal === value.docPath) {
                                imageView += "<div class='img-content img-checked'><img src='"+value.docPath+"' data-value='"+value.docPath+"' class='image-on-bibliotheque disable-click' alt='"+value.name+"'><i class='fa fa-check icon-status-checked canHover' data-img='"+value.docPath+"'></i></div>";
                                hasChecked = true;
                                return false;
                            } 
                        })
                    } else {
                        if (typeof translate != "undefined") {
                            if (imageToShow === value.docPath) {
                                imageView += "<div class='img-content img-checked'><img src='"+value.docPath+"' data-value='"+value.docPath+"' class='image-on-bibliotheque disable-click' alt='"+value.name+"'><i class='fa fa-check icon-status-checked' data-img='"+value.docPath+"'></i></div>";
                                hasChecked = true;
                            }
                        } else {
                            if (defaultPathImage === value.docPath) {
                                imageView += "<div class='img-content img-checked'><img src='"+value.docPath+"' data-value='"+value.docPath+"' class='image-on-bibliotheque disable-click' alt='"+value.name+"'><i class='fa fa-check icon-status-checked' data-img='"+value.docPath+"'></i></div>";
                                hasChecked = true;
                            } 
                        }
                    }
                    if(!hasChecked) {
                        imageView += "<div class='img-content'><img src='"+value.docPath+"' data-value='"+value.docPath+"' class='image-on-bibliotheque' alt='"+value.name+"'><i class='fa fa-check icon-status-checked d-none' data-img='"+value.docPath+"'></i></div>";
                    }
                })
    
                $('.one-panel').prepend(`
                    <div class="biblio-image">
                        ${imageView}
                    </div>
                `)
    
                // évènement on click d'un image
                $('.image-on-bibliotheque').off().on("click", function () {
                    let path = $(this).data("value"),
                        langToSaveInDb = "";
                    if (canSelectMultipleImage === false) {
                        $(".img-content i").addClass("d-none");
                        $(".img-content").removeClass('img-checked');
                        $('.image-on-bibliotheque').removeClass('disable-click').css('cursor', 'pointer');
                    }
                    $(this).addClass("disable-click");
                    $(this).next('i').removeClass('icon-status-checked d-none').addClass('icon-status-checked');
                    $(this).closest('.img-content').addClass('img-checked');
                    if (canSelectMultipleImage)
                        pathImage = defaultPathImage;
                    if (typeof translate != "undefined") {
                        langToSaveInDb = langSelected;
                    }
                    valueChange(path, langToSaveInDb);
                    viewImage();
                })
                
                if(canSelectMultipleImage) {
                    $(".icon-status-checked").hover(function() {
                        $(this).removeClass("fa-check").addClass("fa-times canHover");
                    }, function() {
                        $(this).removeClass("fa-times").addClass("fa-check");
                    });
    
                    $(".icon-status-checked").on("click", function(e) {
                        e.stopImmediatePropagation();
                        if (canSelectMultipleImage) {
                            let imgPath = $(this).data("img");
                            let index = defaultPathImage.indexOf(imgPath);
                            if (index !== -1) {
                                defaultPathImage.splice(index, 1);
                            }
                            $(this).prev('img').removeClass("disable-click").css('cursor', 'pointer');
                            $(this).closest('.img-content').removeClass('img-checked');
                            $(this).addClass("d-none");
                            valueChange(defaultPathImage);
                            viewImage();
                        }
                    });
                }
            }

            $.dynForm({
                beforeBuild : function(){	
                    if (!uploaderInitialized){
                        uploadObj.set(costum.contextType,costum.contextId);
                        uploadObj.initUploader();
                    }
                },
                formId: ".upload-image",
                formObj : {
                    jsonSchema : {  
                        properties : {
                            "image": {
                                "inputType": "uploader",
                                "docType": "image",
                                "contentKey": contentKey,
                                "domElement": domElement,
                                "endPoint": endPoint,
                                "filetypes": filetype,
                                "label": "Image :",
                                ...(typeof maxImageSelected !== "undefined" ? { "itemLimit": maxImageSelected } : ((canSelectMultipleImage === false && typeof maxImageSelected === "undefined") ? { "itemLimit": 1 } : {})),
                                onComplete: function(data){
                                    var langToSaveInDb = "";
                                    pathImage = defaultPathImage;
                                    if (typeof translate != "undefined") {
                                        langToSaveInDb = langSelected;
                                    }
                                    valueChange(data.docPath, langToSaveInDb);
                                    viewImage();
                                }
                            }
                        }
                    }	
                }
            })
        })

        $html.val = function() {
            return {
                name: name,
                value: pathImage,
            }
        }

        return $html;

        

    }


    //INPUT FINDER
    coreTypes.finders = function(context, options){
        placeholder = {},
        object = {};
        fullObject = {};
        finderPopulation = {};
        selectedItems={};
        field = {};
        callback = {};
        invite = {};
        initContacts={};
        roles = {};
        search = {};
        initMe = {};
        initBySearch = {};
        toBeValidated = {};
        filters = {};
        extraParams = {};
        showField = {};
        noResult = {};

        var name = options.name || "organizer",
        payload = options.payload || "",
        classInput = options.class || "",
        defaultValue = options.defaultValue || {},
        callbackSelect = options.callbackSelect || null,
        update = (typeof options.update != "undefined") ? true : null,
        initType = (typeof options.initType != "undefined") ?  options.initType : ["projects", "organizations"],
        multiple = (typeof options.multiple != "undefined") ?  options.multiple : false,
        openSearch = (typeof options.openSearch != "undefined") ?  options.openSearch : false,
        field = options.field || null,
        labelStr = (typeof options.multiple != "undefined" && options.multiple) ? tradDynForm.addtothelist: tradDynForm.changetheelement,
        initValues = (typeof options.initValues != "undefined") ?  options.initValues : null,
        generateID = generateUUID();



        //view start
        var $html = $(`
            <div class="form-group input-group-sm ${classInput}">
                ${ options.label ? `<label>${options.label}</label>`:"" }
                <div class="finder-${name}">
                    <div><button href="javascript:;" class="form-control selectParent${generateID} btn-success margin-bottom-10" data-id="${name}" data-inittype="${initType}" data-types="${initType.join(",")}" data-multiple="${multiple}" data-open="${openSearch}">${labelStr}</button></div>
                    <div class='coInput-form-list-finder-${generateID}' style="max-height:140px; overflow-y:auto" ></div>
                </div>
            </div>
        `)

        if ((typeof options.icon != "undefined") && options.icon != ""){
            $html.find("label").prepend(`
                <i class="fa fa-${options.icon}"></i> 
            `)
        }

        var params = {
            id : name,
            multiple : options.multiple,
            initType : initType,
            values : initValues,
            update : update,
            invite : options.invite,
            roles : options.roles,
            search : options.search,
            initElt : options.initElt,
            extraParams : options.extraParams,
            showField : true,
            noResult : null,
            field : field
        };

        if(typeof options.initContacts != "undefined") params.initContacts=options.initContacts;
        if(typeof options.initContext != "undefined") params.initContext=options.initContext;
        if(typeof options.initMe != "undefined") params.initMe = options.initMe;
        if(typeof options.initBySearch != "undefined") params.initBySearch = options.initBySearch;
        if(typeof options.toBeValidated != "undefined") params.toBeValidated = options.toBeValidated;
        if(typeof options.placeholder != "undefined") params.placeholder=options.placeholder;
        if(typeof options.filters != "undefined") params.filters=options.filters;
        if(typeof options.field != "undefined") params.field = options.field;
        if(typeof options.extraParams != "undefined") params.extraParams = options.extraParams;
        if(typeof options.showField != "undefined") params.showField = options.showField;
        if(typeof options.noResult != "undefined") params.noResult = options.noResult;
        labelStr = (typeof options.multiple != "undefined" && options.multiple) ? tradDynForm.addtothelist: tradDynForm.changetheelement;
        if(typeof options.buttonLabel != "undefined") labelStr = options.buttonLabel;
       


        var object = {};
        field = params.field,
        initMe = (typeof params.initMe != "undefined") ? params.initMe : true,
        iniobject = {},
        initMe = (typeof params.initMe != "undefined") ? params.initMe : true,
        initBySearch = (typeof params.initBySearch != "undefined") ? params.initBySearch : false,
        initContacts = (typeof params.initContacts != "undefined") ? params.initContacts : true,
        placeholder=(typeof params.placeholder != "undefined") ? params.placeholder : "Search an element",
        filters=(typeof params.filters != "undefined") ? params.filters : null,
        extraParams = (typeof params.extraParams != "undefined") ? params.extraParams : null,
        showField = (typeof params.showField != "undefined") ? params.showField : true,
        noResult = (typeof params.noResult != "undefined") ? params.noResult : null,
        toBeValidated=(typeof params.toBeValidated != "undefined") ? params.toBeValidated : null;

        if (Object.keys(defaultValue).length > 0) {
            $.each(defaultValue, function(key, v){
                addInForm(key, v.type, v.name, v.image, defaultValue , true);
            });
        } else if(params.values){
            var valFin = params.values;
            if(typeof params.values.contributors != "undefined"){
                valFin = params.values.contributors;
                $.ajax({
                    type: "POST",
                    url: baseUrl+'/'+moduleId+'/element/getdatadetail/type/'+dyFObj.elementData.map.type+
                                        '/id/'+dyFObj.elementData.map.id+'/dataName/contributors/isInviting/true?tpl=json',
                    dataType: "json",
                    async : false,
                    success: function(data){
                        $.each(data, function(e, v){
                            var good = false;
                            if(params.roles == null){
                                good = true;
                            }
                            else if(notNull(params.roles)){
                                good = true ;
                                $.each(params.roles, function(kR, vR){
                                    if($.inArray(vR, v.rolesLink) == -1)
                                        good = false;
                                });
                            }
                            if(good == true)
                                addInForm(e, v.collection, v.name, v.profilThumbImageUrl,v);
                        });
                    }
                });
            }else{
                $.each(valFin, function(e, v){
                    typeElt = (typeof v.collection == "undefined") ? v.type : v.collection;
                    addInForm(e, typeElt, v.name, v.profilThumbImageUrl, v);
                });
            }

        } else if(!notNull(params.update) && (typeof params.initContext == "undefined" || params.initContext)){
            if(typeof contextData != "undefined" && notNull(contextData) && $.inArray(contextData.collection, params.initType) > -1)
                addInForm(contextData.id, contextData.collection, contextData.name, contextData.profilThumbImageUrl, contextData);
            else if(typeof costum != "undefined" && notNull(costum) && $.inArray(costum.contextType, params.initType) > -1){
                addInForm(costum.contextId, costum.contextType, costum.title, costum.logo, costum);
            }	
            else if(typeof params.initType != "undefined" &&
                    ( ( params.initType.length != 1 &&
                        params.initType[0] != "events" ) ||
                        params.initType[0] == "organizations" ) &&
                    !notNull(params.initElt) && typeof initMe != "undefined" && initMe && !dyFObj.editMode ) {
                        if(userId != "" && typeof userConnected != "undefined" && userConnected == null){
                            ajaxPost(
                                null,
                                baseUrl+"/"+moduleId+"/person/getuser",
                                {"id" : userId},
                                function(data){
                                    userConnected = data;
                                },
                                null,
                                "json",
                                {
                                    async: false
                                }
                            );
                        }
                        if(userConnected != "undefined" && userConnected != null)
                            addInForm(userId, "citoyens", userConnected.name+" ("+tradDynForm.me+")", userConnected.profilThumbImageUrl, userConnected);
            }
        }


        if(typeof params.invite != "undefined" && params.invite != null && params.invite === true){
            invite = params.invite;
            lazyLoad( modules.co2.url+'/js/invite.js',
                    null,
                    function(){
                        if(typeof params.roles != "undefined" && params.roles != null){
                            roles = params.roles;
                        }
                        return true ;
                    });

        }
        else
            invite = null ;

        if(typeof params.search != "undefined" && params.search != null ){
            search = params.search;
        }   
        
        // remove element
        function removeFromForm(id){
            $(".coInput-form-list-finder-"+generateID+" .element-finder-"+id).remove();
            delete object[id];
            delete fullObject[id];
            // triggerCustomEvents("valueChange", name, object, payload, $html);
            triggerCustomEvents("inputBlur", name, object, payload, $html);
        }

        // add element 
        function addInForm(id, type, name, img, data , onInit=true){
            img= (notEmpty(img)) ? baseUrl + img : modules.co2.url + "/images/thumb/default_"+type+".png";
            var str="";
            str="<div class='col-xs-12 element-finder element-finder-"+id+" shadow2 padding-10'>"+
                        '<img src="'+ img+'" class="img-circle pull-left margin-right-10" height="35" width="35">'+
                        '<span class="info-contact pull-left margin-right-5">' +
                            '<span class="name-contact text-bold">' + name + '</span>'+
                            '<br/>'+
                            '<span class="cp-contact pull-left">' + trad[((type == "citoyens" ) ? "citizens" : type)]+ '</span>'+
                        '</span>' +
                        '<button class="bg-red text-white pull-right btn-remove-element" style="border: none;font-size: 15px;border-radius: 6px;padding: 5px 10px !important;" data-id="'+id+'" ><i class="fa fa-times"></i></button>'+
                "</div>";

            object[id]={"type" : type, "name" : name};
    
            if(typeof fullObject == "undefined"){
                fullObject={};
                fullObject={};
            }
            if(typeof fullObject != "undefined" && !fullObject){
                fullObject={};
            }
            fullObject[id]=data;
    
            if(notNull(finderPopulation) && notNull(finderPopulation[id]) && notNull(finderPopulation[id].email)){
                object[id].email = finderPopulation[id].email;
            }
    
            if(notNull(roles) && notNull(roles)){
                object[id].roles = roles;
            }
    
            if(notNull(search) && notNull(search) && notNull(search.filterBy)){
                if(notNull( selectedItems[id][search.filterBy] ) ) {
                    var fBy = {} ;
                    var split = id.split(".");
                    fBy[search.filterBy] = selectedItems[id][search.filterBy] ;
                    object[split[0]]=Object.assign({}, object[id], fBy);
                    delete object[id];
                }
            };  

            if (onInit) {
                $html.find(".coInput-form-list-finder-"+generateID).append(str);
                setTimeout(function () {
                    triggerCustomEvents("inputBlur", params.id, object, payload, $html);
                },2000);
            } else {
                $(".coInput-form-list-finder-"+generateID).append(str);
            } 
            
        }

        //show the panel 
        function showPanel(){
            selectedItems={};
            titleForm="Sélectionner dans la liste";
            if(!notNull(multiple) && !multiple)
                titleForm+="(Un seulement)";
            var dialog = bootbox.dialog({
                title: titleForm,
                message: '<div id="finderSelectHtml" style="max-height:480px;">'+
                            '<input class="form-group form-control" type="text" id="populateFinder" placeholder="'+placeholder+'"/>'+
                            '<div id="list-finder-selected" style="max-height:200px; overflow-y:auto"></div>'+
                            '<hr/><div id="list-finder-selection'+generateID+'" style="height:200px; overflow-y:auto" class="shadow2"><p><i class="fa fa-spin fa-spinner"></i> '+trad.currentlyloading+'...</p></div>'+
                            '<form id="form-invite" class="hidden col-sm-12" style="margin-top : 10px">'+
                            //"<div id='form-invite' class='hidden'>"+
                                '<div class="row margin-bottom-10">'+
                                    '<div class="col-md-1 col-md-offset-1" id="iconUser">'+
                                        '<i class="fa fa-user fa-2x"></i>'+
                                    '</div>'+
                                    '<div class="col-md-9">'+
                                        '<input class="invite-name form-control" placeholder="Name" id="inviteName" name="inviteName" value="" />'+
                                    '</div>'+
                                '</div>'+
                                '<div class="row margin-bottom-10">'+
                                    '<div class="col-md-1 col-md-offset-1">'+
                                        '<i class="fa fa-envelope-o fa-2x"></i>'+
                                    '</div>'+
                                    '<div class="col-md-9">'+
                                        '<input class="invite-email form-control" placeholder="Email" id="inviteEmail" name="inviteEmail" value="" />'+
                                    '</div>'+
                                '</div>'+
                                '<div class="row margin-bottom-10">'+
                                    '<div class="col-md-1 col-md-offset-1">'+
                                        '<i class="fa fa-align-justify fa-2x"></i>'+
                                    '</div>'+
                                    '<div class="col-md-9">'+
                                        '<textarea class="invite-text form-control" id="inviteText" name="inviteText" rows="4" placeholder="Custom message"></textarea>'+
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                        '</div>',
                closeButton:false,
                buttons: {
                    cancel: {
                        label: trad.Close,
                        className: 'btn-default',
                        callback: function(){
                            dialog.modal('hide');
                        }
                    },
                    ok: {
                        label: trad.Add,
                        className: 'btn-success',
                        callback: function(e){
                            e.preventDefault();
                            addSelectedToForm(multiple);
                        }
                    }
                }
            });
            dialog.on('shown.bs.modal', function(e){
                //setTimeout(function(){
                finderPopulation={};
                if(userId != "" && typeof myContacts != "undefined" && myContacts == null){
                    ajaxPost(
                        null,
                        baseUrl+"/"+moduleId+"/person/getcontacts",
                        {"id" : userId},
                        function(data){
                            myContacts = data;
                        },
                        null,
                        "json",
                        {
                            async: false
                        }
                    );
                }
                if(typeof myContacts != "undefined" && notNull(myContacts) && initContacts){
                    $.each(params.initType, function(e, type){
                        if(typeof myContacts[type] != "undefined"){
                            $.each(myContacts[type], function(e, v){
                                if(	typeof toBeValidated == "undefined" ||
                                    toBeValidated == null ){
                                    finderPopulation[e]={
                                        "name": v.name,
                                        "type": type,
                                        "profilThumbImageUrl":v.profilThumbImageUrl
                                    };
                                } else if( toBeValidated == true &&
                                            ( 	typeof v.source == "undefined" ||
                                                (	typeof v.source != "undefined" &&
                                                    v.source != null &&
                                                    ( typeof v.source.toBeValidated == "undefined" ||
                                                        v.source.toBeValidated == null ||
                                                        ( 	typeof costum != "undefined" &&
                                                            costum != null &&
                                                            typeof costum.slug != "undefined" &&
                                                            costum.slug != null &&
                                                            ( 	typeof v.source.toBeValidated[costum.slug] == "undefined" ||
                                                                v.source.toBeValidated[costum.slug] == null ||
                                                                v.source.toBeValidated[costum.slug] != true ) ) ) ) ) ) {
    
                                    finderPopulation[e]={
                                        "name": v.name,
                                        "type": type,
                                        "profilThumbImageUrl":v.profilThumbImageUrl
                                    };
                                }
    
    
    
                                if(type=="events"){
                                    finderPopulation[e].startDate=v.startDate;
                                    finderPopulation[e].endDate=v.endDate;
                                }
                            });
                        }
                    });
    
                }
                populateFinder(finderPopulation, multiple, true);
                if(typeof initBySearch != "undefined" && initBySearch){
                    searchAndPopulateFinder("");
                }
                $("#populateFinder").keyup(function(){
                    if($(this).val().length < 3){
                        filterPopulation($(this).val());
                    }else{
                        if(notNull(open) && open){
                            filterPopulation($(this).val());
                            searchAndPopulateFinder($(this).val());
                        }else{
                            filterPopulation($(this).val());
                        }
                    }
                });
            });
    
            if(typeof invite != "undefined" && invite != null && invite === true){
                dialog.on('shown.bs.modal', function(e){
                     var paramsInvite = {
                         container : "#finderSelectHtml #form-invite"
                     };
                      inviteObj.init(paramsInvite);
                     inviteObj.formInvite(function(data){
    
                         finderPopulation[data.id]={
                            "name": data.name,
                            "email": data.mail,
                            "type": "citoyens",
                            "profilThumbImageUrl": modules.co2.url + "/images/thumb/default_citoyens.png"
                        };
                        selectedItems[data.id]=finderPopulation[data.id];
                        tradTypeElement = (typeof trad[data.type] != "undefined") ? trad[data.type] : data.type;
                         var str ="<div class='population-elt-finder population-elt-finder-"+data.id+" col-xs-12' data-value='"+data.id+"'>"+
                                    '<div class="checkbox-content pull-left">'+
                                        '<label>'+
                                            '<input type="checkbox" class="check-population-finder checkbox-info" data-value="'+data.id+'">'+
                                            '<span class="cr"><i class="cr-icon fa fa-check"></i></span>'+
                                        '</label>'+
                                    '</div>'+
                                    "<div class='element-finder element-finder-"+data.id+"'>"+
                                        '<img src="'+ modules.co2.url + "/images/thumb/default_citoyens.png" +'" class="thumb-send-to pull-left img-circle" height="40" width="40">'+
                                        '<span class="info-contact pull-left margin-left-20">' +
                                            '<span class="name-element text-dark text-bold" data-id="'+data.id+'">' + data.name + '</span>'+
                                            '<br/>'+
                                            '<span class="type-element text-light pull-left">' + tradTypeElement+ '</span>'+
                                        '</span>' +
                                    "</div>"+
                                "</div>";
    
                         $("#form-invite").addClass("hidden");
                         $("#list-finder-selected").append(str);
                         $(".check-population-finder[data-value='"+data.id+"'").trigger("click");
                         $("#list-finder-selection"+generateID).removeClass("hidden");
    
                         if(invite!=null && typeof invite.callback=="function"){
                             invite.callback(data);
                         }
    
                         return true;
                     });
    
                });
            }
    
    
        }

        //populate
        function populateFinder(obj, multiple, first){
            str="";
            if(first && typeof object[userId] == "undefined" && typeof initMe != "undefined" && initMe){
                img= (typeof userConnected.profilThumbImageUrl !== 'undefined' && userConnected.profilThumbImageUrl) ? baseUrl + userConnected.profilThumbImageUrl : modules.co2.url + "/images/thumb/default_citoyens.png";
                if(typeof finderPopulation[userId]=="undefined"){
                    finderPopulation[userId]={
                        name:userConnected.name + ' ('+tradDynForm.me+')',
                        type:"citoyens",
                        profilThumbImageUrl:userConnected.profilThumbImageUrl
                    };
                }
                str+="<div class='population-elt-finder population-elt-finder-"+userId+" col-xs-12' data-value='"+userId+"'>"+
                        '<div class="checkbox-content pull-left">'+
                            '<label>'+
                                '<input type="checkbox" class="check-population-finder checkbox-info" data-value="'+userId+'">'+
                                '<span class="cr"><i class="cr-icon fa fa-check"></i></span>'+
                            '</label>'+
                        '</div>'+
                        "<div class='element-finder element-finder-"+userId+"'>"+
                            '<img src="'+img+'" class="thumb-send-to pull-left img-circle" height="40" width="40">'+
                            '<span class="info-contact pull-left margin-left-20">' +
                                '<span class="name-element text-dark text-bold" data-id="'+userId+'">' + userConnected.name + ' ('+tradDynForm.me+')</span>'+
                                '<br/>'+
                                '<span class="type-element text-light pull-left">' + trad.citizens+ '</span>'+
                            '</span>' +
                        "</div>"+
                    "</div>";
            }
            if(notNull(obj)){
                $.each(obj, function(e, v){
                    if(typeof object[e] == "undefined" && e != userId){
                        if(typeof finderPopulation[e]== "undefined")
                            finderPopulation[e]=v;
                        if($(".population-elt-finder-"+e).length <= 0){
                            typeElt=(typeof v.collection != "undefined") ? v.collection : v.type;
                            img= (typeof v.profilThumbImageUrl !== 'undefined' && v.profilThumbImageUrl) ? baseUrl + v.profilThumbImageUrl : modules.co2.url + "/images/thumb/default_"+typeElt+".png";
                                tradTypeElement = (typeof trad[typeElt] != "undefined") ? trad[typeElt] : typeElt;
                                str+="<div class='population-elt-finder population-elt-finder-"+e+" col-xs-12' data-value='"+e+"'>"+
                                    '<div class="checkbox-content pull-left">'+
                                        '<label>'+
                                            '<input type="checkbox" class="check-population-finder checkbox-info" data-value="'+e+'">'+
                                            '<span class="cr"><i class="cr-icon fa fa-check"></i></span>'+
                                        '</label>'+
                                    '</div>'+
                                    "<div class='element-finder element-finder-"+e+"'>"+
                                        '<img src="'+ img+'" class="thumb-send-to pull-left img-circle" height="40" width="40">'+
                                        '<span class="info-contact pull-left margin-left-20">' +
                                            '<span class="name-element text-dark text-bold" data-id="'+e+'">' + (notNull(field) && showField ? jsonHelper.getValueByPath(v,field) : v.name) + '</span>'+
                                            '<br/>'+
                                            '<span class="type-element text-light pull-left">' + tradTypeElement + '</span>'+
                                        '</span>' +
                                    "</div>"+
                                "</div>";
                        }else{
                            $(".population-elt-finder-"+e).show();
                        }
                    }
                });
            }
            if(first)
                $("#list-finder-selection"+generateID).html(str);
            else
                $("#list-finder-selection"+generateID).append(str);
            bindSelectItems(multiple);
        }

        //bind selected items
        function bindSelectItems(multiple){
            $(".population-elt-finder").off().on("click", function(e){
                if(e.target.className!="cr-icon fa fa-check" && e.target.className!="check-population-finder checkbox-info")
                    $(".check-population-finder[data-value='"+$(this).data("value")+"'").trigger("click");
            });
            $(".check-population-finder").off().on("click", function(){
                if($(this).is(":checked")){
                    if(!multiple){
                       selectedItems={};
                        $("#list-finder-selected").html("");
                    }
                    selectedItems[$(this).data("value")]=finderPopulation[$(this).data("value")];
                    $(".population-elt-finder-"+$(this).data("value")).prependTo("#list-finder-selected");
                }else{
                    delete selectedItems[$(this).data("value")];
                    $(".population-elt-finder-"+$(this).data("value")).prependTo("#list-finder-selection"+generateID);
                }
            });
        }   

        //add selected items
        function addSelectedToForm(multiple){
            if(Object.keys(selectedItems).length > 0){
                if(!multiple){
                    object={};
                    fullObject={};
                    $(".coInput-form-list-finder-"+generateID).html("");
                }
                $.each(selectedItems, function(e, v){
                    typeCol=(typeof v.collection != "undefined") ? v.collection : v.type;
                    v.name = (notNull(field) && showField ? jsonHelper.getValueByPath(v,field) : v.name)
                    addInForm(e, typeCol, v.name, v.profilThumbImageUrl,v,false);
                });

                $(".coInput-form-list-finder-"+generateID+" .btn-remove-element").off("click").on("click", function(e){
                    e.preventDefault();
                    id=$(this).data("id");
                    removeFromForm(id);
                })

                if(typeof callbackSelect != "undefined" && typeof callbackSelect == "function") callbackSelect(selectedItems);
                triggerCustomEvents("inputBlur", params.id, object, payload, $html);
            }
        }

        //filter population 
        function filterPopulation(searchVal){
            //recherche la valeur recherché dans les 3 champs "name", "cp", et "city"
            if(searchVal != "")	$("#list-finder-selection"+generateID+" .population-elt-finder").hide();
            else $("#list-finder-selection"+generateID+" .population-elt-finder").show();
    
            $.each($("#list-finder-selection"+generateID+" .population-elt-finder .name-element"), function() {
                content = $(this).html();
                found = content.search(new RegExp(searchVal, "i"));
                if(found >= 0)
                $("#list-finder-selection"+generateID+" .population-elt-finder-"+$(this).data("id")).show();
            });
        }

        //search and populate finder
        function searchAndPopulateFinder(text){
            $('.finder-create-new').hide();

            var dataSearch = {
                searchType : params.initType,
                name: text
            };
            if(notNull(field)){
                dataSearch["textPath"] = field;
                dataSearch["fields"] = [field.split('.')[0]]
            }
            if(notNull(search) && notNull(search)){
                dataSearch = Object.assign({}, dataSearch, search);
            }
            if(filters){
                dataSearch['filters'] = filters;
            }
            $.ajax({
                type: "POST",
                url: baseUrl+"/"+moduleId+"/search/globalautocomplete",
                data: dataSearch,
                dataType: "json",
                success: function(retdata){
                    if(!retdata){
                        toastr.error(retdata.content);
                    } else {
                        if(retdata.results.length == 0 && invite === true){
                            $("#form-invite").removeClass("hidden");
                            $("#list-finder-selection"+generateID).addClass("hidden");

                            var search =  "#finderSelectHtml #populateFinder" ;
                            var id = "#finderSelectHtml #form-invite" ;
                            var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
                            if(emailReg.test( $(search).val() )){
                                $(id+' #inviteEmail').val( $(search).val());
                                var nameEmail = $(search).val().split("@");
                                $(id+" #inviteName").val(nameEmail[0]);
                            }else{
                                $(id+" #inviteName").val($(search).val());
                                $(id+" #inviteEmail").val("");
                            }
                        } else if (retdata.results.length === 0 && noResult !== null) {
                            $('.finder-create-new').show().text(noResult.label);
                            $('.finder-create-new').off('click', noResult.action).on('click', noResult.action);
                        } else{
                            $("#form-invite").addClass("hidden");
                            $("#list-finder-selection"+generateID).removeClass("hidden");
                            populateFinder(retdata.results, multiple , true);
                        }
                    }
                }
            });
        }

        //on click remove element
        $html.find(".coInput-form-list-finder-"+generateID+" .btn-remove-element").off("click").on("click", function(e){
            e.preventDefault();
            id=$(this).data("id");
            removeFromForm(id);
        })
        

        $html.find(".selectParent"+generateID).click(function(e){
                e.preventDefault();
                showPanel();
        })

        $html.val = function() {
            
        }

        return $html;
    }

    //INPUT FORM LOCALITY
    coreTypes.formLocality = function(context, options){
        var name = options.name || "formLocality",
        payload = options.payload || "",
        classInput = options.class || "",
        generateID = generateUUID(),
        coInputFormLocation = options.defaultValue || [],
        coInputMap = {};
        var formInMap = { 
            actived : false,
            timeoutAddCity : null,
            countryList : null,
            forced : {map:{}},
            countryCodeList : {},
            map : null,
            NE_insee : "",
            NE_lat : "",
            NE_lng : "",
            NE_city : "",
            NE_cp : "",
            NE_street : "",
            NE_country : "",
            NE_level5 : "",
            NE_level5Name : "",
            NE_level4 : "",
            NE_level4Name : "",
            NE_level3 : "",
            NE_level3Name : "",
            NE_level2 : "",
            NE_level2Name : "",
            NE_level1 : "",
            NE_level1Name : "",
            NE_localityId : "",
            NE_betweenCP : false,
            geo : {
                lat : "0",
                lon : "0"
            },
            geoShape : "",
            typeSearchInternational : "",
            formType : "",
            update : null,
            data : null,
            updateLocality : false,
            addressesIndex : false,
            saveCities : {},
            bindActived : false,
            rules : {
                required : false,
                country : false,
                level5 : false,
                level4 : false,
                level3 : false,
                level2 : false,
                level1 : false,
            },
        }
        
        var $html = $(`
            <div class="form-group input-group-sm ${classInput}">
                ${ options.label ? `<label>${options.label}</label>`:"" }
                <div class="formLocality-${name}">
                    <div id='divNewAddress' class='text-dark col-xs-12 no-padding '>
                        <button  class='btn btn-success col-xs-12' style='margin-bottom: 10px;' type='text' id='newAddress${generateID}'>
                            <i class="fa fa-plus"></i>${tradDynForm.addANewAddress}
                        </button>
                    </div>
                </div>
                <div class='coInput-form-list-formLocality-${generateID}' style="max-height:140px; overflow-y:auto" ></div>
            </div>
        `);

        if ((typeof options.icon != "undefined") && options.icon != ""){
            $html.find("label").prepend(`
                <i class="fa fa-${options.icon}"></i> 
            `)
        }

        //show the panel 
        function showPanel(){
            titleForm="Sélectionner un adresse";
            var dialog = bootbox.dialog({
                title: titleForm,
                message: 
                        '<div id="adressSelectHtml'+generateID+'" style="display:flow-root">'+
                            "<div class='col-xs-12 inline-block padding-15 form-in-map formLocality'>"+
                                '<label style="font-size: 13px;" class="col-xs-12 text-left control-label no-padding" for="newElement_country">'+
                                    '<i class="fa fa-chevron-down"></i> '+tradDynForm.country+
                                '</label>'+
                                "<select class='col-xs-12 form-control' name='newElement_country' >"+
                                    "<option value=''>"+trad.chooseCountry+"</option>"+
                                "</select>"+
                                "<div id='divCity' class='hidden dropdown pull-left col-md-12 col-xs-12 no-padding'> "+
                                    '<label style="font-size: 13px;" class="col-xs-12 text-left control-label no-padding" for="newElement_city">'+
                                        '<i class="fa fa-chevron-down"></i> '+trad.city  +
                                    '</label>'+
                                    "<input autocomplete='off' class='col-xs-12' type='text' name='newElement_city' placeholder='"+trad['Search a city, a town or a postal code']+"'>"+
                                    "<ul class='dropdown-menu col-xs-12' id='dropdown-newElement_locality-found' style='margin-top: -2px; background-color : #ea9d13; max-height : 300px ; overflow-y: auto'>"+
                                        "<li><a href='javascript:' class='disabled'>"+tradDynForm.searchACityATownOrAPostalCode +"</a></li>"+
                                    "</ul>"+
                                "</div>"+
                                "<div id='divStreetAddress' class='hidden dropdown pull-left col-md-12 col-xs-12 no-padding'> "+
                                    '<label style="font-size: 13px;" class="col-xs-12 text-left control-label no-padding" for="newElement_street">'+
                                        '<i class="fa fa-chevron-down"></i> '+trad.streetFormInMap +
                                    '</label>'+
                                    "<input class='col-xs-11'  autocomplete='off' type='text' style='margin-right:-3px;height: 35px;' name='newElement_street' placeholder='"+trad.streetFormInMap +"'>"+
                                    "<a href='javascript:;' class='col-xs-1 btn btn-default' style='padding:3px;border-radius:0 4px 4px 0 ; height: 35px;' type='text' id='newElement_btnSearchAddress'><i class='fa fa-search'></i></a>"+
                                "</div>"+
                                "<div class='dropdown pull-left col-xs-12 no-padding'> "+
                                    "<ul class='dropdown-menu' id='dropdown-newElement_streetAddress-found' style='margin-top: -2px; width:100%;background-color : #ea9d13; max-height : 300px ; overflow-y: auto'>"+
                                        "<li><a href='javascript:' class='disabled'>"+trad.currentlyresearching +"</a></li>"+
                                    "</ul>"+
                                "</div>"+
                                "<div id='alertGeo' class='alert alert-warning col-xs-12 hidden' style='margin-bottom: 10px;margin-top: 10px'>"+
							        "<strong>Warning!</strong> "+tradDynForm.doNotForgetToGeolocateYourAddress+
							    "</div>"+
                            '</div>'+
                            "<div id='sumery' class='text-dark col-xs-12 no-padding'>"+
                                "<hr class='col-xs-12'>"+
                                "<h4 class='text-center'>"+tradDynForm.addressSummary +" : </h4>"+
                                "<div class='text-center' id='country_sumery' class='col-xs-12'>"+
                                    "<span>"+tradDynForm.country +" : </span>"+
                                    "<b><span id='country_sumery_value'></span></b>"+
                                "</div>"+
                                "<div id='divMapLocality' class='col-xs-12 no-padding' style='height: 300px;'></div>"+
                            "</div>"+
                        "</div>"
                ,
                closeButton:false,
                buttons: {
                    cancel: {
                        label: trad.Close,
                        className: 'btn-default',
                        callback: function(){
                            dialog.modal('hide');
                        }
                    },
                    ok: {
                        label: trad.Add,
                        className: 'btn-success',
                        callback: function(e){
                            e.preventDefault();
                            formLocalityaddInForm();
                        }
                    }
                }
            });

            

            dialog.on('shown.bs.modal', function(e){
                ajaxPost(
                    null,
                    baseUrl+"/"+moduleId+"/opendata/getcountries/",
                    null,
                    function(data){
                        formInMap.countryList = data;
                        if(formInMap.countryList!=null){
                            var strCountry = "";
                            $.each(formInMap.countryList, function(key, v){
                                formInMap.countryCodeList[v.countryCode] = v ;
                                strCountry += "<option value='"+v.countryCode+"' data-lat='"+v.geo.latitude+"' data-lng='"+v.geo.longitude+"'>"+v.name+"</option>";
                            });
                            $('#adressSelectHtml'+generateID+' [name="newElement_country"]').append(strCountry);
                        }
                    },
                    null
                );

                $('#adressSelectHtml'+generateID+' [name="newElement_country"]').change(function(){
                    formInMap.NE_country = $(this).val();

                    var countryLat = 0;
                    var countryLng = 0;
                    var selectedOption = $(this).find('option:selected');
                    countryLat = selectedOption.data('lat');
                    countryLng = selectedOption.data('lng');
                    $("#adressSelectHtml"+generateID+" #divStreetAddress").addClass("hidden");
                    $("#adressSelectHtml"+generateID+" #dropdown-newElement_cp-found").html("<li><a href='javascript:' class='disabled'>"+trad['Currently researching']+"</a></li>");
                    $("#adressSelectHtml"+generateID+" #dropdown-newElement_city-found").html("<li><a href='javascript:' class='disabled'>"+trad['Search a city, a town or a postal code'] +"</a></li>");
                    if(formInMap.NE_country != ""){
                        $("#adressSelectHtml"+generateID+" #divCP").addClass("hidden");
                        $("#adressSelectHtml"+generateID+" #divCity").removeClass("hidden");
                        $('#adressSelectHtml'+generateID+' [name="newElement_street"]').val("");
                        $('#adressSelectHtml'+generateID+' [name="newElement_city"]').val("");
                    }else{
                        $("#adressSelectHtml"+generateID+" #divCity").addClass("hidden");
                    }

                    $('#adressSelectHtml'+generateID+' #country_sumery_value').html(formInMap.NE_country);

                    var paramsMarker = {
                        elt : {
                            id : 0,
                            type : "2D",
                            geo : {
                                latitude :  countryLat,
                                longitude : countryLng,
                            }
                        },
                        opt : {
                            draggable: true
                        },
                        center : true
                    };
                    coInputMap.clearMap();
                    coInputMap.addMarker(paramsMarker);
                    coInputMap.map.setZoom(5);
                    coInputMap.hideLoader();
                });

                $('#adressSelectHtml'+generateID+' [name="newElement_city"]').keyup(delay(function (e) {
                    e.preventDefault();
                    $("#dropdown-city-found").show();
                    if($('#adressSelectHtml'+generateID+' [name="newElement_city"]').val().trim().length > 1){
                        formInMap.NE_city = $('#adressSelectHtml'+generateID+' [name="newElement_city"]').val();
                        if(notNull(formInMap.timeoutAddCity))
                            clearTimeout(formInMap.timeoutAddCity);
    
                        formInMap.timeoutAddCity = setTimeout(function(){
                            autocompleteFormAddress("locality",$('#adressSelectHtml'+generateID+' [name="newElement_city"]').val());
                        }, 500);
                    }
                }, 750));

                $("#adressSelectHtml"+generateID+" #newElement_btnSearchAddress").click(function(){
                    $("#adressSelectHtml"+generateID+" #dropdown-newElement_streetAddress-found").hide();
                    $(this).html("<i class='fa fa-spin fa-spinner'></i>");
                    var requestPart = "";
                    var street 	= ($('#adressSelectHtml'+generateID+' [name="newElement_street"]').val()  != "") ? $('#adressSelectHtml'+generateID+' [name="newElement_street"]').val() : "";
                    var city 	= formInMap.NE_city;
                    var cp 		= formInMap.NE_cp;
                    var countryCode = formInMap.NE_country;
                    if($('#adressSelectHtml'+generateID+' [name="newElement_street"]').val() != ""){
                        providerName = "nominatim";
                        formInMap.typeSearchInternational = "address";
                        requestPart = addToRequest(requestPart, street);
                        requestPart = addToRequest(requestPart, city);
                        requestPart = addToRequest(requestPart, cp);
                    }else{
                        providerName = "communecter"
                        formInMap.typeSearchInternational = "city";
                        if(cp != ""){
                            requestPart = addToRequest(requestPart, cp);
                        }
                    }
                    formInMap.NE_street = $('#adressSelectHtml'+generateID+' [name="newElement_street"]').val();
                    $("#adressSelectHtml"+generateID+" #dropdown-newElement_streetAddress-found").html("<li><a href='javascript:'><i class='fa fa-spin fa-refresh'></i> "+trad.currentlyresearching+"</a></li>");
                    $("#adressSelectHtml"+generateID+" #dropdown-newElement_streetAddress-found").show();
                    var countryDataGouv = ["FR","GP","MQ","GF","RE","PM","YT","WF","PF","NC"];
                    if(countryDataGouv.indexOf(countryCode) != -1){
                        countryCode = formLocalitychangeCountryForNominatim(countryCode);
                        formLocalitycallDataGouv(requestPart, countryCode);
                    }else{
                        countryCode = formLocalitychangeCountryForNominatim(countryCode);
                        formLocalitycallNominatim(requestPart, countryCode);
                    }
                });
                var paramsMarker = {
                    elt : {
                        id : 0,
                        type : "2D",
                        geo : {
                            latitude : 0,
                            longitude: 0
                        }
                    },
                    opt : {
                        draggable: true
                    },
                    center : true
                };
                var paramsMap = {
                    container : "divMapLocality",
                    latLon : [ 0, 0],
                    activeCluster : false,
                    zoom : 16,
                    activePopUp : false
                }

                coInputMap = mapObj.init(paramsMap);

                coInputMap.addMarker(paramsMarker);
                coInputMap.markerList[0].on('drag', function (e) {
                    var latLonMarker = coInputMap.markerList[0].getLatLng();
                    formInMap.NE_lat = latLonMarker.lat;
                    formInMap.NE_lng = latLonMarker.lng;
                });
                coInputMap.hideLoader();
            });
        }

        // init var
        function initVarNE(){
            formInMap.NE_insee = "";
            formInMap.NE_lat = "";
            formInMap.NE_lng = "";
            formInMap.NE_city = "";
            formInMap.NE_cp = "";
            formInMap.NE_street = "";
            formInMap.NE_country = "";
            formInMap.NE_level5 = "";
            formInMap.NE_level5Name = "";
            formInMap.NE_level4 = "";
            formInMap.NE_level4Name = "";
            formInMap.NE_level3 = "";
            formInMap.NE_level3Name = "";
            formInMap.NE_level2 = "";
            formInMap.NE_level2Name = "";
            formInMap.NE_level1 = "";
            formInMap.NE_level1Name = "";
            formInMap.NE_localityId = "";
            formInMap.NE_betweenCP = false;
        }

        // remove element
        function removeFromForm(index){
            // Vérifier que l'index est valide
            if (index < 0 || index >= coInputFormLocation.length) {
                console.error("Index invalide:", index);
                return;
            }

            // Vérifie si l'élément à supprimer est le centre
            const isCenter = coInputFormLocation[index]?.center === true;

            // Supprime l'élément du tableau
            coInputFormLocation.splice(index, 1);

            // Si l'élément supprimé était le centre et qu'il reste des éléments
            if (isCenter && coInputFormLocation.length > 0) {
                // Assigner center uniquement au premier élément restant
                coInputFormLocation[0].center = true;
            }

            var newView = formLocalityInitViewElement();
            $(".coInput-form-list-formLocality-" + generateID).append(newView);
            $(".coInput-form-list-formLocality-"+generateID+" .btn-remove-element").off("click").on("click", function(e) {
                e.preventDefault();
                const index = $(this).data("index");
                removeFromForm(index);
            });

            $html.find(".coInput-form-list-formLocality-"+generateID+" .btn-adress-principal").off("click").on("click", function(e) {
                e.preventDefault();
                const index = $(this).data("index");
                if ( typeof coInputFormLocation[index] !== undefined || coInputFormLocation[index] === false ) {
                    coInputFormLocation.forEach((location, i) => {
                        if (i !== index) {
                            delete location.center;
                        }
                        // Set 'center' for the selected index
                        coInputFormLocation[index].center = true;
    
                        var newView = formLocalityInitViewElement();
                        $html.find(".coInput-form-list-formLocality-" + generateID).append(newView);
                        $html.find(".coInput-form-list-formLocality-"+generateID+" .btn-remove-element").off("click").on("click", function(e) {
                            e.preventDefault();
                            const index = $(this).data("index");
                            removeFromForm(index);
                        });
                
                    });
                    // Set 'center' for the selected index
                    coInputFormLocation[index].center = true;
                }
                triggerCustomEvents("inputBlur", name, coInputFormLocation, payload, $html);
            });

            triggerCustomEvents("inputBlur", name, coInputFormLocation, payload, $html);
        }

        // init view element 
        function formLocalityInitViewElement(){
            $(".coInput-form-list-formLocality-"+generateID).html("");
            var finalString = "";
            // Boucle sur chaque élément dans coInputFormLocation pour afficher les adresses
            coInputFormLocation.forEach((locObj, index) => {
                var strHTML = "";
                var str = "";
                if (locObj.address.addressCountry)
                    strHTML += locObj.address.addressCountry;
                if (locObj.address.postalCode)
                    strHTML += ", " + locObj.address.postalCode;
                if (locObj.address.addressLocality)
                    strHTML += ", " + locObj.address.addressLocality;
                if (locObj.address.streetAddress)
                    strHTML += ", " + locObj.address.streetAddress;

                if(locObj.center){
                    str = "<div class='col-xs-12 element-adress element-adress-"+index+" shadow2 padding-10'>"+
                            '<span class="info-contact pull-left margin-5">' +
                                '<span class="name-contact text-bold">' + strHTML + '</span>'+
                                '<br/>'+
                            '</span>' +
                            '<button class="bg-red text-white pull-right btn-remove-element" style="border: none;font-size: 15px;border-radius: 6px;padding: 5px 10px !important;" data-index="'+index+'" ><i class="fa fa-times"></i></button>'+
                            '<button class="bg-blue text-white pull-right btn-adress-principal margin-right-5" style="border: none;font-size: 15px;border-radius: 6px;padding: 5px 10px !important;" data-firstAdress="true" data-index="'+index+'"><i class="fa fa-map-marker"></i><span class="lblcentre">'+tradDynForm.mainLocality+'</span></button>'+
                    "</div>";
                } else {
                    str = "<div class='col-xs-12 element-adress element-adress-" + index + " shadow2 padding-10'>" +
                            '<span class="info-contact pull-left margin-5">' +
                                '<span class="name-contact text-bold">' + strHTML + '</span>' +
                                '<br/>' +
                            '</span>' +
                            '<button class="bg-red text-white pull-right btn-remove-element" style="border: none;font-size: 15px;border-radius: 6px;padding: 5px 10px !important;" data-index="' + index + '" ><i class="fa fa-times"></i></button>' +
                            '<button class="bg-blue text-white pull-right btn-adress-principal margin-right-5" style="border: none;font-size: 15px;border-radius: 6px;padding: 5px 10px !important;" data-index="' + index + '"><i class="fa fa-map-marker"></i></button>' +
                        "</div>";
                }

                // Ajouter l'élément HTML dans le conteneur
                finalString += str;
            });

            return finalString;
        }
        // add element 
         function formLocalityaddInForm(){
            var locObj = createLocalityObj();
            var str="";

            var strHTML = "";
			if( locObj.address.addressCountry)
				strHTML += locObj.address.addressCountry;
			if( locObj.address.postalCode)
				strHTML += ", "+locObj.address.postalCode;
			if( locObj.address.addressLocality)
				strHTML += ", "+locObj.address.addressLocality;
			if( locObj.address.streetAddress)
				strHTML += ", "+locObj.address.streetAddress;

            if( coInputFormLocation.length === 0 ){
                locObj.center = true;
                str="<div class='col-xs-12 element-adress element-adress-"+coInputFormLocation.length+" shadow2 padding-10'>"+
                        '<span class="info-contact pull-left margin-5">' +
                            '<span class="name-contact text-bold">' + strHTML + '</span>'+
                            '<br/>'+
                        '</span>' +
                        '<button class="bg-red text-white pull-right btn-remove-element" style="border: none;font-size: 15px;border-radius: 6px;padding: 5px 10px !important;" data-index="'+coInputFormLocation.length+'" ><i class="fa fa-times"></i></button>'+
                        '<button class="bg-blue text-white pull-right btn-adress-principal margin-right-5" style="border: none;font-size: 15px;border-radius: 6px;padding: 5px 10px !important;"  data-index="'+coInputFormLocation.length+'"><i class="fa fa-map-marker"></i><span class="lblcentre">'+tradDynForm.mainLocality+'</span></button>'+
                "</div>";
            } else {
                const newIndex = coInputFormLocation.length;
                str="<div class='col-xs-12 element-adress element-adress-"+newIndex+" shadow2 padding-10'>"+
                        '<span class="info-contact pull-left margin-5">' +
                            '<span class="name-contact text-bold">' + strHTML + '</span>'+
                            '<br/>'+
                        '</span>' +
                        '<button class="bg-red text-white pull-right btn-remove-element" style="border: none;font-size: 15px;border-radius: 6px;padding: 5px 10px !important;" data-index="'+newIndex+'" ><i class="fa fa-times"></i></button>'+
                        '<button class="bg-blue text-white pull-right  btn-adress-principal margin-right-5" style="border: none;font-size: 15px;border-radius: 6px;padding: 5px 10px !important;"  data-index="'+newIndex+'"><i class="fa fa-map-marker"></i></button>'+
                "</div>";
            }

            if ( locObj.address.addressLocality === "" ){
                toastr.error(tradCms.plsSelectCity);
            } else {
                coInputFormLocation.push(locObj);
                $(".coInput-form-list-formLocality-"+generateID).append(str);
    
                $(".coInput-form-list-formLocality-"+generateID+" .btn-remove-element").off("click").on("click", function(e){
                    e.preventDefault();
                    index = $(this).data("index");
                    removeFromForm(index);
                })

                $html.find(".coInput-form-list-formLocality-"+generateID+" .btn-adress-principal").off("click").on("click", function(e) {
                    e.preventDefault();
                    const index = $(this).data("index");
                    if ( typeof coInputFormLocation[index] !== undefined || coInputFormLocation[index] === false ) {
                        coInputFormLocation.forEach((location, i) => {
                            if (i !== index) {
                                delete location.center;
                            }
                            // Set 'center' for the selected index
                            coInputFormLocation[index].center = true;
        
                            var newView = formLocalityInitViewElement();
                            $html.find(".coInput-form-list-formLocality-" + generateID).append(newView);
                            $html.find(".coInput-form-list-formLocality-"+generateID+" .btn-remove-element").off("click").on("click", function(e) {
                                e.preventDefault();
                                const index = $(this).data("index");
                                removeFromForm(index);
                            });
                    
                        });
                        // Set 'center' for the selected index
                        coInputFormLocation[index].center = true;
                    }
                    triggerCustomEvents("inputBlur", name, coInputFormLocation, payload, $html);
                });
                
                triggerCustomEvents("inputBlur", name, coInputFormLocation, payload, $html);
            }
        }

        function createLocalityObj(){
            var locality = {
				address : {
					"@type" : "PostalAddress",
					codeInsee : formInMap.NE_insee,
					streetAddress : formInMap.NE_street.trim(),
					postalCode : formInMap.NE_cp,
					addressLocality : formInMap.NE_city,
					level1 : formInMap.NE_level1,
					level1Name : formInMap.NE_level1Name,
					addressCountry : formInMap.NE_country,
					localityId : formInMap.NE_localityId

				},
				geo : {
					"@type" : "GeoCoordinates",
					latitude : formInMap.NE_lat,
					longitude : formInMap.NE_lng
				},
				geoPosition : {
					"type" : "Point",
					"coordinates" : [ parseFloat(formInMap.NE_lng), parseFloat(formInMap.NE_lat) ]
				}
			};

			if( notEmpty(formInMap.NE_level2) && formInMap.NE_level2 != "undefined" ){
				locality.address.level2 = formInMap.NE_level2;
				locality.address.level2Name = formInMap.NE_level2Name;
			}
			if(notEmpty(formInMap.NE_level3 != "" && formInMap.NE_level3 != "undefined")){
				locality.address.level3 = formInMap.NE_level3;
				locality.address.level3Name = formInMap.NE_level3Name;
			}
			if(notEmpty(formInMap.NE_level4 != "" && formInMap.NE_level4 != "undefined")){
				locality.address.level4 = formInMap.NE_level4;
				locality.address.level4Name = formInMap.NE_level4Name;
			}
			if(notEmpty(formInMap.NE_level5 != "" && formInMap.NE_level5 != "undefined")){
				locality.address.level5 = formInMap.NE_level5;
				locality.address.level5Name = formInMap.NE_level5Name;
			}

			return locality;
        }

        function formLocalitycallDataGouv(requestPart, countryCode){
            $("#adressSelectHtml"+generateID+" #dropdown-newElement_streetAddress-found").html("<i class='fa fa-spin fa-refresh'></i> "+trad.currentlyresearching+" <small>Data Gouv</small>");
			var url = "//api-adresse.data.gouv.fr/search/?q=" + requestPart;
            $.ajax({
				url: url,
				type: 'GET',
				dataType: 'json',
				async:false,
				crossDomain:true,
				complete: function () {},
				success: function (objDataGouv){
					if(objDataGouv.features.length != 0){
						var commonGeoObj = getCommonGeoObject(objDataGouv.features, "data.gouv");
						var res = addResultsInForm(commonGeoObj, countryCode);
						if(res == 0)
							formLocalitycallNominatim(requestPart, countryCode);
					}else{
						formLocalitycallNominatim(requestPart, countryCode);
					}
				},
				error: function (thisError) {
					error(thisError);
				}
			});
        }

        function formLocalitycallNominatim(requestPart, countryCode){
            $("#adressSelectHtml"+generateID+" #dropdown-newElement_streetAddress-found").html("<i class='fa fa-spin fa-refresh'></i> "+trad.currentlyresearching+" <small>Nominatim</small>");
			var url = url = "//nominatim.openstreetmap.org/search?q=" + requestPart + "," + countryCode + "&format=json&polygon=0&addressdetails=1";
            $.ajax({
				url: url,
				type: 'GET',
				dataType: 'json',
				async:false,
				crossDomain:true,
				complete: function () {},
				success: function (objNomi){
					if(objNomi.length != 0){
						var commonGeoObj = getCommonGeoObject(objNomi, "nominatim");
						var res = addResultsInForm(commonGeoObj, countryCode);
						if(res == 0)
							formLocalitycallGoogle(requestPart, countryCode);
					}else{
						formLocalitycallGoogle(requestPart, countryCode);
					}
				},
				error: function (thisError) {
					mylog.log(thisError);
				}
			});
        }

        function formLocalitycallGoogle(){
            $("#adressSelectHtml"+generateID+" #dropdown-newElement_streetAddress-found").html("<i class='fa fa-spin fa-refresh'></i> "+trad.currentlyresearching+" <small>Google Maps</small>");
			var url = "//maps.googleapis.com/maps/api/geocode/json?address=" + requestPart + "," + countryCode;
            $.ajax({
				url: url,
				type: 'GET',
				dataType: 'json',
				async:false,
				crossDomain:true,
				complete: function () {},
				success: function (objDataGoogle){
					if(objDataGoogle.features.length != 0){
						var commonGeoObj = getCommonGeoObject(objDataGoogle.features, "data.gouv");
						var res = addResultsInForm(commonGeoObj, countryCode);
						if(res == 0)
							formLocalitycallGoogle(requestPart, countryCode);
					}else{
						formLocalitycallGoogle(requestPart, countryCode);
					}
				},
				error: function (thisError) {
					mylog.log(thisError);
				}
			});
        }

        function addResultsInForm(commonGeoObj, countryCode){
			var res = commonGeoObj; 
			var html = "";
			$.each(res, function(key, value){
				if(notEmpty(value.countryCode)){
					if(value.countryCode.toLowerCase() == countryCode.toLowerCase()){
						html += "<li><a href='javascript:;' class='item-street-found' data-lat='"+value.geo.latitude+"' data-lng='"+value.geo.longitude+"'><i class='fa fa-marker-map'></i> "+value.name+"</a></li>";
					}
				}
			});
			if(html == "") html = "<i class='fa fa-ban'></i> "+trad.noresult;
			$("#adressSelectHtml"+generateID+" #dropdown-newElement_streetAddress-found").html(html);
			$("#adressSelectHtml"+generateID+" #dropdown-newElement_streetAddress-found").show();
			$("#adressSelectHtml"+generateID+" #newElement_btnSearchAddress").html("<i class='fa fa-search'></i>");
			
            $("#adressSelectHtml"+generateID+" .item-street-found").click(function(){
        		$("#adressSelectHtml"+generateID+" #dropdown-newElement_streetAddress-found").hide();
        		$('#adressSelectHtml'+generateID+' [name="newElement_lat"]').val($(this).data("lat"));
        		$('#adressSelectHtml'+generateID+' [name="newElement_lng"]').val($(this).data("lng"));

                var paramsMarker = {
                    elt : {
                        id : 0,
                        type : "2D",
                        geo : {
                            latitude :  $(this).data("lat"),
                            longitude : $(this).data("lng"),
                        }
                    },
                    opt : {
                        draggable: true
                    },
                    center : true
                };
                coInputMap.clearMap();
                coInputMap.addMarker(paramsMarker);
                setTimeout(function () {
                    coInputMap.map.setZoom(16);
                },1000);
                coInputMap.hideLoader();

        		formInMap.NE_lat = $(this).data("lat");
        		formInMap.NE_lng = $(this).data("lng");
        	});
        }



        function formLocalitychangeCountryForNominatim(country){
            var codeCountry = {
				"FR" : ["RE", "GP", "GF", "MQ", "YT", "NC", "PM"]
			};
			$.each(codeCountry, function(key, countries){
				if(countries.indexOf(country) != -1)
			 		country = key;
			});
			return country ;
        }

        function autocompleteFormAddress(currentScopeType, scopeValue){
            $("#adressSelectHtml"+generateID+" #dropdown-newElement_"+currentScopeType+"-found").html("<li><a href='javascript:'><i class='fa fa-refresh fa-spin'></i></a></li>");
			$("#adressSelectHtml"+generateID+" #dropdown-newElement_"+currentScopeType+"-found").show();

			var paramsSearch =  {
				type: currentScopeType,
				scopeValue: scopeValue,
				geoShape: true,
				formInMap: true,
				countryCode : $('#adressSelectHtml'+generateID+' [name="newElement_country"]').val()
			} ;

			$.ajax({
				type: "POST",
				url: baseUrl+"/"+moduleId+"/city/autocompletemultiscope",
				data: paramsSearch,
				dataType: "json",
				success: function(data){
					html="";
					var inseeGeoSHapes = {};
					formInMap.saveCities = {};
					$.each(data.cities, function(key, value){
						var insee = value.insee;
						var country = value.country;
						if(notEmpty(value.save) &&  value.save == true){
							formInMap.saveCities[insee] = value;
						}
						if(notEmpty(value.geoShape))
							inseeGeoSHapes[insee] = value.geoShape.coordinates[0];

						if(currentScopeType == "city" || currentScopeType == "locality") {
							if(value.postalCodes.length > 0){
								$.each(value.postalCodes, function(keyCP, valueCP){
									var val = valueCP.name;
									var lbl = valueCP.postalCode ;
									var lat = valueCP.geo.latitude;
									var lng = valueCP.geo.longitude;

									var lblList = value.name ;

									if(valueCP.name != value.name)
										lblList +=  ", " + valueCP.name ;

									if(notEmpty(valueCP.postalCode))
										lblList += ", " + valueCP.postalCode ;
									if(notEmpty(value.level5Name))
										lblList += " ( " + value.level5Name + " ) ";
									else if(notEmpty(value.level4Name))
										lblList += " ( " + value.level4Name + " ) ";
									else if(notEmpty(value.level3Name))
										lblList += " ( " + value.level3Name + " ) ";
									else if(notEmpty(value.level2Name))
										lblList += " ( " + value.level2Name + " ) ";

									html += '<li><a href="javascript:;" data-type="'+currentScopeType+'" '+
													'data-locId="'+key+'" '+
													'data-level5="'+value.level5+'" data-level5name="'+value.level5Name+'"'+
													'data-level4="'+value.level4+'" data-level4name="'+value.level4Name+'"'+
													'data-level3="'+value.level3+'" data-level3name="'+value.level3Name+'"'+
													'data-level2="'+value.level2+'" data-level2name="'+value.level2Name+'"'+
													'data-level1="'+value.level1+'" data-level1name="'+value.level1Name+'"'+
													'data-country="'+country+'" '+
													'data-city="'+val+'" data-cp="'+lbl+'" '+
													'data-lat="'+lat+'" data-lng="'+lng+'" '+
													'data-insee="'+insee+'" class="item-city-found">'+lblList+'</a></li>';
								});
							}else{
								var val = value.name;
								var lat = value.geo.latitude;
								var lng = value.geo.longitude;
								var lblList = value.name ;
								if(notEmpty(value.level5Name))
									lblList += " ( " + value.level5Name + " ) ";
								else if(notEmpty(value.level4Name))
									lblList += " ( " + value.level4Name + " ) ";
								else if(notEmpty(value.level3Name))
									lblList += " ( " + value.level3Name + " ) ";
								else if(notEmpty(value.level2Name))
									lblList += " ( " + value.level2Name + " ) ";

								html += '<li><a href="javascript:;" data-type="'+currentScopeType+'" '+
													'data-locid="'+key+'" ';

								html +=	'data-level5="'+value.level5+'" data-level5name="'+value.level5Name+'"'+
										'data-level4="'+value.level4+'" data-level4name="'+value.level4Name+'"'+
										'data-level3="'+value.level3+'" data-level3name="'+value.level3Name+'"'+
										'data-level2="'+value.level2+'" data-level2name="'+value.level2Name+'"'+
										'data-level1="'+value.level1+'" data-level1name="'+value.level1Name+'"';
								html += 'data-country="'+country+'" '+
										'data-city="'+val+'" data-lat="'+lat+'" '+
										'data-lng="'+lng+'" data-insee="'+insee+'" '+
										'class="item-city-found-uncomplete">'+lblList+'</a></li>';
							}
						};
					});

					if(html == "") html = "<i class='fa fa-ban'></i> "+trad.noresult;
					$("#adressSelectHtml"+generateID+" #dropdown-newElement_"+currentScopeType+"-found").html(html);
					$("#adressSelectHtml"+generateID+" #dropdown-newElement_"+currentScopeType+"-found").show();

					$("#adressSelectHtml"+generateID+" .item-city-found, .item-cp-found").click(function(){
						formInMap.NE_insee = $(this).data("insee");
                        formInMap.NE_lat = $(this).data("lat");
                        formInMap.NE_lng = $(this).data("lng");
                        formInMap.NE_city = $(this).data("city");
                        formInMap.NE_country = $(this).data("country");
                        formInMap.NE_level5 = (notEmpty($(this).data("level5")) ? $(this).data("level5") : null) ;
                        formInMap.NE_level5Name = (notEmpty($(this).data("level5name")) ? $(this).data("level5name") : null) ;
                        formInMap.NE_level4 = (notEmpty($(this).data("level4")) ? $(this).data("level4") : null) ;
                        formInMap.NE_level4Name = (notEmpty($(this).data("level4name")) ? $(this).data("level4name") : null) ;
                        formInMap.NE_level3 = (notEmpty($(this).data("level3")) ? $(this).data("level3") : null) ;
                        formInMap.NE_level3Name = (notEmpty($(this).data("level3name")) ? $(this).data("level3name") : null) ;
                        formInMap.NE_level2 = (notEmpty($(this).data("level2")) ? $(this).data("level2") : null) ;
                        formInMap.NE_level2Name = (notEmpty($(this).data("level2name")) ? $(this).data("level2name") : null);
                        formInMap.NE_level1 = (notEmpty($(this).data("level1")) ? $(this).data("level1") : null) ;
                        formInMap.NE_level1Name = (notEmpty($(this).data("level1name")) ? $(this).data("level1name") : null) ;
                        formInMap.NE_localityId = $(this).data("locid");
                        formInMap.NE_cp = $(this).data("cp");

                        $("#adressSelectHtml"+generateID+" #dropdown-newElement_cp-found, #dropdown-newElement_city-found, #dropdown-newElement_streetAddress-found, #dropdown-newElement_locality-found").hide();
                        $("#adressSelectHtml"+generateID+" #divStreetAddress").removeClass("hidden");
			            $("#adressSelectHtml"+generateID+" [name='newElement_city']").val(formInMap.NE_city);
                        var paramsMarker = {
                            elt : {
                                id : 0,
                                type : "2D",
                                geo : {
                                    latitude :  formInMap.NE_lat,
                                    longitude : formInMap.NE_lng,
                                }
                            },
                            opt : {
                                draggable: true
                            },
                            center : true
                        };
                        coInputMap.clearMap();
                        coInputMap.addMarker(paramsMarker);
                        coInputMap.map.setZoom(14);
                        coInputMap.hideLoader();
					});

					$("#adressSelectHtml"+generateID+" .item-city-found-uncomplete").click(function(){
                        formInMap.NE_insee = $(this).data("insee");
                        formInMap.NE_lat = $(this).data("lat");
                        formInMap.NE_lng = $(this).data("lng");
                        formInMap.NE_city = $(this).data("city");
                        formInMap.NE_country = $(this).data("country");
                        formInMap.NE_level5 = (notEmpty($(this).data("level5")) ? $(this).data("level5") : null) ;
                        formInMap.NE_level5Name = (notEmpty($(this).data("level5name")) ? $(this).data("level5name") : null) ;
                        formInMap.NE_level4 = (notEmpty($(this).data("level4")) ? $(this).data("level4") : null) ;
                        formInMap.NE_level4Name = (notEmpty($(this).data("level4name")) ? $(this).data("level4name") : null) ;
                        formInMap.NE_level3 = (notEmpty($(this).data("level3")) ? $(this).data("level3") : null) ;
                        formInMap.NE_level3Name = (notEmpty($(this).data("level3name")) ? $(this).data("level3name") : null) ;
                        formInMap.NE_level2 = (notEmpty($(this).data("level2")) ? $(this).data("level2") : null) ;
                        formInMap.NE_level2Name = (notEmpty($(this).data("level2name")) ? $(this).data("level2name") : null);
                        formInMap.NE_level1 = (notEmpty($(this).data("level1")) ? $(this).data("level1") : null) ;
                        formInMap.NE_level1Name = (notEmpty($(this).data("level1name")) ? $(this).data("level1name") : null) ;
                        formInMap.NE_localityId = $(this).data("locid");

						if ( notEmpty( formInMap.saveCities) &&
                             notEmpty( formInMap.saveCities[formInMap.NE_insee]) &&
                             notEmpty( formInMap.saveCities[formInMap.NE_insee].betweenCP)
						) {
				            formInMap.NE_betweenCP = formInMap.saveCities[formInMap.NE_insee].betweenCP ;
                        }
                        $("#adressSelectHtml"+generateID+" #dropdown-newElement_cp-found, #dropdown-newElement_city-found, #dropdown-newElement_streetAddress-found, #dropdown-newElement_locality-found").hide();
                        $("#adressSelectHtml"+generateID+" #divStreetAddress").removeClass("hidden");
			            $("#adressSelectHtml"+generateID+" [name='newElement_city']").val(formInMap.NE_city);
                        var paramsMarker = {
                            elt : {
                                id : 0,
                                type : "2D",
                                geo : {
                                    latitude :  $(this).data("lat"),
                                    longitude : $(this).data("lng")
                                }
                            },
                            opt : {
                                draggable: true
                            },
                            center : true
                        };
                        coInputMap.clearMap();
                        coInputMap.addMarker(paramsMarker);
                        setTimeout(function () {
                            coInputMap.map.setZoom(12);
                        },1000);
                        coInputMap.hideLoader();
					});
				},
				error: function(error){
					$("#adressSelectHtml"+generateID+" #dropdown-newElement_"+currentScopeType+"-found").html("error");
				}
			});
        }

        function showWarningGeo(bool){
			if(bool == true){
				$("#adressSelectHtml"+generateID+" #alertGeo").removeClass("hidden");
				$("#adressSelectHtml"+generateID+" #newElement_btnSearchAddress").removeClass("btn-default");
				$("#adressSelectHtml"+generateID+" #newElement_btnSearchAddress").addClass("btn-warning");
			}else{
				$("#adressSelectHtml"+generateID+" #alertGeo").addClass("hidden");
				$("#adressSelectHtml"+generateID+" #newElement_btnSearchAddress").removeClass("btn-warning");
				$("#adressSelectHtml"+generateID+" #newElement_btnSearchAddress").addClass("btn-default");
			}
		}
        
        $html.find("#newAddress"+generateID).click(function(e){
            e.preventDefault();
            initVarNE();
            showPanel();
        })

        if ( coInputFormLocation.length > 0 ) {
            var newView = formLocalityInitViewElement();
            $html.find(".coInput-form-list-formLocality-" + generateID).append(newView);
            $html.find(".coInput-form-list-formLocality-"+generateID+" .btn-remove-element").off("click").on("click", function(e) {
                e.preventDefault();
                const index = $(this).data("index");
                removeFromForm(index);
            });

            $html.find(".coInput-form-list-formLocality-"+generateID+" .btn-adress-principal").off("click").on("click", function(e) {
                e.preventDefault();
                const index = $(this).data("index");
                if ( typeof coInputFormLocation[index] !== undefined || coInputFormLocation[index] === false ) {
                    coInputFormLocation.forEach((location, i) => {
                        if (i !== index) {
                            delete location.center;
                        }
                        // Set 'center' for the selected index
                        coInputFormLocation[index].center = true;
    
                        var newView = formLocalityInitViewElement();
                        $html.find(".coInput-form-list-formLocality-" + generateID).append(newView);
                        $html.find(".coInput-form-list-formLocality-"+generateID+" .btn-remove-element").off("click").on("click", function(e) {
                            e.preventDefault();
                            const index = $(this).data("index");
                            removeFromForm(index);
                        });

                        $html.find(".coInput-form-list-formLocality-"+generateID+" .btn-adress-principal").off("click").on("click", function(e) {
                            e.preventDefault();
                            const index = $(this).data("index");
                            if ( typeof coInputFormLocation[index] !== undefined || coInputFormLocation[index] === false ) {
                                coInputFormLocation.forEach((location, i) => {
                                    if (i !== index) {
                                        delete location.center;
                                    }
                                    // Set 'center' for the selected index
                                    coInputFormLocation[index].center = true;
                
                                    var newView = formLocalityInitViewElement();
                                    $html.find(".coInput-form-list-formLocality-" + generateID).append(newView);
                                    $html.find(".coInput-form-list-formLocality-"+generateID+" .btn-remove-element").off("click").on("click", function(e) {
                                        e.preventDefault();
                                        const index = $(this).data("index");
                                        removeFromForm(index);
                                    });
                            
                                });
                                // Set 'center' for the selected index
                                coInputFormLocation[index].center = true;
                            }
                            triggerCustomEvents("inputBlur", name, coInputFormLocation, payload, $html);
                        });
                
                    });
                    // Set 'center' for the selected index
                    coInputFormLocation[index].center = true;
                }
                triggerCustomEvents("inputBlur", name, coInputFormLocation, payload, $html);
            });
        }
        return $html;
    }


    //SELECT ICON 
    coreTypes.inputIcon = function(context, options){
        var name = options.name || "inputIcon", 
        defaultIcoName = options.defaultValue || tradCms.noIcon,
        payload = options.payload,
        classInput = (typeof options.class != "undefined") ? options.class : "",
        haveIcon = false,
        defaultIcon = "",
        generateID = generateUUID(),
        icons = "";
        $.each( fontAwesome , function(k,val) { 
            icons += "<a class='this-icon btn' style='color: #333; float: left;width: 35px;height: 35px;margin: 0 12px 12px 0;text-align: center;cursor: pointer; border-radius: 3px;font-size: 14px;box-shadow: 0 0 0 1px #ddd;' data-icon='fa-"+k+"'><i class='fa fa-"+k+"'></i></a>"
        });

        if (Object.keys(fontAwesome).includes(defaultIcoName.replace("fa-", "").replace("fa", "").trim())) {
            var iconToShow = (defaultIcoName.includes("fa fa-")) ? defaultIcoName : "fa fa-"+defaultIcoName;
            defaultIcon = "<i class='icon-selected "+iconToShow+"' data-icon=''></i>";
            haveIcon = true;
        }

        var $html = $(`
            <div class="form-group input-group-sm ${classInput}" style="margin-bottom: 50px;">
                ${ options.label ? `<label>${options.label}</label>`:"" }
                <div id='edit-icon' class="input-group" style='padding: 0px;' class='col-xs-12 padding-bottom-10'>
                    <div class='sp-display-icon text-center'>
                        <p class="text-icon text-icon_${generateID}">${defaultIcoName}</p>
                        <div class="icon-content icon-content_${generateID} ${(haveIcon) ? "have-icon" : ""}">
                            <span>${defaultIcon}</span>
                            <button type="button" class='erase-icon-selected erase-icon-selected_${generateID} btn btn-danger' ${(!haveIcon) ? "style='display: none;'" : ""}><i class='fa fa-times'></i></button>
                        </div>
                    </div>
                    <div class='no-padding' style='height:150px;'>
                        <input type='text' style='border-radius: inherit;' class='form-control' id='iconSearcher' placeholder='${tradCms.tapToFilter}'>
                        <div id='sp-icon-list' style='position: relative;clear: both;float: none;padding: 12px 0 0 12px;background: #fff;margin: 0;overflow: hidden;overflow-y: auto;max-height: 154px;display: flex;flex-wrap: wrap;'>
                            ${icons}
                        </div>
                    </div>
                </div>
            </div>;
        `)

        if ((typeof options.icon != "undefined") && options.icon != ""){
            $html.find("label").prepend(`
                <i class="fa fa-${options.icon}"></i> 
            `)
        }

        $html.find(".this-icon").click(function(){
            let dataIcon = $(this).data("icon");
            if (dataIcon === "fa-no-icon") {
                dataIcon = "";
                $(`.text-icon_${generateID}`).text(tradCms.noIcon);
                $(`.icon-content_${generateID}`).removeClass("have-icon");
                $(`.icon-content_${generateID} span`).html("");
                $(`.erase-icon-selected_${generateID}`).hide();
            } else {
                $(`.text-icon_${generateID}`).text(`fa ${dataIcon}`);
                $(`.icon-content_${generateID}`).addClass("have-icon");
                $(`.icon-content_${generateID} span`).html("<i class='icon-selected fa "+dataIcon+"' data-icon='"+dataIcon+"'></i>");
                $(`.erase-icon-selected_${generateID}`).show();
            }
            let value = (dataIcon != "") ? dataIcon.split("fa-")[1] : "";
            triggerCustomEvents("valueChange", name, value, payload, $html);
            triggerCustomEvents("inputBlur", name, value, payload, $html);
        })

        $html.find(`.erase-icon-selected_${generateID}`).click(function() {
            bootbox.confirm({message: trad.areyousurtodeleteIcon,
                buttons: {
                    confirm: {
                        label: trad.yes,
                        className: 'btn-success'
                    },
                    cancel: {
                        label: trad.no,
                        className: 'btn-danger'
                    }
            }, 
            callback: function(response) {
                if (response) {
                    $(`.text-icon_${generateID}`).text(tradCms.noIcon);
                    $(`.icon-content_${generateID}`).removeClass("have-icon");
                    $(`.icon-content_${generateID} span`).html("");
                    $(`.erase-icon-selected_${generateID}`).hide();
                    triggerCustomEvents("valueChange", name, "", payload, $html);
                    triggerCustomEvents("inputBlur", name, "", payload, $html);
                }
            }});
        });

        $html.find("#iconSearcher").on("keyup",function(){
            let filter, i;
            filter = $(this).val().toUpperCase();

			var newIconHtml = "";
			var newIconList = Object.keys(fontAwesome).map(function(key, value) {
				return "fa-"+key;
			});
            
			$.each( newIconList , function(k,val) { 
			    newIconHtml += "<a class='this-icon btn' style='float:left;width: 35px;height: 35px;margin: 0 12px 12px 0;text-align: center;cursor: pointer; border-radius: 3px;font-size: 14px;box-shadow: 0 0 0 1px #ddd; color: #333;' data-icon='"+val+"'><i class='fa "+val+"'></i></a>"
			});

            for (i = 0; i < Object.keys(newIconList).length; i++) {
                paragraph = newIconList[i]
                paragraph = paragraph.replace("fa-","")         
                paragraph = paragraph.split('-').join(' ');
                paragraph = paragraph.toUpperCase();
                if (paragraph.includes(filter))
                    $(".this-icon ."+newIconList[i]).parent().show();
                else
                    $(".this-icon ."+newIconList[i]).parent().hide();
            }
        })

        $html.val = function(){
            return {
                name:name, 
                value:$html.find("input").val()
            }
        }

        return $html;
    }

    coreTypes.inputFileFont = function(context, options){
        var name = options.name || "textarea",
            payload = options.payload,
            file = null,
            canDeleteFont = (typeof options.canDeleteFont != "undefined") ? options.canDeleteFont : true,
            accept = options.accept || ["*"];

        var $html = $(`
            <div class="form-group">
                ${ options.label ? `<label>${options.label}</label>`:"" }
                <div class="input-group">
                    <input type="text" class="form-control" name="${options.name}" placeholder="${options.placeholder || ""}" readonly>
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="button" id="add-font-upload"><i class="fa fa-folder-open" aria-hidden="true"></i></button>
                        ${(canDeleteFont) ? `<button class="btn btn-danger" type="button" id="delete-font-upload"><i class="fa fa-trash" aria-hidden="true"></i></button>`:""}
                    </span>
                </div>
            </div>
        `)

        $html.find("#add-font-upload").click(function(){
            var $input = $(`<input type="file" accept="${accept.join(", ")}"/>`)
            $input.change(function(){
                file = $input[0].files[0];
                $html.find("input").val(file.name);
                triggerCustomEvents("valueChange", name, file, payload, $html);
                triggerCustomEvents("inputBlur", name, file, payload, $html);
            })

            $input.trigger("click")
        })

        $html.val = function(){
            return {
                name:name,
                value:file
            }
        }

        return $html;
    }

    //INPUT DYNAMIC
    coreTypes.inputDynamic = function(context, options){
        var name = options.name || "inputDynamic",
            payload = options.payload,
            dynamicValues = options.options || [];

        //create an id for checkbox to switch dynamic content
        var checkboxId = `cb-input-dynamic-${generateUUID()}`;

        //container of input dynamic
        var $html = $(`
            <div class="input-dynamic-container">
                <div class="form-group">
                    ${ options.label ? `<label>${options.label}</label>`:"" }
                    <div class="default-input"></div>
                    <div class="dynamic-input"></div>
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" value="" id="${checkboxId}">
                        <label class="form-check-label" for="${checkboxId}">${ tradCms.dynamicInput }</label>
                    </div>
                </div>
            </div>
        `)

        //get input based on enter option
        var inputParams = getInputParams(context, options.input);
        if(inputParams.options && inputParams.options.label)
            delete inputParams.options.label
        var $defaulInput = getInput(context, inputParams)
        //create input select for the dynamic values
        var $dynamicInput = $(`<select class="form-control"><option></option></select>`)
        //init dynamic input options
        dynamicValues.forEach(function(option){
            $dynamicInput.append(`<option value="${option.value}">${option.label}</option>`)
        })

        //function to propagate change
        var triggerChange = function(isDynamic=false){
            var value = isDynamic ? $dynamicInput.val():$defaulInput.val().value;
            triggerCustomEvents("valueChange", name, value, payload, $html);
            triggerCustomEvents("inputBlur", name, value, payload, $html);
        }
        
        //handle defaulInput change
        $defaulInput.on("valueChange", function(e){
            e.stopPropagation()
            triggerChange(false)
        })
        //handle dynamicInput change
        $dynamicInput.change(function(){
            triggerChange(true)
        })
        //handle change checkbox
        $html.find(`#${checkboxId}`).change(function(){
            var isChecked = $(this).is(':checked');
            triggerChange(isChecked)
            $dynamicInput.css({ display: isChecked ? "block":"none" })
            $defaulInput.css({ display: isChecked ? "none":"block" })
        })

        //append elements
        $html.find(".default-input").html($defaulInput)
        $html.find(".dynamic-input").append($dynamicInput)

        $dynamicInput.css({ display:"none" })

        return $html;
    }

    //INPUT SECTION
    coreTypes.section = function(context, options){
        var payload = options.payload || {},
            pathExist = false,
            name = options.name || "section",
            label = options.label || "section",
            collapseId = `collapse-${generateUUID()}`,
            classInput = options.class || "",
            showInDefault = options.showInDefault || false,
            defaultValue = options.defaultValue || context.settings.defaultValues[name];
        if (typeof options.category != "undefined" && options.category == "subSection")  {
            var $html = $(`
                <div class="input-section section-${name} ${classInput}" >
                    <label>${label}</label>
                    <div id="${collapseId}" class="subsection-content" style="padding-top:10px; border-top: 1px dotted black;"></div>
                </div>
            `)
        } else {
            var $html = $(`
                <div class="input-section section-${name} ${classInput}" style="/*padding-left: 10px;*/">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <p class="panel-title">
                                <a data-toggle="collapse" href="#${collapseId}"><span class="caret"></span> ${label || "Section"}</a>
                            </p>
                        </div>
                        <div id="${collapseId}" class="panel-collapse collapse ${ (showInDefault) ? "in" : "" }" style="padding-left:10px; border-left: 1px dotted black;">
                        </div>
                    </div>
                </div>
            `)
        }

        var $form = $html.find(`#${collapseId}`);
        var inputsParams = options.inputs.map(function(input){

            var params = getInputParams(context, input),
                childName = params.options?.name || params.type;

            if(defaultValue && defaultValue[childName] && typeof params.options.defaultValue == "undefined")
                params.options = $.extend({}, params.options, { defaultValue:defaultValue[childName] })

            //var sectionPath = payload.sectionPath ? `${payload.sectionPath}.${name}`:name

            var sectionPath = [payload.path, payload.sectionPath, name]
                .filter(part => typeof part !== "undefined")
                .join(".");
            if (typeof payload.path != "undefined") {
                pathExist = true;
            }

            params.options.payload = $.extend(
                params.options.payload,
                {
                    sectionName:name,
                    sectionPath:sectionPath
                }
            )
            if (pathExist)
                params.options.payload["pathExist"] = pathExist;

            return params;
        })
        renderInputs(context, $form, inputsParams)
        
        $form.on("valueChange", function(e, params){
            e.stopPropagation()
            $html.trigger("valueChange", $.extend(true, { payload:payload }, params))
        })

        $form.on("inputBlur", function(e, params){
            e.stopPropagation()
            $html.trigger("inputBlur", $.extend(true, { payload:payload }, params))
        })

        return $html;
    }

    coreTypes.dateTimePicker = function(context, options) {
        var name = options.name || "dateTimePicker",
            configDateTime = options.configForDateTime || {},
            payload = options.payload,
            placeholder = options.placeholder,
            defaultValue = "",
            label = options.label,
            inputValue = "",
            classInput = options.class || "";
        
        if(options.defaultValue){
            defaultValue = options.defaultValue;
        } else if (context.settings && context.settings.defaultValues) {
            var values = [];
            if (typeof options.payload != "undefined" && typeof options.payload.sectionPath != "undefined") {
                values = jsonHelper.getValueByPath(context.settings.defaultValues, options.payload.sectionPath);
            } else if ( typeof context.settings.defaultValues.css != "undefined"  && typeof cssHelpers.json[name] != "undefined" ){
                values = context.settings.defaultValues.css
            }
            else {
                values = context.settings.defaultValues;
            }
            defaultValue = typeof values != "undefined" && typeof values[name] != "undefined" ? values[name] : "";
        }
        
        var $html = $(`
            <div class="form-group input-group-sm ${classInput}">
                ${ label ? `<label class="label-for-input-select">${label}</label>`:"" }
                <input type="text" class="form-control" name="${name}"${placeholder ? ` placeholder="${placeholder}"`: ""}${typeof defaultValue != "undefined" ? ` value="${defaultValue}"`: ""}>
            </div>
        `)

        if ((typeof options.icon != "undefined") && options.icon != ""){
            $html.find("label").prepend(`
                <i class="fa fa-${options.icon}"></i> 
            `)
        }

        inputValue = $html.find("input").val();

        let previousValue = '';
        $html.find("input").on('focus', function() {
            previousValue = $(this).val();
        }).on('blur', function() {
            let currentValue = $(this).val();
            if (previousValue !== currentValue) {
              triggerCustomEvents("inputBlur", name, $(this).val(), payload, $html);
            }
        });

        $html.val = function(){
            return {
                name: name,
                value: $html.find("input").val()
            }
        }

        let formatValue = (notEmpty(configDateTime) && typeof configDateTime["format"] != "undefined") ? configDateTime["format"] : 'd/m/Y H:i';

        function formatDate(date, format) {
            const map = {
                'd': date.getDate().toString().padStart(2, '0'),
                'm': (date.getMonth() + 1).toString().padStart(2, '0'),
                'Y': date.getFullYear(),
                'H': date.getHours().toString().padStart(2, '0'),
                'i': date.getMinutes().toString().padStart(2, '0'), 
                's': date.getSeconds().toString().padStart(2, '0')
            };
            return format.replace(/d|m|Y|H|i|s/g, matched => map[matched]);
        }

        $html.find("input").datetimepicker((notEmpty(configDateTime)) ? configDateTime : {
            autoclose: true,
            lang: 'fr',
            format: formatValue,
            onSelectDate: function(value) {
                let date = new Date(value);
                let formattedDate = formatDate(date, formatValue);
                triggerCustomEvents("valueChange", name, formattedDate, payload, $html);
            }
        });

        return $html;
    }

    //INPUT MATOMO
    coreTypes.matomoInput = function(context, options) {
        var name = options.name || "activateMatomo",
            label = options.label,
            defaultValues = options.defaultValues || "";
            payload = options.payload;
        
        var $html = $(`
            <div class="form-group co-input-activate-matomo">
                <div class="activeMatomoInput">
                    <input id="${name}" type="checkbox" class="activeMatomo" name="${name}" ${ typeof defaultValues.active != "undefined" && defaultValues.active == true ? "checked" : ""}>
                    <label for="${name}">${ typeof label != "undefined" ? label : name }</label>
                </div>
                <div class="input-if-matomo-activated">
                    <div class="active-serveur-content" style="display: none;">
                        <div class="switch-serveur-content">
                            <input id="COServer" type="radio" class="activeServer" name="server" value="COServer" ${ (typeof defaultValues.server != "undefined" && typeof defaultValues.server.externe != "undefined" && defaultValues.server.externe == false) || typeof defaultValues.server == "undefined" ? "checked" : ""}>
                            <label for="COServer">Utiliser le serveur matomo de CO</label>
                            <input id="OwnServer" type="radio" class="activeServer" name="server" value="OwnServer" ${ typeof defaultValues.server != "undefined" && typeof defaultValues.server.externe != "undefined" && defaultValues.server.externe == true ? "checked" : ""}>
                            <label for="OwnServer">J'ai mon propre serveur</label>
                        </div>
                        <div class="input-serveur-property" style="display: none;">
                            <input type="text" name="serverAdresse" class="form-control" placeholder="Adresse du serveur" value="${ typeof defaultValues.server != "undefined" && typeof defaultValues.server.adresse != "undefined" ? defaultValues.server.adresse  : ""}">
                            <input type="text" name="token" class="form-control" placeholder="Token d'access" value="${ typeof defaultValues.server != "undefined" && typeof defaultValues.server.token != "undefined" ? defaultValues.server.token  : ""}">
                        </div>
                    </div>
                </div>
            </div>
        `);

        var getValues = function() {
            let values = {};
            if ($html.find(".activeMatomo").is(":checked")) {
                values["active"] = true;
                let server = $html.find("input[name='server']:checked").val();
                if (server == "OwnServer") {
                    values["server"] = {};
                    values["server"]["externe"] = true;
                    if ($html.find("input[name='serverAdresse']").val() != "")
                        values["server"]["adresse"] = $html.find("input[name='serverAdresse']").val();
                    if ($html.find("input[name='token']").val() != "")
                        values["server"]["token"] = $html.find("input[name='token']").val();
                } else {
                    values["server"] = {};
                    values["server"]["externe"] = false;
                }
            } else {
                values["active"] = false;
            }
            return values;
        }

        var triggerChange = function() {
            $html.trigger("valueChange", {
                name: name,
                value: getValues(),
                payload: payload
            });
        }

        if ($html.find(".activeMatomo").is(":checked")) {
            $html.find(".active-serveur-content").show();
        }

        if ($html.find("input[name='server']").is(":checked") && $html.find("input[name='server']:checked").val() == "OwnServer") {
            $html.find(".input-serveur-property").show();
        }

        $html.find(".activeMatomo").change(function() {
            if ($(this).is(":checked")) {
                $html.find(".active-serveur-content").show();
                triggerChange();
            } else {
                $html.find(".active-serveur-content").hide();
                triggerChange();
            }
        })

        $html.find("input[name='server']").change(function() {
            if ($(this).is(":checked")) {
                if ($(this).val() == "OwnServer") {
                    $html.find(".input-serveur-property").show();
                    triggerChange();
                } else {
                    $html.find(".input-serveur-property").hide();
                    triggerChange();
                }
            } else {
                $html.find(".input-serveur-property").hide();
                triggerChange();
            }
        })

        $html.find("input[name='serverAdresse']").change(function() {
            triggerChange();
        })

        $html.find("input[name='token']").change(function() {
            triggerChange();
        })

        $html.val = function(){
            return {
                name: name,
                value: getValues()
            }
        }

        return $html;
    }

}())