var codateObj = {
	container: "#calendar",
	calendarContainer: "#full_calendar",
	params: {
		startDate: null,
		endDate: null
	},
	options: {
		initDate: new Date(),
		header: {
			left: 'prev,next',
			center: 'title',
			right: 'today, agendaWeek, month'
		},
		footer: {
			left: '',
			center: '',
			right: ''
		},
		fixedWeekCount: false,
		// height: 'auto',
		lang: mainLanguage,
		editable: false,
		eventBackgroundColor: '#FFA200',
		textColor: '#fff',
		defaultView: 'month',
		initialView: 'listWeek',
		eventLimit: 0,
		timezone: 'local',
		personalAgenda: false,
		selectable: true,
		unselectAuto: true,
		mixColor: [
			"#78d117",
			"#fb3300",
			"#9fbd38",
			"#4975f1",
			"#42908d",
			"#60345d",
			"#b00068",
			"#e45e80",
			"#ff8000",
			"#feb800",
			"#fc8fa9",
			"#88c49e",
			"#4fed70",
			"#97e9bd",
			"#66d71b"
		]
	},
	init: function (pInit = null) {
		mylog.log("codateObj init", pInit);
		//Init variable
		var copyCodateCalendar = jQuery.extend(true, {}, codateObj);
		copyCodateCalendar.initVar(pInit);
		return copyCodateCalendar;

	},

	initVar: function (pInit) {
		if (pInit != null && typeof pInit.container != "undefined")
			this.container = pInit.container;
		if (pInit != null && typeof pInit.calendarContainer != "undefined")
			this.calendarContainer = pInit.calendarContainer;
		if (typeof pInit.options != "undefined")
			$.extend(this.options, pInit.options);
		if (typeof pInit.searchParams != "undefined")
			$.extend(this.params, pInit.searchParams);
		this.initDefaults(pInit);
		this.initViews(pInit);
		// this.initEvents(pInit);

	},
	initDefaults: function (pInit) {
		var fIObj = this;

		fIObj.codate = {
			selectedDates: [],
			userAnswers: {},
			answers: {},
			allEvents: [],
			results: {}
		}

		//add custom button for codate inside
		fIObj.codate.insIsAdmin = '';
		fIObj.codate.contextId = null;
		fIObj.codate.contextData = {};
		var queryParams = fIObj.commun.getQuery();
		if (queryParams['standalone']) {
			fIObj.codate.contextId = queryParams['contextid'];
			ajaxPost(
				null,
				baseUrl + "/co2/agenda/getcontext/contextId/" + queryParams['contextid'] + "/contextType/" + queryParams['contexttype'],
				{},
				function (res) {
					if (typeof res == "object")
						fIObj.codate.contextData = res;
				}, null, null, { async: false }
			);
		}
		fIObj.codateIsAdmin = false;
		if (userId && typeof contextId != "undefined") {
			if (userConnected && userConnected.links && userConnected.links.memberOf && userConnected.links.memberOf[contextId] && userConnected.links.memberOf[contextId]['isAdmin']) {
				if (userConnected.links.memberOf[contextId]['isAdmin'] == true) {
					fIObj.codate.insIsAdmin = 'inviter';
					fIObj.codateIsAdmin = true;
				}
			}
			if (userConnected && userConnected.links && userConnected.links.projects && userConnected.links.projects[contextId] && userConnected.links.projects[contextId]['isAdmin']) {
				if (userConnected.links.projects[contextId]['isAdmin'] == true) {
					fIObj.codate.insIsAdmin = 'inviter';
					fIObj.codateIsAdmin = true;
				}
			}
		} else if (userId && notNull(fIObj.codate.contextId)) {
			if (userConnected && userConnected.links && userConnected.links.memberOf && userConnected.links.memberOf[fIObj.codate.contextId] && userConnected.links.memberOf[fIObj.codate.contextId]['isAdmin']) {
				if (userConnected.links.memberOf[fIObj.codate.contextId]['isAdmin'] == true) {
					fIObj.codate.insIsAdmin = 'inviter';
					fIObj.codateIsAdmin = true;
				}
			}
			if (userConnected && userConnected.links && userConnected.links.projects && userConnected.links.projects[fIObj.codate.contextId] && userConnected.links.projects[fIObj.codate.contextId]['isAdmin']) {
				if (userConnected.links.projects[fIObj.codate.contextId]['isAdmin'] == true) {
					fIObj.codate.insIsAdmin = 'inviter';
					fIObj.codateIsAdmin = true;
				}
			}
		}
		// fIObj.options.footer.right = fIObj.options.footer.right != '' && fIObj.options.footer.right ? fIObj.options.footer.right + ','+fIObj.codate.insIsAdmin : fIObj.codate.insIsAdmin;
		fIObj.options.customButtons = {
			inviter: {
				text: 'Inviter la communauté',
				click: function () {
					ajaxPost(
						null,
						baseUrl + "/co2/app/invitecommunity",
						{
							"contextId": notNull(contextData) ? contextData.id : fIObj.codate.contextData.id,
							"contextType": notNull(contextData) ? contextData.type : fIObj.codate.contextData.type,
							"objectId": "639c0e2cd2758dee020c2d3b",
							"objectType": "forms"
						},
						function (res) {

						},
					);
				}
			}
		}

		if (pInit != null && typeof pInit.searchObj != "undefined")
			fIObj.searchObj = pInit.searchObj;
	},
	initViews: function (pInit) {
		var fIObj = this;
		mylog.log("codateobj initviews", fIObj);
		fIObj.options.eventClick = function (calEvent, jsEvent, view) {
			if (calEvent.codateEvent) {
				$('a').removeClass('event-clicked');
				$(`a[data-elemid='${calEvent.id}-${calEvent.start.format('YYYY-MM-DD')}']`).addClass('event-clicked');
				const times = JSON.parse(calEvent.codateEvent.times.replace(/'/g, '\"'))[new Date(calEvent.start.format()).toLocaleString("fr", { day: 'numeric', month: 'numeric', year: 'numeric' })] ? JSON.parse(calEvent.codateEvent.times.replace(/'/g, '\"'))[new Date(calEvent.start.format()).toLocaleString("fr", { day: 'numeric', month: 'numeric', year: 'numeric' })] : {};
				fIObj.commun.buildTimeList(fIObj, calEvent.id, calEvent.codateEvent.formId, calEvent.codateEvent.subForms, new Date(calEvent.start.format()), times, calEvent.title)
				fIObj.commun.bindTimeEvent(fIObj);
			} else if (calEvent.simple_event) {
				$('#event-color-nocostum').css('background-color', calEvent['backgroundColor']);
				$('#event-name-nocostum').text(calEvent['title']);
				var start = calEvent['start'];
				var end = calEvent['end'];
				if (start.format('LL') === end.format('LL')) $('#event-date-nocostum').text(start.format('LL') + ' ' + start.format('HH:mm') + ' - ' + end.format('HH:mm'));
				else $('#event-date-nocostum').text(start.format('LLL') + ' - ' + end.format('LLL'));

				$('#event-group-nocostum').text(calEvent['group']);

				$('.rin-modal').on('rinmodal.shown', function () {
					var modal = this;
					if (calEvent['parent'] === 'communecter_agenda') {
						if (calEvent.is_admin) {
							var suppression = $(this).find('.delete');
							suppression.data('id', calEvent['id']);
							suppression.show();
							suppression.off('click').on('click', function () {
								$.confirm({
									title: 'Supprimer',
									content: 'Procéder à la suppression de l\'événement',
									buttons: {
										confirm: {
											text: 'Confirmer',
											action: function () {
												var url = baseUrl + '/' + moduleId + '/element/delete/id/' + suppression.data('id') + '/type/events';
												var request_params = { reasons: 'none' };
												ajaxPost(null, url, request_params, function (__data) {
													if (__data.result) {
														toastr.success(__data.msg);
														$('#calendar_container').trigger('calendar.reload');
														$(modal).rinModal('close');
													} else toastr.error(__data.msg);
												});
											}
										},
										cancel: {
											text: 'Annuler'
										}
									}
								})
							});
						} else {
							$(this).find('.delete').hide();
						}
					} else {
						$(this).find('.delete').hide();
					}
					if (calEvent['url']) {
						$('#redirector').removeClass('lbh-preview-element');
						if (calEvent.url.match(/^#page\.type\.events\.id\.[0-9a-z]{24}$/)) {
							$('#redirector').addClass('lbh-preview-element');
							coInterface.bindLBHLinks();
						}
						$('#redirector').show();
						$('#redirector').attr('href', calEvent['url']);
					} else {
						$('#redirector').hide();
					}
				});

				$('.rin-modal').rinModal('show');
				jsEvent.preventDefault();
			}
		}
		fIObj.options.eventRender = function (event, element, view) {
			if (event.codateEvent) {
				if (event.codateEvent["times"]) element.attr("data-datetime", event.codateEvent["times"])
				maxStr = '';
				if (event.codateEvent['toCheck'] == true) {
					maxStr = `<i class="fa fa-star"></i>`
				}
				var checkedInput = '';
				if (view.name == 'month') {
					if (fIObj.codate.userAnswers[event.id] && fIObj.codate.userAnswers[event.id][event.start.format('DD/MM/YYYY')] && fIObj.codate.userAnswers[event.id][event.start.format('DD/MM/YYYY')].length > 0) {
						checkedInput = 'checked'
					}
					element.html(`
						<div class='fc-content' style="background-color: ${event.codateEvent['bg-color']}">
							<span class='fc-title'>${event.title}</span> <br/>
							${maxStr} <span>${event.codateEvent.nbVote}</span>
						</div>
					`);
					if (checkedInput == 'checked') {
						element.css(
							{
								'cssText': `background-color: ${event.codateEvent["bg-color"]} !important;
											 border: 3px solid #00000054 !important`,
							}
						)
					} else {
						element.css(
							{
								'cssText': `background-color: ${event.codateEvent["bg-color"]} !important`,
							}
						)
					}
					element.attr('data-elemid', event.id + '-' + event.start.format('YYYY-MM-DD'))
				} else {
					const inputId = Date.now().toString(36) + Math.random().toString(36).substring(2)
					checkedInput = '';
					if (fIObj.codate.userAnswers[event.id]) {
						if (fIObj.codate.userAnswers[event.id][event.start.format('DD/MM/YYYY')]) {
							if (fIObj.codate.userAnswers[event.id][event.start.format('DD/MM/YYYY')].indexOf(event.start.format('H:mm')) > -1) {
								checkedInput = 'checked'
							}
						}
					}
					if (checkedInput == 'checked') {
						element.css(
							{
								'cssText': `background-color: ${event.codateEvent["bg-color"]} !important;
											 border: 3px solid black !important`,
							}
						)
					} else {
						element.css(
							{
								'cssText': `background-color: ${event.codateEvent["bg-color"]} !important`,
							}
						)
					}
					element.addClass("changeCheck");
					element.attr("data-for", inputId);
					element.html(`
						<ul class="ks-cboxtags d-flex flex-lg-column justify-content-center">
							<li data-for="${inputId}" class="changeCheck" style="font-size: 13px; cursor: pointer">
								<input type='checkbox' id='${inputId}' name='codateTime' value="${event.start.format('H:mm')}" data-formid="${event.codateEvent['formId']}" data-subform="${event.codateEvent['subForms']}" data-inputid="${event.id}" data-date="${event.start.format('DD/MM/YYYY')}" ${checkedInput}>
								${maxStr} <span>${event.codateEvent.nbVote}</span> <br/>
								<span data-for='${inputId}' class="changeCheck w-100">${event.start.format('HH:mm')}</span>
								<span data-for="${inputId}" class='changeCheck fc-title'>${event.title}</span>
							</li>
						</ul>
					`);

				}
			}
		}
		fIObj.options.dayClick = function (date, jsEvent, view) {
			if (!date.isBefore(moment().utcOffset(0).set({ hour: 0, minute: 0, second: 0, millisecond: 0 }))) {
				$(fIObj.calendarContainer + " .fc-day").removeClass("activeDay");
				$(this).addClass("activeDay");
				const selfCell = this;

				fIObj.params.starDate = moment(date.format()).valueOf();

				// fIObj.searchInAgenda(fIObj, date.format(), "day");

				if ($('#createCodate').is(':checked')) {
					if (fIObj.codate.selectedDates.length == 0) {
						// $(".fc-header-toolbar .fc-right").append(`
						// 	<button type="button" class="btn btn-primary fc-button btnCreateCodate">Configurer codate</button>
						// `);
						fIObj.codate.selectedDates = [fIObj.commun.toReadable(new Date(moment(date.format()).valueOf()))]
						$(selfCell).addClass('selectedDay');
					} else {
						if (fIObj.codate.selectedDates.indexOf(fIObj.commun.toReadable(new Date(moment(date.format()).valueOf()))) == -1) {
							$(selfCell).addClass('selectedDay');
							fIObj.codate.selectedDates.push(fIObj.commun.toReadable(new Date(moment(date.format()).valueOf())))
						} else {
							toastr.info('Date deselected');
							$(selfCell).removeClass('activeDay');
							$(selfCell).removeClass('selectedDay');
							fIObj.codate.selectedDates.splice(fIObj.codate.selectedDates.indexOf(fIObj.commun.toReadable(new Date(moment(date.format()).valueOf()))), 1);
							mylog.log("fiobj data", fIObj.codate)
						}
					}
				} else {
					$('.dropdown-menu.click').remove();
					if (fIObj.codateIsAdmin) {
						fIObj.codate.selectedDates.length = 0;
						// if(fIObj.codate.selectedDates.indexOf(fIObj.commun.toReadable(new Date(moment(date.format()).valueOf()))) > -1)
						// 	fIObj.codate.selectedDates.splice(fIObj.codate.selectedDates.indexOf(fIObj.commun.toReadable(new Date(moment(date.format()).valueOf()))), 1);
						fIObj.codate.selectedDates.push(fIObj.commun.toReadable(new Date(moment(date.format()).valueOf())));
						$('body').append(
							`<ul class='dropdown-menu click'>
								<li>
									<a role="menuitem" tabindex="-1" class="btnCreateCodate" href="javascript:;">Créer codate</a>
								</li>
							</ul>`
						);

						$(selfCell).hover(delay(function (e) {

							$('body').find('.dropdown-menu.click').css({
								'cssText': `
									display: block;
									position: absolute;
									left: ${e.pageX - (selfCell.width() / 2)}px;
									top: ${e.pageY}px;
								`
							});
							$('body').find('.dropdown-menu.click').first().stop(true, true).slideDown(150);
						}, 50), function (e) {
							if ($(selfCell)[0] != $(this)[0])
								$('body').find('.dropdown-menu.click').first().stop(true, true).slideUp(105)
						});
						$(selfCell).trigger('hover');
						$('body').on('click', function (e) {
							$('body').find('.dropdown-menu.click').first().stop(true, true).slideUp(105)
							$('.dropdown-menu.click').remove();
						});
					}

				}
			}
		}
		function create_co_event(start, end) {
			var date_format = "DD/MM/YYYY HH:mm";
			var default_value = {
				startDate: start.format(date_format),
				endDate: end.format(date_format),
			};

			dyFObj.openForm('event', null, default_value, null, {
				afterSave: function (data) {
					var user = {
						id: userConnected['_id']['$id'],
						collection: userConnected['collection'] ? userConnected['collection'] : 'citoyens'
					};
					var event = {
						id: data['map']['_id']['$id'],
						collection: data['map']['collection']
					};
					var aftersave = {
						parent: {},
						links: {
							attendees: {},
							events: {},
							subEvents: {},
						}
					};
					var set_types = [{
						path: 'links.attendees.' + user.id + '.isAdmin',
						type: 'boolean'
					}];
					aftersave.parent[contextData.id] = {
						type: contextData.type,
						name: contextData.name
					};
					aftersave.links.attendees[user.id] = {
						type: user.collection,
						isAdmin: true
					};
					if (event.collection === 'events') {
						aftersave.links.subEvents[data.id] = {
							type: 'events'
						}
						delete aftersave.links.events;
					} else {
						aftersave.links.events[data.id] = {
							type: 'events'
						}
						delete aftersave.links.subEvents;
					}

					const after_event = {
						id: event.id,
						collection: event.collection,
						path: 'allToRoot',
						value: aftersave,
						setType: set_types
					};
					const after_citizen = {
						id: user.id,
						collection: user.collection,
						path: `links.${event.collection}.${event.id}`,
						value: {
							type: event.collection,
							isAdmin: true
						},
						setType: [{
							path: 'isAdmin',
							type: 'boolean'
						}]
					};
					dataHelper.path2Value(after_event, function (response) {
						if (response['result']) {
							dataHelper.path2Value(after_citizen, function (response) {
								if (response['result']) {
									dyFObj.closeForm();
									location.reload();
								}
							});
						}
					});
				}
			}, {
				type: 'bootbox'
			});
		}
		function create_co_date(start, end) {
			let dateIteration = start['_d'];
			let iterationCount = 0;
			const endArr = end['_d'].toLocaleString('fr', { day: 'numeric', month: 'numeric', year: 'numeric' }).split('/');
			let endBoucle = new Date(`${endArr[2]}-${endArr[1]}-${endArr[0]}`);
			var deselectedDate = [];

			if (Math.round((endBoucle.getTime() - dateIteration.getTime()) / (1000 * 60 * 60 * 24)) > 1) {
				while (dateIteration < endBoucle) {
					// alert();
					if (fIObj.codate.selectedDates.length == 0) {
						fIObj.codate.selectedDates = [fIObj.commun.toReadable(dateIteration)];
						$(`.fc-day[data-date='${dateIteration.toISOString().slice(0, 10)}']`).addClass('selectedDay')
					} else {
						if (fIObj.codate.selectedDates.indexOf(fIObj.commun.toReadable(dateIteration)) == -1 && deselectedDate.indexOf(fIObj.commun.toReadable(dateIteration)) == -1) {
							fIObj.codate.selectedDates.push(fIObj.commun.toReadable(dateIteration));
							$(`.fc-day[data-date='${dateIteration.toISOString().slice(0, 10)}']`).addClass('selectedDay')
						} else {
							// if (iterationCount > 1) {
							toastr.info('Date deselected')
							$(`.fc-day[data-date='${dateIteration.toISOString().slice(0, 10)}']`).removeClass('activeDay');
							$(`.fc-day[data-date='${dateIteration.toISOString().slice(0, 10)}']`).removeClass('selectedDay');
							deselectedDate.push(fIObj.codate.selectedDates.splice(fIObj.codate.selectedDates.indexOf(fIObj.commun.toReadable(dateIteration)), 1));
							// }
						}
					}
					dateIteration = fIObj.commun.addMoreDays(dateIteration, 1);
					iterationCount++
				}
			}
			if (userId) {
				mylog.log("fIobj click", fIObj);
				fIObj.commun.codateForm(fIObj, function (data) {
					if (typeof data.parent != "undefined") {
						fIObj.commun.generateForm(fIObj, data.parent, [{
							id: "sondageDate",
							label: data.title != '' ? data.title : 'Enquête par date',
							type: "tpls.forms.cplx.sondageDate"
						}], data.inside)
					} else {
						bootbox.alert({ message: trad["Choose context"] });
					}
				})
			} else {
				toastr.info('Veillez vous connecter')
				$(this).prop('checked', false)
			}
		}
		fIObj.options.select = function (start, end, jsEvent, view) {
			var interval,
				buttons;

			if (end.format("HH:mm") === "00:00")
				end.subtract(1, "minutes");
			if (end.isSame(start, "days"))
				interval = start.format("LL");
			else
				interval = start.format("LL") + " - " + end.format("LL");
			buttons = {
				coevent: {
					text: "Evénement communecter",
					action: function () {
						create_co_event(start, end);
					}
				}
			};
			if (!start.isBefore(moment().utcOffset(0).set({ hour: 0, minute: 0, second: 0, millisecond: 0 })) && $('#createCodate').is(':checked'))
				buttons.codate = {
					text: "Codate",
					action: function () {
						create_co_date(start, end);
					}
				};
			buttons.cancel = {
				text: "Annuler"
			};
			$.confirm({
				title: "Créer un élément",
				content: "Quel élément voulez-vous créer pour cette date<br>" + interval,
				buttons: buttons
			});
		}
		fIObj.options.viewDestroy = function () {
			if (fIObj.codate.allEvents) {
				fIObj.codate.allEvents.forEach((value, index) => {
					fIObj.commun.removeEvent(fIObj, value);
				});
			}
		}
		$(fIObj.calendarContainer).fullCalendar('destroy');
		$(fIObj.calendarContainer).fullCalendar(fIObj.options);
		// $(fIObj.container).fullCalendar("gotoDate", moment(Date.now()));

		var checkboxContainer = $(`
				<ul class="ks-cboxtags m-0 p-0">${fIObj.codate.insIsAdmin ?
				`
						<li class="tooltips" data-toggle="tooltip" data-placement="bottom" data-original-title="Séléctionner des périodes">
							<input type='radio' id='createCodate' value="create" name='createCodate'>
							<label for='createCodate' class="create-codate codate-create-button m-0 p-0"></label>
						</li>`
				: ''
			}
					<li class="tooltips" data-toggle="tooltip" data-placement="bottom" data-original-title="Voter sur un codate">
						<input type='radio' checked id='codateVote' value="vote" name='createCodate'>
						<label for='codateVote' class="vote-codate codate-vote-button m-0 p-0"></label>
					</li>
				</ul>
		`);
		if (userId) {
			// Ajout des boutons actions de codate
			$("#codInsideBtnAction").empty();
			$("#codInsideBtnAction").append(checkboxContainer);
			fIObj.codate.initialized = false;
			$('.tooltips').tooltip();
		}
		// fIObj.commun.initWebSocket(fIObj,coWsConfig, answerId)
		fIObj.bindEventCalendar(fIObj);
		fIObj.fixDayClick(fIObj);

	},
	initEvents: function (pInit) {
		mylog.log("calendarObj.initActions", pInit);
		var str = "";
		var fIObj = this;
		var startDate = moment(fIObj.params.starDate).set("month", moment(fIObj.params.starDate).get("month")).valueOf();
		var endDate = moment(fIObj.params.starDate).set("month", moment(fIObj.params.starDate).get("month") + 1).valueOf();
		var secondEndDate = Math.floor(endDate / 1000);
		var sendStartDate = Math.floor(startDate / 1000);
		fIObj.searchInCalendar(fIObj, sendStartDate, secondEndDate);
	},
	commun: {
		// initWebSocket : function(fIObj , coWsConfig , answerId){

		//     if(!notNull(wsCO) || !wsCO.hasListeners('refresh-codate-answer')) {
		//         var wsCO = null

		//         var socketConfigurationEnabled = coWsConfig.enable && coWsConfig.serverUrl && coWsConfig.pingRefreshCodateAnswerUrl;
		//         if (socketConfigurationEnabled) {
		//             wsCO = io(coWsConfig.serverUrl, {
		//                 query: {
		//                     answerId: answerId
		//                 }
		//             });

		//             wsCO.on('refresh-codate-answer', function () {
		//                 // fIObj.event.update(fIObj);
		// 				toastr.success('test mety');
		//             });
		//         }
		//     }

		// },
		getQuery: function (fIObj) {
			var urlQuery = document.location.href;
			var qs = urlQuery.indexOf('?') > -1 ? urlQuery.substring(urlQuery.indexOf('?') + 1).split("&") : urlQuery.substring(urlQuery.indexOf('#') + 1).split(".");
			if (urlQuery.indexOf('?') > -1) {
				for (var i = 0, result = {}; i < qs.length; i++) {
					qs[i] = qs[i].split('=');
					result[qs[i][0]] = qs[i][1];
				}
			}
			if (urlQuery.indexOf('#') > -1) {
				for (var i = 0, result = {}; i < qs.length; i++) {
					if (qs[i + 1]) {
						qs[i] = [qs[i], qs[i + 1]];
						result[qs[i][0]] = qs[i][1];
					}
				}
			}
			return result;
		},
		toReadable: function (date) {
			return date.toLocaleString('fr', { day: 'numeric', month: 'numeric', year: 'numeric' })
		},
		addMoreDays: function (date = new Date(), days) {
			let res = new Date(date.getTime());
			res.setTime(res.getTime() + days * 24 * 60 * 60 * 1000);
			return res != "Invalid Date" ? res : null;
		},
		codateForm: function (fIObj, callback) {
			dyFObj.closeForm();
			const cdForm = {
				jsonSchema: {
					title: "Create Codate",
					description: "",
					icon: "fa-question",
					properties: {
						parent: {
							inputType: "finder",
							label: "Sélectionner un contexte",
							initMe: false,
							buttonLabel: "Rechercher un element",
							placeholder: "Rechercher un element",
							initContext: false,
							initType: ["organizations", "projects"],
							initBySearch: true,
							initContacts: false,
							openSearch: true,
							search: {
								filters: {
									'$or': {
										["links.members." + userId + ".isAdmin"]: { "$exists": true },
										["links.contributors." + userId + ".isAdmin"]: { "$exists": true }
									}
								}
							},
							multiple: false,
							rules: {
								required: true
							}
						},
						title: {
							inputType: "text",
							label: "Titre du sondage",
							value: 'Enquête par date'
						},
						inside: {
							inputType: "codate",
							label: "Date configuration",
							dateRange: fIObj.codate.selectedDates,
							eventDuration: 48
						}
					},
					onLoads: {
						onload: function () {

						}
					},
					beforeBuild: function () {
					},
					save: function (formData) {
						var today = new Date();
						delete formData.collection; delete formData.scope;
						callback(formData);
					}
				}
			}
			if (typeof contextId != "undefined" && typeof contextType != "undefined" && typeof contextName != "undefined") {
				fIObj.contextData = { parent: {} }
				fIObj.contextData["parent"][contextId] = {
					type: contextType,
					name: contextName
				};
			}
			if (Object.keys(fIObj.codate.contextData).length > 0) {
				fIObj.contextData = { parent: {} }
				fIObj.contextData["parent"][fIObj.codate.contextId] = {
					type: fIObj.codate.contextData.contextType,
					name: fIObj.codate.contextData.contextName
				};
			}
			dyFObj.openForm(cdForm, null, fIObj.contextData, null, null, {
				type: "bootbox"
			});
		},
		configureCodateForm: function (fIObj, params = {}, dateRange = [], timeRange = {}, callback) {
			dyFObj.closeForm();
			mylog.log("selected date", fIObj.codate.selectedDates)
			const cdForm = {
				jsonSchema: {
					title: "Configurer " + params.title,
					description: "",
					icon: "fa-question",
					properties: {
						inside: {
							inputType: "codate",
							label: "Date configuration",
							dateRange: dateRange.concat(fIObj.codate.selectedDates),
							timeRange: timeRange,
							eventDuration: 48
						}
					},
					onLoads: {
						onload: function () {

						}
					},
					beforeBuild: function () {
					},
					save: function (formData) {
						var today = new Date();
						delete formData.collection; delete formData.scope;
						callback(formData);
					}
				}
			}
			dyFObj.openForm(cdForm, null, fIObj.contextData, null, null, {
				type: "bootbox"
			});
		},
		selectUserForm: function (fIObj, mailConfig) {
			dyFObj.closeForm();
			const cdForm = {
				jsonSchema: {
					title: "Select person",
					description: "",
					icon: "envelope",
					properties: {
						persons: {
							inputType: "finder",
							label: "Sélectionner des citoyens",
							initMe: false,
							buttonLabel: "Rechercher une personne",
							placeholder: "Rechercher une personne",
							initContext: false,
							initType: ["citoyens"],
							field: 'email',
							showField: false,
							initBySearch: true,
							initContacts: false,
							openSearch: true,
							search: {
								filters: {
									'$or': {
										'$and': [
											// {"roles.tobeactivated": { "$exists": false }},
											{ "email": { "$exists": true } },
											{ "email": { "$ne": '' } },
											{ "preferences.sendMail": { "$ne": false } },
										]
									}
								}
							},
							multiple: true,
							rules: {
								required: true
							}
						},
						mailList: {
							inputType: "textarea",
							label: "Adresse email des personnes qui ne sont pas sur communecter",
							placeholder: "Un par ligne"
						}
					},
					onLoads: {
						onload: function () {

						}
					},
					beforeBuild: function () {
					},
					save: function (formData) {
						var today = new Date();
						var mailParams = {
							tpl: "basic",
							tplObject: "Invitation",
							tplMail: [],
							html: `
								<h4>Invitation à ${mailConfig.formTitle}</h4><br>
								<p style='white-space:pre-line'>${userConnected.name} vous invite à voter sur la proposition ${mailConfig.formTitle}</p><br>
								<a href="${baseUrl}/#codate.calendar.request.html.contextid.${notNull(contextData) ? contextData.id : fIObj.codate.contextData.id}.contexttype.${notNull(contextData) ? contextData.type : fIObj.codate.contextData.type}.form.${mailConfig.formId}.input.${mailConfig.inputId}.standalone.true">Cliquez ici pour voter</a><br><br>
							`,
							btnRedirect: {
								hash: "#email-feedback",
								label: "cliquer ici si vous ne voulez plus recevoir de mail de cette organisation."
							}
						}
						if (formData.collection)
							delete formData.collection;
						if (formData.scope)
							delete formData.scope;
						if (formData.persons) {
							for ([personKey, personValue] of Object.entries(formData.persons)) {
								if (personValue.email)
									mailParams.tplMail.push(personValue.email)
							}
						}
						if (formData.mailList) {
							var mailsList = formData.mailList.split('\n');
							const emailRegex = new RegExp(/^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/g);
							mailsList.forEach(elem => {
								mylog.log("elem ito", elem)
								if (elem.trim().match(emailRegex))
									mailParams.tplMail.push(elem.trim())
							});
						}
						ajaxPost(
							null,
							baseUrl + "/co2/mailmanagement/createandsend",
							mailParams,
							function (data) {
								$("#txtMsg").val("");
								dyFObj.closeForm();
								toastr.success("Votre mail d'invitation a été bien envoyé");
							},
							function (data) {
								toastr.error("Une problème s'est produite, envoie du mail est intérrompu");
							}
						);
					}
				}
			}
			if (typeof contextId != "undefined" && typeof contextType != "undefined" && typeof contextName != "undefined") {
				fIObj.contextData = { parent: {} }
				fIObj.contextData["parent"][contextId] = {
					type: contextType,
					name: contextName
				};
			}
			if (Object.keys(fIObj.codate.contextData).length > 0) {
				fIObj.contextData = { parent: {} }
				fIObj.contextData["parent"][fIObj.codate.contextId] = {
					type: fIObj.codate.contextData.contextType,
					name: fIObj.codate.contextData.contextName
				};
			}
			dyFObj.openForm(cdForm, null, fIObj.contextData, null, null, {
				type: "bootbox"
			});
		},
		codateFormOnly: function (fIObj, callback) {
			dyFObj.closeForm();
			const cdForm = {
				jsonSchema: {
					title: "Create Codate",
					description: "",
					icon: "fa-question",
					properties: {
						parent: {
							inputType: "finder",
							label: "Sélectionner un contexte",
							initMe: false,
							buttonLabel: "Rechercher un element",
							placeholder: "Rechercher un element",
							initContext: false,
							initType: ["organizations", "projects"],
							initBySearch: true,
							initContacts: false,
							openSearch: true,
							search: {
								filters: {
									'$or': {
										["links.members." + userId + ".isAdmin"]: { "$exists": true },
										["links.contributors." + userId + ".isAdmin"]: { "$exists": true }
									}
								}
							},
							multiple: false,
							rules: {
								required: true
							}
						},
						title: {
							inputType: "text",
							label: "Titre du sondage",
							value: 'Enquête par date'
						}
					},
					onLoads: {
						onload: function () {

						}
					},
					beforeBuild: function () {
					},
					save: function (formData) {
						var today = new Date();
						delete formData.collection; delete formData.scope;
						callback(formData);
					}
				}
			}
			if (typeof contextId != "undefined" && typeof contextType != "undefined" && typeof contextName != "undefined") {
				fIObj.contextData = { parent: {} }
				fIObj.contextData["parent"][contextId] = {
					type: contextType,
					name: contextName
				};
			}
			if (Object.keys(fIObj.codate.contextData).length > 0) {
				fIObj.contextData = { parent: {} }
				fIObj.contextData["parent"][fIObj.codate.contextId] = {
					type: fIObj.codate.contextData.contextType,
					name: fIObj.codate.contextData.contextName
				};
			}
			dyFObj.openForm(cdForm, null, fIObj.contextData, null, null, {
				type: "bootbox"
			});
		},
		generateFormOnly: function (fIObj, context, inputs) {
			ajaxPost(
				null,
				baseUrl + "/co2/aap/generateformoceco/",
				{
					"elId": Object.keys(context)[0],
					"elType": context[Object.keys(context)[0]]["type"],
					"createForm": true,
					"inputs": inputs,
					// "formName" : 'Form '+ Date.now().toString(36) +' à Renommer'
					"formName": inputs[0] && inputs[0].label ? inputs[0].label : new Date().toLocaleString('fr', { day: 'numeric', month: 'numeric', year: 'numeric', hour: 'numeric', minute: 'numeric' }).replace(/\/|\s|:/g, '') + ' à Renommer'
				},
				function (data) {
					toastr.success("Codate créer avec succès");
					fIObj.commun.reloadView(fIObj);
					$('input[name="createCodate"][value="vote"]').prop("checked", true);
					// if($('.col-xs-3.input-panel').length == 0)
					// fIObj.commun.showCodateView(fIObj);
					// fIObj.commun.bindCodateEvent(fIObj);
					fIObj.commun.refreshCodate(fIObj);
					dyFObj.closeForm()
				}, null, null, { async: false }
			);
		},
		saveCodateConfig: function (fIObj, keyUnik, parentFormId, params = null) {
			path = "sondageDate" + keyUnik;
			dataHelper.path2Value(
				{
					id: parentFormId,
					collection: "forms",
					path: "params." + path,
					value: params,
					formParentId: parentFormId,
					key: ""
				}, function () {
					toastr.success("Codate créer avec succès");
					fIObj.commun.reloadView(fIObj);
					$('input[name="createCodate"][value="vote"]').prop("checked", true);
					// if($('.col-xs-3.input-panel').length == 0)
					// fIObj.commun.showCodateView(fIObj);
					// fIObj.commun.bindCodateEvent(fIObj);
					fIObj.commun.refreshCodate(fIObj);
					dyFObj.closeForm()
				}
			)
		},
		generateForm: function (fIObj, context, inputs, params = null) {
			ajaxPost(
				null,
				baseUrl + "/co2/aap/generateformoceco/",
				{
					"elId": Object.keys(context)[0],
					"elType": context[Object.keys(context)[0]]["type"],
					"createForm": true,
					"inputs": inputs,
					"formName": 'Form ' + Date.now().toString(36) + ' à Renommer'
				},
				function (data) {
					if (data.contextSlug) {
						path = "sondageDate" + data.keyUnik;
						dataHelper.path2Value(
							{
								id: data["parentForm"],
								collection: "forms",
								path: "params." + path,
								value: params,
								formParentId: data["parentForm"],
								updatePartial: true,
								key: ""
							}, function () {
								toastr.success("Codate créer avec succès");
								fIObj.commun.reloadView(fIObj);
								$('input[name="createCodate"][value="vote"]').prop("checked", true);
								// if($('.col-xs-3.input-panel').length == 0)
								// fIObj.commun.showCodateView(fIObj);
								// fIObj.commun.bindCodateEvent(fIObj);
								fIObj.commun.refreshCodate(fIObj);
								dyFObj.closeForm()
							}
						)
					}
				}, null, null, { async: false }
			);
		},
		reloadView(fIObj) {
			$(fIObj.container + ' ' + fIObj.calendarContainer).fullCalendar('unselect');
			$(fIObj.container + " .fc-day").removeClass("selectedDay");
			$(fIObj.container + " .fc-day").removeClass("activeDay");
			$('.fc-button.btnCreateCodate').remove();
			if (fIObj.codate.allEvents) {
				fIObj.codate.allEvents.forEach(value => {
					fIObj.commun.removeEvent(fIObj, value)
				});
				fIObj.codate.allEvents.length = 0;
			}
			fIObj.codate.selectedDates.length = 0;
			if (fIObj.codate.selectedInputs) delete fIObj.codate.selectedInputs;
		},
		getCodateData: function (fIObj) {
			mylog.log("fIobj ito", fIObj.codate)
			ajaxPost(
				null,
				baseUrl + "/co2/app/codate",
				{
					"contextId": notNull(contextData) ? contextData.id : fIObj.codate.contextId,
				},
				function (res) {
					fIObj.codate.allData = res;
					fIObj.commun.buildAndSortResult(fIObj)
				}, null, null, { async: false }
			);
		},
		buildAndSortResult: function (fIObj) {
			var globalResults = {};
			var detailedResults = {};
			var detailedUsersResults = {};
			var globalUsersResults = {};
			const fullCalendarView = $(fIObj.container + ' ' + fIObj.calendarContainer).fullCalendar('getView');
			if (fIObj.codate.allData && (!fIObj.codate.results['detailed'] || !fIObj.codate.results['global'])) {
				for ([key, value] of Object.entries(fIObj.codate.allData)) {
					if (value['allForms']) {
						if (Object.keys(value['allForms']).length > 0) {
							for ([fK, fV] of Object.entries(value['allForms'])) {
								globalResults[fK] = {};
								globalUsersResults[fK] = {};
								detailedResults[fK] = {};
								detailedUsersResults[fK] = {};
								for ([iK, iV] of Object.entries(fV)) {
									globalResults[fK][iK] = {};
									globalUsersResults[fK][iK] = {};
									detailedResults[fK][iK] = {};
									detailedUsersResults[fK][iK] = {};
									if (iV['label'] && value['params']['sondageDate' + iK]) {
										for ([dK, dV] of Object.entries(value['params']['sondageDate' + iK]['alldates'])) {
											globalResults[fK][iK][dK] = 0;
											globalUsersResults[fK][iK][dK + '-users'] = {}
											// if (fullCalendarView.name == 'month') {
											if (value['answers'] && !fIObj.codate.results['global']) {
												for ([aK, aV] of Object.entries(value['answers'])) {
													if (aV['answers'] && aV['answers'][fK] && aV['answers'][fK][iK] && aV['answers'][fK][iK][dK] && aV['answers'][fK][iK][dK].length > 0) {
														globalUsersResults[fK][iK][dK + '-users'][aV['user']] = {
															name: aV['name'],
															profil: aV['profil']
														}
														globalResults[fK][iK][dK]++
													}
												}
											}
											// } else {
											dV.forEach(elem => {
												detailedResults[fK][iK][dK + '-' + elem] = 0;
												detailedUsersResults[fK][iK][dK + '-' + elem + '-users'] = {};
												if (value['answers'] && !fIObj.codate.results['detailed']) {
													for ([aK, aV] of Object.entries(value['answers'])) {
														if (aV['answers'] && aV['answers'][fK] && aV['answers'][fK][iK] && aV['answers'][fK][iK][dK] && aV['answers'][fK][iK][dK].indexOf(elem) > -1) {
															detailedUsersResults[fK][iK][dK + '-' + elem + '-users'][aV['user']] = {
																name: aV['name'],
																profil: aV['profil']
															}
															detailedResults[fK][iK][dK + '-' + elem]++
														}
													}
												}
											})
											// }
										}
									}
									if (!fIObj.codate.results['global']) {
										var sortable = [];
										for (var vote in globalResults[fK][iK]) {
											sortable.push([vote, globalResults[fK][iK][vote]]);
										}

										sortable.sort(function (a, b) {
											return b[1] - a[1];
										});
										globalResults[fK][iK] = Object.fromEntries(sortable);
									}
									if (!fIObj.codate.results['detailed']) {
										var detailSortable = [];
										for (var vote in detailedResults[fK][iK]) {
											detailSortable.push([vote, detailedResults[fK][iK][vote]]);
										}
										detailSortable.sort(function (a, b) {
											return b[1] - a[1];
										});
										detailedResults[fK][iK] = Object.fromEntries(detailSortable);
									}
								}
							}
						}
					}
				}
			}

			if (Object.keys(fIObj.codate.results).length == 0) {
				// if (fullCalendarView.name == 'month') {
				fIObj.codate.results['global'] = { ...globalResults };
				fIObj.codate.results['globalUsers'] = { ...globalUsersResults }
				// } else {
				fIObj.codate.results['detailed'] = { ...detailedResults };
				fIObj.codate.results['detailedUsers'] = { ...detailedUsersResults };
				// }
			} else {
				// if (fullCalendarView.name == 'month') {
				if (!fIObj.codate.results['global']) {
					fIObj.codate.results['global'] = { ...globalResults };
					fIObj.codate.results['globalUsers'] = { ...globalUsersResults };
				}
				// } else {
				if (!fIObj.codate.results['detailed'])
					fIObj.codate.results['detailed'] = { ...detailedResults };
				fIObj.codate.results['detailedUsers'] = { ...detailedUsersResults };
				// }
			}
		},
		buildCodateList: function (fIObj, isSwitchView = false) {
			var strContent = '';
			colorIteration = 0;
			activeInputs = [];
			fIObj.codate.allEvents.length = 0;
			const fullCalendarView = $(fIObj.container + ' ' + fIObj.calendarContainer).fullCalendar('getView');
			var queryParams = fIObj.commun.getQuery();
			var inputData = {
				formid: '',
				inputid: ''
			};
			if (typeof queryParams == 'object' && queryParams.form)
				inputData.formid = queryParams.form
			if (typeof queryParams == 'object' && queryParams.input)
				inputData.inputid = queryParams.input
			var checkedBgColor = '#fff';
			var hasCodateForm = false;

			if (fIObj.codate.allData && Object.keys(fIObj.codate.allData).length > 0) {
				var allDataIteraion = 0
				for ([key, value] of Object.entries(fIObj.codate.allData)) {
					if (value['allForms']) {
						if (Object.keys(value['allForms']).length > 0) {
							hasCodateForm = true;
							for ([fK, fV] of Object.entries(value['allForms'])) {
								for ([iK, iV] of Object.entries(fV)) {
									checkedBgColor = '#fff';
									const codeColor = fIObj.options.mixColor[colorIteration] ? fIObj.options.mixColor[colorIteration] : 'rebeccapurple';
									if (iV['label'] && value['params']['sondageDate' + iK]) {
										if (fIObj.codate.selectedInputs) {
											if (fIObj.codate.selectedInputs.indexOf(iK) > -1) {
												if (fIObj.codate.selectedInputs[fIObj.codate.selectedInputs.indexOf(iK)] == iK) {
													checkedBgColor = codeColor;
													for ([dK, dV] of Object.entries(value['params']['sondageDate' + iK]['alldates'])) {
														const dateArr = dK.split('/');
														const startDate = moment(`${dateArr[2]}-${dateArr[1]}-${dateArr[0]}`);
														if (fullCalendarView.name == 'month') {
															var toCheck = false;
															var nbVote = 0;
															if (fIObj.codate.results['global'][fK][iK]) {
																const dateKeysSorted = Object.keys(fIObj.codate.results['global'][fK][iK]);
																nbVote = fIObj.codate.results['global'][fK][iK][dK] ? fIObj.codate.results['global'][fK][iK][dK] : 0;
																if (dateKeysSorted.indexOf(dK) == 0 && fIObj.codate.results['global'][fK][iK][dateKeysSorted[0]] != 0) {
																	toCheck = true;
																}
																if (fIObj.codate.results['global'][fK][iK][dK] == fIObj.codate.results['global'][fK][iK][dateKeysSorted[0]] && fIObj.codate.results['global'][fK][iK][dateKeysSorted[0]] != 0) {
																	toCheck = true;
																}
															}
															if ((!fIObj.codate.initialized || isSwitchView) && dK == Object.keys(value['params']['sondageDate' + iK]['alldates'])[0]) {
																if (new Date(startDate.format('YYYY-MM-DD')) < new Date($($('.fc-day-top')[0]).attr('data-date')) || new Date(startDate.format('YYYY-MM-DD')) > new Date($($('.fc-day-top')[$('.fc-day-top').length - 1]).attr('data-date'))) {
																	$(fIObj.container + ' ' + fIObj.calendarContainer).fullCalendar('gotoDate', startDate);
																}
															}
															fIObj.commun.addEvent(
																fIObj,
																iK,
																iV['label'],
																startDate,
																codeColor,
																JSON.stringify(value['params']['sondageDate' + iK]['alldates']).replace(/"/g, '\''),
																key,
																fK,
																true,
																null,
																toCheck,
																nbVote
															);
														} else {
															if ((!fIObj.codate.initialized || isSwitchView) && dK == Object.keys(value['params']['sondageDate' + iK]['alldates'])[0]) {
																if (new Date(startDate.format('YYYY-MM-DD')) < new Date($($('.fc-day-header')[0]).attr('data-date')) || new Date(startDate.format('YYYY-MM-DD')) > new Date($($('.fc-day-header')[$('.fc-day-header').length - 1]).attr('data-date'))) {
																	$(fIObj.container + ' ' + fIObj.calendarContainer).fullCalendar('gotoDate', startDate);
																}
															}
															dV.forEach(elem => {
																const newStartDate = elem ? moment(`${dateArr[2]}-${dateArr[1]}-${dateArr[0]} ${elem}`) : moment(`${dateArr[2]}-${dateArr[1]}-${dateArr[0]}`);
																const endDate = elem ? moment(`${dateArr[2]}-${dateArr[1]}-${dateArr[0]} ${elem.split(':')[0]}:59`) : moment(`${dateArr[2]}-${dateArr[1]}-${dateArr[0]}`);
																const eventLabel = iV['label'].length > 25 ? iV['label'].substring(0, 25) + '...' : iV['label'];
																var toCheck = false;
																var nbVote = 0;
																if (fIObj.codate.results['detailed'][fK][iK]) {
																	const dateKeysSorted = Object.keys(fIObj.codate.results['detailed'][fK][iK]);
																	if (dateKeysSorted.indexOf(dK + '-' + elem) == 0 && fIObj.codate.results['detailed'][fK][iK][dateKeysSorted[0]] != 0) {
																		toCheck = true;
																	}
																	if (fIObj.codate.results['detailed'][fK][iK][dK + '-' + elem] == fIObj.codate.results['detailed'][fK][iK][dateKeysSorted[0]] && fIObj.codate.results['detailed'][fK][iK][dateKeysSorted[0]] != 0) {
																		toCheck = true;
																	}
																	nbVote = fIObj.codate.results['detailed'][fK][iK][dK + '-' + elem] ? fIObj.codate.results['detailed'][fK][iK][dK + '-' + elem] : 0;
																}
																fIObj.commun.addEvent(
																	fIObj,
																	iK,
																	eventLabel,
																	newStartDate,
																	codeColor,
																	JSON.stringify(value['params']['sondageDate' + iK]['alldates']).replace(/"/g, '\''),
																	key,
																	fK,
																	false,
																	endDate,
																	toCheck,
																	nbVote
																);
															});
															fIObj.commun.bindTimeEvent(fIObj);
														}
													}
													activeInputs.push(iK);
												}
											}
										} else {
											for ([dK, dV] of Object.entries(value['params']['sondageDate' + iK]['alldates'])) {
												const dateArr = dK.split('/');
												const startDate = moment(`${dateArr[2]}-${dateArr[1]}-${dateArr[0]}`);
												if (fullCalendarView.name == 'month') {
													var toCheck = false;
													var nbVote = 0;
													if (fIObj.codate.results['global'][fK][iK]) {
														const dateKeysSorted = Object.keys(fIObj.codate.results['global'][fK][iK]);
														nbVote = fIObj.codate.results['global'][fK][iK][dK] ? fIObj.codate.results['global'][fK][iK][dK] : 0;
														if (dateKeysSorted.indexOf(dK) == 0 && fIObj.codate.results['global'][fK][iK][dateKeysSorted[0]] != 0) {
															toCheck = true;
														}
														if (fIObj.codate.results['global'][fK][iK][dK] == fIObj.codate.results['global'][fK][iK][dateKeysSorted[0]] && fIObj.codate.results['global'][fK][iK][dateKeysSorted[0]] != 0) {
															toCheck = true;
														}
													}
													// if (
													// 	key == Object.keys(fIObj.codate.allData)[Object.keys(fIObj.codate.allData).length - 1] &&
													// 	fK == Object.keys(value['allForms'])[Object.keys(value['allForms']).length - 1] &&
													// 	iK == Object.keys(fV)[Object.keys(fV).length - 2]
													// ) {
													if (inputData.formid != '' && inputData.inputid != '' && key == inputData.formid && iK == inputData.inputid) {
														if ((!fIObj.codate.initialized || isSwitchView) && dK == Object.keys(value['params']['sondageDate' + iK]['alldates'])[0]) {
															if (new Date(startDate.format('YYYY-MM-DD')) < new Date($($('.fc-day-top')[0]).attr('data-date')) || new Date(startDate.format('YYYY-MM-DD')) > new Date($($('.fc-day-top')[$('.fc-day-top').length - 1]).attr('data-date'))) {
																$(fIObj.container + ' ' + fIObj.calendarContainer).fullCalendar('gotoDate', startDate);
															}
														}
														fIObj.commun.addEvent(
															fIObj,
															iK,
															iV['label'],
															startDate,
															codeColor,
															JSON.stringify(value['params']['sondageDate' + iK]['alldates']).replace(/"/g, '\''),
															key,
															fK,
															true,
															null,
															toCheck,
															nbVote
														);
													} else if (activeInputs.length == 0 && inputData.formid == '') {
														if ((!fIObj.codate.initialized || isSwitchView) && dK == Object.keys(value['params']['sondageDate' + iK]['alldates'])[0]) {
															if (new Date(startDate.format('YYYY-MM-DD')) < new Date($($('.fc-day-top')[0]).attr('data-date')) || new Date(startDate.format('YYYY-MM-DD')) > new Date($($('.fc-day-top')[$('.fc-day-top').length - 1]).attr('data-date'))) {
																$(fIObj.container + ' ' + fIObj.calendarContainer).fullCalendar('gotoDate', startDate);
															}
														}
														fIObj.commun.addEvent(
															fIObj,
															iK,
															iV['label'],
															startDate,
															codeColor,
															JSON.stringify(value['params']['sondageDate' + iK]['alldates']).replace(/"/g, '\''),
															key,
															fK,
															true,
															null,
															toCheck,
															nbVote
														);
													}
												} else {
													dV.forEach(elem => {
														const newStartDate = elem ? moment(`${dateArr[2]}-${dateArr[1]}-${dateArr[0]} ${elem}`) : moment(`${dateArr[2]}-${dateArr[1]}-${dateArr[0]}`);
														const endDate = elem ? moment(`${dateArr[2]}-${dateArr[1]}-${dateArr[0]} ${elem.split(':')[0]}:59`) : moment(`${dateArr[2]}-${dateArr[1]}-${dateArr[0]}`);
														const eventLabel = iV['label'].length > 25 ? iV['label'].substring(0, 25) + '...' : iV['label'];
														var toCheck = false;
														var nbVote = 0;
														if (fIObj.codate.results['detailed'][fK][iK]) {
															const dateKeysSorted = Object.keys(fIObj.codate.results['detailed'][fK][iK]);
															if (dateKeysSorted.indexOf(dK + '-' + elem) == 0 && fIObj.codate.results['detailed'][fK][iK][dateKeysSorted[0]] != 0) {
																toCheck = true;
															}
															if (fIObj.codate.results['detailed'][fK][iK][dK + '-' + elem] == fIObj.codate.results['detailed'][fK][iK][dateKeysSorted[0]] && fIObj.codate.results['detailed'][fK][iK][dateKeysSorted[0]] != 0) {
																toCheck = true;
															}
															nbVote = fIObj.codate.results['detailed'][fK][iK][dK + '-' + elem] ? fIObj.codate.results['detailed'][fK][iK][dK + '-' + elem] : 0;
														}
														if (
															key == Object.keys(fIObj.codate.allData)[Object.keys(fIObj.codate.allData).length - 1] &&
															fK == Object.keys(value['allForms'])[Object.keys(value['allForms']).length - 1] &&
															iK == Object.keys(fV)[Object.keys(fV).length - 2]
														) {
															if ((!fIObj.codate.initialized || isSwitchView) && dK == Object.keys(value['params']['sondageDate' + iK]['alldates'])[0]) {
																if (new Date(startDate.format('YYYY-MM-DD')) < new Date($($('.fc-day-header')[0]).attr('data-date')) || new Date(startDate.format('YYYY-MM-DD')) > new Date($($('.fc-day-header')[$('.fc-day-header').length - 1]).attr('data-date'))) {
																	$(fIObj.container + ' ' + fIObj.calendarContainer).fullCalendar('gotoDate', startDate);
																}
															}
															fIObj.commun.addEvent(
																fIObj,
																iK,
																eventLabel,
																newStartDate,
																codeColor,
																JSON.stringify(value['params']['sondageDate' + iK]['alldates']).replace(/"/g, '\''),
																key,
																fK,
																false,
																endDate,
																toCheck,
																nbVote
															);
														}
													});
													fIObj.commun.bindTimeEvent(fIObj);
												}
											}
											// if (
											// 	key == Object.keys(fIObj.codate.allData)[Object.keys(fIObj.codate.allData).length - 1] &&
											// 	fK == Object.keys(value['allForms'])[Object.keys(value['allForms']).length - 1] &&
											// 	iK == Object.keys(fV)[Object.keys(fV).length - 2]
											// ) {
											if (inputData.formid != '' && inputData.inputid != '' && key == inputData.formid && iK == inputData.inputid) {
												activeInputs.push(iK);
												checkedBgColor = codeColor
											} else if (activeInputs.length == 0 && inputData.formid == '') {
												activeInputs.push(iK);
												checkedBgColor = codeColor
											}
										}

										var strAction = `
										<div class="dropdown cod-inside">
											<a href="javascript:;" type="button" class="text-center link-banner text-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
												<i class="fa fa-ellipsis-v a-icon-banner"></i>
											</a>
											<ul class="dropdown-menu arrow_box menu-params cod-inside-menu">
												<li class="text-left">
													<a href="javascript:;" class="bg-white showCodateResult" data-parentform="${key}" data-subformid="${fK}" data-kunik="sondageDate${iK}" data-inputkey="${iK}" data-inputvalue="${JSON.stringify(iV).replace(/"/g, '\'')}" data-paramsdata="${JSON.stringify(value["params"]["sondageDate" + iK]).replace(/"/g, '\'')}">
														<i class="fa fa-file-text-o"></i> Afficher résultat
													</a>
												<li>
												<li class="text-left">
													<a href="javascript:;" class="bg-white copyCodateLink" data-clipboard-text="${baseUrl}/#codate.calendar.request.html.contextid.${notNull(contextData) ? contextData.id : fIObj.codate.contextData.id}.contexttype.${notNull(contextData) ? contextData.type : fIObj.codate.contextData.type}.form.${key}.input.${iK}.standalone.true" data-clipboard-action="copy">
														<i class="fa fa-link"></i> Partager lien
													</a>
												<li>
													`;
										if (fIObj.codateIsAdmin) {
											strAction += `
												<li class="text-left">
													<a href="javascript:;" class="bg-white archiveCodate" data-formid="${key}" data-titleQuestion="${iV['label']}">
														<i class="fa fa-archive"></i> Archiver
													</a>
												</li>
												<li class="text-left">
													<a href="javascript:;" class="bg-white editConfigCodate" data-formid="${key}" data-title="${iV['label']}" data-keyunik="${iK}" data-parentformid="${key}" data-daterange="${Object.keys(value['params']['sondageDate' + iK]['alldates'])}" data-timerange="${JSON.stringify(value["params"]["sondageDate" + iK]['alldates']).replace(/"/g, '\'')}">
														<i class="fa fa-gear"></i> Configurer periode
													</a>
												</li>
												<li class="text-left">
													<a href="javascript:;" class="bg-white sendInvitation" data-formid="${key}" data-title="${iV['label']}" data-keyunik="${iK}" data-parentformid="${key}" data-daterange="${Object.keys(value['params']['sondageDate' + iK]['alldates'])}" data-timerange="${JSON.stringify(value["params"]["sondageDate" + iK]['alldates']).replace(/"/g, '\'')}">
														<i class="fa fa-envelope"></i> Envoyer un mail d'invitation
													</a>
												</li>
												<li class="text-left">
													<a href="javascript:;" class="bg-white btnInviteCommunity" data-formid="${key}" data-title="${iV['label']}" data-keyunik="${iK}" data-parentformid="${key}" data-daterange="${Object.keys(value['params']['sondageDate' + iK]['alldates'])}" data-timerange="${JSON.stringify(value["params"]["sondageDate" + iK]['alldates']).replace(/"/g, '\'')}">
														<i class="fa fa-bell"></i> Inviter la communauté
													</a>
												</li>
												<li class="text-left">
													<a href="javascript:;" class="bg-white renameCodate" data-subformid="${fV['_id']}" data-formid="${key}" data-title="${iV['label']}" data-keyunik="${iK}" data-parentformid="${key}" >
														<i class="fa fa-pencil"></i> Rennomer le codate
													</a>
												</li>
												<li class="text-left">
													<a href="javascript:;" class="bg-white renameForm" data-formtitle="${value['name']}" data-formid="${key}" data-title="${iV['label']}" data-keyunik="${iK}" data-parentformid="${key}" data-daterange="${Object.keys(value['params']['sondageDate' + iK]['alldates'])}" data-timerange="${JSON.stringify(value["params"]["sondageDate" + iK]['alldates']).replace(/"/g, '\'')}">
														<i class="fa fa-cogs"></i> Rennomer le formulaire
													</a>
												</li>
												<li class="text-left">
													<a href="javascript:;" class="bg-white deleteCodateForm" data-id="${key}">
														<i class="fa fa-trash"></i> Supprimer
													</a>
												</li>
											`
										}
										strAction += `			
											</ul>
										</div>`;
										strContent += `
											<div class="d-flex justify-content-between">
												<label class="ins-cpl tooltips" for="${iK}" data-toggle="tooltip" data-placement="bottom" data-original-title="Dans le formulaire ${value['name']}">
													<div class="ins-cpl-actions">
														<input type='checkbox' id='${iK}' class="ins-cpl-check" name='codInput' data-label="${iV['label']}" data-codecolor="${codeColor}" data-formid="${key}" data-subform="${fK}" data-dates="${Object.keys(value['params']['sondageDate' + iK]['alldates'])}" data-dateTime="${JSON.stringify(value['params']['sondageDate' + iK]['alldates']).replace(/"/g, '\'')}" ${fIObj.codate.selectedInputs ? (fIObj.codate.selectedInputs.indexOf(iK) > -1 ? (fIObj.codate.selectedInputs[fIObj.codate.selectedInputs.indexOf(iK)] == iK ? 'checked' : '') : '') : (checkedBgColor != '#fff' ? 'checked' : '')}/>
														<span class="w-70 ins-checkmark" style="background-color: ${checkedBgColor}; border-color: ${codeColor}" data-dates="${Object.keys(value['params']['sondageDate' + iK]['alldates'])}" data-dateTime="${JSON.stringify(value['params']['sondageDate' + iK]['alldates']).replace(/"/g, '\'')}"></span>
													</div>
													<span class="ins-cpl-text">
														${iV['label']} (${Object.keys(value["answers"]).length})
													</span>
												</label>
												${strAction}
											</div>
										`;
										colorIteration++
									} else if (iV['label']) {
										var strAction = `
										<div class="dropdown cod-inside">
											<a href="javascript:;" class="text-center link-banner text-default btnConfigureCodate tooltips" type="button" data-toggle="tooltip" data-placement="bottom" data-original-title="Configurer" data-title="${iV['label']}" data-keyunik="${iK}" data-parentformid="${key}"><i class="fa fa-gear a-icon-banner"></i></a>
											<a href="javascript:;" type="button" class="text-center link-banner text-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
												<i class="fa fa-ellipsis-v a-icon-banner"></i>
											</a>
											<ul class="dropdown-menu arrow_box menu-params cod-inside-menu">`;
										if (fIObj.codateIsAdmin) {
											strAction += `
												<li class="text-left">
													<a href="javascript:;" class="bg-white archiveCodate" data-formid="${key}" data-titleQuestion="${iV['label']}">
														<i class="fa fa-archive"></i> Archiver
													</a>
												</li>
												<li class="text-left">
													<a href="javascript:;" class="bg-white deleteCodateForm" data-id="${key}">
														<i class="fa fa-trash"></i> Supprimer
													</a>
												</li>
											`
										}
										strAction += `			
											</ul>
										</div>`;
										strContent += `
											<div class="col row" style="margin-bottom: 10px;">
												<div class="col-xs-12 d-flex justify-content-between">
													<label class="ins-cpl tooltips" style="overflow-x: clip; width: ${fIObj.codateIsAdmin ? '70%' : '100%'}" for="${iK}" data-toggle="tooltip" data-placement="bottom" data-original-title="Dans le formulaire ${value['name']}">
														<div class="ins-cpl-actions">
															<input type='checkbox' id='${iK}' class="ins-cpl-check" name='codInput' data-label="${iV['label']}" data-codecolor="${codeColor}" data-formid="${key}" data-subform="${fK}" data-dates="" data-dateTime="" />
															<span class="w-70 ins-checkmark" style="background-color: ${checkedBgColor}; border-color: ${codeColor}" data-dates="" data-dateTime=""></span>
														</div>
														<span class="ins-cpl-text">
															${iV['label']}
														</span>
													</label>
													<span class="ins-cpl-text cdt-text-span">(${Object.keys(value["answers"]).length})</span>
													${strAction}
												</div>
												<div class="col-xs-12">
													<span class="cdt-text-info text-red">Sélectionner des périodes pour la configuration</span>
												</div>
											</div>
										`;
										colorIteration++
									}
								}
							}
						}
					} else {

					}
					if (Object.keys(fIObj.codate.allData).length - 1 == allDataIteraion && !hasCodateForm)
						strContent += `<div class="text-center color-gray"><span>Aucun élément</span></div>`
					allDataIteraion++;
				}
			} else {
				strContent += `<div class="text-center color-gray"><span>Aucun élément</span></div>`
			}
			if (!fIObj.codate.selectedInputs) {
				fIObj.codate.selectedInputs = Array.from(activeInputs)
			}
			mylog.log("activveInputs", fIObj.codate.selectedInputs)
			return strContent
		},
		getUserAnswers: function (fIObj, userId) {
			if (fIObj.codate.allData) {
				for ([key, value] of Object.entries(fIObj.codate.allData)) {
					if (value['allForms']) {
						if (Object.keys(value['allForms']).length > 0) {
							for ([fK, fV] of Object.entries(value['allForms'])) {
								for ([iK, iV] of Object.entries(fV)) {
									if (iV['label'] && value['params']['sondageDate' + iK]) {
										for ([aK, aV] of Object.entries(value['answers'])) {
											if (aV['user'] == userId) {
												if (aV['answers']) {
													if (aV['answers'][fK]) {
														if (aV['answers'][fK][iK]) {
															fIObj.codate.userAnswers[iK] = aV['answers'][fK][iK];
															fIObj.codate.answers[iK] = {
																id: aK
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		},
		checkIfExistAnswer: function (fIObj, answers, userId) {
			var answer = null;
			for ([key, value] of Object.entries(answers)) {
				if (value['user'] == userId) {
					answer = {
						[key]: value
					}
				}
			}
			return answer
		},
		addEvent: function (fIObj, id, title, start, color, times, formId, subForm, allDay = true, end = null, toCheck = false, nbVote = 0) {
			if (fIObj.codate.allEvents.indexOf(id) < 0) fIObj.codate.allEvents.push(id);
			$(fIObj.container + ' ' + fIObj.calendarContainer).fullCalendar('renderEvent', allDay ? {
				id: id,
				title: title,
				start: start,
				allDay: allDay,
				codateEvent: {
					'bg-color': color,
					times: times,
					formId: formId,
					subForms: subForm,
					nbVote: nbVote,
					toCheck: toCheck
				}
			} : {
				id: id,
				title: title,
				start: start,
				end: end,
				allDay: allDay,
				codateEvent: {
					'bg-color': color,
					times: times,
					formId: formId,
					subForms: subForm,
					nbVote: nbVote,
					toCheck: toCheck,
				}
			}
			);
			fIObj.fixDayClick(fIObj);
		},
		removeEvent: function (fIObj, id) {
			$(fIObj.container + ' ' + fIObj.calendarContainer).fullCalendar('removeEvents', id);
			fIObj.fixDayClick(fIObj);
		},
		showCodateView: function (fIObj, action = '') {
			fIObj.commun.getCodateData(fIObj);
			if (Object.keys(fIObj.codate.userAnswers).length == 0) {
				fIObj.commun.getUserAnswers(fIObj, userId)
			}
			var codateListHTML = fIObj.commun.buildCodateList(fIObj);
			const fullCalendarView = $(fIObj.container + ' ' + fIObj.calendarContainer).fullCalendar('getView');
			$(fIObj.container + " #codInsideInput").empty();
			$(fIObj.container + " #codInsideInput").append(`
                    ${codateListHTML}
				`);
			$('.tooltips').tooltip();
			if (action == '')
				$('.col-xs-3.ins-card.codate-panel.vote-panel').remove();
			if (fullCalendarView.name == 'month' && action == '') {
				$(fIObj.container).append(`
						<div class="col-xs-3 ins-card codate-panel vote-panel">
							<div class="title ins-card-header text-center">${new Date().toLocaleString("fr", { weekday: "long", day: "numeric", month: "long", year: "numeric" })}</div>
							<div class="times-content ins-card-body col-xs-12">
							</div>
						</div>
					`);
			}
		},
		renderCodateEvent: function (fIObj, isSwitchView = false) {
			fIObj.commun.getCodateData(fIObj);
			if (Object.keys(fIObj.codate.userAnswers).length == 0) {
				fIObj.commun.getUserAnswers(fIObj, userId)
			}
			fIObj.commun.buildCodateList(fIObj, isSwitchView);
		},
		gotoDate: function (fIObj, dateTo) {
			$(fIObj.container + ' ' + fIObj.calendarContainer).fullCalendar('gotoDate', dateTo);
			if ($('input[name="createCodate"][value="vote"]').is(":checked") || notNull(fIObj.codate.contextId)) {
				fIObj.commun.renderCodateEvent(fIObj)
			}
			fIObj.fixDayClick(fIObj);
		},
		buildTimeList: function (fIObj, inputId, formId, subForm, clickedDate, times, title = '') {
			$(fIObj.container + " .vote-panel .title").html(clickedDate.toLocaleString("fr", { weekday: "long", day: "numeric", month: "long", year: "numeric" }));
			var timesHTML = `<ul class="ks-cboxtags d-flex flex-xs-column flex-sm-column flex-lg-column justify-content-center">`;
			dateKey = clickedDate.toLocaleString('fr', { day: 'numeric', month: 'numeric', year: 'numeric' });
			mylog.log('user answer', dateKey, fIObj.codate.userAnswers);
			eventDuration = '';
			const generateCreateBtn = function (timeValue) {
				var strCreateBtn = ''
				if (userId) {
					if (fIObj.codateIsAdmin) {
						strCreateBtn = `<span class="col-xs-3 btn-create-event createEventP" data-time="${timeValue}" data-date="${dateKey}" data-eventduration='${eventDuration}' data-titleQuestion="${title}" data-id="null" data-formid="${formId}"><i class="fa fa-calendar"></i></span>`;
					}
				}
				return strCreateBtn
			}
			if (fIObj.codate.allData[formId]['params']['sondageDate' + inputId])
				if (fIObj.codate.allData[formId]['params']['sondageDate' + inputId]['eventduration'])
					eventDuration = fIObj.codate.allData[formId]['params']['sondageDate' + inputId]['eventduration'];
			times.forEach((tV, tK) => {
				var checkedInput = '';

				if (fIObj.codate.userAnswers[inputId]) {
					if (fIObj.codate.userAnswers[inputId][dateKey]) {
						if (fIObj.codate.userAnswers[inputId][dateKey].indexOf(tV) > -1) {
							checkedInput = 'checked'
						}
					}
				}
				var toCheck = '';
				var nbVote = 0;
				var usersTimeHTML = ``;
				if (fIObj.codate.results['detailed'][subForm][inputId]) {
					const dateKeysSorted = Object.keys(fIObj.codate.results['detailed'][subForm][inputId]);
					if (dateKeysSorted.indexOf(dateKey + '-' + tV) == 0 && fIObj.codate.results['detailed'][subForm][inputId][dateKeysSorted[0]] != 0) {
						toCheck = '<i class="fa fa-star"></i>';
					}
					if (fIObj.codate.results['detailed'][subForm][inputId][dateKey + '-' + tV] == fIObj.codate.results['detailed'][subForm][inputId][dateKeysSorted[0]] && fIObj.codate.results['detailed'][subForm][inputId][dateKeysSorted[0]] != 0) {
						toCheck = '<i class="fa fa-star"></i>';
					}
				}
				if (fIObj.codate.results && fIObj.codate.results['detailedUsers'] && fIObj.codate.results['detailedUsers'][subForm] && fIObj.codate.results['detailedUsers'][subForm][inputId] && fIObj.codate.results['detailedUsers'][subForm][inputId][dateKey + '-' + tV + '-users']) {
					for ([uK, uV] of Object.entries(fIObj.codate.results['detailedUsers'][subForm][inputId][dateKey + '-' + tV + '-users'])) {
						usersTimeHTML += `
							<li class="col-xs-12">
								<div style='display: inline-block'>
									<div class="col-xs-2">
										<img src="${uV.profil ? uV.profil : defaultImage}" alt=""  width="25" height="25" style="border-radius: 100%;object-fit:cover"/>
									</div>
									<div class="col-xs-9">
										<span class="custom-font"><small>${uV.name}</small></span><br/>
									</div>
								</div>
							<li>
						`;
						mylog.log("verif data", uK, tV, usersTimeHTML)
					}
				}

				timesHTML += `
				    <li class="col-xs-12 p0-mAuto row ${fIObj.codateIsAdmin ? '' : 'd-flex'} flex-lg-column justify-content-center">
						<input type='checkbox' id='${tK + tV}' name='codateTime' value="${tV}" ${checkedInput} data-formid="${formId}" data-subform="${subForm}" data-inputid="${inputId}" data-date="${dateKey}" data-times="${times}" data-title="${title}">
						<label for='${tK + tV}' class="col-xs-8 mx-auto dropdown">
							${toCheck} ${tV}
							${usersTimeHTML == '' ?
						`<ul class='dropdown-menu'>
										<li><div class="text-center"><span class="custom-font"><small>Aucun élément</small></span></div></li>
									 </ul>` :
						`<ul class='dropdown-menu row'>
										${usersTimeHTML}
									 </ul>
									`
					}
						</label>
						${generateCreateBtn(tV)}
					</li>
				`
			});
			timesHTML += `</ul>`;
			usersHTML = '';
			var nbVotant = 0;
			var alreadyCounted = [];
			if (fIObj.codate.results && fIObj.codate.results['globalUsers'] && fIObj.codate.results['globalUsers'][subForm] && fIObj.codate.results['globalUsers'][subForm][inputId] && fIObj.codate.results['globalUsers'][subForm][inputId][dateKey + '-users']) {
				for ([uK, uV] of Object.entries(fIObj.codate.results['globalUsers'][subForm][inputId][dateKey + '-users'])) {
					if (alreadyCounted.indexOf(uK) == -1) {
						usersHTML += `
							<div style='display: inline-block'>
								<div class="col-xs-2">
									<img src="${uV.profil ? uV.profil : defaultImage}" alt=""  width="25" height="25" style="border-radius: 100%;object-fit:cover"/>
								</div>
								<div class="col-xs-9">
									<span><small>${uV.name}</small></span><br/>
								</div>
							</div>
						`;
						nbVotant++;
						alreadyCounted.push(uK);
					}
				}
			}
			timesHTML += `
				<div>
					<span>Liste votants (${nbVotant})</span>
					<div>${usersHTML}</div>
				</div>
			`;
			mylog.log('all results', fIObj.codate.results);
			$(fIObj.container + " .vote-panel .times-content").html(timesHTML);

			// bind event
			$(".createEventP").off().on("click", function (event) {
				var self = this;
				event.stopImmediatePropagation();
				const addHours = function (hours, date = new Date()) {
					const dateCopy = new Date(date.getTime());
					dateCopy.setTime(dateCopy.getTime() + hours * 60 * 60 * 1000);
					return dateCopy != "Invalid Date" ? dateCopy.toLocaleString('fr', { day: 'numeric', month: 'numeric', year: 'numeric', hour: 'numeric', minute: 'numeric' }) : null;
				}
				var startDate = `${$(self).attr("data-date")} ${$(self).attr("data-time")}`;
				const [d, m, y] = $(self).attr("data-date").split("/");
				const sDate = new Date(`${y}-${m}-${d} ${$(self).attr("data-time")}`);
				var endDate = addHours($(self).attr("data-eventduration"), sDate);
				const eDate = endDate.indexOf(":") > -1 ? endDate.substring(0, endDate.indexOf(":") - 2) : '';
				const eTime = endDate.indexOf(":") > -1 ? endDate.substring(endDate.indexOf(":") - 2) : '';
				bootbox.dialog({
					title: trad.confirm,
					message: `<span class='text-success bold'><i class='fa fa-info'></i> ${tradForm.wouldYouLikeToCreateTheEvent} ${$(self).attr("data-titleQuestion")}<br><span class="text-black">${trad.fromdate} ${$(self).attr("data-date")} ${trad.to} ${$(self).attr("data-time")} ${trad.until} ${eDate} ${trad.to} ${eTime}</span> </span>`,
					buttons: [
						{
							label: "Ok",
							className: "btn btn-primary pull-left",
							callback: function () {
								const oneParent = {
									[typeof contextId != 'undefined' ? contextId : fIObj.codate.contextId]: {
										name: notNull(contextData) ? contextData.name : fIObj.codate.contextData.name,
										type: notNull(contextData) ? contextData.type : fIObj.codate.contextData.type
									}
								}
								const [day, month, year] = $(self).attr("data-date") ? $(self).attr("data-date").split("/") : null;
								let dataToSend = {
									id: '',
									name: $(self).attr("data-titleQuestion"),
									type: 'workshop',
									organizer: oneParent,
									startDate: startDate,
									endDate: endDate,
									collection: "events",
									key: "event",
									public: true,
									timeZone: Intl.DateTimeFormat().resolvedOptions().timeZone,
									preferences: {
										isOpenData: true,
										isOpenEdition: true
									},
								};

								ajaxPost(
									null,
									baseUrl + "/co2/search/globalautocomplete",
									{
										name: $(self).attr("data-titleQuestion"),
										searchType: 'events',
										indexMin: 0,
										indexMax: 50
									},
									function (res) {
										if (res) {
											var indexFinded = -1;
											if (Object.values(res.results).length > 0) {
												for ([index, value] of Object.entries(res.results)) {
													value.name && value.name.trim() == dataToSend.name.trim() ? indexFinded = 0 : ''
												}
											}
											if (Object.values(res.results).length > 0 && indexFinded > -1) {
												toastr.warning(trad.checkItemAlreadyExist);
											} else {
												ajaxPost(null, baseUrl + "/co2/element/save", dataToSend, function (data) {
													if (data.result) {
														toastr.success(tradForm.modificationSave);
														bootbox.dialog({
															title: trad.confirm,
															message: `<span class='text-success bold'>
																	<i class='fa fa-info'></i> 
																	voulez-vous archiver ${$(self).attr("data-titleQuestion")}<br>
																	<span class="text-black">Cette action va modifier le status du formulaire parent; ça veut dire que s'il y a d'autres codates dans ce formulaire, ils seront archivés aussi.</span> 
																</span>`,
															buttons: [
																{
																	label: "Archiver",
																	className: "btn btn-primary pull-left",
																	callback: function () {
																		if (userId) {
																			if (fIObj.codateIsAdmin) {
																				let dataToSend = {
																					id: $(self).attr("data-formid"),
																					collection: "forms",
																					path: "inArchive",
																					value: true
																				}

																				if (typeof dataToSend.value == "undefined") {
																					toastr.error('value cannot be empty!');
																				} else {
																					dataHelper.path2Value(dataToSend, function (params) {
																						toastr.success("Codate archivé avec succès");
																						fIObj.commun.completeRefreshCodate(fIObj)
																					});
																				}
																			}
																		}
																	}
																},
																{
																	label: "Annuler",
																	className: "btn btn-default pull-left",
																	callback: function () { }
																}
															]
														})
													}
												}, "json");
											}
										}
									}
								)
							}
						},
						{
							label: "Annuler",
							className: "btn btn-default pull-left",
							callback: function () { }
						}
					]
				});
			});
		},
		switchView: function (fIObj, viewName, isSwitchView = false) {
			fIObj.commun.renderCodateEvent(fIObj, isSwitchView);
			if (viewName == 'month') {
				$('.codate-panel.vote-panel').remove();
				$(fIObj.container).append(`
					<div class="col-xs-3 ins-card codate-panel vote-panel">
						<div class="title ins-card-header text-center">${new Date().toLocaleString("fr", { weekday: "long", day: "numeric", month: "long", year: "numeric" })}</div>
						<div class="times-content ins-card-body col-xs-12">
						</div>
					</div>
				`);
			} else {
				$('.codate-panel.vote-panel').remove()
			}
		},
		refreshCodate: function (fIObj, action = '') {
			if (fIObj.codate.allEvents) {
				fIObj.codate.allEvents.forEach(value => {
					fIObj.commun.removeEvent(fIObj, value)
				});
				fIObj.codate.allEvents.length = 0;
			}
			if (fIObj.codate.results['global']) delete fIObj.codate.results['global'];
			if (fIObj.codate.results['detailed']) delete fIObj.codate.results['detailed'];
			fIObj.codate.userAnswers = {};
			// $('.col-xs-3.input-panel').remove();
			// $('.col-xs-3.vote-panel').remove();
			fIObj.commun.showCodateView(fIObj, action);
			fIObj.commun.bindCodateEvent(fIObj);
		},
		completeRefreshCodate: function (fIObj) {
			if (fIObj.codate.allEvents) {
				fIObj.codate.allEvents.forEach(value => {
					fIObj.codate.selectedInputs.splice(fIObj.codate.selectedInputs.indexOf(value), 1);
					fIObj.commun.removeEvent(fIObj, value)
				});
				fIObj.codate.allEvents.length = 0;
				delete fIObj.codate.selectedInputs;
			}
			if (fIObj.codate.results['global']) delete fIObj.codate.results['global'];
			if (fIObj.codate.results['detailed']) delete fIObj.codate.results['detailed'];
			fIObj.codate.userAnswers = {};
			// $('.col-xs-3.input-panel').remove();
			// $('.col-xs-3.vote-panel').remove();
			fIObj.commun.showCodateView(fIObj);
			fIObj.commun.bindCodateEvent(fIObj);
		},
		resetCalendar: function (fIObj) {
			if (fIObj.codate.allEvents) {
				fIObj.codate.allEvents.forEach(value => {
					fIObj.codate.selectedInputs.splice(fIObj.codate.selectedInputs.indexOf(value), 1);
					fIObj.commun.removeEvent(fIObj, value)
				});
				fIObj.codate.allEvents.length = 0;
				// delete fIObj.codate.selectedInputs;
			}
			if (fIObj.codate.results['global']) delete fIObj.codate.results['global'];
			if (fIObj.codate.results['detailed']) delete fIObj.codate.results['detailed'];
			fIObj.codate.userAnswers = {}
		},
		bindCodateEvent: function (fIObj) {
			$(".showCodateResult").off().on('click', function () {
				$(fIObj.container + " " + fIObj.calendarContainer + " .fc-view-container").hide();
				$(fIObj.container + " " + fIObj.calendarContainer + " .fc-toolbar.fc-header-toolbar").hide();
				$(fIObj.container + " " + fIObj.calendarContainer + " .fc-toolbar.fc-footer-toolbar").hide();
				$(fIObj.container + " #codate-result-container").remove();
				$('.col-xs-3.ins-card.codate-panel.vote-panel').remove();
				const codParentForm = $(this).attr("data-parentform");
				const codKunik = $(this).attr("data-kunik");
				const codInputKey = $(this).attr("data-inputkey");
				var codInputValue = JSON.parse($(this).attr("data-inputvalue").replace(/'/g, '\"'));
				const codParamsData = JSON.parse($(this).attr("data-paramsdata").replace(/'/g, '\"'));
				codInputValue['subFormId'] = $(this).attr("data-subformid");
				$(fIObj.container + ' ' + fIObj.calendarContainer + " .fc-toolbar.fc-header-toolbar").before(`
					<div id="codate-result-container" class="">
						<button type="button" class="result-close-button close" style="font-size: 30px !important" aria-hidden="true">×</button>
						<h4>${trad.result + ' ' + codInputValue.label}</h4>
						<div id="cod-result-container" class="col-xs-12" style="overflow-x: auto">
							<div id="codate-result"></div>
						</div>
					</div>
				`);
				$("#codate-result").html("<center><i class='fa fa-spin fa-refresh margin-top-50 fa-2x'></i></center>");
				ajaxPost(
					'#codate-result',
					baseUrl + '/co2/app/getcodateresult',
					{
						"parentForm": { "_id": codParentForm },
						"codatekunik": codKunik,
						"inputKey": codInputKey,
						"inputValue": codInputValue,
						"paramsData": codParamsData,
						"showAllResult": fIObj.codate.insIsAdmin
					},
					function (res) {
						fIObj.commun.bindCodateEvent(fIObj);
					}
				);

				// bind event
				$('.result-close-button').off().on('click', function () {
					$(fIObj.container + " " + fIObj.calendarContainer + " .fc-view-container").show();
					$(fIObj.container + " " + fIObj.calendarContainer + " .fc-toolbar.fc-header-toolbar").show();
					$(fIObj.container + " " + fIObj.calendarContainer + " .fc-toolbar.fc-footer-toolbar").show();
					const fullCalendarView = $('' + fIObj.container + ' ' + fIObj.calendarContainer).fullCalendar('getView');
					if (fullCalendarView.name == 'month') {
						$(fIObj.container).append(`
							<div class="col-xs-3 ins-card codate-panel vote-panel">
								<div class="title ins-card-header text-center">${new Date().toLocaleString("fr", { weekday: "long", day: "numeric", month: "long", year: "numeric" })}</div>
								<div class="times-content ins-card-body col-xs-12">
								</div>
							</div>
						`);
					}
					$("#codate-result-container").remove();
				})
			});
			$(".createEvent").off().on("click", function (event) {
				// dyFObj.openForm('event')
				var self = this;
				event.stopImmediatePropagation();
				const addHours = function (hours, date = new Date()) {
					const dateCopy = new Date(date.getTime());
					dateCopy.setTime(dateCopy.getTime() + hours * 60 * 60 * 1000);
					return dateCopy != "Invalid Date" ? dateCopy.toLocaleString('fr', { day: 'numeric', month: 'numeric', year: 'numeric', hour: 'numeric', minute: 'numeric' }) : null;
				}
				var startDate = `${$(self).attr("data-date")} ${$(self).attr("data-time")}`;
				const [d, m, y] = $(self).attr("data-date").split("/");
				const sDate = new Date(`${y}-${m}-${d} ${$(self).attr("data-time")}`);
				var endDate = addHours($(self).attr("data-eventduration"), sDate);//`${$(self).attr("data-date")} 23:59`;
				const eDate = endDate.indexOf(":") > -1 ? endDate.substring(0, endDate.indexOf(":") - 2) : '';
				const eTime = endDate.indexOf(":") > -1 ? endDate.substring(endDate.indexOf(":") - 2) : '';
				bootbox.dialog({
					title: trad.confirm,
					message: `<span class='text-success bold'><i class='fa fa-info'></i> ${tradForm.wouldYouLikeToCreateTheEvent} ${$(self).attr("data-titleQuestion")}<br><span class="text-black">${trad.fromdate} ${$(self).attr("data-date")} ${trad.to} ${$(self).attr("data-time")} ${trad.until} ${eDate} ${trad.to} ${eTime}</span> </span>`,
					buttons: [
						{
							label: "Ok",
							className: "btn btn-primary pull-left",
							callback: function () {
								const oneParent = {
									[typeof contextId != 'undefined' ? contextId : fIObj.codate.contextId]: {
										name: notNull(contextData) ? contextData.name : fIObj.codate.contextData.name,
										type: notNull(contextData) ? contextData.type : fIObj.codate.contextData.type
									}
								}
								const [day, month, year] = $(self).attr("data-date") ? $(self).attr("data-date").split("/") : null;
								let dataToSend = {
									id: '',
									name: $(self).attr("data-titleQuestion"),
									type: 'workshop',
									organizer: oneParent,
									startDate: startDate,
									endDate: endDate,
									collection: "events",
									key: "event",
									public: true,
									timeZone: Intl.DateTimeFormat().resolvedOptions().timeZone,
									preferences: {
										isOpenData: true,
										isOpenEdition: true
									},
								};
								// mylog.log("create event to send", dataToSend);

								ajaxPost(
									null,
									baseUrl + "/co2/search/globalautocomplete",
									{
										name: $(self).attr("data-titleQuestion"),
										searchType: 'events',
										indexMin: 0,
										indexMax: 50
									},
									function (res) {
										if (res) {
											if (Object.values(res.results).length > 0) {
												toastr.warning(trad.checkItemAlreadyExist);
											} else {
												ajaxPost(null, baseUrl + "/co2/element/save", dataToSend, function (data) {
													if (data.result) {
														toastr.success(tradForm.modificationSave);
														bootbox.dialog({
															title: trad.confirm,
															message: `<span class='text-danger bold'>
																		<i class='fa fa-info'></i> 
																		voulez-vous archiver ${$(self).attr("data-titleQuestion")}<br>
																		<span class="text-black">Cette action va modifier le status du formulaire parent; ça veut dire que s'il y a d'autres codates dans ce formulaire, ils seront archivés aussi.</span> 
																	</span>`,
															buttons: [
																{
																	label: "Archiver",
																	className: "btn btn-primary pull-left",
																	callback: function () {
																		if (userId) {
																			if (fIObj.codateIsAdmin) {
																				let dataToSend = {
																					id: $(self).attr("data-formid"),
																					collection: "forms",
																					path: "inArchive",
																					value: true
																				}

																				if (typeof dataToSend.value == "undefined") {
																					toastr.error('value cannot be empty!');
																				} else {
																					dataHelper.path2Value(dataToSend, function (params) {
																						toastr.success("Codate archivé avec succès");
																						fIObj.commun.completeRefreshCodate(fIObj)
																					});
																				}
																			}
																		}
																	}
																},
																{
																	label: "Annuler",
																	className: "btn btn-default pull-left",
																	callback: function () { }
																}
															]
														})
													}
												}, "json");
											}
										} else {
											toastr.error(trad["somethingwentwrong"]);
										}
									}
								)
								// ajaxPost(null, baseUrl+"/co2/element/save", dataToSend, function(data) {mylog.log(data)}, "json");
								// dataHelper.path2Value(dataToSend, function() {

								// });
								// mylog.log("data create event to send", dataToSend)
							}
						},
						{
							label: "Annuler",
							className: "btn btn-default pull-left",
							callback: function () { }
						}
					]
				});
			});

			$('.archiveCodate').off().on('click', function () {
				const self = this;
				bootbox.dialog({
					title: trad.confirm,
					message: `<span class='text-danger bold'>
								<i class='fa fa-info'></i> 
								voulez-vous archiver ${$(self).attr("data-titleQuestion")}<br>
								<span class="text-black">Cette action va modifier le status du formulaire parent; ça veut dire que s'il y a d'autres codates dans ce formulaire, ils seront archivés aussi.</span> 
							</span>`,
					buttons: [
						{
							label: "Archiver",
							className: "btn btn-primary pull-left",
							callback: function () {
								if (userId) {
									if (fIObj.codateIsAdmin) {
										let dataToSend = {
											id: $(self).attr("data-formid"),
											collection: "forms",
											path: "inArchive",
											value: true
										}

										if (typeof dataToSend.value == "undefined") {
											toastr.error('value cannot be empty!');
										} else {
											dataHelper.path2Value(dataToSend, function (params) {
												toastr.success("Codate archivé avec succès");
												fIObj.commun.completeRefreshCodate(fIObj)
											});
										}
									}
								}
							}
						},
						{
							label: "Annuler",
							className: "btn btn-default pull-left",
							callback: function () { }
						}
					]
				})
			});
			$('.deleteCodateForm').off().on('click', function () {
				var idF = $(this).attr("data-id");
				if (userId && fIObj.codateIsAdmin) {
					bootbox.dialog({
						title: trad.confirmdelete,
						message: "<span class='text-red bold'><i class='fa fa-warning'></i> " + trad.actionirreversible + "</span>",
						buttons: [
							{
								label: "Supprimer",
								className: "btn btn-primary pull-left",
								callback: function () {
									getAjax("", baseUrl + "/survey/form/delete/id/" + idF, function (res) {
										if (res.result) {
											toastr.success(res.msg);
											fIObj.commun.completeRefreshCodate(fIObj)
										}
										else
											toastr.error(res.msg);
									}, "html");
								}
							},
							{
								label: trad.cancel,
								className: "btn btn-default pull-left",
								callback: function () { }
							}
						]
					});
				}
			});
		},
		bindTimeEvent: function (fIObj) {
			$(fIObj.container + " .codate-panel.vote-panel .times-content label.dropdown").hover(delay(function () {
				$(this).find('.dropdown-menu').first().stop(true, true).slideDown(150);
			}, 350), function () {
				$(this).find('.dropdown-menu').first().stop(true, true).slideUp(105)
			});
			$(fIObj.container + " input[name='codateTime']").on('change', function (event) {
				event.stopImmediatePropagation();
				const inputId = $(this).attr("data-inputid");
				const dateKey = $(this).attr("data-date");
				const formId = $(this).attr("data-formid");
				const subForm = $(this).attr("data-subform");
				const val = $(this).val();
				const times = $(this).attr("data-times").split(",");
				const title = $(this).attr("data-title");
				const [d, m, y] = dateKey.split('/');
				const newdateKey = new Date(`${y}-${m}-${d}`)
				const answer = fIObj.commun.checkIfExistAnswer(fIObj, fIObj.codate.allData[formId]['answers'], userId);
				var thisself = this;
				$(this).parent().parent().css({ 'cssText': `display: none !important; transition: all .3s ease` });
				$(thisself).parent().parent().after(`<div class='col-xs-12 changeloading text-center d-flex justify-content-center'><i class="fa fa-spin fa-refresh"></i></div>`)
				mylog.log("parent parent", fIObj.codate.userAnswers)
				if ($(this).is(":checked")) {
					if (fIObj.codate.userAnswers[inputId]) {
						if (fIObj.codate.userAnswers[inputId][dateKey]) {
							if (fIObj.codate.userAnswers[inputId][dateKey].indexOf(val) > -1) {
								/* var formData={
									"id": Object.keys(answer)[0],
									"collection": "answers",
									"path":"answers."+subForm+"."+inputId+"."+dateKey+"."+answer[dateKey].indexOf(val),
									"pull":"answers."+subForm+"."+inputId+"."+dateKey,
									"value":null
								};      
								dataHelper.path2Value( formData, function(params) {
									toastr.success("Réponse modifié avec succès");
								}); */
							} else {
								let dataToSend = {
									id: fIObj.codate.answers[inputId].id,
									collection: "answers",
									path: "answers." + subForm + "." + inputId + "." + dateKey,
									arrayForm: true,
									value: val
								}

								if (typeof dataToSend.value == "undefined") {
									toastr.error('value cannot be empty!');
								} else {
									dataHelper.path2Value(dataToSend, function (params) {
										// ajaxPost("",baseUrl+'/survey/form/pingupdate/answerId/'+ answerId +'/actionVerb/refresh-codate-answer',{},function(data){});
										toastr.success("Réponse modifié avec succès");
										fIObj.commun.refreshCodate(fIObj, 'dontremove');
										fIObj.commun.buildTimeList(fIObj, inputId, formId, subForm, newdateKey, times, title)
										fIObj.commun.bindTimeEvent(fIObj);
										$(thisself).parent().parent().css({ 'cssText': `display: flex !important; transition: all .3s ease` });
										$('.changeloading').remove()
									});
								}
								// alert("ajouterNa ito time valeur ito")
							}
						} else {
							let dataToSend = {
								id: fIObj.codate.answers[inputId].id,
								collection: "answers",
								path: "answers." + subForm + "." + inputId + "." + dateKey,
								arrayForm: true,
								value: val
							}

							if (typeof dataToSend.value == "undefined") {
								toastr.error('value cannot be empty!');
							} else {
								dataHelper.path2Value(dataToSend, function (params) {
									// ajaxPost("",baseUrl+'/survey/form/pingupdate/answerId/'+ answerId +'/actionVerb/refresh-codate-answer',{},function(data){});
									toastr.success("Réponse modifié avec succès");
									fIObj.commun.refreshCodate(fIObj, 'dontremove');
									fIObj.commun.buildTimeList(fIObj, inputId, formId, subForm, newdateKey, times, title);
									fIObj.commun.bindTimeEvent(fIObj);
									$(thisself).parent().parent().css({ 'cssText': `display: flex !important; transition: all .3s ease` });
									$('.changeloading').remove()
								});
							}
							// alert("ajouterNa ito date ito")
						}
					} else {
						if (answer != null) {
							let dataToSend = {
								id: Object.keys(answer)[0],
								collection: "answers",
								path: "answers." + subForm + "." + inputId + "." + dateKey,
								arrayForm: true,
								value: val
							}

							if (typeof dataToSend.value == "undefined") {
								toastr.error('value cannot be empty!');
							} else {
								dataHelper.path2Value(dataToSend, function (params) {
									// ajaxPost("",baseUrl+'/survey/form/pingupdate/answerId/'+ answerId +'/actionVerb/refresh-codate-answer',{},function(data){});
									toastr.success("Réponse modifié avec succès");
									fIObj.commun.refreshCodate(fIObj, 'dontremove');
									fIObj.commun.buildTimeList(fIObj, inputId, formId, subForm, newdateKey, times, title);
									fIObj.commun.bindTimeEvent(fIObj);
									$(thisself).parent().parent().css({ 'cssText': `display: flex !important; transition: all .3s ease` });
									$('.changeloading').remove()
								});
							}
							// alert("misy")
						} else {
							ajaxPost(
								null,
								baseUrl + `/survey/answer/newanswer/form/${formId}/contextId/${notNull(contextData) ? contextData.id : fIObj.codate.contextData.id}/contextType/${notNull(contextData) ? contextData.type : fIObj.codate.contextData.type}`,
								{
									"action": "new"
								},
								function (data) {
									if (data['_id']) {
										let dataToSend = {
											id: data['_id']['$id'],
											collection: "answers",
											path: "answers." + subForm + "." + inputId + "." + dateKey,
											arrayForm: true,
											value: val
										}

										dataHelper.path2Value(dataToSend, function (params) {
											// ajaxPost("",baseUrl+'/survey/form/pingupdate/answerId/'+ answerId +'/actionVerb/refresh-codate-answer',{},function(data){});
											toastr.success("Réponse ajouté avec succès");
											fIObj.commun.refreshCodate(fIObj, 'dontremove');
											fIObj.commun.buildTimeList(fIObj, inputId, formId, subForm, newdateKey, times, title);
											fIObj.commun.bindTimeEvent(fIObj);
											$(thisself).parent().parent().css({ 'cssText': `display: flex !important; transition: all .3s ease` });
											$('.changeloading').remove()
										});
									}
								}
							)
							// alert("tsy misy")
						}
					}
				} else {
					if (fIObj.codate.userAnswers[inputId]) {
						if (fIObj.codate.userAnswers[inputId][dateKey]) {
							if (fIObj.codate.userAnswers[inputId][dateKey].indexOf(val) > -1) {
								var formData = {
									"id": fIObj.codate.answers[inputId].id,
									"collection": "answers",
									"path": "answers." + subForm + "." + inputId + "." + dateKey + "." + fIObj.codate.userAnswers[inputId][dateKey].indexOf(val),
									"pull": "answers." + subForm + "." + inputId + "." + dateKey,
									"value": null
								};
								dataHelper.path2Value(formData, function (params) {
									// ajaxPost("",baseUrl+'/survey/form/pingupdate/answerId/'+ answerId +'/actionVerb/refresh-codate-answer',{},function(data){});
									toastr.success("Réponse modifié avec succès remove");
									fIObj.commun.refreshCodate(fIObj, 'dontremove');
									fIObj.commun.buildTimeList(fIObj, inputId, formId, subForm, newdateKey, times, title);
									fIObj.commun.bindTimeEvent(fIObj);
									$(thisself).parent().parent().css({ 'cssText': `display: flex !important; transition: all .3s ease` });
									$('.changeloading').remove()
								});
							}
						}
					}
				}
			});
		}
	},
	fixDayClick: function (fIObj) {
		//bug dayclick
		const scrollerDayGrid = $(fIObj.container + ' ' + fIObj.calendarContainer + ' .fc-scroller.fc-day-grid-container');
		const fullCalendarName = $(fIObj.container + ' ' + fIObj.calendarContainer).fullCalendar('getView');
		if (fullCalendarName.name == 'month')
			scrollerDayGrid.css({ 'cssText': `overflow: 'inherit'; height: ${scrollerDayGrid.css('height')};` });
	},
	events: [],
	tabOrganiser: [],
	showPopup: false,

	//dateToShow, fIObj, $eventDetail, eventClass, eventCategory,
	widgetNotes: $('#notes .e-slider'),
	sliderNotes: $('#readNote .e-slider'),

	bindEventCalendar: function (fIObj) {
		var fIObj = this;
		if (userId && !fIObj.codate.initialized) {
			fIObj.commun.reloadView(fIObj);
			fIObj.commun.showCodateView(fIObj);
			fIObj.commun.bindCodateEvent(fIObj);
			fIObj.codate.initialized = true;
		}
		$(".fc-today").addClass(".activeDay");
		mylog.log("calendarObj.bindEventCalendar");
		//var popoverElement;
		$(fIObj.container + ' .fc-today-button').on('click', function (event) {
			event.stopImmediatePropagation();
			if (userId) {
				if ($('input[name="createCodate"][value="vote"]').is(":checked") || notNull(fIObj.codate.contextId)) {
					fIObj.commun.renderCodateEvent(fIObj)
				}
			}
		});
		$(fIObj.container + ' .fc-prev-button').click(function (event) {
			event.stopImmediatePropagation();
			var now = $(fIObj.container + ' ' + fIObj.calendarContainer).fullCalendar('getDate');
			mylog.log("calendarObj.bindEventCalendar .fc-next-button now", now, now.format());
			fIObj.params.starDate = moment(now.format()).valueOf();

			// fIObj.searchInAgenda(fIObj, now.format(), "month");
			if (userId) {
				if ($('input[name="createCodate"][value="vote"]').is(":checked") || notNull(fIObj.codate.contextId)) {
					fIObj.commun.renderCodateEvent(fIObj)
				}
			}
		});

		$(fIObj.container + ' .fc-next-button').click(function (event) {
			event.stopImmediatePropagation();
			var now = $(fIObj.container + ' ' + fIObj.calendarContainer).fullCalendar('getDate');

			fIObj.params.starDate = moment(now.format()).valueOf();

			// fIObj.searchInAgenda(fIObj, now.format(), "month");
			if (userId) {
				if ($('input[name="createCodate"][value="vote"]').is(":checked") || notNull(fIObj.codate.contextId)) {
					fIObj.commun.renderCodateEvent(fIObj)
				}
			}
		});
		$(fIObj.container + ' .fc-agendaWeek-button').on('click', function (e) {
			e.stopImmediatePropagation();
			if (userId) {
				const fullCalendarView = $(fIObj.container + ' ' + fIObj.calendarContainer).fullCalendar('getView');
				if ($('input[name="createCodate"][value="vote"]').is(":checked") || notNull(fIObj.codate.contextId)) {
					fIObj.commun.switchView(fIObj, fullCalendarView.name, true)
				}
			}
		});
		$(fIObj.container + ' .fc-month-button').on('click', function (e) {
			e.stopImmediatePropagation();
			if (userId) {
				const fullCalendarView = $(fIObj.container + ' ' + fIObj.calendarContainer).fullCalendar('getView');
				if ($('input[name="createCodate"][value="vote"]').is(":checked") || notNull(fIObj.codate.contextId)) {
					fIObj.commun.switchView(fIObj, fullCalendarView.name)
				}
			}
		})
		/* $('.popover').mouseenter(function(){
			$(this).hide();
		}); */
		$('body').on('click', function (e) {
			if (typeof popoverElement != "undefined" && popoverElement
				&& ((!popoverElement.is(e.target) && popoverElement.has(e.target).length === 0 && $('.popover').has(e.target).length === 0)
					|| (popoverElement.has(e.target) && e.target.id === 'closepopover'))) {
				fIObj.closePopovers();
			}
		});
		$(fIObj.container + ` input[type="radio"][name="createCodate"]`).on('change', function (event) {
			event.stopImmediatePropagation();
			if ($(this).val() == 'create') {
				fIObj.commun.resetCalendar(fIObj);
				/* toastr.info(
					`<span>- Veillez choisir une periode pour créer un sondage de date.</span> <br>
					 <span>- Et une fois la période est sélectionnée, configurer le codate en cliquant sur le bouton configuer codate</span>`,
					'Notice',
					{
						closeButton: true,
						preventDuplicates: true,
						timeOut: 5000,
						positionClass: 'toast-top-center'
					}
				); */
			} else {
				fIObj.commun.reloadView(fIObj);
				fIObj.commun.showCodateView(fIObj);
			}
		})
		$(fIObj.container + ` a.create-codate.codate-create-button`).on('click', function (event) {
			event.stopImmediatePropagation();
			if (userId) {
				mylog.log("fIobj click", fIObj);
				if (fIObj.codateIsAdmin) {
					fIObj.commun.codateFormOnly(fIObj, function (data) {
						if (typeof data.parent != "undefined") {
							fIObj.commun.generateFormOnly(fIObj, data.parent, [{
								id: "sondageDate",
								label: data.title != '' ? data.title : 'Enquête par date',
								type: "tpls.forms.cplx.sondageDate"
							}])
						} else {
							bootbox.alert({ message: trad["Choose context"] });
						}
					})
				} else {
					toastr.info('You are not admin')
				}
			} else {
				toastr.info('Veillez vous connecter')
				$(this).prop('checked', false)
			}
		})
		$(document).on('click', fIObj.container + ' .codate-vote-button', function (event) {
			event.stopImmediatePropagation();
			if (userId) {
				fIObj.commun.reloadView(fIObj);
				fIObj.commun.showCodateView(fIObj);
			} else {
				toastr.info('Veillez vous connecter')
				$("#codateVote").attr('checked', false)
			}
		});
		$(document).on('click', fIObj.container + ' .codate-create-button', function (event) {
			event.stopImmediatePropagation();
			if (!userId) {
				$("#createCodate").attr('checked', false);
			} else {
				fIObj.commun.resetCalendar(fIObj);
				toastr.info(
					`<span>- Veillez choisir une periode pour créer un sondage de date.</span> <br>
					 <span>- Et une fois la période est sélectionnée, configurer le codate en cliquant sur le bouton configuer codate</span>`,
					'Notice',
					{
						closeButton: true,
						preventDuplicates: true,
						timeOut: 5000,
						positionClass: 'toast-top-center'
					}
				);
			}
		});
		$(document).on('click', ".btnCreateCodate", function (event) {
			event.stopImmediatePropagation();
			if (userId) {
				mylog.log("fIobj click", fIObj);
				fIObj.commun.codateForm(fIObj, function (data) {
					if (typeof data.parent != "undefined") {
						fIObj.commun.generateForm(fIObj, data.parent, [{
							id: "sondageDate",
							label: data.title != '' ? data.title : 'Enquête par date',
							type: "tpls.forms.cplx.sondageDate"
						}], data.inside)
					} else {
						bootbox.alert({ message: trad["Choose context"] });
					}
				})
			} else {
				toastr.info('Veillez vous connecter')
				$(this).prop('checked', false)
			}
		});
		$(document).on('click', fIObj.container + " .btnConfigureCodate", function (event) {
			event.stopImmediatePropagation();
			if (userId) {
				mylog.log("fIobj click", fIObj);
				var configParams = {
					title: $(this).attr("data-title"),
					keyUnik: $(this).attr("data-keyunik"),
					parentFormId: $(this).attr("data-parentformid")
				};
				fIObj.commun.configureCodateForm(fIObj, configParams, [], {}, function (data) {
					if (typeof data.inside != "undefined") {
						fIObj.commun.saveCodateConfig(fIObj, configParams.keyUnik, configParams.parentFormId, data.inside)
					} else {
						bootbox.alert({ message: 'Value cannot empty' });
					}
				})
			} else {
				toastr.info('Veillez vous connecter')
				$(this).prop('checked', false)
			}
		});
		$(document).on('click', fIObj.container + " .editConfigCodate", function (event) {
			event.stopImmediatePropagation();
			if (userId) {
				mylog.log("fIobj click", fIObj);
				var configParams = {
					title: $(this).attr("data-title"),
					keyUnik: $(this).attr("data-keyunik"),
					parentFormId: $(this).attr("data-parentformid")
				};
				var dateRange = $(this).attr("data-daterange").split(",");
				var timeRange = JSON.parse($(this).attr("data-timerange").replace(/'/g, '\"'));
				fIObj.codate.selectedDates.forEach(elem => {
					const elemArr = elem.split("/");
					if (dateRange.indexOf(elem) > -1) {
						$(`.fc-day[data-date='${elemArr[2]}-${elemArr[1]}-${elemArr[0]}']`).removeClass('activeDay selectedDay');
						fIObj.codate.selectedDates.splice(fIObj.codate.selectedDates.indexOf(elem), 1);
					}
				});
				fIObj.commun.configureCodateForm(fIObj, configParams, dateRange, timeRange, function (data) {
					if (typeof data.inside != "undefined") {
						fIObj.commun.saveCodateConfig(fIObj, configParams.keyUnik, configParams.parentFormId, data.inside)
					} else {
						bootbox.alert({ message: 'Value cannot empty' });
					}
				})
			} else {
				toastr.info('Veillez vous connecter')
				$(this).prop('checked', false)
			}
		});
		$(document).on('click', fIObj.container + " .renameCodate", function (event) {
			event.stopImmediatePropagation();
			if (userId) {
				mylog.log("fIobj click", fIObj);
				var configParams = {
					title: $(this).attr("data-title"),
					keyUnik: $(this).attr("data-keyunik"),
					parentFormId: $(this).attr("data-parentformid"),
					subFormId: $(this).attr("data-subformid"),
				};
				dyFObj.closeForm();
				const cdForm = {
					jsonSchema: {
						title: "Rennomer " + configParams.title,
						description: "",
						icon: "fa-pencil",
						properties: {
							label: {
								inputType: "text",
								label: "Nom du codate",
								value: configParams.title,
								rules: {
									required: true
								}
							}
						},
						onLoads: {
							onload: function () {

							}
						},
						beforeBuild: function () {
						},
						save: function (formData) {
							var today = new Date();
							delete formData.collection; delete formData.scope;
							if (formData && formData.label && formData.label.trim() != '') {
								dataHelper.path2Value(
									{
										id: configParams.subFormId,
										collection: "forms",
										path: "inputs." + configParams.keyUnik + ".label",
										value: formData.label.trim(),
										formParentId: configParams.parentFormId,
										key: ""
									}, function () {
										toastr.success("Codate rennomer avec succès");
										fIObj.commun.reloadView(fIObj);
										$('input[name="createCodate"][value="vote"]').prop("checked", true);
										// if($('.col-xs-3.input-panel').length == 0)
										// fIObj.commun.showCodateView(fIObj);
										// fIObj.commun.bindCodateEvent(fIObj);
										fIObj.commun.refreshCodate(fIObj);
										dyFObj.closeForm()
									}
								)
							} else {

							}
						}
					}
				}
				dyFObj.openForm(cdForm, null, fIObj.contextData, null, null, {
					type: "bootbox"
				});
			} else {
				toastr.info('Veillez vous connecter')
				$(this).prop('checked', false)
			}
		});
		$(document).on('click', fIObj.container + " .renameForm", function (event) {
			event.stopImmediatePropagation();
			if (userId) {
				mylog.log("fIobj click", fIObj);
				var configParams = {
					inputTitle: $(this).attr("data-title"),
					formTitle: $(this).attr("data-formtitle"),
					keyUnik: $(this).attr("data-keyunik"),
					parentFormId: $(this).attr("data-parentformid"),
				};
				dyFObj.closeForm();
				const cdForm = {
					jsonSchema: {
						title: "Rennomer le formulaire du " + configParams.inputTitle,
						description: "",
						icon: "fa-pencil",
						properties: {
							label: {
								inputType: "text",
								label: "Nom du formulaire",
								value: configParams.formTitle,
								rules: {
									required: true
								}
							}
						},
						onLoads: {
							onload: function () {

							}
						},
						beforeBuild: function () {
						},
						save: function (formData) {
							var today = new Date();
							delete formData.collection; delete formData.scope;
							if (formData && formData.label && formData.label.trim() != '') {
								dataHelper.path2Value(
									{
										id: configParams.parentFormId,
										collection: "forms",
										path: "name",
										value: formData.label.trim(),
										formParentId: configParams.parentFormId,
										key: ""
									}, function () {
										toastr.success("Formulaire rennomer avec succès");
										fIObj.commun.reloadView(fIObj);
										$('input[name="createCodate"][value="vote"]').prop("checked", true);
										// if($('.col-xs-3.input-panel').length == 0)
										// fIObj.commun.showCodateView(fIObj);
										// fIObj.commun.bindCodateEvent(fIObj);
										fIObj.commun.refreshCodate(fIObj);
										dyFObj.closeForm()
									}
								)
							} else {

							}
						}
					}
				}
				dyFObj.openForm(cdForm, null, fIObj.contextData, null, null, {
					type: "bootbox"
				});
			} else {
				toastr.info('Veillez vous connecter')
				$(this).prop('checked', false)
			}
		});
		$(document).on('click', fIObj.container + " .sendInvitation", function (event) {
			event.stopImmediatePropagation();
			var mailParams = {
				formTitle: $(this).attr("data-title"),
				formId: $(this).attr("data-formid"),
				inputId: $(this).attr("data-keyunik"),
			};
			if (userId) {
				fIObj.commun.selectUserForm(fIObj, mailParams)
			} else {
				toastr.info('Veillez vous connecter')
				$(this).prop('checked', false)
			}
		});
		lazyLoad(baseUrl + '/plugins/clipboard/clipboard.min.js', '', function () {
			$(document).on('click', fIObj.container + ' .copyCodateLink', function () {
				var clipboard = new ClipboardJS('.copyCodateLink');
				clipboard.on('success', function (e) {
					e.clearSelection();
					toastr.success(trad.copy);
				});

				clipboard.on('error', function (e) {
				});
			})
		})
		$(document).on('change', fIObj.container + " input[name='codInput']", function (event) {
			event.stopImmediatePropagation();
			const inputId = $(this).attr('id');
			var selfInput = this;
			mylog.log("selected inputs", fIObj.codate.selectedInputs, fIObj.codate.allEvents)
			if (fIObj.codate.allEvents) {
				fIObj.codate.allEvents.forEach(value => {
					fIObj.codate.selectedInputs.splice(fIObj.codate.selectedInputs.indexOf(value), 1);
					fIObj.commun.removeEvent(fIObj, value)
				});
				fIObj.codate.allEvents.length = 0;
			}
			$('.w-70.ins-checkmark').css({ 'background-color': '#fff' });
			$(fIObj.container + " input[name='codInput']").each(function () {
				if (this != selfInput) {
					$(this).prop('checked', false);
				}
			})
			if ($(this).is(":checked")) {
				$(selfInput).next().css({ 'background-color': $(selfInput).next().css('border-color') })
				if (fIObj.codate.selectedInputs) {
					if (fIObj.codate.selectedInputs.indexOf(inputId) < 0) {
						if (fIObj.codate.allEvents.indexOf(inputId) < 0) fIObj.codate.allEvents.push(inputId);
						fIObj.codate.selectedInputs.push(inputId);
						var dates = $(this).attr("data-dates").split(",");
						var viewChanged = false;
						const fullCalendarView = $(fIObj.container + ' ' + fIObj.calendarContainer).fullCalendar('getView');
						dates.forEach((dV, dK) => {
							const dateArr = dV.split('/');
							var startDate = moment(`${dateArr[2]}-${dateArr[1]}-${dateArr[0]}`);
							if (fullCalendarView.name == 'month') {
								const calendarMonth = $(fIObj.container + ' ' + fIObj.calendarContainer).fullCalendar("getDate").month();
								if (dK == 0) {
									if (new Date(startDate.format('YYYY-MM-DD')) < new Date($($('.fc-day-top')[0]).attr('data-date')) || new Date(startDate.format('YYYY-MM-DD')) > new Date($($('.fc-day-top')[$('.fc-day-top').length - 1]).attr('data-date'))) {
										fIObj.commun.gotoDate(fIObj, startDate);
										viewChanged = true
									}
								}
								if (viewChanged == false) {
									var toCheck = false;
									var nbVote = 0;
									mylog.log('verif results', $(selfInput).attr("data-subform"), inputId, fIObj.codate.results['global'])
									if (fIObj.codate.results['global'][$(selfInput).attr("data-subform")][inputId]) {
										const dateKeysSorted = Object.keys(fIObj.codate.results['global'][$(selfInput).attr("data-subform")][inputId]);
										nbVote = fIObj.codate.results['global'][$(selfInput).attr("data-subform")][inputId][dV] ? fIObj.codate.results['global'][$(selfInput).attr("data-subform")][inputId][dV] : 0;
										if (dateKeysSorted.indexOf(dV) == 0 && fIObj.codate.results['global'][$(selfInput).attr("data-subform")][inputId][dateKeysSorted[0]] != 0) {
											toCheck = true;
										}
										if (fIObj.codate.results['global'][$(selfInput).attr("data-subform")][inputId][dV] == fIObj.codate.results['global'][$(selfInput).attr("data-subform")][inputId][dateKeysSorted[0]] && fIObj.codate.results['global'][$(selfInput).attr("data-subform")][inputId][dateKeysSorted[0]] != 0) {
											toCheck = true;
										}
									}
									fIObj.commun.addEvent(
										fIObj,
										inputId,
										$(selfInput).attr("data-label"),
										startDate,
										$(selfInput).attr("data-codecolor"),
										$(selfInput).attr("data-dateTime"),
										$(selfInput).attr("data-formid"),
										$(selfInput).attr("data-subform"),
										true,
										null,
										toCheck,
										nbVote
									);
								}
							} else {
								const calendarDate = $(fIObj.container + ' ' + fIObj.calendarContainer).fullCalendar("getDate");
								const weekStart = calendarDate.startOf('week');
								const weekEnd = calendarDate.endOf('week');
								const times = JSON.parse($(selfInput).attr("data-dateTime").replace(/'/g, '\"'))[dV] ? JSON.parse($(selfInput).attr("data-dateTime").replace(/'/g, '\"'))[dV] : []

								if (dK == 0) {
									mylog.log('comparaison', calendarDate, startDate)
									if (new Date(startDate.format('YYYY-MM-DD')) < new Date($($('.fc-day-header')[0]).attr('data-date')) || new Date(startDate.format('YYYY-MM-DD')) > new Date($($('.fc-day-header')[$('.fc-day-header').length - 1]).attr('data-date'))) {
										fIObj.commun.gotoDate(fIObj, startDate);
										viewChanged = true
									}
								}
								if (viewChanged == false) {
									times.forEach((elem) => {
										var toCheck = false;
										var nbVote = 0;
										if (fIObj.codate.results['detailed'][$(selfInput).attr("data-subform")][inputId]) {
											const dateKeysSorted = Object.keys(fIObj.codate.results['detailed'][$(selfInput).attr("data-subform")][inputId]);
											if (dateKeysSorted.indexOf(dV + '-' + elem) == 0 && fIObj.codate.results['detailed'][$(selfInput).attr("data-subform")][inputId][dateKeysSorted[0]] != 0) {
												toCheck = true;
											}
											if (fIObj.codate.results['detailed'][$(selfInput).attr("data-subform")][inputId][dV + '-' + elem] == fIObj.codate.results['detailed'][$(selfInput).attr("data-subform")][inputId][dateKeysSorted[0]] && fIObj.codate.results['detailed'][$(selfInput).attr("data-subform")][inputId][dateKeysSorted[0]] != 0) {
												toCheck = true;
											}
											nbVote = fIObj.codate.results['detailed'][$(selfInput).attr("data-subform")][inputId][dV + '-' + elem] ? fIObj.codate.results['detailed'][$(selfInput).attr("data-subform")][inputId][dV - +'-' + elem] : 0;
										}
										fIObj.commun.addEvent(
											fIObj,
											inputId,
											$(selfInput).attr("data-label"),
											moment(`${dateArr[2]}-${dateArr[1]}-${dateArr[0]} ` + elem),
											$(selfInput).attr("data-codecolor"),
											$(selfInput).attr("data-dateTime"),
											$(selfInput).attr("data-formid"),
											$(selfInput).attr("data-subform"),
											false,
											moment(`${dateArr[2]}-${dateArr[1]}-${dateArr[0]} ` + elem.split(":")[0] + ':59'),
											toCheck,
											nbVote
										);
									})
								}
							}
						})
					}
				}
			} else {
				if (fIObj.codate.selectedInputs) {
					if (fIObj.codate.allEvents.indexOf(id) > -1) {
						fIObj.codate.allEvents.splice(
							fIObj.codate.allEvents.indexOf(id),
							1
						);
					}
					if (fIObj.codate.selectedInputs.indexOf(inputId) > -1) {
						fIObj.codate.selectedInputs.splice(
							fIObj.codate.selectedInputs.indexOf(inputId),
							1
						);
						fIObj.commun.removeEvent(fIObj, inputId)
					}
				}
			}
		});
		$(document).on('click', fIObj.container + " label.ins-cpl", function (e) {
			e.stopImmediatePropagation();
			if ($("#codate-result-container").is(":visible")) {
				$(fIObj.container + " " + fIObj.calendarContainer + " .fc-view-container").show();
				$(fIObj.container + " " + fIObj.calendarContainer + " .fc-toolbar.fc-header-toolbar").show();
				$(fIObj.container + " " + fIObj.calendarContainer + " .fc-toolbar.fc-footer-toolbar").show();
				const fullCalendarView = $(fIObj.container + ' ' + fIObj.calendarContainer).fullCalendar('getView');
				if (fullCalendarView.name == 'month') {
					$(fIObj.container).append(`
						<div class="col-xs-3 ins-card codate-panel vote-panel">
							<div class="title ins-card-header text-center">${new Date().toLocaleString("fr", { weekday: "long", day: "numeric", month: "long", year: "numeric" })}</div>
							<div class="times-content ins-card-body col-xs-12">
							</div>
						</div>
					`);
				}
				$("#codate-result-container").remove();
			}
		})
		$(document).on('click', fIObj.container + " .changeCheck", function (e) {
			e.stopImmediatePropagation();
			const elem = $("#" + $(this).attr("data-for"));
			parentElement = $(`a[data-for='${$(this).attr("data-for")}']`);
			if (elem.is(":checked")) {
				parentElement.addClass('ins-event-none-border');
				parentElement.removeClass('ins-event-border');
				parentElement.css({ 'border': '' });
				elem.prop("checked", false).trigger("change");
			} else {
				elem.prop("checked", true).trigger("change");
				parentElement.removeClass('ins-event-none-border');
				parentElement.addClass('ins-event-border');
			}
		});
		$(".cod-inside .dropdown-menu.cod-inside-menu ").mouseleave(function () {
			var removeClassInterval = setInterval(function () {
				if ($(".dropdown.cod-inside").hasClass("open")) {
					$(".dropdown.cod-inside").removeClass("open");
					clearInterval(removeClassInterval)
					removeClassInterval = null;
				}
			}, 200)
		});
		$(fIObj.container + ' .btnInviteCommunity').on('click', function (event) {
			event.stopImmediatePropagation();
			if (fIObj.codate.insIsAdmin) {
				ajaxPost(
					null,
					baseUrl + "/co2/app/invitecommunity",
					{
						"contextId": notNull(contextData) ? contextData.id : fIObj.codate.contextData.id,
						"contextType": notNull(contextData) ? contextData.type : fIObj.codate.contextData.type,
						"objectId": $(this).attr("data-formid"),
						"inputId": $(this).attr("data-keyunik"),
						"objectType": "forms"
					},
					function (res) {
						toastr.success(
							'Communauté invité avec succès',
							{
								closeButton: true,
								preventDuplicates: true,
								timeOut: 3000,
							}
						)
					},
				);
			} else {
				toastr.error(
					tradDynForm['You are not authorized'],
					{
						closeButton: true,
						preventDuplicates: true,
						timeOut: 3000,
					}
				)
			}
		});

	},
	templateRef: {
		"competition": "#ed553b",
		"concert": "#b45f04",
		"contest": "#ed553b",
		"exhibition": "#b45f04",
		"festival": "#b45f04",
		"getTogether": "#eb4124",
		"market": "#df01a5",
		"meeting": "#eb4124",
		"course": "#df01a5",
		"workshop": "#eb4124",
		"conference": "#0073b0",
		"debate": "#0073b0",
		"film": "#2e2e2e",
		"crowdfunding": "#93be3d",
		"others": "#93be3d",
	},
	closePopovers: function () {
		var fIObj = this;
		mylog.log("calendarObj.closePopovers ");
		fIObj.showPopup = false;
		$('.popover').not(this).popover('hide');
	},



	popupTemplate: function () {
		//mylog.log("calendarObj.popupTemplate ");
		var template = '<div class="popover" style="max-width:300px; no-padding" >' +
			'<div class="arrow"></div>' +
			'<div class="popover-header" style="background-color:red;">' +
			'<button id="closepopover" type="button" class="close margin-right-5" aria-hidden="true">&times;</button>' +
			'<h3 class="popover-title"></h3>' +
			'</div>' +
			'<div class="popover-content no-padding"></div>' +
			'</div>';
		return template;
	},
	popupHtml: function (fIObj, data) {
		//mylog.log("calendarObj.popupHtml ", data);
		var popupContent = "<div class='popup-calendar'>";

		var color = "orange";
		var ico = 'calendar';
		var imgProfilPath = assetPath + "/images/thumb/default_events.png";
		if (typeof data.profilMediumImageUrl !== "undefined" && data.profilMediumImageUrl != "")
			imgProfilPath = baseUrl + data.profilMediumImageUrl;
		var icons = '<i class="fa fa-' + ico + ' text-' + color + '"></i>!!!';

		var typeElement = "events";
		var icon = 'fa-calendar';

		var onclick = "";
		var url = '#page.type.' + typeElement + '.id.' + data.id;
		//onclick = 'fIObj.closePopovers();urlCtrl.loadByHash("'+url+'");';

		popupContent += "<div class='' id='popup" + data.id + "'>";
		popupContent += "<div class='main-panel'>"
			+ "<div class='col-md-12 col-sm-12 col-xs-12 no-padding'>"
			+ "<div class='thumbnail-profil' style='max-height: 200px;text-align: -webkit-center; overflow-y: hidden;background-color: #cccccc;'><img src='" + imgProfilPath + "' class='popup-info-profil-thumb img-responsive'></div>"
			+ "</div>"
			+ "<div class='col-md-12 col-sm-12 col-xs-12 padding-5'>";

		if ("undefined" != typeof data.title)
			popupContent += "<div class='' style='text-transform:uppercase;'>" + data.title + "</div>";

		if (data.start != null) {
			popupContent += "<div style='color:#777'>";
			var startLbl = "<i class='fa fa-calendar-o'></i> ";
			var startDate = moment(data.start).format("DD MMMM YYYY");
			var endDate = "";
			var hoursStr = "<br/>";
			if (data.allDay)
				hoursStr += tradDynForm.allday;
			else
				hoursStr += "<i class='fa fa-clock-o'></i> " + moment(data.start).format("H:mm");
			if (data.end != null) {
				if (startDate != moment(data.end).format("DD MMMM YYYY")) {
					startLbl += trad.fromdate + " ";
					endDate = " " + trad.todatemin + " " + moment(data.end).format("DD MMMM YYYY");
				}
				if (!data.allDay)
					hoursStr += " - " + moment(data.end).format("H:mm");
			}
			popupContent += startLbl + startDate + endDate + hoursStr;
			popupContent += "</div>";
		}
		popupContent += "</div>";
		//Short description
		if ("undefined" != typeof data['shortDescription'] && data['shortDescription'] != "" && data['shortDescription'] != null) {
			popupContent += "<div id='pop-description' class='popup-section'>"
				+ "<div class='popup-info-profil'>" + data['shortDescription'] + "</div>"
				+ "</div>";
		}
		popupContent += '</div>';

		popupContent += "<a href='" + url + "' target='_blank' onclick='" + onclick + "' class=''>";
		popupContent += '<div class="btn btn-sm btn-more col-md-12 col-sm-12 col-xs-12"><i class="fa fa-hand-pointer-o"></i> en savoir +</div>';
		popupContent += '</a>';

		return popupContent;
	},
	searchInAgenda: function (fIObj, date, format) {
		mylog.log("calendarObj.searchInAgenda", date, format);
		var today = new Date();
		var start = new Date();
		//     toTimestamp(strDate){
		var stringDate = new Date(date);
		var labelStr;
		/*if(format=="day"){
			fIObj.search.startDate=(Date.parse(date)/1000);
			fIObj.search.endDate=(Date.parse(date+" 23:59:59")/1000);

			if(date==today){
				labelStr=trad.today;
			}else{
				labelStr=directory.getWeekDayName(new Date(date).getDay())+" "+stringDate.getDate()+' '+directory.getMonthName(stringDate.getMonth()+1);
			}
		} else {*/
		labelStr = directory.getMonthName(stringDate.getMonth() + 1) + " " + stringDate.getFullYear();
		fIObj.params.startDate = (Date.parse(date) / 1000);
		var endDate = new Date(stringDate.getFullYear(), stringDate.getMonth() + 1, 0);
		fIObj.params.endDate = (Date.parse(endDate) / 1000);
		fIObj.searchInCalendar(fIObj, fIObj.params.startDate, fIObj.params.endDate);
		//}


		//$("#situate-day .date-label").text(labelStr);
		//	coInterface.scrollTo(".dayEvent");
		//fIObj.paramsObj.search.obj.nbPage=0;
		//pageCount=false;
		//fIObj.searchObj.search.obj.count=true;
		//	directory.scrollTop = false;
		//	fIObj.searchObj.agenda.options.noResult=false;
		//	fIObj.searchObj.agenda.options.finishSearch=false;
		//fIObj.searchObj.agenda.options.dayCount=0;
		//	fIObj.searchObj.agenda.options.todayDate = fIObj.searchObj.search.obj.startDate * 1000;
		//	fIObj.searchObj.search.init(fIObj.searchObj);
	},
	searchInCalendar: function (fIObj, startDate, endDate) {
		mylog.log("calendarObj.searchInCalendar", fIObj, startDate, endDate);
		//if(typeof fIObj.searchObj != "undefined"){
		var paramsSearchCalendar = {};//fIObj.searchObj.search.constructObjectAndUrl(fIObj.searchObj,true);
		if (typeof fIObj != "undefined"
			&& typeof fIObj.searchObj != "undefined"
			&& typeof fIObj.searchObj.pInit != "undefined"
			&& typeof fIObj.searchObj.pInit.defaults != "undefined"
			&& typeof fIObj.searchObj.pInit.defaults.sourceKey != "undefined") {
			paramsSearchCalendar.sourceKey = fIObj.searchObj.pInit.defaults.sourceKey;
		}
		$.extend(paramsSearchCalendar, fIObj.params);
		if (fIObj.options.personalAgenda === false)
			ajaxPost(
				null,
				baseUrl + "/" + moduleId + "/search/geteventsforcalendar/startDate/" + startDate + "/endDate/" + endDate,
				paramsSearchCalendar,
				function (data) {
					mylog.log("calendarObj.searchInCalendar success", data);
					if (!data) {
						toastr.error(data.content);
					} else {
						if (fIObj.options.personalAgenda === false)
							fIObj.results.add(fIObj, data.events);
					}
				},
				function (data) {
					mylog.log("calendarObj.searchInCalendar error", data);
				}
			);
		//}
	}/*,
	calculateAgendaWindow: function(fIObj){
		mylog.log('calendarObj.calculateAgendaWindow');
		var today = new Date();
		var todayMoment = moment().seconds(0).minute(0).hour(0);
		today = new Date(today.setSeconds(0));
		today = new Date(today.setMinutes(0));
		today = new Date(today.setHours(0));
		fIObj.params.starDate = today.setDate(today.getDate());
		searchObject.startDate = Math.floor(fIObj.params.starDate / 1000);

	}*/
};