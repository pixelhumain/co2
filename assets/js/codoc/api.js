var API = {
    baseUrl: baseUrl + "/api/codoc",
    repositories:{
        get: function(path=""){
            return new Promise(function(resolve){
                $.get(API.baseUrl+"/getRepositories", { path: path }, function(data){
                    repositories = {};

                    data.forEach(function(repository, index){
                        if(repository.type == "tree")
                            repository.children = {};
                        
                        //some time the id is the same from gitlab
                        repository.id = repository.id+"_"+index;

                        repositories[repository.id] = repository;
                    });

                    resolve(repositories)
                })
            })
        }
    },
    repositoryFiles:{
        getRaw: function(path){
            return new Promise(function(resolve){
                $.get(API.baseUrl+"/getFileRaw", {path:path}, function(data){
                    resolve(data.raw)
                })
            })
        },
        updateFile: function(path, content, message){
            return new Promise(function(resolve){
                $.post(API.baseUrl+"/updateFile", { path:path, content:content, message:message }, function(){
                    resolve()
                })
            })
        }
    }
}