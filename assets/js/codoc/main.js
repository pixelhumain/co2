var codoc = {
    pubsub: new PubSub(),
    content:"",
    path:"",

    eventNames: {
        REPOSITORIES_FETCHED:"repositoriesFetched",
        FILE_RAW_FETCHED:"fileRawFetched",
        FILE_UPDATED: "fileUpdated"
    },
    helper:{
        mdToHTML: function(md){
            var converter = new showdown.Converter({
                tables:true,
                strikethrough:true,
                tasklists:true
            })
            return  converter.makeHtml(md)
        },
        addHistory: function(newPath){
            var currentLocation = new URL(window.location.href)
            currentLocation.search = "?path="+encodeURIComponent(newPath);
            history.pushState({}, null, currentLocation.toString())
            window.dispatchEvent(new Event('popstate'));
        }
    },
    actions: {
        loader:{
            show:function(){
                $(".loader-container").addClass("active")
            },
            hide:function(){
                $(".loader-container").removeClass("active")
            }
        },

        fetchRepositories: function(parentId, parentPath){
           return new Promise(function(resolve){
                API.repositories.get(parentPath).then(function(repositories){
                    codoc.pubsub.publish(codoc.eventNames.REPOSITORIES_FETCHED, { parentId:parentId, repositories:repositories })

                    resolve(repositories);
                })
           })
        },
        fetchFileRaw: function(path){
            return new Promise(function(resolve){
                codoc.actions.loader.show();

                API.repositoryFiles.getRaw(path).then(function(content){
                    codoc.content = content;
                    codoc.path = path;

                    codoc.pubsub.publish(codoc.eventNames.FILE_RAW_FETCHED, { path:path, content:content });
                    resolve({ path:path, content:content });

                    codoc.actions.loader.hide();
                })
            })
        },
        updateFile: function(path, content, message){
            return new Promise(function(resolve){
                codoc.actions.loader.show();

                API.repositoryFiles.updateFile(path, content, message).then(function(){
                    toastr.success("Modification enregistrée.");

                    codoc.pubsub.publish(codoc.eventNames.FILE_UPDATED, { path:path });
                    resolve({ path:path })

                    codoc.actions.loader.hide();
                })
            })
        }
    },

    navigation:{
        views:{
            initBreadcrumb: function(path){
                var pathParts = path.split("/");
                var breadcrumb = pathParts.map(function(step, index){
                    if(index == pathParts.length - 1)
                        step = step.replace(".md", "");
                    return `<span>${step}</span>`
                })
                $(".custom-breadcrumb").html(breadcrumb);
            },
            initNavs: function(container, repositories){
                var $ul = $(`<ul class="menu-group active"></ul>`);

                Object.keys(repositories).forEach(function(id){
                    var repository = repositories[id];
                    
                    if(repository.type != "tree")
                        $ul.append(`<li class="menu-item"><a href="javascript:;"  data-id="${repository.id}" data-path="${repository.path}">${repository.name.replace(".md", "").capitalize()}</a></li>`)
                    else{
                        $ul.append(`
                            <li id="${id}">
                                <span class="menu-group-label" data-id="${repository.id}" data-path="${repository.path}">
                                    ${repository.name.replace(/\d+\s?-\s?/, "")} 
                                    <span><i class="fa fa-caret-down" aria-hidden="true"></i></span>
                                </span>
                            </li>
                        `);
                    }
                })

                $(container).append($ul);

                codoc.navigation.events.initNav()
            }
        },
        events:{
            initNav: function(){
                $(".menu-item a").off("click").click(function(){
                    var path = $(this).data("path");

                    codoc.navigation.views.initBreadcrumb(path)
                    codoc.helper.addHistory(path)

                    codoc.actions.fetchFileRaw(path)
                })

                $(".menu-group-label").off("click").click(function(){
                    var id = $(this).data("id"),
                        path = $(this).data("path");

                    if(!$(this).hasClass("active") && $(this).parent().find("> .menu-group").length < 1)
                        codoc.actions.fetchRepositories(id, path)

                    $(this).toggleClass("active")
                    $(this).parent().find("> .menu-group").toggleClass("active")
                })
            },
            init: function(){
                codoc.pubsub.subscribe(codoc.eventNames.REPOSITORIES_FETCHED, function(payload){
                    var container = payload.parentId ? `#${payload.parentId}`:".nav-body";
                    codoc.navigation.views.initNavs(container, payload.repositories)
                })
            }
        },
        init: function(){
            this.events.init()
        }
    },

    preview:{
        views:{
            init: function(content){
                $(".page-content").scrollTop(0);
                $("#md-viewer").html(codoc.helper.mdToHTML(content))
            }
        },
        events:{
            init: function(){
                codoc.pubsub.subscribe(codoc.eventNames.FILE_RAW_FETCHED, function(payload){
                    codoc.preview.views.init(payload.content)
                })
            }
        },
        init: function(){
            this.events.init()
        }
    },

    editor:{
        instance: null,
        destroy: function(){
            try{
                var editor = codoc.editor.instance;
                if(editor && typeof editor.destroy == "function")
                    editor.destroy()
                codoc.editor.instance = null
            }catch{}
        },
        views:{
            init: function(){
                codoc.editor.destroy();

                codoc.editor.instance = new toastui.Editor({
                    el: document.querySelector("#md-viewer"),
                    height: "auto",
                    initialValue: codoc.content,
                    hideModeSwitch:true
                });  
            }
        }, 
        events:{
            init: function(){
                $(".btn-content-action").off("click").on("click", function(){
                    switch($(this).data("action")){
                        case "edit":
                            $(".btn-content-action").addClass("d-none")
                            $(".btn-content-action[data-action='cancel']").removeClass("d-none")
                            $(".btn-content-action[data-action='save']").removeClass("d-none")

                            codoc.editor.views.init()
                        break;
                        case "cancel":
                            $(".btn-content-action").addClass("d-none")
                            $(".btn-content-action[data-action='edit']").removeClass('d-none')

                            codoc.editor.destroy();
                            codoc.preview.views.init(codoc.content);
                        break;
                        case "save":
                            bootbox.prompt({
                                title: "Pouvez-vous préciser la raison de votre modification ?", 
                                callback : function(message){ 
                                    if(message)
                                        codoc.actions.updateFile(codoc.path, codoc.editor.instance.getMarkdown(), message)
                                }
                            });
                        break;
                    }
                })

                codoc.pubsub.subscribe(codoc.eventNames.FILE_RAW_FETCHED, function(){
                    $(".btn-content-action").addClass("d-none")
                    $(".btn-content-action[data-action='edit']").removeClass('d-none')
                })

                codoc.pubsub.subscribe(codoc.eventNames.FILE_UPDATED, function(){
                    $(".btn-content-action").addClass("d-none")
                    $(".btn-content-action[data-action='edit']").removeClass('d-none')

                    codoc.editor.destroy();
                    codoc.actions.fetchFileRaw(codoc.path)
                })
            }
        },
        init:function(){
            this.events.init()
        }
    },

    init: function(){
        this.navigation.init();
        this.preview.init();
        this.editor.init();
        
        var url = new URL(location.href)
        if(url.searchParams.get("path")){
            var path = url.searchParams.get("path");
            codoc.navigation.views.initBreadcrumb(path);
            codoc.actions.fetchFileRaw(path);
        }
        
        this.actions.fetchRepositories(null, "/");
    }
}


$(function(){
    codoc.init()
})