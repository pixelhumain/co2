Object.defineProperty(String.prototype, 'capitalize', {
    value: function() {
      return this.charAt(0).toUpperCase() + this.slice(1).toLowerCase();
    },
    enumerable: false
});

var codocOld = {
    repositories:{},
    openMenus:[],
    selectedRepository:{
        raw:""
    },
    container:{
        markdown:"#md-viewer"
    },
    mdEditor:{
        instance: null,
        init: function(initialValue=""){
            codoc.mdEditor.destroy();
            codoc.mdEditor.instance = new toastui.Editor({
                el: document.querySelector(codoc.container.markdown),
                height: "auto",
                initialValue: initialValue,
                theme: codoc.theme.get(),
                hideModeSwitch:true
            });  
        },
        destroy: function(){
            try{
                var editor = codoc.mdEditor.instance;
                if(editor && typeof editor.destroy == "function")
                    editor.destroy()
                codoc.mdEditor.instance = null
            }catch{}
        }
    },
    utils:{
        mdToHTML: function(md){
            var converter = new showdown.Converter({
                tables:true,
                strikethrough:true,
                tasklists:true
            })
            return  converter.makeHtml(md)
        },
        getUrlParamByName: function(name, url=window.location.href){
            name = name.replace(/[\[\]]/g, '\\$&');
            var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
                results = regex.exec(url);
            if (!results) return null;
            if (!results[2]) return '';
            return decodeURIComponent(results[2].replace(/\+/g, ' '));
        },
        addHistory: function(newPath){
            var currentLocation = new URL(window.location.href)
            currentLocation.search = "?path="+encodeURIComponent(newPath);
            history.pushState({}, null, currentLocation.toString())
            window.dispatchEvent(new Event('popstate'));
        }
    },
    theme:{
        switch: function(theme){
            localStorage.setItem('theme', theme);

            if(theme == "dark"){
                $("body").addClass("theme-dark")
                $(codoc.container.markdown).removeClass("markdown-body").addClass("markdown-body-dark")
            }else{
                $("body").removeClass("theme-dark")
                $(codoc.container.markdown).addClass("markdown-body").removeClass("markdown-body-dark")
            }
            if(codoc.mdEditor.instance)
                codoc.mdEditor.init(codoc.mdEditor.instance.getMarkdown())
        },
        get:function(){
            return  localStorage.getItem('theme');
        },
        init: function(){
            var theme = localStorage.getItem('theme');
            codoc.theme.switch(theme)
            if(theme == "dark")
                $(".theme-switcher input").attr("checked", true)
        }
    },
    views:{
        menus:{
            get: function(repositories){
                var $ul = $(`<ul class="menu-group"></ul>`);
                Object.keys(repositories).forEach(function(id){
                    var repository = repositories[id];
                    
                    if(repository.type != "tree")
                        $ul.append(`<li class="menu-item"><a href="javascript:;" data-path="${repository.path}">${repository.name.replace(".md", "").capitalize()}</a></li>`)
                    else{
                        var $li = $("<li></li>");
                        $li.append(`<span class="menu-group-label" data-id="${repository.id}" data-path="${repository.path}">${repository.name.replace(/\d+\s?-\s?/, "")} <span><i class="fa fa-caret-down" aria-hidden="true"></i></span></span>`);
                        $li.append(codoc.views.menus.get(repository.children));
                        $ul.append($li)
                    }
                })
                return $ul;
            },
            init: function(repositories){
                $(".nav-body").append(codoc.views.menus.get(repositories))
            }
        },
        breadcrumb:{
            init: function(){
                var path = codoc.utils.getUrlParamByName("path");
                path = path ? path:"";
                var pathParts = path.split("/");
                var breadcrumb = pathParts.map(function(step, index){
                    if(index == pathParts.length - 1)
                        step = step.replace(".md", "");
                    return `<span>${step}</span>`
                })
                $(".custom-breadcrumb").html(breadcrumb);
            }
        },
        loader:{
            show:function(){
                $(".loader-container").addClass("active")
            },
            hide:function(){
                $(".loader-container").removeClass("active")
            }
        }
    },
    events:{
        initEventsMenus: function(){
            $(".menu-item a").click(function(){
                codoc.utils.addHistory($(this).data("path"))
            })

            $(".menu-group-label").click(function(){
                $(this).toggleClass("active")
                $(this).parent().find("> .menu-group").toggleClass("active")

                API.repositories.get($(this).data("path")).then(function(repositories){
                    
                }) 
            })
        },
        initEventsActions: function(){
            $("#btn-update").click(function(){
                codoc.mdEditor.init(codoc.selectedRepository.raw)
                $("#btn-save-update").removeClass("d-none")
                $("#btn-update").addClass("d-none");
                $("#btn-cancel-update").removeClass("d-none")
            })

            $("#btn-cancel-update").click(function(){
                codoc.mdEditor.destroy()
                $(codoc.container.markdown).html(codoc.utils.mdToHTML(codoc.selectedRepository.raw))
                $("#btn-update").removeClass("d-none");
                $("#btn-save-update").addClass("d-none");
                $("#btn-cancel-update").addClass("d-none");
            })

            $("#btn-save-update").click(function(){
                var data = {
                    path: codoc.utils.getUrlParamByName("path"),
                    content: codoc.mdEditor.instance.getMarkdown()
                }
                codoc.views.loader.show()
                API.repositoryFiles.save(data).then(function(){
                    codoc.mdEditor.destroy()

                    codoc.selectedRepository.raw = data.content;
                    $(codoc.container.markdown).html(codoc.utils.mdToHTML(data.content));

                    $("#btn-update").removeClass("d-none");
                    $("#btn-save-update").addClass("d-none");
                    $("#btn-cancel-update").addClass("d-none");

                    codoc.views.loader.hide()
                })
            })
        },
        initEventThemeSwitcher: function(){
            $(".theme-switcher input").change(function(){
               var theme = $(this).is(":checked") ? "dark":"light";
               codoc.theme.switch(theme)
            })
        },
        initEventPopState: function(){
            window.addEventListener('popstate', function(){
                codoc.views.loader.show()
                API.repositoryFiles.get(codoc.utils.getUrlParamByName("path")).then(function(res){
                    if(res.raw){
                        codoc.selectedRepository.raw = res.raw;
                        //afficher les boutons actions
                        $("#btn-update").removeClass("d-none");
                    }else{
                        codoc.selectedRepository.raw = "";
                        $("#btn-update").addClass("d-none");
                    }

                    codoc.mdEditor.destroy()
                    $(codoc.container.markdown).html(codoc.utils.mdToHTML(codoc.selectedRepository.raw))

                    //init breadcrumb
                    codoc.views.breadcrumb.init();

                    $("#btn-save-update").addClass("d-none");
                    $("#btn-cancel-update").addClass("d-none")

                    codoc.views.loader.hide()
                })
            });
        },
        init: function(){
            codoc.events.initEventsMenus()
            codoc.events.initEventsActions()
            codoc.events.initEventThemeSwitcher()
            codoc.events.initEventPopState()
        }
    },
    init: function(){
        codoc.views.loader.show()
        API.repositories.get().then(function(respositories){
            codoc.repositories = respositories;

            codoc.views.menus.init(respositories)
            codoc.events.init()
            codoc.theme.init()
            var path = codoc.utils.getUrlParamByName("path")
            if(path){
                codoc.utils.addHistory(path)
            }
            codoc.views.loader.hide()
        })
    }
}

$(function(){
    codoc.init()
})