Object.defineProperty(String.prototype, 'capitalize', {
    value: function() {
      return this.charAt(0).toUpperCase() + this.slice(1).toLowerCase();
    },
    enumerable: false
});


function PubSub(){
    this.events = {}

    this.subscribe = function(eventName, handler){
        if(this.events[eventName])
            this.events[eventName].push(handler)
        else
            this.events[eventName] = [handler]

        return { eventName, handler }
    }

    this.unsubscribe = function(subscriber){
        if(this.events[subscriber.eventName]){
            this.events[eventName] = this.events[eventName].filter((handler) => handler !== subscriber.func);
        }
    }

    this.publish = function(eventName, payload){
        var handlers = this.events[eventName] || []
        handlers.forEach(function(handler){
            handler.apply(null, [payload])
        })
    }
}
