//Retrieve the countries in ajax and return an array of value
//The selectType change the value key : 
//for select input : {"value":"FR", "text":"France"}
//for select2 input : {"id":"FR", "text":"France"}

function getCountries(selectType) {
	mylog.log("getCountries");
	var result = new Array();
	extraParamsAjax={
		global: false,
		async: false
	}; //////// QUE FAIRE
	ajaxPost(
        null,
        baseUrl+"/"+moduleId+"/opendata/getcountries",
        null,
        function(data){ 
          	$.each(data, function(i,value) {
				if (selectType == "select2") {
					result.push({"id" : value.value, "text" :value.text});
				} else {
					result.push({"value" : value.value, "text" :value.text});
				}
			}) 
  		},
  		null,
  		"json",
  		extraParamsAjax

  	);
	return result;
}

function formatDataForSelect(data, selectType) {
	var result = new Array();
	$.each(data, function(key,value) {
		if (selectType == "select2") {
			result.push({"id" : key, "text" :value});
		} else {
			result.push({"value" : key, "text" :value});
		}
	});
	return result;
}

function getCitiesByPostalCode(postalCode, selectType) {
	var result =new Array();
	extraParamsAjax={
		global: false,
		async: false
	}; //////// QUE FAIRE
	
	ajaxPost(
        null,
        baseUrl+"/"+moduleId+"/opendata/getcitiesbypostalcode",
        {postalCode: postalCode},
        function(data){ 
          	$.each(data, function(key,value) {
				if (selectType == "select2") {
					result.push({"id" : value.insee, "text" :value.name});
				} else {
					result.push({"value" : value.insee, "text" :value.name});
				}
			});
  		},
  		null,
  		"json",
  		extraParamsAjax

  	);
	return result;
}

function getCookie(cookieName) {
	let name = cookieName + "=";
	let allCookie = document.cookie.split(';');
	for(let i = 0; i < allCookie.length; i++) {
		let cookie = allCookie[i];
		while (cookie.charAt(0) == ' ') {
			cookie = cookie.substring(1);
		}
		if (cookie.indexOf(name) == 0) {
			return cookie.substring(name.length, cookie.length);
		}
	}
	return "";
}

/** added by tristan **/
function getCitiesGeoPosByPostalCode(postalCode, selectType) {
	var result =new Array();
	extraParamsAjax={
		global: false,
		async: false
	}; //////// QUE FAIRE
	
	ajaxPost(
        null,
        baseUrl+"/"+moduleId+"/opendata/getcitiesgeoposbypostalcode",
        {postalCode: postalCode},
        function(data){ 
          	result.push(data);
  		},
  		null,
  		"json",
  		extraParamsAjax

  	);
	return result;
}

function isUniqueUsername(username) {
	var response;
	extraParamsAjax={
		global: false,
		async: false
	}; //////// QUE FAIRE
	
	ajaxPost(
        null,
        baseUrl+"/"+moduleId+"/person/checkusername/",
        {username: username},
        function(data){ 
          	response = data;
  		},
  		null,
  		"json",
  		extraParamsAjax
  	);
	mylog.log("isUniqueUsername=", response);
	return response;
}

function checkUniqueEmail(email) {
	var response;
	extraParamsAjax={
		global: false,
		async: false
	}; //////// QUE FAIRE
	ajaxPost(
        null,
        baseUrl+"/"+moduleId+"/person/checkemail/",
        {email: email},
        function(data){ 
          	response = data.res;
  		},
  		null,
  		"json",
  		extraParamsAjax
  	);
	mylog.log("isUniqueEmail=", response);
	return response;
}

function slugUnique(searchValue){
	
	//searchType = (types) ? types : ["organizations", "projects", "events", "needs", "citoyens"];
	var response;
	var data = {
		"id":contextData.id,
		"type": contextData.type,
		"slug" : searchValue
	};
	//$("#listSameSlug").html("<i class='fa fa-spin fa-circle-o-notch'></i> Vérification d'existence");
	//$("#similarLink").show();
	//$("#btn-submit-form").html('<i class="fa  fa-spinner fa-spin"></i>').prop("disabled",true);
	mylog.log("slugUnique data", data);
	ajaxPost(
        null,
        baseUrl+"/"+moduleId+"/slug/check",
        data,
        function(data){ 
          	var msgSlug = "";
			if (!data.result) {
				//response=false;
				if(!$("#ajaxFormModal #slug").parent().hasClass("has-error")){
					$("#ajaxFormModal #slug").parent().removeClass('has-success').addClass('has-error');//.find("span").text("cucu");
					$("#ajaxFormModal .slugtext .help-blockk").removeClass('letter-green').addClass('letter-red');
				}
				//.addClass("has-error").parent().find("span").text("cretin");
				msgSlug = "<small>www." + data.domaineName + "#"+ searchValue + "</small>" +
						  "<br><i class='fa fa-times'></i> " + tradDynForm["This URL is already used"];

				$("#ajaxFormModal #btn-submit-form").attr('disabled','disabled');
				//coInterface.bindLBHLinks();
			} else {
				//response=true;
				if(!$("#ajaxFormModal #slug").parent().hasClass("has-success")){
					$("#ajaxFormModal #slug").parent().removeClass('has-error').addClass('has-success');
					$("#ajaxFormModal .slugtext .help-blockk").removeClass('letter-red').addClass('letter-green');
				}
				//.addClass("has-success").parent().find("span").text("good peydé");
				
				msgSlug = "<small>www." + data.domaineName + "#"+ searchValue + "</small>" +
						  "<br><i class='fa fa-check-circle'></i> " + tradDynForm["This URL is not used"];

				$("#ajaxFormModal #btn-submit-form").removeAttr('disabled');
				//$("#slug").html("<span class='txt-green'><i class='fa fa-thumbs-up text-green'></i> Aucun pseudo avec ce nom.</span>");

			}

			$("#ajaxFormModal .slugtext .help-blockk").html(msgSlug);
			//$("#ajaxFormModal .slugtext .help-block").html(msgSlug).show();
			$("#ajaxFormModal #slug").data("checking", false);
            console.log("checking slug", false, "msg", msgSlug);
  		},
  		function (data){
             mylog.log("error"); mylog.dir(data);
        }
  	);
 	//return response;
}

function addCustomValidators() {
	mylog.log("addCustomValidators");
	//Validate a postalCode
	jQuery.validator.addMethod("validPostalCode", function(value, element) {
	    var response;
		extraParamsAjax={
			global: false,
			async: false
		}; //////// QUE FAIRE
		ajaxPost(
	        null,
	        baseUrl+"/"+moduleId+"/opendata/getcitiesbypostalcode/",
	        {postalCode: value},
	        function(data){ 
	          	response = data;
	  		},
	  		null,
	  		"json",
	  		extraParamsAjax
	  	);
	    if (Object.keys(response).length > 0) {
	    	return true;
	    } else {
	    	return false;
	    }
	}, "Code postal inconnu");
	/*jQuery.validator.addMethod("uniqueSlug", function(value, element) {
	    //Check unique username
	   	return slugUnique(value);
	}, "This slug already exists. Please choose an other one.");*/
	jQuery.validator.addMethod("validPassword", function(value, element) {
		//check password validity
		var passwordregex = new RegExp(
			"^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[-+_!@#$%^&*.,?]).{8,}$"
		);
		var validPassword = value.match(passwordregex);
		if (validPassword == null) {
        	return false;
    	} else {
    		return true;
    	}
	},tradDynForm.thePasswordMustContainAtLeastCharactersLowercaseUppercaseNumberSpecialCharacter)
	jQuery.validator.addMethod("validUserName", function(value, element) {
	    //Check authorized caracters
		var usernameRegex = /^[a-zA-Z0-9\-]+$/;
    	var validUsername = value.match(usernameRegex);
    	if (validUsername == null) {
        	return false;
    	} else {
    		return true;
    	}
    }, tradDynForm.invalidUsernamewithspace);

	jQuery.validator.addMethod("uniqueUserName", function(value, element) {
	    //Check unique username
	   	return isUniqueUsername(value);
	}, "A user with the same username already exists. Please choose an other one.");

	/*jQuery.validator.addMethod("lengthMin", function(value, element, params) {
	    //Check unique username
	    //if(Object.keys(finder.object[element]).length >=value )
	   	//	return true;
	   	//else
	   		return false;
	}, "Select at least {value} item");*/
	jQuery.validator.addMethod("agreeValidation", function(value, element) {
	  //alert();
	  return false;
	  //return this.optional(element) || /^http:\/\/mycorporatedomain.com/.test(value);
	}, "Vous navez pas ");
	jQuery.validator.addMethod("inArray", function(value, element) {
	    //Check authorized caracters
		test = $.inArray( element, value );
    	if (test >= 0) 
    		return true;
    	else
    		return false;
    }, "Invalid : please stick to given values.");

    jQuery.validator.addMethod("greaterThan", function(value, element, params) {
    	mylog.log("greaterThan", value, params);
    	mylog.log(moment(value, "DD/MM/YYYY HH:mm").format());
    	mylog.log(!/Invalid|NaN/.test(new Date(moment(value, "DD/MM/YYYY HH:mm").format()))); 
	    //if (!/Invalid|NaN/.test(new Date(value))) {
	    if (!/Invalid|NaN/.test(new Date(moment(value, "DD/MM/YYYY HH:mm").format()))) {
	    	mylog.log(!/Invalid|NaN/.test(new Date(moment(value, "DD/MM/YYYY HH:mm").format()))); 

	        return moment(value, "DD/MM/YYYY HH:mm").isSameOrAfter(moment($(params[0]).val(), "DD/MM/YYYY HH:mm"));
	    }    
	    return isNaN(value) && isNaN($(params[0]).val()) || (Number(value) > Number($(params[0]).val())); 
	},'Doit ètre aprés {1}.');

// jQuery.validator.addMethod("greaterThanOrSame", function(value, element, params) {
//    	mylog.log("greaterThan", value, params);
//    	mylog.log(moment(value, "DD/MM/YYYY HH:mm").format());
//    	mylog.log(!/Invalid|NaN/.test(new Date(moment(value, "DD/MM/YYYY HH:mm").format()))); 
//     //if (!/Invalid|NaN/.test(new Date(value))) {
//     if (!/Invalid|NaN/.test(new Date(moment(value, "DD/MM/YYYY HH:mm").format()))) {
//     	mylog.log(!/Invalid|NaN/.test(new Date(moment(value, "DD/MM/YYYY HH:mm").format()))); 
//         return moment(value, "DD/MM/YYYY HH:mm").isSameOrAfter(moment($(params[0]).val(), "DD/MM/YYYY HH:mm"));
//     }    
//     return isNaN(value) && isNaN($(params[0]).val()) || (Number(value) > Number($(params[0]).val())); 
// },'Doit ètre aprés {1}.');

	jQuery.validator.addMethod("greaterThanNow", function(value, element, params) {   
		mylog.log("greaterThanNow", value,  params[0]);
		mylog.log(moment(value, params[0])," < ",moment());
	    return moment(value, params[0]).isAfter(moment()); 
	},"Doit être après la date d'aujourd'hui.");

	jQuery.validator.addMethod("duringDates", function(value, element, params) {  
		mylog.log("params",value,$(params[0]).val(),$(params[1]).val());
		if( $(params[0]).val() && $(params[1]).val() ){
			//console.warn(moment(value, "DD/MM/YYYY HH:mm"),moment($(params[0]).val()),moment($(params[1]).val()));
			mylog.log("attoooy",moment(value, "DD/MM/YYYY HH:mm"),moment($(params[0]).val()));
	    	return (moment(value, "DD/MM/YYYY HH:mm").isSameOrAfter(moment($(params[0]).val())) 
	    		&& moment(value, "DD/MM/YYYY HH:mm").isSameOrBefore(moment($(params[1]).val())));
	    	//return  ( new Date(value) >= new Date( $(params[0]).val() ) && new Date(value) <= new Date($(params[1]).val()) );
		} 
		return true;
	},"Cette date est exterieure à l'évènement parent.");

	jQuery.extend(jQuery.validator.messages, {
		required: tradDynForm["This field is required."],
		remote: tradDynForm["Please fix this field."],
		email: tradDynForm["Please enter a valid email address."],
		url: tradDynForm["Please enter a valid URL (ex: http://wwww.communecter.org)"],
		date: tradDynForm["Please enter a valid date."],
		dateISO: tradDynForm["Please enter a valid date (ISO)."],
		number: tradDynForm["Please enter a valid number."],
		digits: tradDynForm["Please enter only digits."],
		creditcard: tradDynForm["Please enter a valid credit card number."],
		equalTo: tradDynForm["Please enter the same value again."],
		maxlength: $.validator.format(tradDynForm["Please enter no more than {0} characters."]),
		minlength: $.validator.format(tradDynForm["Please enter at least {0} characters."]),
		rangelength: $.validator.format(tradDynForm["Please enter a value between {0} and {1} characters long."]),
		range: $.validator.format(tradDynForm["Please enter a value between {0} and {1}."]),
		max: $.validator.format(tradDynForm["Please enter a value less than or equal to {0}."]),
		min: $.validator.format(tradDynForm["Please enter a value greater than or equal to {0}."])
	});
}

function showLoadingMsg(msg){
	$("#main-title-public1").html("<i class='fa fa-refresh fa-spin'></i> "+msg+" ...");
	$("#main-title-public1").show(300);
}

function hideLoadingMsg(){
	$("#main-title-public1").html("");
	$("#main-title-public1").hide(300);
}

function dateSecToString(date){
	var yyyy = date.getFullYear().toString();
	var mm = (date.getMonth()+1).toString(); // getMonth() is zero-based
    var dd  = date.getDate().toString();
    var min  = date.getMinutes().toString();
    var ss  = date.getSeconds().toString();
    date = yyyy + "-" + (mm[1]?mm:"0"+mm[0]) + "-" + (dd[1]?dd:"0"+dd[0]) + " " +
    					(min[1]?min:"0"+min[0]) + ":" + (ss[1]?ss:"0"+ss[0]) + ":00"; // padding
    return date;
}

function dateToStr(date, lang, inline, fullMonth){ //work with date formated : yyyy-mm-dd hh:mm:ss ou millisecond

	if(typeof date == "undefined") return;
	if(fullMonth != true) fullMonth = false;

	//mylog.log("convert format date 1", date);
	if(typeof date.sec != "undefined"){
		date = new Date(date.sec);
		date = dateSecToString(date);
	}
	else if(typeof date == "number"){
		date = new Date(date);
		date = dateSecToString(date);
	}
	//mylog.log(date);
	if(lang == "fr"){
		//(year, month, day, hours, minutes, seconds, milliseconds) 
		//mylog.log("convert format date", date);
		var year 	= date.substr(0, 4);
		var month 	= date.substr(5, 2);//getMonthStr(date.substr(5, 2), lang);
		var day 	= date.substr(8, 2);
		var hours 	= date.substr(11, 2);
		var minutes = date.substr(14, 2);
		

		var str = day + "/" + month + "/" + year;

		if(fullMonth) str = day + " " + getMonthStr(month, "fr") + " " + year;


		if(!inline) str += "</br>";
		else str += " - ";
		str += hours + "h" + minutes;
		
		return str;
	}

	function getMonthStr(monthInt, lang){
		if(lang == "fr"){
			if(monthInt == "01") return "Janvier";
			if(monthInt == "02") return "Février";
			if(monthInt == "03") return "Mars";
			if(monthInt == "04") return "Avril";
			if(monthInt == "05") return "Mai";
			if(monthInt == "06") return "Juin";
			if(monthInt == "07") return "Juillet";
			if(monthInt == "08") return "Août";
			if(monthInt == "09") return "Septembre";
			if(monthInt == "10") return "Octobre";
			if(monthInt == "11") return "Novembre";
			if(monthInt == "12") return "Décembre";
		}
	}
}

function getObjectId(object){
	if(object === null) return null;
	if("undefined" != typeof object._id && "undefined" != typeof object._id.$id) 	return object._id.$id.toString();
	if("undefined" != typeof object.id) 	return object.id;
	if("undefined" != typeof object.$id) 	return object.$id;
	return null;
}

function getFullTextCountry(codeCountry){
	var countries = {
		"FR" : "France",
		"RE" : "Réunion",
		"NC" : "Nouvelle-Calédonie",
		"GP" : "Gouadeloupe",
		"GF" : "Guyanne-Française",
		"MQ" : "MartiniqueMQ",
		"PM" : "Saint-Pierre-Et-Miquelon"
	};
	if(typeof countries[codeCountry] != "undefined")
	return countries[codeCountry];
	else return "";
}

var dataHelper = {
	compare : function ( a, b ) {
		if ( a.last_nom < b.last_nom ){
			return -1;
		}
		if ( a.last_nom > b.last_nom ){
			return 1;
		}
			return 0;
	},
	sortObjectByValue : function(list){
		mylog.log("dataHelper.sortObjectByValue", list);
		var listVal = []
		$.each(list, function(kList, vList){
			listVal.push( ( ( typeof tradCategory[vList] != "undefined") ? tradCategory[vList]:vList ) );
		});
		listVal.sort();
		mylog.log("dataHelper.sortObjectByValue listVal", listVal);
		var newList = {};
		$.each(listVal, function(k, v){
			$.each(list, function(kList, vList){
				if(v == ( ( typeof tradCategory[vList] != "undefined") ? tradCategory[vList]:vList ) ){
					mylog.log("dataHelper.sortObjectByValue v", kList, v);
					newList[kList] = v;
				}
			});
		});
		mylog.log("dataHelper.sortObjectByValue end", newList);
		return newList;
	},
	csvToArray : function (csv, separateur, separateurText){
		var lines = csv.split("\n");			
		var result = [];
		$.each(lines, function(key, value){
			if(value.length > 0){
				var colonnes = value.split(separateur);
				var newColonnes = [];
				$.each(colonnes, function(keyCol, valueCol){
					
					if(typeof separateurText == "undefined" || separateurText =="")
						newColonnes.push(valueCol);
					else{
						if(valueCol.charAt(0) == separateurText && valueCol.charAt(valueCol.length-1) == separateurText){
							var elt = valueCol.substr(1,valueCol.length-2);
							newColonnes.push(elt);
						}else{
							newColonnes.push(valueCol);
						}
					}
					
					
				});
				result.push(newColonnes);
			}
			
		});
		return result;
	},

	markdownToHtml : function (str, showdownOptions=null) { 
		var converter = new showdown.Converter(showdownOptions);
		var res = converter.makeHtml(str);
		if(typeof res == "string"){
			res = res.replace(/<a/g, "<a target='_blank'");
		}
		return res;
	},

	convertMardownToHtml : function (text) { 
		var converter = new showdown.Converter();
		var res = converter.makeHtml(text);
		if(typeof res == "string"){
			res = res.replace(/<a/g, "<a target='_blank'");
		}
		return res;
	},
	
	htmlToMarkdown: function (html) {
		var converter = new showdown.Converter();
		var res = converter.makeMarkdown(html);
		return res;
	},

	convertHtmlToMarkdown: function (html) {
		var converter = new showdown.Converter();
		return converter.makeMarkdown(html);
	},

	stringToBool : function (str) {
		var bool = false;
		if(str == "true" || str == true || str == "1" || str == 1)
			bool = true;
		return bool;
	},

	activateMarkdown : function (elem) { 
		//mylog.log("activateMarkdown", elem);

		markdownParams = {
			savable:false,
			iconlibrary:'fa',
			language:'fr',
			onPreview: function(e) {
				var previewContent = "";
				

				if (e.isDirty()) {
					previewContent = dataHelper.convertMardownToHtml(e.getContent());
				} else {
					previewContent = dataHelper.convertMardownToHtml(e.$textarea.closest('.md-editor.active').find('textarea').val());
				}
				return previewContent;
			},
			onSave: function(e) {
				mylog.log(e);
			},
			// Callback for the 'onShow' event.
		    //   Here we will transform the custom button into a dropdown button.
		    onShow: function(e) {
		      	// Get the custom button named "mention".
		      	var $button = e.$textarea.closest('.md-editor').find('button[data-handler="bootstrap-markdown-mention"]');
				var $buttonQuestionType = e.$textarea.closest('.md-editor').find('button[data-handler="bootstrap-markdown-coformQuestionType"]');
				let $questionTypeParent = $buttonQuestionType.parent();
		      
		     	$button.attr('data-toggle', 'dropdown')
		        .css({
		          'float': 'none'
		        });
				$buttonQuestionType.attr('data-toggle', 'dropdown').addClass('dropdown-toggle');
				$questionTypeParent.addClass('dropdown');
				$(`
					<ul class="dropdown-menu">
						<li><a href=javascript:; class="question-type" data-type="text">Text</a></li>
						<li><a href=javascript:; class="question-type" data-type="textarea">Textarea</a></li>
						<li><a href=javascript:; class="question-type" data-type="number">Nombre</a></li>
						<li><a href=javascript:; class="question-type" data-type="email">Email</a></li>
						<li><a href=javascript:; class="question-type" data-type="date">Date</a></li>
						<li><a href=javascript:; class="question-type" data-type="tag">Tag</a></li>
						<li><a href=javascript:; class="question-type" data-type="address">Adresse</a></li>
						<li><a href=javascript:; class="question-type" data-type="checkboxNew">Checkbox</a></li>
						<li><a href=javascript:; class="question-type" data-type="radioNew">Radio button</a></li>
						<li><a href=javascript:; class="question-type" data-type="evaluation">Evaluation</a></li>
						<li><a href=javascript:; class="question-type" data-type="select">Liste de choix</a></li>
						<li><a href=javascript:; class="question-type" data-type="section">Titre de section</a></li>
						<li><a href=javascript:; class="question-type" data-type="step">Nouvelle étape</a></li>
					</ul>
			  	`).appendTo($questionTypeParent);


		        //.before('<input type="text" style="width:70%" id="findMentionName" />');
		        //TODO : select a text and open finder panel
		        
				$('.question-type').on('click', function() {
					markMD = $(".md-input").val()+`- [${$(this).attr("data-type")}] `;
					$(".md-input").val(markMD);
					$(".md-input").focus()
				})
		        
		        $('button[data-handler="bootstrap-markdown-highlight"]').off().on("click",function() {
		        	markMD = $(".md-input").val()+"<mark>your text to highlight</mark>";
					$(".md-input").val(markMD);
		        })
		        $('button[data-handler="bootstrap-markdown-mention"]').off().on("click",function() { 
		      	  	title = "<h1 class='text-center' style='color:#337ab7'>@ Finder<br/>";
					message = '<i class="fa fa-search" style="color:#337ab7"></i> <input type="text" style="height:50px;color:#666" id="findMentionName" />'+
					            			'</h1><h4><div class="text-center" id="mentionResult"></div></h4>';

						        
					smallMenu.open(title+message,null,null,function(){  
						
			        	$( "#findMentionName" ).autocomplete( {
					        source: function( request, response ) {
					        	$("#mentionResult").html("<i class='fa fa-spin fa-circle-o-notch padding-25 fa-2x letter-azure'></i>");
					        	ajaxPost(
							        null,
							        baseUrl+"/"+moduleId+"/search/globalautocomplete",
							        {
							          	jqAuto : 1,
							            name: request.term,
							            searchType: ["citoyens","organizations","projects","events"],
										searchBy: "ALL"
							        },
							        function(data){ 
							          	if(data.responseText != "[]" ){		        
								          	var resultList = "<div class='text-center' style='margin-top:20px'><ul  style='list-style:none;' id='finderFilters'></ul><ul class='container row' style='list-style:none;'>";
								          	$.each($.parseJSON(data.responseText),function(i,v) {
								          		tobj = dyFInputs.get(v.type); 
								          		color = (tobj.color) ? tobj.color : "#eee" ;
								          		img = (v.img) ? "<img width='50' src='"+baseUrl+v.img+"'/>" : "<i class='fa fa-2x fa-"+tobj.icon+"'></i>";
								          		resultList += "<li class='col-xs-3 "+v.type+"' style='height:80px; border:1px solid #666;padding:5px;'>"+
										          			"<a href='javascript:;' class='mentionLine' data-name='"+v.label+"' data-id='"+v.id+"' data-type='"+v.type+"' data-slug='"+v.slug+"'>"+img+" "+v.value+"</a>"+
										          		+"</li>";	

								          	});
								          	resultList += "</ul></div>";
								            
								            $("#mentionResult").html(resultList);
										    $(".mentionLine").click(function() {
									    		//TODO position on cursor
									    		//send notif to mentionned person
									    		//link markdown 
									    		linkMD = $(".md-input").val()+"["+$(this).data("name")+"]("+baseUrl+"/co2#@"+$(this).data("slug")+")";
									    		$(".md-input").val(linkMD);
									    		$("#openModal").modal("hide");

									    	});
								        } else {
								        	invite = "#element.invite.type."+contextData.type+".id."+contextData.id;
								        	$("#mentionResult").html("<h4>Aucun résultat correspond. <br/>"+
								        		"<a href='"+invite+"' class='lbhp'>Invité par mail ?</a></h4>");
								        	coInterface.bindLBHLinks();
								        }
							  		}
							  	);
					   		},
					      	minLength: 2
					    } );
					    $( "#findMentionName" ).focus();
			        })
		        });


		      $button.dropdown();
		    },
		    additionalButtons: [
		      [{
		        data: [{
		          name: 'mention',
		          title: 'Mention',
		          icon: {
		            'fa': 'fa fa-at'
		          }
		        }]
		      },
		      {
		        name: 'highlight',
		        data: [{
		          name: 'cmdHighLight',
		          title: 'Highlight',
		          icon: {
		            fa: 'fa fa-paint-brush'
		          },
		          callback: function(e) {
		            // Give/remove ** surround the selection
		            var chunk, cursor, selected = e.getSelection(),
		              content = e.getContent();

		            if (selected.length === 0) {
		              // Give extra word
		              chunk = e.__localize('highlighted text');
		            } else {
		              chunk = selected.text;
		            }

		            // transform selection and set the cursor into chunked text
		            if (content.substr(selected.start - 6, 6) === '<mark>' &&
		              content.substr(selected.end, 6) === '<mark>') {
		              e.setSelection(selected.start - 6, selected.end + 6);
		              e.replaceSelection(chunk);
		              cursor = selected.start - 6;
		            } else {
		              e.replaceSelection('<mark>' + chunk + '</mark>');
		              cursor = selected.start + 6;
		            }

		            // Set the cursor
		            e.setSelection(cursor, cursor + chunk.length);
		          }
		        }
		       
		       ]
		      },
			  {
				name: 'coformQuestionType',
		        data: [{
		          name: 'coformQuestionType',
		          title: 'Type question',
				  btnClass: "btn hide",
		          icon: {
		            fa: 'fa fa-th-list'
		          },
				  callback: function(e) {
		            // Give/remove ** surround the selection
		            var chunk, cursor, selected = e.getSelection(),
		              content = e.getContent();
					// mylog.log("cursor", cursor)
					// mylog.log("content", content)
					// $('.question-type').on('click', function() {
					// 	markMD = $(".md-input").val()+`- [${$(this).attr("data-type")}] `;
					// 	$(".md-input").val(markMD);
					// 	cursor = content.length;
					// 	e.setSelection(cursor, cursor);
					// 	mylog.log("event", e)
					// })
		            /* if (selected.length === 0) {
		              // Give extra word
		              chunk = e.__localize('highlighted text');
		            } else {
		              chunk = selected.text;
		            }

		            // transform selection and set the cursor into chunked text
		            if (content.substr(selected.start - 6, 6) === '<mark>' &&
		              content.substr(selected.end, 6) === '<mark>') {
		              e.setSelection(selected.start - 6, selected.end + 6);
		              e.replaceSelection(chunk);
		              cursor = selected.start - 6;
		            } else {
		              e.replaceSelection('<mark>' + chunk + '</mark>');
		              cursor = selected.start + 6;
		            }

		            // Set the cursor
		            e.setSelection(cursor, cursor + chunk.length); */
		          }
		        }]
		      }
		    ]
		    ]
		}

		if( !$('script[src="'+baseUrl+'/plugins/bootstrap-markdown/js/bootstrap-markdown.js"]').length ){
			mylog.log("activateMarkdown if");

			$("<link/>", {
			   rel: "stylesheet",
			   type: "text/css",
			   href: baseUrl+"/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css"
			}).appendTo("head");
			// $.getScript( baseUrl+"/plugins/showdown/showdown.min.js", function( data, textStatus, jqxhr ) {

				$.getScript( baseUrl+"/plugins/bootstrap-markdown/js/bootstrap-markdown.js", function( data, textStatus, jqxhr ) {
					mylog.log("HERE", elem);

					$.fn.markdown.messages['fr'] = {
						'Bold': trad.Bold,
						'Italic': trad.Italic,
						'Heading': trad.Heading,
						'URL/Link': trad['URL/Link'],
						'Image': trad.Image,
						'List': trad.List,
						'Preview': trad.Preview,
						'strong text': trad['strong text'],
						'emphasized text': trad['strong text'],
						'heading text': trad[''],
						'enter link description here': trad['enter link description here'],
						'Insert Hyperlink': trad['Insert Hyperlink'],
						'enter image description here': trad['enter image description here'],
						'Insert Image Hyperlink': trad['Insert Image Hyperlink'],
						'enter image title here': trad['enter image title here'],
						'list text here': trad['list text here']
					};
					$(elem).markdown(markdownParams);
				});


			// });
		} else {
			mylog.log("activateMarkdown else");
			$(elem).markdown(markdownParams);
		}

		//$(elem).before(tradDynForm["syntaxmarkdownused"]);
		$(elem).before("<small class='block letter-light text-left'><i class='fa fa-info-circle'></i> "+tradDynForm["discovermarkdownsyntax"]+"</small>");
	},
	activateMarkdown2 : function(elem){
		/*
		Documentation
		https://github.com/sparksuite/simplemde-markdown-editor ou
		https://bestofvue.com/repo/sparksuite-simplemde-markdown-editor-vuejs-rich-text-editing
		*/
		var simpleMDEoptions = {
			
		}
		const loadmde = function(){
			var simplemde = new SimpleMDE({
				element: $(elem)[0],
				forceSync: true,
				spellChecker:false,
				toolbar: [
					{	
						name: "bold",
						action: SimpleMDE.toggleBold,
						className: "fa fa-bold",
						title: trad.Bold,
					},
					{	
						name: "italic",
						action: SimpleMDE.toggleItalic,
						className: "fa fa-italic",
						title: trad.Italic,
					},
					{	
						name: "strikethrough",
						action: SimpleMDE.toggleStrikethrough,
						className: "fa fa-strikethrough",
						title: trad.Strikethrough,
					},"|",
					{	
						name: "heading",
						action: SimpleMDE.toggleHeadingSmaller,
						className: "fa fa-header",
						title: trad.Heading,
					},
					// {	
					// 	name: "heading-1",
					// 	action: SimpleMDE.toggleHeading1,
					// 	className: "fa fa-header fa-header-x fa-header-1",
					// 	title: trad.BigHeading,
					// },
					// {	
					// 	name: "heading-2",
					// 	action: SimpleMDE.toggleHeading2,
					// 	className: "fa fa-header fa-header-x fa-header-2",
					// 	title: trad.MediumHeading,
					// },
					// {	
					// 	name: "heading-3",
					// 	action: SimpleMDE.toggleHeading3,
					// 	className: "fa fa-header fa-header-x fa-header-3",
					// 	title: trad.SmallHeading,
					// },
					"|",
					{	
						name: "code",
						action: SimpleMDE.toggleCodeBlock,
						className: "fa fa-code",
						title: "Code",
					},
					{	
						name: "quote",
						action: SimpleMDE.toggleBlockquote,
						className: "fa fa-quote-left",
						title: "Quote",
					},"|",
					{	
						name: "unordered-list",
						action: SimpleMDE.toggleUnorderedList,
						className: "fa fa-list-ul",
						title: trad.GenericList,
					},
					{	
						name: "ordered-list",
						action: SimpleMDE.toggleOrderedList,
						className: "fa fa-list-ol",
						title: trad.NumberedList,
					},"|",
					{	
						name: "link",
						action: SimpleMDE.drawLink,
						className: "fa fa-link",
						title: trad['Insert Hyperlink'],
					},
					{	
						name: "image",
						action: SimpleMDE.drawImage,
						className: "fa fa-picture-o",
						title: trad['Insert Image Hyperlink'],
					},
					// {	
					// 	name: "table",
					// 	action: SimpleMDE.drawTable,
					// 	className: "fa fa-table",
					// 	title: trad["Insert Table"],
					// },
					{	
						name: "horizontal-rule",
						action: SimpleMDE.drawHorizontalRule,
						className: "fa fa-minus",
						title: trad["Insert Horizontal Line"],
					},"|",
					{	
						name: "preview",
						action: SimpleMDE.togglePreview,
						className: "fa fa-eye",
						title: trad.Preview,
					},
					{	
						name: "side-by-side",
						action: SimpleMDE.toggleSideBySide,
						className: "fa fa-columns",
						title: trad['Toggle Side by Side'],
					},
					{	
						name: "fullscreen",
						action: SimpleMDE.toggleFullScreen,
						className: "fa fa-arrows-alt",
						title: trad["Toggle Fullscreen"],
					},
					{	
						name: "guide",
						action: ()=> window.open('https://simplemde.com/markdown-guide', '_blank'),
						className: "fa fa-question-circle",
						title: trad["Markdown Guide"],
					}
				],
			});
			simplemde.codemirror.on("blur", function(){
				$(elem).blur();
			});
			$(".stepNumber").parent().on('click',function(){
				setTimeout(() => {
					simplemde.codemirror.refresh(); 
				}, 300); 
			})
			return simplemde;
		}
		const loadFileAndMde = function(callBack){
			$("<link/>", {
				rel: "stylesheet",
				type: "text/css",
				href: baseUrl+"/plugins/simplemde-markdown-editor/simplemde.min.css"
			}).appendTo("head");
			$.getScript( baseUrl+"/plugins/simplemde-markdown-editor/simplemde.min.js", function( data, textStatus, jqxhr ) {
				callBack();
			});
		}

		if(typeof SimpleMDE == "undefined"){
			loadFileAndMde(function(){
				return loadmde();
			})
		}else{
			return loadmde();
		}

	},
	/*
	params.collection
	params.id
	params.path
	params.value
	*/
	path2Value : function ( params, callback, asyncParam, options={} )
	{
		var url = options.url ? option.url : baseUrl;
		asyncParam=(asyncParam==false) ? {async:false}  : {}  ;
		dyFObj.path2Value = {"params" : params};
		mylog.log( "path2Value", url+"/"+moduleId+"/element/updatepathvalue", params);
		if( !notEmpty(params.collection))
			alert("collection cannot be empty");
		else
			ajaxPost(
		        null,
				url+"/"+moduleId+"/element/updatepathvalue",
		        params,
		        function(data){ 
		          	mylog.log("success path2Value : ",data);
		    		dyFObj.path2Value.result = data;
			    	if(data.result){
						if(typeof callback == "function")
							callback(data);
						else{
							toastr.success(data.msg); 
							urlCtrl.loadByHash(location.hash);
						}
			    	}else{
			    		toastr.error(data.msg);
			    	}
		  		},
		  		function(data){
		  			toastr.error("Something went really bad ! Please contact the administrator.");
		  		},
		  		null,
		  		asyncParam
		  	); 
	},

	unsetPath : function(params,callback){
		dyFObj.unsetPath = {"params" : params};
		mylog.log( "unsetPath", baseUrl+"/"+moduleId+"/element/updatepathvalue", params);
		if( !notEmpty(params.collection))
			alert("collection cannot be empty");
		else if( !notEmpty(params.collection))
			alert("collection id cannot be empty");
		else
			ajaxPost(
		        null,
		        baseUrl+"/"+moduleId+"/element/unsetpath",
		        params,
		        function(data){ 
		          	mylog.log("success unsetPath : ",data);
		    		dyFObj.unsetPath.result = data;
			    	if(data.result){
						if(typeof callback == "function")
							callback(data);
						else{
							toastr.success(data.msg); 
							urlCtrl.loadByHash(location.hash);
						}
			    	}else{
			    		toastr.error(data.msg);
			    	}
		  		},
		  		function(data){
		  			toastr.error("Something went really bad ! Please contact the administrator.");
		  		}
		  	); 
	},
	deleteDocument: function(params) {
		return new Promise(function(resolve, reject) {
			var url, post;

			if (!params)
				reject('empty params');
			else if (!params.id)
				reject('empty id');
			else if (!params.collection)
				reject('empty collection');
			else {
				url = baseUrl+"/"+moduleId+"/element/delete/id/" + params.id + '/type/' + params.collection;
				ajaxPost(null, url, post, resolve, reject);
			}
		});
	},
	element_has_costum: function(args) {
		var url = baseUrl + "/co2/cms/costum_have_controller/id/" + args.id + "/type/" + args.type;
		var post = {};
		return new Promise(function(resolve) {
			ajaxPost(null, url, post, resolve);
		});
	},
	printf: function(inputString, replacement){
		if(typeof inputString !== 'string') {
			return '';
		}
		if(typeof replacement === 'undefined') {
			replacement = {};
		};
		return inputString.replace(/{{(.*?)}}/g, function(match, occurence1) {
			if(typeof replacement[occurence1] !== 'undefined') {
				return replacement[occurence1];
			}
			return match;
		});
	},
	getAllLanguageInDb: function() {
        var otherLanguages = costum.otherLanguages || [];
        var costumLanguage = costum.language || "fr";
        var allLanguagesInDb = [costumLanguage, ...otherLanguages];
        return allLanguagesInDb;
    },
	getDefaultColorCostum () {
        if (!window.costumizer || !costumizer.obj || !costumizer.obj.css || !costumizer.obj.css.color)
            return []

        return Object.values(costumizer.obj.css.color).map(function(value) {
            return value
        });
    },
	autoTranslateDeepl: function(textTotranslate, langTotranslate, callback) {
		let textTranslated = "";
		ajaxPost(
			null,
			baseUrl+"/co2/cms/textautotranslate",
			{
				text        : textTotranslate,
				lang_target : langTotranslate,
				costumId    : costum.contextId,
				costumType  : costum.contextType
			},
			function(data) {
				if (data.result) {
					textTranslated = data["words"];
					if (textTotranslate != "") {
						callback(null, textTranslated);
					} else {
						toastr.error("Une erreur s'est produite, le texte à traduire est vide !! Veuillez réessayer");
						callback(tradCms.somethingWrong, null);
					}
				} else {
					toastr.error(tradCms.somethingWrong);
					callback(tradCms.somethingWrong, null);
				}
			},
			null,
			"json"
		);
	},
	chatopenai: function(messages, callback, options = {}) {
		ajaxPost(
			null,
			baseUrl+"/co2/cms/chatopenai",
			{
				messages    : messages,
				options     : options,
				costumId    : costum.contextId,
				costumSlug  : costum.contextSlugW,
				costumType  : costum.contextType
			},
			function(data) {
				if (data.result === true) {
					callback(null, data.messages);
				} else {
					callback(tradCms.somethingWrong, null);
				}
			},
			function(error) {
				toastr.error(tradCms.somethingWrong);
				callback(tradCms.somethingWrong, null);
			},
			"json"
		);
	},
	generateImageOpenai: function(prompt, callback, options = {n: 1, size : "1024x1024"}) {
		ajaxPost(
			null,
			baseUrl+"/co2/cms/generateimageopenai",
			{
				prompt    : prompt,
				options   : options,
				costumId  : costum.contextId,
				costumSlug: costum.contextSlugW,
				costumType: costum.contextType
			},
			function(data) {
				if (data.result === true) {
					callback(null, data.responses);
				} else {
					callback(tradCms.somethingWrong, null);
				}
			},
			function(error) {
				toastr.error(tradCms.somethingWrong);
				callback(tradCms.somethingWrong, null);
			},
			"json"
		);
	},
	arrayReplaceRecursive: function(obj1, obj2) {
		let newObj = $.extend(true, {}, obj1);
		$.each(obj2, function(key, value) {
			if ($.isPlainObject(value) && newObj.hasOwnProperty(key)) {
				newObj[key] = dataHelper.arrayReplaceRecursive(newObj[key], value);
			} else if (!newObj.hasOwnProperty(key)) {
				newObj[key] = value;
			}
		});
		
		return newObj;
	}
}


function coTranslate(text) {
	if(typeof trad !== 'undefined' && typeof trad[text] === 'string')
		return trad[text];
	if(typeof tradCategory !== 'undefined' && typeof tradCategory[text] === 'string')
		return tradCategory[text];
	if(typeof tradCart !== 'undefined' && typeof tradCart[text] === 'string')
		return tradCart[text];
	else return text;
}
