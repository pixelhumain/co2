function classic_form_aac(args) {
    var self = this;
    const otContextForm = {
        jsonSchema: {
            title: "Créer un appel à commun",
            description: "",
            icon: "fa-question",
            properties: {
                name: {
                    inputType: "text",
                    label: "Nom de l'appel à commun",
                    rules: {
                        required: true
                    }
                },
                parent: {
                    inputType: "finder",
                    label: "Sélectionner un contexte",
                    initMe: false,
                    buttonLabel: "Rechercher un element",
                    placeholder: "Rechercher un element",
                    initContext: false,
                    initType: ["organizations", "projects"],
                    initBySearch: true,
                    initContacts: false,
                    value: typeof valueOrga != "undefined" ? valueOrga : {},
                    filters: {
                        '$or': {
                            ["links.members." + userId + ".isAdmin"]: {
                                "$exists": true
                            },
                            ["links.contributors." + userId + ".isAdmin"]: {
                                "$exists": true
                            },
                            ["costum.type"]: {
                                "$exists": false
                            }
                        }
                    },
                    multiple: false,
                    rules: {
                        required: true
                    },
                    noResult: { 
                        label: "Créez l'organisation",
                        action: function () {
                            var customForm = {
                                "beforeBuild" : {
                                    "properties" : {
                                        
                                    }    
                                }, 
                                "afterSave" : function(data) {
                                    console.log("data orga", data)
                                    valueOrga = {}
                                    valueOrga[data.id] = {
                                        _id: {
                                            $id: data.id
                                        },
                                        name: data.map.name,
                                        type: "organizations"
                                    };
                                }
                            };
                            var extendedForm=customForm;
                            dyFObj.openForm("organization",null,null, null, extendedForm);
                            $(".bootbox").modal('hide');
                        }
                    }
                }
            },
            onLoads: {
                onload: function() {

                }
            },
            beforeBuild: function() {},
            save: function(formData) {
                var today = new Date();
                delete formData.collection;
                delete formData.scope;
                args.callback(formData);
            }
        }
    }
    if (typeof contextId != "undefined" && typeof contextType != "undefined" && typeof contextName != "undefined") {
        self.contextData = {
            parent: {}
        }
        self.contextData["parent"][contextId] = {
            type: contextType,
            name: contextName
        };
    }
    dyFObj.openForm(otContextForm, null, self.contextData, null, null, {
        type: "bootbox"
    });
}

const ocecotoolsObj_aac = {
    init: function(otObj) {
        var copyOtObj = Object.assign({}, otObj);
        // copyOtObj.events(copyOtObj);
    },
    createAac: function(otObj) {
        otObj.formConfig(otObj, function(data) {
            if (typeof data.parent != "undefined") {
                otObj.generateOceco(otObj, data, "createAac");
            } else {
                bootbox.alert(trad["Choose context"]);
            }
        })
    },
    
    generateOceco: function(otObj, prms, action) {
        ajaxPost(
            null,
            baseUrl + "/co2/aap/generateformoceco/", {
                "elId": Object.keys(prms.parent)[0],
                "elType": prms.parent[Object.keys(prms.parent)[0]]["type"],
                "createOceco": true,
                "formName": prms.name,
                "action" : action
            },
            function(data) {
                if (typeof data["aap link"] != "undefined") {
                    window.open(data["aap link"], '_self');
                }
            }, null, null, {
                async: false
            }
        );
    },
    generateForm: function(otObj, prms, inputs) {
        ajaxPost(
            null,
            baseUrl + "/co2/aap/generateformoceco/", {
                "elId": Object.keys(prms.parent)[0],
                "elType": prms.parent[Object.keys(prms.parent)[0]]["type"],
                "createForm": true,
                "inputs": inputs,
                "formName": prms.name
            },
            function(data) {
                if (typeof data["config link"] != "undefined") {
                    window.open(data["config link"], '_self');
                    location.reload();
                }
            }, null, null, {
                async: false
            }
        );
    },
    formConfig: function(otObj, callback) {
        classic_form_aac.call(otObj, {
            label: "Nom du appel à commun",
            callback: callback
        })
    }
}
ocecotoolsObj_aac.init(ocecotoolsObj_aac);
var valueOrga = {}