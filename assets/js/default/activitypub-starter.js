var activitypubGuide = {
    constants: {
        urlViewGuide: "/co2/activitypub/getViewGuide",
        steps: {
            SETTING:0,
            COMMUNITY:1,
            SEARCH:2,
            ADD_MEMBER:3,
            ADD_TAGS:4,
            ACTIVITY:5
        },
        localStorageKeyPrefix: "__co_activitypub_guide_step_",
        testUser: "@oceatoon@mastodon.social"
    },
    interval:null,
    currentStep: {
        set step(step) {
            var key = activitypubGuide.constants.localStorageKeyPrefix + userId;
            if(step !== null) 
                localStorage.setItem(key, step);
            else
                localStorage.removeItem(key);
        },
        get step() {
            return localStorage.getItem(activitypubGuide.constants.localStorageKeyPrefix + userId);
        } 
    },
    views: {
        init: function() {
            return new Promise(function(resolve) {
                getAjax("", activitypubGuide.constants.urlViewGuide, function(html) {
                    $("body").append(html)
                    resolve(true)
                })
            })
        }
    },
    events: {
        init: function () {
            $("#ap-guide-carousel").carousel({
                interval:false
            })

            $(".ap-guide-btn-lbh").click(function() {
                urlCtrl.loadByHash($(this).data("hash"))
                activitypubGuide.actions.close();
            })

            $(".ap-guide-btn-search").click(function() {
                activitypubGuide.actions.close();

                var $input = $("#input-search-external-network");
                if($input.length > 0) {
                    $input.val(activitypubGuide.constants.testUser);
                    $input.trigger($.Event("keyup", { keyCode: 13 }));

                    activitypubGuide.interval = setInterval(function(){
                        if($(".external-network-item").length > 0) {
                            clearInterval(activitypubGuide.interval);
                            setTimeout(function(){
                                activitypubGuide.actions.open(activitypubGuide.constants.steps.ADD_MEMBER);
                            }, 1000)
                        }
                    }, 500)
                }
            })

            $(".ap-guide-btn-next").click(function() {
                activitypubGuide.actions.next();
            })

            $(".ap-guide-btn-finished").click(function() {
                activitypubGuide.actions.finished();
            })

            $(".ap-guide-btn-close").click(function(){
                activitypubGuide.actions.finished();
            })
        }
    },
    actions: {
        open: function(step = 0) {
            activitypubGuide.views.init().then(function() {
                activitypubGuide.currentStep.step = step;
                //
                $(".ap-guide-step").removeClass("active");
                $(".ap-guide-step:nth-child("+(step+1)+")").addClass("active");
                //
                $("body").addClass("overflow-hidden");
                //
                activitypubGuide.events.init();
            })
        },
        next: function () {
            $("#ap-guide-carousel").carousel('next')
            activitypubGuide.currentStep.step += 1;
        },
        close: function() {
            $(".ap-guide-wrapper").remove()
            $("body").removeClass("overflow-hidden");
            clearInterval(activitypubGuide.interval);
        },
        finished: function() {
            this.close();
            activitypubGuide.currentStep.step = null;
            activitypubStarter.actions.finished();
        }
    }
}

var activitypubStarter = {
    constants: {
        urlViewStarter: "/co2/activitypub/getViewStarter",
        urlCheckStarter: "/co2/activitypub/CheckStarter",
        urlCloseStarter: "/co2/activitypub/closeStarter"
    },
    views: {
        init: function() {
            return new Promise(function(resolve) {
                getAjax("", activitypubStarter.constants.urlViewStarter, function(html){
                    $("body").append(html);
                    resolve(true);
                })
            })
        }
    },
    events: {
        init: function() {
            $(".ap-starter-btn-start-guide").click(function() {
                activitypubStarter.actions.close();
                activitypubGuide.actions.open();
            })

            $(".ap-starter-btn-close").click(function(){
                activitypubStarter.actions.close();
            })

            $(".ap-starter-btn-finished").click(function(){
                activitypubStarter.actions.finished();
            })
        }
    },
    actions: {
        open: function() {
            if(costum || !["", "#welcome", "#home"].includes(location.hash) || !userId)
                return;
            getAjax("", activitypubStarter.constants.urlCheckStarter, function(res) {
                if(res.open) {
                    activitypubStarter.views.init().then(function() {
                        activitypubStarter.events.init();
                    })
                }
            })
        },
        close: function() {
            if($(".co-popup-activitypub-container").length > 0)
                $(".co-popup-activitypub-container").remove();
        },
        finished: function() {
            this.close();
            ajaxPost("", activitypubStarter.constants.urlCloseStarter);
        }
    }
}