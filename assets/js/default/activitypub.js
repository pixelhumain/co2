var activitypub = {
    services:{
        link:{
            update:function(action, payload,actor, callback){
                $.post(`${baseUrl}/api/activitypub/link`, {action:action, payload:payload,actor: actor}, function(data){
                    callback(data)
                })
            },
            get: function(userId, linkType, actor ,  callback){
                $.get(`${baseUrl}/api/activitypub/getcommunity/userId/${userId}/type/${linkType}/actor/${actor}`, function(data){
                    callback(data);
                })
            },
            getTotalPending: function(userId, linkType, actor, callback){
                activitypub.services.link.get(userId, linkType,actor, function(actors){
                    var total = 0;
                    actors.forEach(function(actor){
                        if(actor.link[linkType] && actor.link[linkType].pending)
                            total++;
                    })
                    callback(total)
                })
            }
        },
        search:function(ressource, callback){
            $.get(`${baseUrl}/api/activitypub/search?address=${ressource}`, function(data){
                callback(data);
            })
        }
    }
}