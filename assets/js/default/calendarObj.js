/* 
	TODO : Continuer la configuration des agendas 
	[] Structurer l'objet et arrivé à une bêta stable et configurable
	[] Documenter l'objet calendar
	[] Comportement de recherche option de passer par le métier si toutes la data n'est pas déjà chargée
	[] Continuer la configuration des calendrier en rendu sur les options de fullcalendar 
	[] Ajouter le l'ajout d'evenement au click ou au slide sur l'agenda personnel 
	[] Recoller le chantier des réservations (terla)
*/
		
var calendarObj = {
	container : "#calendar",
	eventsList : {},
	params : {
		startDate : null,
		endDate : null
	},
	options : {
		initDate : new Date(),
		header : {
			left : 'prev,next',
			center : 'title',
			right : 'today, month'
		},
		fixedWeekCount:false,
		height: 'auto',
		lang : mainLanguage,
		//gotoDate:moment(initDate),
		editable : false,
		eventBackgroundColor: '#FFA200',
		textColor: '#fff',
		defaultView: 'month',
		 initialView: 'listWeek',
		events : [],
		eventLimit: 0,
		timezone : 'local',
		personalAgenda : false
	},
	init : function(pInit = null){
		mylog.log("calendarObj.init",pInit);
		//Init variable
		var copyCalendar = jQuery.extend(true, {}, calendarObj);
		copyCalendar.initVar(pInit);
		return copyCalendar;

	},

	initVar : function(pInit){
		mylog.log("calendarObj.initVar",pInit);
		if(pInit != null && typeof pInit.container != "undefined")
			this.container = pInit.container;
		if(typeof pInit.options != "undefined")
			$.extend(this.options, pInit.options );
		if(typeof pInit.searchParams != "undefined")
			$.extend(this.params, pInit.searchParams );
		this.initDefaults(pInit);
		this.initViews(pInit);
		this.initEvents(pInit);

	},
	initDefaults : function(pInit){
		mylog.log("calendarObj.initDefaults",pInit);
		var str = "";
		var fObj = this;
		//INIT DATE IF NOT CURRENT
		if(typeof fObj.options.initDate == "string"){
			var splitInit = fObj.options.initDate.split("-");
			fObj.options.initDate = new Date(splitInit[0], splitInit[1]-1, splitInit[2]);
		}
		else{
			initDate = null;
			dateToShow = new Date();
		}
		fObj.options.year = fObj.options.initDate.getFullYear();
		fObj.options.month = fObj.options.initDate.getMonth();
		fObj.options.date = fObj.options.initDate.getDate();
		//fObj.calculateAgendaWindow(fObj);	

		if(pInit != null && typeof pInit.searchObj != "undefined")
			fObj.searchObj =  pInit.searchObj;
	},
	initViews : function(pInit){
		mylog.log("calendarObj.initViews",pInit);
		var str = '';
		var fObj = this;
		fObj.options.eventClick = function (calEvent, jsEvent, view) {
			 mylog.log("calendarObj.eventClick", calEvent, jsEvent, view);
			onchangeClick=false;
			var link = "#page.type.events.id."+calEvent.id;
			var hashT=location.hash.split("?");
			var getStatus=urlCtrl.getUrlSearchParams();       
			var hashNav=(hashT[0].indexOf("#") < 0) ? "#"+hashT[0] : hashT[0];
			var urlHistoric=hashNav+"?preview=events."+calEvent.id;
			if(getStatus != "") urlHistoric+="&"+getStatus; 
			history.replaceState({}, null, urlHistoric);
			urlCtrl.openPreview(link);
		}
		fObj.options.eventLimitClick = function(info){
			 mylog.log(info.date, "here bidi");
			var dateMore=info.date._d;
			var datestring = dateMore.getFullYear()+"-"+("0"+(dateMore.getMonth()+1)).slice(-2)+"-"+("0" + dateMore.getDate()).slice(-2) ;
			$(fObj.container+" .fc-day").removeClass("activeDay");
			$(fObj.container+".fc-day[data-date='"+datestring+"']").addClass("activeDay");
			fObj.searchInAgenda(fObj, datestring, "day");
		}
		fObj.options.eventRender = function(fObj,event,element,view) {
			//mylog.log("calendarObj.options.eventRender event",event,"element",element, "element", view);
			/*if(fObj.options.popupRender){
				var popupHtml=fObj.popupHtml(fObj,event);
				element.popover({
					html:true,
					animation: true,
					container:'body',
					title: event.name,
					template:fObj.popupTemplate(),
					placement: 'top',
					trigger: 'focus',
					content: popupHtml,
				});
				element.attr('tabindex', -1);
			}*/
		}
		fObj.options.dayClick = function(date, jsEvent, view) {
			mylog.log("calendarObj.init.dayClick", date, jsEvent, view);
			// change the day's background color just for fun
			$(fObj.container+" .fc-day").removeClass("activeDay");
			$(this).addClass("activeDay");
			
			fObj.params.starDate = moment(date.format()).valueOf();
            //if(typeof fObj.searchObj != "undefined"){
            //	fObj.searchObj.agenda.options.dayCount = 0;
			//	fObj.searchObj.agenda.options.startDate = fObj.params.starDate;
            //}
            mylog.log("calendarObj.init.dayClick fObj.params.starDate", fObj.params.starDate);
			fObj.searchInAgenda(fObj, date.format(), "day");
			// alert('nextis clicked, do something');
		}
		$(fObj.container).fullCalendar('destroy');
		$(fObj.container).fullCalendar(fObj.options);
		$(fObj.container).fullCalendar("gotoDate", moment(Date.now()));
		fObj.bindEventCalendar(fObj);

	},
	initEvents : function(pInit){
		mylog.log("calendarObj.initActions",pInit);
		var str = "";
		var fObj = this;
		var startDate=moment(fObj.params.starDate).set("month", moment(fObj.params.starDate).get("month")).valueOf();
		var endDate = moment(fObj.params.starDate).set("month", moment(fObj.params.starDate).get("month")+1).valueOf();
        var secondEndDate = Math.floor(endDate / 1000);
        var sendStartDate=Math.floor(startDate / 1000);
        fObj.searchInCalendar(fObj, sendStartDate, secondEndDate);	
	},
	events : [],
	tabOrganiser : [],
	showPopup : false,
	results : {
		add : function(fObj,events){
			mylog.log("calendarObj.results.add", events);
			if(events){
				$.each(events,function(eventId,eventObj){
					if(typeof fObj.eventsList[eventId]=="undefined"){
						
						var eventCal = fObj.results.convert(fObj,eventObj);
						mylog.log("calendarObj.results.add eventCal", eventCal);
						if(bool=true){
							if(eventCal){
								fObj.eventsList[eventId] = eventCal ;
								$(fObj.container).fullCalendar('renderEvent',eventCal, true);
							}
						}
					}
				});
			};
		},

		//creates fullCalendar
		convert : function(fObj,eventObj) {
			mylog.log("calendarObj.results.convert eventObj", eventObj);
			//entries for the calendar
			var organiser = "";
			if("undefined" != typeof eventObj["links"] && "undefined" != typeof eventObj.links["organizer"]){
				$.each(eventObj.links["organizer"], function(k, v){
					if($.inArray(k, fObj.tabOrganiser)==-1)
						fObj.tabOrganiser.push(k);
					organiser = k;
				});
			}

			var organizerName = eventObj.name;
			if(eventObj.organizer != "")
			organizerName = eventObj.organizer +" : "+ eventObj.name;
			
			var taskCal = null;
			taskCal = {
					"id" : (typeof eventObj.id != "undefined") ? eventObj.id : eventObj._id.$id ,
					"title" : eventObj.name,
					"content" : (eventObj.description && eventObj.description != "" ) ? eventObj.description : "",
					"start" : null,
					"end" : null,
					//"end" : ( endDate ) ? endDate : startDate,
					"startDate" : eventObj.startDate,
					"endDate" : eventObj.endDate,
					"className": organiser,
					"category": organiser,
					"type": eventObj.type,
					"description":eventObj.description,
					"shortDescription": eventObj.shortDescription,
					"profilMediumImageUrl": eventObj.profilMediumImageUrl,
					"adresse": eventObj.cityName,
					"links":eventObj.links,
					"dow": null,
				};
				if(taskCal.startDate < fObj.results.startDate)
				if(	typeof costum != "undefined" && costum != null &&
	            	typeof costum.calendar != "undefined" && costum.calendar != null &&
            		typeof costum.calendar.convert == "function")
            		taskCal = costum.calendar.convert(eventObj, taskCal);

            var bool = null ;	
			if(typeof eventObj.startDate != "undefined") {
				bool =true ;	
				mylog.log("calendarObj.results.convert eventObj.startDate",eventObj.startDate);
				var startDate = moment(eventObj.startDate).local().format();
				var startTimestamp=new Date(startDate).getTime();
				taskCal["start"] = (startTimestamp < fObj.results.startDate) ?  new Date(fObj.results.startDate*1000): startDate;
				var endDate = null;
				if(eventObj.endDate && eventObj.endDate != "undefined" ){
					mylog.log("calendarObj.results.convert eventObj.endDate",eventObj.endDate);
					var endDate = moment(eventObj.endDate).local().format();
					var endTimestamp=new Date(endDate).getTime();
					taskCal["end"] = (endTimestamp < fObj.results.endDate) ?  new Date(fObj.results.endDate*1000): endDate;
				}
				else 
					taskCal["end"]=taskCal["start"];
				
				mylog.log("calendarObj.results.convert taskCal", taskCal);
				return taskCal;
				//mylog.log("calendarObj.results.convert eventObj", eventObj, startDate);	
			}
			else {
				mylog.log("calendarObj.results.convert eventObj.openingHours",eventObj.openingHours);
				var openingHours = eventObj.openingHours; 
					$.each(openingHours, function(e,v){
						bool = false;
						if(v!=""){
							if(typeof v.hours !="undefined"){
								$.each(v.hours,function(i,value){
									if(e !=6){	
										var d = null;
										d = e + 1 ;			
									}
									else
										d = e - 6 
									var taskCal2 = {};
									taskCal2["dow"]=[d];
									taskCal2["start"]=value.opens;
									taskCal2["end"]=value.closes;
									mylog.log("taskcal",taskCal2);
									if(value.closes=="00:00")
										value.closes="24:00";
									var multiple = $.extend(true,taskCal,taskCal2);
									$(fObj.container).fullCalendar('renderEvent',multiple, false);
								});
							}
						}							
					});	
			}
		}
	},
	
	//dateToShow, fObj, $eventDetail, eventClass, eventCategory,
	widgetNotes : $('#notes .e-slider'), 
	sliderNotes : $('#readNote .e-slider'), 	

	bindEventCalendar : function(fObj){
		var fObj = this;
		$(".fc-today").addClass(".activeDay");
		 mylog.log("calendarObj.bindEventCalendar");
		//var popoverElement;
		$(fObj.container+' .fc-prev-button').click(function(){
			var now = $(fObj.container).fullCalendar('getDate');
			mylog.log("calendarObj.bindEventCalendar .fc-next-button now", now, now.format());
			fObj.params.starDate = moment(now.format()).valueOf();
            /*if(typeof fObj.searchObj != "undefined"){
            	fObj.searchObj.agenda.options.dayCount = 0;
				fObj.searchObj.agenda.options.startDate = fObj.params.starDate;
            }*/
			fObj.searchInAgenda(fObj, now.format(), "month");
		});

		$(fObj.container+' .fc-next-button').click(function(){
			var now = $(fObj.container).fullCalendar('getDate');
			
			fObj.params.starDate = moment(now.format()).valueOf();
			/*if(typeof fObj.searchObj != "undefined"){
				fObj.searchObj.agenda.options.dayCount = 0;
				fObj.searchObj.agenda.options.startDate = fObj.params.starDate;
			}*/
        	fObj.searchInAgenda(fObj, now.format(), "month");
		});
		$('.popover').mouseenter(function(){
			$(this).hide();
		});
		$('body').on('click', function (e) {
			if (typeof popoverElement != "undefined" && popoverElement 
			&& ((!popoverElement.is(e.target) && popoverElement.has(e.target).length === 0 && $('.popover').has(e.target).length === 0) 
			|| (popoverElement.has(e.target) && e.target.id === 'closepopover'))) {    
				fObj.closePopovers();
			}
		});
	
	},
	templateRef : {
		"competition":"#ed553b",
		"concert" :"#b45f04",
		"contest":"#ed553b",
		"exhibition":"#b45f04",
		"festival":"#b45f04",
		"getTogether":"#eb4124",
		"market":"#df01a5",
		"meeting":"#eb4124",
		"course":"#df01a5",
		"workshop":"#eb4124",
		"conference":"#0073b0",
		"debate":"#0073b0",
		"film":"#2e2e2e",
		"crowdfunding":"#93be3d",
		"others":"#93be3d",
	},
	closePopovers : function() {
		var fObj = this;
		mylog.log("calendarObj.closePopovers ");
		fObj.showPopup=false;
		$('.popover').not(this).popover('hide');
	},
	
	
	
	popupTemplate : function(){
			//mylog.log("calendarObj.popupTemplate ");
			var template='<div class="popover" style="max-width:300px; no-padding" >'+
						'<div class="arrow"></div>'+
						'<div class="popover-header" style="background-color:red;">'+
						'<button id="closepopover" type="button" class="close margin-right-5" aria-hidden="true">&times;</button>'+
						'<h3 class="popover-title"></h3>'+
						'</div>'+
						'<div class="popover-content no-padding"></div>'+
						'</div>';
			return template;
	},
	popupHtml : function(fObj,data){
		//mylog.log("calendarObj.popupHtml ", data);
		var popupContent = "<div class='popup-calendar'>";

		var color = "orange";
		var ico = 'calendar';
		var imgProfilPath =  assetPath + "/images/thumb/default_events.png";
		if(typeof data.profilMediumImageUrl !== "undefined" && data.profilMediumImageUrl != "") 
			imgProfilPath =  baseUrl + data.profilMediumImageUrl;
		var icons = '<i class="fa fa-'+ ico + ' text-'+ color +'"></i>!!!';

		var typeElement = "events";
		var icon = 'fa-calendar';

		var onclick = "";
		var url = '#page.type.'+typeElement+'.id.'+data.id;
		//onclick = 'fObj.closePopovers();urlCtrl.loadByHash("'+url+'");';

		popupContent += "<div class='' id='popup"+data.id+"'>";
		popupContent += "<div class='main-panel'>"
						+   "<div class='col-md-12 col-sm-12 col-xs-12 no-padding'>"
						+      "<div class='thumbnail-profil' style='max-height: 200px;text-align: -webkit-center; overflow-y: hidden;background-color: #cccccc;'><img src='" + imgProfilPath + "' class='popup-info-profil-thumb img-responsive'></div>"      
						+   "</div>"
						+   "<div class='col-md-12 col-sm-12 col-xs-12 padding-5'>";

		if("undefined" != typeof data.title)
			popupContent  +=  "<div class='' style='text-transform:uppercase;'>" + data.title + "</div>";

		if(data.start != null){
			popupContent +="<div style='color:#777'>";
			var startLbl="<i class='fa fa-calendar-o'></i> ";
			var startDate=moment(data.start).format("DD MMMM YYYY"); 
			var endDate="";
			var hoursStr="<br/>";
			if(data.allDay)
				hoursStr+=tradDynForm.allday;
			else
				hoursStr+="<i class='fa fa-clock-o'></i> "+moment(data.start).format("H:mm");
			if(data.end != null){
				if(startDate != moment(data.end).format("DD MMMM YYYY")){
					startLbl+=trad.fromdate+" ";
					endDate=" "+trad.todatemin+" "+moment(data.end).format("DD MMMM YYYY");
				}
				if(!data.allDay)
					hoursStr+= " - "+moment(data.end).format("H:mm");
			}
			popupContent += startLbl+startDate+endDate+hoursStr;
			popupContent +="</div>";
		}
		popupContent += "</div>";
		//Short description
		if ("undefined" != typeof data['shortDescription'] && data['shortDescription'] != "" && data['shortDescription'] != null) {
			popupContent += "<div id='pop-description' class='popup-section'>"
			  + "<div class='popup-info-profil'>" + data['shortDescription'] + "</div>"
			+ "</div>";
		}
		popupContent += '</div>';

		popupContent += "<a href='"+url+"' target='_blank' onclick='"+onclick+"' class=''>";
		popupContent += '<div class="btn btn-sm btn-more col-md-12 col-sm-12 col-xs-12"><i class="fa fa-hand-pointer-o"></i> en savoir +</div>';
		popupContent += '</a>';

		return popupContent;
	},
	searchInAgenda : function(fObj, date, format){
		mylog.log("calendarObj.searchInAgenda", date, format);
		var today = new Date();
		var start = new Date(); 
		//     toTimestamp(strDate){
		var stringDate=new Date(date);
		var labelStr;
		/*if(format=="day"){
			fObj.search.startDate=(Date.parse(date)/1000);
			fObj.search.endDate=(Date.parse(date+" 23:59:59")/1000);

			if(date==today){
				labelStr=trad.today;
			}else{
				labelStr=directory.getWeekDayName(new Date(date).getDay())+" "+stringDate.getDate()+' '+directory.getMonthName(stringDate.getMonth()+1);
			}
		} else {*/
			labelStr=directory.getMonthName(stringDate.getMonth()+1)+" "+stringDate.getFullYear();
			fObj.params.startDate=(Date.parse(date)/1000);
			var endDate = new Date(stringDate.getFullYear(), stringDate.getMonth() + 1, 0);
			fObj.params.endDate=(Date.parse(endDate)/1000);
			fObj.searchInCalendar(fObj, fObj.params.startDate, fObj.params.endDate);
		//}

		
		//$("#situate-day .date-label").text(labelStr);
	//	coInterface.scrollTo(".dayEvent");
		//fObj.paramsObj.search.obj.nbPage=0;
		//pageCount=false;
		//fObj.searchObj.search.obj.count=true;
	//	directory.scrollTop = false;
	//	fObj.searchObj.agenda.options.noResult=false;
	//	fObj.searchObj.agenda.options.finishSearch=false;
		//fObj.searchObj.agenda.options.dayCount=0;
	//	fObj.searchObj.agenda.options.todayDate = fObj.searchObj.search.obj.startDate * 1000;
	//	fObj.searchObj.search.init(fObj.searchObj);
	},
	searchInCalendar : function(fObj, startDate, endDate){
		mylog.log("calendarObj.searchInCalendar", fObj, startDate, endDate);
		//if(typeof fObj.searchObj != "undefined"){
			var paramsSearchCalendar= {};//fObj.searchObj.search.constructObjectAndUrl(fObj.searchObj,true);
			if (typeof fObj != "undefined" 
				&& typeof fObj.searchObj != "undefined" 
				&& typeof fObj.searchObj.pInit != "undefined"
				&& typeof fObj.searchObj.pInit.defaults != "undefined"
				&& typeof fObj.searchObj.pInit.defaults.sourceKey != "undefined") {
				paramsSearchCalendar.sourceKey = fObj.searchObj.pInit.defaults.sourceKey;
			}
			$.extend(paramsSearchCalendar, fObj.params);
			if(fObj.options.personalAgenda === false)
			ajaxPost(
		        null,
		        baseUrl+"/"+moduleId+"/search/geteventsforcalendar/startDate/" + startDate + "/endDate/" + endDate,
		        paramsSearchCalendar,
		        function(data){ 
		           	mylog.log("calendarObj.searchInCalendar success", data);
					if(!data){ 
						toastr.error(data.content); 
					} else {
						if(fObj.options.personalAgenda === false)
							fObj.results.add(fObj, data.events);
					}
		  		},
		  		function (data){
					mylog.log("calendarObj.searchInCalendar error", data);
				}
		    );
		 //}
	}/*,
	calculateAgendaWindow: function(fObj){
		mylog.log('calendarObj.calculateAgendaWindow');
		var today = new Date();
		var todayMoment = moment().seconds(0).minute(0).hour(0);
		today = new Date(today.setSeconds(0));
		today = new Date(today.setMinutes(0));
		today = new Date(today.setHours(0));
		fObj.params.starDate = today.setDate(today.getDate());
		searchObject.startDate = Math.floor(fObj.params.starDate / 1000);

	}*/
};
