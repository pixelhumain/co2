/* eslint-disable no-global-assign */
/* eslint-disable no-mixed-spaces-and-tabs */
/* eslint-disable quotes */
/* eslint-disable indent */
/* global $, moment, toastr, bootbox, ajaxPost, baseUrl, moduleId, userId, assetPath, modules, domainName, myContacts, mentionsInit, collection, parentModuleUrl, trad, tradCategory, tradDynForm, smallMenu, dataHelper, typeObj, themeParams, onchangeClick, directoryViewMode, urlCtrl, links, contextData, dyFInputs, dyFObj, uiCoop, agenda, STARTDATE, showMap, costum, mapCO, isMapEnd, pageCount, spinSearchAddon, searchObject, searchAllEngine, searchInterface, interopSearch, isCustom, co, coInterface, mylog, notNull, notEmpty, checkAndCutLongString, linkify, escapeHtml, updateContact, personCOLLECTION, getObjectId, networkJson, getTypeInteropData, slugify, addslashes, inArray, myMultiTags, myScopes, mainLanguage, toggle */
/*
searchObject, searchAllEngine, searchInterface, interopSearch, isCustom modules/co2/assets/js/default/search.js
co, coInterface, mylog, linkify modules/co2/assets/js/co.js
mylog, notNull, inArray, toggle, checkAndCutLongString pixelhumain/ph/js/api.js
costum pixelhumain/ph/themes/CO2/views/layouts/initJs.php && modules/co2/views/custom/init.php
trad, tradCategory, tradDynForm modules/co2/views/translation/trad.php
mapCO pixelhumain/ph/themes/CO2/views/layouts/mainSearch.php
isMapEnd pixelhumain/ph/themes/CO2/views/layouts/initJs.php
baseUrl, moduleId pixelhumain/ph/themes/CO2/views/layouts/initJs.php && ...
agenda modules/co2/assets/js/default/calendar.js
themeParams, onchangeClick pixelhumain/ph/themes/CO2/views/layouts/initJs.php
showMap modules/co2/assets/js/default/index.js
spinSearchAddon modules/co2/assets/js/default/globalsearch.js
pageCount modules/co2/views/app/search.php && ...
STARTDATE modules/co2/views/app/search.php && ...
links modules/co2/assets/js/links.js
contextData modules/co2/views/element/profilSocial.php && ...
dataHelper, getObjectId modules/co2/assets/js/dataHelpers.js
uiCoop modules/co2/assets/js/cooperation/uiCoop.js && modules/dda/assets/js/uiCoop.js
urlCtrl modules/co2/assets/js/co.js && code/pixelhumain/ph/themes/CO2/assets/js/coController.js ...
userId, directoryViewMode pixelhumain/ph/themes/CO2/views/layouts/initJs.php
smallMenu modules/co2/assets/js/co.js && modules/news/assets/js/init.js
notEmpty modules/co2/assets/js/co.js && pixelhumain/ph/js/api.js
dyFInputs, dyFObj pixelhumain/ph/plugins/jquery.dynForm.js
myContacts, parentModuleUrl, assetPath, modules, domainName, mainLanguage, myScopes pixelhumain/ph/themes/CO2/views/layouts/initJs.php
collection modules/co2/assets/js/co.js && ...
mentionsInit modules/co2/assets/js/co.js && ...
escapeHtml modules/co2/assets/js/co.js && modules/news/assets/js/init.js
typeObj pixelhumain/ph/themes/CO2/assets/js/typeObj.js
updateContact modules/co2/assets/js/default/editInPlace.js && modules/co2/views/pod/contactsList.php
personCOLLECTION modules/co2/views/default/directory.php && modules/co2/views/element/profilSocial.php && ...
networkJson pixelhumain/ph/themes/network/views/layouts/mainSearch.php
getTypeInteropData modules/co2/assets/js/interoperability/interoperability.js
slugify pixelhumain/ph/js/api.js && pixelhumain/ph/plugins/jquery.dynForm.js
myMultiTags pixelhumain/ph/themes/CO2/views/layouts/scopes/multi_tag.php && ...
*/

var indexStepInit = 30;
var indexStep = indexStepInit;
var currentIndexMin = 0;
var currentIndexMax = indexStep;
var scrollEnd = false;
var totalData = 0;

var timeout = null;
//WARNING searchType est utilisé dans search.js
var searchType = '';
var searchSType = '';
var pageEvent = false;
var loadingData = false;
var mapElements = new Array();
var favTypes = [];
//var searchPage = 0;
var translate = {
	'organizations': 'Organisations',
	'projects': 'Projets',
	'events': 'Événements',
	'people': 'Citoyens',
	'followers': 'Ils nous suivent'
};

function startSearch(indexMin, indexMax, callBack, notUrl) {
	searchAllEngine.searchInProgress = true;
	mylog.log('directory.js startSearch agenda.', indexMin, indexMax);
	mylog.log('directory.js startSearch searchObject.types agenda.', searchObject.types);
	if (typeof searchObject.source != 'undefined') {
		interopSearch(searchObject.source, searchObject.source);
	} else {
		if (searchObject.text.indexOf('co.') === 0) {
			var searchT = searchObject.text.split('.');
			if (searchT[1] && typeof co[searchT[1]] == 'function') {
				co[searchT[1]](searchObject.text);
				return;
			} else {
				co.mands();
			}
		}
		if (location.hash.indexOf('#live') < 0) {
			//  startNewsSearch(true);
			//else{

			mylog.log('directory.js startSearch agenda. callBack', typeof callBack, loadingData);

			if (loadingData) return;
			loadingData = true;
			//scrollH= 0;
			//MAPREMOVE
			//showIsLoading(true);

			mylog.log('directory.js startSearch agenda. searchObject.indexMin', searchObject.indexMin, indexMax, searchObject.indexStep, searchObject.types);
			searchObject.indexMin = (typeof indexMin == 'undefined') ? 0 : indexMin;
			autoCompleteSearch(indexMin, indexMax, callBack, notUrl);
		}
	}

}




function initUiCoopDirectory() {

	$.each($('.searchEntity.coopPanelHtml .descMD'), function () {
		$(this).html(dataHelper.markdownToHtml($(this).html()));
	});

	$('.btn-openVoteDetail').off().click(function () {
		var coopId = $(this).data('coop-id');
		$('.all-coop-detail-votes' + coopId).toggleClass('hide');
		$('.all-coop-detail-desc' + coopId).toggleClass('hide');
	});

	$('.btn-coopfilter').off().click(function () {
		$('.coopFilter').addClass('hidden');
		$('.' + $(this).data('filter') + 'Filter').removeClass('hidden');
		directory.sortSearch('.coopFilter:not(.hidden)');
	});

	$('.openCoopPanelHtml').off().click(function () {
		mylog.log('HERE .openCoopPanelHtml');
		var coopType = $(this).data('coop-type');
		var coopId = $(this).data('coop-id');
		var idParentRoom = $(this).data('coop-idparentroom');
		var parentId = $(this).data('coop-parentid');
		var parentType = $(this).data('coop-parenttype');
		var afterLoad = null;
		var hashT = location.hash.split('?');
		var getStatus = searchInterface.getUrlSearchParams();
		var urlHistoric = hashT[0].substring(0) + '?preview=' + coopType + '.' + coopId;
		if (getStatus != '') urlHistoric += '&' + getStatus;
		history.replaceState({}, null, urlHistoric);
		//onchangeClick=false;
		//location.hash="preview="+coopType+"."+coopId;//+"."+parentId+"."+parentType;
		uiCoop.prepPreview(coopType, coopId, idParentRoom, parentId, parentType, afterLoad);
		if ($(this).data('coop-section')) {
			var coopSection = $(this).data('coop-section');
			if (coopSection == 'amendments') {
				afterLoad = function () {
					uiCoop.showAmendement(true);
					$("#modal-preview-coop #amendement-container").css("top", $("#mainNav").outerHeight() + coInterface.menu.initTopPostion);
				};
			} else if (coopSection == 'vote') {
				mylog.log('.openCoopPanelHtml vote');
				afterLoad = function () { };
			}
			else if (coopSection == 'comments') {
				afterLoad = function () {
					setTimeout(function () {
						$('#coop-container').animate({
							scrollTop: $('.btn-select-arg-comment').offset().top
						}, 1000);
					}, 1000);

				};
			}
		}
		coopType = coopType == 'actions' ? 'action' : coopType;
		coopType = coopType == 'proposals' ? 'proposal' : coopType;
		coopType = coopType == 'resolutions' ? 'resolution' : coopType;



		if (notNull(contextData) && contextData.id == parentId && contextData.type == parentType && typeof isOnepage == 'undefined' && idParentRoom != '') {
			toastr.info(trad['processing']);
			uiCoop.startUI();
			$('#modalCoop').modal('show');
			onchangeClick = false;
			if (coopType == 'rooms') {
				uiCoop.getCoopData(contextData.type, contextData.id, 'room', null, coopId);
			} else {
				setTimeout(function () {
					uiCoop.getCoopData(contextData.type, contextData.id, 'room', null, idParentRoom,
						function () {
							toastr.info(trad['processing']);
							uiCoop.getCoopData(contextData.type, contextData.id, coopType, null, coopId);
						}, false);
				}, 1000);
			}
		} else {
			if (coopType == 'rooms') {
				var hash = '#page.type.' + parentType + '.id.' + parentId +
					'.view.coop.room.' + idParentRoom + '.' + coopType + '.' + coopId;
				urlCtrl.loadByHash(hash);
			} else {
				uiCoop.getCoopDataPreview(coopType, coopId, afterLoad);
			}
		}
	});


	$('.timeline-panel .btn-send-vote').off().click(function () {
		var idParentProposal = $(this).data('idparentproposal');
		var voteValue = $(this).data('vote-value');
		var idParentRoom = $(this).data('idparentroom');
		mylog.log('send vote', voteValue);
		uiCoop.sendVote('proposal', idParentProposal, voteValue, idParentRoom, null, true);
	});
	document.querySelectorAll('#btn-activity-link').forEach(btn => {
		btn.addEventListener('click',function(){
			var link = this.getAttribute('data-activity-link');
			redirigerVersPage(link);
		})
	});
}
function isValidUrl(urlString) {
	var urlPattern = new RegExp('^(https?:\\/\\/)?' + // validate protocol
		'((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // validate domain name
		'((\\d{1,3}\\.){3}\\d{1,3}))' + // validate OR ip (v4) address
		'(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // validate port and path
		'(\\?[;&a-z\\d%_.~+=-]*)?' + // validate query string
		'(\\#[-a-z\\d_]*)?$', 'i'); // validate fragment locator
	return !!urlPattern.test(urlString);
}
function redirigerVersPage(url) {
	window.open(url, '_blank');
}

function isFromActivityPub(params) {
	return params.fromActivityPub != undefined && params.fromActivityPub != null ? params.fromActivityPub : false;
}
function activityPubObjectId(params) {
	return params.objectId != undefined && params.objectId != null ? params.objectId : "";
}
function activityPubAction(actionType,collection) {
	if(collection == 'events'){
	  return actionType == "follow" ? "joinevent" : "leaveevent";
	}else if(collection = 'projects'){
	  return actionType == "follow" ? "followProject" : "unfollowProject";
	}else{
	  return '';
	}
	
}
function getDomain(url) {
	const domainRegex = /^(?:https?:\/\/)?(?:[^@\n]+@)?(?:www\.)?([^:\/\n]+)/im;
	const matches = url.match(domainRegex);
	if (matches && matches.length >= 2) {
		return matches[1];
	}
	return null;
}
function checkLocalActor(params) {
	var showLocalIcon = false;
	var idRegex = /^[a-zA-Z0-9]+$/;
	  if (idRegex.test(params.id)) {
	    showLocalIcon = true;
	}
	return showLocalIcon
  }
  function getContributionData(contributors, userId) {
	var contributionData = 0;
	if (Array.isArray(contributors)) {
	  contributors.forEach(contributor => {
		if (typeof contributor.invitorId !== 'undefined') {
		  if (contributor.invitorId == userId) {
			contributionData = contributor;
		  }
		}
	  });
	}
	return contributionData;
  }
  /**
   * la fonction permet de savoir si un acteur activitypub a le droit d'editer  celui ci permet de reassigner le droit 
   * d'administration des  contributeurs (roles, privileges d'un utilisateur))
   * @param {*} contributors  list des contributeurs du projet
   * @param {*} userId  l'identification activitypub de l'utilisateur (ex: https://xxxxx.app/api/activitypub/users/u/xxx)
   * @returns true | false
   */
  function isActorCanEdit(contributors, userId) {
	var canEdit = false;
	var contributorData = getContributionData(contributors, userId);
	if (typeof contributorData != "undefined" && contributorData != null) {
	  canEdit = (typeof contributorData.isAdmin != "undefined" && typeof contributorData.isAdminPending == "undefined")
	}

	return canEdit ;
  }
  function isFollowApElement(params) {
	var count = 0;
	if(typeof params.links!=undefined && typeof params.links.activitypub!='undefined'){
	  var linkAp = params.links.activitypub;
	  if(typeof linkAp.followers !="undefined"){
		if (Array.isArray(linkAp.followers) && linkAp.followers.length > 0) {
		  linkAp.followers.forEach(follower => {
			if (typeof follower.invitorId !== 'undefined') {
			  if (follower.invitorId == activitypubActorId) {
			  count +=1;
			  }
			}
		  });
		}
	  }
	}
	return count > 0; 
}
function isInSameDomain(objectId, attributedTo) {
	var objectIdDomain = getDomain(objectId);
	var attributedToDomain = getDomain(attributedTo);

	return objectIdDomain === attributedToDomain;
}

function formattedProcess(formatedData, community) {
	if (!isValidUrl(formatedData.id)) formatedData.rolesAndStatusLink = (notNull(community.connectType) && exists(community.links[community.connectType][formatedData.id]) ? community.links[community.connectType][formatedData.id] : {});
	//if(exists(community.links[community.connectType][formatedData.id])) formatedData.rolesAndStatusLink=community.links[community.connectType][formatedData.id];
	formatedData.statusLinkHtml = '';
	if (typeof (formatedData.rolesAndStatusLink) != 'undefined') {
		if (typeof (formatedData.rolesAndStatusLink.toBeValidated) != 'undefined' && typeof (formatedData.rolesAndStatusLink.isAdminPending) != 'undefined')
			formatedData.statusLinkHtml = '<span class=\'entityStatusLink waitingToBecome text-red italic\'>' + trad.waitingValidationForAdmin + '</span>';
		else if (typeof (formatedData.rolesAndStatusLink.toBeValidated) != 'undefined')
			formatedData.statusLinkHtml = '<span class=\'entityStatusLink waitingToBecome text-red italic\'>' + trad.waitingValidation + '</span>';
		else if (typeof (formatedData.rolesAndStatusLink.isAdminPending) != 'undefined')
			formatedData.statusLinkHtml = '<span class=\'entityStatusLink memberAskForAdmin text-red italic\'>' + trad.memberAskToBeAdmin + '</span>';
		else if (typeof formatedData.rolesAndStatusLink.isAdminInviting != "undefined")
			formatedData.statusLinkHtml = '<span class=\'entityStatusLink adminInviting text-red italic\'>' + trad.invitingToAdmin + '</span>';
		else if (typeof formatedData.rolesAndStatusLink.isAdmin != 'undefined')
			formatedData.statusLinkHtml = '<span class=\'entityStatusLink admin\'>' + trad.administrator + '</span>';
		if (typeof formatedData.rolesAndStatusLink.isInviting != 'undefined') {
			formatedData.statusLinkHtml += '<span class=\'entityStatusLink italic\'>' + trad.InvitationLaunched + '</span>';
		}


		//var thisRoles = '';
		formatedData.rolesHtml = '';
		if (typeof formatedData.rolesAndStatusLink.roles =="object" && notNull(formatedData.rolesAndStatusLink.roles) && notEmpty(formatedData.rolesAndStatusLink.roles)) {
			formatedData.rolesHtml += '<small><b>' + trad.roleroles + ' :</b> ';
			formatedData.rolesHtml += (Array.isArray(formatedData.rolesAndStatusLink.roles)) ? formatedData.rolesAndStatusLink.roles.join(', ') : formatedData.rolesAndStatusLink.roles;
			//$.each(formatedData.rolesLink, function(key, value){
			//	if(typeof value != 'undefined' && value != '' && value != 'undefined')
			//		formatedData.containerClass += slugify(value)+' ';
			//});
			formatedData.rolesHtml += '</small>';
			//formatedData.rolesHtml = thisRoles;
		}
	}
}

var directory = {

	elemClass: smallMenu.destination + ' .searchEntityContainer ',
	path: 'div' + smallMenu.destination + ' div.favSection',
	tagsT: [],
	scopesT: [],
	multiTagsT: [],
	multiScopesT: [],
	appKeyParam: null,
	scrollTop: true,
	viewMode: directoryViewMode,
	costum: (typeof costum != 'undefined' && costum != null
		&& typeof costum.htmlConstruct != 'undefined'
		&& typeof costum.htmlConstruct.directory != 'undefined') ? costum.htmlConstruct.directory : null,
	colPos: 'left',
	dirLog: false,
	simplePanelHtml: function (params) {
		var str = '';
		str += '<div class=\'col-lg-4 col-md-4 col-sm-6 col-xs-12 searchEntityContainer simplePanelHtml ' + params.containerClass + ' contain_' + params.collection + '_' + params.id + '\'>';
		str += '<div class="searchEntity" id="entity' + params.id + '">';

		if (typeof params.edit != 'undefined' && notNull(params.edit))
			str += directory.getAdminToolBar(params);
		if (typeof params.updated != "undefined" && notNull(params.updated))
			str += directory.showDatetimePost(params.collection, params.id, params.updated, 30);
		else if (typeof params.created != "undefined" && notNull(params.created))
			str += directory.showDatetimePost(params.collection, params.id, params.created, 30);



		str += '<a href=\'' + params.hash + '\' class=\'container-img-profil ' + params.hashClass + '\'>' + params.imageProfilHtml + '</a>';
		//str += '<div class=\'padding-10 informations tooltips\'  data-toggle=\'tooltip\' data-placement=\'top\' data-original-title=\''+tipIsInviting+'\'>';

		str += '<div class=\'entityRight profil no-padding\'>';

		str += '<div class=\'entityCenter no-padding\'>';
		str += '<a href=\'' + params.hash + '\' class=\'pull-right ' + params.hashClass + '\'><i class="fa fa-' + params.icon + ' bg-' + params.color + '"></i></a>';
		str += '</div>';

		str += '<a  href=\'' + params.hash + '\' class=\'entityName bold text-dark ' + params.hashClass + '\'>' +
			params.name +
			'</a>';

		str += '<div class=\'entityDescription\'>' + ((params.shortDescription == null) ? '' : params.shortDescription) + '</div>';
		if (typeof params.tagsHtml != "undefined") {
			str += '<div class=\'tagsContainer text-red\'>' + params.tagsHtml + '</div>';
		}

		if (/*userId != null && userId != '' && */typeof params.id != 'undefined' && typeof params.collection != 'undefined')
			str += directory.socialToolsHtml(params);
		str += '</div>';
		str += '</div>';
		str += '</div>';
		str += '</div>';
		str += '</div>';
		return str;
	},
	classifiedPanelHtml : function(params){
		mylog.log("classifiedPanelHtml",params);
		var str = '';
		str += '<div class=\'col-lg-4 col-md-4 col-sm-6 col-xs-12 searchEntityContainer ' + params.containerClass + ' contain_' + params.collection + '_' + params.id + '\'>';
		str += '<div class="item-slide" id="entity' + params.id + '">';

		if (typeof params.updated != "undefined" && notNull(params.updated))
			str += directory.showDatetimePost(params.collection, params.id, params.updated, 30);
		else if (typeof params.created != "undefined" && notNull(params.created))
			str += directory.showDatetimePost(params.collection, params.id, params.created, 30);
		str += '<div class=\'entityCenter\'>';
		str += '<a href=\'' + params.hash + '\' class=\'pull-right ' + params.hashClass + '\'><i class="fa fa-' + params.icon + ' bg-' + params.color + '"></i></a>';
		str += '</div>';

		str += '<div class=\'img-back-card\'>' + params.imageProfilHtml;
		str += '<div class=\'text-wrap searchEntity\'>';

		str += '<div class=\'entityRight profil no-padding\'>';


		str += '<a  href=\'' + params.hash + '\' class=\'entityName letter-turq ' + params.hashClass + '\'>' +
			params.name +
			'</a>';

		var devise = (typeof params.devise != 'undefined') ? params.devise : '';
		if (typeof params.price != 'undefined' && params.price != '')
			str += '<div class=\'entityPrice\'>' + params.price + ' ' + devise + '</div>';
		if ($.inArray(params.collection, ['classifieds', 'ressources']) >= 0 && typeof params.category != 'undefined') {

			var breadcrumbHTML = `<span class="type">${tradCategory[params.section]}</span>`;
			if (params.category && params.type != 'poi') {
				var subBreadcrumb = (tradCategory[params.category]) ? (" > " + tradCategory[params.category]) : (" > " + params.category);
				if (params.subtype)
					subBreadcrumb += (tradCategory[params.subtype]) ? (" > " + tradCategory[params.subtype]) : (" > " + params.subtype);

				breadcrumbHTML += `<span class="subtype">${subBreadcrumb}</span>`;
			}
			str += `
			<div class="entityType col-xs-12 no-padding">
				${breadcrumbHTML}
			</div>
		`;
		}

		if (notNull(params.localityHtml)) {
			str += '<div class="entityLocality no-padding text-center">' +
				'<span> ' + params.localityHtml + '</span>' +
				'</div>';
		}
		/*if (params.contactInfo != '' && typeof params.contactInfo != 'undefined') {
	str += '<div class="entityContact no-padding">'+
				'<span> <i class="fa fa-phone" aria-hidden="true"></i> '+params.contactInfo+'</span>'+
		   '</div>';
		}*/
		str += '</div>';
		str += '</div>';
		str += '</div>';
		str += '<div class=\'slide-hover co-scroll\'>';

		str += '<div class="text-wrap">';

		str += '<h4 class="entityName"><a  href=\'' + params.hash + '\' class=\' letter-turq ' + params.hashClass + '\'>' +
			params.name +
			'</a></h4>';

		var devise = (typeof params.devise != 'undefined') ? params.devise : '';
		if (typeof params.price != 'undefined' && params.price != '')
			str += '<div class=\'entityPrice\'> ' + params.price + ' ' + devise + '</div>';

		if ($.inArray(params.collection, ['classifieds', 'ressources']) >= 0 && typeof params.category != 'undefined') {
			var breadcrumbHTML = `<span class="type">${tradCategory[params.section]}</span>`
			if (params.category && params.type != 'poi') {
				var subBreadcrumb = (tradCategory[params.category]) ? (" > " + tradCategory[params.category]) : (" > " + params.category);
				if (params.subtype)
					subBreadcrumb += (tradCategory[params.subtype]) ? (" > " + tradCategory[params.subtype]) : (" > " + params.subtype);

				breadcrumbHTML += `<span class="subtype">${subBreadcrumb}</span>`;
			}

			str += `
				<div class="entityType col-xs-12 no-padding">
					${breadcrumbHTML}
				</div>
			`;
		}

		if (notNull(params.localityHtml)) {
			str += '<div class="entityLocality no-padding text-center">' +
				'<span> ' + params.localityHtml + '</span>' +
				'</div>';
		}

		if (params.contactInfo != '' && typeof params.contactInfo != 'undefined') {
			str += '<div class="entityContact no-padding text-center">' +
				'<span> <i class="fa fa-phone" aria-hidden="true"></i> ' + params.contactInfo + '</span>' +
				'</div>';
		}
		str += '<hr>';
		str += '<p class="p-short">' + ((notNull(params.descriptionStr)) ? params.descriptionStr : "") + '</p>';
		if (typeof params.tagsHtml != "undefined") {
			str += '<ul class="tag-list">' +
				params.tagsHtml +
				'</ul>';
		}
		//str += '<hr>';

		if (/*userId != null && userId != '' && */ typeof params.id != 'undefined' && typeof params.collection != 'undefined')
			str += '<hr>' +
				'<div class="desc">' + directory.socialToolsHtml(params) + '</div>';
		str += '</div>';
		str += '</div>';
		str += '</div>';
		str += '</div>';
		str += '</div>';
		return str;
	},
	// ********************************
	//  ELEMENT DIRECTORY PANEL
	// ********************************
	elementPanelHtml: function (params) {
		mylog.log("elementPanelHtml", "Params", params);
		var dateStr = "";
		if (typeof params.updated != "undefined" && notNull(params.updated))
			dateStr += /*'<div class="dateUpdated dateUpdated-sm date-position">'+*/directory.showDatetimePost(params.collection, params.id, params.updated, 30)/*+'</div>'*/;
		else if (typeof params.created != "undefined" && notNull(params.created))
			dateStr += /*'<div class="dateUpdated dateUpdated-sm date-position">'+*/directory.showDatetimePost(params.collection, params.id, params.created, 30)/*+'</div>'*/;
		var str = '';
		str += '<div class="swiper-slide">';
		str += '<div id="entity_' + params.collection + '_' + params.id + '" class="col-lg-4 col-md-4 col-sm-6 col-xs-12 searchEntityContainer ' + params.containerClass + '">';
		str +=
		'<div class="item-slide commonConfigCard">' +
		'<div class="entityCenter" style="position: absolute;">';
			if (!checkLocalActor(params)) {
				str +=
				'<div class="bg-'+params.color+'" style="width: 45px;height: 45px;font-size: 20px;text-align: center;border-radius: 5px 0 21px 0;border-right: 2px solid #fff;border-bottom: 2px solid #fff;color: #fff;"><img style="width:20px;height:20px;margin-top: 10px" src="https://cdn.onlinewebfonts.com/svg/img_223015.png" alt="logo fediverse" width="32" height="32"></div>';
			  } else {
				str +=
				'<span><i class="fa fa-' +
				params.icon +
				" bg-" +
				params.color +
				'"></i></span>';
			}
			str += "</div>";
			str +=
			'<div class="img-back-card">';
			str += params.imageProfilHtml;
			if (isFromActivityPub(params)) {
				if (params.providerInfo != undefined) {
				str +=
					'<div style="position: absolute;bottom: 0;z-index:2;width:100%;padding-bottom: 10px;">';
				str +=
					'<div style="display:flex;justify-content:end;align-items:center" class="federation-provider-info">';
				str +=
					'<div style="display: flex;flex-direction: column;align-items: flex-start;padding-right:5px">';
				str +=
					'<span style="font-size: 11px;background-color: #777;color: white;padding: 1px 4px;border-radius: 2px;">envoyé depuis</span>';
				str +=
					'<a style="text-decoration: none;font-size: 14px;transition: .2s;color: white;font-weight:bold" href="' +
					params.providerInfo.url +
					'" target="__blank">';
				str += params.providerInfo.name;
				str += "</a>";
				str += "</div>";
				str +=
					'<img style="width:32px;height:32px;margin-left: 5px;margin-right: 10px;" src="https://www.fediverse.to/static/images/icon.png" alt="' +
					params.providerInfo.name +
					'" width="32" height="32">';
				str += "</div>";
				str += "</div>";
				}
			}
			if (isFromActivityPub(params)) {
				str += '<div class="text-wrap searchEntity" style="padding-bottom: 60px">';
			} else {
				str += '<div class="text-wrap searchEntity">';
			}

			str += '<h4 class="entityName">' +
			'<a href="' + params.hash + '" class="uppercase ' + params.hashClass + '">' + params.name + '</a>' +
			'</h4>' +
			'<div class="small-infos-under-title text-center">';

		str+= directory.typeConfigAppend (params.type);
		/*if (typeof params.type != 'undefined') {
			str += '<div class="text-center entityType">' +
				'<span class="text-white">' + ((typeof tradCategory[params.type] != "undefined") ? tradCategory[params.type] : params.type) + '</span>' +
				'</div>';
		}*/
		if (notEmpty(params.statusLinkHtml))
			str += params.statusLinkHtml;

		str+= directory.roleConfigAppend (params.rolesHtml);
		/*if (notEmpty(params.rolesHtml))
			str += '<div class=\'rolesContainer elipsis\'>' + params.rolesHtml + '</div>';*/

		str+= directory.localityConfigAppend (params.localityHtml);
		/*if (notNull(params.localityHtml)) {
			str += '<div class="entityLocality no-padding">' +
				'<span>' + params.localityHtml + '</span>' +
				'</div>';
		}*/
		str += '</div>' +
			'<div class="entityDescription"></div>' +
			'</div>' +
			'</div>' +
			//hover
			'<div class="slide-hover co-scroll">' +
			'<div class="text-wrap">' +
			'<h4 class="entityName">' +
			'<a href="' + params.hash + '" class="' + params.hashClass + '">' + params.name + '</a>' +
			'</h4>';

		str+= directory.typeConfigAppend (params.type);
		/*if (typeof params.type != 'undefined') {
			str += '<div class="entityType">' +
				'<span class="text-white">' + ((typeof tradCategory[params.type] != "undefined") ? tradCategory[params.type] : params.type) + '</span>' +
				'</div>';
		}*/
		if (notEmpty(params.statusLinkHtml))
			str += params.statusLinkHtml;

		str+= directory.roleConfigAppend (params.rolesHtml);
		/*if (notEmpty(params.rolesHtml))
			str += '<div class=\'rolesContainer\'>' + params.rolesHtml + '</div>';*/

		if (typeof params.edit != 'undefined' && notNull(params.edit))
			str += directory.getAdminToolBar(params);
		if(params.id == userId && typeof params.rolesAndStatusLink != "undefined" && typeof params.rolesAndStatusLink["isInviting"] != "undefined" && typeof params.rolesAndStatusLink["invitorId"] != "undefined" && params.rolesAndStatusLink["invitorId"] != userId){
			str += directory.getUserInvitToolBar(params);
		}

		str+= directory.localityConfigAppend (params.localityHtml);
		/*if (notNull(params.localityHtml)) {
			str += '<div class="entityLocality text-center">' +
				'<span> ' + params.localityHtml + '</span>' +
				'</div>';
		}*/
		//		str +=					'<hr>';
		str+= directory.descriptionConfigAppend (params.descriptionStr);
		/*str += '<p class="p-short">' + params.descriptionStr + '</p>';*/
		if (typeof params.tagsHtml != "undefined")
			str += '<ul class="tag-list">' + params.tagsHtml + '</ul>';

		str += directory.countLinksHtml(params);

		str += directory.socialToolsConfigAppend (params);
		/*if (typeof params.id != 'undefined' && typeof params.collection != 'undefined')
			str += directory.socialToolsHtml(params);*/
		str += '</div>' +
			'</div>' +
			'</div>' +
			'</div>'+
			'</div>';
		return str;
	},


	elementPanelHtmlSmallCard: function (params) {
		mylog.log("elementPanelHtmlSmallCard", "Params", params);
		var str = '';
		str = `
		<div id="entity_${ params.collection}_${params.id}" class="col-lg-2 col-md-3 col-sm-4 col-xs-12 searchEntityContainer ${params.containerClass} click-element" data-href="${params.hash}" data-type-elt="${params.collection}" data-class="${params.hashClass}">
			<div class="small-card item-slide">
				<div class="icon-elm bg-${params.color}">
					<span><i class="fa fa-${params.icon} "></i></span>
				</div>
				<div class="block-card-img">
					<a href="${params.hash }" class=" ${params.hashClass}">
						<div class="card-img-container " >
							${params.imageProfilHtml}					
						</div>
					</a>
					<div class="block-card-body">						
						<h4 class="entityName"><a href="${params.hash }" class=" ${params.hashClass}">${params.name}</a></h4>`					
					str+= `</div>`
				str+= `</div>`
				str+= ` <div class="block-card-hover co-scroll">
							<div class="text-wrap">
								<h4 class="entityName"> <a href="${params.hash}" class="${params.hashClass}">${params.name}</a> </h4>`
								str+= directory.typeConfigAppend (params.type);
								if (notEmpty(params.statusLinkHtml))
									str += params.statusLinkHtml;

								str+= directory.roleConfigAppend (params.rolesHtml);

								if (typeof params.edit != 'undefined' && notNull(params.edit))
									str += directory.getAdminToolBar(params);
								if(params.id == userId && typeof params.rolesAndStatusLink != "undefined" && typeof params.rolesAndStatusLink["isInviting"] != "undefined" && typeof params.rolesAndStatusLink["invitorId"] != "undefined" && params.rolesAndStatusLink["invitorId"] != userId){
									str += directory.getUserInvitToolBar(params);
								}
								str+= directory.localityConfigAppend (params.localityHtml);
								str += directory.countLinksHtml(params);
								str += directory.socialToolsConfigAppend (params);
							str+= `</div>` 
					str += `</div>`  
			str += `</div> 
			</div> `
		// str += `<div class="col-xs-12 small-card-xs no-padding  ${params.containerClass} visible-xs"> 
		// 	<div class="col-xs-3 no-padding entityImg">						
		// 		<a href="${params.hash }" class=" ${params.hashClass}">
		// 			${params.imageProfilHtml}
		// 		</a>
		// 	</div>
		// 	<div class="col-xs-9 entityName">	
		// 		<a href="${params.hash }" class=" ${params.hashClass}">${params.name}</a>	
		// 	</div> 
		// </div>`
		;
		return str;
	},
	// ********************************
	//  CONFIG ELEMENT DANS LES CART (tags,locality,Description court, role ....)
	// ********************************

	descriptionConfigAppend : function (description) {
		var dataStr = "";
		if (notNull(description)) {
			dataStr += '<div class="commonClassDesc">' +
				description +
				'</div>';
		}
		return dataStr;
	},
	localityConfigAppend : function (locality) {
		var dataStr = "";
		if (notNull(locality)) {
			dataStr += 	'<div class="entityLocality">' +
				'<span class="commonClassLocality"> ' + locality + '</span>' +
				'</div>';
		}
		return dataStr;
	},
	tagsConfigAppend : function (tags) {
		var dataStr = "";
		if (typeof tags != "undefined")
			dataStr += '<div class="tag-list">' + tags + '</div>';

		return dataStr;
	},
	socialToolsConfigAppend : function (params) {
		var dataStr = "";
		if (typeof params.id != 'undefined' && typeof params.collection != 'undefined')
			dataStr += directory.socialToolsHtml(params);

		return dataStr;
	},
	roleConfigAppend : function (role) {
		var dataStr = "";
		if (notEmpty(role))
			dataStr += '<div class="rolesContainer">' + role + '</div>';

		return dataStr;
	},
	typeConfigAppend : function (type) {
		var dataStr = "";
		if (typeof type != 'undefined') {
			dataStr +=	'<div class="entityType text-center">' +
				'<span class="commonClassType">' + ((typeof tradCategory[type] != "undefined") ? tradCategory[type] : type) + '</span>'+
				'</div>';
		};

		return dataStr;
	},
	collectionConfigAppend : function (collection) {
		var dataStr = "";
		if (typeof collection != 'undefined') {
			dataStr +=	`<p class="commonClassCollection">${collection}</p>`;
		};

		return dataStr;
	},



	// ********************************
	//  ELEMENT DIRECTORY PANEL FULLWIDTH
	// ********************************
	elementPanelHtmlFullWidth: function (params) {
		mylog.log("elementPanelHtmlFullWidth", "Params", params);

		var str = '';

		str += '<div class="swiper-slide">';
		str += '<div class="container-lib-item">' +
			'<div id="entity_' + params.collection + '_' + params.id + '" class="col-md-12 col-xs-12 col-sm-12 no-padding lib-item searchEntityContainer ' + params.containerClass + '" data-category="view">' +
			'<div class="lib-panel">' +
			'<div class="row box-shadow commonConfigCard">' +
			'<div class="col-xs-12 col-sm-3 col-md-2 no-padding">' +
			'<a href="' + params.hash + '" class="' + params.hashClass + '">' +
			params.imageProfilHtml +
			'</a>' +
			'</div>' +
			'<div class="col-xs-12 col-sm-9 col-md-10">' +
			'<div class="lib-row lib-header">' +
			'<a href="' + params.hash + '" class="' + params.hashClass + ' commonClassName">' + params.name + '</a>' +
			'<div class="lib-header-seperator"></div>' +
			'</div>';

		str+= directory.localityConfigAppend (params.localityHtml);
		/*if (notNull(params.localityHtml)) {
			str += '<div class="entityLocality">' +
				'<span class="commonClassLocality"> ' + params.localityHtml + '</span>' +
				'</div>';
		}*/
		str += directory.descriptionConfigAppend (params.descriptionStr);
		/*if (notNull(params.descriptionStr)) {
			str += '<div class="lib-row lib-desc commonClassDesc">' +
				params.descriptionStr +
				'</div>';
		}*/
		str += directory.tagsConfigAppend (params.tagsHtml);
		/*if (typeof params.tagsHtml != "undefined")
			str += '<ul class="tag-list">' + params.tagsHtml + '</ul>';*/
		str += '</div>' +
			'</div>' +
			'</div>' +
			'</div>' +
			'</div>' +
			'</div>';

		return str;
	},

	// ********************************
	//  ELEMENT DIRECTORY PANEL Mi WIDTH
	// ********************************
	elementPanelHtmlMiWidth: function (params) {
		mylog.log("elementPanelHtmlMiWidth", "Params", params);

		var str = '';

		str += '<div class="swiper-slide">';
		str += '<div id="entity_' + params.collection + '_' + params.id + '" class="col-lg-6 col-md-6 col-xs-12 col-sm-12 elementPanelHtmlMiWidth no-padding lib-item searchEntityContainer ' + params.containerClass + '" data-category="view">' +
			'<div class="lib-panel">' +
			'<div class="row box-shadow">' +
			'<div class="col-xs-12 col-sm-4 col-md-4 no-padding">' +
			'<a href="' + params.hash + '" class="' + params.hashClass + '">' +
			params.imageProfilHtml +
			'</a>' +
			'</div>' +
			'<div class="col-xs-12 col-sm-8 col-md-8">' +
			'<div class="lib-row lib-header">' +
			'<a href="' + params.hash + '" class="' + params.hashClass + ' commonClassName">' + params.name + '</a>' +
			'<div class="lib-header-seperator"></div>' +
			'</div>';

		str += directory.countLinksHtml(params);
		str+= directory.localityConfigAppend (params.localityHtml);
		str += directory.tagsConfigAppend (params.tagsHtml);
		str += '</div>' +
			'</div>' +
			'</div>' +
			'</div>' +
			'</div>';

		return str;
	},


	materialDesign : function(params){


		var dateStr="";
		if(typeof params.updated != "undefined" && notNull(params.updated))
			dateStr += /*'<div class="dateUpdated dateUpdated-sm date-position">'+*/directory.showDatetimePost(params.collection, params.id, params.updated,30)/*+'</div>'*/;
		else if(typeof params.created != "undefined" && notNull(params.created))
			dateStr += /*'<div class="dateUpdated dateUpdated-sm date-position">'+*/directory.showDatetimePost(params.collection, params.id, params.created,30)/*+'</div>'*/;
		var str='';
		str += '<div class="swiper-slide">';
		str +=	'<div id="entity_'+params.collection+'_'+params.id+'" class="searchEntityContainer col-lg-4 col-md-4 col-sm-6 col-xs-12 '+params.containerClass+' commonConfigCard">'+
			'<article class="material-card color1">'+
			'<a class="' + params.hashClass + '" href="' + params.hash + '">'+
				'<h2><span class="commonClassName">'+params.name+'</span></h2>'+
			'</a>'+
			'<div class="mc-content">'+
			'<div class="img-container">'+
			params.imageProfilHtml +
			'</div>'+
			'<div class="mc-description co-scroll">';
		str+= directory.localityConfigAppend (params.localityHtml);
		/*if (notNull(params.localityHtml)) {
			str +=				    '<span class="commonClassLocality">'+
				params.localityHtml+
				'</span><br>';
		}*/
		str+= directory.descriptionConfigAppend (params.descriptionStr);
		/*if (notNull(params.descriptionStr))
			str +=	'<span class="commonClassDesc">'+params.descriptionStr+'</span>';*/
		str += directory.tagsConfigAppend (params.tagsHtml);
		/*if (typeof params.tagsHtml != "undefined")
			str += '<ul class="tag-list">' + params.tagsHtml + '</ul>';*/

		str +=	'</div>'+
			'</div>'+
			'<a class="mc-btn-action ripple">'+
			'<i class="fa fa-bars"></i>'+
			'</a>'+
			'<div class="mc-footer">';
			if (typeof params.id != 'undefined' && typeof params.collection != 'undefined')
				str += directory.socialToolsHtml(params);
			else
			str +='<a class="btn' + params.hashClass + '" href="' + params.hash + '"> '+tradBadge.viewDetails+' <i class="fa fa-fw fa-arrow-right ripple"></i> </a>';

		str +=	'</div>'+
			'</article>'+
			'</div>'+
			'</div>';

		return str;
	},

	// ********************************
	// AAC FTL design
	// ********************************

	communDesign : function(params) {
		mylog.log('PARAMS DIRECTORY', params)
		var tagsHtml = '';
		const nbTagToShow = 2;
		var tagsTooltipsHtml = '';
		var isSelectedCommun = false;
		const shortDescId = "aapStep1lzi62x3etw49gyc424d";
		const besoinsTags = "aapStep1m03ot9qymmfgashp7l";
		var usersCount = 0;
		if (params.answers && params.answers.aapStep1 && params.answers.aapStep1[shortDescId]) {
			params.descriptionStr = params.answers.aapStep1[shortDescId];
		} else {
			params.descriptionStr = null;
		}
		if (params.answers && params.answers.aapStep1 && params.answers.aapStep1[besoinsTags]) {
			params.tags = params.answers.aapStep1[besoinsTags];
		} else {
			params.tags = null;
		}
		if(typeof params.tags != 'undefined' && params.tags != null) {
			for(let i = 0 ; i < params.tags.length ; i++) {
				if (i < nbTagToShow) {
					tagsHtml += `<span class="tags" style="white-space: nowrap">${params.tags[i]}</span>`
				} else {
					tagsTooltipsHtml += `<span class="tags">${params.tags[i]}</span>`
				}
			}
		}
		if (params && 
			costum &&
			costum.contextId &&
			params.answers && 
			params.answers.aapStep2 && 
			params.answers.aapStep2.choose && 
			params.answers.aapStep2.choose[costum.contextId] && 
			params.answers.aapStep2.choose[costum.contextId].value == "selected"
		) {
			isSelectedCommun = true;
		}
		if (!notEmpty(tagsHtml)) {
			tagsHtml = `<span class="bs-mb-2">&nbsp;</span>`;
		}
		var funding = {
			total: 0,
			funded: 0
		};
		var progress = 0;
		if (typeof params.funds != "undefined") {
			funding = params.funds.reduce(function(prev, current) {
				return ({
					total: prev.total + current.price,
					funded: prev.funded + current.financer.reduce((a, b) => (a + b), 0)
				})
			}, funding);
		}
		progress = parseInt((funding.funded / (funding.total > 0 ? funding.total : 1)) * 100);
		if (typeof params.links != "undefined" && params.links.tls) {
			usersCount = Object.keys(params.links.tls).length;
		}
		if (params.redirectPage) {
			params.redirectPage += ".communId." +params.id
		}
		var str = `
			<div class="aac-card col-xs-12 col-lg-3 col-md-4 col-sm-6 ${ !isSelectedCommun ? "not-selected-commun" : "" }" style="--dtl-costum-color1: ${ costum.css && costum.css.color && costum.css.color.color1 ? costum.css.color.color1 : "#43c9b7" }; --dtl-costum-color2: ${ costum.css && costum.css.color && costum.css.color.color2 ? costum.css.color.color2 : "#4623c9" }; --dtl-costum-color3: ${ costum.css && costum.css.color && costum.css.color.color3 ? costum.css.color.color3 : "#ffc9cb" }">
				<div class="commun-card" ${ !isSelectedCommun ? `data-notselected-label="${trad.waiting}"` : "" }>
					<div class="commun-card-image">
						<img src="${params.image}"/>
					</div>
					${
						(userId && params.user && params.user == userId) ||
						(isSuperAdmin || isInterfaceAdmin) ?
							`<a href="javascript:;" class="btn btn-default commun-delete-btn" data-communid="${ params.id }" data-commun-title="${ params.name }">
								<i class="fa fa-times fa-2xx"></i>
							</a>`
						: ""
					}
					<div class="commun-card-body">
						<div class="commun-title">
							<a class="btn-know-more lbh desc-2-ellipsis this-commun-title" href="${ params.redirectPage ? params.redirectPage : "" }">${params.name}</a>
						</div>
						<div class="commun-desc desc-3-ellipsis bs-mb-2 ${ params.descriptionStr ? 'has-desc' : '' }">
							${params.descriptionStr ? params.descriptionStr : `(${ trad.nodescription })`}
						</div>
						${tagsHtml != "" ? `
							<div class="commun-tags">
								${tagsHtml} 
								${ 
									tagsTooltipsHtml != "" ? (
										`
											<span class='dropdown'>
												<a class='dropdown-toggle' type='button' id='${ params.id }' data-toggle='dropdown'>
													<span class="tags small-width"><i class="fa fa-ellipsis-h"></i></span>
												</a>
												<div class='dropdown-menu bs-px-3 tags-dropdown' role='menu' aria-labelledby='${ params.id }'>
													${ tagsTooltipsHtml }
												</div>
											</span>
										`
									) : ''
								}
							</div>` : ''
						}
						<div class="commun-collect">
							<span class="collect-montant text-ellipsis"> ${ funding.funded ? funding.funded.toLocaleString("fr-FR") : funding.funded }<i class="fa fa-eur"></i></span>
							<span class="collect-text">${ trad["Already collected"] }</span>
						</div>
						<div class="commun-progress">
							<div class="progress">
								<div class="progress-bar" role="progressbar" aria-valuenow="70"
									aria-valuemin="0" aria-valuemax="100" style="width:${progress}%">
									${progress}%
								</div>
							</div>
						</div>
						<div class="commun-footer">
							<div class="footer-text">
								<span>${usersCount} ${usersCount > 1 ? tradCms.users.toLowerCase() : tradCms.user.toLowerCase()}</span>
								<span>${params.interrest_count} ${params.interrest_count > 1 ? trad["interested "] : trad["interested"]}</span>
							</div>    
							<div class="footer-btn">
								<a class="btn-know-more lbh" href="${ params.redirectPage ? params.redirectPage : "" }"><img src="${modules.costum.url + '/images/aap/lesCommuns/fleche2.png'}" alt="" /></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		`
		return str;
	},

	// ********************************
	// ROTATING CARD
	// ********************************

	rotatingCard: function (params) {
		mylog.log("paramsparams",params);

		var elementType = "";
		if (params.collection == "events" || params.collection == "organizations" || params.collection == "citoyens" || params.collection == "projects")
			bannerElementType = params.collection+'.png'
		else
			bannerElementType = 'connexion-lines.jpg'

		var urlBanner = (typeof params.profilRealBannerUrl != 'undefined') ? params.profilRealBannerUrl : themeUrl+'/assets/img/background-onepage/' + bannerElementType;
		mylog.log("urlBanner",urlBanner);
		var str = `
		<div class="swiper-slide">
		<div id="entity_` + params.collection + `_` + params.id + `" class="col-lg-4 col-md-4 col-sm-6 col-xs-12 rotatingCard searchEntityContainer ${params.containerClass}">
             <div class="card-container manual-flip commonConfigCard">
                <div class="card">
                    <div class="front">
                        <div class="cover">
                            <img src="${urlBanner}"/>
                        </div>
                        <div class="user">
                            ${params.imageProfilHtml}
                        </div>
                        <div class="content">
                            <div class="main co-scroll">
                            	<a href="${params.hash}" class="${params.hashClass}">
                                	<h3 class="name commonClassName">${params.name}</h3>
                                </a>`;
        str+= directory.collectionConfigAppend (params.collection);
                                /*'<p class="profession">${params.collection}</p>';*/
		str+= directory.localityConfigAppend (params.localityHtml);
                                /*if (notNull(params.localityHtml)) {
        str += 					`<p class="text-center commonClassLocality">${params.localityHtml}</p>`;
                                }*/
		str += `            </div>
                            <div class="footer">
                                <button class="btn btn-simple btn-rotatingCard" >
                                    <i class="fa fa-mail-forward"></i> ${trad.information}
                                </button>
                            </div>
                        </div>
                    </div> <!-- end front panel -->
                    <div class="back">
                        <div class="header">
                            <h5 class="motto commonClassName">${params.name}</h5>
                        </div>
                        <div class="content ">
                            <div class="main co-scroll">`
		str+= directory.typeConfigAppend (params.type);
								/*if (typeof params.type != 'undefined') {
		str += 						'<div class="entityType text-center">' +
										'<span class="text-dark">' + ((typeof tradCategory[params.type] != "undefined") ? tradCategory[params.type] : params.type) + '</span>'+
									'</div>';
								};*/
								if (notEmpty(params.statusLinkHtml))
									str += params.statusLinkHtml;
		str+= directory.roleConfigAppend (params.rolesHtml);
								/*if (notEmpty(params.rolesHtml))
									str += '<div class=\'rolesContainer\'>' + params.rolesHtml + '</div>';*/
								if (typeof params.edit != 'undefined' && notNull(params.edit))
									str += directory.getAdminToolBar(params);
		str+= directory.descriptionConfigAppend (params.descriptionStr);
								/*if (notNull(params.descriptionStr))
									str += `<p class="text-center commonClassDesc">${params.descriptionStr}</p>`;*/
		str += directory.tagsConfigAppend (params.tagsHtml);
								/*if (typeof params.tagsHtml != "undefined")
									str += '<ul class="tag-list text-center">' + params.tagsHtml + '</ul>';*/

         str +=                 `<div class="stats-container">`;
									str += directory.countLinksHtml(params);
		str += directory.socialToolsConfigAppend (params);
									/*if (typeof params.id != 'undefined' && typeof params.collection != 'undefined')
										str += directory.socialToolsHtml(params);*/
		str +=                   `</div>

                            </div>
                        </div>
                        <div class="footer">
                            <a href="javascript:;" class="btn btn-simple btn-rotatingCard" rel="tooltip" title="Flip Card">
                                <i class="fa fa-reply"></i> ${trad.back}
                            </a>
                            <a href="${params.hash}" class="btn btn-simple btn-rotatingCard ${params.hashClass}" rel="tooltip" title="Flip Card"> 
                                ${tradBadge.viewDetails} <i class="fa  fa-mail-forward"></i>
                            </a>
                        </div>
                    </div> 
                </div> 
            </div> 
           </div> 
        </div> `;
		return str;
	},


	// ********************************
	//  LIST ELEMENT PANEL
	// ********************************
	listElementHorizontal: function (params) {
		let str = "";
		let virg = "";
		let titleLink = "javascript:;";
		let imageUrl = baseUrl + moduleUrl + "/images/thumbnail-default.jpg";
		if (typeof params.profilMediumImageUrl !== "undefined" && params.profilMediumImageUrl !== "") {
			imageUrl = baseUrl + params.profilMediumImageUrl;
		}
		str += `
				<li class="listElement">	
					<span class="imageList" style="background-image: url(&quot;${imageUrl}&quot;);"></span>
						<div class="informationContent">
							<a class="buttonShowAllTitle" href="${titleLink}">
								${params.name}
							</a>
							<span>
								<span class="infoDate">
									${directory.decomposeDate(params.created, 'dayNumber-month', true)}, ${directory.decomposeDate(params.created, 'year', true)}
								</span>
								<span class="dot"> ·</span>
								<span class="infoTag">`;
		if (typeof params.tags !== "undefined") {
			$.each(params.tags, function (e, v) {
				str += `
												<span class="elementTag" onclick="modalShowAll.actions.filterTag('${v}')">
													${virg} ${v}
												</span>
											`;
				virg = ",";
			});
		}
		str += `
						</span>
					</span>
				</div>
			</li>
		`;
		return str;
	},

	// ********************************
	//  ELEMENT DIRECTORY PANEL
	// ********************************
	countLinksHtml: function (data) {
		//----------------------------Count Contributors followers attendees------------------------------
		data.thisContributorsCounter = '', data.thisFollowersCounter = '', data.thisAttendeesCounter = '';
		data.thisContributorsCounterUnity = '', data.thisFollowersCounterUnity = '', data.thisAttendeesCounterUnity = '';
		data.thisContributorsCounterIcon = '', data.thisFollowersCounterIcon = '', data.thisAttendeesCounterIcon = '';
		if (typeof data.links != "undefined" && typeof data.links.contributors != "undefined" && Object.keys(data.links.contributors).length != 0) {
			data.thisContributorsCounter = Object.keys(data.links.contributors).length;
			data.thisContributorsCounterUnity = Object.keys(data.links.contributors).length == 1 ? trad.contributor : trad.contributors;
			data.thisContributorsCounterIcon = '<i class="fa fa-group"></i>';
		}
		if (typeof data.links != "undefined" && typeof data.links.followers != "undefined" && Object.keys(data.links.followers).length != 0) {
			data.thisFollowersCounter = Object.keys(data.links.followers).length;
			data.thisFollowersCounterUnity = Object.keys(data.links.followers).length == 1 ? trad["follower"] : trad["followers"];
			data.thisFollowersCounterIcon = '<i class="fa fa-link"></i>';
		}
		if (typeof data.links != "undefined" && typeof data.links.attendees != "undefined" && Object.keys(data.links.attendees).length != 0) {
			data.thisAttendeesCounter = Object.keys(data.links.attendees).length;
			data.thisAttendeesCounterUnity = Object.keys(data.links.attendees).length == 1 ? trad["attendee"] : trad["attendees"];
			data.thisAttendeesCounterIcon = '<i class="fa fa-group"></i>';
		}
		var html = '';
		html += '<div class="counter text-bold">';
		if (data.thisContributorsCounter != '') {
			html += '<div class="list-dash">' +
				'<h3 class="pull-left">' + data.thisContributorsCounterIcon + '</h3>' +
				'<h4 class="list-dash-item">' + data.thisContributorsCounter + '</h4>' +
				'<p class="list-dash-text">' + data.thisContributorsCounterUnity + '</p>' +
				'</div>';
		}
		if (data.thisFollowersCounter != '') {
			html += '<div class="list-dash">' +
				'<h3 class="pull-left">' + data.thisFollowersCounterIcon + '</h3>' +
				'<h4 class="list-dash-item">' + data.thisFollowersCounter + '</h4>' +
				'<p class="list-dash-text">' + data.thisFollowersCounterUnity + '</p>' +
				'</div>';
		}
		if (data.thisAttendeesCounter != '') {
			html += '<div class="list-dash">' +
				'<h3 class="pull-left">' + data.thisAttendeesCounterIcon + '</h3>' +
				'<h4 class="list-dash-item">' + data.thisAttendeesCounter + '</h4>' +
				'<p class="list-dash-text">' + data.thisAttendeesCounter + '</p>' +
				'</div>';
		}
		html += '</div>';
		return html;
	},
	socialToolsHtml: function (params) {
		var str = '';
		str += '<div class="socialEntityBtnActions btn-link-content">';
		if(typeof params.isSpam == "undefined" || (typeof params.isSpam != "undefined" && !params.isSpam)){
			if (params.id != userId && $.inArray(params.collection, ['poi', 'classifieds']) == -1) {
				var tip = (params.collection == 'events') ? trad['participate'] : trad['Follow'];
				var classBind = 'followBtn';
				var strLink, actionType, titleMember;
				if (notNull(myContacts)
					&& (typeof myContacts[params.collection] != 'undefined')
					&& (typeof myContacts[params.collection][params.id] != 'undefined')
					&& (typeof myContacts[params.collection][params.id]._id != 'undefined')
					&& (typeof myContacts[params.collection][params.id]._id.$id != 'undefined')
				) {
					if (params.id == myContacts[params.collection][params.id]._id.$id) {
						classBind = 'text-white disabled';
						actionType = '';
						if (params.collection == 'organizations')
							titleMember = trad.alreadyMember;
						else if (params.collection == 'citoyens')
							titleMember = trad.alreadyFriend;
						else titleMember = trad.alreadyContributor;

						strLink = '<i class="fa fa-user-circle"></i> ' + titleMember;
					}
				} else if (notNull(myContacts)
					&& (typeof myContacts["follows"] != 'undefined')
					&& (typeof myContacts["follows"][params.id] != 'undefined')
					&& (typeof myContacts["follows"][params.id]._id != 'undefined')
					&& (typeof myContacts["follows"][params.id]._id.$id != 'undefined')
				) {
					if (params.id == myContacts["follows"][params.id]._id.$id) {
						mylog.log("membre de: ", params.name, params.id, "Mon contacts", myContacts["follows"][params.id]._id.$id);
						tip = (params.collection == 'events') ? trad['alreadyAttendee'] : trad['alreadyFollow'];
						strLink = '<i class="fa fa-unlink"></i> ' + tip;
						actionType = 'unfollow';
						classBind += ' active';
					} else {
						classBind = 'text-white disabled';
						actionType = '';
						titleMember = (params.collection == 'organizations') ? trad.alreadyMember : trad.alreadyContributor;
						strLink = '<i class="fa fa-user-circle"></i> ' + titleMember;
					}
				} else {
					if(params.collection == "organizations"){
						tip = trad.becomeMember;
					}
					strLink = '<i class="fa fa-link fa-rotate-270"></i> ' + tip;
					actionType = 'follow';
				}

				if (isFromActivityPub(params)) {
					if (params.hasParticipate) {
						tip = (params.collection == 'events') ? trad['alreadyAttendee'] : trad['alreadyFollow'];
						strLink = '<i class="fa fa-unlink"></i> ' + tip;
						actionType = 'unfollow';
						classBind += ' active';
					} else {
						strLink = '<i class="fa fa-link fa-rotate-270"></i> ' + tip;
						actionType = 'follow';

					}
				}
				if (isFromActivityPub(params)) {
					str += '<a href="javascript:;" class="btn btn-default btn-link ' + classBind + '"' +
						' data-toggle="tooltip" data-placement="left" data-original-title="' + tip + '"' +
						' data-ownerlink="' + actionType + '" data-id="' + params.id + '" data-type="' + params.collection + '"' +
						' data-name="' + params.name + '" data-action="' + activityPubAction(actionType) + '" data-payload="' + activityPubObjectId(params) + '" data-isFromActivitypub="' + isFromActivityPub(params) + '" >' + // data-isFollowed='"+isFollowed+"'
						strLink +
						'</a>';
				} else {
					if(actionType == 'follow'){
						if(notNull(userId) && userId != ''){
							str += '<a href="javascript:;" class="btn btn-default btn-link ' + classBind + '"' +
							' data-toggle="tooltip" data-placement="left" data-original-title="' + tip + '"' +
							' data-ownerlink="' + actionType + '" data-id="' + params.id + '" data-type="' + params.collection + '"' +
							' data-name="' + params.name + '" >' + // data-isFollowed='"+isFollowed+"'
							strLink +
							'</a>';
						}
					}else{
						str += '<a href="javascript:;" class="btn btn-default btn-link ' + classBind + '"' +
						' data-toggle="tooltip" data-placement="left" data-original-title="' + tip + '"' +
						' data-ownerlink="' + actionType + '" data-id="' + params.id + '" data-type="' + params.collection + '"' +
						' data-name="' + params.name + '" >' + // data-isFollowed='"+isFollowed+"'
						strLink +
						'</a>';
					}					
				}
			}

			if (params.collection != 'citoyens')
				str += '<a href="javascript:;" id="btn-share-' + params.type + '" class="btn btn-info btn-link btn-share-panel"' +
					' data-ownerlink="share" data-id="' + params.id + '" data-type="' + params.collection + '" >' +
					'<i class="fa fa-share"></i> ' + trad['share'] +
					'</a> ';

			if(typeof userConnected != "undefined" && userConnected != null && typeof userConnected.roles != "undefined"  && typeof userConnected.roles.superAdmin != "undefined"  && userConnected.roles.superAdmin){
				str += '<a href="javascript:;" id="btn-addToSpam-' + params.type + '" class="btn btn-info btn-link btn-addToSpam-element"' +
				' data-id="' + params.id + '" data-type="' + params.collection + '" style="background-color:red!important;">' +
				'<i class="fa fa-exclamation-circle"></i> ' + 'Spam' +
				'</a> ';
			}
		}
		else if(typeof params.isSpam != "undefined" && params.isSpam && typeof userConnected != "undefined" && userConnected != null && typeof userConnected.roles != "undefined"  && typeof userConnected.roles.superAdmin != "undefined"  && userConnected.roles.superAdmin){
			str += '<a href="javascript:;" id="btn-share-' + params.type + '" class="btn btn-info btn-link btn-no-Spam"' +
					' data-ownerlink="share" data-id="' + params.id + '" data-type="' + params.collection + '" >' +
					'<i class="fa fa-exclamation-circle"></i> ' +trad.noSpam+
					'</a> ';

			str += '<a href="javascript:;" id="btn-deletespam' + params.type + '" class="btn btn-info btn-link btn-delete-spam"' +
					' data-id="' + params.id + '" data-type="' + params.collection + '" style="background-color:red!important;" >' +
					'<i class="fa fa-trash"></i> ' +trad.deletElement+
					'</a> ';
		}

		str += '</div>';
		return str;
	},
	buttonShareSocial: function (params) {
		var str = '<div class="socialEntityBtnActions btn-link-content"><a href="javascript:;" id="btn-share-' + params.type + '" class="btn btn-info btn-link btn-share-panel"' +
			' data-ownerlink="share" data-id="' + params.id + '" data-type="' + params.collection + '">' +
			'<i class="fa fa-share"></i> ' + trad['share'] +
			'</a> </div>';
		return str;
	},
	//Boutons de partage


	socialBarHtml: function (params) {
		var str = '';
		if (notEmpty(params.socialBarConfig) && notEmpty(params.socialBarConfig.btnList)) {
			str = '<div class="socialBar">';
			var btnSize = 16;
			if (notEmpty(params.socialBarConfig.btnSize))
				btnSize = params.socialBarConfig.btnSize;
			for (var i = 0; i < params.socialBarConfig.btnList.length; i++) {
				str += directory.shareBtnHtml(params.socialBarConfig.btnList[i], params, btnSize);
			}
			str += '</div>';
		}
		return str;
	},

	shareBtnHtml: function (media, params, size) {
		var url = baseUrl + '/co2/app/page/type/' + params.type + '/id/' + params.id;
		var str = '';
		switch (media.type) {
			case 'co':
				if (userId != null && userId != '')
					str = '<img class="co3BtnShare btn-share" width="' + size + '" data-ownerlink="share" data-id="' + params.id + '"" data-type="' + params.type + '" src="' + parentModuleUrl + '/images/social/co-icon-64.png"/>';
				break;
			case 'facebook':
				//str='<i class="fa fa-facebook-square" style="float:right;margin-left:0.5em;" onclick="window.open("https://www.facebook.com/sharer.php?u='+url+'","_blank")"></i>';
				str = '<img class="co3BtnShare" width="' + size + '" src="' + parentModuleUrl + '/images/social/facebook-icon-64.png" onclick="window.open(`https://www.facebook.com/sharer.php?u=' + url + '`,`_blank`)"/>';
				break;
			case 'twitter':
				//str='<i class="fa fa-twitter-square" style="float:right;margin-left:0.5em;" onclick="window.open("https://twitter.com/intent/tweet?url='+url+'","_blank")"></i>';
				str = '<img class="co3BtnShare" width="' + size + '" src="' + parentModuleUrl + '/images/social/twitter-icon-64.png" onclick="window.open(`https://twitter.com/intent/tweet?url=' + url + '`,`_blank`)"/>';
				break;
			case 'custom':
				str = '<img class="co3BtnShare" width="' + size + '" src="' + media.img + '" onclick="window.open("' + media.shareLink + url + '","_blank")"/>';
				break;
			default:
				mylog.log('<!-- Warning : invalid media (directory.shareBtnHtml) -->', media);
		}
		return str;
	},





	// ********************************
	// CALCULATE NEXT PREVIOUS 
	// ********************************
	findNextPrev: function (hash) {
		mylog.log('----------- findNextPrev', hash);
		var p = 0;
		var n = 0;
		var nid = 0;
		var pid = 0;
		var found = false;
		var l = $('.searchEntityContainer .container-img-profil').length;
		$.each($('.searchEntityContainer .container-img-profil'), function (i, val) {

			if ($(val).attr('href') == hash) {
				found = i;

				return false;
			}
		});

		var prevIx = (found == 0) ? l - 1 : found - 1;
		p = $($('.searchEntityContainer .container-img-profil')[prevIx]).attr('href');
		pid = urlCtrl.map(p).id;

		var nextIx = (found == l - 1) ? 0 : found + 1;
		n = $($('.searchEntityContainer .container-img-profil')[nextIx]).attr('href');
		nid = urlCtrl.map(n).id;




		return {
			prev: '<a href="' + p + '" data-modalshow="' + pid + '" class="lbhp text-dark"><i class="fa fa-2x fa-angle-left"></i> </a> ',
			next: '<a href="' + n + '" data-modalshow="' + nid + '" class="lbhp text-dark"> <i class="fa fa-2x fa-angle-right"></i></a>'
		};
	},

	// ********************************
	// EVENT DIRECTORY PANEL
	// ********************************
	eventPanelHtml: function (params) {
		mylog.log("eventPanelHtml", params);
		params.id = params._id.$id;
		params.hash = "#page.type." + params.collection + ".id." + params.id;

		if (directory.dirLog) mylog.log('-----------eventPanelHtml', params);
		var str = '';
		str += '<div class="swiper-slide">';
		str += '<div id="entity_' + params.collection + '_' + params.id + '" class=\'col-lg-4 col-md-4 col-sm-6 col-xs-12 searchEntityContainer smartgrid-slide-element start-masonry events contain_' + params.collection + '_' + params.id + ' \'>';
		str += '<div class=\'searchEntity card-event commonConfigCard\' id=\'entity' + params.id + '\'>';
		var dateFormated = directory.getDateFormated(params);

		params.attendees = '';
		var cntP = 0;
		var cntIv = 0;

		if (typeof params.links != 'undefined')
			if (typeof params.links.attendees != 'undefined') {
				$.each(params.links.attendees, function (key, val) {
					if (typeof val.isInviting != 'undefined' && val.isInviting == true)
						cntIv++;
					else
						cntP++;
				});
			}
		if (isFromActivityPub(params)) {
			cntP = typeof params.participantCount != 'undefined' ? params.participantCount : 0;
		}
		params.attendees = '<hr class="margin-top-10 margin-bottom-10">';
		if (typeof params.id != 'undefined' && typeof params.collection != 'undefined')
			params.attendees += directory.socialToolsHtml(params);
		/*var isFollowed=false;
		if(typeof params.isFollowed != 'undefined' ) isFollowed=true;
		  
		if(userId != null && userId != '' && params.id != userId){

			var isShared = false;

			params.attendees += '<button id=\'btn-share-event\' class=\'btn btn-share-event btn-xs btn-share\''+
							  ' data-ownerlink=\'share\' data-id=\''+params.id+'\' data-type=\''+params.collection+'\' '+//data-name='"+params.name+"'"+
							  ' data-isShared=\''+isShared+'\'>'+
							  '<i class=\'fa fa-share\'></i> '+trad['share']+'</button>';
		}
		if(userId != null && userId != '' && params.id != userId ){
			var tip = trad['interested'];
			var actionConnect='follow';
			var icon='chain';
			var textBtn= '<i class=\'fa fa-chain\'></i> '+trad['participate'];
			var classBtn='text-white';
			if(isFollowed){
				actionConnect='unfollow';
				icon='unlink';
				classBtn='text-white';
				textBtn= '<i class=\'fa fa-unlink\'></i> '+trad['alreadyAttendee'];
			}
			params.attendees += '<a href="javascript:;" class="btn btn-xs btn-event-participate tooltips followBtn '+classBtn+'"' + 
					  'data-toggle="tooltip" data-placement="left" data-original-title="'+tip+'"'+
					  ' data-ownerlink=\''+actionConnect+'\' data-id=\''+params.id+'\' data-type=\''+params.collection+'\' data-name=\''+params.name+'\''+
					  ' data-isFollowed=\''+isFollowed+'\'>'+
					  textBtn+
					'</a>';
		}*/

		params.attendees += '<small class="light margin-left-10 tooltips pull-left"  ' +
			'data-toggle="tooltip" data-placement="top" data-original-title="' + trad['guest-s'] + '">' +
			cntIv + ' <i class="fa fa-envelope"></i>' +
			'</small>';

		params.attendees += '<small class="light margin-left-10 tooltips pull-left"  ' +
			'data-toggle="tooltip" data-placement="top" data-original-title="' + trad['attendee-s'] + '">' +
			cntP + ' <i class="fa fa-street-view"></i>' +
			'</small>';

		var countSubEvents = (params.links && params.links.subEvents) ? '<br/><i class=\'fa fa-calendar\'></i> ' + Object.keys(params.links.subEvents).length + ' ' + trad['subevent-s'] : '';
		/*var defaultImgDirectory = '<div class="div-img"><i class="fa fa-picture-o fa-5x"></i></div>';
		if('undefined' != typeof directory.costum && notNull(directory.costum)  
					  && typeof directory.costum.results != 'undefined' 
					  && typeof directory.costum.results[params.collection] != 'undefined' 
					  && typeof directory.costum.results[params.collection].defaultImg != 'undefined')
			//defaultImgDirectory= '<img class=\'img-responsive\' onload=\'directory.checkImage(this);\' src=\''+assetPath+directory.costum.results[params.type].defaultImg+'\'/>';
			defaultImgDirectory= '<img class=\'img-responsive\' src=\''+assetPath+directory.costum.results[params.collection].defaultImg+'\'/>';      
		if('undefined' != typeof params.profilMediumImageUrl && params.profilMediumImageUrl != '')
			//defaultImgDirectory= '<img class=\'img-responsive\' onload=\'directory.checkImage(this);\' src=\''+baseUrl+params.profilMediumImageUrl+'\'/>';
			defaultImgDirectory= '<img class=\'img-responsive\' src=\''+baseUrl+params.profilMediumImageUrl+'\'/>';*/

		str += '<div class="card-image">' + params.imageProfilHtml;
		str += '<div class="box-content co-scroll">';
		if (typeof params.type != "undefined") {
			var typeEvent = (typeof tradCategory[params.type] != 'undefined') ? tradCategory[params.type] : params.type;
			str += '<h5 class="text-white lbh add2fav"><i class="fa fa-angle-right text-orange"></i>&nbsp; ' +
				typeEvent;
			str += '</h5>';
		}
		str += params.localityHtml != '' ? '<div class="col-xs-12"><span class="h4 event-locality margin-left-5">' + params.localityHtml + '</span></div>' : '';
		if (isFromActivityPub(params) && 'undefined' != typeof params.organizer && params.organizer != null) {
	
			str += '<div class="col-xs-12 orgenizedby no-padding">';
			str += '<span class="bold">' + tradDynForm.organizedby + ' : </span>';
			str += '<a href="' + params.organizer.url + '" target="_blank" ';
			str += '><img src="' + params.organizer.profilThumbImageUrl + '" class="img-circle img-organizer margin-left-5" width="35" height="35"/> ' + params.organizer.name;
			str += '</a>';
			str += '</div>';
		} else {
			
			if ('undefined' != typeof params.organizer && params.organizer != null) {
				var countOrganizer = Object.keys(params.organizer).length;
				str += '<div class="col-xs-12 orgenizedby no-padding">';
				str += '<span class="bold">' + tradDynForm.organizedby + ' : </span>';
				mylog.log("organizer", params.organizer);
				$.each(params.organizer, function (e, v) {
					var imgIcon = (typeof v.profilThumbImageUrl != 'undefined' && v.profilThumbImageUrl != '') ? baseUrl + '/' + v.profilThumbImageUrl : assetPath + '/images/thumb/default_' + v.type + '.png';
					str += '<a href="#page.type.' + v.type + '.id.' + e + '" class="lbh-preview-element tooltips" ';
					if (countOrganizer > 1) {
						str += 'data-toggle="tooltip" data-placement="top" title="' + v.name + '">';
						str += '<img src="' + imgIcon + '" class="img-circle img-organizer margin-left-5" width="35" height="35"/>';
					}

						if(countOrganizer==1) str+='><img src="'+imgIcon+'" class="img-circle img-organizer margin-left-5" width="35" height="35"/> '+v.name;
		              	str+='</a>';     
					});
					str+='</div>';
				}
		}
        
		if(notNull(params.descriptionStr))
			str +=  '<p class="event-short-description description col-xs-12 margin-top-10 margin-bottom-10">'+params.descriptionStr+'</p>';
		if(typeof params.tagsHtml != "undefined")
			str +=  '<ul class="tag-list">'+params.tagsHtml+'</ul>';

		str +=    '<div class="margin-bottom-10 col-xs-12 no-padding event-bottom">' + 
                    params.attendees + 
                  '</div>';

		if (params.btnShowParticipant) {
		str += 	   '<div class="allParticipant">'+
						`<button onclick="showAllAtendee.actions.listParticipant('${params.collection}','${params._id.$id}');" > Afficher les participants </button>`+
					'</div>'
				  ;
		}
                            
		str += '</div>';
		str += '</div>';
		str += '<div class="col-xs-12 card-content entityName">';
		if (isFromActivityPub(params)) {
			str += '<button class="btn-link text-decoration-none letter-orange text-bold no-margin h4" style="line-height:1.4" id="btn-activity-link" data-activity-link=' + params.objectId + '>' + params.name + '</button>';
		} else {
			str += '<h4><a  href="' + params.hash + '" class="lbh add2fav">' + params.name + '</a></h4>';
		}

		if (params.providerInfo != undefined) {
			str += '<div style="display:flex;justify-content:end;align-items:center" class="federation-provider-info">';
			str += '<div style="display: flex;flex-direction: column;align-items: flex-start;padding-right:5px">';
			str += '<span style="font-size: 11px;background-color: #777;color: white;padding: 1px 4px;border-radius: 2px;">envoyé depuis</span>';
			str += '<a style="text-decoration: none;font-size: 14px;transition: .2s;" href="' + params.providerInfo.url + '" target="__blank">';
			str += params.providerInfo.name;
			str += '</a>';
			str += '</div>';
			str += '<img style="height: 30px;margin-left: 5px;" src="' + params.providerInfo.icon + '" alt="' + params.providerInfo.name + '" width="32" height="32">'
			str += '</div>';
		}
		str += '</div>';
		str += '<div class="col-xs-12 card-date">' + dateFormated + countSubEvents;
		str += '</div>';

		str += '</div>';
		str += '</div>';
		str += '</div>';
		return str;
	},

	// ********************************
	// CROWDFUNDING DIRECTORY PANEL
	// ********************************
	crowdfundingPanelHtml: function (params) {
		mylog.log("crowdfundingPanelHtml", params);

		function get_progress_values() {
			var currency_symbol = "€";
			var min_perc = 1;  // We'd like to show something even if either quantity equals zero.

			// if (typeof params.goal == "undefined") {
			// 	return {
			// 		goal: "Undefined",
			// 		donations: params.donationNumber.toString() + currency_symbol,
			// 		pledges: params.pledgeNumber.toString() + currency_symbol,
			// 		donations_perc: min_perc.toString(),
			// 		pledges_perc: min_perc.toString()
			// 	};
			// }

			// Compute amounts
			var goal = (typeof params.goal != "undefined") ? parseInt(params.goal) : 0;
			var donations = (typeof params.donationNumber != "undefined") ? params.donationNumber : 0;
			var pledges = (typeof params.pledgeNumber != "undefined") ? params.pledgeNumber : 0;
			var rest = goal - donations - pledges;

			// Compute percentages
			var donations_perc = Math.ceil(donations / goal * 100);
			var pledges_perc = "";
			if (rest <= 0) {
				pledges_perc = 100 - donations_perc;
			} else {
				pledges_perc = Math.ceil(pledges / goal * 100);
			}

			// Clamp
			if (donations_perc <= min_perc) {
				donations_perc = min_perc;
				pledges_perc = Math.min(pledges_perc, 100 - min_perc);
			}
			if (pledges_perc <= min_perc) {
				pledges_perc = min_perc;
				donations_perc = Math.min(donations_perc, 100 - min_perc);
			}


			return {
				goal: goal.toString() + currency_symbol,
				donations: donations.toString() + currency_symbol,
				pledges: pledges.toString() + currency_symbol,
				donations_perc: donations_perc.toString(),
				pledges_perc: pledges_perc.toString()
			};
		}
		var prog = get_progress_values();


		var parentId = Object.keys(params.parent)[0];
		var pic_url = baseUrl;
		pic_url += (typeof params.profilMediumImageUrl) != "undefined" ? params.profilMediumImageUrl : defaultImage;
		var iban = (typeof params.iban != "undefined") ? params.iban : "";
		var donationPlatform = (typeof params.donationPlatform != "undefined") ? params.donationPlatform : "";
		var directDonation = (typeof params.directDonation != "undefined") ? params.directDonation : false;
		var name = (typeof params.nameCamp != "undefined") ? params.nameCamp : params.name;
		var description = ((params.collection == "projects")) ? params.descriptionCamp : ((typeof params.description != "undefined") ? params.description : null);
		var hash = (typeof params.idCamp != "undefined") ? "#page.type.crowdfunding.id." + params.idCamp : params.hash;
		// $iban = isset($camp["iban"]) ? $camp["iban"] : "" ;
		// $donationPlatform = isset($camp["donationPlatform"]) ? $camp["donationPlatform"] : "" ;
		var str = '';
		str += '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 searchEntityContainer simplePanelHtml ' + params.containerClass + 'contain_' + params.collection + '_' + params.id + ' campaign-vignette">';
		str += '<div class="searchEntity" id="entity' + params.id + '">';
		str += '<div class="panel-heading row">';
		str += '<div class="col-xs-12 col-sm-6 col-md-6">';
		str += '<a href="' + hash + '" class="lbh-preview-element">';
		str += '<h3 id="col2" class="no-margin">' + name + '</h3><br>';
		str += '<h4 id="col2" class="no-margin">' + params.parent[parentId].name + '</h4>';
		str += '</a>';
		str += '</div>';
		str += '<div class="col-xs-12 col-sm-6 col-md-6 group-amount-target-ctn">';
		//	str+=					'<a href="#'+params.hash+'" target="_blank">';
		str += '<span class="btn btn-default pull-right campaign-goal" data-goal="' + params.goal + '" data-description-camp="' + description + '" >';
		str += trad.goal + ' : <span class="badge">' + prog.goal + '</span>';
		str += '</span>';
		//	str+=					'</a>';
		str += '</div>';
		str += '</div>';
		str += '<div class="panel-body row">';
		str += '<div class="col-xs-12 col-sm-3 col-md-3">';
		str += '<div class="square-pic-group">';
		str += '<img src="' + pic_url + '" class="img-responsive"/>';
		str += '</div>';
		str += '</div>';
		str += '<div class="col-md-9 col-sm-9 col-xs-12 campaign-progress">';
		str += '<div class="row">';
		str += '<div class="col-md-12">';
		str += '<div class="progress tooltips addDonation" href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="Ajuster le montant" data-id="' + parentId + '" data-name="' + params.parent[parentId].name + '" data-type="' + params.parent[parentId].type + '" data-id-camp="' + params.id + '" data-name-camp="' + params.name + '" data-iban="' + iban + '" data-donationPlatform="' + donationPlatform + '" data-directDonation="' + directDonation + '" data-goal="' + parseInt(params.goal) + '" data-pledgeamount="' + params.pledgeNumber + '" data-donationamount="' + params.donationNumber + '" data-amount="" >';
		str += '<div class="progress-bar donation-bar" role="progressbar" style="width:' + prog.donations_perc + '%"></div>';
		str += '<div class="progress-bar pledge-bar" role="progressbar" style="width:' + prog.pledges_perc + '%"></div>';
		str += '</div>';
		str += '</div>';
		str += '</div>';
		str += '<div class="row">';
		str += '<div class="col-md-4 col-xs-12 progress-bar-key">';
		str += '<div><span class="label donation-label">' + trad.donations + ' : ' + prog.donations + '</span></div>';
		str += '<div><span class="label pledge-label">' + trad.pledges + ' : ' + prog.pledges + '</span></div>';
		str += '</div>';
		str += '<div class="col-md-6 col-xs-12 text-center">';
		str += '<a data-id="' + parentId + '" data-name="' + params.parent[parentId].name + '" data-type="' + params.parent[parentId].type + '" data-id-camp="' + params.id + '" data-name-camp="' + params.name + '" data-iban="' + iban + '" data-donationPlatform="' + donationPlatform + '" data-directDonation="' + directDonation + '" data-goal="' + parseInt(params.goal) + '" data-donationamount="' + params.donationNumber + '" data-pledgeamount="' + params.pledgeNumber + '" class="addDonation btn btn-lg btn-primary">' + trad.iCofundThisCampaign + '</a>';
		str += '</div>';
		str += '</div>';
		str += '</div>';
		str += '</div>';
		str += '</div>';
		str += '</div>';

		return str;
	},

	// ********************************
	// OLD EVENT DIRECTORY PANEL
	// ********************************
	eventPanelHtmlFullWidth: function (params) {
		mylog.log("eventPanelHtml", params);
		params.id = params._id.$id;
		params.hash = "#page.type." + params.collection + ".id." + params.id;

		if (directory.dirLog) mylog.log('-----------eventPanelHtml', params);
		var str = '';
		str += '<div class=\'col-xs-12 searchEntityContainer events contain_' + params.collection + '_' + params.id + ' \'>';
		str += '<div class=\'searchEntity\' id=\'entity' + params.id + '\'>';
		var dateFormated = directory.getDateFormated(params);

		params.attendees = '';
		var cntP = 0;
		var cntIv = 0;

		if (typeof params.links != 'undefined')
			if (typeof params.links.attendees != 'undefined') {
				$.each(params.links.attendees, function (key, val) {
					if (typeof val.isInviting != 'undefined' && val.isInviting == true)
						cntIv++;
					else
						cntP++;
				});
			}

		params.attendees = '<hr class="margin-top-10 margin-bottom-10">';

		var isFollowed = false;
		if (typeof params.isFollowed != 'undefined') isFollowed = true;

		if (userId != null && userId != '' && params.id != userId) {

			var isShared = false;

			params.attendees += '<button id=\'btn-share-event\' class=\'btn btn-share-event btn-xs btn-share\'' +
				' data-ownerlink=\'share\' data-id=\'' + params.id + '\' data-type=\'' + params.collection + '\' ' +//data-name='"+params.name+"'"+
				' data-isShared=\'' + isShared + '\'>' +
				'<i class=\'fa fa-share\'></i> ' + trad['share'] + '</button>';
		}
		if (userId != null && userId != '' && params.id != userId) {
			var tip = trad['interested'];
			var actionConnect = 'follow';
			var icon = 'chain';
			var textBtn = '<i class=\'fa fa-chain\'></i> ' + trad['participate'];
			var classBtn = 'text-white';
			if (isFollowed) {
				actionConnect = 'unfollow';
				icon = 'unlink';
				classBtn = 'text-white';
				textBtn = '<i class=\'fa fa-unlink\'></i> ' + trad['alreadyAttendee'];
			}
			params.attendees += '<a href="javascript:;" class="btn btn-xs btn-event-participate tooltips followBtn ' + classBtn + '"' +
				'data-toggle="tooltip" data-placement="left" data-original-title="' + tip + '"' +
				' data-ownerlink=\'' + actionConnect + '\' data-id=\'' + params.id + '\' data-type=\'' + params.collection + '\' data-name=\'' + params.name + '\'' +
				' data-isFollowed=\'' + isFollowed + '\'>' +
				textBtn +
				'</a>';
		}

		params.attendees += '<small class="light margin-left-10 tooltips pull-left"  ' +
			'data-toggle="tooltip" data-placement="bottom" data-original-title="' + trad['guest-s'] + '">' +
			cntIv + ' <i class="fa fa-envelope"></i>' +
			'</small>';

		params.attendees += '<small class="light margin-left-10 tooltips pull-left"  ' +
			'data-toggle="tooltip" data-placement="bottom" data-original-title="' + trad['attendee-s'] + '">' +
			cntP + ' <i class="fa fa-street-view"></i>' +
			'</small>';

		var countSubEvents = (params.links && params.links.subEvents) ? '<br/><i class=\'fa fa-calendar\'></i> ' + Object.keys(params.links.subEvents).length + ' ' + trad['subevent-s'] : '';
		var defaultImgDirectory = '<div class="div-img"><i class="fa fa-picture-o fa-5x"></i></div>';
		if ('undefined' != typeof directory.costum && notNull(directory.costum)
			&& typeof directory.costum.results != 'undefined'
			&& typeof directory.costum.results[params.collection] != 'undefined'
			&& typeof directory.costum.results[params.collection].defaultImg != 'undefined')
			//defaultImgDirectory= '<img class=\'img-responsive\' onload=\'directory.checkImage(this);\' src=\''+assetPath+directory.costum.results[params.type].defaultImg+'\'/>';
			defaultImgDirectory = '<img class=\'img-responsive\' src=\'' + assetPath + directory.costum.results[params.collection].defaultImg + '\'/>';
		if ('undefined' != typeof params.profilMediumImageUrl && params.profilMediumImageUrl != '')
			//defaultImgDirectory= '<img class=\'img-responsive\' onload=\'directory.checkImage(this);\' src=\''+baseUrl+params.profilMediumImageUrl+'\'/>';
			defaultImgDirectory = '<img class=\'img-responsive\' src=\'' + baseUrl + params.profilMediumImageUrl + '\'/>';

		str += '<div class="col-xs-12 col-sm-4 col-md-4 no-padding">';
		str += '<a href="' + params.hash + '" class="container-img-profil lbh add2fav block">' + defaultImgDirectory + '</a>';
		str += '</div>';
		str += '<div class="col-md-8 col-sm-8 col-xs-12 no-padding bg-img-event-header">';
		// '<div class="col-md-8 col-sm-8 col-xs-12 no-padding bg-img-event-header" style="background:url(\''+baseUrl+params.profilMediumImageUrl+'\')">';
		str += '<div class="col-xs-12 no-padding header-opacity">';
		str += '<div class="col-md-6 col-sm-6 col-xs-12 entityName margin-top-20">';
		str += '<h4><a href="' + params.hash + '" class="entityName lbh add2fav">' +
			params.name +
			'</a></h4>';
		str += '</div>';
		str += '<div class="col-md-6 col-sm-6 col-xs-12 margin-top-20">';
		str += '<i class="fa fa-calendar fa-2x date-icon"></i> ' + dateFormated + countSubEvents;
		str += '</div>';

		str += '</div>';
		str += '</div>';


		str += '<div class="col-md-8 col-sm-8 col-xs-12 entityRight">';

		if (typeof params.type != "undefined") {
			var typeEvent = (typeof tradCategory[params.type] != 'undefined') ? tradCategory[params.type] : params.type;
			str += '<div class="col-md-4 col-sm-5 col-xs-12">';
			str += '<h5 class="text-dark lbh add2fav"><i class="fa fa-angle-right text-orange"></i>&nbsp; ' +
				typeEvent;
			str += '</h5>';
			str += '</div>';
		}
		str += params.localityHtml != '' ? '<div class="col-md-8 col-sm-7 col-xs-12"><span class="h4 event-locality margin-left-5">' + params.localityHtml + '</span></div>' : '';
		if ('undefined' != typeof params.organizer && params.organizer != null) {
			var countOrganizer = Object.keys(params.organizer).length;
			str += '<div class="col-xs-12 orgenizedby no-padding">';
			str += '<span class="bold">' + tradDynForm.organizedby + ' : </span>';
			$.each(params.organizer, function (e, v) {
				var imgIcon = (typeof v.profilThumbImageUrl != 'undefined' && v.profilThumbImageUrl != '') ? baseUrl + '/' + v.profilThumbImageUrl : assetPath + '/images/thumb/default_' + v.type + '.png';
				str += '<a href="#page.type.' + v.type + '.id.' + e + '" class="lbh-preview-element tooltips" ';
				if (countOrganizer > 1) str += 'data-toggle="tooltip" data-placement="top" title="' + v.name + '"';
				str += '>';
				if (countOrganizer == 1) str += '<img src="' + imgIcon + '" class="img-circle img-organizer margin-left-5" width="35" height="35"/> ' + v.name;
				str += '</a>';
			});
			str += '</div>';
		}


		if (notNull(params.descriptionStr))
			str += '<p class="text-dark event-short-description col-xs-12 margin-top-10 margin-bottom-10">' + params.descriptionStr + '</p>';
		if (typeof params.tagsHtml != "undefined")
			str += '<ul class="tag-list">' + params.tagsHtml + '</ul>';

		str += '<div class="margin-bottom-10 col-md-12 no-padding event-bottom">' +
			params.attendees +

			'</div>';
		str += '</div>';

		str += '</div>';
		str += '</div>';

		str += '</div>';
		return str;
	},

	// ********************************
	// CITY DIRECTORY PANEL
	// ********************************
	cityPanelHtml: function (params) {
		mylog.log('-----------cityPanelHtml', params);
		var domContainer = (notNull(params.input)) ? params.input + ' .scopes-container' : '';
		var typeSearchCity = 'city';
		var levelSearchCity = 'city';


		var str = '';
		str += '<a href="javascript:" class="col-md-12 col-sm-12 col-xs-12 no-padding communecterSearch item-globalscope-checker searchEntity" ';
		str += 'data-scope-value="' + params.key + '"' +
			'data-scope-name="' + params.name + '"' +
			'data-scope-level="' + levelSearchCity + '"' +
			'data-scope-type="' + typeSearchCity + '"' +
			'data-scope-notsearch="' + true + '"' +
			'data-append-container="' + domContainer + '"';
		str += '>';
		str += '<div class="col-xs-12 margin-bottom-10 ' + params.type + ' ' + params.elTagsList + ' ">';
		str += '<div class="padding-10 informations">';
		str += '<div class="entityRight no-padding">';

		var title = '<span> ' + params.name + ' ' + (notEmpty(params.postalCode) ? ' - ' + params.postalCode : '') + '</span>';

		var subTitle = '';
		if (notEmpty(params.level4Name))
			subTitle += (subTitle == '' ? '' : ', ') + params.level4Name;
		if (notEmpty(params.level3Name))
			subTitle += (subTitle == '' ? '' : ', ') + params.level3Name;
		if (notEmpty(params.level2Name))
			subTitle += (subTitle == '' ? '' : ', ') + params.level2Name;

		subTitle += (subTitle == '' ? '' : ', ') + params.country;
		str += ' <span class="entityName letter-red ">' +
			'<i class="fa fa-university"></i>' + title +
			'<br/>' +
			'<span style="color : grey; font-size : 13px">' + subTitle + '</span>' +
			'</span>';

		str += '</div>';
		str += '</div>';
		str += '</div>';
		str += '</a>';
		return str;
	},
	// ********************************
	// Zone DIRECTORY PANEL
	// ********************************
	zonePanelHtml: function (params) {
		mylog.log('-----------zonePanelHtml', params);
		var domContainer = (notNull(params.input)) ? params.input + ' .scopes-container' : '';
		var typeSearchCity, levelSearchCity;
		if (params.level.indexOf('1') >= 0) {
			typeSearchCity = 'level1';
			levelSearchCity = '1';
		} else if (params.level.indexOf('2') >= 0) {
			typeSearchCity = 'level2';
			levelSearchCity = '2';
		} else if (params.level.indexOf('3') >= 0) {
			typeSearchCity = 'level3';
			levelSearchCity = '3';
		} else if (params.level.indexOf('4') >= 0) {
			typeSearchCity = 'level4';
			levelSearchCity = '4';
		} else if (params.level.indexOf('5') >= 0) {
			typeSearchCity = 'level5';
			levelSearchCity = '5';
		}

		var subTitle = '';

		if (notEmpty(params.level4) && params.id != params.level4) {
			subTitle += (subTitle == '' ? '' : ', ') + params.level4Name;
		}
		if (notEmpty(params.level3) && params.id != params.level3) {
			subTitle += (subTitle == '' ? '' : ', ') + params.level3Name;
		}
		if (notEmpty(params.level2) && params.id != params.level2) {
			subTitle += (subTitle == '' ? '' : ', ') + params.level2Name;
		}

		var str = '';
		str += '<a href="javascript:" class="col-md-12 col-sm-12 col-xs-12 no-padding communecterSearch item-globalscope-checker searchEntity" ';
		str += 'data-scope-value="' + params.key + '" ' +
			'data-scope-name="' + params.name + '" ' +
			'data-scope-level="' + levelSearchCity + '" ' +
			'data-scope-type="' + typeSearchCity + '" ' +
			//'data-scope-values="'+JSON.stringify(valuesScopes)+'" ' +
			'data-scope-notsearch="' + true + '" ' +
			'data-append-container="' + domContainer + '" ';
		str += '>';
		str += '<div class="col-xs-12 margin-bottom-10 ' + params.type + ' ' + params.elTagsList + ' ">';
		str += '<div class="padding-10 informations">';
		str += '<div class="entityRight no-padding">';
		// params.hash = '; //'main-col-search';
		// params.onclick = "setScopeValue($(this))"; //'"+params.name.replace('"', '\"')+"');";
		// params.onclickCp = "setScopeValue($(this));";
		// params.target = ';
		// params.dataId = params.name; 

		var title = '<span>' + params.name + '</span>';
		subTitle += (subTitle == '' ? '' : ', ') + params.countryCode;
		str += ' <span class="entityName letter-red">' +
			'<i class="fa fa-bullseye bold text-red"></i>' +
			'<i class="fa bold text-dark">' +
			levelSearchCity +
			'</i> ' +
			title +
			'<br/>' +
			'<span style="color : grey; font-size : 13px">' + subTitle + '</span>' +
			'</span>';

		str += '</div>';
		str += '</div>';
		str += '</div>';
		str += '</a>';
		return str;
	},
	networkPanelHtml: function (params, key) {

		mylog.log('-----------networkPanelHtml', params, key);
		params.title = escapeHtml(params.title);
		var str = '';

		str += '<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 searchEntityContainer contain_' + params.type + '_' + params.id + '">';
		str += '<div class="searchEntity" id="entity' + params.id + '">';


		if (userId != null && userId != '' && params.id != userId /*&& !inMyContacts(params.typeSig, params.id)*/ && location.hash.indexOf('#page') < 0 && searchObject.initType != 'all') {
			var isFollowed = false;

			if (typeof params.isFollowed != 'undefined')
				isFollowed = true;
			mylog.log('isFollowed', params.isFollowed, isFollowed);

			str += '<a href="javascript:;" class="btn btn-default btn-sm btn-add-to-directory bg-white tooltips followBtn"' +
				' data-toggle="tooltip" data-placement="left" data-original-title="' + trad['Follow'] + '"' +
				' data-ownerlink="follow" data-id="' + params.id + '" data-type="' + params.type + '" data-name="' + params.skin.title + '" data-isFollowed="' + isFollowed + '">' +
				'<i class="fa fa-chain"></i>' +
				'</a>';
		}
		str += '<div class="panel-heading border-light col-xs-12">';
		str += '<a href="' + baseUrl + '/network/default/index?src=' + baseUrl + '/' + moduleId + '/network/get/id/' + params.id + '" target="_blank" class="text-dark">';
		str += '<h4 class="panel-title text-dark pull-left">' + params.skin.title + '</h4></a>';
		str += '<span class="col-xs-12">' + (notNull(params.skin.shortDescription) ? params.skin.shortDescription : '') + '</span>';
		str += '</div>';

		if (typeof params.edit != 'undefined' && params.edit == true) {
			str += '<ul class="nav navbar-nav margin-5 col-md-12">';

			str += '<li class="text-red pull-right">';
			str += '<a href="javascript:;"  onclick="removeNetwork("' + params.id + '");" class="margin-left-5 bg-white tooltips btn btn-link btn-sm" ' +
				'data-toggle="tooltip" data-placement="top" data-original-title="' + trad['delete'] + '" >';
			str += '<i class="fa fa-trash"></i>' + trad['delete'];
			str += '</a>';
			str += '</li>';

			str += '<li class="text-red pull-right">';
			str += '<a href="javascript:;" onclick="updateNetwork("' + params.id + '");" ' +
				'class="bg-white tooltips btn btn-link btn-sm" data-toggle="tooltip" data-placement="top" data-original-title="' + trad['update'] + '" >';
			str += '<i class="fa fa-pencil"></i>' + trad['update'];
			str += '</a>';
			str += '</li>';

			str += '<li class="text-red pull-right">';
			str += '<a href="javascript:;" onclick="" ' +
				'class="bg-white tooltips btn btn-link btn-sm" data-toggle="tooltip" data-placement="top" data-original-title="' + trad['share'] + '" >';
			str += '<i class="fa fa-pencil"></i>' + trad['share'];
			str += '</a>';
			str += '</li>';

			str += '</ul>';
		}
		str += '</div>';
		str += '</div>';
		return str;
	},

	network2PanelHtml: function (params) {
		mylog.log('----------- network2PanelHtml', params);
		params.hash = baseUrl + '/network/default/index?src=' + baseUrl + '/' + moduleId + '/network/get/id/' + params.id;
		var str = '';
		var classType = params.type;
		str += '<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 searchEntityContainer ' + classType + ' ' + params.elTagsList + ' ' + params.elRolesList + ' contain_' + params.type + '_' + params.id + '">';
		str += '<div class="searchEntity" id="entity' + params.id + '">';

		if (typeof params.edit != 'undefined')
			str += this.getAdminToolBar(params);

		if (params.updated != null)
			str += '<div class="dateUpdated"><i class="fa fa-flash"></i> <span class="hidden-xs">' + trad.actif + ' </span>' + params.updated + '</div>';

		str += '<a href="' + params.hash + '" target="_blank" class="container-img-banner add2fav >' + params.imgBanner + '</a>';
		str += '<div class="padding-10 informations">';

		str += '<div class="entityRight no-padding">';

		if (typeof params.size == 'undefined' || params.size == undefined || params.size == 'max') {
			str += '<div class="entityCenter no-padding">';
			str += '<a href="' + params.hash + '" target="_blank" class="container-img-profil add2fav ">' + params.imgProfil + '</a>';
			str += '<a href="' + params.hash + '" target="_blank" class="add2fav pull-right margin-top-15 ">' + params.htmlIco + '</a>';
			str += '</div>';
		}

		str += '<a href="' + params.hash + '" target="_blank" class="' + params.size + ' entityName bold text-dark add2fav ">' + params.skin.title + '</a>';

		str += '<div class="entityDescription">' + ((params.skin.shortDescription == null) ? '' : params.skin.shortDescription) + '</div>';
		str += '<div class="tagsContainer">';

		if (typeof params.edit != 'undefined' && params.edit == true) {
			str += '<ul class="nav text-center">';
			str += '<li class="text-red pull-right">';
			str += '<a href="javascript:;"  onclick="removeNetwork("' + params.id + '");" class="margin-left-5 bg-white tooltips btn btn-link btn-sm" ' +
				'data-toggle="tooltip" data-placement="top" data-original-title="' + trad['delete'] + '" >';
			str += '<i class="fa fa-trash"></i>';
			str += '</a>';
			str += '</li>';

			str += '<li class="text-red pull-right">';
			str += '<a href="javascript:;" onclick="updateNetwork("' + params.id + '");" ' + 'class="margin-left-5 bg-white tooltips btn btn-link btn-sm" data-toggle="tooltip" data-placement="top" data-original-title="' + trad['update'] + '" >';
			str += '<i class="fa fa-pencil"></i>';
			str += '</a>';
			str += '</li>';


			str += '</ul>';
		}
		str += '</div>';

		str += '</div>';
		str += '</div>';
		str += '</div>';
		str += '</div>';
		return str;
	},

	coopPanelHtml: function (params) {
		//params.invite = true;
		mylog.log('-----------proposalPanelHtml', params);
		var idParentRoom = typeof params.idParentRoom != 'undefined' ? params.idParentRoom : '';
		if (idParentRoom == '' && params.collection == 'rooms') idParentRoom = params.id;
		//mylog.log("-----------idParentRoom", idParentRoom);

		var name = params.name;

		name = escapeHtml(name);
		var thisId = typeof params['_id'] != 'undefined' &&
			typeof params['_id']['$id'] != 'undefined' ? params['_id']['$id'] :
			typeof params['id'] != 'undefined' ? params['id'] : '';

		var filterClass = '';
		var sortData = '';
		if (typeof params.votes != 'undefined') {
			if (typeof params.votes.up != 'undefined') {
				filterClass += ' upFilter';
				sortData += ' data-sort="' + params.votes.up.length + '" ';
			}
			if (typeof params.votes.down != 'undefined') {
				filterClass += ' downFilter';
				sortData += ' data-sort="' + params.votes.down.length + '"';
			}
		}

		var str = '';
		//	if(size == 'S')     
		str += '<div class=\'col-lg-4 col-md-4 col-sm-6 col-xs-12 blockCoop' + thisId + ' ' + filterClass + ' coopFilter coop-wraper searchEntityContainer smartgrid-slide-element margin-bottom-10 \' ' + sortData + ' style=\'word-wrap: break-word; \'>';
		//	else 
		//		str += '<div class=\'col-xs-12 coop-wraper margin-bottom-10 coopFilter '+filterClass+' \' '+sortData+' style=\'word-wrap: break-word;\'>';

		var linkParams = ' data-coop-type=\'' + params.collection + '\' ' +
			' data-coop-id=\'' + thisId + '\' ' +
			' data-coop-idparentroom=\'' + idParentRoom + '\' ' +
			' data-coop-parentid=\'' + params.parentId + '\' ' + 'data-coop-parenttype=\'' + params.parentType + '\' ';

		str += '<div class="blockCoop' + params.parentId + ' searchEntity coopPanelHtml" ' + linkParams + '>';
		var diffColor = (params.sourceKey) ? 'border-left:3px solid red;' : '';
		str += '<div class="panel-heading border-light col-lg-12 col-xs-12" style="' + diffColor + '">';


		// IMAGE 
		//if(typeof params.imgMediumProfil == 'undefined'){
		//quand on reload apres un vote on ne passe pas forcement par showResultsDirectoryHtml
		//	if(!params.useMinSize){
		//		params.imgProfil = '<i class="fa fa-image fa-2x"></i>';
		//		params.imgMediumProfil = '<i class="fa fa-image fa-2x"></i>';
		//	}

		//	if('undefined' != typeof params.profilMediumImageUrl && params.profilMediumImageUrl != '')
		//		params.imgMediumProfil= '<img class="img-responsive" onload="directory.checkImage(this);" src="'+baseUrl+params.profilMediumImageUrl+'"/>';
		//}
		str += '<a href="javascript:;" ' + linkParams + ' class="all-coop-detail-desc' + thisId + ' openCoopPanelHtml container-img-profil">' + params.imageProfilHtml + '</a>';

		// OPEN PANEL BTN
		// str += "<button class='btn btn-sm btn-default pull-right openCoopPanelHtml bold letter-turq' "+linkParams+
		//         "><i class='fa fa-chevron-right'></i> <span class='hidden-xs'>"+trad.Open+"</span></button>";

		// NAME
		if (name != '') {
			var objElt = (dyFInputs.get(params.collection)) ? dyFInputs.get(params.collection) : typeObj['default'];
			str += '<a href="javascript:;" class="openCoopPanelHtml" style="text-decoration:none;" ' + linkParams + ' >' +
				'<h4 class="panel-title tooltips" data-toggle="tooltip" data-placement="bottom" data-original-title="' + name + '">' +
				'<i class="fa fa-' + objElt.icon + '"></i> ' +
				((name.length > 100) ? name.substring(0, 100) + '...' : name) + '</h4></a>';
		}

		if (typeof params.producer != 'undefined' &&
			params.producer != null &&
			Object.keys(params.producer).length > 0) {
			var count = Object.keys(params.producer).length;
			var htmlAbout = '';
			$.each(params.producer, function (e, v) {
				var heightImg = (count > 1) ? 35 : 25;
				var imgIcon = (typeof v.profilThumbImageUrl != 'undefined' && v.profilThumbImageUrl != '') ? baseUrl + '/' + v.profilThumbImageUrl : parentModuleUrl + '/images/thumb/default_' + v.type + '.png';

				htmlAbout += '<span ';
				if (count > 1) htmlAbout += 'data-toggle="tooltip" data-placement="left" title="' + v.name + '"';
				htmlAbout += '>' +
					'<img src="' + imgIcon + '" class="img-circle margin-right-10" width=' + heightImg + ' height=' + heightImg + ' />';
				if (count == 1) htmlAbout += v.name;
				//htmlAbout+="</a>";
				htmlAbout += '</span>';
			});
			var htmlHeader = ((params.type == typeObj.event.col) ? trad['Planned on'] : tradCategory.carriedby);
			htmlHeader += ' : ' + htmlAbout;

			str += htmlHeader;
		}

		// STATE OF THE PROPOSAL

		if (params.collection != 'rooms') {
			str += '<h5 class="col-sm-12 no-padding">';
			var statusColor = 'red';
			if (params.status == 'adopted')
				statusColor = 'green';
			else if (params.status == 'refused')
				statusColor = 'red';
			else if (params.status == 'amendable')
				statusColor = 'orange';
			str += '<small class="text-' + statusColor + '"><i class="fa fa-certificate"></i> ' + trad[params.status] + '</small>';

			// YOUR VOTE STATUS
			if (typeof userId != 'undefined' && userId != null && userId != '') {
				if ((params.status == 'tovote' || params.status == 'amendementAndVote') && params.hasVote === false)
					str += '<small class="margin-left-15 letter-red"><i class="fa fa-ban"></i> ' + trad['You did not vote'] + '</small>';
				else if ((params.status == 'tovote' || params.status == 'amendementAndVote') && params.hasVote !== false)
					str += '<small class="margin-left-15"><i class="fa fa-thumbs-up"></i> ' + trad['You did vote'] + '</small>';
			}

			str += '</h5>';

		}



		str += '<div class="all-coop-detail">';

		str += '<div class="all-coop-detail-desc' + thisId + '">';


		if (typeof params.descriptionStr != 'undefined' && params.descriptionStr != '') {
			str += '<hr>';
			str += '<span class="col-xs-12 no-padding text-dark descMD">' + params.descriptionStr + '</span>';
		}
		var voteCount = '';
		if (params.votes) {
			var vc = 0;
			if (params.votes.up)
				vc += Object.keys(params.votes.up).length;
			if (params.votes.down)
				vc += Object.keys(params.votes.down).length;
			if (params.votes.uncomplet)
				vc += Object.keys(params.votes.uncomplet).length;
			if (params.votes.white)
				vc += Object.keys(params.votes.white).length;
			voteCount = ' (' + vc + ')';
		}
		//SHOW HIDE VOTE BTNs
		var btnSize;
		if (typeof userId != 'undefined' && userId != null && userId != '') {
			btnSize = (params.status == 'amendementAndVote') ? '6' : '12';
			if ((params.status == 'tovote' || params.status == 'amendementAndVote') && params.hasVote === false)
				str += '<a href="javascript:;" ' + linkParams + ' data-coop-section="vote"  class="bg-green  openCoopPanelHtml btn col-sm-' + btnSize + ' "><i class="fa fa-gavel"></i> ' + trad.Vote + voteCount + '</a>';

			else if ((params.status == 'tovote' || params.status == 'amendementAndVote') && params.hasVote !== false)
				str += '<a href="javascript:;" ' + linkParams + ' data-coop-section="vote"  class="openCoopPanelHtml btn btn-default col-sm-' + btnSize + ' "><i class="fa fa-eye"></i> ' + trad['See votes'] + voteCount + '</a>';
		} else {
			btnSize = (params.status == 'amendementAndVote') ? '6' : '12';
			if ((params.status == 'tovote' || params.status == 'amendementAndVote') && params.hasVote === false)
				str += '<a href="javascript:" data-toggle="modal" data-target="#modalLogin"  class="btn-menu-connect bg-green btn col-sm-' + btnSize + ' "><i class="fa fa-gavel"></i> ' + trad.Vote + voteCount + '</a>';
		}

		if ((params.status == 'amendementAndVote' || params.status == 'amendable')) {
			var amendCount = (params.amendements) ? ' (' + Object.keys(params.amendements).length + ')' : '';
			str += '<a href="javascript:;" ' + linkParams + ' data-coop-section="amendments"  class="openCoopPanelHtml btn btn-default text-purple col-sm-6 "><i class="fa fa-list"></i> ' + trad.Amendements + amendCount + '</a>';
		}

		if (params.votes) {
			str += '<div class="col-sm-12 padding-10">';
			if (params.votes.up)
				str += '<div class="col-sm-3 text-green tooltips text-center vote-counter-entity" data-toggle="tooltip" data-placement="bottom" data-original-title="' + trad.Agree + '"><i class="fa fa-thumbs-up"></i> ' + Object.keys(params.votes.up).length + '</div>';
			if (params.votes.down)
				str += '<div class="col-sm-3 text-red tooltips text-center vote-counter-entity" data-toggle="tooltip" data-placement="bottom" data-original-title="' + trad.Disagree + '"><i class="fa fa-thumbs-down"></i> ' + Object.keys(params.votes.down).length + '</div>';
			if (params.votes.uncomplet)
				str += '<div class="col-sm-3 text-orange tooltips text-center vote-counter-entity" data-toggle="tooltip" data-placement="bottom" data-original-title="' + trad.Uncomplet + '"><i class="fa fa-hand-grab-o"></i> ' + Object.keys(params.votes.uncomplet).length + '</div>';
			if (params.votes.white)
				str += '<div class="col-sm-3 text-dark tooltips text-center vote-counter-entity" data-toggle="tooltip" data-placement="bottom" data-original-title="' + trad.Abstain + '"><i class="fa fa-circle-o"></i> ' + Object.keys(params.votes.white).length + '</div>';
			str += '</div>';
		}




		str += '</div>';

		//VOTE BTNS
		str += '<div class="all-coop-detail-votes' + thisId + ' hide">';

		//BTN toggle description
		str += '<a href="javascript:" data-coop-id="' + thisId + '"  class="btn-openVoteDetail"><i class="text-dark fa fa-arrow-circle-left"></i></a>';

		if ((params.auth || typeof params.idParentRoom == 'undefined')
			&& (params.status == 'tovote' || params.status == 'amendementAndVote')) {


			var isMulti = typeof params.answers != 'undefined';
			var answers = isMulti ? params.answers :
				{ 'up': 'up', 'down': 'down', 'white': 'down', 'uncomplet': 'uncomplet' };

			$.each(answers, function (key, val) {
				var voteRes = (typeof params.voteRes != 'undefined' &&
					typeof params.voteRes[key] != 'undefined') ? params.voteRes[key] : false;

				str += '<div class="col-xs-12 no-padding timeline-panel">';

				if ((params.status == 'tovote' || params.status == 'amendementAndVote') && (!params.hasVote || params.voteCanChange == 'true')) {
					str += '<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2 no-padding text-right pull-left margin-top-20">';

					str += '  <button class="btn btn-send-vote btn-link btn-sm bg-vote bg-' + voteRes['bg-color'] + '"';
					str += '     title="' + trad.clicktovote + '" ';
					str += '      data-idparentproposal="' + thisId + '"';
					str += '      data-idparentroom="' + params.idParentRoom + '"';
					str += '      data-vote-value="' + key + '"><i class="fa fa-gavel"></i>';
					str += '  </button>';

					if (params.hasVote === '' + key)
						str += '<br><i class="fa fa-user-circle padding-10" title="' + trad['You voted for this answer'] + '"></i> ';

					str += '</div>';
				}

				str += '<div class="col-lg-11 col-md-11 col-sm-11 col-xs-10">';

				var hashAnswer = !isMulti ? trad[voteRes['voteValue']] : (key + 1);

				str += '<div class="padding-10 margin-top-15 border-vote border-vote-' + key + '">';
				str += '<i class="fa fa-hashtag"></i><b>' + hashAnswer + '</b> ';
				if (isMulti)
					str += voteRes['voteValue'];
				str += '</div>';

				if (voteRes !== false && voteRes['percent'] != 0) {
					str += '<div class="progress progress-res-vote">';
					str += '<div class="progress-bar bg-vote bg-' + voteRes['bg-color'] + '" role="progressbar" ';
					str += 'style="width:' + voteRes['percent'] + '%">';
					str += voteRes['percent'] + '%';
					str += '</div>';
					str += '<div class="progress-bar bg-transparent" role="progressbar" ';
					str += 'style="width:' + (100 - voteRes['percent']) + '%">';
					str += voteRes['votant'] + ' <i class="fa fa-gavel"></i>';
					str += '</div>';
					str += '</div>';
				}
				str += '</div>';

				str += '</div>';
			});
		}

		str += '</div>';

		str += '</div>';
		str += '</div>';
		str += '</div>';
		str += '</div>';
		return str;

	},
	searchTypeHtml: function () {
		var spanType = '';

		$.each(searchObject.types, function (key, val) {
			mylog.log('directory.searchTypeHtml typeHeader', val, typeObj[val]);
			var params = (searchInterface.isForced('types, category') && typeObj.isDefined(searchObject.forced.category)) ?
				typeObj.get(searchObject.forced.category) : typeObj.get(val);
			spanType += '<span class="text-' + params.color + '">' +
				'<i class="fa fa-' + params.icon + ' hidden-sm hidden-md hidden-lg padding-5"></i> <span class="hidden-xs">' + params.name + '</span>' +
				'</span>';

		});
		return spanType;
	},
	loadingImg: 0,
	checkImageLoad: 0,
	checkImage: function ($this) {
		/*var imgH = $this.scrollHeight;
		  if(imgH > 240){
			marginTop= imgH/3;
			$($this).css({"margin-top":"-"+marginTop+"px"});
		  }*/
	},
	loadingImg: function ($this) {
		$($this).removeClass("isLoading");
	},
	deleteElement: function (type, id, btnCLick, callback, confirmMsg = "", succesMsg = "") {
		mylog.log("directory.deleteElement", typeof callback);
		var urlToSend = baseUrl + "/" + moduleId + "/element/delete/type/" + type + "/id/" + id;

		bootbox.confirm(confirmMsg != "" ? confirmMsg : trad.areyousuretodelete,
			function (result) {
				if (!result) {
					if (typeof btnClick != "undefined" && btnClick != null)
						btnClick.empty().html('<i class="fa fa-trash"></i>');
					return;
				} else {
					ajaxPost(
						null,
						urlToSend,
						{},
						function (data) {
							if (data && data.result) {
								toastr.info(succesMsg != "" ? succesMsg : "élément effacé");
								if ($("#" + type + id).length > 0)
									$("#" + type + id).remove();
								else if ($(".contain_" + type + "_" + id).length > 0)
									$(".contain_" + type + "_" + id).remove();
								else
									urlCtrl.loadByHash(location.hash);
								if ($("#openModal").hasClass("in"))
									$("#openModal").modal("hide");

								if (typeof callback == "function")
									callback(data, type, id);
								if ($("#modal-content-costumizer-administration").css("display") == "block" && $("#modal-content-costumizer-administration").find(`.bodySearchContainer`).length == 1)
									$(`.lbh-costumizer[data-link='template']`).trigger('click')

							} else {
								toastr.error("something went wrong!! please try again.");
							}
						}
					);
				}
			});
	},
	cmsModelListHtml: function (params) {
		mylog.log("cmsModelListHtml", "Params", params);
		if (typeof params.collection == "undefined")
			params.collection = "cms";
		var str = '';
		let state = "";
		if (typeof costum.tplUsed != "undefined") {
			if (typeof costum.tplUsed[params.id] != "undefined") {
				state = costum.tplUsed[params.id]
			}
		}
		str +=	'<div id="entity_'+params.collection+'_'+params.id+'" class="col-lg-4 col-md-6 col-sm-6 col-12 mt-5 searchEntityContainer '+params.containerClass+'">'+
		'<article class="material-card Light-Green">'+
		'<h2 class="mc-expans">'+
		'<span>'+params.name+'</span>'+
		'</h2>'+
		'<div class="mc-content">'+
		'<div class="img-container">' 
		if ( typeof params.screenshot != 'undefined' && params.screenshot != '') {       
			str += '<div class="div-img"><img class="img-responsive" src="'+baseUrl + '/' + params.screenshot[0]+'"></div>'+
			'<a href="'+baseUrl + '/' + params.screenshot[0]+'" class="icon-preview-tpl thumb-info text-center" data-lightbox="' +params.name+params.id+'" data-title="Aperçu de ' + params.name + '">' +
			'<i class="fa fa-search-plus"></i>'+
			'</a>'

		}else{                                     
			str +=' <div class="div-img" style="color: white;"><i class="fa fa-image fa-2x"></i></div>'
		}
		str += '</div>'
		str += '<div class="mc-description ">'; 
		str +=params.descriptionStr
		str += '</div>'+
		'</div>'+
		'<a class="mc-btn-action mc-expans ripple">'+
		'<i class="fa fa-bars"></i>'+
		'</a>'+
		'<div class="mc-footer">'
		if (state == "using") {
			str += 	'<div class="text-center" style="width:100%;">'+
			'<p class="tpl-stat" style="padding:2px;display:none;position:absolute;bottom: 70px;font-size:15px;background-color: #dcedc8;width: 100%;"class="capitalize" style="color:#33691e">('+tradCms.currenttemplate+')</p></div>'
			str += '<a style="min-width: 200px; background-color:#8bc34a;" href="javascript:;" class="btn btn-info btn-link custom-block-cms use-this-template" data-action="'+state+'" data-id="' + params.id + '" data-name="' + params.name + '" data-collection="' + params.collection + '">' +
			'<font style="font-size: 15px" class="capitalize">' +
			'<i class="fa fa-refresh padding-right-10"></i> '+trad.reset+' </font>' +
			'</a>';
		}else if(state == "used"){
			str += '<div class="text-center" style="width:100%;">'+
			'<p class="tpl-stat" style="padding:2px;display:none;position:absolute;bottom: 70px;font-size:15px;background-color: #dcedc8;width: 100%; " class="capitalize" style="color:#33691e">('+tradCms.used+')</p></div>'
			str += '<a style="min-width: 200px; background-color:#8bc34a;" href="javascript:;" class="btn btn-info btn-link custom-block-cms use-this-template" data-action="'+state+'" data-id="' + params.id + '" data-name="' + params.name + '" data-collection="' + params.collection + '">' +
			'<font style="font-size: 15px" class="capitalize">' +
			'<i class="fa fa-history padding-right-10"></i> '+tradCms.useagain+' </font>' +
			'</a>';
		}else{
			str +='<a style="min-width: 200px; background-color:#8bc34a;" href="javascript:;" class="btn btn-info btn-link custom-block-cms use-this-template" data-action="'+state+'" data-id="' + params.id + '" data-name="' + params.name + '" data-collection="' + params.collection + '">' +
			'<font style="font-size: 15px">' +
			'<i class="fa fa-check padding-right-10"></i> ' + trad.choose + '</font>' +
			'</a>';
		}
		str += '<a style="min-width: 200px; background-color:#8bc34a;" class="btn btn-info btn-link go-to" target="_blank" href="' +baseUrl+'/costum/co/index/slug/'+ params.source.key + '">'+
		'<font style=""><i class="fa fa-eye padding-right-10"></i>Voir le demo</a></font>'
		str += '<div style="width:100%;display: flex;justify-content: center;padding: 5px;">'
		if (Object.keys(params["parent"]).includes(costum.contextId) && (typeof userConnected.roles != "undefined" && typeof userConnected.roles.superAdmin != "undefined" && userConnected.roles.superAdmin == true)) {
			str += "<a href='javascript:;' class='btn btn-info btn-link' onclick='costumizer.template.actions.update(\"" + params.id + "\",\"" + params.collection + "\");'>" +
			'<font style="font-size: 15px"><i class="fa fa-pencil-square-o padding-right-10"> </i> ' + trad.update + '</font>' +
			'</a>';

			str += `<a href='javascript:;' class='btn btn-info btn-link delete-tpl' data-id='${params.id}' data-collection="${params.collection}">`;
			str += '<font style="font-size: 15px"><i class="fa fa-trash padding-right-10"> </i> ' + trad.delete + '</font>' +
			'</a>';
		}

		if (isSuperAdminCms) {
			if (typeof params.shared == "undefined") {
				str +='<a href="javascript:;" class="btn btn-info btn-link custom-block-cms share-this-template action-hide" data-id="' + params.id + '" data-name="' + params.name + '" data-collection="' + params.collection + '">' +
				'<i class="fa fa-share padding-right-10" style="font-size: 15px"></i><font class="capitalize" style="font-size: 16px">' +
				tradCms.shareforpublic + '</font>' +
				'</a>';
			}else{
				str +='<a href="javascript:;" class="btn btn-info btn-link custom-block-cms share-this-template action-share" data-id="' + params.id + '" data-name="' + params.name + '" data-collection="' + params.collection + '">' +
				'<font class="capitalize" style="font-size: 15px">' +
				'<i class="fa fa-lock padding-right-10"></i> ' + trad.hide + '</font>' +
				'</a>';
			}
		}
		if ( typeof params.screenshot != 'undefined' && params.screenshot != '') {                                	
			$.each( params.screenshot , function(k,val) { 
				if (k != 0) 
					str += '<a href="'+val+'" class="hidden btn btn-info thumb-info" data-lightbox="' +params.name+params.id+'" data-title="Aperçu de ' + params.name + '">' +
				'<font style="font-size: 18px"><i class="fa fa-photo padding-right-10"></i>' + trad.Preview + '</font>' +
				'</a>';
			});
		}
		str += '</div>'
		str += '</div>'+
		'</article>'+
		'</div>';

		return str;
	},
	bindBtnElement: function () {
		mylog.log("directory.bindBtnElement");
		coInterface.bindLBHLinks();
		//Bind button I Co-fund for crowdfunding
		$(".addDonation").off().on("click", function () {
			if (notNull(costum)) {
				costum.unloggedMode = dyFObj.unloggedMode;
				if (typeof costum[costum.slug].donation.setUnloggedMode == "function") {
					costum[costum.slug].donation.setUnloggedMode();
				}
			}

			var goal = $(this).data("goal");
			var amount = $(".tooltip .tooltip-inner").text();
			var pledgeAmount = $(this).data("pledgeamount");
			var donationAmount = $(this).data("donationamount");
			var directDonation = $(this).data("directdonation");
			//alert(directDonation);
			var dataEdit = {};

			// if (typeof contextData =="undefined" || !notNull(contextData)){
			dataEdit.receiver = {};
			var receiverId = $(this).data("id");
			dataEdit.receiver[receiverId] = {};
			dataEdit.receiver[receiverId].type = $(this).data("type");
			dataEdit.receiver[receiverId].name = $(this).data("name");

			mylog.log(amount, typeof amount);

			if (typeof amount != "undefined" && amount > 0) {
				dataEdit.amount = amount;
			}
			//}
			dataEdit.parent = {};
			var idCamp = $(this).data("id-camp");
			dataEdit.parent[idCamp] = {};
			dataEdit.parent[idCamp].type = "crowdfunding";
			dataEdit.parent[idCamp].name = $(this).data("name-camp");
			// dataEdit.iban= $(this).data("iban");
			// dataEdit.donationPlatform= $(this).data("donationPlatform");
			mylog.log("dataEdit", dataEdit);
			if (directDonation == true) {
				urlCtrl.modalExplain({
					icon: "money",
					name: trad.donation + " / " + trad.pledge,
					text: "<ul style='list-style-type:none'><li>" + trad.donation + " : " + trad.supportCrowdfundingCampaign + "</li><br><li>" + trad.pledge + " : " + trad.deferDonationUntilRequirement + "</li></ul>",
					link: {
						pledge: {
							label: trad.pledge,
							icon: "money",
							link: "javascript:;",
							onclick: 'dyFObj.openForm("donation",null,{"type":"pledge"})',
							dataExtend: dataEdit
						},
						donation: {
							label: trad.donation,
							icon: "dollar",
							link: "javascript:;",
							onclick: 'dyFObj.openForm("donation",null,{"type":"donation"})',
							dataExtend: dataEdit
						}
					}
				});
			} else {
				dataEdit.type = "pledge";
				if (goal >= pledgeAmount + donationAmount) {
					dyFObj.openForm("donation", null, dataEdit);
				}
				else {
					var path = parentModuleUrl;
					urlCtrl.modalExplain({
						name: trad.gaugeReached,
						logo: path + "/images/gauge.png",
						text: trad.pledgesReachedGoal + trad.inviteToBoostOtherProjects,
						link: {
							pledge: {
								label: trad.iSupportAnyway,
								icon: "money",
								link: "javascript:;",
								onclick: 'dyFObj.openForm("donation",null,{"type":"pledge"})',
								dataExtend: dataEdit
							}
						}
					});
					//  		$("#ajax-modal .modal-header h3").text("La jauge est atteinte !").css("color","#8ac4c5");
					//  		$("#ajax-modal-modal-body").append(
					// 	'<div class="">'+
					// 		'L\'obectif de la campagne  de financement a atteint par les promesses de dons. Il ne vous est plus possible de faire une promesse'+
					// 	'</div>');
					// $("#ajax-modal").modal();

				}
			}
		});

		// bind animation on progess bar
		$(".campaign-progress .progress").mousemove(function (e) {
			//e.stopPropagation();
			$(this).css("cursor", "pointer");
			var goal = $(this).data("goal");
			var pledge = $(this).data("pledgeamount");
			var donation = $(this).data("donationamount");
			var restAmount = goal - pledge - donation;

			var donationPour = ((100 * donation / goal) < 1) ? 1 : Math.ceil(100 * donation / goal);
			var pledgePour = ((100 * pledge / goal) < 1) ? 1 : Math.ceil(100 * pledge / goal);

			var donationWidth = $(this).find(".donation-bar").width();
			var eltWidth = $(this).width();
			var pledgeWidth = pledgePour * eltWidth / 100;
			mylog.log("pledgeWidth", pledgeWidth, "donationWidth", donationWidth);

			var restWidth = eltWidth - donationWidth - pledgeWidth;
			var unityAmount = restAmount / restWidth;

			var parentOffset = $(this).offset();
			var relX = e.pageX - parentOffset.left;
			var relY = e.pageY - parentOffset.top; relX

			var relAmount = relX - donationWidth - pledgeWidth;

			var prevPledge = relAmount * unityAmount;
			var roundPrev = Math.ceil(prevPledge / 10) * 10;

			var ratePrev = Math.ceil(100 * relAmount / eltWidth) + pledgePour;

			if (ratePrev + donationPour == 101) {
				ratePrev = ratePrev - 1;
			}

			var displayedRate = ratePrev;

			mylog.log("Pledge percentage", displayedRate);

			var tipWidth = $(this).next().width();
			tipWidth = tipWidth / 2;
			if (roundPrev < 0) {
				$(this).siblings(".tooltip").addClass("hidden");
			} else {
				$(this).siblings(".tooltip").removeClass("hidden");
				$(this).find(".pledge-bar").css("width", displayedRate + "%");
			}
			$(this).siblings(".tooltip").css("left", relX - tipWidth + 15);
			$(this).attr("data-original-title", roundPrev);
			$(this).siblings(".tooltip").find(".tooltip-inner").text(roundPrev);
			$(this).attr("data-amount", roundPrev);

			mylog.log("pledge amount", roundPrev);

		});


		$(".campaign-progress .progress").mouseleave(function () {
			mylog.log("out");
			$(this).siblings(".tooltip").removeClass("hidden");
			var goal = $(this).data("goal");
			var pledge = $(this).data("pledgeamount");
			var initialPledge = (100 * pledge / goal < 1) ? 1 : Math.ceil(100 * pledge / goal);
			$(this).find(".pledge-bar").css("width", initialPledge + "%");
		});

		$(".click-element").off().on("click", function (e) {
			if (e.target.tagName === 'DIV') {
				var href = $(this).data("href");
				var divclass = $(this).data("class");
				var typeElt = $(this).data("type-elt");
				if(typeElt != "poi")
					$(this).css("transform", "scale(1.2, 1.2");
				else {
					$(this).addClass("active");
					setTimeout(() => {
						$(this).removeClass("active");
					}, 1000)
				}
				if(divclass == " lbh"){
					urlCtrl.loadByHash($(this).data("href"));
					$(this).removeClass("active")
				}else if(divclass == " lbh-preview-element"){
					onchangeClick=false;
					link = href;
					previewHash=link.split(".");
					if($.inArray(previewHash[2], ["proposals", "proposal", "actions", "resolutions"]) >= 0){
							ddaT=(previewHash[2]=="proposals") ? "proposal" : previewHash[2];
							uiCoop.getCoopDataPreview(ddaT,previewHash[4]);
					}else{
						hashT=location.hash.split("?");
						getStatus=urlCtrl.getUrlSearchParams(); 
						hashNav=(hashT[0].indexOf("#") < 0) ? "#"+hashT[0] : hashT[0]; 	
						if (previewHash.length>1){
							urlHistoric=hashNav+"?preview="+previewHash[2]+"."+previewHash[4];
							if($("#entity"+previewHash[4]).length > 0) setTimeout(function(){$("#entity"+previewHash[4]).addClass("active");},200);
						}
						else{
							mylog.log("hashNav",hashNav);
							pageName=(previewHash[0].indexOf("#")==0) ? previewHash[0].substr(1) : previewHash[0];
							urlHistoric=hashNav+"?preview=page.costum.view."+pageName ;
							if (typeof costum.app[link] !="undefined" && typeof(costum.app[link].hash!="undefined") && costum.app[link].hash.indexOf("view")>-1 && typeof(costum.app[link].urlExtra!="undefined")){
								link="view"+costum.app[link].urlExtra;
							}
							
						} 
						if(getStatus != "") urlHistoric+="&"+getStatus; 
						history.replaceState({}, null, urlHistoric);
						urlCtrl.openPreview(link);
						if(!costum || !costum.editMode)
							history.replaceState({}, null, urlCtrl.getUrlWithHashInSearchParams().href)
					}
					//$(this).removeClass("active")
				}
            }
			
		})
		$(".campaign-goal").off().on("click", function () {
			var goal = $(this).data("goal");
			var descr = (notNull($(this).data("description-camp"))) ? $(this).data("description-camp") : trad.nodescription;

			urlCtrl.modalExplain({
				icon: "bullseye",
				name: trad.goal + " " + goal + " €",
				text: dataHelper.convertMardownToHtml(descr)
			});
		});

		//End crowdfunding button-------
		$(".btn-update-contact").click(function () {
			updateContact($(this).data("contact-key"), $(this).data("contact-name"), $(this).data("contact-email"),
				$(this).data("contact-role"), $(this).data("contact-telephone"));
		});
		$(".btn-edit-preview, .btn-edit-element").off().on("click", function () {
			$("#modal-preview-coop").hide(300);
			$("#modal-preview-coop").html("");
			dyFObj.editElement($(this).data("type"), $(this).data("id"), $(this).data("subtype"));
		});
		$(".deleteThisBtn").off().on("click", function () {
			mylog.log("deleteThisBtn click");
			$(this).empty().html('<i class="fa fa-spinner fa-spin"></i>');
			directory.deleteElement($(this).data("type"), $(this).data("id"), $(this));
		});
		$('.tooltips').tooltip();
		initUiCoopDirectory();
		directory.bindMediaSharingElt();
		directory.bindToAddSpamElt();
		directory.bindNoSpamElt();
		directory.bindDeleteSpam();
		directory.bindShareElt();
		directory.bindEventAdmin();
		//Specific case of link in directory
		$(".followBtn").off().on("click", function () {
			mylog.log(".followBtn");
			if (!notEmpty(userId)) {
				toastr.warning(trad.LoginFirst);
				$('#modalLogin').modal("show");
				return true;
			}
			var parentId = $(this).attr("data-id");
			var parentType = $(this).attr("data-type");
			var childId = userId;
			var childType = "citoyens";
			var name = $(this).attr("data-name");
			var id = $(this).attr("data-id");
			var isFromActivityPub = $(this).attr("data-isfromactivitypub");
			var action = $(this).attr("data-action");
			var payload = $(this).attr("data-payload");
			var connectType = (parentType == "events") ? "connect" : "follow";
			var actionData = {
				"childId": childId,
				"childType": childType,
				"parentType": parentType,
				"parentId": parentId,
				"connectType": connectType,
				 // element info
		        "id": id,
		        "name": name,
			};
			//traduction du type pour le floopDrawer
			var thiselement = $(this);
			$(this).html("<i class='fa fa-spin fa-circle-o-notch text-azure'></i>");
			var connectType = (type == "events") ? "connect" : "follow";
			if ($(this).attr("data-ownerlink") == "follow") {
				callback = function () {
					//toastr.success(data.msg); 
					labelLink = (parentType == "events") ? trad.alreadyAttendee : trad.alreadyFollow;
					if (thiselement.hasClass("btn-add-to-directory"))
						labelLink = "";
					thiselement.html("<small><i class='fa fa-unlink'></i> " + labelLink + "</small>");
					thiselement.addClass("active");
					thiselement.attr("data-ownerlink", "unfollow");
					thiselement.attr("data-original-title", labelLink);
					if (isFromActivityPub == "true") {
		              if(parentType == "events"){
		                thiselement.attr("data-action", "leaveevent");
		              }else if(parentType == "projects"){
		                thiselement.attr("data-action", "unfollowProject");
		              }
		            }
				};
				if (parentType == "events") {
					if (isFromActivityPub == "true") {

						links.handleActivityPubFollow(action, payload, actionData, callback);
					} else {
						links.connectAjax(parentType, parentId, childId, childType, connectType, null, callback);
					}

				} else if (parentType == "citoyens") {
		            var activitypubEnabled = $(this).data("activitypub-enabled");
		            if (activitypubEnabled) {

		              links.followActivityPubActor(
		                parentType,
		                parentId,
		                childId,
		                childType,
		                callback
		              );
		            }
		          }
				else {
					if (isFromActivityPub == "true") {
		              if (parentType == "projects") {
		              	actionData.type = "projects";
		                links.handleActivityPubFollow(action, payload, actionData, callback);
		              }
		            } else {
		              links.follow(parentType, parentId, childId, childType, callback);
		            }
				}
			}
			else if ($(this).attr("data-ownerlink") == "unfollow") {
				connectType = (parentType == "events") ? "attendees" : "followers";
				callback = function () {
					labelLink = (parentType == "events") ? trad.participate : trad.Follow;
					if (thiselement.hasClass("btn-add-to-directory"))
						labelLink = "";
					$(thiselement).html("<small><i class='fa fa-chain'></i> " + labelLink + "</small>");
					$(thiselement).attr("data-ownerlink", "follow");
					$(thiselement).attr("data-original-title", labelLink);
					$(thiselement).removeClass("text-white");
					if (isFromActivityPub == "true") {
		              if(parentType == "events"){
		                thiselement.attr("data-action", "joinevent");
		              }else if(parentType == "projects"){
		                thiselement.attr("data-action", "followProject");
		              }
		             
		            }
				};
				if (isFromActivityPub == "true") {
					links.handleActivityPubFollow(action, payload, actionData, callback);
				} else {
					if (
		              $(thiselement).data("activitypub-enabled") != undefined &&
		              $(thiselement).data("activitypub-enabled") != null &&
		              $(thiselement).data("activitypub-enabled") == true
		            ) {
		              links.unFollowActivityPubActor(
		                parentType,
		                parentId,
		                childId,
		                childType,
		                connectType,
		                null,
		                callback
		              );
		            }else{
		            	links.disconnectAjax(parentType, parentId, childId, childType, connectType, null, callback);
		            }
					
				}
			}
		});
		$(".btn-tag-panel").off().on('click', function (e) {
			var dataTag = $(this).data("tag");
			if (location.hash.indexOf("tags") == -1 && location.hash.indexOf("?") != -1) {
				location.hash = location.hash + "&tags=" + $(this).data("tag");
			}
			else if (location.hash.indexOf("tags") == -1 && location.hash.indexOf("?") == -1) {
				location.hash = location.hash + "?tags=" + $(this).data("tag");
			}
			else if (location.hash.indexOf("tags") != -1) {

				mylog.log("location.hash", location.hash.split("&"));
				location.hash = location.hash.split("&").map(function (item, index) {
					// mylog.log(item.split("="));
					if (item.indexOf("tags") != -1) {
						var item = item.split("=");
						var tagsvalue = item[1].split(",");
						tagsvalue.push(dataTag);
						var tagsStr = tagsvalue.join();
						item = item[0] + "=" + tagsStr;
					}
					return item;
				}).join("&");

			}
			urlCtrl.loadByHash(location.hash);
		});
		$(".btn-rotatingCard, .btn-rotate-card").on("click",function(e){
			e.stopImmediatePropagation();
			$(this).closest('.rotatingCard .card-container').toggleClass('hover');
			$(this).closest('.rotating-card .rotating-card-container').toggleClass('hover');
		});
		$('.material-card > .mc-btn-action').click(function () {
			var card = $(this).parent('.material-card');
			var icon = $(this).children('i');
			icon.addClass('fa-spin-fast');

			if (icon.hasClass("fa-arrow-left")) {
				card.removeClass('mc-active');

				window.setTimeout(function() {
					icon
						.removeClass('fa-arrow-left ripple')
						.removeClass('fa-spin-fast')
						.addClass('fa-bars');

				}, 800);
			} else {
				card.addClass('mc-active');

				window.setTimeout(function() {
					icon
						.removeClass('fa-bars')
						.removeClass('fa-spin-fast')
						.addClass('fa-arrow-left ripple');

				}, 800);
			}
		});
	},
	addToAlert: function () {
		var message = "<span class='text-dark'>" + tradDynForm.saveresearchandalert + "</span><br>";
		var searchName = (searchObject.text != "") ? searchObject.text : "";
		var boxBookmark = bootbox.dialog({
			title: message,
			message: '<div class="row">  ' +
				'<div class="col-md-12"> ' +
				'<form class="form-horizontal"> ' +
				'<label class="col-md-12 no-padding" for="awesomeness">' + tradDynForm.searchtosave + ' : </label><br> ' +
				'<span>' + location.hash + '</span><br><br>' +
				'<label class="col-md-12 no-padding" for="awesomeness">' + tradDynForm.nameofsearch + ' : </label><br> ' +
				'<input type="text" id="nameFavorite" class="nameSearch wysiwygInput form-control" value="' + searchName + '" style="width: 100%" placeholder="' + tradDynForm.addname + '..."></input>' +
				'<br>' +

				'<label class="col-md-6 col-sm-6 no-padding" for="awesomeness">' + tradDynForm.bealertbymailofnewclassified + ' ?</label> ' +
				'<div class="col-md-4 col-sm-4"> <div class="radio no-padding"> <label for="awesomeness-0"> ' +
				'<input type="radio" name="awesomeness" id="awesomeness-0" value="true"  checked="checked"> ' +
				tradDynForm.yes + ' </label> ' +
				'</div><div class="radio no-padding"> <label for="awesomeness-1"> ' +
				'<input type="radio" name="awesomeness" id="awesomeness-1" value="false"> ' + tradDynForm.no + ' </label> ' +
				'</div> ' +
				'</div> </div>' +
				'</form></div></div>',
			buttons: {
				success: {
					label: trad.save,
					className: "btn-primary",
					callback: function () {
						var formData = {
							"parentId": userId,
							"parentType": "citoyens",
							"url": location.hash,
							"searchUrl": searchInterface.getParamsUrlForAlert(),
							"alert": $("input[name='awesomeness']:checked").val(),
							"name": $("#nameFavorite").val(),
							"type": "research"
						}
						if (typeof costum != "undefined" && costum != null) {
							formData.mailParams = {
								logo: costum.logo,
								title: costum.title,
								url: costum.url
							};
						}
						mylog.log(formData);
						ajaxPost(
							null,
							baseUrl + "/" + moduleId + "/bookmark/save",
							formData,
							function (data) {
								if (data.result) {
									toastr.success(data.msg);
								}
								else {
									if (typeof (data.type) != "undefined" && data.type == "info")
										toastr.info(data.msg);
									else
										toastr.error(data.msg);
								}
							}
						);
					}
				},
				cancel: {
					label: trad.cancel,
					className: "btn-secondary",
					callback: function () {
					}
				}
			}

		});
	},
	bindMediaSharingElt: function () {
		$('.btn-share-panel').off().click(function () {
			directory.showShareModal($(this).attr('data-type'), $(this).attr('data-id'));
		});
	},
	bindToAddSpamElt: function () {
		$('.btn-addToSpam-element').off().click(function () {
			directory.showAddToSpamModal($(this).attr('data-type'), $(this).attr('data-id'));
		});
	},

	bindNoSpamElt: function () {
		$('.btn-no-Spam').off().click(function () {
			directory.showNoSpamModal($(this).attr('data-type'), $(this).attr('data-id'));
		});
	},
	 
	bindDeleteSpam: function () {
		$('.btn-delete-spam').off().click(function () {
			directory.showDeleteSpamModal($(this).attr('data-type'), $(this).attr('data-id'));
		});
	},
	 
	showAddToSpamModal: function (addToSpamElementType, addToSpamElementId) {
		var str = '<div class="panel panel-default" >'+
			'<div class="panel-heading">'+
				'<h4 class="text-purple" style="text-align:center;">'+
					'<span style="text-align:center;">'+trad.aboutElement+'</span>'+
				'</h4>'+
			'</div>'+
			'<div class="panel-body no-padding">'+
				'<div class="list-group no-padding">'+
					'<div id="floopType-projects" class="floop-type-list">'+
						'<div class=" floopEntries col-xs-12 no-padding">'+
							'<div class="btn-chk-contact btn-scroll-type info-of-element-to-add-spam row" style="margin-left:20px;margin-top:20px;">'+
								
							'</div>'+
						'</div>'+
					'</div>'+
				'</div>'+
			'</div>'+

		'</div>';

		str += '<div class="panel panel-default" >'+
			'<div class="panel-heading">'+
				'<h4 class="text-purple" style="text-align:center;">'+
					'<span style="text-align:center;">'+trad.linkedToSpam+'</span>'+
				'</h4>'+
			'</div>'+
			'<div class="panel-body no-padding">'+
				'<div class="list-group no-padding">'+
					'<div id="floopType-projects" class="floop-type-list">'+
						'<div class=" floopEntries col-xs-12 no-padding">'+
							'<div class="btn-chk-contact btn-scroll-type element-lie-direct-of-element-to-add-spam row" style="margin-left:20px;margin-top:20px;">'+
								
							'</div>'+
						'</div>'+
					'</div>'+
				'</div>'+
			'</div>'+

		'</div>';

		str += '<div id="element-lie-in-creator-add-to-spam"></div>';


		str += '<div class="panel panel-default" >'+
			'<div class="panel-heading">'+
				'<h4 class="text-purple" style="text-align:center;">'+
					'<span style="text-align:center;">'+trad.logsLinkedElement+'</span>'+
				'</h4>'+
			'</div>'+
			'<div class="panel-body no-padding">'+
				'<div class="list-group no-padding">'+
					'<div id="floopType-projects" class="floop-type-list">'+
						'<div class=" floopEntries col-xs-12 no-padding">'+
							'<div class="btn-chk-contact btn-scroll-type logs-of-element-to-add-spam row" style="margin-left:20px;margin-top:20px;">'+
								
							'</div>'+
						'</div>'+
					'</div>'+
				'</div>'+
			'</div>'+

		'</div>';


		str += '<button class="share-co-btn addToSpam-btn-co " style="background-color:red!important;color:#ffffff!important;" data-id="'+addToSpamElementId+'" data-type="'+addToSpamElementType+'">' +
				'<i class="fa fa-exclamation-triangle"></i> ' + trad.addToSpam +
			'</button>' +
			'<button class="btn btn-default share-co-btn" data-dismiss="modal" style="background-color:#cacaca!important;">'+
                '<i class="fa fa-times"></i> ' +trad.cancel+             
			'</button>';
		$('#modal-share').modal('show');
		$('#modal-share #btn-share-it, #modal-share #msg-share, #modal-share button[data-dismiss=modal]').hide();
		$('#modal-share #htmlElementToShare').html(str);
		$('#modal-share .title-share-modal').html("");
		
		directory.getInfoBeforeAddToSpamElement(addToSpamElementType, addToSpamElementId);
	},

	showNoSpamModal: function (addToSpamElementType, addToSpamElementId) {
		var str = "";

		str += '<div id="container-info-before-no-spam"></div>';
		str += '<button class="share-co-btn noSpam-btn-co " style="background-color:green!important;color:#ffffff!important;" data-id="'+addToSpamElementId+'" data-type="'+addToSpamElementType+'">' +
				'<i class="fa fa-exclamation-circle"></i> ' +trad.noSpam+
			'</button>' +
			'<button class="btn btn-default share-co-btn" data-dismiss="modal" style="background-color:#cacaca!important;">'+
                '<i class="fa fa-times"></i> ' +trad.cancel+             
			'</button>';
		$('#modal-share').modal('show');
		$('#modal-share #btn-share-it, #modal-share #msg-share, #modal-share button[data-dismiss=modal]').hide();
		$('#modal-share #htmlElementToShare').html(str);
		$('#modal-share .title-share-modal').html("");

		directory.getInfoBeforeDeleteSpam(addToSpamElementType, addToSpamElementId, "noSpam");
		directory.bindNoSpamAction();
	},

	showDeleteSpamModal: function (addToSpamElementType, addToSpamElementId) {
		var str = "";

		str += '<div id="container-info-before-delete-spam"></div>';
		str += '<button class="share-co-btn deleteSpam-btn-co " style="background-color:red!important;color:#ffffff!important;" data-id="'+addToSpamElementId+'" data-type="'+addToSpamElementType+'">' +
				'<i class="fa fa-trash"></i> ' +trad.delete+
			'</button>' +
			'<button class="btn btn-default share-co-btn" data-dismiss="modal" style="background-color:#cacaca!important;">'+
                '<i class="fa fa-times"></i> ' +trad.cancel+             
			'</button>';
		$('#modal-share').modal('show');
		$('#modal-share #btn-share-it, #modal-share #msg-share, #modal-share button[data-dismiss=modal]').hide();
		$('#modal-share #htmlElementToShare').html(str);
		$('#modal-share .title-share-modal').html(trad.warning);

		directory.getInfoBeforeDeleteSpam(addToSpamElementType, addToSpamElementId, "delete");
		directory.bindDeleteSpamAction();
	},

	bindNoSpamAction : function(){
		$('.noSpam-btn-co').off().click(function () {
			if( ! $(".noSpam-btn-co").hasClass("disabled")){
				const type = $(this).attr('data-type');
				const id = $(this).attr('data-id');
				var params = {
					"id" : id,
					"type" : type
				};

				$(".noSpam-btn-co").addClass("disabled");
				$(".noSpam-btn-co").find(".fa").removeClass("fa-exclamation-circle").addClass("fa-spinner fa-spin");

				ajaxPost(
					null,
					baseUrl + "/news/co/nospam",
					params,
					function (data) {
						$(".noSpam-btn-co").removeClass("disabled");
						$(".noSpam-btn-co").find(".fa").addClass("fa-exclamation-circle").removeClass("fa-spinner fa-spin");

						if(typeof data.msg != "undefined" && typeof data.result != "undefined" && data.result)
							toastr.success(data.msg);
						else if(typeof data.msg != "undefined" && ( (typeof data.result != "undefined" && !data.result) || (typeof data.result == "undefined") ) ) 
							toastr.error(data.msg);
						$("#modal-share").modal("toggle");
						$(".main-search-bar-addon.central-search-bar").trigger("click");
					}
				);
			}
		});
	},

	bindDeleteSpamAction : function(){
		$('.deleteSpam-btn-co').off().click(function () {
			if( ! $(".deleteSpam-btn-co").hasClass("disabled")){
				const type = $(this).attr('data-type');
				const id = $(this).attr('data-id');

				$(".deleteSpam-btn-co").addClass("disabled");
				$(".deleteSpam-btn-co").find(".fa").removeClass("fa-trash").addClass("fa-spinner fa-spin");
				var params = {
					"id" : id,
					"type" : type
				};
	
				ajaxPost(
					null,
					baseUrl + "/news/co/deletespam",
					params,
					function (data) {
						if(typeof data.msg != "undefined" && typeof data.result != "undefined" && data.result)
							toastr.success(data.msg);
						else if(typeof data.msg != "undefined" && ( (typeof data.result != "undefined" && !data.result) || (typeof data.result == "undefined") ) ) 
							toastr.error(data.msg);
						$("#modal-share").modal("toggle");
						$(".main-search-bar-addon.central-search-bar").trigger("click");
						$(".deleteSpam-btn-co").removeClass("disabled");
						$(".deleteSpam-btn-co").find(".fa").addClass("fa-trash").removeClass("fa-spinner fa-spin");
					}
				);
			}
		});
	},

	bindAddToSpamElt: function (dataToSpam = {}) {
		$('.addToSpam-btn-co').off().click(function () {
			if(typeof dataToSpam.ipspam != "undefined" && !$('.addToSpam-btn-co').hasClass("disabled")){
				var params = {
					"ipspam" :dataToSpam.ipspam,
					"id" :dataToSpam.id_element,
					"type" :dataToSpam.type
				};

				$(".addToSpam-btn-co").addClass("disabled");
				$(".addToSpam-btn-co").find(".fa").removeClass("fa-exclamation-triangle").addClass("fa-spinner fa-spin");

				ajaxPost(
					null,
					baseUrl + "/news/co/addipspam",
					params,
					function (data) {
						$(".addToSpam-btn-co").removeClass("disabled");
						$(".addToSpam-btn-co").find(".fa").addClass("fa-exclamation-triangle").removeClass("fa-spinner fa-spin");

						if(typeof data.msg != "undefined")
							toastr.success(data.msg);
						$("#modal-share").modal("toggle");
						/* $("#entity_"+dataToSpam.type+"_"+dataToSpam.id_element + " .container-img-card img").attr("src", modules.co2.url+"/images/spam.jpg");
						$("#entity_"+dataToSpam.type+"_"+dataToSpam.id_element + " .container-img-card img").data("src", modules.co2.url+"/images/spam.jpg");
						$("#entity_"+dataToSpam.type+"_"+dataToSpam.id_element + " .item-slide .slide-hover").remove(); */
												$("#entity_"+dataToSpam.type+"_"+dataToSpam.id_element).css("display", "none");
					}
				);

			}
		});
	},
	spamElement : function(dataToSpam = {}){
		if(typeof dataToSpam.ipspam != "undefined"){
			var params = {
				"ipspam" :dataToSpam.ipspam,
				"id" :dataToSpam.id_element,
				"type" :dataToSpam.type
			};
			ajaxPost(
				null,
				baseUrl + "/news/co/addipspam",
				params,
				function (data) {
					$("#entity_"+dataToSpam.type+"_"+dataToSpam.id_element).css("display", "none");
				}
			);

		}
	},

	addColorForNote : function(note) {
		if(note > 5)
			note = 5;
		note = Math.min(Math.max(note, 0), 5);
		
		let red = 255;
		let green = Math.floor(200 * (1 - note / 5));
		let blue = Math.floor(200 * (1 - note / 5));

		let color = `rgb(${red}, ${green}, ${blue})`;
		
		return color;
	},

	setInfoElementNoSpamInModalNoSpam : function(data){
		var listElementToDelete = "";
		var str = "";

		// info sur le spam
		if(typeof data.element != "undefined" && data.element != null){
			str += '<div class="panel panel-default" >'+
			'<div class="panel-heading">'+
				'<h4 class="text-purple" style="text-align:center;">'+
					'<span style="text-align:center;">'+trad.infoSpam+'</span>'+
				'</h4>'+
			'</div>'+
			'<div class="panel-body no-padding">'+
				'<div class="list-group no-padding">'+
					'<div id="floopType-projects" class="floop-type-list">'+
						'<div class=" floopEntries col-xs-12 no-padding">'+
							'<div class="btn-chk-contact btn-scroll-type row" style="margin-left:20px;margin-top:20px;">';
							if(typeof data.element.name != "undefined"){
								str += '<div class="col-lg-5 col-md-5 col-sm-5 col-xs-5" style="margin-bottom:10px;">'+
											'<a class="lbh-preview-element">'+
												'<span class="name-contact text-dark text-bold">'+trad.spamName+'</span>'+
											'</a>'+
											'<span class="floop-statu col-xs-12 no-padding">- '+ data.element.name +'</span>'+
										'</div>'
							}
							if(typeof data.element.type != "undefined"){
								str += '<div class="col-lg-5 col-md-5 col-sm-5 col-xs-5" style="margin-bottom:10px;">'+
											'<a class="lbh-preview-element">'+
												'<span class="name-contact text-dark text-bold">'+trad.spamType+'</span>'+
											'</a>'+
											'<span class="floop-statu col-xs-12 no-padding">- '+ data.element.type +'</span>'+
										'</div>'
							}
							if(typeof data.element.userSpamDetector != "undefined" && data.element.userSpamDetector != null && typeof data.element.userSpamDetector.name != "undefined"){
								str += '<div class="col-lg-5 col-md-5 col-sm-5 col-xs-5" style="margin-bottom:10px;">'+
											'<a class="lbh-preview-element">'+
												'<span class="name-contact text-dark text-bold">'+trad.detectBy+'</span>'+
											'</a>'+
											'<span class="floop-statu col-xs-12 no-padding">- '+ data.element.userSpamDetector.name +'</span>'+
										'</div>'
							}

							if(typeof data.element.dateSpamDetector != "undefined" && data.element.dateSpamDetector != null && typeof data.element.dateSpamDetector.sec != "undefined"){
								str += '<div class="col-lg-5 col-md-5 col-sm-5 col-xs-5" style="margin-bottom:10px;">'+
											'<a class="lbh-preview-element">'+
												'<span class="name-contact text-dark text-bold">'+trad.detectThe+'</span>'+
											'</a>'+
											'<span class="floop-statu col-xs-12 no-padding">- '+ data.element.dateSpamDetector.sec  +'</span>'+
										'</div>'
							}


			str += '</div>'+'</div>'+'</div>'+'</div>'+'</div>'+'</div>';
		}

		$("#container-info-before-no-spam").html(str);
	},

	setInfoElementDeleteInModalDeleteSpam : function(data){
		var listElementToDelete = "";
		var str = "";

		// info sur le spam
		if(typeof data.element != "undefined" && data.element != null){
			str += '<div class="panel panel-default" >'+
			'<div class="panel-heading">'+
				'<h4 class="text-purple" style="text-align:center;">'+
					'<span style="text-align:center;">'+trad.infoSpam+'</span>'+
				'</h4>'+
			'</div>'+
			'<div class="panel-body no-padding">'+
				'<div class="list-group no-padding">'+
					'<div id="floopType-projects" class="floop-type-list">'+
						'<div class=" floopEntries col-xs-12 no-padding">'+
							'<div class="btn-chk-contact btn-scroll-type row" style="margin-left:20px;margin-top:20px;">';
							if(typeof data.element.name != "undefined"){
								str += '<div class="col-lg-5 col-md-5 col-sm-5 col-xs-5" style="margin-bottom:10px;">'+
											'<a class="lbh-preview-element">'+
												'<span class="name-contact text-dark text-bold">'+trad.spamName+'</span>'+
											'</a>'+
											'<span class="floop-statu col-xs-12 no-padding">- '+ data.element.name +'</span>'+
										'</div>'
							}
							if(typeof data.element.type != "undefined"){
								str += '<div class="col-lg-5 col-md-5 col-sm-5 col-xs-5" style="margin-bottom:10px;">'+
											'<a class="lbh-preview-element">'+
												'<span class="name-contact text-dark text-bold">'+trad.spamType+'</span>'+
											'</a>'+
											'<span class="floop-statu col-xs-12 no-padding">- '+ data.element.type +'</span>'+
										'</div>'
							}
							if(typeof data.element.userSpamDetector != "undefined" && data.element.userSpamDetector != null && typeof data.element.userSpamDetector.name != "undefined"){
								str += '<div class="col-lg-5 col-md-5 col-sm-5 col-xs-5" style="margin-bottom:10px;">'+
											'<a class="lbh-preview-element">'+
												'<span class="name-contact text-dark text-bold">'+trad.detectBy+'</span>'+
											'</a>'+
											'<span class="floop-statu col-xs-12 no-padding">- '+ data.element.userSpamDetector.name +'</span>'+
										'</div>'
							}

							if(typeof data.element.dateSpamDetector != "undefined" && data.element.dateSpamDetector != null && typeof data.element.dateSpamDetector.sec != "undefined"){
								str += '<div class="col-lg-5 col-md-5 col-sm-5 col-xs-5" style="margin-bottom:10px;">'+
											'<a class="lbh-preview-element">'+
												'<span class="name-contact text-dark text-bold">'+trad.detectThe+'</span>'+
											'</a>'+
											'<span class="floop-statu col-xs-12 no-padding">- '+ data.element.dateSpamDetector.sec  +'</span>'+
										'</div>'
							}


			str += '</div>'+'</div>'+'</div>'+'</div>'+'</div>'+'</div>';
		}

		// element supprime
		str += '<div class="panel panel-default" >'+
			'<div class="panel-heading">'+
				'<h4 class="text-purple" style="text-align:center;">'+
					'<span style="text-align:center;">'+trad.elementDeletedSpam+'</span>'+
				'</h4>'+
			'</div>'+
			'<div class="panel-body no-padding">'+
				'<div class="list-group no-padding">'+
					'<div id="floopType-projects" class="floop-type-list">'+
						'<div class=" floopEntries col-xs-12 no-padding">'+
							'<div class="btn-chk-contact btn-scroll-type row" style="margin-left:20px;margin-top:20px;">';

							if(data != null && typeof data.toDelete && data.toDelete != null){
								$.each(data.toDelete, function(typeElement, elementsDelete){
									if(elementsDelete != null && typeof elementsDelete.count != "undefined"){
										listElementToDelete += '<div class="col-lg-5 col-md-5 col-sm-5 col-xs-5" style="margin-bottom:10px;">'+
											'<a class="lbh-preview-element">'+
												'<span class="name-contact text-dark text-bold">'+ typeElement + ' <span class="badge text-white bg-turq"> '+ elementsDelete.count +' </span>' +'</span>'+
											'</a>';
										$.each(elementsDelete, function(idElementDelete, elementDelete){
											if(idElementDelete != "count"){
												var nameElement = (elementDelete != null && typeof elementDelete.name != "undefined" ) ? elementDelete.name : idElementDelete;
												listElementToDelete += '<span class="floop-statu col-xs-12 no-padding">- '+ nameElement +'</span>';
											}
										});
										listElementToDelete += '</div>';
									}

								});
							}
							

 			
								
		str += listElementToDelete + '</div>'+'</div>'+'</div>'+'</div>'+'</div>'+'</div>';

		$("#container-info-before-delete-spam").html(str);
	},

	setInfoElementInModalAddToSpam : function(data){
		var str = '<div class="col-lg-5 col-md-5 col-sm-5 col-xs-5" style="margin-bottom:10px;">'+
		'<a class="lbh-preview-element">'+
			'<span class="name-contact text-dark text-bold">'+trad.elementType+'</span>'+
		'</a>'+
		'<span class="floop-statu col-xs-12 no-padding">'+data.type+'</span>'+
		'</div>';

		if(typeof data.nameElement != "undefined"){
			str += '<div class="col-lg-5 col-md-5 col-sm-5 col-xs-5" style="margin-bottom:10px;">'+
			'<a class="lbh-preview-element">'+
				'<span class="name-contact text-dark text-bold">'+trad.nameElement+'</span>'+
			'</a>'+
			'<span class="floop-statu col-xs-12 no-padding">'+data.nameElement+'</span>'+
			'</div>';
		}


		if(typeof data.creator != "undefined"){
			str += '<div class="col-lg-5 col-md-5 col-sm-5 col-xs-5" style="margin-bottom:10px;">'+
			'<a class="lbh-preview-element">'+
				'<span class="name-contact text-dark text-bold">'+trad.addBy+'</span>'+
			'</a>';

			if(typeof data.creator.name != "undefined")
				str += '<span class="floop-statu col-xs-12 no-padding"> - '+data.creator.name+'</span>';
			else if(typeof data.creator._id != "undefined" && typeof data.creator._id.id != "undefined")
				str += '<span class="floop-statu col-xs-12 no-padding"> - '+data.creator._id.id+'</span>';
			
			str += '</div>';
		}

		if(typeof data.impacte != "undefined" && data.impacte > 0){
			str += '<div class="col-lg-5 col-md-5 col-sm-5 col-xs-5" style="margin-bottom:10px;">'+
			'<a class="lbh-preview-element">'+
				'<span class="name-contact text-dark text-bold">'+trad.impact+'</span>'+
			'</a>';

			str += '<span class="floop-statu col-xs-12 no-padding" style="background-color:'+directory.addColorForNote(data.impacte)+' ;height:15px;"> </span>';
			str += '</div>';
		}
		

		
		

		if(str == "")
			str = trad.noElement+"</br>";

		$(".info-of-element-to-add-spam").html(str);
	},

	setElementLieDirectementInModalAddToSpam : function(data){
		str = "";
		if(typeof data.allLinks != "undefined"){
			$.each(data.allLinks, function(collection, elements){
				str += '<div class="col-lg-5 col-md-5 col-sm-5 col-xs-5" style="margin-bottom:10px;">'+
				'<a class="lbh-preview-element">'+
					'<span class="name-contact text-dark text-bold">'+collection+'</span>'+
				'</a>';

				$.each(elements, function(idElement, element){
					if(typeof element.name != "undefined")
						str += '<span class="floop-statu col-xs-12 no-padding"> - '+element.name +'</span>';
					else
						str += '<span class="floop-statu col-xs-12 no-padding"> - '+idElement +'</span>';
				});
				str += '</div>';
			});
		}
		

		if(str == "")
			str = trad.noElement+"</br>";

		$(".element-lie-direct-of-element-to-add-spam").html(str);
	},

	setListElementLieAtCreatorInModalAddToSpam : function(data){
		var str = '<div class="panel panel-default" >'+
			'<div class="panel-heading">'+
				'<h4 class="text-purple" style="text-align:center;">'+
					'<span style="text-align:center;">'+"élément lié Au Créateur du spam"+'</span>'+
				'</h4>'+
			'</div>'+
			'<div class="panel-body no-padding">'+
				'<div class="list-group no-padding">'+
					'<div id="floopType-projects" class="floop-type-list">'+
						'<div class=" floopEntries col-xs-12 no-padding">'+
							'<div class="btn-chk-contact btn-scroll-type row" style="margin-left:20px;margin-top:20px;">';

							$.each(data.linksOfCreator, function(collection, elements){
								str += '<div class="col-lg-5 col-md-5 col-sm-5 col-xs-5" style="margin-bottom:10px;">'+
								'<a class="lbh-preview-element">'+
									'<span class="name-contact text-dark text-bold">'+collection+'</span>'+
								'</a>';
				
								$.each(elements, function(idElement, element){
									if(typeof element.name != "undefined")
										str += '<span class="floop-statu col-xs-12 no-padding"> - '+element.name +'</span>';
									else
										str += '<span class="floop-statu col-xs-12 no-padding"> - '+idElement +'</span>';
								});
								str += '</div>';
							});
								
		str += '</div>'+'</div>'+'</div>'+'</div>'+'</div>'+'</div>';

		$("#element-lie-in-creator-add-to-spam").html(str);
	},

	setListLogsInModalAddToSpam : function(data){
		var str = ""

		if(typeof data.logs != "undefined" && data.logs != null){
			
			str += '<table class="tabling-pages table  table-hover col-xs-12">'+
			'<thead>'+
				'<tr>'+
					'<th class="col title">'+trad.ipAddress+'</th>'+
					'<th class="col key-app">'+trad.action+'</th>'+
					/* '<th class="col key-app">Params</th>'+ */
				'</tr>'+
			'</thead>'+
			'<tbody>';

			$.each(data.logs, function(id, logs){
				var ipAddress = "";
				var action = "";
				var params = "";
				var count = "";

				if(typeof logs.ipAddress != "undefined")
					ipAddress = logs.ipAddress;

				if(typeof logs.action != "undefined")
					action = logs.action;

				if(typeof logs.count != "undefined")
					count = logs.count;

				str += '<tr>'+
					'<td>'+ ipAddress +'</td>'+
					'<td>'+ action + " " + '<span class="badge text-white bg-turq">'+ count +'</span>' + '</td>'+
					/* '<td>'+ params +'</td>'+ */
				'</tr>';
			});

			str += '</tbody>'+
			'</table>';
			
		}

		$(".logs-of-element-to-add-spam").html(str);
	},

	getInfoBeforeAddToSpamElement : function(type, id){
		params = {
			"id" : id,
			"type" : type
		};

		ajaxPost(
			null,
			baseUrl + "/news/co/getinfobeforeaddtospamelement",
			params,
			function (data) {
				if(typeof data.notFound == "undefined"){
					directory.setInfoElementInModalAddToSpam(data);
					directory.setElementLieDirectementInModalAddToSpam(data);
					directory.setListLogsInModalAddToSpam(data);

					if(typeof data.linksOfCreator != "undefined"){
						directory.setListElementLieAtCreatorInModalAddToSpam(data);
					}
					directory.bindAddToSpamElt(data);
				}
			}
		);
	},

	getInfoBeforeDeleteSpam : function(typeToDelete, idToDelete, action=""){
		params = {
			"id" : idToDelete,
			"type" : typeToDelete
		};

		ajaxPost(
			null,
			baseUrl + "/news/co/getinfobeforedeletespamelement",
			params,
			function (data) {
				if(typeof data.notFound == "undefined"){
					if(action == "delete")
						directory.setInfoElementDeleteInModalDeleteSpam(data);
					else if (action == "noSpam")
						directory.setInfoElementNoSpamInModalNoSpam(data);
				}
			}
		);
	},

	showShareModal: function (shareEltType, shareEltId) {
		mylog.log('init btn-share ');

		var url = baseUrl + '/co2/app/page/type/' + shareEltType + '/id/' + shareEltId;
		var str = '<div class="socialBar col-xs-12">' +
			'<label class="title text-dark margin-top-5">' + trad.copylink + ' :</label>' +
			'<input type="text" class="form-control margin-bottom-10" value="' + url + '"/>';
		if (userId != null && userId != '') {
			var shareLogoPath = parentModuleUrl + '/images/CO.png';
			var nameSoftware = " Communecter";
			if (notNull(costum)) {
				nameSoftware = costum.title;
				if (typeof costum.logoMin != "undefined")
					shareLogoPath = baseUrl + costum.logoMin;
				else if (typeof costum.logo != "undefined")
					shareLogoPath = baseUrl + costum.logo;
			}

			str += '<button class="share-co-btn btn-share co-btn" data-id="' + shareEltId + '"" data-type="' + shareEltType + '">' +
				'<img class="" width="40" data-ownerlink="share"  src="' + shareLogoPath + '"/> ' + nameSoftware;
			'</button>';
		}
		str += '<button class="share-co-btn facebook-btn" onclick="window.open(\'https://www.facebook.com/sharer.php?u=' + url + '\',\'_blank\')">' +
			'<img class="" width="25" src="' + parentModuleUrl + '/images/social/facebook-icon-64.png"/> Facebook' +
			'</button>';
		//str='<i class="fa fa-twitter-square" style="float:right;margin-left:0.5em;" onclick="window.open("https://twitter.com/intent/tweet?url='+url+'","_blank")"></i>';
		str += '<button class="share-co-btn twitter-btn" onclick="window.open(\'https://twitter.com/intent/tweet?url=' + url + '\',\'_blank\')">' +
			'<img class="" width="25" src="' + parentModuleUrl + '/images/social/twitter-icon-64.png"/> Twitter' +
			'</button>' +
			'</div>';
		$('#modal-share').modal('show');
		$('#modal-share #htmlElementToShare').html(str);
		$('#modal-share #btn-share-it, #modal-share .title-share-modal, #modal-share #msg-share').hide();
		directory.bindShareElt();
	},
	bindShareElt: function () {
		$('.btn-share').off().click(function () {
			var thiselement = this;
			var type = $(thiselement).attr('data-type');
			var id = $(thiselement).attr('data-id');

			$('#modal-share').modal('show');

			mylog.log('directory.bindBtnShareElt ' + type + ' - ' + id);
			var html = '';

			if ($('#news-list li#' + type + id + ' .timeline-panel').length > 0)
				html = $('#news-list li#' + type + id + ' .timeline-panel').html();

			if ($('.timeline-body .newsActivityStream' + id).length > 0)
				html = $('.timeline-body .newsActivityStream' + id).html();

			if (html == '' && $('.searchEntityContainer#entity_' + type + '_' + id).length > 0) {
				var light = $('.searchEntityContainer#entity_' + type + '_' + id).hasClass('entityLight') ? 'entityLight' : '';
				html = '<div class="searchEntityContainer smartgrid-slide-element ' + type + ' ' + light + '">' + $('.searchEntityContainer#entity_' + type + '_' + id).html() + '</div>';
			}

			if (html == '' && type != 'news' && type != 'activityStream' && typeof contextData != 'undefined' && notNull(contextData)) {
				mylog.log('HERE', contextData);
				html = directory.showResultsDirectoryHtml(new Array(contextData), type);
			}

			$('#modal-share #htmlElementToShare').html(html);
			$('#modal-share #btn-share-it, #modal-share .title-share-modal, #modal-share #msg-share').show();
			$('#modal-share #btn-share-it').attr('data-id', id);
			$('#modal-share #btn-share-it').attr('data-type', type);
			//mentionsInit.get('#msg-share');

			$('#modal-share #btn-share-it').off().click(function () {
				directory.shareIt('#modal-share #btn-share-it');
			});
		});
	},
	shareIt: function (thiselement) {
		var formData = new Object();
		formData.parentId = $(thiselement).attr('data-id');
		formData.childId = userId;
		formData.childType = 'citoyens';
		formData.connectType = 'share';
		var type = $(thiselement).attr('data-type');

		var comment = $("#msg-share").val();
		formData.comment = comment;

		//formData=mentionsInit.beforeSave(formData, '#msg-share',"comment");

		$('#msg-share').val('');

		//traduction du type pour le floopDrawer
		var typeOrigine = dyFInputs.get(type).col;
		if (typeOrigine == "persons") { typeOrigine = personCOLLECTION; }
		formData.parentType = typeOrigine;
		if (type == "person") type = "people";
		else type = dyFInputs.get(type).col;
		ajaxPost(
			null,
			baseUrl + "/news/co/share",
			formData,
			function (data) {
				if (data.result) {
					//console.log(data);
					//mentionsInit.reset('#msg-share');
					$("#modal-share #htmlElementToShare").html("");
					$(thiselement).attr("data-original-title", "Vous avez partagé cette page avec votre réseau");
					toastr.success(data.msg);
				}
			}
		);
	},
	switcherViewer: function (results, dom) {
		$(".switchDirectoryView").off().on("click", function () {
			$(".switchDirectoryView").removeClass("active");
			$(this).addClass("active");
			directory.viewMode = $(this).data("value");
			var str = directory.showResultsDirectoryHtml(results);
			var domId = (notNull(dom)) ? dom : "#dropdown_search";
			$(domId).html(str);
			//setTimeout(function(){ directory.checkImage(results);}, 200);
			//active les link lbh
			coInterface.bindLBHLinks();
			//initBtnAdmin();
			directory.bindBtnElement();
			if (userId != "") {
				var param = {
					typeEntity: "citoyens",
					value: directory.viewMode,
					name: "directoryView"
				};
				ajaxPost(
					null,
					baseUrl + "/co2/element/updatesettings",
					param,
					function (data) { }
				);
			}
		});
	},
	showResultsDirectoryHtml: function (resultsDirectory, renderType, smartGrid, community, unique,data) {

		mylog.log('directory.showResultsDirectoryHtml', resultsDirectory, renderType, data);
		directory.resultKeys = [];
		directory.resultKeys = Object.keys(resultsDirectory);

		var str = '';
		if(unique) {
			var formatParamsList = [];
			$.each(resultsDirectory, function (i, params) {
				formatParamsList.push(directory.prepParamsHtml(params, smartGrid, community, data))
			});
			if (notNull(renderType) && typeof renderType == "string") {
				renderViewFunction = eval(renderType);
				if (typeof renderViewFunction == "function")
					str = renderViewFunction(formatParamsList, smartGrid);
				else
					str = directory.elementPanelHtml(formatParamsList, smartGrid);
			} else
				str = directory.elementPanelHtml(formatParamsList, smartGrid);
		} else {
			$.each(resultsDirectory, function (i, params) {
				var formatParams = directory.prepParamsHtml(params, smartGrid, community, data);
				//Pour agenda, renderType == eventPanelHtml
				//Pour #dda, renderType == coopPanelHtml
				// RenderType est donné dans filters.js, searchObj.result.renderView
				if (notNull(renderType) && typeof renderType == "string") {
					renderViewFunction = eval(renderType);
					if (typeof renderViewFunction == "function")
						str += renderViewFunction(formatParams, smartGrid);
					else
						str += directory.elementPanelHtml(formatParams, smartGrid);
				} else
					str += directory.elementPanelHtml(formatParams, smartGrid);
			});
		}
		//	} //end each
		mylog.log('END -----------showResultsDirectoryHtml (' + str.length + ' html caracters generated)');
		return str;
	},
	prepParamsHtml: function (data, smartGrid, community, extraData) {
		var formatedData = jQuery.extend(true, {}, data);
		mylog.log("formated data prepParamsHtml", formatedData);
		formatedData.containerClass = "smartgrid-slide-element start-masonry ";
		formatedData.containerClass += formatedData.collection + " ";
		if (typeof formatedData.type != 'undefined' && notNull(formatedData.type))
			formatedData.containerClass += formatedData.type + " ";
		if (typeof formatedData.category != 'undefined' && notNull(formatedData.category))
			formatedData.containerClass += formatedData.category + ' ';
		if (typeof formatedData.id == "undefined" && typeof formatedData._id != "undefined")
			formatedData.id = formatedData._id.$id;
		formatedData.containerClass+="msr-id-"+formatedData.id+" ";
			formatedData.hash = "#page.type." + formatedData.collection + ".id." + formatedData.id;
		if (typeof extraData != "undefined" && extraData.redirectPage) {
			formatedData.redirectPage = extraData.redirectPage
		}
		if (typeof directory.costum != 'undefined' &&
			directory.costum != null) {
				if(typeof directory.costum.preview != 'undefined' &&
				directory.costum.preview === true){
					formatedData.hashClass = ' lbh-preview-element';
				}
				let pg = window.location.href.split("#")[1];
				if(pg && pg.indexOf("?")!=-1){
					pg=pg.split("?")[0];
				}
				if(pg && typeof directory.costum[pg] != 'undefined' &&
				directory.costum[pg].preview === false){
					formatedData.hashClass = ' lbh';
				}
		} else if (typeof directory.costum != 'undefined' &&
			directory.costum != null &&
			typeof directory.costum.preview != 'undefined' &&
			directory.costum.preview === false) {
			formatedData.hashClass = ' lbh';
		} else
			formatedData.hashClass = ($.inArray(formatedData.collection, ['crowdfunding', 'poi', 'classifieds']) >= 0) ? ' lbh-preview-element' : ' lbh';
		var typeElt = (typeof formatedData.type != "undefined") ? formatedData.type : null;
		var settingsTypeElt = directory.getTypeObj(formatedData.collection, typeElt);
		formatedData.color = (typeof settingsTypeElt.color != "undefined") ? settingsTypeElt.color : "grey";
		formatedData.icon = (typeof poi.filters[formatedData.type] != "undefined") ? poi.filters[formatedData.type].icon : ((typeof settingsTypeElt.icon != "undefined") ? settingsTypeElt.icon : "circle");
		!formatedData.descriptionStr ? formatedData.descriptionStr = "" : "";
		if (typeof formatedData.shortDescription != 'undefined' && notNull(formatedData.shortDescription)) 
			formatedData.descriptionStr = formatedData.shortDescription;
		else if (typeof formatedData.description != 'undefined' && notNull(formatedData.description)) {
			formatedData.description = formatedData.description.replace(/(<([^>]+)>)/ig, '');
			formatedData.descriptionStr = (formatedData.description.length > 150) ? formatedData.description.substring(0, 150) + '...' : formatedData.description;
		}
		if (typeof formatedData.startDate != "undefined" && typeof formatedData.startDate.sec != "undefined") {//isTimestamp(formatedData.startDate)) {
			formatedData.startDate = new Date(formatedData.startDate.sec * 1000).toISOString();
		}
		if (typeof formatedData.endDate != "undefined" && typeof formatedData.endDate.sec != "undefined") {
			formatedData.endDate = new Date(formatedData.endDate.sec * 1000).toISOString();
		}
		var thisTags = '';
		var countTagsInClass = 0;
		var countFourTags0nly = 1;
		if (typeof formatedData.tags != 'undefined' && formatedData.tags != null) {
			$.each(formatedData.tags, function (key, value) {
				if (typeof value != 'undefined' && value != '' && value != 'undefined') {
					var tagTrad = typeof tradCategory[value] != 'undefined' ? tradCategory[value] : value;
					//thisTags += '<li class="tag" data-tag-value="'+slugify(value, true)+'" data-tag-label="'+tagTrad+'">#' + tagTrad + '</li> ';
					if (countFourTags0nly < 5) {
						thisTags += '<a href="javascript:;" class="btn-tag-panel" data-tag="' + tagTrad + '"><span class="badge bg-transparent btn-tag tag commonClassTags" data-tag-value="' + slugify(value, true) + '" data-tag-label="' + tagTrad + '">#' + tagTrad + '</span> </a>';
						countFourTags0nly++;
					} else {
						thisTags += '<a href="javascript:;" class="btn-tag-panel btn-tags-hidden hidden" data-tag="' + tagTrad + '"><span class="badge bg-transparent btn-tag tag commonClassTags" data-tag-value="' + slugify(value, true) + '" data-tag-label="' + tagTrad + '">#' + tagTrad + '</span> </a>';
					}
					if (countTagsInClass < 6) {
						formatedData.containerClass += slugify(value, true) + ' ';
						countTagsInClass++;
					}
				}
			});
			if (countFourTags0nly > 4 && (formatedData.tags.length - 4) != 0) {
				thisTags += '<a href="javascript:;" class="badge bg-transparent btn-tag tag numberTag commonClassTags">+ ' + (formatedData.tags.length - 4) + '</a> ';
			}
			formatedData.tagsHtml = thisTags;
		}
		// Community est égale dans les page element à contextData 
		// Il permet de générer l'affichage des status des liens (admin, inviting, pending) et les roles 
		// Community est l'héritage de searchObj.results.contextData qui peut être transmis à l'initialisation du searchObj  
		if (notNull(community)) {
			formatedData = directory.prepContextData(formatedData, community);
		}
		//formatedData.elRolesList = '';

		if (notNull(formatedData.tobeactivated) && formatedData.tobeactivated == true) {
			formatedData.isInviting = true;
		}
		var onloadImage = "directory.checkImage(this)";
		if (notNull(smartGrid) && smartGrid === true)
			onloadImage = "directory.loadingImg(this)";

		formatedData.imageProfilHtml = '<div class="div-img"><i class=\'fa fa-image fa-2x\'></i></div>';
		if ('undefined' != typeof directory.costum && notNull(directory.costum)
			&& typeof directory.costum.results != 'undefined'
			&& typeof directory.costum.results[formatedData.collection] != 'undefined'
			&& typeof directory.costum.results[formatedData.collection].defaultImg != 'undefined')
			formatedData.imageProfilHtml = '<div class="container-img-card"><img class=\'img-responsive img-profil-entity lzy_img isLoading\' data-id="'+formatedData.id+'" onload=\'' + onloadImage + '\' onerror=\'' + onloadImage + '\' src=\'' + modules.co2.url + '/images/thumbnail-default.jpg\' data-src="' + assetPath + directory.costum.results[formatedData.collection].defaultImg + '"/></div>';
		if ('undefined' != typeof formatedData.profilMediumImageUrl && formatedData.profilMediumImageUrl != '') {
			if (isValidUrl(formatedData.profilMediumImageUrl)) {
				formatedData.imageProfilHtml = '<div class="container-img-card"><img class=\'img-responsive img-profil-entity lzy_img isLoading\' data-id="'+formatedData.id+'" onload=\'' + onloadImage + '\' onerror=\'' + onloadImage + '\' src=\'' + modules.co2.url + '/images/thumbnail-default.jpg\' data-src=\'' + formatedData.profilMediumImageUrl + '\'/></div>'
			} else {
				formatedData.imageProfilHtml = '<div class="container-img-card"><img class=\'img-responsive img-profil-entity lzy_img isLoading\' data-id="'+formatedData.id+'" onload=\'' + onloadImage + '\' onerror=\'' + onloadImage + '\' src=\'' + modules.co2.url + '/images/thumbnail-default.jpg\' data-src=\'' + baseUrl + formatedData.profilMediumImageUrl + '\'/></div>';

			}
		}
		if (typeObj[formatedData.collection] && typeObj[formatedData.collection].col == "poi"
			&& typeof formatedData.medias != "undefined" && typeof formatedData.medias[0] != "undefined"
			&& typeof formatedData.medias[0].content != "undefined" && typeof formatedData.medias[0].content.image != "undefined")
			formatedData.imageProfilHtml = '<div class="container-img-card"><img class=\'img-responsive img-profil-entity lzy_img isLoading\' onload=\'' + onloadImage + '\' onerror=\'' + onloadImage + '\' src=\'' + modules.co2.url + '/images/thumbnail-default.jpg\'  data-src=\'' + formatedData.medias[0].content.image + '\'/></div>';

		if (formatedData.collection== "badges"
			&& typeof formatedData.badgeClass != "undefined" && typeof formatedData.badgeClass.image != "undefined"){
			formatedData.imageProfilHtml = '<div class="container-img-card"><img class=\'img-responsive img-profil-entity lzy_img isLoading\' onload=\'' + onloadImage + '\' onerror=\'' + onloadImage + '\' src=\'' + modules.co2.url + '/images/thumbnail-default.jpg\'  data-src=\'' + formatedData.badgeClass.image + '\'/></div>';
		}
		
		if (formatedData.imageSameAsContext && formatedData.profilMediumImageUrl && formatedData.profilMediumImageUrl != '') {
			if (isValidUrl(formatedData.profilMediumImageUrl)) {
				formatedData.imageProfilHtml = '<div class="container-img-card"><img class=\'img-responsive img-profil-entity lzy_img isLoading\' data-id="'+formatedData.id+'" onload=\'' + onloadImage + '\' onerror=\'' + onloadImage + '\' src=\'' + modules.co2.url + '/images/thumbnail-default.jpg\' data-src=\'' + formatedData.profilMediumImageUrl + '\'/></div>'
			} else {
				formatedData.imageProfilHtml = '<div class="container-img-card"><img class=\'img-responsive img-profil-entity lzy_img isLoading\' data-id="'+formatedData.id+'" onload=\'' + onloadImage + '\' onerror=\'' + onloadImage + '\' src=\'' + modules.co2.url + '/images/thumbnail-default.jpg\' data-src=\'' + baseUrl + formatedData.profilMediumImageUrl + '\'/></div>';

			}
		}

		formatedData.localityHtml = "", formatedData.streetAddress = '', formatedData.postalCode = '', formatedData.city = '', formatedData.cityName = '', formatedData.mapMarker = '';
		if (formatedData.address != null) {
			formatedData.streetAddress = formatedData.address.streetAddress ? formatedData.address.streetAddress : '';
			formatedData.postalCode = formatedData.address.postalCode ? formatedData.address.postalCode : '';
			formatedData.city = formatedData.address.addressLocality ? formatedData.address.addressLocality : '';
			if (formatedData.streetAddress != '' || formatedData.postalCode != '' || formatedData.city != '')
				//formatedData.country = formatedData.address.level1Name ? formatedData.address.level1Name : '';
				formatedData.localityHtml = '<i class="fa fa-map-marker text-bold"></i> ' + formatedData.streetAddress + ' ' + formatedData.postalCode + ' ' + formatedData.city;

		}
		return formatedData;
	},
	prepContextData: function (formatedData, community) {
		if (notNull(community) && exists(community.links) && notNull(community.links) && notNull(community.connectType) && exists(community.links[community.connectType])) {
			if (notNull(community.edit) && community.edit === true) formatedData.edit = community.connectType;
			formattedProcess(formatedData, community);
			formatedData.connectType = community.connectType;
			// reassigner la mode edition quand c'est activitypub
			if(notNull(contextData.projectCreator)){
			if(notNull(contextData.projectCreator) && contextData.projectCreator != activitypubActorId ){
				if (notNull(activitypubActorId)) {
					if (notNull(community.links["activitypub"]) &&
					exists(community.links["activitypub"][community.connectType])) {
				    	community.edit = isActorCanEdit(community.links["activitypub"][community.connectType], activitypubActorId);
				  }
				}
			}
			}else{
				if(notNull(contextData.attributedTo) && contextData.attributedTo != activitypubActorId ){
					if (notNull(activitypubActorId)) {
					if (notNull(community.links["activitypub"]) && notNull(community.connectType) &&
					exists(community.links["activitypub"][community.connectType]))  {
				    	community.edit = isActorCanEdit(community.links["activitypub"][community.connectType], activitypubActorId);
				  }
				}
			}
			}
			
			if(!isValidUrl(formatedData.id)) formatedData.rolesAndStatusLink = (notNull(community.connectType) && exists(community.links[community.connectType][formatedData.id]) ? community.links[community.connectType][formatedData.id] : {});
			//if(exists(community.links[community.connectType][formatedData.id])) formatedData.rolesAndStatusLink=community.links[community.connectType][formatedData.id];
			formatedData.statusLinkHtml = '';
			if (typeof (formatedData.rolesAndStatusLink) != 'undefined') {
				if (typeof (formatedData.rolesAndStatusLink.toBeValidated) != 'undefined' && typeof (formatedData.rolesAndStatusLink.isAdminPending) != 'undefined')
					formatedData.statusLinkHtml = '<span class=\'entityStatusLink waitingToBecome text-red italic\'>' + trad.waitingValidationForAdmin + '</span>';
				else if (typeof (formatedData.rolesAndStatusLink.toBeValidated) != 'undefined')
					formatedData.statusLinkHtml = '<span class=\'entityStatusLink waitingToBecome text-red italic\'>' + trad.waitingValidation + '</span>';
				else if (typeof (formatedData.rolesAndStatusLink.isAdminPending) != 'undefined')
					formatedData.statusLinkHtml = '<span class=\'entityStatusLink memberAskForAdmin text-red italic\'>' + trad.memberAskToBeAdmin + '</span>';
				else if (typeof formatedData.rolesAndStatusLink.isAdminInviting != "undefined")
					formatedData.statusLinkHtml = '<span class=\'entityStatusLink adminInviting text-red italic\'>' + trad.invitingToAdmin + '</span>';
				else if (typeof formatedData.rolesAndStatusLink.isAdmin != 'undefined')
					formatedData.statusLinkHtml = '<span class=\'entityStatusLink admin\'>' + trad.administrator + '</span>';
				if (typeof formatedData.rolesAndStatusLink.isInviting != 'undefined' && formatedData.collection != "projects") {
					formatedData.statusLinkHtml += '<span class=\'entityStatusLink italic\'>' + trad.InvitationLaunched + '</span>';
				}
				if (typeof formatedData.rolesAndStatusLink.isInviting != 'undefined' && formatedData.collection == "projects") {
					formatedData.statusLinkHtml += '<span class=\'entityStatusLink italic\'></span>';
				}


				//var thisRoles = '';
				formatedData.rolesHtml = '';
				if (typeof formatedData.rolesAndStatusLink.roles =="object" && notNull(formatedData.rolesAndStatusLink.roles) && notEmpty(formatedData.rolesAndStatusLink.roles) ) {
					formatedData.rolesHtml += '<small><b>' + trad.roleroles + ' :</b> ';
					formatedData.rolesHtml += (Array.isArray(formatedData.rolesAndStatusLink.roles)) ? formatedData.rolesAndStatusLink.roles.join(', ') : formatedData.rolesAndStatusLink.roles;
					//$.each(formatedData.rolesLink, function(key, value){
					//	if(typeof value != 'undefined' && value != '' && value != 'undefined')
					//		formatedData.containerClass += slugify(value)+' ';
					//});
					formatedData.rolesHtml += '</small>';
					//formatedData.rolesHtml = thisRoles;
				}
			}
		}
		if(notNull(community) && exists(community.links) && notNull(community.links) && notNull(community.links["activitypub"]) && notNull(community.connectType) &&
				exists(community.links["activitypub"][community.connectType])){
				if (notNull(contextData.attributedTo)) {
					var isSameDomain = isInSameDomain(contextData.objectId, activitypubActorId);
					if (!isSameDomain && contextData.attributedTo !== activitypubActorId) {
						if (notNull(activitypubActorId)) {
							if (notNull(community.links["activitypub"]) && exists(community.links["activitypub"][community.connectType])) {
								community.edit = isActorCanEdit(community.links["activitypub"][community.connectType], activitypubActorId);
							}
						}
					}
				}
				if (notNull(community.edit) && community.edit === true) formatedData.edit = community.connectType;
				formattedProcess(formatedData, community);
		}

		return formatedData;
	},
	getUserInvitToolBar : function(data){
		var html = '';
		html += '<div class="adminToolBarDirectory">';
		if(typeof data.rolesAndStatusLink != 'undefined' && typeof data.rolesAndStatusLink['isInviting'] != 'undefined' &&  data.rolesAndStatusLink['isInviting']){
			html += '<button class="btn btn-default btn-xs acceptAsBtn"' +
				' data-type="' + data.collection + '" data-id="' + data.id + '" data-connect-validation="isInviting" data-parent-hide="2"' +
				'>' +
				'<i class="fa fa-user"></i> ' + trad['accept'] +
				'</button> ';
		}

		html += '<button class="btn btn-default btn-xs disconnectConnection"' +
					' data-type="' + data.collection + '" data-id="' + data.id + '" data-connection="'+data.connectType+'" data-parent-hide="3"' +
					'>' +
					'<i class="fa fa-unlink"></i> ' + trad['refuse'] +
					'</button> ';
		html += '</div>'
		return html;
	},
	getAdminToolBar: function (data) {
		mylog.log('directory.getAdminToolBar', data);
		//var countBtn=0;
		var html = '';
		html += '<div class="adminToolBarDirectory">';

		if ($.inArray(data.edit, ['members', 'contributors', 'attendees']) >= 0) {
			if (data.collection != 'organizations' && typeof data.rolesAndStatusLink != 'undefined' && typeof data.rolesAndStatusLink['isAdmin'] == 'undefined') {
				html += '<button class="addAsAdminBtnCommunity btn btn-default btn-xs" data-id="' + data.id + '" ' +
					'data-type="' + data.collection +
					'"' +
					'data-activitypub-enabled="' +
					(typeof data.activitypub != "undefined" ? data.activitypub : false) +
					'"' +
					'data-activitypub-link="' +
					(typeof elementData.objectId != "undefined"
						? elementData.objectId
						: "") +
					'"' +
					'">' +
					'<i class="fa fa-user-plus"></i> ' + trad.addasadmin +
					'</button>';
			}
			if (/*data.collection != 'organizations' &&*/ typeof data.rolesAndStatusLink != 'undefined' && typeof data.rolesAndStatusLink['toBeValidated'] != 'undefined' && typeof data.rolesAndStatusLink['isAdminPending'] == 'undefined') {
				html += '<button class="btn btn-default btn-xs acceptAsBtn"' +
					' data-type="' + data.collection + '" data-id="' + data.id + '" data-connect-validation="toBeValidated" data-parent-hide="2"' +
					'data-activitypub-enabled="' +
					(typeof data.activitypub != "undefined" ? data.activitypub : false) +
					'"' +
					'data-activitypub-link="' +
					(typeof elementData.objectId != "undefined"
					? elementData.objectId
					: "") +
					'"' +
					'>' +
					'<i class="fa fa-user"></i> ' + trad['acceptas' + data.edit] +
					'</button> ';
			} else if (data.collection != 'organizations' && typeof data.rolesAndStatusLink != 'undefined' && typeof data.rolesAndStatusLink['isAdminPending'] != 'undefined') {
				html += '<button class="btn btn-default btn-xs acceptAsBtn"' +
					' data-type="' + data.collection + '" data-id="' + data.id + '" data-connect-validation="isAdminPending" data-parent-hide="2"' +
					'data-activitypub-enabled="' +
					(typeof data.activitypub != "undefined" ? data.activitypub : false) +
					'"' +
					'data-activitypub-link="' +
					(typeof elementData.objectId != "undefined"
						? elementData.objectId
						: "") +
					'"' +
					'>' +
					'<i class="fa fa-user-plus"></i> ' + trad['acceptasadmin'] +
					'</button> ';
			}
			if ($.inArray(data.edit, ['members', 'contributors', 'attendees']) >= 0) {
				var roles = '';
				if (typeof data.rolesAndStatusLink != "undefined" && typeof data.rolesAndStatusLink.roles == "object"  && exists(data.rolesAndStatusLink.roles) && data.rolesAndStatusLink.roles != null)
					roles += data.rolesAndStatusLink.roles.join(', ').replace("'", "&#8217;");
				html += '<button class="btn btn-default btn-xs manageRolesCommunity" data-id="' + data.id + '" ' +
					'data-type="' + data.collection + '" ' +
					'data-name="' + addslashes(data.name) + '" ' +
					'data-connector="' + data.edit + '" ' +
					'data-roles="' + roles + 
					'"' +
					'data-activitypub-enabled="' +
					(typeof data.activitypub != "undefined" ? data.activitypub : false) +
					'"' +
					'data-activitypub-link="' +
					(typeof elementData.objectId != "undefined"
						? elementData.objectId
						: "") +
					'"' +
					'">' +
					'<i class="fa fa-pencil"></i> ' + trad.addmodifyroles +
					'</button>';
			}

			if (typeof data.rolesAndStatusLink != 'undefined' &&
				((typeof data.rolesAndStatusLink['isAdminPending'] != 'undefined' && data.rolesAndStatusLink['isAdminPending'] == true) ||
					(typeof data.rolesAndStatusLink['isAdmin'] != 'undefined' && data.rolesAndStatusLink['isAdmin'] == true))) {
				html += ' <button class="removeadminrights btn-default btn-xs" data-id="'+data.id+'"  data-type="'+data.collection+'"data-name="'+data.name+'" data-connection="' + data.edit + '"  data-isadmin="true"' + 
				'data-activitypub-enabled="' +
				(typeof data.activitypub != "undefined" ? data.activitypub : false) +
				'"' +
				'data-activitypub-link="' +
				(typeof elementData.objectId != "undefined"
				  ? elementData.objectId
				  : "") +
				'"' +
				'>' +
					
				
				'<i class="fa fa-user-times"></i> ' + trad.removeadminrights +
					'</button> ';
			}
			if (data.collection == 'organizations'
				|| (typeof data.rolesAndStatusLink != 'undefined' /*&& (typeof data.rolesAndStatusLink['isAdmin'] == 'undefined' || typeof data.rolesAndStatusLink['isAdminPending'] != 'undefined')*/)) {
				html += '<button class="btn btn-default btn-xs disconnectConnection"' +
					' data-type="' + data.collection + '" data-id="' + data.id + '" data-connection="' + data.edit + '" data-parent-hide="3"' +
					'data-activitypub-enabled="' +
					(typeof data.activitypub != "undefined" ? data.activitypub : false) +
					'"' +
					'data-activitypub-link="' +
					(typeof elementData.objectId != "undefined"
					? elementData.objectId
					: "") +
					'"' +
					//' style="bottom:'+(30*countBtn)+'px"'
					'>' +
					'<i class="fa fa-unlink"></i> ' + trad['delete' + data.edit] +
					'</button> ';
				//	countBtn++;
			}
		}
		if (data.edit == 'follows') {
			html += '<button class="btn btn-default btn-xs disconnectConnection" data-type="' + data.collection + '" data-id="' + data.id + '" data-connection="' + data.edit + '" data-parent-hide="2"'+
			'data-activitypub-enabled="' +
			(typeof data.activitypub != "undefined" ? data.activitypub : false) +
			'"' +
			'data-activitypub-link="' +
			(typeof elementData.objectId != "undefined"
			  ? elementData.objectId
			  : "") +
			'"' +
			'>' +
				'<i class="fa fa-unlink"></i> ' + trad.unfollow +
				'</button> ';
			//	countBtn++;
		}
		if ($.inArray(data.edit, ['organizations', 'projects', 'poi', 'crowdfunding', 'classifieds', 'networks']) >= 0) {
			html += '<button class="btn btn-default btn-xs disconnectConnection"' +
				' data-type="' + data.collection + '" data-id="' + data.id + '" data-connection="' + data.edit + '" data-parent-hide="3"' +
				'data-activitypub-enabled="' +
				(typeof data.activitypub != "undefined" ? data.activitypub : false) +
				'"' +
				'data-activitypub-link="' +
				(typeof elementData.objectId != "undefined"
				? elementData.objectId
				: "") +
				'"' +
				//' style="bottom:'+(30*countBtn)+'px"
				'>' +
				'<i class="fa fa-unlink"></i> ' + trad['cancellink'] +
				'</button> ';
			//countBtn++;
		}


		html += '</div>';
		//mylog.log('directory.getAdminToolBar html end', html);
		return html;
	},
	bindEventAdmin: function () {
		$('.adminToolBar').each(function () {
			if ($(this).children('button').length == 0) {
				$(this).parent().find('.adminIconDirectory').remove();
				$(this).remove();
			}
		});
		$(".manageRolesCommunity").off().on("click", function () {
			if (
				typeof $(this).data("activitypub-enabled") != "undefined" &&
				($(this).data("activitypub-enabled") == "true" ||
				$(this).data("activitypub-enabled") == true)
			) {
				//console.log("updata role community", $(this).data("roles"));
				links.updateRolesActivityPub(
				$(this).data("activitypub-link"),
				$(this).data("id"),
				$(this).data("name"),
				$(this).data("connector"),
				$(this).data("roles")
				);
			} else {
				links.updateRoles($(this).data("id"), $(this).data("type"), $(this).data("name"), $(this).data("connector"), $(this).data("roles"));
			}
		});
		$(".addAsAdminBtnCommunity").off().on("click", function () {
			if (
				typeof $(this).data("activitypub-enabled") != undefined &&
				($(this).data("activitypub-enabled") == "true" ||
				$(this).data("activitypub-enabled") == true)
			) {
				links.postActivityPub(
				contextData.type,
				contextData.id,
				{
					objectId: $(this).data("activitypub-link"),
					targetId: $(this).data("id"),
					connectType: "addAsAdmin",
				},
				"addAsAdmin"
				);
			} else {
				links.connect(contextData.type, contextData.id, $(this).data("id"), $(this).data("type"), 'admin', '', true);
			}
		});
		$('.adminIconDirectory, .container-img-profil').mouseenter(function () {
			$(this).parent().find('.adminToolBar').show();
		});

		$('.adminToolBar').mouseleave(function () {
			$(this).hide();
		});

		mylog.log('initBtnAdmin');

		$('.removeadminrights').off().on("click", function () { 
			var id = $(this).data("id");
			var type = $(this).data("type"); 
			var name = $(this).data("name"); 
			var connectType = $(this).data("connection")
			var isAdmin = $(this).data("isadmin");

			if (
				typeof $(this).data("activitypub-enabled") != undefined &&
				($(this).data("activitypub-enabled") == "true" ||
				$(this).data("activitypub-enabled") == true)
			) {
				links.postActivityPub(
				  contextData.type,
				  contextData.id,
				  {
					parentId: contextData.id,
					objectId: $(this).data("activitypub-link"),
					targetId: $(this).data("id")
				  },
				 "delete_contribution"
				);
			} else {
				links.updateadminlink(contextData.type,
				contextData.id,
				id,
				type,
				connectType,
				(isAdmin === true ? false : true ),
				name
			);
		}
			
		})
		$('.disconnectConnection').off().on("click", function () {
			var $this = $(this);
			if (
				typeof $(this).data("activitypub-enabled") != undefined &&
				($(this).data("activitypub-enabled") == "true" ||
				$(this).data("activitypub-enabled") == true)
			) {
				links.disconnectActivityPub(
				contextData.type,
				contextData.id,
				$(this).data("id"),
				$(this).data("activitypub-link"),
				$(this).data("connection"),
				function () {
					$this.parents().eq($this.data("parent-hide")).fadeOut();
				}
				);
			} else {
				links.disconnect(contextData.type,
					contextData.id,
					$this.data('id'),
					$this.data('type'),
					$this.data('connection'),
					function () {
						$this.parents().eq($this.data('parent-hide')).fadeOut();
				});
			}
		});
		$('.numberTag').off().on('click', function () {
			$(this).parent().find('.numberTag').hide();
			$(this).parent().find('.btn-tags-hidden').removeClass('hidden');
		});

		$('.acceptAsBtn').off().on('click', function () {
			if (
				typeof $(this).data("activitypub-enabled") != "undefined" &&
				($(this).data("activitypub-enabled") == "true" ||
				$(this).data("activitypub-enabled") == true)
			) {
				links.validateActivityPub(
				contextData.type,
				contextData.id,
				$(this).data("activitypub-link"),
				$(this).data("id"),
				$(this).data("type"),
				$(this).data("connect-validation")
				);
			} else {
				if (typeof contextDataAap != "undefined" && typeof contextDataAap["id"] != "undefined" && typeof contextDataAap["type"] != "undefined") {
					links.validate(contextDataAap["type"], contextDataAap["id"], $(this).data("id"), $(this).data("type"), $(this).data("connect-validation"));
				} else {
					links.validate(contextData.type, contextData.id, $(this).data("id"), $(this).data("type"), $(this).data("connect-validation"));
				}
			}
		});
	},
	getTypeObj: function (col, type) {
		var elt = {};
		var e = (notNull(type) && typeof typeObj[type] != "undefined") ? type : col;
		if (typeof typeObj[e] != 'undefined') {
			if (typeof typeObj[e].name != 'undefined')
				elt.name = typeObj[e].name;
			else if (typeof typeObj[e].sameAs != 'undefined')
				elt.name = typeObj[typeObj[e].sameAs].name;

			if (typeof typeObj[e].icon != 'undefined')
				elt.icon = typeObj[e].icon;
			else if (typeof typeObj[e].sameAs != 'undefined')
				elt.icon = typeObj[typeObj[e].sameAs].icon;

			if (typeof typeObj[e].color != 'undefined')
				elt.color = typeObj[e].color;
			else if (typeof typeObj[e].sameAs != 'undefined')
				elt.color = typeObj[typeObj[e].sameAs].color;

			if (typeof typeObj[e].formType != 'undefined')
				elt.formType = typeObj[e].formType;
			else if (typeof typeObj[e].sameAs != 'undefined')
				elt.formType = typeObj[typeObj[e].sameAs].formType;

			if (typeof typeObj[e].formSubType != 'undefined')
				elt.formSubType = typeObj[e].formSubType;
			else if (typeof typeObj[e].sameAs != 'undefined' && typeof typeObj[typeObj[e].sameAs].formSubType != 'undefined')
				elt.formSubType = typeObj[typeObj[e].sameAs].formSubType;

			if (typeof typeObj[e].createLabel != 'undefined')
				elt.createLabel = typeObj[e].createLabel;
			else if (typeof typeObj[e].sameAs != 'undefined')
				elt.createLabel = typeObj[typeObj[e].sameAs].createLabel;

			if (typeof typeObj[e].col != 'undefined')
				elt.col = typeObj[e].col;
			else if (typeof typeObj[e].sameAs != 'undefined')
				elt.col = typeObj[typeObj[e].sameAs].col;
			if (typeof typeObj[e].ctrl != 'undefined')
				elt.ctrl = typeObj[e].ctrl;
			else if (typeof typeObj[e].sameAs != 'undefined')
				elt.ctrl = typeObj[typeObj[e].sameAs].ctrl;

			if (typeof typeObj[e].formParent != 'undefined')
				elt.formParent = typeObj[e].formParent;

		}
		return elt;
	},
	//builds a small sized list
	buildList: function (list) {
		$('.favSectionBtnNew,.favSection').remove();
		mylog.warn('START >>>>>> buildList', smallMenu.destination + ' #listDirectory');

		$.each(list, function (key, slist) {
			var subContent = directory.showResultsDirectoryHtml(slist, key);
			if (notEmpty(subContent)) {
				favTypes.push(typeObj[key].col);

				var color = (typeObj[key] && typeObj[key].color) ? typeObj[key].color : 'dark';
				var icon = (typeObj[key] && typeObj[key].icon) ? typeObj[key].icon : 'circle';
				$(smallMenu.destination + ' #listDirectory').append('<div class="' + typeObj[key].col + 'fav favSection ">' +
					'<div class=" col-xs-12 col-sm-12">' +
					'<h4 class="text-left text-' + color + '"><i class="fa fa-angle-down"></i> ' + trad[key] + '</h4><hr>' +
					subContent +
					'</div>');
				$('.sectionFilters').append(' <a class="text-black btn btn-default favSectionBtn favSectionBtnNew  bg-' + color + '"' +
					' href="javascript:directory.showAll(".favSection",directory.elemClass);toggle(".' + typeObj[key].col + 'fav",".favSection",1)"> ' +
					'<i class="fa fa-' + icon + ' fa-2x"></i><br>' + trad[key] +
					'</a>');
			}
		});

		directory.bindBtnElement();
		directory.filterList();
		$(directory.elemClass).show();

	},

	getWeekDayName: function (numWeek) {
		var wdays = new Array(trad['sunday'], trad['monday'], trad['tuesday'], trad['wednesday'], trad['thursday'], trad['friday'], trad['saturday'], trad['sunday']);
		if (typeof wdays[numWeek] != 'undefined') return wdays[numWeek];
		else return '';
	},
	getMonthName: function (numMonth) {
		numMonth = parseInt(numMonth);
		var wdays = new Array('', trad['january'], trad['february'], trad['march'], trad['april'], trad['may'], trad['june'], trad['july'], trad['august'], trad['september'], trad['october'], trad['november'], trad['december']);
		if (typeof wdays[numMonth] != 'undefined') return wdays[numMonth];
		else return '';
	},

	//build list of unique tags based on a directory structure
	//on click hides empty parent sections
	filterList: function (elClass, dest) {
		directory.tagsT = [];
		directory.scopesT = [];
		$('#listTags').html('');
		$('#listScopes').html('<h4><i class="fa fa-angle-down"></i> Où</h4>');
		mylog.log('tagg', directory.elemClass);
		$.each($(directory.elemClass), function (k, o) {

			var oScope = $(o).find('.entityLocality').text();

			$.each($(o).find('.btn-tag'), function (i, oT) {
				var oTag = $(oT).data('tag-value');
				if (notEmpty(oTag) && !inArray(oTag, directory.tagsT)) {
					directory.tagsT.push(oTag);
					$('#listTags').append('<a class="btn btn-xs btn-link text-red text-left w100p favElBtn ' + slugify(oTag) + 'Btn" data-tag="' + slugify(oTag) + '" href="javascript:directory.toggleEmptyParentSection(".favSection",".' + slugify(oTag) + '",directory.elemClass,1)"><i class="fa fa-tag"></i> ' + oTag + '</a><br/>');
				}
			});
			if (notEmpty(oScope) && !inArray(oScope, directory.scopesT)) {
				directory.scopesT.push(oScope);
				$('#listScopes').append('<a class="btn btn-xs btn-link text-red text-left w100p favElBtn ' + slugify(oScope) + 'Btn" href="javascript:directory.searchFor("' + oScope + '")"><i class="fa fa-map-marker"></i> ' + oScope + '</a><br/>');
			}
		});

	},

	//todo add count on each tag
	filterTags: function (withSearch, open) {
		directory.tagsT = [];
		$('#listTags').html('');
		if (withSearch) {
			$('#listTags').append('<h5 class=""><i class="fa fa-search"></i> ' + trad['filtertags'] + '</h5>');
			$('#listTags').append('<input id="searchBarTextJS" data-searchPage="true" type="text" class="input-search form-control">');
		}

		$('#listTags').append('<a class="btn btn-link text-red favElBtn favAllBtn" ' +
			'href="javascript:directory.toggleEmptyParentSection(".favSection",null,directory.elemClass,1)">' +
			' <i class="fa fa-refresh"></i> <b>' + trad['seeall'] + '</b></a><br/>');

		$.each($(directory.elemClass), function (k, o) {
			$.each($(o).find('.btn-tag'), function (i, oT) {
				var realTag = $(oT).data('tag-label');


				var oTag = $(oT).data('tag-value').toLowerCase();
				if (notEmpty(oTag) && !inArray(oTag, directory.tagsT)) {
					directory.tagsT.push(oTag);

					$('#listTags').append('<a class="btn btn-link favElBtn text-red elipsis ' + slugify(oTag) + 'Btn" ' +
						'data-tag="' + slugify(oTag) + '" ' +
						'href="javascript:directory.toggleEmptyParentSection(".favSection",".' + slugify(oTag) + '",directory.elemClass,1)">' +
						'#' + realTag +
						'</a><br> ');
				}
			});
		});
		if (directory.tagsT.length && open) {
			directory.showFilters();
		}

	},
	sectionFilter: function (objJson, dest, what, type) {
		mylog.log('sectionFilter', objJson, what, dest);
		if (typeof objJson.sources != 'undefined') {
			var str = '';
			$('.sourcesInterrop').show(800);
			$.each(objJson.sources, function (e, v) {
				str += '<button class="btn btn-default col-xs-12 padding-10 bold text-dark elipsis btn-select-source dropDesign" ' +
					'data-source="' + v.label + '" data-key="' + v.key + '">' +
					'<div class="checkbox-filter pull-left"><label>' +
					'<input type="checkbox" class="checkbox-info">' +
					'<span class="cr"><i class="cr-icon fa fa-circle"></i></span>' +
					'</label></div>' +
					'<span class="pull-left" ><img src="' + parentModuleUrl + v.path + '" width="20" height="20" class=""/></span>' +
					'<span class="pull-left" >' + v.label + '</span>' +
					'</button>';
			});
			$('.dropdown-sources').show();
			$('.dropdown-sources .dropdown-menu').html(str);
		} else {
			$('.dropdown-sources').hide(800);
		}
		if (typeof objJson.sections != 'undefined') {
			str = '';
			$.each(objJson.sections, function (e, v) {
				str += '<button class="btn btn-default col-xs-12 padding-10 bold text-dark elipsis btn-select-section dropDesign" ' +
					'data-section-anc="' + v.label + '" data-key="' + v.key + '" ' +
					'data-section="classifieds">' +
					'<div class="checkbox-filter pull-left"><label>' +
					'<input type="checkbox" class="checkbox-info">' +
					'<span class="cr"><i class="cr-icon fa fa-circle"></i></span>' +
					'</label></div>' +
					'<i class="fa fa-' + v.icon + ' hidden-xs"></i> ' +
					tradCategory[v.labelFront] +
					'</button>';
			});
			$('.dropdown-section .dropdown-menu').html(str);
		}
		if (typeof objJson.filters != 'undefined') {
			str = '';
			$.each(objJson.filters, function (k, o) {
				if (type == 'btn') {
					str += '<div class="col-md-4 padding-5 categoryBtnC ' + k + '">' +
						'<a class="btn tagListEl btn-select-category elipsis categoryBtn ' + k + 'Btn " data-tag="' + k + '" ' +
						'data-key="' + k + '" href="javascript:;">' +
						'<i class="fa fa-' + o.icon + '"></i> <br>' + tradCategory[k] +
						'</a>' +
						'</div>';
				}
				else {
					str += '<button class="btn btn-default col-xs-12 text-dark btn-select-category margin-bottom-5 elipsis dropDesign" style="margin-left:-5px;" data-keycat="' + k + '">' +
						'<div class="checkbox-filter pull-left"><label>' +
						'<input type="checkbox" class="checkbox-info">' +
						'<span class="cr"><i class="cr-icon fa fa-circle"></i></span>' +
						'</label></div>' +
						'<i class="fa fa-' + o.icon + ' hidden-xs"></i> ' + tradCategory[k] + '</button><br>';
				}
				if (typeof o.subcat != 'undefined' && type != 'btn') {
					$.each(o.subcat, function (i, oT) {
						str += '<button class="btn btn-default col-xs-12 text-dark margin-bottom-5 margin-left-15 hidden keycat keycat-' + k + ' dropDesign" data-categ="' + k + '" data-keycat="' + oT.key + '">' +
							'<div class="checkbox-filter pull-left"><label>' +
							'<input type="checkbox" class="checkbox-info">' +
							'<span class="cr"><i class="cr-icon fa fa-circle"></i></span>' +
							'</label></div>' +
							tradCategory[i] + '</button><br class="hidden">';
					});
				} else if (typeof objJson.subcat != 'undefined') {
					$.each(objJson.subcat, function (i, oT) {
						var icon = (typeof oT.icon != 'undefined') ? oT.icon : 'angle-right';
						str += '<button class="btn btn-default text-dark col-xs-12 margin-bottom-5 margin-left-15 elipsis hidden keycat keycat-' + k + ' dropDesign" data-categ="' + k + '" data-keycat="' + oT.key + '">' +
							'<div class="checkbox-filter pull-left"><label>' +
							'<input type="checkbox" class="checkbox-info">' +
							'<span class="cr"><i class="cr-icon fa fa-circle"></i></span>' +
							'</label></div>' +
							'<i class="fa fa-' + icon + '"></i> ' + tradCategory[i] + '</button><br class="hidden">';
					});
				}
				$('.dropdown-category .dropdown-menu').html(str);
			});
		}
	},
	showFilters: function () {
		if ($('#listTags').hasClass('hide')) {
			$('#listTags').removeClass('hide');
			$('#dropdown_search').removeClass('col-md-offset-1');
		} else {
			$('#listTags').addClass('hide');
			$('#dropdown_search').addClass('col-md-offset-1');
		}
		$('#listTags').removeClass('hide');
		$('#dropdown_search').removeClass('col-md-offset-1');
	},

	addMultiTagsAndScope: function () {
		directory.multiTagsT = [];
		directory.multiScopesT = [];
		$.each(myMultiTags, function (oTag, oT) {
			if (notEmpty(oTag) && !inArray(oTag, directory.multiTagsT)) {
				directory.multiTagsT.push(oTag);
				//mylog.log(oTag);
				$('#listTags').append('<a class="btn btn-xs btn-link btn-anc-color-blue  text-left w100p favElBtn ' + slugify(oTag) + 'Btn" data-tag="' + slugify(oTag) + '" href="javascript:directory.searchFor("#' + oTag + '")"><i class="fa fa-tag"></i> ' + oTag + '</a><br/>');
			}
		});
		$.each(myScopes.multiscopes, function (oScope, oT) {
			var oScope = oT.name;
			if (notEmpty(oScope) && !inArray(oScope, directory.multiScopesT)) {
				directory.multiScopesT.push(oScope);
				$('#listScopes').append('<a class="btn btn-xs btn-link text-white text-left w100p favElBtn ' + slugify(oScope) + 'Btn" data-tag="' + slugify(oScope) + '" href="javascript:directory.searchFor("' + oScope + '")"><i class="fa fa-tag"></i> ' + oScope + '</a><br/>');
			}
		});
	},

	//show hide parents when empty
	toggleEmptyParentSection: function (parents, tag, children) {
		mylog.log('toggleEmptyParentSection("' + parents + '","' + tag + '","' + children + '")');
		var showAll = true;
		if (tag) {
			$('.favAllBtn').removeClass('active');
			//apply tag filtering
			$(tag + 'Btn').toggleClass('btn-link text-white').toggleClass('active text-white');

			if ($('.favElBtn.active').length > 0) {
				showAll = false;
				var tags = '';
				$.each($('.favElBtn.active'), function (i, o) {
					tags += '.' + $(o).data('tag') + ',';
				});
				tags = tags.replace(/,\s*$/, '');
				mylog.log(tags);
				toggle(tags, children, 1);

				directory.toggleParents(directory.elemClass);
			}
		}

		if (showAll)
			directory.showAll(parents, children);

		$('.my-main-container').scrollTop(0);
	},

	showAll: function (parents, children, path, color) {
		//show all
		if (!color)
			color = 'text-white';
		$('.favElBtn').removeClass('active btn-dark-blue').addClass('btn-link ');
		$('.favAllBtn').addClass('active');
		$(parents).removeClass('hide');
		$(children).removeClass('hide');
	},
	//be carefull with trailing spaces on elemClass
	//they break togglePArents and breaks everything
	toggleParents: function (path) {
		//mylog.log("toggleParents",parents,children);
		$.each(favTypes, function (i, k) {
			if ($(path.trim() + '.' + k).length == $(path.trim() + '.' + k + '.hide ').length)
				$('.' + k + 'fav').addClass('hide');
			else
				$('.' + k + 'fav').removeClass('hide');
		});
	},

	//fait de la recherche client dans les champs demandé
	search: function (parentClass, searchVal) {
		mylog.log('searchDir searchVal', searchVal);
		if (searchVal.length > 2) {
			$.each($(directory.elemClass), function (i, k) {
				var found = null;
				if ($(this).find('.entityName').text().search(new RegExp(searchVal, 'i')) >= 0 ||
					$(this).find('.entityLocality').text().search(new RegExp(searchVal, 'i')) >= 0 ||
					$(this).find('.tagsContainer').text().search(new RegExp(searchVal, 'i')) >= 0) {

					found = 1;
				}

				if (found)
					$(this).removeClass('hide');
				else
					$(this).addClass('hide');
			});

			directory.toggleParents(directory.elemClass);
		} else
			directory.toggleEmptyParentSection(parentClass, null, directory.elemClass, 1);
	},

	searchFor: function (str) {
		$('.searchSmallMenu').val(str).trigger('keyup');
	},
	decomposeDate: function (created, type, onlyString = null) {
		if (onlyString === null)
			onlyString = false;

		const d = new Date(created * 1000);
		const day = moment().day(d.getDay()).local().locale(mainLanguage).format('dddd');
		const [dayNumber, month, year] = moment(d).local().locale(mainLanguage).format('DD MMMM YYYY').split(' ');

		let str = '';
		switch (type) {
			case 'year':
				str = year;
				break;
			case 'month-year':
				str = `${month} ${year}`;
				break;
			case 'dayNumber-month-year':
				str = `${dayNumber} ${month} ${year}`;
				break;
			case 'dayNumber-month':
				str = `${dayNumber} ${month}`;
				break;
			case 'day':
				str = `${day} ${dayNumber}`;
				break;
			case 'dayNumber':
				str = `${dayNumber}`;
				break;
			case 'day-month-year':
				str = `${day} ${month} ${year}`;
				break;
			case 'day-month':
				str = `${day} ${dayNumber} ${month}`;
				break;
			case 'month':
				str = month;
				break;
		}

		if (onlyString !== true) {
			str = `<i class="fa fa-calendar"></i> ${str}`;
			return `<div class="dateUpdated">${str}</div>`;
		}

		return str;
	},
	showDatetimePost: function (collection, id, created, hideIfNbDaySup = null, showDateOnly = null) {
		var str = '';

		formatDateView = "DD MMMM ⋅ HH:MM";
		var d = new Date();
		var current_seconde = Math.floor(new Date().getTime() / 1000);

		var seconde = current_seconde.valueOf() - created.valueOf();
		var hours = Math.floor(seconde / 60 / 60);
		var minutes = Math.floor(seconde / 60);
		var day = Math.floor(hours / 24);

		if (notNull(hideIfNbDaySup) && day > hideIfNbDaySup)
			return '';
		else if (notNull(showDateOnly) && showDateOnly == true) {
			d.setTime(created * 1000);
			str += '<i class="fa fa-calendar"></i> ' + moment().day(d.getDay()).local().locale(mainLanguage).format('dddd') + ' ' + moment(d).local().locale(mainLanguage).format("DD MMMM YYYY");
			return '<div class=\'dateUpdated\'>' + str + '</div>';
		} else {
			if (seconde < 60) {
				str += '<i class="fa fa-clock-o"></i> ' + trad.justnow;
			}
			else if (seconde >= 60 && minutes < 60) {
				str += '<i class="fa fa-clock-o"></i> ' + trad.ago + ' ' + minutes + ' ' + trad.min
			}
			else if (minutes >= 60 && hours < 12) {
				str += '<i class="fa fa-clock-o"></i> ' + trad.ago + ' ' + hours + ' h';
			}
			else if (hours >= 12 && hours < 24) {
				var dd = new Date();
				dd.setTime(dd.getTime());
				d.setTime(created * 1000);
				var today = dd.getDay();
				var day = d.getDay();
				if (today == day) {
					d.setTime(created * 1000);
					str += '<i class="fa fa-clock-o"></i> ' + trad.tod + '. ' + d.getHours() + ':' + d.getMinutes();
				} else {
					d.setTime(created * 1000)
					str += '<i class="fa fa-clock-o"></i> ' + trad.yest + '. ' + d.getHours() + ':' + d.getMinutes();
				}
			}// si created >= 1jour et created < 1semaine
			else if (hours >= 24 && hours < 168) {
				d.setTime(created * 1000);
				var datedate = d.getDate() < 10 ? '0' + d.getDate() : d.getDate();
				str += '<i class="fa fa-clock-o"></i> ' + moment().day(d.getDay()).local().locale(mainLanguage).format('ddd') + ' ' + datedate + ' ⋅ ' + d.getHours() + ':' + d.getMinutes();
			}// si created >= 1 semaine
			else if (hours >= 168) {
				var dd = new Date();
				dd.setTime(dd.getTime());
				d.setTime(created * 1000)
				var now = dd.getFullYear();
				var creat = d.getFullYear();
				//si année craated = année actuel
				if (now == creat) {
					d.setTime(created * 1000);
					str += '<i class="fa fa-clock-o"></i> ' + moment().day(d.getDay()).local().locale(mainLanguage).format('ddd') + ' ' + moment(d).local().locale(mainLanguage).format("DD MMMM ⋅ HH:MM");
				} else {
					d.setTime(created * 1000);
					str += '<i class="fa fa-clock-o"></i> ' + moment().day(d.getDay()).local().locale(mainLanguage).format('ddd') + ' ' + moment().local().locale(mainLanguage).format("DD MMMM YYYY ⋅ HH:MM");
				}
			}
			return '<div class=\'dateUpdated\' data-created=\''+created+'\'>' + str + '</div>';
		}
	},
	get_time_zone_offset: function () {
		var current_date = new Date();
		return -current_date.getTimezoneOffset() / 60;
	},
	getDateFormated: function (params, onlyStr, allInfos) {
		if (typeof params.recurrency != 'undefined' && params.recurrency) {
			return directory.initOpeningHours(params, allInfos);
		} else {
			params.startDateDB = notEmpty(params.startDate) ? params.startDate : null;
			params.startDay = notEmpty(params.startDate) ? moment(params.startDate/*,"YYYY-MM-DD HH:mm"*/).local().locale('fr').format('DD') : '';
			params.startMonth = notEmpty(params.startDate) ? moment(params.startDate/*,"YYYY-MM-DD HH:mm"*/).local().locale('fr').format('MM') : '';
			params.startYear = notEmpty(params.startDate) ? moment(params.startDate/*,"YYYY-MM-DD HH:mm"*/).local().locale('fr').format('YYYY') : '';
			params.startDayNum = notEmpty(params.startDate) ? moment(params.startDate/*,"YYYY-MM-DD HH:mm"*/).local().locale('fr').format('d') : '';
			params.startTime = notEmpty(params.startDate) ? moment(params.startDate/*,"YYYY-MM-DD HH:mm"*/).local().locale('fr').format('HH:mm') : '';

			params.endDateDB = notEmpty(params.endDate) ? params.endDate : null;
			params.endDay = notEmpty(params.endDate) ? moment(params.endDate/*,"YYYY-MM-DD HH:mm"*/).local().locale('fr').format('DD') : '';
			params.endMonth = notEmpty(params.endDate) ? moment(params.endDate/*,"YYYY-MM-DD HH:mm"*/).local().locale('fr').format('MM') : '';
			params.endYear = notEmpty(params.startDate) ? moment(params.endDate/*,"YYYY-MM-DD HH:mm"*/).local().locale('fr').format('YYYY') : '';
			params.endDayNum = notEmpty(params.startDate) ? moment(params.endDate/*,"YYYY-MM-DD HH:mm"*/).format('d') : '';
			params.endTime = notEmpty(params.endDate) ? moment(params.endDate/*,"YYYY-MM-DD HH:mm"*/).local().locale('fr').format('HH:mm') : '';
			params.startDayNum = directory.getWeekDayName(params.startDayNum);
			params.endDayNum = directory.getWeekDayName(params.endDayNum);

			params.startMonth = directory.getMonthName(params.startMonth);
			params.endMonth = directory.getMonthName(params.endMonth);
			params.color = 'orange';


			var startLbl = (params.endDay != params.startDay) ? trad['fromdate'] : '';
			var endTime = (params.endDay == params.startDay && params.endTime != params.startTime) ? ' - ' + params.endTime : '';
			mylog.log('params.allDay', !notEmpty(params.allDay), params.allDay);


			var str = '';
			var dStart = params.startDay + params.startMonth + params.startYear;
			var dEnd = params.endDay + params.endMonth + params.endYear;
			mylog.log('DATEE', dStart, dEnd);

			if (params.startDate != null) {
				if (notNull(onlyStr)) {
					str += '<h4 class="text-bold letter-orange no-margin">';
					if (params.endDate != null && dStart != dEnd)
						str += '<small class="">' + trad.fromdate + '</small> ';
					str += '<span class="letter-' + params.color + '">' + params.startDay + '</span>';
					if (params.endDate == null || dStart == dEnd || (params.startMonth != params.endMonth || params.startYear != params.endYear))
						str += ' <small class="">' + params.startMonth + '</small>';
					if (params.endDate == null || dStart == dEnd || params.startYear != params.endYear)
						str += ' <small class="">' + params.startYear + '</small>';
					if (params.endDate != null && dStart != dEnd)
						str += ' <small class="">' + trad.todate + '</small> <span class="letter-' + params.color + '">' + params.endDay + '</span> <small class="">' + params.endMonth + ' ' + params.endYear + '</small>';
					str += '</h4>';

					str += '<small class="margin-top-5"><b><i class="fa fa-clock-o"></i> ' +
						params.startTime + endTime + '</b></small>';

				} else {
					str += '<h3 class="letter-' + params.color + ' text-bold no-margin" style="font-size:20px;">' +
						'<small>' + startLbl + ' </small>' +
						'<small class="letter-' + params.color + '">' + params.startDayNum + '</small> ' +
						params.startDay + ' ' + params.startMonth +
						' <small class="letter-' + params.color + '">' + params.startYear + '</small>';
					if (params.collection == "events" && notNull(params.startTime)) {
						str += ' <small class="pull-right margin-top-5"><b><i class="fa fa-clock-o margin-left-10"></i> ' +
							params.startTime + '</b></small>';
					}
					str += '</h3>';

				}
			}

			if (params.endDate != null && dStart != dEnd && !notNull(onlyStr)) {
				str += '<h3 class="letter-' + params.color + ' text-bold no-margin" style="font-size:20px;">' +
					'<small>' + trad['todate'] + ' </small>' +
					'<small class="letter-' + params.color + '">' + params.endDayNum + '</small> ' +
					params.endDay + ' ' + params.endMonth +
					' <small class="letter-' + params.color + '">' + params.endYear + '</small>';
				if (params.collection == "events") {
					str += ' <small class="pull-right margin-top-5"><b><i class="fa fa-clock-o margin-left-10"></i> ' +
						params.endTime + '</b></small>';
				}
				str += '</h3>';
			}
			return str;
		}
	},
	isOpenedStr: function (params) {
		var statu = trad.Close;
		var html = '';
		var count = 0;
		var openHour = '';
		var closesHour = '';
		var nameDay = '';
		$.each(params.openingHours, function (i, data) {
			if ((typeof data == 'object' && notNull(data)) || (typeof data == 'string' && data.length > 0)) {
				var dayNum = (i == 6) ? 0 : (i + 1);
				var day = '';
				if (count > 0) html += '';
				var color = 'text-orange hidden';
				var today = new Date();
				var getD = today.getDay()

				//alert(dayNum);
				//
				if ((getD - 1) == i &&
					typeof data.hours[0] != 'undefined' &&
					typeof data.hours[0].opens != 'undefined' &&
					typeof data.hours[0].closes != 'undefined') {
					color = 'text-red';
					openHour = data.hours[0].opens;
					closesHour = data.hours[0].closes;
					statu = trad.Opened;
					nameDay += '<b style="font-variant: small-caps;" class="' + color + '"> ' + moment().day(dayNum).local().locale(mainLanguage).format('dddd') + '</b>';
				}


				count++;
			}

		});
		html += '<span class="uppercase text-green bold no-padding"> ' + statu + ' </span>' + nameDay + '<small class="pull-right margin-top-5"><b><i class="fa fa-clock-o"></i> ' + openHour + '-' + closesHour + '</b></small><a href="javascript:;" class="ssmla text-orange" id="btn-start-detail" data-view="detail"> ' + trad.Seetimetables + '</a>';
		return html;
	},
	initOpeningHours: function (params, allInfos) {
		mylog.log('initOpeningHours', params, allInfos);
		var html = (notNull(allInfos)) ? '<span class=\'ttr-desc-4 bold uppercase" style="padding-left: 15px; padding-bottom: 10px;"\'><i class="fa fa-calendar"></i>&nbsp;' + trad.eachWeeks + '</span>' : '<span class=\'uppercase bold no-padding\'>' + trad.each + ' </span>';
		mylog.log('initOpeningHours contextData.openingHours', params.openingHours);
		if (notNull(params.openingHours)) {
			var count = 0;

			var openHour = '';
			var closesHour = '';
			$.each(params.openingHours, function (i, data) {
				mylog.log('initOpeningHours data', data, data.allDay, notNull(data));
				mylog.log('initOpeningHours notNull data', notNull(data), typeof data, data.length);
				if ((typeof data == 'object' && notNull(data)) || (typeof data == 'string' && data.length > 0)) {
					var day = '';
					var dayNum = (i == 6) ? 0 : (i + 1);
					if (notNull(allInfos)) {
						mylog.log('initOpeningHours data.hours', data.hours);
						day = '<li class="tl-item">';
						day += '<div class="item-title">' + moment().day(dayNum).local().locale(mainLanguage).format('dddd') + '</div>';
						$.each(data.hours, function (i, hours) {
							mylog.log('initOpeningHours hours', hours);
							day += '<div class="item-detail">' + hours.opens + ' : ' + hours.closes + '</div>';
						});
						day += '</li>';
						if (moment().format('dd') == data.dayOfWeek)
							html += day;
						else
							html += day;
					} else {
						if (count > 0) html += ', ';


						var color = 'text-orange';

						if (typeof agenda != 'undefined' && agenda != null &&
							(moment(agenda.getStartMoment(agenda.dayCount)).isoWeekday() - 1) == i &&
							typeof data.hours[i] != 'undefined' &&
							typeof data.hours[i].opens != 'undefined' &&
							typeof data.hours[i].closes != 'undefined') {

							color = 'text-red';
							openHour = data.hours[i].opens;
							closesHour = data.hours[i].closes;
						}


						html += '<b style=\'font-variant: small-caps;\' class=\'' + color + '\'>' + moment().day(dayNum).local().locale(mainLanguage).format('dddd') + '</b>';

					}
					count++;
				}

			});

			html += '<small class="pull-right margin-top-5"><b><i class="fa fa-clock-o"></i> ' + openHour + '-' + closesHour + '</b></small>';
		} else
			html = '<i>' + trad.notSpecified + '</i>';

		return html;
	},
	logos : function(params){ // Logo listing design
		mylog.log("logos", "Params", params);
		let str = '';
		let imageUrl = baseUrl + moduleUrl + "/images/thumbnail-default.jpg";
		if (typeof params.profilMediumImageUrl !== "undefined" && params.profilMediumImageUrl !== "") {
			imageUrl = baseUrl + params.profilMediumImageUrl;
		}
		str += '<div class="col-xs-6 col-sm-4 col-md-3 col-lg-2 padding-10 logo-item">'+
			'<a  href="' + params.hash + '" class="text-center '+params.hashClass+'" style="/*height:100%; display:flex*/">'+
				'<img src="'+imageUrl+'" style="max-width: 100%;display: block;margin: auto;" alt="logo">'+
			'</a>'+
			'</div>';
		if(params.collection == "citoyens"){
			str = '<div class="col-xs-6 col-sm-4 col-md-3 col-lg-2 padding-10 logo-item tooltips" data-emplacement = "center" data-toggle = "tooltip" data-original-title="'+params.name+'"> '+
			'<a  href="' + params.hash + '" class="text-center '+params.hashClass+'" style="/*height:100%; display:flex*/">'+
				'<img src="'+imageUrl+'" style="max-width: 100%;display: block;margin: auto;" alt="logo">'+
			'</a>'+
			'</div>';
		} 

		return str;
	},
	/* VIEW TABLEAU */
	table : { 
		context: {},
		timeoutAddCity : null,
		paramsSearch : {
			text:null,
			page:"",
			types:[]
		},
		panel : {
			table : {
				check:{
					name : "",
					class : "col checkbox-tools check-element" 
				},
				logo : {
					name : "Logo",
					class: "col-xs-1 text-center"
				},
				name: {
					name : "Nom",
					class: ""
				},
				address : { 
					name : "Adresse",
					class: ""
				},
				tags : { 
					name : "Mot clef"
				},
				// created : { 
				// 	name : "Date de création"
				// },
				links : {
					name : ""
				}
			}
		},
		results : {},
		modals : {},
		actionsStr : false,
		setType:[],
		init : function(pInit,fObj){
			var pInit = (typeof pInit !== 'undefined') ? pInit : null;
			//Init variable	
			var copyTable = jQuery.extend(true, {}, directory.table);
			copyTable.initVar(pInit,fObj);
			return copyTable;
	
		},
		initVar : function(pInit,fObj){
			var pInit = (typeof pInit !== 'undefined') ? pInit : null;
			var aObj = this;
			if(typeof fObj.search.obj.doublon && fObj.search.obj.doublon){
				aObj.paramsSearch.doublon = true
			}
			aObj.container = ( ( pInit != null && typeof pInit.container != "undefined" ) ? pInit.container : "dropdown_search" );
		 	aObj.initView();
			aObj.initTable();	
			aObj.events.bind();		
		},
		initView : function(){

			var aObj = this;
			var str = "";
			if(typeof userConnected != "undefined" && userConnected != null && typeof userConnected.roles != "undefined"  && typeof userConnected.roles.superAdmin != "undefined"  && userConnected.roles.superAdmin){
				str += '<div class="btn-table  col-xs-2 col-md-1 col-lg-1 padding-10"><button class="spam-element multiple-spam-element btn disabled" data-id="spam_all"><i class="fa fa-exclamation-circle"></i> Spam</button></div>'
			}
			if(typeof aObj.paramsSearch.doublon == 'undefined' || (typeof aObj.paramsSearch.doublon != 'undefined' && !aObj.paramsSearch.doublon)){
				str += 	'<div class="pageTable col-xs-12 padding-10"></div>';
			}
			str +=	'<div class="panel-body panel-table">'+
				'<div>'+
					'<table style="table-layout: fixed; word-wrap: break-word;" class="table table-striped table-bordered table-hover directoryTable" id="table">'+
						'<thead>'+
							'<tr id="headerTable">'+
							'</tr>'+
						'</thead>'+
						'<tbody class="directoryLines">'+
							'<tr><td colspan="7"></td></tr>'+
						'</tbody>'+
					'</table>'+
				'</div>'+
			'</div>';

			if(typeof aObj.paramsSearch.doublon == 'undefined' || (typeof aObj.paramsSearch.doublon != 'undefined' && !aObj.paramsSearch.doublon)){
				str +='<div class="pageTable col-md-12 col-sm-12 col-xs-12 padding-20"></div>';
			}
	
			$(".bodySearchContainer").html(str);
			coInterface.showLoader(".bodySearchContainer  #table .directoryLines tr td");
		},
		initTable : function(){
			var aObj = this ;
			var str = "" ;
			if(	typeof aObj.panel.table != "undefined" && 
				aObj.panel.table != null ){
				$.each(aObj.panel.table, function(key, value){ 
					if(key != "actions"){
						var classCol = ((typeof value.class != "undefined") ? value.class : " text-center ");
						if(key == "logo"){
							str +="<th class='"+classCol+" fa-2x'><i class='fa fa-image'></i></th>";
						}else if(key =="check"){
							str +=' <th class="'+classCol+'"><input type="checkbox" class="selectAllElements" value="'+id+','+type+'"><span class="countElmSelect"></span></th>';
						}
						else if(value === "true")
							str +="<th>"+key+"</th>";
						else if(typeof value != "undefined" && typeof value.name != "undefined"){
							var sort = "";
							if(typeof value.sort != "undefined" && typeof value.sort.field != "undefined"){
								if(typeof value.sort.up != "undefined")
									sort += "<a href='javascript:;' data-sort='up' data-field='"+value.sort.field+"' class='sortAdmin' > <i class='fa fa-arrow-up'></i> </a>" ;
								if(typeof value.sort.down != "undefined")
									sort += "<a href='javascript:;' data-sort='down' data-field='"+value.sort.field+"' class='sortAdmin' > <i class='fa fa-arrow-down'></i> </a>" ;
							}
							str +="<th class='"+classCol+"'>"+value.name+" "+sort+"</th>";
						}
					}
					
				});
				
			}
			$(".bodySearchContainer #headerTable").append(str) ;
			coInterface.bindLBHLinks();
		},
		initViewTable : function(aObj){
			var sumColumn="";
			var posSumColumn = 0;
			var sum=null;
			var tot="";
			var totalColumn=0;
	
			//vérifie si une entrée du tableau contribut l'attribut sum pour faire la somme
			$.each(aObj.panel.table, function(k, v){
				if($.inArray("sum",Object.keys(v))>-1){
					sumColumn=k;
					posSumColumn=Object.keys(aObj.panel.table).indexOf(k);
				}
			});
	
			if(Object.keys(aObj.results).length > 0){
				if ($(".bodySearchContainer  #table .directoryLines").find('.processingLoader').length > 0) {
					$(".bodySearchContainer  #table .directoryLines").html("");
				}
				$.each(aObj.results ,function(key, values){
					var entry = directory.table.buildDirectoryLine( values, values.collection);
					$(".bodySearchContainer  #table .directoryLines").append(entry);
					if(sumColumn){
						if(typeof values[sumColumn]!="Number")
							sum = Number(values[sumColumn]);
						totalColumn = totalColumn + sum ;
					}	
				});	
				if(sumColumn){
					tot += '<tr style="height: 20px;"></tr>';
					tot += '<tr style="border-top:solid;border-bottom:solid;"><td style="border-left:none !important;border-right:none !important;font-weight:800;">TOTAL</td</tr>';
	
	
					for(var p=0;p<posSumColumn-1;p++){
						tot+='<td style="border-left:none !important;border-right:none !important;"></td>';
					}
					tot += '<td style="border-left:none !important;border-right:none !important;">'+totalColumn+'</td>';
					$(".bodySearchContainer  #table .directoryLines").append(tot);
				}		
			}else{
				$(".bodySearchContainer  #table .directoryLines").html('<tr><td class="text-center" colspan="7">'+trad['noresult']+'</td></tr>');
			}
	
		},
		buildDirectoryLine : function( e, collection, icon ){
			var aObj = this;
			var strHTML="";
			if( typeof e._id == "undefined" || 
				( (typeof e.name == "undefined" || e.name == "") && 
				  (e.text == "undefined" || e.text == "") ) )
				return strHTML;
			var actions = "";
			var classes = "";
			var id = e._id.$id;
			var status=[];
			strHTML += '<tr id="entity_'+e.collection+'_'+id+'" class="'+e.collection+' line selectElm">';
				strHTML += "<a>"+aObj.columTable(e, id, e.collection)+"</a>";
			strHTML += '</tr>';
			return strHTML;
		},
		columTable : function(e, id, collection){
			var str = "" ;
			var aObj = this ;
			if(	typeof aObj.panel.table != "undefined" && 
				aObj.panel.table != null ){
				$.each(aObj.panel.table, function(key, value){
					if(key != "actions"){
						str += '<td class="center '+key+' '+value["class"] +' ">';
						if(typeof aObj.values[key] != "undefined" && value === "true") {
							str += aObj.values[key](e, id, collection, aObj);
						} else if(typeof aObj.values[key] != "undefined") {
							str += aObj.values[key](e, id, collection, aObj, value);
						}
						str += '</td>';
					}
				});
			}
			directory.table.events.bind();	
			directory.bindBtnElement();
			return str ;
		},
		values : {
			check : function(e, id, type, aObj, params){
				str =' <input type="checkbox" class="selectElements" value="'+id+','+type+'">';
				return str;
			},
			type : function(e, id, type, aObj, params){
				//mylog.log(e);
				var img = "";
				if (e && typeof e.profilThumbImageUrl != "undefined" && e.profilThumbImageUrl!="")
					img = '<img width="50" height="50" alt="image" class="img-circle" src="'+baseUrl+e.profilThumbImageUrl+'"><br/><span class="uppercase bold">'+type+"</span>";
				else{
					var typology= (typeof e.type !="undefined") ? e.type : type ;
					img = '<i class="fa '+icon+' fa-2x"></i> <br/><span class="uppercase bold">'+typology+"</span>";
				}	
				var lbhN = "lbh" ;
				var urlH='#page.type.'+type+'.id.'+id;
				var targetLink="target='_blank'";
				if(typeof params.preview !="undefined" && params.preview){
					lbhN =  "lbh-preview-element";
					targetLink="";
				}
				if(typeof params.notLink !="undefined" && params.notLink){
					lbhN="";
					urlH="javascript:;";
					targetLink="";
				}
				var str = 	'<a href="'+urlH+'" class="col-xs-12 no-padding text-center '+lbhN+'" '+targetLink+'>'+
								img +
							'</a>';
				return str;
			},
			logo : function(e, id, type, aObj, params){
				var img = "";
				if (e && typeof e.profilImageUrl != "undefined" && e.profilImageUrl!="")
					img = '<img height="50" width="50" alt="imageTable" class="img-table" src="'+baseUrl+e.profilImageUrl+'">';
				else{
					var typology= (typeof e.type !="undefined") ? e.type : type ;
					img = '<i class="fa fa-image fa-2x"></i> ';
					//<br/><span class="uppercase bold">'+typology+"</span>
				}	
				var lbhN = "lbh" ;
				var urlH='#page.type.'+type+'.id.'+id;
				var targetLink="target='_blank'";
				if(typeof params.preview !="undefined" && params.preview){
					lbhN =  "lbh-preview-element";
					targetLink="";
				}
				if(typeof params.notLink !="undefined" && params.notLink){
					lbhN="";
					urlH="javascript:;";
					targetLink="";
				}
				var str = 	'<a href="'+urlH+'" class="col-xs-12 no-padding text-center '+lbhN+'" '+targetLink+'>'+
								img +
							'</a>';
				return str;
			},
			name : function(e, id, type, aObj, params){
				var str = "";
				if(type == "answers"){
					var title="Réponse vide";
					if(typeof e.mappingValues != "undefined" && typeof e.mappingValues.name != "undefined" && typeof e.mappingValues.name != null)
						title=e.mappingValues.name;
					str = '<a href="#answer.index.id.'+id+'" class="" target="_blank">'+title+'</a>';
				} else if (type == "forms"){
					str = '<a href="#form.edit.id.'+id+'" class="" target="_blank">'+e.name+'</a>';
				} else {
					var title;
					if(typeof e.name != "undefined")
						title=e.name;
					else if(typeof e.text != "undefined")
						title=e.text;
					else if(typeof e.title != "undefined")
						title=e.title;
					
					//var lbhN = "lbh" ;
					var lbhN = "" ;
					var urlH='#page.type.'+type+'.id.'+id;
					var lbhN="lbh";
					// var targetLink="target='_blank'";
					if(typeof e.preview !="undefined" && e.preview){
						targetLink="";
						lbhN =  "lbh-preview-element";
					}
					if(type == "poi"){
						lbhN =  "lbh-preview-element";
					}
					if(typeof e.notLink !="undefined" && e.notLink){
						lbhN="";
						targetLink="";
						urlH="javascript:;";
					}
					
					str = '<a href="'+urlH+'" class="'+lbhN+'">'+title+'</a>';
				}
				
				return str;
			},
			tags : function(e, id, type, aObj, className){
				//mylog.log(e);
				var str="";
				// if(notEmpty(e.tags)){
				// 	$.each(e.tags, function(e,v){
				// 		tagsStr+="<span class='badge bg-white text-red shadow2'>"+v+"</span>";
				// 	});
				// }
				var thisTags = '';
				var countTagsInClass = 0;
				var countFourTags0nly = 1;
				if (typeof e.tags != 'undefined' && e.tags != null) {
					$.each(e.tags, function (key, value) {
						if (typeof value != 'undefined' && value != '' && value != 'undefined') {
							var tagTrad = typeof tradCategory[value] != 'undefined' ? tradCategory[value] : value;
							//thisTags += '<li class="tag" data-tag-value="'+slugify(value, true)+'" data-tag-label="'+tagTrad+'">#' + tagTrad + '</li> ';
							if (countFourTags0nly < 5) {
								thisTags += '<a href="javascript:;" class="btn-tag-panel" data-tag="' + tagTrad + '"><span class="badge bg-transparent btn-tag tag commonClassTags" data-tag-value="' + slugify(value, true) + '" data-tag-label="' + tagTrad + '">#' + tagTrad + '</span> </a>';
								countFourTags0nly++;
							} else {
								thisTags += '<a href="javascript:;" class="btn-tag-panel btn-tags-hidden hidden" data-tag="' + tagTrad + '"><span class="badge bg-transparent btn-tag tag commonClassTags" data-tag-value="' + slugify(value, true) + '" data-tag-label="' + tagTrad + '">#' + tagTrad + '</span> </a>';
							}
							if (countTagsInClass < 6) {
								e.containerClass += slugify(value, true) + ' ';
								countTagsInClass++;
							}
						}
					});
					if (countFourTags0nly > 4 && (e.tags.length - 4) != 0) {
						thisTags += '<a href="javascript:;" class="badge bg-transparent btn-tag tag numberTag commonClassTags">+ ' + (e.tags.length - 4) + '</a> ';
					}
					e.tagsHtml = thisTags;
				}
				str += directory.tagsConfigAppend (e.tagsHtml);
				//var str = '<div class="'+(typeof className == "string" ? className : "")+'">'+tagsStr+'</div>';
				return str;
			},	
			address : function(e, id, type, aObj){
				var localityHtml="",streetAddress='', postalCode = '', city='',str='';
				if (e.address != null) {
					streetAddress= e.address.streetAddress ? e.address.streetAddress : '';
					postalCode = e.address.postalCode ? e.address.postalCode : '';
					city = e.address.addressLocality ? e.address.addressLocality : '' ;
				}
				localityHtml = (e.streetAddress != '' || e.postalCode!='' || e.city !='') ? streetAddress+' '+postalCode+' '+city : trad.UnknownLocality;
				
				var lbhN = "" ;
				var urlH='#page.type.'+type+'.id.'+id;
				var targetLink="target='_blank'";
				if(typeof e.preview !="undefined" && e.preview){
					targetLink="";
					lbhN =  "lbh-preview-element";
				}
				if(typeof e.notLink !="undefined" && e.notLink){
					lbhN="";
					targetLink="";
					urlH="javascript:;";
				}
				
				str = '<a href="'+urlH+'" class="'+lbhN+'" '+targetLink+'>'+localityHtml+'</a>';
				return str;
					
	
			},
			created : function(e , id, type, aObj){
				mylog.log("createdddd", moment(moment(new Date(e.created * 1000).toISOString()), "DD/MM/YYYY").format("DD/MM/YYYY à HH:mm")); 
				var str = "";
				str += moment(moment(new Date(e.created * 1000).toISOString()), "DD/MM/YYYY").format("DD/MM/YYYY");
				return str;
			},
			links : function(e , id, type, aObj){
				var str = "";
				str += directory.countLinksHtml(e);
				return str;
			}
		},
		actions : {
			spam : function(e){
				var elements = {};
				$('.selectElements:checked').each(function () {
					var value = $(this).val().split(",");
					elements[value[0]] = {
						id : value[0],
						type : value[1],
						ipspam : []
					}
					
				}) 
				if(Object.keys(elements).length > 0){
					$.each(elements, function(ke,ve){	
						var elementToSpam = {
							"ipspam" : ve.ipspam,
							"id_element" : ve.id,
							"type" : ve.type
						};
						directory.spamElement(elementToSpam);
					})
					toastr.success("spam avec succès")

				}
			}
		},
		events : {			
			bind : function(){ 
				$(".selectAllElements").off().on("click", function(e) { 
					var allChecked = $('.selectElements:checked').length == $('.selectElements').length;
					$('.selectElements').prop("checked", !allChecked);
					$('.selectAllElements').prop("checked", !allChecked);
					if($(".checkbox-tools .selectElements:checked").length > 0){
						$('.countElmSelect').html( $('.selectElements:checked').length);
					}else{
						$('.countElmSelect').html("");
					}
					
					if($(".checkbox-tools .selectElements:checked").length > 0)
						$(".multiple-spam-element").removeClass("disabled");
					else
						$(".multiple-spam-element").addClass("disabled");
				});
				$(".spam-element").off().on("click",function(e){
					directory.table.actions.spam(e);
				});
				$('.check.check-element').off().on('click', function(event) {
					if (!$(event.target).is('input[type="checkbox"]')) {
						var checkElm = $(this).find('input[type="checkbox"]'); 
						checkElm.prop('checked', !checkElm.prop('checked'));
						if($(".checkbox-tools .selectElements:checked").length > 0){
							$('.countElmSelect').html( $('.selectElements:checked').length);
						}else{
							$('.countElmSelect').html("");
						}					
						if($(".checkbox-tools .selectElements:checked").length > 0)
							$(".multiple-spam-element").removeClass("disabled");
						else
							$(".multiple-spam-element").addClass("disabled");
						}
				});
				$(".selectElements").off().on("click",function(){
					if($(".checkbox-tools .selectElements:checked").length > 0){
						$('.countElmSelect').html( $('.selectElements:checked').length);
					}else{
						$('.countElmSelect').html("");
					}					
					if($(".checkbox-tools .selectElements:checked").length > 0)
						$(".multiple-spam-element").removeClass("disabled");
					else
						$(".multiple-spam-element").addClass("disabled");
				});
			}
		}
	}

};

