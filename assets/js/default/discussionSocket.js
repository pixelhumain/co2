discussionSocket = {
	socketId: "",
    init : function() {
		wsCOpubSub.subscriber(function(wsCO){
			wsCO
            .on('connect', (data) => {
                if (typeof coWsData != "undefined") {
                    coWsData = $.extend(true, {}, wsCO)
                }
            })
            .on("user_leave_discussion", (data) => {
                if (data.userId) {
                    discussionSocket.userState.userDisonnected(data.discussionId ? data.discussionId : null, data.userId)
                }
            })
            .on('update_discussion_state', function(data) {
                if (data && data.discussionId && discussionComments.params && discussionComments.params.answer && discussionComments.params.answer._id && data.discussionId == discussionComments.params.answer._id.$id) {
                    if (data.state && data.state.discussionRoomMembers && !data.userId) {
                        $.each(data.state.discussionRoomMembers, function(userIndex, userValue) {
                            if (notEmpty(userValue)) {
                                discussionSocket.userState.userConnected(data.discussionId, userIndex, userValue.name, userValue.userData, data.params ? data.params : {})
                            }
                        })
                    } else if (data.userId) {
                        discussionSocket.userState.userDisonnected(data.discussionId, data.userId)
                    }
                }
            })
			.on('change_comment', function(data) {
                // if (data && data.discussionId && discussionComments.params && discussionComments.params.answer && discussionComments.params.answer._id && data.discussionId == discussionComments.params.answer._id.$id) {
                if (data && data.discussionId) {
                    if (data.params && data.params.onUpdate) {
                        if (typeof data.params.onUpdate == "object") {
                            $.each(data.params.onUpdate, function(onIndex, onValue) {
                                if (typeof discussionSocket.socketListener[onValue] == "function") {
                                    discussionSocket.socketListener[onValue](data.discussionId, data.params)
                                }
                            })
                        } else if (typeof discussionSocket.socketListener[data.params.onUpdate] == "function") {
                            discussionSocket.socketListener[data.params.onUpdate](data.discussionId, data.params)
                        }
                    }
                }
            })
            .on('update_user_activity', function(data) {
                if (data && data.discussionId && discussionComments.params && discussionComments.params.answer && discussionComments.params.answer._id && data.discussionId == discussionComments.params.answer._id.$id) {
                    if (data.state && data.state.discussionRoomMembers) {
                        if (data.userId) {
                            discussionSocket.socketListener[data.params.onUpdate](data.discussionId, data.userId, data.params)
                        }
                    }
                }
            })
		})
	},
	emitEvent: function(wsCO, action, params = {}) {
		if (coWsConfig.enable  && userConnected && ((typeof discussionComments != "undefined" && discussionComments.params.answer && discussionComments.params.answer._id) || params.discuAnsId)) {
			const discuId = typeof discussionComments != "undefined" && discussionComments.params.answer && discussionComments.params.answer._id ? discussionComments.params.answer._id.$id : params.discuAnsId;
            wsCO.emit("discussion_event", {
				action: action,
				params: params,
                discussionId: discuId,
				connectedUserData: {
                    profilThumbImageUrl: userConnected.profilThumbImageUrl ? userConnected.profilThumbImageUrl : modules.co2.url + "/images/thumb/default_citoyens.png" 
                }
			})
		}
	},
    userState: {
        userConnected : function(discussionId, connectedUserId, userName, userData, params){
            if (params && params.disableStatus) {
                return;
            }
            if (!discussionComments.members.active[discussionId]) {
                discussionComments.members.active[discussionId] = {}
            }
            if (!discussionComments.members.active[discussionId][connectedUserId]) {
                discussionComments.members.active[discussionId][connectedUserId] = {
                    id: connectedUserId,
                    name: userName,
                    currentActivity: "inactive",
                    profilThumbImageUrl: userData.profilThumbImageUrl,
                    htmlContent : `
                        <a href="javascript:" class="user-bulle" id="user-${connectedUserId}">
                            <img src="${userData.profilThumbImageUrl}" alt="${ (typeof discussionComments.common.getInitials == "function" ? discussionComments.common.getInitials(userName ? userName : "user connected") : "US") }">
                        </a>
                    `
                };
            }
			discussionComments.members.onlineCounter[discussionId] = Object.keys(discussionComments.members.active[discussionId]).length;			
            if ($(".discussions-content .connected-users #user-"+connectedUserId).length < 1 && connectedUserId != userId) {
                if (discussionComments.members.active[discussionId] && discussionComments.members.active[discussionId][connectedUserId] && discussionComments.members.active[discussionId][connectedUserId].htmlContent) {
                    $(".discussions-content .connected-users").append(discussionComments.members.active[discussionId][connectedUserId].htmlContent)
                }
            }
		},
        userDisonnected : function(discussionId, connectedUserId){
            if (discussionId && discussionComments.members && discussionComments.members.active && discussionComments.members.active[discussionId]) {
                if (discussionComments.members.active[discussionId][connectedUserId]) {
                    delete discussionComments.members.active[discussionId][connectedUserId]
                }
                discussionComments.members.onlineCounter ? discussionComments.members.onlineCounter[discussionId] = Object.keys(discussionComments.members.active[discussionId]).length : "";
            } else if (discussionComments.members.active) {
                $.each(discussionComments.members.active, function(discussionIndex, discussionValue) {
                    if (discussionValue[connectedUserId]) {
                        delete discussionComments.members.active[discussionIndex][connectedUserId]
                    }
                    discussionComments.members.onlineCounter ? discussionComments.members.onlineCounter[discussionIndex] = Object.keys(discussionComments.members.active[discussionIndex]).length : "";			
                })
            }
            $(".discussions-content .connected-users #user-"+connectedUserId).remove();
            $(`.discussions-content .panel-left-content .questions-list .rubric-connected-users #user-${connectedUserId}`).remove()
		}
    },
    socketListener: {
        newComment: function(discussionId, params) {
            if (discussionId) {
                const isSameDiscu = discussionComments.params && discussionComments.params.answer && discussionComments.params.answer._id && discussionComments.params.answer._id.$id == discussionId;
                if (isSameDiscu && typeof discussionComments.common.refreshDiscussionCommentsParamsData == "function" && params.newCommentData) {
                    discussionComments.common.refreshDiscussionCommentsParamsData(params.newCommentData, discussionId)
                    //========== update new comment pastille on left menu of the discussion =============//
                    if (typeof discussionComments != "undefined") {
                        const thisElem = $(".discussions-content .panel-left-content .questions-list");
                        if (thisElem.length > 0) {
                            var currentActive = "";
                            const activeElem = thisElem.find(".discuLi.active");
                            if (thisElem.find(".discuLi.active").hasClass("allComments")) {
                                currentActive = "allComments";
                            } else if (thisElem.find(".discuLi.active").attr("data-inputkey")) {
                                currentActive = thisElem.find(".discuLi.active").attr("data-inputkey");
                            }
                            if (discussionComments.params.newComment && typeof showOneComment != "undefined") {
                                const isAnswer = notEmpty(params.newCommentData.parentId);
                                const argval = params.newCommentData.argval ? params.newCommentData.argval : "";
                                const mentionsArray = params.newCommentData.mentionsArray ? params.newCommentData.mentionsArray : null;
                                if (
                                    currentActive == "allComments" ||
                                    (
                                        activeElem.attr('data-path') &&
                                        activeElem.attr('data-path') == params.newCommentData.path
                                    )
                                ) {
                                    if (isAnswer && $("#comments-list-"+params.newCommentData.parentId + " > .item-comment").length == 0) {
                                        appendAnswerCommentContent(params.newCommentData.parentId, params.newCommentData.parentId, params.newCommentData.contextType, params.newCommentData.path)
                                    }
                                    showOneComment(params.newCommentData, params.newCommentData.parentId, isAnswer, params.commentId, argval, mentionsArray, {ifNbDaySup: 2});
                                    intervalTimerBreak.commentShownTimer = $.extend(true, {}, intervalTimerBreak.default);
                                    var commentShownInterval = setInterval(() => {
                                        if ($("#item-comment-" + params.commentId).length > 0) {
                                            clearInterval(commentShownInterval);
                                            delete intervalTimerBreak.commentShownTimer
                                            if (discussionComments.common.updateAnswerBtnNb) {
                                                discussionComments.common.updateAnswerBtnNb("#footer-comments-"+params.newCommentData.parentId, params.newCommentData.parentId)
                                            }
                                            if (typeof discussionComments != "undefined" && typeof discussionComments.common != "undefined" && typeof discussionComments.common.highlightComment != "undefined") {
                                                if ($("#item-comment-" + params.commentId).is(":visible")) {
                                                    $(`[id*="footer-comments-"] a.lblComment, [class*="link-show-more-"] a`).addClass("alreadyclicked")
                                                    discussionComments.common.highlightComment(params.commentId, params.commentId, false)
                                                } else {
                                                    discussionComments.common.highlightComment(params.commentId, params.commentId, false, true)
                                                }
                                            }
                                        } else if ( intervalTimerBreak.commentShownTimer && intervalTimerBreak.commentShownTimer.timer > intervalTimerBreak.commentShownTimer.requestTimeout) {
                                            clearInterval(commentShownInterval);
                                            delete intervalTimerBreak.commentShownTimer
                                        } else {
                                            intervalTimerBreak.commentShownTimer && intervalTimerBreak.commentShownTimer.timer ? intervalTimerBreak.commentShownTimer.timer++ : clearInterval(commentShownInterval);
                                        }
                                    }, 700);
                                }
                            }
                            thisElem.html(discussionComments.common.buildCommentedQuestionList(typeof discussionComments.params.orderedQuestion != "undefined" && notEmpty(discussionComments.params.orderedQuestion) ? discussionComments.params.orderedQuestion : {}, false, currentActive));
                            discussionComments.common.bindDiscussionContentEvent()
                        }
                        if ($(".aac-tl-comments").length > 0) {
                            var newCommentsCount = discussionComments.params && discussionComments.params.toHighlightComments && discussionComments.params.toHighlightComments.allComments ? discussionComments.params.toHighlightComments.allComments.length : 0;
                            if (discussionComments.params && discussionComments.params.toHighlightComments) {
                                const exludeKeys = ["allComments"];
                                $.each(discussionComments.params.toHighlightComments, function(rubricKey, rubricVal) {
                                    if (exludeKeys.indexOf(rubricKey) == -1 && discussionComments.params.toHighlightComments[rubricKey]) {
                                        newCommentsCount += discussionComments.params.toHighlightComments[rubricKey].length
                                    }
                                })
                            }
                            var hasNotSeenComment = false;
                            if (typeof discussionComments.params.allDiverNotSeenComments != "undefined" && discussionComments.params.allDiverNotSeenComments > 0) {
                                $(".aac-tl-comments").addClass("has-not-seen-comments").attr("data-not-seen-comments", newCommentsCount)
                                $(".aac-tl-comments .edite-txt").attr("data-not-seen-comments", newCommentsCount)
                                hasNotSeenComment = true;
                            } else {
                                $(".aac-tl-comments").removeClass("has-not-seen-comments").removeAttr("data-not-seen-comments")
                                $(".aac-tl-comments .edite-txt").removeAttr("data-not-seen-comments")
                            }
                            if (typeof discussionComments.params.orderedQuestion != "undefined" && notEmpty(discussionComments.params.orderedQuestion)) {
                                var iteration = 0
                                $.each(discussionComments.params.orderedQuestion, (stepIndex, stepVal) => {
                                    if (stepVal.inputs) {
                                        $.each(stepVal.inputs, (inputIndex, inputVal) => {
                                            if (typeof inputVal?.notSeenComments != "undefined" && inputVal.notSeenComments > 0) {
                                                hasNotSeenComment = true;
                                            }
                                        })
                                    }
                                    if (iteration == Object.keys(discussionComments.params.orderedQuestion).length - 1 && hasNotSeenComment) {
                                        $(".aac-tl-comments").addClass("has-not-seen-comments").attr("data-not-seen-comments", newCommentsCount)
                                        $(".aac-tl-comments .edite-txt").attr("data-not-seen-comments", newCommentsCount)
                                    } else {
                                        $(".aac-tl-comments").removeClass("has-not-seen-comments").removeAttr("data-not-seen-comments")
                                        $(".aac-tl-comments .edite-txt").removeAttr("data-not-seen-comments")
                                    }
                                    iteration++;
                                })
                            }
                        }
                    }
                }
                if ($("#propItem"+discussionId).is(":visible")) {
                    if (typeof aapObj != "undefined" && typeof aapObj.directory != "undefined") {
                        aapObj.directory.reloadAapMiniItem(aapObj, discussionId)
                    }
                }
            }
        },
        deleteComment: function(discussionId, params) {
            const isSameDiscu = discussionComments.params && discussionComments.params.answer && discussionComments.params.answer._id && discussionComments.params.answer._id.$id == discussionId;
            if (isSameDiscu && params.deletedCommentId && (".discussions-content #item-comment-" + params.deletedCommentId).length > 0) {
                $("#item-comment-"+params.deletedCommentId).remove();
                if (discussionComments.common.updateAnswerBtnNb && params.parentCommentId) {
                    discussionComments.common.updateAnswerBtnNb("#footer-comments-" + params.parentCommentId, params.parentCommentId)
                }
            }
            if ($("#propItem"+discussionId).is(":visible")) {
                if (typeof aapObj != "undefined" && typeof aapObj.directory != "undefined") {
                    aapObj.directory.reloadAapMiniItem(aapObj, discussionId)
                }
            }
        },
        addVote: function (discussionId, params) {
            const isSameDiscu = discussionComments.params && discussionComments.params.answer && discussionComments.params.answer._id && discussionComments.params.answer._id.$id == discussionId;
            if ($(".discussions-content").length < 1) {
                return;
            }
            if (isSameDiscu && typeof callbackVote == "function" && params && params.typeObj && params.idObj && params.voteStatus && params.userId) {
                callbackVote(params.typeObj, params.idObj, params.voteStatus, params.userId)
            }
        },
        removeVote: function (discussionId, params) {
            const isSameDiscu = discussionComments.params && discussionComments.params.answer && discussionComments.params.answer._id && discussionComments.params.answer._id.$id == discussionId;
            if ($(".discussions-content").length < 1) {
                return;
            }
            if (isSameDiscu && typeof removeVote == "function" && params && params.typeObj && params.idObj && params.voteStatus && params.userId) {
                removeVote(params.typeObj, params.idObj, params.voteStatus, false, params.userId)
            }
        },
        userWriting: function(discussionId, currentUserId, params) {
            const isSameDiscu = discussionComments.params && discussionComments.params.answer && discussionComments.params.answer._id && discussionComments.params.answer._id.$id == discussionId;
            if (!isSameDiscu) {
                return;
            }
            if (discussionComments.members.active[discussionId] && discussionComments.members.active[discussionId][currentUserId]) {
                const currentActiveUser = discussionComments.members.active[discussionId][currentUserId];
                discussionComments.members.active[discussionId][currentUserId].currentActivity = params.userActivity;
                discussionComments.members.active[discussionId][currentUserId].htmlContent = `
                        <a href="javascript:" class="user-bulle ${ params.userActivity && params.userActivity == "writing" ? "writing" : ""}" id="user-${currentUserId}">
                            <img src="${discussionComments.members.active[discussionId][currentUserId].profilThumbImageUrl}" alt="${ (typeof discussionComments.common.getInitials == "function" ? discussionComments.common.getInitials(currentActiveUser.name ? currentActiveUser.name : "user connected") : "US") }">
                        </a>
                    `
            }
            if (params.userActivity && params.userActivity == "writing") {
                if ($(`.discussions-content .panel-left-content .questions-list .discuLi[data-path="${ params.rubric }"]  .rubric-connected-users #user-${currentUserId}`).length < 1) {
                    if (discussionComments.members.active[discussionId] && discussionComments.members.active[discussionId][currentUserId] && discussionComments.members.active[discussionId][currentUserId].htmlContent) {
                        $(`.discussions-content .panel-left-content .questions-list .discuLi[data-path="${ params.rubric }"] .rubric-connected-users`).append(discussionComments.members.active[discussionId][currentUserId].htmlContent)
                    }
                } else if ($(`.discussions-content .panel-left-content .questions-list .discuLi[data-path="${ params.rubric }"] .rubric-connected-users #user-${currentUserId}`).length > 0) {
                    $(`.discussions-content .panel-left-content .questions-list .discuLi[data-path="${ params.rubric }"] .rubric-connected-users #user-${currentUserId}`).replaceWith(discussionComments.members.active[discussionId][currentUserId].htmlContent)
                }
            } else {
                // if ($(`.discussions-content .panel-left-content .questions-list .discuLi[data-path="${ params.rubric }"] .rubric-connected-users #user-${currentUserId}`).length > 0) {
                    $(`.discussions-content .panel-left-content .questions-list .rubric-connected-users #user-${currentUserId}`).remove()
                // }
            }
            if ($(".discussions-content .connected-users #user-"+currentUserId).length < 1 && currentUserId != userId) {
                if (discussionComments.members.active[discussionId] && discussionComments.members.active[discussionId][currentUserId] && discussionComments.members.active[discussionId][currentUserId].htmlContent) {
                    $(".discussions-content .connected-users").append(discussionComments.members.active[discussionId][currentUserId].htmlContent)
                }
            } else if ($(".discussions-content .connected-users #user-"+currentUserId).length > 0 && discussionComments.members.active[discussionId] && discussionComments.members.active[discussionId][currentUserId] && discussionComments.members.active[discussionId][currentUserId].htmlContent) {
                $(".discussions-content .connected-users #user-"+currentUserId).replaceWith(discussionComments.members.active[discussionId][currentUserId].htmlContent)
            }
        },
        newDiscussionGroup: function (discussionId, params) {
            const isSameDiscu = discussionComments.params && discussionComments.params.answer && discussionComments.params.answer._id && discussionComments.params.answer._id.$id == discussionId;
            if (isSameDiscu && params.newDiscuGroupData && params.newDiscuGroupData.privilege) {
                var showNewDiscuCategory = false;
                if (params.newDiscuGroupData.privilege == "private") {
                    if (isInterfaceAdmin || isSuperAdmin) {
                        showNewDiscuCategory = true;
                    }
                }
                var userCanAccessPrivateDiscu = false;
                if (
                    typeof discussionComments.common.checkIfUserCanAccesPrivateDiscu != "undefined" &&
                    discussionComments.params.referentUsersPath &&
                    discussionComments.params.answer && discussionComments.params.answer
                ) {
                    userCanAccessPrivateDiscu = discussionComments.common.checkIfUserCanAccesPrivateDiscu(discussionComments.params.referentUsersPath, discussionComments.params.answer)
                }
                if (params.newDiscuGroupData.privilege == "authorAdmin") {
                    if (
                        (
                            userId && discussionComments.params.answer && discussionComments.params.answer.user &&
                            userId == discussionComments.params.answer.user
                        ) ||
                        isInterfaceAdmin || isSuperAdmin ||
                        userCanAccessPrivateDiscu
                    ) {
                        showNewDiscuCategory = true;
                    }
                } else if (params.newDiscuGroupData.privilege == "authorAll") {
                    showNewDiscuCategory = true;
                }
                const thisElem = $(".discussions-content .panel-left-content .questions-list");
                if (showNewDiscuCategory && params.newDiscuGroupData.stepId && (thisElem.length > 0 || $(".aac-tl-comments").length > 0)) {
                    discussionComments.params.orderedQuestion[params.newDiscuGroupData.stepId] = {
                        id: params.newDiscuGroupData.id,
						name: params.newDiscuGroupData.name ? params.newDiscuGroupData.name : "",
						formParent: discussionComments.params.parentForm && discussionComments.params.parentForm._id ? discussionComments.params.parentForm._id.$id : null,
						step: params.newDiscuGroupData.stepId,
						"_id": { '$id': params.newDiscuGroupData.id },
                        inputs: {
                            [params.newDiscuGroupData.id]: {
                                label: params.newDiscuGroupData.name,
                                showAnyway: true,
                                commentPath: params.newDiscuGroupData.path
                            }
                        }
                    }
                    discussionComments.params.steps[params.newDiscuGroupData.stepId] = {
						[params.newDiscuGroupData.id]: {
							id: params.newDiscuGroupData.id,
							name: params.newDiscuGroupData.name,
							formParent: discussionComments.params.parentForm && discussionComments.params.parentForm._id ? discussionComments.params.parentForm._id.$id : null,
							step: params.newDiscuGroupData.stepId,
							"_id": { '$id': params.newDiscuGroupData.id },
							inputs: {
								[params.newDiscuGroupData.id]: {
									label: params.newDiscuGroupData.name,
									showAnyway: true,
									commentPath: params.newDiscuGroupData.path
								}
							}
						}
					}
                    var currentActive = "";
                    if (thisElem.find(".discuLi.active").hasClass("allComments")) {
                        currentActive = "allComments";
                    } else if (thisElem.find(".discuLi.active").attr("data-inputkey")) {
                        currentActive = thisElem.find(".discuLi.active").attr("data-inputkey");
                    }
                    thisElem.html(discussionComments.common.buildCommentedQuestionList(typeof discussionComments.params.orderedQuestion != "undefined" && notEmpty(discussionComments.params.orderedQuestion) ? discussionComments.params.orderedQuestion : {}, false, currentActive));
                    discussionComments.common.bindDiscussionContentEvent()
                }
            }
        },
        updateDiscussionGroup: function (discussionId, params) {
            const discuListElem = $(".discussions-content .panel-left-content .questions-list")
            const isSameDiscu = discussionComments.params && discussionComments.params.answer && discussionComments.params.answer._id && discussionComments.params.answer._id.$id == discussionId;
            if (!isSameDiscu) {
                return;
            }
            if ((discuListElem.length > 0 || $(".aac-tl-comments").length > 0) && params && params.discuGroupData && params.discuGroupData.stepId && params.discuGroupData.discuPath && params.discuGroupData.name) {
                const currentStepId = params.discuGroupData.stepId;
                const discuPath = params.discuGroupData.discuPath;
                if (currentStepId && discussionComments.params.steps[currentStepId] && discussionComments.params.steps[currentStepId][discuPath]) {
                    discussionComments.params.steps[currentStepId][discuPath].name = params.discuGroupData.name
                }
                if (discussionComments.params.orderedQuestion && discussionComments.params.orderedQuestion[currentStepId] && discussionComments.params.orderedQuestion[currentStepId].inputs && discussionComments.params.orderedQuestion[currentStepId].inputs[discuPath]) {
                    discussionComments.params.orderedQuestion[currentStepId].inputs[discuPath].label = params.discuGroupData.name
                }
            }
            if (discuListElem.length > 0) {
                var currentActive = "";
                if (discuListElem.find(".discuLi.active").hasClass("allComments")) {
                    currentActive = "allComments";
                } else if (discuListElem.find(".discuLi.active").attr("data-inputkey")) {
                    currentActive = discuListElem.find(".discuLi.active").attr("data-inputkey");
                }
                discuListElem.html(discussionComments.common.buildCommentedQuestionList(typeof discussionComments.params.orderedQuestion != "undefined" && notEmpty(discussionComments.params.orderedQuestion) ? discussionComments.params.orderedQuestion : {}, false, currentActive));
                discussionComments.common.bindDiscussionContentEvent()
            }
        },
        deleteDiscussionGroup: function (discussionId, params) {
            const discuListElem = $(".discussions-content .panel-left-content .questions-list")
            const isSameDiscu = discussionComments.params && discussionComments.params.answer && discussionComments.params.answer._id && discussionComments.params.answer._id.$id == discussionId;

            if (isSameDiscu && (discuListElem.length > 0 || $(".aac-tl-comments").length > 0) && params && params.discuGroupData && params.discuGroupData.stepId && params.discuGroupData.discuPath) {
                const currentStepId = params.discuGroupData.stepId;
                const discuPath = params.discuGroupData.discuPath;
                const discuCommentPath = params.discuGroupData.discuCommentPath;
                var currentActive = "";
                const deletedDiscu = discuListElem.find(`[data-path="${discuCommentPath}"]`);
                if (currentStepId && discussionComments.params.steps[currentStepId] && discussionComments.params.steps[currentStepId][discuPath]) {
                    delete discussionComments.params.steps[currentStepId][discuPath]
                }
                if (discussionComments.params.orderedQuestion && discussionComments.params.orderedQuestion[currentStepId] && discussionComments.params.orderedQuestion[currentStepId].inputs && discussionComments.params.orderedQuestion[currentStepId].inputs[discuPath]) {
                    delete discussionComments.params.orderedQuestion[currentStepId].inputs[discuPath]
                }
                if (deletedDiscu.length > 0) {
                    if (deletedDiscu.hasClass("active")) {
                        discuListElem.find(".discuLi").first().trigger("click")
                    }
                    if (discuListElem.find(".discuLi.active").hasClass("allComments")) {
                        currentActive = "allComments";
                    } else if (discuListElem.find(".discuLi.active").attr("data-inputkey")) {
                        currentActive = discuListElem.find(".discuLi.active").attr("data-inputkey");
                    }
                    discuListElem.html(discussionComments.common.buildCommentedQuestionList(typeof discussionComments.params.orderedQuestion != "undefined" && notEmpty(discussionComments.params.orderedQuestion) ? discussionComments.params.orderedQuestion : {}, false, currentActive));
                    discussionComments.common.bindDiscussionContentEvent()
                }
            }
        }
    }
}
discussionSocket.init();