/*
	###############################################################################################
	################################# searchObj DOCUMENTION #######################################
	############# _object_ contient les vues et l'algorithme du moteur de recherche ###############
	############################### <codoc/process/search/read.md> ################################
	###############################################################################################
	# Definition :
		- @searchObj est un objet étendable prévu pour les app de recherche en tout genre (agenda, live, admin, search, et autres)./
		-Il permet à la fois de générer les filtres des app, de générer le contenu de l'en tête des résultats, le traitement des résultats.
		-Il contient aussi toutes la logique de traitement de donnée en partant des parametres de recherche jusqu'à l'affichage des résultats.
		-Il peut être configurer (exemple : "co2.views.app.filters.search").
			Toutes les configurations prises en charge à l'initialisation disponible en l'état sont listées dans le doc modules/codoc/search/config
		-Cette objet est chargé en $.extend jquery (ex : modules/co2/views/app/filters/search) et permet une costumisation compléte des fonctions

	# Arborescence
		|_ `filters` : _Object_ contient toutes les vues des filtres et ces fonctionnalités liées
		|_ `header` : _Object_ contient toutes les vues et fonctionnalité de l'en-tête de la recherce
		|_ `search` : _Object_ contient l'ingénieurie de recherche de l'initialisation des parametres, la récupération/traitement des données et fonction de callback
		|_ `results` : _Object_ contenant les méthodes de traitement de la donnée et d'affichage dans son contenu
		|_

	# Algo de recherche avec la carte
	** 3 comportements sont associés au chargement des cartes et des directorys :**
	## [Comportement 1 : Initialisation par la map **fObj.results.map.active : true & fObj.results.map.sameAsDirectory : false**]
	|_ 	La carte est visible et affiche tous les résultats. On la dissocie de l'affichage du directory
	|_ Le directory n'aura jamais un indexStep à 0 (affichage de tous les résultats)
	## [Comportement 2 : Initialisation par le directory **fObj.results.map.active : false & fObj.results.map.sameAsDirectory : false**]
	|_ L'app s'initialise sur un rendu html classique (indexStep à 0 impossible) avec un load classique
	|_ Au clique, sur l'affiche de la map on passe sur une recherche map.active true qui va afficher tous les résultats du au settings sameAsDirectory à false
	|_ Pour le moment, on indique d'effectuer une recherche auparavent si le nombre de résultat est trop important ( > 15k)
	## [comportement 3 : Initialisation par le directory **fObj.results.map.active : false & fObj.results.map.sameAsDirectory : true**]
	|_ Intéressant quand une carte est affichée en simultannée avec un directory (ex : deal #dossier)



*/

var searchObj = {
	container : "" ,
	footerDom : ".footerSearchContainer",
	urlData : baseUrl+"/" + moduleId + "/search/globalautocomplete",
	options : { },
	pInit : { },
	init : function(pInit = null){
		mylog.log("searchObj.init",pInit);
		// filtre par answers
		if(typeof appConfigPageFilters != "undefined" && appConfigPageFilters != null){
			// get filters data
			var answersOrga = searchObj.helpers.getAnswersWithOrga(appConfigPageFilters);
			$.each(appConfigPageFilters, function(k,v){
				var answerConfig = answersOrga[k];
				// Check if answerData exists
				if(answerConfig){
					answerConfig = Object.keys(answerConfig).sort().reduce((obj, key) => {
						obj[key.toLowerCase()] = answerConfig[key];
						return obj;
					}, {});
					// Si les champs visibles sont définis
					if(typeof v.visible != "undefined"){
						if(typeof v.visible == "string"){
							answerConfig = answerConfig[v.visible.toLowerCase()];
						}else if(typeof v.visible == "object"){
							v.visible = v.visible.map(function(k){
								return k.toLowerCase();
							});
							$.each(answerConfig, function(key, value){
								if(!v.visible.includes(key)){
									delete answerConfig[key];
								}
							});
						}
					}
					pInit.filters[k] = {
						name : v.label,
						view : "dropdownList",
						type : "valueArray",
						field : "_id",
						event : "inArray",
						typeList : "object",
						list : answerConfig,
					};
					// si le filtre par tags est activé
					if(typeof v.filterByTag != "undefined"){
						pInit.filters[k].filterByTag = v.filterByTag;
						pInit.filters[k].otherField = "tags";
					}
				}
			});
		}
		//Init variable
		var copyFilters = jQuery.extend(true, {}, searchObj);
		copyFilters.initVar(pInit);
		return copyFilters;

	},
	initVar : function(pInit){
		mylog.log("searchObj.initVar",pInit);
		this.pInit = pInit;
		// Configuration du dom Html des filtres
		if(pInit != null && typeof pInit.container != "undefined")
			this.filters.dom = pInit.container;
		if(pInit != null && typeof pInit.footerDom != "undefined")
			this.footerDom =  pInit.footerDom;
		if(pInit != null && typeof pInit.lastSearchDone != "undefined")
			this.lastSearchDone =  pInit.lastSearchDone;
		if(pInit != null && typeof pInit.redirectPage != "undefined")
			this.redirectPage =  pInit.redirectPage;
		if(pInit != null && typeof pInit.disableScrollTop != "undefined")
			this.disableScrollTop =  pInit.disableScrollTop;
		if(pInit != null && typeof pInit.disableScrollTopPageClick != "undefined")
			this.disableScrollTopPageClick =  pInit.disableScrollTopPageClick;
		// Url ajax de recherche de données
		if(pInit != null && typeof pInit.urlData != "undefined")
			this.urlData =  pInit.urlData;
		// LoadEvent est relative à l'évnement de chargement scroll // page //
		// Mais il peut être aussi liée au app utilisée comme l'agenda, l'admin ou live
		if( (pInit != null && typeof pInit.loadEvent != "undefined") ){
			$.extend(this.search.loadEvent, pInit.loadEvent);
			if(typeof pInit[this.search.loadEvent.default] != "undefined"
				&& typeof pInit[this.search.loadEvent.default].options != "undefined")
				$.extend(this[this.search.loadEvent.default].options, pInit[this.search.loadEvent.default].options);
			// Initialisation du panel admin avec ces configs
			if(typeof this[this.search.loadEvent.default].initDefaults != "undefined")
			 	this[this.search.loadEvent.default].initDefaults(this,pInit);
		}
		// Config params relative à l'affichage des résultats recherchés
		if( (pInit != null && typeof pInit.results != "undefined") ){
			if(typeof pInit.results.renderView != "undefined")
				this.results.renderView= pInit.results.renderView;
			if(typeof pInit.results.multiCols != "undefined")
				this.results.multiCols= pInit.results.multiCols;
			if(typeof pInit.results.scrollableDom != "undefined")
				this.results.scrollableDom= pInit.results.scrollableDom;
			if(typeof pInit.results.dom != "undefined")
				this.results.dom= pInit.results.dom;
			if(typeof pInit.results.afterRenderingEvent != "undefined")
				this.results.afterRenderingEvent= pInit.results.afterRenderingEvent;
			if(typeof pInit.results.community != "undefined")
				this.results.community= pInit.results.community;
			if(typeof pInit.results.map != "undefined"){
				$.extend(this.results.map, pInit.results.map );
			}
			if(typeof pInit.results.callBack != "undefined")
				this.results.callBack=pInit.results.callBack;
			if(typeof pInit.results.imgObsCallback != "undefined")
				this.results.imgObsCallback=pInit.results.imgObsCallback;

			if(typeof pInit.results.smartGrid != "undefined")
				this.results.smartGrid= pInit.results.smartGrid;
			if(typeof pInit.results.events != "undefined")
				this.results.events= pInit.results.events;
			if(typeof pInit.results.unique != "undefined")
				this.results.unique= pInit.results.unique;
		}

		//init mapContent options
		if(pInit != null && pInit.mapContent && pInit.mapContent.hideViews){
			this.mapContent.hideViews = pInit.mapContent.hideViews;
		}


		// Config params relative au header
		if(pInit != null && typeof pInit.header != "undefined"){
			if(typeof pInit.header.options != "undefined")
				this.header.options =  pInit.header.options;
			if(typeof pInit.header.dom != "undefined")
				this.header.dom =  pInit.header.dom;
			if(typeof pInit.header.views != "undefined")
				$.extend(this.header.views, pInit.header.views );
			if(typeof pInit.header.events != "undefined")
				$.extend(this.header.events, pInit.header.events );
		}

		/* INITIATION DE LA PROPRIÉTÉ MAPOBJ ==========> */
		//on prend le map passer en option s'il existe sinon la mapCO par defaut
		var mapContext = (pInit.mapCo)?pInit.mapCo:mapCO
		/*
			verifier si la map est une instance de CoMap (refactor de MapObj)
			pour gérer les deux objets à la fois
		*/
		if(mapContext instanceof CoMap){
			var comapOptions = mapContext.getOptions();
			this.mapObj = {
				parentContainer: comapOptions.parentContainer,
				map:mapContext.getMap(),
				addElts: function(elts){
					mapContext.addElts(elts)
				},
				removeElts: function(id){
					mapContext.removeMarker(id)
				},
				clearMap: function(){
					mapContext.clearMap()
				},
				showLoader: function(){
					mapContext.showLoader()
				},
				fitBounds:function(){
					mapContext.fitBounds()
				}
			}
		}else if(mapContext instanceof MapD3){
			var comapOptions = mapContext.getOptions();
			this.mapObj = {
				showLegende: comapOptions.showLegende,
				clusterType : comapOptions.clusterType,
				markerType : comapOptions.markerType,
				dynamicLegende: comapOptions.dynamicLegende,
				legende: comapOptions.legende,
				legendeVar: comapOptions.legendeVar,
				legendeLabel: comapOptions.legendeLabel,
				groupBy: comapOptions.groupBy,
				parentContainer: comapOptions.parentContainer,
				map:mapContext.getMap(),
				addElts: function(elts){
					mapContext.addElts(elts)
				},
				clearMap: function(){
					mapContext.clearMap()
				},
				showLoader: function(){
					mapContext.showLoader()
				},
				fitBounds:function(){
					mapContext.fitBounds()
				}
			}
		}else{
			this.mapObj = {
				parentContainer: mapContext.parentContainer,
				map:mapContext.map,
				addElts: function(elts){
					mapContext.addElts(elts)
				},
				clearMap: function(){
					mapContext.clearMap()
				},
				showLoader: function(){
					mapContext.showLoader()
				},
				fitBounds:function(){

				}
			}
		}
		/* <========== INITIATION DE LA PROPRIÉTÉ MAPOBJ */

		//if(pInit.layoutDirection)
		//	this.layoutDirection = pInit.layoutDirection;

		this.initDefaults(pInit);
		//Initialisation des vues
		this.initViews(pInit);

		this.initEvents(pInit);

	},
	initDefaults : function(pInit){
		mylog.log("searchObj.initDefaults",pInit);
		var str = "";
		var fObj = this;
		//searchObject.reset();
		if(typeof pInit.defaults != "undefined"){
			mylog.log("searchObj.initDefaults defaults");
			if(typeof pInit.defaults.filters != "undefined"){
				$.each(pInit.defaults.filters,function(k,v){
					if(typeof fObj.search.obj.filters == "undefined")
						fObj.search.obj.filters = {};
					fObj.search.obj.filters[k] = v;
				});
			}
			if(typeof pInit.defaults.forced != "undefined"){
				$.each(pInit.defaults.forced,function(k,v){
					if(typeof fObj.search.obj.forced == "undefined" )
						fObj.search.obj.forced = {};
					fObj.search.obj.forced[k] = v;

				});
			}
			if(typeof pInit.defaults.types != "undefined")
				fObj.search.obj.types = pInit.defaults.types ;
			if(typeof pInit.defaults.type != "undefined")
				fObj.search.obj.type = pInit.defaults.type ;
			if(typeof pInit.defaults.tags != "undefined"){
				fObj.search.obj.tags = pInit.defaults.tags;
			}
			if(typeof pInit.defaults.params  != "undefined"){
				fObj.search.obj.params = pInit.defaults.params;
			}
			if(typeof pInit.defaults.textPath != "undefined"){
				fObj.search.obj.textPath = pInit.defaults.textPath;
			}
			if(pInit != null && typeof pInit.filters != "undefined" &&  typeof pInit.filters.text != "undefined"&&  typeof pInit.filters.text.searchBy != "undefined"){ 
				fObj.search.obj.searchBy = pInit.filters.text.searchBy
			}
			if(typeof pInit.defaults.tagsPath != "undefined"){
				fObj.search.obj.tagsPath = pInit.defaults.tagsPath;
			}
			if(typeof pInit.defaults.section != "undefined")
				fObj.search.obj.section = pInit.defaults.section ;
			if(typeof pInit.defaults.category != "undefined")
				fObj.search.obj.category = pInit.defaults.category ;

			if(typeof pInit.defaults.private != "undefined")
				fObj.search.obj.private = pInit.defaults.private ;

			if(typeof pInit.defaults.sort != "undefined")
				fObj.search.obj.sort = pInit.defaults.sort ;

			if(typeof pInit.defaults.fields != "undefined")
				fObj.search.obj.fields = pInit.defaults.fields ;

			if(typeof pInit.defaults.fieldShow != "undefined")
				fObj.search.obj.fieldShow = pInit.defaults.fieldShow ;

			if(typeof pInit.defaults.doublon != "undefined")
				fObj.search.obj.doublon = pInit.defaults.doublon ;

			if(typeof pInit.defaults.openingHours != "undefined")
				fObj.search.obj.openingHours = pInit.defaults.openingHours ;

			if(typeof pInit.defaults.mapping != "undefined")
				fObj.search.obj.mapping = pInit.defaults.mapping ;

			if(typeof pInit.defaults.notSourceKey != "undefined")
				fObj.search.obj.notSourceKey = pInit.defaults.notSourceKey ;

			if(typeof pInit.defaults.sourceKey != "undefined")
				fObj.search.obj.sourceKey = pInit.defaults.sourceKey ;

			if(typeof pInit.defaults.indexStep != "undefined" && (pInit.defaults.indexStep>0 || pInit.defaults.indexStep == "all")){
				fObj.search.obj.indexStep = pInit.defaults.indexStep ;
			}
			if(typeof pInit.defaults.sortBy != "undefined")
				fObj.search.obj.sortBy = pInit.defaults.sortBy ;
			if(typeof pInit.defaults.activeContour != "undefined")
				fObj.search.obj.activeContour = pInit.defaults.activeContour ;

			if(typeof pInit.defaults.community != "undefined")
				fObj.search.obj.community = pInit.defaults.community ;
		}
		if(typeof costum != "undefined" && notNull(costum) && typeof costum.searchObject != "undefined" && typeof costum.searchObject.indexStep != "undefined" && costum.searchObject.indexStep > 0)
            fObj.search.obj.indexStep=costum.searchObject.indexStep;
        if(typeof costum != "undefined" && notNull(costum) && typeof costum.searchObject != "undefined" && typeof costum.searchObject.sortBy != "undefined")
            fObj.search.obj.sortBy=costum.searchObject.sortBy;
        if(typeof costum != "undefined" && notNull(costum) && typeof costum.searchObject != "undefined" && typeof costum.searchObject.links != "undefined")
            fObj.search.obj.links=costum.searchObject.links;

		if(pInit != null && typeof pInit.options != "undefined")
			fObj.search.obj.options =  pInit.options;
	},
	initEvents : function(pInit){
		var fObj = this;
		mylog.log("searchObj.initEvents filters");
		fObj.filters.initEvents(fObj, pInit);
		mylog.log("searchObj.initEvents Header");
		fObj.header.initEvents(fObj, pInit);
		fObj.mapContent.initEvents(fObj);
		$(".removeAllActiveFilters").off().on("click", function(){
			//alert($(this).data("domfilter")+" ."+$(this).data("filterk")+".active");
			$($(this).data("domfilter")+" ."+$(this).data("filterk")+".active").each(function(){
				$(this).removeClass("active");
				fObj.filters.manage.removeActive(fObj, $(this), true);
			});
			fObj.results.map.changedfilters=true;
			fObj.search.init(fObj);
		});
		/******** TODO : CODE A REFACTORER *******************/
		/*if(pInit != null && typeof pInit.calendar != "undefined"){
			var today = new Date();
			today = new Date(today.setSeconds(0));
			today = new Date(today.setMinutes(0));
			today = new Date(today.setHours(0));
			var paramsCalendarAgenda = {
				container : ".calendarSearch",
				searchObj : fObj,
				startDate : today.setDate(today.getDate())
			}
			this.calendar = calendarObj.init(paramsCalendarAgenda);

		}*/
		/*if($(".btn-tags-unik").length > 0){
			$(".btn-tags-unik").off().on("click", function(){
	            coInterface.showLoader("#dropdown_search",trad.currentlyresearching);
	            $(".btn-tags-unik").removeClass("active");
	            if(!notNull($(this).data("k")) || $(this).data("k")=="")
	                filterSearch.search.obj.tags=[];
	            else{
	                $(this).addClass("active");
	                filterSearch.search.obj.tags=[$(this).data("k")];
	            }

	            if(filterSearch.search.loadEvent.active == "agenda" ){
	                filterSearch.agenda.options.dayCount = 0;
	                filterSearch.agenda.options.finishSearch = false;
	                filterSearch.agenda.options.countsLaunchSesearch = 0;
	                filterSearch.agenda.options.nbSearch = 0;
	            }
	            filterSearch.search.init(filterSearch, true);
	        });
	    }*/
	    /******** TODO : FIN CODE A REFACTORER *******************/
	},
	initViews : function(pInit){
		mylog.log("searchObj.initViews",pInit);
		var fObj = this;
		fObj.filters.initViews(fObj, pInit);
		fObj.header.initViews(fObj);
		fObj.mapContent.initViews(fObj);

		//filters initDefaults must be call after the initViews
		fObj.filters.initDefaults(fObj, pInit);
	},
	helpers: {
		toggleMapVisibility: function(fObj){
			coInterface.initHtmlPosition();
			if(fObj.results.map.active){
				fObj.results.map.active=false;
				fObj.search.currentlyLoading = false;
				fObj.search.countResults = false;
				fObj.search.loadEvent.active = "scroll";
				fObj.search.loadEvent.bind = false;
				fObj.search.loadEvent.default = "pagination"

				$(fObj.mapObj.parentContainer).fadeOut();
				//$(mapCO.hideContainer).show();
				fObj.mapObj.map.invalidateSize();
				if(!fObj.results.map.sameAsDirectory && fObj.results.map.changedfilters){
					fObj.results.map.changedfilters=false;
					fObj.search.init(fObj);
				}
			}else{
				if(fObj.results.numberOfResults > 6000){
					fObj.results.showLimitMessage(fObj);
				}else{
					fObj.results.map.active=true;
					$(fObj.mapObj.parentContainer).fadeIn();
					//$(mapCO.hideContainer).hide();

					/*
						keep current zoom state when toogle map visibility
						that's why I commented the following code
					*/
					/* var mapCO_options = mapCO.getOptions();
					fObj.mapObj.map.panTo(mapCO_options.mapOpt.center);
					fObj.mapObj.map.setZoom(mapCO_options.mapOpt.zoom);
					fObj.mapObj.map.invalidateSize(); */
					if(!fObj.results.map.sameAsDirectory && fObj.results.map.changedfilters){
						fObj.results.map.changedfilters=false;
						fObj.search.init(fObj);
					}
				}
			}
		},
		toggleGraphMode: function(fObj, option){
			urlCtrl.openPreview("/view/url/costum.views.custom.OpenAtlas.community-graph", {"context":contextData, "options":option});
		},
		toggleKanbanMode: function(fObj, option){
			urlCtrl.openPreview("/view/url/costum.views.custom.kanban.kanban-roles", {"context":contextData, "options":option});
		},
		toggleBadgesMode: function(fObj, option){
			urlCtrl.openPreview("/view/url/costum.views.custom.kanban.kanban-badges", {"context":contextData, "options":option});
		},
		getAnswersWithOrga: function(configAnswers){
			var results = {};
			var params = {searchedData : configAnswers};
			ajaxPost(
				null,
				baseUrl + "/costum/francetierslieux/autoglobalthematicntwrk",
				params,
				function (data) {
					$.each(data, function(key, value){
						if (value.count.answers > 0) {
							if(typeof value.results == "object" && Object.keys(value.results).length > 0){
								results[key] = {};
								$.each(value.results, function(k, v){
									results[key][k] = {
										label: k,
										value: v.orgaNameArray
									}
								});
							}
							// results = Object.keys(results).sort().reduce((obj, key) => {
							// 	obj[key] = results[key];
							// 	return obj;
							// })
							// results = ;
						}
					});
				},
				null,
				"json",
				{ async: false }
			);
			return results;
		}
	},
	filters : {
		dom : "#filterContainer",
		initViews : function(fObj, pInit){
			var str = '<div class="visible-xs">'+ fObj.filters.views.xs() +'</div>';
			if(typeof pInit.subHeader != "undefined") {
				var moreClass = pInit.subHeader.class ? pInit.subHeader.class : '';
				str += `<div id="${pInit.subHeader.dom ? pInit.subHeader.dom : 'btnResetActiveFilters'}" class="col-xs-12 no-padding hidden-xs ${ moreClass }">`
				if(typeof pInit.subHeader.views != 'undefined') {
					$.each(pInit.subHeader.views, function(k, v) {
						if(fObj.filters.views[v['view']]) {
							str += fObj.filters.views[v['view']](k, v);
						}
					})
				}
				str += '</div>';
			}
			str+=`<div id="filterContainerInside" class="panel-group container-filters-menu menu-filters-xs ${pInit.layoutDirection == "horizontal" ? "container-filters-horizontal":""}">`;
			str+='<div class="count-result-xs col-xs-12 visible-xs"></div>';
			if(typeof pInit.filters != "undefined"){
				$.each(pInit.filters,function(k,v){
					//if(k != "text"){
						var filterView="";
						mylog.log("searchObj.initViews each", k,v);
						if(typeof v.view != "undefined" && typeof fObj.filters.views[v.view] != "undefined"){
							filterView = fObj.filters.views[v.view](k,v, fObj);
						}else if(typeof fObj.filters.views[k] == "function"){
							filterView = fObj.filters.views[k](k,v,fObj);
						}
						if(v.event == "buttonD3"){
							var config = {
								"view": "labelMap",
                    			"type" : "labelmap",
								"name" : "Legende de la map",
								"event" : "changeLabel",
								"lists" : Object.fromEntries(Object.entries(pInit.filters).filter(([key, val]) => {
									return (typeof val.view != "undefined" && val.view == "dropdownList" && typeof val.event != "undefined" && val.event != "buttonD3")
								}))
							}
							filterView += fObj.filters.views[config.view](null, config, fObj)
						}
						if(typeof v.dom != "undefined")
							$(v.dom).html(filterView);  
						else
							str+=filterView;
					//}
				});
			}
			str+='<div id="activeFilters" class="col-xs-12 no-padding" style="display:none;">';
			str+=    '<div class="pull-left margin-right-5" style="height:30px;">';
			str+=	    '<div class="activeFilter-label" style="display: flex;align-items: center;height: inherit;">';
			str+=		    '<span><i class="fa fa-filter" style="color:#9fbd38;"></i> '+trad.activeFilters+' : </span>';
			str+=	    '</div>';
			str+=	'</div>';
			str+='</div>';
			if(typeof pInit.aapFooter != "undefined"){
				var moreClass = pInit.aapFooter.class ? pInit.aapFooter.class : '';
				str += `<div id="${pInit.aapFooter.subdom ? pInit.aapFooter.subdom : 'btnSaveActiveFilters'}" class="col-xs-12 no-padding ${ moreClass }">`
				strMore = `<div id="${pInit.aapFooter.subdom ? pInit.aapFooter.subdom : 'btnSaveActiveFilters'}" class="col-xs-12 no-padding">`;
				if(typeof pInit.aapFooter.views != 'undefined') {
					$.each(pInit.aapFooter.views, function(k, v) {
						if(fObj.filters.views[v['view']]) {
							str += fObj.filters.views[v['view']](k, v);
							strMore += fObj.filters.views[v['view']](k, v);
							
						}
					})
				}
				fObj['pInit']['subdom'] = pInit.aapFooter.subdom ? '#'+pInit.aapFooter.subdom : "#btnSaveActiveFilters"
				str += '</div>';
				strMore += '</div>';
				$("#centerBtnSaveFilter").append(strMore)
			}
			str += '</div>';
			$(fObj.filters.dom).html(str);
		},
		initEvents : function(fObj, pInit){
			if(typeof pInit.filters != "undefined"){
				$.each(pInit.filters,function(k,v){
					mylog.log("searchObj.initEvents each", k,v);
					var domEvents = (typeof v.dom != "undefined") ? v.dom : fObj.filters.dom;
					if(typeof v.event != "undefined" && typeof fObj.filters.events[v.event] != "undefined"){
						fObj.filters.events[v.event](fObj, domEvents, k, v);
					}else if(typeof fObj.filters.events[k] != "undefined")
						fObj.filters.events[k](fObj, domEvents, k, v);
					if(v.event == "buttonD3"){
						fObj.filters.events.changeLabel(fObj, domEvents, "labelmap")
					}
				});
			}

			function switchFilterLgXs() {
				if($(window).width() < 768 && typeof pInit.defaults != 'undefined' && typeof pInit.defaults.appendToXs != 'undefined') {
					$(pInit.container + ".searchObjCSS,"+pInit.container).detach().appendTo(pInit.defaults.appendToXs);
					$("#filterContainerInside").hide();
					$("#filterContainerAapOceco").hide()
				} else if(typeof pInit.defaults != 'undefined' && typeof pInit.defaults.appendToLg != 'undefined') {
					$(pInit.container + ".searchObjCSS,"+pInit.container).detach().appendTo(pInit.defaults.appendToLg);
					$("#filterContainerInside").show()
				}
				if(typeof pInit.defaults != 'undefined' && typeof pInit.defaults.onResizeFunc != 'undefined')
						pInit.defaults.onResizeFunc()
			}
			switchFilterLgXs()
			if(typeof pInit.defaults != 'undefined' && typeof pInit.defaults.onResize != 'undefined' && pInit.defaults.onResize === true) {
				window.onresize = (e) => {
					e.stopImmediatePropagation();
					switchFilterLgXs()
				}
			}

			if(typeof pInit.subHeader != "undefined") {
				if(typeof pInit.subHeader.views != 'undefined') {
					$.each(pInit.subHeader.views, function(k, v) {
						var domEvents = (typeof v.dom != "undefined") ? v.dom : fObj.filters.dom;
						if(typeof v['event'] != "undefined" && fObj.filters.events[v['event']]) {
							fObj.filters.events[v['event']](fObj, domEvents, k, v)
						}
					})
				}
			}
			if(typeof pInit.aapFooter != "undefined") {
				if(typeof pInit.aapFooter.views != 'undefined') {
					$.each(pInit.aapFooter.views, function(k, v) {
						var domEvents = (typeof v.dom != "undefined") ? v.dom : fObj.filters.dom;
						if(typeof v['event'] != "undefined" && fObj.filters.events[v['event']]) {
							fObj.filters.events[v['event']](fObj, domEvents, k, v)
						}
					})
				}
			}

			//Show/Hide menu filters
			$('#show-filters-xs,.btnCloseMenuFilter i').off().on('click', function() {
				//$header = $(this);
				if($(".filterXs #dropdown_search_detail").is(":visible")) {
					$(".filterXs #show-list-xs").find("i").removeClass("fa-times").addClass("fa-file-text");
                    $(".filterXs #dropdown_search_detail").hide()
                }
				if($(".menu-filters-xs").is(":visible")) {
					$(".btnCloseMenuFilter").hide();
					$("#show-filters-xs").find("i").removeClass("fa-times").addClass("fa-filter");
				} else {
					$(".btnCloseMenuFilter").show();
					$("#show-filters-xs").find("i").removeClass("fa-filter").addClass("fa-times");
				}
			    $(".menu-filters-xs").slideToggle(500);
			});
			// Initialized main search bar in main menu (ex : mainNav)
			if($(".main-search-bar.central-search-bar").length >= 0){
				$(".main-search-bar.central-search-bar").off().on("keyup",delay(function (e) {
					if(e.keyCode != 13){
						fObj.search.obj.text = $(this).val();
						$(".main-search-bar.central-search-bar").val(fObj.search.obj.text);
						fObj.filters.actions.text.spin(fObj, true, null, ".central-search-bar");
						fObj.search.init(fObj);
					}

				}, 750)).on("keyup", function(e){
					if(e.keyCode == 13){
						fObj.search.obj.text = $(this).val();
						$(".main-search-bar.central-search-bar").val(fObj.search.obj.text);
						fObj.filters.actions.text.spin(fObj, true, null, ".central-search-bar");
						fObj.search.init(fObj);
					}

				});
				$(".main-search-bar-addon.central-search-bar").off().on("click", function(){
						fObj.search.obj.text = $(".main-search-bar.central-search-bar").val();
						$(".main-search-bar.central-search-bar").val(fObj.search.obj.text);
						fObj.filters.actions.text.spin(fObj, true, null, ".central-search-bar");
						fObj.search.init(fObj);
	            });
			}

			if(typeof fObj.filters != 'undefined' && typeof fObj.filters.dom != 'undefined') {
				$(fObj.filters.dom+".aap-filter, .designed-column-filter "+ fObj.filters.dom).on('shown.bs.collapse', '.collapse', function (e) {
					e.stopImmediatePropagation();
					$(fObj.filters.dom + " #filterContainerInside").find(".collapse.in").each(function () {
						$(this).parent().find(".fa.fa-caret-up").removeClass("fa-caret-up").addClass("fa-caret-down");
						$(this).parent().find("div.panel-heading.accordion-bg-hover.tr-all").removeClass('in')
                        $(this).removeClass("in");
                    });
					const panelHeadIcon = $(this).parent().find(".fa.fa-caret-down");
                    $(this).addClass("in");
					panelHeadIcon.fadeOut(300, function() {
						panelHeadIcon.toggleClass("fa-caret-down fa-caret-up")
						panelHeadIcon.fadeIn(300)
					})
					$(this).parent().find("div.panel-heading.accordion-bg-hover.tr-all").addClass("in");
                }).on('hidden.bs.collapse', function () {
					const panelHeadIcon = $(this).parent().find(".fa.fa-caret-up");
					panelHeadIcon.fadeOut(300, function() {
						panelHeadIcon.toggleClass("fa-caret-up fa-caret-down")
						panelHeadIcon.fadeIn(300)
					})
					$(this).parent().find("div.panel-heading.accordion-bg-hover.tr-all").removeClass('in')
                });
			}

			$('.list-item-filters-map button').click(function(){
				$(this).closest('.list-item-filters-map').toggleClass('active')
            })
		},
		initDefaults : function(fObj, pInit){
			mylog.log("searchObj.filters.init pInit", pInit);
			if(typeof pInit.filters != "undefined"){
				// Initialisation des filtres themes & scopeObj par exemple [voir si pertinent ici]
				$.each(pInit.filters,function(k,v){
					mylog.log("searchObj.filters.init each", k,v);
					if(v.view == "scopeList"){
						fObj.filters.defaults["scopeList"](fObj, k,v);
					}
					if(typeof fObj.filters.defaults[k] != "undefined" ){
						fObj.filters.defaults[k](fObj, k,v);
					}
				});
			}
			if(notEmpty(fObj.search.obj.tags)){
				$.each(fObj.search.obj.tags, function(e,v){
					labelTags = (typeof tradCategory[v] != "undefined") ? tradCategory[v] : v;
					fObj.filters.manage.addActive(fObj,{type : "tags", value : v, label : labelTags});
				});
			}

			// Mode dans localStorage
			modeFilter = localStorage.getItem("filtersmodeView");
			if ( notNull(costum) && typeof costum.slug != "undefined")
				modeFilter = localStorage.getItem("filtersmodeView"+costum.slug); 
			if(typeof fObj.header != "undefined" 
				&& typeof fObj.header.options != "undefined" 
				&& typeof fObj.header.options.right != "undefined" 
				&& typeof fObj.header.options.right.group != "undefined" 
				&& typeof fObj.header.options.right.group.switchView != "undefined" 
				&& fObj.header.options.right.group.switchView 
				&&  notNull(modeFilter)
			){
				$(".changeViews").removeClass("active");
				$(".changeViews[data-mode^="+modeFilter+"]").addClass("active");
				fObj.search.obj.mode = modeFilter;
				if(modeFilter != "table"){
					var viewMode = "elementPanelHtmlSmallCard"
					if(modeFilter == "list")
						viewMode = "elementPanelHtmlMiWidth" 
					fObj.results.renderView = "directory."+viewMode;
					if(fObj.search.loadEvent.default == "table"){
						fObj.search.loadEvent.default = "scroll";
						fObj.results.$grid = null;
						$(".bodySearchContainer").html(`
							<div class="no-padding col-xs-12" id="dropdown_search"> </div>
							<div class="no-padding col-xs-12 text-left footerSearchContainer smartgrid-slide-element"></div> 
						`);
					}
					fObj.results.multiCols = true;
				}else{
					fObj.search.loadEvent.default = "table";
					fObj.results.multiCols = false;
					fObj.results.smartGrid = false;
				}
			}

			// Analyse des params GET dans l'url afin de les initialiser dans la vue filtres actives
			getParamsUrls=location.hash.split("?");
			if(typeof getParamsUrls[1] != "undefined" ){
				//var parts = getParamsUrls[1].split("&");
				var parts = new URLSearchParams(getParamsUrls[1]);
		        var $_GET = {};
		        var initScopesResearch={"key":"open","ids":[]};
		        
				for (let entry of parts.entries()) {
				//for (var i = 0; i < parts.length; i++) {
		            //var temp = parts[i].split("=");
		            //$_GET[decodeURIComponent(temp[0])] = decodeURIComponent(temp[1]);
					$_GET[entry[0]] = entry[1];
		        }

		        if(Object.keys($_GET).length > 0){
		        	var paramsGetExclude=["scopeType"];

		            $.each($_GET, function(e,v){
		            	if($.inArray(e, paramsGetExclude) < 0){
			                //v=decodeURI(v);
			                if(e=="text"){
								var domSearchText= (typeof pInit.filters != "undefined" && typeof pInit.filters[e] != "undefined" && typeof pInit.filters[e].dom != "undefined") ? pInit.filters[e].dom : fObj.filters.dom;
			                	fObj.search.obj[e]=v;
			                	$(domSearchText+" .searchBar-filters .main-search-bar[data-field='text']").val(v);
			                }
			                else if(e=="types"){
			                	fObj.search.obj.types=[];
			                	values=v.split(",");
			                	$.each(values, function(i, val){
		                			elt=directory.getTypeObj(val);
		                			fObj.search.obj.types.push(val);
		                			fObj.filters.manage.addActive(fObj,{type:"types",value:elt.name, field:val, key:val});
			                	});
			                }
			                else if(e=="valueArray"){
								values=v.split(",");
								const addArrayFilter = (valueArray, element) => {
									var inArrayValue = (Array.isArray(valueArray)) ? valueArray : [valueArray];
									if(!exists(fObj.search.obj.filters))
										fObj.search.obj.filters = {};
									if(element.data("otherfield")){
										if(exists(fObj.search.obj.filters) && !exists(fObj.search.obj.filters['$or'])){
											var filtersData = $.extend({}, fObj.search.obj.filters)
											fObj.search.obj.filters = {};
											fObj.search.obj.filters['$or'] = filtersData;
										}
										if(!exists(fObj.search.obj.filters['$or']))
											fObj.search.obj.filters['$or'] = {};
										if(!exists(fObj.search.obj.filters['$or'][element.data("field")]))
											fObj.search.obj.filters['$or'][element.data("field")] = {'$in': inArrayValue};
										else if(!exists(fObj.search.obj.filters['$or'][element.data("field")]['$in']))
											fObj.search.obj.filters['$or'][element.data("field")]['$in']= inArrayValue;
										else
											if (Array.isArray(valueArray))
												fObj.search.obj.filters['$or'][element.data("field")]['$in'].push(...valueArray);
											else 
												fObj.search.obj.filters['$or'][element.data("field")]['$in'].push(valueArray);
			
										if(!exists(fObj.search.obj.filters['$or'][element.data("otherfield")]))
											fObj.search.obj.filters['$or'][element.data("otherfield")] = {'$in': [element.data("label")]};
										else if(!exists(fObj.search.obj.filters['$or'][element.data("otherfield")]['$in']))
											fObj.search.obj.filters['$or'][element.data("otherfield")]['$in'] = element.data("label");
										else
											fObj.search.obj.filters['$or'][element.data("otherfield")]['$in'].push(element.data("label"));
									}else {
										if(exists(fObj.search.obj.filters['$or'] ) && exists(fObj.search.obj.filters['$or']["tags"])){
											if(!exists(fObj.search.obj.filters['$or'][element.data("field")]))
												fObj.search.obj.filters['$or'][element.data("field")] = {'$in': inArrayValue};
											else if(!exists(fObj.search.obj.filters['$or'][element.data("field")]['$in']))
												fObj.search.obj.filters['$or'][element.data("field")]['$in']= inArrayValue;
											else
												if (Array.isArray(valueArray))
													fObj.search.obj.filters['$or'][element.data("field")]['$in'].push(...valueArray);
												else 
													fObj.search.obj.filters['$or'][element.data("field")]['$in'].push(valueArray);	
										}else{
											if(!exists(fObj.search.obj.filters[element.data("field")]))
												fObj.search.obj.filters[element.data("field")] = {'$in': inArrayValue};
											else if(!exists(fObj.search.obj.filters[element.data("field")]['$in']))
												fObj.search.obj.filters[element.data("field")]['$in']= inArrayValue;
											else
												if (Array.isArray(valueArray))
													fObj.search.obj.filters[element.data("field")]['$in'].push(...valueArray);
												else 
													fObj.search.obj.filters[element.data("field")]['$in'].push(valueArray);
										}
									}
									// if(!exists(fObj.search.obj.filters[element.data("field")]))
									// 	fObj.search.obj.filters[element.data("field")] = {'$in': inArrayValue};
									// else if(!exists(fObj.search.obj.filters[element.data("field")]['$in']))
									// 	fObj.search.obj.filters[element.data("field")]['$in']= inArrayValue;
									// else
									// 	if (Array.isArray(valueArray))
									// 		fObj.search.obj.filters[element.data("field")]['$in'].push(...valueArray);
									// 	else 
									// 		fObj.search.obj.filters[element.data("field")]['$in'].push(valueArray);
								}
								$.each(values, function(key, value){
									if($(`button[data-key='${value}']`).length > 0){
										var $this = $(`button[data-key='${value}']`);
										var arrayValue = ($this.data("value").includes("\\")) ? $this.data("value").split("\\") : [$this.data("value")];
										addArrayFilter(arrayValue, $this);
										if(typeof fObj.search.obj[$this.data("type")] != "undefined" && exists(fObj.search.obj[$this.data("type")]))
											if(fObj.search.obj[$this.data("type")].includes(value))
												fObj.search.obj[$this.data("type")].splice(fObj.search.obj[$this.data("type")].indexOf(value), 1)
											else
												fObj.search.obj[$this.data("type")].push(value)
										else
											fObj.search.obj[$this.data("type")] = [value];

										fObj.filters.manage.addActive(fObj, $this);
									}
								});
			                }else if(e=="tags"){
			                	values=v.split(",");
			                	$.each(values, function(i, val){
			                		fObj.search.obj.tags.push(val);
			                		fObj.filters.manage.addActive(fObj,{type: e, value : val});
			                	});
			                }
			                else if($.inArray(e,["cities","zones","cp"]) > -1){
			                	$.each(v.split(","), function(i, j){ initScopesResearch.ids.push(j) });
			                    if(initScopesResearch.key!="" && initScopesResearch.ids.length > 0)
			                    	checkMyScopeObject(initScopesResearch, $_GET, function(){
			                    		if(typeof myScopes.open != "undefined" && Object.keys(myScopes.open).length > 0){
					                    	$.each(myScopes.open, function(i, scope){
					                    		if(typeof scope.active != "undefined" && scope.active)
					                				fObj.filters.manage.addActive(fObj, {type:"scope",value:scope.name,key:i});
					                		});
					                	}
			                    	});

			                    if(typeof myScopes.open != "undefined" && Object.keys(myScopes.open).length > 0){
			                    	$.each(myScopes.open, function(i, scope){
			                    		if(typeof scope.active != "undefined" && scope.active)
			                				fObj.filters.manage.addActive(fObj, {type:"scope",value:scope.name,key:i});
			                		});
			                	}
			                }else if(e=="nbPage"){
			                	fObj.search.obj.nbPage=(Number(v)-1);
			                	fObj.search.obj.forced.page=true;
			                }else if(e=="map"){
			                	if(v == "true" || v == true){
									fObj.results.map.active=true;
									$(fObj.mapObj.parentContainer).fadeIn();
									// fObj.helpers.toggleMapVisibility(fObj);
								}
			                }else if(e=="mode"){
								$(".changeViews").removeClass("active");
								$(".changeViews[data-mode^="+v+"]").addClass("active");
			                	fObj.search.obj.mode = v;
								if(v != "table"){
									var viewMode = "elementPanelHtmlSmallCard"
									if(v == "list")
										viewMode = "elementPanelHtmlMiWidth" 
									fObj.results.renderView = "directory."+viewMode;
									fObj.search.obj.nbPage = 0;
									if(fObj.search.loadEvent.default == "table"){
										fObj.search.loadEvent.default = "scroll";
										fObj.results.$grid = null;
										$(".bodySearchContainer").html(`
											<div class="no-padding col-xs-12" id="dropdown_search"> </div>
											<div class="no-padding col-xs-12 text-left footerSearchContainer smartgrid-slide-element"></div> 
										`);
									}
									fObj.results.multiCols = true;
								}else{
									fObj.search.loadEvent.default = "table";
									fObj.results.multiCols = false;
									fObj.results.smartGrid = false;
								}
			                }else if(e=="preview"){
								mylog.log("preview",v)
			                }else{
			                	if(v.indexOf(",") >= 1 ){
			                		values=v.split(",");
			                		if(typeof fObj.search.obj[e] == "undefined") fObj.search.obj[e]=[];
				                	$.each(values, function(i, val){
				                		fObj.search.obj[e].push(val);
				                		tradValue=val;
										var key = val;
										if(typeof fObj.pInit.filters[e] != "undefined"){
											if(typeof fObj.pInit.filters[e]["list"] != "undefined" && typeof fObj.pInit.filters[e]["keyValue"] != "undefined" && !fObj.pInit.filters[e]["keyValue"]){
												if(typeof fObj.pInit.filters[e]["list"][val] != "undefined"){
													val = fObj.pInit.filters[e]["list"][val];
													tradValue = val;
												}
											}
										}
				                		if(typeof tradCategory[val] != "undefined")
				                			tradValue= tradCategory[val];
				                		else if(typeof trad[val] != "undefined")
				                			tradValue= trad[val];
										var dataFilter = {
											key: key,
											value:val.name,
											label:tradValue
										}
										if(typeof val.name != "undefined")
											dataFilter.value = val.name
										else if (typeof val == "string")
											dataFilter.value = val

										if(typeof pInit.filters != "undefined" && typeof pInit.filters[e] != "undefined"  && typeof pInit.filters[e].type != "undefined")
											dataFilter.type = pInit.filters[e].type;
										else
											dataFilter.type = e;
										if(typeof pInit.filters != "undefined" && typeof pInit.filters[e] != "undefined"  && typeof pInit.filters[e].field != "undefined")
											dataFilter.field = pInit.filters[e].field 

				                		fObj.filters.manage.addActive(fObj,dataFilter);
				                	});
			                	}else{
									if (
										typeof fObj.pInit.filters[e] != "undefined" && 
										typeof fObj.pInit.filters[e].type != "undefined" && 
										typeof fObj.pInit.filters[e].toUrlHistory != "undefined"
									) {
										const filterType = fObj.pInit.filters[e].type;
										const filterField = typeof fObj.pInit.filters[e].field != "undefined" ? fObj.pInit.filters[e].field : e;
										if (typeof fObj.search.obj[filterType] == "filters") {
											fObj.search.obj[filter] = {};
										}
										fObj.search.obj[filterType][filterField] = [v];
									}
									fObj.search.obj[e]=[v];
				                	tradValue=v;
									var key = v;
									if(typeof fObj.pInit.filters[e] != "undefined"){
										if(typeof fObj.pInit.filters[e]["list"] != "undefined" && typeof fObj.pInit.filters[e]["keyValue"] != "undefined" && !fObj.pInit.filters[e]["keyValue"]){
											if(typeof fObj.pInit.filters[e]["list"][v] != "undefined"){
												v = fObj.pInit.filters[e]["list"][v];
												tradValue = v;
											}
										}
									}
			                		if(typeof tradCategory[v] != "undefined")
			                			tradValue= tradCategory[v];
			                		else if(typeof trad[v] != "undefined")
			                			tradValue= trad[v];
									else if (typeof tradValue === "string" && /^[a-f\d]{24}$/i.test(tradValue)) {
										if (typeof fObj.pInit.filters[e] != "undefined") {
											if(typeof fObj.pInit.filters[e]["list"] != "undefined" && typeof fObj.pInit.filters[e]["list"][v] == "string") {
												tradValue = fObj.pInit.filters[e]["list"][v];
												if (typeof tradCategory[tradValue] != "undefined") {
													tradValue = tradCategory[tradValue];
												}
												else if (typeof trad[tradValue] != "undefined") {
                                                    tradValue = trad[tradValue];
                                                }
											}
										}
									}
									var dataFilter = {
										key: key,
										value:v,
										label:tradValue
									}
									
									if(typeof pInit.filters != "undefined" && typeof pInit.filters[e] != "undefined"  && typeof pInit.filters[e].type != "undefined")
										dataFilter.type = pInit.filters[e].type;
									else
										dataFilter.type = e;
									if(typeof pInit.filters != "undefined" && typeof pInit.filters[e] != "undefined"  && typeof pInit.filters[e].field != "undefined")
										dataFilter.field = pInit.filters[e].field 
									fObj.filters.manage.addActive(fObj,dataFilter);
				                }
			                }
		                }
		         	});
		        }
		    }
			// manage active toActivate
			if(typeof pInit.toActivate != 'undefined') {
				for(let [key, value] of Object.entries(pInit.toActivate)) {
					var dataFilter = {
						key : null,
						value : null,
						type : "filters"
					}
					if(value["value"]) dataFilter["value"] = value["value"];
					if(value["field"]) dataFilter["field"] = value["field"];
					if(value["type"]) dataFilter["type"] = value["type"];
					if(value["key"]) dataFilter["key"] = value["key"];
					if(dataFilter.type != "filters" && typeof value["likeFilters"] != "undefined") dataFilter["likeFilters"] = value["likeFilters"]
					fObj.filters.manage.addActive(fObj,dataFilter);
				}
			}
		},
		defaults:{
			scopeList : function(fObj, k, v){
				ajaxPost(null,
					baseUrl + '/co2/search/getzone/',
					v.params, function(data){
					mylog.log('searchObj.filters.scopeList ajaxPost success', data);
					fObj.filters.lists.scopeList[k] = data ;
					var str = "";
					$.each(data,function(kD,vD){
						if($.inArray("4",v.params.level) >= 0){
							vD.name = vD.name.toUpperCase();
						}
						str+='<button type="button" data-toggle="dropdown" data-id="'+kD+'" data-key="'+k+'" class= "btn-filters-select col-xs-12 "'+
								'data-value="'+vD.name+'" '+
								'data-type="scopeList">'+vD.name+
							'</button>';
					});
					$(".scopeList"+k+" .list-filters").html(str);
					var domEvents = (typeof v.dom != "undefined") ? v.dom : fObj.filters.dom;
					fObj.filters.events.scopeList(fObj,domEvents);
				}, function(data){
					mylog.log('filertObj.initFilters ajaxPost error', data);
				});
			},
			themes : function(fObj, k, v){
				if(typeof v.lists != "undefined" && notNull(v.lists)){
					mylog.log("searchObj.views.themes v.lists", v.lists);
					mylog.log("searchObj.views.themes fObj.data.lists", fObj.lists);
					fObj.filters.lists.themes = v.lists;
					var str = "";
					$.each(v.lists, function(key, cat){
						if(typeof cat.name != "undefined" && cat.name != null){


							var nameTheme = typeof tradTags[cat.name.toLowerCase()] != "undefined" ?
							tradTags[cat.name.toLowerCase()].charAt(0).toUpperCase()+tradTags[cat.name.toLowerCase()].slice(1)
							: (cat.name).charAt(0).toUpperCase() + (cat.name).slice(1);


							str += '<a href="javascript:;" class="btn btn-default col-sm-2 col-xs-3 padding-10 btn-select-filliaire"'+
										' data-fkey="'+key+'" '+
										' data-keycat="'+cat.name+'">'+
										'<i class="fa '+cat.icon+' fa-2x"></i><br>'+
											nameTheme +
									'</a>';
						}
					});
					$(fObj.filters.dom+" .divBtnTheme").html(str);
				}
			},

			odd : function(fObj, k, v, widthClass="col-md-4 col-sm-6 col-xs-12"){
				var str = "";

				if(typeof v.list != "undefined" && notNull(v.list)){
					v.lists = v.list;
				}

				if(typeof v.lists != "undefined" && notNull(v.lists)){
					fObj.filters.lists.odd = v.lists;
					$.each(v.lists, function(key, odd){
						if(typeof odd.name != "undefined" && odd.name != null){

							var nameTheme = typeof tradTags[odd.name.toLowerCase()] != "undefined" ?
							tradTags[odd.name.toLowerCase()].charAt(0).toUpperCase()+tradTags[odd.name.toLowerCase()].slice(1)
							: (odd.name).charAt(0).toUpperCase() + (odd.name).slice(1);

							str += `<div style="background:`+ odd.color + `;height:150px!important;" class="`+widthClass+` container-btn-select-odd-tags">
											<div style="height:100%">
												<a class="btn-filters-select btn-select-odd-tags" href="javascript:;" style="text-decoration: none;" data-fkey="`+key+`"
													data-keyodd="`+odd.name+`">
													<h5 style="display:flex; color:white; font-size:11px;"><b class="margin-right-5 number-odd" style="font-size:10pt; font-weight:bolder">`+ key + ` </b> <span class="title-odd"> `+ nameTheme + `</span></h5>
													<div class="text-center">
														<img style="width:100%;height:auto;"  src="`+co2AssetPath+odd.image + `">
													</div>
												</a>
											<div>	
										</div>
									</div></div>`;
						}
					});
					if(fObj.filters.dom){
						$(fObj.filters.dom+" .divBtnOdd").html(str);
					}else{
						return str;
					}
				}
			},
		},
		views : {
			xs : function (k,v){
				str='<button type="button" class="btn btn-default showHide-filters-xs" id="show-filters-xs">'+
						'<span class="filter-xs-count topbar-badge badge animated bounceIn hide badge-tranparent"></span>'+
						'<i class="fa fa-filter"></i>'+
					'</button>'+
					'<div class="searchBarInMenu pull-right">'+
						'<input type="text" class="form-control pull-left text-center main-search-bar central-search-bar" id="main-search-bar" placeholder="'+trad.whatlookingfor+'">'+
						'<span class="input-group-addon pull-left main-search-bar-addon central-search-bar" id="main-search-bar-addon">'+
						'<i class="fa fa-arrow-circle-right"></i>'+
						'</span>'+
					'</div>';
				return str;
			},
			scopeList : function (k,v){
				mylog.log(v.params);
				label=(typeof v.name != "undefined") ? v.name: "Zones géographiques";
				str='<li class="dropdown">'+
						'<a href="javascript:;" class="dropdown-toggle menu-button btn-menu"  type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-label-xs="types" data-toggle="tooltip" data-placement="bottom">'+
							label+' <i class="fa fa-angle-down margin-left-5"></i>'+
						'</a>'+
						'<div class="dropdown-menu arrow_box scopeList'+k+'" style="overflow-y: auto;" aria-labelledby="dropdownTypes">'+
							'<div class="list-filters">';
						str+='</div></div>'+
					'</li>';


				return str;
			},
			favorites : function(k, v){
				var tooltipMsg=(typeof v.tooltips != "undefined") ? v.tooltips : trad.Favorites;
				var keyApp=(typeof pageApp != "undefined") ? pageApp : "search";
				str='<button type="button" class="btn-favorites-filters tooltips" data-app="'+keyApp+'" data-toggle="tooltip" data-placement="left" title="'+tooltipMsg+'">'+
						'<i class="fa fa-star"></i>'+
					'</button>';/*+
					'<div id="preview-favorites"></div>';*/
	            return str;
			},
			savedFilters : function(k, v) {
				var moreClass = v.class ? v.class : '';
				str = `<button 
							class="btn btn-filter-action saveActiveFilter disabled ${ moreClass }" 
							data-appKey="${v.appKey}"
							data-concernedForm="${v.concernedForm}"
						>
							<i class="fa fa-save"></i> ${trad.save} ${tradDynForm.Filters}
						</button>`;
				return str;
			},
			resetFilter : function(k, v) {
				var moreClass = v.class ? v.class : '';
				str = `<span 
							class="cursor-pointer reset-filter ${ moreClass }" 
						>
							${ v.label ? v.label : trad["Reset search"] }
						</span>`;
				return str;
			},
			text : function(k, v){
				placeholder=(typeof v.placeholder != "undefined") ? v.placeholder : trad.whatlookingfor;
				const icon = typeof v.icon != "undefined" ? v.icon : 'fa-arrow-circle-right';
				customClass = typeof v.customClass != "undefined" ? v.customClass : '';
				inputAlias = typeof v.inputAlias != "undefined" ? v.inputAlias : '';
				str='<div class="searchBar-filters pull-left '+ customClass +'">'+
	                '<input type="text" class="form-control pull-left text-center main-search-bar search-bar" data-field="'+k+'" data-text-path="'+(typeof v.field != "undefined" ? v.field : "" )+'" data-search-by="'+(typeof v.searchBy != "undefined" ? v.searchBy : "" )+'" placeholder="'+placeholder+'">'+
	                '<span class="text-white input-group-addon pull-left main-search-bar-addon " data-field="'+k+'" data-icon="'+icon+'" '+(inputAlias != '' ? 'data-alias="'+inputAlias+'"' : "")+' >'+
	                    '<i class="fa '+icon+'"></i>'+
	                '</span>'+
	            	"</div>";
	            	return str;
			},
			types : function(k, v, fObj){
				mylog.log("searchObj.views.types", k,v);
				label=(typeof v.name != "undefined") ? v.name: "Types";
				fObj.filters.actions.types.initList=v.lists;
				str='<li class="dropdown">'+
						'<a href="javascript:;" class="dropdown-toggle menu-button btn-menu"  type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-label-xs="types" data-toggle="tooltip" data-placement="bottom">'+
							label+' <i class="fa fa-angle-down margin-left-5"></i>'+
						'</a>'+
						'<div class="dropdown-menu arrow_box" style="overflow-y: auto;" aria-labelledby="dropdownTypes">'+
							'<div class="list-filters">';
					        $.each(v.lists, function(k,type){
					            elt=directory.getTypeObj(type);
					            textColor= (typeof elt.color != "undefined") ? "text-"+elt.color : "";
					            str+='<button type="button" class="btn padding-10 col-xs-12 '+textColor+'" data-field="'+type+'" data-key="'+type+'" data-type="types" data-value="'+ucfirst(elt.name)+'">'+
					                    '<i class="fa fa-'+elt.icon+'"></i> '+
					                    '<span class="elipsis label-filter '+textColor+'">'+ucfirst(elt.name)+'</span>'+
					                    '<span class="count-badge-filter '+textColor+'" id="count'+type+'"></span>'+
					                '</button>';
					        });

							if(typeof userConnected != "undefined" && userConnected != null && typeof userConnected.roles != "undefined"  && typeof userConnected.roles.superAdmin != "undefined"  && userConnected.roles.superAdmin){
								str+='<button type="button" class="btn padding-10 col-xs-12 text-red" data-field="spam" data-key="spam" data-type="types" data-value="spam">'+
					                    '<i class="fa fa-exclamation-circle"></i> '+
					                    '<span class="elipsis label-filter text-red">'+ucfirst("spam")+'</span>'+
										'<span class="count-badge-filter  text-red" id="countspam"></span>'+
					                '</button>';
							}
						str+='</div>'+
						'</div>'+
					'</li>';
				return str;
			},

			odd : function(k,v){
				var str = "";
				var fObj = this;
				str +='<li class="dropdown dropdown-tags" >'+
							'<a href="javascript:;" class="dropdown-toggle menu-button btn-menu text-dark pull-left" '+
								' data-label-xs="tags" type="button" id="dropdownTags" data-toggle="dropdown" '+
								' aria-haspopup="true" aria-expanded="false" data-toggle="tooltip" data-placement="bottom"'+
								'title="ODD" alt="ODD">'+"ODD "+' <i class="fa fa-angle-down"></i></a>'+
							'<div class="dropdown-menu arrow_box hidden-xs" aria-labelledby="dropdownTags" style="width : 500px !important">'+
								'<div class="col-xs-12 no-padding divSearchTheme divSearchThemeOdd">'+
									'<input id="inputSearchOdd" type="text" class="inputSearchOdd col-sm-7 col-xs-9 no-padding" data-role="tagsinput" name="tags" value="">'+
									'<button type="button" class="btn btn-default letter-green col-sm-2 col-xs-1 btn-odd-start-search">'+
										'<i class="fa fa-arrow-circle-right"></i> <span class="hidden-xs">'+trad.Validate+'</span>'+
									'</button>'+
									'<button type="button" class="btn btn-default letter-blue col-sm-3 col-xs-2 btn-odd-refresh ">'+
										'<i class="fa fa-refresh"></i> <span class="hidden-xs">'+trad.Refresh+'</span>'+
									'</button>'+
								'</div>'+
								'<div class="col-xs-12 no-padding divBtnOdd list-filters" style="max-height:350px!important;">';



					str +=	'</div>'+
						'</div>'+
					'</li>';
				return str;
			},
			
			tags : function(k,v){
				mylog.log("searchObj.views.tags", k,v);
				var str = "";
				return str;
			},
			imgIconBadgeAndLabel : function(fObj, k, v, vobj){
				var str="";
				var lbl = ( exists(v.label) ) ? v.label : k;
				var buttonLabel= ( vobj.noLabel ) ? "" : lbl;

				if(typeof v.img != "undefined"){
					var h = (vobj.imgConfig && vobj.imgConfig.h ) ? vobj.imgConfig.h : "50px";
					str ='<img style="height:'+h+'" src="'+v.img+'" class="img-responsive btnImg"/>';
				}
				else if(typeof v.icon != "undefined")
					str ='<i class="fa fa-'+v.icon+' fa-2x"></i>';
				if(typeof v.count != "undefined"){
					if(notEmpty(str)){
						str=//'<div class="fit-content pull-left">'
								//+
								str+
								'<span class="badge counter">'+v.count+'</span>'+
								'<div class="text-center col-xs-12">'+buttonLabel+'</div>';
							//'</div>';
					}
					else
						str=buttonLabel+' <span class="badge counter">'+v.count+'</span>';
				}else{
					str=str+buttonLabel;
				}
				return str;
			},
			labelAndActionBtn : function(fObj, k, v, vobj){
				var str="";
				var lbl = ( exists(v.label) ) ? v.label : k;
				var buttonLabel= ( vobj.noLabel ) ? "" : lbl;
				mylog.log("vObj", vobj)
				if(v.btnAction) {
					if(Object.keys(v.btnAction).length > 0) {
						str += "<div class='btn-group pull-right'>"
						$.each(v.btnAction, function(i, sub){
							actionIcon = sub.icon ? sub.icon : "fa-pencil";
							classAction = sub.action ? sub.action : i;
							classParent = sub.classParent ? sub.classParent : "";
							str += `<a href="javascript:;" class="${classAction+' '+classParent} mr-2" data-key="${k}" data-appKey="${vobj.appKey}" data-concernedForm="${vobj.concernedForm}" data-index="${k}" data-label="${v.label}" data-description="${v.description}"><i class="fa ${actionIcon}"></i></a>`
						});
						str += "</div>"
					}
				}
				return str;
			},
			select : function(k,v){
				mylog.log("searchObj.views.select", k,v);
				var str = "<select>";
				str += '<option value="-1" >ALL</option>';
				if(typeof v.list != "undefined" && v.list != null){
					$.each( v.list ,function(kL,vL){
						str +='<option value="'+vL+'" >'+vL+'</option>';
					});
				}
				str += "</select>";
				return str;
			},
			buttonList:function(k, v, classButton, fObj){
				var str = "";
				var dataEvent="data-event='"+((typeof v.event != "undefined") ? v.event : k)+"'";
				var dataKFilter="data-filterK='"+k+"'";
				var dataActive= (exists(v.active) && v.active) ? "data-active='true' " : "";
				var dataType= "data-type='"+((exists(v.type)) ? v.type : k)+"'";
				var dataMultiple=(typeof v.multiple != "undefined") ? "data-multiple='"+v.multiple+"'" :"";
				// ["btn1","btn2",]

				if(exists(v.countFieldPath)){
					if(!exists(fObj.filters.views.countFieldPath))
						fObj.filters.views.countFieldPath = [];
					fObj.filters.views.countFieldPath.push(v.countFieldPath);
				}

				if(exists(v.countOnlyPath)){
					if(!exists(fObj.filters.views.countOnlyPath))
						fObj.filters.views.countOnlyPath = [];
					fObj.filters.views.countOnlyPath.push(v.countOnlyPath);
				}
				if(typeof v.list != "undefined" && v.list != null){
					//includeSubList=(typeof v.list=="object" && !Array.isArray(v.list)) ? true : false;
					$.each( v.list ,function(kL,vL){
						mylog.log("searchObj.views.selectList v.list", kL,vL);
						var dataField=(typeof v.field != "undefined") ? "data-field='"+v.field+"'" : "";
						var buttonLabel = vL;
						var dataLabel = "";
						var counterBadge = "";
						if(exists(v.activateCounter) && v.activateCounter){
							counterBadge = ' <span class="label label-default badge-theme-count text-white '+((v.remove0)?"remove0":"")+'" data-countkey="'+kL.toString().toLowerCase().trim().replace(/\s/g, '').replace(/"/g, '&quot;')+'" data-countvalue="'+vL.toString().toLowerCase().trim().replace(/\s/g, '').replace(/"/g, '&quot;')+'" data-countlock="false">0</span>';
						}
						if( (typeof v.trad =="undefined" || v.trad!=false) && typeof tradCategory[vL] != 'undefined'){
							buttonLabel = tradCategory[vL];
							dataLabel= "data-label='"+escapeHtml(buttonLabel)+"'";
						}else{
							dataLabel= "data-label='"+escapeHtml(buttonLabel)+"'";
						}
						if( exists(v.event) && v.event=="sortBy" ){
							var dataLabel = kL;
							var dataKey = Object.keys(vL)[0];
							var dataValue = Object.values(vL)[0];
							var dataIsRadio = (typeof v.isRadioButton != "undefined" && v.isRadioButton[dataKey] ? `data-isradio='${v.isRadioButton[dataKey].toString()}'` : "")
							var checkmarkSpan = '';
							if(classButton.includes('accordion-row')) {
								checkmarkSpan = '<span class="accordion-checkmark"></span>';
							}
							str +='<button type="button" class="'+classButton+' btn-filters-select '+k+'" '+
										dataType+' '+
										'data-key="'+dataKey+'" '+
										'data-value="'+dataValue+'" '+
										'data-label="'+dataLabel+'" '+
										dataIsRadio +
										dataEvent+' '+
										dataKFilter+' '+
										dataActive+'>'+
										checkmarkSpan+
										'<span class="label-text">' + dataLabel + '</span>'
									'</button>';
						}else if(typeof vL == "string"){
							keyButton=(exists(v.keyValue) && (!v.keyValue || v.keyValue==="false")) ? kL:vL;
							valueButton=(typeof v.list=="object" && exists(v.keyValue) && (!v.keyValue || v.keyValue==="false")) ? keyButton : vL;
							var checkmarkSpan = '';
							if(classButton.includes('accordion-row')) {
								checkmarkSpan = '<span class="accordion-checkmark"></span>';
							}  
							str +=	'<button type="button" class="btn-filters-select '+k+' '+classButton+'" '+
										dataType+' '+
										'data-key="'+keyButton+'" '+
										'data-value="'+valueButton+'" '+
										dataField+' '+
										dataLabel+' '+
										dataEvent+' '+
										dataKFilter+' '+
										dataActive+' '+
										dataMultiple+'>'+
											checkmarkSpan+
											'<span class="label-text">'+ buttonLabel + '</span>'+
											counterBadge+
									'</button>';
						} else if( exists(v.typeList) && v.typeList=="object" ){
							var valueFilter=(exists(vL.value)) ? vL.value : kL;
							var keyButton=(exists(vL.key)) ? vL.key : kL;
							var dataLabel=(exists(vL.label)) ? "data-label='"+vL.label+"'" : "";
							var dataField=(exists(vL.field)) ? "data-field='"+vL.field+"'" : dataField;
							var htmlContentButton=fObj.filters.views.imgIconBadgeAndLabel(fObj, kL, vL, v);
							var lbl = ( exists(vL.label) ) ? vL.label : kL;
							var dataTooltip = ( v.tooltip ) ? "data-toggle='tooltip' data-placement='"+v.tooltip+"' title='"+lbl+"' " : '';
							dataLabel=(exists(vL.label)) ? "data-label='"+vL.label+"'" : "";
							dataField=(exists(vL.field)) ? "data-field='"+vL.field+"'" : dataField;
							var otherField = (exists(v.otherField) && exists(v.filterByTag) && ((exists(vL.label) && v.filterByTag.includes(vL.label)) || v.filterByTag.includes(kL))) ? "data-otherfield='"+v.otherField+"'" : '';
							//var badgeCount=(exists(vL.count)) ? " "+fObj.filters.views.badge(fObj, vL.count) : "";
							var useOr = (exists(vL.or)) ? vL.or : false;
							var dataOr = useOr ? "data-or="+useOr : '';
							var checkmarkSpan = '';
							if(classButton.includes('accordion-row')) {
								checkmarkSpan = '<span class="accordion-checkmark"></span>';
							}
							mylog.log("OtherField", otherField);
							str +='<button type="button" class="'+classButton+' btn-filters-select '+k+'" '+
										dataType+' '+
										'data-key="'+keyButton+'" '+
										'data-value="'+(typeof valueFilter == "object" ? valueFilter.join("\\") : valueFilter)+'" '+
										dataField+' '+
										otherField+' '+
										dataLabel+' '+
										dataEvent+' '+
										dataKFilter+' '+
										dataActive+' '+
										dataMultiple+' '+
										dataOr+' '+
										dataTooltip+'>'+
										checkmarkSpan+
											htmlContentButton+
									'</button>';
						}else{
							var checkmarkSpan = '';
							if(notEmpty(classButton) && classButton.includes('accordion-row')) {
								checkmarkSpan = '<span class="accordion-checkmark"></span>';
							}
							str +='<button type="button" class="mainTitle '+classButton+' btn-filters-select '+k+'" '+
										dataField+' '+
										dataType+' '+
										'data-value="'+kL+'" '+
										dataEvent+' '+
										dataKFilter+' '+
										dataMultiple+'>'+
										checkmarkSpan+
											'<span class="label-text">' + kL + '</span>'
									'</button>';
							if(Object.keys(vL).length > 0){
								$.each(vL, function(i, sub){
									str +='<button type="button" class="'+classButton+' btn-filters-select '+k+'" '+
												dataField+' '+
												dataType+' '+
												'data-value="'+sub+'" '+
												dataEvent+' '+
												dataKFilter+' '+
												dataMultiple+'>'+
													'<span class="label-text">'+ sub + '</span>'
											'</button>';
								});
							}
						}
					});
				}
				return str;
			},
			labelMap:function(k, v, fObj){
				mylog.log("searchObj.views.labelMap", k,v, fObj);
				var style = typeof fObj.mapObj.legende != "undefined" ? '' : 'style="display: none;"';
				var labelStr=(typeof v.name != "undefined") ? v.name: "Legende du Map";
				var classContainer= (typeof v.classDom != "undefined") ? v.classDom : "";
				var str='<li class="dropdown labelMap" '+style+'>'+
							'<a href="javascript:;" class="dropdown-toggle menu-button btn-menu"  type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-label-xs="types" data-toggle="tooltip" data-placement="bottom">'+
								labelStr+' <i class="fa fa-angle-down margin-left-5"></i>'+
							'</a>'+
							'<div class="dropdown-menu arrow_box '+classContainer+'" style="overflow-y: auto;" aria-labelledby="dropdownTypes">'+
								'<div class="list-filters">';
						$.each(v.lists, function(kL, vL){
							str += '<button type="button" class="btn-filters-select '+v.type+' col-xs-12" data-type="'+v.type+'" data-key="'+vL.name+'" data-value="'+kL+'" data-event="'+v.event+'" data-filterk="'+v.type+'">'+(vL.name)+'</button>';
						});		
				str +='</div>'+
							'</div>'+
						'</li>';
				return str;
			},
			buttonSavedList:function(k, v, classButton, fObj, withActionBtn = true,){
				var str = "";
				var dataEvent="data-event='"+((typeof v.event != "undefined") ? v.event : k)+"'";
				var dataActive= (exists(v.active) && v.active) ? "data-active='true' " : "";
				if(typeof v.list != "undefined" && v.list != null){
					$.each( v.list ,function(kL,vL) {
						var dataField=(typeof v.field != "undefined") ? "data-field='"+v.field+"'" : "";
						var buttonLabel = vL;
						var dataLabel=(exists(vL.label)) ? "data-label='"+vL.label+"'" : "";
						if(typeof tradCategory[vL] != 'undefined'){
							buttonLabel = tradCategory[vL];
						}
						if( exists(v.typeList) && v.typeList=="arrayOfObjects" ){
							mylog.log("keyButton", kL, typeof kL)
							if(vL.label) {
								var keyButton=(exists(vL.key)) ? vL.key : kL;
								if(withActionBtn) {
									vL.btnAction = {
										edit : {
											icon: "fa-pencil text-info",
											action: "editSavedFilter",
											classParent: k
										},
										delete : {
											icon: "fa-trash text-danger",
											action: "deleteSavedFilter",
											classParent: k
										}
									}
								}
								var htmlContentButton=fObj.filters.views.labelAndActionBtn(fObj, kL, vL, v);
								var lbl = ( exists(vL.description) ) ? vL.description : kL;
								var dataTooltip = ( v.tooltip ) ? `data-title='${lbl}' ` : '';
								var tooltipClass = ( v.tooltip) ? "tooltips" : "";
								var useOr = (exists(vL.or)) ? vL.or : false;
								var dataOr = useOr ? "data-or="+useOr : '';
								var dataLabelActivateFilters = vL.labelActivateFilters ? `data-labelActivateFilters="${vL.labelActivateFilters}"` : "";
								if(typeof fObj.dataForFilters == 'undefined')
									fObj.dataForFilters = {}
								vL.allBtnAttributes ? fObj.dataForFilters[kL] = vL.allBtnAttributes : "";
								if(typeof fObj.dataFilters == 'undefined')
									fObj.dataFilters = {}
								vL.data ? fObj.dataFilters[kL] = JSON.parse(vL.data) : "";
								vL.labelActivateFilters ? fObj.labelActivateFilters = {[kL] : vL.labelActivateFilters} : "";
								
								str +=`<div class="${classButton} no-padding">
											<button type="button" class="${classButton} col-xs-8 btn-saved-filters-select ${k} ${tooltipClass}"
												data-key="${keyButton}" 
												${dataLabel} 
												${dataOr} 
												${dataLabelActivateFilters} 
												${dataTooltip} >
												${vL.label}
											</button>
											${htmlContentButton}
										</div>`;
							}
						}else{
							str +='<button type="button" class="mainTitle '+classButton+' btn-filters-select '+k+'" '+
										dataField+' '+
										dataType+' '+
										'data-value="'+kL+'" '+
										dataEvent+' '+
										dataKFilter+' '+
										dataMultiple+'>'+
											kL+
									'</button>';
							if(Object.keys(vL).length > 0){
								$.each(vL, function(i, sub){
									str +='<button type="button" class="'+classButton+' btn-filters-select '+k+'" '+
												dataField+' '+
												dataType+' '+
												'data-value="'+sub+'" '+
												dataEvent+' '+
												dataKFilter+' '+
												dataMultiple+'>'+
													sub+
											'</button>';
								});
							}
						}
					});
				}
				return str;
			},
			horizontalList : function(k,v, fObj){
				mylog.log("searchObj.views.selectList", k,v);
				var labelStr=(typeof v.name != "undefined") ? v.name: "";
				var classButton= (typeof v.classList != "undefined") ? v.classList+" btn-list-horizontal" : "btn-list-horizontal";
				var classContainer= (typeof v.classDom != "undefined") ? v.classDom : "";
				mylog.log("testsaa",fObj.filters.dom);
				var str='<div class="col-xs-12 horizontalList '+k+'FiltersClass '+classContainer+'">'+
							'<span class="pull-left btn">'+labelStr+'</span>';
					var seeallBtn = (typeof v.multiple != "undefined" && v.multiple) ? '<button type="button" class="removeAllActiveFilters '+classButton+'" data-domfilter="'+((typeof v.dom != "undefined") ? v.dom : fObj.filters.dom)+'" data-filterk="'+k+'"><i class="fa fa-refresh"></i> '+trad.seeall+'</button>' : "";
					if(typeof v.seeall == "undefined" || v.seeall)
						str+= seeallBtn;
					str+=		fObj.filters.views.buttonList(k, v, classButton, fObj);
					if(typeof v.seeall != "undefined" && v.seeall == "after")
						str+= seeallBtn;
				str+=	'</div>';
				
				return str;
			},
			verticalList : function(k,v, fObj){
				mylog.log("searchObj.views.selectList", k,v);
				var labelStr=(typeof v.name != "undefined") ? v.name: "";
				var classButton= (typeof v.classList != "undefined") ? v.classList+" btn-list-horizontal" : "btn-list-horizontal";
				var classContainer= (typeof v.classDom != "undefined") ? v.classDom : "";
				mylog.log("testsaa",fObj.filters.dom);
				var str='<div class="col-xs-12 verticalList '+k+'FiltersClass '+classContainer+'">';
							if(notEmpty(labelStr))
							str+='<span class="pull-left btn">'+labelStr+'</span>';
					var seeallBtn = (typeof v.multiple != "undefined" && v.multiple) ? '<button type="button" class="removeAllActiveFilters btn '+classButton+'" data-domfilter="'+((typeof v.dom != "undefined") ? v.dom : fObj.filters.dom)+'" data-filterk="'+k+'"><i class="fa fa-refresh"></i> '+trad.seeall+'</button>' : "";
					if(typeof v.seeall == "undefined" || v.seeall)
						str+= seeallBtn;
					str+=fObj.filters.views.buttonList(k, v, classButton, fObj);
					if(typeof v.seeall != "undefined" && v.seeall == "after")
						str+= seeallBtn;
					str+=	'</div>';
						
				return str;
			},
			accordionList : function(k, v, fObj) {
				var labelStr=(typeof v.name != "undefined") ? v.name: "Ajouter un filtre";
				var classButton= (typeof v.classList != "undefined") ? v.classList : "col-xs-12";
				var classContainer= (typeof v.classDom != "undefined") ? v.classDom : "";
				var str = `
					<div class="${classContainer}">
						<div class="panel-heading col-xs-12 cursor-pointer accordion-bg-hover tr-all" data-toggle="collapse" data-parent="#filterContainerInside" href="#filterCollapse-${k}">
							<h4 class="panel-title" style="font-size: 14px; text-transform: none; display: flex !important; justify-content: space-between !important">
								<span>${labelStr}</span>
								<span class="fa fa-caret-down animate__fadeInDown animate__fadeOutUp"></span>
							</h4>
						</div>
						<div id="filterCollapse-${k}" class="panel-collapse collapse animate__fadeInDown animate__fadeOutUp">
							<div class="panel-body row" style="border: none !important">
								${fObj.filters.views.buttonList(k, v, 'accordion-row '+classButton, fObj)}
							</div>
						</div>
					</div>
				`;
				return str;
			},
			megaMenuAccordion : function(k,v, fObj){
				var labelStr=(typeof v.name != "undefined") ? v.name: "Ajouter un filtre";
				if(!exists(fObj.filters.views.countFieldPath))
					fObj.filters.views.countFieldPath = [];

				if(exists(v.countFieldPath))
					fObj.filters.views.countFieldPath.push(v.countFieldPath);
				var megaMenu = '';
				// if(typeof v.list === 'object' && v.list !== null && !Array.isArray(v.list)){
				// 	megaMenu = '<div class="dropdown dropdown-large"><a href="javascript:;"  class="dropdown-toggle menu-button btn-menu mega-menu-dropdown"  type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-label-xs="theme" data-toggle="tooltip" data-placement="bottom">'+labelStr+'<b class="caret"></b></a><div class="dropdown-menu dropdown-menu-large" style="overflow-y: auto;" aria-labelledby="dropdownTypes"><div class="mega-menu-container" style="width:100%"></div>';
				// 	for (const [keyT, valueT] of Object.entries(v.list)) {
			    //     	megaMenu += `<div class="col"><div><h5 class="col-12 dropdown-title">${keyT}</h5>`;
			    //     	for (const [keyChild, valueChild] of Object.entries(valueT)){
			    //     		var newValue = {list:{}, event:v.event, activateCounter:true, remove0:(v.remove0)?v.remove0:false};
			    //     		newValue.list[keyChild] = valueChild;
			    //     		if(typeof v.field != "undefined"){
			    //     			newValue["field"] = v.field;
			    //     		}
			    //     		if(typeof v.type != "undefined"){
			    //     			newValue["type"] = v.type;
			    //     		}
				//             megaMenu+=`<div class="col-12">${fObj.filters.views.buttonList(k, newValue, "thin", fObj)}</div>`;
				//         }
			    //     	megaMenu += '<br/></div></div>';
				//     }

				//     megaMenu += `</div></div>`;
		        // }else{

					var classContainer= (typeof v.classDom != "undefined") ? v.classDom : "";
					var classButton= (typeof v.classList != "undefined") ? v.classList : "col-xs-12";
		            megaMenu+=`
					<div class="${classContainer}">
						<div class="panel-heading col-xs-12 cursor-pointer accordion-bg-hover tr-all" data-toggle="collapse" data-parent="#filterContainerInside" href="#filterCollapse-${k}">
							<h4 class="panel-title" style="font-size: 14px; text-transform: none; display: flex !important; justify-content: space-between !important">
								<span>${labelStr}</span>
								<span class="fa fa-caret-down animate__fadeInDown animate__fadeOutUp"></span>
							</h4>
						</div>
						<div id="filterCollapse-${k}" class="panel-collapse collapse animate__fadeInDown animate__fadeOutUp">
							<div class="panel-body row" style="border: none !important">
								${fObj.filters.views.buttonList(k, v, 'accordion-row '+classButton, fObj)}
							</div>
						</div>
					</div>`;
		        // }
				return megaMenu;
			},
			accordionListSavedFilter : function(k, v, fObj) {
				var labelStr=(typeof v.name != "undefined") ? v.name: "Ajouter un filtre";
				var classButton= (typeof v.classList != "undefined") ? v.classList : "col-xs-12";
				var classContainer= (typeof v.classDom != "undefined") ? v.classDom : "";
				var str = `
					<div>
						<div class="panel-heading col-xs-12 cursor-pointer accordion-bg-hover tr-all" data-toggle="collapse" data-parent="#filterContainerInside" href="#filterCollapse-${k}">
							<h4 class="panel-title" style="font-size: 14px; text-transform: none; display: flex !important; justify-content: space-between !important">
								<span>${labelStr}</span>
								<span class="fa fa-caret-down animate__fadeInDown animate__fadeOutUp"></span>
							</h4>
						</div>
						<div id="filterCollapse-${k}" class="panel-collapse collapse animate__fadeInDown animate__fadeOutUp">
							<div class="panel-body row" style="border: none !important">
								${fObj.filters.views.buttonSavedList(k, v, 'saved-filter-accordion-row my-1 '+classButton, fObj)}
							</div>
						</div>
					</div>
				`;
				return str;
			},
			themesList : function(k, v){
				var str = "";
				var fObj = this;
				str +=`<div class="dropdown dropdown-tags col-xs-12 no-padding" >
							<div class="panel-heading col-xs-12 dropdown-toggle cursor-pointer pull-left accordion-bg-hover tr-all"
								data-label-xs="tags" type="button" id="dropdownTags" data-toggle="dropdown"
								aria-haspopup="true" aria-expanded="false" data-toggle="tooltip" data-placement="bottom"
								title="Thématiques" alt="Thématiques"
							>
								<h4 class="panel-title" style="font-size: 14px; text-transform: none; display: flex !important; justify-content: space-between !important">
									<span>${ trad.Thematic }</span>
									<span class="fa fa-caret-down animate__fadeInDown animate__fadeOutUp"></span>
								</h4>
							</div>
							<div class="dropdown-menu arrow_box hidden-xs" aria-labelledby="dropdownTags" style="width : 600px !important">
								<div class="col-xs-12 no-padding divSearchTheme">
									<input id="inputSearchThemes" type="text" class="inputSearchThemes col-sm-8 col-xs-10 no-padding" data-role="tagsinput" name="tags" value="">
									<button type="button" class="btn btn-default letter-green col-sm-2 col-xs-1 btn-theme-start-search">
										<i class="fa fa-arrow-circle-right"></i> <span class="hidden-xs">${ trad.Validate }</span>
									</button>
									<button type="button" class="btn btn-default letter-blue col-sm-2 col-xs-1 btn-theme-refresh ">
										<i class="fa fa-refresh"></i> <span class="hidden-xs">${ trad.Refresh }</span>
									</button>
								</div>
								<div class="col-xs-12 no-padding divBtnTheme">
								</div>
							</div>
						</div>`;
				return str;
			},
			extraUrl : function(k,v,fObj){
				mylog.log("searchObj.views.extraUrl", k,v);
                var labelStr=(typeof v.name != "undefined") ? v.name: "";
				var classButton= (typeof v.classList != "undefined") ? v.classList : "";
				var classContainer= (typeof v.classDom != "undefined") ? v.classDom : "";
				var str='<div class="col-xs-12 horizontalList '+k+'FiltersClass '+classContainer+'">'+
							'<span class="pull-left btn">'+labelStr+'</span>';
							$.each(v.list, function (kL,vL){
					str+=       '<button type="button" class="'+classButton+' btn-filters-select '+k+'" data-type="'+v.type+'" data-field="'+fObj.urlData+'" data-value="'+kL+'" data-extra-url="'+vL+'">'+kL+'</button>';
                            });
                    str+='</div>';
                return str;         
			},
			dropdownList : function(k,v, fObj){
				mylog.log("searchObj.views.selectList", k,v);
				var labelStr=(typeof v.name != "undefined") ? v.name: "Ajouter un filtre";
				var classButton= (typeof v.classList != "undefined") ? v.classList : "col-xs-12";
				var classContainer= (typeof v.classDom != "undefined") ? v.classDom : "";
				var str='';
				if(Object.keys(v.list).length == 1){
					classButton += " btn-menu"
					str = `<li class="dropdown">
					${fObj.filters.views.buttonList(k, v, classButton, fObj)}
					</li>`;
				}else{
					str = '<li class="dropdown">'+
						'<a href="javascript:;" class="dropdown-toggle menu-button btn-menu"  type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-label-xs="'+labelStr+'" data-toggle="tooltip" data-placement="bottom">'+
							labelStr+' <i class="fa fa-angle-down margin-left-5"></i>'+
						'</a>'+
						'<div class="dropdown-menu arrow_box '+classContainer+'" style="overflow-y: auto;" aria-labelledby="dropdownTypes">'+
							'<div class="list-filters">'+
									fObj.filters.views.buttonList(k, v, classButton, fObj)+
							'</div>'+
						'</div>'+
					'</li>';
				}
				return str;
			},
			selectMultiple : function(k,v, fObj){
				mylog.log("searchObj.views.selectMultiple", k,v.list);
				var str = "<div class='selectMultiple'>"+
					"<div class=' '><label>"+v.name+"<label></div>"+
					'<input id="selectMultiple'+k+'" class="'+k+'" data-field ="'+v.field+'"  data-type="'+v.type+'" type="text" data-type="select2">'+
				"</div>";
				
				setTimeout(function(){					
					$("#selectMultiple"+k).select2({
						data:v.list, 
						tags : true, 
					});  
				},100)
					
				
				return str;
			},
			megaMenuDropdown : function(k,v, fObj){

				var labelStr=(typeof v.name != "undefined") ? v.name: "Ajouter un filtre";
				if(!exists(fObj.filters.views.countFieldPath))
					fObj.filters.views.countFieldPath = [];

				if(exists(v.countFieldPath))
					fObj.filters.views.countFieldPath.push(v.countFieldPath);
				var megaMenu = '';
				if(typeof v.list === 'object' && v.list !== null && !Array.isArray(v.list)){
					megaMenu = '<div class="dropdown dropdown-large"><a href="javascript:;"  class="dropdown-toggle menu-button btn-menu mega-menu-dropdown"  type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-label-xs="theme" data-toggle="tooltip" data-placement="bottom">'+labelStr+'<b class="caret"></b></a><div class="dropdown-menu dropdown-menu-large" style="overflow-y: auto;" aria-labelledby="dropdownTypes"><div class="mega-menu-container" style="width:100%"></div>';
					for (const [keyT, valueT] of Object.entries(v.list)) {
			        	megaMenu += `<div class="col"><div><h5 class="col-12 dropdown-title">${keyT}</h5>`;
			        	for (const [keyChild, valueChild] of Object.entries(valueT)){
			        		var newValue = {list:{}, event:v.event, activateCounter:(typeof v.activeCounter != "undefined")?v.activateCounter:true, remove0:(v.remove0)?v.remove0:false};
							newValue.list[keyChild] = valueChild;
			        		if(typeof v.field != "undefined"){
			        			newValue["field"] = v.field;
			        		}
			        		if(typeof v.type != "undefined"){
			        			newValue["type"] = v.type;
			        		}
				            megaMenu+=`<div class="col-12">${fObj.filters.views.buttonList(k, newValue, "thin", fObj)}</div>`;
				        }
			        	megaMenu += '<br/></div></div>';
				    }

				    megaMenu += `</div></div>`;
		        }else{

					var classContainer= (typeof v.classDom != "undefined") ? v.classDom : "";
					var classButton= (typeof v.classList != "undefined") ? v.classList : "col-xs-12";
		            megaMenu+='<li class="dropdown">'+
							'<a href="javascript:;" class="dropdown-toggle menu-button btn-menu"  type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-label-xs="types" data-toggle="tooltip" data-placement="bottom">'+
								labelStr+' <i class="fa fa-angle-down margin-left-5"></i>'+
							'</a>'+
							'<div class="dropdown-menu arrow_box '+classContainer+'" style="overflow-y: auto;" aria-labelledby="dropdownTypes">'+
								'<div class="list-filters">'+
										fObj.filters.views.buttonList(k, v, classButton , fObj)+
								'</div>'+
							'</div>'+
						'</li>';
		        }
				return megaMenu;
			},
			largeMenuDropdown : function(k, v, fObj){
				filterList = fObj.filters.views.buildBtnListWithImg(fObj, k, v, (v.colClass?v.colClass:"col-md-3 col-sm-4 col-xs-6"));
				var str = `
				<li class="dropdown position-static">
					<a data-dropdown-init class="dropdown-toggle menu-button btn-menu" href="#" id="navbarDropdown" role="button"
						data-toggle="dropdown" type="button" aria-expanded="false">
						${v.name}
						<i class="fa fa-angle-down margin-left-5"></i>
					</a>
					<div class="dropdown-menu w-100" aria-labelledby="navbarDropdown" style="border-top-left-radius: 0; border-top-right-radius: 0;">
						<div class="container-fluid">
							<div class="row ">
								${(filterList?filterList:"")}
							</div>
						</div>
          			</div>
				</li>`;
				return str;
			},
			buildBtnListWithImg : function(fObj, k, v, widthClass="col-md-4 col-sm-6 col-xs-12"){
				var str = "";
				if(typeof v.list != "undefined" && notNull(v.list)){
					v.lists = v.list;
				}
				if(typeof v.lists != "undefined" && notNull(v.lists)){
					fObj.filters.lists.odd = v.lists;
					$.each(v.lists, function(key, odd){
						if(typeof odd == "string"){
							odd = {
								name: odd,
								color : "white",
								image:""
							}
						}

						if(typeof odd.name != "undefined" && odd.name != null){
							var nameTheme = typeof tradTags[odd.name.toLowerCase()] != "undefined" ?
							tradTags[odd.name.toLowerCase()].charAt(0).toUpperCase()+tradTags[odd.name.toLowerCase()].slice(1)
							: (odd.name).charAt(0).toUpperCase() + (odd.name).slice(1);
							str += `<div style="${(odd.image!="")?'height:120px!important;':''}" class="`+widthClass+`">
										<button style="background-color:`+ odd.color + `;"
											data-type="${v.type}"
											data-label="${odd.name}"
											data-event="${v.event}"
											data-filterK="${k}" 
											data-value="${odd.tags}"
											data-key="${odd.name}"
											class="btn-filters-select ${k} filter-item-with-image"
										>
											<div class="padding-5" >
												<h5 style="display:flex; color:${(odd.color=="white"?"black":"white")}; font-size:12px;">${(odd.image!="")?'<b class="margin-right-5 number-odd" style="font-size:18pt; font-weight:bolder">'+ key + ' </b>':''} <span class="title-odd"> `+ nameTheme + `</span></h5>
												<div class="text-center">
													${(typeof odd.image!="undefined" && odd.image!="")?`<img style="height:60px;"  src="`+ (co2AssetPath+odd.image) + `">`:""}
												</div>
											</div>
										<button>	
									</div>`;
						}
					});
				}
				return str;
			},
			buildComplexeList : function(inc, list, fObj, inputKey, value){
				var listStr="";
				$.each(list,function(kL,vL){
					var icon = (typeof vL.icon != "undefined") ? "<i class='fa fa-"+vL.icon+"'></i>" : "";
					var visibleClass = (typeof value.levelOptions[inc].visible == "undefined" || value.levelOptions[inc].visible == true) ? "" : "hidden";
					//if (typeof value.levelOptions[inc].visible != "undefined" && value.levelOptions[inc].visible == true) {
					var listLevelClass=(typeof value.levelOptions[inc].class != "undefined") ? value.levelOptions[inc].class : "col-md-3 col-sm-6 col-xs-12";
					var filterEventClass=(typeof value.levelOptions[inc].event == "undefined") ? "btn-filters-select" : "";
					var showNextLevelEvent=(typeof value.levelOptions[inc+1] != "undefined" && typeof value.levelOptions[inc+1].visible != "undefined" && !value.levelOptions[inc+1].visible) ? "openSubList" : "";
					var dataField=(typeof value.levelOptions[inc].field != "undefined") ? "data-field='"+value.levelOptions[inc].field+"'" : "";
					var dataType=(typeof value.levelOptions[inc].type != "undefined") ? value.levelOptions[inc].type : value.type;
					var dataMultiple=(typeof value.multiple != "undefined") ? "data-multiple='"+value.multiple+"'" :"";
					var keyButton=(exists(value.keyValue) && (!value.keyValue || value.keyValue==="false")) ? kL:vL.label;

					if(typeof vL.multiple != "undefined"){
						dataMultiple=(vL.multiple===true) ? "data-multiple='"+vL.multiple+"'" : "";
					}
					if(typeof vL.subList != "undefined")
						listStr +='<div class="complexList-level-container-'+inc+' '+inputKey+'-'+kL+'-container '+listLevelClass+' '+visibleClass+'">';

						listStr += '<button type="button" class="'+filterEventClass+' '+inputKey+' '+showNextLevelEvent+' lvl'+inc+' col-xs-12" data-stop-propagation="true" data-level="'+inc+'" data-next-level="'+(inc+1)+'" '+dataField+' '+dataMultiple+' data-type="'+dataType+'" data-key="'+kL+'" data-value="'+keyButton+'" >'+icon +" "+vL.label+'</button>';
						if(typeof value.levelOptions[inc].separator != "undefined" && value.levelOptions[inc].separator)
							listStr += '<hr class="">';
					if(typeof vL.subList != "undefined") {
						listStr+=fObj.filters.views.buildComplexeList((inc+1), vL.subList, fObj, inputKey, value);
						listStr +='</div>';
					}

				});
				return listStr;
			},
			complexeSelect : function(k,v, fObj){
				var labelStr = (typeof v.name != "undefined") ? v.name : "Ajouter un filtre";
				var str='<li class="dropdown">'+
							'<a href="javascript:;" class="dropdown-toggle menu-button btn-menu"  type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-label-xs="types" data-toggle="tooltip" data-placement="bottom">'+
								labelStr+' <i class="fa fa-angle-down margin-left-5"></i>'+
							'</a>'+
							'<div id="dropdownComplexe" class="dropdown-menu dropdown-menu-complexe filters-affix-dropdown row" style="overflow-y: auto;" aria-labelledby="dropdownComplexeSelect">'+
								'<div class="list-filters filterComplexe col-md-12">';
								if(typeof v.list != "undefined"){
										str+=fObj.filters.views.buildComplexeList(0, v.list, fObj, k, v);
								}
						str+='</div></div></li>';
				return str;
			},
			scope : function(k,v){
				mylog.log("searchObj.views scope");
				placeholder = (typeof v.name != "undefined") ? v.name : trad.where+" ?";
				return 	'<div id="costum-scope-search"><div id="input-sec-search">'+
							'<div class="input-group shadow-input-header">'+
								'<span class="input-group-addon scope-search-trigger">'+
									'<i class="fa fa-map-marker fa-fw" aria-hidden="true"></i>'+
								'</span>'+
								'<input type="text" class="form-control input-global-search" autocomplete="no"'+
									' id="searchOnCity" placeholder="'+placeholder+'">'+
							'</div>'+
							'<div class="dropdown-result-global-search col-xs-12 col-sm-5 col-md-5 col-lg-5 no-padding" '+
								' style="display: none;">'+
								'<div class="content-result">'+
								'</div>'+
							'</div>'+
					'</div></div>';
			},
			price : function(k,v){
				mylog.log("searchObj.views.themes ",k,v);
				var str = "";
				var fObj = this;
				str +='<li class="dropdown dropdown-tags" >'+
							'<a href="javascript:;" class="dropdown-toggle menu-button btn-menu  pull-left" '+
								' data-label-xs="price" type="button" id="dropdownPrice" data-toggle="dropdown" '+
								' aria-haspopup="true" aria-expanded="false" data-toggle="tooltip" data-placement="bottom"'+
								'title="Thématiques" alt="Thématiques">'+
								'Prix <i class="fa fa-angle-down"></i>'+
							'</a>'+
							'<div class="dropdown-menu arrow_box hidden-xs" aria-labelledby="dropdownPrice" style="width : 600px !important">'+
								'<div class="col-xs-12 no-padding divSearchPrice">'+
									'<div class="col-xs-12 col-sm-6">'+
										'<label class="col-xs-12 no-padding">'+trad.minPrice+'</label>'+
										'<input class="minPriceInput col-xs-12 no-padding" name="minPrice" placeholder="'+trad.minPrice+'">'+
									"</div>"+
									'<div class="col-xs-12 col-sm-6">'+
										'<label class="col-xs-12 no-padding">'+trad.maxPrice+'</label>'+
										'<input class="maxPriceInput col-xs-12 no-padding" name="maxPrice" placeholder="'+trad.maxPrice+'">'+
									"</div>"+
									'<button type="button" class="btn btn-default letter-green col-sm-4 col-xs-6 btn-price-start-search">'+
										'<i class="fa fa-arrow-circle-right"></i> <span class="">'+trad.search+'</span>'+
									'</button>'+
								'</div>'+
							'</div>'+
					'</li>';
				return str;
			},
			/* Système de filtre géolocalisé
				A finir: TODO checklist
				[] Ajouter dans la vue une dropdown permettant de régler le rayon de recherche
				[] Ajouter l'évenement lors du changement du rayon (ou avec bouton valider moins intrusif)
				[] Au validate, récupérer la valeur et l'envoyer => retravailler _function_ searchObj.filters.events.around
			*/
			around : function(){
				return '<div id="around-me-filters" class="filters-btn">'+
							"<span><i class='fa fa-street-view'></i> Around me</pan>"+
						'</div>'
			},
			themes : function(k,v){
				mylog.log("searchObj.views.themes ",k,v);
				var str = "";
				var fObj = this;
				str +='<li class="dropdown dropdown-tags" >'+
							'<a href="javascript:;" class="dropdown-toggle menu-button btn-menu  pull-left" '+
								' data-label-xs="tags" type="button" id="dropdownTags" data-toggle="dropdown" '+
								' aria-haspopup="true" aria-expanded="false" data-toggle="tooltip" data-placement="bottom"'+
								'title="Thématiques" alt="Thématiques">'+trad.Thematic+'<i class="fa fa-angle-down"></i></a>'+
							'<div class="dropdown-menu arrow_box hidden-xs" aria-labelledby="dropdownTags" style="width : 600px !important">'+
								'<div class="col-xs-12 no-padding divSearchTheme">'+
									'<input id="inputSearchThemes" type="text" class="inputSearchThemes col-sm-8 col-xs-10 no-padding" data-role="tagsinput" name="tags" value="">'+
									'<button type="button" class="btn btn-default letter-green col-sm-2 col-xs-1 btn-theme-start-search">'+
										'<i class="fa fa-arrow-circle-right"></i> <span class="hidden-xs">'+trad.Validate+'</span>'+
									'</button>'+
									'<button type="button" class="btn btn-default letter-blue col-sm-2 col-xs-1 btn-theme-refresh ">'+
										'<i class="fa fa-refresh"></i> <span class="hidden-xs">'+trad.Refresh+'</span>'+
									'</button>'+
								'</div>'+
								'<div class="col-xs-12 no-padding divBtnTheme">';




					str +=	'</div>'+
						'</div>'+
					'</li>';
				return str;
			},
			collapssibleList: function(k, v){
				var item = "",
					sub_items = "";
				for(let i in v.list){
					sub_items += `
						<li>
							<button type="button" class="btn" id="${generateIdFromString(v.list[i], "filter_")}" data-type='tags' data-value="${v.list[i]}" data-name="${k}">
								<i class="fa fa-check" aria-hidden="true"></i> ${v.list[i]}
							</button>
						</li>
					`;
				}
				item = `
					<div class="list-item-filters-map">
						<button type="button" id="filter-${k}">
							<i class="fa fa-${v.icon}" aria-hidden="true"></i>
							<span>${v.name}</span>
							<span class="badge counter"></span>
						</button>
						<ul>${sub_items}</ul>
					</div>
				`;
				return item;
			}
		},
		events : {
			text : function(fObj, domFilters){
				$(domFilters+" .main-search-bar").off().on("keyup",delay(function (e) {
					if(e.keyCode != 13)
						fObj.filters.actions.text.launch(fObj, $(this).data("field"), e, domFilters);

				}, 750)).on("keyup", function(e){
					if(e.keyCode == 13)
						fObj.filters.actions.text.launch(fObj, $(this).data("field"), e, domFilters);

				});
				$(domFilters+"  .main-search-bar-addon").off().on("click", function(){
					fObj.filters.actions.text.launch(fObj, $(this).data("field"), null, domFilters);
	            });
				
				$(domFilters.replace("#", ".")+".filter-alias.custom-filter  .main-search-bar-addon").off("click").on("click", function(e){
					fObj.filters.actions.text.launch(fObj, $(domFilters+" .searchBar-filters .main-search-bar-addon").data("field"));
	            });
			},
			types : function(fObj, domFilters){
				mylog.log("searchObj.events.type");
				$(domFilters+" button[data-type='types']").off().on("click",function(){
					fObj.filters.actions.types.add(fObj, $(this).data("key"));
					fObj.filters.manage.addActive(fObj, $(this), true, false);//"types", $(this).data("value"), $(this).data("key"), $(this).data("field")*/);

					//fObj.search.init(fObj, false);
				});
			},
			changeLabel: function(fObj, domFilters, k, v){
				$(domFilters+" ."+k+"[data-type='labelmap']").off().on("click",function(){
					var value = $(this).data("value");
					var key = $(this).data("key");
					var customMap=(typeof paramsMapD3CO.mapCustom != "undefined") ? paramsMapD3CO.mapCustom : {tile : "mapbox"};
					var mapContext = new MapD3({
						container : fObj.mapObj.parentContainer,
						activePopUp : true,
						showLegende: (typeof fObj.mapObj != "undefined" && typeof fObj.mapObj.showLegende != "undefined") ? fObj.mapObj.showLegende : true,
						clusterType : fObj.mapObj.clusterType,
						markerType : fObj.mapObj.markerType,
						dynamicLegende:false,
						legende: costum.lists[value],
						legendeVar: fObj.mapObj.legendeVar,
						legendeLabel: key,
						groupBy: fObj.mapObj.groupBy,
						mapOpt:{
							zoomControl:false
						},
						mapCustom: customMap,
						elts : {}
					});
						
					var comapOptions = mapContext.getOptions();
					fObj.mapObj = {
						showLegende: comapOptions.showLegende,
						clusterType : comapOptions.clusterType,
						markerType : comapOptions.markerType,
						dynamicLegende: comapOptions.dynamicLegende,
						legende: comapOptions.legende,
						legendeVar: comapOptions.legendeVar,
						legendeLabel: comapOptions.legendeLabel,
						groupBy: comapOptions.groupBy,
						parentContainer: comapOptions.parentContainer,
						map:mapContext.getMap(),
						addElts: function(elts){
							mapContext.addElts(elts)
						},
						clearMap: function(){
							mapContext.clearMap()
						},
						showLoader: function(){
							mapContext.showLoader()
						},
						fitBounds:function(){
							mapContext.fitBounds()
						}
					};
					fObj.search.init(fObj);
				});
			},
			buttonD3 : function(fObj, domFilters,k, v){
				$(domFilters+" ."+k+"[data-type='changemap']").off().on("click",function(){
					var value = $(this).data("value");
					var mapContext = null;
					if(value != "default"){
						var customMap=(typeof paramsMapD3CO.mapCustom != "undefined") ? paramsMapD3CO.mapCustom : {tile : "mapbox"};
						mapContext = new MapD3({
							container : fObj.mapObj.parentContainer,
							activePopUp : true,
							showLegende: (typeof fObj.mapObj != "undefined" && typeof fObj.mapObj.showLegende != "undefined") ? fObj.mapObj.showLegende : true,
							clusterType : value != "heatmap" ? value : "pie",
							markerType : (value != "heatmap") ? "default" : value,
							dynamicLegende: fObj.mapObj.dynamicLegende,
							legende: fObj.mapObj.legende,
							legendeVar: fObj.mapObj.legendeVar,
							legendeLabel: fObj.mapObj.legendeLabel,
							groupBy: fObj.mapObj.groupBy,
							mapOpt:{
								zoomControl:false
							},
							mapCustom: customMap,
							elts : {}
						});
						$(".labelMap").show();
						var comapOptions = mapContext.getOptions();
						fObj.mapObj = {
							showLegende: comapOptions.showLegende,
							clusterType : comapOptions.clusterType,
							markerType : comapOptions.markerType,
							dynamicLegende: comapOptions.dynamicLegende,
							legende: comapOptions.legende,
							legendeVar: comapOptions.legendeVar,
							legendeLabel: comapOptions.legendeLabel,
							groupBy: comapOptions.groupBy,
							parentContainer: comapOptions.parentContainer,
							map:mapContext.getMap(),
							addElts: function(elts){
								mapContext.addElts(elts)
							},
							clearMap: function(){
								mapContext.clearMap()
							},
							showLoader: function(){
								mapContext.showLoader()
							},
							fitBounds:function(){
								mapContext.fitBounds()
							}
						};
					}else{
						var customMap=(typeof paramsMapCO.mapCustom!="undefined") ? paramsMapCO.mapCustom : {tile : "mapbox"};
						mapContext = new CoMap({
							container : fObj.mapObj.parentContainer,
							activePopUp : true,
							mapOpt:{
								zoomControl:false
							},
							mapCustom:customMap,
							elts : {}
						});
						$(".labelMap").hide();

						var comapOptions = mapContext.getOptions();
						var fmapObj = {...fObj.mapObj};
						fObj.mapObj = {
							parentContainer: comapOptions.parentContainer,
							map:mapContext.getMap(),
							addElts: function(elts){
								mapContext.addElts(elts)
							},
							clearMap: function(){
								mapContext.clearMap()
							},
							showLoader: function(){
								mapContext.showLoader()
							},
							fitBounds:function(){
								mapContext.fitBounds()
							}
						};
						if(fmapObj.showLegende){
							fObj.mapObj.showLegende = fmapObj.showLegende;
						}
						if(fmapObj.clusterType){
							fObj.mapObj.clusterType = fmapObj.clusterType;
						}
						if(fmapObj.markerType){
							fObj.mapObj.markerType = fmapObj.markerType;
						}
						if(fmapObj.dynamicLegende){
							fObj.mapObj.dynamicLegende = fmapObj.dynamicLegende;
						}
						if(fmapObj.legende){
							fObj.mapObj.legende = fmapObj.legende;
						}
						if(fmapObj.legendeVar){
							fObj.mapObj.legendeVar = fmapObj.legendeVar;
						}
						if(fmapObj.legendeLabel){
							fObj.mapObj.legendeLabel = fmapObj.legendeLabel;
						}
					}
					fObj.search.init(fObj);
				});
			},
			tags : function(fObj, domFilters, k){
				mylog.log("searchObj.events.tags");
				$(domFilters+" ."+k+"[data-type='tags']").off().on("click",function(){
					var value = $(this).data("value");
					if(fObj.search.obj.tags.indexOf(value) < 0){
						fObj.search.obj.tags.push($(this).data("value"));
						//fObj.search.init(fObj);
						fObj.filters.manage.addActive(fObj, $(this),true);//"tags",value, $(this).data("key"), $(this).data("field"));
					}else{
						$(".tooltip[role='tooltip']").remove();
						fObj.filters.manage.xsActiveFilter(fObj);
						fObj.filters.manage.removeActive( fObj, $(this) );
						coInterface.initHtmlPosition();
					}
				});
			},
			select2 : function(fObj, domFilters, k){				
				mylog.log("searchObj.events.tag2s");
				$(domFilters+" ."+k+"[data-type='select2']").off().on("click",function(){
					var value = [];
					value[k] = $(domFilters+" ."+k+"[data-type='select2']").val().split(",");
					fObj.search.obj.tags =  fObj.search.obj.tags.concat(value[k]) 
					for(var i=0; i<fObj.search.obj.tags.length; ++i) {
						for(var j=i+1; j<fObj.search.obj.tags.length; ++j) {
							if(fObj.search.obj.tags[i] === fObj.search.obj.tags[j])
							fObj.search.obj.tags.splice(j--, 1);
						}
					}	
					$.each(value[k], function(k, v){ 
						if(fObj.search.obj.tags.indexOf(v) < 0){
							fObj.search.obj.tags.push(v);								
						}
					}) 
					var allValue = [];
					$.each($("input[data-type='select2']"), function(k,v){
						$.each($(v).val().split(","), function(k, vv){
							allValue.push(vv);
						})
					});
					$.each(fObj.search.obj.tags, function(kt, vt){ 
						if(allValue.indexOf(vt) < 0){
							if(typeof vt == "string"){
								fObj.search.obj.tags.splice(fObj.search.obj.tags.indexOf(vt),1);		
							}						
						}
					})
					fObj.search.init(fObj);
				})
			},
			around : function(fObj, domFilters){
				mylog.log("searchObj.events.tags");
				$(domFilters+" #around-me-filters").off().on("click",function(){
					/* Ajouter à l'événement de régleur de rayon dans l'objet,
						il sera interprété dans le reste du métier @fObj.filters.actions.geoPosition.getLocation
						fObj.search.obj.dist=$(this).value();
					*/
					fObj.filters.actions.geoPosition.getLocation(fObj);
					fObj.filters.manage.addActive(fObj,{type:"around",value:"À 3 km",key:"around"});
				});
			},
			selectList : function(fObj, domFilters, k){
				mylog.log("searchObj.events.selectList");
				$(domFilters+" .btn-filters-select."+k).off().on("click",function(event){
					if($(this).data("stop-propagation"))
						event.stopPropagation();
					var selectValue = ( notNull($(this).data("key")) ) ? $(this).data("key") : $(this).data("value") ;
					var alreadyAdded = false;
					if($(this).data("type") != "filters"){
						if(notNull($(this).data("multiple"))){
							if(typeof fObj.search.obj[$(this).data("type")]=="undefined"){
								fObj.search.obj[$(this).data("type")]=[];
							}else if(!Array.isArray(fObj.search.obj[$(this).data("type")])){
								fObj.search.obj[$(this).data("type")]=[fObj.search.obj[$(this).data("type")]];
							}
							if (fObj.search.obj[$(this).data("type")].indexOf(selectValue) == -1){
								fObj.search.obj[$(this).data("type")].push( selectValue );
							}else{
								alreadyAdded = true;
							}
						} else{
							$(`${fObj.filters.dom} #activeFilters .filters-activate[data-type="${$(this).data("type")}"]`).fadeOut().remove();
							fObj.search.obj[$(this).data("type")] = [selectValue];
						}
	                }
					else if(typeof $(this).data("field") != "undefined"){
						if(typeof fObj.search.obj.filters == "undefined" )
							fObj.search.obj.filters = {};

						if(typeof fObj.search.obj.filters[$(this).data("field")] == "undefined" )
							fObj.search.obj.filters[$(this).data("field")] = [];
	                    fObj.search.obj.filters[$(this).data("field")].push( selectValue );
	                  //	fObj.search.init(fObj);
					//	fObj.filters.manage.addActive(fObj, "filters",$(this).data("value"), selectValue, $(this).data("field"), null, $(this).data("label"));
					}
					//fObj.search.init(fObj);
					if(!alreadyAdded){
						fObj.filters.manage.addActive(fObj, $(this), true);
					}
				
				});
			},
			extraUrl :function(fObj, domFilters, k){
				mylog.log("urlData Event");
                $(domFilters+" .btn-filters-select."+k).off().on("click",function(event){
                	var originUrl = $(this).data("field");
                	var extraUrl = $(this).data("extra-url");
                	fObj.urlData=originUrl+extraUrl;
                	fObj.filters.manage.addActive(fObj, $(this), true);
                });      
		    },
			filters : function(fObj, domFilters, k){
				mylog.log("searchObj.events.selectList nemany", domFilters);
				$(domFilters+" .btn-filters-select."+k).off().on("click",function(){
					mylog.log("searchObj.events filters button[data-type='filters']");
					if($(this).data("type") != "filters"){
						if(notNull($(this).data("multiple"))){
							if(typeof fObj.search.obj[$(this).data("type")]=="undefined")
								fObj.search.obj[$(this).data("type")]=[];
							fObj.search.obj[$(this).data("type")].push($(this).data("key"));
						}else
							fObj.search.obj[$(this).data("type")]=$(this).data("key");
	                   	//fObj.search.init(fObj);
						//fObj.filters.manage.addActive(fObj, $(this).data("type"),$(this).data("value"), $(this).data("key"), $(this).data("field"));

					}
					else if(typeof $(this).data("field") != "undefined"){

						if(typeof fObj.search.obj.filters == "undefined" )
							fObj.search.obj.filters = {};

						if(typeof fObj.search.obj.filters[$(this).data("field")] == "undefined" )
							fObj.search.obj.filters[$(this).data("field")] = [];
							if(Array.isArray(fObj.search.obj.filters[$(this).data("field")]))
	                    		fObj.search.obj.filters[$(this).data("field")].push($(this).data("key"));
							else{
								fObj.search.obj.filters[$(this).data("field")] = [];
								fObj.search.obj.filters[$(this).data("field")].push($(this).data("key"));
							}
					}
					fObj.filters.manage.addActive(fObj, $(this), true);//"filters",$(this).data("value"), $(this).data("key"), $(this).data("field"));
				});
			},
			favorites : function(fObj, domFilters, k){
				$(domFilters+" .btn-favorites-filters").off().on("click", function(){
					urlCtrl.openPreview("config/type/citoyens/id/"+userId, {
							appKey:$(this).data("app"),
							domFilters: domFilters,
							searchP : fObj.search.obj,
							activatedF : $(domFilters+" .filters-activate").length
						});
				});
			},
			saveActiveFilters : function(fObj, domFilters, k){
				mylog.log('init saveactivefilters', domFilters);
				var selectorElementaire = fObj.pInit['subdom'] ? fObj.pInit['subdom'] : '#filterContainerL';
				$(domFilters+" .btn-filter-action,"+selectorElementaire+" .btn-filter-action").off().on("click", function(){
					var self = this;
					sectionDyfFilter = {
                        "jsonSchema": {
                            "title": trad.saveActiveFilters,
                            "icon": "save",
                            "text": "Enregistrement des filtres activés",
                            "properties": {
                                label : {
                                    inputType: "text",
                                    label: trad.nameForFilterToSave,
                                    placeholder: trad.EnterText,
                                    rules: {
                                        required: true
                                    },
                                    value: ""
                                },
                                description : {
                                    inputType: "textarea",
                                    label: tradDynForm.shortDescription,
                                    placeholder: trad.EnterText,
                                    value: ""
                                },
                            },
                            onLoads : {},
                            save: function() {
								const allActivateFilters = $(domFilters+" .filters-activate");
								mylog.log('data activatefilters', domFilters, allActivateFilters)
								var activateFiltersToSend = [];
								var allAtributes = {}
								$.each(allActivateFilters, function(k,v) {
									const label = $(v).find("span.activeFilters").text();
									activateFiltersToSend.push(label)
									const attrArray = Array.from($(v).get(0).attributes);
									attributes = attrArray.reduce((attrs, attr) => {
										if(attr.nodeName.indexOf("data") >= 0){
											attrs !== '' && (attrs += ' ')
											attrs += `${attr.nodeName}="${attr.nodeValue}"`
										}
										return attrs
									}, '');
									allAtributes[label+k] = attributes
								})
                                let dataToSend = {
                                    id : userId,
									collection : "citoyens",
									path : "preferences."+$(self).attr("data-appkey")+"."+$(self).attr("data-concernedForm"),
									arrayForm : true,
									value :	{
										allBtnAttributes : allAtributes,
										labelActivateFilters : activateFiltersToSend.toString(),
										data : fObj.search.obj ? JSON.stringify(fObj.search.obj) : {}
									}
                                }
                                $.each(sectionDyfFilter.jsonSchema.properties, function(k, val) {
									dataToSend.value[k] = $("#" + k).val()
                                });
                    
                                mylog.log("save tplCtx", dataToSend);
                                if (typeof dataToSend.value == "undefined") {
                                    toastr.error('value cannot be empty!');
                                } else {
                                    dataHelper.path2Value(dataToSend, function(params) {
                                        urlCtrl.loadByHash(location.hash)
                                    });
                                }
                
                            }
                        }
                    };
					dyFObj.openForm(sectionDyfFilter);
				});
			},
			activeSavedFilters : function(fObj, domFilters, k) {
				$(domFilters+" .btn-saved-filters-select."+k).off().on("click",function(){
					$(domFilters + " #activeFilters .filters-activate").each(function(index) {
						fObj.filters.manage.removeActive(fObj, $(this), true)
					})
					fObj.dataFilters ? (
						fObj.dataFilters[$(this).data("key")] ? fObj.search.obj = JSON.parse(JSON.stringify(fObj.dataFilters[$(this).data("key")])) : ""
					) : "";
					fObj.search.obj && fObj.search.obj.fields ? fObj.search.obj.fields = [] : "";
					if(fObj.search.obj.text != "")
						$(domFilters+` input[type='text'][data-text-path='${fObj.search.obj.textPath}']`).val(fObj.search.obj.text);
					fObj.filters.manage.addActive(fObj, $(this), true);
				});
				$(domFilters+" .editSavedFilter."+k).off().on("click", function() {
					var self = this;
					sectionDyfFilter = {
                        "jsonSchema": {
                            "title": trad.editSavedFilter,
                            "icon": "save",
                            "text": "Enregistrement des filtres activés",
                            "properties": {
                                label : {
                                    inputType: "text",
                                    label: trad.nameForFilterToSave,
                                    placeholder: trad.EnterText,
                                    rules: {
                                        required: true
                                    },
                                    value: $(self).attr("data-label")
                                },
                                description : {
                                    inputType: "textarea",
                                    label: tradDynForm.shortDescription,
                                    placeholder: trad.EnterText,
                                    value: $(self).attr("data-description")
                                },
                            },
                            onLoads : {},
                            save: function() {
                                let dataToSend = {
                                    id : userId,
									collection : "citoyens",
									path : "preferences."+$(self).attr("data-appkey")+"."+$(self).attr("data-concernedForm")+"."+$(self).attr("data-index"),
									value :	{
										allBtnAttributes : fObj.dataForFilters[$(self).attr("data-key")] ? JSON.stringify(fObj.dataForFilters[$(self).attr("data-key")]) : "",
										labelActivateFilters : fObj.labelActivateFilters[$(self).attr("data-key")] ? fObj.labelActivateFilters[$(self).attr("data-key")] : "",
										data : fObj.dataFilters[$(self).attr("data-key")] ? JSON.stringify(fObj.dataFilters[$(self).attr("data-key")]) : {}
									}
                                }
                                $.each(sectionDyfFilter.jsonSchema.properties, function(k, val) {
									dataToSend.value[k] = $("#" + k).val()
                                });
                    
                                mylog.log("save tplCtx", dataToSend);
                                if (typeof dataToSend.value == "undefined") {
                                    toastr.error('value cannot be empty!');dataHelper.path2Value(dataToSend, function(params) {
                                        urlCtrl.loadByHash(location.hash)
                                    });
                                } else {
                                    dataHelper.path2Value(dataToSend, function(params) {
                                        urlCtrl.loadByHash(location.hash)
                                    });
                                }
                
                            }
                        }
                    };
					dyFObj.openForm(sectionDyfFilter);
				});
				$(domFilters+" .deleteSavedFilter."+k).off().on("click", function() {
					var self = this;
					bootbox.confirm( trad.areyousuretodelete, 
						function(result) {
							if(result){
								var formData={
									"id":userId,
									"collection":"citoyens",
									"path":"preferences."+$(self).attr("data-appkey")+"."+$(self).attr("data-concernedForm")+"."+$(self).attr("data-index"),
									"pull":"preferences."+$(self).attr("data-appkey")+"."+$(self).attr("data-concernedForm"),
									"value":null
								};      
								dataHelper.path2Value( formData, function(params) {
									toastr.success("Filtre enregistré bien supprimé");
									urlCtrl.loadByHash(location.hash)
								});
							}
					})
				});
			},
			resetFilter : function(fObj, domFilters, k){
				mylog.log('init event resetFilter', k, domFilters);
				var selectorElementaire = fObj.pInit['dom'] ? fObj.pInit['dom'] : '#filterContainerL';
				$(domFilters+" .reset-filter,"+selectorElementaire+" .reset-filter").off("click").on("click", function(){
					var self = this;
					if(fObj.filters && fObj.filters.manage && fObj.filters.manage.resetFilter) {
						fObj.filters.manage.resetFilter(fObj, ".btn-filters-select")
					}
				});
			},
			exists : function(fObj, domFilters, k){
				mylog.log("searchObj.events.selectList");
				$(domFilters+" .btn-filters-select."+k).off().on("click",function(){
					mylog.log("searchObj.exists button[data-type='filters']");
					classToRemove=(notNull($(this).data("filterK"))) ? '[data-filterK="'+$(this).data("filterK")+'"]' : '[data-field="'+$(this).data("field")+'"]';
					if($(domFilters+' .filters-activate'+classToRemove).length > 0)
						$(domFilters+' .filters-activate'+classToRemove).fadeOut().remove();
					if($(this).data("type") != "filters"){
						fObj.search.obj[$(this).data("type")]={'exists':$(this).data("key")};
	                	//fObj.search.init(fObj);
						//fObj.filters.manage.addActive(fObj, $(this).data("type"),$(this).data("value"), $(this).data("key"), $(this).data("field"), "exists");

					}
					else if($(this).data("value")!="true" 
							&& $(this).data("value")!="false" 
							&& $(this).data("value")!="1"
							&& $(this).data("value")!="0" 
							&& typeof $(this).data("field") != "undefined"
						){
							fObj.search.obj.filters[$(this).data("field")+"."+$(this).data("value")]={'$exists' : true};
							//fObj.search.init(fObj);
					}
					else if(typeof $(this).data("field") != "undefined"){
						if(typeof fObj.search.obj.filters == "undefined" )
							fObj.search.obj.filters = {};
						var valueExists=$(this).data("value");
						// Exemple des membres actif (ou l'on veut ni les invitations, ni les tobevalidated)
						if($(this).data("field").indexOf("&&") > 0){
							fieldsExists=$(this).data("field").split("&&");
							$.each(fieldsExists, function(e,v){
								fieldEx=v;
								if(v.indexOf("=") > 0){
									fieldAndVal=v.split("=");
									fieldEx=fieldAndVal[0];
									valueExists=fieldAndVal[1];
								}
								fObj.search.obj.filters[fieldEx]={'$exists' : valueExists};
							});
						}else if($(this).data("field").indexOf("||") > 0){
							fieldsExists=$(this).data("field").split("||");
							fObj.search.obj.filters['$or']={};
							$.each(fieldsExists, function(e,v){
								fieldEx=v;
								if(v.indexOf("=") > 0){
									fieldAndVal=v.split("=");
									fieldEx=fieldAndVal[0];
									valueExists=fieldAndVal[1];
								}
								fObj.search.obj.filters['$or'][fieldEx]={'$exists' : valueExists};
							});
						}else if($(this).data("or")){
							if(typeof fObj.search.obj.filters['$or'] == "undefined" )
								fObj.search.obj.filters['$or']={};
								fObj.search.obj.filters['$or'][$(this).data("field")]={'$exists' : valueExists};
						}else{
							//if(!exists(fObj.search.obj.filters[$(this).data("field")])) fObj.search.obj.filters[$(this).data("field")] = [];
							fObj.search.obj.filters[$(this).data("field")]={'$exists' : valueExists};

						} 

						//fObj.search.init(fObj);
						//fObj.filters.manage.addActive(fObj, "filters",$(this).data("value"), $(this).data("key"), $(this).data("field"), "exists", $(this).data("label"), $(this).data("filterK"));
					}
					//fObj.search.init(fObj);
					fObj.filters.manage.addActive(fObj, $(this), true);//"filters",$(this).data("value"), $(this).data("key"), $(this).data("field"), "exists", $(this).data("label"), $(this).data("filterK"));


				});
			},
			inArray : function(fObj, domFilters, k){
				mylog.log("searchObj.events.selectList");
				$(domFilters+" .btn-filters-select."+k).off().on("click",function(){
					mylog.log("searchObj.events inArray", $(this).data("type"), $(this).data("field"), $(this).data("value"));
					if(exists($(this).data("key"))){
						valueArr= $(this).data("key");
						dataInspec='data-key="'+$(this).data("key")+'"';
					}else{
						valueArr= $(this).data("value");
						dataInspec='data-value="'+$(this).data("value")+'"';
					}
					if($(domFilters+' .filters-activate[data-field="'+$(this).data("field")+'"]['+dataInspec+']').length > 0)
						$(domFilters+' .filters-activate[data-field="'+$(this).data("field")+'"]['+dataInspec+']').fadeOut().remove();

					const addArrayFilter = (valueArray) => {
						var inArrayValue = (Array.isArray(valueArray)) ? valueArray : [valueArray];
						if(!exists(fObj.search.obj.filters))
							fObj.search.obj.filters = {};
						if($(this).data("otherfield")){
							if(exists(fObj.search.obj.filters) && !exists(fObj.search.obj.filters['$or'])){
								var filtersData = $.extend({}, fObj.search.obj.filters)
								fObj.search.obj.filters = {};
								fObj.search.obj.filters['$or'] = filtersData;
							}
							if(!exists(fObj.search.obj.filters['$or']))
								fObj.search.obj.filters['$or'] = {};
							if(!exists(fObj.search.obj.filters['$or'][$(this).data("field")]))
								fObj.search.obj.filters['$or'][$(this).data("field")] = {'$in': inArrayValue};
							else if(!exists(fObj.search.obj.filters['$or'][$(this).data("field")]['$in']))
								fObj.search.obj.filters['$or'][$(this).data("field")]['$in']= inArrayValue;
							else
								if (Array.isArray(valueArray))
									fObj.search.obj.filters['$or'][$(this).data("field")]['$in'].push(...valueArray);
								else 
									fObj.search.obj.filters['$or'][$(this).data("field")]['$in'].push(valueArray);
							
							if(!exists(fObj.search.obj.filters['$or'][$(this).data("otherfield")]))
								fObj.search.obj.filters['$or'][$(this).data("otherfield")] = {'$in': [$(this).data("label")]};
							else if(!exists(fObj.search.obj.filters['$or'][$(this).data("otherfield")]['$in']))
								fObj.search.obj.filters['$or'][$(this).data("otherfield")]['$in'] = $(this).data("label");
							else
								fObj.search.obj.filters['$or'][$(this).data("otherfield")]['$in'].push($(this).data("label"));
						}else {
							if(exists(fObj.search.obj.filters['$or'] ) && exists(fObj.search.obj.filters['$or']["tags"])){
								if(!exists(fObj.search.obj.filters['$or'][$(this).data("field")]))
									fObj.search.obj.filters['$or'][$(this).data("field")] = {'$in': inArrayValue};
								else if(!exists(fObj.search.obj.filters['$or'][$(this).data("field")]['$in']))
									fObj.search.obj.filters['$or'][$(this).data("field")]['$in']= inArrayValue;
								else
									if (Array.isArray(valueArray))
										fObj.search.obj.filters['$or'][$(this).data("field")]['$in'].push(...valueArray);
									else 
										fObj.search.obj.filters['$or'][$(this).data("field")]['$in'].push(valueArray);	
							}else{
								if(!exists(fObj.search.obj.filters[$(this).data("field")]))
									fObj.search.obj.filters[$(this).data("field")] = {'$in': inArrayValue};
								else if(!exists(fObj.search.obj.filters[$(this).data("field")]['$in']))
									fObj.search.obj.filters[$(this).data("field")]['$in']= inArrayValue;
								else
									if (Array.isArray(valueArray))
										fObj.search.obj.filters[$(this).data("field")]['$in'].push(...valueArray);
									else 
										fObj.search.obj.filters[$(this).data("field")]['$in'].push(valueArray);
							}
						}
					}

					if($(this).data("type") != "filters"){
						if (typeof $(this).data("type") != "undefined" && $(this).data("type") == "valueArray" && typeof $(this).data("value") != "undefined" && typeof $(this).data("field") != "undefined") {
							var arrayValue = ($(this).data("value").includes("\\")) ? $(this).data("value").split("\\") : [$(this).data("value")];
							addArrayFilter(arrayValue);
							if(typeof fObj.search.obj[$(this).data("type")] != "undefined" && exists(fObj.search.obj[$(this).data("type")]))
								if(fObj.search.obj[$(this).data("type")].includes(valueArr))
									fObj.search.obj[$(this).data("type")].splice(fObj.search.obj[$(this).data("type")].indexOf(valueArr), 1)
								else
									fObj.search.obj[$(this).data("type")].push(valueArr)
							else
								fObj.search.obj[$(this).data("type")] = [valueArr];
						} else {
							if(exists(fObj.search.obj[$(this).data("type")]['$in']))
								fObj.search.obj[$(this).data("type")]['$in'].push(valueArr);
							else
								fObj.search.obj[$(this).data("type")]={'$in':[valueArr]};
						}
					}
					else if(typeof $(this).data("field") != "undefined"){
						addArrayFilter(valueArr);
	                }
					 // Mahefa
					//fObj.search.init(fObj);
					fObj.filters.manage.addActive(fObj, $(this), true);//$(this).data("type"),$(this).data("value"), $(this).data("key"), $(this).data("field"), "inArray");
				});
			},
			price : function(fObj, domFilters){
				$(domFilters+" .btn-price-start-search").off().on("click",function(){
					fObj.filters.actions.price.remove(fObj);
					if(notEmpty($(domFilters+" .minPriceInput").val()))
						fObj.searcj.obj.priceMin=$(domFilters+" .minPriceInput").val();
					if(notEmpty($(domFilters+" .maxPriceInput").val()))
						fObj.searcj.obj.priceMax=$(domFilters+" .maxPriceInput").val();
					if(notEmpty(priceStr)){
						//fObj.search.init(fObj);
						fObj.filters.actions.price.add(fObj)
					}
				});
			},
			scope : function(fObj, domFilters){
				mylog.log("searchObj.events.scope");
				myScopes.open={};
				bindSearchCity("#costum-scope-search", function(){
					$(domFilters+" .item-globalscope-checker").off().on('click', function(){
						mylog.log("searchObj.events.scope .item-globalscope-checker");
						$("#costum-scope-search .input-global-search").val("");
						$(".dropdown-result-global-search").hide(700).html("");
						myScopes.type="open";
						mylog.log("searchObj.events.scope globalscope-checker",  $(this).data("scope-name"), $(this).data("scope-type"), $(this).data("scope-value"));
						mylog.log("searchObj.events.scope globalscope-checker 2", myScopes.search,  myScopes.search[$(this).data("scope-value")]);
						var newScope=fObj.filters.actions.scopeList.scopeObject(myScopes.search[$(this).data("scope-value")]);
						//var newKeyScope = newScope.key
						//delete newScope.key;
						//myScopes.open[newKeyScope]=newScope;
						myScopes.open[newScope.key]=newScope;
						localStorage.setItem("myScopes",JSON.stringify(myScopes));
						fObj.filters.manage.addActive(fObj, {type:"scope",value:$(this).data("scope-name"), key : newScope.key}, true);
						//fObj.search.init(fObj);
					});
				});
			},
			scopeList : function(fObj, domFilters){
				myScopes.open={};
				$(domFilters+" button[data-type='scopeList']").off().on('click', function(){
					mylog.log("searchObj.events.scopeList .item-globalscope-checker");
					myScopes.type="open";
					mylog.log("searchObj.events.scopeList globalscope-checker", $(this).data("id"));
					var newScope=fObj.filters.actions.scopeList.scopeObject(fObj.filters.lists.scopeList[$(this).data("key")][$(this).data("id")]);
					//	var newKeyScope = newScope.key
					//	delete newScope.key;
					//	myScopes.open[newKeyScope]=newScope;

					myScopes.open[newScope.key]=newScope;
					localStorage.setItem("myScopes",JSON.stringify(myScopes));
					fObj.filters.manage.addActive(fObj, {type:"scope",value:$(this).data("value"), key : newScope.key}, true);//"scope", newScope.name, newScope.key);
					//fObj.search.init(fObj);
				});
			},
			themes : function(fObj, domFilters){
				mylog.log("searchObj.events.themes",fObj,domFilters);
				/*$(".divSearchTheme").click(function(e){
					e.stopPropagation();
				});*/
				$(domFilters +" .inputSearchThemes").tagsinput({
					containerClass: 'col-xs-10 col-sm-8'
				});
				$(domFilters + " .divBtnTheme .btn-select-filliaire").off().on("click",function(event){
		            mylog.log("searchObj.events.themes searchInterface.init .divBtnTheme .btn-select-filliaire", fObj.filters.lists.themes);
		           	event.stopPropagation();
		            var fKey = $(this).data("fkey");
		            var tagsArray=[];
		            mylog.log("searchObj.events.themes searchInterface.init .divBtnTheme .btn-select-filliaire fKey", fKey);
		            $.each(fObj.filters.lists.themes[fKey]["tags"], function(key, tag){
		            	mylog.log("searchObj.events.themes searchInterface.init .divBtnTheme .btn-select-filliaire tag", tag);
		                tag=(typeof tradTags[tag] != "undefined") ? tradTags[tag] : tag;
		                $(domFilters +' .inputSearchThemes').tagsinput('add', tag);
		            });
		     	});

		        $(domFilters + " .btn-theme-start-search").off().on("click", function(event){
		        	mylog.log("searchObj.events.themes searchInterface.init .btn-theme-start-search");
		           	//event.stopPropagation();
		            var tagsThemes = $(domFilters +' .inputSearchThemes').tagsinput('items');

		            if( tagsThemes != null && tagsThemes.length > 0){
		            	$.each(tagsThemes, function(kT, vT){
		            		if($.inArray(vT,fObj.search.obj.tags) < 0){
			            		fObj.filters.manage.addActive(fObj,{type:"tags",value:vT, key : vT, field : vT});//"tags", vT, vT, vT);
			            		fObj.search.obj.tags.push(vT);
		            		}
		            	});
		            }
					fObj.search.init(fObj);


		        });
		        $(domFilters + " .btn-theme-refresh").off().on("click", function(event){
		        	event.stopPropagation();
		        	$(domFilters +' .inputSearchThemes').tagsinput('removeAll');
		        	mylog.log("searchObj.events.themes searchInterface.init .btn-theme-refresh");
		        });
		    },
			odd : function(fObj, domFilters){
				$(domFilters +" .inputSearchOdd").tagsinput({
					containerClass: 'col-xs-10 col-sm-8'
				});
				$(domFilters + " .divBtnOdd .btn-select-odd-tags").off().on("click",function(event){
		            mylog.log("searchObj.events.odd searchInterface.init .divBtnOdd .btn-select-odd-tags", fObj.filters.lists.odd);
		           	event.stopPropagation();
		            var fKey = $(this).data("fkey");
		            var tagsArray=[];
		            mylog.log("searchObj.events.themes searchInterface.init .divBtnOdd .btn-select-odd-tags fKey", fKey);
		            $.each(fObj.filters.lists.odd[fKey]["tags"], function(key, tag){
		            	mylog.log("searchObj.events.odd searchInterface.init .divBtnOdd .btn-select-odd-tags tag", tag);
		                tag=(typeof tradTags[tag] != "undefined") ? tradTags[tag] : tag;
		                $(domFilters +' .inputSearchOdd').tagsinput('add', tag);
		            });
		     	});

		        $(domFilters + " .btn-odd-start-search").off().on("click", function(event){
		        	mylog.log("searchObj.events.odd searchInterface.init .btn-odd-start-search");
		           	//event.stopPropagation();
		            var tagsOdd = $(domFilters +' .inputSearchOdd').tagsinput('items');

		            if( tagsOdd != null && tagsOdd.length > 0){
		            	$.each(tagsOdd, function(kT, vT){
		            		if($.inArray(vT,fObj.search.obj.tags) < 0){
			            		fObj.filters.manage.addActive(fObj,{type:"tags",value:vT, key : vT, field : vT});//"tags", vT, vT, vT);
			            		fObj.search.obj.tags.push(vT);
		            		}
		            	});
		            }
					fObj.search.init(fObj);


		        });
		        $(domFilters + " .btn-odd-refresh").off().on("click", function(event){
		        	event.stopPropagation();
		        	$(domFilters +' .inputSearchOdd').tagsinput('removeAll');
		        	mylog.log("searchObj.events.odd searchInterface.init .btn-odd-refresh");
		        });
		    },
			sortBy : function(fObj, domFilters,k){
				mylog.log("searchObj.events.selectList");
				$(domFilters+" .btn-filters-select."+k).off().on("click",function(){
					mylog.log("searchObj.events filters button[data-type='filters']");
					self = this;
					if($(this).attr("data-isradio")) {
						var selector = "";
						const keysForRadioSelector = $(this).attr("data-isradio").split(",");
						keysForRadioSelector.forEach((keyVal, keyIndex) => {
							selector += `button[data-key="${keyVal}"]`;
							if(keyIndex < keysForRadioSelector.length - 1)
								selector += ","
						})
						$(selector).each(function(i, elemItem) {
							if(!$(this).is($(self))) {
								fObj.filters.manage.removeActive(fObj, $(this), true);
							}
						})
					}


					if($(this).data("type") == "sortBy"){
							fObj.search.obj[$(this).data("type")]={};
							fObj.search.obj[$(this).data("type")][$(this).data("key")] = $(this).data("value");
					}
					fObj.filters.manage.addActive(fObj, $(this), true);//"filters",$(this).data("value"), $(this).data("key"), $(this).data("field"));
				});
			},
			doublon : function(fObj, domFilters, k){
				$(domFilters+" .btn-filters-select."+k).off().on("click",function(){
					if(typeof fObj.search.obj.doublon == "undefined")
						fObj.search.obj.doublon = true;	
					fObj.filters.manage.addActive(fObj, $(this), true);
				})
			},
			openingHours : function(fObj, domFilters, k){
				$(domFilters+" .btn-filters-select."+k).off().on("click",function(){
					if(typeof fObj.search.obj.openingHours == "undefined")
						fObj.search.obj.openingHours = true;	
					fObj.filters.manage.addActive(fObj, $(this), true);
				})
			}
		},
		manage:{
			/*addActiveHorizontal: function(fObj, type, value, key, field, event, label, kFilter){
				mylog.log("searchObj.manage.addActive", type, value, key);
				var dataKey = (typeof key != "undefined") ? 'data-key="'+key+'" ' : "" ;
				var dataField = (typeof field != "undefined") ? 'data-field="'+field+'" ' : "" ;
				var dataEvent = (notNull(event)) ? 'data-event="'+event+'" ': "";
				var dataKFilter=(notNull(kFilter)) ? 'data-filterK="'+kFilter+'" ':"";
				var labelButton = (notNull(label)) ? label : value;
				str='<div class="filters-activate tooltips" data-position="bottom" data-title="Effacer" '+
							dataKey +
							dataField +
							dataEvent +
							dataKFilter +
							'data-value="'+value+'" '+
							'data-name="'+name+'" '+
							'data-type="'+type+'">' +
						"<i class='fa fa-times-circle'></i>"+
						"<span "+
							"class='activeFilters' "+
							dataKey +
							dataField +
							dataEvent +
							dataKFilter+
							'data-value="'+value+'" '+
							'data-scope-name="'+name+'" '+
							'data-type="'+type+'">' +
							labelButton +
						"</span>"+
					"</div>";
				$(fObj.filters.dom+" #activeFilters").append(str);
				fObj.filters.manage.xsActiveFilter(fObj);
				fObj.results.map.changedfilters=true;
				coInterface.initHtmlPosition();
				$(fObj.filters.dom+" .filters-activate").off().on("click",function(){
					$(".tooltip[role='tooltip']").remove();
					$(this).fadeOut().remove();
					fObj.filters.manage.xsActiveFilter(fObj);
					fObj.filters.manage.removeActive(fObj, $(this).data("type"), $(this).data("value"), $(this).data("key"), $(this).data("field"), $(this).data("event"));
					coInterface.initHtmlPosition();
				});
			},
			addActiveVertical: function(fObj, type, value, key, field, event){
				var id =  generateIdFromString(value, "filter_")
				$("#"+id).addClass("active")
				fObj.filters.manage.updateActiveCounter(fObj, value)
			},
			addActive: function(fObj, type, value, key, field, event, label, kFilter){
				if(fObj.layoutDirection && fObj.layoutDirection === "vertical")
					fObj.filters.manage.addActiveVertical(fObj, type, value, key, field, event, label);
				else
					fObj.filters.manage.addActiveHorizontal(fObj, type, value, key, field, event, label);
			},*/
			addActive : function(fObj, dataF, launchSearch, refreshTotalResults){
				var attributes = "";
				var activeClass=false;
				if(dataF instanceof jQuery){
					var labelButton = (notNull(dataF.data("label"))) ? dataF.data("label") : dataF.data("value");
					if(notNull(dataF.attr("data-active"))){
						activeClass=true;
					}
					//const el = document.getElementById("book-link-29")
					const attrArray = Array.from(dataF.get(0).attributes);
					attributes = attrArray.reduce((attrs, attr) => {
						if(attr.nodeName.indexOf("data") >= 0){
						    attrs !== '' && (attrs += ' ')
						    attrs += `${attr.nodeName}="${attr.nodeValue}"`
					    }
					    return attrs
					}, '');
					//alert("jQuery");
				}else{
					if(typeof dataF.key != "undefined") attributes+='data-key="'+dataF.key+'" ';
					if(typeof dataF.field != "undefined") attributes+='data-field="'+dataF.field+'" ';
					if(typeof dataF.value != "undefined") attributes+='data-value="'+dataF.value+'" ';
					if(typeof dataF.type != "undefined") attributes+='data-type="'+dataF.type+'" ';
					var labelButton = (typeof dataF.label != "undefined") ? dataF.label : dataF.value;
					if(typeof labelButton == "undefined" && dataF != null && typeof dataF.key != "undefined" && dataF.key == "spam")
						labelButton = "spam";
				}
				if(!activeClass){
					var isActiveElem = false;
					if(dataF instanceof jQuery && notNull(dataF.attr("data-active-elem")) && dataF.attr("data-active-elem") == "true"){
						isActiveElem=true;
					}
					if(!isActiveElem) {
						if(dataF instanceof jQuery) {
							if(dataF.attr("data-labelActivateFilters")) {
								const labelActivateFilters = dataF.attr("data-labelActivateFilters").split(",");
								/* fObj.dataFilters ? (
									fObj.dataFilters[dataF.data("key")] ? fObj.search.obj = fObj.dataFilters[dataF.data("key")] : ""
								) : ""; */
								str = "";
								labelActivateFilters.forEach((e, i)=> {
									attributesUtil = fObj.dataForFilters ? (
														fObj.dataForFilters[dataF.data("key")] ? (
															fObj.dataForFilters[dataF.data("key")][e+i] ? fObj.dataForFilters[dataF.data("key")][e+i] : attributes
														) : attributes
													) : attributes;
									str +=`<div class="filters-activate tooltips" data-position="bottom" data-title="Effacer" 
											${attributesUtil}> 
										<i class='fa fa-times-circle'></i>
										<span 
											class='activeFilters' 
											${attributesUtil}> 
											${(exists(trad[e]) ? trad[e] : e) }
										</span>
									</div>`;	
									var btnTemp = $(`<div class="filters-activate tooltips" data-position="bottom" data-title="Effacer" 
															${attributesUtil}> 
														<i class='fa fa-times-circle'></i>
														<span 
															class='activeFilters' 
															${attributesUtil}> 
															${(exists(trad[e]) ? trad[e] : e) }
														</span>
													</div>`);
									const elemToActivate = $(fObj.filters.dom+` button[data-type='${btnTemp.attr('data-type')}'][data-key="${btnTemp.attr('data-key')}"][${typeof btnTemp.attr('data-field') != 'undefined' ? 'data-field="'+btnTemp.attr('data-field')+'"' : 'data-value="'+btnTemp.attr('data-value')+'"'}]`)
									elemToActivate.attr("data-active-elem", "true");
									elemToActivate.addClass("active");
									//find title head to add active class on it
									elemToActivate.parent().parent().parent().find(`div.panel-heading[data-parent="#filterContainerInside"]`).removeClass("active").addClass("active")
								})
							} else {
								dataF.attr("data-active-elem", "true");
								dataF.addClass("active");
								//find title head to add active class on it
								dataF.parent().parent().parent().find(`div.panel-heading[data-parent="#filterContainerInside"]`).removeClass("active").addClass("active")
								str='<div class="filters-activate tooltips" data-position="bottom" data-title="Effacer" '+
											attributes+'>' +
										"<i class='fa fa-times-circle'></i>"+
										"<span "+
											"class='activeFilters' "+
											attributes+'>' +
											(exists(trad[labelButton]) ? trad[labelButton] : labelButton) +
										"</span>"+
								"</div>";
							}
						} else { 
							if(typeof dataF.type !="undefined" && dataF.type ==  "filters"){ 
								var field = "";
								if(typeof dataF.field != "undefined") 
									field = dataF.field;
								else
									field = dataF.type 
								$(`.${field}[data-key="${dataF['key']}"]`).attr("data-active-elem", "true"); 
								$(`.${field}[data-key="${dataF['key']}"]`).addClass("active");
								//find head title to add active filter
								$(`.${field}[data-type="${dataF['type']}"][data-key="${dataF['key']}"],[data-type="${dataF['type']}"][data-field="${field}"][data-key="${dataF['key']}"]`).parent().parent().parent().find(`div.panel-heading[data-parent="#filterContainerInside"]`).removeClass("active").addClass("active")

								if(typeof dataF["field"] != "undefined" && typeof dataF["value"] != "undefined") {
									$(`.${field}[data-type="${dataF['type']}"][data-value="${dataF["value"]}"][data-key="${dataF['key']}"],[data-type="${dataF['type']}"][data-field="${field}"][data-value="${dataF["value"]}"][data-key="${dataF['key']}"]`).attr("data-active-elem", "true"); 
									$(`.${field}[data-type="${dataF['type']}"][data-value="${dataF["value"]}"][data-key="${dataF['key']}"],[data-type="${dataF['type']}"][data-field="${field}"][data-value="${dataF["value"]}"][data-key="${dataF['key']}"]`).addClass("active");
								}
							}
							if(typeof dataF.likeFilters != "undefined") {
								var field = "";
								if(typeof dataF.field != "undefined") 
									field = dataF.field;
								else
									field = dataF.type 
								//find head title to add active filter
								$(`.${field}[data-type="${dataF['type']}"][data-key="${dataF['key']}"],[data-type="${dataF['type']}"][data-field="${field}"][data-key="${dataF['key']}"]`).parent().parent().parent().find(`div.panel-heading[data-parent="#filterContainerInside"]`).removeClass("active").addClass("active")

								if(typeof dataF["field"] == "undefined" && typeof dataF["value"] != "undefined") {
									$(`.${field}[data-type="${dataF['type']}"][data-value="${dataF["value"]}"][data-key="${dataF['key']}"],[data-type="${dataF['type']}"][data-value="${dataF["value"]}"][data-key="${dataF['key']}"]`).attr("data-active-elem", "true"); 
									$(`.${field}[data-type="${dataF['type']}"][data-value="${dataF["value"]}"][data-key="${dataF['key']}"],[data-type="${dataF['type']}"][data-value="${dataF["value"]}"][data-key="${dataF['key']}"]`).addClass("active");
								}
								if(typeof dataF["field"] != "undefined" && typeof dataF["value"] != "undefined") {
									$(`.${field}[data-type="${dataF['type']}"][data-value="${dataF["value"]}"][data-key="${dataF['key']}"],[data-type="${dataF['type']}"][data-field="${field}"][data-value="${dataF["value"]}"][data-key="${dataF['key']}"]`).attr("data-active-elem", "true"); 
									$(`.${field}[data-type="${dataF['type']}"][data-value="${dataF["value"]}"][data-key="${dataF['key']}"],[data-type="${dataF['type']}"][data-field="${field}"][data-value="${dataF["value"]}"][data-key="${dataF['key']}"]`).addClass("active");
								}
							}
							
						
							str='<div class="filters-activate tooltips" data-position="bottom" data-title="Effacer" '+
										attributes+'>' +
									"<i class='fa fa-times-circle'></i>"+
									"<span "+
										"class='activeFilters' "+
										attributes+'>' +
										(exists(trad[labelButton]) ? trad[labelButton] : labelButton) +
									"</span>"+
							"</div>";
						}
						$(fObj.filters.dom+" #activeFilters").append(str);
						var found = {};
						$(fObj.filters.dom+" #activeFilters").children().each(function(){
							var $this = $(this);
							var key = $this.data('value')+'-'+$this.data('key');
							var field = $this.data('value')+'-'+$this.data('field');
							if(found[key+""+field]) $this.remove(); else found[key+""+field] = true;
						});


						var parentSiblings = $(fObj.filters.dom).parent().siblings();
						var centerContent = parentSiblings.length > 0 ? parentSiblings[parentSiblings.length - 1] : null;
						activeFiltersContainer = null;
						if(centerContent) {
							activeFiltersContainer = $(centerContent).find("#centerFilters-active");
							activeFiltersContainer.append(str)
							var foundCenter = {};
							$(activeFiltersContainer).children().each(function(){
								var $this = $(this);
								var key = $this.data('value')+'-'+$this.data('key');
								var field = $this.data('value')+'-'+$this.data('field');
								if(foundCenter[key+""+field]) $this.remove(); else foundCenter[key+""+field] = true;
							});
							$(activeFiltersContainer).children().off().on("click", function(){
								$(".tooltip[role='tooltip']").remove();
								var that = this;
								var keyToRemove;
								$.each($(activeFiltersContainer).children(), function(k,v){
									if(that == this) {
										keyToRemove = k;
										$(this).fadeOut().remove();
									}
								});
								$.each($(fObj.filters.dom+" .filters-activate"), function(k,v){
									if(k == keyToRemove) {
										$(this).fadeOut().remove();
									}
								});

								const filterBtnElm = $(fObj.filters.dom+` [data-type="${$(this).attr('data-type')}"][data-key="${$(this).attr('data-key')}"][${typeof $(this).attr('data-field') != 'undefined' ? 'data-field="'+$(this).attr('data-field')+'"' : 'data-value="'+$(this).attr('data-value')+'"'}]`);
								filterBtnElm.removeAttr('data-active');
								filterBtnElm.removeClass('active');
								fObj.filters.manage.xsActiveFilter(fObj);
								fObj.filters.manage.removeActive(fObj, $(this));
								coInterface.initHtmlPosition();
							})
						}
						$(fObj.filters.dom+" .filters-activate").off().on("click",function(){
							$(".tooltip[role='tooltip']").remove();
							if(centerContent) {
								var that = this;
								var keyToRemove;
								$.each($(fObj.filters.dom+" .filters-activate"), function(k,v){
									if(that == this) {
										keyToRemove = k;
									}
								});
								$.each($(activeFiltersContainer).children(), function(k,v){
									if(k == keyToRemove) {
										$(this).fadeOut().remove();
									}
								})
							}
							$(this).fadeOut().remove();
							const filterBtnElm = $(fObj.filters.dom+` [data-type="${$(this).attr('data-type')}"][data-key="${$(this).attr('data-key')}"][${typeof $(this).attr('data-field') != 'undefined' ? 'data-field="'+$(this).attr('data-field')+'"' : 'data-value="'+$(this).attr('data-value')+'"'}]`);
							filterBtnElm.removeAttr('data-active-elem');
							filterBtnElm.removeClass('active');
							fObj.filters.manage.xsActiveFilter(fObj);
							fObj.filters.manage.removeActive(fObj, $(this));//$(this).data("type"), $(this).data("value"), $(this).data("key"), $(this).data("field"), $(this).data("event"));
							coInterface.initHtmlPosition();
						});
					} else {
						if(dataF.hasClass("active")){
							dataF.removeClass("active");
							dataF.removeAttr("data-active-elem");
							var parentElem = dataF.parent().parent().parent();
							var perentBody = dataF.parent();
							//find and check if all child does not have class active then remove active class on the title head
							if(dataF.parent().find('.accordion-row.active').length < 1)
								dataF.parent().parent().parent().find(`div.panel-heading[data-parent="#filterContainerInside"]`).removeClass("active")
							launchSearch=false;
							fObj.filters.manage.removeActive(fObj, dataF);//$(this).data("type"), $(this).data("value"), $(this).data("key"), $(this).data("field"), $(this).data("event"));
						}
					}
				}else{
					if(dataF.hasClass("active")){
						dataF.removeClass("active");
						dataF.removeAttr("data-active-elem");
						//dataF.removeAttr("data-active");
						//launchSearch=false;
						/*dataF.removeClass("active");
						dataF.removeAttr("data-active");
						var parentElem = dataF.parent().parent().parent();
						var perentBody = dataF.parent();*/
						//find and check if all child does not have class active then remove active class on the title head
						if(dataF.parent().find('.accordion-row.active').length < 1)
							dataF.parent().parent().parent().find(`div.panel-heading[data-parent="#filterContainerInside"]`).removeClass("active")
						
						fObj.filters.manage.removeActive(fObj, dataF, true);//$(this).data("type"), $(this).data("value"), $(this).data("key"), $(this).data("field"), $(this).data("event"));
							
							//launchSearch=false;
						//fObj.filters.manage.removeActive(fObj, dataF);//$(this).data("type"), $(this).data("value"), $(this).data("key"), $(this).data("field"), $(this).data("event"));
					}else{
						if(!notNull(dataF.data("multiple"))){
							dataF.parent().children().each(function(e){
								if($(this).hasClass("active")){
									$(this).removeClass("active");
									fObj.filters.manage.removeActive(fObj, $(this), true);
								}
							})
						}
						dataF.addClass("active");
					}
				}
				fObj.results.map.changedfilters=true;
				if(notNull(launchSearch) && launchSearch===true){
					fObj.search.init(fObj, refreshTotalResults);
				}
				fObj.filters.manage.xsActiveFilter(fObj);
				coInterface.initHtmlPosition();
				$(`div[data-type='inproject']`).remove();
				if(str.includes(`data-type="inproject"`)) {
					$(fObj.filters.dom+" .saveActiveFilter").addClass("disabled");
					$("#aapSaveFilters .saveActiveFilter").addClass("disabled")
				} else {
					if($(fObj.filters.dom+" .filters-activate").length > 0) {
						$(fObj.filters.dom+" .saveActiveFilter").removeClass("disabled");
						$("#aapSaveFilters .saveActiveFilter").removeClass("disabled");
						$("#centerFilters-active").addClass("notnull")
					} else {
						$(fObj.filters.dom+" .saveActiveFilter").addClass("disabled");
						$("#aapSaveFilters .saveActiveFilter").addClass("disabled");
						$("#centerFilters-active").removeClass("notnull")
					}
				}
				if(!activeClass && $(fObj.filters.dom+ " #activeFilters .activeFilters").length > 0)
					$(fObj.filters.dom+" #activeFilters:hidden").show();
			},
			removeActive : function(fObj, dataF, noStartSearch){//type, value, key, field, event){
				var type = dataF.data("type");
				var event = dataF.data("event");
				var field = dataF.data("field");
				var otherField = dataF.data("otherfield");
				var label = dataF.data('label');
				var value = dataF.data("value");
				var key = dataF.data("key");
				var useOr = dataF.data("or");
				const removeArray = (arr, val) => {
					var filtered = arr.filter(function (value, index, arr) {
						return !val.includes(value);
					});
					return filtered;
				}
				mylog.log("searchObj.manage removeActive", type, value, key, field, event);
				//if(exists(fObj.search.obj[type])){
				if(notNull(event) && event=="exists"){
					if(type == "filters"){
						if(field.indexOf("&&") > 0){
							$.each(field.split("&&"), function(e, v){
								fieldRemove=(v.indexOf("=") > 0) ? v.split("=")[0] : v;
								if(exists(fObj.search.obj[type][fieldRemove])) delete fObj.search.obj[type][fieldRemove];
							});
						}else if(field.indexOf("||") > 0 && exists(fObj.search.obj[type]['$or']))
							delete fObj.search.obj[type]['$or'];
						else if(useOr && exists(fObj.search.obj[type]['$or']))
							delete fObj.search.obj[type]['$or'][field];
						else if(exists(fObj.search.obj[type][field]))
							delete fObj.search.obj[type][field];
						else if(exists(fObj.search.obj[type][field+"."+value])){
							delete fObj.search.obj[type][field+"."+value];
						}
					}
					else
						delete fObj.search.obj[type];
				}else if(notNull(event) && event=="inArray"){
					var valueArr=(exists(key)) ? key : value;
					if(type == "filters" && exists(fObj.search.obj[type][field])){
						fObj.search.obj[type][field]['$in']=removeFromArray(fObj.search.obj[type][field]['$in'], valueArr);
						if(!notEmpty(fObj.search.obj[type][field]['$in'])) delete fObj.search.obj[type][field];
					}else{
						if (typeof value != undefined && type == "valueArray") {
							if(otherField){
								var arrayValue = (value.includes("\\")) ? value.split("\\") : [value];
								fObj.search.obj.filters["$or"][field]['$in']=removeArray(fObj.search.obj.filters["$or"][field]['$in'], arrayValue);
								fObj.search.obj.filters["$or"][otherField]['$in']=removeArray(fObj.search.obj.filters["$or"][otherField]['$in'], [label]);
								if(!notEmpty(fObj.search.obj.filters["$or"][field]['$in'])) delete fObj.search.obj.filters["$or"][field];
								if(!notEmpty(fObj.search.obj.filters["$or"][otherField]['$in'])) delete fObj.search.obj.filters["$or"][otherField];
								if(!notEmpty(fObj.search.obj.filters["$or"])) delete fObj.search.obj.filters["$or"];
							}else{
								var arrayValue = (value.includes("\\")) ? value.split("\\") : [value];
								if(exists(fObj.search.obj.filters["$or"]) && exists(fObj.search.obj.filters["$or"][field]) && exists(fObj.search.obj.filters["$or"][field]['$in'])){
									fObj.search.obj.filters["$or"][field]['$in']=removeArray(fObj.search.obj.filters["$or"][field]['$in'], arrayValue);
									if(!notEmpty(fObj.search.obj.filters["$or"][field]['$in'])) delete fObj.search.obj.filters["$or"][field];
									if(!notEmpty(fObj.search.obj.filters["$or"])) delete fObj.search.obj.filters["$or"];	
								}else{
									fObj.search.obj.filters[field]['$in']=removeArray(fObj.search.obj.filters[field]['$in'], arrayValue);
									if(!notEmpty(fObj.search.obj.filters[field]['$in'])) delete fObj.search.obj.filters[field]['$in'];
								}
							}
							if(fObj.search.obj[type].includes(key))
								fObj.search.obj[type].splice(fObj.search.obj[type].indexOf(key), 1)
						} else {
							fObj.search.obj[type]['$in']=removeFromArray(fObj.search.obj[type]['$in'], valueArr);
							if(!notEmpty(fObj.search.obj[type]['$in'])) delete fObj.search.obj[type];
						}
					}
				}
				else if(type=="filters"){
					valToRemove=(notNull(key)) ? key : value;
					mylog.log("searchObj.manage removeActive filters", fObj.search.obj);
					if(typeof fObj.search.obj.filters[field] == "string")
						delete fObj.search.obj.filters[field];
					if( typeof fObj.search.obj.filters != "undefined" && typeof fObj.search.obj.filters[field] != "undefined"){
						// fObj.search.obj.filters[field].splice(fObj.search.obj.filters[field].indexOf(valToRemove),1);
						fObj.search.obj.filters[field] = removeFromArray(fObj.search.obj.filters[field], valToRemove);
						if(!notEmpty(fObj.search.obj.filters[field]))
							delete fObj.search.obj.filters[field];
					}
				}else if(type=="types")
					fObj.filters.actions.types.remove(fObj, key);
				else if(type=="price")
					fObj.filters.actions.price.remove(fObj);
				else if(type=="tags"){
					var id = generateIdFromString(value, "filter_")
					$("#"+id).removeClass("active")
					if(Array.isArray(fObj.search.obj.tags))
						fObj.search.obj.tags.splice(fObj.search.obj.tags.indexOf(value),1);
					else{
						fObj.search.obj.tags="";
					}
					
				}
				else if(type=="scope"){
					delete myScopes.open[key];
				}
				else if(type=="around"){
					delete fObj.search.obj.geoSearch;
				}else if(typeof fObj.search.obj[type] != "undefined"){
					if(Array.isArray(fObj.search.obj[type])){
						keyValue=(notNull(key)) ? key : value;
						fObj.search.obj[type]=removeFromArray(fObj.search.obj[type], keyValue);
					}
					else{
						delete fObj.search.obj[type];
					}
				}else if(type=="url"){
					fObj.urlData=field;
				}
				$(fObj.filters.dom+` .filters-activate[data-type='${type}'][data-key="${key}"][${typeof field != 'undefined' ? 'data-field="'+field+'"' : 'data-value="'+value+'"'}]`).remove();
				$(`#centerFilters-active .filters-activate[data-type='${type}'][data-key="${key}"][${typeof field != 'undefined' ? 'data-field="'+field+'"' : 'data-value="'+value+'"'}]`).remove();
				
				//find and check if all child does not have class active then remove active class on the title head
				const filterBtn = $(fObj.filters.dom+` button[data-type='${type}'][data-key="${key}"][${typeof field != 'undefined' ? 'data-field="'+field+'"' : 'data-value="'+value+'"'}]`);
				filterBtn.removeClass('active');
				filterBtn.removeAttr('data-active-elem');
				if(filterBtn.parent().find('.accordion-row.active').length < 1)
					filterBtn.parent().parent().parent().find(`div.panel-heading[data-parent="#filterContainerInside"]`).removeClass("active")
				
				fObj.filters.manage.updateActiveCounter(fObj, value)
				if(!notNull(noStartSearch)){
					fObj.results.map.changedfilters=true;
					fObj.search.init(fObj);
				}

				if($(fObj.filters.dom+" .filters-activate").length > 0) {
					$(fObj.filters.dom+" .saveActiveFilter").removeClass("disabled");
					$("#aapSaveFilters .saveActiveFilter").removeClass("disabled");
					$("#centerFilters-active").addClass("notnull")
				} else {
					$(fObj.filters.dom+" .saveActiveFilter").addClass("disabled");
					$("#aapSaveFilters .saveActiveFilter").addClass("disabled");
					$("#centerFilters-active").removeClass("notnull")
				}

				if($(fObj.filters.dom+ " #activeFilters .activeFilters").length==0){
					$(fObj.filters.dom+ " #activeFilters").hide();

				}
			},
			xsActiveFilter : function(fObj){
				countFilter=$(fObj.filters.dom+" .filters-activate").length;
				if( countFilter > 0)
				{
				    $(fObj.filters.dom+" .filter-xs-count").html(countFilter);
					$(fObj.filters.dom+" .filter-xs-count").removeClass('hide').addClass('animated bounceIn badge-success');
					$("#show-filters-lg .filter-xs-count,.showHide-filters-xs .filter-xs-count").html(countFilter);
					$("#show-filters-lg .filter-xs-count,.showHide-filters-xs .filter-xs-count").removeClass('hide').addClass('animated bounceIn badge-success')
				} else {
					$(fObj.filters.dom+" .filter-xs-count").html("");
					$(fObj.filters.dom+" .filter-xs-count").removeClass('animated bounceIn badge-success').addClass('hide');
					$("#show-filters-lg .filter-xs-count,.showHide-filters-xs .filter-xs-count").html("");
					$("#show-filters-lg .filter-xs-count,.showHide-filters-xs .filter-xs-count").removeClass('animated bounceIn badge-success').addClass('hide')
				}
			},
			updateActiveCounter: function(fObj, value){
				if(fObj.layoutDirection && fObj.layoutDirection === "vertical"){
					var id = generateIdFromString(value, "filter_"),
						dataName = $("#"+id).data("name"),
						totalActive = $("button.active[data-name='"+dataName+"']").length;
					$('#filter-'+dataName+" span.counter").text((totalActive>0)?totalActive:"")
				}
			},
			resetFilter : function(fObjInitial, filterKey) {
				if(typeof fObjInitial.filters != "undefined" && typeof fObjInitial.filters.dom != "undefined" && filterKey) {
					$(fObjInitial.filters.dom +" "+filterKey+".active").each(function(){
						$(this).removeClass("active");
						fObjInitial.filters.manage.removeActive(fObjInitial, $(this), true);
						fObjInitial.filters.manage.xsActiveFilter(fObjInitial);
						fObjInitial.filters.manage.updateActiveCounter(fObjInitial, 0)
					});
					$(fObjInitial.filters.dom+" .main-search-bar,"+fObjInitial.filters.dom.replace("#", ".")+".filter-alias.custom-filter  .alias-main-search-bar").val("");
					if(fObjInitial.search.obj && fObjInitial.search.obj.text)
						fObjInitial.search.obj.text = ""
					fObjInitial.results.map ? fObjInitial.results.map.changedfilters=true : "";
					fObjInitial.search.init(fObjInitial);
				}
			}
		},
		lists : {
			scopeList : {},
			themes : {}
		},
		actions : {
			price : {
				add: function(fObj){
					priceStr="";
					deviseStr=(typeof fObj.search.obj.devise != "undefined") ? fObj.search.obj.devise : "€";
					if(typeof fObj.search.obj.priceMin != "undefined")
						priceStr=trad.price+" > "+fObj.search.obj.priceMin+" "+deviseStr;
					if(typeof fObj.search.obj.priceMax != "undefined")
						priceStr=(notEmpty(priceStr)) ?  fObj.search.obj.priceMin+" "+deviseStr+" < "+trad.price+" < "+fObj.search.obj.priceMax+" "+deviseStr : trad.price+" < "+fObj.search.obj.priceMax+" "+deviseStr;
					fObj.filters.manage.addActive(fObj, {type : "price", "value": priceStr}, true);
				},
				remove: function(fObj){
					if($(fObj.filters.dom+' .filters-activate[data-type="price"]').length > 0)
						$(fObj.filters.dom+' .filters-activate[data-type="price"]').fadeOut().remove();
					if(typeof fObj.search.obj.priceMin != "undefined")
						delete fObj.search.obj.priceMin;
					if(typeof fObj.search.obj.priceMax != "undefined")
						delete fObj.search.obj.priceMax;
				}
			},
			scopeList : {
				scopeObject: function(values){
					mylog.log("searchObj.actions.scopeList.scopeObject", values);
					var objToPush={
						name:values.name,
						active:true,
					} ;
					if(typeof values.id != "undefined")
						objToPush.id = values.id;
					else if(typeof values._id != "undefined")
						objToPush.id = values._id.$id;

					if(typeof values.country != "undefined")
						objToPush.countryCode = values.country;
					else
						objToPush.countryCode = values.countryCode;

					if(typeof values.postalCode != "undefined")
						objToPush.postalCode = values.postalCode;

					if(typeof values.key != "undefined")
						objToPush.key = values.key;

					if(typeof values.postalCode != "undefined" && typeof values.uniqueCp != "undefined" && values.uniqueCp == false){
						mylog.log("searchObj.actions.scopeList.scopeObject communexionObj cp values", values);
						objToPush.type = "cities";
						objToPush.name = values.cityName;
						//objToPush.active = ((notNull(values.allCP) && values.allCP == false) ?  false : true );
						objToPush.allCP = values.allCP;
						objToPush.id = values.city;
						objToPush.postalCode = values.postalCode;
						if(typeof objToPush.key == "undefined")
							objToPush.key = values.id+"city"+values.postalCode;
					}else if(typeof values.level != "undefined"){
						var l = values.level[0] ;
						objToPush.level = parseInt(l);
						objToPush.type = "level"+l;
						if(typeof objToPush.key == "undefined")
							objToPush.key = objToPush.id+objToPush.type;
					} else {
						objToPush.type = "cities";
						//objToPush.name = ((notNull(values.allCP) && values.allCP == false) ?  values.name : values.cityName );
						objToPush.name = (typeof values.name!="undefined") ? values.name : values.cityName;
						//TODO demander a rapha qu'elle est l'utilité de la variable objToPush.active et de celle du dessus
						// objToPush.active = ((notNull(values.allCP) && values.allCP == false) ?  false : true );
						objToPush.allCP = (typeof values.postalCodes!="undefined") ? values.postalCodes : values.allCP;
						objToPush.id = (typeof values._id!="undefined" && typeof values._id.$id!="undefined") ? values._id.$id : values.city;

						if(typeof objToPush.key == "undefined")
							objToPush.key = (typeof objToPush.id!="undefined") ? objToPush.id+"city" : values.id+"city";
					}


					mylog.log("searchObj.actions.scopeList.scopeObject objToPush", objToPush);
					return objToPush;
				}
			},
			text : {
				launch : function(fObj, key, e=null, dom=null){
					var htmlDom=(notNull(dom)) ? dom : fObj.filters.dom;
					searchValue=$(htmlDom+" .searchBar-filters .main-search-bar[data-field='"+key+"']").val();
					var textPath = $(htmlDom+" .searchBar-filters .main-search-bar[data-field='"+key+"']").data('text-path');
					var searchBy = $(htmlDom+" .searchBar-filters .main-search-bar[data-field='"+key+"']").data('search-by');
					var launchIt = true;
					if(key=="text" || textPath !=""){
						if(textPath !="")
							key ="text";
						if(searchValue.charAt(0)=="#"){
							if((e.keyCode==13 || e==null) && searchValue.length>2 && !fObj.search.obj["tags"].includes(searchValue.replace("#",""))){
								const tagValue = searchValue.replace("#","")
								fObj.search.obj["tags"].push(tagValue);
								fObj.filters.manage.addActive(fObj,{type : "tags", value : tagValue, label : tagValue});
								launchIt = true;
							}else{
								launchIt = false;
							}
						}else{
							fObj.search.obj[key] = searchValue;
							if(textPath !="") fObj.search.obj.textPath = textPath;
							if(searchBy !="") fObj.search.obj.searchBy = searchBy;
						}
					}else{
						if(notEmpty(searchValue))
							fObj.search.obj.filters[key] = searchValue;
						else if(typeof fObj.search.obj.filters[key] != "undefined")
							delete fObj.search.obj.filters[key];
					}
					if(launchIt){
						fObj.results.map.changedfilters=true;
						fObj.filters.actions.text.spin(fObj, true, key);
						fObj.search.init(fObj);
					}
				},
				spin : function(fObj, bool, key, domTarget){
				    parentContainer=fObj.filters.dom+ " .searchBar-filters";
					dataField=(notNull(key)) ? "[data-field='"+key+"']" : "";
					defaultClass = typeof $(parentContainer+" .main-search-bar-addon"+dataField).attr('data-icon') != 'undefined' ? $(parentContainer+" .main-search-bar-addon"+dataField).attr('data-icon') : "fa-arrow-circle-right";
					removeClass= (bool) ? defaultClass : "fa-spin fa-circle-o-notch";
				    addClass= (bool) ? "fa-spin fa-circle-o-notch" : defaultClass;
				    if(notNull(domTarget)){
				    	parentContainer="";
				    	dataField=domTarget;
				    }
				    if(!bool){
				    	parentContainer="";
				    	dataField="";
				    }
					var iconSelector = $(parentContainer+" .main-search-bar-addon"+dataField);
					if(typeof $(parentContainer+" .main-search-bar-addon"+dataField).attr('data-alias') != 'undefined') {
						var selectorStr = $(parentContainer+" .main-search-bar-addon"+dataField).attr('data-alias');
						iconSelector = $(parentContainer+" .main-search-bar-addon"+dataField+","+selectorStr)
					}
				    iconSelector.find("i").removeClass(removeClass).addClass(addClass);
				}
			},
			multiple : {
				add : function(fObj, type, key, value){
					fObj.search.obj[type]=[];
					if(notNull(key))
						fObj.search.obj[type].push(key);
					$(fObj.filters.dom + " #activeFilters .filters-activate[data-type='"+type+"']").each(function(){
						if($.inArray($(this).data('key'), fObj.search.obj[type]) < 0)
							fObj.search.obj[type].push($(this).data('key'));
					});
				},
				remove : function(fObj, type, key, value){
					if (fObj.search.obj[type].indexOf(key) > -1)
						fObj.search.obj[type].splice(fObj.search.obj[type].indexOf(key),1);

				}
			},
			types : {
				initList : [],
				add : function(fObj, key, count){
					fObj.search.obj.types=[];
					if(notNull(key))
						fObj.search.obj.types.push(key);
					$(fObj.filters.dom + " #activeFilters .filters-activate[data-type='types']").each(function(){
						if($.inArray($(this).data('key'), fObj.search.obj.types) < 0)
							fObj.search.obj.types.push($(this).data('key'));
					});
					fObj.filters.actions.types.checkTypes(fObj, count);
				},
				remove: function(fObj, key, checkTypes){
					if (fObj.search.obj.types.indexOf(key) > -1)
						fObj.search.obj.types.splice(fObj.search.obj.types.indexOf(key),1);
					if(checkTypes!==false){
						$(fObj.filters.dom + " #activeFilters .filters-activate[data-type='types']").each(function(){
							if($.inArray($(this).data('key'), fObj.search.obj.types) < 0)
								fObj.search.obj.types.push($(this).data('key'));
						});
						fObj.filters.actions.types.checkTypes(fObj);
					}
				},
				checkTypes : function(fObj, count){
					if(fObj.search.obj.types.length==0) fObj.search.obj.types=jQuery.extend(true, [], fObj.filters.actions.types.initList);
					fObj.results.countResults(fObj);
					if(count !== false)
						fObj.header.set(fObj);
				},
				setBadges : function(fObj){
					$.each(fObj.results.count, function(e,v){
						//var type=(e=='citoyens')? 'persons' : e;
						$('#count'+e).text(v);
					});
				}
			},
			themes : {
				initList : [],
				isLoaded:false,
				callBack : function(data){},
				setThemesCounts : function(fObj){
					
					$('.badge-theme-count').each(function() {
							$(this).text(0);
					});
					
					ajaxPost(
		                null,
		                // baseUrl+"/"+moduleId+"/search/globalautocomplete",
		                fObj.urlData,
		                {	"filters":fObj.search.obj.filters,
		                	"searchType" : fObj.search.obj.types,
		                	"notSourceKey" : fObj.search.obj.notSourceKey,
		                	"indexStep" : "0",
		                	"fields" : typeof fObj.filters != "undefined" && typeof fObj.filters.views != "undefined" && exists(fObj.filters.views.countFieldPath) ? ["name", "tags", "extraInfo"].concat(fObj.filters.views.countFieldPath) : ["name", "tags", "extraInfo","answers"]
		                },
		                function(data){
		                	if(!data){
		                		toastr.error(data.content);
		                	}else{
		                		let results = data.results;
								mylog.log("Set themes counts"+results.length, results,fObj.search.obj.filters);
								var createCounter = function(countField, field = ""){
									if(countField){
										var tagsSet = Array.from(new Set(countField));
										for (var i = tagsSet.length-1; i >= 0; i--) {
											let tradKey = Object.keys(trad).find(k=>trad[k].toLowerCase()==tagsSet[i].toLowerCase());
											let prefixSelctor = field ? ` [data-field="${ field }"] ` : "";
											let themeBadge = $(fObj.container+ prefixSelctor + ' [data-countkey="'+tagsSet[i].replace(/"/g, '&quot;')+'"]');

											if(themeBadge.length==0){
												themeBadge = $(fObj.container+ prefixSelctor + ' [data-countvalue="'+tagsSet[i].toLowerCase().trim().replace(/\s/g, '').replace(/"/g, '&quot;')+'"]');
											}
											
											if(themeBadge.length==0){
												themeBadge = $(fObj.container+ prefixSelctor + ' [data-countkey="'+(tradKey ? tradKey : '').replace(/"/g, '&quot;')+'"]');
											}

											if(themeBadge.length==0){
												$(fObj.container+' .badge-theme-count').each(function() {
													if(Array.isArray($(this).data("countvalue")) && $(this).data("countvalue").includes(tagsSet[i])){
														themeBadge = $(this);
													}
												})
											}

											if(themeBadge.length!=0){
												//if(themeBadge.data("countlock")=="false"){
													let count = parseInt(themeBadge.html(), 10)+1;
													themeBadge.html(count);
													themeBadge.removeClass("label-default");
													themeBadge.addClass("label-primary");
												//}
											}
										}
									}
								}

								for (const [key, value] of Object.entries(results)) {
									if(exists(fObj.filters.views.countFieldPath)){
										$.each(fObj.filters.views.countFieldPath, function(i,j){
											let countField = jsonHelper.getValueByPath(value,j);
											if(typeof countField == "string")
												countField = countField.split();
											createCounter(countField, j)
										})
									}

									if(!exists(fObj.filters.views.countOnlyPath) && exists(value.tags)){
										let countField = value.tags;
										createCounter(countField);
									}
								}

								//sort counter
								if(exists(fObj.filters.views) && exists(fObj.filters.views.countFieldPath)){
									$.each(fObj.filters.views.countFieldPath,function(i,j){
										var list = $(fObj.container+" [data-field='"+j+"'] .badge-theme-count").get();
										list.sort(function(a, b) {
											return parseInt(b.innerHTML) - parseInt(a.innerHTML);
										});

										if(Array.isArray(list) && exists(list[0])){
											var parent = list[0].parentNode.parentNode;
											for (var i = 0; i < list.length; i++) {
												parent.appendChild(list[i].parentNode);
											}
										}
									});
								}

								$(fObj.container+' .badge-theme-count').each(function() {
									$(this).data("countlock", "true");
									if($(this).text()=="0" && $(this).hasClass("remove0")){
										$(this).parent().remove();
									}
								});


								fObj.filters.actions.themes.isLoaded = true;
								fObj.filters.actions.themes.callBack(results);
								
		                	}
						}
					);
				}
			},
			geoPosition:{
				getLocation: function(fObj) {
		  			if (navigator.geolocation) {
		    			navigator.geolocation.getCurrentPosition(fObj.filters.actions.geoPosition.showPosition);
		  			} else {
		    			x.innerHTML = "Geolocation is not supported by this browser.";
		  			}
				},
				showPosition: function(position) {
					fObj.search.obj.geoSearch = fObj.filters.actions.geoPosition.getBoundingBox ([
						position.coords.latitude,
						position.coords.longitude
					], fObj.search.obj.dist),
					fObj.search.init(fObj);
				},
				getBoundingBox (centerPoint, distance) {
					var MIN_LAT, MAX_LAT, MIN_LON, MAX_LON, R, radDist, degLat, degLon, radLat, radLon, minLat, maxLat, minLon, maxLon, deltaLon;
					if (distance < 0) {
						return 'Illegal arguments';
					}
					// helper functions (degrees<–>radians)
					Number.prototype.degToRad = function () {
						return this * (Math.PI / 180);
					};
					Number.prototype.radToDeg = function () {
						return (180 * this) / Math.PI;
					};
					// coordinate limits
					MIN_LAT = (-90).degToRad();
					MAX_LAT = (90).degToRad();
					MIN_LON = (-180).degToRad();
					MAX_LON = (180).degToRad();
					// Earth's radius (km)
					R = 6378.1;
					// angular distance in radians on a great circle
					radDist = distance / R;
					// center point coordinates (deg)
					degLat = centerPoint[0];
					degLon = centerPoint[1];
					// center point coordinates (rad)
					radLat = degLat.degToRad();
					radLon = degLon.degToRad();
					// minimum and maximum latitudes for given distance
					minLat = radLat - radDist;
					maxLat = radLat + radDist;
					// minimum and maximum longitudes for given distance
					minLon = void 0;
					maxLon = void 0;
					// define deltaLon to help determine min and max longitudes
					deltaLon = Math.asin(Math.sin(radDist) / Math.cos(radLat));
					if (minLat > MIN_LAT && maxLat < MAX_LAT) {
						minLon = radLon - deltaLon;
						maxLon = radLon + deltaLon;
						if (minLon < MIN_LON) {
							minLon = minLon + 2 * Math.PI;
						}
						if (maxLon > MAX_LON) {
							maxLon = maxLon - 2 * Math.PI;
						}
					}
					// a pole is within the given distance
					else {
						minLat = Math.max(minLat, MIN_LAT);
						maxLat = Math.min(maxLat, MAX_LAT);
						minLon = MIN_LON;
						maxLon = MAX_LON;
					}
					geoBox={
						latMinScope : minLat.radToDeg(),
						latMaxScope : maxLat.radToDeg(),
						lngMinScope : minLon.radToDeg(),
						lngMaxScope : maxLon.radToDeg()
					}
					return geoBox;
				}
			}
		}
	},
	table : {
		initDefaults : function(fObj, pInit){
			fObj.table.init(fObj, pInit);
		},
		init : function(fObj,pInit){
			fObj.search.obj.ranges={};
			$.each(fObj.search.obj.types, function(e, v){
				fObj.search.obj.ranges[v]={indexMin : 0, indexMax : fObj.search.obj.indexStep};
			});
			fObj.search.multiCols.results={};
			fObj.table.directory = directory.table.init(pInit,fObj);
			
		},
		render : function(fObj, results, data){
			fObj.table.directory.results = results;
			fObj.table.directory.initViewTable(fObj.table.directory);
		},
		event : function(fObj){
			$(window).off("scroll");
			if(fObj.search.obj.indexStep > 0){
				if(fObj.results.numberOfResults%fObj.search.obj.indexStep == 0){
					var numberPage=Math.floor(fObj.results.numberOfResults/fObj.search.obj.indexStep);
				}else{
					var numberPage=Math.floor(fObj.results.numberOfResults/fObj.search.obj.indexStep)+1;
				}
			    $('.pageTable').pagination({
			        items: numberPage,
			        itemOnPage: fObj.search.obj.indexStep,
			        currentPage: (fObj.search.obj.nbPage+1),
			        hrefTextPrefix:"?page=",
			        cssStyle: 'light-theme',
			        prevText: '<i class="fa fa-chevron-left"></i> ' + trad["previous"],
			        nextText: trad["next"] + ' <i class="fa fa-chevron-right"></i>',
			        onInit: function () {

			        },
			        onPageClick: function (page, evt) {
						$('.selectAllElements').prop("checked", false);
						$('.countElmSelect').html("");
			        	coInterface.simpleScroll(0, 10);
			        	fObj.search.obj.nbPage=(page-1);
			            fObj.search.obj.indexMin=fObj.search.obj.indexStep*fObj.search.obj.nbPage;
						$(".bodySearchContainer  #table .directoryLines").html("<tr> <td colspan='5' class='text-center'></td></tr>")
						coInterface.showLoader(".bodySearchContainer  #table .directoryLines tr td");
			            fObj.search.start(fObj);
			        }
			    });
			}
		},
	},
	admin : {
		//paramsAdmin : {},
		options : {},
		directory : {},
		initDefaults : function(fObj, pInit){
			mylog.log("searchObj.admin.initSearchObj", pInit);
			/**** Extension de l'objet adminDirectory [/co2/assets/js/admin.js]
				Il contient la vue des entrées type table et les actions liées au événement demandé
			********/
			fObj.admin.directory = adminDirectory.init(pInit.loadEvent.options);
		},
		init : function(fObj){
			mylog.log("searchObj.admin.initf",fObj);
			mylog.log("searchObj.admin.init fObj.admin.adminDirectory", fObj.admin.directory);			
			// if(fObj.search.obj.indexStep > 0){
			// 	fObj.search.obj.nbPage=0;
			// 	if($(fObj.footerDom+" .pageTable").length <=0)
			// 		$(fObj.footerDom).append('<div class="pageTable col-xs-12 text-center"></div>');
			// }
		},
		searchParams : function(fObj, params){
			params.private = true;
			return params
		},

		start : function(fObj){
			mylog.log("searchObj.admin.start");
			coInterface.showLoader(fObj.results.dom, trad.currentlyresearching);
		},
		render : function(fObj, results, data){
      		mylog.log("filterAdmin.results.render adminDirectory", fObj.admin.directory, results, data);
			fObj.admin.directory.results = results;
			fObj.admin.directory.initViewTable(fObj.admin.directory);
			fObj.admin.directory.bindAdminBtnEvents(fObj.admin.directory);
			if(typeof fObj.admin.directory.bindCostum == "function")
				fObj.admin.directory.bindCostum(fObj.admin.directory);
		},
		event : function(fObj){
			mylog.log("searchObj.admin.event");
			if(fObj.search.obj.indexStep > 0){
				if(fObj.results.numberOfResults%fObj.search.obj.indexStep == 0){
					var numberPage=Math.floor(fObj.results.numberOfResults/fObj.search.obj.indexStep);
				}else{
					var numberPage=Math.floor(fObj.results.numberOfResults/fObj.search.obj.indexStep)+1;
				}
			    $('.pageTable').pagination({
			        items: numberPage,
			        itemOnPage: fObj.search.obj.indexStep,
			        currentPage: (fObj.search.obj.nbPage+1),
			        hrefTextPrefix:"?page=",
			        cssStyle: 'light-theme',
			        prevText: '<i class="fa fa-chevron-left"></i> ' + trad["previous"],
			        nextText: trad["next"] + ' <i class="fa fa-chevron-right"></i>',
			        onInit: function () {

			        },
			        onPageClick: function (page, evt) {
			        	coInterface.simpleScroll(0, 10);
			        	fObj.search.obj.nbPage=(page-1);
			            fObj.search.obj.indexMin=fObj.search.obj.indexStep*fObj.search.obj.nbPage;
			            fObj.search.start(fObj);
			        }
			    });
			}
		},
    	callBack : function(fObj, data){
    		mylog.log("searchObj.admin.callBack");
			fObj.admin.directory.results = data;
			if($("#"+fObj.admin.directory.container+" .sortAdmin").length > 0){
				$("#"+fObj.admin.directory.container+" .sortAdmin").off().on("click",function(){
					fObj.search.obj.sort = {};
					if($(this).data("sort") == "up")
						fObj.search.obj.sort[$(this).data("field")] = 1 ;
					else
						fObj.search.obj.sort[$(this).data("field")] = -1 ;
					fObj.search.start(fObj);
				});
			}
    	}
	},
	dashboard : {
		options : {},
		successComplete : function(fObj, data){
			fObj.search.currentlyLoading = true;
			$(fObj.results.dom).find(".loader").remove();
    		$(fObj.results.dom+" .processingLoader").remove();
        	$(fObj.results.dom).html(data);

		}
	},
	crowdfunding : {
		event : function(fObj){
			if(contextData){

			//alert(contextData.links[links.connectType[contextData.type]][userId].isAdmin);
				if(typeof contextData!="undefined" && typeof contextData.links!="undefined" && typeof contextData.type!="undefined" && typeof contextData.links[links.connectType[contextData.type]]!="undefined"  && typeof contextData.links[links.connectType[contextData.type]][userId]!="undefined" && contextData.links[links.connectType[contextData.type]][userId].isAdmin==true){
			// if(isElementAdmin){
					var str='';
					str+='<div id="headCrowd">';
				// See pledges and donations only from direct project parent of a campaign
					if(typeof fObj!="undefined" && typeof fObj.search!="undefined" && typeof fObj.search.obj!="undefined" && typeof fObj.search.obj.types!="undefined" && fObj.search.obj.types.length==1 && fObj.search.obj.types[0]=="crowdfunding"){

						if(fObj.resultKeys.length>0){


							str+=	'<span class="" style="padding:5px;margin:10px;">Accéder et gérer : </span>';
							str+=	'<button type="button" style="padding:5px;margin:10px;" class="pledgeTable" data-type="pledge">Promesse de dons</button>';
							str+=	'<button type="button" style="padding:5px;margin:10px;" class="pledgeTable" data-type="donation">Dons</button>';


							$(".pledgeTable").off().on("click",function(){

								var typeTable=$(this).data("type");
								var title ='Récapitulatif des ';
								  	title += (typeTable=="pledge") ? "promesses de dons" : "dons" ;

								  	var data={
								    title : title,
								    //types : ["crowdfunding"],
								    table : {
								      name: {
								        name : "Nom"
								      },
								      amount : {
								        name : "Montant",
								        sum : true
								      },
								      type : {
								        name : "type"
								      },
								      validateField :{
								        field : "type",
								        value : "donation"
								      }

								    },
								    actions:{
								       delete : true
								    },
								    paramsFilter : {
								        // container : "#filterContainer",
								        defaults : {
								          types : [ "crowdfunding" ],
								          //type : "pledge"
								          filters : {
								            type : typeTable,
								          }
								        }
								      }

								  };

								  if(typeTable=="pledge"){
								  	data.actions.validatePledge = true;
								  }

								 data.paramsFilter.defaults.filters["$or"]={};
								  //data.paramsFilter.defaults.filters.parent[contextData.id]={'$exists' :true};
								  $.each(fObj.resultKeys,function(i,k){
								  	data.paramsFilter.defaults.filters["$or"]["parent."+k] = {'$exists' :true};
								  });


								  ajaxPost('#ajax-modal-modal-body', baseUrl+'/'+moduleId+'/admin/directory/type/'+contextData.type+'/id/'+contextData.id, data, function(){},"html");
								  // $("#ajax-modal .close-modal, #ajax-modal button[data-dismiss='modal']").off().on("click",function(){
								  // 	 filterGroup.search.init(filterGroup);
								  // });
								  $("#ajax-modal").modal("show");
								 $("#ajax-modal .modal-header").hide();
									$("#ajax-modal-modal-title").hide();

							});
						}
					}else{
						str+='<span><i class="fa fa-info"><i> Les campagnes de financement participatif sont obligatoirement liées à un projet. Si vous souhaitez créer une campagne, créez préalablement un projet qui la porte.</span>'

					}
					str+='</div>';
					if ($("#headCrowd")){
						$("#headCrowd").remove();
					}
					$(str).insertBefore(fObj.header.dom);

				}
			}
		},
		render : function (fObj, results, data){

			var urlRender=baseUrl+"/" + moduleId + "/crowdfunding/getcampaignandcounters";
			mylog.log("fObj render",fObj);

			// if the returning data is projects that have campaign and not crowdfunding campaign
			if(typeof fObj!="undefined" && typeof fObj.search!="undefined" && typeof fObj.search.obj!="undefined" && typeof fObj.search.obj.types!="undefined" && fObj.search.obj.types.length==1 && fObj.search.obj.types[0]=="projects"){
				urlRender=urlRender+"byparent";
			}

			ajaxPost(
				null,
				urlRender,
				data,
				function(data){
					var results=data.results;
					fObj.resultKeys=[];
					fObj.resultKeys=Object.keys(results);
					fObj.results.render(fObj, results, data);
				},
				null,
				null,
				{async:false}
			);

		}
	},
	graph : {
		dom: '#graph-container',
		graph: null,
		circle: null,
		circlerelation: null,
		relation: null,
		circlerelations: [],
		mindmap : null,
		circularbarplot: null,
		network: null,
		venn: null,
		tooltip: null,
		currentGraph: null,
		validGraphs: ['circle', 'relation', 'network', 'mindmap', 'circlerelation', 'venn','circularbarplot'],
		lastResult: null,
		authorizedTags: null,
		defaultGraph: 'circle',
		mindmapDepth: 3,
		autoDraw: true,
		max: null,
		initPosition: null,
		init : function(fObj){
			mylog.log("init graph", fObj);
			if(fObj.pInit.graph){
				if(fObj.pInit.graph.dom){
					this.dom = fObj.pInit.graph.dom;
				}
				if(fObj.pInit.graph.authorizedTags){
					this.authorizedTags = fObj.pInit.graph.authorizedTags;
				}
				if(typeof fObj.pInit.graph.autoDraw != "undefined"){
					this.autoDraw = fObj.pInit.graph.autoDraw;
				}
				if(fObj.pInit.graph.defaultGraph){
					this.defaultGraph = fObj.pInit.graph.defaultGraph
				}
				if(fObj.pInit.graph.mindmapDepth){
					this.mindmapDepth = fObj.pInit.graph.mindmapDepth
				}
				if(fObj.pInit.graph.circlerelations){
					this.circlerelations = fObj.pInit.graph.circlerelations
				}
				if(fObj.pInit.graph.initPosition){
					this.initPosition = fObj.pInit.graph.initPosition
				}
				if(fObj.pInit.graph.max){
					this.max = fObj.pInit.graph.max
				}
			}
			
			if(this.currentGraph){
				graphName = this.currentGraph;
			}
			this.tooltip = new GraphTooltip(this.dom)
			this.tooltip.hide();
			window.onresize = (e) => {
				this.tooltip.goToNode();
			}

			this.circle = new CircleGraph([], d => d.group, this.authorizedTags)
			this.venn = new VennGraph([], this.authorizedTags)
			this.circlerelation = new CircleRelationGraph([], d => d.group, this.authorizedTags, this.circlerelations)
			this.relation = new RelationGraph([], this.authorizedTags)
			this.network = new NetworkGraph([], d => d.data.type);
			this.mindmap = new MindmapGraph([], this.mindmapDepth, {id: "root", label: "Recherche"});
			this.circularbarplot = new CircularBarplotGraph();
			if(this.max){
				this.circularbarplot.setMax(this.max);
			}
			this.circlerelation.setInitPosition(this.initPosition);
			this.network.setLabelFunc((d,i,n) => {
				if("group" == d.data.group){
					if(Object.keys(trad).includes(d.data.label)){
						return trad[d.data.label];
					}
				}
				if("root" == d.data.group){
					if(typeof d.data.label == undefined){
						return "Recherche";
					}
				}
				return d.data.label;
			})

			this.mindmap.setLabelFunc((d,i,n) => {
				if(d.data.id == "root"){
					return "Recherche";
				}
				if(Object.keys(trad).includes(d.data.label)){
					return trad[d.data.label];
				}
				return d.data.label;
			})
			this.circlerelation.setOnClickNode((e,d,n) => {
				console.log("CLICKED",e,d,n)
				this.tooltip.node = d3.select(e.currentTarget)
				this.tooltip.setContent(d)
				this.tooltip.show()
			})
			this.circlerelation.setOnZoom((e,d,n) => {
				this.tooltip.goToNode();
			})
			this.circle.setOnClickNode((e,d,n) => {
				console.log("CLICKED",e,d,n)
				this.tooltip.node = d3.select(e.currentTarget)
				this.tooltip.setContent(d)
				this.tooltip.show()
			})
			this.circle.setOnZoom((e,d,n) => {
				this.tooltip.goToNode();
			})
			this.venn.setOnClickNode((e,d,n) => {
				console.log("CLICKED",e,d,n)
				this.tooltip.node = d3.select(e.currentTarget)
				this.tooltip.setContent(d)
				this.tooltip.show()
			})
			this.venn.setOnZoom((e,d,n) => {
				this.tooltip.goToNode();
			})
			this.relation.setOnClickNode((e,d,n) => {
				console.log("CLICKED",e,d,n)
				this.tooltip.node = d3.select(e.currentTarget)
				this.tooltip.setContent(d)
				this.tooltip.show()
			})
			this.relation.setOnZoom((e,d,n) => {
				this.tooltip.goToNode();
			})
			this.network.setOnClickNode((e,d,n) => {
				if(["group", "root"].includes(d.data.group)){
					return;
				}
				console.log("CLICKED",e,d,n)
				this.tooltip.node = d3.select(e.currentTarget).select("circle")
				this.tooltip.setContent(d)
				this.tooltip.show()
			})
			this.network.setOnZoom((e,d,n) => {
				this.tooltip.goToNode();
			})
			this.network.setOnTick(() => {
				this.tooltip.goToNode();
			})

			this.mindmap.setOnClickNode((e,d,n) => {
				console.log("CLICKED",e,d,n)
				if(d.data.collection){
					/*this.tooltip.node = d3.select(e.currentTarget)
					this.tooltip.setContent(d)
					this.tooltip.show()*/
					urlCtrl.openPreview("#page.type."+d.data.collection+".id."+d.data.id);
				}else if(d.data.id){
					urlCtrl.openPreview("#page.type.citoyens.id."+d.data.id);
				}
			})
			this.mindmap.setOnMouseoverNode((e,d) => {
				this.tooltip.goToNode();
			})
			this.mindmap.setOnMouseoutNode((e,d) => {
				this.tooltip.goToNode();
			})
			this.mindmap.setOnZoom((e,d,n) => {
				this.tooltip.goToNode();
			})

			coInterface.bindLBHLinks();
			if(fObj.results.graph.active){
				this.switchGraph(fObj, fObj.results.graph.active)
			}else{
				this.switchGraph(fObj, this.defaultGraph)
			}
			return true;
		},
		switchGraph : function(fObj, graphName){
			console.log("SWITCH TO", graphName);
			if(fObj.graph.validGraphs.includes(graphName.toLowerCase()) && fObj.graph.currentGraph != graphName.toLowerCase()){
				if(fObj.graph.graph && fObj.graph.graph._rootSvg){
					fObj.graph.graph._rootSvg.remove();
				}
				fObj.results.graph.active = graphName;
				fObj.graph.graph = fObj.graph[graphName];
				if(fObj.graph.autoDraw){
					const loader = $(fObj.graph.dom).append('<div id="loader-graph"></div>')
					coInterface.showLoader(loader.find('#loader-graph'), 'Chargement')
					fObj.graph.beforeDraw(fObj);
					fObj.graph.graph.draw(fObj.graph.dom);
				}
				if(fObj.graph.lastResult){
					fObj.graph.graph.updateData(fObj.graph.graph.preprocessResults(fObj.graph.lastResult.results), fObj.graph.autoDraw);
				}
				// console.log("HERE")
				$(this.dom).find('#loader-graph').remove()
				// window.location.hash = "#graph-" + graphName
			}
		},
		beforeDraw : function(fObj){

		},
		draw: function(fObj){
			if(!fObj.graph.graph.isDrawed){
				fObj.graph.graph.draw(fObj.graph.dom);
			}
			if(fObj.graph.lastResult){
				fObj.graph.graph.updateData(fObj.graph.graph.preprocessResults(fObj.graph.lastResult.results), true);
			}
		},
		successComplete : function(fObj, rawData){
			const tags = fObj.search.obj.tags ? fObj.search.obj.tags : [];
			const authorizedTags = tags.length > 0 ? tags.filter(el => fObj.graph.authorizedTags.includes(el)) : fObj.graph.authorizedTags;
			fObj.graph.graph.setAuthorizedTags(authorizedTags);
			this.lastResult = rawData;
			fObj.graph.graph.updateData(this.graph.preprocessResults(rawData.results), fObj.graph.autoDraw);
			fObj.graph.graph.initZoom();
		},
	},
	live : {
		stopPropagation : false,
		options : {
			nbCol : 2,
			dateLimit : 0,
			indexStep: 5,
			inline : false,
			isLive : true,
			scroll : true
		},
		init : function(fObj){
			fObj.live.options.dateLimit=0;
			coInterface.showLoader(fObj.results.dom, trad.currentlyresearching);
			var scrollTopPosFilter=(typeof pageProfil != "undefined" && typeof pageProfil.affixPageMenu != "undefined") ? pageProfil.affixPageMenu+5 : 0;
			if(notNull(fObj.live.options.scroll) && fObj.live.options.scroll!=="false") 
				coInterface.simpleScroll(scrollTopPosFilter, 400);
		},
		searchParams : function(fObj, searchOpt){
			var searchParams=$.extend({},fObj.live.options);
			searchParams.search=searchOpt;
			if(typeof fObj.live.options.search != "undefined" && notEmpty(fObj.live.options.search)){
				$.each(["searchTags", "locality", "type"], function(e,v){
					if(typeof fObj.live.options.search[v] != "undefined" && notEmpty(fObj.live.options.search[v]))
						searchParams.search[v]=fObj.live.options.search[v];
				});
			}
			if (searchParams.scroll == "false") {
				fObj.live.stopPropagation = true;
			}
			return searchParams;
		},
		start : function(fObj){
		},
		successComplete : function(fObj, data){
			if(fObj.search.loadEvent.bind){
				fObj[fObj.search.loadEvent.active].event(fObj);
				fObj.search.loadEvent.bind=false;
			}
			fObj.search.currentlyLoading = false;
			$(fObj.results.dom+" .container-live").find(".loader").remove();
    		$(fObj.results.dom+" .processingLoader").remove();
        	$(fObj.results.dom).append(data);
        	if($(fObj.results.dom+" #noMoreNews").length){
				$(fObj.results.dom+" .container-live").append(loading);
				fObj.live.stopPropagation = true;
        	}
        	fObj.live.options.dateLimit=$(fObj.results.dom+" .dateLimit").val();
        	$(fObj.results.dom+" .dateLimit").remove();
        	if($(fObj.results.dom+" .relaunchSearch").length){
        		$(fObj.results.dom+" .relaunchSearch").remove();
        		fObj.search.start(fObj);
			}

			const arr = document.querySelectorAll('img.lzy_img')
			arr.forEach((v) => {
				if (fObj.results.smartGrid) {
					v.dom = fObj.results.dom + " .container-live";
				}
				imageObserver.observe(v);
			});
			fObj.live.callBack(fObj);

		},
		event : function(fObj){
			$(window).off("scroll touchmove", fObj.live.scroll).on("scroll touchmove", fObj, fObj.live.scroll);
		},
		scroll : function(e){
				//mylog.log(fObj);
			   if(!e.data.search.currentlyLoading && !e.data.live.stopPropagation ){
	                var heightWindow = $(e.data.results.dom).height();
	                mylog.log("searchObj.live.event scroll2", heightWindow , ($(this).scrollTop()), (heightWindow-$(window).height()));
	                if( $(this).scrollTop() >= (heightWindow-$(window).height()))
	                       e.data.search.start(e.data);
	            }
	        //});
		},
    	callBack : function(fObj, data){
    		mylog.log("searchObj.live.callBack",data);
    	}
	},
	agenda : {
		options : {
			dayCount : 0,
			noResult : true,
			nbLaunchSesearch : 0,
			countsLaunchSesearch : 0,
			todayDate: null,
			startDate : null,
			heightScroll : 200,
			limitSearch : 7,
			nbSearch : 0,
			searchPasteEvents : false,
			finishSearch: false,
			calendarConfig : {}
		},
		stopPropagation : false,
		init : function(fObj){
			//coInterface.simpleScroll(0, 400);
			coInterface.showLoader(fObj.results.dom, trad.currentlyresearching);
			coInterface.simpleScroll(0, 500);
			fObj.agenda.options.searchPasteEvents = false;
			fObj.agenda.options.finishSearch = false;

			if($(fObj.footerDom+" .pageTable").length <=0)
				$(fObj.footerDom).append('<div class="pageTable col-xs-12 text-center"></div>');
		},
		searchParams : function(fObj, searchOpt){
			if(fObj.agenda.options.dayCount>6 || fObj.agenda.options.searchPasteEvents){
				searchOpt.recurrency=false;
			}
			if(typeof searchOpt.startDate != 'undefined' && searchOpt.startDate != null )
				searchOpt.startDateUTC = moment(searchOpt.startDate*1000).local().format();

			return searchOpt;
		},
		getStartDate : function(fObj){
			mylog.log("searchObj.agenda.getStartDate", fObj.agenda.options.dayCount,fObj.agenda.options.startDate);
			// if(fObj.agenda.options.startDate != null)
			// 	var today = new Date(fObj.agenda.options.startDate);
			// else
			var today = new Date();
			today = new Date(today.setSeconds(0));
			today = new Date(today.setMinutes(0));
			today = new Date(today.setHours(0));
			var STARTDATE = today.setDate(today.getDate());
			mylog.log("searchObj.agenda.getStartDate return", fObj.agenda.options.dayCount, STARTDATE);
			return STARTDATE ;
		},
		getStartMoment : function(fObj){
			mylog.log("searchObj.agenda.getStartMoment", fObj.agenda.options.dayCount, fObj.agenda.options.todayDate);
			//var startDate = fObj.agenda.getStartDate(fObj);
			var startDate = fObj.agenda.options.todayDate;
			var startMoment = moment(startDate).local().set('date', moment(startDate).get('date') + fObj.agenda.options.dayCount).format();
			mylog.log("searchObj.agenda.getStartMoment return", fObj.agenda.options.dayCount, startMoment);
			return startMoment;
		},
		getDateHtml : function(fObj){
			mylog.log("searchObj.agenda.getDateHtml", fObj.agenda.options.startDate, moment(fObj.agenda.options.startDate).local().locale("fr").format('dddd DD MMMM'));

			// var startMoment = fObj.agenda.getStartMoment(fObj);
			// mylog.log("searchObj.agenda.getDateHtml startMoment", startMoment);
			var strdate = "";
			if(!fObj.agenda.options.searchPasteEvents){
				strdate = '<div class="col-xs-12 smartgrid-slide-element start-masonry">' +
							'<div class="dayEvent col-xs-12">' +
								'<h2 class="text-white date">' +
								'<span class="date-label">' +
								moment(fObj.agenda.options.startDate).local().locale("fr").format('dddd DD MMMM') +
								'</span></h2>' +
							'</div>' +
					'</div>';
			}
			return strdate;
		},
		start : function(fObj){
			mylog.log("searchObj.agenda.start", fObj.agenda.options.dayCount);

			//if(fObj.agenda.options.dayCount == 0){
			//	mylog.log("searchObj.agenda.start fObj.header.dom", fObj.results.dom);

			//}


			mylog.log("searchObj.agenda.start 2", fObj.agenda.options.todayDate);
			if( fObj.agenda.options.todayDate == null ){
				fObj.agenda.options.todayDate = fObj.agenda.getStartDate(fObj);
				fObj.agenda.options.startDate = fObj.agenda.options.todayDate
			}
			mylog.log("searchObj.agenda.start before startMoment", fObj.agenda.options.startDate);
			var startMoment = fObj.agenda.getStartMoment(fObj);
			mylog.log("searchObj.agenda.start startMoment", startMoment);
        	fObj.agenda.options.startDate = new Date(startMoment);



	        mylog.log("searchObj.agenda.start fObj.agenda.options.startDate", fObj.agenda.options.dayCount, fObj.agenda.options.startDate);
	        fObj.search.obj.startDate = Math.floor(fObj.agenda.options.startDate / 1000);

		},
		event : function(fObj){
			mylog.log("searchObj.agenda.event");
			$(window).off("scroll touchmove", fObj.agenda.scroll).on("scroll touchmove", fObj, fObj.agenda.scroll);
		},
		scroll : function(e){
			e.preventDefault();
			//mylog.log("searchObj.agenda.event scroll1", !fObj.search.currentlyLoading, !fObj.scroll.stopPropagation );
            if(!e.data.search.currentlyLoading && !e.data.agenda.options.finishSearch){
             	if(e.data.agenda.options.searchPasteEvents && !e.data.scroll.stopPropagation){
	                var heightWindow =  $("html").height();
	                if( $(this).scrollTop() >= (heightWindow-$(window).height())){
	                	e.data.search.obj.indexMin=e.data.search.obj.indexMin+e.data.search.obj.indexStep;
	                    e.data.search.start(e.data);
	                }
             	}else if(!e.data.agenda.options.finishSearch){
	                var heightWindow =  $("#search-content").height();
	                //mylog.log("searchObj.agenda.event scroll2", heightWindow , ($(window).scrollTop() + $(window).height()), ($(document).height() - e.agenda.options.heightScroll) );
	                if($(window).scrollTop() + $(window).height() > $(document).height() - e.data.agenda.options.heightScroll) {
	                    //mylog.log("searchObj.agenda.event scroll 3 ", e.agenda.options.dayCount );
	                    if(typeof e.data.agenda.options.dayCount != "undefined"){
	                    	e.data.agenda.options.dayCount++;
	                        e.data.search.start(e.data);
	                    }
	                }
	            }
            }
		},
    	callBack : function(fObj, data){
    		mylog.log("searchObj.agenda.callBack",data,fObj.agenda.options);

			if(fObj.agenda.options.finishSearch == false && !fObj.agenda.options.searchPasteEvents){
				if(data.length > 0)
					fObj.agenda.options.nbSearch = 0;
				else
					fObj.agenda.options.nbSearch++;
				mylog.log("searchObj.agenda.callBack if ", fObj.agenda.options.limitSearch, fObj.agenda.options.nbSearch, data.length);
				if( data.length == 0 && fObj.agenda.options.limitSearch <= fObj.agenda.options.nbSearch){
					mylog.log("searchObj.agenda.callBack if searchNextEvent");
					fObj.agenda.searchNextEvent(fObj);
				} else {
					if( data.length == 0 &&
				        typeof fObj.agenda.options.noResult != "undefined" &&
				        fObj.agenda.options.noResult === true ){
						mylog.log("searchObj.agenda.callBack noResult");
				        fObj.agenda.options.dayCount++;
	                    fObj.search.start(fObj);
				    } else if(
				        typeof fObj.agenda.options.countsLaunchSesearch != "undefined" &&
				        typeof fObj.agenda.options.nbLaunchSesearch != "undefined" &&
				        fObj.agenda.options.nbLaunchSesearch > 0 &&
				        fObj.agenda.options.nbLaunchSesearch > fObj.agenda.options.countsLaunchSesearch){
				    	mylog.log("searchObj.agenda.callBack nbLaunchSesearch");
				        fObj.agenda.options.countsLaunchSesearch++;
				        fObj.agenda.options.dayCount++;
	                    fObj.search.start(fObj);
				    }
				}
			}else if(fObj.agenda.options.searchPasteEvents && data.length == 0 &&
				        typeof fObj.agenda.options.noResult != "undefined" &&
				        fObj.agenda.options.noResult === true ){
				fObj.agenda.end(fObj);
			}
    	},
    	searchNextEvent: function(fObj){
    		// SearchNextEvent est une fonction qui est activé si on ne trouve plus dans les 7 deniers jours looper de résultats
    		// Cette fonction permet de retrouver le prochain événement correspondant à la recherche
    		// Si il y a un evenement (non récuurent) qui arrive alors il relance le processus de recherche d'event a partir de cette date
    		// Sinon il passe à la fin des résultats
			mylog.log("searchObj.agenda.searchNextEvent");
		    var params = fObj.search.constructObjectAndUrl(fObj, true);
		    params=fObj.agenda.searchParams(fObj, params);

			mylog.log("searchObj.agenda.searchNextEvent params",params);

			$.ajax({
				type: "POST",
				url: baseUrl+"/co2/search/searchnextevent",
				data: params,
				dataType: "json",
				error: function (data){
					mylog.log("searchObj.agenda.searchNextEvent error", data);
				},
				success: function(data){
					mylog.log("searchObj.agenda.searchNextEvent success", data);
					if(typeof data.nextEvent != "undefined"){
						//$("#dropdown_search .processingLoader").remove();
						$.each(data.nextEvent, function(keyE, valE){

							// mylog.log("agenda.searchNextEvent success each", valE.startDate );
							// mylog.log("agenda.searchNextEvent success each", valE.startDate.sec );
							var datestart = valE.startDate.sec*1000 ;
							var datestartM =  moment(datestart).format() ;
							// mylog.log("agenda.searchNextEvent success each", datestart, datestartM );

							// mylog.log("agenda.searchNextEvent success each", params.startDateUTC, datestartM );
							var duration = moment.duration(moment(datestartM).diff(moment(params.startDateUTC)));
							var days = Math.floor(duration.asDays());
							// mylog.log("agenda.searchNextEvent success duration days", duration, days );

							fObj.agenda.options.dayCount += days ;
							fObj.agenda.options.nbSearch = 0;

							fObj.search.start(fObj);
						});
					} else {
						// Si plus d'événement alors on affiche la fin des résultats
						var str=fObj.agenda.end(fObj, data);
						if(fObj.results.smartGrid){
							$str=$(str);
							fObj.results.$grid.append($str).masonry( 'appended', $str, true ).masonry( 'layout' ); ;
						}else
							$(fObj.results.dom).append(str);
						//$(fObj.results.dom).append(fObj.results.end(fObj));
						fObj.agenda.options.finishSearch = true;
						$(fObj.results.dom+" .seePasteEvent").off().on("click", function(){
							coInterface.showLoader(fObj.results.dom, trad.currentlyresearching);
							coInterface.simpleScroll(0, 200);
							fObj.search.obj.count=true;
							fObj.search.obj.countType=["events"];
							fObj.search.obj.indexMin=0;
							if(typeof fObj[fObj.search.loadEvent.active].stopPropagation != "undefined")
								fObj[fObj.search.loadEvent.active].stopPropagation=false;
							if(fObj.results.smartGrid){
								//if(fObj.results.smartGrid){
								if(fObj.results.$grid === null){
									fObj.results.$grid=$(fObj.results.dom).masonry({
						      			itemSelector: '.smartgrid-slide-element',
						      			columnWidth: '.searchEntityContainer.smartgrid-slide-element'
						    		});
						    	}else{
									fObj.results.$grid.masonry('destroy');
									fObj.results.$grid.masonry({itemSelector: '.smartgrid-slide-element', 'columnWidth': '.searchEntityContainer.smartgrid-slide-element'});
							   	}
							}
							fObj.search.loadEvent.bind=true;
							fObj.search.countResult=true;
							fObj.agenda.options.searchPasteEvents=true;
							fObj.agenda.options.nbSearch=0;
							fObj.agenda.options.finishSearch=false;
	        				fObj.search.start(fObj);
	        				$(fObj.results.dom).append("<a href='javascript:;' class='btn bg-orange text-white backToTodayEvent'>"+
	        					'<span class="icon-stack">'+
								   '<i class="fa fa-calendar icon-stack-3x"></i>'+
								   '<i class="fa fa-refresh icon-stack-1x right_align"></i>'+
								'</span> '+trad.backToToday+'</a>');
	        				$(fObj.results.dom+" .backToTodayEvent").off().on("click", function(){
	        					fObj.search.init(fObj);
	        					$(this).fadeOut(300).remove();
	        				});
						});
					}

				}
			});
		},
		end : function(fObj, data){
			var str = '<div class="col-xs-12 smartgrid-slide-element">' +
			'<div class="dayEvent col-xs-12 endResults">' +
				'<h2 class="text-white col-xs-12 date text-center">' +
					'<span class="col-xs-12 date-label small uppercase">' +
						trad.noMoreEvents+
					'</span>'+
					'<a href="javascript:dyFObj.openForm(\'event\')" class="bg-orange text-white btn small margin-top-20 margin-bottom-20">'+trad.addEvent+'</a><br/>';
					if(notNull(data) && typeof data.countPasteEvents != "undefined" && data.countPasteEvents > 0){
						// On regarde si une recherche est en cours sinon on affiche po la possibilité de voir les events passés !!
						var seePaste=false;
						$.each(["type", "tags", "text", "filters"], function(e, v){
							if(typeof fObj.search.obj[v] != "undefined" && notEmpty(fObj.search.obj[v]) ){
								seePaste=true;
							}
						});
						if(notEmpty(getSearchLocalityObject())){
							seePaste=true;
						}
						if(seePaste){
							str+=
							'<span class="col-xs-12 date-label small uppercase msgMorePasteEvent">'+
								data.msg+
							'</span>'+
							'<a href="javascript:;" class="seePasteEvent bg-orange text-white btn small margin-top-20 margin-bottom-10">'+
								trad.clickToSeeThem+
							'</a>';
						}
					}
				str+='</h2>' +
				'</div>'+
			'</div>';
			return str;
		}
	},
	pagination : {
		init : function(fObj){
			var scrollTopPosFilter=(typeof pageProfil != "undefined" && typeof pageProfil.affixPageMenu != "undefined") ? pageProfil.affixPageMenu+5 : 0;
			if (typeof fObj.disableScrollTop == "undefined" || (typeof fObj.disableScrollTop != "undefined" && typeof fObj.disableScrollTop == false)) {
				coInterface.simpleScroll(scrollTopPosFilter, 400);
			}
			if(typeof fObj.search.obj.forced != "undefined"
				&& typeof fObj.search.obj.forced.page != "undefined" && fObj.search.obj.forced.page){
				fObj.search.obj.indexMin=fObj.search.obj.indexStep*fObj.search.obj.nbPage;
				delete fObj.search.obj.forced.page;
			}else
				fObj.search.obj.nbPage=0;

			if(fObj.search.obj.indexStep > 0){
				if($(fObj.footerDom+" .pageTable").length <=0)
					$(fObj.footerDom).append('<div class="pageTable col-xs-12 text-center"></div>');
			}
			if (typeof coInterface.showCostumLoader != "undefined") {
				coInterface.showCostumLoader(fObj.results.dom, trad.currentlyresearching)
			} else {
				coInterface.showLoader(fObj.results.dom, trad.currentlyresearching);
			}
		},
		start : function(fObj){
			if(fObj.results.map.active == false){
				if (typeof coInterface.showCostumLoader != "undefined") {
					coInterface.showCostumLoader(fObj.results.dom, trad.currentlyresearching)
				} else {
					coInterface.showLoader(fObj.results.dom, trad.currentlyresearching);
				}
			}
		},
		event : function(fObj){
			if(fObj.search.obj.indexStep > 0){
				var numberPage=Math.ceil(fObj.results.numberOfResults/fObj.search.obj.indexStep);
			    $('.pageTable').pagination({
			        items: numberPage,
			        itemOnPage: fObj.search.obj.indexStep,
			        currentPage: (fObj.search.obj.nbPage+1),
			        hrefTextPrefix:"?page=",
			        cssStyle: 'light-theme',
			        prevText: '<i class="fa fa-chevron-left"></i> ' + trad["previous"],
			        nextText: trad["next"] + ' <i class="fa fa-chevron-right"></i>',
			        onInit: function () {
			              // fire first page loading
			        },
			        onPageClick: function (page, evt) {
			        	/*if(fObj.results.smartGrid){
							fObj.results.$grid.masonry('destroy');
			        	}*/
			            var scrollTopPosFilter=(typeof pageProfil != "undefined" && typeof pageProfil.affixPageMenu != "undefined") ? pageProfil.affixPageMenu+5 : 0;
			        	if (typeof fObj.disableScrollTopPageClick == "undefined" || (typeof fObj.disableScrollTopPageClick != "undefined" && typeof fObj.disableScrollTopPageClick == false)) {
							coInterface.simpleScroll(scrollTopPosFilter, 10);
						}
			            fObj.search.obj.nbPage=(page-1);
			            fObj.search.obj.indexMin=fObj.search.obj.indexStep*fObj.search.obj.nbPage;
			            fObj.search.start(fObj);
			        }
			    });
			}
		}
	},
	scroll : {
		options : {
			domTarget : "#search-content",
			scrollTarget : window
		},
		init : function(fObj){
			var scrollTopPosFilter=(typeof pageProfil != "undefined" && typeof pageProfil.affixPageMenu != "undefined" && pageProfil.affixPageMenu > 0) ? pageProfil.affixPageMenu+5 : 0;
			coInterface.simpleScroll(scrollTopPosFilter, 400);
			$(fObj.footerDom+" .pageTable").remove();
			if (typeof coInterface.showCostumLoader != "undefined") {
				coInterface.showCostumLoader(fObj.results.dom, trad.currentlyresearching)
			} else {
				coInterface.showLoader(fObj.results.dom, trad.currentlyresearching);
			}
		},
		start : function(fObj){
			//coInterface.showLoader(fObj.results.dom+' #btnShowMoreResult',trad.currentlyresearching);
		},
		event : function(fObj){
			//heightToStartSearch=$(window).height()-$().offset().top);
			if(fObj.pInit && fObj.pInit.defaults && fObj.pInit.defaults.scrollDom)
				fObj.scroll.options.scrollTarget=fObj.pInit.defaults.scrollDom;
			$(fObj.scroll.options.scrollTarget).bind("scroll",function(){
	            if(!fObj.search.currentlyLoading && !fObj.scroll.stopPropagation){
					var domTarget = fObj.scroll.options.domTarget;
					var endOfPixelScroll = "";
					if (domTarget && typeof domTarget === 'string' && $(domTarget).is(":visible")) {
						endOfPixelScroll = Math.round($(domTarget).height() - ($(window).height() - $(domTarget).offset().top));
					} 
					// else {
					// 	console.error("Erreur : domTarget non défini ou ne possède pas de propriété 'offset'");
					// }
	                // var endOfPixelScroll = $(fObj.scroll.options.domTarget).height()-($(window).height()-$(fObj.scroll.options.domTarget).offset().top);
	                if( $(this).scrollTop() >= endOfPixelScroll){
	                	if(!fObj.search.multiCols.isMulti(fObj))
	                		fObj.search.obj.indexMin=fObj.search.obj.indexMin+fObj.search.obj.indexStep;

	                    fObj.search.start(fObj);
	                }
	            }
	        });
			/*var selector = fObj.pInit && fObj.pInit.defaults && fObj.pInit.defaults.scrollDom ? fObj.pInit.defaults.scrollDom : window 
			$(selector).bind("scroll",function(){
	            if(!fObj.search.currentlyLoading && !fObj.scroll.stopPropagation){
	                var heightWindow = $("#search-content").height();
	                if(fObj.pInit && fObj.pInit.defaults && fObj.pInit.defaults.scrollDom) {
						if( $(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight - 170){
							if(!fObj.search.multiCols.isMulti(fObj))
								fObj.search.obj.indexMin=fObj.search.obj.indexMin+fObj.search.obj.indexStep;
	
							fObj.search.start(fObj);
						}
					} else {
						if( $(this).scrollTop() >= (heightWindow-$(selector).height())){
							if(!fObj.search.multiCols.isMulti(fObj))
								fObj.search.obj.indexMin=fObj.search.obj.indexMin+fObj.search.obj.indexStep;

							fObj.search.start(fObj);
						}
					}
	            }
	        });*/

	        if ( notNull(costum)
				&& typeof costum.editMode != "undefined" 
				&& costum.editMode 
				&& fObj.results.scrollableDom != "undefined"){		        	        
	        		$(fObj.results.scrollableDom).bind("scroll",function(){
	        			if(!fObj.search.currentlyLoading && !fObj.scroll.stopPropagation){
	        				var heightWindow = $("#search-content").height();
	        				if( $(this).scrollTop() >= (heightWindow-$(window).height())){
	        					if(!fObj.search.multiCols.isMulti(fObj))
	        						fObj.search.obj.indexMin=fObj.search.obj.indexMin+fObj.search.obj.indexStep;
	        					
	        					fObj.search.start(fObj);
	        				}
	        			}
	        		});
			}
		},
		callBack : function(fObj){
			if($(fObj.results.dom+" #btnShowMoreResult").length > 0) $(fObj.results.dom+" #btnShowMoreResult").remove();
			if(!fObj.scroll.stopPropagation){
				if(fObj.results.smartGrid){
					$moreRes=$('<div id="btnShowMoreResult" class="col-xs-12 smartgrid-slide-element start-masonry"></div>');
					fObj.results.$grid.append($moreRes).masonry( 'appended', $moreRes, true ).masonry( 'layout' ); ;
				}
				else
					$(fObj.results.dom).append('<div id="btnShowMoreResult" class="col-xs-12"></div>');
				if (typeof coInterface.showCostumLoader != "undefined") {
					coInterface.showCostumLoader(fObj.results.dom+' #btnShowMoreResult', trad.currentlyresearching)
				} else {
					coInterface.showLoader(fObj.results.dom+' #btnShowMoreResult',trad.currentlyresearching);
				}
			}

		},
		stopPropagation : false
	},
	// # SearchObj.header définit le zone d'en-tête au dessus des résultats
	// <<Elle contient en général le nombre de résultat de la recherche ou diverses boutons comme celui de  vue cartographie>>
	// SearchObj.header contient comme les autres vues de cet objet
		// ##-DOM (string)  => container html [class/id] dans le document qui contient l'en-tête
		// ##-INIT() (function)  qui initialise les vues html dans le header et action les events liés
		// ##-VIEWS (object)  qui contient une multitude de vues (compteur des résultats, types recherché, bouttons de vue différentes...)
		//			Cet objet peut recevoir autant de brique html nécessaire à l'application de recherche ou au costum
		// ##-EVENTS (object) relient les événements jquery liés au différentes vues
	header : {
		dom : ".headerSearchContainer",
		initViews : function(fObj){
			mylog.log("searchObj.header.init", fObj.header.options);
			headerStr = '<div class="col-xs-12">';
			$.each(fObj.header.options, function(k,v){
				headerStr+='<div class="headerSearch'+k+' '+v.classes+'">';
				if(typeof v.group != "undefined"){
					$.each(v.group, function(e, obj){
						headerStr+=fObj.header.views[e](fObj, obj);
					});
				}
				headerStr+="</div>";
			});
			headerStr+= '</div>';
			mylog.log("searchObj.header.init fObj.header.dom", fObj.header.dom);
			mylog.log("searchObj.header.init $(fObj.header.dom).length", $(fObj.header.dom).length);
			mylog.log("searchObj.header.init headerStr", headerStr);
			$(fObj.header.dom).append(headerStr);
		},
		initEvents : function(fObj){
			$.each(fObj.header.options, function(k,v){
				if(typeof v.group != "undefined"){
					$.each(v.group, function(key,event){
						if(typeof fObj.header.events[key] != "undefined")
							fObj.header.events[key](fObj);
					});
				}
			});
		},
		options : {
			left : {
				classes : 'col-xs-8 elipsis no-padding',
				group:{
					count : true,
					types : true
				}
			},
			right : {
				classes : 'col-xs-4 text-right no-padding',
				group : {
					map : true
				}
			}
		},
		set : function(fObj){
				//var countRes=fObj.results.numberOfResults(fObj);
				var resultsStr = (fObj.results.numberOfResults > 1) ? trad.results : trad.result;
				if($(fObj.header.dom+" .countResults").length > 0)
					$(fObj.header.dom+" .countResults").html('<i class=\'count-results-icon fa fa-angle-down\'></i> <span class="count-nbr">' + fObj.results.numberOfResults + '</span><span class="count-lbl"> '+resultsStr+'</span>');
				if($(fObj.filters.dom+" .count-result-xs").length > 0)
					$(fObj.filters.dom+" .count-result-xs").html('<span><i class=\'fa fa-angle-down\'></i> ' + fObj.results.numberOfResults + ' '+resultsStr+'</span>');
				if($(fObj.header.dom+" .typeResults").length > 0) $(fObj.header.dom+" .typeResults").html(directory.searchTypeHtml());
		},
		lists : {
			csv : {},
			pdf : {}
		},
		views : {
			count : function(fObj, v){
				return '<h4 class="pull-left countResults"></h4>';
			},
			types : function(fObj,v){
				return '<h4 class="pull-left elipsis padding-left-5"><small class="typeResults"></small></h4>';
			},
			switcher : function(fObj, v){
				str='<button type="button" class="btn switchDirectoryView ';
					if(directory.viewMode=='list') str+='active ';
				str+=     'margin-right-5" data-value="list">'+
						'<i class="fa fa-bars"></i>'+
					'</button>'+
					'<button type="button" class="btn switchDirectoryView ';
					if(directory.viewMode=='block') str+='active ';
				str +=   '" data-value="block">'+
						'<i class="fa fa-th-large"></i>'+
					'</button>';
				return str;
			},
			map : function(fObj,v){
				return  '<button type="button" class="btn-show-map hidden-xs" style="" title="'+trad.showmap+'" alt="'+trad.showmap+'">'+
							'<i class="fa fa-map-marker"></i> '+trad.map+
						'</button>';
			},
			kanban : function(fObj,v){
				let btnK = ''
				if(typeof canEdit != 'undefined' && canEdit == true && typeof contextData!="undefined" && contextData!=null && contextData.type != 'citoyens') {
					btnK = `<button type="button" class="btn-show-kanban btn btn-default hidden-xs">
								<i class="fa fa-trello"></i>  Rôles
							</button>&nbsp;`  
				}
				if(costum && location.hash.indexOf("#admin")!=-1){
					btnK = `<button type="button" class="btn-show-kanban btn btn-default hidden-xs margin-right-10 ${v.class}">
								<i class="fa fa-trello"></i>  ${(v.label||"Rôles")}
							</button>`;
				}
				if(contextData && contextData.type == 'organizations' && userConnected != null && typeof userConnected != 'undefined' && typeof userConnected.links != 'undefined' && typeof userConnected.links.memberOf != 'undefined') {
					$.map(userConnected.links.memberOf, function(val, key){
						if(key == contextData.id && typeof val.isAdmin != 'undefined') {
							btnK = `<button type="button" class="btn-show-kanban btn btn-default hidden-xs">
										<i class="fa fa-trello"></i>  Rôles
									</button>&nbsp;`  
						} 
					})
				}
				if(contextData && contextData.type == 'projects' && userConnected != null && typeof userConnected != 'undefined' && typeof userConnected.links != 'undefined' && typeof userConnected.links.projects != 'undefined') {
					$.map(userConnected.links.projects, function(val, key){
						if(key == contextData.id && typeof val.isAdmin != 'undefined') {
							btnK = `<button type="button" class="btn-show-kanban btn btn-default hidden-xs">
										<i class="fa fa-trello"></i>  Rôles
									</button>&nbsp;`  
						} 
					})
				}
				return btnK;
			},
			badges : function(fObj,v){
				let btnB = ''
				if(typeof canEdit != 'undefined' && canEdit == true && contextData.type != 'citoyens') {
					btnB = `<button type="button" class="btn-show-badges btn btn-dark hidden-xs" style = "border : 1px solid gray !important;">
								<i class="fa fa-bookmark"></i> Badges
							</button>&nbsp;`  
				}
				if(contextData.type == 'organizations' && userConnected != null && typeof userConnected != 'undefined' && typeof userConnected.links != 'undefined' && typeof userConnected.links.memberOf != 'undefined') {
					$.map(userConnected.links.memberOf, function(val, key){
						if(key == contextData.id && typeof val.isAdmin != 'undefined') {
							btnB = `<button type="button" class="btn-show-badges btn btn-dark hidden-xs" style = "border : 1px solid gray !important;">
										<i class="fa fa-bookmark"></i> Badges
									</button>&nbsp;`  
						} 
					})
				}
				if(contextData.type == 'projects' && userConnected != null && typeof userConnected != 'undefined' && typeof userConnected.links != 'undefined' && typeof userConnected.links.projects != 'undefined') {
					$.map(userConnected.links.projects, function(val, key){
						if(key == contextData.id && typeof val.isAdmin != 'undefined') {
							btnB = `<button type="button" class="btn-show-badges btn btn-dark hidden-xs" style = "border : 1px solid gray !important;">
										<i class="fa fa-bookmark"></i> Badges
									</button>&nbsp;`  
						} 
					})
				}
				return btnB;
			},
			conode : function(fObj, v){
				return  '<button type="button" class="ssmla btn-show-node btn btn-primary hidden-xs" data-view="conode" style="margin-right: 5px" title="Graph de Communauté" alt="Graph de Communauté">'+
							'<i class="fa fa-map-marker"></i> Noeud'+
						'</button>';
			},
			cohexagone : function(fObj, v){
				return  `<button type="button" class="ssmla btn-show-hexagone btn btn-primary hidden-xs" data-view="cohexagone"  style="margin-right: 5px; display: inline-flex;align-items: center;" title="Graph de Communauté" alt="Graph de Communauté">
							<svg xmlns="http://www.w3.org/2000/svg"  style="width: 20px;fill:white" viewBox="0 0 256 256"><path d="M232,80.18v95.64a16,16,0,0,1-8.32,14l-88,48.17a15.88,15.88,0,0,1-15.36,0l-88-48.17a16,16,0,0,1-8.32-14V80.18a16,16,0,0,1,8.32-14l88-48.17a15.88,15.88,0,0,1,15.36,0l88,48.17A16,16,0,0,1,232,80.18Z"></path></svg> Hexagone
						</button>`;
			},
			graph: function(fObj, v){
				var options = "";
				$.each(v, function(index, opt){
					options+=" data-"+opt+"='"+opt+"' "+" data-"+index+"='"+opt+"' ";
				})
				const svgGraphIcon = `
				<svg style="height: 12px; width: 15px;fill:white" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="svg-graph-icon" x="0px" y="0px" viewBox="0 0 349.899 349.898" style="enable-background:new 0 0 349.899 349.898;" xml:space="preserve">
				<path d="M175.522,12.235c-42.6,0-77.256,34.649-77.256,77.25c0,42.6,34.656,77.255,77.256,77.255    c42.591,0,77.257-34.656,77.257-77.255C252.779,46.895,218.113,12.235,175.522,12.235z"/>
				<path d="M77.255,337.663c42.599,0,77.255-34.641,77.255-77.251c0-42.594-34.656-77.25-77.255-77.25    C34.653,183.162,0,217.818,0,260.412C0,303.012,34.653,337.663,77.255,337.663z"/>
				<path d="M272.648,183.151c-42.603,0-77.256,34.65-77.256,77.256c0,42.604,34.653,77.25,77.256,77.25    c42.6,0,77.251-34.646,77.251-77.25C349.909,217.818,315.248,183.151,272.648,183.151z"/>
				</svg>`;
				
				return  '<button id="btn-load-graph" type="button" class="btn-show-graph btn btn-primary hidden-xs" '+options+' style="" title="afficher sur un graph" alt="afficher sur un graph">'+
							''+svgGraphIcon+' '+(v.label||"Graph")+
						'</button>';
			},
			calendar : function(fObj,v){
				return  '<button type="button" class="btn btn-show-calendar bg-orange" >'+
							'<i class="fa fa-calendar"></i> '+trad.calendar+
						'</button>';
			},
			fediverse: function (fObj, v) {
				return (
				  '<span class="form-check" style="margin-left:5px;margin-right:5px">' +
				  '<label class="btn btn-default btn-md"  style="background-color: #ddd;font-weight:bold" for="check-filter-fediverse"><img id="image-fediverse"  style="width:20px;height:20px;margin-left: 5px;margin-right: 5px;" src="https://cdn.onlinewebfonts.com/svg/img_223015.png"  width="20" height="20"><input class="btn-check" style="margin-right:5px" type="checkbox" id="check-filter-fediverse">' +
				  trad.fediverse +
				  "</label>" +
				  "</span>"
				);
			  },
			alert: function(fObj,v){
				return '<button type="button" class="btn btn-default letter-blue addToAlert margin-right-5 tooltips" data-toggle="tooltip" data-placement="bottom" title="'+trad.bealertofnewitems+'" data-value="list" onclick="directory.addToAlert();">'+
						'<i class="fa fa-bell"></i> <span class="hidden-xs">'+trad.alert+'</span>'+
					'</button>';
			},
			add : function(fObj, v){
				btn= '<div class="header-create-btn pull-right">';
				//var btn='';
				if(fObj.search.obj.types.length > 1){
					btn+='<a href="javascript:;" class="btn margin-left-5 btn-add pull-right text-green-k" '+
		            'data-target="#dash-create-modal" '+
		            'data-toggle="modal">'+
		              '<i class="fa fa-plus-circle"></i> '+trad.createpage+
		          '</a>';
				}else if(fObj.search.obj.types[0]=='persons'){
					btn+='<a href="#element.invite" class="btn text-yellow lbhp btn-add pull-right">'+
		                    '<i class="fa fa-plus-circle"></i> '+trad.invitesomeone+
		              '</a>';
				}else{
					const testHexColor = (color) => {
						const hexColorRegex = /^#([0-9A-Fa-f]{6}|[0-9A-Fa-f]{8})$/;
							if (hexColorRegex.test(color)) {
								return true
							}
							return false
					};
					var elt=typeObj.get(fObj.search.obj.types[0]);
					mylog.log("elt", elt);
					var labelAdd = (typeof v.label!="undefined") ? v.label : elt.createLabel;
					var subData='';
					if(typeof elt.formSubType != 'undefined')
						subData='data-form-subtype="'+elt.formSubType+'"';
					var btnHtml = '<button type="button" class="btn btn-open-form btn-add pull-right text-white bg-'+elt.color+'" ';
					if (typeof elt.color!="undefined" && elt.color.includes("#"))
						btnHtml = '<button type="button" class="btn btn-open-form btn-add pull-right text-white " style="background:'+elt.color+'" '
					var colorClass = (testHexColor(elt.color)) ? '' : 'bg-' + elt.color;
					var styleColor = (testHexColor(elt.color)) ? 'style="background-color:'+elt.color+';"' : '';
					btn+='<button type="button" class="btn btn-open-form btn-add pull-right text-white '+ colorClass +'" '+styleColor+' '+
		            'data-form-type="'+elt.formType+'" '+
		            subData+'>'+
		              '<i class="fa fa-plus-circle"></i> '+labelAdd+
		          '</button>';
				}
				btn+=	'</div>';
				return btn;
			},
			invite : function(fObj,v){
				mylog.log("searchObj.header.views.invite ",v);
				var labelInvite = (typeof v.label!="undefined") ? v.label : "Ajouter des membres";
				var str = "";
				if(typeof canEdit != 'undefined' && canEdit == true && typeof v.contextType != "undefined" && typeof v.contextId != "undefined"){
					str += `<a href="#element/invite/type/${v.contextType}/id/${v.contextId}" class="btn btn-success btn-invite pull-right lbhp ${v.class||""}"><i class="fa fa-user-plus"></i>${labelInvite} </a>`;
				}
				return  str;
			},
			csv : function(fObj,v){
				mylog.log("searchObj.header.views.csv ",v);
				var labelBtnCsv = (typeof v[0].label != "undefined")?v[0].label : "CSV";
				fObj.header.lists.csv = v;
				var str = "";
				if(v.length === 1 ){
					str += '<a class="pull-right btnCsv btn btn-default text-green-k margin-left-5" href="javascript:;" data-id="0" data-url="'+v[0].url+'"><i class="fa fa-table"></i> '+labelBtnCsv+'</a>';;
				} else {
					str +=  '<button type="button" class="btn btn-default dropdown-toggle text-green-k margin-left-5" type="button" id="dropdownCSV" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'+
								'<i class="fa fa-table"></i> CSV'+
							'</button>'+
							'<ul class="dropdown-menu dropdown-menu-left" aria-labelledby="dropdownCSV">';
								$.each(v, function(kV, vV){
									str += '<li class="text-left"><a class="pull-right btnCsv" href="javascript:;" data-id="'+kV+'" data-url="'+vV.url+'">'+vV.name+'</a></li>';
								});
					str +=  '</ul>';
				}

				return  str;
			},
			pdf : function(fObj,v){
				mylog.log("searchObj.header.views.pdf ",v);
				fObj.header.lists.pdf = v;
				var str = "";
				if(v.length === 1 ){
					str += '<a class="pull-right btnPdf btnPdfAnimation btn btn-default text-red margin-left-5" href="javascript:;" data-id="0" data-url="'+v[0].url+'"><i class="fa fa-file-pdf-o"></i><span class="btnpdftext">PDF</span> <span class="pdfloading-animate"></span></a>';
					str += '<div id="uniquemaguelle" class="hidden"></div>';
				} else {
					str +=  '<div><button type="button" class="btn btn-default dropdown-toggle text-red margin-left-5" type="button" id="dropdownPDF" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'+
								'<i class="fa fa-file-pdf-o"></i> PDF'+
							'</button>'+
							'<ul class="dropdown-menu dropdown-menu-left" aria-labelledby="dropdownPDF">';
								$.each(v, function(kV, vV){
									str += '<li class="text-left"><a class="pull-right btnPdf" href="javascript:;" data-id="'+kV+'" data-url="'+vV.url+'">'+vV.name+'</a></li>';
								});
					str +=  '</ul></div>';

				}
				return  str;
			},
			custom : function(fObj,v){
				var str="";
				if(typeof v.html=="string"){
					str=v.html;
				}
				return str;
			},
			community : function(fObj,v){
				mylog.log("searchObj.header.views.comunity ",v);
				var str = "";
				if(exists(aapObj) && notEmpty(costum.type) && costum.type === "aap"){
					str += '<a href="#communityAap" class="btn btn-success btn-xs pull-right margin-right-10 lbh"><i class="fa fa-users"></i> '+trad["Manage the community"]+'</a>';
				}
				else if(notEmpty(v.contextSlug)){
					str += '<a href="#@'+v.contextSlug+'.view.directory.dir.members" class="btn btn-success btn-xs pull-right margin-right-10 lbh"><i class="fa fa-users"></i> '+trad["Manage the community"]+'</a>';
				}
				return  str;
			},
			switchView : function(fObj,v){
				var str = "";
				str += `
					<a href = "javascript:;" class = "changeViews active" data-views = "elementPanelHtmlSmallCard" data-mode ="grid"> <i class="fa fa-table"></i></a>
					<a href = "javascript:;" class = "changeViews" data-views = "elementPanelHtmlMiWidth" data-mode ="list"> <i class="fa fa-list"></i></a>				
					<a href = "javascript:;" class = "changeViews" data-views = "table" data-mode ="table"> <i class="fa fa-bars"></i></a>				
						
				`;
				return str;
			}
		},
		events : {
			switchView : function(fObj){
				$(".changeViews").off().on("click", function(e){
					$(".changeViews").removeClass("active");
					$(this).addClass("active");
					var view = $(this).data("views")
					var mode = $(this).data("mode")
					if ( notNull(costum) && typeof costum.slug != "undefined")
						localStorage.setItem("filtersmodeView"+costum.slug,mode);
					else
						localStorage.setItem("filtersmodeView",mode);
					fObj.search.obj.mode = mode;
					if(view != "table"){
						fObj.results.renderView = "directory."+view;
						if(fObj.search.loadEvent.default == "table"){
							fObj.search.loadEvent.default = "scroll";
							fObj.results.$grid = null;
							$(".bodySearchContainer").html(`
								<div class="no-padding col-xs-12" id="dropdown_search"> </div>
								<div class="no-padding col-xs-12 text-left footerSearchContainer smartgrid-slide-element"></div> 
							`);
						}
						fObj.results.multiCols = true; 
						fObj.search.obj.nbPage = 0;
						fObj.search.init(fObj);
					}else{
						fObj.search.loadEvent.default = "table";
						fObj.results.multiCols = false;
						fObj.results.smartGrid = false;
						fObj.search.init(fObj);
					}
				});
			},
			map : function(fObj){
				$(".btn-show-map").off().on("click", function(e){
					fObj.helpers.toggleMapVisibility(fObj);
				});
			},
			graph: function(fObj){
				$(".btn-show-graph").off().on("click", function(e){
					fObj.helpers.toggleGraphMode(fObj, $(this).data())
				})
			},
			conode: function(fObj){
				if(typeof pageProfil != "undefined" && typeof pageProfil.bindViewActionEvent != "undefined" && typeof pageProfil.bindViewActionEvent == "function"){
					pageProfil.bindViewActionEvent();
				}
				// $(".btn-show-node").off().on("click", function(e){
				// 	var view = $(this).data('view');
				// 	if(typeof pageProfil != "undefined" && typeof pageProfil.views != "undefined" && typeof pageProfil.views[view] != "undefined"){
				// 		pageProfil.views[view]($(this));
				// 	}
				// })
			},
			cohexagone: function(fObj){
				if(typeof pageProfil != "undefined" && typeof pageProfil.bindViewActionEvent != "undefined" && typeof pageProfil.bindViewActionEvent == "function"){
					pageProfil.bindViewActionEvent();
				}
				// $(".btn-show-hexagone").off().on("click", function(e){
				// 	fObj.helpers.toggleHexagoneMode(fObj, $(this).data())
				// })
			},
			kanban: function(fObj){
				$(".btn-show-kanban").off().on("click", function(e){
					fObj.helpers.toggleKanbanMode(fObj, $(this).data())
				})
			},
			badges: function(fObj){
				$(".btn-show-badges").off().on("click", function(e){
					fObj.helpers.toggleBadgesMode(fObj, $(this).data())
				})
			},
			calendar : function(fObj){
				$(fObj.header.dom+" .btn-show-calendar").off().on("click", function(){
					$("#modal-calendar").modal("show");
					var paramsAjax={};
					paramsAjax["searchP"]=fObj.search.constructObjectAndUrl(fObj);
					if(typeof fObj.agenda.options.calendarConfig != "undefined")
						$.extend(paramsAjax["searchP"], fObj.agenda.options.calendarConfig);

					ajaxPost('#modal-calendar .content-modal-calendar', baseUrl+'/'+moduleId+'/app/calendar',
						paramsAjax,
						function(){
							var clickBsDoc=false;
							var openDashEvent=true;
				            var $win = $(document); // or $box parent container
							var $box = $("#modal-calendar");
							var $lbhBtn=$(".lbh, .lbhp, .close-modal, .btn-dash-link");
				            var $preview = $('#modal-preview-coop');
				            $("#modal-calendar").show(200);
				            if(!clickBsDoc){
				                clickBsDoc=true;
				                $win.on("click.Bst", function(event){
				    				if($lbhBtn.has(event.target).length != 0 //checks if descendants of $box was clicked
				    	    				||
				    	    				$lbhBtn.is(event.target)){
				                               $("#modal-calendar").hide(200);
				    				}else{
				                        if($preview.has(event.target).length != 0 || $preview.is(event.target)){
				                            $("#modal-calendar").show();
				                        }else if(!openDashEvent){
				                                if ($box.has(event.target).length == 0 //checks if descendants of $box was clicked
				                                    &&
				                                    !$box.is(event.target) //checks if the $box itself was clicked
				                                  ){
				                                $("#modal-calendar").hide(200);
				                            }
				                        }
				                    }
				                    openDashEvent=false;
				    				//alert("you clicked inside the box");
				    			});
				            }
										});
					});
			},
			csv: function(fObj){
				mylog.log("searchObj.header.events.csv ");
				$(fObj.header.dom+" .btnCsv").off().on("click", function(){
					mylog.log("searchObj.header.events.csv.btnCsv ", $(this).data("id"), $(this).data("url"));
					var csv = fObj.header.lists.csv[$(this).data("id")];
					var paramsSearch = fObj.search.constructObjectAndUrl(fObj);
					paramsSearch = jQuery.extend(paramsSearch, fObj.paramsSearch);
					mylog.log("searchObj.header.events.csv.btnCsv csv", csv);
					if(typeof csv.defaults != "undefined")
						paramsSearch = jQuery.extend(paramsSearch, csv.defaults);

					if(typeof csv.fields != "undefined")
						paramsSearch.fields = csv.fields;

					ajaxPost(
				        null,
				        $(this).data("url"),
				        paramsSearch,
				        function(data){
				        	mylog.log("csv data", data);
				        	const { Parser } = json2csv;
							const fields = data.fields;
							const json2csvParser = new Parser({ fields , delimiter: ';' });
							const csv = json2csvParser.parse(data.results);
				   			var exportedFilenmae = 'export.csv';
							var blob = new Blob([csv], { type: 'text/csv;charset=utf-8;' });
							if (navigator.msSaveBlob) { // IE 10+
								navigator.msSaveBlob(blob, exportedFilenmae);
							} else {
								var link = document.createElement("a");
								if (link.download !== undefined) { // feature detection
									// Browsers that support HTML5 download attribute
									var url = URL.createObjectURL(blob);
									link.setAttribute("href", url);
									link.setAttribute("download", exportedFilenmae);
									link.style.visibility = 'hidden';
									document.body.appendChild(link);
									link.click();
									document.body.removeChild(link);
								}
							}

				  		},
				  		function(data){
				  			$("#searchResults").html("erreur");
				  		}
				  	);
				});
			},
			pdf: function(fObj){
				mylog.log("searchObj.header.events.pdf ");
				$(fObj.header.dom+" .btnPdf").off().on("click", function(){

					var thisbtnpdf = $(this);
					thisbtnpdf.addClass("pdfloading");

					mylog.log("searchObj.header.events.pdf.btnPdf ", $(this).data("id"), $(this).data("url"));
					var pdf = fObj.header.lists.pdf[$(this).data("id")];
					var paramsSearch = fObj.search.constructObjectAndUrl(fObj);
					paramsSearch = jQuery.extend(paramsSearch, fObj.paramsSearch);
					mylog.log("searchObj.header.events.pdf.btnPdf pdf", pdf);
					if(typeof pdf.defaults != "undefined")
						paramsSearch = jQuery.extend(paramsSearch, pdf.defaults);

					if(typeof pdf.fields != "undefined")
						paramsSearch.fields = pdf.fields;

					var paramsSearchpdftext = "";
					var paramspdfSup = $(this).data("url");
					var paramsSearchpdfstatus = "";

					if (typeof paramsSearch.name != "undefined"){
						paramsSearchpdftext = paramsSearch.name;
					}

					if (paramsSearchpdftext != ""){
						paramspdfSup += '/text/'+paramsSearchpdftext;
					}

					if (typeof paramsSearch.filters != "undefined" && typeof paramsSearch.filters.priorisation != "undefined"){
						paramsSearchpdfstatus = paramsSearch.filters.priorisation;
					}

					if (paramsSearchpdfstatus != ""){
						paramspdfSup += '/status/'+paramsSearchpdfstatus;
					}

					mylog.log("oooiooo", paramsSearch);
/*
						ajaxPost(
							null,
							paramspdfSup,
							paramsSearch,
							function (data) {
								$(".btnPdf").removeClass("pdfloading");
								mylog.log("pdf data", typeof data);
								try {
									/!*var blob = new Blob([data]);
									var link = document.createElement('a');
									link.href = window.URL.createObjectURL(blob);
									link.download = "action.pdf";
									link.click();*!/
									/!*const {jsPDF} = window.jspdf;
									var div = document.createElement('div');
									div.innerHTML = data.trim();
									// Change this to div.childNodes to support multiple top-level nodes
									//div.firstChild;
									console.log("html2canvas div.firstChild", div);
									var doc4 = new jsPDF();
									doc4.html(div, {
										callback: function (doc) {
											doc4.save("a4.pdf");
										}
									});*!/

									var exportedFilenmae = 'export.pdf';
									var blob = new Blob([data], { type: 'text/csv;charset=utf-8;' });
									if (navigator.msSaveBlob) { // IE 10+
										navigator.msSaveBlob(blob, exportedFilenmae);
									} else {
										var link = document.createElement("a");
										if (link.download !== undefined) { // feature detection
											// Browsers that support HTML5 download attribute
											var url = URL.createObjectURL(blob);
											link.setAttribute("href", url);
											link.setAttribute("download", exportedFilenmae);
											link.style.visibility = 'hidden';
											document.body.appendChild(link);
											link.click();
											document.body.removeChild(link);
										}
									}
								} catch (e) {
									console.log("html2canvas catch");
									console.log(e);
								}
							},
							function (data) {
								$(".btnPdf").removeClass("pdfloading");
								$("#searchResults").html("erreur");
							}
						);*/

					window.open(paramspdfSup);
					$(".btnPdf").removeClass("pdfloading");

				});
			},
			alert : function(fObj){
				$(fObj.header.dom+" .addToAlert").off().on("click", function(){
					var message = "<span class='text-dark'>"+tradDynForm.saveresearchandalert+"</span><br>";
					var searchName = (fObj.search.obj.text != "" ) ? fObj.search.obj.text : "";
					var boxBookmark = bootbox.dialog({
				            title: message,
				            message: '<div class="row">  ' +
			                      '<div class="col-md-12"> ' +
			                        '<form class="form-horizontal"> ' +
			                          '<label class="col-md-12 no-padding" for="awesomeness">'+tradDynForm.searchtosave+' : </label><br> ' +
			                          '<span>'+location.hash+'</span><br><br>'+
			                          '<label class="col-md-12 no-padding" for="awesomeness">'+tradDynForm.nameofsearch+' : </label><br> ' +
			                          '<input type="text" id="nameFavorite" class="nameSearch wysiwygInput form-control" value="'+searchName+'" style="width: 100%" placeholder="'+tradDynForm.addname+'..."></input>'+
			                          '<br>'+

			                        '<label class="col-md-6 col-sm-6 no-padding" for="awesomeness">'+tradDynForm.bealertbymailofnewclassified+' ?</label> ' +
			                        '<div class="col-md-4 col-sm-4"> <div class="radio no-padding"> <label for="awesomeness-0"> ' +
			                          '<input type="radio" name="awesomeness" id="awesomeness-0" value="true"  checked="checked"> ' +
			                          tradDynForm.yes+' </label> ' +
			                          '</div><div class="radio no-padding"> <label for="awesomeness-1"> ' +
			                          '<input type="radio" name="awesomeness" id="awesomeness-1" value="false"> '+tradDynForm.no+' </label> ' +
			                          '</div> ' +
			                    '</div> </div>' +
			                    '</form></div></div>',
				            buttons: {
				                success: {
			                    label: trad.save,
			                    className: "btn-primary",
			                    callback: function () {
			                        var formData={
			                          "parentId":userId,
			                          "parentType":"citoyens",
			                          "url": location.hash,
			                          "searchUrl":fObj.search.getUrlSearchParams(),
			                          "alert":$("input[name='awesomeness']:checked").val(),
			                          "name":$("#nameFavorite").val(),
			                          "type":"research"
			                        }
			                        if(typeof costum != "undefined" && costum != null){
			                          formData.mailParams={
			                            logo : costum.logo,
			                            title : costum.title,
			                            url : costum.url
			                          };
			                        }
			                        mylog.log(formData);
			                        ajaxPost(
								        null,
								        baseUrl+"/"+moduleId+"/bookmark/save",
								        formData,
								        function(data){
										    if(data.result){
			                                  toastr.success(data.msg);
				                            }
				                            else{
				                              if(typeof(data.type)!="undefined" && data.type=="info")
				                                toastr.info(data.msg);
				                              else
				                                toastr.error(data.msg);
				                            }
								  		}
								    );
			                    }
			                },
			                cancel: {
								label: trad.cancel,
								className: "btn-secondary",
								callback: function() {
								}
			                }
			            }
					});
				});
			},
			fediverse: function (fObj) {
				$(fObj.header.dom + " #check-filter-fediverse")
				.off()
				.on("change", function () {
					const image = document.getElementById("image-fediverse");
					if ($(this).prop("checked")) {
					image.src = "https://www.fediverse.to/static/images/icon.png";
					} else {
					image.src = "https://cdn.onlinewebfonts.com/svg/img_223015.png";
					}
					fObj.search.obj.fediverse = $(this).prop("checked");
					fObj.search.init(fObj);
				});
			},
			add : function(fObj){
				mylog.log("searchObj.header.events.add ");
				coInterface.bindButtonOpenForm();
			},
			custom : function(fObj){
				mylog.log("searchObj.header.events.custom ");
			}
		}
	},
	results : {
		dom : "#dropdown_search",
		data : {},
		count : {},
		graph: {
			active: false
		},
		map : {
			active : false,
			sameAsDirectory:false,
			changedfilters : true,
			showMapWithDirectory:false
		},
		afterRenderingEvent: null,
		community : null,
		numberOfResults : 0,
		smartGrid: false,
		$grid: null,
		multiCols : true,
		renderView : "directory.simplePanelHtml",
		unique: false,
		callback : null,
		smartCallBack: function(fObj,timeOut, last){
			/*mylog.log($(".smartgrid-slide-element img.isLoading").length, "length img not loaded", $(".smartgrid-slide-element.last-item-loading").length);
			if($(".smartgrid-slide-element img.isLoading").length <= 0){
				fObj.results.$grid.masonry({itemSelector: ".smartgrid-slide-element"});
			}else{
    			setTimeout(function(){
    				fObj.results.smartCallBack(fObj, (timeout+100));
    			}, (timeout+100));
    		}*/
		},
		init : function(fObj){

		},
		render : function(fObj, results, data){
			//$(fObj.results.dom).html(JSON.stringify(data));
     		mylog.log("searchObj.results.render", fObj.search.loadEvent.active, fObj.agenda.options, results, data);
     		if(fObj.results.map.active){
				if(notEmpty(results)){
     			    fObj.results.addToMap(fObj, results, data);
				}else{
					mylog.log("fObj no results for map",fObj);
					mapObj.hideLoader();
					toastr.error(trad['noresultmatch']);					
				}	
     		}else{
				str = "";
				if( fObj.search.loadEvent.active == "agenda" &&
					(  Object.keys(results).length > 0 ) ){
					str = fObj.agenda.getDateHtml(fObj);
				}
				if(typeof results != 'undefined' && Object.keys(results).length > 0){
					//parcours la liste des résultats de la recherche
					if (typeof data == "object" && typeof fObj.redirectPage != "undefined" && fObj.redirectPage) {
						data.redirectPage = fObj.redirectPage
					}
					str += directory.showResultsDirectoryHtml(results, fObj.results.renderView, fObj.results.smartGrid, fObj.results.community, fObj.results.unique,data); 
					if(Object.keys(results).length < fObj.search.obj.indexStep && fObj.search.loadEvent.active != "agenda")
						str += fObj.results.end(fObj);
				}else if( fObj.search.loadEvent.active != "agenda" )
					str += fObj.results.end(fObj);
				if(fObj.results.smartGrid){
					$str=$(str);
					fObj.results.$grid.append($str).masonry( 'appended', $str, true ).masonry( 'layout' ); ;
					fObj.results.$grid.masonry({itemSelector: ".smartgrid-slide-element.start-masonry", 	columnWidth: '.searchEntityContainer.smartgrid-slide-element'});
			
				}else
					$(fObj.results.dom).append(str);

				fObj.results.events(fObj);
				if(fObj.results.map.sameAsDirectory)
					fObj.results.addToMap(fObj, results,data);

				const arr = document.querySelectorAll('img.lzy_img')
				arr.forEach((v) => {
					if (fObj.results.smartGrid) {
						v.dom = fObj.results.dom;
						v.callBackOnload=function(target){
							if(typeof fObj.results.imgObsCallback == "function")
								fObj.results.imgObsCallback(target)
							// Callback to launch masonry only element who is after to avoid huge calculation 
							fObj.results.$grid.masonry({itemSelector: ".smartgrid-slide-element.start-masonry", 	columnWidth: '.searchEntityContainer.smartgrid-slide-element'});
							if($(".msr-"+target.dataset.id).length > 0 && $(".msr-"+target.dataset.id).hasClass("start-masonry"))
								$(".msr-"+target.dataset.id).removeClass("start-masonry");
						}
					}
					imageObserver.observe(v);
				});
				if(notNull(fObj.results.callBack) && typeof fObj.results.callBack== "function")
					fObj.results.callBack(fObj);
			}
		},
		end : function(fObj){
			mylog.log("searchObj.results.end");
			var str='';
			//Event scroll and all searching
			$(fObj.results.dom+' #btnShowMoreResult').remove();
			//msg specific for end search
			var match= (fObj.search.obj.text != '') ? 'match' : '';
			/*var totalRes=0;
			$.each(fObj.results.count, function(k, v){
				totalRes+=v;
			});*/

			var msg= (fObj.results.numberOfResults==0) ? trad['noresult'+match] : "";
			//var msg= (fObj.results.numberOfResults==0) ? trad['noresult'+match] : trad['nomoreresult'+match];
			var contributeMsg='<span class=\'italic\'><small>'+trad.contributecommunecterslogan+'</small><br/></span>';
			if(userId !=''){
				contributeMsg+='<a href="javascript:;" data-target="#dash-create-modal" data-toggle="modal" data-toggle="tooltip" data-placement="top" class="text-green-k tooltips" > '+
                	'<i class="fa fa-plus-circle"></i> '+trad.sharesomething+
            	'</a>';
			} else {
				contributeMsg+='<a href="javascript:;" class="letter-green margin-left-10" data-toggle="modal" data-target="#modalLogin">'+
                      '<i class="fa fa-sign-in"></i> '+trad.connectyou+
              '</a>';
			}
			var addClassEndRes=(fObj.results.numberOfResults==0) ? "searchEntityContainer" : "";
			if(msg != "")
				str = '<div class="pull-left col-xs-12 text-left divEndOfresults smartgrid-slide-element start-masonry '+addClassEndRes+'">'+
						'<h5 class="endOfresults text-dark padding-20" style="margin-bottom:10px; margin-left:15px;border-left: 2px solid lightgray;" >'+
							msg+'<br/>'+
							//contributeMsg+
						'</h5><br/>'+
					'</div>';
			return str;
		},
		events : function(fObj){
			mylog.log("searchObj.results.events");
			$(fObj.results.dom+" .processingLoader").remove();
			//active les link lbh
			coInterface.bindLBHLinks();
			//initialise les boutons pour garder une entité dans Mon répertoire (boutons links)
			directory.bindBtnElement();
			coInterface.bindButtonOpenForm();
			if(fObj.results.afterRenderingEvent){
				if(typeof fObj.results.afterRenderingEvent == "function"){
					fObj.results.afterRenderingEvent();
				}
			}
		},
		addToMap : function(fObj, results, data){
			mylog.log("searchObj.results.addToMap", results);
			if(typeof data != "undefined" && typeof data.zones != "undefined"){
				Object.assign(results, data.zones);
			}
    		fObj.mapObj.addElts(results);
			fObj.mapObj.map.invalidateSize();
			fObj.mapObj.fitBounds();
    	},
		countResults : function(fObj){
			mylog.log("searchObj.results.numberOfResults");
			fObj.results.numberOfResults=0;
			$.each(fObj.search.obj.types, function(e, type){
				if(typeof fObj.results.count[type] != "undefined"){
					fObj.results.numberOfResults+=fObj.results.count[type];
				}
			});

			if(fObj.results.count["citoyens"]==0){
				$("#btn-load-graph").remove();
			}
		},
		changeNumberOfResults : function(fObj){

		},
		showLimitMessage : function(fObj){
			bootbox.alert({
			    size: "small",
			    title: "Le nombre de résultats est trop important",
			    message: "Veuillez filtrer et réduire le chargement de la carte à un maximun de 6OOO résultats",
			    callback: function(){ /* your callback code */ }
			});
		}
	},
	search : {
		currentlyLoading : false,
		countResult : false,
		loadEvent:{
			active : "",
			multi : "scroll",
			default : "pagination",
			bind : false,
			options : {
			}
		},
		obj : {
			text:"",
		    nbPage:0,
		    indexMin:0,
		    indexStep:30,
		    count:true,
		    scroll : true,
		    scrollOne : false,
		    tags:[],
		    initType : "",
		    filters:{},
		    types:[],
		    countType:[],
		    locality:{},
		    forced : {},
			fediverse: false
		},
		init : function(fObj, count){
			if(typeof fObj.search.obj.nbPage != "undefined" 
				&& typeof fObj.search.obj.forced != "undefined"
				&& typeof fObj.search.obj.forced.page != "undefined" 
				&& fObj.search.obj.forced.page ){
				fObj.search.obj.indexMin=fObj.search.obj.indexStep*fObj.search.obj.nbPage;
				delete fObj.search.obj.forced.page;
			}else{
				fObj.search.obj.indexMin = 0;
			}
			fObj.agenda.options.dayCount=0;
			if(fObj.results.map.active===true || fObj.results.map.showMapWithDirectory){
				fObj.mapObj.clearMap();
				fObj.mapObj.showLoader();
			}
			if(fObj.results.map.changedfilters){
				if(count!==false){
					fObj.search.obj.count=true;
					if(fObj.filters.actions.types.initList.length==0) fObj.filters.actions.types.initList=fObj.search.obj.types;
					fObj.search.obj.countType=jQuery.extend(true, [], fObj.filters.actions.types.initList);
					fObj.filters.actions.types.add(fObj, null, false);
				}
				if(fObj.search.multiCols.isMulti(fObj)){
					fObj.search.multiCols.init(fObj);
					fObj.search.loadEvent.active=fObj.search.loadEvent.multi;
				}else{
					fObj.search.loadEvent.active=fObj.search.loadEvent.default;
					if(typeof fObj.search.obj.ranges != "undefined") delete fObj.search.obj.ranges;
				}
				if(typeof fObj[fObj.search.loadEvent.active] != "undefined" && typeof fObj[fObj.search.loadEvent.active].stopPropagation != "undefined")
					fObj[fObj.search.loadEvent.active].stopPropagation=false;
				if(fObj.results.smartGrid){
					//if(fObj.results.smartGrid){
					if(fObj.results.$grid === null){
						fObj.results.$grid=$(fObj.results.dom).masonry({
			      			itemSelector: '.smartgrid-slide-element',
			      			columnWidth: '.searchEntityContainer.smartgrid-slide-element'
			    		});
			    	}else{
						fObj.results.$grid.masonry('destroy');
						fObj.results.$grid.masonry({itemSelector: '.smartgrid-slide-element', 'columnWidth': '.searchEntityContainer.smartgrid-slide-element'});
				   	}
				}

				fObj.search.loadEvent.bind=true;
				fObj.search.countResult=true;
				if(typeof fObj[fObj.search.loadEvent.active] != "undefined" && typeof fObj[fObj.search.loadEvent.active].init != "undefined") fObj[fObj.search.loadEvent.active].init(fObj);
			}
	        fObj.search.start(fObj);
    	},
    	callBack : function(fObj, results){
    		mylog.log("searchObj.search.callBack", results);
    		fObj.search.currentlyLoading = false;
    		fObj.search.obj.count=false;
    		fObj.search.obj.countType=[];
    		fObj.filters.actions.text.spin(fObj, false);
    		if(typeof results != 'undefined' && Object.keys(results).length < fObj.search.obj.indexStep){
    			fObj[fObj.search.loadEvent.active].stopPropagation=true;
    			$(fObj.results.dom+" .processingLoader").remove();
    		}
    		if(fObj.results.smartGrid)
    			fObj.results.smartCallBack(fObj, 0);
    		if(typeof fObj[fObj.search.loadEvent.active].callBack != "undefined") fObj[fObj.search.loadEvent.active].callBack(fObj,results);

    	},
    	lazyQueueForSearching : function(fObj){
    		setTimeout(function(){
    			fObj.search.start(fObj);
    		}, 200);
    	},
    	start : function(fObj){
    		if(!scopeLoadingForSearch){
				if(!fObj.search.currentlyLoading){
					mylog.log("searchObj.search.start");
					fObj.search.currentlyLoading = true;
					if(typeof fObj[fObj.search.loadEvent.active] != "undefined" && typeof fObj[fObj.search.loadEvent.active].start != "undefined") fObj[fObj.search.loadEvent.active].start(fObj);
					fObj.search.autocomplete(fObj, fObj.search.callBack);
				}
	    	}else{
	    		fObj.search.lazyQueueForSearching(fObj)
	    	}
    	},
    	autocomplete : function(fObj, callBack){
    		mylog.log("searchObj.search.autocomplete");
			var searchParams=fObj.search.constructObjectAndUrl(fObj);
			if(fObj.results.map.active && !fObj.results.map.sameAsDirectory){
				searchParams["indexMin"]=0;
				searchParams["indexStep"]=0;
				searchParams["mapUsed"]=true;
				if(typeof searchParams.ranges != "undefined"){ delete searchParams.ranges;}
			}
			if(typeof fObj.mapObj.showLegende != "undefined")
				searchParams["activeContour"]=true;
			if(typeof fObj[fObj.search.loadEvent.active].searchParams != "undefined")
				searchParams = fObj[fObj.search.loadEvent.active].searchParams(fObj, searchParams);
			ajaxPost(
				null,
				fObj.urlData,
				searchParams,
				function(data){
					//mylog.log("searchObj.search.autocomplete >>> success autocomplete search !!!! ", data); //mylog.dir(data);
					if(!data){
						if(notNull(data) && notNull(data.content))
							toastr.error(data.content);
					} else {
						if(data.results && fObj.results)
							fObj.results.datares = data.results;
						if(typeof fObj[fObj.search.loadEvent.active].successComplete != "undefined"){
							fObj[fObj.search.loadEvent.active].successComplete(fObj, data)
						}else{
							if(fObj.search.countResult && typeof data.count != "undefined"){
								fObj.results.count=data.count;
								fObj.filters.actions.types.setBadges(fObj);
								fObj.results.countResults(fObj);
								fObj.header.set(fObj);
							}
							//Prepare results object for render
							var results=data.results;
							if(typeof fObj.search.obj.ranges != "undefined"
								&& !fObj.results.map.active)
			                	results=fObj.search.multiCols.getResults(fObj, results);
			                //Render results
							if(typeof fObj[fObj.search.loadEvent.active].beforeRender == "function")
								fObj[fObj.search.loadEvent.active].beforeRender(fObj, results, data)
					   
			                if(typeof fObj[fObj.search.loadEvent.active].render == "function")
								fObj[fObj.search.loadEvent.active].render(fObj, results, data)
							else
								fObj.results.render(fObj, results, data);

    						//bind search interface event on init (callBack because number needs for pagination)
							if(fObj.search.loadEvent.bind){
							 	fObj[fObj.search.loadEvent.active].event(fObj);
								fObj.search.loadEvent.bind=false;
							}

    						//signal que le chargement est terminé
							if(typeof callBack == "function"){
								callBack(fObj, results);
							}

							if($(".badge-theme-count").length!=0 && fObj.filters.actions.themes.isLoaded==false){
								fObj.filters.actions.themes.setThemesCounts(fObj);
							}
						}
						if(typeof fObj.lastSearchDone != "undefined" && typeof fObj.filters != "undefined" && typeof fObj.filters.dom != "undefined") {
							if(notNull(fObj.filters.dom) && fObj.filters.dom != "") {
								const filterDomKey = fObj.filters.dom.replace(/\#|\./g, '')
								fObj.lastSearchDone[filterDomKey] = new Date().getTime();
							}
						}
					}
				},
				function (data){
					mylog.log("searchObj.search.autocomplete >>> error autocomplete search");
					mylog.dir(data);
					$(fObj.results.dom).append(data.responseText);
					//signal que le chargement est terminé
					fObj.search.currentlyLoading = false;
				}/*,
				null,
				{async:false}*/
			);
    	},
    	constructObjectAndUrl: function(fObj, notImpactUrl){
	        mylog.log("searchObj.search.constructObjectAndUrl", fObj.search.obj, notImpactUrl);
	        var searchConstruct={};


	        if(fObj.search.obj.text != "")
	            searchConstruct.name=fObj.search.obj.text;

			if(typeof fObj.search.obj.textPath != "undefined")
				searchConstruct.textPath=fObj.search.obj.textPath;
			if(typeof fObj.search.obj.searchBy != "undefined")
				searchConstruct.searchBy=fObj.search.obj.searchBy;
			if(typeof fObj.search.obj.fieldShow != "undefined")
				searchConstruct.fieldShow=fObj.search.obj.fieldShow;

			if(typeof fObj.search.obj.doublon != "undefined")
				searchConstruct.doublon=fObj.search.obj.doublon;

	        if(typeof fObj.search.obj.types != "undefined")
	            searchConstruct.searchType=fObj.search.obj.types;
	        if(typeof fObj.search.obj.tags != "undefined" && fObj.search.obj.tags.length > 0)
	            searchConstruct.searchTags=fObj.search.obj.tags;
			if(typeof fObj.search.obj.tagsPath != "undefined")
	            searchConstruct.tagsPath=fObj.search.obj.tagsPath;
	        if(typeof fObj.search.obj.type != "undefined")
	            searchConstruct.type = fObj.search.obj.type;
	        if(typeof fObj.search.obj.section != "undefined")
	            searchConstruct.section = fObj.search.obj.section;
	        if(typeof fObj.search.obj.category != "undefined")
	            searchConstruct.category = fObj.search.obj.category;
	        if(typeof fObj.search.obj.subType != "undefined")
	            searchConstruct.subType = fObj.search.obj.subType;
	        if(typeof fObj.search.obj.priceMin != "undefined")
	            searchConstruct.priceMin = fObj.search.obj.priceMin;
	        if(typeof fObj.search.obj.priceMax != "undefined")
	            searchConstruct.priceMax = fObj.search.obj.priceMax;
	        if(typeof fObj.search.obj.devise != "undefined")
	            searchConstruct.devise = fObj.search.obj.devise;
	        if(typeof fObj.search.obj.startDate != "undefined" && !fObj.agenda.options.searchPasteEvents){
	            //if(fObj.search.obj.text=="")
	              searchConstruct.startDate = fObj.search.obj.startDate;
	             // $(".calendar").show(700);
	            //}else
	             // $(".calendar").hide(700);
	        }
	        if(typeof fObj.search.obj.endDate != "undefined"){
	            if(fObj.search.obj.text=="")
	              searchConstruct.endDate = fObj.search.obj.endDate;
	        }
	        if(typeof fObj.search.obj.source != "undefined")
	            searchConstruct.source = fObj.search.obj.source;

			if(typeof fObj.search.obj.params != "undefined")
	            searchConstruct.params=fObj.search.obj.params;
	        if(typeof fObj.search.obj.indexMin != "undefined" && notNull(fObj.search.obj.indexMin))
	            searchConstruct.indexMin=fObj.search.obj.indexMin;
	        if(typeof fObj.search.obj.initType != "undefined")
	            searchConstruct.initType=fObj.search.obj.initType;
	        if(typeof fObj.search.obj.count != "undefined" && fObj.search.obj.count)
	            searchConstruct.count=fObj.search.obj.count;
	        if(typeof fObj.search.obj.ranges != "undefined" && /*(typeof fObj.search.obj.indexStep == "undefined" ||*/ fObj.search.obj.indexStep > 0/*)*/)
	            searchConstruct.ranges=fObj.search.obj.ranges;
	        if(typeof fObj.search.obj.countType != "undefined")
	            searchConstruct.countType=fObj.search.obj.countType;
	        if(typeof fObj.search.obj.geoSearch != "undefined")
	            searchConstruct.geoSearch=fObj.search.obj.geoSearch;
	        if(typeof fObj.search.obj.sourceKey != "undefined")
	            searchConstruct.sourceKey=fObj.search.obj.sourceKey;


	        if(typeof fObj.search.obj.indexStep != "undefined")
	            searchConstruct.indexStep=fObj.search.obj.indexStep;
	        if(typeof fObj.search.obj.links != "undefined")
	            searchConstruct.links=fObj.search.obj.links;
	        if(typeof fObj.search.obj.notSourceKey != "undefined")
	            searchConstruct.notSourceKey=fObj.search.obj.notSourceKey;
	        if(typeof fObj.search.obj.openingHours != "undefined")
	            searchConstruct.openingHours=fObj.search.obj.openingHours;
	        if(typeof fObj.search.obj.filters != "undefined")
	            searchConstruct.filters=fObj.search.obj.filters;
	        if(typeof fObj.search.obj.options != "undefined")
	            searchConstruct.options=fObj.search.obj.options;
	        if(typeof fObj.search.obj.sortBy != "undefined")
	            searchConstruct.sortBy=fObj.search.obj.sortBy;
	        if(typeof fObj.search.obj.sort != "undefined")
	            searchConstruct.sort=fObj.search.obj.sort;
	        if(typeof fObj.search.obj.fields != "undefined")
	            searchConstruct.fields=fObj.search.obj.fields;
			if(typeof fObj.search.obj.mapping != "undefined")
				searchConstruct.mapping=fObj.search.obj.mapping;
			if(typeof fObj.search.obj.activeContour != "undefined")
				searchConstruct.activeContour=fObj.search.obj.activeContour;
	        //COMMUNITY
	        if(fObj.search.obj.community != false)
	            searchConstruct.community = fObj.search.obj.community;
	        // Locality
	        searchConstruct.locality = getSearchLocalityObject();
	        //Construct url with all necessar params
	        if(exists(fObj.search.obj.forced))
	    		searchConstruct=fObj.search.injectForcedParams(searchConstruct, fObj.search.obj.forced);
	        if(!notNull(notImpactUrl))
	            fObj.search.manageHistory(fObj);
			// fediverse
			if (fObj.search.obj.fediverse != "undefined")
			searchConstruct.fediverse = fObj.search.obj.fediverse;
		   	//
	        return searchConstruct;
	    },
	  	injectForcedParams : function(searchP, forced){
	  		$.each(forced, function(e, v){
	  			if(exists(searchP[e]))
	  				$.extend(searchP[e], v);
	  			else
	  				searchP[e]=v;
	  		});
	  		return searchP;

	  	},
	    forcedParams : function(fObj, label){
    	    res = false;
	        checkAnd=[];
	        checkOr=[];
	        if(label.indexOf(',') > -1)
	            checkAnd= label.split(",");
	        else if(label.indexOf('|') > 0)
	            checkOr= label.split("|");
	        else
	            checkOr=[label];
	        if(typeof checkAnd != "undefined" && notEmpty(checkAnd)){
	            $.each(checkAnd, function(e, v){
	                if(typeof fObj.search.obj.forced != "undefined" && typeof fObj.search.obj.forced[v] != "undefined")
	                    res = true;
	                else{
	                    res = false;return false;
	                }
	            });
	        }else{
	            $.each(checkOr, function(e, v){
	                if(typeof fObj.search.obj.forced != "undefined" && typeof fObj.search.obj.forced[v] != "undefined"){
	                    res=true;return false;
	                }
	            });
	        }
	        return res;
	    },
	    getUrlSearchParams : function(fObj, extraParams=["nbPage","mode"], checkForced=true){
			arrayToSearch=["text","types", "tags", "type","section", "category", "subType", "valueArray","priceMin", "priceMax", "devise","source"];
	        if(notNull(extraParams)){ arrayToSearch=arrayToSearch.concat(extraParams);}
	        getStatus="";
	        $.each(arrayToSearch, function(e,label){
	            if(typeof fObj.search.obj[label] != "undefined"){
					if (typeof fObj.pInit.filters != "undefined" && typeof fObj.pInit.filters[label] != "undefined"  && typeof fObj.pInit.filters[label].toUrlHistory != "undefined") {
						if (typeof fObj.pInit.filters[label].type != "undefined") {
							const thisFilterType = fObj.pInit.filters[label].type;
							const thisFilterField = fObj.pInit.filters[label].field ? fObj.pInit.filters[label].field : label;
							if (fObj.search.obj[thisFilterType] && !fObj.search.obj[thisFilterType][thisFilterField]) {
								fObj.search.obj[label] = "";
							}
						}
					}
	                if(!checkForced || !fObj.search.forcedParams(fObj, label)){
	                    value=encodeURIComponent((Array.isArray(fObj.search.obj[label])) ? fObj.search.obj[label].join(",") : fObj.search.obj[label]);
	                    if(label=="text" && notEmpty(value))//>0 || fObj.search.obj[label]!= "")
	                        getStatus+=((getStatus!="") ? "&":"")+label+"="+value;
	                    else if(label=="nbPage"){
	                        if(Number(value) != 0 && notEmpty(value))
	                            getStatus+=((getStatus!="") ? "&":"")+label+"="+(Number(value)+1);
	                    }
	                    else if(label=="types" && typeof fObj.search.obj[label] != "undefined"){
	                    	//fObj.search.objranges
	                    	if(fObj.filters.actions.types.initList.length != fObj.search.obj.types.length && typeof fObj.search.obj.ranges == "undefined")
	                            getStatus+=((getStatus!="") ? "&":"")+label+"="+value;
	                        else if(typeof fObj.search.obj.ranges != "undefined"){
	                        	var typesInRanges=[];
	                        	$.each(fObj.search.obj.ranges, function(e, v){
	                        		typesInRanges.push(e);
	                        	});
	                        	if(fObj.filters.actions.types.initList.length != typesInRanges.length)
	                        		getStatus+=((getStatus!="") ? "&":"")+label+"="+typesInRanges.join(",");
	                        }
	                    }
	                    else if($.inArray(label, ["startDate", "endDate"]) >= 0){
	                        if(!notEmpty(fObj.search.obj.text))
	                            getStatus+=((getStatus!="") ? "&":"")+label+"="+value;
	                    }else if(notEmpty(value))
	                        getStatus+=((getStatus!="") ? "&":"")+label+"="+value;
						else if(label == "mode"){
							if(notEmpty(value))
								getStatus+=((getStatus!="") ? "&":"")+label+"="+value;
						}
	                }
	            }
	        });
	        return getUrlSearchLocality(getStatus, true);
	    },
    	manageHistory : function(fObj, setExtraP=true, setSearchP=true){
    		onchangeClick=false;
			hashT=location.hash.split("?");
			setUrl=hashT[0].substring(0);
			extraParamsArray=["preview"];
			var moreParams=["nbPage","mode"];
			extraUrl="";
			if(setExtraP && notEmpty(hashT[1])){
				checkTableGet=hashT[1].split("&");
				$.each(checkTableGet, function(e, v){
					nameLabel=v.split("=")[0];
					if($.inArray(nameLabel,extraParamsArray)>=0)
						extraUrl+=(notEmpty(extraUrl)) ? "&"+v : v;
				});
				setUrl+=(notEmpty(extraUrl)) ? "?"+extraUrl : "";
			}

			// ajout de filters dans l'url manager de filterJS pour que ce dernier ne supprime pas le filters qu'on veut mettre dans l'url
			if (fObj.pInit && typeof fObj.pInit.filters != "undefined") {
				const exculdeDefault =["text","types", "tags", "type","section", "category", "subType", "valueArray","priceMin", "priceMax", "devise","source"];
				$.each(fObj.pInit.filters, function(key, val) {
					if ($.inArray(key, exculdeDefault) < 0 && typeof val.toUrlHistory != "undefined") {
						moreParams.push(key);
					}
				})
			}
			if(setSearchP){
	        	searchParamsUrl=fObj.search.getUrlSearchParams(fObj, moreParams);
	        	if(notEmpty(searchParamsUrl)){
	        		setUrl+= (notEmpty(extraUrl)) ? "&" : "?" ;
	        		setUrl+= searchParamsUrl;
	        	}
	        }
	        if(historyReplace)
	            history.replaceState({}, null, setUrl);
	        else if(location.hash != setUrl)
	            location.hash=setUrl;
	        historyReplace=false;
		},
    	multiCols : {
    		results : {},
    		//types : [],
    		isMulti : function(fObj){
    			res=false;
    			if(fObj.search.obj.types.length > 1 && fObj.results.multiCols===true){
    				$.each(fObj.search.obj.types, function(e, v){
    					if($.inArray(v,["NGO", "LocalBusiness", "Group", "GovernmentOrganization", "Cooperative"]) == -1)
    						res = true;
    				});
    			}
    			return res;
    		},
    		init : function(fObj){
    			if(typeof fObj.search.obj.nbPage!= "undefined") fObj.search.obj.nbPage=0;
    			fObj.search.obj.ranges={};
    			//fObj.search.multiCols.types=jQuery.extend(true, [], fObj.search.obj.types);
    			$.each(fObj.search.obj.types, function(e, v){
		            fObj.search.obj.ranges[v]={indexMin : 0, indexMax : fObj.search.obj.indexStep};
		        });
		        fObj.search.multiCols.results={};
    		},
    		getResults : function(fObj, data){
		        var resToShow={};
		        var injectData={};
		        var sorting=[];
		        $.each(fObj.filters.actions.types.initList, function(e, v){
		        	injectData[v]=0;
		        });
		        Object.assign(fObj.search.multiCols.results, data);
		        $.each(fObj.search.multiCols.results, function(e,v ){
		            if(typeof v.updated == "undefined" && typeof v.created == "undefined")
		            	delete fObj.search.multiCols.results[e];
		            else{
			            var sortingValues="",
							dateOfLastUpdate = v.updated ? v.updated:v.created;
			            if(typeof dateOfLastUpdate == "object")
							dateOfLastUpdate= dateOfLastUpdate.sec;
			            else if(notNull(dateOfLastUpdate))
			            	sortingValues=dateOfLastUpdate;
			            fObj.search.multiCols.results[e]["sorting"]=sortingValues;
			            sorting.push(sortingValues);
			        }
				});
				mylog.log(fObj.search.multiCols.results, "setSorting");
		        sorting.sort().reverse();
		        sorting=sorting.splice(0,30);
		        mylog.log(sorting,"sortingArray");
		        $.each(sorting, function(e, v){
		            $.each(fObj.search.multiCols.results, function(key, value){
		              	if(v==value.sorting){
			                resToShow[key]=value;
							typeKey=(value.collection=="organizations") ? value.type : value.collection;
							if(typeof injectData[typeKey] != "undefined")
			                	injectData[typeKey]++;
							else if(typeof injectData[value.collection] != "undefined")
								injectData[value.collection]++;
			                delete fObj.search.multiCols.results[key];
			           	}
		            });
		        });
		        mylog.log(injectData, "injectData");
		        $.each(injectData, function (type, v){
		            if(v==0)
		            	fObj.filters.actions.types.remove(fObj, type, false);
		            else if(typeof fObj.search.obj.ranges[type] != "undefined"){
		            	if($.inArray(type, fObj.search.obj.types)<0)
		            		fObj.search.obj.types.push(type);
		              	fObj.search.obj.ranges[type].indexMin=fObj.search.obj.ranges[type].indexMax;
		             	fObj.search.obj.ranges[type].indexMax=fObj.search.obj.ranges[type].indexMin+v;
		            }
		        });
		        mylog.log(resToShow, "resToShow");
		        return resToShow;
		    }
		}
	},
	mapContent: {
		hideViews: [],
		initViews: function(fObj){
			$.each(fObj.mapContent.views, function(k, getView){
				if(fObj.mapContent.hideViews.indexOf(k) < 0){
					var viewObj = getView(fObj)
					$(viewObj.container).append(viewObj.view)
				}
			})
		},
		initEvents: function(fObj){
			$.each(fObj.mapContent.events, function(k, v){
				v(fObj)
			})
		},
		views: {
			btnHideMap: function(fObj){
				var btn = "",
					elementNotExist = $(fObj.mapObj.parentContainer).find('.filter-btn-hide-map').length == 0;

				if(elementNotExist && !fObj.results.map.showMapWithDirectory)
					btn = "<button type='button' class='filter-btn-hide-map'><i class='fa fa-times' aria-hidden='true'></i></button>"
				return {
					container:fObj.mapObj.parentContainer,
					view: btn
				};
			}
		},
		events: {
			btnHideMap: function(fObj){
				$('.filter-btn-hide-map').off().click(function(){
					console.log("------------ btn hide map ----------------", fObj.search)
					fObj.helpers.toggleMapVisibility(fObj)
				})
			}
		}
	}
};
