"use strict";
var ocecotoolsObj = {
    init : function(otObj){
        //otObj.loadPlugins(otObj,function(){
            otObj.initSmartWizard(otObj,function(){
                otObj.events(otObj);
            })
        //})
    },
    /*loadPlugins : function(otObj,callBack){
        lazyLoad( baseUrl+'/plugins/jquery-smartwizard-v6/dist/js/jquery.smartWizard.min.js', 
        baseUrl+'/plugins/jquery-smartwizard-v6/dist/css/smart_wizard_all.min.css',
        callBack);
    },*/
    container : "#smartwizard-ocecoTools",
    contextData:{
        parent: {},
        createdFormId : null,
        inputs : null,
    },
    what : {
        id : "",
        label : "",
        field : "",
    },
    views : {
        smartWizard : function(otObj){
            var html = `
            <div class="container-fluid padding-top-35 co-scroll" style="position: fixed;top: 0;right: 0;left: 0;bottom: 0;z-index: 10000;background: #fff;height: 100%;overflow-y: auto;">
                <div class="container-fluid" id="smartwizard-ocecoTools" style="margin-top:70px">
                    <ul class="nav">
                        <li class="nav-item">
                        <a class="nav-link" href="#step-1">
                            <span class="num">1</span>
                            ${tradForm["Create a form"]+" "+otObj.what.label} 
                        </a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="#step-2">
                            <span class="num">2</span>
                            ${tradForm["Configuring the form"]+" "+otObj.what.label}
                        </a>
                        </li>
                    </ul>
                
                    <div class="tab-content">
                        <div id="step-2" class="tab-pane text-center padding-top-35 padding-bottom-35" role="tabpanel" aria-labelledby="step-2">
                            <button class="btn btn-lg btn-primary new-form-ocecotools" >${tradForm["New form"]}</button>
                            <div class="new-form-container-ocecotools co-scroll" style="/*position: relative;height: 500px;overflow-y: scroll;overflow-x:hidden*/"></div>
                        </div>
                        <div id="step-3" class="tab-pane text-center padding-top-35 padding-bottom-35" role="tabpanel" aria-labelledby="step-3">
                            <button class="btn btn-lg btn-primary config-form-ocecotools hidden" >${tradForm["Configuring the form"]}</button>
                            <div class="config-form-container-ocecotools"></div>
                        </div>
                    </div>
                
                    <div class="progress">
                        <div class="progress-bar" role="progressbar" style="width: 0%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                </div>
            </div>`;
            return html;
        }
    },
    events : function(otObj){
        if(typeof contextId != "undefined" && typeof contextType != "undefined" && typeof contextName != "undefined"){
            otObj.contextData["parent"][contextId] = {
                type : contextType,
                name : contextName
            };
            otObj.initCurrentContext(otObj);
        }

        $('.choose-context-ocecotools').off().on('click',function(){   
            dyFObj.openForm(otObj.dynforms.chooseContext(otObj),null,otObj.contextData);
        })
        $('.new-form-ocecotools').off().on('click',function(){
            dyFObj.openForm(otObj.dynforms.formConfig(otObj),null,otObj.contextData);
        })
    },
    dynforms: {
        chooseContext : function(otObj) {
            var filtersContextOcecotools = {'$or' : {}};
            filtersContextOcecotools['$or']["links.members."+userId] = { "$exists": "true" }
            filtersContextOcecotools['$or']["links.projects."+userId] = { "$exists": "true" }
            return {
                jsonSchema: {
                    title: "Quel contexte ?",
                    description: "",
                    icon: "fa-question",
                    properties : {
                        parent :{
                            inputType : "finder",
                            label : "Sélectionner un context",
                            initMe :false,
                            buttonLabel : "Rechercher un element",
                            placeholder :"Rechercher un element",
                            initContext : false,
                            initType : ["organizations","projects"],
                            initBySearch : true,
                            initContacts : false,
                            openSearch :true,
                            search : {
                                filters: { "links.members.5ef8a3fd690864c5338b4d52": { "$exists": "true" }}
                            },
                            multiple : false,
                            rules :{
                                required : true
                            }
                        }
                    },
                    save: function () {
                        otObj.contextData["parent"] = finder.object["parent"];
                        otObj.initCurrentContext(otObj);
                        $(otObj.container).smartWizard("next");
                        dyFObj.closeForm();
                    }
                }
            }
        },
        formConfig : function(otObj) {
            return {
                jsonSchema: {
                    title: tradForm["New form"] +" "+ otObj.what.label,
                    description: "",
                    icon: "fa-question",
                    properties : {
                        name : { 
                            inputType : "hidden",
                            rules: {
                                required:true
                            },
                            value : otObj.what.label
                        },
                        parent :{
                            inputType : "finder",
                            label : "Sélectionner un contexte",
                            initMe :false,
                            buttonLabel : "Rechercher un element",
                            placeholder :"Rechercher un element",
                            initContext : false,
                            initType : ["organizations","projects"],
                            initBySearch : true,
                            initContacts : false,
                            openSearch :true,
                            search : {
                                filters: { 
                                    '$or': [
                                        {["links.members."+userId]: { "$exists": "true" }},
                                        {["links.contributors."+userId]: { "$exists": "true" }}
                                    ]
                                }
                            },
                            multiple : false,
                            rules :{
                                required : true
                            }
                        },
                        startDate: {
                            inputType : "datetime",
                            label : tradDynForm.startDate,
                            value : new Date().getDate() + '/' + (new Date().getMonth()+1) + '/' + new Date().getFullYear(),
                            rules: {
                                required:true
                            }
                        },
                        endDate: { 	
                            inputType : "datetime",
                            label : tradDynForm.endDate ,
                            value : new Date().getDate() + '/' + (new Date().getMonth()+3) + '/' + new Date().getFullYear(),
                            rules: {
                                required:true
                            }
                        },
                    },
                    onLoads : {
                        onload: function(){

                        }
                    },
                    beforeBuild : function(){
                    },
                    save: function (formData) {
                        var today = new Date();
                        delete formData.collection; delete formData.scope;
                        
                        formData.subForms = [
                            "codate"+today.getDate()+today.getMonth()+ today.getFullYear()+"_"+Date.now(),
                        ]
                        formData.active = true,
                        formData.temporarymembercanreply = true,
                        mylog.log(formData,this);
                        var tplCtx = {
                            collection: "forms",
                            value: formData,
                            format: true,
                            path: "allToRoot",
                        };

                        if (typeof tplCtx.value == "undefined")
                            toastr.error('value cannot be empty!');
                        else {
                            dataHelper.path2Value(tplCtx, function (prms) {
                                otObj.contextData.createdFormId = prms.saved._id.$id;
                                otObj.contextData["parent"] =  prms.saved.parent;
                                tplCtx.value.id = tplCtx.value.subForms[0];
                                tplCtx.value.type = "openForm";
                                tplCtx.value.inputs = {};
                                tplCtx.value.inputs[generateIdFromString(tplCtx.value.name)+"_"+Date.now()] = {
                                    "label" : notEmpty(tplCtx.value.name) ? tplCtx.value.name : otObj.what.label,
                                    "type" : otObj.what.field,
                                }
                                delete tplCtx.value.description;
                                delete tplCtx.value.active;
                                delete tplCtx.value.startDate;
                                delete tplCtx.value.endDate;
                                delete tplCtx.value.subForms;
                                delete tplCtx.value.parent;
                                dataHelper.path2Value(tplCtx, function (prms2){
                                    if(prms2.result){
                                        //$(".form-actions").remove();
                                        otObj.contextData.inputs = prms2.saved;
                                        otObj.addParams(otObj);
                                        toastr.success(trad.saved);
                                        $(otObj.container).smartWizard("next");
                                        dyFObj.closeForm();
                                    }
                                })
                            });
                        }
                    }
                }
            }
        }
    },
    initCurrentContext : function(otObj){
        var tempKey = Object.keys(otObj.contextData["parent"])[0];
        var tempVal = otObj.contextData["parent"][tempKey];
        $('.choosed-context-ocecotools').html(`
            <h4>  <a href="#page.type.${tempVal.type}.id.${tempKey}" class="lbh-preview-element letter-green">${tempVal.name}</a> </h4>
        `)
        coInterface.bindLBHLinks();
    },
    copyLink : function(){
        $(".copy-link-form-ocecotools").on().click(function(){
            var clipboard = new ClipboardJS('.copy-link-form-ocecotools');
            clipboard.on('success', function(e) {
                e.clearSelection();
                toastr.success(trad.copy);
            });
            
            clipboard.on('error', function(e) {
            });
        })
    },
    openConfig : function(otObj,smWizardStep){
        let clipboardText = `${baseUrl}#answer.index.id.new.form.${otObj.contextData.createdFormId}.standalone.true`;
        if(costum != null)
            clipboardText = `${baseUrl}/costum/co/index/slug/${costum.contextSlug}#answer.index.id.new.form.${otObj.contextData.createdFormId}.mode.w.standalone.true`;

        ajaxPost(".config-form-container-ocecotools", baseUrl+'/survey/form/edit/id/'+otObj.contextData.createdFormId,
        null,
        function(html){
            $(otObj.container).find(".addStepBtn,.createFrom,.sortBtn,.addQuestion,.deleteLine,.deleteFormBtn,.editFormBtn,.copystandalonelink").remove();

            $('.previewTpl.btn-danger').each(function(){
                $(this).trigger('click');
                return false;
            })

            $('.form-config-btn-container').append(`
                <a href="javascript:;" class="btn btn-danger bold copy-link-form-ocecotools" data-original-title=' + trad.copyformlink + ' data-clipboard-text="${clipboardText}" data-clipboard-action="copy" >
                <i class="fa fa-link"></i> ${tradForm["Click to copy the shareable link"]}
                </a>
            `);
            $('.sw-toolbar-elm .sw-btn-next').remove();
            $('.sw-toolbar-elm').append(`
                <a href="javascript:;" class="btn btn-danger btn-sm bold copy-link-form-ocecotools" data-original-title=' + trad.copyformlink + ' data-clipboard-text="${clipboardText}" data-clipboard-action="copy" >
                <i class="fa fa-link"></i> ${tradForm["Click to copy the shareable link"]}
                </a>
            `);
            otObj.copyLink();
        },"html");
    },
    addParams : function(otObj){
        if(notEmpty(otObj.defaultParams[otObj.what.id])){
            var path = "";
            if(otObj.what.id == "timeEvaluation"){
                path = "params"
                $.each(otObj.defaultParams[otObj.what.id],function(k,v){
                    otObj.defaultParams[otObj.what.id][k+Object.keys(otObj.contextData.inputs.inputs)[0]] = v;
                    delete otObj.defaultParams[otObj.what.id][k];
                })
            }
            var prms = {
                id : otObj.contextData.createdFormId,
                path : path,
                collection : "forms",
                value : otObj.defaultParams[otObj.what.id]
            }
            dataHelper.path2Value(prms,function(){
    
            })
        }
    },
    initSmartWizard :function(otObj,callBack){
        $('.btn-ocecotools').off().on('click',function(){
            otObj.what.label = $(this).data('label');
            otObj.what.field = $(this).data('path');
            otObj.what.id = $(this).data('id');
            $(otObj.container).remove();
            $("body").append(otObj.views.smartWizard(otObj));
            $(otObj.container).smartWizard({
                enableUrlHash: false,
                theme : "arrows",
                autoAdjustHeight: false,
                anchor: {
                    enableNavigation: false
                },
                lang: { 
                    next: trad.next,
                    previous: trad.previous
                },
                keyboard: {
                    keyNavigation: true,
                    keyLeft: [37],
                    keyRight: [39]
                },
                toolbar: {
                    showNextButton: false,
                    showPreviousButton: false,
                    extraHtml: `
                    <button class="btn btn-sm btn-danger close-ocecotools-panel" style="margin-top:2px" onclick="">${trad["Close"]}</button>
                    <button class="btn sw-btn-prev sw-btn disabled" type="button">${ucfirst(trad.previous)}</button>
                    <button class="btn sw-btn-next sw-btn" type="button">${ucfirst(trad.next)}</button>`
                }
            });

            $(otObj.container).on("leaveStep", function(e, anchorObject, currentStepIndex, nextStepIndex, stepDirection) {
                mylog.log("anchorObject: ",anchorObject,"currentStepIndex: ",currentStepIndex,"nextStepIndex: ",nextStepIndex,"stepDirection: ",stepDirection)
                if(stepDirection == "forward"){
                    if(currentStepIndex == 0 && Object.keys(otObj.contextData.parent).length == 0){
                        $(otObj.container).smartWizard("setState", [currentStepIndex], "error");
                        return false;
                    }
                    if(currentStepIndex == 1 && !notEmpty(otObj.contextData.createdFormId)){
                        $(otObj.container).smartWizard("setState", [currentStepIndex], "error");
                        return false;
                    }
                    $(otObj.container).smartWizard("unsetState", [currentStepIndex], "error");

                }
            });

            $(otObj.container).on("showStep", function(e, anchorObject, stepIndex, stepDirection, stepPosition) {
                if(stepIndex == 0){
                    dyFObj.openForm(otObj.dynforms.formConfig(otObj),null,otObj.contextData);
                }
                if(stepIndex == 1){
                    otObj.openConfig(otObj,stepIndex);
                }
            });

            $(otObj.container).on("loaded", function(e) {
                $('.close-ocecotools-panel').off().on('click',function(){
                    $("html, body").animate({ scrollTop: 0 }, "slow");
                    $(otObj.container).parent().remove();
                    $(otObj.container).smartWizard("reset");
                })
            });

            callBack();
            $('.sw .toolbar').css("position","relative");
        })
    }
}

ocecotoolsObj.defaultParams = {
    timeEvaluation : {
        "categories" : {
            "LOGISTIQUE" : {
                "Travail" : {
                    "Emploi / Travail à domicile / Etudes" : [ 
                        ""
                    ],
                    "Association" : [ 
                        ""
                    ],
                    "Bénévolat" : [ 
                        ""
                    ],
                    "Formation / personnel" : [ 
                        ""
                    ]
                },
                "Transports" : {
                    "Trajet Domicile / Travail" : [ 
                        ""
                    ],
                    "Loisirs perso" : [ 
                        ""
                    ],
                    "Loisirs enfants" : [ 
                        ""
                    ],
                    "Week end" : [ 
                        ""
                    ]
                },
                "Domicile" : {
                    "Courses" : [ 
                        ""
                    ],
                    "Gestion foyer (entretien / administratif)" : [ 
                        ""
                    ],
                    "Education" : [ 
                        ""
                    ]
                }
            },
            "RELATIONS SOCIALES" : {
                "réseaux  virtuels" : {
                    "facebook" : [ 
                        ""
                    ],
                    "twitter" : [ 
                        ""
                    ],
                    "autres" : [ 
                        ""
                    ]
                },
                "réseaux vivants" : {
                    "collègues" : [ 
                        ""
                    ],
                    "voisins" : [ 
                        ""
                    ],
                    "commerçants" : [ 
                        ""
                    ],
                    "Autres" : [ 
                        ""
                    ]
                },
                "Amis" : {
                    "Divertissements" : [ 
                        ""
                    ],
                    "Discussions" : [ 
                        ""
                    ]
                },
                "Famille" : {
                    "Divertissements" : [ 
                        ""
                    ],
                    "Discussions" : [ 
                        ""
                    ]
                },
                "Foyer" : {
                    "Repas" : [ 
                        ""
                    ],
                    "Jeux" : [ 
                        ""
                    ],
                    "Sorties" : [ 
                        ""
                    ],
                    "Discussions" : [ 
                        ""
                    ],
                    "TV" : [ 
                        ""
                    ]
                }
            },
            "SOI" : {
                "Santé" : {
                    "Se nourrir" : [ 
                        ""
                    ],
                    "Dormir" : [ 
                        ""
                    ],
                    "Soin / hygiène" : [ 
                        ""
                    ]
                },
                "Temps perso" : {
                    "Se reposer" : [ 
                        ""
                    ],
                    "Réfléchir / penser" : [ 
                        ""
                    ],
                    "Créer" : [ 
                        ""
                    ],
                    "Autres" : [ 
                        ""
                    ]
                },
                "Loisirs" : {
                    "Sport" : [ 
                        ""
                    ],
                    "bien être" : [ 
                        ""
                    ],
                    "jardiner" : [ 
                        ""
                    ],
                    "bricoler" : [ 
                        ""
                    ],
                    "s'informer" : [ 
                        ""
                    ],
                    "Lecture / Musique" : [ 
                        ""
                    ],
                    "Spectacles" : [ 
                        ""
                    ],
                    "Jeux vidéos" : [ 
                        ""
                    ],
                    "Web surfing" : [ 
                        ""
                    ],
                    "Nature" : [ 
                        ""
                    ]
                }
            }
        },
        "categoryNumber" : 3,
        "categoryTitle" : "Catégories",
        "criterias" : {
            "1" : {
                "name" : "0 à 5",
                "coeff" : "1"
            },
            "2" : {
                "name" : "5 à 10",
                "coeff" : "1"
            },
            "3" : {
                "name" : "10 à 15",
                "coeff" : "1"
            },
            "4" : {
                "name" : "15 à 20",
                "coeff" : "1"
            },
            "5" : {
                "name" : "20 à 25",
                "coeff" : "1"
            },
            "6" : {
                "name" : "25 à 30",
                "coeff" : "1"
            },
            "7" : {
                "name" : "30 à 35",
                "coeff" : "1"
            },
            "8" : {
                "name" : "35 à 40",
                "coeff" : "1"
            },
            "9" : {
                "name" : "40 à 45",
                "coeff" : "1"
            },
            "10" : {
                "name" : "45 à 50",
                "coeff" : "1"
            },
            "11" : {
                "name" : "50 et plus",
                "coeff" : "1"
            }
        },
        "criteriaLabel" : "Heures / semaine"
    },
}

ocecotoolsObj.init(ocecotoolsObj);