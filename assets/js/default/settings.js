var tradLabel={
	"default" : "By default",
	"desactivated" : "Desactivated",
	"low" : "Low",
	"high" : "High"
}
var settings = {
	bindEventsConfidentiality : function(contextId, contextType){ 		
		$(".confidentialitySettings").off().on("click",function(){
	    	param = new Object;
	    	param.type = $(this).attr("type");
	    	param.value = $(this).attr("value");
	    	param.typeEntity = contextType;
	    	param.idEntity = contextId;
			$(".btn-group-"+param.type+" .btn").removeClass("active");
			$(this).addClass("active");
			ajaxPost(
	        	null,
		        baseUrl+"/"+moduleId+"/element/updatesettings",
		        param,
		        function(data){ 
			        toastr.success(data.msg);
			        if(param.type=="crowdfunding" && param.value=="true"){
			        	urlCtrl.loadByHash("#page.type."+contextType+".id."+contextId+".view.directory.dir.crowdfunding");
			        }else if (param.type=="feedback" && param.value=="true"){
						if(contextData["type"] == "projects" || contextData["type"] == "organizations") {
							ajaxPost(
								null,
								baseUrl + "/survey/form/checkfeedback",
								{
									id : contextId ,
									type : contextData["type"]
								},
								function (data) {
									var urlCtrlafterLoad;
									if( typeof data.value != "undefined" && ( data.value == false || data.value == "false") ){

										var formtopaste = {
											"name" : "Feedback form",
											"description" : "",
											"subForms" : [
												"aapStep1",
												"aapStep2",
												"aapStep3",
												"aapStep4"
											],
											"type" : "aap",
											"subType" : "ocecoform",
											"actionDomains" : [
												"bibendum augue",
												"faucibus orci luctus",
												"diam sodales hendrerit"
											],
											"active" : true,
											"anyOnewithLinkCanAnswer" : true,
											"canModify" : true,
											"canReadOtherAnswers" : true,
											"document" : "",
											"endDate" : "31/12/2056",
											"endDateNoconfirmation" : "31/12/2056",
											"hasStepValidations" : "",
											"image" : "",
											"objectives" : [
												"Pellentesque congue.",
												"Maecenas adipiscing",
												"Maecenas adipiscin"
											],
											"oneAnswerPerPers" : true,
											"level2subType" : "feedback",
											"params" : {
												"inputForAnswerName" : "",
												"budgetdepense" : {
													"group" : [
														"Feature",
														"Costum",
														"Chef de Projet",
														"Data",
														"Mantenance",
														"Devis",
														"Installation",
														"Audit",
														"Fonctionnalité"
													],
													"nature" : [
														"investissement",
														"fonctionnement",
														"Développement",
														"developpement",
														"Test"
													]
												},
												"onlymemberaccess" : false,
												"adminabsoluteaccess" : true,
												"aapStep1" : {
													"haveEditingRules" : false,
													"haveReadingRules" : false,
													"canEdit" : "",
													"canRead" : ""
												},
												"aapStep2" : {
													"haveEditingRules" : false,
													"haveReadingRules" : false,
													"canEdit" : "",
													"canRead" : ""
												},
												"aapStep3" : {
													"haveEditingRules" : false,
													"haveReadingRules" : false,
													"canEdit" : "",
													"canRead" : ""
												},
												"aapStep4" : {
													"haveEditingRules" : false,
													"haveReadingRules" : false,
													"canEdit" : "",
													"canRead" : ""
												},
												"tags" : {
													"list" : [
														"Data",
														"markdown",
														"editor",
														"OCECO",
														"Dev",
														"communication",
														"COFORM",
														"Communecter",
														"Documentation",
														"CMS",
														"Refactor",
														"Prestation",
														"small",
														"OpenAtlas",
														"Développement",
														"feedback",
														"CoDossier",
														"ocecoMobi",
														"sso",
														"statistiques",
														"Offre",
														"openBadge",
														"TiersLieux",
														"Collaboration",
														"Interopérabilité",
														"chien",
														"blockcms",
														"identification",
														"yii2",
														"chocolat",
														"costum",
														"coRémunération",
														"Bar",
														"Fonctionnement",
														"Pôle",
														"test",
														"news",
														"Fabrique",
														"bookmark",
														"scic",
														"element",
														"activity",
														"editeur",
														"Compte",
														"coConstruction",
														"registration",
														"contribution",
														"connectivity",
														"community",
														"low",
														"amélioration",
														"référencement",
														"participation",
														"Commun",
														"Facilitation",
														"Méthodologie",
														"Accompagnement",
														"mediawiki",
														"Ariane",
														"HedgeDoc",
														"DDA",
														"CodiMD",
														"Pad",
														"OBSERVATOIRE",
														"Better",
														"Brainstorm",
														"data",
														"Consultation",
														"extern",
														"université",
														"scrum",
														"diagnostique",
														"commune",
														"journalisme",
														"Content",
														"workflow",
														"dossier",
														"Financement",
														"évaluation",
														"Connaissance",
														"Solutions",
														"Stratégie",
														"Communs",
														"Cofinancement",
														"projet",
														"uploader",
														"uploader,Refactor",
														"uploader,Refactor,amélioration",
														"uploader,amélioration",
														"subvention",
														"subvention,ANCT",
														"subvention,ANCT,AAP",
														"TiersLieux,Prefecture",
														"TiersLieux,Prefecture,Réunion",
														"amélioration,directory",
														"crowdfunding",
														"crowdfunding,Financement",
														"OCECO,gitlab",
														"OCECO,ocecoMobi,test,Dev",
														"Refactor,design",
														"Refactor,search",
														"tags",
														"tags,Refactor",
														"Refactor,Data",
														"design",
														"OCECO,gitlab,OCECO,ocecoMobi,test,Dev",
														"",
														"template",
														"innovation",
														"graph",
														"FabLab",
														"cotools",
														"calendar",
														"agenda",
														"traduction"
													]
												},
												"checkboxNewurgency" : {
													"list" : [
														"Urgent"
													]
												}
											},
											"private" : false,
											"showAnswers" : true,
											"startDate" : "10/11/2021",
											"startDateNoconfirmation" : "31/12/2056",
											"temporarymembercanreply" : false,
											"withconfirmation" : true,
											"updated" : 1648462469,
											"inputConfig" : {
												"multiDecide" : "tpls.forms.aap.evaluation"
											},
											"evaluationCriteria" : {
												"activateLocalCriteria" : true,
												"whoCanEvaluate" : "",
												"type" : "starCriterionBased",
												"criterions" : [
													{
														"label" : "Budget",
														"coeff" : "1",
														"note" : "0"
													},
													{
														"label" : "Contribution au Commun",
														"coeff" : "1",
														"note" : "0"
													},
													{
														"label" : "Temps de réalisation",
														"coeff" : "1",
														"note" : "0"
													},
													{
														"label" : "Humain",
														"coeff" : "1",
														"note" : "0"
													}
												]
											},
											"what" : ""
										}

										data["feedbackTpl"] = Object.values(data["feedbackTpl"])[0];

										formtopaste["config"] = data["feedbackTpl"]["_id"]["$id"];

										formtopaste["parent"] = {};
										formtopaste["parent"][contextId] = {};
										formtopaste["parent"][contextId]["type"] = contextData["type"];
										formtopaste["parent"][contextId]["name"] = contextData["name"];

										formtopaste["creator"] = userId;

										tplCtx = {
											collection: "forms",
											value: formtopaste,
											format : true,
											path: "allToRoot"
										};

										inputsarray = [
											{
												"titre": {
													"label": "Nom de la proposition",
													"placeholder": "Nom de la proposition",
													"info": "",
													"type": "text"
												},
												"description": {
													"label": "Description de la proposition",
													"placeholder": "Nom de la proposition",
													"info": "",
													"type": "textarea",
													"markdown": true
												},
												"image": {
													"label": "Ajouter une image pour votre proposition",
													"placeholder": "",
													"info": "",
													"type": "tpls.forms.uploader",
													"uploader": {
														"docType": "image",
														"contentKey": "profil",
														"paste": "true",
														"restricted": "true",
														"itemLimit": "5"
													}
												},
												"depense": {
													"label": "Les étapes du projet ",
													"placeholder": "",
													"info": "",
													"type": "tpls.forms.ocecoform.budget"
												},
												"tags": {
													"label": "Thématique",
													"placeholder": "",
													"info": "",
													"type": "tpls.forms.tags"
												},
												"urgency": {
													"label": "Urgence",
													"placeholder": "",
													"info": "",
													"type": "tpls.forms.cplx.checkboxNew"
												}
											},
											{
												"decide" : {
													"label" : "Dépenses",
													"placeholder" : "Dépenses",
													"info" : "",
													"type" : "tpls.forms.ocecoform.multiDecide"
												},
												"evaluation" : {
												}
											},
											{
												"financer" : {
													"label" : "Financement",
													"placeholder" : "financement",
													"info" : "",
													"type" : "tpls.forms.ocecoform.financementFromBudget"

												},
												"generateproject" : {
													"label" : "Projet associé",
													"placeholder" : "",
													"info" : "Générer le projet",
													"type" : "tpls.forms.ocecoform.generateprojectbtn"
												}
											},
											{
												"suivredepense" : {
													"label" : "Dépenses",
													"placeholder" : "Dépenses",
													"info" : "",
													"type" : "tpls.forms.ocecoform.suiviFromBudget"
												}
											}
										];

										dataHelper.path2Value(tplCtx, function (params) {
											for (i = 1 ; i < 5 ; i++){
												var inputs = {
													"isSpecific" : true,
													"type" : "openForm",
													"step" : "aapStep"+i,
												}

												inputs["formParent"] = params["saved"]["_id"]["$id"]
												inputs["inputs"] = inputsarray[i - 1];

												tplCtxi = {
													collection: "inputs",
													value: inputs,
													format : true,
													path: "allToRoot"
												};

												dataHelper.path2Value(tplCtxi, function (params) {

												});
											}
										});
										urlCtrl.loadByHash(location.hash , null , urlCtrl.afterLoad);
									} else {
										urlCtrl.loadByHash(location.hash , null , urlCtrl.afterLoad);
									}
								}
							);

						}

			        } else {
						urlCtrl.loadByHash(location.hash , null , urlCtrl.afterLoad);
						if(
							param.type == "activitypub" && 
							param.value == "true" &&
							activitypubGuide.currentStep.step == activitypubGuide.constants.steps.SETTING
						) {
							activitypubGuide.actions.open(activitypubGuide.constants.steps.COMMUNITY);
						}
					}
			    }
			);
			if (param.type === 'private') {
				if (params && params.element && params.element.preferences) {
					if (param.value === 'true'){
						params.element.preferences.private = true;
						delete contextData.hasRC;
					} else {
						params.element.preferences.private = false;
						delete contextData.hasRC;
					}
				}
			}
		});
	},
	bindButtonConfidentiality : function(preferences){
		var fieldPreferences={};
		$.each(nameFields, function(e, v){
			fieldPreferences[v]=true;
		});
		//To checked private or public
		$.each(typePreferences, function(e, typePref){
			$.each(fieldPreferences, function(field, hidden){
				if(notNull(preferences) && typeof preferences[typePref] != "undefined" && $.inArray(field, preferences[typePref])>-1){
					$('.btn-group-'+field+' > button[value="'+typePref.replace("Fields", "")+'"]').addClass('active');
					fieldPreferences[field]=false;		
				}
			});
		});
		//To checked if there are hidden
		$.each(fieldPreferences, function(field, hidden){
			if(hidden) $('.btn-group-'+field+' > button[value="hide"]').addClass('active');
		});
		$.each(typePreferencesBool, function(field, typePrefB){
			mylog.log("pref ", field, typePrefB, preferences)
			if(notNull(preferences) && typeof preferences[typePrefB] != "undefined" && ( preferences[typePrefB] == true || preferences[typePrefB] == "true") )
				$('.btn-group-'+typePrefB+' > button[value="true"]').addClass('active');	
			else
				$('.btn-group-'+typePrefB+' > button[value="false"]').addClass('active');
		});
	},
	bindEventsSettings : function(){
		$(".BSswitch").bootstrapSwitch();
	   	$(".BSswitch").on("switchChange.bootstrapSwitch", function (event, state) {
	    	settings.savePreferencesNotification("notifications",state, "citoyens", userId, $(this).data("sub"));
	    });
	   	$(".btn-show-block").click(function(){
	   		$(".show-block").hide(700);
	   		$(this).parents().eq(1).find(".show-block").show(700);
	   		/*if($(this).data("name")=="community")
	   			settings.getCommunitySettings();*/
	   		if($(this).data("name")=="mymails")
	   			settings.settingsCommunityEvents();

	   	});
	},
	initNotificationsAccount: function(preferences){
		if(typeof preferences.notifications != "undefined"){
			$.each(preferences.notifications, function(e, v){
				$(".BSswitch[data-sub='"+e+"'").removeAttr("checked");
			});
		}
		if(typeof preferences.mails != "undefined"){
			$("#mails-settings .changeValueDrop").text(tradLabel[preferences.mails]);
		}
	},
	settingsCommunityEvents : function(){
		$(".settingsCommunity").off().on("click", function() {
			settings.savePreferencesNotification($(this).data("settings"),$(this).data("value"), $(this).data("type"), $(this).data("id"));
			$(this).parents().eq(2).find(".dropdown-settings .changeValueDrop").html(tradSettings[$(this).data("value")]);
		});
		$("#community-settings #search-in-settings").keyup(function(){
			settings.filterSettingsCommunity($(this).val());
		});
		
		//parcourt tous les types de contacts
		$.each(["organizations", "projects", "events"], function(key, type){ 
			//initialise le scoll automatique de la liste de contact
			$(".settingsHeader #btn-scroll-type-"+type).mouseover(function(){
				var scrollTOP = $("#container-settings-view #scroll-type-"+type).position().top;
				$('#container-settings-view').scrollTop(scrollTOP);
			});
		});
		$("#btnSettingsInfos").off().on("click",function(){
			$("#modalExplainSettings").modal("show"); 
		});
		coInterface.bindLBHLinks();
	},
	//recherche text par nom, cp, city, slug
	filterSettingsCommunity: function(searchVal){
		
		//masque/affiche tous les contacts présents dans la liste
		if(searchVal != "")	$("#community-settings .notification-label-communtiy").hide();
		else				$("#community-settings .notification-label-communtiy").show();
		//recherche la valeur recherché dans les 3 champs "name", "cp", et "city"
		$.each($("#community-settings .name-contact"), function() { settings.checkItemSearch($(this), searchVal); });
		$.each($("#community-settings .slug-contact"), function() { settings.checkItemSearch($(this), searchVal, "slug"); });
		$.each($("#community-settings .cp-contact"),   function() { settings.checkItemSearch($(this), searchVal); });
		$.each($("#community-settings .city-contact"), function() { settings.checkItemSearch($(this), searchVal); });
	},
	//si l'élément contient la searchVal, on l'affiche
	checkItemSearch : function(thisElement, searchVal, type){
		var content = (typeof type != "undefined" && type=="slug") ? thisElement.val() : thisElement.html();
		var found = content.search(new RegExp(searchVal, "i"));
		if(found >= 0){
			var id = thisElement.attr("idcontact");
			
			$("#community-settings .contact"+id).show();
		}
	},
	showHideOldElements : function(type) {
		$(".oldSettingsCommunity"+type).toggle("slow");
	},
	// Return true if the endDate of the Element is before the current Date. 
	isOldElement : function(value) {
		var endDate = (typeof value["endDate"] != undefined) ? value["endDate"] : "";
		if (endDate == "") return false;
		return new Date(endDate) < new Date();
	},
	savePreferencesNotification : function(settingsName, settingsValue, parentType, parentId, settingsSubName){
		var updateSettings={
			"settings" : settingsName,
			"value" : settingsValue,
			"type" : parentType,
			"id" : parentId
		};
		if(notNull(settingsSubName)) updateSettings.subName=settingsSubName;
		ajaxPost(
        	null,
	        baseUrl+"/"+moduleId+"/element/updatesettings",
	        updateSettings,
	        function(data){ 
		        if(data.result){
		  			if($.inArray(updateSettings.settings, ["mails", "notifications"])>0)
		  				settings.updateMyContacts(updateSettings);
					toastr.success(tradSettings.notificationsSettingsSuccess);
		  		}
				else
					toastr.error(data.msg);
		    }
	    );
	},
	updateMyContacts : function(values){
		//typeContact=(values.type=="citoyens") ? "people" : values.type;
		if(typeof myContacts[values.type] != "undefined" && typeof myContacts[values.type][values.id] != "undefined"){
			myContacts[values.type][values.id][values.settings]=values.value;
		}
	},
	getCommunitySettings : function(typeSet){
		var scrollContent = "";
		var str = "";
		if(typeof myContacts != "undefined"){
			$.each(myContacts, function(type, array){
				if(type != "citoyens" && type!="follows"){
			
			scrollContent += "<a href='javascript:' id='btn-scroll-type-"+type+"' class='text-"+typeObj[typeObj[type].sameAs].color+" btn-scroll-type pull-left'><i class='fa fa-"+typeObj[typeObj[type].sameAs].icon+"'></i> <span class='hidden-xs'>"+trad['my'+type]+"</span></a>";
			str += 		'<div class="panel panel-default scroll-container col-xs-12 no-padding" id="scroll-type-'+type+'">  '+	
							'<div class="panel-heading">';
				str += 			'<h4 class="text-'+typeObj[typeObj[type].sameAs].color+'">'+
									'<i class="fa fa-'+typeObj[typeObj[type].sameAs].icon+'"></i> <span class="">'+trad['my'+type]+"</span>";
									if (type == "events" || type == "projects") {
				str += 					'<button onclick="settings.showHideOldElements(\''+type+'\')" class="tooltips btn btn-default btn-sm pull-right btn_shortcut_add text-'+typeObj[typeObj[type].sameAs].color+'" data-placement="left" data-original-title="'+trad["showhideold"+type]+'">'+
											'<i class="fa fa-history"></i>'+
										'</button>';		
									}
				str += 			'</h4>'+
							'</div>';
								
					$.each(array, function(e, value){
						if((typeof value.isFollowed == "undefined" || (typeof value.isAdmin != "undefined" && typeof value.isAdminPending == "undefined")) 
							&& typeof value.toBeValidated == "undefined"){
							var oldElement = isOldElement(value);
							var profilThumbImageUrl = (typeof value.profilThumbImageUrl != "undefined" && value.profilThumbImageUrl != "") ? baseUrl + value.profilThumbImageUrl : defaultImage;
							var id = (typeof value._id != "undefined" && typeof value._id.$id != "undefined") ? value._id.$id : id;
							var setNotif = (typeof value.notifications != "undefined") ? tradSettings[value.notifications] : tradSettings["bydefault"];
							var setMails = (typeof value.mails != "undefined") ? tradSettings[value.mails] : tradSettings["bydefault"];
							var elementClass = oldElement ? "oldSettingsCommunity"+type : "";
							var elementStyle = oldElement ? "display:none" : ""; 
							var cp = (typeof value.address != "undefined" && notNull(value.address) && typeof value.address.postalCode != "undefined") ? value.address.postalCode : typeof value.cp != "undefined" ? value.cp : "";
							var city = (typeof value.address != "undefined" && notNull(value.address) && typeof value.address.addressLocality != "undefined") ? value.address.addressLocality : "";
							var isEltAdmin=(typeof value.isAdmin != "undefined" && typeof value.isAdminPending == "undefined") ? true : false;
							str+='<div class="col-xs-12 padding-5 notification-label-communtiy '+elementClass+' contact'+id+'" style="'+elementStyle+'" id="settingsItem-'+type+'-'+id+'" idcontact="'+id+'">'+
									'<div class="btn-chk-contact col-md-6 col-sm-6 col-xs-12">' +
										'<img src="'+ profilThumbImageUrl+'" class="thumb-send-to bg-'+typeObj[typeObj[type].sameAs].color+' pull-left" height="35" width="35">'+
										'<span class="info-contact col-xs-10 margin-top-5">' +
											'<span class="name-contact text-dark text-bold elipsis pull-left" idcontact="'+id+'"><a href="#page.type.'+type+'.id.'+id+'" class="lbh-preview-element">' + value.name + '</a></span>'+
											'<input type="hidden" class="slug-contact" idcontact="'+id+'" value="'+value.slug+'">'+
											'<br/>'+
											'<span class="text-red pull-left">'+((isEltAdmin) ? trad.administrator : ucfirst(trad.member))+'</span>'+
											'<br/>'+
											'<span class="cp-contact text-light pull-left" idcontact="'+id+'">' + cp + '&nbsp;</span>'+
											'<span class="city-contact text-light pull-left" idcontact="'+id+'">' + city + '</span>'+
										'</span>' +
									'</div>';
								//if(typeSet=="notifications")
									str+=settings.getToolbarSettings(setNotif, setMails, type, id, isEltAdmin);
								//else if(typeSet=="confidentiality")
								//	str+=settings.getToolbarSettingsConfidentiality(type,id);
							str+='</div>';
						}
					});
					str+="</div>";
				}
			});
			$("#community-settings-list").html(str);
			//$("#settingsScrollByType").html(scrollContent);
			settings.settingsCommunityEvents();
		}
	},
	getToolbarSettings: function(setNotif, setMails, type, id, isAdmin){
		html='<div class="col-md-6 col-sm-6 col-xs-12">';
			if(isAdmin){
				html+='<div class="col-xs-12 no-padding margin-bottom-5">'+
	  				'<a class="btn btn-default col-md-12 col-sm-12 col-xs-12 dropdown-settings" href="javascript:;" onclick="settings.showPanelConfidentiality(\''+type+'\',\''+id+'\',true)">'+
	  					'<i class="fa fa-cogs"></i> '+tradSettings.settingsConfidentiality+
	  				'</a>'+	
	        	'</div>';
	        }
			html+=settings.getToolbarSettingsNotifications(setNotif, setMails, type, id)+
    	'</div>';
		return html;
	},
	getToolbarSettingsNotifications: function(setNotif, setMails, type, id){
		str='<div class="col-xs-12 no-padding">'+
				'<div class="dropdown no-padding col-xs-12 margin-bottom-5">'+
      				'<a data-toggle="dropdown" class="btn btn-default col-md-12 col-sm-12 col-xs-12 dropdown-settings" href="javascript:;">'+
      					'<i class="fa fa-bell"></i> <span class="hidden-xs">'+trad.notifications+' : </span><span class="changeValueDrop">'+setNotif+'</span> <i class="fa fa-caret-down" style="font-size:inherit;"></i>'+
      				'</a>'+
						'<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">'+
  						'<li>'+
      						'<a href="javascript:;" class="settingsCommunity" data-settings="notifications" data-value="desactivated" data-type="'+type+'" data-id="'+id+'">'+
      							tradSettings.desactivated+
      						'</a>'+
    					'</li>'+
    					'<li>'+
      						'<a href="javascript:;" class="settingsCommunity" data-settings="notifications" data-value="low" data-type="'+type+'" data-id="'+id+'">'+
      							tradSettings.low+
      						'</a>'+
    					'</li>'+
    					'<li>'+
      						'<a href="javascript:;" class="settingsCommunity" data-settings="notifications" data-value="default" data-type="'+type+'" data-id="'+id+'">'+
      							tradSettings.bydefault+
      						'</a>'+
    					'</li>'+
    					'<li>'+
      						'<a href="javascript:;" class="settingsCommunity" data-settings="notifications" data-value="high" data-type="'+type+'" data-id="'+id+'">'+
      							tradSettings.high+
      						'</a>'+
    					'</li>'+
					'</ul>'+
        		'</div>'+
        	'</div>'+
        	'<div class="col-xs-12 no-padding">'+
				'<div class="dropdown no-padding col-xs-12 margin-bottom-5">'+
      				'<a data-toggle="dropdown" class="btn btn-default col-md-12 col-sm-12 col-xs-12 dropdown-settings" href="javascript:;">'+
      					'<i class="fa fa-envelope"></i> <span class="hidden-xs">Emails : </span><span class="changeValueDrop">'+setMails+'</span> <i class="fa fa-caret-down" style="font-size:inherit;"></i>'+
      				'</a>'+
						'<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">'+
  						'<li>'+
      						'<a href="javascript:;" class="settingsCommunity" data-settings="mails" data-value="desactivated" data-type="'+type+'" data-id="'+id+'">'+
      								tradSettings.desactivated+
      						'</a>'+
    					'</li>'+
    					'<li>'+
      						'<a href="javascript:;" class="settingsCommunity" data-settings="mails" data-value="low" data-type="'+type+'" data-id="'+id+'">'+
      							tradSettings.low+
      						'</a>'+
    					'</li>'+
    					'<li>'+
      						'<a href="javascript:;" class="settingsCommunity" data-settings="mails" data-value="default" data-type="'+type+'" data-id="'+id+'">'+
      							tradSettings.bydefault+
      						'</a>'+
    					'</li>'+
    					'<li>'+
      						'<a href="javascript:;" class="settingsCommunity" data-settings="mails" data-value="high" data-type="'+type+'" data-id="'+id+'">'+
      							tradSettings.high+
      						'</a>'+
    					'</li>'+
					'</ul>'+
        		'</div>'+
        	'</div>';
    	//'</div>';
		return str;
	},
	/*getToolbarSettingsConfidentiality: function(type, id){
		return html;
	},*/
	showPanelConfidentiality: function(type, id, modal){
		ajaxPost('#modalConfidentialityCommunity' ,baseUrl+'/'+moduleId+"/settings/confidentiality/type/"+type+"/id/"+id+"/modal/true",
			 null,function(){
			 	$("#modal-confidentiality").modal("show");
			 });
	},
	bindMyAccountSettings : function(element){
		$("#btn-update-password-setting").click(function(){
			var form = {
				saveUrl : baseUrl+"/"+moduleId+"/person/changepassword",
				dynForm : {
					jsonSchema : {
						title : trad["Change password"],
						icon : "fa-key",
						onLoads : {
					    	//pour creer un subevnt depuis un event existant
					    	onload : function(){
					    		//dyFInputs.setHeader("bg-green");
					    		$("#ajax-modal .modal-header").addClass("bg-dark");
				                $("#ajax-modal .infocustom p").addClass("text-dark");
				    	   	}
				    	},
						afterSave : function(data){
							dyFObj.closeForm();
						},
						afterBuild: function(){
							$(".icon-eye").on('click', function(){
								$(this).toggleClass("fa-eye fa-eye-slash");
								var input = $("#"+$(this).data("field"));
								if (input.attr("type") == "password") {
									input.attr("type", "text");
								} else {
									input.attr("type", "password");
								}
							})
						},
						properties : {
							mode : dyFInputs.inputHidden(),
							userId : dyFInputs.inputHidden(),
							oldPassword : dyFInputs.password(trad["Old password"]),
							newPassword : dyFInputs.password("", { required : true, minlength : 8 } ),
							newPassword2 : dyFInputs.password(trad["Repeat your new password"], {required : true, minlength : 8, equalTo : "#ajaxFormModal #newPassword"})	
						}
					}
				}
			};
			//alert("btn-update-password");
			var dataUpdate = {
				mode : "changePassword",
		        userId : userId
		    };

			dyFObj.openForm(form, null, dataUpdate);
			//lert("btn-update-password");
		});

		$("#btn-delete-element-setting").on("click", function(){
	    	$("#modal-delete-element").modal("show");
	    });

	    $("#downloadProfil-setting").click(function () {
	    	extraParamsAjax={
	    		async:false,
				crossDomain:true
			};	
	    	ajaxPost(
	        	null,
		        baseUrl+"/"+moduleId+"/data/get/type/citoyens/id/"+contextData.id,
		        null,
		        function(obj){ 
			        $("<a/>", {
					    "download": "profil.json",
					    "href" : "data:application/json," + encodeURIComponent(JSON.stringify(obj))
					  }).appendTo("body")
					  .click(function() {
					    $(this).remove()
					  })[0].click() ;
			    }
		    );
		});
	},
}

/*function getHeaderCommunitySettings(){
 	var HTML = '<div class="settingsHeader bg-white no-padding">'+
				'<div id="settingsScrollByType" class="pull-left"></div>' +
				'<a href="javascript:;" id="btnSettingsInfos" class="text-dark pull-right margin-right-20"><i class="fa fa-info-circle"></i> <span class="hidden-xs"> All infos</span></a>' +
				'<input type="text" id="search-in-settings" class="form-control" placeholder="'+trad.searchnamepostalcity+'">'+
			'</div>';

	return HTML;
}*/
