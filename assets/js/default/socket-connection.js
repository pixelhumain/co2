var wsCO = null;
var wsCOpubSub = {
    subscribers: [],
    subscriber(action) {
        this.subscribers.push(action)
    },
    publish(){
        this.subscribers.forEach((_, index) => this.subscribers[index](wsCO))
    }
}
$(function(){
    var socketConfigurationEnabled = coWsConfig && coWsConfig.enable && coWsConfig.serverUrl && coWsConfig.pingNorificationUrl,
        userConnected = Boolean(userId)
    if(socketConfigurationEnabled && userConnected){
        wsCO = io(coWsConfig.serverUrl, {
            query:{
                userId: userId
            }
        })

        wsCOpubSub.publish();
        
        wsCO.on('new-notification', function(){
            getAjaxNotification("", "refresh", "citoyens", userId, true)
        })
    }
})