var directorySocket = {
    init: function() {
        if(currentUser){
            directorySocket.emitEvent(wsCO, "join_room", {userId: userId, userName: currentUser.name});
        }
        if(wsCO){
            wsCO.on("join_user", function(data){
                // pageProfil.init();
				if(pageProfil.params.view == "conode"){
					window[`communityGraph${contextData.id}`].addChild(data.collection, {
						id: data._id.$id,
						name: data.name,
						bread: data.name,
						group: data.collection,
						profil: typeof data.profilThumbImageUrl != "undefined" ? data.profilThumbImageUrl : (typeof data.profilImageUrl != "undefined" ? data.profilImageUrl : null),
					});
				} else if(pageProfil.params.view == "directory" && pageProfil.params.dir == "members"){
					
					let datas = {};
					let _id = data._id.$id;

					datas[_id] = data;

					let community = directorySocket.events.communityVariable(data, _id);

					let str = directory.showResultsDirectoryHtml(datas, "", "", community);

					let targetDiv = document.querySelector(`.msr-id-${_id}`);
					if (targetDiv) {
						targetDiv.parentElement.remove();
					} else {
						directorySocket.events.updateNomberElements(datas, "add");
					}
					filterGroup.results.$grid.prepend(str);
					filterGroup.results.$grid.masonry('destroy');
					filterGroup.results.$grid.masonry({itemSelector: '.smartgrid-slide-element', 'columnWidth': '.searchEntityContainer.smartgrid-slide-element'});

					targetDiv = document.querySelector(`.msr-id-${_id}`);
					let image = targetDiv.querySelectorAll('img.lzy_img');
					image.forEach((v) => {
						v.dom = "#content-results-profil";
						imageObserver.observe(v);
					});
					directory.bindEventAdmin();
					if(filterGroup.results.map.active){
						filterGroup.mapObj.addElts(datas);
					}

				} else {
					filterGroup.search.init(filterGroup);
				}

				if(typeof window[`kanbanDom${contextData.id}`] != "undefined"){
					let newRole = {
						_id : data._id.$id,
						id : data._id.$id,
						title : "<b>"+data.name+"</b>",
						header : "memberof",
						canEditHeader: false,
						html : true,
						collection : data.collection,
						slug : data.slug,
						actions : [
							{
								'icon': 'fa fa-trash' ,
								'bstooltip' : {
									'text' : 'Supprimer le membre de la communauter', 
									'position' : 'left'
								}, 
								'action': 'onRemoveMember'
							}
						]
					}
					window[`kanbanDom${contextData.id}`].kanban('addData', newRole);
				}
            }).on("remove_user", function(data){
				let _id = data._id.$id;
				if(pageProfil.params.view == "conode"){
					window[`communityGraph${contextData.id}`].removeChild(data.collection, _id);
				}else {
					let targetDiv = document.querySelector(`.msr-id-${_id}`);
				
					if (targetDiv) {
						targetDiv.parentElement.remove();
						filterGroup.results.$grid.masonry('destroy');
						filterGroup.results.$grid.masonry({itemSelector: '.smartgrid-slide-element', 'columnWidth': '.searchEntityContainer.smartgrid-slide-element'});

						directorySocket.events.updateNomberElements(data, "remove");
					}
					if(filterGroup.results.map.active && typeof filterGroup.mapObj.removeElts != "undefined"){
						filterGroup.mapObj.removeElts(_id);
					}
				}
				if(typeof window[`kanbanDom${contextData.id}`] != "undefined"){
					var indexToDelete = window[`kanbanDom${contextData.id}`].find(`[data-column="memberof"] .kanban-list-card-detail`).index($(`div.kanban-list-card-detail[data-id=${_id}]`));
					window[`kanbanDom${contextData.id}`].kanban('deleteData', { column: "memberof", id:_id,  index: indexToDelete });
				}
            }).on("invite_user", function(data){
				let show = false;
				if(data.invitedId == userId) {
					directorySocket.events.invitedMe();
				}

				if(
					typeof userConnected.roles != "undefined" && typeof userConnected.roles.superAdmin != "undefined" && userConnected.roles.superAdmin
				){
					show = true;
				}else if(contextData.type == "organizations"){
					if(typeof userConnected.links.memberOf[contextData.id] != "undefined"
						&& typeof userConnected.links.memberOf[contextData.id].isAdmin != "undefined"
						&& userConnected.links.memberOf[contextData.id].isAdmin
						&& (typeof userConnected.links.memberOf[contextData.id].isAdminPending == "undefined" || typeof userConnected.links.memberOf[contextData.id].isInviting == "undefined")
					){
						show = true;
					}
				}else if(contextData.type == "projects"){
					if(typeof userConnected.links.projects[contextData.id] != "undefined"
						&& typeof userConnected.links.projects[contextData.id].isAdmin != "undefined"
						&& userConnected.links.projects[contextData.id].isAdmin
						&& (typeof userConnected.links.projects[contextData.id].isAdminPending == "undefined" || typeof userConnected.links.projects[contextData.id].isInviting == "undefined")
					){
						show = true;
					}
				}else if(contextData.type == "organizations"){
					if(typeof userConnected.links.memberOf[contextData.id] != "undefined"
						&& typeof userConnected.links.memberOf[contextData.id].isAdmin != "undefined"
						&& userConnected.links.memberOf[contextData.id].isAdmin
						&& (typeof userConnected.links.memberOf[contextData.id].isAdminPending == "undefined" || typeof userConnected.links.memberOf[contextData.id].isInviting == "undefined")
					){
						show = true;
					}
				}
                if(typeof data.donne != "undefined" && data.donne != null && show){
					
					let donne = data.donne;
					let datas = {};
					datas[data.invitedId] = donne;

					let community = directorySocket.events.communityVariable(donne, data.invitedId);
					let str = directory.showResultsDirectoryHtml(datas, "", "", community);
					var targetDiv = document.querySelector(`.msr-id-${data.invitedId}`);
					if (targetDiv) {
						targetDiv.parentElement.remove();
					} else {
						directorySocket.events.updateNomberElements(datas, "add");
					}

					filterGroup.results.$grid.prepend(str);
					filterGroup.results.$grid.masonry('destroy');
					filterGroup.results.$grid.masonry({itemSelector: '.smartgrid-slide-element', 'columnWidth': '.searchEntityContainer.smartgrid-slide-element'});

					targetDiv = document.querySelector(`.msr-id-${data.invitedId}`);
					var newSpan = document.createElement('span');
					newSpan.className = 'entityStatusLink italic';
					newSpan.textContent = 'Invitation envoyée';

					let slideHover = targetDiv.querySelector('.slide-hover');
					var textWrap = slideHover.querySelector('.text-wrap');
					var adminToolBar = slideHover.querySelector('.adminToolBarDirectory');

					let image = targetDiv.querySelectorAll('img.lzy_img');
					image.forEach((v) => {
						v.dom = "#content-results-profil";
						imageObserver.observe(v);
					});

					directory.bindEventAdmin();
					textWrap.insertBefore(newSpan, adminToolBar);
                }
            }).on('add_members', function(data){
				if(typeof window[`kanbanDom${contextData.id}`] != "undefined" && window[`kanbanDom${contextData.id}`]){
					window[`load${contextData.id}`] = false;
					if($(`div#kanban-wrapper-${data.column.id}`).length == 0){
						var copyColumn = $.extend({}, data.column);
						copyColumn.editable = false;
						copyColumn.menus = [];
						mylog.log("Column to add", copyColumn);
						window[`kanbanDom${contextData.id}`].kanban('addColumn', {dom: $(".js-add-column").prev(".kanban-list-wrapper"), column: copyColumn});
						let newRole = {
							_id : copyColumn.id,
							id : copyColumn.id,
							instanceIdentity : copyColumn.id,
							title : "<i>"+copyColumn.label+"</i>",
							name : copyColumn.label,
							header : "listRole",
							canEditHeader: false,
							html : true,
							canMoveCard: false,
							actions : [
								{icon: 'fa fa-trash', action: 'onRemoveRole'},
								{icon: 'fa fa-user',badge: copyColumn.count, action: '' }
							]
						}
						window[`kanbanDom${contextData.id}`].kanban('addData', newRole);
					}
					var copy = $.extend({}, data.data);
					copy.canMoveCard = false;
					copy.actions = [
						{ icon: 'fa fa-trash', action: 'onDelete'}
					];
					if(typeof contextData.links != "undefined" && 
						typeof contextData.links[data.connectType] != "undefined" &&
						typeof contextData.links[data.connectType][copy.instanceIdentity] != "undefined" &&
						typeof contextData.links[data.connectType][copy.instanceIdentity].roles != "undefined"){
							mylog.log("Roles to push", contextData.links[data.connectType][copy.instanceIdentity].roles)
							contextData.links[data.connectType][copy.instanceIdentity].roles.push(data.column.label);
						}
						else
							contextData.links[data.connectType][copy.instanceIdentity]={"roles": [data.column.label]};
					if(typeof window[`dataMember${contextData.id}`] != "undefined" &&
						typeof window[`dataMember${contextData.id}`][copy.instanceIdentity] != "undefined" &&
						typeof window[`dataMember${contextData.id}`][copy.instanceIdentity].links != "undefined" &&
						typeof window[`dataMember${contextData.id}`][copy.instanceIdentity].links[data.connectType == "members" ? "memberOf" : data.connectType] != "undefined" &&
						typeof window[`dataMember${contextData.id}`][copy.instanceIdentity].links[data.connectType == "members" ? "memberOf" : data.connectType][contextData.id] != "undefined" &&
						typeof window[`dataMember${contextData.id}`][copy.instanceIdentity].links[data.connectType == "members" ? "memberOf" : data.connectType][contextData.id].roles != "undefined"){
							window[`dataMember${contextData.id}`][copy.instanceIdentity].links[data.connectType == "members" ? "memberOf" : data.connectType][contextData.id].roles.push(data.column.label);
						}else{
							window[`dataMember${contextData.id}`][copy.instanceIdentity].links[data.connectType == "members" ? "memberOf" : data.connectType][contextData.id].roles = [data.column.label];
						}
					window[`kanbanDom${contextData.id}`].kanban('addData', copy);
					let htm = `${data.column.count} <span class="fa fa-user"> </span>`;
					$('div[data-id='+data.column.id+'] .kanban-footer-card .nbpers').html(htm);
					if(!$(`div#kanban-wrapper-${data.column.id}`).is(":visible")){
						$(`div[data-id=${data.column.id}]`).click();
					}
					pageProfil.views.directory();
				}
			}).on("remove_members", function(data){
				window[`load${contextData.id}`] = false;
				let htm = `${data.column.count} <span class="fa fa-user"> </span>`;
				$('div[data-id='+data.column.id+'] .kanban-footer-card .nbpers').html(htm);
				if(!$(`div#kanban-wrapper-${data.column.id}`).is(":visible")){
					$(`div[data-id=${data.column.id}]`).click();
				}
				if(data.column.count == 0) {
					$('div[data-id = '+data.column.id+']').addClass('hidden')
					$('#kanban-wrapper-'+data.column.id).addClass('hidden')
				}
				if(typeof contextData.links != "undefined" && 
					typeof contextData.links[data.connectType] != "undefined" &&
					typeof contextData.links[data.connectType][data.data.instanceIdentity] != "undefined" &&
					typeof contextData.links[data.connectType][data.data.instanceIdentity].roles != "undefined"){
						contextData.links[data.connectType][data.data.instanceIdentity].roles.splice(contextData.links[data.connectType][data.data.instanceIdentity].roles.indexOf(data.column.label), 1);
					}
				if(typeof window[`dataMember${contextData.id}`] != "undefined" &&
					typeof window[`dataMember${contextData.id}`][data.data.instanceIdentity] != "undefined" &&
					typeof window[`dataMember${contextData.id}`][data.data.instanceIdentity].links != "undefined" &&
					typeof window[`dataMember${contextData.id}`][data.data.instanceIdentity].links[data.connectType == "members" ? "memberOf" : data.connectType] != "undefined" &&
					typeof window[`dataMember${contextData.id}`][data.data.instanceIdentity].links[data.connectType == "members" ? "memberOf" : data.connectType][contextData.id] != "undefined" &&
					typeof window[`dataMember${contextData.id}`][data.data.instanceIdentity].links[data.connectType == "members" ? "memberOf" : data.connectType][contextData.id].roles != "undefined"){
						let indice = window[`dataMember${contextData.id}`][data.data.instanceIdentity].links[data.connectType == "members" ? "memberOf" : data.connectType][contextData.id].roles.indexOf(data.column.label);
						if(indice != -1){
							window[`dataMember${contextData.id}`][data.data.instanceIdentity].links[data.connectType == "members" ? "memberOf" : data.connectType][contextData.id].roles.splice(indice, 1);
						}
						// window[`dataMember${contextData.id}`][data.data.instanceIdentity].links[data.connectType == "members" ? "memberOf" : data.connectType][contextData.id].roles.push(data.column.label);
					}
				var indexToDelete = window[`kanbanDom${contextData.id}`].find(`[data-column=${data.column.id}] .kanban-list-card-detail`).index($(`div.kanban-list-card-detail[data-id=${data.data.id}${data.column.id}]`));
				window[`kanbanDom${contextData.id}`].kanban('deleteData', { column: data.column.id, id:data.data.id,  index: indexToDelete });
				pageProfil.views.directory();
			}).on("remove_role", function(data){
				
				if(contextData.type == "organizations"){
					if(typeof window[`dataMember${contextData.id}`] != "undefined"){
						$.map(window[`dataMember${contextData.id}`], function(value, key) {
							if(typeof value.links != "undefined" && typeof value.links.memberOf != "undefined"){
								$.map(value.links.memberOf, function(members, keyMembers) {
									if(keyMembers == contextData.id && typeof members.roles != "undefined" && typeof members.roles == 'object' && Array.isArray(members.roles)){
										let indice = members.roles.indexOf(data.column.name);
										if(indice != -1){
											members.roles.splice(indice, 1);
											if(typeof contextData.links != "undefined" && 
											typeof contextData.links[data.connectType] != "undefined" &&
											typeof contextData.links[data.connectType][key] != "undefined" &&
											typeof contextData.links[data.connectType][key].roles != "undefined"){
												contextData.links[data.connectType][key].roles.splice(contextData.links[data.connectType][key].roles.indexOf(data.column.name), 1);
											}
										}
										if(typeof window[`dataMember${contextData.id}`] != "undefined" &&
											typeof window[`dataMember${contextData.id}`][key] != "undefined" &&
											typeof window[`dataMember${contextData.id}`][key].links != "undefined" &&
											typeof window[`dataMember${contextData.id}`][key].links[data.connectType == "members" ? "memberOf" : data.connectType] != "undefined" &&
											typeof window[`dataMember${contextData.id}`][key].links[data.connectType == "members" ? "memberOf" : data.connectType][contextData.id] != "undefined" &&
											typeof window[`dataMember${contextData.id}`][key].links[data.connectType == "members" ? "memberOf" : data.connectType][contextData.id].roles != "undefined"){
												let indiceDataMember = window[`dataMember${contextData.id}`][key].links[data.connectType == "members" ? "memberOf" : data.connectType][contextData.id].roles.indexOf(data.column.name);
												if(indiceDataMember != -1){
													window[`dataMember${contextData.id}`][key].links[data.connectType == "members" ? "memberOf" : data.connectType][contextData.id].roles.splice(indice, 1);
												}
												// window[`dataMember${contextData.id}`][key].links[data.connectType == "members" ? "memberOf" : data.connectType][contextData.id].roles.push(data.column.label);
											}
									}
								})
							}
						})
					}
				}else if(contextData.type == "projects"){
					if(typeof window[`dataMember${contextData.id}`] != "undefined"){
						$.map(window[`dataMember${contextData.id}`], function(value, key) {
							if(typeof value.links != "undefined" && typeof value.links.projects != "undefined"){
								$.map(value.links.projects, function(members, keyMembers) {
									if(keyMembers == contextData.id && typeof members.roles != "undefined" && typeof members.roles == 'object' && Array.isArray(members.roles)){
										let indice = members.roles.indexOf(data.column.name);
										if(indice != -1){
											members.roles.splice(indice, 1);
											if(typeof contextData.links != "undefined" && 
											typeof contextData.links[data.connectType] != "undefined" &&
											typeof contextData.links[data.connectType][key] != "undefined" &&
											typeof contextData.links[data.connectType][key].roles != "undefined"){
												contextData.links[data.connectType][key].roles.splice(contextData.links[data.connectType][key].roles.indexOf(data.column.name), 1);
											}
										}
										if(typeof window[`dataMember${contextData.id}`] != "undefined" &&
											typeof window[`dataMember${contextData.id}`][key] != "undefined" &&
											typeof window[`dataMember${contextData.id}`][key].links != "undefined" &&
											typeof window[`dataMember${contextData.id}`][key].links[data.connectType == "members" ? "memberOf" : data.connectType] != "undefined" &&
											typeof window[`dataMember${contextData.id}`][key].links[data.connectType == "members" ? "memberOf" : data.connectType][contextData.id] != "undefined" &&
											typeof window[`dataMember${contextData.id}`][key].links[data.connectType == "members" ? "memberOf" : data.connectType][contextData.id].roles != "undefined"){
												let indiceDataMember = window[`dataMember${contextData.id}`][key].links[data.connectType == "members" ? "memberOf" : data.connectType][contextData.id].roles.indexOf(data.column.name);
												if(indiceDataMember != -1){
													window[`dataMember${contextData.id}`][key].links[data.connectType == "members" ? "memberOf" : data.connectType][contextData.id].roles.splice(indice, 1);
												}
												// window[`dataMember${contextData.id}`][key].links[data.connectType == "members" ? "memberOf" : data.connectType][contextData.id].roles.push(data.column.label);
											}
									}
								})
							}
						})
					}
				}else if(contextData.type == "events"){
					if(typeof window[`dataMember${contextData.id}`] != "undefined"){
						$.map(window[`dataMember${contextData.id}`], function(value, key) {
							if(typeof value.links != "undefined" && typeof value.links.events != "undefined"){
								$.map(value.links.events, function(members, keyMembers) {
									if(keyMembers == contextData.id && typeof members.roles != "undefined" && typeof members.roles == 'object' && Array.isArray(members.roles)){
										let indice = members.roles.indexOf(data.column.name);
										if(indice != -1){
											members.roles.splice(indice, 1);
											if(typeof contextData.links != "undefined" && 
											typeof contextData.links[data.connectType] != "undefined" &&
											typeof contextData.links[data.connectType][key] != "undefined" &&
											typeof contextData.links[data.connectType][key].roles != "undefined"){
												contextData.links[data.connectType][key].roles.splice(contextData.links[data.connectType][key].roles.indexOf(data.column.name), 1);
											}
										}
										if(typeof window[`dataMember${contextData.id}`] != "undefined" &&
											typeof window[`dataMember${contextData.id}`][key] != "undefined" &&
											typeof window[`dataMember${contextData.id}`][key].links != "undefined" &&
											typeof window[`dataMember${contextData.id}`][key].links[data.connectType == "members" ? "memberOf" : data.connectType] != "undefined" &&
											typeof window[`dataMember${contextData.id}`][key].links[data.connectType == "members" ? "memberOf" : data.connectType][contextData.id] != "undefined" &&
											typeof window[`dataMember${contextData.id}`][key].links[data.connectType == "members" ? "memberOf" : data.connectType][contextData.id].roles != "undefined"){
												let indiceDataMember = window[`dataMember${contextData.id}`][key].links[data.connectType == "members" ? "memberOf" : data.connectType][contextData.id].roles.indexOf(data.column.name);
												if(indiceDataMember != -1){
													window[`dataMember${contextData.id}`][key].links[data.connectType == "members" ? "memberOf" : data.connectType][contextData.id].roles.splice(indice, 1);
												}
												// window[`dataMember${contextData.id}`][key].links[data.connectType == "members" ? "memberOf" : data.connectType][contextData.id].roles.push(data.column.label);
											}
									}
								})
							}
						})
					}
				}
				var indexToDelete = window[`kanbanDom${contextData.id}`].find(`[data-column=${data.column.header}] .kanban-list-card-detail`).index($(`div.kanban-list-card-detail[data-id=${data.column.id}]`));
				window[`kanbanDom${contextData.id}`].kanban('deleteData', { column: data.column.header, id:data.column.id,  index: indexToDelete });
				$('#kanban-wrapper-'+data.column.id).addClass('hidden')
				$('#kanban-wrapper-'+data.column.id).removeClass('visible')
                                        
				pageProfil.views.directory();
			}).on("add_role", function(data){
				let connectType = contextData.type == "organizations" ? "memberOf" : (contextData.type == "projects" ? "projects" : "events");
				if(typeof window[`kanbanDom${contextData.id}`] != "undefined"){
					data.newRoles.forEach(element => {
						let roles = $('div[data-column="listRole"] .kanban-list-card-title');

						let exists = roles.filter(function() {
							return $(this).text().trim() == element;
						});
						let columnId = "Column"+Date.now();
						if(exists.length == 0){
							var copyColumn = $.extend({}, {
								"id" : columnId,
								"label": element,
							});
							copyColumn.editable = false;
							copyColumn.menus = [];
							window[`kanbanDom${contextData.id}`].kanban('addColumn', {dom: $(".js-add-column").prev(".kanban-list-wrapper"), column: copyColumn});
							let newRole = {
								_id : columnId,
								id : columnId,
								instanceIdentity : columnId,
								title : "<b>"+element+"</b>",
								name : element,
								header : "listRole",
								canEditHeader: false,
								html : true,
								canMoveCard: false,
								actions : [
									{icon: 'fa fa-trash', action: 'onRemoveRole'},
									{icon: 'fa fa-user',badge: 1, action: '' }
								]
							}
							window[`kanbanDom${contextData.id}`].kanban('addData', newRole);
						}else{
							columnId = exists.closest('.kanban-list-card-detail').attr('data-id');
							let nb = parseInt($('div[data-id='+columnId+'] .kanban-footer-card .nbpers').text()) + 1;
							let htm = `${nb} <span class="fa fa-user"> </span>`;
							$('div[data-id='+columnId+'] .kanban-footer-card .nbpers').html(htm);
						}
						if(typeof contextData.links != "undefined" && 
							typeof contextData.links[connectType] != "undefined" &&
							typeof contextData.links[connectType][data.memberid] != "undefined" &&
							typeof contextData.links[connectType][data.memberid].roles != "undefined"){
								if(contextData.links[connectType][data.memberid].roles.indexOf(element) == -1)
									contextData.links[connectType][data.memberid].roles.push(element);
							}
							else
								contextData.links[connectType][data.memberid]={"roles": [element]};
						
						if($(`.kanban-list-wrapper.roles-list[data-column="${columnId}"]`).find(`.kanban-list-card-detail[data-id='${data.memberid}${columnId}']`).length == 0){
							let mb = {
								_id : data.memberid,
								id : data.memberid,
								instanceIdentity : data.memberid,
								title : '<b>'+window[`dataMember${contextData.id}`][data.memberid].name+'</b>',
								name : window[`dataMember${contextData.id}`][data.memberid].name,
								header : columnId,
								collection : window[`dataMember${contextData.id}`][data.memberid].collection,
								html : true,
								canMoveCard: false,
								actions : [
									{
										'icon': 'fa fa-trash', 
										'bstooltip' : { 
											'text' : 'Enlever cette role du membre',
											'position' : 'left'
										}, 
										'action': 'onDelete'
									}
								]
							}
							window[`kanbanDom${contextData.id}`].kanban('addData', mb);
						}
						
						if(typeof data.oldRoles != "undefined" && $.inArray(element, data.oldRoles) >= 1)
							data.oldRoles.splice(data.oldRoles.indexOf(element) , 1);
						
					});
					if(typeof data.oldRoles != "undefined"){
						Object.values(data.oldRoles).forEach(element => {

							if(typeof contextData.links != "undefined" && 
							typeof contextData.links[connectType] != "undefined" &&
							typeof contextData.links[connectType][data.memberid] != "undefined" &&
							typeof contextData.links[connectType][data.memberid].roles != "undefined"){
								let indice = contextData.links[connectType][data.memberid].roles.indexOf(element);
								if(indice != -1){
									contextData.links[connectType][data.memberid].roles.splice(indice, 1);
								}
							}
							if(typeof window[`dataMember${contextData.id}`] != "undefined" &&
								typeof window[`dataMember${contextData.id}`][data.memberid] != "undefined" &&
								typeof window[`dataMember${contextData.id}`][data.memberid].links != "undefined" &&
								typeof window[`dataMember${contextData.id}`][data.memberid].links[connectType] != "undefined" &&
								typeof window[`dataMember${contextData.id}`][data.memberid].links[connectType][contextData.id] != "undefined" &&
								typeof window[`dataMember${contextData.id}`][data.memberid].links[connectType][contextData.id].roles != "undefined"){
									let indiceDataMember = window[`dataMember${contextData.id}`][data.memberid].links[connectType][contextData.id].roles.indexOf(element);
									if(indiceDataMember != -1){
										window[`dataMember${contextData.id}`][data.memberid].links[connectType][contextData.id].roles.splice(indiceDataMember, 1);
									}
									// window[`dataMember${contextData.id}`][data.memberid].links[connectType][contextData.id].roles.push(data.column.label);
								}
							let roles = $('div[data-column="listRole"] .kanban-list-card-title');
							let exists = roles.filter(function() {
								return $(this).text().trim() == element;
							});
							let columnId = exists.closest('.kanban-list-card-detail').attr('data-id');
							let nb = parseInt($('div[data-id='+columnId+'] .kanban-footer-card .nbpers').text()) - 1;
							let htm = `${nb} <span class="fa fa-user"> </span>`;
							$('div[data-id='+columnId+'] .kanban-footer-card .nbpers').html(htm);
							var indexToDelete = window[`kanbanDom${contextData.id}`].find(`[data-column=${columnId}] .kanban-list-card-detail`).index($(`div.kanban-list-card-detail[data-id=${data.memberid+columnId}]`));
							window[`kanbanDom${contextData.id}`].kanban('deleteData', { column: columnId, id:data.memberid+columnId,  index: indexToDelete });
				
							if(nb == 0){
								$('div[data-id = '+columnId+']').addClass('hidden')
								$('#kanban-wrapper-'+columnId).addClass('hidden')
							}
						});
					}
				}
			});
        }
    },
    emitEvent: function(wsCO, nameEvent, data) {
        if (coWsConfig.enable) {
			wsCO.emit("directory_event", {
				name: nameEvent,
				id: contextData.id,
				data: data
			})
		}
    },
    events: {
        invitedMe: function(){
			var containInvitation = "";
			var copyContextData = JSON.parse(JSON.stringify(contextData));
			copyContextData.id = contextData._id.$id;
			if(typeof copyContextData.costum != "undefined")
				delete copyContextData.costum;
			if(typeof copyContextData.oceco != "undefined")
				delete copyContextData.oceco;
			if(typeof copyContextData.contacts != "undefined")
				delete copyContextData.contacts;
			if(typeof copyContextData.links != "undefined")
				delete copyContextData.links;
			containInvitation += `<div id="containInvitation" class="animated bounceInRight "></div>`;
			var param = {
				element : copyContextData
			}
            // $("#social-header").append(containInvitation);
			ajaxPost(
				null,
				baseUrl + '/co2/element/getinvitedme',
				param,
				function(data){ 
					$("#social-header .animated.bounceInRight").html(data);
				}
			);
        },
		communityVariable: function(data, _id) {
			
			let community = {
				connectType : "members",
				links : {
					"members" : {}
				}
			};
			
			if(
				typeof userConnected.roles != "undefined" && typeof userConnected.roles.superAdmin != "undefined" && userConnected.roles.superAdmin
			){
				community.edit = true;
			}else if(contextData.type == "organizations"){
				if(typeof userConnected.links.memberOf[contextData.id] != "undefined"
					&& typeof userConnected.links.memberOf[contextData.id].isAdmin != "undefined"
					&& userConnected.links.memberOf[contextData.id].isAdmin
					&& (typeof userConnected.links.memberOf[contextData.id].isAdminPending == "undefined" || typeof userConnected.links.memberOf[contextData.id].isInviting == "undefined")
				){
					community.edit = true;
				}
			}else if(contextData.type == "projects"){
				if(typeof userConnected.links.projects[contextData.id] != "undefined"
					&& typeof userConnected.links.projects[contextData.id].isAdmin != "undefined"
					&& userConnected.links.projects[contextData.id].isAdmin
					&& (typeof userConnected.links.projects[contextData.id].isAdminPending == "undefined" || typeof userConnected.links.projects[contextData.id].isInviting == "undefined")
				){
					community.edit = true;
				}
			}else if(contextData.type == "events"){
				if(typeof userConnected.links.events[contextData.id] != "undefined"
					&& typeof userConnected.links.events[contextData.id].isAdmin != "undefined"
					&& userConnected.links.events[contextData.id].isAdmin
					&& (typeof userConnected.links.events[contextData.id].isAdminPending == "undefined" || typeof userConnected.links.events[contextData.id].isInviting == "undefined")
				){
					community.edit = true;
				}
			}
			

			community.links["members"][_id] = {
				type: data.collection,
				date : data.modified
			}

			return community;
		},
		updateNomberElements: function(datas, type) {
			if(contextData.type == "organizations" || contextData.type == "projects"){
				let aElement = document.querySelector(`a[data-type-dir="${contextData.type == "organizations" ? "members" : "contributors"}"]`);
				let badgeElement = aElement.querySelector('.badge');
				let newNbbadge = type == "add" ?  Object.keys(datas).length + parseInt(badgeElement.textContent) : parseInt(badgeElement.textContent) - 1;
				badgeElement.textContent = newNbbadge;
			}
			let spanElement = document.querySelector('.count-nbr');
			let nbnewdata =  type == "add" ? Object.keys(datas).length + parseInt(spanElement.textContent) : parseInt(spanElement.textContent) - 1;
			spanElement.textContent = nbnewdata;
		}
    }
};