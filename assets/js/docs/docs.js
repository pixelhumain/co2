function initDocJs(faIcon, title){
    setTitle(" La doc : <span class='text-red'><i class='fa fa-"+faIcon+"'></i> "+title+"</span>", "binoculars",title);

    $(".carousel-control").click(function(){
        var top = $("#docCarousel").position().top-30;
        $(".my-main-container").animate({ scrollTop: top, }, 100 );
    });

    $(".btn-carousel-previous").click(function(){ //toastr.success('success!'); mylog.log("CAROUSEL CLICK");
        var top = $("#docCarousel").position().top-30;
        $(".my-main-container").animate({ scrollTop: top, }, 100 );
        setTimeout(function(){ $(".carousel-control.left").click(); }, 500);
    });

    $(".btn-carousel-next").click(function(){ //toastr.success('success!'); mylog.log("CAROUSEL CLICK");
        var top = $("#docCarousel").position().top-30;
        $(".my-main-container").animate({ scrollTop: top, }, 100 );
        setTimeout(function(){ $(".carousel-control.right").click(); }, 500);
    });
}


var documentation = {
    addLinkToDocsPage : false,
    openDoc : function() {
        $('#openModal').modal("hide");
        urlCtrl.loadByHash("#documentation");
    },
    getBuildPoiDoc : function (poiId,container,linkToDoc) {
        mylog.log("documentation","getBuildPoiDoc",poiId,container);
        if(linkToDoc)
        documentation.addLinkToDocsPage = true;
        if(!notEmpty(container) )
        container = '#container-docs';

        var url = baseUrl+'/co2/element/get/type/poi/id/'+poiId+"/edit/true";
        //alert(url);
        ajaxPost(null ,url, null,function(data){
            // $('#container-docs').removeClass('col-xs-9').removeClass('col-xs-12');
            // $('#menu-left').removeClass('hide');
            descHtml = documentation.renderDocHTML(data);
            $(container).html(descHtml);
        });
    },
    renderDocHTML : function(data){
        mylog.log("documentation","renderDocHTML",data.map); 

        // if(typeof data.map.titleColor == "undefined")
        //     data.map.titleColor = "#00A177";
        // descHtml = "";//"<h1 style='color:"+data.map.titleColor+"'>"+data.map.name+"</h1>";
        // if (typeof data.map.color != "undefined") {
        //     $("#"+data.map.name).css("color",data.map.color);
        // } 
        descHtml = "";
        if(documentation.addLinkToDocsPage)
            descHtml = "<h4><a onclick='documentation.openDoc()' href='javascript:;'>DOCUMENTATION</a></h4>"+descHtml;

        //descHtml += dataHelper.markdownToHtml(data.map.description); 

        mylog.log("doooc", data.map.documents);

        if(data.map.documents){
            mylog.log("building","poi doc associated documents",data.map.documents);
            descHtml += "<br/><h4>Documents</h4>";
            $.each(data.map.documents, function(k,d) { 
                if(d.doctype=="file"){
                    var dicon = "fa-file";
                    var fileType = d.name.split(".")[1];
                    mylog.log("documents",d);
                    if( fileType == "png" || fileType == "jpg" || fileType == "jpeg" || fileType == "gif" )
                    dicon = "fa-file-image-o";
                    else if( fileType == "pdf" )
                    dicon = "fa-file-pdf-o";
                    else if( fileType == "xls" || fileType == "xlsx" || fileType == "csv" )
                    dicon = "fa-file-excel-o";
                    else if( fileType == "doc" || fileType == "docx" || fileType == "odt" || fileType == "ods" || fileType == "odp" )
                    dicon = "fa-file-text-o";
                    else if( fileType == "ppt" || fileType == "pptx" )
                    dicon = "fa-file-text-o";
                    dicon = "fa-file";
                    descHtml += "<a href='"+d.path+"' target='_blanck'><i class='text-red fa "+dicon+"'></i> "+d.name+"</a><br/>" 
                }
            });
        }

        if (data.map.files) {
            mylog.log("building","cms doc associated documents",data.map.links);

            $.each(data.map.files, function(k,d) { 
                mylog.log("buildingDoc",d);
                if(d.doctype=="file"){
                    var dicon = "fa-file";
                    var fileType = d.name.split(".")[1];
                    mylog.log("documents",d);
                    if( fileType == "png" || fileType == "jpg" || fileType == "jpeg" || fileType == "gif" )
                    dicon = "fa-file-image-o";
                    else if( fileType == "pdf" )
                    dicon = "fa-file-pdf-o";
                    else if( fileType == "xls" || fileType == "xlsx" || fileType == "csv" )
                    dicon = "fa-file-excel-o";
                    else if( fileType == "doc" || fileType == "docx" || fileType == "odt" || fileType == "ods" || fileType == "odp" )
                    dicon = "fa-file-text-o";
                    else if( fileType == "ppt" || fileType == "pptx" )
                    dicon = "fa-file-text-o";
                    //dicon = "fa-file";
                    //descHtml += "<a href='"+d.docPath+"' target='_blanck'><i class='text-red fa "+dicon+"'></i> "+d.name+"</a><br/>";

                    /********* Modif by nicoss ****************/

                    descHtml += 
                      '<div class="job-box d-md-flex align-items-center justify-content-between mb-30">'+
                        '<a href="'+d.docPath+'" target="_blanck">'+
                          '<div class="job-left my-4 d-md-flex align-items-center flex-wrap">'+
                              '<div class="img-holder mr-md-4 mb-md-0 mb-4 mx-auto mx-md-0 d-lg-flex"> <i class="fa '+dicon+'"></i>'+
                              '</div>'+
                              '<div class="job-content">'+
                                  '<h5 class="text-center text-md-left">'+d.name+'</h5>'+
                              '</div>'+
                          '</div>'+
                        '</a>'+
                          '<div class="job-right my-4 flex-shrink-0">'+
                              '<a href="'+d.docPath+'" class="btn w-100 d-sm-inline-block btn-default" download> <i class="fa fa-download"></i> Telecharger</a>'+
                          '</div>'+
                      '</div>';

                }
            });
        }

        descHtml += "<a href='javascript:;' class='editThisDoc btn btn-xs btn-primary icon-btn margin-left-10' data-id='"+data.map["_id"]["$id"]+"' data-type='"+data.map.collection+"' > <span class='btn-glyphicon fa fa-edit img-circle text-primary' aria-hidden='true'></span> Modifier </a>";
        descHtml += "<a href='javascript:;' class='deleteThisDoc btn btn-xs icon-btn btn-danger margin-left-10' data-id='"+data.map["_id"]["$id"]+"' data-type='"+data.map.collection+"' > <span class='btn-glyphicon fa fa-trash img-circle text-danger' aria-hidden='true'></span> Supprimer </a>";
        return descHtml;
    }
}