dynForm = {
    jsonSchema : {
	    title : tradBadge.assignBadge,
	    icon : " bookmark-o",
	    type : "object",
	    onLoads : {
	    	onload : function(data){
    		 	dyFInputs.setSub("bg-azure");
    		}
	    },
        beforeSave : function(data,callB){
            $("#ajaxFormModal #expiredOn").val( moment(   $("#ajaxFormModal #expiredOn").val(), "DD/MM/YYYY HH:mm").format() );
        },
        afterSave : function(data,callB){
            if(location.href == baseUrl+'/#badge') {
                dyFObj.commonAfterSave(data, callB)
            } else {
                dyFObj.closeForm()
            }
        },
        afterBuild: function(data){
            setTimeout(() => {
                $("#ajaxFormModal #expiredOn").datetimepicker('setOptions', {minDate: 0});
            },500);
        },
        save: function(formData) {
            mylog.log("assign badge", formData);
            if(!formData.badgeId){
                toastr.error(tradBadge.assignFailedMessage);
                return;
            }
            getAjax(null, baseUrl + '/' + moduleId + '/badges/get-criteria/badge/' + formData.badgeId , (data) => {
            var confirmDialog = bootbox.confirm({title: tradBadge.criteriaConfirmTitle, message: dataHelper.markdownToHtml(data.criteria), 
                callback: (res) => {
                    if(res){
                        var afterSave = null;
                        if( typeof dyFObj[dyFObj.activeElem].dynForm.jsonSchema.afterSave == "function") 
                            afterSave = dyFObj[dyFObj.activeElem].dynForm.jsonSchema.afterSave;
                        $.ajax({
                            type:"POST",
                            url: baseUrl + '/co2/badges/assign',
                            data: formData,
                            success:function(data) {
                                if(data.result){
                                    toastr.success(tradBadge.assignSuccessMessage);
                                    window.assign = true;
                                    $(`#entity_badges_${formData.badgeId} .already-assigned-text`).css("display","inline");
                                    $(`#entity_badges_${formData.badgeId} .btn-assign-container`).css("display","none");
                                    $('#ajax-modal').modal("hide");
                                    $('#modal-finder').modal('hide')
                                    // if(afterSave){
                                    //     mylog.log("after save", afterSave);
                                    //     afterSave();
                                    // }
                                }else{
                                    toastr.error(tradBadge.assignFailedMessage);
                                    $("#btn-submit-form").html(tradDynForm.submit + ' <i class="fa fa-arrow-circle-right"></i>').prop("disabled",false).one(function() { 
                                        $( settings.formId ).submit();	        	
                                    });
                                }
                            },
                            error:function (xhr, ajaxOptions, thrownError){
                                toastr.error("Erreur lors de l'assignation des badges");
                                $("#btn-submit-form").html(tradDynForm.submit + ' <i class="fa fa-arrow-circle-right"></i>').prop("disabled",false).one(function() { 
                                    $( settings.formId ).submit();	        	
                                });
                            }
                        })
                    }else{
                        $("#btn-submit-form").html(tradDynForm.submit + ' <i class="fa fa-arrow-circle-right"></i>').prop("disabled",false).one(function() { 
                            $( settings.formId ).submit();	        	
                        });
                    }
                }})
                confirmDialog.on('shown.bs.modal',  () => {
                    $(".modal-dialog .modal-footer .bootbox-accept").attr("disabled", "true");
                    const div = document.createElement("div");
                    div.setAttribute("style","text-align: start;")
                    const input = document.createElement("input")
                    input.setAttribute("style","margin-right:10px;")
                    input.setAttribute("type", "checkbox")
                    const label = document.createElement("label");
                    label.innerText = "J'assume que les elements suivants suivent les critères de ce badge";
                    div.appendChild(input);
                    div.appendChild(label);
                    $(input).on('click',function() {
                        if(input.checked) {
                            $(".modal-dialog .modal-footer .bootbox-accept").attr("disabled", null);
                        }else{
                            $(".modal-dialog .modal-footer .bootbox-accept").attr("disabled", "true");
                        }
                    });
                    window.input = input;
                    $('.modal-dialog .modal-footer').prepend(div);
                });
            });
        },
	    properties : {
            award : {
                inputType : "finder",
                label : tradBadge.addAwards,
                multiple: true,
                initMe:true,
                placeholder:tradBadge.findAwards,
                rules : { required : true}, 
                initType: ["citoyens","organizations", "projects"],
                initBySearch: true,
                openSearch :true,
                initContacts: false,
                filters: {
                    'email' : {'$exists' : true}
                }
            },
            narative : dyFInputs.textarea(tradBadge.narative, tradBadge.explainMainNarative,null,true),
            evidences: {
                inputType: "lists",
					label: tradBadge.evidences,
					entries:{
						url:{
							type:"text",
                            placeholder: tradBadge.explainEvidence,
							label : tradBadge.evidence + " {num}",
							class:"col-md-12"
						},
						narative:{
                            label: tradBadge.narative + " {num}",
                            placeholder: tradBadge.explainEvidenceNarative,
							type: "text",
							class: "col-md-12"
						}
					}
            },
            // expiredOn: dyFInputs.dateInput("datetime", tradBadge.expirationDate,tradBadge.explainExpirationDate),
            badgeId: dyFInputs.inputHidden()
	    }
	}
};
