dynForm = {
    jsonSchema : {
    title : tradBadge.addBadge,
	    icon : " bookmark-o",
	    type : "object",
	    onLoads : {
	    	onload : function(data){
    		 	dyFInputs.setSub("bg-azure");
                 $("#ajaxFormModal .structagstags, #ajaxFormModal .colorcolorpicker, #ajaxFormModal .publiccheckboxSimple").hide();
                if(data.criteria && data.criteria.narrative){
                    $("#ajaxFormModal #criteria").val(data.criteria.narrative)
                }
                if(!(costum && costum.slug && costum.slug == 'ctenat')){
                    const div = document.createElement("div");
                    
                    div.setAttribute("id", "badge-creator-container");
                    div.style.display = "none";
                    div.style.height = "100vh";
                    div.style.width = "100vw";
                    div.style.top = "0";
                    div.style.left = "0";
                    div.style.position = "fixed";
                    div.style.backgroundColor = "white";
                    div.style.zIndex = 20;
                    $("#ajax-modal .modal-content").prepend(div);
                    
                    const button = document.createElement("button");
                    button.innerText = tradBadge.customizeBadge;
                    button.classList.add("btn", "btn-primary");
                    button.addEventListener('click', (e) => {
                        e.stopPropagation();
                        e.preventDefault();
                        div.style.display = "block";
                        coInterface.showLoader("#badge-creator-container");
                        getAjax("#badge-creator-container", baseUrl + '/co2/badges/badge-creator', null, 'html');
                    })
                    $("#ajaxFormModal #imageUploader .qq-upload-button-selector").before(button);
                }else{
                    $("#ajaxFormModal .criteriatextarea, #ajaxFormModal .isParcourscheckboxSimple").hide()
                }
                
    		}
	    },
        beforeBuild : function(){
            //alert("before Build orga");
            dyFObj.setMongoId('badges', function(){
                //uploadObj.gotoUrl = '/co2/badges';
            });
        },

        beforeSave : function(data,callB){
            if(!notEmpty($("#ajaxFormModal #tags").val())){
                strTags=$("#ajaxFormModal #name").val();
                if(typeof finder.object.parent != "undefined" && Object.keys(finder.object.parent).length > 0){
                    $.each(finder.object.parent, function(e,v){
                        strTags+=","+v.name;
                    });
                }
                $("#ajaxFormModal #tags").val(strTags);
            }
            if(!(costum && costum.slug && costum.slug == 'ctenat')){
                $('#ajaxFormModal input[type="hidden"][name^="criteria"]').val($('#ajaxFormModal #criteria').val())
            }
        },
        afterSave : function(data,callB){
            if(location.href == baseUrl+'/#badge') {
                dyFObj.commonAfterSave(data, callB)
            } else {
                dyFObj.closeForm()
            }
            //window.location.reload();
           /* if( $(uploadObj.domTarget).fineUploader('getUploads').length > 0 ){
                $(uploadObj.domTarget).fineUploader('uploadStoredFiles');   
            }
            */
            
        },
	    properties : (() => {
            
            let properties = {
            name : dyFInputs.name("badge"),
            parent : {
                inputType : "finder",
                label : tradBadge.addBadgeParent,
                //multiple : true,
                initMe:false,
                placeholder:tradBadge.findExistingBadge,
                rules : { required : false}, 
                initType: ["badges"],
                initBySearch: true,
                openSearch :true
            },
	    }
        if(!(costum && costum.slug && costum.slug == 'ctenat')){
            properties["issuer"] = {
                inputType : "finder",
                label : tradBadge.addIssuer,
                multiple : false,
                initMe:true,
                placeholder:tradBadge.findExistingIssuer,
                rules : { required : true, lengthMin:[1]}, 
                initType: ["citoyens","organizations", "projects"],
                initBySearch: true,
                openSearch :true,
                initContacts: false,
                filters: {
                    'email' : {'$exists' : true}
                }
            };
            properties["isParcours"] = dyFInputs.checkboxSimple("false", "isParcours", 
            {"onText" : trad.yes,
             "offText": trad.no,
             "onLabel" : tradDynForm.yes,
             "offLabel": tradDynForm.no,
             "labelText": "Badge Parcours ?",
             "labelInformation": tradBadge.explainBadgeParcours
            });
            properties["tags"] = dyFInputs.tags();
            properties["icon"] =  dyFInputs.inputText("font awesome icon","fa-user, fa-pencil, fa-clock...");
            
            properties["criteria[narrative]"] = dyFInputs.inputHidden("...");
            properties["preferences[publicFields]"] = dyFInputs.inputHidden([]);
			properties["preferences[privateFields]"] = dyFInputs.inputHidden([]);
			properties["preferences[isOpenData]"] = dyFInputs.inputHidden(true);
            properties["preferences[isOpenEdition]"] = dyFInputs.inputHidden(true);
            properties["img"] = {...dyFInputs.image(), itemLimit : 1, "rules" : {
                "required" : true
            },}
            properties["criteria"] = dyFInputs.textarea("Criteria", tradBadge.exampleCriteria,{required: true},true);
        }
        if(!properties.tags){
            properties["tags"] = dyFInputs.tags();
        }
        if(!properties.icon){
            properties["icon"] =  dyFInputs.inputText("font awesome icon","fa-user, fa-pencil, fa-clock...");
        }
        if(!properties.img){
            properties["img"] = dyFInputs.image()
        }
        properties = {...properties,             
            public : dyFInputs.checkboxSimple("true", "public", 
            	{"onText" : trad.yes,
            	 "offText": trad.no,
            	 "onLabel" : tradDynForm.public,
            	 "offLabel": tradDynForm.private,
            	 "labelText": tradBadge.makeBadgeVisible+" ?",
            	 //"labelInInput": "Activer les amendements",
            	 "labelInformation": tradBadge.explainVisibleBadge
            }),
            color : dyFInputs.colorpicker("Color", "hexa or web colors allowed"),
            description : dyFInputs.textarea(tradDynForm.longDescription, tradBadge.exampleDescription,null,true),
            structags :dyFInputs.tags(null,"Structurer le contenu","option : Structure et Hierarchie (parent ou parent.enfant)","fa-user, fa-pencil, fa-clock...")
        }
        return properties;
    })()   
	}
};
