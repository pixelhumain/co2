dynForm = {
    jsonSchema : {
	    title : "cms contruction",
	    in : "file-o",
	    type : "object",
	    onLoads : {
	    	//pour creer un subevnt depuis un event existant
	    	sub : function(){
	    	},
	    	onload : function(data){
	    		dataHelper.activateMarkdown("#ajaxFormModal #description");
	    	}
	    },
	    beforeSave : function(){
			if( typeof $("#ajaxFormModal #description").code === 'function' )  
	    		$("#ajaxFormModal #description").val( $("#ajaxFormModal #description").code() );
	    },
	    beforeBuild : function(){
	    	dyFObj.setMongoId('cms',function(){
	    		uploadObj.gotoUrl
	    	});
	    },
		afterSave : function(data, callB){
			mylog.log("afterSave cms", data, typeof callB);
			dyFObj.commonAfterSave(data, callB);
		},
	    canSubmitIf : function () { 
	    	 return ( $("#ajaxFormModal #type").val() ) ? true : false ;
	    },
	    properties : {
	    	info : {
                inputType : "custom",
                html:"<p class='text-azure'>"+
                		"<br/>Gérez votre contenu vous meme"+
                		"<hr>"+
					 "</p>"
            },
            type : dyFInputs.inputHidden(),
	        name : dyFInputs.name("cms", {}),
	        parent : {
	            inputType : "finder",
	            label : tradDynForm.whoiscarrypoint,
	           	multiple : true,
	           	rules : { lengthMin:[1, "parent"]}, 
    			initType: ["organizations", "projects", "events"],
    			openSearch :true
	        },
	        structags : {
                inputType : "tags",
                placeholder : "Structurer le contenu",
                values : null,
                label : "Structure et Hierarchie (parent ou parent.enfant)"
            },
            documentation : {
                inputType : "uploader",
                label : "Document associé (5Mb max)",
                showUploadBtn : false,
                docType : "file",
                itemLimit : 5,
                contentKey : "file",
                domElement : "documentationFile",
                placeholder : "Le pdf",
                afterUploadComplete : null,
                template : "qq-template-manual-trigger",
                filetypes : [
                    "pdf","xls","xlsx","doc","docx","ppt","pptx","odt","ods","odp", "csv","png","jpg","jpeg","gif","eps"
                    ]
            },
	        description : dyFInputs.textarea(tradDynForm.longDescription, "...",null,true)
        }
	}
};