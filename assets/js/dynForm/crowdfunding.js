dynForm = {
		    jsonSchema : {
			    title : trad.addCrowdfunding,
			    icon : "money",
			    type : "object",
			    onLoads : {
			    	onload : function(){
			    		dyFInputs.setHeader("bg-nightblue");
			    		$("#ajaxFormModal #type").val("campaign");
		    	   	},
			    	"sub" : function(){
			    		dyFInputs.setSub("bg-nightblue");
			    	}
			    },
			    beforeBuild : function(){
			    	dyFObj.setMongoId('crowdfunding', function(){
			    		uploadObj.gotoUrl = (contextData != null && contextData.type && contextData.id ) ? "preview="+contextData.type+"."+contextData.id : location.hash;
			    	});
			    },
			    afterSave : function(data,callB){
			    	dyFObj.commonAfterSave(data, callB);
			    },
			    beforeSave : function(){
			    	if( typeof $("#ajaxFormModal #description").code === 'function' ) 
			    		$("#ajaxFormModal #description").val( $("#ajaxFormModal #description").code() );
			    },
			    properties : {
			    	info : {
		                inputType : "custom",
		                html:"<p class='text-nightblue'>"+tradDynForm["infocreatecampaign"]+"<hr>" +
							  "</p>",
		            },
			        name : dyFInputs.name(),
			        image : dyFInputs.image(),
			        similarLink : dyFInputs.similarLink,
		            parent : {
			            inputType : "finder",
			            label : tradDynForm.objectofcampaign,
			           	multiple : false,
			           	rules : { required : true, lengthMin:[1, "parent"]}, 
            			initType: ["organizations", "projects"],
            			openSearch :true
			        },
			        directDonation : dyFInputs.checkboxSimple("false", "directDonation", 
            										 {"onText" : trad.yes,
            										  "offText": trad.no,
            										  "onLabel" : "Promesses et dons directs",
            										  "offLabel": "Promesses uniquement",
            										  "labelText": "Activer les dons directs ?",
            										  //"labelInInput": "Activer les amendements",
            										 // "labelInformation": tradDynForm.explainvisibleproject
            		}), 
		            description : dyFInputs.textarea(tradDynForm["description"], "...",{},true),
		            startDate : dyFInputs.startDateInput("datetime","Début de la campagne"),
		            endDate : dyFInputs.endDateInput("datetime","Fin de la campagne"),
		            goal : dyFInputs.text("Objectif de la campagne","Montant visé",{required : true}),
		            type : dyFInputs.inputHidden([]),
		            donationMean : dyFInputs.radio("Moyen de paiement que vous choisissez",{"iban" : { icon:"usd", lbl:"Virement bancaire" },"donationPlatform":{ icon:"money", lbl:"Plateforme de collecte de dons" }}),
		            iban :  dyFInputs.text("IBAN (si vous souhaitez recevoir les dons par virement)","Vérifiez l'exactitude de vos informations bancaires"),
		            donationPlatform :  dyFInputs.text("Lien internet (si vous souhaitez recevoir les dons sur votre plate-forme de collecte de dons)","Renseignez l'url en commençant par http(s)://"),
		            urls : dyFInputs.urls
		            //collection : dyFInputs.inputHidden("crowdfunding")
			    }
			}
		};