
dynForm = {
		    jsonSchema : {
			    title : "Promesse de don / don",
			    icon : "money",
			    type : "object",
			    onLoads : {
			    	onload : function(){
                        mylog.log("dyFObj",dyFObj);
                        mylog.log("this donation",$(this));
			    		dyFInputs.setHeader("bg-blue");
                        
			    		var today = new Date();
						var dd = String(today.getDate()).padStart(2, '0');
						var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
						var yyyy = today.getFullYear();
						var s = today.getSeconds();
				    	var reference = dd+mm+yyyy+s;
				    	$("#ajaxFormModal #name").val(reference);
                        //$("#ajaxFormModal #invoice").val("Non");
				    	// if($("#ajaxFormModal #type").val()=="pledge"){
         //                    dyFCustom.setTitle("Faire une promesse de don");
				    	// 	$("#ajaxFormModal #donationPlatform").hide();
         //                    $("#ajaxFormModal #iban").hide();

				    	// }
         //                else{
         //                    dyFCustom.setTitle("Faire un don");
         //                }
		    	   	},
			    	sub : function(){
			    		dyFInputs.setSub("bg-blue");
			    	}
			    },
			    beforeBuild : function(){
			    	dyFObj.setMongoId('crowdfunding', function(){
			    		uploadObj.gotoUrl = (contextData != null && contextData.type && contextData.id ) ? "#page.type."+contextData.type+".id."+contextData.id+".view.detail" : location.hash;
			    	});
			    	

			    },
			    afterSave : function(data,callB){
			    	dyFObj.commonAfterSave(data, callB);
                    $(".explain-modal").modal("hide");
			    },
			    beforeSave : function(){
			    	if( typeof $("#ajaxFormModal #description").code === 'function' ) 
			    		$("#ajaxFormModal #description").val( $("#ajaxFormModal #description").code() );
			    },
			    properties : {
	            	info : {
		                inputType : "custom",
		                html:"<p class='text-blue'>Participez au co-financement d'un projet<hr>" +
							  "</p>",
		            },
		            similarLink : dyFInputs.similarLink,
		            //name : dyFInputs.text("Référence",""),
                    civility : {
                    	inputType : "select",
                    	label : "Civilité",
                    	placeholder : "Choisir ...",
                        options : ["Monsieur","Madame","Autre"],
                    	groupOptions : false,
                        groupSelected : false,
                    	optionsValueAsKey:true,
                        select2 : {
                        	multiple : false
                        },
                        rules: {
                            required:true
                        }
                    },
                    surname : {
                        inputType : "text",
                        label : "Nom de famille",
                        placeholder : "Nom de famille",
                        rules: {
                            required:true
                        }
                    },
                    donatorName : {
                        inputType : "text",
                        label : "Prénom",
                        placeholder : "Prénom",
                        rules: {
                            required:true
                        }
                    },
                    email : {
                        label : "Email",
                        placeholder : "Email",
                        rules: {
                            required:true,
                            email: true
                        }
                    },
                    telephone : {
                        inputType : "text",
                        label : "Téléphone",
                        placeholder : "Téléphone"
                    },
                    amount : {
                        inputType: "text",
                        label : "Montant de votre promesse de don, en Euros",
                        placeholder : "Montant en Euros",
                        rules: {
                            "required":true,
                            "number" : true
                        }
                    },
                    invoice : {
                    	inputType : "checkboxSimple",
                    	label: "Souhaitez-vous une facture ?",
                        params : {
                            "onText" : "Oui",
                            "offText" : "Non",
                            "onLabel" : "Je souhaite une facture",
                            "offLabel" : "Je ne souhaite pas de facture",
                            "labelInformation" : ""
                        },
                        checked : false
                    },
                    receiver : {
                        inputType : "finder",
                        id : "receiver",
                        label : "A quel commun s'adresse la promesse de don",
                        initType : [
                            "projects"
                        ],
                        initMe : false,
                        rules : {
                            required : true,
                            lengthMin : [ 
                            	1, 
                                "parent"
                            ]
                        }  
                    },
                    parent : {
                        inputType : "finder",
                        id : "receiver",
                        label : "Campagne financée",
                        initType : [
                            "crowdfunding"
                        ],
                        initMe : false,
                        rules : {
                            "required" : true,
                            "lengthMin" : [ 
                            	1, 
                                "parent"
                            ]
                        }  
                    },
                    type: {
                    	inputType : "hidden"
                    },
                    name : {
                    	inputType : "hidden"
                    }
                    // donationPlatform : {
                    //     label : "Plateforme de don",
                    //     inputType : "iframe"
                    // },
                    // iban : {
                    //     label : "IBAN pour effectuer votre virement",
                    //     inputType : "text"
                    // }

                    

	            }



			    // properties : {
			    // 	info : {
		     //            inputType : "custom",
		     //            html:"<p class='text-blue'>Participez au co-financement d'un projet<hr>" +
							//   "</p>",
		     //        },
		     //        similarLink : dyFInputs.similarLink,
			    //     civility : dyFInputs.inputSelect("Civilité","Choisir ...",["M.","Mme"],{required :true}),
			        
		     //        parent : {
			    //         inputType : "finder",
			    //         label : tradDynForm.objectofcampaign,
			    //        	multiple : false,
			    //        	rules : { required : true, lengthMin:[1, "parent"]}, 
       //      			initType: ["organizations", "projects"],
       //      			openSearch :true
			    //     },
		     //        shortDescription : dyFInputs.textarea(tradDynForm["shortDescription"], "...",{ maxlength: 140 }),
		     //        startDate : dyFInputs.dateInput("datetime","Début de la campagne"),
		     //        endDate : dyFInputs.dateInput("datetime","Fin de la campagne"),
		     //        goal : dyFInputs.text("Objectif de la campagne","Montant visé"),
		     //        type : dyFInputs.inputHidden([])
			    // }
			}
		};