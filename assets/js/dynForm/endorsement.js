const filters = { 'email': { '$exists': true } }
filters['links.members.' + userId + '.isAdmin'] = true;
filters['links.members.' + userId + '.isAdminPending'] = { $exists: false };
dynForm = {
    jsonSchema: {
        title: "Endosser un element",
        icon: " bookmark-o",
        type: "object",
        onLoads: {
            onload: function(data) {
                dyFInputs.setSub("bg-azure");
                if (!data.claimElementId) {
                    toastr.error("Erreur claimElementId not set")
                    error = true;
                }
                if (!data.claimElementType) {
                    toastr.error("Erreur claimElementType not set")
                    error = true;
                }
            },
        },
        beforeBuild: function() {
            dyFObj.setMongoId('endorsements', function() {});
        },
        afterSave: function(data, callB) {
            mylog.log("AFTER SAVE ENDORSEMENT", data)
            if (data.map.claimBadge) {
                dyFObj.commonAfterSave(data, () => {
                    getAjax("#endosser-content", baseUrl + '/co2/badges/endorsement-modal/badge/' + $("#modal-endosser").data("badge") + '/id/' + $("#modal-endosser").data("elementId") + '/type/' + $("#modal-endosser").data("elementType"), null, 'html');
                    dyFObj.closeForm();
                });
            } else {
                dyFObj.commonAfterSave(data, callB);
            }
        },
        properties: {
            endorsementComment: dyFInputs.textarea(tradBadge.whyEndorse, "...", { required: true }, true),
            issuer: {
                inputType: "finder",
                label: tradBadge.endorseBy,
                multiple: false,
                initMe: true,
                rules: { required: true, lengthMin: [1] },
                initType: ["citoyens", "organizations", "projects"],
                initBySearch: true,
                openSearch: true,
                initContacts: false,
                filters
            },
            claimElementId: dyFInputs.inputHidden(),
            claimElementType: dyFInputs.inputHidden(),
            claimBadge: dyFInputs.inputHidden(),
        }
    }
};