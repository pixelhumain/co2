dynForm = {
    jsonSchema : {
	    title : tradBadge.addBadgeFromExternal,
	    icon : " bookmark-o",
	    type : "object",
	    onLoads : {
	    	//pour creer un subevnt depuis un event existant
	    	sub : function(){
	    		dyFInputs.setSub("bg-azure");
	    	},
	    	onload : function(data){
	    		dyFInputs.setSub("bg-azure");
	    		$(".jsontextarea, .urltext, .imageuploader, .typetext, #btn-submit-form").hide();
	    	},
	    },
	    beforeSave : function(){
	    	if( typeof $("#ajaxFormModal #description").code === 'function' )  
	    		$("#ajaxFormModal #description").val( $("#ajaxFormModal #description").code() );
			//RULES 
			// (dyFObj[dyFObj.activeElem].dynForm.jsonSchema.properties)
	    },
	    beforeBuild : function(){
	    	dyFObj.setMongoId('badges',function(){
	    	});
	    },
		afterSave : function(data, callB){
			dyFObj.commonAfterSave(data, callB);
	    },
		save: function(formData) {
			var files = $("#ajaxFormModal .imageuploader #imageUploader").fineUploader('getUploads',{status: "submitted"});
			var image = null;
			var type = formData["type"];
			if(files.length == 1){
				image =  $("#ajaxFormModal .imageuploader #imageUploader").fineUploader('getFile', files[0].id);
			}
			
			var possibleTypes = ["url", "json", "image"];
			var form_data = new FormData();
			for ( var key in formData ) {
				if(key == type || possibleTypes.indexOf(key) < 0){
					form_data.append(key, formData[key]);
				}
			}
			if(type == 'image' && image != null){
				form_data.append('image', image)
			}
			var contextDataId= (typeof contextData!="undefined" && notNull(contextData)) ? contextData.id : costum.contextId;
			var contextDataCollection= (typeof contextData!="undefined"&& notNull(contextData)) ? contextData.collection : costum.contextType ;

			form_data.append('contextId', contextDataId);
			form_data.append('contextCollection', contextDataCollection);
			$.ajax({
				url         : baseUrl + '/' + moduleId + "/badges/validator",
				data        : form_data,
				processData : false,
				contentType : false,
				type: 'POST'
			}).done(function(data){
				if(data.result){
					toastr.success(tradBadge.assignSuccessMessage);
					urlCtrl.loadByHash(location.hash);
				}else{
					if(data.msg == "No valid email on the element which receive the badge"){
						toastr.error(tradBadge.noValidEmail);
					}else{
						toastr.error(tradBadge.cantAssignToElement);
					}
					$("#btn-submit-form").html(tradDynForm.submit +' <i class="fa fa-arrow-circle-right"></i>').prop("disabled",false).one(function() { 
						$( settings.formId ).submit();	        	
					});
				}
			});
		},
	    canSubmitIf : function () { 
	    	 return ( $("#ajaxFormModal #section").val() ) ? true : false ;
	    },
	    actions : {
	    	clear : function() {
	    		$("#ajaxFormModal #type").val("");
	    		$("#ajaxFormModal #section").val("");
	    		$(".breadcrumbcustom").html("");
	    		$(".sectionBtntagList").show(); 
	    		$(".jsontextarea, .urltext, .imageuploader, #btn-submit-form").hide();
				$("#ajaxFormModal .imageuploader #imageUploader").fineUploader('reset');
				$("#ajaxFormModal #url").rules("add", {required: false})
				$("#ajaxFormModal #json").rules("add", {required: false})
				dyFObj[dyFObj.activeElem].dynForm.jsonSchema.properties.image.rules.required = false;
			}
	    },
	    properties : {
            breadcrumb : {
                inputType : "custom",
                html:"",
            },
            sectionBtn :{
                label : tradBadge.chooseAddMode,
	            inputType : "tagList",
                placeholder : tradBadge.chooseAddMode,
                list : {
                    url: {label: 'Url', key: 'url', icon: 'link'},
                    image: {label: 'Image', key: 'image', icon: 'upload'},
                    json: {label: 'JSON', key: 'json', icon: 'file-text-o'},
                },
                trad : tradCategory,
                init : function(){
                	$(".sectionBtn").off().on("click",function()
	            	{
	            		$(".typeBtntagList").show();
	            		$(".sectionBtn").removeClass("active btn-dark-blue text-white");
	            		$( "."+$(this).data('key')+"Btn" ).toggleClass("active btn-dark-blue text-white");
						
						$(".breadcrumbcustom").html( "<h4><a href='javascript:;'' class='btn btn-xs btn-danger'  onclick='dyFObj.elementObj.dynForm.jsonSchema.actions.clear()'>"+
							"<i class='fa fa-times'></i></a> <span>"
							+$(this).data('tag')+"</span></h4>" );
						
						$(".sectionBtntagList").hide();
                        switch($(this).data('key')){
                            case 'url':
                                $(".urltext").show();
								$("#ajaxFormModal #url").rules("add", {required: true})
							break;
							case 'json':
								$(".jsontextarea").show();
								$("#ajaxFormModal #json").rules("add", {required: true})
                            break;
                            case 'image':
								$(".imageuploader").show();
								dyFObj[dyFObj.activeElem].dynForm.jsonSchema.properties.image.rules.required = true;
                            break;
                        }
						$("#ajaxFormModal #type").val($(this).data('key'));
						dyFObj.canSubmitIf();
	            	});
	            }
            },
			type: dyFInputs.text(),
	        image : {
				...dyFInputs.image(), 
				filetypes:['svg', 'png'],
				itemLimit : 1,
				rules: {
					required: false,
				}
			},
            json : dyFInputs.textarea("JSON", "...",{
				required: false,
			},false),
            url : dyFInputs.text('url', 'https://example.org/assertion.json',{
				required: false,
			}),
	    }
	}
};