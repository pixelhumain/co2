dynForm = {
    jsonSchema : {
	    title : tradBadge.addExistingBadge,
	    icon : " bookmark-o",
	    type : "object",
	    onLoads : {
	    	onload : function(data){
    		 	dyFInputs.setSub("bg-azure");
                $(".parentfinder").hide();
    		}
	    },
        afterSave : function(data,callB){
            // dyFObj.commonAfterSave(data, callB);
            mylog.log("afterSave PARCOURS BADGE", data);
        },
	    properties : {
            badge : {
                inputType : "finder",
                label : tradBadge.addBadge,
                multiple: false,
                initMe:false,
                placeholder:tradBadge.addBadge,
                rules : { required : true}, 
                initType: ["badges"],
                initBySearch: true,
                openSearch :true,
                initContacts: false,
                initContext: false
            }
	    }
	}
};
