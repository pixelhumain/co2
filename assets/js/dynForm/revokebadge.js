dynForm = {
    jsonSchema : {
	    title : tradBadge.revokeBadge,
	    icon : " bookmark-o",
	    type : "object",
	    onLoads : {
	    	onload : function(data){
    		 	dyFInputs.setSub("bg-azure");
    		}
	    },
        afterSave : function(data,callB){
            dyFObj.commonAfterSave(data, callB);
        },
        save: function(formData) {
            mylog.log("revoke badge", formData);
            if(!formData.badgeId){
                toastr.error(tradBadge.revokeFailedMessage);
                return;
            }
            var afterSave = null;
			if( typeof dyFObj[dyFObj.activeElem].dynForm.jsonSchema.afterSave == "function") 
				afterSave = dyFObj[dyFObj.activeElem].dynForm.jsonSchema.afterSave;
            $.ajax({
                type:"POST",
                url: baseUrl + '/co2/badges/revoke',
                data: formData,
                success:function(data) {
                    if(data.result){
                        toastr.success(tradBadge.revokeSuccessMessage);
                        window.assign = true;
                        $(`#entity_badges_${formData.badgeId} .already-assigned-text`).css("display","inline");
                        $(`#entity_badges_${formData.badgeId} .btn-assign-container`).css("display","none");
                        $('#ajax-modal').modal("hide");
                        
                        if(location.href == baseUrl+'/#page.type.badges.id.'+formData.badgeId+'.views.all'){
                            mylog.log("after save", afterSave);
                            afterSave();
                        } else {
                            dyFObj.closeForm()
                        }
                    }else{
                        toastr.error(tradBadge.revokeFailedMessage);
                        $("#btn-submit-form").html( tradDynForm.submit + ' <i class="fa fa-arrow-circle-right"></i>').prop("disabled",false).one(function() { 
                            $( settings.formId ).submit();	        	
                        });
                    }
                },
                error:function (xhr, ajaxOptions, thrownError){
                    toastr.error("Erreur lors de l'assignation des badges");
                    $("#btn-submit-form").html( tradDynForm.submit + ' <i class="fa fa-arrow-circle-right"></i>').prop("disabled",false).one(function() { 
                        $( settings.formId ).submit();	        	
                    });
                }
            })
        },
	    properties : {
            revokeReason : dyFInputs.textarea(tradBadge.reason, "...",null,true),
            badgeId: dyFInputs.inputHidden(),
            awardId: dyFInputs.inputHidden(),
            awardType: dyFInputs.inputHidden(),
	    }
	}
};
