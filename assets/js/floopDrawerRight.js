/*
	Pour info, depuis qu'on utilise le nouveau design Tango
	on utilise aussi le floopDrawerRight.js
	floopDrawer.js n'est plus utilisé
*/
const aapIcon = typeof ocecoToolsItems != "undefined" && ocecoToolsItems && ocecoToolsItems.createNewOcecoForm && ocecoToolsItems.createNewOcecoForm.logo ? 
					{ url: baseUrl+ ocecoToolsItems.createNewOcecoForm.logo, alt: "logo-oceco" } : "bullhorn";

//liste des types à afficher avec leurs icons
var floopContactTypes = [	{ name : "citoyens",  		color: "yellow"	, icon:"user"	, limit : 20, labelEmpty : trad.notyetinvitedcontacts },
							{ name : "projects", 		color: "purple"	, icon:"lightbulb-o", limit : 10,	labelEmpty : trad.notyetcontributorinitiative},
							{ name : "events", 			color: "orange"	, icon:"calendar"	, limit : 10, show : false, labelEmpty : trad.nocontact	},
							{ name : "organizations", 	color: "green" 	, icon:"group"	, limit : 10, labelEmpty : trad.havenotlinksorganizations		},
							{ name : "callforprojects", color: "vine" 	, icon:aapIcon	, limit : 10, labelEmpty : trad["You have not yet created links with any call for project"]		},
							{ name : "follows", 		color: "turq" 	, icon:"rss"		, limit : 10, labelEmpty : trad.notfollowinganyone	}
						];							
					
							
var openPanelType = { 	"citoyens" 		 : "citoyens",
						"organizations"  : "organizations",
						"projects" 	 	 : "projects",
						"events" 		 : "events",
						"follows"		 : "citoyens",
						"callforprojects": "forms",
					};

var tooltips_lbl = { 	"citoyens" 		  : trad.addsomeonetoyourdirectory,
						"organizations"   : trad.createaneworganization,
						"projects" 	 	  : trad.createanewproject,
						"projectsHistory" : trad.showhideoldprojects,
						"events" 		  : trad.createanewevent,
						"eventsHistory"   : trad.showhideoldevents,
						"callforprojects" : trad.callforprojectsorocecoform,
						"follows"		  : trad.follower
					};

var floopTypeUsed = new Array();
var floopShowLock = false;
var timeoutShowFloopDrawer = false;

var floopContacts;
var floopDrawer={
	dom : "#floopDrawerDirectory",
	options : {
		status: true,
		actions : {
			chat : true
		}
	},
	types : [	{ name : "citoyens",  		color: "yellow"	, icon:"user"	, limit : 20, show : true, add : true,labelEmpty : trad.notyetinvitedcontacts		},
		{ name : "projects", 		color: "purple"	, icon:"lightbulb-o", limit : 10, show : true, add : true,	labelEmpty : trad.notyetcontributorinitiative	},
		{ name : "events", 			color: "orange"	, icon:"calendar"	, limit : 10, show : false, add : true,labelEmpty : trad.nocontact	},
		{ name : "organizations", 	color: "green" 	, icon:"group"	, limit : 10, show : true, add : true, labelEmpty : trad.havenotlinksorganizations		},
		{ name : "callforprojects", color: "vine" 	, icon: aapIcon	, limit : 10, show : true, add : true, labelEmpty : trad["You have not yet created links with any call for project"]		},
		{ name : "follows", 		color: "turq" 	, icon:"rss"		, limit : 10, show : true, add : false, labelEmpty : trad.notfollowinganyone	}
	],
	header : {},
	results : {},
	search:{},
	init : function(pInit){
		if(typeof pInit.dom){
			this.dom=pInit.dom;
		}
		if(typeof pInit.types != "undefined")
			$.extend(this.types, pInit.types);
		if(typeof pInit.options != "undefined")
			$.extend(this.options, pInit.options);
		this.results=(notNull(myContacts)) ? myContacts : {};
	    if(typeof pInit.header != "undefined")
			$.extend(this.header, pInit.header);

		this.initViews(this);
		this.initEvents(this);
		 // initFloopScrollByType(domHtml);

	      //$("#floopDrawerDirectory").hide();
	  /*    if($(".tooltips").length) {
	        $('.tooltips').tooltip();
	    }
	    $("#btnFloopClose").click(function(){
	      	showFloopDrawer(false);
	    });
	    $(".main-col-search").mouseenter(function(){
	      	showFloopDrawer(false);
	    });*/

	    //bindEventFloopDrawer();
	
	},
	initViews : function(fObj){
		var str = this.views.container(fObj);
	    $(fObj.dom).html(str);
	},
	initEvents : function(fObj){
		fObj.events.scrollByType(fObj);

		fObj.events.showAll(fObj);
		fObj.events.search(fObj);
	    if($(".tooltips").length) {
	        $('.tooltips').tooltip();
	    }
	    if($(fObj.dom).hasClass("floopPreview")){
	    	$(fObj.dom).mouseleave(function(){ 
		        showFloopDrawer(false);
		    });
	    }
	    $(fObj.dom+" .btnFloopClose").off().on("click",function(){
	     	showFloopDrawer(false);
	    });
	    /*$(".main-col-search").mouseenter(function(){
	      	showFloopDrawer(false);
	    });*/
	    fObj.events.bindResults(fObj);
	},
	actions: {
		views : {
			chat : function(fObj, id, type, value, drop){
				chatName = value.name;
				chatSlug = (value.slug) ? value.slug : ""; 
				chatUsername = (value.username) ? value.username : "";	 

				chatColor = (value.hasRC) ? "text-red" : "";
				str = '<button class="btn btn-xs btn-default col-xs-12 btn-open-chat pull-right '+chatColor+' contact'+id+'" '+
						'data-name-el="'+value.name+'" data-username="'+chatUsername+'" data-slug="'+chatSlug+'" data-type-el="'+type.name+'"  data-open="'+( (typeof value.preferences != "undefined" && value.preferences.isOpenEdition)?true:false )+'"  data-hasRC="'+( ( value.hasRC )?true:false)+'" data-id="'+id+'">'+
					'<i class="fa fa-comments"></i> ';
					if(drop)
						str+= trad.discuss
				str+='</button>';
				return str;
			},
			disconnect : function(fObj, id, type, value, drop){
				label = (type.name=="follows") ? trad.notfollowanymore : trad.disconnect;
				str = '<button data-id="'+id+'" data-type="'+value.collection+'" data-connect="'+type.name+'" class="btnDisconnect btn btn-xs btn-default col-xs-12 text-red"><span class="fa fa-stack"></span> '+
							'<i class="fa fa-unlink"></i> ';
							if(drop)
								str+= label;
				str+='</button>';
				return str;
			},
			costum : function(fObj, id, type, value, drop){
				mylog.log("ddddd",value,value.links)
				str = "";
				if (typeof value.costum != "undefined" && notEmpty(value.costum)) {
					str += "<a class='btn btn-xs btn-default col-xs-12 ' href='"+baseUrl+"/costum/co/index/slug/"+value.slug+"' data-toggle='tooltip' data-placement='bottom' title='' target='_blank'><img src='" + assetPath + "/images/logo-costum.png' style='width: 18px;'>"+ trad.visitCostum +"</a>";
					if(typeof value.isAdmin != "undefined" && value.isAdmin){
						str += "<a class='btn btn-xs btn-default col-xs-12 ' href='"+baseUrl+"/costum/co/index/slug/"+value.slug+"/edit/true' data-toggle='tooltip' data-placement='bottom' title='' target='_blank'><img src='" + assetPath + "/images/logo-costum.png' style='width: 18px;'>"+ trad.editCostum+ "</a>";
					}	
				}
				return str;
			},
			fluxNews : function(fObj, id, type, value, drop){
				str = '<button class="seeFluxNews btn btn-xs btn-default col-xs-12 " data-type="'+value.collection+'" data-id="'+id+'">'+
							'<i class="fa fa-rss"></i> ';
							if(drop)
								str+= trad.newsfeed;
				str+='</button>';
				return str;
			},
			answer: function(fObj, id, type, value){
				var str="";
				if((typeof value.isInviting != "undefined" || typeof value.isAdminInviting != "undefined") 
					&& typeof value.invitorId != "undefined" && value.invitorId != userId){
					refuseLabel=trad.refuse;
					acceptLabel=trad.accept;
					verb="Join";
					labelAdmin="";
					option=null;
					linkValid="isInviting";
					//$msgRefuse="Are you sure to refuse ivitation";
					if(typeof value.isAdminInviting != "undefined"){
						verb="Administrate";
						option="isAdminInviting";
						labelAdmin=" to administrate";
						linkValid="isAdminInviting";
						//msgRefuse=Yii::t("common","Are you sure to refuse to administrate {what}", array("{what}"=>Yii::t("common"," this ".Element::getControlerByCollection($element["collection"]))));
					}
					//if ($element["collection"] == "citoyens"){
					//	$labelAdmin=" to become friend";
					//}

					//$labelInvitation=Yii::t("common", "{who} invited you".$labelAdmin, array("{who}"=>"<a href='#page.type.".Person::COLLECTION.".id.".$invitedMe["invitorId"]."' class='lbh'>".$invitedMe["invitorName"]."</a>"));
					//$tooltipAccept=$verb." this ".Element::getControlerByCollection($element["collection"]);
					/*if ($element["collection"] == Event::COLLECTION){
						$inviteRefuse="Not interested";
						$inviteAccept="I go";
					}*/
					str+= "<span class='col-xs-12 no-padding'>"+
						//"<div class='padding-5'>"+						$labelInvitation.": <br/>".
							'<button class="btn btn-xs tooltips btn-accept-floop" data-type="'+value.collection+'" data-id="'+id+'" data-connect="'+type.name+'" data-link="'+linkValid+'">'+
								'<i class="fa fa-check "></i> '+acceptLabel+
							'</button>'+
							'<button class="btn btn-xs tooltips btn-refuse-floop margin-left-5" data-type="'+value.collection+'" data-id="'+id+'" data-connect="'+type.name+'" data-link="'+linkValid+'" data-options="'+option+'">'+
								'<i class="fa fa-remove"></i> '+refuseLabel+
							'</button>'+
							"</span>";
						//	"</div>"+
						
				}
				return str;
			}
		},
		events:{
			chat: function(fObj){
				$(fObj.dom+" .btn-open-chat").off().on("click", function(){
			    	var nameElo = $(this).data("name-el");
			    	var idEl = $(this).data("id");
			    	var usernameEl = $(this).data("username");
			    	var slugEl = $(this).data("slug");
			    	var typeEl = dyFInputs.get($(this).data("type-el")).col;
			    	var openEl = $(this).data("open");
			    	var hasRCEl = ( $(this).data("hasRC") ) ? true : false;
			    	//alert(nameElo +" | "+typeEl +" | "+openEl +" | "+hasRCEl);
			    	var ctxData = {
			    		name : nameElo,
			    		type : typeEl,
			    		id : idEl
			    	}
			    	if(typeEl == "citoyens")
			    		ctxData.username = usernameEl;
			    	else if(slugEl)
			    		ctxData.slug = slugEl;
			    	rcObj.loadChat(nameElo ,typeEl ,openEl ,hasRCEl, ctxData );
			    });
			},
			disconnect:function(fObj){
				//if($(fObj.dom+" .btnDisconnect").length > 0){
					$(fObj.dom+" .btnDisconnect").off().on("click", function(){
						var childId = $(this).data("id");
						var childType = $(this).data("type");
						var connectType = $(this).data("connect");
						links.disconnect("citoyens",userId, childId, childType,connectType, function(){
							delete myContacts[connectType][childId];
							$(fObj.dom+" #floopItem-"+connectType+"-"+childId).fadeOut();
						});
					});
				//}
			},
			answer : function(fObj){
				$(fObj.dom+" .btn-accept-floop").off().on("click", function(){
					var typeConnect=$(this).data("link");
					var connectType=$(this).data("connect");
					var childId = $(this).data("id");
					var childType = $(this).data("type");
					links.validate(childType,childId, userId,'citoyens',typeConnect, function(parentType, parentId, childId, childType, linkOption){
						if(typeof myContacts[connectType][parentId][typeConnect]!= "undefined")
							delete myContacts[connectType][parentId][typeConnect];
						var typeFloop={};
						var typeMore=connectType;
						$.each(fObj.types, function(e,v){
							if(v.name==typeMore){
								typeFloop=v;
							}
						});	
						$(fObj.dom+" #floopItem-"+connectType+"-"+parentId).replaceWith(fObj.views.entry(fObj, parentId, typeFloop, myContacts[connectType][parentId]));
						 fObj.events.bindResults(fObj);
					});
				});
				$(fObj.dom+" .btn-refuse-floop").off().on("click", function(){
					var connectType=$(this).data("connect");
					var options=$(this).data("options");
					var childId = $(this).data("id");
					var childType = $(this).data("type");
					links.disconnect("citoyens",userId, childId, childType,connectType,function(){
							delete myContacts[connectType][childId];
							$(fObj.dom+" #floopItem-"+connectType+"-"+childId).fadeOut();
						},option);
				});
			},
			fluxNews : function(fObj){
				$(fObj.dom+" .seeFluxNews").off().on("click", function(){
					//if(typeof loadMyStream != "undefined")
						newsObj.loadLive('news/co/index/type/'+$(this).data("type")+'/id/'+$(this).data("id")+"/nbCol/1/inline/true");
				});
			}
		}
	},
	views : {
		container : function(fObj){
			mylog.log("check fObj", fObj);
			var HTML = 			'<div class="floopHeader bg-white">'+
										'<i class="fa fa-connect-develop pull-left text-dark" style="margin-right:15px;margin-top:4px;"></i> '+
									'<div id="floopScrollByType" class="pull-left">';
										$.each(fObj.types, function(e, value){
											if(value.show) {
												var iconHtml= "";
												if(typeof value.icon != "undefined" && typeof value.icon == "object" && value.icon.url)
													iconHtml = `<i class="move-center"><img width="23" height="20" style="filter: brightness(50);" src="${ value.icon.url }" alt="${ value.icon.alt }" /></i>`;
												else 
													iconHtml = `<i class='fa fa-${value.icon}'></i>`;
				HTML += 					"<a href='javascript:' id='btn-scroll-type-"+value.name+"' class='bg-"+value.color+" btn-scroll-type'>"+iconHtml+"</a>";
											}
										});
				HTML +=					'</div>' ;
									if(notEmpty(fObj.header)){
										if(typeof fObj.header.close != "undefined"){
				HTML +=						'<button class="btnFloopClose"><i class="fa fa-times"></i></button>';
										}
									}									
				HTML +=				'</div>';
				HTML += 		'<div class="input-group search-floop col-xs-12">'+
									/*'<span class="input-group-addon bg-green text-white">'+
										'<span class="fa fa-search">'+
										'</span>'+
									'</span>'+*/	
									'<input type="text" id="search-contact" style="width:100%" class="form-control" aria-label="'+trad.searchnamepostalcity+'" placeholder="'+trad.searchnamepostalcity+'">'+
								'</div>';
				//HTML += 		'<div class="col-xs-3"><i class="fa fa-search" style="padding:15px 0px 15px 11px;"></i></div><div class="col-xs-9"><input type="text" id="search-contact" class="form-control" placeholder="'+trad.searchnamepostalcity+'"></div>';
				HTML += 		'<div class="floopScroll co-scroll col-xs-12 no-padding">' ;
									
								$.each(fObj.types, function(key, type){
									if(typeof type.show =="undefined" || type.show){
									//var n=0;
									//compte le nombre d'élément à afficher
									var numberOftype= fObj.results[type.name] ? Object.keys(fObj.results[type.name]).length : 0;
									//$.each(contacts[type.name], function(key2, value){ n++; });
									//si aucun élément, on affiche pas cette section
									//if(n > 0){
									var urlBtnAdd = "";
									var classBtnAdd=""
									if(type.add){
								
										//alert(type.name);

										if(type.name == "citoyens") {	 urlBtnAdd = "#element.invite";
											classBtnAdd="lbhp";
									}	
										if(type.name == "organizations") urlBtnAdd = "javascript:dyFObj.openForm( 'organization');";
										if(type.name == "events") 		 urlBtnAdd = "javascript:dyFObj.openForm( 'event');";
										if(type.name == "projects") 	 urlBtnAdd = "javascript:dyFObj.openForm( 'project');";
										if(type.name == "follows"); 		// urlBtnAdd = "dyFObj.openForm( 'person')";
										if(type.name == "callforprojects") urlBtnAdd = `javascript:$('.btn-ocecotools[data-id=createNewOcecoForm]').trigger('click');`;
									}
									//floopTypeUsed.push(type);

									var nameType = trad['my'+type.name];
									var iconHtml = "";
									if(typeof type.label != "undefined" && type.label != null)
										nameType = type.label;
									if(typeof type.icon != "undefined" && typeof type.icon == "object" && type.icon.url) {
										iconHtml = `<i class=""><img width="25" height="20" style="filter: sepia(100%) hue-rotate(0deg);" src="${ type.icon.url }" alt="${ type.icon.alt }" /></i>`;
											
									} else {
										iconHtml = '<i class="fa fa-'+type.icon+'"></i>'
									}

				HTML += 			'<div class="panel panel-default" id="scroll-type-'+type.name+'">  '+	
										'<div class="panel-heading">';
				HTML += 					'<h4 class="text-'+type.color+'">';
												if(type.add){
				HTML += 								'<a href="'+urlBtnAdd+'" class="tooltips '+classBtnAdd+' btn btn-default btn-sm pull-right btn_shortcut_add bg-'+type.color+'" data-placement="left" data-original-title="'+tooltips_lbl[type.name]+'">'+
													'<i class="fa fa-plus"></i>'+
												'</a>';
												}	
				HTML += 								iconHtml+
												'<span class="">'+ nameType +"</span>"+
												'<span class="badge text-white bg-'+type.color+'">'+numberOftype+'</span>';
				HTML += 					'</h4>'+
										'</div>'+
										'<div class="panel-body no-padding">'+
											'<div class="list-group no-padding">'+
												'<ul id="floopType-'+type.name+'" class="floop-type-list">';
												if(numberOftype == 0){
				HTML += 							'<label class="no-element col-xs-12"><i class="fa fa-angle-right"></i> '+type.labelEmpty+'</label>';
													if(type.add){
				HTML += 									'<a href="'+urlBtnAdd+'" class="'+classBtnAdd+' btn button-noLinks btn-default col-xs-12 btn_shortcut_add bg-'+type.color+'" data-placement="left" data-original-title="'+tooltips_lbl[type.name]+'">'+
														'<i class="fa fa-plus"></i> '+tooltips_lbl[type.name]+
													'</a>';		
													}							
												}else{
													inc=1;
													$.each(fObj.results[type.name], function(key2, value){ 
														if(inc<type.limit)
															//return false;
					HTML += 							fObj.views.entry(fObj, key2, type, value);
														inc++;
													});		
													if(numberOftype > type.limit){
														HTML+='<button class="btn-show-all btn-show-more-'+type.name+'" data-type="'+type.name+'">'+
															'<i class="fa fa-angle-down"></i> '+trad.seeall+
														'</button>';
													}
												}	
				HTML += 						'</ul>' +	
											'</div>'+	
										'</div>'+
									'</div>';
									}
								});									
				HTML += 		'</div>' +
								'</div>'+
							  '</div>' +
							  '</div>';
				return HTML;

		},
		entry : function(fObj,id, type, value){
				var oldElement = isOldElement(value);

				var cp = (typeof value.address != "undefined" && notNull(value.address) && typeof value.address.postalCode != "undefined") ? value.address.postalCode : typeof value.cp != "undefined" ? value.cp : "";
				var city = (typeof value.address != "undefined" && notNull(value.address) && typeof value.address.addressLocality != "undefined") ? value.address.addressLocality : "";
				var defaultImg=type.name;
				var typefollow = trad[((typeof value.type != "undefined" && notNull(value.type) ) ? value.type : "").slice(0, -1)];//(typeof value.type != "undefined" && notNull(value.type) ) ? value.type : "";
				//if(defaultImg=="people")
				//	defaultImg="citoyens";
				//typefollow = typefollow.slice(0, -1);
				var profilThumbImageUrl = (typeof value.profilThumbImageUrl != "undefined" && value.profilThumbImageUrl != "") ? baseUrl + value.profilThumbImageUrl : parentModuleUrl + "/images/thumb/default_"+defaultImg+".png";
				var id = (typeof value._id != "undefined" && typeof value._id.$id != "undefined") ? value._id.$id : id;
				var path = "#page.type."+openPanelType[type.name]+".id."+id;
				var elementClass = oldElement ? "oldFloopDrawer"+type.name : "";
				var elementStyle = oldElement ? "display:none" : ""; 

				var HTML = '<li style="'+elementStyle+'" class="'+elementClass+' floopEntries col-xs-12 no-padding" id="floopItem-'+type.name+'-'+id+'" idcontact="'+id+'">' +
								'<div class="btn-chk-contact btn-scroll-type">' +
					'<img src="' + profilThumbImageUrl+'" data-src="' + profilThumbImageUrl + '" class="thumb-send-to bg-' + type.color +' lzy_img" height="35" width="35">'+
										'<span class="info-contact btn-select-contact elipsis">' +
											'<a href="'+path+'" class="lbh-preview-element contact'+id+'">' +
										
											'<span class="name-contact text-dark text-bold" idcontact="'+id+'">' + value.name + '</span>'+
											'</a>';
							
											if(typeof fObj.options.status != "undefined" && fObj.options.status)
					HTML+=						fObj.views.status(fObj, value);	
											if(typeof fObj.options.answer != "undefined" && fObj.options.answer)
					HTML+=						fObj.actions.views.answer(fObj,id, type, value);							
											if(type.name == "follows") {
					HTML +=						'<span class="type-follow col-xs-12 no-padding pull-left text-'+type.color+'">'+typefollow+'&nbsp;</span>';
											};
											if(notEmpty(cp) || notEmpty(city)){

					HTML +=					'<div class="col-xs-12 no-padding">'+ 
												'<span class="city-contact text-light pull-left elipsis" idcontact="'+id+'">' + cp + ' '+ city +'</span>'+
											'</div>';
											}
					HTML +=				'</span>'+
									'</div>' ;
						
					// 	if (typeof value.costum != "undefined" && notEmpty(value.costum)) {
					// HTML += "<a class='link-to-costum-floop-drawer' href='"+baseUrl+"/costum/co/index/slug/"+value.slug+"' data-toggle='tooltip' data-placement='bottom' title='"+trad.visitCostum+"' target='_blank'><img src='" + assetPath + "/images/logo-costum.png' style='width: 18px;'></a>";
					// 	}

						if(typeof fObj.options.actions != "undefined"){
							var countActions=Object.keys(fObj.options.actions).length;
							var dropdownActions=(countActions> 1) ? true : false;
							if(dropdownActions){
					HTML+= 		'<div class="dropdown pull-right dropdown-settings-floop"><a href="#" data-toggle="dropdown" class="btn bg-white dropdown-toggle pull-right margin-right-5">'+
									'<i class="fa fa-ellipsis-v"></i>'+
								'</a>'+
								'<ul class="dropdown-menu pull-right dropdown-dark arrow_box" role="menu">';
							}
							mylog.log("eeeee",fObj.options.actions)
							$.each(fObj.options.actions, function(e, v){
					HTML+=		fObj.actions.views[e](fObj, id,type, value, dropdownActions);
							});		
									
							if(dropdownActions){
					HTML+=				'</ul></div>';
							}
						}
				return HTML+'</li>';

		},
		status : function(fObj,value){
			var str="";
			if(typeof value.isInviting != "undefined") 
				str+='<span class="floop-statu col-xs-12 no-padding">'+trad.waitanswer+'</span>';
			if(typeof value.toBeValidated != "undefined") 
				str+='<span class="floop-statu col-xs-12 no-padding">'+trad.waitanswer+'</span>';
			else if(typeof value.isAdminPending != "undefined") 
				str+='<span class="floop-statu col-xs-12 no-padding">'+trad.awaitadminresponse+'</span>';
			else if(typeof value.isAdmin != "undefined")
				str+='<span class="floop-statu col-xs-12 no-padding">'+trad.administrator+'</span>';
			return str;
		}
	},
	events : {
		scrollByType:function(fObj){
			//parcourt tous les types de contacts
			$.each(fObj.types, function(key, type){ 
				//initialise le scoll automatique de la liste de contact
				$(fObj.dom+" .floopHeader #btn-scroll-type-"+type.name).mouseover(function(){
					var scrollTOP = $(fObj.dom+' .floopScroll').scrollTop() + 
										$(fObj.dom+" .floopScroll #scroll-type-"+type.name).position().top;
					$(fObj.dom+' .floopScroll').scrollTop(scrollTOP);
				});
			});
		},
		search : function(fObj){
			$(fObj.dom+" #search-contact").off().on("keyup",delay(function (e) {
					if(e.keyCode != 13)
						fObj.filter.search(fObj, $(this).val());
						//fObj.filters.actions.text.launch(fObj, $(this).data("field"));
						
				}, 750)).on("keyup", function(e){
					if(e.keyCode == 13)
						fObj.filter.search(fObj, $(this).val());
						//fObj.filters.actions.text.launch(fObj, $(this).data("field"));
					
				});
			//on("keyup", function(){

//				delay(function (e) {
    		//		fObj.filter.search(fObj, $(this).val());
//				}, 300);
	//		});
		},
		showAll : function(fObj){
			$(fObj.dom+" .btn-show-all").off().on("click", function(){
				var typeFloop={};
				var typeMore=$(this).data("type");
				$.each(fObj.types, function(e,v){
					if(v.name==typeMore){
						typeFloop=v;
					}
				});	
				fObj.render.append(fObj,typeFloop, fObj.results[$(this).data("type")]);
				$(this).remove();
				
			});
		},
		bindResults : function(fObj){
			coInterface.bindLBHLinks();
			if(typeof fObj.options.answer != "undefined")
			 	 fObj.actions.events.answer(fObj);
			if(typeof fObj.options.actions != "undefined" ){
			 	$.each(fObj.options.actions, function(e,v){
					if(typeof fObj.actions.events[e] == "function")
			 			fObj.actions.events[e](fObj);
			 	});
			}
		}
	},
	filter: {
		search: function(fObj, search){
			//fObj.filter.value=search;
			if(search != ""){
				$.each(fObj.types, function(e,v){
					if(v.show){
						//$(fObj.dom+" #floopType-"+v.name+" .floopEntries").hide();
						var res={};
						Object.entries(fObj.results[v.name]).forEach(([key, array]) => {
							if(array.name.search(new RegExp(search, "i"))>=0){
								res[key]=array;
							}else if(typeof array.address != "undefined" && typeof array.address.addressLocality != "undefined" &&
								array.address.addressLocality.search(new RegExp(search, "i"))>=0)
									res[key]=array;
							//else 
							//	return false;
						});
						$(fObj.dom+" #floopType-"+v.name+" .floopEntries, "+fObj.dom+" .btn-show-all").hide();
						fObj.render.append(fObj, v, res);

					}
				});
			}else{
				$(fObj.dom+" .floopEntries").show();
				$(fObj.dom+" .btn-show-all").show();
			}
		},
		regExp: function(array){
			if(array.name==this.filter.value){
				return true;
			}else if(typeof array.address != "undefined" && typeof array.address.addressLocality != "undefined" &&
				array.address.addressLocality == this.filter.value)
				return true;
			else
				return false;
		}
	},
	render : {
		append: function(fObj, type, results){
			var str="";
			//inc=1;
			$.each(results, function(key, value){ 
				if($(fObj.dom+ " #floopItem-"+type.name+"-"+key).length <= 0)
					str += fObj.views.entry(fObj, key, type, value);
				else if(!$(fObj.dom+ " #floopItem-"+type.name+"-"+key).is(":visible"))
					$(fObj.dom+ " #floopItem-"+type.name+"-"+key).show();
			//	inc++;
			});	
			if(str!=""){
				if($(fObj.dom+" .btn-show-all[data-type='"+type.name+"']").length >= 0)
					$(fObj.dom+" #floopType-"+type.name+" .btn-show-all").before(str);
				else
					$(fObj.dom+" #floopType-"+type.name).append(str);
			}
			fObj.events.bindResults(fObj);
		}
	}


};
function initFloopDrawer(domHtml, options){
	//if(typeof options.floopMap != "undefined")
	//	$.extend(floopContactsType, options.floopMap);
	domHtml = (!notNull(domHtml))?"#floopDrawerDirectory":domHtml;
	//console.log("nicoss",myContacts);
	if( typeof costum != "undefined" && 
		costum != null && 
		typeof costum.slug != "undefined" && 
		typeof costum[costum.slug] != "undefined" && 
                    typeof costum[costum.slug].floopContactTypes != "undefined" && 
                    costum[costum.slug].floopContactTypes != null ){
		floopContactTypes = costum[costum.slug].floopContactTypes;
	}
                     
	if(myContacts != null){
      var floopDrawerHtml = buildListContactHtml(myContacts, options);
      $(domHtml).html(floopDrawerHtml);
      initFloopScrollByType(domHtml);

      //$("#floopDrawerDirectory").hide();
      if($(".tooltips").length) {
        $('.tooltips').tooltip();
      }
     /* $("#btnFloopClose").click(function(){
      	showFloopDrawer(false);
      });
      $(".main-col-search").mouseenter(function(){
      	showFloopDrawer(false);
      });*/

      bindEventFloopDrawer();
    }
}
function getFloopContacts(){ return floopContacts; }

function buildListContactHtml(contacts, options){

	floopContacts = contacts;
	floopTypeUsed  = new Array();
	
	var HTML = 			'<div class="floopHeader bg-white">'+
							//'<a href="#person.directory.id.'+userId+'?tpl=directory2" '+
							//	'class="text-white pull-left lbh" style="color:white !important;">'+
								//t("My directory")+
								'<i class="fa fa-link pull-left text-dark" style="margin-right:15px;margin-top:4px;"></i> '+
							//'</a>'+
							'<div id="floopScrollByType" class="pull-left"></div>' +
							'<button id="btnFloopClose"><i class="fa fa-times"></i></button>' +
							
						'</div>';
		HTML += 		'<div class="input-group search-floop">'+
							'<span class="input-group-addon bg-green text-white">'+
								'<span class="fa fa-search">'+
								'</span>'+
							'</span>'+	
							'<input type="text" id="search-contact" class="form-control" aria-label="'+trad.searchnamepostalcity+'" placeholder="'+trad.searchnamepostalcity+'">'+
						'</div>';
		//HTML += 		'<div class="col-xs-3"><i class="fa fa-search" style="padding:15px 0px 15px 11px;"></i></div><div class="col-xs-9"><input type="text" id="search-contact" class="form-control" placeholder="'+trad.searchnamepostalcity+'"></div>';
		HTML += 		'<div class="floopScroll co-scroll col-xs-12 no-padding">' ;
							
						$.each(floopContactTypes, function(key, type){
							if(typeof type.show =="undefined" || type.show){
							//var n=0;
							//compte le nombre d'élément à afficher
							var numberOftype= contacts[type.name] ? Object.keys(contacts[type.name]).length : 0;
							//$.each(contacts[type.name], function(key2, value){ n++; });
							//si aucun élément, on affiche pas cette section
							//if(n > 0){
							var urlBtnAdd = "";
							//alert(type.name);
							if(type.name == "citoyens") 	 urlBtnAdd = "dyFObj.openForm( 'person')";
							if(type.name == "organizations") urlBtnAdd = "dyFObj.openForm( 'organization')";
							if(type.name == "events") 		 urlBtnAdd = "dyFObj.openForm( 'event')";
							if(type.name == "projects") 	 urlBtnAdd = "dyFObj.openForm( 'project')";
							if(type.name == "follows"); 		// urlBtnAdd = "dyFObj.openForm( 'person')";
							floopTypeUsed.push(type);

							var nameType = trad['my'+type.name];
							var iconHtml = "";
							if(typeof type.label != "undefined" && type.label != null)
								nameType = type.label;
							if(typeof type.icon != "undefined" && typeof type.icon == "object" && type.icon.url) {
								iconHtml = `<i class=""><img width="25" height="20" src="${ type.icon.url }" alt="${ type.icon.alt }" /></i>`;	
							} else {
								iconHtml = '<i class="fa fa-'+type.icon+'"></i>'
							}

		HTML += 			'<div class="panel panel-default" id="scroll-type-'+type.name+'">  '+	
								'<div class="panel-heading">';
		HTML += 					'<h4 class="text-'+type.color+'">'+
										'<button onclick="'+urlBtnAdd+'" class="tooltips btn btn-default btn-sm pull-right btn_shortcut_add bg-'+type.color+'" data-placement="left" data-original-title="'+tooltips_lbl[type.name]+'">'+
											'<i class="fa fa-plus"></i>'+
										'</button>' +		
										iconHtml+
										'<span class="">'+ nameType +"</span>"+
										'<span class="badge text-white bg-'+type.color+'">'+numberOftype+'</span>';
										//if(myId != ""){
		//HTML += 						'<button onclick="'+urlBtnAdd+'" class="tooltips btn btn-default btn-sm pull-right btn_shortcut_add text-'+type.color+'" data-placement="left" data-original-title="'+tooltips_lbl[type.name]+'">'+
		//									'<i class="fa fa-plus"></i>'+
		//								'</button>';
							
											/*if (type.name == "events" || type.name == "projects") {
		HTML += 						'<button onclick="showHideOldElements(\''+type.name+'\')" class="tooltips btn btn-default btn-sm pull-right btn_shortcut_add text-'+type.color+'" data-placement="left" data-original-title="'+tooltips_lbl[type.name+'History']+'">'+
											'<i class="fa fa-history"></i>'+
										'</button>';		
											}*/
										//}
		HTML += 					'</h4>'+
								'</div>'+
								'<div class="panel-body no-padding">'+
									'<div class="list-group no-padding">'+
										'<ul id="floopType-'+type.name+'">';
										if(numberOftype == 0){
		HTML += 							'<label class="no-element"><i class="fa fa-angle-right"></i> '+trad["no"+type.name]+'</label>';									
										}else{
											//limit=(type.show) ? 
											inc=1;
											$.each(contacts[type.name], function(key2, value){ 
												if(inc<type.limit)
													//return false;
			HTML += 							getFloopItem(key2, type, value);
												inc++;
											});		
											if(numberOftype > type.limit){
												HTML+='<button onclick="showTypeFloop(\''+type.name+'\', true)" class="btn-show-all btn-show-more-'+type.name+'">'+
													'<i class="fa fa-angle-down"></i> '+trad.seeall+
												'</button>';
											}
										}	
		HTML += 						'</ul>' +	
									'</div>'+	
								'</div>'+
							'</div>';
							}
						});									
		HTML += 		'</div>' +
						'</div>'+
					  '</div>' +
					  '</div>';
		
				

		return HTML;
}

function showTypeFloop(type, more){
	if(typeof type =="string"){
		var str="";
		inc=1;
		$.each(floopContacts[type], function(key2, value){ 
			//if()
			str +=getFloopItem(key2, type, value);
		});	
		$("#floopType-"+type).append(str);
		//var str=$(contacts[type.name]).size()
	}else{

	}
}
//création HTML d'un élément
function getFloopItem(id, type, value){
	var oldElement = isOldElement(value);

	var cp = (typeof value.address != "undefined" && notNull(value.address) && typeof value.address.postalCode != "undefined") ? value.address.postalCode : typeof value.cp != "undefined" ? value.cp : "";
	var city = (typeof value.address != "undefined" && notNull(value.address) && typeof value.address.addressLocality != "undefined") ? value.address.addressLocality : "";
	defaultImg=type.name;
	var typefollow = trad[((typeof value.type != "undefined" && notNull(value.type) ) ? value.type : "").slice(0, -1)];//(typeof value.type != "undefined" && notNull(value.type) ) ? value.type : "";
	//if(defaultImg=="people")
	//	defaultImg="citoyens";
	//typefollow = typefollow.slice(0, -1);
	var profilThumbImageUrl = (typeof value.profilThumbImageUrl != "undefined" && value.profilThumbImageUrl != "") ? baseUrl + value.profilThumbImageUrl : parentModuleUrl + "/images/thumb/default_"+defaultImg+".png";
	var id = (typeof value._id != "undefined" && typeof value._id.$id != "undefined") ? value._id.$id : id;
	var path = "#page.type."+openPanelType[type.name]+".id."+id;
	var elementClass = oldElement ? "oldFloopDrawer"+type.name : "";
	var elementStyle = oldElement ? "display:none" : ""; 

	var HTML = '<li style="'+elementStyle+'" class="'+elementClass+'" id="floopItem-'+type.name+'-'+id+'" idcontact="'+id+'">' +
					'<a href="'+path+'" class="btn btn-default btn-scroll-type btn-select-contact lbh-preview-element elipsis contact'+id+'">' +
						'<div class="btn-chk-contact">' +
		'<img src="' + modules.co2.url +'/images/thumbnail-default.jpg" data-src="' + profilThumbImageUrl + '" class="thumb-send-to bg-' + type.color +' lzy_img" height="35" width="35">'+
							'<span class="info-contact">' +
								'<span class="name-contact text-dark text-bold" idcontact="'+id+'">' + value.name + '</span>'+
								'<br/>';
								if(typeof value.isInviting != "undefined") {
		HTML+=						'<span class="floop-statu">En attente de réponse</span><br/>';
								}
								if(typeof value.toBeValidated != "undefined") {
		HTML+=						'<span class="floop-statu">En attente de réponse</span><br/>';
								}
								else if(typeof value.isAdminPending != "undefined") {
		HTML+=						'<span class="floop-statu">En attente de réponse pour administrer</span><br/>';
								}
								else if(typeof value.isAdmin != "undefined") {
		HTML+=						'<span class="floop-statu">Administrateur</span><br/>';
								}

								if(type.name == "follows") {
		HTML +=						'<span class="type-follow pull-left text-'+type.color+'">'+typefollow+'&nbsp;</span>';
								};
		HTML +=					'<span class="cp-contact text-light pull-left" idcontact="'+id+'">' + cp + '&nbsp;</span>'+
								'<span class="city-contact text-light pull-left" idcontact="'+id+'">' + city + '</span>'+
							'</span>'+
						'</div>' +
					'</a>';
			
			chatName = value.name;
			chatSlug = (value.slug) ? value.slug : ""; 
			chatUsername = (value.username) ? value.username : "";	 

			chatColor = (value.hasRC) ? "text-red" : "";
			HTML += '<button class="btn btn-xs btn-default btn-open-chat pull-right '+chatColor+' contact'+id+'" '+
					'data-name-el="'+value.name+'" data-username="'+chatUsername+'" data-slug="'+chatSlug+'" data-type-el="'+type.name+'"  data-open="'+( (typeof value.preferences != "undefined" && value.preferences.isOpenEdition)?true:false )+'"  data-hasRC="'+( ( value.hasRC )?true:false)+'" data-id="'+id+'">'+
				'<i class="fa fa-comments"></i>'+
			'</button>';
			/*if( value.preferences.isOpenEdition == false )
				HTML += '<i class="fa fa-lock pull-right margin-top-20 margin-right-5 text-light tooltips" '+
							'data-position="left" data-original-title="value.preferences.isOpenEdition == false"></i>';*/
	return HTML+'</li>';
}

var translation = {
		"My citoyens" 		: "Mes contacts",
		"My follows"		: "Mes abonnements",
		"My organizations" 	: "Mes organisations",
		"My projects" 		: "Mes projets",
		"My events" 		: "Mes événements",
		"My directory" 		: "Mon répertoire",
		"Search name, postal code, city ..." : "nom, code postal, ville ..."
}

function t(string){
	if(typeof translation[string] != "undefined"){
		return translation[string];
	}else { return string; }
}
function showFloopDrawer(show){ 
	//alert($(".floopDrawer").hasClass("floopPreview"));
	if($(".floopDrawer").hasClass("floopPreview")){
		if(show){
			if($(".floopDrawer.floopPreview" ).css("display") == "none"){
				$(".floopDrawer.floopPreview").stop().show();
				$(".floopDrawer.floopPreview" ).css("width", 0);
				$(".floopDrawer.floopPreview" ).animate( { width: 300 }, 300 );
			}
		}else{
			$(".floopDrawer.floopPreview").hide("fast");
		}
	}
}
function getFloopContactTypes(type){
	var goodType = null;
	$.each(floopContactTypes, function(key, value){
		if(value.name == type) {
			goodType = value;
		}
	});
	return goodType;
}

function bindEventFloopDrawer(domHtml=''){
	$(domHtml+" .floopDrawer .btn-show-all").keyup(function(){
		filterFloopDrawer($(this).val());
	});
	$(domHtml+" .floopDrawer #search-contact").keyup(function(){
		filterFloopDrawer($(this).val());
	});
/*	$(domHtml).mouseleave(function(){ 
	        showFloopDrawer(false);
	    });*/

	//parcourt tous les types de contacts
	$.each(floopContactTypes, function(key, type){ 
		//initialise le scoll automatique de la liste de contact
		$(".floopHeader #btn-scroll-type-"+type.name).mouseover(function(){

			//var width = $(this).width();
	        //$("#floopDrawerDirectory").css({left:width});
	        //showFloopDrawer(true);
	        var scrollTOP = $('.floopScroll').scrollTop() + /*90$('.floopScroll').position().top*/ 
							$(".floopScroll #scroll-type-"+type.name).position().top;
			$('.floopScroll').scrollTop(scrollTOP);
		});
	});

   /* $("#ajaxSV,#menu-top-container").mouseenter(function() { 
      if(!floopShowLock)
			showFloopDrawer(false);
    });
    $(".floopDrawer, .floopDrawer #search-contact").mouseover(function() {
      showFloopDrawer(true);
    });
    $(".menuIcon.no-floop-item, .box-add, .blockUI, .mapCanvas").mouseover(function() {
      	if(!floopShowLock)
			showFloopDrawer(false);
    });*/

    $(".btn-open-chat").click(function(){
    	var nameElo = $(this).data("name-el");
    	var idEl = $(this).data("id");
    	var usernameEl = $(this).data("username");
    	var slugEl = $(this).data("slug");
    	var typeEl = dyFInputs.get($(this).data("type-el")).col;
    	var openEl = $(this).data("open");
    	var hasRCEl = ( $(this).data("hasRC") ) ? true : false;
    	//alert(nameElo +" | "+typeEl +" | "+openEl +" | "+hasRCEl);
    	var ctxData = {
    		name : nameElo,
    		type : typeEl,
    		id : idEl
    	}
    	if(typeEl == "citoyens")
    		ctxData.username = usernameEl;
    	else if(slugEl)
    		ctxData.slug = slugEl;
    	rcObj.loadChat(nameElo ,typeEl ,openEl ,hasRCEl, ctxData );
    });

}

function initFloopScrollByType(domHtml){
	var HTML = "";
	$.each(floopTypeUsed, function(key, value){ 
		var type = value.name; //openPanelType[value.name];
		var iconHtml = "";
		if(typeof value.icon != "undefined" && typeof value.icon == "object" && value.icon.url)
			iconHtml = `<i class="move-center"><img width="23" height="20" style="filter: brightness(50);" src="${ value.icon.url }" alt="${ value.icon.alt }" /></i>`;
		else 
			iconHtml = `<i class='fa fa-${value.icon}'></i>`;
		HTML += "<a href='javascript:' id='btn-scroll-type-"+type+"' class='bg-"+value.color+" btn-scroll-type'>"+iconHtml+"</button>";
	});
	$(domHtml+" #floopScrollByType").html(HTML);
}

//recherche text par nom, cp et city
function filterFloopDrawer(searchVal){
	
	//masque/affiche tous les contacts présents dans la liste
	if(searchVal != "")	$(".floopDrawer .btn-select-contact, .floopDrawer .btn-open-chat").hide();
	else				$(".floopDrawer .btn-select-contact, .floopDrawer .btn-open-chat").show();
	//recherche la valeur recherché dans les 3 champs "name", "cp", et "city"
	$.each($(".floopDrawer .name-contact"), function() { checkFloopSearch($(this), searchVal); });
	$.each($(".floopDrawer .cp-contact"),   function() { checkFloopSearch($(this), searchVal); });
	$.each($(".floopDrawer .city-contact"), function() { checkFloopSearch($(this), searchVal); });
}

//si l'élément contient la searchVal, on l'affiche
function checkFloopSearch(thisElement, searchVal, type){
	var content = thisElement.html();
	var found = content.search(new RegExp(searchVal, "i"));
	if(found >= 0){
		var id = thisElement.attr("idcontact");
		
		$(".floopDrawer .contact"+id).show();
	}
}

//ajout d'un élément dans la liste
function addFloopEntity(entityId, entityType, entityValue, linksType){
	//Exception with citoyens collection which is managed like people in display
	//if(entityType == "citoyens") entityType = "people";
	myContacts=(!notNull(myContacts)) ? [] : myContacts ;
	if(notNull(linksType)){
		if(typeof myContacts[linksType] == "undefined"){
			myContacts[linksType]={entityId : entityValue};
		}else
			myContacts[linksType][entityId]= entityValue;
	}else{
		if(typeof myContacts[entityType] == "undefined"){
			myContacts[entityType]={entityId : entityValue};
		}else
			myContacts[entityType][entityId]= entityValue;
	}
	/*floopContacts[entityType][entityId]=entityValue;
	var type = getFloopContactTypes(entityType);
	//We check if the element is already displayed
	if($('#floopItem-'+type.name+'-'+entityId).length < 1){
		var html = getFloopItem(entityId, type, entityValue);
		$("ul#floopType-"+entityType).prepend(html);
	}
	
	$("ul#floopType-"+entityType+" .no-element").hide();
	floopShowLock = true;
    showFloopDrawer(true);

    setTimeout(function(){
    	if ($('.floopScroll').position().top != null ) {
			var scrollTOP = $('.floopScroll').scrollTop() - $('.floopScroll').position().top +
							$(".floopScroll #scroll-type-"+entityType).position().top;
			$('.floopScroll').scrollTop(scrollTOP);
		}
	}, 1000);

	timeoutShowFloopDrawer = setTimeout(function(){
		floopShowLock = false;
		showFloopDrawer(false);
		clearTimeout(timeoutShowFloopDrawer);
	}, 4000);
	//toastr.success("ajout de l'element floop ok");*/
}

//ajout d'un élément dans la liste
function removeFloopEntity(entityId, entityType, connectType){
	//if(entityType == "citoyens") entityType = "people";
	connectLink = (connectType=="follows") ? connectType : entityType; 
	if(typeof myContacts != "undefined" && 
		typeof myContacts[connectLink] != "undefined" &&
		typeof myContacts[connectLink][entityId] != "undefined")
		delete myContacts[connectLink][entityId];
	/*type = getFloopContactTypes(entityType);
	removeFromMyContacts(entityId, entityType);
	if(typeof type != "undefined" && type != null && typeof type.name != "undefined")
		$('#floopItem-'+type.name+'-'+entityId).remove();*/
}
//ajout d'un élément dans la liste
function changeNameFloopEntity(entityId, entityType, entityName){
	//if(entityType == "citoyens") entityType = "people";
	type = getFloopContactTypes(entityType);
	//removeFromMyContacts(entityId, entityType);
	$('#floopItem-'+type.name+'-'+entityId).find(".name-contact").text(entityName);
}
function removeFromMyContacts(entityId, entityType){
	if(typeof floopContacts != "undefined" && 
		typeof floopContacts[entityType] != "undefined" &&
		typeof floopContacts[entityType][entityId] != "undefined")
		delete floopContacts[entityType][entityId];
}
function showHideOldElements(type) {
	$(".oldFloopDrawer"+type).toggle("slow");
}

// Return true if the endDate of the Element is before the current Date. 
function isOldElement(value) {
	var endDate = (typeof value["endDate"] != undefined) ? value["endDate"] : "";
	if (endDate == "") return false;
	return new Date(endDate) < new Date();
}
