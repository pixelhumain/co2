var coImagePicker_defaultOptions = {
    container:null,
    allow:{
        adding:["folder", "file"],
        deleting:["folder", "file"]
    },
    onUpdate:function(type, subject){

    },
    onSelect:function(subject){

    }
}

(function($){
    $.fn.coImagePicker = function(options){
        var modal = $(`
            <div id="cnb-modal" class="modal fade" role="dialog">
                <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Galerie</h4>
                    </div>
                    <div class="modal-body">
                    </div>
                </div>
                </div>
            </div>
        `)
        var settings = $.extend(true, coImagePicker_defaultOptions, options);

    }
}(jQuery))
