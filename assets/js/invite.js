// TODO
/*

	 - ne peu t pas inviter des gens si pas co 
	 - email personne inscrit non envoyer 
	 - verb ask au lieu de invite
	- 




*/
var inviteObj = {
	container : "",
	init : function(initParams){
		inviteObj.container = initParams.container;
	},
	formInvite : function(callback){
		mylog.log("finder formInvite ", inviteObj.container, callback, inviteObj);


		$(inviteObj.container+' #btnInviteNew').off().on("click", function(e){
			mylog.log("finder btnInviteNew ", inviteObj);
			var formInvite = $(inviteObj.container);
			mylog.log("finder form ", formInvite, inviteObj);
			formInvite.submit(function(e){ mylog.log("finder submit ", e); e.preventDefault() });
			var errorHandler = $('.errorHandler', formInvite);
			mylog.log("finder errorHandler ", errorHandler);
			formInvite.validate({
				rules : {
					inviteEmail : {
						minlength : 2,
						required : true,
						email: true
					},
					inviteName : {
						minlength : 2,
						required : true,
					},
					inviteText : {
						maxlength : 500,
					}
				},
				submitHandler : function(formInvite) {

					//alert("submitHandler");
					mylog.log("finder submitHandler form", formInvite, inviteObj);
					var mail = $(inviteObj.container+' #inviteEmail').val();
					var msg = $(inviteObj.container+' #inviteText').val();
					var name = $(inviteObj.container+' #inviteName').val();
					errorHandler.hide();
					if(typeof callback != "undefined" && callback != null){

						var data = {
							id : inviteObj.keyUniqueByMail(mail),
							mail : mail,
							msg : msg,
							name : name,
							type : "citoyens"
						}
						$(inviteObj.container+' #inviteEmail').val("");
						$(inviteObj.container+' #inviteText').val("");
						$(inviteObj.container+' #inviteName').val("");
						callback(data);
					}else{
						

						if(typeof listInvite.invites[mail] == "undefined"){
							var keyUnique = keyUniqueByMail(mail);
							listInvite.invites[keyUniqueByMail(mail)] = {
								name : name,
								mail : mail,
								msg : msg
							} ;

							if(parentType != "citoyens")
								listInvite.invites[keyUnique].isAdmin = "";

							$(inviteObj.container+' #inviteEmail').val("");
							$(inviteObj.container+' #inviteText').val("");
							$(inviteObj.container+' #inviteName').val("");
							$(inviteObj.container+" #form-invite").hide();
						} else {
							toastr.error(tradDynForm.alreadyInTheList);
						}
						showElementInvite(listInvite, true);
						bindRemove();
					}
				},
				invalidHandler: function(event, validator) {
					//alert("invalidHandler");
					mylog.log("finder invalidHandler", event, validator)
					var errors = validator.numberOfInvalids();
					if (errors) {
						var message = errors == 1
						? 'You missed 1 field. It has been highlighted'
						: 'You missed ' + errors + ' fields. They have been highlighted';
						mylog.log("finder message", message);
						$("div.error span").html(message);
						$("div.error").show();
					} else {
						$("div.error").hide();
					}
				}

			});
		});
	},
	keyUniqueByMail(mail) {
		var keyUnique = "";
		for (var i=0; i < mail.length; i++) {
			keyUnique += mail.charCodeAt(i);
		}
		return keyUnique ;
	}
};
