function Cokanban(p) {
    if(notEmpty(p)){
        this.projectId = p.projectId;
        this.container = p.container;
        this.context = p.context;
        this.formId = p.formId ?? null;
        this.userId = p.userId ?? null;
    }
    this.room = null;
    this.actions = [];
    this.tags = {
        "totest" : "totest",
        "todiscuss" : "discuter",
    }
    this.cards = {
        done : [],
        totest : [],
        todiscuss : [],
        tracking : [],
        todo : [],
    }
    this.kanban = null;

    this.setRoom = function(projectId){
        var room = "";
        var url = baseUrl + '/costum/project/project/request/room';
        var post = {
            project: projectId
        };
        ajaxPost(null,url, post,
                function(data) {
                    room = data
                }, null, null, {
                    async: false
                }
            );
        this.room = room;
    }

    this.setActions = function(projectId,personal = null) {
        this.setRoom(projectId);
        var actions = [];
        var url = baseUrl + '/costum/project/action/request/actions_by_project_id';
        var post = {
            id: projectId,
        };
        if(notEmpty(personal)){
            post["personal"] = personal
        }
        ajaxPost(null,url, post,
                function(data) {
                    if(data.result){
                        actions = data.data
                    }else{
                        toastr.error(tradDynForm.explainprivateprojects2);
                    }
                        
                }, null, null, {
                    async: false
                }
            );
        this.actions = actions;
    };

    this.getOneActions = function(actionId){
        var action = {};
        var url = baseUrl + '/costum/project/action/request/action';
        var post = {
            action: actionId
        };
        ajaxPost(null,url, post,
            function(data) {
                action = data
            }, null, null, {
                async: false
            }
        );
        return action;
    }

    this.getOneActionsDetail = function(actionId){
        var action = {};
        var url = baseUrl + '/costum/project/action/request/action_detail_data';
        var post = {
            id: actionId
        };
        ajaxPost(null,url, post,
            function(data) {
                action = data
            }, null, null, {
                async: false
            }
        );
        return action;
    }

    this.addImageAndFile = function(actionId) {
        var url = baseUrl + '/costum/project/action/request/add_image_file';
        var post = {
            action: actionId
        };
        ajaxPost(null,url, post,
            function(data) {
            }, null, null, {
                async: false
            }
        );
    };

    this.getImageFile = function(actionId) {
        var imagesAndFiles = [];
        var url = baseUrl + '/costum/project/action/request/media';
        var post = {
            action: actionId
        };
        ajaxPost(null,url, post,
            function(data) {
                imagesAndFiles = data;
            }, null, null, {
                async: false
            }
        );
        return imagesAndFiles;
    };

    this.countCheckedTasks = function(tasks){
        var countCheckedTasks = 0;
        $.each(tasks,function(k,v){
            if(exists(v.checked) && v.checked){
                countCheckedTasks++;
            }
        })
        return countCheckedTasks;
    }

    this.openImage = function (actionId){
        $("#lightbox-image").empty();
        var images = this.getImageFile(actionId);
        mylog.log(images,"imagesAndFiles");
            images = images["images"];
        var imageUrls = Object.values(images).map(function(v){
            return v.docPath;
        });

        var targetElement = document.getElementById('lightbox-image');

        var imageElements = [];
        for (var i = 0; i < imageUrls.length; i++) {
            var imageElement = document.createElement('a');
            imageElement.setAttribute('href', imageUrls[i]);
            imageElement.setAttribute('data-lightbox', 'images');
            imageElement.setAttribute('data-title', "");
            imageElement.textContent = "click";
            targetElement.appendChild(imageElement);
        }
        $("#lightbox-image [data-lightbox]").click();
    }

    this.openFile = function (actionId,actionName){
        var html =`
        <h2 class="margin-bottom-15">${actionName}</h2>
        <ul class="list-group">`;
        var files = this.getImageFile(actionId);
        files = files["files"];
        $.each(files,function(k,v){
            html +=`<li class="list-group-item">
                <a href="${v.docPath}" target="_blank">${v.name}</a>
            </li>` 
        })
        html +='</ul>';
        smallMenu.open(html, null, null, function(){});
    }

    this.openContributors = function(actionId){
        var action = this.getOneActionsDetail(actionId);
        if(exists(action.contributors)){
            var html = ``;
            $.each(action.contributors,function(k,v){
                html+=`
                <li class="list-group-item no-border padding-5">
                    <div class="media">
                        <a class="media-left lbh-preview-element" href="#page.type.citoyens.id.${k}">
                            <img class="media-object radius-20" width="30" height="30" src="${v.image}" alt="${v.name}">
                        </a>
                        <div class="media-body">
                            <a class="media-left lbh-preview-element small" href="#page.type.citoyens.id.${k}">
                             ${v.name}
                            </a>
                        </div>
                    </div>
                </li>
`
            })
            $(`#dropdown-menu-${actionId}`).empty().html(html);
            coInterface.bindLBHLinks();
        }
    }


    this.refreshItem = function(actionId){
        var action = this.getOneActions(actionId);
        var keyBoard = $("[data-eid="+actionId+"]").parent().parent().data("id");
        var index = $("[data-eid="+actionId+"]").index();
        $("[data-eid="+actionId+"]").remove();
        this.addElementInBoard(keyBoard,action,index);
    }

    this.showActionDetailModal= function(__id){
        return new Promise(function(__resolve) {
            var url = baseUrl + '/costum/project/action/request/action_detail_html';
            var post = {
                id: __id
            };
            ajaxPost(null, url, post, __resolve, null, 'text');
        });
    }

    this.updateStatusAction= function(params,callBack) {
        params.path = 'status';
        switch (params.key) {
            case 'todo':
                params.value = params.key;
                dataHelper.path2Value(params, function(data) {
                    if(typeof callBack=="function")
                        callBack(data)
                });
                break;
            case 'done':
                params.value = params.key;
                dataHelper.path2Value(params, function(data) {
                    if(typeof callBack=="function")
                        callBack(data)
                });
                break;
            case 'tracking':
                params.path = 'tracking';
                params.value = true;
                dataHelper.path2Value(params, function(data) {
                    if(typeof callBack=="function")
                        callBack(data)
                });
                break;
            case 'todiscuss':
                params.path = 'tags';
                params.value = [this.tags.todiscuss];
                dataHelper.path2Value(params, function(data) {
                    if(typeof callBack=="function")
                        callBack(data)
                });
                break;
                break;
            case 'totest':
                params.path = 'tags';
                params.value = [this.tags.totest];
                dataHelper.path2Value(params, function(data) {
                    if(typeof callBack=="function")
                        callBack(data)
                });
                break;
            default:
                params = null;
        }
    },

    this.dynformEditAction = function(idAction){
        var initList = $this.getImageFile(idAction);
        var initListImage = exists(initList["images"]) ? initList["images"] : [];
        var initListFile = exists(initList["images"]) ? initList["files"] : [];
        delete initList;
        var today = new Date();
        $this = this;
        return {
            "jsonSchema": {
                "title": "Editer un action",
                "icon": "fa-task",
                "properties": {
                    name: dyFInputs.name("action"),
                    tags: {
                        inputType: "tags",
                        label: trad.tags,
                    },
                    contributors: {
                        inputType: "finder",
                        label: "Assigné à qui ?",
                        multiple: true,
                        rules: {
                            lengthMin: [1, "contributors"]
                        },
                        initType: ["citoyens"],
                        search: {
                            '$or': {
                                ["links.memberOf." + $this.context._id.$id + ".isAdmin"]: {
                                    "$exists": true
                                },
                                ["links.projects." + $this.context._id.$id + ".isAdmin"]: {
                                    "$exists": true
                                }
                            }
                        },
                        initBySearch: true,
                        initMe: true,
                        initContext: false,
                        initContacts: false,
                        openSearch: true
                    },
                    image : {
                        inputType: "uploader",
                        label: trad.images,
                        docType: "image",
                        itemLimit: 4,
                        domElement: "actionimage",
                        filetypes: ["jpeg", "jpg", "gif", "png"],
                        showUploadBtn: false,
                        endPoint :"/subKey/actionImage",
                        initList : initListImage
                    },
                    file : {
                        inputType: "uploader",
                        label: trad.files,
                        docType: "file",
                        contentKey: "file",
                        afterUploadComplete: null,
                        template: "qq-template-manual-trigger",
                        itemLimit: 4,
                        domElement: "actionfile",
                        filetypes: ["pdf","xls","xlsx","doc","docx","ppt","pptx","odt","ods","odp", "csv"],
                        showUploadBtn : false,
                        endPoint :"/subKey/actionFile",
                        initList : initListFile
                    }
                },
                beforeBuild: function() {
                    uploadObj.set("actions", idAction);
                },
                afterBuild: function(data) {
                    $("#name").focus();
                },
                save: function(data) {
                    data.collection = "actions";
                    delete data.scope;
                    let links = {
                        contributors: data.contributors
                    };
                    delete data.contributors;
                    dataHelper.path2Value({
                        id: idAction,
                        collection: "actions",
                        path: "allToRoot",
                        value: data,
                        updatePartial: true,
                    }, function(p) {
                        dyFObj.commonAfterSave(p,function(){
                            $this.addImageAndFile(p.id);
                            if (Object.keys(links.contributors).length != 0) {
                                dataHelper.path2Value({
                                    id: idAction,
                                    path: "links",
                                    collection: "actions",
                                    value: links
                                }, function(parms) {})
                            }
                            $this.refreshItem(idAction);
                            setTimeout(() => {
                                dyFObj.closeForm();
                            }, 500);
                        });
                    })
                }
            }
        }
    }
    this.dynformAddAction = function(key="todo",){
        var $this = this;
        var today = new Date();
        return {
            "jsonSchema": {
                "title": "Ajouter un action",
                "icon": "fa-task",
                "properties": {
                    id: dyFInputs.inputHidden(""),
                    idParentRoom: dyFInputs.inputHidden($this.room),
                    name: dyFInputs.name("action"),
                    credits: {
                        inputType: "hidden",
                        order: 7,
                        rules: {
                            number: true
                        },
                        value: 1
                    },
                    tags: {
                        inputType: "tags",
                        label: trad.tags,
                    },
                    contributors: {
                        inputType: "finder",
                        label: "Assigné à qui ?",
                        multiple: true,
                        rules: {
                            lengthMin: [1, "contributors"]
                        },
                        initType: ["citoyens"],
                        search: {
                            '$or': {
                                ["links.memberOf." + $this.context._id.$id + ".isAdmin"]: {
                                    "$exists": true
                                },
                                ["links.projects." + $this.context._id.$id + ".isAdmin"]: {
                                    "$exists": true
                                }
                            }
                        },
                        initBySearch: true,
                        initMe: true,
                        initContext: false,
                        initContacts: false,
                        openSearch: true
                    },
                    status: dyFInputs.inputHidden("todo"),
                    idParentResolution: dyFInputs.inputHidden(""),
                    email: dyFInputs.inputHidden(((userId != null && userConnected != null) ? userConnected.email : "")),
                    idUserAuthor: dyFInputs.inputHidden(userId),
                    parentId: dyFInputs.inputHidden(this.projectId),
                    answerId: dyFInputs.inputHidden(this.answerId),
                    parentType: dyFInputs.inputHidden("projects"),
                    parentIdSurvey: dyFInputs.inputHidden(typeof form != "undefined" ? form : ""),
                    parentIdSurvey: dyFInputs.inputHidden(typeof form != "undefined" ? form._id.$id : ""),
                    parentTypeSurvey: dyFInputs.inputHidden(typeof form != "undefined" ? "forms" : ""),
                    role: dyFInputs.inputHidden(typeof role != "undefined" ? role : ""),
                    image : {
                        inputType: "uploader",
                        label : trad.images,
                        docType : "image",
                        itemLimit : 4,
                        domElement : "actionimage",
                        filetypes : ["jpeg", "jpg", "gif", "png"],
                        endPoint :"/subKey/actionImage",
                        showUploadBtn : false,
                    },
                    file : {
                        inputType: "uploader",
                        label: trad.files,
                        docType: "file",
                        itemLimit: 4,
                        domElement: "actionfile",
                        filetypes: ["pdf","xls","xlsx","doc","docx","ppt","pptx","odt","ods","odp", "csv"],
                        endPoint :"/subKey/actionFile",
                        showUploadBtn : false,
                    }
                },
                beforeBuild : function(){
                    dyFObj.setMongoId('actions', function(){
                        //uploadObj.id
                    });
                },
                afterBuild: function(data) {
                    $("#name").focus();
                    //alert($this.projectId);
                },
                beforeSave: function() {
                },
                afterSave: function() {
                    alert("ggooo")
                },
                save: function(data) {
                    data.collection = "actions";
                    delete data.scope;
                    data.parentId = $("#ajaxFormModal #parentId").val();
                    let links = {
                        contributors: data.contributors
                    };
                    mylog.log(data.contributors, "linkskoo");
                    delete data.contributors;
                    dataHelper.path2Value({
                        collection: "actions",
                        path: "allToRoot",
                        value : data,
                    },function(p){
                        dyFObj.commonAfterSave(p,function(){
                            $this.addImageAndFile(p.saved.id);
                            $this.updateStatusAction({id : p.saved.id,collection:"actions",key:key},function(res){
                                $this.addElementInBoard(key,res.elt);
                            });
                            
                            if (Object.keys(links.contributors).length != 0) {
                                dataHelper.path2Value({
                                    id: p.saved.id,
                                    path: "links",
                                    collection: "actions",
                                    value: links
                                }, function(parms) {})
                            }
                            setTimeout(() => {
                                dyFObj.closeForm();
                            }, 500);
                        });
                    })
                }
            }
        }
    }

    this.events = function(){
        var $this = this;
        $('.cokanban-card .card-title,.cokanban-card .action-tasks').off().on('click',function(e){
            var id = $(this).data('id');
            $this.showActionDetailModal(id).then(function(html){
                $("#modal-detail-action").empty().html(html);
                $('.action-modal').on('hidden.bs.modal', function() {
                    $this.refreshItem(id);
                })
            });
        })

        $('.card-title .edit-action').off().on("click",function(e){
            e.stopPropagation();
            var actionId = $(this).data("id");
            var action = $this.getOneActions(actionId);
            dyFObj.openForm(
                $this.dynformEditAction(actionId)
                ,null,action,null,null,{
                    type: "bootbox",
                    notCloseOpenModal:true
                }
            )
        })

        $('.new-action,.kanban-board-header').off().on('click', function() {
            var btn = $(this);
            var keyBoard = btn.parent().data('id');
            $('.search-action-field').val("").trigger('keyup');
            dyFObj.openForm(
                $this.dynformAddAction(keyBoard)
                ,null,null,null,null,{
                    type: "bootbox",
                    notCloseOpenModal:true
                }
            )
        })

        $('.action-comment').off().on('click', function() {
            var id = $(this).data("id");
            var title = $(this).data("title");
            commentObj.openPreview("actions", id,"",title);
        })

        $('.action-image').off().on('click', function() {
            var id = $(this).data("id");
            $this.openImage(id);
        })

        $('.action-attachment').off().on('click', function() {
            var id = $(this).data("id");
            var name = $(this).data("name");
            $this.openFile(id,name);
        })
        $('.action-contributor').off().on('click', function() {
            var id = $(this).data("id");
            $this.openContributors(id);           
        })
        $('.my-action-filter').off().on('change',function(){
            var newUrl = location.href.split('#')[0];
            var hashUrl = location.hash;
            var getParams = (typeof location.hash.split('?')[1]!="undefined" ? ("?" + location.hash.split('?')[1]) : "");
            var hashNoParams = location.hash.split('?')[0];
            var projectId = hashParams()?.["projectId"];

            if (location.href.indexOf("userId")!= -1) {
                hashNoParams = hashNoParams.split(".");
                var index = hashNoParams.indexOf("userId");
                hashNoParams.splice(index+1,1);
                hashNoParams = removeFromArray(hashNoParams, "userId");
                hashNoParams = hashNoParams.join('.');
            }

            if ($(this).is(':checked')) {
                history.pushState('', document.title, newUrl + hashNoParams + '.userId.'+userId + getParams);
                if(notEmpty(projectId))
                    $this.changeProject(projectId,userId);
            }else{
                history.pushState('', document.title, newUrl + hashNoParams + getParams);
                if(notEmpty(projectId))
                    $this.changeProject(projectId);
            }
        })
    }

    this.doubleScroll =  function(){
        var container = this.container;
        var kanbanContainerWidth = $(container+" .kanban-container").width()
        if(container.indexOf("#") != -1){
            var attr= `id=${container.substring(1)}-scroll`
            var elScroll = `${container}-scroll`
        }else{
            var attr= `class=${container.substring(1)}-scroll`
            var elScroll = `${container}-scroll`
        }
        
        $(container).before(`<div ${attr}><div></div></div>`).before(`<div id="modal-detail-action"></div>`);
        $(elScroll+" div").css("width", kanbanContainerWidth);
        $(elScroll).scroll(function() {
            $(container).scrollLeft($(elScroll).scrollLeft());
        });
        $(container).scroll(function() {
            $(elScroll).scrollLeft($(container).scrollLeft());
        });
        this.setCss(container);
    }

    this.setCss = function(container){
        var elScroll = `${container}-scroll`
        var style = `
        ${elScroll} div{
            position:relative;
            width:1500px;
            height:1px
        }
        ${container} {
            position:relative;
            overflow-x: auto;
            padding: 20px 0;
            width:100%;
            -webkit-overflow-scrolling: touch;
            margin-bottom:10px;
            height: 97%;
        }

        ${elScroll} {
            position:relative;
            width:100%;
            overflow-x: scroll; 
            overflow-y:hidden
        }

        ${container}::-webkit-scrollbar-track,
        ${elScroll}::-webkit-scrollbar-track,
        .kanban-board .kanban-drag::-webkit-scrollbar-track,
        .kanban-item .panel-body::-webkit-scrollbar-track,
        .kanban-project-list::-webkit-scrollbar-track{
            -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.1);
            background-color: #F5F5F5;
        }

        ${container}::-webkit-scrollbar,
        ${elScroll}::-webkit-scrollbar,
        .kanban-board .kanban-drag::-webkit-scrollbar,
        .kanban-item .panel-body::-webkit-scrollbar,
        .kanban-project-list::-webkit-scrollbar{
            width: 5px;
            height: 5px;
            background-color: #F5F5F5;
        }

        ${container}::-webkit-scrollbar-thumb,
        ${elScroll}::-webkit-scrollbar-thumb,
        .kanban-board .kanban-drag::-webkit-scrollbar-thumb,
        .kanban-item .panel-body::-webkit-scrollbar-thumb,
        .kanban-project-list::-webkit-scrollbar-thumb
        {
            background-color: #979797;
            /*background-image: -webkit-gradient(linear,
                                            40% 0%,
                                            75% 84%,
                                            from(#4D9C41),
                                            to(#19911D),
                                            color-stop(.6,#67737c))*/
            border-radius:5px;                               
        }
        `
        $('#cssFile').append(style);
    }

    this.cardTemlateByStatus = function(v) {
        $this = this;
        var k = v._id.$id
        return {
            id: k,
            class: ["action" + k],
            title: this.createCard(v),
            drag: function(el, source) {
                mylog.log("START DRAG: " + el.dataset.eid);
            },
            dragend: function(el) {
                mylog.log("END DRAG: " + el.dataset.eid);
            },
            drop: function(el, target, source, sibling) {
                mylog.log(el.dataset.tags)
                target.tplCtx = {
                    id: el.dataset.eid,
                    collection: "actions",
                    path: "status",
                    value: target.parentNode.dataset.id,
                    format: true,
                }
                target.removeTags = function(tagsArr, tagToRemove) {
                    var index = tagsArr.indexOf(tagToRemove);
                    if (index != -1) {
                        delete target.tplCtx.arrayForm;
                        delete target.tplCtx.edit;
                        target.tplCtx.value = null;
                        target.tplCtx.path = "tags." + index;
                        target.tplCtx.pull = "tags";
                        dataHelper.path2Value(target.tplCtx, function(params) {

                        })
                    }
                }

                if (target.parentNode.dataset.id == "todo") {
                    dataHelper.path2Value(target.tplCtx, function(params) {
                        if (notEmpty(params.elt.tags)) {
                            target.removeTags(params.elt.tags, $this.tags.todiscuss);
                            target.removeTags(params.elt.tags, $this.tags.totest)
                        }
                        ajaxPost("", baseUrl + '/survey/answer/rcnotification/action/newstatusaction', {
                            actionId: el.dataset.eid,
                            status: "todo"
                        }, function() {}, "html")
                    });
                }

                if (target.parentNode.dataset.id == "done") {
                    dataHelper.path2Value(target.tplCtx, function(params) {
                        if (notEmpty(params.elt.tags)) {
                            target.removeTags(params.elt.tags, $this.tags.todiscuss);
                            target.removeTags(params.elt.tags, $this.tags.totest)
                        }
                        ajaxPost("", baseUrl + '/survey/answer/rcnotification/action/newstatusaction', {
                            actionId: el.dataset.eid,
                            status: "done"
                        }, function() {}, "html")
                    });
                }
                if (target.parentNode.dataset.id == "tracking") {
                    target.tplCtx.path = "tracking";
                    target.tplCtx.value = true;
                    dataHelper.path2Value(target.tplCtx, function(params) {
                        if (notEmpty(params.elt.tags)) {
                            target.removeTags(params.elt.tags, $this.tags.todiscuss);
                            target.removeTags(params.elt.tags, $this.tags.totest)
                        }
                    })
                }
                if (target.parentNode.dataset.id == "todiscuss") {
                    target.tplCtx.path = "tags";
                    target.tplCtx.value = $this.tags.todiscuss;
                    target.tplCtx.arrayForm = true;
                    target.tplCtx.edit = false;
                    dataHelper.path2Value(target.tplCtx, function(params) {
                        if (notEmpty(params.elt.tags)) {
                            target.removeTags(params.elt.tags, $this.tags.totest)
                        }
                    })
                }
                if (target.parentNode.dataset.id == "totest") {
                    target.tplCtx.path = "tags";
                    target.tplCtx.value = $this.tags.totest;
                    target.tplCtx.arrayForm = true;
                    target.tplCtx.edit = false;
                    dataHelper.path2Value(target.tplCtx, function(params) {
                        if (notEmpty(params.elt.tags)) {
                            target.removeTags(params.elt.tags, $this.tags.todiscuss)
                        }
                    })
                }

                if (target.parentNode.dataset.id != "tracking") {
                    dataHelper.path2Value({
                        id: el.dataset.eid,
                        collection: "actions",
                        path: "tracking",
                        format: true,
                        value: "false",
                    }, function(params) {
                        toastr.success(trad.saved);
                    })
                }
                $this.countCardInColumn();
            },
            tags: exists(v.tags) ? v.tags : [],
            click: function(el) {

            },
        }
    }

    this.searchActionByName = function() {
        $this = this;
        var container = $this.container;
        var html = 
        `<div class="kanban-filter-container">
            <input type="text" class="form-control search-action-field margin-top-5 margin-bottom-5" placeholder="${trad.search} action">
            <div class="dropdown hidden">
                <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">${tradCms.filter}
                <span class="caret"></span></button>
                <ul class="dropdown-menu">
                    <li>
                        <div class="checkbox margin-left-10">
                            <label>
                                <input type="checkbox" ${notEmpty(this.userId) && this.userId == userId  ? 'checked' : ""} class="padding-left-10 my-action-filter" value="">Mes actions
                            </label>
                        </div>
                    </li>
                </ul>
            </div>
            <input type="text" class="pull-right" ${notEmpty(this.userId) ? 'value="df"' : ''} id="search-person" placeholder="Voir l'action d'une personne">
        </div><br />`;
        $(container+"-scroll").before(html);
        $(".search-action-field").on("keyup", function() {
            setTimeout(() => {
                var searchKey = $(this).val();
                $('.kanban-drag .kanban-item').each(function() {
                    var txt = $(this).find('.card-title span').text();
                    if (txt.match(new RegExp(searchKey, "gi"))) {
                        $(this).show(500)
                    } else {
                        $(this).hide(500);
                    }
                })
                $this.countCardInColumn();
            }, 1200);
        })
    },

    this.searchPersonAction = function(){
        var searchContextId = "";

        var hideSeachInput = function(){
            if($('.select2-search-choice').length != 0){
                $('.select2-search-field').css("height","8px")
            }else{
                $('.select2-search-field').css("height","initial")
            }
        }

        ajaxPost(
            null,
            baseUrl+"/"+moduleId+"/slug/getinfo/key/openAtlas",
            null,
            function(data){
                searchContextId = data.contextId
            }, null, null,
            {
                async : false
            }
        );
        var defaultValue = {};
        if(notEmpty(this.userId)){
            ajaxPost('', baseUrl + "/co2/element/get/type/citoyens/id/" + this.userId,
            null,
            function(data) {
               mylog.log(data,"datatoako")
               defaultValue = {[this.userId] : data.map.name}
            }, null, null, {
                async: false
            });
        }
        $("#search-person").empty().select2({
            maximumSelectionSize : 1,
            multiple : false,
            tags : true,
            "tokenSeparators": [','],
            createSearchChoice: function(term, data) {
                if ($(data).filter(function() {
                    return this.text.localeCompare(term) === 0;
                }).length === 0) {
                    return { id: term, text: term };
                }
            },
            initSelection: function(element, callback) {
                var data = [];
                for (var key in defaultValue) {
                    data.push({ id: key, text: defaultValue[key] });
                }
                callback(data);
            },
            ajax: {
                url: baseUrl + "/" + moduleId + "/search/globalautocomplete",
                dataType: 'json',
                type: "POST",
                quietMillis: 50,
                data: function(term) {
                    var fltrs = {
                        '$or': []
                    };
                    var objFltrs = {};

                    if (notEmpty(searchContextId)) {
                        objFltrs["links.memberOf." + searchContextId] = {
                            "$exists": "true"
                        }
                        objFltrs["links.projects." + searchContextId] = {
                            "$exists": "true"
                        }
                        fltrs['$or'] = objFltrs;
                    }

                    return {
                        name: term,
                        searchType: ["citoyens"],
                        filters: fltrs
                    };
                },
                results: function(data) {
                    return {
                        results: $.map(Object.values(data.results), function(item) {
                            return {
                                text: item.name,
                                id: item._id.$id
                            }
                        })
                    };
                }
            }
        }).on('change', () => {
            var userIdd = ($("#search-person").val());
            if(notEmpty(userIdd) && userIdd.includes("df") ){
                userIdd = userIdd.split(',');
                userIdd = removeFromArray(userIdd,"df");
                userIdd = userIdd.join('');
            }

            var newUrl = location.href.split('#')[0];
            var hashUrl = location.hash;
            var getParams = (typeof location.hash.split('?')[1]!="undefined" ? ("?" + location.hash.split('?')[1]) : "");
            var hashNoParams = location.hash.split('?')[0];
            var projectId = hashParams()?.["projectId"];

            if (location.href.indexOf("userId")!= -1) {
                hashNoParams = hashNoParams.split(".");
                var index = hashNoParams.indexOf("userId");
                hashNoParams.splice(index+1,1);
                hashNoParams = removeFromArray(hashNoParams, "userId");
                hashNoParams = hashNoParams.join('.');
            }

            if (notEmpty(userIdd)) {
                history.pushState('', document.title, newUrl + hashNoParams + '.userId.'+userIdd + getParams);
                if(notEmpty(projectId))
                    $this.changeProject(projectId,userIdd);
            }else{
                history.pushState('', document.title, newUrl + hashNoParams + getParams);
                if(notEmpty(projectId))
                    $this.changeProject(projectId);
            }
            setTimeout(() => {
                hideSeachInput();
            }, 800);
        });
        hideSeachInput();
    }

    this.countCardInColumn =  function(){
        $.each(this.cards, function(k) {
            var cCounter = $('[data-id=' + k + '] .kanban-drag').children(".kanban-item:visible").length;
            if (cCounter != 0)
                $('[data-id=' + k + '] .kanban-board-header .kanban-column-counter').text(`(${cCounter})`);
            else
                $('[data-id=' + k + '] .kanban-board-header .kanban-column-counter').text('');
        })
    }

    this.createCard = function(v){
        if(exists(v?.tasks)){
            var tasksStatus = this.countCheckedTasks(v?.tasks) +"/"+v?.tasks.length;
            var classTasks = (this.countCheckedTasks(v?.tasks) == v?.tasks.length) ? " letter-green" :
            (this.countCheckedTasks(v?.tasks) == 0 ? " letter-red":"");
        }
        var html = `
        <div class="cokanban-card">
            <div class="card-title" data-id="${v._id.$id}">
                <span>${ucfirst(v.name)}</span>
                <a href="javascript:;" class="btn btn-default pull-right edit-action no-border" data-id='${v._id.$id}'>
                    <i class="fa fa-pencil"></i>
                </a>
            </div>
            <div class="kanban-button">
                <div id="lightbox-image"></div>`;
                if(exists(v?.links?.contributors)){
                    html += 
                    `<div class="dropdown pull-right" style="display:inline">
                        <a href="javascript:;" class="btn btn-default no-border tooltips dropdown-toggle action-contributor" type="button" data-id="${v._id.$id}" data-name="${escapeHtml(v.name)}" data-toggle="dropdown" data-placement="bottom" data-original-title="${trad.contributors}">
                        ${Object.keys(v?.links?.contributors).length} <i class="fa fa-users"></i>
                        <ul class="dropdown-menu" id="dropdown-menu-${v._id.$id}" style="right:0;left: -217px;"></ul>
                    </div>`;
                }
                if(exists(v?.media?.images)){
        html += `<a href="javascript:;" class="btn btn-default pull-right action-image no-border tooltips" data-id="${v._id.$id}" data-placement="bottom" data-original-title="${trad.images}">
                    ${v?.media?.images.length+'<i class="fa fa-image margin-left-5"></i>'}
                </a>`;
                }
                if(exists(v?.mediaFile?.files)){
        html += `<a href="javascript:;" class="btn btn-default pull-right action-attachment no-border tooltips" data-id="${v._id.$id}" data-name="${escapeHtml(v.name)}" data-placement="bottom" data-original-title="${trad.files}">
                    ${v?.mediaFile?.files.length+'<i class="fa fa-paperclip margin-left-5"></i>'}
                </a>`;
                }
                if(exists(v?.tasks)){
        html += `<a href="javascript:;" class="btn btn-default pull-right action-tasks no-border bold tooltips ${classTasks}" data-id="${v._id.$id}" data-name="${escapeHtml(v.name)}" data-placement="bottom" data-original-title="${trad.task}s">
                    ${tasksStatus+'<i class="fa fa-tasks margin-left-5"></i>'}
                </a>`;
                }
        html+=`<a href="javascript:;" class="btn btn-default pull-right action-comment no-border tooltips" data-id="${v._id.$id}" data-title="${escapeHtml(v.name)}" data-placement="bottom" data-original-title="${trad.comments}">
                    ${exists(v.commentCount) ? v.commentCount : ""} <i class="fa fa-comment"></i>
                </a>`;
        html+=`</div>
        </div>
        `;
        return html;
    }
    
    this.createBoardItem = function(actions) {
        $.each(actions, (k, v) => {
            if(notEmpty(v.status) && v.status != "disabled"){
                if(exists(v.status) && (v.status == 'success' || v.status == 'done') )
                    this.cards.done.push(this.cardTemlateByStatus(v));
                else if((exists(v.tags) && Array.isArray(v.tags) && v.tags.includes(this.tags.totest) ))
                    this.cards.totest.push( this.cardTemlateByStatus(v));
                else if((exists(v.tags) && Array.isArray(v.tags) && v.tags.includes(this.tags.todiscuss) ))
                    this.cards.todiscuss.push( this.cardTemlateByStatus(v));
                else if((exists(v.tracking) && v.tracking) || v.status == "inprogress")
                    this.cards.tracking.push( this.cardTemlateByStatus(v));
                else if(exists(v.status) && (v.status == 'todo' || v.status == "disabled") )
                    this.cards.todo.push(this.cardTemlateByStatus(v)); 
            }
        })
    },

    this.createBoard = function(){
        return [
            {
                id: "todiscuss",
                title: `<span class="tooltips" data-toggle="tooltip" data-placement="bottom" data-original-title="${trad.addAction}"><i class='hidden fa fa-comment'></i> A discuter <span class="kanban-column-counter"></span> <i class='fa fa-plus pull-right margin-top-5'></i></span> `,
                class: "/warning",
                dragTo: ["todo", "done", "tracking", "todiscuss", "totest"],
                item: this.cards.todiscuss
            },
            {
                id: "todo",
                title: `<span class="tooltips" data-toggle="tooltip" data-placement="bottom" data-original-title="${trad.addAction}"><i class='hidden fa fa-thumb-tack'></i> To do <span class="kanban-column-counter"></span><i class='fa fa-plus pull-right margin-top-5'></i></span> `,
                class: "/info,good",
                dragTo: ["todo", "done", "tracking", "todiscuss", "totest"],
                item: this.cards.todo
            },
            {
                id: "tracking",
                title: `<span class="tooltips" data-toggle="tooltip" data-placement="bottom" data-original-title="${trad.addAction}"><i class='hidden fa fa-star'></i> En cours <span class="kanban-column-counter"></span><i class='fa fa-plus pull-right margin-top-5'></i></span> `,
                class: "/warning",
                dragTo: ["todo", "done", "tracking", "todiscuss", "totest"],
                item: this.cards.tracking
            },
            {
                id: "totest",
                title: `<span class="tooltips" data-toggle="tooltip" data-placement="bottom" data-original-title="${trad.addAction}"><i class='hidden fa fa-gavel'></i> A tester <span class="kanban-column-counter"></span><i class='fa fa-plus pull-right margin-top-5'></i></span> `,
                class: "/info",
                dragTo: ["todo", "done", "tracking", "todiscuss", "totest"],
                item: this.cards.totest
            },
            {
                id: "done",
                title: `<span class="tooltips" data-toggle="tooltip" data-placement="bottom" data-original-title="${trad.addAction}"><i class='hidden fa fa-check'></i> Terminé <span class="kanban-column-counter"></span><i class='fa fa-plus pull-right margin-top-5'></i></span> `,
                class: "/success",
                dragTo: ["done", "tracking", "todiscuss", "totest"],
                item: this.cards.done
            }
        ]
    }

    this.initKanban = function(){
        var actions = this.actions;
        this.createBoardItem(actions);
        var kanban = new jKanban({
            element: this.container,
            gutter: "10px",
            widthBoard: "350px",
            itemHandleOptions: {
                enabled: true,
            },
            click: function(el) {
                mylog.log("Trigger on all items click!");
            },
            context: function(el, e) {
                mylog.log("Trigger on all items right-click!");
            },
            dropEl: function(el, target, source, sibling) {
                mylog.log(target.parentElement.getAttribute('data-id'));
                mylog.log(el, target, source, sibling)
            },
            buttonClick: function(el, boardId) {

            },
            /*itemAddOptions: {
                enabled: true,
                content: '+ action',
                class: 'new-action',
                footer: true
            },*/
            boards: this.createBoard(),
        });
        this.kanban = kanban;
    }

    this.addElementInBoard = function(keyBoard,valueAction,index=0){
        var card = this.cardTemlateByStatus(valueAction);
        this.kanban.addElement(keyBoard, card,index);
        this.events();
        this.countCardInColumn();
    }

    this.reset = function(){
        this.cards = {
            done : [],
            totest : [],
            todiscuss : [],
            tracking : [],
            todo : [],
        }
        this.actions = [];
        this.room = null;
        $(".kanban-item").remove();
    }

    this.changeProject = function(projectId,userId = null){
        this.projectId = projectId;
        this.reset();
        this.setActions(this.projectId,userId);
        this.createBoardItem(this.actions);
        $this = this;
        $.each(this.cards,function(keyBoard,value){
            mylog.log(keyBoard,value,"keyBoard")
            $.each(value,function(k,v){
                $this.kanban.addElement(keyBoard, v)
            })
        })
        this.countCardInColumn();
        this.events();
    }

    this.overrideComment = function(){
        var $this = this;
        commentObj.openPreview = function(type, id, path, title, coformKey, formId, options) {
            let actionName = title;
            if (typeof options == "undefined" || (typeof options != "undefined" && typeof options.notCloseOpenModal == "undefined"))
                $("#openModal").modal("hide");
            title = decodeURI(title);
            hashPreview = "?preview=comments." + type + "." + id;
            urlPreview = baseUrl + '/' + moduleId + "/comment/index/type/" + type + "/id/" + id;
            if (notNull(path)) {
                urlPreview += "/path/" + path;
                hashPreview += "." + path;
            }
            if (notNull(title))
                hashPreview += "." + encodeURI(title);
            else
                title = "";
            hashT = location.hash.split("?");
            getStatus = searchInterface.getUrlSearchParams();
            urlHistoric = hashT[0].substring(0) + hashPreview;
            if (getStatus != "") urlHistoric += "&" + getStatus;
            $("#modal-preview-comment .title-comment").html(title);
            $("#modal-preview-comment .btn-close-preview").data("id",id);
            coInterface.showLoader("#modal-preview-comment .comment-tree");
            $("#modal-preview-comment").show(200);

            getAjax('#modal-preview-comment .comment-tree', urlPreview, function() {
                commentObj.bindModalPreview();
            }, "html");
            if (coformKey && formId) {
                commentObj.coformKey = coformKey;
                commentObj.formId = formId;
                commentObj.afterSaveReload = function() {
                    reloadInput(commentObj.coformKey, commentObj.formId);

                }
            }
            commentObj.afterSaveReload = function() {
                ajaxPost(
                    null,
                    baseUrl + '/' + moduleId + "/comment/countcommentsfrom", {
                        "type": type,
                        "id": id,
                        "path": path
                    },
                    function(data) {
                        $("#btn-comment-" + id).html(data.count + " <i class='fa fa-commenting'></i>")
                        ajaxPost("", baseUrl + '/survey/answer/rcnotification/action/commentaction', {
                                actionId: id,
                            },
                            function(data) {}, "html");
                    }
                );
            }
        };
        commentObj.bindModalPreview = function(){
            $("#modal-preview-comment .btn-close-preview").off().on("click", function(){
                var id= $(this).data('id');
                commentObj.closePreview(id);
            });

            $(".main-container").on("click",function() {
                if( $("#modal-preview-comment").css("display") == "block" ){
                    var id = $("#modal-preview-comment .btn-close-preview").data('id');
                    commentObj.closePreview(id); 
                }
            });
        }
        commentObj.closePreview = function(id=null) {
            $(".main-container").off();
            $("#modal-preview-comment").css("display", "none");
            if(id!=null){
                $this.refreshItem(id);
            }
        }
    }

    this.init = function(){
        this.setActions(this.projectId,this.userId);
        this.initKanban();
        this.overrideComment();
        this.doubleScroll();
        this.searchActionByName();
        this.countCardInColumn();
        this.searchPersonAction();
        this.events();
        $('#'+this.container.substring(1)+'-scroll').scrollLeft($(window).width()- ($(window).width() -200));
    }
    this.init();
}