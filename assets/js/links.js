function isFromActivityPub(params) {
	return params.fromActivityPub != undefined && params.fromActivityPub != null
	  ? params.fromActivityPub
	  : false;
}
function activityPubObjectId(params) {
	return params.objectId != undefined && params.objectId != null
	  ? params.objectId
	  : "";
}
function getActorType() {
	if (
	  contextData != undefined &&
	  contextData.type != null &&
	  contextData.typeProject != null &&
	  contextData.typeProject === "projects" &&
	  contextData.typeElement != null &&
	  contextData.typeElement != "projects"
	) {
	  return "project";
	} else if (
	  contextData != undefined &&
	  contextData.type != null &&
	  contextData.type === "organizations" &&
	  contextData.typeElement != null &&
	  contextData.typeElement === "Group"
	) {
	  return "group";
	} else {
	  return "person";
	}
}
var links = {
	connectType : {
		"citoyens":"friends",
		"organizations":"members",
		"projects":"contributors",
		"events": "attendees"
	},
	linksTypes : {
        "citoyens" :{
        	"citoyens": "friends",
        	"projects" :  "projects",
			"events" :  "events",
            "organizations" :  "memberOf"
        },
    	"organizations" :{
    		"projects" :  "projects",
			"events" :  "events",
            "citoyens" : "members",
            "organizations" : "members"
        },
    	"events" :{
    			"events" : "subEvent",
                "citoyens" : "attendees"
        },
    	"projects" :{
    			"organizations" : "contributors",
				"citoyens" : "contributors",
				"projects" :  "projects"
		}
	},
	linksExternalContext : {
        "citoyens" :{
        	"citoyens": "friends",
        	"projects" :  "contributors",
			"events" :  "attendees",
            "organizations" :  "members"
        },
    	"organizations" :{
    		"projects" :  "cotributors",
			"events" :  "attendees",
            "citoyens" : "memberOf",
            "organizations" : "memberOf"
        },
    	"events" :{
    			"events" : "events", // Je sais pas si ce lien existe (elt.parent used ?)
                "citoyens" : "events"
        },
    	"projects" :{
    			"organizations" : "projects",
				"citoyens" : "projects",
				"projects" :  "contributors" // Je sais pas si ce lien existe (elt.parent used ?)
		}
	},
	getConnect : function(typeParent, typeChild ){
		return links.linksTypes[ typeParent ][ typeChild ];
	},
	disconnect : function(parentType,parentId,childId,childType,connectType, callback, linkOption, msg) {
		mylog.log("links.disconnect ", parentType,parentId,childId,childType,connectType, callback, linkOption, msg);
		var messageBox = (notNull(msg)) ? msg : trad["removeconnection"+connectType];
		$(".disconnectBtnIcon").removeClass("fa-unlink").addClass("fa-spinner fa-spin");
		bootbox.dialog({
	        onEscape: function() {
	            $(".disconnectBtnIcon").removeClass("fa-spinner fa-spin").addClass("fa-unlink");
	        },
	        message: '<div class="row">  ' +
	            '<div class="col-md-12"> ' +
	            '<span>'+messageBox+' ?</span> ' +
	            '</div></div>',
	        buttons: {
	            success: {
	                label: "Ok",
	                className: "btn-primary",
	                callback: function () {
	                	links.disconnectAjax(parentType, parentId, childId,childType,connectType, linkOption, callback);
	                }
	            },
	            cancel: {
	            	label: trad["cancel"],
	            	className: "btn-secondary",
	            	callback: function() {
	            		$(".disconnectBtnIcon").removeClass("fa-spinner fa-spin").addClass("fa-unlink");
	            	}
	            }
	        }
	    });      
	}, 
	disconnectAjax: function (parentType, parentId, childId,childType,connectType, linkOption, callback, extraParams){
		var formData = {
			"childId" : childId,
			"childType" : childType, 
			"parentType" : parentType,
			"parentId" : parentId,
			"connectType" : connectType,
		};
		if(typeof linkOption != "undefined" && linkOption)
			formData.linkOption=linkOption;
		ajaxPost(
	        null,
	        baseUrl+"/"+moduleId+"/link/disconnect",
	        formData,
	        function(data){ 
	            if ( data && data.result ) {
					typeConnect=formData.parentType;
					idConnect=formData.parentId;
					if(formData.parentId==userId){
						typeConnect=formData.childType;
						idConnect=formData.childId;
					}
					removeFloopEntity(idConnect, typeConnect, connectType);
					if(typeof extraParams != "undefined" && extraParams.showNotification && extraParams.showNotification == true)
						toastr.success("Le lien a été supprimé avec succès");
					else if(typeof extraParams == "undefined")
						toastr.success("Le lien a été supprimé avec succès");
					if (typeof callback == "function") 
						callback();
					else
						urlCtrl.loadByHash(location.hash);
				} else {
				   toastr.error("You leave succesfully");
				}
	        }
	    );
	},
	updateadminlink : function(parentType,parentId,childId,childType,connectType,isadmin,nameCitoyen = null, callBack){
		var messageBox = trad.removeadmin+ " " + nameCitoyen ;
		bootbox.dialog({
			onEscape: function() {
				$(".disconnectBtnIcon").removeClass("fa-spinner fa-spin").addClass("fa-unlink");
			},
			message: '<div class="row">  ' +
				'<div class="col-md-12"> ' +
				'<span>'+messageBox+' ?</span> ' +
				'</div></div>',
			buttons: {
				success: {
					label: "Ok",
					className: "btn-primary",
					callback: function () {
						links.updateadminlinkAjax(parentType,parentId,childId,childType,connectType,isadmin,nameCitoyen = null, callBack);
	               	}
				},
				cancel: {
					label: trad["cancel"],
					className: "btn-secondary",
					callback: function() {
						$(".disconnectBtnIcon").removeClass("fa-spinner fa-spin").addClass("fa-unlink");
					}
				}
			}
		}); 

	},
	updateadminlinkAjax : function(parentType,parentId,childId,childType,connectType,isadmin,nameCitoyen = null, callBack){
		var params = {
			parentId :parentId,
			parentType : parentType,
			childId : childId,
			childType : childType,
			connect : connectType,
			isAdmin : isadmin
		};
		ajaxPost(
			null,
			baseUrl+"/"+moduleId+"/link/updateadminlink/",
			params,
			function(data){ 
				if(typeof callBack == 'function') {
					callBack()
				} else {
					toastr.success(data.msg)
					urlCtrl.loadByHash(location.hash);
				}
			}
		); 
	},
	// Javascript function used to validate a link between parent and child (ex : member, admin...)
	validate: function(parentType, parentId, childId, childType, linkOption, callback) {
		var formData = {
			"childId" : childId,
			"childType" : childType, 
			"parentType" : parentType,
			"parentId" : parentId,
			"linkOption" : linkOption,
		};
		ajaxPost(
	        null,
	        baseUrl+"/"+moduleId+"/link/validate",
	        formData,
	        function(data){ 
	            if (data.result) {
					toastr.success(data.msg);
					if (typeof callback == "function") 
						callback(parentType, parentId, childId, childType, linkOption);
					else
						urlCtrl.loadByHash(location.hash);

				} else {
					toastr.error(data.msg);
				}
	        }
	    );  
	},
	follow : function(parentType, parentId, childId, childType, callback){
		mylog.log("follow",parentType, parentId, childId, childType, callback);
		//$(".followBtn").removeClass("fa-link").addClass("fa-spinner fa-spin");
		var formData = {
			"childId" : childId,
			"childType" : childType, 
			"parentType" : parentType,
			"parentId" : parentId,
		};
		//alert("andrana");
		ajaxPost(
	        null,
	        baseUrl+"/"+moduleId+"/link/follow",
	        formData,
	        function(data){ 
	            if(data.result){
					if (formData.parentType)
						addFloopEntity(formData.parentId, formData.parentType, data.parent, "follows");
					toastr.success(data.msg);	
					if (typeof callback == "function") 
						callback();
					else
						urlCtrl.loadByHash(location.hash);
				}
				else
					toastr.error(data.msg);
	        }
	    ); 
	},
	connect: function(parentType, parentId, childId, childType, connectType, parentName, actionAdmin, callback,title, message) {
		if(parentType=="events" && connectType=="attendee")
			$(".connectBtn").removeClass("fa-link").addClass("fa-spinner fa-spin");
		else
			$(".becomeAdminBtn").removeClass("fa-user-plus").addClass("fa-spinner fa-spin");
		titleBox="";
		messageBox = "";
		messageBox=(userId==childId) ? trad["suretojoin"+parentType] : trad.validateaddedofthisuser;
		if (connectType=="admin")
			messageBox += " " + trad["as"+connectType];
		if($.inArray(connectType, ["admin","attendee", "friend"])< 0){
			if(!notNull(title)){
				if(notNull(parentName))
					titleBox = trad["suretojoin"+parentType]+" "+parentName+" "+trad["as"+connectType]+" ?";
				else
					titleBox = trad["suretojoin"+parentType]+" "+trad["as"+connectType]+" ?";
			}else {
				if(notNull(parentName))
					titleBox = title +" "+ parentName +" ?";
				else
					titleBox = title +" ?";
			}
			if(!notNull(message)){
				messageBox = tradDynForm.wouldbecomeadmin;
			}else{
				messageBox = message;
			}
			var isChechedTrue = "";
			var isChechedFalse = "";
			if(notNull(actionAdmin) && actionAdmin) 
				isChechedTrue = 'checked="checked"'
			else
				isChechedFalse = 'checked="checked"'

			messageBox='<div class="row">  ' +
                '<div class="col-md-12"> ' +
                    '<form class="form-horizontal"> ' +
	                    '<label class="col-md-4 control-label" for="awesomeness">'+messageBox+'?</label> ' +
	                    '<div class="col-md-4"> <div class="radio"> <label for="awesomeness-0"> ' +
	                    '<input type="radio" name="askForAdmin" id="awesomeness-0" value="admin" '+ isChechedTrue + '>' +
	                    tradDynForm.yes+' </label> ' +
	                    '</div><div class="radio"> <label for="awesomeness-1"> ' +
	                    '<input type="radio" name="askForAdmin" id="awesomeness-1" value="'+connectType+'"' + isChechedFalse +'> '+tradDynForm.no+' </label> ' +
	                    '</div> ' +
	                    '</div> </div>' +
               		'</form>'+
               	'</div>'+
            '</div>';
        }
		bootbox.dialog({
                title:  titleBox,
                onEscape: function() {
	                $(".becomeAdminBtn").removeClass("fa-spinner fa-spin").addClass("fa-user-plus");
                },
                message: messageBox,
                buttons: {
                    success: {
                        label: "Ok",
                        className: "btn-primary",
                        callback: function () {
                        	var roles=null;
							var originalConnectType=connectType;
                        	if($("input[name='askForAdmin']").length){
	                           	roles = ($('#role').val() != "") ? $('#role').val() : null;
	                            connectType = $("input[name='askForAdmin']:checked").val();
	                        }
							if(connectType=="admin" && notNull(contextData) && notNull(contextData.preferences) && 
								notNull(contextData.preferences.forbiddenAdminRequest) && contextData.preferences.forbiddenAdminRequest && ((typeof isInterfaceAdmin !="undefined" && !isInterfaceAdmin) || (typeof canEdit !="undefined" && !canEdit) )){
									bootbox.dialog({
										title:  trad["notAllowedAskAdmin"],
										onEscape: function() {
											$(".becomeAdminBtn").removeClass("fa-spinner fa-spin").addClass("fa-user-plus");
										},
										message: trad["onlyAdminCanInviteAdmin"]+".",
										buttons: {
											success: {
												label: (originalConnectType!=="admin") ? trad["Join"]+" "+trad["as"+originalConnectType] : trad["Close"],
												className: "btn-primary text-white",
												callback: function () {
													if(originalConnectType!=="admin"){(originalConnectType!=="admin")
														links.connectAjax(parentType, parentId, childId, childType, originalConnectType, roles, callback);
													}
												}
											}
										}
									});				
							}else{
								links.connectAjax(parentType, parentId, childId, childType, connectType, roles, callback);
							}
                            
                        }
                    },
                    cancel: {
                    	label: trad["cancel"],
                    	className: "btn-secondary",
                    	callback: function() {
                    		$(".becomeAdminBtn").removeClass("fa-spinner fa-spin").addClass("fa-user-plus");
                    	}
                    }
                }
            }
        );
	},
	connectAjax : function(parentType, parentId, childId, childType, connectType, roles, callback,extraUrl=""){
		var formData = {
			"childId" : childId,
			"childType" : childType, 
			"parentType" : parentType,
			"parentId" : parentId,
			"connectType" : connectType
		};
		if(typeof roles != "undefined" && notNull(roles) )
			formData.roles=roles;
		ajaxPost(
	        null,
	        baseUrl+"/"+moduleId+"/link/connect"+extraUrl,
	        formData,
	        function(data){ 
	            if(data.result){
					addFloopEntity(data.parent["_id"]["$id"], data.parentType, data.parent);
					if(typeof data.msg != "undefined"){
						toastr.success(data.msg);
					}
					if (typeof callback == "function") 
						callback();
					else
						urlCtrl.loadByHash(location.hash);
				}
				else{
					if(typeof(data.type)!="undefined" && data.type=="info")
						if(typeof data.msg != "undefined"){
							toastr.success(data.msg);
						}
					else
						toastr.error(data.msg);
				}
	        }
	    ); 
	},
	updateRoles : function(childId, childType, childName, connectType, roles) {
		mylog.log("co2 links.js");
		var form = {
			saveUrl : baseUrl+"/"+moduleId+"/link/removerole/",
			dynForm : {
				jsonSchema : {
					title : tradDynForm.modifyoraddroles+"<br/>"+childName,// trad["Update network"],
					icon : "fa-key",
					onLoads : {
						sub : function(){
							$("#ajax-modal .modal-header").removeClass("bg-dark bg-purple bg-red bg-azure bg-green bg-green-poi bg-orange bg-yellow bg-blue bg-turq bg-url")
										  				  .addClass("bg-dark");
							//bindDesc("#ajaxFormModal");
						}
					},
					beforeSave : function(){
						mylog.log("beforeSave");
				    	//removeFieldUpdateDynForm(contextData.type);
				    	var r = [];
				    	$('#s2id_roles li').each(function(i,li){ 
				    		
				    		if(typeof $(li).children("div").html()!="undefined") 
								r.push($(li).children("div").html()) 
							})
				    	$("#roles").val( r.join() ) ;
				    },
					afterSave : function(data){
						mylog.dir(data);
						dyFObj.closeForm();
						if(typeof contextData.links != "undefined" && 
							typeof contextData.links[connectType] != "undefined" &&
							typeof contextData.links[connectType][childId] != "undefined" &&
							typeof contextData.links[connectType][childId].roles != "undefined"){
								var oldRoles=$.extend({}, contextData.links[connectType][childId].roles);
								contextData.links[connectType][childId].roles = data.roles ;
							}
							else
								contextData.links[connectType][childId]={"roles": data.roles};
						if(typeof contextData.rolesLists != "undefined"){
							$.each(data.roles, function(e, v){
								if(rolesList.indexOf(v) == -1)
									rolesList.push(v);
								if(typeof contextData.rolesLists[v] != "undefined")
									contextData.rolesLists[v].count++;
								else
									contextData.rolesLists[v]={count:1, label: v};
								if(typeof oldRoles != "undefined" && $.inArray(v, oldRoles) >= 1)
									oldRoles.splice(oldRoles.indexOf(v) , 1);
							});
							if(typeof oldRoles != "undefined" && notEmpty(oldRoles)){
								$.each(oldRoles, function(e, v){
									if(typeof contextData.rolesLists[v] != "undefined"){
										contextData.rolesLists[v].count--;
										if(contextData.rolesLists[v].count==0)
											delete contextData.rolesLists[v];
									}
								});
							}
						}
						if(typeof filterGroup != "undefined"){
							if(notNull(filterGroup.results.community)){
								if(typeof filterGroup.results.community.links != "undefined" && 
									typeof filterGroup.results.community.links[connectType] != "undefined" &&
									typeof filterGroup.results.community.links[connectType][childId] != "undefined" &&
									typeof filterGroup.results.community.links[connectType][childId].roles != "undefined")
										filterGroup.results.community.links[connectType][childId].roles = data.roles ;
								else
									filterGroup.results.community.links[connectType][childId]={"roles": data.roles};
							}
							//filterGroup.search.init(filterGroup);
						}//else
						pageProfil.views.directory();
						if(typeof directorySocket != "undefined"){
							directorySocket.emitEvent(wsCO, 'add_role', {newRoles: data.roles, oldRoles: oldRoles, connectType: connectType, ...data});
						}
					},
					properties : {
						contextId : dyFInputs.inputHidden(),
						contextType : dyFInputs.inputHidden(), 
						roles : {
							"inputType" : "tags",
							"label":tradDynForm["addroles"],
							"placeholder":tradDynForm["addroles"],
							"values":(costum && costum.roles)?costum.roles:(rolesList?rolesList:[])
						},//dyFInputs.tags(rolesList, tradDynForm["addroles"] , tradDynForm["addroles"], null, true),
						childId : dyFInputs.inputHidden(), 
						childType : dyFInputs.inputHidden(),
						connectType : dyFInputs.inputHidden()
					}
				}
			}
		};

		var dataUpdate = {
	        contextId : jsonHelper.pathExists('contextData.id') ? contextData.id : contextId,
	        contextType : jsonHelper.pathExists('contextData.type') ? contextData.type : contextType,
	        childId : childId,
	        childType : childType,
	        connectType : connectType,
		};

		if(notEmpty(roles))
			dataUpdate.roles = roles.split(",");
		dyFObj.openForm(form, "sub", dataUpdate);		
	},
	loadContextDataLinks : function(parentType, parentId){	 
		var formData = {
			"type" : parentType,
			"id" : parentId
		};
		ajaxPost(
	        null,
	        baseUrl+"/"+moduleId+"/link/loadcontextdatalinks",
	        formData,
	        function(data){  
	            contextData.links = data.links;
				// $.each(data.counts,function(k,v){
				// 	contextData.counts[k] = v;
				// })
	        },null,null,{async:false}
	    ); 
	},
	handleActivityPubFollow: function (action, data, payload, callback) {
		if(action == 'followProject'){
			ajaxPost(
				null,
				baseUrl + "/api/activitypub/link",
				{
				action: 'follow_project',
				payload: data,
				actor: {
					id: payload.id,
					type: payload.type,
					name: payload.name,
				},
				},
				function (data) {
					toastr.success("Le lien a été crée avec succès");
					callback();
				}
			);
		}
		else if(action == 'unfollowProject'){
		  ajaxPost(
			null,
			baseUrl + "/api/activitypub/link",
			{
			  action: 'unfollow_project',
			  payload: {objectId:data},
			  actor: {
				id: payload.id,
				type: payload.type,
				name: payload.name,
			  }
			},
			function (data) {
			  toastr.success("Le lien a été supprimé avec succès");
			  callback();
			}
		  );
		}else{
		  // special mobilizon
		  ajaxPost(
			null,
			baseUrl + "/api/activitypub/" + action,
			{ data: data, payload: payload, contextData: contextData },
			function (data) {
			  toastr.success(
				action == "joinevent"
				  ? "Vous êtes désormais participant de cet événement"
				  : "Le lien a été supprimé avec succès"
			  );
			  callback();
			}
		  );
		}
	  },
	postActivityPub: function (parentType, parentId, payload, action) {
		const actorType = getActorType();
		const actorName =
		  actorType === "group"
			? contextData.slug
			: actorType === "project"
			  ? contextData.slug
			  : contextData.username;
		ajaxPost(
		  null,
		  baseUrl + "/api/activitypub/link",
		  {
			action: action,
			payload: payload,
			actor: {
			  id: contextData.id,
			  type: actorType,
			  name: actorName,
			},
		  },
		  function (data) {
			toastr.success("Le lien a été mis à jour avec succès");
			urlCtrl.loadByHash(`#page.type.${parentType}.id.${parentId}`);
			console.clear();
		 }
		);

	  },
	  followActivityPub: function (parentType, parentId, payload, action) {
		const actorType = getActorType();
		const actorName =
		  actorType === "group"
			? contextData.slug
			: actorType === "project"
			  ? contextData.slug
			  : contextData.username;
		ajaxPost(
		  null,
		  baseUrl + "/api/activitypub/link",
		  {
			action: action,
			payload: payload,
			actor: {
			  id: contextData.id,
			  type: actorType,
			  name: actorName,
			},
		  },
		  function (data) {
			toastr.success("Le lien a été crée avec succès");
			  urlCtrl.loadByHash(`#page.type.${parentType}.id.${parentId}`);
			  console.clear();
		  }
		);
	  },
	  unfollowActivityPub: function (parentType, parentId, payload, action) {
		const actorType = getActorType();
		const actorName =
		  actorType === "group"
			? contextData.slug
			: actorType === "project"
			  ? contextData.slug
			  : contextData.username;
		ajaxPost(
		  null,
		  baseUrl + "/api/activitypub/link",
		  {
			action: action,
			payload: payload,
			actor: {
			  id: contextData.id,
			  type: actorType,
			  name: actorName,
			},
		  },
		  function (data) {
			toastr.success("Le lien a été supprimé avec succès");
			  urlCtrl.loadByHash(`#page.type.${parentType}.id.${parentId}`);
			  console.clear();
        
		  }
		);
	  },
	  connectActivityPub: function (
		parentType,
		parentId,
		objectId,
		childId,
		childType,
		connectType,
		parentName,
		actionAdmin,
		callback,
		title,
		message
	  ) {
		if (parentType == "events" && connectType == "attendee")
		  $(".connectBtn").removeClass("fa-link").addClass("fa-spinner fa-spin");
		else
		  $(".becomeAdminBtn")
			.removeClass("fa-user-plus")
			.addClass("fa-spinner fa-spin");
		titleBox = "";
		messageBox = "";
		messageBox =
		  userId == childId
			? trad["suretojoin" + parentType]
			: trad.validateaddedofthisuser;
		if (connectType == "admin") messageBox += " " + trad["as" + connectType];
		if ($.inArray(connectType, ["admin", "attendee", "friend"]) < 0) {
		  if (!notNull(title)) {
			if (notNull(parentName))
			  titleBox =
				trad["suretojoin" + parentType] +
				" " +
				parentName +
				" " +
				trad["as" + connectType] +
				" ?";
			else
			  titleBox =
				trad["suretojoin" + parentType] +
				" " +
				trad["as" + connectType] +
				" ?";
		  } else {
			if (notNull(parentName)) titleBox = title + " " + parentName + " ?";
			else titleBox = title + " ?";
		  }
		  if (!notNull(message)) {
			messageBox = tradDynForm.wouldbecomeadmin;
		  } else {
			messageBox = message;
		  }
		  messageBox =
			'<div class="row">  ' +
			'<div class="col-md-12"> ' +
			'<form class="form-horizontal"> ' +
			'<label class="col-md-4 control-label" for="awesomeness">' +
			messageBox +
			"?</label> " +
			'<div class="col-md-4"> <div class="radio"> <label for="awesomeness-0"> ' +
			'<input type="radio" name="askForAdmin" id="awesomeness-0" value="admin"> ' +
			tradDynForm.yes +
			" </label> " +
			'</div><div class="radio"> <label for="awesomeness-1"> ' +
			'<input type="radio" name="askForAdmin" id="awesomeness-1" value="' +
			connectType +
			'" checked="checked"> ' +
			tradDynForm.no +
			" </label> " +
			"</div> " +
			"</div> </div>" +
			"</form>" +
			"</div>" +
			"</div>";
		}
		bootbox.dialog({
		  title: titleBox,
		  onEscape: function () {
			$(".becomeAdminBtn")
			  .removeClass("fa-spinner fa-spin")
			  .addClass("fa-user-plus");
		  },
		  message: messageBox,
		  buttons: {
			success: {
			  label: "Ok",
			  className: "btn-primary",
			  callback: function () {
				var roles = null;
				if ($("input[name='askForAdmin']").length) {
				  roles = $("#role").val() != "" ? $("#role").val() : null;
				  connectType = $("input[name='askForAdmin']:checked").val();
				}
				console.log(
				  "roles",
				  roles,
				  "connectType",
				  connectType,
				  "objectId",
				  objectId
				);
				links.followActivityPub(
				  parentType,
				  parentId,
				  {

					objectId: objectId,
					connectType: connectType =="admin" ? "contribute_as_admin" : connectType,
				  },
				  connectType == "admin" ? "contribute_as_admin": connectType
				);
			  },
			},
			cancel: {
			  label: trad["cancel"],
			  className: "btn-secondary",
			  callback: function () {
				$(".becomeAdminBtn")
				  .removeClass("fa-spinner fa-spin")
				  .addClass("fa-user-plus");
			  },
			},
		  },
		});
	  },
	  disconnectActivityPub: function (
		parentType,
		parentId,
		childId,
		childType,
		connectType,
		callback,
		linkOption,
		msg
	  ) {
		mylog.log(
		  "links.disconnect ap",
		  parentType,
		  parentId,
		  childId,
		  childType,
		  connectType,
		  callback,
		  linkOption,
		  msg
		);

		var messageBox = notNull(msg)
		  ? msg
		  : trad["removeconnection" + connectType];
		$(".disconnectBtnIcon")
		  .removeClass("fa-unlink")
		  .addClass("fa-spinner fa-spin");
		bootbox.dialog({
		  onEscape: function () {
			$(".disconnectBtnIcon")
			  .removeClass("fa-spinner fa-spin")
			  .addClass("fa-unlink");
		  },
		  message:
			'<div class="row">  ' +
			'<div class="col-md-12"> ' +
			"<span>" +
			messageBox +
			" ?</span> " +
			"</div></div>",
		  buttons: {
			success: {
			  label: "Ok",
			  className: "btn-primary",
			  callback: function () {
				links.postActivityPub(
				  parentType,
				  parentId,
				  {
					parentId: parentId,
					objectId: childType,
					targetId: childId,
				  },
				 "delete_contribution"
				);
			  },
			},
			cancel: {
			  label: trad["cancel"],
			  className: "btn-secondary",
			  callback: function () {
				$(".disconnectBtnIcon")
				  .removeClass("fa-spinner fa-spin")
				  .addClass("fa-unlink");
			  },
			},
		  },
		});
	  },
	  updateRolesActivityPub: function (
		childId,
		childType,
		childName,
		connectType,
		roles
	  ) {
		mylog.log("co2  aplinks.js");
		var form = {
		  saveUrl: baseUrl + "/api/activitypub/manageroles",
		  dynForm: {
			jsonSchema: {
			  title: tradDynForm.modifyoraddroles + "<br/>" + childName, // trad["Update network"],
			  icon: "fa-key",
			  onLoads: {
				sub: function () {
				  $("#ajax-modal .modal-header")
					.removeClass(
					  "bg-dark bg-purple bg-red bg-azure bg-green bg-green-poi bg-orange bg-yellow bg-blue bg-turq bg-url"
					)
					.addClass("bg-dark");
				  //bindDesc("#ajaxFormModal");
				},
			  },
			  beforeSave: function () {
				mylog.log("beforeSave");
				//removeFieldUpdateDynForm(contextData.type);
				var r = [];
				$("#s2id_roles li").each(function (i, li) {
				  if (typeof $(li).children("div").html() != "undefined")
					r.push($(li).children("div").html());
				});
				$("#roles").val(r.join());
			  },
			  afterSave: function (data) {
				mylog.dir(data);
				dyFObj.closeForm();
				if (
				  typeof contextData.links != "undefined" &&
				  typeof contextData.links[connectType] != "undefined" &&
				  typeof contextData.links[connectType][childId] != "undefined" &&
				  typeof contextData.links[connectType][childId].roles !=
				  "undefined"
				) {
				  var oldRoles = $.extend(
					{},
					contextData.links[connectType][childId].roles
				  );
				  contextData.links[connectType][childId].roles = data.roles;
				} else
				  contextData.links[connectType][childId] = { roles: data.roles };
				if (typeof contextData.rolesLists != "undefined") {
				  $.each(data.roles, function (e, v) {
					if(rolesList.indexOf(v) == -1)
						rolesList.push(v);
					if (typeof contextData.rolesLists[v] != "undefined")
					  contextData.rolesLists[v].count++;
					else contextData.rolesLists[v] = { count: 1, label: v };
					if (
					  typeof oldRoles != "undefined" &&
					  $.inArray(v, oldRoles) >= 1
					)
					  oldRoles.splice(oldRoles.indexOf(v), 1);
				  });
				  if (typeof oldRoles != "undefined" && notEmpty(oldRoles)) {
					$.each(oldRoles, function (e, v) {
					  if (typeof contextData.rolesLists[v] != "undefined") {
						contextData.rolesLists[v].count--;
						if (contextData.rolesLists[v].count == 0)
						  delete contextData.rolesLists[v];
					  }
					});
				  }
				}
				if (typeof filterGroup != "undefined") {
				  if (notNull(filterGroup.results.community)) {
					if (
					  typeof filterGroup.results.community.links != "undefined" &&
					  typeof filterGroup.results.community.links[connectType] !=
					  "undefined" &&
					  typeof filterGroup.results.community.links[connectType][
					  childId
					  ] != "undefined" &&
					  typeof filterGroup.results.community.links[connectType][
						childId
					  ].roles != "undefined"
					)
					  filterGroup.results.community.links[connectType][
						childId
					  ].roles = data.roles;
					else
					  filterGroup.results.community.links[connectType][childId] = {
						roles: data.roles,
					  };
				  }
				  //filterGroup.search.init(filterGroup);
				} //else
				pageProfil.views.directory();
			  },
			  properties: {
				contextId: dyFInputs.inputHidden(),
				contextType: dyFInputs.inputHidden(),
				roles: {
				  inputType: "tags",
				  label: tradDynForm["addroles"],
				  placeholder: tradDynForm["addroles"],
				  values: rolesList,
				}, //dyFInputs.tags(rolesList, tradDynForm["addroles"] , tradDynForm["addroles"], null, true),
				objectId: dyFInputs.inputHidden(),
				targetId: dyFInputs.inputHidden(),
				connectType: dyFInputs.inputHidden(),
			  },
			},
		  },
		};

		var dataUpdate = {
		  contextId: contextData.id,
		  contextType: contextData.type,
		  objectId: childId,
		  targetId: childType,
		  connectType: connectType,
		};

		if (notEmpty(roles)) dataUpdate.roles = roles.split(",");
		dyFObj.openForm(form, "sub", dataUpdate);
	  },
	  validateActivityPub: function (
		parentType,
		parentId,
		objectId,
		actorId,
		actorType,
		linkOption,
		callback
	  ) {
		var formData = {
		  objectId: objectId,
		  targetId: actorId,
		  connectType: linkOption,
		};
		links.postActivityPub(parentType, parentId, formData, linkOption);
	  },
	  quitPageActivityPub: function (parentType, parentId, objectId) {
		var formData = {
		  objectId: objectId,
		};
		links.postActivityPub(parentType, parentId, formData, "leave_contribution");
	  },
	  acceptInvitationActivityPub: function (
		parentType,
		parentId,
		objectId,
		actorId,
		actorType,
		linkOption,
		callback
	  ) {
		var formData = {
		  objectId: objectId,
		  connectType: "acceptInvitation",
		};
		links.postActivityPub(parentType,
		  parentId, formData,  "acceptInvitation");
	  },
	  refusedInvitationActivityPub: function (
		parentType,
		parentId,
		objectId,
		actorId,
		actorType,
		linkOption,
		callback
	  ) {
		var formData = {
		  objectId: objectId,
		};
		links.postActivityPub(parentType, parentId, formData, "reject_contribution");

	  },
	  becomeAdministratorActivityPub: function (
		parentType,
		parentId,
		objectId,
		actorId,
		actorType,
		linkOption,
		callback
	  ) {
		var formData = {
		  objectId: objectId,
		  connectType: "contribute_as_admin",
		};
		links.followActivityPub(parentType,parentId, formData, "contribute_as_admin");
	  },
}