var cnbListBlocks = {
    list:[
        {
            label:"Page",
            tagName:"mj-body",
            attributes:[],
            inputs:[],
            data:{
                tagName: "mj-body",
                attributes: {
                    "background-color": "#F2F2F2"
                },
                children:[
                    {
                        tagName:"mj-section",
                        attributes:{},
                        children:[
                            {
                                tagName:"mj-column",
                                attributes:{
                                    "background-column":"white"
                                }
                            }
                        ]
                    }
                ]
            }
        },
        {
            label:"Section",
            image:"section.png",
            tagName:"mj-section",
            attributes:[
                "background-color", "background-position", "background-position-x", "background-position-y",
                "background-repeat", "background-size", "background-url", "border", "border-bottom",
                "border-left", "border-radius", "border-right", "border-top", "direction", "full-width",
                "padding", "padding-bottom", "padding-left", "padding-right", "padding-top", "text-align"
            ],
            data:{
                tagName:"mj-section",
                attributes:{
                    padding:"0px"
                },
                children:[
                    {
                        tagName:"mj-column",
                        attributes:{
                            "padding":"20px"
                        }
                    }
                ]
            },
            inputs:[
                "textAlign",
                "border",
                "borderRadius",
                "padding",
                "direction",
                "backgroundColor",
                {
                    type:"imagePicker",
                    options:{
                        name:"background-url"
                    }
                },
                "backgroundSize",
                "backgroundRepeat"
            ]
        },
        {
            label:"Articles",
            image:"section.png",
            tagName:"mj-wrapper",
            attributes:[],
            data:{
                tagName:"mj-wrapper",
                attributes:{
                    padding:"0px"
                },
                children:[
                    /* {
                        tagName:"mj-section",
                        attributes:{
                            padding:"0px"
                        },
                        children:[
                            {
                                tagName:"mj-column",
                                attributes:{
                                    "padding":"20px"
                                }
                            }
                        ]
                    } */
                ]
            },
            inputs:[]
        },
        {
            label:"Colonne",
            image:"column.png",
            tagName:"mj-column",
            attributes:[
                "background-color", "inner-background-color", "border", "border-bottom", "border-left",
                "border-right", "border-top", "border-radius", "inner-border", "inner-border-bottom",
                "inner-border-left", "inner-border-right", "inner-border-top", "inner-border-radius",
                "width", "vertical-align", "padding", "padding-top", "padding-bottom", "padding-left",
                "padding-right"
            ],
            inputs:[
                "width",
                "padding",
                "verticalAlign",
                "backgroundColor",
                "border",
                "borderRadius"
            ],
            data:{
                tagName:"mj-column",
                attributes:{
                    "padding":"20px"
                },
                children:[]
            }
        },
        {
            label:"Element article",
            tagName:"article-element",
            image:"article-element.png",
            attributes:[],
            inputs:[],
            data:{
                "tagName": "mj-section",
                "attributes": {
                    "padding": "0px",
                    "text-align": "left",
                    "background-color": "white",
                    "border-bottom": "1px solid #eeeeee"
                },
                "children": [
                    {
                        "tagName": "mj-column",
                        "attributes": {
                            "padding": "20px",
                            "width": "40%"
                        },
                        "children": [
                            {
                                "tagName": "mj-image",
                                "attributes": {
                                    "align": "center",
                                    "width": "200px",
                                    "height": "150px",
                                    "src": "%image%",
                                    "padding": "0px   "
                                }
                            }
                        ]
                    },
                    {
                        "tagName": "mj-column",
                        "attributes": {
                            "padding": "20px 20px 0px 0px",
                            "width": "60%"
                        },
                        "children": [
                            {
                                "tagName": "mj-text",
                                "attributes": {
                                    "color": "#555",
                                    "font-size": "16px",
                                    "align": "left",
                                    "padding": "0px   ",
                                    "line-height": "22px",
                                    "font-weight": "bold"
                                },
                                "content": "%content%"
                            },
                            {
                                "tagName": "mj-text",
                                "attributes": {
                                    "color": "#5b5b5b",
                                    "font-size": "14px",
                                    "align": "left",
                                    "padding": "10px 0px 0px "
                                },
                                "content": "%date%"
                            },
                            {
                                "tagName": "mj-button",
                                "attributes": {
                                    "align": "right",
                                    "background-color": "#9fbd38",
                                    "color": "white",
                                    "inner-padding": "10px 15px 10px 15px",
                                    "padding": "10px 0px 0px ",
                                    "border-radius": "30px",
                                    "href": "%link%"
                                },
                                "content": "Voir plus"
                            }
                        ]
                    },
                    {
                        "tagName": "mj-column",
                        "attributes": {
                            "padding": "0px 0px 0px 0px",
                            "width": "100%"
                        },
                        "children": []
                    }
                ]
            }
        },
        {
            label:"Text",
            image:"text.png",
            tagName:"mj-text",
            attributes:[
                "color", "font-family", "font-size", "font-style", "font-weight", "line-height",
                "letter-spacing", "height", "text-decoration", "text-transform", "align",
                "container-background-color", "padding", "padding-top", "padding-bottom",
                "padding-left", "padding-right"
            ],
            inputs:[
                {
                    type:"textarea",
                    options:{
                        name:"content",
                        payload:{
                            dynamicContent:true
                        }
                    }
                },
                "color",
                {
                    type:"select",
                    options:{
                        name:"font-family",
                        options:[
                            {
                                value:"Ubuntu",
                                label:"Ubuntu"
                            },
                            {
                                value:"Helvetica",
                                label:"Helvetica"
                            },
                            {
                                value:"Arial",
                                label:"Arial"
                            },
                            {
                                value:"sans-serif",
                                label:"sans-serif"
                            }
                        ]
                    }
                },
                "fontSize",
                "fontStyle",
                "fontWeight",
                "lineHeight",
                "letterSpacing",
                "height",
                "textDecoration",
                "textTransform",
                "align",
                {
                    type:"color",
                    options:{
                        name:"container-background-color"
                    }
                },
                "padding"
            ],
            data:{
                tagName:"mj-text",
                attributes:{
                    "color":"#555",
                    "font-size":"14px",
                    "align":"center"
                },
                content:"Ecrire votre text ici !!!"
            }
        },
        {
            label:"Bouton",
            image:"button.png",
            tagName:"mj-button",
            attributes:[
                "align", "background-color", "border", "border-bottom", "border-left", "border-radius",
                "border-right", "border-top", "color", "container-background-color", "font-family",
                "font-size", "font-style", "font-weight", "height", "href", "inner-padding", "letter-spacing",
                "line-height", "padding", "padding-bottom", "padding-left", "padding-right", "padding-top",
                "rel", "target", "text-align", "text-decoration", "text-transform", "title", "vertical-align",
                "width"
            ],
            inputs:[
                {
                    type:"textarea",
                    options:{
                        name:"content"
                    }
                },
                {
                    type:"inputSimple",
                    options:{
                        name:"href",
                        payload:{
                            dynamicContent:true
                        }
                    }
                },
                "width",
                "height",
                "color",
                {
                    type:"select",
                    options:{
                        name:"font-family",
                        options:[
                            {
                                value:"Ubuntu",
                                label:"Ubuntu"
                            },
                            {
                                value:"Helvetica",
                                label:"Helvetica"
                            },
                            {
                                value:"Arial",
                                label:"Arial"
                            },
                            {
                                value:"sans-serif",
                                label:"sans-serif"
                            }
                        ]
                    }
                },
                "fontSize",
                "fontStyle",
                "fontWeight",
                "align",
                "textDecoration",
                "textTransform",
                "letterSpacing",
                "lineHeight",
                "border",
                "borderRadius",
                "padding",
                {
                    type:"padding",
                    options:{
                        name:"inner-padding"
                    }
                },
                "backgroundColor",
                {
                    type:"color",
                    options:{
                        name:"container-background-color"
                    }
                }
            ],
            data:{
                tagName:"mj-button",
                attributes:{
                    "align":"center",
                    "background-color":"#9fbd38",
                    "color":"white",
                    "inner-padding": "10px 15px"
                },
                content:"Mon bouton"
            }
        },
        {
            label:"Image",
            image:"image.png",
            tagName:"mj-image",
            attributes:[
                "align", "alt", "border", "border-top", "border-bottom", "border-left", "border-right",
                "border-radius", "container-background-color", "fluid-on-mobile", "height", "href",
                "name", "padding", "padding-bottom", "padding-left", "padding-right", "padding-top",
                "rel", "sizes", "src", "srcset", "target", "title", "usemap", "width"
            ],
            data:{
                tagName:"mj-image",
                attributes:{
                    "align":"center",
                    "width":"200px",
                    "height":"200px",
                    "src":"assets/images/image-placeholder.jpg"
                }
            },
            inputs:[
                {
                    type:"imagePicker",
                    options:{
                        name:"src",
                        payload:{
                            dynamicContent:true
                        }
                    }
                },
                {
                    type:"inputSimple",
                    options:{
                        name:"href"
                    }
                },
                "width",
                "height",
                "align",
                "padding",
                "border",
                "borderRadius",
                {
                    type:"color",
                    options:{
                        name:"container-background-color"
                    }
                }
            ]
        },
        {
            label:"Separateur",
            image:"divider.png",
            tagName:"mj-divider",
            attributes:[
                "border-color","border-style","border-width","container-background-color","padding",
                "padding-bottom","padding-left","padding-right","padding-top","width","align"
            ],
            inputs:[
                "borderColor",
                "borderStyle",
                "borderWidth",
                "width",
                "padding",
                "align"
            ],
            data:{
                tagName:"mj-divider",
                attributes:{}
            }
        },
        {
            label:"Espacement",
            image:"spacer.png",
            tagName:"mj-spacer",
            attributes:[
                "container-background-color", "height", "padding", "padding-bottom", "padding-left", "padding-right",
                "padding-top"
            ],
            data:{
                tagName:"mj-spacer",
                attributes:{
                    "height":"50px"
                }
            }
        },
        {
            label:"Social",
            image:"social.png",
            tagName:"mj-social",
            attributes:[
                "align", "border-radius", "color", "container-background-color",
                "font-family", "font-size", "font-style", "font-weight", "icon-height",
                "icon-size", "inner-padding", "line-height", "mode", "padding", "padding-bottom",
                "padding-left", "padding-right", "padding-top", "icon-padding", "text-padding", "text-decoration"
            ],
            input:[
                {
                    type:"cnbInputSelect",
                    option:{
                        name:"mode",
                        label:"Orientation",
                        options:[
                            {
                                value:"vertical",
                                label:"Vertical"
                            },
                            {
                                value:"horizontal",
                                label:"Horizontal"
                            }
                        ]
                    }
                },
                {
                    type:"cnbInputAlign",
                    option:{
                        name:"align",
                        label:"Alignement"
                    }
                },
                {
                    type:"cnbInputColor",
                    option:{
                        name:"color",
                        label:"Couleur du text"
                    }
                },
                {
                    type:"cnbInputSimple",
                    option:{
                        name:"font-size",
                        label:"Taille de police"
                    }
                },
                {
                    type:"cnbInputSelect",
                    option:{
                        name:"font-family",
                        label:"Police",
                        options:[
                            {
                                value:"Ubuntu",
                                label:"Ubuntu"
                            },
                            {
                                value:"Helvetica",
                                label:"Helvetica"
                            },
                            {
                                value:"Arial",
                                label:"Arial"
                            },
                            {
                                value:"sans-serif",
                                label:"sans-serif"
                            }
                        ]
                    }
                },
                {
                    type:"cnbInputSelect",
                    option:{
                        name:"font-style",
                        label:"Style du police",
                        options:[
                            {
                                value:"normal",
                                label:"Normal"
                            },
                            {
                                value:"italic",
                                label:"Italic"
                            },
                            {
                                value:"oblique",
                                label:"Oblique"
                            }
                        ]
                    }
                },
                {
                    type:"cnbInputSelect",
                    option:{
                        name:"font-weight",
                        label:"Epaisseur du police",
                        options:[
                            {
                                value:"normal",
                                label:"Normal"
                            },
                            {
                                value:"bold",
                                label:"Gras"
                            }
                        ]
                    }
                },
                {
                    type:"cnbInputSelect",
                    option:{
                        name:"text-decoration",
                        label:"Decoration du text",
                        options:[
                            {
                                value:"underline",
                                label:"Souligner"
                            },
                            {
                                value:"overline",
                                label:"Surligner"
                            },
                            {
                                value:"line-through",
                                label:"Ligne à travers"
                            },
                            {
                                value:"none",
                                label:"Rien"
                            }
                        ]
                    }
                },
                {
                    type:"cnbInputSimple",
                    option:{
                        name:"line-height",
                        label:"Hauteur de la ligne"
                    }
                },
                {
                    type:"cnbInputSimple",
                    option:{
                        name:"icon-height",
                        label:"Hauteur de l'icon"
                    }
                },
                {
                    type:"cnbInputSimple",
                    option:{
                        name:"icon-size",
                        label:"Taille de l'icon"
                    }
                },
                {
                    type:"cnbInputPadding",
                    option:{
                        name:"icon-padding",
                        label:"Rembourrage de l'icon"
                    }
                },
                {
                    type:"cnbInputPadding",
                    option:{
                        name:"padding",
                        label:"Rembourrage"
                    }
                },
                {
                    type:"cnbInputPadding",
                    option:{
                        name:"padding",
                        label:"Rembourrage interieur"
                    }
                },
                {
                    type:"cnbInputSimple",
                    option:{
                        name:"border-radius",
                        label:"Rayon du bordure"
                    }
                },
                {
                    type:"cnbInputColor",
                    option:{
                        name:"container-background-color",
                        label:"Couleur de fond de conteneur"
                    }
                }
            ],
            data:{
                tagName:"mj-social",
                attributes:{
                    "font-size":"15px",
                    "icon-size":"30px",
                    "mode":"horizontal"
                },
                children:[
                    {
                        tagName:"mj-social-element",
                        attributes:{
                            "name":"facebook",
                            "href":"#",
                        },
                        content:"Facebook"
                    },
                    {
                        tagName:"mj-social-element",
                        attributes:{
                            "name":"twitter",
                            "href":"#"
                        },
                        content:"Twitter"
                    }
                ]
            }
        },
        {
            label:"Element social",
            tagName:"mj-social-element",
            image:"social-element.png",
            attributes:[],
            inputs:[
                {
                    type:"select",
                    options:{
                        name:"name",
                        options:["facebook", "twitter", "google", "pinterest", "linkedin", "tumblr", "xing"]
                    }
                },
                {
                    type:"textarea",
                    options:{
                        name:"content"
                    }
                },
                {
                    type:"inputSimple",
                    options:{
                        name:"href"
                    }
                },
                "backgroundColor",
                {
                    type:"inputSimple",
                    options:{
                        name:"icon-size"
                    }
                },
                "borderRadius",
                "color",
                "fontSize",
                "fontWeight",
                "padding"
            ],
            data:{
                tagName:"mj-social-element",
                attributes:{
                    name:"facebook",
                    href:"#"
                },
                content:"Facebook"
            }
        }
    ],
    getBlock(tagName){
        if(tagName === "mj-head")
            return {
                label:"Entête",
                tagName:"mj-head"
            }
        var block = null
        for(var i=0; i<this.list.length; i++){
            if(this.list[i].tagName === tagName){
                block = this.list[i]
                break;
            }
        }
        return JSON.parse(JSON.stringify(block));
    }
}

var defaultPageContent = {
    tagName: "mjml",
    attributes: {},
    children:[
        {
            tagName: "mj-head",
            children:[
                {
                    tagName:"mj-style",
                    content:`
                        img{
                            object-fit:cover !important;
                        }
                    `
                }
            ],
            attributes: {},
        },
        {
            tagName: "mj-body",
            attributes: {
                "background-color": "#F2F2F2"
            },
            children:[]
        }
    ]
}