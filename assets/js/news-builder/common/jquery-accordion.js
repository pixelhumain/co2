$.fn.accordion = function(){
    return this.each(function(){
        var $content = $(this).find(".accordion-content")
        var $button = $(this).find(".accordion-button")
        var $this = $(this)

        $content.attr("data-height", $content.height())

        if(!$(this).hasClass("active"))
            $content.css({height:0})
        
        $button.click(function(){
            $this.toggleClass("active")

            if($this.hasClass("active"))
                $content.animate({height:$content.data("height")}, 300)
            else
                $content.animate({height:0}, 300)
        })

        return this;
    })
}