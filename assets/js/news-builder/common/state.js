function PubSub(){
    this.events = {}

    this.subscribe = function(eventName, handler){
        if(this.events[eventName])
            this.events[eventName].push(handler)
        else
            this.events[eventName] = [handler]

        return { eventName, handler }
    }

    this.unsubscribe = function(subscriber){
        if(this.events[subscriber.eventName]){
            this.events[eventName] = this.events[eventName].filter((handler) => handler !== subscriber.func);
        }
    }

    this.publish = function(eventName, payload){
        var handlers = this.events[eventName] || []
        handlers.forEach(function(handler){
            handler.apply(null, [payload])
        })
    }
}

var cnbState = {
    _pageContent:{},
    _blockPathRefs:{},
    _selectedBlock:null,
    _ressources:{
        images:[],
        articles:[]
    },
    _pubSub: new PubSub(),
    _utils:{
        refreshBlockPathRefs(){
            var pathRefs = {}
            var addPath = function(data, currentPath){
                var ref = generateUUID()
                pathRefs[ref] = currentPath
                if(data.children){
                    data.children.forEach(function(child, index){
                        addPath(child, currentPath.concat(['children', index]))
                    })
                }
            }
            addPath(cnbState._pageContent, [])
            cnbState._blockPathRefs = pathRefs;
            cnbState._selectedBlock = null;
        }
    },
    _init(){
        this.setters.setPageContent(defaultPageContent)
    },
    setters:{
        setPageContent(pageContent){
            var prevSelectedBlockPath = cnbState._blockPathRefs[cnbState._selectedBlock];

            cnbState._pageContent = pageContent;
            cnbState._utils.refreshBlockPathRefs()

            if(prevSelectedBlockPath)
                cnbState._selectedBlock = cnbState.getters.getBlockRef(prevSelectedBlockPath);

            cnbState._pubSub.publish("block-added", {})
        },
        addBlock(block, target){
            var pageContent = cloneObject(cnbState._pageContent),
                targetBlockPath = cnbState._blockPathRefs[target.ref],
                tagetPosition = target.position,
                addedBlockPath = [];

            switch(tagetPosition){
                case "in":
                    targetBlockPath.push('children')
                    var children = _.get(pageContent, targetBlockPath, [])
                    children.push(block)
                    pageContent = _.set(pageContent, targetBlockPath, children)
                    addedBlockPath = targetBlockPath.concat([ children.length - 1 ])
                break;
                case "bottom":
                    var targetIndex = targetBlockPath[targetBlockPath.length - 1] + 1,
                        targetBlockPath = targetBlockPath.slice(0, targetBlockPath.length - 1),
                        children = _.get(pageContent, targetBlockPath, []);

                    children.splice(targetIndex, 0, block)
                    pageContent = _.set(pageContent, targetBlockPath, children)
                    addedBlockPath = targetBlockPath.concat([ targetIndex ])
                break;
            }

            cnbState.setters.setPageContent(pageContent)
            cnbState._pubSub.publish("block-added", { ref: cnbState.getters.getBlockRef(addedBlockPath) })
        },
        deleteBlock(ref){
            var path = cnbState._blockPathRefs[ref]
            if(path){
                var pageContent = objectArrayUnset(cnbState._pageContent, path)
                cnbState.setters.setPageContent(pageContent)
                cnbState._pubSub.publish("block-removed", {})
            }
        },
        selectBlock(ref){
            if(cnbState._blockPathRefs[ref]){
                cnbState._selectedBlock = ref;
            }else{
                cnbState._selectedBlock = null;
            }
            cnbState._pubSub.publish("block-selected", { ref:ref })
        },
        selectParentBlock(ref){
            if(cnbState._blockPathRefs[ref]){
                var parentPath = cnbState._blockPathRefs[ref].slice(0, -1).slice(0, -1)
                    parentRef = cnbState.getters.getBlockRef(parentPath);
                
                cnbState._selectedBlock = parentRef;
                cnbState._pubSub.publish("block-selected", { ref:parentRef })
            }
        },
        setBlockProperty(name, value, fromCustomizer=true){
            if(cnbState._selectedBlock){
                var path = cnbState._blockPathRefs[cnbState._selectedBlock],
                    pageContent = cloneObject(cnbState._pageContent);

                if(value){
                    if(name === "content")
                        pageContent = _.set(pageContent, path.concat(['content']), value)
                    else
                        pageContent = _.set(pageContent, path.concat(['attributes', name]), value)   
                }else{
                    _.unset(pageContent, path.concat(['attributes', name]))
                }

                this.setPageContent(pageContent);
                cnbState._pubSub.publish("block-change", { ref: cnbState._selectedBlock, name, value, fromCustomizer:fromCustomizer })
            }
        },
        setBlockProperties(properties){
            if(cnbState._selectedBlock){
                var path = cnbState._blockPathRefs[cnbState._selectedBlock],
                    pageContent = cloneObject(cnbState._pageContent);

                properties.forEach(function(property){
                    var name = property.name,
                        value = property.value;
                    if(value){
                        if(name === "content")
                            pageContent = _.set(pageContent, path.concat(['content']), value)
                        else
                            pageContent = _.set(pageContent, path.concat(['attributes', name]), value)   
                    }else{
                        _.unset(pageContent, path.concat(['attributes', name]))
                    }
                })

                this.setPageContent(pageContent);
                cnbState._pubSub.publish("block-change", { ref: cnbState._selectedBlock })
            }
        },
        ressources:{
            articles:{
                set(ids){
                    cnbState._ressources.articles = ids
                    cnbState._pubSub.publish("article-added", ids)
                },
                remove(id){
                    cnbState._ressources.articles = cnbState._ressources.articles.filter(function(currentId){
                        return currentId !== id;
                    })
                }
            },
            images:{
                set(images){
                    cnbState._ressources.images = images;
                },
                add(image){
                    cnbState._ressources.images.push(image);
                },
                remove(id){
                    cnbState._ressources.images = cnbState._ressources.images.filter(function(image){
                        return id !== image.id
                    })
                }
            }
        }
    },
    getters:{
        getPageContent(editMode=true){
            var pageContent = cloneObject(cnbState._pageContent)

            if(!editMode)
                return pageContent;

            var addAttributes = function(content, path=[]){
                if(!content.attributes)
                    content.attributes = {}
                content.attributes["css-class"] = `cnb-block cnb-block-tagName-${content.tagName} cnb-block-ref-${cnbState.getters.getBlockRef(path)}`;
                if(content.children){
                    content.children.forEach(function(child, index){
                        addAttributes(child, path.concat(["children", index]))
                    })
                }
                return content;
            }

            return addAttributes(pageContent);
        },
        getBlockRef(path){
            var refs = Object.keys(cnbState._blockPathRefs)
            for(var i=0; i<refs.length;i++){
                var ref = refs[i]
                if(path.join("") === cnbState._blockPathRefs[ref].join(""))
                    return ref;
            }
            return null;
        },
        getBlockPath(ref){
            return cnbState._blockPathRefs[ref];
        },
        getBlock(ref){
            var path = cnbState._blockPathRefs[ref]
            if(!path)
                return null;

            var blockData = _.get(cnbState._pageContent, path, {}),
                block = cnbListBlocks.getBlock(blockData.tagName)

            block.data = blockData;

            return block;
        },
        getSelectedBlockRef(){
            return cnbState._selectedBlock;
        },
        isInsideArticles(ref){
            var path = cnbState._blockPathRefs[ref],
                isInside = false;

            for(var i=0; i<path.length; i++){
                var block = _.get(cnbState._pageContent, path.slice(0, i+1), {})
                if(block.tagName === "mj-wrapper"){
                    isInside = true;
                    break;
                }
            }

            return isInside;
        },
        buildPageContent(callback, toHTML=false){
            var getValueByVariableContent = function(content, newsItem){
                var variableName = content.match(/%(.+)%/)[1]
                var value = ""

                switch(variableName){
                    case "content":
                        value = newsItem.text
                        if(value.length > 145){
                            value = value.split("").slice(0, 145).join("") + "..."
                        }
                        break;
                    case "date":
                        value = moment.unix(newsItem.date.sec).format("ddd DD MMMM YYYY. hh:mm")
                        break;
                    case "author_name":
                        value = newsItem.author.name
                        break;
                    case "author_profil":
                        value = newsItem.author.profilThumbImageUrl
                        break;
                    case "link":
                        value = `${baseUrl}/#page.type.news.id.${newsItem._id.$id}`
                        break;
                    case "image":
                        if(newsItem.mediaImg && newsItem.mediaImg.images && newsItem.mediaImg.images.length > 0)
                            value = `${baseUrl}/${newsItem.mediaImg.images[0].imagePath}`
        
                        break;
                }
                return value;
            }

            var assignVariable = function(mjmlObject, newsItem){
                if(mjmlObject.content && /%(.+)%/.test(mjmlObject.content)){
                    mjmlObject.content = getValueByVariableContent(mjmlObject.content, newsItem)
                }
                
                if(mjmlObject.attributes){
                    Object.keys(mjmlObject.attributes).forEach(function(attr){
                        if(/%(.+)%/.test(mjmlObject.attributes[attr])){
                            mjmlObject.attributes[attr] = getValueByVariableContent(mjmlObject.attributes[attr], newsItem)
                        }
                    })
                }
                
                if(mjmlObject.children){
                    mjmlObject.children.forEach(function(child){
                        assignVariable(child, newsItem)
                    })
                }
                
                return mjmlObject;
            }

            var build = function(mjmlObject, news){
                if(mjmlObject.tagName === "mj-wrapper"){
                    var templateItem = {
                        tagName:"mj-wrapper",
                        attributes:{
                            padding:"0px"
                        },
                        children: mjmlObject.children
                    }
                    
                    mjmlObject.children = []
        
                    news.forEach(function(newsItem){
                        mjmlObject.children.push(assignVariable(cloneObject(templateItem), newsItem))
                    })
                }else if(mjmlObject.children){
                    mjmlObject.children.forEach(function(child){
                        build(child, news)
                    })
                }
        
                return mjmlObject;
            }
            
            cnbAPI.getNewsByIds(cnbState._ressources.articles).then(function(news){
                var data = build(cloneObject(cnbState.getters.getPageContent()), news);
                callback(toHTML?mjmlToHTML(data):data)
            })
        },
        ressources:{
            articles:{
                get(){
                    return cnbState._ressources.articles;
                }
            },
            images:{
                get(pathOnly=false){
                    var images = cnbState._ressources.images;
                    if(!pathOnly)
                        return images;

                    return images.map(function(image){
                        return image.docPath;
                    })
                }
            },
            getAll(){
                return cnbState._ressources;
            }
        }
    },
    subscribeToEvent(eventName, handler){
        this._pubSub.subscribe(eventName, handler)
    }
}
cnbState._init();