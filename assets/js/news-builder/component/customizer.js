
var cnbCustomizer = {
    container:".right-navigation",
    utils:{
        formatePaddingValueToString(value){
            return `${value.paddingTop} ${value.paddingRight} ${value.paddingBottom} ${value.paddingLeft}`
        },
        formatePaddingValueToObject(value){
            var valueParts = value.split(" ");

            if(valueParts.length < 1)
                return {}
            
            if(valueParts.length == 1)
                return {
                    paddingTop: valueParts[0],
                    paddingRight: valueParts[0],
                    paddingBottom: valueParts[0],
                    paddingLeft: valueParts[0]
                }
            
            return {
                paddingTop: valueParts[0] || "",
                paddingRight: valueParts[1] || "",
                paddingBottom: valueParts[2] || "",
                paddingLeft: valueParts[3] || ""
            }
        },
        formateBorderRadiusToString(value){
            return ["borderTopRightRadius", "borderBottomRightRadius", "borderBottomLeftRadius", "borderTopLeftRadius"]
                   .map(border => value[border] ? value[border]:"")
                   .join(" ")
        },
        getInputs(blockRef){
            var block = cnbState.getters.getBlock(blockRef),
                isBlockInsideArticles = cnbState.getters.isInsideArticles(blockRef);           

            var inputs = block.inputs || []
            
            inputs = inputs.map(function(inputParams){
                if(typeof inputParams === "string"){
                    inputParams = {
                        type:inputParams,
                        options:{
                            name:inputParams.replace(/([a-z])([A-Z])/g, '$1-$2').toLowerCase()
                        }
                    }
                }

                var inputName = inputParams.options?.name || inputParams.type;

                //init default value
                if(inputName == "padding"){
                    inputParams.options = inputParams.options || {}
                    inputParams.options.defaultValue = cnbCustomizer.utils.formatePaddingValueToObject(block.data.attributes[inputName] || block.data[inputName] || "")
                    
                }else if(inputParams.options?.payload?.dynamicContent && isBlockInsideArticles){
                    var defaultValue = block.data.attributes[inputName] || block.data[inputName] || null;

                    var options = [];
                    if(block.tagName === "mj-image"){
                        options = [
                            { label:"Photo de profil auteur", value:"%author_profil%" },
                            { label:"Image", value:"%image%" }
                        ]
                    }else if(block.tagName === "mj-button"){
                        options = [
                            { label:"Lien", value:"%link%" }
                        ]
                    }else{
                        options = [
                            { label:"Contenu", value:"%content%" },
                            { label:"Date", value:"%date%" },
                            { label:"Auteur", value:"%author_name%" }
                        ]
                    }

                    return {
                        type:"inputDynamic",
                        options:{
                            name:inputName,
                            defaultValue:defaultValue,
                            input:inputParams,
                            options:options
                        }
                    }
                }else{
                    var defaultValue = block.data.attributes[inputName] || block.data[inputName] || null;
                    if(defaultValue){
                        inputParams.options = inputParams.options || {}
                        inputParams.options.defaultValue = defaultValue
                    }
                }
                
                return inputParams;
            })

            return inputs;
        }
    },
    actions:{
        updateInputs(ref){
            $(cnbCustomizer.container).html("")
            if(ref){
                new CoInput({
                    container:cnbCustomizer.container,
                    inputs:cnbCustomizer.utils.getInputs(ref),
                    customInputType:{
                        imagePicker:function(context, options){
                            var name = options.name || "image-picker",
                                label = options.label || "Image picker",
                                value = options.defaultValue || "";
    
                            var $html = $(`
                                <div class="form-group input-group-sm">
                                    <div class="input-group">
                                        <span class="input-group-addon">@</span>
                                        <input 
                                            type="text" 
                                            name="${name}"
                                            class="form-control" 
                                            style="cursor:pointer;"
                                            value="${value}" 
                                            readonly
                                        >
                                    </div>
                                </div>
                            `)
    
                            $html.find(`input[name="${name}"]`).click(function(){
                                var that = this;
                                cnbResource.actions.selectImage(function(url){
                                    value = url
                                    $(that).val((new URL(url)).pathname.split("/").reverse()[0])
                                    $html.trigger("valueChange", { 
                                        name:name, 
                                        value:url
                                    })
                                })
                            })
    
                            $html.val = function(){
                                return {
                                    name:name,
                                    value:value
                                }
                            }
    
                            return $html;
                        }
                    },
                    i18n:tradNewsbuilder.inputs,
                    onchange:function(name, value){
                        var formatedValue = value;

                        if(name == "padding")
                            formatedValue = cnbCustomizer.utils.formatePaddingValueToString(value)
                        else if(name == "border-radius")
                            formatedValue = cnbCustomizer.utils.formateBorderRadiusToString(value)
                        

                        cnbState.setters.setBlockProperty(name, formatedValue)
                    }
                })
            }
        }
    },
    init(){
        cssHelpers.form.extendCoInput()

        cnbState.subscribeToEvent("block-selected", function(payload){
            cnbCustomizer.actions.updateInputs(payload.ref)
        })

        cnbState.subscribeToEvent("block-change", function(payload){
            if(!payload.fromCustomizer)
                cnbCustomizer.actions.updateInputs(payload.ref)
        })

        cnbState.subscribeToEvent("block-removed", function(){
            $(cnbCustomizer.container).html("")  
        })
    }
}