var cnbPage = {
    container:"#cnb-page",
    utils:{
        constants:{
            containersChildren:{
                "mj-body": ["mj-wrapper", "mj-section"],
                "mj-wrapper": ["mj-section", "article-element"],
                "mj-section": ["mj-column"],
                "mj-column": ["mj-text", "mj-button", "mj-image", "mj-social", "mj-divider", "mj-spacer"],
                "mj-social": ["mj-social-element"]
            },
            componentsBlock:["mj-text", "mj-button", "mj-image", "mj-social", "mj-divider", "mj-spacer", "mj-social-element", "article-element"]
        },
        getBlockRef($el){
            var classNames = $el.attr("class").split(" "),
                ref = null;

            classNames.forEach(function(className){
                if(/^(cnb-block-ref-)\S+/.test(className)){
                    ref = className.replace("cnb-block-ref-", "")
                }
            })

            return ref;
        }
    },
    views:{
        initBlockOutline(){
            $(".cnb-block").each(function(){
                var blockRef = cnbPage.utils.getBlockRef($(this)),
                    block = cnbState.getters.getBlock(blockRef),
                    isSelectedBlock = (blockRef === cnbState.getters.getSelectedBlockRef())

                if(isSelectedBlock)
                    $(this).addClass("selected")

                if(block){
                    if(block.tagName !== "mj-body"){
                        var nbArticles = cnbState.getters.ressources.articles.get().length;

                        $(this).append(`
                            <div class="cnb-block-outline">
                                <div class="outline"></div>
                                <div class="header">
                                    <div style="height: 0px; z-index: 100;">
                                        <div class="header-container">
                                            <div class="label">
                                                ${ block?.label }
                                            </div>
                                            <div class="cnb-block-outline-actions-container">
                                                <div class="cnb-block-outline-action color-danger" data-action="delete" data-ref="${blockRef}">
                                                    <i class="fa fa-trash" aria-hidden="true"></i>
                                                </div>
                                                <div class="cnb-block-outline-action color-info" data-action="select-parent" data-ref="${blockRef}">
                                                    <i class="fa fa-level-up" aria-hidden="true"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            ${
                                (block.tagName === "mj-wrapper") ? (`
                                    <div class="mj-wrapper-info-container">
                                        <button class="btn-toggle-info-mj-wrapper" data-ref="${blockRef}">
                                            <i class="fa fa-chevron-left" aria-hidden="true"></i>
                                            <i class="fa fa-chevron-right" aria-hidden="true"></i>
                                        </button>
                                        <div class="mj-wrapper-info-content">
                                            <h4>Bloc article</h4>
                                            <p><b>Vous avez sélectionné ${ nbArticles < 1 ? "aucun article":"("+nbArticles+") article(s)" }</b></p>
                                            <button class="btn btn-sm btn-success btn-manage-ressource">Gérer vos ressources</button>
                                            <p>
                                                <i>
                                                Ce bloc permet de répéter dynamiquement cette section en fonction des 
                                                articles que vous avez sélectionné dans vos ressources. Vous pouvez utiliser 
                                                les contenus de chaque article en spécifiant la valeur des éléments dans cette 
                                                section comme dynamique.
                                                </i>
                                            </p>
                                        </div>
                                    </div>
                                `):""
                            }
                        `)
                    }
                }
            })
        },
        init(){
            var html = mjmlToHTML(cnbState.getters.getPageContent());

            //replace dynamic content placeholder
            html = html.replace(/%image%/g, `${BASE_URL_IMAGE}/img-placeholder.png`)
            html = html.replace(/%content%/, `
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
                sed do eiusmod tempor incididunt ut labore et dolore magna aliqua...
            `)
            html = html.replace(/%date%/, "Date de publication de l'article")
            html = html.replace(/%author_name%/, "Nom de l'auteur")

            $(cnbPage.container).html(html)
            this.initBlockOutline()
        }
    },
    events:{
        initDroppable(){
            $(".cnb-block").droppable({
                greedy:true,
                drop:function(e, ui){
                    var block = cnbListBlocks.getBlock($(ui.draggable).data('tagname')),
                        targetBlockRef = cnbPage.utils.getBlockRef($(this)),
                        targetBlock = cnbState.getters.getBlock(targetBlockRef);
                    
                    var constants = cnbPage.utils.constants
                    if(constants.containersChildren[targetBlock.tagName] && constants.containersChildren[targetBlock.tagName].indexOf(block.tagName) > -1){
                        cnbState.setters.addBlock(block.data, {
                            ref:targetBlockRef,
                            position:"in"
                        })
                    }else if(constants.componentsBlock.indexOf(targetBlock.tagName) > -1){
                        cnbState.setters.addBlock(block.data, {
                            ref:targetBlockRef,
                            position: "bottom"
                        })
                    }
                }
            })
        },
        initEventBlock(){
            $(".cnb-block").on("mouseover", function(e){
                if(!$(this).hasClass("cnb-block-tagName-mj-body")){
                    $(".cnb-block-outline").removeClass("active")
                    $(this).find(" > .cnb-block-outline").addClass("active")
                }
                e.stopPropagation()
            }).mouseleave(function(){
                $(this).find(" > .cnb-block-outline").removeClass("active")
            })

            $(".cnb-block-outline-action").click(function(e){
                e.stopPropagation()

                var action = $(this).data("action"),
                    blockRef = $(this).data("ref")

                switch(action){
                    case "delete":
                        cnbState.setters.deleteBlock(blockRef)
                    case "select-parent":
                        cnbState.setters.selectParentBlock(blockRef)
                    break;
                }
            })

            $(".cnb-block").click(function(e){
                e.stopPropagation();
                var blockRef = cnbPage.utils.getBlockRef($(this));
                cnbState.setters.selectBlock(blockRef)
            })

            $(".cnb-block").dblclick(function(e){
                e.stopPropagation();
                var blockRef = cnbPage.utils.getBlockRef($(this)),
                    block = cnbState.getters.getBlock(blockRef);
            
                if(block && block.tagName === "mj-image"){
                    cnbResource.actions.selectImage(function(url){
                        cnbState.setters.setBlockProperty("src", url, false)
                    })
                }
            })

            $(".btn-toggle-info-mj-wrapper").click(function(e){
                e.stopPropagation();

                var ref = $(this).data("ref"),
                    selectedBlockRef = cnbState.getters.getSelectedBlockRef();
                    
                cnbState.setters.selectBlock((ref===selectedBlockRef) ? null:ref);
            })

            $(cnbPage.container).find(".btn-manage-ressource").click(function(){
                $(this).closest(".mj-wrapper-info-container").removeClass("active")
                $(cnbPage.container).css({ overflow: "auto" })

                $("li[data-tab='cnb-ressources-panel']").click()
                cnbResource.actions.openModalArticles()
            })
        },
        init(){
            this.initDroppable()
            this.initEventBlock()

            //prevent onclick on an link in page builder
            $(cnbPage.container + " a").click(function(e){
                e.preventDefault()
            })
        }
    },
    stateListener:{
        init(){
            cnbState.subscribeToEvent("block-added", function(){
                cnbPage.views.init()
                cnbPage.events.init()
            })
            cnbState.subscribeToEvent("block-removed", function(){
                cnbPage.views.init()
                cnbPage.events.init()
            })

            cnbState.subscribeToEvent("block-change", function(payload){
                cnbPage.views.init()
                cnbPage.events.init()
            })

            cnbState.subscribeToEvent("article-added", function(){
                cnbPage.views.init()
                cnbPage.events.init()
            })

            cnbState.subscribeToEvent("block-selected", function(payload){
                $(".cnb-block").removeClass("selected")
                $(cnbPage.container).css({ overflow:"auto" })
                if(payload.ref){
                    var block = cnbState.getters.getBlock(payload.ref);
                    $(`.cnb-block-ref-${payload.ref}`).addClass("selected")
                    //set page overflow to visible when article (mj-wrapper) is selected for is not to hide the popup
                    if(block.tagName === "mj-wrapper")
                        $(cnbPage.container).css({ overflow:"visible" })
                }
            })
        }
    },
    init(){
        this.views.init()
        this.events.init()
        this.stateListener.init()
    }
}