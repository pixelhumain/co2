var cnbBlocks = {
    container:"#cnb-draganddrop-block",
    initViews(){
        var $container = $(`
            <div class="accordion active">
                <button class="accordion-button">
                    <span>Conteneurs</span>
                </button>
                <div class="accordion-content">
                    <div class="contents-list" id="cnb-block-container-list"></div>
                </div>
            </div>
            <div class="accordion active">
                <button class="accordion-button">
                    <span>Blocks</span>
                </button>
                <div class="accordion-content">
                    <div class="contents-list" id="cnb-block-content"></div>
                </div>
            </div>
        `)

        cnbListBlocks.list.forEach(function(block){
            if(["mj-body"].indexOf(block.tagName) < 0){
                var item = `
                    <div class="item component-item" data-tagname="${block.tagName}">
                        <img src="${BASE_URL_IMAGE}/components/${block.image}" alt="" draggable="false">
                        <p>${block.label}</p>
                    </div>
                `

                if(["mj-wrapper", "mj-section","mj-column"].indexOf(block.tagName) < 0){
                    $container.find("#cnb-block-content").append(item)
                }else{
                    $container.find("#cnb-block-container-list").append(item)
                }
            }
        })

        $(this.container).html($container)
    },
    initEvents(){
        var $dropzone = $(cnbPage.container),
            offset = $dropzone.offset(),
            offsetHeight = offset.top + $dropzone.height();

        var handlers = { 
            top:null, 
            bottom:null,
            clear:function(){
                clearInterval(this.top),
                clearInterval(this.bottom)
            }
        }
        var params = { distance:100, timer:200, step:20 }

        $(this.container).find(".component-item").draggable({
            helper: "clone",
            zIndex:999999,
            containment:"document",
            scroll:true,
            start:function(e, ui){
                $("#cnb-page").addClass("draggover")
            },
            stop:function(){
                $("#cnb-page").removeClass("draggover")
                handlers.clear()
            },
            drag: function(e){
                //this feature allows auto scrolling
                var isMoving = false
                //Top
                if((e.pageY - offset.top) <= params.distance){
                    isMoving = true
                    handlers.clear()
                    handlers.top = setInterval(function(){
                        $dropzone.scrollTop($dropzone.scrollTop() - params.step)
                    }, params.timer)
                }
                //bottom
                if(e.pageY >= (offsetHeight - params.distance)){
                    isMoving = true
                    handlers.clear()
                    handlers.bottom = setInterval(function(){
                        $dropzone.scrollTop($dropzone.scrollTop() + params.step)
                    }, params.timer)
                }
                if(!isMoving)
                    handlers.clear()
            }
        })
    },
    init(){
        this.initViews()
        this.initEvents()
    }
}