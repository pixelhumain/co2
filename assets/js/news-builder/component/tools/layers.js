var cnbLayers = {
    container:"#cnb-layers-tab",
    actions:{
        refreshPath(){
            var pageContent = cnbState.getters.getPageContent()
            cnbLayers.initViews(pageContent);

            $(cnbLayers.container).find(".btn-path-node").click(function(){
                cnbState.setters.selectBlock($(this).data("ref"))
            })

            cnbLayers.actions.expandLayer(cnbState.getters.getSelectedBlockRef())
        },
        expandLayer(ref){
            $(".btn-path-node").removeClass("active")
            if(ref){
                var $btnPathNode = $(`.btn-path-node[data-ref="${ref}"]`);
                $btnPathNode.addClass("active")

                $(cnbLayers.container).find("li").removeClass("expand")
                $btnPathNode.parents("li").each(function(){
                    $(this).addClass("expand")
                })
            }
        }
    }, 
    initViews(pageContent){
        var generatePath = function(data, path=[]){
            var blockInfo = cnbListBlocks.getBlock(data.tagName),
                ref = cnbState.getters.getBlockRef(path),
                html = "",
                hasChildren = data.children && data.children.length > 0;

            if(data.tagName !== "mj-head"){
                if(data.tagName !== "mjml"){
                    html += `
                        <li>
                            <span class="btn-path-node" data-ref="${ref}">
                                ${hasChildren?(`
                                    <i class="fa fa-caret-down" aria-hidden="true"></i>
                                    <i class="fa fa-caret-right" aria-hidden="true"></i>
                                `):""}
                                <span>${blockInfo?.label}</span>
                            </span>
                    `
                }
    
                if(data.children && data.children.length > 0){
                    html += "<ul>"
                    data.children.forEach(function(child, index){
                        html += generatePath(child, path.concat(['children', index]))
                    })
                    html += "</ul>"
                }
                if(data.tagName !== "mjml")
                    html += "</li>"
            }

            return html;
        }

        var html = generatePath(pageContent)
        $(this.container).html(html)
    },
    initEvents(){
        cnbState.subscribeToEvent("block-added", this.actions.refreshPath)
        cnbState.subscribeToEvent("block-removed", this.actions.refreshPath)
        cnbState.subscribeToEvent("block-change", this.actions.refreshPath)
        cnbState.subscribeToEvent("block-selected", function(payload){
            cnbLayers.actions.expandLayer(payload.ref)
        })
    },
    init(){
        this.actions.refreshPath()
        this.initEvents()
    }
}