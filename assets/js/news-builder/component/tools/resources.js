var cnbResource = {
    container:"#cnb-ressources-panel",
    actions:{
        refreshImagesList(){
            cnbAPI.getImages(userId).then(function(images){
                cnbState.setters.ressources.images.set(images)
                cnbResource.views.images.initList(images)
                cnbResource.events.initImageEvents()
            })
        },
        refreshArticlesList(){
            var ids = cnbState.getters.ressources.articles.get()

            cnbAPI.getNewsByIds(ids).then(function(news){
                cnbResource.views.articles.initList(news)
                cnbResource.events.initArticleEvents()
            })
        },
        openModalArticles(){
            var $listArticleContainer = $(`
                <div class="cnb-article-list-container">
                    <div class="cnb-article-list"></div>
                    <button class="btn btn-default">Afficher plus</button>
                </div>
            `)

            var $footer = $(`
                <div class="d-flex justify-content-between align-items-center">
                    <span><span class="badge" id="nb-articles">0</span> sélectionné(s)</span>
                    <button class="btn btn-success" id="btn-validate">Valider</button>
                </div>
            `)

            var dateLimit = null,
                selectedIds = new Set(cnbState.getters.ressources.articles.get());

            var fetch = function(callback=null){
                cnbAPI.getNews(context.type, context.id, dateLimit).then(function(res){
                    dateLimit = res.dateLimit
                    
                    res.news.forEach(function(article){
                        $listArticleContainer.find(".cnb-article-list").append(
                            cnbResource.views.articles.getListItem(article, selectedIds.has(article._id.$id))
                        )
                    })

                    if(res.endStream)
                        $listArticleContainer.find("button").css({ display:"none" })

                    $listArticleContainer.find("input[type='checkbox']").off().click(function(e){
                        e.stopPropagation()                    
                    }).change(function(){
                        var id = $(this).data("id")
                        if($(this).is(':checked'))
                            selectedIds.add(id)
                        else
                            selectedIds.delete(id)

                        $footer.find("#nb-articles").html(selectedIds.size)
                    })

                    if(callback)
                        callback()
                })
            }

            $listArticleContainer.find("button").click(function(){
                fetch()
            })

            $footer.find("#btn-validate").click(function(){
                var ids = Array.from(selectedIds)
                cnbState.setters.ressources.articles.set(ids)
                cnbModal.close()
                cnbResource.actions.refreshArticlesList()
            })

            fetch(function(){
                cnbModal.open("Articles", $listArticleContainer, $footer)
            })
        },
        selectImage(callback){
            var $list = $(`<div class="cnb-dialog-images-list"></div>`)
    
            cnbState.getters.ressources.images.get().forEach(function(image){
                $list.append(`
                    <div class="cnb-dialog-images-list-item" data-id="${image.id}" data-url="${baseUrl + "/" + image.path}">
                        <img src="${baseUrl + "/" + image.path}"/>
                    </div>
                `)
            })

            var $btnManageRessource = $(`<button class="btn btn-success">Gérer les ressources</button>`)

            $btnManageRessource.click(function(){
                cnbModal.close()
                $("li[data-tab='cnb-ressources-panel']").click()
            })

            cnbModal.open(
                "Choisissez un image",
                $list,
                $btnManageRessource
            )

            $list.find(".cnb-dialog-images-list-item").click(function(){
                callback($(this).data("url"))
                cnbModal.close()
            })
        }
    },
    views:{
        images:{
            initList(images){
                var $container = $(cnbResource.container).find(".cnb-images-mangager-list")

                $container.html("")
                images.forEach(function(img){
                    $container.append(`
                        <div class="cnb-images-mangager-item">
                            <img src="${baseUrl + "/" + img.path}" >
                            <button class="btn-delete-cnb-image" data-id="${img.id}"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                        </div>
                    `)
                })
            },
            init(){
                var $html = $(`
                    <div class="cnb-images-manager">
                        <div class="cnb-images-mangager-header">
                            <p><i class="fa fa-picture-o"></i> Images</p>
                            <button class="btn-cnb-upload-image"><i class="fa fa-plus-circle" aria-hidden="true"></i></button>
                        </div>
                        <div class="cnb-images-mangager-list">
                        </div>
                    </div>
                `)
                $(cnbResource.container).append($html);
                cnbResource.actions.refreshImagesList()
            }
        },
        articles:{
            modal:{

            },
            getListItem(article, selected=false){
                return `
                    <div class="cnb-article-item">
                        <div class="cnb-article-item-header">
                            <div>
                            <img src="${baseUrl}/${article.author.profilThumbImageUrl}" alt="">
                            <div>
                                <p>${article.author.name}</p>
                                <p>${moment.unix(article.date.sec).format("ddd DD MMMM YYYY. hh:mm")}</p>
                            </div>
                            </div>
                            <div>
                            <input type="checkbox" data-id="${article._id.$id}" ${selected?"checked":""}>
                            </div>
                        </div>
                        <div class="cnb-article-item-body">
                            ${article.text}
                        </div>
                    </div>
                `
            },
            initList(articles){
                var self = this;
                var $container = $(cnbResource.container).find(".cnb-articles-manager-list")

                $container.html("")
                articles.forEach(function(article){
                    $container.append(self.getListItem(article, true))
                })
            },
            init(){
                var $html = $(`
                    <div class="cnb-images-manager">
                        <div class="cnb-images-mangager-header">
                            <p><i class="fa fa-newspaper-o"></i> Articles</p>
                            <button class="btn-cnb-add-article"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                        </div>
                        <div class="cnb-articles-manager-list">
                        </div>
                    </div>
                `)

                $(cnbResource.container).append($html)
            }
        },
        init(){
            this.images.init()
            this.articles.init()
        }
    },
    events:{
        initImageEvents(){
            $(cnbResource.container).find(".btn-cnb-upload-image").off().click(function(){
                var $input = $(`<input type="file" accept=".gif, .jpg, .png"/>`)
                $input.change(function(){
                    cnbAPI.uploadImage(context.type, context.id, $input[0].files[0]).then(function(){
                        cnbResource.actions.refreshImagesList()
                    })
                })
                $input.trigger("click")
            })

            $(cnbResource.container).find(".btn-delete-cnb-image").off().click(function(){
                if(confirm("Voulez-vous vraiment supprimer l'image ?")){
                    var id = $(this).data("id");
                    cnbAPI.deleteImage(id).then(function(){
                        var images = cnbState.getters.ressources.images.get().filter(function(image){
                            return image.id !== id
                        })
                        cnbState.setters.ressources.images.set(images)
                        cnbResource.actions.refreshImagesList()
                    })
                }
            })
        },
        initArticleEvents(){
            $(cnbResource.container).find(".btn-cnb-add-article").off().click(function(){
                cnbResource.actions.openModalArticles()
            })

            $(cnbResource.container).find(".cnb-article-item input[type='checkbox']").off().change(function(){
                var id = $(this).data("id"),
                    ids = cnbState.getters.ressources.articles.get()
                
                ids = ids.filter(function(i){
                    return i !== id
                })
                cnbState.setters.ressources.articles.set(ids)
                cnbResource.actions.refreshArticlesList()
            })
        },
        init(){
            this.initImageEvents()
            this.initArticleEvents()
        }
    },
    init(){
        this.views.init()
        this.events.init()
    }
}