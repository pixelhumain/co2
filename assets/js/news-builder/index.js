function saveNews(){
    var $form = $(`
        <div class="cnb-form-save-template">
            <form>
                <div class="form-group">
                    <label>Nom du modèle</label>
                    <input type="text" name="name" class="form-control">
                </div>
                <div class="form-group">
                    <label>Image</label>
                    <div class="input-image">
                        <img src="#" alt="">
                        <i class="fa fa-picture-o" aria-hidden="true"></i>
                    </div>
                    <div style="display:none">
                        
                    </div>
                </div>
                <hr>
                <button type="submit" class="btn btn-success">Enregistrer</button>
            </form>
        </div>
    `)

    var imageFile = null;
    $form.find(".input-image").click(function(){
        var $input = $(`<input type="file" accept=".gif, .jpg, .png"/>`)
        
        $input.change(function(){
            imageFile = $(this)[0].files[0]
            $form.find(".input-image img").attr("src",URL.createObjectURL($(this)[0].files[0]))
            $form.find(".input-image").addClass("loaded")
        })

        $input.trigger("click");
    })

    $form.find("form").submit(function(e){
        e.preventDefault()
        var data = {
            context:context,
            template:{
                name:$(this).find("input[name='name']").val(),
                page:JSON.stringify(cnbState.getters.getPageContent(false))
            }
        }

        if(imageFile)
            data.template.image = imageFile;

        cnbAPI.saveTemplate(context.type, context.id, data).then(function(){
            cnbModal.close()
        })
    })

    cnbModal.open("Enregistrement du modèle", $form)
}

function openTemplate(onOpen = null){
    cnbAPI.getTemplate().then(function(templates){
        var $container = $(`<div class="cnb-template-list"></div>`)

        if(templates.length < 1){
            $container.html(`<p class="cnb-template-list-empty">Aucun modèle enregistré.</p>`);
        }else{
            templates.forEach(function(template, index){
                $container.append(`
                    <div class="cnb-template-list-item" data-index="${index}">
                        <div class="cnb-template-img">
                        <img src="${baseUrl}/${template.image}" alt="">
                        </div>
                        <div class="cnb-template-info">
                            <p>${template.name}</p>
                            <p><span>Créé par: </span><a href="${baseUrl}/#@${template.author.slug}" target="_blank">${template.author.name}</a></p>
                            ${
                                template.authorId === userId ? `
                                    <button class="btn btn-sm btn-danger" id="btn-delete-template" data-index="${index}">
                                        <i class="fa fa-trash-o" aria-hidden="true"></i> Supprimer
                                    </button>
                                `:''
                            }
                        </div>
                    </div>
                `)
            })
    
    
            $container.find("a").click(function(e){
                e.stopPropagation()
            })
    
            $container.find(".cnb-template-list-item").click(function(){
                var template = templates[$(this).data("index")];
                cnbState.setters.setPageContent(JSON.parse(template.data));

                if(onOpen){
                    onOpen(template);
                }

                cnbModal.close()
            })
        }

        $container.find("#btn-delete-template").click(function(e){
            e.stopPropagation()
            var template = templates[$(this).data("index")]
            if(confirm("Voulez-vous vraiment supprimer ce modèle?")){
                cnbAPI.deleteTemplate(template.id).then(function(){
                    cnbModal.close()
                    toastr.options = {
                        "closeButton": true,
                        "positionClass": "toast-top-center",
                        "timeOut": "0"
                    }
                    toastr.success("Le modèle a été supprimer.")
                })
            }
        })

        cnbModal.open("Choisir un modèle", $container)
    })
}

function previewPage(){
    
}

function sendNews(){
    var selectedCommunity = [],
        externalReceivers = new Set();
    cnbAPI.getCommunity(context.type, context.id).then(function(persons){
        var $communityList = $(`
            <div class="cnb-community">
                <div class="alert-container"></div>
                <div class="form-group">
                    <label for="comment">Objet:</label>
                    <textarea class="form-control" rows="2" id="comment"></textarea>
                </div>
                <ul class="nav nav-tabs cnb-nav-tabs">
                    <li class="active" data-target="cnb-tab-community"><a href="javascript:;">Communautés</a></li>
                    <li data-target="cnb-tab-other"><a href="javascript:;">Autres</a></li>
                </ul>
                <div style="width: 100%; min-height:300px; padding: 10px; border: 1px solid #ddd; border-top:none;">
                    <div class="cnb-tab cnb-tab-community active">
                        <div class="cnb-community-header">
                            <div class="cnb-community-search">
                                <input type="text" placeholder="Rechercher..." name="" id="cnb-community-search-input">
                                <span><i class="fa fa-search" aria-hidden="true"></i></span>
                            </div>
                            <div>
                                <label for="checkboxAllCommunity">Selectionner toutes</label>
                                <input type="checkbox" name="" id="checkboxAllCommunity">
                            </div>
                        </div>
                        <div class="cnb-community-body">
                            <div class="cnb-community-list">
                            </div>
                        </div>
                    </div>
                    <div class="cnb-tab cnb-tab-other">
                        <div class="d-flex">
                            <div class="input-group input-group-sm" style="width:100%">
                                <span class="input-group-addon"><i class="fa fa-envelope-o" aria-hidden="true"></i></span>
                                <input type="text" class="form-control" placeholder="Adresse mail" id="cnb-input-mail">
                                <span class="input-group-btn">
                                    <button class="btn btn-info btn-sm" id="btn-add-receiver" disabled><i class="fa fa-plus" aria-hidden="true"></i></button>
                                </span>
                            </div>
                        </div>
                        <ul class="cnb-other-receiver"></ul>
                    </div>
                </div>
                <div class="cnb-community-footer d-flex justify-content-between align-items-center" style="padding:10px 0px;">
                    <span><span class="badge" id="nb-receiver">0</span> destinataire(s) ajouté(s)</span>
                    <button class="btn btn-success" id="btn-confirm-send">Confirmer & envoyer</button>
                </div>
            </div>
        `)

        persons.forEach(function(person){
            $communityList.find(".cnb-community-list").append(`
                <div class="cnb-community-list-item">
                    <div class="cnb-community-list-item-info">
                    <div>
                        <img src="${baseUrl}/${person.profilThumbImageUrl}" alt="">
                    </div>
                    <div>
                        <p><a href="${baseUrl}/#@${person.slug}" target="_blank">${person.name}</a></p>
                        <p>${person.email}</p>
                    </div>
                    </div>
                    <div class="cnb-community-list-item-action">
                        <input type="checkbox" data-id="${person.id}">
                    </div>
                </div>
            `)
        })

        $communityList.find("#cnb-community-search-input").keyup(function(){
            var value = $(this).val().toLowerCase()
            $communityList.find(".cnb-community-list-item").filter(function(){
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            })
        })

        $communityList.find("#checkboxAllCommunity").change(function(){
            if($(this).is(':checked')){
                selectedCommunity = persons.map(function(person){
                    return person.id
                })
                $communityList.find("input[type='checkbox']").prop('checked', true)
            }else{
                selectedCommunity = []
                $communityList.find("input[type='checkbox']").prop('checked', false)
            }

            $communityList.find("#nb-receiver").html(selectedCommunity.length)
        })

        $communityList.find(".cnb-community-list-item input[type='checkbox']").change(function(){
            var id = $(this).data("id"),
                isChecked = $(this).is(':checked');
            
            if(isChecked)
                selectedCommunity.push(id)
            else
                selectedCommunity.splice(selectedCommunity.indexOf(id), 1)

            $communityList.find("#nb-receiver").html(selectedCommunity.length + externalReceivers.size)
        })

        $communityList.find("#btn-confirm-send").click(function(){
            var subject = $communityList.find("textarea").val(),
                error = false;

            if(!subject)
                error = "Veuillez spécifier l'objet du mail.";
            else if((selectedCommunity.length + externalReceivers.size) < 1 )
                error = "Veuillez ajouter au moin un destinataire.";

            if(error){
                $communityList.find(".alert-container").html(`
                    <div class="alert alert-danger" role="alert" style="margin-bottom:10px;">
                        <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                        ${error}
                    </div>
                `)
            }else{
                cnbState.getters.buildPageContent(function(pageContent){
                    var mail = {
                        subject:subject,
                        receivers:{
                            community:selectedCommunity,
                            external:Array.from(externalReceivers)
                        },
                        content:JSON.stringify(pageContent)
                    }

                    cnbAPI.sendMail(context.type, context.id, mail).then(function(){
                        cnbModal.close()
                        toastr.options = {
                            "closeButton": true,
                            "positionClass": "toast-top-center",
                            "timeOut": "0"
                          }
                        toastr.success("Enregistrez le modèle si vous voulez l'utiliser encore plutard.", "Newsletter envoyé")
                    })
                })
            }
        })

        $communityList.find(".cnb-nav-tabs li").click(function(){
            $communityList.find(".cnb-nav-tabs li").removeClass("active")
            $(this).addClass("active")
            $communityList.find(".cnb-tab").removeClass("active")
            $communityList.find("."+$(this).data("target")).addClass("active")
        })

        $communityList.find("#btn-add-receiver").click(function(){
            var mail = $communityList.find("#cnb-input-mail").val()
            if(/^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(mail)){
                $communityList.find("#cnb-input-mail").val("")
                $(this).attr("disabled", true)

                if(!externalReceivers.has(mail)){
                    externalReceivers.add(mail)
                    $item = $(`
                        <li>
                            <button><i class="fa fa-minus" aria-hidden="true"></i></button>
                            <span>${mail}</span>
                        </li>
                    `)
                    $item.find("button").click(function(){
                        $(this).parent("li").remove()
                        externalReceivers.delete(mail)
                        $communityList.find("#nb-receiver").html(selectedCommunity.length + externalReceivers.size)
                    })
                    $communityList.find(".cnb-other-receiver").append($item)
                }

                $communityList.find("#nb-receiver").html(selectedCommunity.length + externalReceivers.size)
            }
        })

        $communityList.find("#cnb-input-mail").keyup(function(){
            if(/^[^\s@]+@[^\s@]+\.[^\s@]+$/.test($(this).val()))
                $communityList.find("#btn-add-receiver").removeAttr("disabled")
            else
                $communityList.find("#btn-add-receiver").attr("disabled", true)
        })

        cnbModal.open("Séléctionner le(s) destinataire(s)", $communityList)
    })
}

$(function(){
    var currentTemplate = null,
        previewMode = false;

    //init tools
    cnbBlocks.init()
    cnbLayers.init()
    cnbResource.init()
    $(".side-navigation-header li").click(function(){
        $(".side-navigation-header li").removeClass("active")
        $(this).addClass("active")

        var tab = $(this).data("tab")
        $('.cnb-sidenav-tab').removeClass("active")
        $(`#${tab}`).addClass("active")
    })

    //init page
    cnbPage.init()

    //init customizer
    cnbCustomizer.init()

    $('button[data-action="save-news"]').click(function(){
        if(currentTemplate && userId === currentTemplate.authorId){
            $.confirm({
                title: 'Enregistrement du modèle',
                content: 'Comment voulez-vous enregistrer le modèle ?',
                containerFluid:true,
                buttons: {
                    update:{
                        text:"Mettre à jour",
                        btnClass:"btn-green text-transform-none",
                        action:function(){
                            cnbAPI.updateTemplate(currentTemplate.id, {
                                data:JSON.stringify(cnbState.getters.getPageContent(false))
                            })
                        }
                    },
                    save:{
                        text:"Enregistrer comme nouveau modèle",
                        btnClass:"btn-warning text-transform-none",
                        action:function(){
                            saveNews()
                        }
                    },
                    cancel: {
                        text:"Annuler",
                        btnClass:"text-transform-none",
                        action:function(){}
                    }
                }
            });
        }else{
            saveNews()
        }
    })

    $('button[data-action="open-template"]').click(function(){
        openTemplate(function(template){
            currentTemplate = template
        })
    })

    $('button[data-action="preview"]').click(function(){
        previewMode = !previewMode;

        $(".cnb-modal-fullwidth").removeClass("active")
        $(this).removeClass("active")
        if(previewMode){
            cnbState.getters.buildPageContent(function(html){
                $(".cnb-modal-fullwidth").addClass("active")
                $(".cnb-modal-fullwidth").html(html)
            }, true)
            $(this).addClass("active")
        }
    })

    $('button[data-action="send-news"]').click(function(){
        sendNews()
    })
})