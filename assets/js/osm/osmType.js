osmAllTYpe = {
    transportsAeriensParCable : {
        name : "Transports aériens par câble",
        element : {
            typeTransport : {
                name : "Type de transport",
                element : {
                    ligneTelepherique : {
                        name : "Ligne de téléphérique",
                        tag : "aerialway",
                        type : "cable_car",
                        marker : "/images/markers/osm/ligneTelepherique.png",
                    },
                    telecabine : {
                        name : "Télécabine",
                        tag : "aerialway",
                        type : "gondola",
                    },
                    telesiege : {
                        name : "Télésiège",
                        tag : "aerialway",
                        type : "chair_lift",
                    },
                    telesiegeAndtelecabine : {
                        name : "Un mélange de télécabines et de télésièges sur le même axe",
                        tag : "aerialway",
                        type : "mixed_lift",
                    },
                    teleski : {
                        name : "Téléski",
                        tag : "aerialway",
                        type : "drag_lift,t-bar,j-bar,platter,rope_tow",
                    },
                    remonteMecanique : {
                        name : "Remontée mécanique",
                        tag : "aerialway",
                        type : "magic_carpet",
                    },

                    tyrolienne : {
                        name : "Tyrolienne",
                        tag : "aerialway",
                        type : "zip_line",
                    },
                },
            },

            equipement : {
                name : "Equipements",
                element : {
                    pylone : {
                        name : "Pylône supportant le câble d'un transport aérien",
                        tag : "aerialway",
                        type : "pylon",
                    },
                    stationEntreeOuArret : {
                        name : "Station d'entrée ou d'arrêt pour les usagers",
                        tag : "aerialway",
                        type : "station",
                    },
                },
            },

            autre : {
                name : "Autre",
                element : {
                    pylone : {
                        name : "Ascenseur pour des biens",
                        tag : "aerialway",
                        type : "goods",
                    },
                },
            },
        },
    },

    aviation : {
        name : "Aviation",
        element : {
            aeroport : {
                name : "Aéroport",
                tag : "aeroway",
                type : "aerodrome",
                marker : "/images/markers/osm/aeroport.png"
            },

            airstrip : {
                name : "Zone où les avions légers peuvent atterrir et décoller",
                tag : "aeroway",
                type : "airstrip",
            },

            apron : {
                name : "Aire de stationnement",
                tag : "aeroway",
                type : "apron",
            },

            control_center : {
                name : "Le centre de contrôle",
                tag : "aeroway",
                type : "control_center",
            },

            fuel : {
                name : "Station d'avitaillement pour avions",
                tag : "aeroway",
                type : "fuel",
            },
            gate : {
                name : "Porte d'embarquement",
                tag : "aeroway",
                type : "gate",
            },
            hangar : {
                name : "Hangar",
                tag : "aeroway",
                type : "hangar",
            },
            helipad : {
                name : "Hélisurface",
                tag : "aeroway",
                type : "helipad",
            },
            heliport : {
                name : "Héliport",
                tag : "aeroway",
                type : "heliport",
            },
            highway_strip : {
                name : "Piste d'atterrissage",
                tag : "aeroway",
                type : "highway_strip",
            },
            holding_position : {
                name : "Point derrière lequel les aéronefs sont considérés comme dégagés d'une voie de passage",
                tag : "aeroway",
                type : "holding_position",
            },
            jet_bridge : {
                name : "Passerelle d'embarquement des passagers",
                tag : "aeroway",
                type : "jet_bridge",
            },
            navigationaid : {
                name : "Radiobalise",
                tag : "aeroway",
                type : "navigationaid",
            },
            beacon : {
                name : "Aide à la radionavigation",
                tag : "aeroway",
                type : "beacon",
            },
            parking_position : {
                name : "Endroit où un avion peut se garer",
                tag : "aeroway",
                type : "parking_position",
            },
            runway : {
                name : "Piste d'atterrissage / d'envol",
                tag : "aeroway",
                type : "runway",
            },
            stopway : {
                name : "Surface utilisée lors d'un décollage interrompu(stopway)",
                tag : "aeroway",
                type : "stopway",
            },
            taxilane : {
                name : "Voie de circulation d'un aéroport qui fait partie de l'aire de stationnement (apron)",
                tag : "aeroway",
                type : "taxilane",
            },
            taxiway : {
                name : "Voie de circulation",
                tag : "aeroway",
                type : "taxiway",
            },
            terminal : {
                name : "Terminaux et bâtiments d'aéroport",
                tag : "aeroway",
                type : "terminal",
            },
            tower : {
                name : "Tour de contrôle",
                tag : "aeroway",
                type : "tower",
            },
            windsock : {
                name : "Manche à air",
                tag : "aeroway",
                type : "windsock",
            },
            

        },
    },

    consommation : {
        name : "Consommation",
        element : {
            bar : {
                name : "Bar, bistrot",
                tag : "amenity",
                type : "bar",
                icon : "fa fa-glass",
                marker : "/images/markers/osm/bar.png"
            },
            biergarten : {
                name : "Brasserie en plein air",
                tag : "amenity",
                type : "biergarten",
            },
            cafe : {
                name : "Café",
                tag : "amenity",
                type : "cafe",
                marker : "/images/markers/osm/cafe.png",
            },
            fast_food : {
                name : "Restauration rapide (Fast Food)",
                tag : "amenity",
                type : "fast_food",
                marker : "/images/markers/osm/fast_food.png",
            },
            food_court : {
                name : "Zone de restauration avec de multiples caisses et une zone de consommation commune",
                tag : "amenity",
                type : "food_court",
            },
            pub : {
                name : "Pub",
                tag : "amenity",
                type : "pub",
            },
            ice_cream : {
                name : "Glacier",
                tag : "amenity",
                type : "ice_cream",
                marker : "/images/markers/osm/ice_cream.png",
            },
            restaurant : {
                name : "Restaurant",
                tag : "amenity",
                type : "restaurant",
                marker : "/images/markers/osm/restaurant.png",
            },
        
        }
    },

    education : {
        name : "Éducation",
        element : {
            college : {
                name : "Établissement d'enseignement supérieur non universitaire",
                tag : "amenity",
                type : "college",
                marker : "/images/markers/osm/college.png",
            },
            dancing_school : {
                name : "Une école de danse ou un studio de danse",
                tag : "amenity",
                type : "dancing_school",
            },
            driving_school : {
                name : "Auto-école",
                tag : "amenity",
                type : "driving_school",
            },
            first_aid_school : {
                name : "Un lieu où l'on peut suivre des cours de premiers secours",
                tag : "amenity",
                type : "first_aid_school",
            },
            kindergarten : {
                name : "École maternelle",
                tag : "amenity",
                type : "kindergarten",
            },
            language_school : {
                name : "École de langues",
                tag : "amenity",
                type : "language_school",
            },
            library : {
                name : "Bibliothèque publique (municipale, universitaire, etc)",
                tag : "amenity",
                type : "library",
                marker : "/images/markers/osm/library.png",
            },
            surf_school : {
                name : "Une école de surf",
                tag : "amenity",
                type : "surf_school",
            },
            toy_library : {
                name : "Un lieu où l'on peut emprunter des jeux et des jouets",
                tag : "amenity",
                type : "toy_library",
            },
            research_institute : {
                name : "Un établissement doté pour faire de la recherche",
                tag : "amenity",
                type : "research_institute",
            },
            training : {
                name : "Lieu public où l'on peut se former",
                tag : "amenity",
                type : "training",
            },
            music_school : {
                name : "École de musique",
                tag : "amenity",
                type : "music_school",
                marker : "/images/markers/osm/music_school.png",
            },
            school : {
                name : "École primaire, collège, lycée, gymnase (Suisse)",
                tag : "amenity",
                type : "school",
                marker : "/images/markers/osm/school.png",
            },
            traffic_park : {
                name : "Écoles de conduite pour mineurs",
                tag : "amenity",
                type : "traffic_park",
            },
            university : {
                name : "Université, campus universitaire",
                tag : "amenity,building",
                type : "university",
                marker : "/images/markers/osm/university.png",
                icon : "fa fa-graduation-cap",
                descrpition : "Un établissement d’enseignement universitaire.",
                input : [
                ]
            },
        }
    },

    transports : {
        name : "Transports",
        element : {
            bicycle_parking : {
                name : "Parking à vélos",
                tag : "amenity",
                type : "bicycle_parking",
                marker : "/images/markers/osm/bicycle_parking.png",
            },

            bicycle_repair_station : {
                name : "Point de réparation pour vélos",
                tag : "amenity",
                type : "bicycle_repair_station",
                marker : "/images/markers/osm/bicycle_parking.png",
            },

            bicycle_rental :  {
                name : "Location de vélos",
                tag : "amenity",
                type : "bicycle_rental",
                marker : "/images/markers/osm/bicycle_parking.png",
            },

            boat_rental: {
                name : "Location de Bateau",
                tag : "amenity",
                type : "boat_rental",
            },

            boat_sharing : {
                name : "Partage de bateaux",
                tag : "amenity",
                type : "boat_sharing",
            },

            bus_station : {
                name : "Gare routière",
                tag : "amenity",
                type : "bus_station",
                marker : "/images/markers/osm/bus_station.png",
            },

            car_rental : {
                name : "Location de voitures",
                tag : "amenity",
                type : "car_rental",
            },

            car_sharing : {
                name : "Station d'autopartage (ne pas confondre avec le covoiturage)",
                tag : "amenity",
                type : "car_sharing",
            },

            car_wash : {
                name : "Station de lavage pour automobiles",
                tag : "amenity",
                type : "car_wash",
                marker : "/images/markers/osm/car_wash.png",
            },

            compressed_air: {
                name : "Station de gonflage des pneus",
                tag : "amenity",
                type : "compressed_air",
            },

            vehicle_inspection : {
                name : "Point d'inspection des véhicule par le Gouvernement",
                tag : "amenity",
                type : "vehicle_inspection",
            },

            charging_station: {
                name : "Installation de recharge pour véhicules électriques",
                tag : "amenity",
                type : "charging_station",
            },

            driver_training: {
                name : "Lieu de formation à la conduite sur un parcours fermé",
                tag : "amenity",
                type : "driver_training",
            },

            ferry_terminal: {
                name : "Terminal de ferry",
                tag : "amenity",
                type : "ferry_terminal",
            },

            fuel: {
                name : "Station-service",
                tag : "amenity",
                type : "fuel",
                marker : "/images/markers/osm/fuel.png",
                /* option : [
                    {
                        name : "Nom",
                        key : "name",
                    },
                    {
                        name : "Nom",
                        key : "name",
                    },
                ] */
            },

            grit_bin : {
                name : "Boîte contenant du sable ou un mélange de sel et de sable pour lutter contre le verglas",
                tag : "amenity",
                type : "grit_bin",
            },

            motorcycle_parking: {
                name : "Parking pour véhicules deux-roues motorisés",
                tag : "amenity",
                type : "motorcycle_parking",
            },

            parking: {
                name : "Parking",
                tag : "amenity",
                type : "parking",
                marker : "/images/markers/osm/parking.png",
            },

            parking_entrance: {
                name : "Entrée ou sortie de parking souterrain simple ou à plusieurs étages",
                tag : "amenity",
                type : "parking_entrance",
            },

            parking_space: {
                name : "Place de parking unique",
                tag : "amenity",
                type : "parking_space",
            },

            taxi: {
                name : "Station de taxis",
                tag : "amenity",
                type : "taxi",
                marker : "/images/markers/osm/taxi.png",
            },

            weighbridge: {
                name : "Balance pour peser les véhicules et les marchandises",
                tag : "amenity",
                type : "weighbridge",
            },
        }
    },

    argent : {
        name : "Argent",
        element : {
            atm : {
                name : "Distributeur automatique de billets",
                tag : "amenity",
                type : "atm",
                marker : "/images/markers/osm/atm.png",
            },

            payment_terminal : {
                name : "Kiosque/terminal de paiement en libre-service",
                tag : "amenity",
                type : "payment_terminal",
            },

            banque : {
                name : "Banque",
                tag : "amenity",
                type : "bank",
                marker : "/images/markers/osm/banque.png",
            },

            bureau_de_change : {
                name : "Bureau de change",
                tag : "amenity",
                type : "bureau_de_change",
            },
        }
    },

    sante : {
        name : "Santé",
        element : {
            baby_hatch : {
                name : "Tour d'abandon",
                tag : "amenity",
                type : "baby_hatch",
            },

            clinic : {
                name : "Clinique",
                tag : "amenity",
                type : "clinic",
                marker : "/images/markers/osm/clinic.png",
            },

            dentist : {
                name : "Dentiste, chirurgien dentiste",
                tag : "amenity",
                type : "dentist",
                marker : "/images/markers/osm/dentist.png",
            },

            doctors : {
                name : "Cabinet médical, médecin généraliste ou spécialiste",
                tag : "amenity",
                type : "doctors",
                marker : "/images/markers/osm/doctors.png",
            },

            hospital : {
                name : "Hôpital",
                tag : "amenity",
                type : "hospital",
                marker : "/images/markers/osm/hospital.png",
            },

            nursing_home : {
                name : "Centre d'hébergement pour personnes handicapées ou âgées",
                tag : "amenity",
                type : "nursing_home",
            },

            pharmacy: {
                name : "Pharmacie",
                tag : "amenity",
                type : "pharmacy",
                marker : "/images/markers/osm/pharmacy.png",
            },

            social_facility : {
                name : "Services sociaux",
                tag : "amenity",
                type : "social_facility",
            },

            veterinary: {
                name : "Vétérinaire",
                tag : "amenity",
                type : "veterinary",
                marker : "/images/markers/osm/veterinary.png",
            },
        }
    },

    loisirArtCulture : {
        name : "Loisirs, arts et culture",
        element : {
            arts_centre : {
                name : "Centre des arts, centre culturel",
                tag : "amenity",
                type : "arts_centre",
                marker : "/images/markers/osm/arts-crafts.png",
            },

            brothel : {
                name : "Bordel/Établissement dédié à la prostitution",
                tag : "amenity",
                type : "brothel",
            },

            casino : {
                name : "Casino",
                tag : "amenity",
                type : "casino",
            },

            cinema : {
                name : "Cinéma",
                tag : "amenity",
                type : "cinema",
                marker : "/images/markers/osm/cinema.png",
            },

            community_centre : {
                name : "Salle des fêtes",
                tag : "amenity",
                type : "community_centre",
                marker : "/images/markers/osm/community.png",
            },

            conference_centre : {
                name : "Grand bâtiment utilisé pour organiser une convention",
                tag : "amenity",
                type : "conference_centre",
            },

            events_venue: {
                name : "Bâtiment spécifiquement utilisé pour l'organisation d'événements",
                tag : "amenity",
                type : "events_venue",
            },

            exhibition_centre : {
                name : "Centre d'exposition",
                tag : "amenity",
                type : "exhibition_centre",
            },

            fountain: {
                name : "Fontaine",
                tag : "amenity",
                type : "fountain",
            },

            gambling: {
                name : "Salle de jeux",
                tag : "amenity",
                type : "gambling",
            },

            love_hotel: {
                name : "Love hôtel(hôtel de court séjour dans le but d'offrir aux clients une certaine intimité pour des activités sexuelles)",
                tag : "amenity",
                type : "love_hotel",
                marker : "/images/markers/osm/love_hotel.png",
                
            },

            music_venue: {
                name : "Lieu intérieur pour écouter de la musique contemporaine en direct",
                tag : "amenity",
                type : "music_venue",
                marker : "/images/markers/osm/music_venue.png",
            },

            nightclub: {
                name : "Boîte de nuit",
                tag : "amenity",
                type : "nightclub",
                marker : "/images/markers/osm/nightlife.png",
            },

            planetarium: {
                name : "Planétarium",
                tag : "amenity",
                type : "planetarium",
            },

            public_bookcase : {
                name : "Microbibliothèque publique",
                tag : "amenity",
                type : "public_bookcase",
                marker : "/images/markers/osm/books-media.png",
            },

            social_centre: {
                name : "Pôle associatif. Centre pour fraternités, sororités, organisations de vétérans, organisations fraternelles, sociétés de professionnels, associations, locaux syndicaux et Organisation à but non lucratif.",
                tag : "amenity",
                type : "social_centre",
            },

            stripclub: {
                name : "Boîte de strip-tease",
                tag : "amenity",
                type : "stripclub",
            },

            studio: {
                name : "Studio d'enregistrement",
                tag : "amenity",
                type : "studio",
            },

            swingerclub: {
                name : "Club échangiste",
                tag : "amenity",
                type : "swingerclub",
            },

            theatre: {
                name : "Théâtre",
                tag : "amenity",
                type : "theatre",
                marker : "/images/markers/osm/theatre.png",
            },
        }
    },

    publicService : {
        name : "Service public",
        element : {
            courthouse : {
                name : "Palais de justice",
                tag : "amenity",
                type : "courthouse",
                marker : "/images/markers/osm/courthouse.png",
            },

            fire_station : {
                name : "Caserne de pompiers",
                tag : "amenity",
                type : "fire_station",
            },

            police : {
                name : "Poste de police, Gendarmerie",
                tag : "amenity",
                type : "police",
                marker : "/images/markers/osm/police.png",
            },

            post_box : {
                name : "Boîte aux lettres",
                tag : "amenity",
                type : "post_box",
            },

            post_depot : {
                name : "Dépôt de poste ou bureau de livraison",
                tag : "amenity",
                type : "post_depot",
            },

            post_office : {
                name : "Bureau de poste",
                tag : "amenity",
                type : "post_office",
                marker : "/images/markers/osm/post_office.png",
            },

            prison : {
                name : "Prison",
                tag : "amenity",
                type : "prison",
            },

            ranger_station : {
                name : "Services publics des Parcs Nationaux",
                tag : "amenity",
                type : "ranger_station",
            },

            townhall : {
                name : "Mairie",
                tag : "amenity",
                type : "townhall",
            },
        }
    },

    facilities : {
        name : "Installations",
        element : {
            bbq : {
                name : "Barbecue. Un gril public pour la cuisson de viandes ou de légumes",
                tag : "amenity",
                type : "bbq",
            },

            bench : {
                name : "Banc public",
                tag : "amenity",
                type : "bench",
            },

            dog_toilet : {
                name : "Zone désignée pour que les chiens puissent uriner et excréter",
                tag : "amenity",
                type : "dog_toilet",
            },

            dressing_room : {
                name : "Espace réservé au changement de vêtements",
                tag : "amenity",
                type : "dressing_room",
            },

            drinking_water : {
                name : "Source d'eau potable pour promeneurs et randonneurs",
                tag : "amenity",
                type : "drinking_water",
            },

            give_box : {
                name : "Installation où les gens déposent et récupèrent divers types d'objets dans le but de les partager et de les réutiliser gratuitement",
                tag : "amenity",
                type : "give_box",
            },

            mailroom : {
                name : "Une salle de courrier pour recevoir des paquets ou des lettres",
                tag : "amenity",
                type : "mailroom",
            },

            parcel_locker : {
                name : "Machine pour ramasser et envoyer des colis",
                tag : "amenity",
                type : "parcel_locker",
            },

            shelter : {
                name : "Abri contre le mauvais temps",
                tag : "amenity",
                type : "shelter",
            },

            shower : {
                name : "Bains publics",
                tag : "amenity",
                type : "shower",
            },

            telephone : {
                name : "Téléphone public, cabine publique",
                tag : "amenity",
                type : "telephone",
            },

            toilets : {
                name : "Toilettes publiques",
                tag : "amenity",
                type : "toilets",
                marker : "/images/markers/osm/toilets.png",
            },

            water_point : {
                name : "Point d'eau Endroit où vous pouvez obtenir de grandes quantités d'eau potable",
                tag : "amenity",
                type : "water_point",
            },

            watering_place : {
                name : "Point d'eau où les animaux peuvent boire",
                tag : "amenity",
                type : "watering_place",
            },
        }
    },

    wasteManagement : {
        name : "Gestion des déchets",
        element : {
            sanitary_dump_station : {
                name : "Station de vidange sanitaire",
                tag : "amenity",
                type : "sanitary_dump_station",
            },

            recycling : {
                name : "Point de collecte pour le recyclage",
                tag : "amenity",
                type : "recycling",
            },

            waste_basket : {
                name : "Poubelle publique",
                tag : "amenity",
                type : "waste_basket",
                marker : "/images/markers/osm/waste_basket.png",
            },

            waste_disposal : {
                name : "Un réceptacle destiné à recevoir des déchets domestiques ou industriels enveloppés dans des sacs en plastique",
                tag : "amenity",
                type : "waste_disposal",
            },

            waste_transfer_station : {
                name : "Station de transfert(décharge,incinérateur,recyclage,...)",
                tag : "amenity",
                type : "waste_transfer_station",
            },
        }
    },

    others : {
        name : "Autres",
        element : {
            animal_boarding : {
                name : "Pension(Lieu où vous pouvez laisser votre animal)",
                tag : "amenity",
                type : "animal_boarding",
            },

            animal_breeding : {
                name : "Installation où l'on élève des animaux",
                tag : "amenity",
                type : "animal_breeding",
            },

            animal_shelter : {
                name : "Refuge. Lieu qui récupère les animaux qui sont en difficulté",
                tag : "amenity",
                type : "animal_shelter",
            },

            animal_training : {
                name : "Installation utilisée pour le dressage non compétitif des animaux",
                tag : "amenity",
                type : "animal_training",
            },

            baking_oven : {
                name : "Four à pain. Un four utilisé pour la cuisson du pain et autres denrées similaires, situé par exemple dans une boulangerie",
                tag : "amenity",
                type : "baking_oven",
            },

            clock : {
                name : "Horloge publique",
                tag : "amenity",
                type : "clock",
            },

            crematorium: {
                name : "Crématorium",
                tag : "amenity",
                type : "crematorium",
            },

            dive_centre: {
                name : "Centre de plongée",
                tag : "amenity",
                type : "dive_centre",
            },

            funeral_hall: {
                name : "Lieu où se déroule une cérémonie funéraire",
                tag : "amenity",
                type : "funeral_hall",
            },

            grave_yard: {
                name : "Petit cimetière",
                tag : "amenity",
                type : "grave_yard",
            },

            hunting_stand: {
                name : "Affût de chasse",
                tag : "amenity",
                type : "hunting_stand",
            },

            internet_cafe: {
                name : "Cybercafé",
                tag : "amenity",
                type : "internet_cafe",
                marker : "/images/markers/osm/internet.png",
            },

            kitchen: {
                name : "Cuisine publique dans un établissement à l'usage de tous ou des clients",
                tag : "amenity",
                type : "kitchen",
            },

            kneipp_water_cure: {
                name : "Bain de pied extérieur",
                tag : "amenity",
                type : "kneipp_water_cure",
            },

            lounger: {
                name : "Objet pour s'allonger",
                tag : "amenity",
                type : "lounger",
            },

            marketplace: {
                name : "Place de marché",
                tag : "amenity",
                type : "marketplace",
                marker : "/images/markers/osm/market.png",
            },

            monastery: {
                name : "Monastère",
                tag : "amenity",
                type : "monastery",
                marker : "/images/markers/osm/monastery.png",
            },

            photo_booth: {
                name : "Cabine photographique automatique",
                tag : "amenity",
                type : "photo_booth",
                marker : "/images/markers/osm/photography.png",
            },

            place_of_mourning: {
                name : "Pièce ou un bâtiment où les familles et les amis peuvent venir, avant les funérailles, voir le corps de la personne décédée",
                tag : "amenity",
                type : "place_of_mourning",
            },

            place_of_worship: {
                name : "Édifice religieux (église, chapelle, mosquée, temple, synagogue, etc.)",
                tag : "amenity",
                type : "place_of_worship",
                input : [
                    {
                        name : "Religion",
                        tag : "religion",
                        type : "text"
                    },
                ]
            },

            public_bath: {
                name : "Lieu où le public peut se baigner en commun",
                tag : "amenity",
                type : "public_bath",
            },

            refugee_site: {
                name : "Établissement humain abritant des réfugiés ou des personnes déplacées à l'intérieur de leur propre pays",
                tag : "amenity",
                type : "refugee_site",
            },

            vending_machine: {
                name : "Distributeur automatique - boissons, tickets, etc. Tout sauf de l'argent",
                tag : "amenity",
                type : "vending_machine",
            },
        }
    },


    tourisme : {
        name : "Tourisme",
        element : {
            alpine_hut : {
                name : "Refuge de montagne",
                tag : "tourism",
                type : "alpine_hut",
            },

            apartment : {
                name : "Meublé de tourisme sous la forme d'un ou plusieurs appartements à louer pour de courtes périodes dans un but de vacances",
                tag : "tourism",
                type : "apartment",
                marker : "/images/markers/osm/apartment.png",
            },

            aquarium : {
                name : "Aquarium public",
                tag : "tourism",
                type : "aquarium",
            },

            artwork : {
                name : "Œuvre d'art",
                tag : "tourism",
                type : "artwork",
            },

            attraction : {
                name : "Attraction touristique",
                tag : "tourism",
                type : "attraction",
                marker : "/images/markers/osm/attraction.png",
            },

            camp_pitch : {
                name : "Emplacement dans un camping ou une aire pour caravanes",
                tag : "tourism",
                type : "camp_pitch",
                marker : "/images/markers/osm/places.png",
            },

            camp_site: {
                name : "Camping",
                tag : "tourism",
                type : "camp_site",
                marker : "/images/markers/osm/camp_site.png",
            },

            caravan_site: {
                name : "Aire pour caravanes",
                tag : "tourism",
                type : "caravan_site",
            },

            chalet: {
                name : "Meublé de tourisme (contient les gîtes ruraux entre autres) avec des installations de cuisine / salle de bain / WC autonomes",
                tag : "tourism",
                type : "chalet",
            },

            gallery: {
                name : "Galerie d'art",
                tag : "tourism",
                type : "gallery",
            },

            guest_house: {
                name : "Chambres d'hôtes chez l'habitant",
                tag : "tourism",
                type : "guest_house",
            },

            hostel: {
                name : "Auberge de jeunesse",
                tag : "tourism",
                type : "hostel",
            },

            hotel: {
                name : "Hôtel",
                tag : "tourism",
                type : "hotel",
                marker : "/images/markers/osm/hotel.png",
            },

            information: {
                name : "Office de tourisme, Point d'information",
                tag : "tourism",
                type : "information",
            },

            motel: {
                name : "Motel",
                tag : "tourism",
                type : "motel",
                marker : "/images/markers/osm/motel.png",
            },

            museum: {
                name : "Musée",
                tag : "tourism",
                type : "museum",
                marker : "/images/markers/osm/museum.png",
            },

            picnic_site: {
                name : "Aire de pique-nique",
                tag : "tourism",
                type : "picnic_site",
            },

            theme_park: {
                name : "Parc d'attraction",
                tag : "tourism",
                type : "theme_park",
                marker : "/images/markers/osm/theme_park.png",
            },

            viewpoint: {
                name : "Point de vue",
                tag : "tourism",
                type : "viewpoint",
            },

            wilderness_hut	: {
                name : "Abri de bivouac, refuge non gardé",
                tag : "tourism",
                type : "wilderness_hut",
            },

            zoo: {
                name : "Zoo",
                tag : "tourism",
                type : "zoo",
            },

            yes: {
                name : "Pour signaler un intérêt touristique sur un élément déjà taggué par ailleurs",
                tag : "tourism",
                type : "yes",
            },
        }
    },

    sports : {
        name : "Sports",
        element : {
            pin9 : {
                name : "Bowling, variante européenne avec 9 quilles",
                tag : "sport",
                type : "9pin",
            },

            pin10 : {
                name : "Bowling américain (10 quilles)",
                tag : "sport",
                type : "10pin",
            },

            american_football : {
                name : "Football américain",
                tag : "sport",
                type : "american_football",
                marker : "/images/markers/osm/american_football.png"
            },

            aikido : {
                name : "Aïkido",
                tag : "sport",
                type : "aikido",
            },

            archery : {
                name : "Tir à l'arc",
                tag : "sport",
                type : "archery",
            },

            athletics : {
                name : "Athlétisme",
                tag : "sport",
                type : "athletics",
            },

            australian_football: {
                name : "Football australien",
                tag : "sport",
                type : "australian_football",
            },

            badminton: {
                name : "Badminton",
                tag : "sport",
                type : "badminton",
            },

            bandy: {
                name : "Bandy ou hockey sur gazon russe",
                tag : "sport",
                type : "bandy",
            },

            base: {
                name : "BASE jump",
                tag : "sport",
                type : "base",
            },

            baseball: {
                name : "Baseball",
                tag : "sport",
                type : "baseball",
            },

            basketball: {
                name : "Basket-ball",
                tag : "sport",
                type : "basketball",
                marker : "/images/markers/osm/basketball.png",
            },

            beachvolleyball: {
                name : "Beach-volley",
                tag : "sport",
                type : "beachvolleyball",
            },

            billiards: {
                name : "Billard.",
                tag : "sport",
                type : "billiards",
            },

            bmx: {
                name : "Bicycle Motocross (BMX) ou bicross",
                tag : "sport",
                type : "bmx",
            },

            bobsleigh: {
                name : "Bobsleigh ou bobsled",
                tag : "sport",
                type : "bobsleigh",
            },

            boules: {
                name : "Jeux de boules(pétanque, lyonnaise, extrême, ...)",
                tag : "sport",
                type : "boules",
            },

            bowls: {
                name : "Boulingrin",
                tag : "sport",
                type : "bowls",
            },

            boxing: {
                name : "Boxe",
                tag : "sport",
                type : "boxing",
            },

            bullfighting	: {
                name : "La tauromachie, à pied ou à cheval",
                tag : "sport",
                type : "bullfighting",
            },

            canadian_football: {
                name : "Football canadien",
                tag : "sport",
                type : "canadian_football",
            },

            canoe: {
                name : "Canoë-Kayak",
                tag : "sport",
                type : "canoe",
            },

            chess: {
                name : "Jeu d'échecs",
                tag : "sport",
                type : "chess",
            },

            cliff_diving: {
                name : "Saut ou plongeon dans l'eau depuis une falaise ou un rocher",
                tag : "sport",
                type : "cliff_diving",
            },

            climbing: {
                name : "Escalade",
                tag : "sport",
                type : "climbing",
            },

            climbing_adventure: {
                name : "Parcours acrobatique en hauteur (PAH) ou parcours aventure en forêt, parcours accrobranche",
                tag : "sport",
                type : "climbing_adventure",
            },

            cockfighting: {
                name : "Combat de coqs",
                tag : "sport",
                type : "cockfighting",
            },

            cricket: {
                name : "Cricket",
                tag : "sport",
                type : "cricket",
            },

            croquet: {
                name : "Croquet",
                tag : "sport",
                type : "croquet",
            },

            curling: {
                name : "Curling",
                tag : "sport",
                type : "curling",
            },

            cycling: {
                name : "Cyclisme",
                tag : "sport",
                type : "cycling",
            },

            darts: {
                name : "Fléchettes",
                tag : "sport",
                type : "darts",
            },

            dog_racing : {
                name : "Course de chiens",
                tag : "sport",
                type : "dog_racing",
            },



            equestrian  : {
                name : "Équitation",
                tag : "sport",
                type : "equestrian",
            },

            fencing : {
                name : "Escrime",
                tag : "sport",
                type : "fencing",
            },

            field_hockey : {
                name : "Hockey sur gazon",
                tag : "sport",
                type : "field_hockey",
            },
        }
    },

    leisure : {
        name : "Loisirs",
        element : {
            adult_gaming_centre : {
                name : "Salle de jeux pour adultes. Un lieu avec des jeux payants (machines à sous, jeux vidéo, simulateurs de conduite, flippers,...)",
                tag : "leisure",
                type : "adult_gaming_centre",
            },

            amusement_arcade : {
                name : "Salle de jeux. Un lieu avec des jeux payants",
                tag : "leisure",
                type : "amusement_arcade",
            },

            bandstand : {
                name : "Kiosque à musique",
                tag : "leisure",
                type : "bandstand",
            },

            bathing_place : {
                name : "Lieu public où l'on peut se baigner librement dans la nature",
                tag : "leisure",
                type : "bathing_place",
            },

            beach_resort: {
                name : "Pour définir les limites d'une station balnéaire ou d'une plage gérée",
                tag : "leisure",
                type : "beach_resort",
            },

            bird_hide: {
                name : "Observatoire (souvent camouflé) pour observer les oiseaux",
                tag : "leisure",
                type : "bird_hide",
            },

            bleachers: {
                name : "Des rangées de bancs surélevés et étagés trouvés lors de manifestations de spectateurs",
                tag : "leisure",
                type : "bleachers",
            },

            bowling_alley: {
                name : "Une installation qui est équipée pour jouer aux quilles",
                tag : "leisure",
                type : "bowling_alley",
            },

            common: {
                name : "Espace où chacun peut se promener où bon lui semble ",
                tag : "leisure",
                type : "common",
            },

            dance: {
                name : "Un endroit pour danser (piste de danse, salle de bal, dancing). Ne pas confondre avec une discothèque",
                tag : "leisure",
                type : "dance",
            },

            disc_golf_course: {
                name : "Parcours de  Disc golf",
                tag : "leisure",
                type : "disc_golf_course",
            },

            dog_park: {
                name : "Aire clôturée ou non, où les propriétaires de chiens peuvent faire prendre de l'exercice à leur animal de compagnie",
                tag : "leisure",
                type : "dog_park",
            },

            escape_game: {
                name : "Jeu d'évasion",
                tag : "leisure",
                type : "escape_game",
            },

            firepit: {
                name : "Emplacement de feu. Emplacement ou structure permanente prévu(e) pour accueillir les feux de camp",
                tag : "leisure",
                type : "firepit",
            },

            fishing: {
                name : "Zone de pêche (de loisir)",
                tag : "leisure",
                type : "fishing",
            },

            fitness_centre: {
                name : "Centre de fitness",
                tag : "leisure",
                type : "fitness_centre",
            },

            fitness_station: {
                name : "Un lieu avec des machines d'exercice, individuelles ou en groupe, en plein air",
                tag : "leisure",
                type : "fitness_station",
            },

            garden: {
                name : "Jardin (décoratif ou à but scientifique)",
                tag : "leisure",
                type : "garden",
            },
        }
    },

    route : {
        name : "Routes",
        element : {
            motorway : {
                name : "Autoroute",
                tag : "highway",
                type : "motorway",
            },

            trunk : {
                name : "Voie rapide ou voie express",
                tag : "highway",
                type : "trunk",
            },

            primary : {
                name : "Route majeure reliant des grandes villes",
                tag : "highway",
                type : "primary",
            },

            secondary : {
                name : "Route faisant partie du réseau national sans être une 'primary' (secondaire)",
                tag : "highway",
                type : "secondary",
            },

            tertiary : {
                name : "Route reliant des villages ou des quartiers a l'intérieur des villes",
                tag : "highway",
                type : "tertiary",
            },

            unclassified : {
                name : "Voie de desserte locale, sans trafic de transit, ni en zone résidentielle",
                tag : "highway",
                type : "unclassified",
            },

            residential : {
                name : "Route de desserte ou à l'intérieur d'une zone résidentielle qui n'a pas de nomenclature spécifique",
                tag : "highway",
                type : "residential",
            },

            bus_stop : {
                name : "Arrêt de bus",
                tag : "highway",
                type : "bus_stop",
            },
        }
    },

    government : {
        name : "Agences et institutions gouvernementales",
        element : {
            archive : {
                name : "Archive",
                tag : "government",
                type : "archive",
            },

            audit : {
                name : "Institution gouvernementale effectuant un contrôle financier et/ou juridique de la branche exécutive du pouvoir",
                tag : "government",
                type : "audit",
            },

            administrative : {
                name : "Bureau administratif du gouvernement",
                tag : "government",
                type : "administrative",
            },

            bailiff: {
                name : "Bureau d'un huissier de justice",
                tag : "government",
                type : "bailiff",
            },

            border_control: {
                name : "Bureau ou bâtiment de contrôle des frontières",
                tag : "government",
                type : "border_control",
            },

            cadaster: {
                name : "Bureau du cadastre",
                tag : "government",
                type : "cadaster",
            },

            county: {
                name : "Département",
                tag : "government",
                type : "county",
            },

            communications: {
                name : "Communication",
                tag : "government",
                type : "communications",
            },

            culture: {
                name : "Bureau de la culture",
                tag : "government",
                type : "culture",
            },

            customs: {
                name : "Structure située à proximité ou au niveau d'une frontière internationale ou à l'intérieur d'un pays, où les voyageurs et/ou les véhicules sont inspectés pour l'importation et l'exportation de marchandises",
                tag : "government",
                type : "customs",
            },

            data_protection: {
                name : "Indique le service gouvernemental chargé de la protection des données",
                tag : "government",
                type : "data_protection",
            },

            education: {
                name : "Bureau d'un département de l'éducation",
                tag : "government",
                type : "education",
            },

            election_commission: {
                name : "Commission électorale",
                tag : "government",
                type : "election_commission",
            },

            emergency: {
                name : "Bureau gouvernemental chargé de l'administration/du contrôle des situations d'urgence",
                tag : "government",
                type : "emergency",
            },

            energy: {
                name : "Office gouvernemental de l'énergie",
                tag : "government",
                type : "energy",
            },

            environment: {
                name : "Office gouvernemental de l'environnement",
                tag : "government",
                type : "environment",
            },

            forestry: {
                name : "Bureau gouvernemental des forêts",
                tag : "government",
                type : "forestry",
            },

            healthcare: {
                name : "Bureau d'un département de la santé",
                tag : "government",
                type : "healthcare",
            },

            immigration: {
                name : "Bureau d'un département de l'immigration",
                tag : "government",
                type : "immigration",
            },

            intelligence: {
                name : "Bureau d'un département du renseignement",
                tag : "government",
                type : "intelligence",
            },

            investigation: {
                name : "Office d'un département d'enquête",
                tag : "government",
                type : "investigation",
            },

            it: {
                name : "Un service gouvernemental responsable des technologies de l'information et/ou de la communication",
                tag : "government",
                type : "it",
            },

            legislative: {
                name : "Un bureau législatif gouvernemental",
                tag : "government",
                type : "legislative",
            },

            migration: {
                name : "Office d'un département de migration",
                tag : "government",
                type : "migration",
            },

            ministry: {
                name : "Le siège d'un ministère",
                tag : "government",
                type : "ministry",
            },

            pension_fund: {
                name : "Office d'un département du fonds de pension",
                tag : "government",
                type : "pension_fund",
            },

            parliament: {
                name : "Bureau d'un département du parlement",
                tag : "government",
                type : "parliament",
            },

            police: {
                name : "Bureau de Police",
                tag : "government",
                type : "police",
            },

            presidency: {
                name : "Bureau d'un département de la présidence",
                tag : "government",
                type : "presidency",
            },

            prosecutor: {
                name : "Bureau du procureur",
                tag : "government",
                type : "prosecutor",
            },

            public_safety: {
                name : "Bureau de la sécurité publique",
                tag : "government",
                type : "public_safety",
            },

            public_service: {
                name : "L'office de la fonction publique",
                tag : "government",
                type : "public_service",
            },

            public_utility: {
                name : "Bureau des services publics",
                tag : "government",
                type : "public_utility",
            },

            register_office: {
                name : "Bureau d'enregistrement d'état-civil: naissances, mariages, décès,...",
                tag : "government",
                type : "register_office",
            },

            registration: {
                name : "Bureau d'immatriculation",
                tag : "government",
                type : "registration",
            },

            senate: {
                name : "Bureau du Sénat",
                tag : "government",
                type : "senate",
            },

            social_security: {
                name : "Office de la sécurité sociale",
                tag : "government",
                type : "social_security",
            },

            social_services: {
                name : "Office des services sociaux",
                tag : "government",
                type : "social_services",
            },

            social_welfare: {
                name : "Office d'aide sociale",
                tag : "government",
                type : "social_welfare",
            },

            sport: {
                name : "Office départemental du sport",
                tag : "government",
                type : "sport",
            },

            statistics: {
                name : "Bureau gouvernemental chargé de collecter et d'analyser des statistiques sur des sujets tels que l'économie et la démographie ; généralement responsable du recensement.",
                tag : "government",
                type : "statistics",
            },

            tax: {
                name : "Centre des finances publiques",
                tag : "government",
                type : "tax",
            },

            town_council: {
                name : "Bureau du conseil municipal",
                tag : "government",
                type : "town_council",
            },

            transportation: {
                name : "Un bureau gouvernemental qui s'occupe d'un aspect quelconque du transport",
                tag : "government",
                type : "transportation",
            },
            
            treasury: {
                name : "Trésor public",
                tag : "government",
                type : "treasury",
            },

            water_utility_company: {
                name : "Société de distribution d'eau",
                tag : "government",
                type : "water_utility_company",
            },

            youth_welfare_department: {
                name : "Service d'aide sociale à la jeunesse",
                tag : "government",
                type : "youth_welfare_department",
            },
        }
    },

    habitation : {
        name : "Habitation",
        element : {
            apartments : {
                name : "Immeuble contenant principalement des appartements",
                tag : "building",
                type : "apartments",
            },

            barracks : {
                name : "Caserne",
                tag : "building",
                type : "barracks",
            },

            bungalow : {
                name : "Bungalow, pavillon",
                tag : "building",
                type : "bungalow",
            },

            cabin : {
                name : "Cabane",
                tag : "building",
                type : "cabin",
            },

            detached : {
                name : "Maison individuelle non attenante à une autre (villa 4 façades)",
                tag : "building",
                type : "detached",
            },

            unclassified : {
                name : "Voie de desserte locale, sans trafic de transit, ni en zone résidentielle",
                tag : "building",
                type : "unclassified",
            },

            dormitory : {
                name : "Résidence universitaire ou foyer",
                tag : "building",
                type : "dormitory",
            },

            farm : {
                name : "Bâtiment d'habitation d'une ferme",
                tag : "building",
                type : "farm",
            },

            ger : {
                name : "Yourte installée de manière permanente ou saisonnière",
                tag : "building",
                type : "ger",
            },

            hotel: {
                name : "Hôtel",
                tag : "building",
                type : "hotel",
            },

            house: {
              name : "Maison familiale",
              tag : "building",
              type : "house",
            },

            houseboat: {
                name : "Péniche, bateau principalement utilisé comme habitation",
                tag : "building",
                type : "houseboat",
            },

            residential : {
                name : "Bâtiments résidentiels",
                tag : "building",
                type : "residential",
            },

            semidetached_house: {
                name : "Une villa 3 façades c'est à dire ayant un mur en commun avec la maison voisine",
                tag : "building",
                type : "semidetached_house",
            },

            static_caravan: {
                name : "Mobilhome laissé de manière (semi)permanente à un même emplacement",
                tag : "building",
                type : "static_caravan",
            },

            stilt_house: {
                name : "Une maison sur pilotis, au dessus du sol ou d'un plan d'eau",
                tag : "building",
                type : "stilt_house",
            },

            terrace : {
                name : "Rangée de maisons résidentielles mitoyennes",
                tag : "building",
                type : "terrace",
            },

            tree_house: {
                name : "Cabane dans les arbres",
                tag : "building",
                type : "tree_house",
            },

            trullo: {
                name : "Une hutte en pierre",
                tag : "building",
                type : "trullo",
            },
        }
    },
};


generalInputOsmContribution = [
    {
        name : "Nom en Français",
        tag : "name:fr",
        type : "text"
    },
    {
        name : "Nom en Anglais",
        tag : "name:en",
        type : "text"
    },
    {
        name : "Adresse : Ville",
        tag : "addr:city",
        type : "text"
    },
    {
        name : "Adresse : Code Postal",
        tag : "addr:postcode",
        type : "text"
    },
    {
        name : "Exploitant",
        tag : "operator",
        type : "text"
    },
    {
        name : "Type d'exploitant",
        tag : "operator:type",
        type : "select",
        option : [
            { label : "Publique", value : "public" }, { label : "Privé", value : "private" }, { label : "Gouvernement", value : "government" }, { label : "Entreprise", value : "business" }
            , { label : "Université", value : "university" }, { label : "Religieux", value : "religious" }, { label : "Privé à but non lucratif", value : "private_non_profit" }, { label : "Communauté", value : "community" }, { label : "Conseil", value : "council" }
            , { label : "Ecole", value : "school" }, { label : "Militaire", value : "military" }, { label : "Public / Gouvernement", value : "public/government" }
            , { label : "NGO", value : "ngo" }, { label : "charitable", value : "charitable" }, { label : "Association", value : "association" }, { label : "Coopérative", value : "cooperative" }
            , { label : "Société", value : "corporation" }, { label : "Zone d'urgence", value : "emergency_zone" }, { label : "Aide privée", value : "private_aided" }, { label : "Installation gouvernementale - publique", value : "government_facility-public" }
            , { label : "Gouvernement du barangay", value : "barangay_government" }, { label : "Privé à but lucratif", value : "private_for_profit" }, { label : "Soins de santé primaires de l'état", value : "state_primary_health_care" }
        ]
    },
];