<?php

use PixelHumain\PixelHumain\components\ThemeHelper;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\Application;

/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class CommunecterController extends Controller
{

  public $version = "v0.3.4";
  public $versionDate = "24/09/202116:00";
  public static $versionDates = [
      "v0.3.4" => "24/09/202116:00",
      "v0.3.3" => "15/07/202116:00",
      "v0.3.1" => "22/03/202112:00",
      "v0.2.8.11" => "05/10/202012:00" ,
      "v0.2.8.10" => "29/07/2016 19:12" ,
  ];
  public $title = "Communectez";
  public $subTitle = "se connecter à sa commune";
  public $pageTitle = "Communecter, le réseau social libre et connecté";
  public static $moduleKey = "communecter";
  public $keywords = "communecter,connecter, commun,commune, réseau, sociétal, citoyen, société, territoire, participatif, social, smarterre,tiers lieux, smart city, smartcity ";
  public $description = "Retrouvez la richesse qui vous entoure. Créez votre réseau, communiquez et agissez sur votre société. Communecter, le numérique libre au service du réel !";
  public $projectName = "";
  public $projectImage = "/images/CTK.png";
  public $projectImageL = "/images/logo.png";
  /*public $footerImages = array(
      array("img"=>"/images/logoORD.PNG","url"=>"http://openrd.io"),
      array("img"=>"/images/logo_region_reunion.png","url"=>"http://www.regionreunion.com"),
      array("img"=>"/images/technopole.jpg","url"=>"http://technopole-reunion.com"),
      array("img"=>"/images/Logo_Licence_Ouverte_noir_avec_texte.gif","url"=>"https://data.gouv.fr"),
      array("img"=>'/images/blog-github.png',"url"=>"https://github.com/orgs/pixelhumain/dashboard"),
      array("img"=>'/images/opensource.gif',"url"=>"http://opensource.org/"));*/
  const theme = "ph-dori";
  public $person = null;
  public $themeStyle = "theme-style11";//3,4,5,7,9
  public $notifications = array();


  public $pages = array(
    "admin" => array(
      "index"     => array("href" => "/ph/co2/admin"),
      "accueil"     => array("href" => "/ph/co2/accueil"),
      "directory" => array("href" => "/ph/co2/admin/directory"),
      "community" => array("href" => "/ph/co2/admin/community"),
      "reference" => array("href" => "/ph/co2/admin/reference"),
      "setsource" => array("href" => "/ph/co2/admin/setsource"),
      "switchto"  => array("href" => "/ph/co2/admin/switchto"),
      "delete"    => array("href" => "/ph/co2/admin/delete"),
      "activateuser"  => array("href" => "/ph/co2/admin/activateuser"),
      "importdata"    => array("href" => "/ph/co2/admin/importdata"),
      "previewdata"    => array("href" => "/ph/co2/admin/previewdata"),
      "importinmongo"    => array("href" => "/ph/co2/admin/importinmongo"),
      "assigndata"    => array("href" => "/ph/co2/admin/assigndata"),
      "checkdataimport"    => array("href" => "/ph/co2/admin/checkdataimport"),
      "openagenda"    => array("href" => "/ph/co2/admin/openagenda"),
      "checkventsopenagendaindb"    => array("href" => "/ph/co2/admin/checkventsopenagendaindb"),
      "importeventsopenagendaindb"    => array("href" => "/ph/co2/admin/importeventsopenagendaindb"),
      "checkgeocodage"   => array("href" => "/ph/co2/admin/checkgeocodage"),
      "getentitybadlygeolocalited"   => array("href" => "/ph/co2/admin/getentitybadlygeolocalited"),
      "getdatabyurl"   => array("href" => "/ph/co2/admin/getdatabyurl"),
      "adddata"    => array("href" => "/ph/co2/admin/adddata"),
      "adddataindb"    => array("href" => "/ph/co2/admin/adddataindb"),
      "createfileforimport"    => array("href" => "/ph/co2/admin/createfileforimport"),
      "sourceadmin"    => array("href" => "/ph/co2/admin/sourceadmin"),
      "moderate"    => array("href" => "/ph/co2/admin/moderate"),
      "statistics"    => array("href" => "/ph/co2/stat/chart"),
      "checkcities"    => array("href" => "/ph/co2/admin/checkcities"),
      "checkcedex"    => array("href" => "/ph/co2/admin/checkcedex"),
      "downloadfile" => array("href" => "/ph/co2/admin/downloadfile"),
      "createfile" => array("href" => "/ph/co2/admin/createfile"),
      "mailerrordashboard" => array("href" => "/ph/co2/admin/mailerrordashboard"),
      "mailslist" => array("href" => "ph/co2/admin/mailslist"),
      "notsendmail" => array("href" => "/ph/co2/admin/notsendmail"),
      "addnotsendmail" => array("href" => "/ph/co2/admin/addnotsendmail"),
      "getmailinglist" => array("href" => "/ph/co2/admin/getmailinglist"),
    ),
    "docs" => array(
      "index"     => array("href" => "/ph/co2/docs"),
    ),
    "adminpublic" => array(
      "index"    => array("href" => "/ph/co2/adminpublic/index"),
      "createfile" => array("href" => "/ph/co2/adminpublic/createfile"),
      "adddata"    => array("href" => "/ph/co2/adminpublic/adddata"),
      "adddataindb"    => array("href" => "/ph/co2/adminpublic/adddataindb"),
      "assigndata"    => array("href" => "/ph/co2/adminpublic/assigndata"),
      "setmapping" => array("href" => "/ph/co2/adminpublic/setmapping/"),
      "deletemapping" => array("href" => "/ph/co2/adminpublic/deletemapping/"),
      "getdatabyurl"   => array("href" => "/ph/co2/adminpublic/getdatabyurl"),
      "previewdata"    => array("href" => "/ph/co2/adminpublic/previewdata"),
      "mailslist" => array("href" => "ph/co2/adminpublic/mailslist"),
    ),
    "collections" => array(
      "add"    => array("href" => "/ph/co2/collections/add"),
      "list"    => array("href" => "/ph/co2/collections/list"),
      "crud"    => array("href" => "/ph/co2/collections/crud"),
    ),
    "folder" => array(
      "list"    => array("href" => "/ph/co2/folder/list"),
      "crud"    => array("href" => "/ph/co2/folder/crud"),
    ),
    "tool" => array(
      "get"    => array("href" => "/ph/co2/tool/get")
    ),
    "cron" => array(
      "docron"    => array("href" => "/ph/co2/cron/docron"),
      "checkdeletepending"    => array("href" => "/ph/co2/cron/checkdeletepending")
    ),
    "rocketchat" => array(
      "index"    => array("href" => "/ph/co2/rocketchat/index"),
      "cors"    => array("href" => "/ph/co2/rocketchat/cors"),
      "login"    => array("href" => "/ph/co2/rocketchat/login"),
      "logint"    => array("href" => "/ph/co2/rocketchat/logint"),
      "test"    => array("href" => "/ph/co2/rocketchat/test"),
      "testt"    => array("href" => "/ph/co2/rocketchat/testt"),
      "chat"    => array("href" => "/ph/co2/rocketchat/chat"),
      "list"    => array("href" => "/ph/co2/rocketchat/list"),
      "invite"    => array("href" => "/ph/co2/rocketchat/invite"),
    ),
    "default" => array(
      "index"                => array("href" => "/ph/co2/default/index", "public" => true),
      "render"                => array("href" => "/ph/co2/default/render", "public" => true),
      "directory"            => array("href" => "/ph/co2/default/directory", "public" => true),
      "directoryjs"            => array("href" => "/ph/co2/default/directoryjs", "public" => true),
      "agenda"               => array("href" => "/ph/co2/default/agenda", "public" => true),
      "news"                 => array("href" => "/ph/co2/default/news", "public" => true),
      "home"                 => array("href" => "/ph/co2/default/home", "public" => true),
      "apropos"              => array("href" => "/ph/co2/default/apropos", "public" => true),
      "add"                  => array("href" => "/ph/co2/default/add"),
      "view"                 => array("href" => "/ph/co2/default/view", "public" => true),
      "trad"                 => array("href" => "/ph/co2/default/trad", "public" => true),
      "dir"                  => array("href" => "/ph/co2/default/dir", "public" => true),
      "twostepregister"      => array("href" => "/ph/co2/default/twostepregister"),
      "switch"               => array("href" => "/ph/co2/default/switch"),
      "live"                 => array("href" => "/ph/co2/default/live"),
      "sitemap"                 => array("href" => "/ph/co2/default/sitemap"),
      "img"                 => array("href" => "/ph/co2/default/img"),
    ),
    "zone"=> array(
      "getscopebyids"               => array("href" => "/ph/co2/zone/getscopebyids", "public" => true),
    ),
    "city"=> array(
      "index"               => array("href" => "/ph/co2/city/index", "public" => true),
      "detail"              => array("href" => "/ph/co2/city/detail", "public" => true),
      "detailforminmap"     => array("href" => "/ph/co2/city/detailforminmap", "public" => true),
      "dashboard"           => array("href" => "/ph/co2/city/dashboard", "public" => true),
      "directory"           => array("href" => "/ph/co2/city/directory", "public" => true,
                                     "title"=>"City Directory", "subTitle"=>"Find Local Actors and Actions : People, Organizations, Events"),
      'statisticpopulation' => array("href" => "/ph/co2/city/statisticpopulation", "public" => true),
      'getcitydata'         => array("href" => "/ph/co2/city/getcitydata", "public" => true),
      'getcityjsondata'     => array("href" => "/ph/co2/city/getcityjsondata", "public" => true),
      'statisticcity'       => array("href" => "/ph/co2/city/statisticcity", "public" => true),
      'statisticPopulation' => array("href" => "/ph/co2/city/statisticPopulation", "public" => true),
      'getcitiesdata'       => array("href" => "/ph/co2/city/getcitiesdata"),
      'opendata'            => array("href" => "/ph/co2/city/opendata","public" => true),
      'getoptiondata'       => array("href" => "/ph/co2/city/getoptiondata"),
      'getlistoption'       => array("href" => "/ph/co2/city/getlistoption"),
      'getpodopendata'      => array("href" => "/ph/co2/city/getpodopendata"),
      'addpodopendata'      => array("href" => "/ph/co2/city/addpodopendata"),
      'getlistcities'       => array("href" => "/ph/co2/city/getlistcities"),
      'creategraph'         => array("href" => "/ph/co2/city/creategraph"),
      'graphcity'           => array("href" => "/ph/co2/city/graphcity"),
      'updatecitiesgeoformat' => array("href" => "/ph/co2/city/updatecitiesgeoformat","public" => true),
      'getinfoadressbyinsee'  => array("href" => "/ph/co2/city/getinfoadressbyinsee"),
      'cityexists'          => array("href" => "/ph/co2/city/cityexists"),
      'autocompletemultiscope'          => array("href" => "/ph/co2/city/autocompletemultiscope"),
      "save"               => array("href" => "/ph/co2/city/save", "public" => true),
      'getlevel'          => array("href" => "/ph/co2/city/getlevel"),
      'getcitiesbyscope'          => array("href" => "/ph/co2/city/getcitiesbyscope"),
      'getgeoshape'          => array("href" => "/ph/co2/city/getgeoshape"),
      'create'          => array("href" => "/ph/co2/city/create"),
    ),
    "news"=> array(
      "index"   => array( "href" => "/ph/co2/news/index", "public" => true,'title' => "Fil d'actualités - N.E.W.S", "subTitle"=>"Nord.Est.West.Sud","pageTitle"=>"Fil d'actualités - N.E.W.S"),
      "latest"  => array( "href" => "/ph/co2/news/latest"),
      "save"    => array( "href" => "/ph/co2/news/save"),
      "detail"    => array( "href" => "/ph/co2/news/detail"),
      "delete"    => array( "href" => "/ph/co2/news/delete"),
      "updatefield"    => array( "href" => "/ph/co2/news/updatefield"),
      "update"    => array( "href" => "/ph/co2/news/update"),
      "extractprocess" => array( "href" => "/ph/co2/news/extractprocess"),
      "moderate" => array( "href" => "/ph/co2/news/moderate"),
      "share"          => array("href" => "/ph/co2/news/share"),
    ),
    "search"=> array(
      "getmemberautocomplete" => array("href" => "/ph/co2/search/getmemberautocomplete"),
      "getshortdetailsentity" => array("href" => "/ph/co2/search/getshortdetailsentity"),
      "searchnextevent" => array("href" => "/ph/co2/search/searchnextevent"),
      "index"                 => array("href" => "/ph/co2/search/index"),
      "geteventsforcalendar"                 => array("href" => "/ph/co2/search/geteventsforcalendar"),
      "mainmap"               => array("href" => "/ph/co2/default/mainmap", "public" => true),
      "getzone" => array("href" => "/ph/co2/search/getzone", "public" => true)
    ),
    "network" => array(
      "simplydirectory"    => array("href" => "/ph/co2/network/simplydirectory"),
      "get"    => array("href" => "/ph/co2/network/get")
    ),
    "rooms"=> array(
      "index"    => array("href" => "/ph/co2/rooms/index"),
      "saveroom" => array("href" => "/ph/co2/rooms/saveroom"),
      "editroom" => array("href" => "/ph/co2/rooms/editroom"),
      "external" => array("href" => "/ph/co2/rooms/external"),
      "actions"  => array("href" => "/ph/co2/rooms/actions"),
      "action"   => array("href" => "/ph/co2/rooms/action"),
      "editaction" => array("href" => "/ph/co2/rooms/editaction"),
      'saveaction' => array("href" => "/ph/co2/rooms/saveaction"),
      'closeaction' => array("href" => "/ph/co2/rooms/closeaction"),
      'assignme' => array("href" => "/ph/co2/rooms/assignme"),
      'fastaddaction' => array("href" => "/ph/co2/rooms/fastaddaction"),
      'move' => array("href" => "/ph/co2/rooms/move"),
    ),
    "gantt"=> array(
      "index"            => array("href" => "/ph/co2/gantt/index", "public" => true),
      "savetask"         => array("href" => "/ph/co2/gantt/savetask"),
      "removetask"       => array("href" => "/ph/co2/gantt/removetask"),
      "generatetimeline" => array("href" => "/ph/co2/gantt/generatetimeline"),
      "addtimesheetsv"   => array("href" => "/ph/co2/gantt/addtimesheetsv"),
    ),
    "need"=> array(
        "index" => array("href" => "/ph/co2/need/index", "public" => true),
        "description" => array("href" => "/ph/co2/need/dashboard/description"),
        "dashboard" => array("href" => "/ph/co2/need/dashboard"),
        "detail" => array("href" => "/ph/co2/need/detail", "public" => true),
        "saveneed" => array("href" => "/ph/co2/need/saveneed"),
        "updatefield" => array("href" => "/ph/co2/need/updatefield"),
        "addhelpervalidation" => array("href" => "/ph/co2/need/addhelpervalidation"),
        "addneedsv" => array("href" => "/ph/co2/need/addneedsv"),
      ),
    "person"=> array(
        "login"           => array("href" => "/ph/co2/person/login",'title' => "Log me In"),
        "logged"           => array("href" => "/ph/co2/person/logged"),
        "settings"           => array("href" => "/ph/co2/person/settings"),
        "sendemail"       => array("href" => "/ph/co2/person/sendemail"),
        "index"           => array("href" => "/ph/co2/person/dashboard",'title' => "My Dashboard"),
        "authenticate"    => array("href" => "/ph/co2/person/authenticate",'title' => "Authentication"),
        "authenticatetoken"    => array("href" => "/ph/co2/person/authenticatetoken",'title' => "Authentication token"),
        "dashboard"       => array("href" => "/ph/co2/person/dashboard"),
        "detail"          => array("href" => "/ph/co2/person/detail", "public" => true),
        "follows"         => array("href" => "/ph/co2/person/follows"),
        "disconnect"      => array("href" => "/ph/co2/person/disconnect"),
        "register"        => array("href" => "/ph/co2/person/register"),
        "activate"        => array('href' => "/ph/co2/person/activate"),
        "updatesettings"        => array('href' => "/ph/co2/person/updatesettings"),
        "validateinvitation" => array('href' => "/ph/co2/person/validateinvitation", "public" => true),
        "logout"          => array("href" => "/ph/co2/person/logout"),
        'getthumbpath'    => array("href" => "/ph/co2/person/getThumbPath"),
        'getnotification' => array("href" => "/person/getNotification"),
        'changepassword'  => array("href" => "/person/changepassword"),
        'changerole'      => array("href" => "/person/changerole"),
        'checkusername'   => array("href" => "/person/checkusername"),
        'checkemail'   => array("href" => "/person/checkemail"),
        "invite"          => array("href" => "/ph/co2/person/invite"),
        "invitation"      => array("href" => "/ph/co2/person/invitation"),
        "updatefield"     => array("href" => "/person/updatefield"),
        "update"          => array("href" => "/person/update"),
        "getuserautocomplete" => array('href' => "/person/getUserAutoComplete"),
        'checklinkmailwithuser'   => array("href" => "/ph/co2/checklinkmailwithuser"),
        'getuseridbymail'   => array("href" => "/ph/co2/getuseridbymail"),
        "getbyid"         => array("href" => "/ph/co2/person/getbyid"),
        "getorganization" => array("href" => "/ph/co2/person/getorganization"),
        "updatename"      => array("href" => "/ph/co2/person/updatename"),
        "updateprofil"      => array("href" => "/ph/co2/person/updateprofil"),
        "updatewithjson"      => array("href" => "/ph/co2/person/updatewithjson"),
        "updatemultitag"      => array("href" => "/ph/co2/person/updatemultitag"),
        "updatemultiscope"      => array("href" => "/ph/co2/person/updatemultiscope"),
        "sendinvitationagain"      => array("href" => "/ph/co2/person/sendinvitationagain"),
        "removehelpblock"      => array("href" => "/ph/co2/person/removehelpblock"),


        "chooseinvitecontact"=> array('href'    => "/ph/co2/person/chooseinvitecontact"),
        "sendmail"=> array('href'   => "/ph/co2/person/sendmail"),

        "telegram"               => array("href" => "/ph/co2/person/telegram", "public" => true),

        //Init Data
        "clearinitdatapeopleall"  => array("href" =>"'/ph/co2/person/clearinitdatapeopleall'"),
        "initdatapeopleall"       => array("href" =>"'/ph/co2/person/initdatapeopleall'"),
        "importmydata"            => array("href" =>"'/ph/co2/person/importmydata'"),
        "about"                   => array("href" => "/person/about"),
        "data"                    => array("href" => "/person/scopes"),
        "directory"               => array("href" => "/ph/co2/city/directory", "public" => true, "title"=>"My Directory", "subTitle"=>"My Network : People, Organizations, Events"),


        "get"      => array("href" => "/ph/co2/person/get"),
        "getcontactsbymails"      => array("href" => "/ph/co2/person/getcontactsbymails"),
        "shoppingcart"      => array("href" => "/ph/co2/person/shoppingcart"),
        "updatescopeinter" => array("href" => "/ph/co2/person/updatescopeinter"),
    ),
    "organization"=> array(
      "get"                 => array("href" => "/ph/co2/organization/get"),
    ),
    "event"=> array(
    ),
    "project"=> array(
      "get"     => array("href" => "/ph/co2/project/get"),
    ),
    "crowdfunding"=> array(
      "validatepledge"     => array("href" => "/ph/co2/crowdfunding/validatepledge"),
      "getcampaignandcountersbyparent"=> array("href" => "/ph/co2/crowdfunding/getcampaignandcountersbyparent"),
      "getcampaignandcounters"=> array("href" => "/ph/co2/crowdfunding/getcampaignandcounters"),
      "getpledgesfromcampaignid" => array("href" => "/ph/co2/crowdfunding/getpledgesfromcampaignid"),
    ),
    "chart" => array(
	    "addchartsv"      => array("href" => "/ph/co2/chart/addchartsv"),
		  "index"      => array("href" => "/ph/co2/chart/index"),
      "header"      => array("href" => "/ph/co2/chart/header"),
		  "editchart"       => array("href" => "/ph/co2/chart/editchart"),
		  "get"       => array("href" => "/ph/co2/chart/get"),
    ),
    "cms"=> array(
      "directory"    => array("href" => "/ph/co2/cms/directory"),
      "delete"    => array("href" => "/ph/co2/cms/delete"),
      "insert"    => array("href" => "/ph/co2/cms/insert"),
      "getlistsbuilder"=> array("href" => "/ph/co2/cms/getlistsbuilder"),
      "admindasboard"=>array("href" => "/ph/co2/cms/admindashboard"),
      "updatecostum"=>array("href" => "/ph/co2/cms/updateCostum"),
      "refreshmenu"=>array("href" => "/ph/co2/cms/refreshmenu"),
    ),
    "cssFile"=> array(
      "insert"    => array("href" => "/ph/co2/cssFile/insert"),
      "getbycostumid"    => array("href" => "/ph/co2/cssFile/getbycostumid"),
    ),
    "pod" => array(
      "slidermedia" => array("href" => "/ph/co2/pod/slidermedia", "public" => true),
      "photovideo"   => array("href" => "ph/co2/pod/photovideo"),
      "fileupload"   => array("href" => "ph/co2/pod/fileupload"),
      "activitylist"   => array("href" => "ph/co2/pod/activitylist"),
      "circuit"      => array("href" => "/ph/co2/pod/circuit"),
      "preferences"      => array("href" => "/ph/co2/pod/preferences"),
    ),
    "settings" => array(
      "index" => array("href" => "/ph/co2/settings/index", "public" => true),
      "redirect" => array("href" => "/ph/co2/settings/redirect"),
      "notificationsaccount" => array("href" => "/ph/co2/settings/notificationsaccount"),
      "notificationscommunity" => array("href" => "/ph/co2/settings/notificationscommunity"),
      "confidentiality" => array("href" => "/ph/co2/settings/confidentiality"),
      "confidentialitycommunity" => array("href" => "/ph/co2/settings/confidentialitycommunity"),
      "chatmanager" => array("href" => "/ph/co2/settings/chatmanager"),
      "myaccount" => array("href" => "/ph/co2/settings/myaccount"),
    ),
    "bookmark" => array(
      "delete"        => array("href" => "ph/communecter/bookmark/delete"),
      "save"        => array("href" => "ph/communecter/bookmark/save"),
    ),
    "slug" => array(
      "check"        => array("href" => "ph/communecter/slug/check"),
      "getinfo"        => array("href" => "ph/communecter/slug/getinfo"),
    ),
    "gallery" => array(
      "index"        => array("href" => "ph/communecter/gallery/index"),
      "gallery"        => array("href" => "ph/communecter/gallery/gallery"),
      "crudcollection"        => array("href" => "ph/communecter/gallery/crudcollection"),
      "crudfile"        => array("href" => "ph/communecter/gallery/crudfile"),
      "removebyid"   => array("href" => "ph/communecter/gallery/removebyid"),
      "filter"   => array("href" => "ph/communecter/gallery/filter"),
    ),
    "link" => array(
      "removemember"        => array("href" => "/ph/co2/link/removemember"),
      "removerole"        => array("href" => "/ph/co2/link/removerole"),
      "removecontributor"   => array("href" => "/ph/co2/link/removecontributor"),
      "disconnect"        => array("href" => "/ph/co2/link/disconnect"),
      "connect"           => array("href" => "/ph/co2/link/connect"),
      "multiconnect"           => array("href" => "/ph/co2/link/multiconnect"),
      "follow"           => array("href" => "/ph/co2/link/follow"),
      "validate"          => array("href" => "/ph/co2/link/validate"),
      "updateadminlink"          => array("href" => "/ph/co2/link/updateadminlink"),
      "validateinvitationbymail" => array("href" => "/ph/co2/link/validateinvitationbymail"),
      "linkchildparent"        => array("href" => "/ph/co2/link/linkchildparent"),
      "createinvitationlink" => array("href" => "/ph/co2/link/createinvitationlink")
    ),
    "document" => array(
      "resized"             => array("href"=> "/ph/communecter/document/resized", "public" => true),
      "list"                => array("href"=> "/ph/communecter/document/list"),
      "save"                => array("href"=> "/ph/communecter/document/save"),
      "deletedocumentbyid"  => array("href"=> "/ph/communecter/document/deletedocumentbyid"),
      "removeAndBacktract"  => array("href"=> "/ph/communecter/document/removeAndBacktract"),
      "getlistbyid"         => array("href"=> "ph/communecter/document/getlistbyid"),
      "upload"              => array("href"=> "ph/communecter/document/upload"),
      "update"              => array("href"=> "ph/communecter/document/update"),
      "upload-save"          => array("href"=> "ph/communecter/document/upload-save"),
      "readauthorized"          => array("href"=> "ph/communecter/document/readauthorized"),
      "delete"              => array("href"=> "ph/communecter/document/delete"),
      'getlistdocumentswhere' => array("href"=> "ph/communecter/document/getlistdocumentswhere"),
    ),
    "survey" => array(
      "index"       => array("href" => "/ph/survey/co/index", "public" => true),
      "delete"      => array("href" => "/ph/survey/co/delete"),
      "tags"=> array("href" => "/ph/survey/co/tags"),
      "forms"=> array("href" => "/ph/survey/co/forms"),
      "form"=> array("href" => "/ph/survey/co/form"),
    ),
    "answer" => array(
        "index"              => array('href' => 'ph/survey/answer/index'),
        "new"              => array('href' => 'ph/survey/answer/new'),
        "directory"=> array('href' => 'ph/survey/answer/directory'),
        "views"=> array('href' => 'ph/survey/answer/views'),
        "get"              => array('href' => 'ph/survey/answer/get'),
        "send"              => array('href' => 'ph/survey/answer/send'),
        "sendmail"              => array('href' => 'ph/survey/answer/sendmail'),
        "validate"              => array('href' => 'ph/survey/answer/validate'),
        "dashboard"=> array('href' => 'ph/survey/answer/dashboard'),
        "graphbuilder"=> array('href' => 'ph/survey/answer/graphbuilder'),
        "ocecoformdashboard"=> array('href' => 'ph/survey/answer/ocecoformdashboard'),
        "ocecoorgadashboard"=> array('href' => 'ph/survey/answer/ocecoorgadashboard'),
        "reloadinput" => array('href' => 'ph/survey/answer/reloadinput'),
        "reloadwizard" => array('href' => 'ph/survey/answer/reloadwizard'),
        "newanswer" => array('href' => 'ph/survey/answer/newanswer'),
        "aapevaluate" => array('href' =>'survey.controllers.answer.AapEvaluateAction'),
        "answerwithemail" => array('href' =>'survey.controllers.answer.AnswerwithemailAction'),
        "getinput" => array('href' =>'survey.controllers.answer.GetinputAction'),
        "countvisit" => array('href' =>'survey.controllers.answer.CountvisitAction'),
        "getstep" => array('href' =>'survey.controllers.answer.GetstepAction'),
        "getparamswizard" => array('href' =>'survey.controllers.answer.GetParamsWizardAction'),
        "updatewizard" => array('href' =>'survey.controllers.answer.UpdateWizardAction'),
        "answernotification" => array('href' =>'survey.controllers.answer.AnswerNotificationAction'),
        "addproposition" => array('href' =>'survey.controllers.answer.AddpropositionAction'),
        "importanswer" => array('href' =>'survey.controllers.answer.ImportanswerAction'),
        "rcnotification" => array('href' =>'survey.controllers.answer.RcnotificationAction'),
        "multidashboard" => array('href' =>'survey.controllers.answer.MultiorgadashboardAction'),
        "transferanswer" => array('href' =>'survey.controllers.answer.TransferAnswerAction'),
        "actionlist" => array('href' =>'survey.controllers.answer.ActionlistAction'),
        "renamekey" => array('href' =>'survey.controllers.answer.RenamekeyAction'),
        "answer" => array('href' =>'survey.controllers.answer.AnswerAction'),
        "getparamsfinancerstandalone" => array('href' =>'survey.controllers.answer.GetParamsFinancerStandaloneAction'),
    ),
    "form" => array(
        "create"              => array('href' => 'ph/survey/form/create'),
        "edit"              => array('href' => 'ph/survey/form/edit'),
        "get"              => array('href' => 'ph/survey/form/get'),
        "getopenform"              => array('href' => 'ph/survey/form/getopenform'),
        "generateinputs"              => array('href' => 'ph/survey/form/generateinputs'),
        "generateproject"              => array('href' => 'ph/survey/form/generateproject'),
        "generatedepense"              => array('href' => 'ph/survey/form/generatedepense'),
        "delete"              => array('href' => 'ph/survey/form/delete'),
        "schema"=> array("href" => "/ph/survey/form/schema"),
        "getaapconfig" => array('href' => 'ph/survey/form/getaapconfig'),
        "templatizeform" => array('href' => 'ph/survey/form/templatizeform'),
        "duplicatetemplate" => array('href' => 'ph/survey/form/duplicatetemplate'),
        "getaapview" => array('href' => 'ph/survey/form/getaapview'),
        "checkfeedback" => array('href' => 'ph/survey/form/checkfeedback'),
        "coremudashboard" => array('href' => 'ph/survey/form/coremudashboard'),
        "coremufinancement" => array('href' => 'ph/survey/form/coremufinancement'),
        "getaaprole" => array('href' => 'ph/survey/form/getaaprole'),
        "pingupdate" => array('href' => 'ph/survey/form/pingupdate'),
        "switchcoremu" => array('href' =>'ph/survey/form/switchcoremu'),
    ),
    "discuss"=> array(
      "index" => array( "href" => "/ph/co2/discuss/index", "public" => true),
    ),
    "comment"=> array(
      "index"        => array( "href" => "/ph/co2/comment/index", "public" => true),
      "save"         => array( "href" => "/ph/co2/comment/save"),
      'abuseprocess' => array( "href" => "/ph/co2/comment/abuseprocess"),
      "testpod"      => array("href"  => "/ph/co2/comment/testpod"),
      "moderate"     => array( "href" => "/ph/co2/comment/moderate"),
      "delete"       => array( "href" => "/ph/co2/comment/delete"),
      "updatefield"  => array( "href" => "/ph/co2/comment/updatefield"),
      "update"  => array( "href" => "/ph/co2/comment/update"),
      "countcommentsfrom" => array( "href" => "/ph/co2/comment/countcommentsfrom"),
    ),
    "order"=> array(
       "save"   => array("href" => "/ph/co2/order/save"),
       "get"   => array("href" => "/ph/co2/order/get"),
    ),
    "circuit"=> array(
       "save"   => array("href" => "/ph/co2/circuit/save"),
       "index"   => array("href" => "/ph/co2/circuit/index"),
    ),
    "pay"=> array(
       "index"   => array("href" => "/ph/co2/pay/index"),
       "in"   => array("href" => "/ph/co2/pay/in"),
    ),
    "backup"=> array(
       "save"   => array("href" => "/ph/co2/backup/save"),
       "delete"   => array("href" => "/ph/co2/backup/delete"),
       "update"   => array("href" => "/ph/co2/backup/update"),
    ),
    "orderitem"=> array(
       "save"   => array("href" => "/ph/co2/orderitem/save"),
       "get"   => array("href" => "/ph/co2/orderitem/get"),
    ),
    "action"=> array(
       "addaction"   => array("href" => "/ph/co2/action/addaction"),
       "list"   => array("href" => "/ph/co2/action/list"),
    ),
    "notification"=> array(
      "getnotifications"          => array("href" => "/ph/co2/notification/get","json" => true),
      "marknotificationasread"    => array("href" => "/ph/co2/notification/remove"),
      "removeall" => array("href" => "/ph/co2/notification/removeall"),
      "update"                    => array("href" => "/ph/co2/notification/update"),
    ),
    "gamification"=> array(
      "index" => array("href" => "/ph/co2/gamification/index"),
    ),
    "graph"=> array(
      "getdata" => array("href" => "/ph/graph/getdata"),
      "viewer" => array("href" => "/ph/graph/viewer"),
      "d3" => array("href" => "/ph/graph/d3"),
      "search" => array("href" => "/ph/graph/search"),
      "doc" => array("href" => "/ph/graph/default/doc"),
      "children" => array("href" => "/ph/graph/co/children"),
    ),
    "cotools"=> array(
      "get" => array("href" => "/ph/cotools/default/get"),
    ),
    "log"=> array(
      "monitoring" => array("href" => "/ph/co2/log/monitoring"),
      "dbaccess"  => array("href" => "/ph/co2/log/dbaccess"),
      "clear"  => array("href" => "/ph/co2/log/clear"),
      //"cleanup"  => array("href" => "/ph/co2/log/cleanup")
    ),
    "stat"=> array(
      "createglobalstat" => array("href" => "/ph/co2/stat/createglobalstat"),
    ),
    "mailmanagement"=> array(
      "droppedmail" => array("href" => "/co2/mailmanagement/droppedmail"),
      "updatetopending" => array("href" => "/co2/mailmanagement/updatetopending"),
      "createandsend" => array("href" => "/co2/mailmanagement/createandsend"),
      "removedata" => array("href" => "/co2/mailmanagement/removedata"),
      "askdata" => array("href" => "/co2/mailmanagement/askdata"),
      "getdata" => array("href" => "/co2/mailmanagement/getdata"),
      "relaunchinvitation" => array("href" => "/co2/mailmanagement/relaunchinvitation"),
    ),
    "element"=> array(
      "updatepathvalue"            => array('href' => "/ph/co2/element/update/value",            "public" => true),
      "updatesettings"      => array('href' => "/ph/co2/element/updatesettings"),
      "updatefield"         => array("href" => "/ph/co2/element/updatefield"),
      "updatefields"        => array("href" => "/ph/co2/element/updatefields"),
      "updateblock"         => array("href" => "/ph/co2/element/updateblock"),
      "updatestatus"         => array("href" => "/ph/co2/element/updatestatus"),
      "detail"              => array("href" => "/ph/co2/element/detail", "public" => true),
      "newhome"              => array("href" => "/ph/co2/element/newhome", "public" => true),
      "getalllinks"         => array("href" => "/ph/co2/element/getalllinks"),
      "geturls"             => array("href" => "/ph/co2/element/geturls"),
      "getcuriculum"        => array("href" => "/ph/co2/element/getcuriculum"),
      "getcontacts"         => array("href" => "/ph/co2/element/getcontacts"),
      "simply"              => array("href" => "/ph/co2/element/simply", "public" => true),
      "directory"           => array("href" => "/ph/co2/element/directory", "public" => true),
      "directory2"          => array("href" => "/ph/co2/element/directory2", "public" => true),
      "addmembers"          => array("href" => "/ph/co2/element/addmembers", "public" => true),
      "save"                => array("href" => "/ph/co2/element/save"),
      "list"                => array("href" => "/ph/co2/element/list"),
      "savecontact"         => array("href" => "/ph/co2/element/savecontact"),
      "saveurl"             => array("href" => "/ph/co2/element/saveurl"),
      "get"                 => array("href" => "/ph/co2/element/get"),
      "delete"              => array("href" => "/ph/co2/element/delete"),
      "notifications"       => array("href" => "/ph/co2/element/notifications"),
      "about"               => array("href" => "/ph/co2/element/about"),
      "getdatadetail"       => array("href" => "/ph/co2/element/getdatadetail"),
      "stopdelete"          => array("href" => "/ph/co2/element/stopdelete"),
      'getthumbpath'        => array("href" => "/ph/co2/element/getThumbPath"),
      'getcommunexion'      => array("href" => "/ph/co2/element/getcommunexion"),
      'getdatabyurl'        => array("href" => "/ph/co2/element/getdatabyurl"),
      'network'        => array("href" => "/ph/co2/element/network"),
      'getnetworks'        => array("href" => "/ph/co2/element/getnetworks"),
      "invoice"                => array("href" => "/ph/co2/element/invoice"),
      "invite"                => array("href" => "/ph/co2/element/invite"),
      "askdata" => array("href" => "/co2/element/askdata"),
      "deletedata" => array("href" => "/co2/element/deletedata"),
//      "remove" => array("href" => "/co2/element/remove"),
      "getlastevents" => array("href" => "/co2/element/getlastevents"),
      "getcospace" => array("href" => "/co2/element/getcospace"),
      "unsetpath" => array("href" => "/co2/element/unsetpath"),
      "getexternalnetwork" => array("href" => "/co2/element/getexternalnetwork")
    ),
    "app" => array(
      "welcome"             => array('href' => "/ph/co2/app/welcome",         "public" => true),
      "view"             => array('href' => "/ph/co2/app/view",         "public" => true),
      "dashboard"             => array('href' => "/ph/co2/app/dashboard",         "public" => true),
      "home"              => array('href' => "/ph/co2/app/home",         "public" => true),
      "index"             => array('href' => "/ph/co2/app/index",             "public" => true),
      "live"              => array('href' => "/ph/co2/app/live",              "public" => true),
      "annonces"          => array('href' => "/ph/co2/app/annonces",          "public" => true),
      "live"              => array('href' => "/ph/co2/app/live",              "public" => true),
      "agenda"            => array('href' => "/ph/co2/app/agenda",            "public" => true),
      "help"            => array('href' => "/ph/co2/app/help",            "public" => true),
      "page"              => array('href' => "/ph/co2/app/page",              "public" => true),
      "admin"              => array('href' => "/ph/co2/app/admin",              "public" => true),
      "search"            => array('href' => "/ph/co2/app/search",            "public" => true),
      "config" => array("href" => "/ph/co2/app/config"),
      "agenda"            => array('href' => "/ph/co2/app/agenda",            "public" => true),
      "calendar"            => array('href' => "/ph/co2/app/calendar",            "public" => true),
      "admin"             => array('href' => "/ph/co2/app/admin",             "public" => true),
      "docs"             => array('href' => "/ph/co2/app/docs",             "public" => true),
      "info"              => array('href' => "/ph/co2/app/info",              "public" => true),
      "chat"              => array('href' => "/ph/co2/app/chat",              "public" => true),
      "interoperability" => array(
        "index"              => array('href' => 'ph/co2/interoperability/index',  "public" => true),
        "get"              => array('href' => 'ph/co2/interoperability/get',  "public" => true),
        "copedia"              => array('href' => 'ph/co2/interoperability/copedia',  "public" => true),
        "co-osm"              => array('href' => 'ph/co2/interoperability/co-osm',  "public" => true),
        "co-osm-getode"      => array('href' => 'ph/co2/interoperability/co-osm-getnode',  "public" => true),
        "wikitoco"              => array('href' => 'ph/co2/interoperability/wikitoco',  "public" => true),
        "pushtypewikidata"    => array('href' => 'ph/co2/interoperability/pushtypewikidata',  "public" => true),
        "wikidata-put-description"    => array('href' => 'ph/co2/interoperability/wikidata-put-description',  "public" => true),
      ),
      "interop"            => array('href' => "/ph/co2/app/interop",            "public" => true),
      "map"              => array('href' => "/ph/co2/app/map",         "public" => true),
      "badgecreator"              => array('href' => "/ph/co2/app/badgecreator",         "public" => true),
      "pingrefreshview"           => array('href' => "/ph/co2/app/pingrefreshview",      "public" => true),
      "badge"              => array('href' => "/ph/co2/app/badge",         "public" => true)
    ),
    "co" => array(
      "index"        => array('href' => "ph/costum/co/index")
    ),
    "cooperation" => array(
      "getcoopdata"        => array('href' => "ph/co2/cooperation/getcoopdata"),
      "savevote"           => array('href' => "ph/co2/cooperation/savevote"),
      "deleteamendement"   => array('href' => "ph/co2/cooperation/deleteamendement"),
      "getmydashboardcoop" => array('href' => "ph/co2/cooperation/getmydashboardcoop"),
      "previewcoopdata"    => array('href' => "ph/co2/cooperation/previewcoopdata"),
    ),
    "pdf" => array(
      "create"        => array('href' => "ph/co2/pdf/create")
    ),
    "connect" => array(
      "test"        => array("href" => "ph/sso/co/test", "module" => "connect"),
    ),
    "ressources" => array(
      "co"        => array("href" => "ph/ressources/co", "module" => "ressources"),
    ),
    "interop" => array(
      "co"        => array("href" => "ph/interop/co/index", "module" => "interop"),
    ),
    "badges" => array(
      "index"        => array("href" => "ph/badges/co/index", "module" => "badges"),
      "issuers"        => array("href" => "/ph/co2/badges/issuers", "public" => true),
      "profiles"        => array("href" => "/ph/co2/badges/profiles", "public" => true),
      "assertions"        => array("href" => "/ph/co2/badges/assertions", "public" => true),
      "assign"        => array("href" => "/ph/co2/badges/assign", "public" => false),
      "revoke"        => array("href" => "/ph/co2/badges/revoke", "public" => false),
      "cancel-revoke"        => array("href" => "/ph/co2/badges/revoke", "public" => false),
      "confirm"        => array("href" => "/ph/co2/badges/confirm", "public" => false),
      "badges"        => array("href" => "/ph/co2/badges/badges", "public" => true),
      "elementbadge"        => array("href" => "/ph/co2/badges/elementbadge", "public" => true),
      "views"        => array("href" => "/ph/co2/badges/views", "public" => true),
      "validator"        => array("href" => "/ph/co2/badges/validator", "public" => true),
      "finder"        => array("href" => "/ph/co2/badges/finder", "public" => true),
      "details"        => array("href" => "/ph/co2/badges/details", "public" => true),
      "settings"        => array("href" => "/ph/co2/badges/settings", "public" => true),
      "rebuild"        => array("href" => "/ph/co2/badges/rebuild", "public" => true),
      "badge-creator"        => array("href" => "/ph/co2/badges/badge-creator", "public" => true),
      "parcours"        => array("href" => "/ph/co2/badges/parcours", "public" => true),
      "parcoursedit"        => array("href" => "/ph/co2/badges/parcoursadder", "public" => false),
      "parcoursadder"        => array("href" => "/ph/co2/badges/parcoursadder", "public" => true),
      "parcoursdeleter"        => array("href" => "/ph/co2/badges/parcoursdeleter", "public" => true),
      "count-attente"        => array("href" => "/ph/co2/badges/count-attente", "public" => true),
      "get-criteria"        => array("href" => "/ph/co2/badges/get-criteria", "public" => true),
      "endorsement-modal"        => array("href" => "/ph/co2/badges/endorsement-modal", "public" => true),
    ),
    "chat" => array(
        "default"        => array (
          "msg" => array("href" => "ph/chat/default/msg", "module" => "chat"),
          "list" => array("href" => "ph/chat/default/list", "module" => "chat"),
        )
    ),
    "export" => array(
        "pdfelement"                => array("href" => "/ph/co2/export/pdfelement"),
        "csvelement"                => array("href" => "/ph/co2/export/csvelement"),
        "csv"                => array("href" => "/ph/co2/export/csv"),
    ),
     "template"=>array(
      "get"=> array('href' => "ph/co2/template/get"),
      "use"=> array('href' => "ph/co2/template/use")
    ),
    "translate" => array(
        "translate2lang"                => array("href" => "/ph/co2/translate/translate2lang")
    ),
  );

  public $costum;

  public $appConfig = array();

  function initPage(){
    //review the value of the userId to check loosing session
    //creates an issue with Json requests : to clear add josn:true on the page definition here
    //if( Yii::app()->request->isAjaxRequest && (!isset( $page["json"] )) )

    //bug du double load

    // echo "<script type='text/javascript'> alert('render CommunecterController initPage ".$this->module->id." | ".Yii::app()->controller->id." | ".Yii::app()->controller->action->id."'); </script>";
    //@$_POST["costumSlug"]." =? ".@Yii::app()->session[ "costum" ][ "slug" ]."'); </script>";


    // $this->renderPartial("co2.views.default.error");
    // exit;
    $this->costumCacheParams();

  //  var_dump("outside");exit;
    // discussion bouboule oceatoon
    //soit on passe toutes les request ajaxGET en POST pour géré costumSlug
    //soit on prévoit un magnifique refactor de tout les appels ajax , ajaxPost, getAjax, $.ajax...
    //pour le moment le switch entre costum et co2 risque de poser probleme
    // else if( $this->module->id == "co2")
    //   unset( Yii::app()->session[ "costum" ] );


    Yii::app()->params["version"] = Yii::app()->params["versionAssets"] ? Yii::app()->params["versionAssets"] : $this->version;
    if( @$_GET["theme"] ){
      ThemeHelper::setWebsiteTheme($_GET["theme"]);
      Yii::app()->session["theme"] = $_GET["theme"];
    }
    else if(@Yii::app()->session["theme"])
      ThemeHelper::setWebsiteTheme(Yii::app()->session["theme"]);

    /*if( in_array(Yii::app()->controller->id,$this->$modules) ){
      $this->redirect(Yii::app()->createUrl( "/".Yii::app()->controller->id."/".Yii::app()->controller->action->id ));
    }*/

    //managed public and private sections through a url manager
    //if( Yii::app()->controller->id == "admin" && !Yii::app()->session[ "userIsAdmin" ]
      //&& (!@Yii::app()->session[ "costum" ] || !@Yii::app()->session[ "costum" ]["admins"]
        //|| !@Yii::app()->session[ "costum" ]["admins"][Yii::app()->session["userId"]]) ||
          //(!isset(Yii::app()->session["costum"]["admin"])
            //&& isset(Yii::app()->session["costum"]["admin"]["accessMember"])
            //&& Authorisation::isElementMember(Yii::app()->session["costum"]["contextId"],Yii::app()->session["costum"]["contextType"], Yii::app()->session["userId"])))
      //throw new CHttpException(403,Yii::t('error','Unauthorized Access.'));

    //if( Yii::app()->controller->id == "adminpublic" && ( !Yii::app()->session[ "userIsAdmin" ] && !Yii::app()->session[ "userIsAdminPublic" ] ) )
     // throw new CHttpException(403,Yii::t('error','Unauthorized Access.'));

    if( in_array( $this->module->id, array("chat", "ressources", "classifieds", "interop" ) ) )

      $page = $this->pages[$this->module->id][Yii::app()->controller->id][Yii::app()->controller->action->id];
    else if( Yii::app()->controller->id != "test" && isset($this->pages[Yii::app()->controller->id][Yii::app()->controller->action->id]))
      $page = $this->pages[Yii::app()->controller->id][Yii::app()->controller->action->id];
    $pagesWithoutLogin = array(
                            //Login Page
                            "person/login",
                            "person/register",
                            "person/authenticate",
                            "person/activate",
                            "person/sendemail",
                            "person/checkusername",
                            //Document Resizer
                            "document/resized");

    $prepareData = true;

    $headers = getallheaders();
    if( isset($headers["X-Auth-Token"]) && isset($headers["X-User-Id"]) && isset($headers["X-Auth-Name"]) ){
      $prepareData = false;
		}
    else if( (!isset( $page["public"] ) ) && (!isset( $page["json"] ))
      && !in_array(Yii::app()->controller->id."/".Yii::app()->controller->action->id, $pagesWithoutLogin)
      && !Yii::app()->session[ "userId" ] )
    {
        Yii::app()->session["requestedUrl"] = Yii::app()->request->url;
        //if( Yii::app()->request->isAjaxRequest)
          //echo "<script type='text/javascript'> checkIsLoggued('".Yii::app()->session['userId']."'); </script>";

    }

    //login auto from cookie if user not connected and checked remember
    // if(!isset(Yii::app()->session["userId"]) &&
    //     isset( Yii::app()->request->cookies['remember'] ) &&
    //     Yii::app()->request->cookies['remember']->value == "true" &&
    //     isset( Yii::app()->request->cookies['lyame'] ) &&
    //     isset( Yii::app()->request->cookies['drowsp'] ) &&
    //     @Yii::app()->request->cookies['drowsp']->value != "null"){
    //         $pwdDecrypt = $this->pwdDecrypt(Yii::app()->request->cookies['drowsp']->value);
    //         $emailDecrypt = $this->pwdDecrypt(Yii::app()->request->cookies['lyame']->value);
    //         $res = Person::login($emailDecrypt, $pwdDecrypt, false);
    // }
    self::connectCookie();

    if( isset( $_GET["backUrl"] ) )
      Yii::app()->session["requestedUrl"] = $_GET["backUrl"];
    /*if( !isset(Yii::app()->session['logguedIntoApp']) || Yii::app()->session['logguedIntoApp'] != $this->module->id)
      $this->redirect(Yii::app()->createUrl("/".$this->module->id."/person/logout"));*/
    if( $prepareData )
    {
      //$this->sidebar1 = array_merge( Menu::menuItems(), $this->sidebar1 );
      //$this->person = Person::getPersonMap(Yii::app() ->session["userId"]);
      $this->title = (isset($page["title"])) ? $page["title"] : $this->title;
      $this->subTitle = (isset($page["subTitle"])) ? $page["subTitle"] : $this->subTitle;
      $this->pageTitle = (isset($page["pageTitle"])) ? $page["pageTitle"] : $this->pageTitle;
      //if(!empty(Yii::app()->session["userId"]))
      //  $this->notifications = ActivityStream::getNotifications( array( "notify.id" => Yii::app()->session["userId"] ) );

      // if( $_SERVER['SERVER_NAME'] == "127.0.0.1" || $_SERVER['SERVER_NAME'] == "localhost" )
      //   CornerDev::addWorkLog("communecter",Yii::app()->session["userId"],Yii::app()->controller->id,Yii::app()->controller->action->id);
    }

    //load any DB config Params
    Application::loadDBAppConfig();
  }

  function meta($c) {
    /* var_dump($c["host"]);
    var_dump($this->module->relCanonical);
    var_dump($c["url"]);
    var_dump(Yii::app()->getRequest()->getBaseUrl(true));
    var_dump(str_replace("wwww.", "", $c["host"])); */

    if(isset($c["host"])){
                        $this->module->relCanonical = "https://".$c["host"];
                } else if (isset($this->module->relCanonical))
                    $this->module->relCanonical = Yii::app()->createUrl($c["url"]);

                if(isset($c["metaDesc"]))
                    $shortDesc=$c["metaDesc"];
                else{
                    $shortDesc =  @$c["shortDescription"] ? $c["shortDescription"] : "";
                        if($shortDesc=="")
                            $shortDesc = @$c["description"] ? $c["description"] : "";
                }
                if(isset($c["language"])){
                    Yii::app()->language=$c["language"];
                }

                    $this->module->description = $shortDesc;
                    $this->module->pageTitle = (isset($c["metaTitle"])) ? $c["metaTitle"] : @$c["title"];
                    $this->module->author = (isset($c["metaTitle"])) ? $c["metaTitle"] : @$c["title"];
                    if(isset($c["metaKeywords"]))
                        $this->module->keywords = $c["metaKeywords"];
                    else
                       $this->module->keywords = (@$c["tags"]) ? implode(",", @$c["tags"]) : "";
                    if (isset($c["favicon"])) {
                        $mod = $this->module->id;
                        //ex images can be given
                        if( substr_count($c["favicon"], '#') ){
                            $pieces = explode("#", $c["favicon"]);
                            $mod = $pieces[0];
                            $c["favicon"] = $pieces[1];
                        }

                        if(!empty($c["favicon"]))
                          $this->module->favicon = Yii::app()->getModule( $mod )->getAssetsUrl().$c["favicon"];
                        else{
                            $whereGetFavicon = array(
                              "id"=> $this->costum["contextId"],
                              "type"=>$this->costum["contextType"],
                              "subKey"=> "favicon",
                            );

                          if(isset($c["faviconSourceImage"]) && !empty($c["faviconSourceImage"])){
                            $imagePathExplode = explode("/", $c["faviconSourceImage"]);
                            $nameImage = $imagePathExplode[count($imagePathExplode) - 1];
                            $whereGetFavicon["name"] = $nameImage;
                            unset($whereGetFavicon["subKey"]);
                          }         
                          
                          $initFavicon = Document::getListDocumentsWhere(
                            $whereGetFavicon,"image"
                          );
                          $this->module->favicon = !empty($initFavicon) ? $initFavicon[0]["imageThumbPath"] : "";
                          
                          if(!file_exists($this->module->favicon))
                            $this->module->favicon = !empty($initFavicon) ? $initFavicon[0]["imagePath"] : "";
                        }
                        if(!empty($this->costum["type"]) && $this->costum["type"] == "aap" && (!isset($this->module->favicon) || (isset($this->module->favicon) && empty($this->module->favicon) )))
                          $this->module->favicon = Yii::app()->getModule('co2')->assetsUrl."/images/oceco1.png";

                    }
                    if (@$c["metaImg"]) {
                        $mod = $this->module->id;
                        //ex images can be given
                        if( substr_count($c["metaImg"], '#') ){
                            $pieces = explode("#", $c["metaImg"]);
                            $mod = $pieces[0];
                            $c["metaImg"] = $pieces[1];
                        }
                        if(!empty($c["metaImg"]))
                          $this->module->image = Yii::app()->getModule( $mod )->getAssetsUrl().$c["metaImg"];
                        else{
                              $initMetaImg = Document::getListDocumentsWhere(
                              array(
                                "id"=> $this->costum["contextId"],
                                "type"=>$this->costum["contextType"],
                                "subKey"=> "favicon",
                              ),"image"
                            );
                            $this->module->image = !empty($initMetaImg) ? $initMetaImg[0]["imageThumbPath"] : "";
                        }
                    }
                    else if(@$c["logo"]){
                        $mod = $this->module->id;
                        //ex images can be given
                        if( substr_count($c["logo"], '#') ){
                            $pieces = explode("#", $c["logo"]);
                            $mod = $pieces[0];
                            $c["logo"] = $pieces[1];
                        }
                        $this->module->image = $c["logo"];
                    }
                    if(@$c["logo"]){
                        $this->module->image = $c["logo"];
                    }
                    //var_dump($this->module->relCanonical);
                    //exit;
  }

function cacheCostumInit($slug, $editMode, $returnCostum=null, $forcedCache=false){
    if(CacheHelper::get($slug) && !$forcedCache){
        $cacheCostum = CacheHelper::get($slug);
        if(!empty($cacheCostum["resetCache"]))
            return false;
        $cacheCostum["editMode"]=$editMode;
        if(isset(Yii::app()->session["userId"])){
            $el = [
                "id"   => $cacheCostum["contextId"],
                "type" => $cacheCostum["contextType"]
            ];

            $costumUser = Costum::getCheckCustomUser($cacheCostum,$el);
            $this->costum = array_merge($cacheCostum, $costumUser);

        }else{
                $this->costum = $cacheCostum;
        }

        if(isset($cacheCostum["appConfig"]) && !empty($cacheCostum["appConfig"]) && count($cacheCostum["appConfig"])>0){
            $this->appConfig = $cacheCostum["appConfig"];
        }
        return true;
    } else {

        if(!empty($returnCostum)){
            CacheHelper::set($slug, $returnCostum);
            if(isset(Yii::app()->session["userId"])){
                $el = array();
                $el["id"] = $returnCostum["contextId"];
                $el["type"] = $returnCostum["contextType"];
                $costumUser = Costum::getCheckCustomUser($returnCostum,$el);
                $this->costum = array_merge($returnCostum, $costumUser);
            }else{
                $this->costum = $returnCostum;
            }
            if(isset($returnCostum["appConfig"]) && !empty($returnCostum["appConfig"]) && count($returnCostum["appConfig"])>0){
                $this->appConfig = $returnCostum["appConfig"];
            }
        }
        return false;
    }
  }
  protected function costumCacheParams(){
    if(empty(CacheHelper::get("appConfig"))){
        CacheHelper::set("appConfig", CO2::getThemeParams());
    }
    $this->appConfig = CacheHelper::get("appConfig");
    if( isset($_POST["costumSlug"]) ){
        if(!$this->cacheCostumInit($_POST["costumSlug"], @$_POST["costumEditMode"])){
          if( isset($_POST["costumId"]) && isset($_POST["costumType"]) )
          {
              //cas d'un costum utilisé comme un template par plusieru element
              $el = Element::getElementById($_POST["costumId"], $_POST["costumType"]);
              //regle un prob de surchagement des contextId donc on passe par le slug
              if(isset($el))
                $returnCostum = Costum::init( null, null, null,$el["slug"],null,null,null,null,"co2/components/CommunecterController.php 0.1" );
              else
                $returnCostum = Costum::init( null, $_POST["costumId"], $_POST["costumType"],null,null,null,null,"co2/components/CommunecterController.php 1" );
          }
          else
          {
            //pas d'element fourni juste un slug de costum
            //WARNING ici je ne peux pas avoir session["costum"]["contextId"]) session["costum"]["contextType"]
            // je colle le code en dessous au cas ou en commentaire
            /* if( isset(Yii::app()->session["costum"]["contextId"]) && isset(Yii::app()->session["costum"]["contextType"] ) )
            {
              $el = Element::getElementById(Yii::app()->session["costum"]["contextId"],Yii::app()->session["costum"]["contextType"]);
              //regle un prob de surchagement des contextId donc on passe par le slug
              if(isset($el))
                Costum::init( null, null, null,$el["slug"],null,null,null,null,"co2/components/CommunecterController.php 0.2" );

            } else */
              $returnCostum = Costum::init( null, null,null,$_POST["costumSlug"],null,null,null,"co2/components/CommunecterController.php 2" );

          }

          $this->cacheCostumInit($_POST["costumSlug"], $_POST["costumEditMode"]??false, $returnCostum);
        }
    } else if($this->module->id!="costum"){
       $this->appConfig = CO2::getThemeParams();
        $layoutPathCO3 ="../../modules/co2/config/co3.json";
        $strSettingsCO3 = file_get_contents($layoutPathCO3);
        $settingsCO3 = json_decode($strSettingsCO3, true);
        $this->appConfig=Costum::filterThemeInCustom($settingsCO3,$this->appConfig);
        $params=$this->appConfig;
    }
  }
  public function beforeAction($action){
    Yii::$app->response->headers->set('Access-Control-Allow-Origin', '*');
		Yii::$app->response->headers->set('Access-Control-Allow-Methods', 'GET, POST, OPTIONS');
		Yii::$app->response->headers->set('Access-Control-Allow-Headers', 'Content-Type, Authorization, X-Requested-With');

		// si la requête est de type OPTIONS, on retourne une réponse 200
		if($_SERVER[ 'REQUEST_METHOD' ] === 'OPTIONS'){
			Yii::$app->response->setStatusCode(200);
			return Yii::$app->response->send();
		}

    if( $_SERVER['SERVER_NAME'] == "127.0.0.1" || $_SERVER['SERVER_NAME'] == "localhost" ){
      Yii::app()->assetManager->forceCopy = getenv("YII_prod") ? false : true;
      //if(Yii::app()->controller->id."/".Yii::app()->controller->action->id != "log/dbaccess")
        //Yii::app()->session["dbAccess"] = 0;
    }

    $headers = getallheaders();
    if( isset($headers["X-Auth-Token"]) && isset($headers["X-User-Id"]) && isset($headers["X-Auth-Name"]) && Authorisation::isMeteorConnected( $headers["X-Auth-Token"], $headers["X-User-Id"], $headers["X-Auth-Name"] ) ){

		} else if (isset($headers['Authorization']) && preg_match('/Bearer\s(\S+)/', $headers['Authorization'], $matches)) {
			Authorisation::isJwtconnected($matches[1]);
		}

    $this->costumCacheParams();

    //var_dump(@$_POST["costumSlug"]); exit;
    $this->manageLog();

    return parent::beforeAction($action);
  }


  /**
   * Start the log process
   * Bring back log parameters, then set object before action and save it if there is no return
   * If there is return, the method save in session the log object which will be finished and save in db during the method afteraction
   */
  protected function manageLog(){
    //Bring back logs needed
    $actionsToLog = Log::getActionsToLog();
    $actionInProcess = Yii::app()->controller->id.'/'.Yii::app()->controller->action->id;

    //Start logs if necessary
    if(isset($actionsToLog[$actionInProcess])) {

      //To let several actions log in the same time
      if(!$actionsToLog[$actionInProcess]['waitForResult']){
        Log::save(Log::setLogBeforeAction($actionInProcess));
      }else if(isset(Yii::app()->session["logsInProcess"]) && is_array(Yii::app()->session["logsInProcess"])){
        Yii::app()->session["logsInProcess"] = array_merge(
          Yii::app()->session["logsInProcess"],
          array($actionInProcess => Log::setLogBeforeAction($actionInProcess))
        );
      } else{
         Yii::app()->session["logsInProcess"] = array($actionInProcess => Log::setLogBeforeAction($actionInProcess));
      }
    }
  }

  protected function pwdDecrypt($jsonString){  //return $jsonString;
    $passphrase = 'JbQmfH"h^W7q86JU1V(<64aEv';
      $jsondata = json_decode($jsonString, true);
      try {
          $salt = hex2bin($jsondata["s"]);
          $iv  = hex2bin($jsondata["iv"]);
      } catch(Exception $e) { return null; }
      $ct = base64_decode($jsondata["ct"]);
      $concatedPassphrase = $passphrase.$salt;
      $md5 = array();
      $md5[0] = md5($concatedPassphrase, true);
      $result = $md5[0];
      for ($i = 1; $i < 3; $i++) {
          $md5[$i] = md5($md5[$i - 1].$concatedPassphrase, true);
          $result .= $md5[$i];
      }
      $key = substr($result, 0, 32);

      //var_dump($iv); exit;

      $data = openssl_decrypt($ct, 'aes-256-cbc', $key, true, $iv);
      return json_decode($data, true);
  }


  protected function connectCookie(){
    if(!isset(Yii::app()->session["userId"]) &&
          isset( Yii::app()->request->cookies['remember'] ) &&
          Yii::app()->request->cookies['remember']->value == "true" &&
          isset( Yii::app()->request->cookies['lyame'] ) &&
          isset( Yii::app()->request->cookies['drowsp'] ) &&
          @Yii::app()->request->cookies['drowsp']->value != "null"){
              $pwdDecrypt = $this->pwdDecrypt(Yii::app()->request->cookies['drowsp']->value);
              $emailDecrypt = $this->pwdDecrypt(Yii::app()->request->cookies['lyame']->value);
              $res = Person::login($emailDecrypt, $pwdDecrypt, false);
      }
  }

}

