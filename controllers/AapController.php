<?php

namespace PixelHumain\PixelHumain\modules\co2\controllers;

use CommunecterController;
use PixelHumain\PixelHumain\modules\survey\controllers\actions\exportationCsv\AapGeneriqueExportCSVAction;

class AapController extends CommunecterController
{

	public function beforeAction($action)
	{
		return parent::beforeAction($action);
	}

	public function actions()
	{
		return array(
			'commonaap'						=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\aap\AapAction::class,
			'agenda'						=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\aap\AgendaAction::class,
			'generateconfig'				=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\aap\GenerateConfigAction::class,
			'getviewbypath'					=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\aap\GetViewByPath::class,
			'deleteaap'						=> 'costum.controllers.actions.aap.DeleteAapAction',
			'publicrate'					=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\aap\PublicRateAction::class,
			'searchtags'					=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\aap\SearchTagsAction::class,
			'searchroles'					=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\aap\SearchRolesAction::class,
			'searchanswer'					=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\aap\SearchAnswerAction::class,
			'pushtags'						=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\aap\PushTagsAction::class,
			'getelementbyid'				=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\aap\getElementById::class,
			'directoryproposal'				=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\aap\DirectoryProposalAction::class,
			'directoryelement'          	=>  \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\aap\DirectoryElementAction::class,
			'directoryproject'				=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\aap\DirectoryProjectAction::class,
			'directoryorganismchooser'		=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\aap\DirectoryOrganismChooserAction::class,
			"cosindni-export-csv"			=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\aap\CosindniExportCSVAction::class,
			"aap-export-csv"				=> AapGeneriqueExportCSVAction::class,
			"getfinancor"					=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\aap\GetFinancorsByFormIdAction::class,
			"exportpdf"						=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\aap\ExportpdfAction::class,
			"exportcsvfinancer"				=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\aap\ExportcsvfinancerAction::class,
			"sendmail"						=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\aap\MailAction::class,
			"attachedfile"					=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\aap\AttachedFile::class,
			"addnotified"					=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\aap\NotificationStatusAction::class,
			"getactionsbyanswer"			=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\aap\GetActionsByAnswer::class,
			"actiondetail"					=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\aap\ActionDetail::class,
			"getprojectsbyanswers"			=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\aap\GetProjectsByAnswers::class,
			"reloadpanel"					=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\aap\ReloadPanelAction::class,
			"generateformoceco"				=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\aap\GenerateFormOcecoAction::class,
			"duplicateproposition" 			=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\aap\DuplicatePropositionAction::class,
			"cacs"							=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\aap\CacsAction::class,
			'copyfieldsfromfield'			=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\aap\CopyAnswerFromOneFieldAction::class,
			'proposition'					=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\aap\PropositionAction::class,
			'project'						=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\aap\ProjectAction::class,
			'proposalbycostumslug'			=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\aap\ProposalByCostumSlugAction::class,
			'sousorga'						=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\aap\SousOrgaAction::class,
			'exceltable'					=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\aap\ExcelTableAction::class,
			'refreshanswer'					=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\aap\RefreshAnswerAction::class,
			'getsuborga'					=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\aap\ExcelTableAction::class,
			'changeaapsession'				=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\aap\ChangeSessionAction::class,
			'observatory'					=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\aap\ObservatoryAction::class,
			'userdashboard'					=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\aap\UserDashboardAction::class,
            'setftlinput'					=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\aap\setFtlInputAction::class,
            'funding'						=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\aap\FundingAction::class,
			'campagne-migration'			=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\aap\CampagneMigrationAction::class,
            'camp-tls-list-html'			=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\aap\MyTlsHtmlAction::class,
			'camp-tl-fund-html'				=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\aap\TlFundHtmlAction::class,
			'camp-cart-bill-info-html'		=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\aap\CartBillInfoHtmlAction::class,
			'camp-fund-validate'			=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\aap\CampFundValidateAction::class,
			'camp-tl-bill'					=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\aap\CampTlBillAction::class,
			'camp-tl-payment-method'		=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\aap\CampPaymentMethodAction::class,
			"camp-tl-check-status" => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\aap\CheckStatusAction::class,
			"depense-images"				=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\aap\DepenseImageAction::class,
            'camp-candidat-paielist-html'	=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\aap\CandidatpaieAction::class,
			"aac" 							=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\aap\AacAction::class,
            'config_payment'                => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\aap\ConfigPaymentAction::class
        );
	}
}
