<?php
    
    namespace PixelHumain\PixelHumain\modules\co2\controllers;
    
    use Authorisation;
    use Cms;
    use CommunecterController;
    use Form;
    use Organization;
    use PHDB;
    use PixelHumain\PixelHumain\components\View;
    use PixelHumain\PixelHumain\modules\citizenToolKit\components\UtilsHelper;
    use Project;
    use Yii;
    use CacheHelper;
    use Costum;
    use Rest;
    use MongoId;
    use PixelHumain\PixelHumain\modules\citizenToolKit\models\Api;
    use stdClass;
    use MongoDate;
    use PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\UseTemplateAction;
    use CClientScript;
    use DateTime;
    
    class AapMigrationController extends CommunecterController {
        
        public function beforeAction($action) {
            return parent::beforeAction($action);
        }
        
        /**
         * http://communecter74-dev/co2/aapMigration/NewRefactorConfig/slug
         */
        public function actionNewRefactorConfig($slug = null) {
            if (!Authorisation::isUserSuperAdmin(Yii::app()->session["userId"])) {
                echo "<h1 style='color:red;text-align:center'>SUPER ADMIN SEULEMENT !!</h1>";
                return;
            }
            $this->formStartDateAndDate();
            $this->specifics();
            $this->newMenuApp($slug);
            $this->cosindnismarterre();
            $this->editStatusUpdated();
            $this->set_csv_exprotation_path();
            $this->setViewedProposalFromViewsCollection();
            $this->setPresentationPageTemplate();
            $this->setRequiredInput();
            $this->setProjectStatus();
            $this->updateDateFormat();
            
            //Anatole Rija Schuman, ampio eto ny fonction ilaina amin'ny migration
        }
        
        private function newMenuApp($slug = null) {
            $config = file_get_contents("../../modules/co2/data/aap.json", FILE_USE_INCLUDE_PATH);
            $config = json_decode($config, true);
            $menuXsLeftPath = "costum.htmlConstruct.header.menuTop.left.buttonList.xsMenu.buttonList.app.buttonList";
            $menuAppLeftPath = "costum.htmlConstruct.header.menuTop.left.buttonList.app.buttonList";
            $menuAppRightPath = "costum.htmlConstruct.header.menuTop.right.buttonList.app.buttonList";
            $where = array("costum.type" => "aap");
            if (!empty($slug)) {
                $where["slug"] = $slug;
            }

            $elts = PHDB::find(
                Organization::COLLECTION,
                $where,
                array("slug","costum","collection")
            );
            $eltsProjects = PHDB::find(
                Project::COLLECTION,
                $where,
                array("slug","costum","collection")
            );
            $elts = array_merge($eltsProjects,$elts);
            //***********buttonList query inside loop for order***************/
            foreach ($elts as $key => $value) {
                $useCostum = empty(Api::getArrayValueByPath($value,"costum.app")) && empty(Api::getArrayValueByPath($value,"costum.htmlConstruct"));
                $collection = $value["collection"];
                $prepath = "costum.";
                $cmenuAppLeftPath = $menuAppLeftPath;
                $cmenuXsLeftPath = $menuXsLeftPath;
                $cmenuAppRightPath = $menuAppRightPath;

                if(!$useCostum && empty(Api::getArrayValueByPath($value,$menuXsLeftPath))){
                    PHDB::update($collection,array("slug"=> $value["slug"]),array(
                        '$set' => array(
                            $menuXsLeftPath => array("#welcome" => true),
                        )
                    ));
                }

                $initialUnsets = array();
                $sets = array();

                if($useCostum){
                    $collection = Costum::COLLECTION;
                    
                    $cmenuAppLeftPath = str_replace($prepath,"",$menuAppLeftPath);
                    $cmenuXsLeftPath = str_replace($prepath,"",$menuXsLeftPath);
                    $cmenuAppRightPath = str_replace($prepath,"",$menuAppRightPath);
                    $prepath = "";
                }

                $unsets = array(
                    $prepath."app.#oceco" => true,
                    $cmenuAppLeftPath.".#oceco" => true,
                    $cmenuXsLeftPath.".#oceco" => true
                );

                foreach ($config["app"] as $k => $v) {
                    $initialUnsets[$cmenuXsLeftPath."." . $k] = true;
                    $initialUnsets[$cmenuAppLeftPath."." . $k] = true;
                }

                foreach ($config["menuTopRight"] as $k => $v) {
                    $initialUnsets[$cmenuAppRightPath."." . $k] = true;
                }
                PHDB::update(
                    $collection,
                    array("slug"=> $value["slug"]),
                    array('$unset' => $initialUnsets),
                );
 

                foreach ($config["menuTopLeft"] as $k => $v) {
                    $setMenuTopLeft = array(
                        $cmenuXsLeftPath."." . $k => $v,
                        $cmenuAppLeftPath."." . $k => $v,
                        $prepath."css.menuTop.left.app.buttonList.marginLeft" => "10px",
                        $prepath."css.menuTop.left.app.buttonList.lineHeight" => "3",
                        $prepath."htmlConstruct.header.menuTop.left.buttonList.app.dropdownFullWidth" => true,
                    );
                    switch ($value["slug"]) {
                        case 'laRaffinerie3':
                            $setMenuTopLeft[$prepath."css.menuTop.left.app.buttonList.lineHeight"] = 2;
                            break;
                        default:
                            # code...
                            break;
                    }
                    PHDB::update($collection,array("slug"=> $value["slug"]),array(
                        '$set' => $setMenuTopLeft
                    ));
                }
                
                //menuRight{
                    $existedMenuRightCss = PHDB::findOne($collection,array("slug" => $value["slug"],$prepath."css.menuTop.right" => ['$exists'=>true]),array($prepath."css.menuTop.right"));
                    $existedMenuRightCss = Api::getArrayValueByPath($existedMenuRightCss,$prepath."css.menuTop.right");
                    foreach ($existedMenuRightCss as $kmr => $vmr) {
                        if(Api::isAssociativeArray($vmr) && ($kmr != "userProfil" && $kmr != "dropdown")){
                            PHDB::update(
                                $collection,
                                array("slug"=> $value["slug"]),
                                array('$set' => array(
                                    $prepath."css.menuTop.right.".$kmr.".fontSize" => "23",
                                    $prepath."css.menuTop.right.".$kmr.".paddingTop" => "14"
                                )),
                            );
                        }
                    }

                    $existedMenuRight = PHDB::findOne($collection,array("slug" => $value["slug"],$prepath."htmlConstruct.header.menuTop.right.buttonList" => ['$exists'=>true]),array($prepath."htmlConstruct.header.menuTop.right.buttonList"));
                    $existedMenuRight = Api::getArrayValueByPath($existedMenuRight,$prepath."htmlConstruct.header.menuTop.right.buttonList");
                    foreach ($existedMenuRight as $kmr => $vmr) {
                        if(Api::isAssociativeArray($vmr) && ($kmr != "userProfil" && $kmr != "dropdown")){
                            PHDB::update(
                                $collection,
                                array("slug"=> $value["slug"]),
                                array('$set' => array(
                                    $prepath."css.menuTop.right.".$kmr.".fontSize" => "23",
                                    $prepath."css.menuTop.right.".$kmr.".paddingTop" => "14"
                                )),
                            );
                        }
                    }

                    PHDB::update($collection,array("slug"=> $value["slug"]),array(
                        '$unset' => array(
                            $prepath."htmlConstruct.header.menuTop.right.buttonList.userProfil" => true,
                            $prepath."htmlConstruct.header.menuTop.right.buttonList.dropdown" => true,
                            $prepath."htmlConstruct.element.menuTop.right.buttonList.classifieds" => true,
                            $prepath."htmlConstruct.element.menuTop.right.buttonList.events" => true,
                            $prepath."htmlConstruct.element.menuTop.right.buttonList.form" => true,
                            $prepath."htmlConstruct.element.menuTop.right.buttonList.xsMenu.buttonList.classifieds" => true,
                            $prepath."htmlConstruct.element.menuTop.right.buttonList.xsMenu.buttonList.events" => true,
                            $prepath."htmlConstruct.element.menuTop.right.buttonList.xsMenu.buttonList.form" => true,
                        )
                    ));

                    PHDB::update($collection,array("slug"=> $value["slug"]),array(
                        '$set' => array(
                            $prepath."htmlConstruct.header.menuTop.left.buttonList.app.class" => "hidden-xs",
                            $prepath."css.menuTop.right.chat.fontSize" => "23",
                            $prepath."css.menuTop.right.chat.color" => "#292929",
                            $prepath."css.menuTop.right.chat.paddingTop" => "14",
                            $prepath."css.menuTop.right.notifications.fontSize" => "23",
                            $prepath."css.menuTop.right.notifications.color" => "#292929",
                            $prepath."css.menuTop.right.notifications.paddingTop" => "14",
                            $prepath."css.menuTop.right.app.buttonList.fontSize" => "23",
                            $prepath."css.menuTop.right.app.buttonList.color" => "#292929",
                            $prepath."css.menuTop.right.app.buttonList.paddingTop" => "0",
                            $prepath."css.menuTop.right.app.buttonList.paddingRight" => "7",
                            $prepath."css.menuTop.right.app.paddingTop" => "14",
                            $prepath."css.menuTop.right.userProfil.marginTop" => "12",
                            $prepath."css.menuTop.right.userProfil.color" => "#292929",
                            $prepath."css.menuTop.right.userProfil.background" => "#ffffff",
                            $prepath."css.menuTop.right.dropdown.marginTop" => "11",
                            $prepath."css.menuTop.right.dropdown.icon.fontSize" => "25",
                            $prepath."css.menuTop.right.dropdown.icon.color" => "#292929",
                            $prepath."css.menuTop.right.login.paddingTop" => "10",
                            $prepath."css.menuTop.right.login.fontSize" => "18",
                        )
                    ));

                    PHDB::update($collection,array("slug"=> $value["slug"]),array(
                        '$unset' => array(
                            $prepath."css.menuTop.right.login.fontSize" => true,
                            $prepath."css.menuTop.right.login.background" => true,
                            $prepath."css.menuTop.right.login.color" => true,
                        )
                    ));
                
                    PHDB::update($collection,array("slug"=> $value["slug"]),array(
                        '$set' => array(
                            $prepath."htmlConstruct.header.menuTop.right.buttonList.chat" => true,
                        )
                    ));

                    PHDB::update($collection,array("slug"=> $value["slug"]),array(
                        '$set' => array(
                            $prepath."htmlConstruct.header.menuTop.right.buttonList.notifications" => true,
                        )
                    ));
                    foreach ($config["menuTopRight"] as $k => $v) {
                        PHDB::update($collection,array("slug"=> $value["slug"]),array(
                            '$set' => array(
                                $cmenuAppRightPath."." . $k => $v,
                            )
                        ));
                    }
                    PHDB::update($collection,array("slug"=> $value["slug"]),array(
                        '$set' => array(
                            $prepath."htmlConstruct.header.menuTop.right.buttonList.userProfil" => [
                                "name" => true,
                                "img" => true
                            ],
                            $prepath."htmlConstruct.header.menuTop.right.buttonList.register" => true,
                            $prepath."htmlConstruct.header.menuTop.right.buttonList.login" => true,
                        )
                    ));
                    PHDB::update($collection,array("slug"=> $value["slug"]),array(
                        '$set' => array(
                            $prepath."htmlConstruct.header.menuTop.right.buttonList.dropdown" => [
                                "buttonList" => [
                                    "admin" => true,
                                    "logout" => true,
                                ]
                            ],
                        ),
                        '$unset' => array(
                            $prepath."adminPanel.menu.community" => true
                        )
                    ));
                //}

                foreach ($config["app"] as $k => $v) {
                    $sets[$prepath."app." . $k] = $v;
                }


                PHDB::update(
                    $collection,
                    array("slug"=> $value["slug"]),
                    array('$set' => $sets, '$unset' => $unsets),
                );
            }
            
            // ******************* end set new config value ****************

            foreach ($elts as $k => $v) {
                CacheHelper::delete($v['slug']);
            }
            
            if (CacheHelper::get($_SERVER['SERVER_NAME']))
                CacheHelper::delete($_SERVER['SERVER_NAME']);
            
            echo "<h2 style='color:green;'> OK -" . count($elts) . " costum.app mis à jour, costum.menuTop mis à jour</h2>";
        }
        
        private function cosindnismarterre() {
            /******************************************************** Add template Email *******************************************/
            $template = array(
                "avis_favorable_QPV"      => [
                    "name"   => "Avis favorable QPV",
                    "object" => "Notification d’attribution de subvention dans le cadre de l’Appel à Projet Contrat de Ville    2022",
                    "html"   => "costum.views.custom.coSindniSmarterre.templateEmail.avis_favorable_QPV"
                ],
                "avis_favorable_hors_QPV" => [
                    "name"   => "Avis favorable hors QPV",
                    "object" => "Notification d’attribution de subvention dans le cadre de l’Appel à Projet Contrat de Ville    2022",
                    "html"   => "costum.views.custom.coSindniSmarterre.templateEmail.avis_favorable_hors_QPV"
                ],
                "duplicate_action"        => [
                    "name"   => "Demande de duplication",
                    "object" => "Demande de duplication du dossier",
                    "html"   => "costum.views.custom.coSindniSmarterre.templateEmail.duplicate_action"
                ],
            );
            PHDB::update(
                Form::COLLECTION,
                array("_id" => new MongoId("620a5e534f94bf26f34c5be8")),
                array('$set' => [
                    "params.templateEmails" => $template,
                    "params.pdf_exporter" => "costum.views.custom.coSindniSmarterre.aap.exportation-pdf-cosindnismarterre"
                    ])
            );

            PHDB::update(
                Form::INPUTS_COLLECTION,
                array("_id" => new MongoId("620a5e534f94bf26f34c5be9")),
                array('$set' => [
                    "inputs.interventionArea.type" => "tpls.forms.cplx.qpvChooser",
                    ])
            );
            
            PHDB::update(
                Organization::COLLECTION,
                array("slug" => ['$in' => ["coSinDni", "coSindniSmarterre"]]),
                array('$set' => ["oceco.haveSpecificFilters" => true])
            );

            PHDB::remove(Form::COLLECTION,array(
                "_id" => [
                    '$in' => [
                        new MongoId("63c64789a03350321c5e0c81"),
                        new MongoId("62022533d36cba553006f84d"),
                        new MongoId("62fcff6935253661234ad7cb"),
                        new MongoId("62fcf8a435253661234ad74a")
                    ]
                ]
            ));
            
            PHDB::remove(Form::ANSWER_COLLECTION,array(
                "form" => "62022533d36cba553006f84d"
            ));

            PHDB::update(Organization::COLLECTION,array("slug"=>"coSindniSmarterre"),array(    
                '$set' => array(
                    "costum.htmlConstruct.header.menuTop.left.buttonList.xsMenu.buttonList.app.buttonList.#organismAap.buttonList.#observatoryAap" => true,
                    "costum.htmlConstruct.header.menuTop.left.buttonList.app.buttonList.#organismAap.buttonList.#observatoryAap" => true
                ),
                '$unset' => array(
                    "costum.htmlConstruct.header.menuTop.left.buttonList.xsMenu.buttonList.app.buttonList.#organismAap.buttonList.#dashboardAap" => true,
                    "costum.htmlConstruct.header.menuTop.left.buttonList.app.buttonList.#organismAap.buttonList.#dashboardAap" => true
                ),
            ));

            echo "<h2 style='color:green;'>OK - templateEmails àjouté dans cosindismarterre</h2>";
            /******************************************************** End Add template Email *******************************************/
        }
        
        private function editStatusUpdated() {
            $form = PHDB::find(Form::COLLECTION, ["type" => "aap"]);
            $answers = PHDB::find(
                Form::ANSWER_COLLECTION,
                array("form" => ['$in' => array_keys($form)]),
                ['status', 'statusUpdated']
            );
            $count = 0;
            foreach ($answers as $ansKey => $ansValue) {
                $set = [];
                if (isset($ansValue['status'])) {
                    $currentSatusUpdated = !empty($ansValue['statusUpdated']) ? $ansValue['statusUpdated'] : time();
                    if (empty($ansValue['status'])) {
                        $set['statusInfo'] = new stdClass();
                    } else {
                        foreach ($ansValue['status'] as $statusKey => $statusValue) {
                            if (!Api::isAssociativeArray($ansValue['status']) && ($statusValue != "" || $statusValue != null)) {
                                $set['statusInfo.' . $statusValue] = [
                                    'user'    => '',
                                    'updated' => $currentSatusUpdated
                                ];
                            } else {
                                $set['statusInfo'] = new stdClass();
                            }
                        }
                    }
                    // echo '<pre>';
                    // echo json_encode($set);
                    // echo '</pre>';
                    // exit();
                } else {
                    $set['statusInfo'] = new stdClass();
                }
                if (PHDB::update(
                    Form::ANSWER_COLLECTION,
                    array('_id' => new MongoId($ansKey)),
                    array('$set' => $set)
                )) {
                    // echo $count .'<br>';
                    $count++;
                }
            }
            PHDB::updateWithOptions(
                Form::ANSWER_COLLECTION,
                array("statusUpdated" => ['$exists' => true], "form" => ['$in' => array_keys($form)], 'statusInfo' => ['$exists' => true]),
                array('$unset' => ["statusUpdated" => true]),
                array('multi' => true, 'multiple' => true, 'upsert' => true)
            );
            echo "<h2 style='color:green;'>statusUpdated modifé en object successfully, element updated " . $count . "</h2>";
        }

        private function setRequiredInput() {
            $forms = PHDB::find(Form::COLLECTION, ["subForms" => ['$exists' => true], "params" => ['$exists' => true]], ["type", "subType", "subForms", "params"]);
            $inputUpdated = 0;
            foreach ($forms as $formKey => $formValue) {
                $paramsKeys = array_keys($formValue["params"]);
                if(strpos(implode(',',$paramsKeys),"validateStep") !== FALSE) {
                    if((isset($formValue["type"]) && $formValue["type"] == "aap") || (isset($formValue["subType"])  && $formValue["subType"] == "aap")) {
                        $subForms = PHDB::find(Form::INPUTS_COLLECTION, ["step" => ['$in' => $formValue["subForms"]], "formParent" => $formKey], ["collection", "formParent", "step", "inputs"]);
                    } else {
                        $subForms = PHDB::find(Form::COLLECTION, ["id" => ['$in' => $formValue["subForms"]]], ["collection", "formParent", "id", "inputs"]);
                    }
                    $collection = (isset($formValue["type"]) && $formValue["type"] == "aap") || (isset($formValue["subType"])  && $formValue["subType"] == "aap") ? Form::INPUTS_COLLECTION : Form::COLLECTION;
                    $formParamsIteration = 0;
                    $subFormsToSets = array();
                    foreach ($formValue["params"] as $paramsKey => $paramsValue) {
                        if(strpos($paramsKey, "validateStep") !== FALSE) {
                            if(isset($paramsValue["inputList"])) {
                                foreach ($paramsValue["inputList"] as $inputIndex => $inputItem) {
                                    foreach ($subForms as $subFormKey => $subFormValue) {
                                        if(isset($subFormValue["inputs"][$inputItem])) {
                                            $subFormsToSets[$subFormKey]["collection"] = $collection;
                                            $subFormsToSets[$subFormKey]["sets"]["inputs.".$inputItem.".isRequired"] = true;                       
                                        }
                                    }
                                }
                            }
                        }
                        if(isset($subFormsToSets) && $formParamsIteration == count($formValue["params"]) - 1) {
                            foreach ($subFormsToSets as $formToSetKey => $formToSetValue) {
                                if(
                                    $formToSetKey &&
                                    PHDB::updateWithOptions(
                                        $formToSetValue["collection"],
                                        ["_id" => new MongoId($formToSetKey)],
                                        ['$set' => $formToSetValue["sets"]],
                                        array('multi' => true, 'multiple' => true, 'upsert' => true)
                                    )
                                ) {
                                    $inputUpdated++;
                                }
                            }
                        }
                        $formParamsIteration++;
                    }
                }
            }
            echo "<h2 style='color:green;'>Input required set successfully, element updated " . $inputUpdated . "</h2>";
        }
        
        private function set_csv_exprotation_path() {
            $db_forms = PHDB::find(Form::COLLECTION, ['type' => 'aap'], ['parent', 'params']);
            $csv_export_view_path = [
                '61fa919180ebb509810d9190' => 'costum.views.custom.appelAProjet.callForProjects.exportation-csv-cosindnismarterre'
            ];
            $counter = 0;
            foreach ($db_forms as $db_id => $db_form) {
                $parent_key = array_keys($db_form['parent'] ?? []);
                $path = 'survey.views.custom.exportation-csv.appel_a_projet';
                if (!empty($parent_key) && array_key_exists($parent_key[0], $csv_export_view_path)) $path = $csv_export_view_path[$parent_key[0]];
                
                if (empty($db_form['params']['csv_exporter']) || $db_form['params']['csv_exporter'] !== $path) {
                    $params = !empty($db_form['params']) && is_array($db_form['params']) ? $db_form['params'] : [];
                    $params['csv_exporter'] = $path;
                    $set = ['params' => $params];
                    
                    $update = PHDB::updateWithOptions(
                        Form::COLLECTION,
                        ['_id' => new MongoId($db_id)],
                        ['$set' => $set],
                        ['upsert' => true]
                    );
                    $counter += $update['ok'] ? $update['nModified'] : 0;
                }
            }
            echo "<h2 style='color:green;'>$counter Formulaires mises à jours pour la configuration d'export CSV</h2>";
        }

        private function setViewedProposalFromViewsCollection(){
            $forms =  PHDB::distinct("views","form",array("form" => ['$exists' => true]));
            $formIds = array_map(function($v){
                if(!empty($v) && Api::isValidMongoId($v)) 
                    return new MongoId($v);
                else
                    return "";
            },$forms);

            $formsAap = PHDB::find(Form::COLLECTION,array("_id" => ['$in'=> $formIds],"type"=>"aap"),array("type"));
            $formsAapIds = array_keys($formsAap);
            $views = PHDB::find("views",array('form' => ['$in'=> $formsAapIds]));
            
            $answers = [];
            foreach ($views as $k => $v) {
                $answerId = $v["parentId"];
                $userId = $v["userId"];
                if(!isset($answers[$answerId])){
                    $answers[$answerId] = [];
                }
                $answers[$answerId][$userId] = new MongoDate(time());
            }
            
            $count = 0;
            foreach ($answers as $k => $v) {
                $where = array(
                    "_id" => new MongoId($k)
                );
                $action = array('$set'=>[]);
                foreach ($v as $userId => $mongoDate) {
                    $action['$set']["views.".$userId.".date"] = $mongoDate;
                }
                if(PHDB::update(Form::ANSWER_COLLECTION,$where,$action)){
                    $count++;
                }
            }
            echo "<h2 style='color:green;'>Views (seen by) updated in ".$count." answers</h2>";
        }

        private function setPresentationPageTemplate(){
            PHDB::remove(Cms::COLLECTION,array("page" => "presentationAap"));
            $params = array(
                "id" => "6486fbbbecbabb3b1f7ef93b", // id template
                "page"		 => "presentationAap",
                //"page"		 => "welcome",
				"action"	 => "",
				"collection" => null,
				"parentId"   => null,
				"parentSlug" => null,
				"parentType" => null
            );

            $elts = PHDB::find(
                Organization::COLLECTION,
            array("costum.type" => "aap"/*"slug" => ['$in' => ["proceco","coSindniSmarterre","coSinDni"]]*/),
                array("slug","collection","name","tplUsed") 
            );

            foreach ($elts as $kel => $vel) {
                $params["collection"] = Cms::COLLECTION;
                $params["parentId"] = (string)$vel["_id"];
                $params["parentSlug"] = $vel["slug"];
                $params["parentType"] = $vel["collection"];
                $vel["contextType"] = $vel["collection"];
                $vel["contextId"] = (string)$vel["_id"];
                $vel["app"] = $vel["costum"]["app"] ?? [];
                $contextCostum = $vel;
                
                $template = Element::getElementById($params["id"],Template::COLLECTION);
                $res = Cms::useTemplate($template,$contextCostum,false);
                CacheHelper::delete($vel['slug']);
            }
            echo "<h2 style='color:green;'>Template added to présentation(presentationAap)</h2>";
        }

        private function specifics(){
            $css = <<<EOF
                #mainNav .btn-menu-connect{
                    padding: 16px 5px 0 5px !important;
                    font-size: 18px !important; 
                }
                .cosDyn-menuTop .cosDyn-left .cosDyn-app .cosDyn-buttonList,
                .cosDyn-menuTop .lbh-menu-app
                {
                    text-decoration : none;
                }
                
                .cosDyn-menuTop .cosDyn-left .cosDyn-app .cosDyn-buttonList:hover,
                .cosDyn-menuTop .lbh-menu-app:hover
                {
                    color : var(--aap-primary-color) !important;
                }
                .cosDyn-menuTop .cosDyn-left .cosDyn-app .cosDyn-buttonList.active,
                .cosDyn-menuTop .lbh-menu-app.active
                {
                    color : var(--aap-primary-color) !important;
                }
                .navbar-top .menu-btn-top {
                    font-size: 18px;
                    padding-top: 16px !important;
                    position: relative;
                }
            EOF;
            $elts = PHDB::find(
                Organization::COLLECTION,
                array("costum.type" => "aap"),
                array("slug" => 1,"costum"=>1)
            );
            $eltsProjects = PHDB::find(
                Organization::COLLECTION,
                array("costum.type" => "aap"),
                array("slug" => 1,"costum"=>1)
            );

            foreach($elts as $key => $value){
                switch ($value["slug"]) {
                    case 'lesCommunsDesTierslieux':
                       PHDB::update(
                            Organization::COLLECTION,
                            array("slug" => $value["slug"]),
                            array('$unset' => array(
                                "costum.htmlConstruct.header.menuTop.left.buttonList.app.buttonList.#en-projet" => true,
                                "costum.htmlConstruct.header.menuTop.left.buttonList.app.buttonList.#en-proposition" => true,
                                "costum.app.#en-projet" => true,
                                "costum.app.#en-proposition" => true,
                                "costum.app.#add-answer" => true
                            ))
                        );
                        break;
                    case 'arianelila':
                        PHDB::update(
                                Organization::COLLECTION,
                                array("slug" => $value["slug"]),
                                array('$unset' => array(
                                    "costum.type" => true
                                ))
                            );
                            break;
                    case 'proceco':
                        Cms::createORupdateCss( $key, $css);
                        PHDB::update(
                            Organization::COLLECTION,
                            array("slug" => $value["slug"]),
                            array('$set' => array(
                                "costum.css.loader.loaderUrl" => "loader_8"
                            ))
                        );
                        break;
                    case 'coSindniSmarterre':
                        Cms::createORupdateCss( $key, $css);
                        PHDB::update(
                            Organization::COLLECTION,
                            array("slug" => $value["slug"]),
                            array('$set' => array(
                                "costum.css.loader.loaderUrl" => "loader_8"
                            ))
                        );
                        break;
                    case 'coSinDni':
                        Cms::createORupdateCss( $key, $css);
                        PHDB::update(
                            Organization::COLLECTION,
                            array("slug" => $value["slug"]),
                            array('$set' => array(
                                "costum.app.#actions" => [
                                    "hash" => "#app.view",
                                    "urlExtra" => "/page/actions/url/costum.views.custom.coSinDni.cacsAction",
                                    "subdomainName" => "Actions"
                                ],
                                "costum.css.loader.loaderUrl" => "loader_8"
                            ))
                        );
                        PHDB::update(
                            Organization::COLLECTION,
                            array("slug" => $value["slug"]),
                            array('$set' => array(
                                "costum.app.#cacs" => [
                                    "hash" => "#app.view",
                                    "urlExtra" => "/page/cacs/url/costum.views.custom.coSinDni.cacsList",
                                    "subdomainName" => "Cacs"
                                ]
                            ))
                        );
                        PHDB::update(
                            Organization::COLLECTION,
                            array("slug" => $value["slug"]),
                            array('$set' => array(
                                "costum.app.#observatory" => [
                                    "hash" => "#app.view",
                                    "urlExtra" => "/page/observatory/url/costum.views.custom.coSinDni.cacsObservatory",
                                    "subdomainName" => "Observatoire"
                                ]
                            ))
                        );
                        PHDB::update(
                            Organization::COLLECTION,
                            array("slug" => $value["slug"]),
                            array('$set' => array(
                                "costum.app.#quartiers" => [
                                    "hash" => "#app.view",
                                    "urlExtra" => "/page/quartiers/url/costum.views.custom.coSinDni.cacsQuartier",
                                    "subdomainName" => "Quartiers"
                                ]
                            ))
                        );
                        break;
                    default:
                        # code...
                        break;
                }
            }
            PHDB::update(Organization::COLLECTION,array('slug' => "proceco"),array('$set'=> ["oceco.subOrganization" => true]));
        }

        private function setProjectStatus() {
            $projects = PHDB::find(Project::COLLECTION, ["properties.avancement" => ['$exists' => false], "parent" => ['$exists' => true]], ['name']);
            $projectsId = array();
            foreach ($projects as $projectKey => $projectValue) {
                array_push($projectsId, new MongoId($projectKey));
            }
            if(count($projectsId) > 0)
            PHDB::updateWithOptions(
                Project::COLLECTION,
                ['_id' => ['$in' => $projectsId]],
                ['$set' => ["properties.avancement" => "concept"]],
                array('multi' => true, 'multiple' => true, 'upsert' => true)
            );
        }
        private function formStartDateAndDate() {
            $forms = PHDB::find(Form::COLLECTION,array(
                '$or' => [
                    array('startDate' => ['$exists'=>true]),
                    array('endDate' => ['$exists'=>true])
                ]
            ),array('startDate','endDate'));

            foreach ($forms as $k => $v) {
                if(!empty($v['startDate']) && is_string($v['startDate']) ){
                    $timestamp = null;
                    $dateTime = DateTime::createFromFormat('d/m/Y H:i', $v['startDate']);
                    $date = DateTime::createFromFormat('d/m/Y', $v['startDate']);
                    if ($dateTime !== false && $dateTime->format('d/m/Y H:i') === $v['startDate']){
                        $timestamp = $dateTime->getTimestamp();
                    }elseif ($date !== false && $date->format('d/m/Y') === $v['startDate']){
                        $timestamp = $date->getTimestamp();
                    }
                    
                    PHDB::update(Form::COLLECTION,
                        array('_id' => new MongoId($k)),
                        array(
                            '$set' => [
                                'startDate' => $timestamp,
                            ]
                        )
                    );
                }

                if(!empty($v['endDate']) && is_string($v['endDate']) ){
                    $timestamp = null;
                    $dateTime = DateTime::createFromFormat('d/m/Y H:i', $v['endDate']);
                    $date = DateTime::createFromFormat('d/m/Y', $v['endDate']);
                    if ($dateTime !== false && $dateTime->format('d/m/Y H:i') === $v['endDate']){
                        $timestamp = $dateTime->getTimestamp();
                    }elseif ($date !== false && $date->format('d/m/Y') === $v['endDate']){
                        $timestamp = $date->getTimestamp();
                    }

                    PHDB::update(Form::COLLECTION,
                        array('_id' => new MongoId($k)),
                        array(
                            '$set' => [
                                'endDate' => $timestamp,
                            ]
                        )
                    );
                }
            }
        }

        private function updateDateFormat() {
            $fields = [
                'startDate',
                'startDateNoconfirmation',
                'endDate',
                'endDateNoconfirmation'
            ];
	        $dbAapFormList = PHDB::find(Form::COLLECTION, [], $fields);
            $counter = 0;
            foreach ($dbAapFormList as $formId => $dbAapForm) {
                $set = [];
                foreach ($fields as $field) {
                    if(empty($dbAapForm[$field]) || !in_array(gettype($dbAapForm[$field]), ['integer', 'string'])) {
                        continue;
                    }
                    if(is_integer($dbAapForm[$field])) {
                        $set[$field] = new MongoDate($dbAapForm[$field]);
                    } else if(is_string($dbAapForm[$field])) {
                        $set[$field] = new MongoDate(strtotime($dbAapForm[$field]));
                    }
                }
                if(!empty($set)) {
                    PHDB::update(Form::COLLECTION,
                        ['_id' => new MongoId($formId)],
                        ['$set' => $set]
                    );
                    $counter++;
                }
            }
            echo "<h2 style='color:green;'>$counter Formulaires mises à jours pour la transformation des dates</h2>";
        }
    }