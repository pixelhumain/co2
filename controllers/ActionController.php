<?php

namespace PixelHumain\PixelHumain\modules\co2\controllers;

use CommunecterController;

/**
 * ActionController.php
 *
 * @author: Sylvain Barbot <sylvain.barbot@gmail.com>
 * Date: 7/29/15
 * Time: 12:25 AM
 */
class ActionController extends CommunecterController {
  

	public function beforeAction($action) {
	    parent::initPage();
	    return parent::beforeAction($action);
	}

	public function actions()
	{
	    return array(
	        'addaction'		=>	\PixelHumain\PixelHumain\modules\citizenToolKit\controllers\action\AddActionAction::class,
	       	'list'			=>	\PixelHumain\PixelHumain\modules\citizenToolKit\controllers\action\ListAction::class,
			"reorder"		=>	\PixelHumain\PixelHumain\modules\citizenToolKit\controllers\action\ReorderAction::class,
			"cornerdev"		=> 	\PixelHumain\PixelHumain\modules\citizenToolKit\controllers\action\CornerdevAction::class,
			"getactionsbyallidsparentwithrange" => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\action\GetActionsByAllIdsParentWithRangeAction::class,
			"getrangeactionsbyidparent" => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\action\GetActionsByIdParentWithRangeAction::class,
			'getprojectbypoleandparentid'		=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\action\GetByPoleAndParentIdAction::class,
			'getprojectbyidwithlimit'		=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\action\GetActionsByIdWithLimit::class,
	    );
	}
}