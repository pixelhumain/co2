<?php

namespace PixelHumain\PixelHumain\modules\co2\controllers;

use CommunecterController;

use Action;
/**
 * SiteController.php
 *
 * @author: Ifaliana Arimanana <ifaomega@gmail.com>
 * Date: 07/04/2023
 */

class ActivityLogController extends CommunecterController {
	public function beforeAction($action) {
	    parent::initPage();
	    return parent::beforeAction($action);
	}

	public function actions()
	{
	    return array(
	        'addactivitylog' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\activitylog\AddLogAction::class	    
	    );
	     
	}
}