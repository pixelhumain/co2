<?php
namespace PixelHumain\PixelHumain\modules\co2\controllers;

use Controller;
use MongoId;
use Person;
use PHDB;
use Preference;
use Rest;

class ActivitypubController extends Controller {
    
    public function actionGetViewGuide() {
        return $this->renderPartial("activitypubGuide");
    }

    public function actionGetViewStarter() {
        return $this->renderPartial("activitypubStarter");
    }

    public function actionCheckStarter() {
        $person = Person::getById($_SESSION["userId"]);

        return Rest::json([
            "open" => (!Preference::isActivitypubActivate($person["preferences"]) &&
                       !Preference::isActivitypubStarterClosed($person["preferences"]))
        ]);
    }

    public function actionCloseStarter() {
        Preference::updateConfidentiality($_SESSION["userId"], Person::COLLECTION, [
            "type" => "activitypubStarterClosed",
            "value" => true,
            "idEntity" => $_SESSION["userId"],
            "typeEntity" => Person::COLLECTION
        ]);
        return Rest::json(["success"=>true]);
    }
}