<?php

namespace PixelHumain\PixelHumain\modules\co2\controllers;

use CommunecterController;

/**
 * 
 *
 * @author: Raphael RIVIERE
 * Date:
 */
class AdminpublicController extends CommunecterController {

  public function beforeAction($action)
  {
	parent::initPage();
	return parent::beforeAction($action);
  }

	public function actions()
	{
		return array(
		'setmapping' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\adminpublic\SetMappingAction::class,
		'deletemapping' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\adminpublic\DeleteMappingAction::class,
		'adddata' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\adminpublic\AddDataAction::class,
	    'adddataindb' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\adminpublic\AddDataInDbAction::class,
	    'getdatabyurl' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\adminpublic\GetDataByUrlAction::class,
	    'assigndata'  => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\adminpublic\AssignDataAction::class,
	    'previewdata'  => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\adminpublic\PreviewDataAction::class,
  	    'mailslist' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\adminpublic\MailsListAction::class,
  	    'createfile' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\adminpublic\CreateFileAction::class,
		);
	}
}