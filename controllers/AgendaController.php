<?php
    
    namespace PixelHumain\PixelHumain\modules\co2\controllers;
    
    use PixelHumain\PixelHumain\modules\citizenToolKit\controllers\agenda\OpenAgendaAction;
    use PixelHumain\PixelHumain\modules\co2\controllers\actions\agenda;
    
    class AgendaController extends \CommunecterController {
        public function beforeAction($action) {
            parent::initPage();
            return parent::beforeAction($action);
        }
        
        public function actions(): array {
            return [
                'calendar'   => agenda\CalendarAction::class,
                'getcontext' => agenda\GetContextAction::class,
            ];
        }
    }