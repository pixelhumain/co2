<?php

namespace PixelHumain\PixelHumain\modules\co2\controllers;

use Badge;
use CO2Stat;
use CommunecterController;
use Element;
use Import;
use Mail;
use Organization;
use Slug;
use PHDB;
use Rest;
use Yii;

class AppController extends CommunecterController {
  

	public function beforeAction($action) {
	    parent::initPage();
	    return parent::beforeAction($action);
	}

	public function actions()
	{
	    return array(
	        'index'     		=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\app\IndexAction::class,
	        'live'    			=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\app\LiveAction::class,
	        'savereferencement' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\app\SaveReferencementAction::class,
	        'home'              => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\app\HomeAction::class,
            'view'              => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\app\ViewAction::class,
            'page'              => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\app\PageAction::class,
            'panelall'              => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\app\PanelAction::class,
            'calendar'          => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\app\CalendarAction::class,
            'search'            => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\app\SearchAction::class,
            'dashboard'         => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\app\DashboardAction::class,
            'welcome'           => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\app\WelcomeAction::class,
            'config'            => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\app\ConfigAction::class,
            'codate'            => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\app\CodateInsideAction::class,
            'invitecommunity'   => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\app\InviteAction::class,
            'getcodateresult'   => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\app\CodateResultAction::class,
            
            //'sendmailformcontact' => 'citizenToolKit.controllers.app.SendMailFormContactAction',
            'checkurlexists'    => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\app\CheckUrlExistsAction::class,
            'admin'             => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\app\AdminAction::class,
            'newsbuilder' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\app\NewsbuilderAction::class,
            'aap' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\aap\IndexAction::class,
            'pingrefreshview'   => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\app\OnPingRefreshViewAction::class,
            'codoc' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\app\CodocAction::class
        );
	}


  
    public function actionSocial($type=null){
        CO2Stat::incNbLoad("co2-search");   
        $params = array("type" => @$type );
        return $this->renderPartial("search", $params, true);
    }


    public function actionActivities(){
        CO2Stat::incNbLoad("terla-activities"); 
        $params = array("type" => "services");
        return $this->renderPartial("search", $params, true);
    }

    public function actionStore(){
        CO2Stat::incNbLoad("terla-store"); 
        $params = array("type" => "products");
        return $this->renderPartial("search", $params, true);
    }

    public function actionCircuits(){
        CO2Stat::incNbLoad("terla-store"); 
        $params = array("type" => "circuits");
        return $this->renderPartial("search", $params, true);
    }

    public function actionMap($slug=NULL, $src=NULL, $type=NULL){
        $appMap = null;
        if($slug){
            $collection=Slug::getElementBySlug($slug,array("collection"));
            $element = PHDB::findOne($collection["type"], ["slug"=>$slug], ["costum"]);
            if(isset($element["costum"]["map"])){
                $appMap = $element["costum"]["map"];
                $appMap["filterConfig"]["defaults"]["sourceKey"] = $slug;
            }
        }else if($src){
            $file_contents = @file_get_contents(base64_decode($src));
            if($file_contents){
                $data = json_decode($file_contents, true);
                if(isset($data["appMap"]))
                    $appMap = $data["appMap"];
            }
        }

        return $this->renderPartial("map", ["appMap"=>$appMap], true);
    }

    public function actionBadge($json = null)
    {
        $count = PHDB::count(Badge::COLLECTION, ['preferences.private' => false]);
        $rawIssuers = PHDB::distinct(Badge::COLLECTION, "issuer");
        $alreadyThere = [];
        $issuers = [];
        foreach ($rawIssuers as $id => $value) {
            if(count($value) != 1 || in_array( array_keys($value)[0], $alreadyThere)){
                continue;
            }
            array_push($alreadyThere, array_keys($value)[0]);
            $issuers[array_keys($value)[0]] = ['label' => str_replace('(Moi)', '', array_values($value)[0]["name"]), 'field' => 'issuer.' . array_keys($value)[0], 'value' => true, 'or' => true];
        }
        if ($json == true) {
            return Rest::json($issuers);
        }
        else {
            return $this->renderPartial("badge-finder", ['badgeCount' => $count, 'issuers' => $issuers], true);
        }
    }

    /*public function actionAap($page="welcome",$context=null,$form=null)
    {
        CO2Stat::incNbLoad("co2-aap");	
        $params = Aap::prepareParams($page,$context,$form);
        return $this->renderPartial("../aap/".$page,$params, true);
    }*/
   
    /*public function actionLive(){
        CO2Stat::incNbLoad("co2-live"); 
        $params = array();
        echo $this->renderPartial("live", $params, true);
    }*/

	public function actionAgenda(){
		CO2Stat::incNbLoad("co2-agenda");	
        $params = array("type" => "events");
    	return $this->renderPartial("search", $params, true);
	}

	public function actionDocs($page=null, $dir=null){
        CO2Stat::incNbLoad("co2-docs");   
        $params = array(
            "page" => @$page,
            "dir"=>@$dir,
        );
        return $this->renderPartial("co2.views.docs.index", $params, true);
    }
    

    public function actionAdminpublic($view = null){
        CO2Stat::incNbLoad("co2-adminpublic");   
        $view = ( !empty($view) ? $view : "index");
        $params = array();
        if($view == "createfile"){
            $count = PHDB::count(Import::MAPPINGS, array() ) ;
            if($count == 0){
                Import::initMappings(); 
            }
            $params["allMappings"] = Import::getMappings(); 
        }

        return $this->renderPartial("../adminpublic/".$view, $params, true);
    }

    public function actionChat(){
        CO2Stat::incNbLoad("co2-chat");   
        $params = array("iframeOnly"=>true);
        return $this->renderPartial("../rocketchat/iframe", $params, true);
    }

    public function actionRooms($type,$id){ exit;
        CO2Stat::incNbLoad("co2-rooms");    
        $params = array("id" => @$id,
                        "type" => @$type
                        );
        //print_r($params);
        return $this->renderPartial("rooms", $params, true);
    }
    // TODO : Code plus utiliser a vérifier 
    public function actionAnnonces(){
        CO2Stat::incNbLoad("co2-rooms");   
        return $this->redirect( Yii::app()->createUrl("/eco") );
    }



    public function actionHelp(){ 
        return $this->redirect( Yii::app()->createUrl("/ressources/co/ressources") );
    }

    public function actionInterop(){
        //echo "Hello there "; echo Yii::app()->createUrl("/interop/co/index"); exit; 
        return $this->redirect( Yii::app()->createUrl("/interop") );
    }
 

    public function actionInteroperability(){
        CO2Stat::incNbLoad("co2-interoberability");
        return $this->renderPartial("interoperability", array(), true);
    } 

    public function actionInfo($p){
        $CO2DomainName = isset(Yii::app()->params["CO2DomainName"]) ? 
                               Yii::app()->params["CO2DomainName"] : "CO2";

        $page = @$p ? $p : "apropos";
        return $this->renderPartial("info/" . $CO2DomainName . "/" . $page, array(), true);
    }

    public function actionSmartconso($p="home"){ //error_log("ecoconso");
        $CO2DomainName = isset(Yii::app()->params["CO2DomainName"]) ? 
                               Yii::app()->params["CO2DomainName"] : "CO2";

        $page = @$p ? $p : "home";
        return $this->renderPartial("../smartconso/" . $page, array(), true);
    }

    public function actionCity($insee, $postalCode){
        
        return $this->renderPartial("city", array("insee"=> $insee, "postalCode" => $postalCode), true);
    }

    private function rpHash($value) { 
        $hash = 5381; 
        $value = strtoupper($value); 
        for($i = 0; $i < strlen($value); $i++) { 
            $hash = ($this->leftShift32($hash, 5) + $hash) + ord(substr($value, $i)); 
        } 
        return $hash; 
    } 

     // Perform a 32bit left shift 
     private function leftShift32($number, $steps) { 
        // convert to binary (string) 
        $binary = decbin($number); 
        // left-pad with 0's if necessary 
        $binary = str_pad($binary, 32, "0", STR_PAD_LEFT); 
        // left shift manually 
        $binary = $binary.str_repeat("0", $steps); 
        // get the last 32 bits 
        $binary = substr($binary, strlen($binary) - 32); 
        // if it's a positive number return it 
        // otherwise return the 2's complement 
        return ($binary[0] == "0" ? bindec($binary) : 
            -(pow(2, 31) - bindec(substr($binary, 1)))); 
    } 


    public function actionSendMailFormContact(){       
        if ($this->rpHash($_POST['captchaUserVal']) == $_POST['captchaHash']){
            Mail::sendMailFormContact($_POST["emailSender"], $_POST["names"], $_POST["subject"], $_POST["contentMsg"]);
            
            $res = array("res"=>true, "captcha"=>true);  
            return Rest::json($res); exit;
        }else{
            $res = array("res"=>false, "captcha"=>false, "msg"=>"Code de sécurité incorrecte");  
            return Rest::json($res); exit;
        }

        $res = array("res"=>false, "msg"=>"Une erreur inconnue est survenue. Sorry");  
        return Rest::json($res);
        exit;
    }

    public function actionSendMailFormContactPrivate(){
        if ($this->rpHash($_POST['captchaUserVal']) == $_POST['captchaHash']){

            if(!empty($_POST["typeReceiverParent"])){
                $element = Element::getByTypeAndId($_POST["typeReceiverParent"], $_POST["idReceiverParent"]);
                $idReceiver = $_POST["idReceiver"];

                if( @$element && !empty($element) && 
					!empty($element["contacts"]) && 
					!empty($element["contacts"][$idReceiver]) && 
					!empty($element["contacts"][$idReceiver]["email"]) ){
					
					$emailReceiver = $element["contacts"][$idReceiver]["email"];
					error_log("EMAIL FOUND : ".$emailReceiver);

					
                }
            } else if (!empty($_POST["emailSender"]))  {
                $emailReceiver = $_POST["emailFrom"];
            }

            if(!empty($emailReceiver))
                Mail::sendMailFormContactPrivate($_POST["emailSender"], $_POST["names"], $_POST["subject"], $_POST["contentMsg"], $emailReceiver);
            
            $res = array("res"=>true, "captcha"=>true);  
            return Rest::json($res); exit;

            
        }else{
            $res = array("res"=>false, "captcha"=>false, "msg"=>"Code de sécurité incorrecte");  
            return Rest::json($res); exit;
        }

        $res = array("res"=>false, "msg"=>"Une erreur inconnue est survenue. Sorry");  
        return Rest::json($res);
        exit;
    }

    
}