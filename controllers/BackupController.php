<?php
namespace PixelHumain\PixelHumain\modules\co2\controllers;

use CommunecterController;

/**
 * SiteController.php
 *
 * @author: Tibor Katelbach <tibor@pixelhumain.com>
 * Date: 7/23/12
 * Time: 12:25 AM
 */
class BackupController extends CommunecterController {
  
	public function beforeAction($action) {
		parent::initPage();
		return parent::beforeAction($action);
  	}

	public function actions()
	{
	    return array(
	        //CTK actions
	        'save'                => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\backup\SaveAction::class,
	        'delete'                => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\backup\DeleteAction::class,
	        'update'                => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\backup\UpdateAction::class,
	    );
	}
    

}