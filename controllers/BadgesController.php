<?php
namespace PixelHumain\PixelHumain\modules\co2\controllers;

use CommunecterController;

/**
 * SiteController.php
 *
 * @author: Tibor Katelbach <tibor@pixelhumain.com>
 * Date: 7/23/12
 * Time: 12:25 AM
 */
class BadgesController extends CommunecterController {
  
	public function beforeAction($action) {
		parent::initPage();
		return parent::beforeAction($action);
  	}

	public function actions()
	{
	    return array(
	        'index' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\badges\IndexAction::class,
	        'issuers' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\badges\IssuersAction::class,
	        'profiles' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\badges\ProfilesAction::class,
	        'assertions' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\badges\AssertionsAction::class,
	        'badges' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\badges\BadgesAction::class,
			'assign' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\badges\AssignAction::class,
			'revoke' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\badges\RevokeAction::class,
			'cancel-revoke' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\badges\CancelRevokeAction::class,
			'confirm' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\badges\ConfirmAction::class,
			'elementbadge' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\badges\ElementbadgeAction::class,
			'views' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\badges\ViewsAction::class,
			'validator' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\badges\ValidatorAction::class,
			'finder' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\badges\FinderAction::class,
			'details' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\badges\DetailsAction::class,
			'settings' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\badges\SettingsAction::class,
			'rebuild' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\badges\RebuildAction::class,
			'badge-creator' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\badges\BadgecreatorAction::class,
			'parcours' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\badges\ParcoursAction::class,
			'parcoursadder' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\badges\ParcoursadderAction::class,
			'parcoursedit' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\badges\ParcourseditAction::class,
			'parcoursdeleter' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\badges\ParcoursdeleterAction::class,
			'count-attente' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\badges\CountAttenteAction::class,
			'get-criteria' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\badges\GetCriteriaAction::class,
			'endorsement-modal' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\badges\EndorsementModalAction::class,
			'getchild' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\badges\GetChildBadgeAction::class,
			'setbadgeparentinblockcms' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\badges\SetBadgeParentInBlockCmsAction::class,
			'getcitoyen-possess-badge' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\badges\GetCitoyenWhoPossessBadgeAction::class,
			'getbadge' =>  \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\badges\GetBadgeAction::class,
	    );
	}
    

}