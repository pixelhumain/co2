<?php
namespace PixelHumain\PixelHumain\modules\co2\controllers;

use CommunecterController;

/**
 * SiteController.php
 *
 * @author: Tibor Katelbach <tibor@pixelhumain.com>
 * Date: 7/23/12
 * Time: 12:25 AM
 */
class BookmarkController extends CommunecterController {
  
	public function beforeAction($action) {
		parent::initPage();
		return parent::beforeAction($action);
  	}

	public function actions()
	{
	    return array(
	        //CTK actions
	        'delete'                => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\bookmark\DeleteAction::class,
	        'save'                => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\bookmark\SaveAction::class,
	        'sendmailnotif'                => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\bookmark\SendMailNotifAction::class,
	    );
	}
    

}