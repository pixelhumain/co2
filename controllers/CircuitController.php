<?php

namespace PixelHumain\PixelHumain\modules\co2\controllers;
use CommunecterController;

/**
 * CommentController.php
 *
 * @author: Sylvain Barbot
 * Date: 2/7/15
 * Time: 12:25 AM
 */
class CircuitController extends CommunecterController {
  

	public function beforeAction($action) {
	    parent::initPage();
	    return parent::beforeAction($action);
	}

	public function actions()
	{
	    return array(
	        //'index'       			=> 'citizenToolKit.controllers.comment.IndexAction',
	        'save'					=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\circuit\SaveAction::class,
	        'index'					=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\circuit\IndexAction::class,
	        /*'abuseprocess'			=> 'citizenToolKit.controllers.comment.AbuseProcessAction',
	        'moderate'				=> 'citizenToolKit.controllers.comment.ModerateAction',
	        'countcommentsfrom'		=> 'citizenToolKit.controllers.comment.CountCommentsAction',
	        'delete'				=> 'citizenToolKit.controllers.comment.DeleteAction',
	        'updatefield'			=> 'citizenToolKit.controllers.comment.UpdateFieldAction',*/
	    );
	}

	public function actionTestPod() {
		$params = array();
		return $this->render( "testpod" , $params );
	}
}