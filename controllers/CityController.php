<?php

namespace PixelHumain\PixelHumain\modules\co2\controllers;

use CommunecterController;

/**
 * SiteController.php
 *
 * @author: antonio ramirez <antonio@clevertech.biz>
 * Date: 7/23/12
 * Time: 12:25 AM
 */
class CityController extends CommunecterController {
  

	public function beforeAction($action) {
	    parent::initPage();
	    return parent::beforeAction($action);
	}

	public function actions()
	{
	    return array(
	        'index'       			=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\city\IndexAction::class,
	        'detail'                => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\city\DetailAction::class,
	        'detailforminmap'       => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\city\DetailforminmapAction::class,
	        'directory'    		 	=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\city\DirectoryAction::class,
	        'calendar'      		=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\city\CalendarAction::class,
	        'statisticpopulation' 	=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\city\StatisticPopulationAction::class,
	        'getcitydata'     		=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\city\GetCityDataAction::class,
	        'getcityjsondata'     	=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\city\GetCityJsonDataAction::class,
	        'getcitiesdata'     	=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\city\GetCitiesDataAction::class,
	        'statisticcity'			=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\city\statisticCityAction::class,
	        'opendata'				=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\city\OpenDataAction::class,
	        'getoptiondata'			=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\city\GetOptionDataAction::class,
	        'getlistoption'			=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\city\GetListOptionAction::class,
	        'getpodopendata'		=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\city\GetPodOpenDataAction::class,
	        'addpodopendata'		=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\city\AddPodOpenDataAction::class,
	        'getlistcities'			=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\city\GetListCitiesAction::class,
	        'creategraph'			=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\city\CreateGraphAction::class,
	        'graphcity'				=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\city\GraphCityAction::class,
	        'updatecitiesgeoformat' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\city\UpdateCitiesGeoFormatAction::class,
	        'getinfoadressbyinsee'  => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\city\GetInfoAdressByInseeAction::class,
	        'cityexists'  			=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\city\CityExistsAction::class,
	        'autocompletemultiscope'=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\city\AutocompleteMultiScopeAction::class,
	        'save'       			=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\city\SaveAction::class,
	        'getlevel'		=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\city\GetLevelAction::class,
	        'getcitiesbyscope' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\city\GetCitiesByScopeAction::class,
	        'getgeoshape'		=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\city\GetGeoshapeAction::class,
	        'create'				=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\city\CreateAction::class,

	    );
	}
}