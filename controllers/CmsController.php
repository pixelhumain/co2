<?php

namespace PixelHumain\PixelHumain\modules\co2\controllers;

use CommunecterController;

class CmsController extends CommunecterController {
	public function beforeAction($action) {
	    parent::initPage();
	    return parent::beforeAction($action);
	}

	public function actions()
	{
	    return array(
	        'directory'    		 	=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\DirectoryAction::class,
	        'delete'    		 	=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\DeleteAction::class,
			'insert'    		 	=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\InsertAction::class,
	    	'addstructuredcolumn'	=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\AddStructuredCmsAction::class,
			'getcmsaction'    		=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\GetCmsAction::class,
			"getlistsbuilder"	 	=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\GetListsBuilderAction::class,
			"cmsmaker"			 	=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\CmsMakerAction::class,
			'gettemplate'	 		=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\GetTemplateAction::class,
			'inserttemplatejson' 	=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\InsertTemplateJsonAction::class,
			'gettemplatejson' 	    => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\GetTemplateJsonAction::class,
			'importbcms' 	  	    => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\ImportBcmsAction::class,
			'exportbcmsasjson' 	    => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\ExportBcmsAsJsonAction::class,
			'categoryusedbytplcms'  => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\GetCategoryUsedbyTemplateAction::class,
			'admindashboard' 		=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\admin\ViewAction::class,
	        'resetcache'    		=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\admin\ResetCacheAction::class,
			'updatecostum' 			=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\admin\UpdateCostumAction::class,
			'saveversion' 			=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\admin\SaveVersionCostumAction::class,
			'savetplcontent' 		=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\admin\SaveTemplateContentAction::class,
			'costumadmin' 			=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\admin\GetAdminAction::class,
			'getversion' 			=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\admin\GetVersionCostumAction::class,
			'featureinfo' 			=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\GetFeatureInfoAction::class,
			'restoreversion' 		=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\admin\RestoreCostumAction::class,
			'deletesavedcostum' 	=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\admin\DeleteSavedCostumAction::class,
			'getimagedocument' 		=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\admin\GetImageDocumentAction::class,
			'duplicate' 			=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\DuplicatePagesAction::class,
			'notemarkdown' 			=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\CmsMarkdownAction::class,
            'deletepage' 			=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\DeletePageAction::class,
            'usecms' 				=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\UseCmsAction::class,
			'duplicateblock'    	=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\DuplicateCmsAction::class,
			'updatepersistentcms'   => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\UpPersistCmsAction::class,
            'editdesign' 			=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\EditDesignAction::class,
			'getcmsaction'			=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\GetCmsAction::class,
			'getallcms' 			=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\GetAllCmsCostumAction::class,
            'getlistfile' 			=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\GetListFile::class,
			'usetemplate' 			=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\UseTemplateAction::class,
			'updatetemplate'		=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\UpdateTemplateAction::class,
			'previewtemplate'		=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\PreviewTemplateAction::class,
			'migrate' 				=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\MigrationAction::class,
			'check' 				=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\CheckCmsElementAction::class,
			'getlistblock' 			=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\GetChildByIdParentAction::class,
			'updatelistblock' 		=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\UpdateParentOfElementAction::class,
			'checkorphans' 			=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\CheckCmsOrphansAction::class,
			'updateblockorder'		=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\UpdateOrderAction::class,
			'deletetemplate'		=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\DeleteTemplate::class,
	    	'refreshmenu'			=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\RefreshMenuAction::class,
	    	'insertblocksection'	=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\InsertBlockSectionAction::class,
	    	'insertgallery'	        => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\InsertContainerSlideGalleryAction::class,
	    	'insertslide'			=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\InsertSlideRevealAction::class,
	    	'updateorderreveal'		=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\UpdateOrderRevealAction::class,
	    	'repairecms'			=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\RepaireCmsAction::class,
			'updatevaluepagecms' 	=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\admin\UpdateValuePageCmsAction::class,
			'refreshblock'			=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\RefreshBlockAction::class,
			'refreshmultiblock'	    => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\RefreshMultiBlockAction::class,
            'textautotranslate'		=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\TextAutoTranslateAction::class,
			'saveasblockcms'        => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\saveBlockAsBlockCmsAction::class,
            'savelog' 			    => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\admin\SaveLogsAction::class,
            'getlog' 			    => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\admin\GetLogsAction::class,
            'restorelogversion'     => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\admin\RestoreLogAction::class,
            'lastchange'            => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\admin\LastChangeLogAction::class,
            'createcostum' 			=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\costum\CreateCostumAction::class,			 
			"costum_have_controller"	=>	\PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\ElementHasCostumAction::class,
			"getinfo"	=>	\PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\costum\GetInfoAction::class,
			"chatopenai" 				=>  \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\ChatOpenAIAction::class,
			"generateimageopenai" 		=>  \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\GenerateImageOpenAIAction::class,
			"checkkeyopenai"        	=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\CheckKeyOpenAIAction::class,
			"savekeyopenai"				=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\SaveKeyOpenAIAction::class,
			"insertcontainercarousel"   => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\insertContainerCarouselAction::class,
			"updatecachefonts" 		=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\admin\UpdateFontsCacheAction::class, 
			"getallcontextform"		=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\survey\GetAllFormsContextAction::class,
			"getvaluebyidwithfields"	=>	\PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\costum\GetValueByIdWithFieldsAction::class,
			"navigatorai" => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\NavigatorDetailOpenAIAction::class,
	    );
	}
}