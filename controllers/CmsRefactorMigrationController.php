<?php

/* Author Ifaliana (ifaomega@gmail.com)
* Controller to update data with all bash done on db
* Documentation done before each function and in communecter/docs/devlog.md
*
*
*/

namespace PixelHumain\PixelHumain\modules\co2\controllers;
use Action;
use Cms;
use CommunecterController;
use Costum;
use CTKException;
use MongoDate;
use MongoId;
use MongoRegex;
use MongoWriteConcernException;
use Organization;
use Person;
use PHDB;
use Rest;
use Role;
use Yii;	
use function json_encode;
use function mb_strtolower;
use function strtr;

class CmsRefactorMigrationController extends CommunecterController {

	public function beforeAction($action) {
		return parent::beforeAction($action);
	}
	public function actions(){
	    return array(
			'migrate' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\MigrationAction::class,
			'uptadetypeblock' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\UpdateTypeBlockAction::class,
			'cleandatacontainerspcms' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\CleanDataContainerspcmsAction::class,
			'blockcms' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\migration\BlockCMSAction::class,
			'migrateaddclass' =>  \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\MigrateAddClassAction::class,
			'migratecssstructure' =>  \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\MigrateCssStructureAction::class,
			'migrateparamsbutton' =>  \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\MigrateParamsButtonAction::class,
			'migrateancientimage' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\MigrateAncientImageAndBGImageAction::class,
			"migrateparamsbasicmaps" => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\BasicMapsMigrationAction::class,
			'migrationprogresscircle' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\ProgressCircleMigrationAction::class,
			'bashtextwithvalue' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\BashTextWithValueAction::class,
			'migratetargetanchor' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\MigrateAnchorTargerAction::class,
			"migrateparamsmapd3" => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\MapD3MigrationAction::class,
			'migratehideon' =>  \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\MigrationHideOnAction::class,
			'migrationprogresscircle' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\ProgressCircleMigrationAction::class,
			'migrationsimplepie' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\SimplePieMigrationAction::class,
			'migrationquestionlocal' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\QuestionLocalMigrationAction::class,
			'migrationfootercontact' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\FooterContactMigrationAction::class,
			"migrationcontactform" => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\ContactFormMigrationAction::class,
			"migrationfootercollabo" => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\FooterWithCollaboMigrationAction::class,
			"migrationfooterlink" => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\FooterWithLinkMigrationAction::class,
			"migrateparamsbasicmaps" => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\BasicMapsMigrationAction::class,
			'bashelementcount' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\BashElementCountAction::class,
			'bashtextwithvalue' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\BashTextWithValueAction::class,
			'bashtextwithvalueandicon' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\BashTextWithValueAndIconAction::class,
			'bashformslist' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\BashFormsListAction::class,
			'bashmodalagenda' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\BashModalAgendaCalendarAction::class,
			'bashtextcoformbutton' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\BashTextWithButtonCoformAction::class,
			'bashannuairedropdown' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\BashAnnuaireDropdownAction::class,
			'bashtitlestyle' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\BashTitleStyleAction::class,
			'bashtextwithbtncreateacount' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\BashTextWithBtnCreateAcountAction::class,
			"migrateparamsslideforzone" => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\SlideForOneZoneMigrationAction::class,
			'bashnembox' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\BashNemBoxAction::class,
			'bashimgleftbtnright' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\BashImgLeftWithBtnRightAction::class,
			'bashimgleft' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\BashImgLeftAction::class,
			'bashimgtoleftandrightandtxt' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\BashImgToLeftAndRightWithTxtHeadingAction::class,
			'bashlinkdocs' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\BashLinkDocsAction::class,
			'bashtextcollapse' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cms\BashTextCollapseAction::class,
	    );
	}
}
