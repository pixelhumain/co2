<?php

namespace PixelHumain\PixelHumain\modules\co2\controllers;

use CommunecterController;

/**
 * CommentController.php
 *
 * @author: Sylvain Barbot
 * Date: 2/7/15
 * Time: 12:25 AM
 */
class CommentController extends CommunecterController {
  

	public function beforeAction($action) {
	    parent::initPage();
	    return parent::beforeAction($action);
	}

	public function actions()
	{
	    return array(
	        'index'       			=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\comment\IndexAction::class,
	        'save'					=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\comment\SaveAction::class,
	        'abuseprocess'			=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\comment\AbuseProcessAction::class,
	        'moderate'				=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\comment\ModerateAction::class,
	        'countcommentsfrom'		=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\comment\CountCommentsAction::class,
	        'delete'				=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\comment\DeleteAction::class,
	        'updatefield'			=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\comment\UpdateFieldAction::class,
	        'update'				=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\comment\UpdateAction::class,
	    );
	}

	public function actionTestPod() {
		$params = array();
		return $this->render( "testpod" , $params );
	}
}