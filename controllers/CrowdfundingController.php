<?php
namespace PixelHumain\PixelHumain\modules\co2\controllers;


use CommunecterController;

class CrowdfundingController extends CommunecterController {
  

	public function beforeAction($action) {
	    parent::initPage();
	    return parent::beforeAction($action);
	}

	public function actions()
	{
	    return array(
			'getcampaignandcountersbyparent' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\crowdfunding\getCampaignAndCountersByParentAction::class,
	        'getcampaignandcounters' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\crowdfunding\getCampaignAndCountersAction::class,
            'validatepledge'    	 => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\crowdfunding\ValidatePledgeAction::class,
	        'getpledgesfromcampaignid' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\crowdfunding\getPledgesFromCampaignIdAction::class,
	        'getpledgesfromcampaignid' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\crowdfunding\getCountersByIdAction::class
	    );
	}
}