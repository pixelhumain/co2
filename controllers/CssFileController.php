<?php

namespace PixelHumain\PixelHumain\modules\co2\controllers;
use CommunecterController;

/**
 * CssController.php
 *
 */
class CssFileController extends CommunecterController {
  

	public function beforeAction($action) {
	    parent::initPage();
	    return parent::beforeAction($action);
	}

	public function actions()
	{
	    return array(
	        'insert'    => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cssFile\AddAction::class,
			'getbycostumid'    => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cssFile\GetbycostumidAction::class,
	    );
	}
}