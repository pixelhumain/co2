<?php
	
/* Author Bouboule (clement.damiens@gmail.com)
* Controller to update data with all bash done on db
* Documentation done before each function and in communecter/docs/devlog.md
*
*
*/

namespace PixelHumain\PixelHumain\modules\co2\controllers;
use Action;
use ActivityStream;
use Answer;
use Badge;
use Bookmark;
use City;
use Classified;
use Cms;
use Comment;
use CommunecterController;
use Costum;
use Cron;
use Ctenat;
use CTKException;
use DataValidator;
use Document;
use Element;
use Event;
use Folder;
use Form;
use Htmlconverter;
use Link;
use Mail;
use MongoDate;
use MongoId;
use MongoRegex;
use MongoWriteConcernException;
use News;
use Nominatim;
use OpenData;
use Organization;
use Person;
use PHDB;
use Poi;
use Project;
use Proposal;
use Resolution;
use Ressource;
use Rest;
use Role;
use Room;
use SearchNew;
use SIG;
use Slug;
use Survey;
use Tags;
use Template;
use Yii;
use Zone;
use function json_encode;
use function mb_strtolower;
use function strtr;
use FranceTierslieux;
use Mapping;
use DateTime;

class DatamigrationController extends CommunecterController {
  
	public function beforeAction($action) {
		return parent::beforeAction($action);
	}
	
	public function actionPurgeSevrerCms($limit=0){
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
			$blckUnder=PHDB::findAndLimitAndIndex(Cms::COLLECTION, array('subtype'=>"underConstruction", 'page'=> 'block'), 10000,$limit);
			echo count($blckUnder)."block type underConstruct<br/>";
			$blckChild=PHDB::find(Cms::COLLECTION, array("blockParent"=>array('$in'=>array_keys($blckUnder))));
			$blckSubChild=PHDB::find(Cms::COLLECTION, array("blockParent"=>array('$in'=>array_keys($blckChild))));
			$blckSubSUBChild=PHDB::find(Cms::COLLECTION, array("blockParent"=>array('$in'=>array_keys($blckSubChild))));
			
			echo count($blckChild)."block type child (n+1)<br/>";
			echo count($blckSubChild)."block type Sub de Child (n+2)<br/>";
			echo count($blckSubSUBChild)."block type Sub de SubChild (n+3)<br/>";
		
			$countFoldToDelete=0;
			$arrayAllBlock=array_merge(array_keys($blckUnder), array_keys($blckChild), array_keys($blckSubChild), array_keys($blckSubSUBChild));
			echo "Total block to inspect: ".count($arrayAllBlock)."</br>";
			$docCollectionToDelete=PHDB::remove(Document::COLLECTION, array("id"=>array('$in'=>$arrayAllBlock)));
			echo "<br/><b>Total des document en collections supprimés : ".$docCollectionToDelete." </b></br>";
			foreach($arrayAllBlock as $id){
				if(Yii::$app->fs->has(Yii::app()->params['uploadDir']."communecter/cms/".$id)){
					// echo "delete folder : ".$id." <br/>";
					$countFoldToDelete++;
					// Delete folder entiere
					Yii::$app->fs->deleteDir(Yii::app()->params['uploadDir']."communecter/cms/".$id);
				}
			}
			echo "<br/><b>Total des folder upload/communecter/cms supprimés : ".$countFoldToDelete." </b></br>";
			
			echo "Total blocks Cms to delete: ".count($arrayAllBlock)."</br>";

    }
  }

  public function actionConvertLinkForCostumWelcome(){
        $output="";
        if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
            $costumProj=PHDB::count(Project::COLLECTION, array("costum"=>array('$exists'=>true)));
            $costumOrga=PHDB::count(Organization::COLLECTION, array("costum"=>array('$exists'=>true)));

            $costumCostum=PHDB::count(Costum::COLLECTION, array());
            echo "<h1>count project costum:".$costumProj." projet ayant un costum</h1><br/>";
            echo "<h1>count orga costum:".$costumOrga." orga ayant un costum</h1><br/>"; 
            echo "<h1>count costum costum:".$costumCostum." cosqtum ayant un costum</h1><br/>"; 
            foreach([Organization::COLLECTION, Project::COLLECTION, Costum::COLLECTION] as $v){
                if($v==Costum::COLLECTION){
                    $c=PHDB::find($v, array("slug"=>array('$exists'=>true)), array("slug", "app", "htmlConstruct.redirect"));
                }else{
                    $c=PHDB::find($v, array("costum"=>array('$exists'=>true)), array("_id", "slug", "costum"));
                }
                $nbWithCmsWelcome=0;
                foreach($c as $data){
                    $contextType=$v;
                    $contextId=@(string)$data["_id"];
                    $contextSlug=@$data["slug"];
                    if($v==Costum::COLLECTION){
                    //                        var_dump($data);
                        $slugdata=PHDB::findOne(Slug::COLLECTION, array("name"=>$data["slug"]));
                        $contextId=@$slugdata["id"];
                        $contextType=@$slugdata["type"];
                      //  if(@$data["slug"]=="ctenat")
                    }
                    $cmsList=PHDB::count(Cms::COLLECTION, array(
                                          "page"=>"welcome",
                                          "parent.".$contextId => array('$exists'=>1),
                                          ));
                    
                    if($v==Costum::COLLECTION){
                            if($contextSlug=="costumize")
                                var_dump($data);
                           if($cmsList>0){
                            echo "<h1>".$contextSlug."</h1>";
                            var_dump($data["htmlConstruct"]["redirect"]);
                            var_dump($data["app"]);
                           }
                           // echo "<span style='width:100%'>On regarde ".$data["slug"]." qui a ".$cmsList." cms en welcome page</span><br/>";
                    }else{
                        //if(!empty($data["costum"]["app"]))
                          //  var_dump($data["costum"]["app"]);
                    }
                    if($cmsList>0){
                        $nbWithCmsWelcome++;
                    }
                }
                echo '<h3>'.count($c)." costum from ".$v." dont ".$nbWithCmsWelcome." avec des cms sur la page welcome</h3>"; 
            }
        }else{
            echo "<h1>Are Talking To Me</h1><iframe src='https://giphy.com/embed/VW66IuyWkTdT7neytT' width='480' height='360' frameBorder='0' allowFullScreen></iframe>";
        }
    }
    public function actionGetAllIntemplateCol(){
        $output="";
        if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
            PHDB::updateWithOptions(Template::COLLECTION, array("_id"=>array('$exists'=>true)), array('$set'=>array("type"=>"costum")), array("multiple"=>true));
            //PHDB::remove(Template::COLLECTION, array("type"=>"page"));

            //PHDB::remove(Template::COLLECTION, array("type"=>"blockCms"));
            $customBlocks = Cms::getCmsByWhere(array("type" => "blockCms"),array( 'name' => 1 ));
            foreach($customBlocks as $e => $v){
                if(!isset($v["cmsList"]))
                    $v["blocTplId"]=(string)$v["_id"];
                unset($v["_id"]);
                var_dump($v);
                $v["collection"]=Template::COLLECTION;
                if(isset($v["cmsList"]))
                    $v["type"]="page";
                Yii::app()->mongodb->selectCollection(Template::COLLECTION)->insert( $v );
               
              /*  $where=array(
                    "id"=>$e, "type"=>$v["collection"], "doctype"=>"image"
                );
                $fileImage=Document::getListDocumentsWhere($where, "image");
                if(count($fileImage) > 1)
                    var_dump($fileImage);
                //PHDB
                */
            }
        }
    }
    public function actionGetCmsListFromTemplate(){
        $output="";
        if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){

            $templates = PHDB::find(Template::COLLECTION, array("type"=> "costum"));
            $tplCmsList = [];
            foreach($templates as $key => $template){
                if (isset($template["cmsList"])) {
                    $tplCmsList = array_merge($tplCmsList, $template["cmsList"]);
                }

            }   

            $tplCmsList = array_unique($tplCmsList);

            foreach ($tplCmsList as $key => $value) {
                $output .= $value.",\n"; 
            }
            echo "<p>".count($tplCmsList)."</p>";
            echo '<textarea>'.$output.'</textarea>' ;
        }
    }
    public function actionMoveCmsListTemplateCol($id){
        $output="";
        if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
            $arrayIds = explode(",", $id);
            $arrayCmsList = [];


            foreach ($arrayIds as $key => $value) {
                if ($value != "") {
                    $checkIfExistInTpl = PHDB::findOneById("templates",$value);
                    if(empty($checkIfExistInTpl)){
                        $eachCms = PHDB::findOneById(Cms::COLLECTION, $value);
                        echo "<pre>";
                        var_dump(Cms::getByIdWithChildreen("63d9e7c20ebece48d061c45b"));
                        echo "</pre>";
                        if (!empty($eachCms)) {
                            $arrayCmsList[$value] = $eachCms;
                        }

                    }else{

                        echo "afa ao : ".$value."<br>";

                    }
                }
            }
            // PHDB::batchInsert("templates",$arrayCmsList);  
        }
    }
  public function actionGetPageIntemplateCol(){
        $output="";
        if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
            // PHDB::updateWithOptions(Template::COLLECTION, array("_id"=>array('$exists'=>true)), array('$set'=>array("type"=>"costum")), array("multiple"=>true)); 
            $customBlocks = Cms::getCmsByWhere(array("type" => "template"),array( 'name' => 1 ));
            foreach($customBlocks as $e => $v){
                    // PHDB::remove(Template::COLLECTION, array("name"=>$v["name"]));
                unset($v["_id"]);
                echo "<h1>Vita!</h1>";
                var_dump($v);
                $v["collection"]=Template::COLLECTION;
                if(isset($v["cmsList"]))
                    $v["type"]="page";
                Yii::app()->mongodb->selectCollection(Template::COLLECTION)->insert( $v ); 
            }
        }
    }

    public function actionMigrateCmsTplCategory(){
        $output="";
        if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
            $tplCms = PHDB::find(Template::COLLECTION, ["type" => "blockCms"]);
            $count = 0;
            $category = [];
            foreach ($tplCms as $key => $value) {   
                $count++;
                $pathParts = explode(".", $value["path"]);      
                if (!in_array($pathParts[2],$category)) {
                   $category[] = $pathParts[2];
                }
                PHDB::update(Template::COLLECTION, array( "_id" => new MongoId($key) ), array('$set'=>array("category"=>$pathParts[2])));
            }
            echo "<pre>";
            echo "category: <br>";
            foreach ($category as $key => $value) {
                echo "- ".$value."<br>";
            }
            echo "</pre>";
            echo "<h3>These category are added.</h3>";
            echo "<b>Sum: ".$count."<br></b>";
        }
    }

    public function actionRemoveUrlforPagesIndexStaticPageUsingCms(){
        $output="";
        if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
            $costums=array(
                Project::COLLECTION=>PHDB::find(Project::COLLECTION, array("costum.app"=>array('$exists'=>true)),array("costum.app", "slug")),
                Organization::COLLECTION=>PHDB::find(Organization::COLLECTION, array("costum.app"=>array('$exists'=>true)),array("costum.app", "slug")),
                Costum::COLLECTION=>PHDB::find(Costum::COLLECTION, array("app"=>array('$exists'=>true)),array("app", "slug", "name"))
            );
            $costumCostum=PHDB::count(Costum::COLLECTION, array());
            echo "<h1>count project costum:".count($costums[Project::COLLECTION])." projet ayant un costum et des appPages</h1><br/>";
            echo "<h1>count orga costum:".count($costums[Organization::COLLECTION])." orga ayant un costumet des appPages</h1><br/>"; 
            echo "<h1>count costum costum:".count($costums[Costum::COLLECTION])." costum ayant un costum et des appPages</h1><br/>"; 
            foreach([Organization::COLLECTION, Project::COLLECTION, Costum::COLLECTION] as $v){
                foreach ($costums[$v] as $key => $value) {
                    $pages=($v==Costum::COLLECTION) ? $value["app"] : $value["costum"]["app"];
                   // $title=(@$value["title"]) ? @$value["title"] : @$value["name"];
                     $str="";   
                    foreach($pages as $slug =>$data){
                        if(isset($data["urlExtra"]) 
                            && (!empty($data["hash"]) && $data["hash"]=="#app.view") 
                            && strpos($data["urlExtra"], "/url/costum.views.tpls.staticPage")){
                            $str.="<span>".$slug.":".@$data["hash"]." > ".$data["urlExtra"]."</span><br/>";
                            $pages[$slug]["urlExtra"]=str_replace("/url/costum.views.tpls.staticPage", "", $data["urlExtra"]);
                        }
                    }
                    if(!empty($str)){
                            echo "<h1>".$value["slug"]."</h1>".$str;
                            $path=($v==Costum::COLLECTION) ? "app" : "costum.app";
                   
                            var_dump($pages);
                            PHDB::update($v, array("_id"=>$value["_id"]), array('$set'=>array($path=>$pages)));
                    }
                    
                }
                //echo '<h3>'.count($c)." costum from ".$v." dont ".$nbWithCmsWelcome." avec des cms sur la page welcome</h3>"; 
            }
        }else{
            echo "<h1>Are Talking To Me</h1><iframe src='https://giphy.com/embed/VW66IuyWkTdT7neytT' width='480' height='360' frameBorder='0' allowFullScreen></iframe>";
        }
    }
    public function actionConvertLinkForCostumWelcomeBasedOnBlockFirst(){
        /* CLEAN ET QUESTION

        */
        $output="";
        if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
            $block=PHDB::find(Cms::COLLECTION, array("page"=>"welcome"));
            echo count($block)." sont des block de welcome page";
            $arrayParent=array();
            $blockWithEmptyParent=array();
            $slugCostumToUp=array();
            $blockWithUnexistedParent=0;
            foreach($block as $id => $data){
                if(!empty($data["parent"])){
                    foreach($data["parent"] as $k => $v){
                        if($k=="organizations")
                            PHDB::remove(Cms::COLLECTION, array("_id"=>$data["_id"]));
                        else if(!isset($arrayParent[$k]))
                            $arrayParent[$k]=$v;
                    }
                }else
                    array_push($blockWithEmptyParent, $id);
            }
           foreach($arrayParent as $key => $data){
                $col="";
                if(isset($data["type"]))
                    $col=$data["type"];
                else if(isset($data["collection"]))
                    $col=$data["collection"];
              //  echo $col.":".$key."<br/>";
                //if($key=="organizations")

                if(!empty($col) && strlen($key)>23){
                    $ctxCostum=PHDB::findOne($col, array("_id"=>new MongoId($key)), array("slug","name","costum"));
                    if(empty($ctxCostum)){
                        $blockWithUnexistedParent+=PHDB::count(Cms::COLLECTION, array("parent.".$key=>array('$exists'=>true)));
                        //$blcWithGost=PHDB::count(Cms::COLLECTION, array("parent.".$key=>array('$exists'=>true)));
                        //var_dump($blcWithGost);
                        //$blockWithUnexistedParent=$blockWithUnexistedParent+count($blcWithGost);
                    }else{
                        if(isset($ctxCostum["costum"]["htmlConstruct"]["redirect"])){
                            var_dump($ctxCostum["name"]);
                            var_dump($ctxCostum["costum"]["htmlConstruct"]["redirect"]);
                        }
                        // if($ctxCostum["costum"]["slug"]==="filiereGenerique")
                        //     var_dump($ctxCostum["slug"]);
                        if(!in_array(@$ctxCostum["costum"]["slug"], $slugCostumToUp))
                            array_push($slugCostumToUp, @$ctxCostum["costum"]["slug"]);
                        //echo $ctxCostum["name"]." // ".$ctxCostum["costum"]["slug"]."<br/>";
                    }
                }else{
                    echo "ERRRRRRRRRRRRRROOOOOOOOOOOOOOOOORRRRRRRRRRRRRR";
                    var_dump($key);
                    var_dump($data);
                }
            }
            //var_dump($arrayParent);
            foreach($slugCostumToUp as $slug){
                echo "<h1>".$slug."</h1><br/>";
                $costum=PHDB::findOne(Costum::COLLECTION, array("slug"=>$slug));
                if(isset($costum["app"])){
                    foreach($costum["app"] as $k => $v){
                        if($k!=="#welcome" && isset($v["hash"]) && $v["hash"]=="#app.welcome"){
                            $v["hash"]="#app.view";
                            $v["urlExtra"]="/page/welcome";
                            $costum["app"][$k]=$v;
                        }
                    }
                    if(!isset($costum["app"]["#welcome"])){
                        echo "<h3>contient des app mais pas de welcome</h3>";
                        $costum["app"]["#welcome"]=array("hash"=>"#app.view", "urlExtra"=>"/page/welcome");
                       
                    }else{
                        echo "<h3>contient une app welcome</h3>";
                        $costum["app"]["#welcome"]["hash"]="#app.view";
                        $costum["app"]["#welcome"]["urlExtra"]="/page/welcome";
                      
                    }

                }
                if(!isset($costum["app"])){
                    echo "<h3>On ajoute une entrée #welcome avec le bon redire vers view</h3>";
                    $costum["app"]=array("#welcome"=>array("hash"=>"#app.view", "urlExtra"=>"/page/welcome"));
                }
                if(empty($costum["_id"])){
                    var_dump($slug);echo"ERRRRRRRRRRRRRRRRRRRRRRROOOOOOOOOOOOOOOOOR";
                    var_dump($costum);
                }else{
                    PHDB::update(Costum::COLLECTION,array("_id"=>$costum["_id"]), 
                            array('$set'=>array("app"=>$costum["app"])));
                }  
                //var_dump($costum["app"]);
            }
            var_dump($slugCostumToUp);
            echo $blockWithUnexistedParent." blocks avec des parents fantôme<br/>";
            echo count($arrayParent)." context Elt ont des blocks welcome<br/>";
            echo count($blockWithEmptyParent)." blocs n'ont pas de parent dans leur donnée";
            
        }else{
            echo "<h1>Are Talking To Me</h1><iframe src='https://giphy.com/embed/VW66IuyWkTdT7neytT' width='480' height='360' frameBorder='0' allowFullScreen></iframe>";
        }
  }  
  public function actionAddAnchorTargetToParentForMenuSinglePageBloc(){
     if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
            ini_set('memory_limit', '-1');
            ini_set('max_execution_time', 300);
            $searchCms=PHDB::find(Cms::COLLECTION, array("menuSinglePage"=>array('$exists'=>true)), array("_id", "menuSinglePage","blockParent", "path"));
                            echo "<h1>".count($searchCms)." blocs qui ont une ancre à copier sur le parent</h1>";

            foreach($searchCms as $k => $v){
                if(empty($v["menuSinglePage"])){
                    echo "<br/><br/><h1>Empty menuSinglePage ".$k." remove menuSingePage".$v["path"]."</h1>";
                    PHDB::update(Cms::COLLECTION, array("_id"=> $v["_id"]), 
                        array('$unset'=> array("menuSinglePage"=>"")));
                    if(empty($v["blockParent"]))
                        echo "<br/><span>With empty bck parent</span>";

                }else if(empty($v["blockParent"])){
                    echo "<br/><br/><h1>menuSiglePage : ".$v["menuSinglePage"]." with Empty blockParent ".$k." with path : ".$v["path"]."</h1>";
                    PHDB::update(Cms::COLLECTION, array("_id"=> $v["_id"]), 
                        array('$set'=> array("anchorTarget"=>$v["menuSinglePage"]), '$unset'=> array("menuSinglePage"=>"")));

                   // echo "path supercontainer:".$v["path"];
                }else{
                    PHDB::update(Cms::COLLECTION, array("_id"=>new MongoId($v["blockParent"])),  array('$set'=> array("anchorTarget"=>$v["menuSinglePage"])));
                    PHDB::update(Cms::COLLECTION, array("_id"=> $v["_id"]), 
                        array('$unset'=> array("menuSinglePage"=>"")));

                    echo "<br/><h1>Ici ".$k." remove menuSingePage".$v["menuSinglePage"]."</h1>";
                }
                echo "<br/>-------------------------</br>--------------------------------</br>";
               } 
            //var_dump($searchCms);
    }else{
        echo "<h1>Are Talking To Me</h1><iframe src='https://giphy.com/embed/VW66IuyWkTdT7neytT' width='480' height='360' frameBorder='0' allowFullScreen></iframe>";
    }
  }

  public function actionUpdateHaveTplBackup(){
	$output = "";
	$i = 0;
	if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
		$searcCms=PHDB::find(Cms::COLLECTION, 
								array(
										"haveTpl"=>true,
										"tplParent" => array('$exists'=>true),
										"type" => "blockCopy"
									)
								);
		$output.= "<h1>".count($searcCms)." blocs qui a tplParent et haveTpl true</h1>";
		$backup = 0 ;
		$tplVide = 0 ;
		$tplUsersVide = 0 ;
		foreach($searcCms as $kCms => $vCms){
			$tpl = PHDB::findOne(Cms::COLLECTION,array("_id" => new MongoId($vCms["tplParent"])));
			if(isset($tpl)){
				if(isset($tpl["tplsUser"][key((array)$vCms["parent"])][$vCms["page"]])){
					if( $tpl["tplsUser"][key((array)$vCms["parent"])][$vCms["page"]] != "using" ){
						$backup++;
						PHDB::update(Cms::COLLECTION, array("_id"=> $vCms["_id"]), array('$set'=> array("haveTpl"=>"backup"))); 
					}
				}else{
					$tplUsersVide++; 
					PHDB::update(Cms::COLLECTION, array("_id"=> $vCms["_id"]), array('$set'=> array("haveTpl"=>"backup"))); 
				}
			}else{
				$tplVide++; 
				PHDB::update(Cms::COLLECTION, array("_id"=> $vCms["_id"]), array('$set'=> array("haveTpl"=>"backup"))); 
			}
		}
		$output .= "Mise à jour de ".$backup." bloc backup effectué <br>" ;
		$output .= "Mise à jour de ".$tplVide." blocs avec tpl vide <br>" ;
		$output .= "Mise à jour de ".$tplUsersVide." blocs qui n'est pas dans tplsUser <br>" ;
	}else{
		$output .= "Vous n'êtes pas autorisé";
	}
	return $output;
  }
	public function actionFixBackup(){
        $searcCms=PHDB::find(Cms::COLLECTION, 
                        array(
                                "haveTpl"=>"backup",
                                "tplParent" => array('$exists'=>true),
                                "type" => "blockCopy"
                            )
                        );
        foreach($searcCms as $kCms => $vCms){
            $tpl = PHDB::findOne(Cms::COLLECTION,array("_id" => new MongoId($vCms["tplParent"])));
            if(isset($tpl)){
                if(isset($tpl["tplsUser"][key((array)$vCms["parent"])][$vCms["page"]])){
                    if( $tpl["tplsUser"][key((array)$vCms["parent"])][$vCms["page"]] == "using" ){
                        PHDB::update(Cms::COLLECTION, array("_id"=> $vCms["_id"]), array('$set'=> array("haveTpl"=>true))); 
                    }
                }
            }
        }   
    }

    public function actionChangeWithToStdr($limit = 500){
        if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
             $countFounded = 0;
            for ($i=1;$i < 11;$i++) { 
                $searcCms=PHDB::findAndLimitAndIndex(Cms::COLLECTION, 
                    array(
                        "class.width"=> "sp-cms-".$i."0"
                    ),
                    $limit,
                    0
                );
                foreach($searcCms as $kCms => $vCms){
                    PHDB::update(Cms::COLLECTION, array("_id"=> $vCms["_id"]), array('$set'=> array("class.width"=>"sp-cms-std","css.width" => $i."0%"))); 
                } 
                $countFounded +=count($searcCms);
                echo "<br><br>CMS sp-cms-".$i."0";
                echo "<br> Found & changed:".count($searcCms);
                echo "<br><b> Limit: ".$limit."</b>";  
                if (count($searcCms) == $limit) {
                    echo "<br><b style='color:red'>Nbr du CMS egale à la limit,<br>
                    Il faut relancer le BATCH pour finir les autres CMS qui sont dehors de la limit</b>";
                }
            }
            echo "<br><br><b> Total: ".$countFounded;  
        }else{
            echo "Vous n'êtes pas autorisé";
        }
    }
	public  function actionDeleteCocity()
	{ 
		$allCocity = PHDB::find(Organization::COLLECTION,array("costum.slug" => "cocity"));

		foreach($allCocity as $k => $vCocity){ 
			$cms= PHDB::find(Cms::COLLECTION,array("parent.".(String)$vCocity["_id"]=>array('$exists'=>true)),array("_id"));
			foreach($cms as $kc => $vCms)	
			{
				PHDB::remove(Cms::COLLECTION, array("_id"=>$vCms["_id"]));
			}	
			PHDB::remove(Organization::COLLECTION, array("_id"=>$vCocity["_id"]));		
		}
		return count($allCocity)." supprimer";
		
	}
    public function actionUpCmsOldVersionToNew(){
        $output="";
        if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
                ini_set('memory_limit', '-1');
                ini_set('max_execution_time', 1000);
                $searchCms=PHDB::find(Cms::COLLECTION, array("type"=>"blockCopy", "subtype"=> "supercms"), array("_id")); 
                $output .= "<h1>".count($searchCms)." blocs qui sont des parents et qui vont rentrer dans la moulaga</h1>";
                //var_dump($searchCms);
                $counterFinal=self::checkIfCMSGotChild($searchCms);
                $output .= '<h1>'.$counterFinal.' bloccccccs ont été modifiées</h1>';
        }else{
			$output .="<h1>Are Talking To Me</h1><iframe src='https://giphy.com/embed/VW66IuyWkTdT7neytT' width='480' height='360' frameBorder='0' allowFullScreen></iframe>";
        }
		return $output;

    }
    public function actionAddUnitOnValue(){
        $output="";
        if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )) {
            $arrayProperty = [
                "keyList" => ["fontSize","width","height","top","right","left","bottom","borderWidth","paddingTop","paddingLeft","paddingRight","paddingBottom","borderRadius","marginTop","marginRight","marginBottom","marginLeft"],
                "ring" => ["ring1","ring2"],
                "menuTopPosition" => ["left","center","right"],
                "button" => ["networkFloop","notifications","chat","admin","dropdown","xsMenu","app","login","logo","toolbarAdds","userProfil"],
                "subButon" => ["xsMenu.buttonList","app.buttonList","dropdown.icon","xsMenu.icon"]
            ];



            $types = array(Organization::COLLECTION, Project::COLLECTION, Costum::COLLECTION, Event::COLLECTION);
            foreach ($types as $keyType => $type) {
                if ($type == Costum::COLLECTION){
                    $condition= array("css" => array('$exists' => true));
                    $ArrSet = "css";
                } else {
                    $condition = array("costum.css" => array('$exists' => true));
                    $ArrSet = "costum.css";
                }

                $element = PHDB::find($type, $condition);
                $output .= "<h1><span style='color:blue'>" . $type . "</span> COLLECTION</h1>";
                foreach ($element as $k => $v) {
                    $slug = $v["slug"];
                    //verifier si un costum appartien à un element
                    if ($type == Costum::COLLECTION) {
                        $typesElement = array(Organization::COLLECTION, Project::COLLECTION, Event::COLLECTION);
                        foreach ($typesElement as $kElts => $vElts) {
                            $findElement = PHDB::find($vElts, array("slug" => $slug));
                            if (!empty($findElement)) {
                                $DataCostum = Costum::init(null, null, null, $slug, null, null, null, null, "co2/components/CommunecterController.php 0.1");
                            }
                        }
                    } else {
                        $DataCostum = Costum::init(null, null, null, $slug, null, null, null, null, "co2/components/CommunecterController.php 0.1");
                    }


                    if (isset($DataCostum["css"])) {
                        $newCss = $DataCostum["css"];
                        if (isset($DataCostum["css"]["loader"])) {
                            //var_dump($DataCostum["css"]["loader"]);
                            foreach ($arrayProperty["ring"] as $kRing => $vRing) {
                                if (isset($DataCostum["css"]["loader"][$vRing])) {
                                    foreach ($arrayProperty["keyList"] as $k => $vList) {
                                        if (isset($DataCostum["css"]["loader"][$vRing][$vList]) && $DataCostum["css"]["loader"][$vRing][$vList] != "auto" && strpos($DataCostum["css"]["loader"][$vRing][$vList], "px") === false && strpos($DataCostum["css"]["loader"][$vRing][$vList], "%") === false) {
                                            $newCss["loader"][$vRing][$vList] = $DataCostum["css"]["loader"][$vRing][$vList] . 'px';
                                            //var_dump($vList.' : '.$newCss["loader"][$vRing][$vList]);
                                        }
                                    }
                                }
                            }
                        }

                        if (isset($DataCostum["css"]["menuTop"])) {
                            foreach ($arrayProperty["menuTopPosition"] as $kPosition => $vPosition) {
                                foreach ($arrayProperty["button"] as $kButton => $vButton) {
                                    if (isset($DataCostum["css"]["menuTop"][$vPosition][$vButton])) {
                                        // var_dump($vPosition); var_dump($vButton);
                                        foreach ($arrayProperty["keyList"] as $k => $vList) {
                                            if (isset($DataCostum["css"]["menuTop"][$vPosition][$vButton][$vList]) && $DataCostum["css"]["menuTop"][$vPosition][$vButton][$vList] !== "auto" && strpos($DataCostum["css"]["menuTop"][$vPosition][$vButton][$vList], "px") === false && strpos($DataCostum["css"]["menuTop"][$vPosition][$vButton][$vList], "%") === false) {
                                                $newCss["menuTop"][$vPosition][$vButton][$vList] = $DataCostum["css"]["menuTop"][$vPosition][$vButton][$vList] . 'px';
                                            }
                                        }
                                        foreach ($arrayProperty["subButon"] as $kSub => $vSub) {
                                            //verifier dans les proprieter buttonList et icone dans chaque bouton
                                            $btnSub = explode(".", $vSub);
                                            if (isset($btnSub[1]) && $btnSub[0] == $vButton) {
                                                $subProperty = $btnSub[1];
                                                foreach ($arrayProperty["keyList"] as $k => $vList) {
                                                    if (isset($DataCostum["css"]["menuTop"][$vPosition][$vButton][$subProperty][$vList]) && $DataCostum["css"]["menuTop"][$vPosition][$vButton][$subProperty][$vList] !== "auto" && strpos($DataCostum["css"]["menuTop"][$vPosition][$vButton][$subProperty][$vList], "px") === false && strpos($DataCostum["css"]["menuTop"][$vPosition][$vButton][$subProperty][$vList], "%") === false) {
                                                        //var_dump($vList);
                                                        $newCss["menuTop"][$vPosition][$vButton][$subProperty][$vList] = $DataCostum["css"]["menuTop"][$vPosition][$vButton][$subProperty][$vList] . 'px';
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        if (isset($DataCostum["css"]["menuLeft"])) {

                            foreach ($arrayProperty["button"] as $kButton => $vButton) {
                                if (isset($DataCostum["css"]["menuLeft"][$vButton])) {
                                    // var_dump($vPosition); var_dump($vButton);
                                    foreach ($arrayProperty["keyList"] as $k => $vList) {
                                        if (isset($DataCostum["css"]["menuLeft"][$vButton][$vList]) && $DataCostum["css"]["menuLeft"][$vButton][$vList] !== "auto" && strpos($DataCostum["css"]["menuLeft"][$vButton][$vList], "px") === false && strpos($DataCostum["css"]["menuLeft"][$vButton][$vList], "%") === false) {
                                            $newCss["menuLeft"][$vButton][$vList] = $DataCostum["css"]["menuLeft"][$vButton][$vList] . 'px';
                                        }
                                    }
                                    foreach ($arrayProperty["subButon"] as $kSub => $vSub) {
                                        //verifier dans les proprieter buttonList et icone dans chaque bouton
                                        $btnSub = explode(".", $vSub);
                                        if (isset($btnSub[1]) && $btnSub[0] == $vButton) {
                                            $subProperty = $btnSub[1];
                                            foreach ($arrayProperty["keyList"] as $k => $vList) {
                                                //var_dump(isset($DataCostum["css"]["menuLeft"][$vButton][$subProperty][$vList]));
                                                if (isset($DataCostum["css"]["menuLeft"][$vButton][$subProperty][$vList]) && $DataCostum["css"]["menuLeft"][$vButton][$subProperty][$vList] !== "auto" && strpos($DataCostum["css"]["menuLeft"][$vButton][$subProperty][$vList], "px") === false && strpos($DataCostum["css"]["menuLeft"][$vButton][$subProperty][$vList], "%") === false) {
                                                    $newCss["menuLeft"][$vButton][$subProperty][$vList] = $DataCostum["css"]["menuLeft"][$vButton][$subProperty][$vList] . 'px';
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        
                        if (isset($newCss) && !empty($newCss) && $newCss !== $DataCostum["css"]) {
                            $output .= "<h3>" . $slug . "</h3>";
                            PHDB::update($type, array("slug" => $slug),
                                array(
                                    '$set' =>array($ArrSet=>$newCss)
                                )
                            );
                        }
                    }
                }
            }

        }else{
            $output .="<h1>Are Talking To Me</h1><iframe src='https://giphy.com/embed/VW66IuyWkTdT7neytT' width='480' height='360' frameBorder='0' allowFullScreen></iframe>";
        }
        return $output;
    }

    public function actionMigrateOneVideo() {
        $output="";
        if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
            $searchVideo = PHDB::find(Cms::COLLECTION, array("path"=>"tpls.blockCms.video.oneVideo"));
            $output .= "<h1>".count($searchVideo)." blocs qui vont rentrer dans la moulaga</h1>";
            $i = 0;

            foreach($searchVideo as $keyBloc => $vBloc){
                $arrayParams = [];
                if (isset($vBloc["path"])) {
                    $arrayParams["path"] = "tpls.blockCms.superCms.elements.video";
                }
                if (isset($vBloc["linkVideo"])) {
                    $arrayParams["link"] = $vBloc["linkVideo"];
                }
                if (isset($vBloc["videoHeight"])) {
                    $arrayParams["css"]["height"] = $vBloc["videoHeight"];
                }
                if (isset($vBloc["videoWidth"])) {
                    $arrayParams["css"]["width"] = $vBloc["videoWidth"];
                }
                if (isset($vBloc["autoplay"]) && ($vBloc["autoplay"] == "true" || $vBloc["autoplay"] == "false")) {
                    $arrayParams["autoplay"] = filter_var($vBloc["autoplay"], FILTER_VALIDATE_BOOLEAN);
                }
                if (isset($vBloc["repeat"]) && ($vBloc["repeat"] == "true" || $vBloc["repeat"] == "false")) {
                    $arrayParams["repeat"] = filter_var($vBloc["repeat"], FILTER_VALIDATE_BOOLEAN);
                }

                if (isset($arrayParams) && !empty($arrayParams) &&  $arrayParams != $vBloc) {
                    PHDB::update(Cms::COLLECTION,array( "_id" => new MongoId($keyBloc) ),
                        array (
                        '$set'=> $arrayParams,'$unset' => array("linkVideo"=>"","videoWidth"=>"","videoHeight"=>"")
                        ));
                    $i++;
                }
            }

            $output .= "<h1 style='color: blue'> => ".$i." blocs à jour </h1>";

        }else{
            $output .="<h1>Are Talking To Me</h1><iframe src='https://giphy.com/embed/VW66IuyWkTdT7neytT' width='480' height='360' frameBorder='0' allowFullScreen></iframe>";
        }
        return $output;
    }
    public function actionMigrateTargetAnchorToAnchorTarget() {
        $output="";
        if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
            $searchSection = PHDB::find(Cms::COLLECTION, array("targetAnchor"=>array('$exists'=>true)));
            $output .= "<h1>".count($searchSection)." blocs qui vont rentrer dans la moulage</h1>";
            $i = 0;

            foreach($searchSection as $keyBloc => $vBloc){
                $arrayParams = [];
                if (isset($vBloc["targetAnchor"])) {
                    $arrayParams["anchorTarget"] = $vBloc["targetAnchor"];
                }

                if (isset($arrayParams) && !empty($arrayParams) &&  $arrayParams != $vBloc) {
                    PHDB::update(Cms::COLLECTION,array( "_id" => new MongoId($keyBloc) ),
                        array (
                            '$set'=> $arrayParams,'$unset' => array("targetAnchor"=>"")
                        ));
                    $i++;
                }
            }

            $output .= "<h1 style='color: blue'> => ".$i." blocs à jour </h1>";

        }else{
            $output .="<h1>Are Talking To Me</h1><iframe src='https://giphy.com/embed/VW66IuyWkTdT7neytT' width='480' height='360' frameBorder='0' allowFullScreen></iframe>";
        }
        return $output;
    }

    public function actionUpLanguageCms($index=0, $limit=50){
        $output="";
        if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )) {
            ini_set('memory_limit', '-1');
            ini_set('max_execution_time', 1000);
            $searchBlock = PHDB::findAndLimitAndIndex(Cms::COLLECTION,
                array('$or'=>
                    array(
                        array("path"=>"tpls.blockCms.superCms.elements.title","textUpdated" => array('$exists' => false)),
                        array("path"=>"tpls.blockCms.superCms.elements.supertext","textUpdated" => array('$exists' => false)),
                        array("path"=>"tpls.blockCms.superCms.elements.button","textUpdated" => array('$exists' => false)),
                        array("path"=>"tpls.blockCms.superCms.elements.buttonOpenModal","textUpdated" => array('$exists' => false)),
                        array("path"=>"tpls.blockCms.superCms.elements.video","textUpdated" => array('$exists' => false)),
                        array("path"=>"tpls.blockCms.superCms.elements.image","textUpdated" => array('$exists' => false)),
                    )
                ),
                $limit,
                $index
            );

            $output .= "<h1>".count($searchBlock)." blocs qui vont rentrer dans la moulaga</h1>";
            $i = 0;
            //var_dump($searchBlock);

			

            foreach($searchBlock as $keyBloc => $vBloc){
                $arrayParams = [];
				$costumSpecificLanguage = [
					"ries" => "it",
				];
				$lang = $vBloc["source"]["key"] ?? "";
				
				if ($lang != ""){
					$langToUse = "fr";
					if (isset($costumSpecificLanguage[$lang]) ){
						$langToUse = $costumSpecificLanguage[$lang];
					}
					
					if (isset($vBloc["text"])) {
						$arrayParams["text"][$langToUse] = $vBloc["text"];
					}
					if (isset($vBloc["title"])) {
						$arrayParams["title"][$langToUse] = $vBloc["title"];
					}
					if (isset($vBloc["link"]) && $vBloc["path"] == "tpls.blockCms.superCms.elements.video") {
						$arrayParams["link"][$langToUse] = $vBloc["link"];
					}
					if (isset($vBloc["image"])) {
						$arrayParams["image"][$langToUse] = $vBloc["image"];
					}
					
					$arrayParams["textUpdated"] = true;
				}


				if (isset($arrayParams) && !empty($arrayParams) &&  $arrayParams != $vBloc) {
					PHDB::update(Cms::COLLECTION,array( "_id" => new MongoId($keyBloc) ),
						array (
							'$set'=> $arrayParams
						));
					$i++;
				}
				
            }
            $output .= "<h1 style='color: blue'> => ".$i." blocs à jour </h1>";
            
        }else{
             $output .="<h1>Are Talking To Me</h1><iframe src='https://giphy.com/embed/VW66IuyWkTdT7neytT' width='480' height='360' frameBorder='0' allowFullScreen></iframe>";
        }
        return $output;
    }

	/*public function actionUpLanguageForCostumPage() {
		$output = "";
		$collections = array(Organization::COLLECTION, Project::COLLECTION, Event::COLLECTION);
		if(Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]))) {
			$listCostumWithPathCostumApp = [];
			$newStructurePages = [];
			$costumSpecific = [
				"ries" => "it"
			];
			foreach ($collections as $key => $collection) {
				$listCostumWithPathCostumApp = array_merge(PHDB::find($collection, array('costum' => array('$exists' => true), "costum.app" => array('$exists' => true))), $listCostumWithPathCostumApp);
			}
			$output .= "<h1>".count($listCostumWithPathCostumApp)." Costum.app => Costum.pages dans collections (organizations, events, projects) qui vont être mise à jour</h1>";
			$i = 0;
			if (!empty($listCostumWithPathCostumApp)) {
				foreach($listCostumWithPathCostumApp as $keyCostum => $valCostum) {
					if (isset($valCostum["costum"])) {
						$page = [];
						$language = "fr";
						if (isset($valCostum["slug"]) && isset($costumSpecific[$valCostum["slug"]])) {
							$language = $costumSpecific[$valCostum["slug"]];
						}
						if (isset($valCostum["costum"]["app"])) {
							foreach($valCostum["costum"]["app"] as $keyPage => $valPage) {
								$page[$keyPage] = $valPage;
								if(isset($valPage["subdomainName"])){
									$page[$keyPage]["name"] = [];
									$page[$keyPage]["name"][$language] = $valPage["subdomainName"];
									unset($page[$keyPage]["subdomainName"]);
								}
							}
						}
						PHDB::update($valCostum["collection"], array("_id" => new MongoId($valCostum["_id"])), array(
							'$set' => array("costum.pages" => $page),
							'$unset' => array("costum.app" => true)
						));
					}
					$i++;
				}
			}
			$output .= "<h1 style='color: blue'> => ".$i." Costum.app => Costum.pages à jour </h1>";
			$output .= "============================================================";
			$listCostumWithPathApp = [];
			$listCostumWithPathApp = array_merge(PHDB::find(Costum::COLLECTION, array('app' => array('$exists' => true))), $listCostumWithPathApp);
			if (!empty($listCostumWithPathApp)) {
				$output .= "<h1>".count($listCostumWithPathApp)." app => pages dans collections costums qui vont être mise à jour</h1>";
				$ic = 0;
				foreach($listCostumWithPathApp as $keyPathApp => $valPathApp) {
					if (isset($valPathApp["app"])) {
						$page = [];
						$language = "fr";
						if (isset($valPathApp["slug"]) && isset($costumSpecific[$valPathApp["slug"]])) {
							$language = $costumSpecific[$valPathApp["slug"]];
						}
						foreach($valPathApp["app"] as $keyApp => $valApp) {
							$page[$keyApp] = $valApp;
							if(isset($valApp["subdomainName"])){
								$page[$keyApp]["name"] = [];
								$page[$keyApp]["name"][$language] = $valApp["subdomainName"];
								unset($page[$keyApp]["subdomainName"]);
							}
						}
						PHDB::update(Costum::COLLECTION, array("_id" => new MongoId($valPathApp["_id"])), array(
							'$set' => array("pages" => $page),
							'$unset' => array("app" => true)
						));
					}
					$ic++;
				}
				$output .= "<h1 style='color: blue'> => ".$ic." app => pages à jour </h1>";
			}
		} else {
			$output .="<h1>Are Talking To Me</h1><iframe src='https://giphy.com/embed/VW66IuyWkTdT7neytT' width='480' height='360' frameBorder='0' allowFullScreen></iframe>";
	   	}
		return $output;
	}*/

	public function actionUpLanguageForCostumPage() {
		$output = "";
		$collections = array(Organization::COLLECTION, Project::COLLECTION, Event::COLLECTION);
		if(Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]))) {
			$listCostumWithPathCostumApp = [];
			$newStructurePages = [];
			$costumSpecific = [
				"ries" => "it"
			];
			foreach ($collections as $key => $collection) {
				$listCostumWithPathCostumApp = array_merge(PHDB::find($collection, array('costum' => array('$exists' => true), "costum.app" => array('$exists' => true))), $listCostumWithPathCostumApp);
			}
			$output .= "<h1>".count($listCostumWithPathCostumApp)." Costum.app dans collections (organizations, events, projects) qui vont être mise à jour</h1>";
			$i = 0;
			if (!empty($listCostumWithPathCostumApp)) {
				foreach($listCostumWithPathCostumApp as $keyCostum => $valCostum) {
					if (isset($valCostum["costum"])) {
						$page = [];
						$language = "fr";
						if (isset($valCostum["slug"]) && isset($costumSpecific[$valCostum["slug"]])) {
							$language = $costumSpecific[$valCostum["slug"]];
						}
						if (isset($valCostum["costum"]["app"])) {
							foreach($valCostum["costum"]["app"] as $keyPage => $valPage) {
								$page[$keyPage] = $valPage;
								if(isset($valPage["subdomainName"])){
									$page[$keyPage]["name"] = [];
									$page[$keyPage]["name"][$language] = $valPage["subdomainName"];
									unset($page[$keyPage]["subdomainName"]);
								}
							}
						}
						PHDB::update($valCostum["collection"], array("_id" => new MongoId($valCostum["_id"])), array(
							'$set' => array("costum.app" => $page)
						));
					}
					$i++;
				}
			}
			$output .= "<h1 style='color: blue'> => ".$i." Costum.app à jour </h1>";
			$output .= "============================================================";
			$listCostumWithPathApp = [];
			$listCostumWithPathApp = array_merge(PHDB::find(Costum::COLLECTION, array('app' => array('$exists' => true))), $listCostumWithPathApp);
			if (!empty($listCostumWithPathApp)) {
				$output .= "<h1>".count($listCostumWithPathApp)." app dans collections costums qui vont être mise à jour</h1>";
				$ic = 0;
				foreach($listCostumWithPathApp as $keyPathApp => $valPathApp) {
					if (isset($valPathApp["app"])) {
						$page = [];
						$language = "fr";
						if (isset($valPathApp["slug"]) && isset($costumSpecific[$valPathApp["slug"]])) {
							$language = $costumSpecific[$valPathApp["slug"]];
						}
						foreach($valPathApp["app"] as $keyApp => $valApp) {
							$page[$keyApp] = $valApp;
							if(isset($valApp["subdomainName"])){
								$page[$keyApp]["name"] = [];
								$page[$keyApp]["name"][$language] = $valApp["subdomainName"];
								unset($page[$keyApp]["subdomainName"]);
							}
						}
						PHDB::update(Costum::COLLECTION, array("_id" => new MongoId($valPathApp["_id"])), array(
							'$set' => array("app" => $page)
						));
					}
					$ic++;
				}
				$output .= "<h1 style='color: blue'> => ".$ic." app à jour </h1>";
			}
		} else {
			$output .="<h1>Are Talking To Me</h1><iframe src='https://giphy.com/embed/VW66IuyWkTdT7neytT' width='480' height='360' frameBorder='0' allowFullScreen></iframe>";
	   	}
		return $output;
	}

    public function actionUpLanguageForTemplateApp() {
        $output = "";
        $collections = array("templates", Project::COLLECTION, Event::COLLECTION);
        if(Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]))) {
            $template = [];
            $template = PHDB::find("templates", array("siteParams.app" => array('$exists' => true)));

            $output .= "<h1>".count($template)." Templates qui a de APP qui vont être mise à jour</h1>";
            $i = 0;
            if (!empty($template)) {
                foreach($template as $keyTpl => $valTemplate) {
                    if (isset($valTemplate["siteParams"])) {
                        $page = [];
                        $language = "fr";
                        if (isset($valTemplate["slug"]) && isset($costumSpecific[$valTemplate["slug"]])) {
                            $language = $costumSpecific[$valTemplate["slug"]];
                        }
                        if (isset($valTemplate["siteParams"]["app"])) {
                            foreach($valTemplate["siteParams"]["app"] as $keyPage => $valPage) {
                                $page[$keyPage] = $valPage;
                                if(isset($valPage["subdomainName"])){
                                    $page[$keyPage]["name"] = [];
                                    $page[$keyPage]["name"][$language] = $valPage["subdomainName"];
                                    unset($page[$keyPage]["subdomainName"]);
                                }
                            }
                        }
                        PHDB::update("templates", array("_id" => new MongoId($valTemplate["_id"])), array(
                            '$set' => array("siteParams.app" => $page)
                        ));
                    }
                    $i++;
                }
            }
            $output .= "<h1 style='color: blue'> => ".$i." Templates à jour </h1>";
            $output .= "============================================================";            
        } else {
            $output .="<h1>Are Talking To Me</h1><iframe src='https://giphy.com/embed/VW66IuyWkTdT7neytT' width='480' height='360' frameBorder='0' allowFullScreen></iframe>";
        }
        return $output;
    }

	public function actionUpInitLanguageForCostum() {
        $output="";
		$collections = array(Organization::COLLECTION, Project::COLLECTION, Event::COLLECTION);
        if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )) {
			$listCostum = [];
			$costumLanguageSpecific = [
				"ries" => "it"
			];
			foreach($collections as $key => $collection) {
				$listCostum = array_merge(PHDB::find($collection, array("costum" => array('$exists' => true), "costum.language" => array('$exists' => false))), $listCostum);
			}
			$output .= "<h1>".count($listCostum)." costums qui vont être mise à jour</h1>";
			$i = 0;
			foreach($listCostum as $keyCostum => $vCostum) {
				if (isset($vCostum["costum"])) {
					$language = "fr";
					$setPath = "costum.language";
					if (isset($vCostum["slug"]) && isset($costumLanguageSpecific[$vCostum["slug"]])) {
						$language = $costumLanguageSpecific[$vCostum["slug"]];
					}
					PHDB::update($vCostum["collection"], array("_id" => new MongoId($vCostum["_id"])), array('$set' => array($setPath => $language)));
					$i++;
				}
			}
			$output .= "<h1 style='color: blue'> => ".$i." costums à jour </h1>";
        } else {
             $output .="<h1>Are Talking To Me</h1><iframe src='https://giphy.com/embed/VW66IuyWkTdT7neytT' width='480' height='360' frameBorder='0' allowFullScreen></iframe>";
        }
        return $output;
    }

	private function transformButtonListApp(&$appValue, &$i, $slug, &$costums) {
		foreach ($appValue as $key => &$value) {
			if (strpos($key, '#') === 0) {
				if (gettype($value) == "array") {
					if (isset($value['buttonList'])) {
						$this->transformButtonListApp($value['buttonList'], $i, $slug, $costums);
						$appValue[$key] = ['buttonList' => $value['buttonList']];
					} else {
						$appValue[$key] = true;
						if (!in_array($slug, $costums)) {
							$costums[] = $slug;
						}
						$i++;
					}
				} 
			}
		}
	}

	public function actionUpChangeHtmlConstructMenu() {
		$output = "";
		$collections = array(Organization::COLLECTION, Project::COLLECTION, Event::COLLECTION);
		$pathToCheck = ["menuLeft", "menuRight", "menuBottom", "subMenu","menuTop"];
		$menuTopPosition = ["left","center","right"];
		if (Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]))) {
			$costumWithPathCostumHtmlConstruct = [];
			foreach ($collections as $key => $collection) {
				$costumWithPathCostumHtmlConstruct = array_merge(PHDB::find($collection, array('costum' => array('$exists' => true), "costum.htmlConstruct" => array('$exists' => true))), $costumWithPathCostumHtmlConstruct);
			}
			if (!empty($costumWithPathCostumHtmlConstruct)) {
				$i = 0;
				$costumPathCostumHtmlConstructChanged = [];
				foreach($costumWithPathCostumHtmlConstruct as $keyCostum => $valCostum) {
					if (isset($valCostum["costum"]) && isset ($valCostum["costum"]["htmlConstruct"])) {
						$htmlConstruct = $valCostum["costum"]["htmlConstruct"];
						foreach($pathToCheck as $keypathtocheck => $valuepathtocheck) {
							if ($keypathtocheck != "menuTop" && isset($htmlConstruct[$valuepathtocheck]) && isset($htmlConstruct[$valuepathtocheck]["buttonList"]) && isset($htmlConstruct[$valuepathtocheck]["buttonList"]["app"])) {
								$appValue = $htmlConstruct[$valuepathtocheck]["buttonList"]["app"];
								if (isset($appValue["buttonList"])) {
									$this->transformButtonListApp($appValue["buttonList"], $i, $valCostum["costum"]["slug"], $costumPathCostumHtmlConstructChanged);
									$htmlConstruct[$valuepathtocheck]["buttonList"]["app"]["buttonList"] = $appValue["buttonList"];
								}
							} else {
								if (isset($htmlConstruct["header"]) && isset($htmlConstruct["header"]["menuTop"])) {
									$htmlConstructMenuTop = $htmlConstruct["header"]["menuTop"];
									foreach($menuTopPosition as $keyMenuTopPosition => $valueMenuTopPosition) {
										if (isset($htmlConstructMenuTop[$valueMenuTopPosition]) && isset($htmlConstructMenuTop[$valueMenuTopPosition]["buttonList"]) && isset($htmlConstructMenuTop[$valueMenuTopPosition]["buttonList"]["app"])) {
											$appValue = $htmlConstructMenuTop[$valueMenuTopPosition]["buttonList"]["app"];
											if (isset($appValue["buttonList"])) {
												$this->transformButtonListApp($appValue["buttonList"], $i, $valCostum["costum"]["slug"], $costumPathCostumHtmlConstructChanged);
												$htmlConstruct["header"]["menuTop"][$valueMenuTopPosition]["buttonList"]["app"]["buttonList"] = $appValue["buttonList"];
											}
										}
									}
								}
							}
						}
						PHDB::update($valCostum["collection"], array("_id" => new MongoId($valCostum["_id"])), array(
							'$set' => array("costum.htmlConstruct" => $htmlConstruct)
						));
					}
				}
			}
			if (!empty($costumPathCostumHtmlConstructChanged)) {
				$output .= "<h1>Slug du costum qui sont changé : </h1>";
				foreach ($costumPathCostumHtmlConstructChanged as $key => $value) {
					$output .= "<strong>- $value</strong><br>";
				}
			}
			$output .= "<h1 style='color: blue'> => ".$i." Costum.htmlConstruct à jour </h1>";
			$costumWithPathHtmlConstruct = [];
			$costumWithPathHtmlConstruct = array_merge(PHDB::find(Costum::COLLECTION, array('htmlConstruct' => array('$exists' => true))), $costumWithPathHtmlConstruct);
			if (!empty($costumWithPathHtmlConstruct)) {
				$ic = 0;
				$costumPathHtmlConstructChanged = [];
				foreach($costumWithPathHtmlConstruct as $keyCo => $valCo) {
					if (isset($valCo["htmlConstruct"])) {
						$htmlConstruct = $valCo["htmlConstruct"];
						foreach($pathToCheck as $keypathtocheck => $valuepathtocheck) {
							if ($keypathtocheck != "menuTop" && isset($htmlConstruct[$valuepathtocheck]) && isset($htmlConstruct[$valuepathtocheck]["buttonList"]) && isset($htmlConstruct[$valuepathtocheck]["buttonList"]["app"])) {
								$appValue = $htmlConstruct[$valuepathtocheck]["buttonList"]["app"];
								if (isset($appValue["buttonList"])) {
									$this->transformButtonListApp($appValue["buttonList"], $ic, $valCo["slug"], $costumPathHtmlConstructChanged);
									$htmlConstruct[$valuepathtocheck]["buttonList"]["app"]["buttonList"] = $appValue["buttonList"];
								}
							} else {
								if (isset($htmlConstruct["header"]) && isset($htmlConstruct["header"]["menuTop"])) {
									$htmlConstructMenuTop = $htmlConstruct["header"]["menuTop"];
									foreach($menuTopPosition as $keyMenuTopPosition => $valueMenuTopPosition) {
										if (isset($htmlConstructMenuTop[$valueMenuTopPosition]) && isset($htmlConstructMenuTop[$valueMenuTopPosition]["buttonList"]) && isset($htmlConstructMenuTop[$valueMenuTopPosition]["buttonList"]["app"])) {
											$appValue = $htmlConstructMenuTop[$valueMenuTopPosition]["buttonList"]["app"];
											if (isset($appValue["buttonList"])) {
												$this->transformButtonListApp($appValue["buttonList"], $ic, $valCo["slug"], $costumPathHtmlConstructChanged);
												$htmlConstruct["header"]["menuTop"][$valueMenuTopPosition]["buttonList"]["app"]["buttonList"] = $appValue["buttonList"];
											}
										}
									}
								}
							}
						}
						PHDB::update(Costum::COLLECTION, array("_id" => new MongoId($valCo["_id"])), array(
							'$set' => array("htmlConstruct" => $htmlConstruct)
						));
					}
				}
				if (!empty($costumPathHtmlConstructChanged)) {
					$output .= "<h1>Slug du costum qui sont changé : </h1>";
					foreach ($costumPathHtmlConstructChanged as $key => $value) {
						$output .= "<strong>- $value</strong><br>";
					}
				}
				$output .= "<h1 style='color: blue'> => ".$ic." htmlConstruct à jour </h1>";
			}
		} else {
			$output .="<h1>Are Talking To Me</h1><iframe src='https://giphy.com/embed/VW66IuyWkTdT7neytT' width='480' height='360' frameBorder='0' allowFullScreen></iframe>";
	   	}
	   	return $output;

	}

    public function actionMigrateBtnOpenModalToButton(){
        $output="";
        if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )) {

            $searchBlock = PHDB::findAndLimitAndIndex(Cms::COLLECTION,array("path"=>"tpls.blockCms.superCms.elements.buttonOpenModal"));

            $output .= "<h1>".count($searchBlock)." blocs qui vont rentrer dans la moulaga</h1>";
            $i = 0;
            
            foreach($searchBlock as $keyBloc => $vBloc){
                $arrayParams = [];
                $arrayParams["path"] = "tpls.blockCms.superCms.elements.button";

                if (isset($arrayParams) && !empty($arrayParams) &&  $arrayParams != $vBloc) {
                    PHDB::update(Cms::COLLECTION,array( "_id" => new MongoId($keyBloc) ),
                        array (
                            '$set'=> $arrayParams
                        ));
                    $i++;
                }
                
            }
            $output .= "<h1 style='color: blue'> => ".$i." blocs à jour </h1>";
            
        }else{
             $output .="<h1>Are Talking To Me</h1><iframe src='https://giphy.com/embed/VW66IuyWkTdT7neytT' width='480' height='360' frameBorder='0' allowFullScreen></iframe>";
        }
        return $output;
    }

	public function actionMigrateTextToSupertext(){
        $output="";
        if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )) {

            $searchBlock = PHDB::findAndLimitAndIndex(Cms::COLLECTION,array("path"=>"tpls.blockCms.superCms.elements.text"));

            $output .= "<h1>".count($searchBlock)." blocs qui vont rentrer dans la moulaga</h1>";
            $i = 0;
			
            foreach($searchBlock as $keyBloc => $vBloc){
                $arrayParams = [];
				$arrayParams["path"] = "tpls.blockCms.superCms.elements.supertext";

				if (isset($arrayParams) && !empty($arrayParams) &&  $arrayParams != $vBloc) {
					PHDB::update(Cms::COLLECTION,array( "_id" => new MongoId($keyBloc) ),
						array (
							'$set'=> $arrayParams
						));
					$i++;
				}
				
            }
            $output .= "<h1 style='color: blue'> => ".$i." blocs à jour </h1>";
            
        }else{
             $output .="<h1>Are Talking To Me</h1><iframe src='https://giphy.com/embed/VW66IuyWkTdT7neytT' width='480' height='360' frameBorder='0' allowFullScreen></iframe>";
        }
        return $output;
    }

	public function actionUpdateWidthForNewImage(){
        $output="";
        if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )) {
			
            $searchBlock = PHDB::findAndLimitAndIndex(Cms::COLLECTION,array("path"=>"tpls.blockCms.superCms.elements.image"));

            $output .= "<h1>".count($searchBlock)." blocs images qui vont rentrer dans la moulaga</h1>";
            $i = 0;
			
            foreach($searchBlock as $keyBloc => $vBloc){
                $arrayParams = [];

				if ( !(isset($vBloc['css']['width'])) || $vBloc['css']['width'] == "auto"){
					$arrayParams['css']['width'] = "100%";
				}

				if (isset($arrayParams) && !empty($arrayParams) &&  $arrayParams != $vBloc) {
					PHDB::update(Cms::COLLECTION,array( "_id" => new MongoId($keyBloc) ),
						array (
							'$set'=> $arrayParams
						));
					$i++;
				}
				
            }
            $output .= "<h1 style='color: blue'> => ".$i." blocs image à jour </h1>";
            
        }else{
             $output .="<h1>Are Talking To Me</h1><iframe src='https://giphy.com/embed/VW66IuyWkTdT7neytT' width='480' height='360' frameBorder='0' allowFullScreen></iframe>";
        }
        return $output;
    }

    public function actionUpCssMapCostum(){
        $output="";
        if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )) {
            $arrayBtnList = [
                "button" => ["networkFloop","notifications","chat","admin","dropdown.icon","xsMenu.icon","app.buttonList"],
                "app" => ["app"],
                "app.button" => ["app.buttonList"],
                "app.button.design" => ["app.buttonList"],
                "xsMenu" => ["xsMenu"],
                "xsMenu.button" => ["xsMenu.buttonList"],
                "xsMenu.button.design" => ["xsMenu.buttonList"],
                "userProfil" => ["userProfil"],
                "connectBtn" => ["login"],
                "badge" => ["chat.badge","notifications.badge"],
                "add" => ["add"],
                "logo" => ["logo"],
                "toolbarAdds" => ["toolbarAdds"],
                "toDelete" => ["scopeBtn","filterBtn","menuApp"]
            ];

            //var_dump($arrayBtnList);
            $types = array(Organization::COLLECTION, Project::COLLECTION, Costum::COLLECTION);
            foreach ($types as $keyType => $type) {


                if ($type == Costum::COLLECTION){
                    $cond= array("htmlConstruct.menuBottom.add" => array('$exists' => true));
                    $condition= array("css" => array('$exists' => true));
                    $ArrSet = "css";
                } else {
                    $cond= array("costum.htmlConstruct.menuBottom.add" => array('$exists' => true));
                    $condition= array("costum.css" => array('$exists' => true));
                    $ArrSet = "costum.css";
                }
                $elts = PHDB::find($type, $cond);
                $output .= "<h1>" . count($elts) . " <span style='color:blue'>" . $type . "</span> Deleted htmlConstruct.menuBottom.add</h1>";
                foreach ($elts as $k => $v) {
                    $slug = @$v["slug"];
                    $output .=  "<h3>" . $slug . "</h3>";
                    if ($type == Costum::COLLECTION){
                        $returnHtmlConstruct = $v["htmlConstruct"];
                        unset($returnHtmlConstruct["menuBottom"]);
                        $setArr = "htmlConstruct";
                        //var_dump($returnHtmlConstruct);
                    } else {
                        //var_dump($v["costum"]["htmlConstruct"]);
                        $returnHtmlConstruct = $v["costum"]["htmlConstruct"];
                        unset($returnHtmlConstruct["menuBottom"]);
                        $setArr = "costum.htmlConstruct";
                        //var_dump($returnHtmlConstruct);
                    }

                    if (isset($returnHtmlConstruct) && !empty($returnHtmlConstruct)) {
                        PHDB::update($type, array("slug" => $slug),
                            array(
                                '$set' =>array($setArr=>$returnHtmlConstruct)
                            )
                        );
                    }
                }



                $element = PHDB::find($type, $condition);
                $output .= "<h1><span style='color:blue'>" . $type . "</span> update mapCss</h1>";
                foreach ($element as $k => $v) {
                    $slug = @$v["slug"];
                    //$output .= "<h3>" . $slug . "</h3>";
                    //verifier si un costum appartien à un element
                    if ($type == Costum::COLLECTION) {
                        $typesElement = array(Organization::COLLECTION, Project::COLLECTION, Event::COLLECTION);
                        foreach ($typesElement as $kElts => $vElts) {
                            $findElement = PHDB::find($vElts, array("slug" => $slug));
                            if (!empty($findElement)) {
                                $returnCostum = Costum::init(null, null, null, $slug, null, null, null, null, "co2/components/CommunecterController.php 0.1");
                            }
                        }
                    } else {
                        $returnCostum = Costum::init(null, null, null, $slug, null, null, null, null, "co2/components/CommunecterController.php 0.1");
                        //$output .= "<h3> " . $slug . "</h3>";
                    }

                    //var_dump($returnCostum);
                    if (isset($returnCostum["css"])) {
                        //$output .= "<h3><span style='color:blue'>" . $type . "</span> " . $slug . "</h3>";
                        $newCss = $returnCostum["css"];

                        if (isset($returnCostum["css"]["menuTop"])) {
                            foreach ($returnCostum["css"]["menuTop"] as $kBtn => $vBtn) {
                                if (isset($arrayBtnList[$kBtn])) {
                                    //var_dump($kBtn);
                                    foreach ($arrayBtnList[$kBtn] as $kCssProperties => $valCssProperties) {
                                        $positionType = ["left", "center", "right"];
                                        foreach ($positionType as $kPos => $position) {
                                            if (isset($returnCostum["htmlConstruct"]["header"]["menuTop"][$position]["buttonList"][$valCssProperties])) {
                                                //verification s'il existe dejà l'entré css menuTop position $valCssProperties
                                                if (isset($newCss["menuTop"][$position][$valCssProperties])) {
                                                    foreach ($newCss["menuTop"][$kBtn] as $k => $cssProperty) {
                                                        //s'il existe pas le propriete css on l'ajoute
                                                        if (!isset($newCss["menuTop"][$position][$valCssProperties][$k])) {
                                                            $newCss["menuTop"][$position][$valCssProperties][$k] = $cssProperty;
                                                        }
                                                    }
                                                    if (isset($arrayBtnList[$kBtn . ".button"]) && isset($newCss["menuTop"][$position][$valCssProperties]["buttonList"]) && isset($newCss["menuTop"][$kBtn]["button"])) {
                                                        foreach ($newCss["menuTop"][$kBtn]["button"] as $kCssBtnList => $valCssBtnList) {
                                                            if (!isset($newCss["menuTop"][$position][$valCssProperties]["buttonList"][$kCssBtnList])) {
                                                                $newCss["menuTop"][$position][$valCssProperties]["buttonList"][$kCssBtnList] = $valCssBtnList;
                                                            }
                                                        }
                                                        unset($newCss["menuTop"][$position][$valCssProperties]["button"]);
                                                    } elseif (isset($arrayBtnList[$kBtn . ".button"]) && isset($newCss["menuTop"][$kBtn]["button"]) && !isset($newCss["menuTop"][$position][$valCssProperties]["buttonList"])) {
                                                        $newCss["menuTop"][$position][$valCssProperties]["buttonList"] = $newCss["menuTop"][$kBtn]["button"];
                                                        unset($newCss["menuTop"][$position][$valCssProperties]["button"]);
                                                    }
                                                    //var_dump($newCss["menuTop"][$position][$valCssProperties]);
                                                } else {
                                                    //var_dump($valCssProperties);
                                                    $newCss["menuTop"][$position][$valCssProperties] = $newCss["menuTop"][$kBtn];
                                                    //Ajouter proprieté buttonList dans $kBtn
                                                    if (isset($arrayBtnList[$kBtn . ".button"]) && isset($newCss["menuTop"][$kBtn]["button"])) {
                                                        $newCss["menuTop"][$position][$valCssProperties]["buttonList"] = $newCss["menuTop"][$kBtn]["button"];
                                                        unset($newCss["menuTop"][$position][$valCssProperties]["button"]);
                                                    }
                                                }
                                                //deplacer tout les proprieté du design dans buttonList puis effacer design
                                                if (isset($arrayBtnList[$kBtn . ".button.design"]) && isset($newCss["menuTop"][$kBtn]["button"]["design"])) {
                                                    //var_dump($valCssProperties);
                                                    foreach ($newCss["menuTop"][$kBtn]["button"]["design"] as $kCssDesign => $vCssDesign) {
                                                        if (!isset($newCss["menuTop"][$kBtn]["button"][$kCssDesign])) {
                                                            $newCss["menuTop"][$position][$valCssProperties]["buttonList"][$kCssDesign] = $vCssDesign;
                                                        }
                                                    }
                                                    unset($newCss["menuTop"][$position][$valCssProperties]["buttonList"]["design"]);
                                                }

                                                //var_dump($newCss["menuTop"][$position][$valCssProperties]);
                                                //var_dump($newCss["menuTop"][$kBtn]);
                                            }
                                            // ajouter les badge dans les bouton cible
                                            $badgeBtn = explode(".", $valCssProperties);
                                            if (isset($badgeBtn[1])) {
                                                if (isset($returnCostum["htmlConstruct"]["header"]["menuTop"][$position]["buttonList"][$badgeBtn[0]]) && !isset($newCss["menuTop"][$position][$badgeBtn[0]][$badgeBtn[1]])) {
                                                    //var_dump($kBtn);
                                                    $newCss["menuTop"][$position][$badgeBtn[0]][$badgeBtn[1]] = $newCss["menuTop"][$kBtn];
                                                }
                                            }

                                        }

                                    }
                                    //Effacer menuTop[$vBtn]
                                    unset($newCss["menuTop"][$kBtn]);
                                }
                            }
                        }
                        //verifie s'il existe un css menuleft
                        if (isset($returnCostum["css"]["menuLeft"])) {
                            foreach ($returnCostum["css"]["menuLeft"] as $kBtn => $vBtn) {
                                if (isset($arrayBtnList[$kBtn])) {
                                    foreach ($arrayBtnList[$kBtn] as $kCssProperties => $valCssProperties) {
                                        if (isset($returnCostum["htmlConstruct"]["menuLeft"]["buttonList"][$valCssProperties])) {
                                            if (isset($newCss["menuLeft"][$valCssProperties])){
                                                foreach ($newCss["menuLeft"][$kBtn] as $k => $cssProperty) {
                                                    //s'il existe pas le propriete css on l'ajoute
                                                    if (!isset($newCss["menuLeft"][$valCssProperties][$k])) {
                                                        $newCss["menuLeft"][$valCssProperties][$k] = $cssProperty;
                                                    }
                                                }
                                                if (isset($arrayBtnList[$kBtn.".button"]) && isset($newCss["menuLeft"][$valCssProperties]["buttonList"]) && isset($newCss["menuLeft"][$kBtn]["button"])) {
                                                    foreach ($newCss["menuLeft"][$kBtn]["button"] as $kCssBtnList => $valCssBtnList) {
                                                        if (!isset($newCss["menuLeft"][$valCssProperties]["buttonList"][$kCssBtnList])) {
                                                            $newCss["menuLeft"][$valCssProperties]["buttonList"][$kCssBtnList] = $valCssBtnList;
                                                        }
                                                    }
                                                    unset($newCss["menuLeft"][$valCssProperties]["button"]);
                                                } elseif (isset($arrayBtnList[$kBtn.".button"]) && isset($newCss["menuLeft"][$kBtn]["button"]) && !isset($newCss["menuLeft"][$valCssProperties]["buttonList"])){
                                                    $newCss["menuLeft"][$valCssProperties]["buttonList"] = $newCss["menuLeft"][$kBtn]["button"];
                                                    unset($newCss["menuLeft"][$valCssProperties]["button"]);
                                                }
                                                //var_dump($newCss["menuLeft"][$valCssProperties]);
                                            }else {
                                                $newCss["menuLeft"][$valCssProperties] = $newCss["menuLeft"][$kBtn];
                                                //Ajouter proprieté buttonList dans $kBtn
                                                if (isset($arrayBtnList[$kBtn.".button"]) && isset($newCss["menuLeft"][$kBtn]["button"])){
                                                    $newCss["menuLeft"][$valCssProperties]["buttonList"] = $newCss["menuLeft"][$kBtn]["button"];
                                                    unset($newCss["menuLeft"][$valCssProperties]["button"]);
                                                }
                                            }
                                            //var_dump($valCssProperties);
                                            //deplacer tout les proprieté du design dans buttonList puis effacer design
                                            if (isset($arrayBtnList[$kBtn.".button.design"]) && isset($newCss["menuLeft"][$kBtn]["buttonList"]["design"])){
                                                //var_dump($valCssProperties);
                                                foreach ($newCss["menuLeft"][$kBtn]["buttonList"]["design"] as $kCssDesign => $vCssDesign) {
                                                    if (!isset($newCss["menuLeft"][$kBtn]["buttonList"][$kCssDesign])) {
                                                        $newCss["menuLeft"][$valCssProperties]["buttonList"][$kCssDesign] = $vCssDesign;
                                                    }
                                                }
                                                unset($newCss["menuLeft"][$valCssProperties]["buttonList"]["design"]);
                                            }
                                        }
                                        // ajouter les badge dans les bouton cible
                                        $badgeBtn = explode(".", $valCssProperties);
                                        if (isset($badgeBtn[1])){
                                            if (isset($returnCostum["htmlConstruct"]["menuLeft"]["buttonList"][$badgeBtn[0]]) && !isset($newCss["menuLeft"][$badgeBtn[0]][$badgeBtn[1]])) {
                                                //var_dump($kBtn);
                                                $newCss["menuLeft"][$badgeBtn[0]][$badgeBtn[1]] = $newCss["menuLeft"][$kBtn];
                                            }
                                        }
                                    }
                                    //Effacer menuLeft[$vBtn]
                                    //unset($newCss["menuLeft"][$kBtn]);
                                }
                            }

                        }
                        //verifie s'il existe un css menuBottom
                        if (isset($returnCostum["css"]["button"]) && isset($returnCostum["css"]["button"]["footer"])) {
                            foreach ($returnCostum["css"]["button"]["footer"] as $kBtn => $vBtn) {
                                if (isset($arrayBtnList[$kBtn])) {
                                    foreach ($arrayBtnList[$kBtn] as $kCssProperties => $valCssProperties) {
                                        if (isset($returnCostum["htmlConstruct"]["menuBottom"]["buttonList"][$valCssProperties])) {
                                            $newCss["menuBottom"][$valCssProperties] = $newCss["button"]["footer"][$kBtn];
                                        }
                                    }
                                }
                            }
                            //Effacer button
                            unset($newCss["button"]);
                        }

                        $menuType = ["menuTop","menuLeft"];
                        // btn  to delete in menuLeft
                        foreach ($arrayBtnList["toDelete"] as $keyBtnDelete => $valueBtnDelete) {
                            if (isset($newCss[$valueBtnDelete])) {
                                unset($newCss[$valueBtnDelete]);
                            }
                            foreach ($menuType as $keyMenu => $valueMenu) {
                                if (isset($newCss[$valueMenu][$valueBtnDelete])) {
                                    unset($newCss[$valueMenu][$valueBtnDelete]);
                                }
                            }
                        }


                        if (isset($newCss) && !empty($newCss) &&  $newCss != $returnCostum["css"]) {
                            $output .= "<h3>" . $slug . "</h3>";
                            PHDB::update($type, array("slug" => $slug),
                                array(
                                    '$set' =>array($ArrSet=>$newCss)
                                )
                            );
                        }
                    }
                }
            }

        }else{
            $output .="<h1>Are Talking To Me</h1><iframe src='https://giphy.com/embed/VW66IuyWkTdT7neytT' width='480' height='360' frameBorder='0' allowFullScreen></iframe>";
        }
        return $output;
    }

    public function checkIfCMSGotChild($cmsParent, $nb=0){
       // echo "opuioiii";
        foreach($cmsParent as $k => $v){
        //   echo $k;
          //  $nb++;
           // break;
            $arrayList=array();
            $childCms=PHDB::find(Cms::COLLECTION, 
                        array('$or'=>
                            array(
                                array("tplParent"=>$k),
                                array("cmsParent"=>$k)
                            )
                        ), 
                        array("_id", "cmsParent", "tplParent")); 
            if(!empty($childCms)){
                foreach($childCms as $id => $child){
                    
                  PHDB::update(Cms::COLLECTION, array("_id"=> $child["_id"]), 
                        array('$set'=> array("blockParent"=>$k), '$unset'=> array("tplParent"=>"", "cmsParent"=>"")));
                    array_push($arrayList, $id);
                }
                $nb+=self::checkIfCMSGotChild($childCms,$nb);
                PHDB::update(Cms::COLLECTION, array("_id"=> $v["_id"]), 
                            array('$set'=> array("cmsList"=>$arrayList)));

            }     
            $nb++;

        }
        return $nb;
    }

    public function actionAnswerWithGhostCter(){
        $output="";
        if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
            $answersCter=PHDB::find(Answer::COLLECTION, array("cterSlug"=>array('$exists'=>true), "source.key"=>"ctenat"), array("cterSlug", "context", "_id"));
            $output.="<h1>".count($answersCter)." réponses sont du ctenational</h1>";
            $c=0; $arrayIds=[];
            foreach($answersCter as $k => $v){
               
               // var_dump($answersCter);
                foreach($v["context"] as $cter=>$d){
                    if($d["type"] == Project::COLLECTION && !array_key_exists($cter, $arrayIds))
                        $arrayIds[$cter]= array("slug"=>$v["cterSlug"]);
                }
                $c++;
                //if($c==30) break;
            }
            $output .= "<h1>Les réponses sont réparties sur ".count($arrayIds)." dispositifs";
            $countOrph=0;
                
            foreach($arrayIds as $id=> $slug){
                $cter=PHDB::findOne(Project::COLLECTION,array("_id"=>new MongoId($id)), array("_id", "slug"));
                if(empty($cter)){
                    $output.= "<h3>Le dispositifs ".$slug["slug"]." ".$id." n'existe plus</h3>";
                    //$orphAnswer=array_key_exists($id, $answersCter);
                    $orphAnswer=PHDB::find(Answer::COLLECTION, array("context.".$id=>array('$exists'=>true), "source.key"=>"ctenat"), array("cterSlug", "context", "_id","answers"));
                    $output.= "<span>Ce dispositif comporte ".count($orphAnswer)." réponses</span>";
                    $output.="<ul>";
                    $countOrph+=count($orphAnswer);
                    foreach($orphAnswer as $k => $v){
                        $output.="<li>".$k." : ".@$v["answers"]["project"]["name"]."</li>";
                    }   
                    $output.="</ul>";
                }
            }
            $output.='<h1>Au total, '.$countOrph.' réponses sont des gosthers</h1>';
            return $output;
        }else
            return "the OulaOup: take auth bad robot";
    }
  	public function actionAddLevel5ToCities(){
    	if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){

    		// !!!!!!!!!!!! Gerer les villes à arrondissement !
    		// foreach

    		$epci=PHDB::find(Zone::COLLECTION, array("level"=>"5"));
    		
    		//var_dump($epci);exit;
    			
    		foreach ($epci as $k => $cc){

    			//var_dump($cc);exit;
    			foreach($cc["cities"] as $i => $cityId){
	    			PHDB::update(City::COLLECTION,array("_id"=>new MongoId($cityId)),array('$set'=>array("level5"=>$k,"level5Name"=>$cc["name"])));
	    			//var_dump($geo);exit;

	    		}	
	    	}		

    		
    		
    		// $cities=PHDB::find(City::COLLECTION, array("level3Name"=>"Hauts-de-France"));
    		// $count=0;
    		
    		// foreach ($cities as $key => $city){
    		// 	//var_dump($key);exit;
    		// 	$lat=$city["geo"]["latitude"];
    		// 	$lon=$city["geo"]["longitude"];
    		// 	$address=Nominatim::getCityByLatLon($lat, $lon);
    			
	    	// 	$textRegExp = (isset($address["address"]["municipality"])) ? SearchNew::accentToRegex($address["address"]["municipality"]) : null;
  				// $where = array( "name" => new MongoRegex("/.*{$textRegExp}.*/i"),"level"=>"5");
  				// try{
  				// 	$agglo=PHDB::findOne(Zone::COLLECTION,$where);
  				// 	//var_dump((string)$agglo["_id"]);exit;
  				// 	PHDB::update(City::COLLECTION,array("_id"=>new MongoId($key)), array('$set' => array("level5"=>(string)$agglo["_id"],"level5Name"=>$agglo["name"])));
  				// 	$count++;
  				// } catch (CTKException $e){
      //       		$str .=("Erreur de la mAJ de la ville" .$city["name"]);
						// die(); 
      //       	}	
  				
      //       	//exit;
	    		
	    	// }	
	    	// $str .= $count. " villes mAJ";
	    	



    	}	

    }		

    public function actionAddLevel5ToElementAddress(){
    	if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){

    		// !!!! Ajouter les dom-TOM ["RE","MQ","GP","GF","YT"]
    		$france=["FR"];
    		$org= PHDB::find(Organization::COLLECTION,array("address.localityId"=>array('$exists'=>true),"address.addressCountry"=>array('$in'=>$france)));

    		$count=0;
    		foreach($org as $key => $value){

    			$city=PHDB::findOne(City::COLLECTION,array("_id"=>new MongoId($value["address"]["localityId"])));
    			if(isset($city["level5"])) {
    				PHDB::update(Organization::COLLECTION,array("_id"=>new MongoId($key)),array('$set'=>array("address.level5"=>$city["level5"],"address.level5Name"=>$city["level5Name"])));
    			}
    			$count++;
    		}


    		// $cities=PHDB::find(City::COLLECTION, array('$and' => array(array("level5Name"=> array('$exists'=>true)), array("level5"=> array('$exists'=>true)))));
    		// //var_dump($cities);exit;
    		// $count=0;
    		// //var_dump($cities);exit;
    		// foreach ($cities as $key => $city){
    		// 	// var_dump($key);
    		// 	// var_dump($city["level5"]);
    		// 	// var_dump($city["level5Name"]); exit;
    		// 	$org= PHDB::find(Organization::COLLECTION,array("address.localityId"=>$key));
    		// 	//var_dump($org);exit;
    		// 	foreach($org as $k => $orga){
	    	// 		try{
	    	// 		PHDB::update(Organization::COLLECTION,array("address.localityId"=>$key), array('$set' => array("address.level5"=>$city["level5"],"address.level5Name"=>$city["level5Name"])));
	    	// 		$count++;	
	    	// 		}catch (CTKException $e){
	     //        		$str .=("Erreur de la mAJ de l'orga'" .$orga["slug"]);
						// die(); 
	     //        	}
	     //        }

	    	// }
	    	return $count. "orgas MAJ" ;
    	}	
    }	

    public function actionAddGeoPositionToLevel5(){
    	if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]))){	
    		$epci=PHDB::find(Zone::COLLECTION, array("level"=>"5"),array("cities"));
    		
    		//var_dump($epci);exit;
    			$count=0;
    			$lat=array();
    			$lon=array();
    		foreach ($epci as $key => $cc){

    			//var_dump($cc);exit;
    			foreach($cc["cities"] as $i => $cityId){
	    			$geo=PHDB::findOne(City::COLLECTION,array("_id"=>new MongoId($cityId)),array("geo"));
	    			//var_dump($geo);exit;

	    			array_push($lat,$geo["geo"]["latitude"]);
	    			array_push($lon,$geo["geo"]["longitude"]);
	    		}

	    		$epciLat=array_sum($lat)/count($lat);
	    		$epciLon=array_sum($lon)/count($lon);
	    		$params = array(
	    			"geo" => array(
	    				"@type" => "GeoCoordinates",
	    				"latitude" => (string)$epciLat ,
	    				"longitude" => (string)$epciLon
	    			),
	    			"geoPosition" => array(
	    				"type" => "Point",
	    				"float" => "true",
	    				"coordinates" => array(
	    					floatval($epciLat),
	    					floatval($epciLon)
	    				)
	    			)

	    		);
	    		PHDB::update(Zone::COLLECTION,array("_id"=>new MongoId($key)),array('$set'=>$params));
	    		$count++;	
	    	}
	    	return $count. " EPCI MAJ";
	    	
    	}	
    }	
  	public function actionNASourceAllElts(){
  		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
            $colElts = array(Person::COLLECTION, Organization::COLLECTION, Poi::COLLECTION);
            $res = array();
            foreach ($colElts as $keyCol => $col) {
            	$elts=PHDB::find($col, array("source" => array('$exists' => 0)), array("name"));
            	$res[$col] = 0;
            	foreach ($elts as $id => $elt) {
            		$source = array(
	                    'insertOrign' => "import",
	                    'date'=> new MongoDate(time()),
	                    "key" => "notragora",
	                    "keys" => array("notragora") );

	                PHDB::update($col,
	                    array("_id" => $elt["_id"]),
	                    array('$set' => array("source" => $source))
	                );

	                $res[$col]++;  
            	}
            }

            return Rest::json($res); 
        }
    } 
    public function actionAddOrientationsInActions(){
    	if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){		
    		$orientations=PHDB::find(Badge::COLLECTION, array("source.key"=>"ctenat", "category"=>"strategy"));
    		foreach($orientations as $k => $v){
    			if(isset($v["links"]) && isset($v["links"]["projects"]) && !empty($v["links"]["projects"])){
    				foreach($v["links"]["projects"] as $e => $data){
						$project=PHDB::findOne(Project::COLLECTION, array("_id"=>new MongoId($e)), array("links.orientations"));
						$linksOrientation=array("orientations"=>array());
						if(isset($project["links"]) && isset($project["links"]["orientations"])){
							$linksOrientation["orientations"]=$project["links"]["orientations"];
						}
						$linksOrientation["orientations"][$k]=array("type"=>"badges");
						PHDB::update(Project::COLLECTION,array("_id"=>new MongoId($e)), 
							array('$set'=>array("links.orientations"=>$linksOrientation["orientations"])));
						var_dump($e);
						var_dump($linksOrientation);
					}
    			}
    		}
    	}
    }

    public function actionAddPathCms(){ 
    	if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){		
    		$getAllDoc=PHDB::find(Cms::COLLECTION, array("source.key"=>"ctenat", "path" => array('$exists'=> false)));
		    foreach($getAllDoc as $k => $v){
		         PHDB::update(Cms::COLLECTION, 
		           array("_id" => $v["_id"]),
		           array('$set' => array("path" => "tpls.blockCms.text.cte_desc"))
		         );
		    };
		    $countcms = count($getAllDoc);
    		return "<span class='col-xs-12'>///////////////".$countcms."  CMS TRAITER EN PATH //////////////////<br/><br/></span>";

    	}
    }

    public function actionMovePoiCMSToCMS(){
    	if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){

            $output = '';
    		$getAll=PHDB::find(Poi::COLLECTION, array("type"=> "cms"));
    		$countPoi = count($getAll);
            $output .= "<span class='col-xs-12'>///////////////".$countPoi." POI TYPE CMS A TRAITER//////////////////<br/><br/></span>";
			$i=0;
    		foreach($getAll as $k => $v){
    			$i++;
    			$newCMS=$v;
    			$newCMS["collection"]=Cms::COLLECTION;
    			unset($newCMS["type"]);
    			if(isset($newCMS["costumSlug"])){
    				$newCMS["source"]=array("key"=>$newCMS["costumSlug"], "keys"=>[$newCMS["costumSlug"]], "insertOrigin"=>"costum");
    				unset($newCMS["costumSlug"]);
    				if(isset($newCMS["costumId"])) unset($newCMS["costumId"]);
    				if(isset($newCMS["costumType"])) unset($newCMS["costumType"]);
    			}
    			if(!isset($newCMS["source"]) && isset($newCMS["parent"])){
    				foreach ($newCMS["parent"] as $key => $value) {
    					$elt=Element::getElementById( $key, $value["type"],null, array("slug", "costum"));
    					break;
    					
    				}
    				if(isset($elt["costum"]) || !empty(Costum::getBySlug($elt["slug"])))
	    					$newCMS["source"]=array("key"=>$elt["slug"], "keys"=>[$elt["slug"]], "insertOrigin"=>"costum");
	    			
    			}

				Yii::app()->mongodb->selectCollection(Cms::COLLECTION)->insert($newCMS);
				PHDB::remove(Poi::COLLECTION, array("_id"=>new MongoId($k)));
    			//var_dump($newCMS);
                $output .= "<br/><br/>";
    			/*PHDB::update($col,
    				array("_id" => $v["_id"]) , 
					array('$set' => array("collection" => $col)));*/
    		}
            $output .= "<span class='col-xs-12'>".$i." traité</span>";
            return $output;
    	}
    }
	 
	public function actionUpdateToolChatIntEmpty(){
		// correction de tools.chat.int vide
        $output ='';
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
    	$types=array(Organization::COLLECTION,	Project::COLLECTION);
    	foreach($types as $col){
			$getChat=PHDB::find($col, 
			array("hasRC" => array('$exists' => true), 
			"slug" => array('$exists' => true), 
			"tools.chat.int" => array('$exists' => false)));

            $output .= "<span class='col-xs-12'>///////////////".$col."//////////////////<br/><br/>";
            $output .= count($getChat)." ".$col." mis à jour <br/><br/></span>";
    		foreach($getChat as $k => $v){
				if (isset($v["preferences"]["private"]) && ($v["preferences"]["private"] === true || $v["preferences"]["private"] === 'true')) {
					$path = '/group/'.$v["slug"];
				} else {
					$path = '/channel/'.$v["slug"];
				}
    			PHDB::update($col,
    				array("_id" => $v["_id"]) , 
					array('$set' => array("hasRC" => true), '$addToSet' => array("tools.chat.int" => array('name' => $v["slug"], 'url' => $path))));
    		}
		}
	}
        return $output;
	}
	
    public function actionAddCollectionInData(){
        $output ='';
      $types=array(Organization::COLLECTION, Badge::COLLECTION, Person::COLLECTION,Event::COLLECTION,Project::COLLECTION,News::COLLECTION,City::COLLECTION,Poi::COLLECTION,Proposal::COLLECTION, Resolution::COLLECTION, Action::COLLECTION,Classified::COLLECTION,Answer::COLLECTION, Document::COLLECTION, Comment::COLLECTION, Bookmark::COLLECTION, Folder::COLLECTION, Room::COLLECTION);
    	foreach($types as $col){
    		PHDB::updateWithOptions($col, 
    			array("collection"=>array('$exists' => false )),  
    			array('$set' => array("collection" => $col)),
                array('multiple' => true));
            $output .= "<span class='col-xs-12'>///////////////".$col." mis à jour//////////////////<br/><br/></span>";
    		/*$str .= count($getAll)." ".$col."mis à jour <br/><br/></span>";
    		foreach($getAll as $k => $v){
    			PHDB::update($col,
    				array("_id" => $v["_id"]) , 
					array('$set' => array("collection" => $col)));
    		}*/
    	}
        return $output;
    }
    public function actionRemoveTitleForNameResAndProp(){
        $output ='';
    	$types=array(Proposal::COLLECTION,Resolution::COLLECTION);
    	foreach($types as $col){
    		$getAll=PHDB::find($col, array("name"=>array('$exists'=>false),"title"=>array('$exists'=>true)));
            $output .= "<span class='col-xs-12'>///////////////".$col."//////////////////<br/><br/>";
            $output .= count($getAll)." ".$col."mis à jour <br/><br/></span>";
    		foreach($getAll as $k => $v){
    			$up=array('$set' => array("name" => $v["title"]),'$unset'=>array("title"=>""));
    			PHDB::update($col,
    				array("_id" => $v["_id"]) , 
    				$up);
    		}
    	}
        return $output;
    }
     public function actionReplaceEmailCollectifPacte(){
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
			$group=PHDB::find(Organization::COLLECTION, array("source.key"=>"siteDuPactePourLaTransition"));
			$countIsUp=0;
			foreach($group as $k => $v){
				$set=array();
				foreach ($v["scope"] as $key => $value) {
                    $postalCodeForEmail=$value["postalCode"];
                    break;
               	}
               	if(!empty($v["email"]) && strrpos($v["email"], "@listes.transition-citoyenne.org") == false )
               		$set["publicMail"]=$v["email"];
				if(empty($v["email"]) || strrpos($v["email"], "@listes.transition-citoyenne.org") == false )
                	$set["email"] = "pacte-".mb_strtolower($postalCodeForEmail)."@listes.transition-citoyenne.org";  
               	if(!empty($set)){
	                PHDB::update(Organization::COLLECTION,
	    				array("_id" => $v["_id"]) , 
	    				array('$set'=>$set));
	                $countIsUp++;
	            }
			}
			return $countIsUp." groupe mise à jour";
		}
}   
 	 public function actionRemoveGroupDoublon(){
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
			$group=PHDB::find(Organization::COLLECTION, array("source.key"=>"siteDuPactePourLaTransition"));
			$scopeExist=array();
			$countDel=0;
			$str = "";
			foreach($group as $key => $v){
				foreach($v["scope"] as $kS => $bl){
					$keyScope=$kS;
					$cityId=$bl["city"];
				}
				$city=City::getById($cityId, array("postalCodes", "name") );
				if(in_array($keyScope, $scopeExist)){
					$str .= "delete ".$key." ".$v["name"]."<br/>";
					PHDB::remove(Organization::COLLECTION, array("_id"=>$v["_id"]));
					  
					$countDel++;
				}
				else{
					array_push($scopeExist, $keyScope);
					if(count($city["postalCodes"])> 1){
						foreach ($city["postalCodes"] as $cT => $code) {
							$kMulti=$cityId."cities".$code["postalCode"];
							if(!in_array($kMulti,$scopeExist))
								array_push($scopeExist, $kMulti);		
						}
					}
				}
			}
			return $str.$countDel." groupe suprimmé";
		}
}   
public function actionRemoveContractDoublon(){
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
			$contracts=PHDB::find(Poi::COLLECTION, array("source.key"=>"siteDuPactePourLaTransition", "type"=>"contract"));
			$contractRemove=array();
			$countDel=0;
			$str = "";
			foreach($contracts as $key => $v){
				if(!in_array($key, $contractRemove)){
					$searchRegExp = SearchNew::accentToRegex($v["name"]);
					$contractsSame=PHDB::find(Poi::COLLECTION, array("name"=>new MongoRegex("/.*{$searchRegExp}.*/i"), "insee"=> $v["insee"],"source.key"=>"siteDuPactePourLaTransition", "type"=>"contract"));
					if(!empty($contractsSame)){
						foreach($contractsSame as $i => $doublon){
							if($i!=$key){
								$str .=  $i." : ".$doublon["insee"]." / ".$doublon["name"];
								//PHDB::remove(Poi::COLLECTION, array("_id"=>$doublon["_id"]));
								array_push($contractRemove,$i);
								$countDel++;
							}
						}
					}
				}
			}
			return $str.$countDel." groupe suprimmé";
		}
}   
  public function actionKnowsToFollows(){
      $output ='';
	if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
		$persons=PHDB::find(Person::COLLECTION);
		foreach($persons as $key => $data){
			if(isset($data["links"]["followers"]) || isset($data["links"]["follows"])){
				$followers=array();
				$follows=array();
				if(isset($data["links"]["followers"]) && !empty($data["links"]["followers"])){
					$followers=$data["links"]["followers"];
				}
				if(isset($data["links"]["follows"]) && !empty($data["links"]["follows"])){
					$follows=$data["links"]["follows"];
				}
				PHDB::update(Person::COLLECTION,
					array("_id" => $data["_id"]) , 
					array('$unset' => array("links.followers" => ""))
				);
				PHDB::update(Person::COLLECTION,
					array("_id" => $data["_id"]) , 
					array('$unset' => array("links.follows" => ""))
				);
				if(!empty($followers)){
					//foreach ($followers as $uid => $e){	
						PHDB::update(Person::COLLECTION,
							array("_id" => $data["_id"]) , 
							array('$set' => array("links.follows" => $followers))
							);
					//}
				}
				if (!empty($follows)){
					foreach ($follows as $uid => $e){	
						if($e["type"]=="citoyens"){
						PHDB::update(Person::COLLECTION,
							array("_id" => $data["_id"]) , 
							array('$set' => array("links.followers.".$uid => $e))
							);
						} else {
							PHDB::update(Person::COLLECTION,
							array("_id" => $data["_id"]) , 
							array('$set' => array("links.follows.".$uid => $e))
							);
						}
					}
				}
				$newLinks=PHDB::findOneById(Person::COLLECTION ,$data["_id"]);
                $output .= "<br/>/////////////////////////// NEW LINK ////////////////////<br/>";
                //$output .= //print_r($newLinks["links"], true);
				}	
			}
	}
      return $output;
  }
 	public function actionRelaunchUserChtitube(){
  		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]))){
  			
  			$chtitubeurs=PHDB::find(Person::COLLECTION, array("reference.costum"=>array('$in'=>["chtitube"])), array("email", "name","username","slug"));
  			
  			$i=0;
  			foreach($chtitubeurs as $key => $p){
  				if(!empty($p["email"])) {
		            $fromMail=(isset($params["fromMail"]) && !empty($params["fromMail"])) ? $params["fromMail"] : null;
		            $tpl="<span style='width:100%;color:black'>Bonjour <b style='color:#efc201; font-size:18px'>".$p["username"]."</b>,</br><br/>".
		            	"Le jeu chtitube s'arrête Dimanche ... Mais vous avez encore toutes vos chances !<br/><br/>".
		            	"Alors, il est temps d'ajouter tout ce que vous avez envi de partager sur le Pas-de-Calais et dans votre commune.<br/><br/><br/>".
		            	"<div style='width:100%;text-align:center'><a href='https://www.chtitube.fr/#play' style='color:black;width:30%;border-radius:4px;background: #f7d43c;border: 1px solid black;padding: 10px 30px;'>Jouer maintenant</a></div><br/><br/><br/>".
		            	"Chtitube n'est pas seulement un jeu mais la construction commune d'une cartographie que vous pouvez d'ores et déjà voir ".
		            	"<a href='https://www.chtitube.fr/#search' style='color:#efc201;font-size:18px;font-weight:800'>ICI</a>.<br/><br/>".
		            	"Alors à vos claviers pour gagner les bières de nos partenaires brasserie de Mai, brasserie Arras'In, brasserie des 7 bonnettes et brasserie paysanne de l'Artois.<br/><br/>".
		            	"Chti'tubement vôtre !!";
		            $logo=Yii::app()->getModule( "costum" )->getAssetsUrl()."/images/chtitube/logo.png";
		            $res = array (
		                "type" => Cron::TYPE_MAIL,
		                "tpl"=>"basic",
		                "subject" => "Chtitube, jusque Dimanche tout peut se jouer !",
		                "from"=>"noreply@chtitube.fr",
		                "to" => $p["email"],
		                "expeditor"=>"Chtitube",
		                "source"=>array("key"=>"chtitube"),
		                "tplParams" =>array(
		                	"baseUrl"=>"https://www.chtitube.fr/",
		                	"logo"=>$logo,
            				"logo2"=>"",
            				"logoHeader"=>$logo,
            				"title"=>"Chtitube",
            				"html"=>$tpl,
		                	"style"=>array("header"=>"background-color:black;"),
            				"user"=>$key) 
		            );
		            var_dump($res);
		            Mail::schedule($res);
		            $i++;
		        } else {
		            throw new CTKException(Yii::t("common","Missing email!"));
		        }
  			}
  			return $i." mails envoyés";
  		}
  	}
    public function actionActionWithnoAnswer(){
        $output ='';
  		//if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]))){
  			
  			$projects=PHDB::find(Project::COLLECTION, array("source.key"=>"ctenat", "category"=>"ficheAction"), array("links", "name","creator","slug"));
  			
  			$creators=[];
  			$projectNoAnswers = [];
  			$countAnswer=count($projects);
  			$i=0;
  			foreach($projects as $key => $p){
  				if(!isset($p["links"]) || !isset($p["links"]["answers"]))
  				{
  					$i++;
                    $output .= "-------------------------------------------------<br/>";
                    $output .= "-------------------------------------------------<br/>";
                    $output .= "-----------------------".$i."--------------------------<br/>";
                    $output .= "Action : <a target='_blank' href='https://cte.ecologique-solidaire.gouv.fr/#@".$p["slug"]."'>".$p["name"]." : @".$p["slug"]."</a><br/>";
                    $output .= "-------------------------<br/>";
  					
  					//who is the user
  					$person=PHDB::findOne(Person::COLLECTION, array("_id"=>new MongoId($p["creator"])), array("links", "name", "email"));
                    $output .= "<b>creator : ". $person["name"]." : ".$person["email"]."</b> <br/>";
                    $output .= "-------------------------<br/>";
  				} 
  			}
        $output .= $i." n'ont pas de candidatures sur ".$countAnswer." ficheActions";
  		//}
        return $output;
  	}
   public function actionRemoveLinkCterWithCter(){
       $output ='';
  		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]))){
  			$projects=PHDB::find(Project::COLLECTION, array("source.key"=>"ctenat", "category"=>"cteR"), array("links", "name"));
  			$iTer=0;
  			foreach($projects as $k => $v){
  				if(isset($v["links"]) && isset($v["links"]["projects"])){
	  				foreach($v["links"]["projects"] as $e => $act){
	  					$proj=PHDB::findOne(Project::COLLECTION, array("_id"=> new MongoId($e)),array("name", "links", "category"));
	  					if(isset($proj["category"]) && $proj["category"]=="cteR"){
                            $output .= $proj["name"]."links à supprimer";
        					PHDB::update( Project::COLLECTION, 
                       			array("_id" => new MongoId($k)) , 
                       			array('$unset' => array("links.projects.".$e => "") )
                       		);
	  						if(isset($proj["links"]) && isset($proj["links"]["projects"]) && isset($proj["links"]["projects"][$k])){
                                $output .= "<br/>Lien projet du parent <b>".$v["name"]."<b/>présent aussi sur l'enfant : ".$proj["name"];
	  							PHDB::update( Project::COLLECTION, 
	                       			array("_id" => new MongoId($e)) , 
	                       			array('$unset' => array("links.projects.".$k => "") )
	                       		);
	  						}
                            $output .= "<br/>-------------------------------------------<br/>";
	  						$iTer++;
	  					}
	  				}
	  			}
	  		}
            $output .= "<br/>-------------------------------------------------------------------------------------------<br/>";
            $output .= "<br/>-------------------------------------------------------------------------------------------<br/>";
            $output .= "<br/>-----------------".$iTer." cte liens avec autres cte supprimé------------------------------<br/>";
            $output .= "<br/>-------------------------------------------------------------------------------------------<br/>";
            $output .= "<br/>-------------------------------------------------------------------------------------------<br/>";
	  	}
          return $output;
	}
	public function actionUpdateAnswersIndicateurs(){
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]))){
			$answers=PHDB::find(Form::ANSWER_COLLECTION, array("source.key"=>"ctenat", "answers" => array('$exists' => 1)));
			$iTer=0;
			$iFa=0; 

			foreach($answers as $k => $v){
				if(!empty($v["answers"]) && 
					!empty($v["formId"])&& 
					!empty($v["answers"][$v["formId"]]) && 
					!empty($v["answers"][$v["formId"]]["answers"]) && 
					!empty($v["answers"][$v["formId"]]["answers"]["murir"])&& 
					!empty($v["answers"][$v["formId"]]["answers"]["murir"]["results"])){

					$indList = $v["answers"][$v["formId"]]["answers"]["murir"]["results"] ;
					$newsInd = array();
					foreach ($indList as $keyI => $valI) {
						
						if( is_array($valI["indicateur"]) ){
							foreach ($valI["indicateur"] as $keyInd => $valInd ) {
								$n = array();
								$n["indicateur"] = $valInd;

								if($keyInd == 0 && !empty($valI["objectif"]))
									$n["objectif"] = $valI["objectif"];

								if($keyInd == 0 &&  !empty($valI["reality"]))
									$n["reality"] = $valI["reality"];

								$newsInd[] = $n;
							}					
						} else
							$newsInd[] = $valI;
					}//
					$iFa++;
					PHDB::update(Form::ANSWER_COLLECTION,
						array("_id" => new MongoId($k)) , 
						array('$set' => array("answers.".$v["formId"].".answers.murir.results" => $newsInd))
					);


				}
				
			}
			//Rest::json($res); exit;
			return $iFa." fiches actions ";
		}else{
			return "Accès réservé au Big Bosses !";
		}
	}
	public function actionUpdateAnswersActionCandidateMissing(){
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]))){
			$answers=PHDB::find(Form::ANSWER_COLLECTION, 
						array("source.key"=>"ctenat", 
							'$or' => array(
								array( "priorisation" => array('$exists' => 0) ),
								array( "priorisation" => array('$nin' => array(
									Ctenat::STATUT_ACTION_VALID,
								    Ctenat::STATUT_ACTION_MATURATION,
								    //Ctenat::STATUT_ACTION_LAUREAT,
								    Ctenat::STATUT_ACTION_REFUSE,
								    Ctenat::STATUT_ACTION_CANDIDAT,
								    Ctenat::STATUT_ACTION_CONTRACT))),
							) ), array("formId", "priorisation", "answers") );
			$iTer=0;
			$iFa=0;
			$listCter = array();
			if(!empty($answers)){
				foreach($answers as $k => $v){
					if(!empty($v["answers"]) && 
						!empty($v["formId"])&& 
						!empty($v["answers"][$v["formId"]]) && 
						!empty($v["answers"][$v["formId"]]["answers"]) && 
						!empty($v["answers"][$v["formId"]]["answers"]["project"])&& 
						!empty($v["answers"][$v["formId"]]["answers"]["project"]["id"])){
						//Rest::json($v["answers"]); exit;
						$project=PHDB::findOneById(Project::COLLECTION,$v["answers"][$v["formId"]]["answers"]["project"]["id"], array("name", "slug"));

						$cter=PHDB::findOne(Project::COLLECTION,array("slug"=> $v["formId"]), array("name", "slug"));
						$iFa++;
						//$str .= "CTER concerné : ".$cter["name"]." : -> ".$cter["slug"]."<br/>";

						$prio = "";

						if(!empty($v["priorisation"])){
							if($v["priorisation"] == "abandoned"){
								$prio = Ctenat::STATUT_ACTION_REFUSE;
							}else if($v["priorisation"] == "selected"){
								$prio = Ctenat::STATUT_ACTION_VALID;
							}else if($v["priorisation"] == "reserved"){
								$prio = Ctenat::STATUT_ACTION_MATURATION;
							}else if($v["priorisation"] == Ctenat::STATUT_ACTION_LAUREAT){
								$prio = Ctenat::STATUT_ACTION_MATURATION;
							}
						} else {
							if( $cter["slug"] == "cteNordArdeche" || $cter["slug"] == "cteSudArdeche" )
								$prio = Ctenat::STATUT_ACTION_VALID;
							else
								$prio = Ctenat::STATUT_ACTION_CANDIDAT;
						}

						//$str .= $prio."   CTER concerné : ".$cter["name"]." : -> ".$cter["slug"]."<br/>";
						if( $prio != ""){
							if(!isset($listCter[$cter["slug"]])){
								$listCter[$cter["slug"]]["count"] = 1;
								$listCter[$cter["slug"]]["projects"][] = $k. " -> ".$project["name"]. " : ".$prio ;
							}else{
								$listCter[$cter["slug"]]["projects"][] = $k. " -> ".$project["name"]. " : ".$prio;
								$listCter[$cter["slug"]]["count"]++;
							}
							PHDB::update(Form::ANSWER_COLLECTION,
								array("_id" => new MongoId($k)) , 
								array('$set' => array("priorisation" => $prio))
							);

							Ctenat::updatePrioAnswers($k, $v["formId"], $prio, "priorisation");
						}
					}
				}
			}
			return Rest::json($listCter);
		}else{
			return "Accès réservé au Big Bosses !";
		}		
	}


  public function actionUpdateProjectOpenEditionCteNat(){
      $output ='';
  		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]))){
  			$projects=PHDB::find(Project::COLLECTION, array("source.key"=>"ctenat"));
  			$iTer=0;
  			$iFa=0; 

  			foreach($projects as $k => $v){
  				if(isset($v["category"])){
	  				if($v["category"]=="cteR")
	  					$iTer++;
	  				if($v["category"]=="ficheAction")
	  					$iFa++;
	  			}else
                    $output .= $v["name"]."<br/>";
  				//if(!isset($v["category"])){
  				$preferences=$v["preferences"];
  				if(isset($preferences["isOpenEdition"])){
  					$preferences["isOpenEdition"]=false;
  				}
	  			PHDB::update(Project::COLLECTION,
					array("_id" => $v["_id"]) , 
					array('$set' => array("preferences"=>$preferences))
				);
	  			//}
  			}
            $output .= $iFa." fiches actions et ".$iTer." territoires sont passés en openEdition false'";
  		}else{
            $output .= "Accès réservé au Big Bosses !";
  		}
          return $output;
  }

  public function actionUpdateOrgaOpenEditionCteNat(){
      $output ='';
  		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]))){
  			$elms=PHDB::find(Organization::COLLECTION, array("source.key"=>"ctenat"));
  			$iO=0;

  			foreach($elms as $k => $v){
  				$iO++;
  				if(isset($preferences)){
	  				$preferences=$v["preferences"];
	  				if(isset($preferences["isOpenEdition"])){
	  					$preferences["isOpenEdition"]=false;
	  				}
		  			PHDB::update(Organization::COLLECTION,
						array("_id" => $v["_id"]) , 
						array('$set' => array("preferences"=>$preferences))
					);
		  		}
  			}
            $output .= $iO." orgas sont passés en openEdition false'";
  		}else{
            $output .= "Accès réservé au Big Bosses !";
  		}
      return $output;
  }
 
  ////////////// --------------- BASH CTENAT ---------------------------------------------------------////////////



	// public function actionHotfixLinksProjects(){
	// 	if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]))){
	// 		$projects=PHDB::find(Project::COLLECTION, array("source.key"=>"ctenat",
	// 														"links.projects" => array('$exists' => 1) ) );
	// 		$i=0; 
	// 		foreach($projects as $k => $v){
	// 			$i++;
	// 			// var_dump(is_object($v["links"]["projects"]));
	// 			// $str .= " <br> ";
	// 			// var_dump(is_array($v["links"]["projects"]));
	// 			// $str .= " <br> ";
	// 			if( empty($v["links"]["projects"]) ){
	// 				$str .= $k." : ".$v["name"]." <br> ";
					
	// 				// PHDB::update(Project::COLLECTION,
	// 				// 				array("_id" => $v["_id"]) , 
	// 				// 				array('$set' => array("category"=>"cteR"))
	// 				// 			);
	// 			}
	// 		}
	// 		$str .= $i." projets ont reçus la category 'cteR'";
	// 	}else{
	// 		$str .= "Accès réservé au Big Bosses !";
	// 	}		
	// }

  	public function actionHotfixLinkCteR(){
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]))){
			$projects=PHDB::find( Project::COLLECTION, array("source.key"=>"ctenat",
															"category" => "cteR"),
														array("category") ) ;
			$i=0;
			//Rest::json($projects); exit;
			foreach($projects as $k => $v){
				if( !empty($v["category"]) && $v["category"] == "cteR") {
					$actions=PHDB::find(Project::COLLECTION, array("links.projects.".$k=> array('$exists' => 1) ), array("name") );
					if(!empty($actions)){
						foreach ($actions as $kA => $valA) {
							Link::connect($k, Project::COLLECTION, $kA, Project::COLLECTION, Yii::app()->session["userId"], "projects",false,false,false, false, false);
						}
					}
				}
			}
			return $i." projets ont reçus la category 'cteR'";
		}else{
			return "Accès réservé au Big Bosses !";
		}		
	}

	public function actionHotfixImportAction(){
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]))){
			$projects=PHDB::find(Project::COLLECTION, array("source.key"=>"ctenat" /*,
														"source.insertOrign"=>"import" */ ),
														array("links", "category"));
			$i=0;
			
			foreach($projects as $k => $v){
				if( !empty($v["links"]) ){
					if( !empty($v["links"]["projects"]) ){


						$linkP = array();
						foreach($v["links"]["projects"] as $kLP => $vLP){
							if( !empty($vLP["isInviting"]) )
								unset($vLP["isInviting"]);
							if( !empty($vLP["isAdmin"]) )
								unset($vLP["isAdmin"]);

							if( !empty($vLP["invitorId"]) )
								unset($vLP["invitorId"]);

							if( !empty($vLP["invitorName"]) )
								unset($vLP["invitorName"]);

							$linkP[$kLP] = $vLP;
						}

						$v["links"]["projects"] = $linkP;
						
					}

					if( !empty($v["links"]["contributors"]) &&
						!empty($v["links"]["contributors"]["55e042ffe41d754428848363"]) ){
						unset($v["links"]["contributors"]["55e042ffe41d754428848363"]);
					}
					//Rest::json($v); exit;
					$i++;
					PHDB::update(Project::COLLECTION,
									array("_id" => $v["_id"]) , 
									array('$set' => array("links"=>$v["links"]))
								);
				}
			}
			return $i." projets ont reçus la category 'cteR'";
		}
	}

	public function actionAddSourceAnswers(){
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]))){
			$cter=PHDB::find(Project::COLLECTION, array("source.key"=>"ctenat", "category" => "cteR"), array("slug", "source") );

			$sourceU = array("insertOrign" => "costum",
								"key" => "ctenat",
								"keys" => array("ctenat") ) ;
			$i=0; 
			foreach($cter as $k => $v){
				$answers=PHDB::find("answers", array("formId"=>$v["slug"],
													"source" => array('$exists'=> 0) ) );


				if( !empty($answers) ){
					//Rest::json($answers); exit;
					foreach ($answers as $keyA => $valA) {
						if(!empty($valA["answers"])){

							// if(!empty($valA["source"]) &&
							// 	!empty($valA["source"]["status"]) && 
							// 	!empty($valA["source"]["status"]["ctenat"]) &&
							// 	$valA["source"]["status"]["ctenat"] == "CTE Signé" )
							// 	$sourceU["status"]["ctenat"] = 
							PHDB::update("answers",
									array("_id" => $valA["_id"]) , 
									array('$set' => array("source"=>$sourceU))
								);

							$i++;
						}
					}
					
					
				}
			}
			return $i." answers ont reçus la sources";

		}else{
			return "Accès réservé au Big Bosses !";
		}		
	}
	
	public function actionCibleDDAnswers(){
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]))){
			$answers=PHDB::find("answers", array("source.key"=>"ctenat"));

			//Rest::json($answers); exit;
			$i=0; 
			foreach($answers as $k => $v){
				
				if( !empty($v["answers"]) && 
					!empty($v["formId"])&& 
					!empty($v["answers"][$v["formId"]]) && 
					!empty($v["answers"][$v["formId"]]["answers"]) && 
					!empty($v["answers"][$v["formId"]]["answers"]["caracter"]) && 
					isset($v["answers"][$v["formId"]]["answers"]["caracter"]["cibleDDPrincipal"]) &&
					gettype($v["answers"][$v["formId"]]["answers"]["caracter"]["cibleDDPrincipal"]) == "array" ){
					//var_dump($v["answers"][$v["formId"]]["answers"]["caracter"]["cibleDDPrincipal"]); 

					if(empty($v["answers"][$v["formId"]]["answers"]["caracter"]["cibleDDPrincipal"]))
						$v["answers"][$v["formId"]]["answers"]["caracter"]["cibleDDPrincipal"] = "" ;
					else
						$v["answers"][$v["formId"]]["answers"]["caracter"]["cibleDDPrincipal"] = $v["answers"][$v["formId"]]["answers"]["caracter"]["cibleDDPrincipal"][0];
					//$str .= " : ".$k."<br>";
					$i++;
					PHDB::update("answers",
									array("_id" => $v["_id"]) , 
									array('$set' => array("answers"=>$v["answers"]))
								);
				}
			}
			return $i." projets ont reçus la category 'cteR'";
		}else{
			return "Accès réservé au Big Bosses !";
		}		
	}

	public function actionUpdateStatusAction(){
		$str = "";
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]))){
			$cter=PHDB::find(Project::COLLECTION, array("source.key"=>"ctenat", 
														"category"=> "cteR",
														"source.status.ctenat" => Ctenat::STATUT_CTER_SIGNE) );
			$i=0;

			foreach($cter as $k => $v){
				$str .= $v["name"]."<br/>";
				
				$answers=PHDB::find("answers", array("formId"=>$v["slug"]) );
				$str .= "------".count($answers)."<br/>";
				foreach ($answers as $kA => $vA) {
					$i++;
					PHDB::update("answers",
									array("_id" => $vA["_id"]) , 
									array('$set' => array("priorisation"=>"Action validée"))
								);
				}
				
			}
			$str .= $i." projets ont reçus le status  'Action validée'";
		}else{
			$str = "Accès réservé au Big Bosses !";
		}	
		return $str;	
	}

  public function actionUpdateCategoryOnTerritoryCteNat(){
  		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]))){
  			$projects=PHDB::find(Project::COLLECTION, array("source.key"=>"ctenat"));
  			$i=0; 
  			foreach($projects as $k => $v){
  				$i++;
  				if(!isset($v["category"])){
	  				PHDB::update(Project::COLLECTION,
										array("_id" => $v["_id"]) , 
										array('$set' => array("category"=>"cteR"))
									);
	  			}
  			}
  			return $i." projets ont reçus la category 'cteR'";
  		}else{
  			return "Accès réservé au Big Bosses !";
  		}		
  }
  
  public function actionUpdateBadgesCteNat(){
  	$str = "";
  	if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]))){
		$badges=PHDB::find(Badge::COLLECTION, array("source.key"=>"ctenat"));
		$i=0; 
		$iCat=0;
		$iTags=0;
		foreach($badges as $k => $v){
			$i++;
			if($v["category"]=="action"){
				$iCat++;
				PHDB::update(Badge::COLLECTION,
								array("_id" => $v["_id"]) , 
								array('$set' => array("category"=>"domainAction"))
							);
			}
			if(!isset($v["tags"]) || empty($v["tags"])){
				$upTags=[];
				array_push($upTags, $v["name"]);
				if(isset($v["parent"])){
					foreach($v["parent"] as $id => $value){
						array_push($upTags, $value["name"]);
					}
				}
				$iTags++;
				PHDB::update(Badge::COLLECTION,
					array("_id" => $v["_id"]) , 
					array('$set' => array("tags"=>$upTags))
				);
			}
		}
		$str .= $i." badges ont été traités<br/>";
		$str .= $iCat." badges ont recu la catégorie domainAction<br/>";
		$str .= $iTags." badges ont recu les tags<br/>";
		return $str;
	}else{
		return "Accès réservé au Big Bosses !";
	}		
  }
  public function actionStatutCtenat() {
  		$str = "";
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
			$nbUser = 0;
			$where3 = array("source.key" => "ctenat" );
			$resElt3 = PHDB::find(Project::COLLECTION, $where3 , array("name", "source"));

			//Rest::json($resElt3); exit ;
			foreach ($resElt3 as $key => $value) {

				if(!empty($value["source"])){
					$value["source"]["status"] = array("ctenat" => "Territoire Candidat");
					//$nbUser++;
					$res = PHDB::update(Project::COLLECTION, 
									array("_id"=>new MongoId($key)),
									array('$set' => array("source" => $value["source"]) ) );

					if( $res["ok"] == 1 ){
						$nbUser++;
					} else {
						$str .= "<br/> Error with user id : ".$key;
					}
				}

			}
			//Rest::json($tags); exit;
			$str .= "Number of project modified : ".$nbUser;
		}
		return $str;
	}

	public function actionCorrectionStatutCtenat() {
		$str = "";
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
			$nbUser = 0;
			$where3 = array( "source.status.ctenat" => array('$exists' => 1),
							 "source.key"  => array('$exists' => 0));
			$resElt3 = PHDB::find(Project::COLLECTION, $where3 , array("name", "source"));

			//Rest::json($resElt3); exit ;
			foreach ($resElt3 as $key => $value) {

				if(!empty($value["source"])){
					$value["source"]["key"] = "ctenat";
					$value["source"]["keys"] = array("ctenat");
					$value["source"]["insertOrign"] = "costum";
					//$nbUser++;,
					$res = PHDB::update(Project::COLLECTION, 
									array("_id"=>new MongoId($key)),
									array('$set' => array("source" => $value["source"]) ) );

					if( $res["ok"] == 1 ){
						$nbUser++;
					} else {
						$str .= "<br/> Error with user id : ".$key;
					}
				}

			}
			//Rest::json($tags); exit;
			$str .=  "Number of project modified : ".$nbUser;
		}
		return $str;
	}

	public function actionClearsLinksCTENAT(){
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]))){
			$projects=PHDB::find(Project::COLLECTION, array("source.key"=>"ctenat"));
			$i=0;
			$set = array();
			foreach($projects as $k => $v){
				
				if(!empty($v["links"])){
					if(!empty($v["links"]["answers"])){
						foreach($v["links"]["answers"] as $kA => $vA){
							$fA = PHDB::findOneById("answers", $kA);
							if(empty($fA))
								unset($v["links"]["answers"][$kA]);
						}
					}

					if(!empty($v["links"]["contributors"])){
						foreach($v["links"]["contributors"] as $kA => $vA){
							if(!empty($vA) && !empty($vA["type"]) && $vA["type"] == Project::COLLECTION)
								unset($v["links"]["contributors"][$kA]);
						}
					}
  				
  				$set["links"] = $v["links"];
				}

				if(!empty($v["parent"])){
				foreach($v["parent"] as $kA => $vA){
					if( !empty($vA["type"]) ){
						$eltA = PHDB::findOneById($vA["type"], $kA);
						if(empty($eltA))
							unset($v["parent"][$kA]);
					}
				}

				$set["parent"] = $v["parent"];
			}

			if(!empty($set)){
				$i++;
				$res = PHDB::update(Project::COLLECTION, 
								array("_id"=>new MongoId($k)),
								array('$set' => $set ));
			}
			
  			
			}
			return $i." projets ont reçus la category 'cteR'";
		}else{
			return "Accès réservé au Big Bosses !";
		}		
	}
	public function actionParentCteR(){
		$str = "";
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]))){
			$projects=PHDB::find(Project::COLLECTION, array("source.key"=>"ctenat",
															"source.status.ctenat"=>"Territoire lauréat"));
			//Rest::json($projects); exit ;
			$i=0;
			$set = array();
			$listNameParent =  array();
			$sourceU = array("insertOrign" => "import",
							"key" => "ctenat",
							"keys" => array("ctenat") );
			$str .= count($projects).": <br><br>";
			$str = "";
			foreach($projects as $k => $v){
				
				$str .= $v["name"]." : <br>";
				$update = false;
				if(!empty($v["scope"])){
					foreach ($v["scope"] as $kS => $valS) {

						if(!empty($valS["name"])){
							$where = array("source.key" => "ctenat");
							$where["name"] = $valS["name"];
							$parentCteR = PHDB::findOne(Organization::COLLECTION, $where);

							if(empty($parentCteR) && !in_array($valS["name"], $listNameParent)){
								$listNameParent[] = $valS["name"];

								$eltPorteurTer = array(	"name" => $valS["name"],
														"type" => Organization::TYPE_GOV,
														"collection" => Organization::COLLECTION,
														"source" => $sourceU );
								$res = Element::save($eltPorteurTer);

								$where = array("source.key" => "ctenat");
								$where["name"] = $valS["name"];
								$parentCteR = PHDB::findOne(Organization::COLLECTION, $where);
							}

							

							if(!empty($parentCteR)){
								if(empty($v["parent"]))
									$v["parent"] = array();

								if(!empty($parentCteR) && empty($v["parent"][(String)$parentCteR["_id"]]) ) {
									$v["parent"][(String)$parentCteR["_id"]] = array( "type" => Organization::COLLECTION, "name" => $parentCteR["name"]);
								}

								if(count($v["parent"]) > 0){
									PHDB::update( Project::COLLECTION,
									    array("_id"=>new MongoId( $k ) ), 
										array('$set' => array( "parent" => $v["parent"] ) ) );
								}


								if(!empty($parentCteR) && !empty($parentCteR["_id"])){
									if(empty($parentCteR["links"]))
										$parentCteR["links"] = array();

									if(empty($parentCteR["links"]["projects"]))
										$parentCteR["links"]["projects"] = array();

									if(empty($parentCteR["links"]["projects"][(String)$v["_id"]]) ) {
										$parentCteR["links"]["projects"][(String)$v["_id"]] = array( "type" => Project::COLLECTION, "name" => $v["name"]);

										PHDB::update( Organization::COLLECTION,
										    array("_id"=>new MongoId( (String)$parentCteR["_id"]) ), 
											array('$set' => array( "links" => $parentCteR["links"] ) ) );
									}
								}
								$update = true;
								$str .= "---------".$valS["name"]."<br>";
								
							}
							
						}


					}

					if($update == true)
						$i++;
				}else{
					$str .= $v["name"]."<br>";
				}					
			}
			$str .= $i." projets modifier";
			$str .= "<br><br>".$str;	
			return $str;
		}else{
			return "Accès réservé au Big Bosses !";
		}

			
	}


	public function actionAddAdminCter(){
		$str = "";
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]))){
			$projects=PHDB::find(Project::COLLECTION, array("source.key"=>"ctenat",
															"category"=>"cteR"));
			//Rest::json($projects); exit ;
			$i=0;
			$set = array();
			$listNameParent =  array();
			$sourceU = array("insertOrign" => "import",
							"key" => "ctenat",
							"keys" => array("ctenat") );
			$bigboss = PHDB::find(Person::COLLECTION, array("email" => array('$in' => array("antoine.daval@gmail.com", "fanny.bontemps@developpement-durable.gouv.fr", "elois.divol@developpement-durable.gouv.fr", "juliette.maitre@cerema.fr"))));
			//Rest::json($bigboss); exit ;
			foreach ($bigboss as $key => $value) {
				$str .= "bigbos ---------".$key."<br>"; 
			}
			$str .= "<br>";
			$res = array();

			foreach($projects as $k => $v){
				if(!empty($bigboss)){
					$str .= "<br>".$v["name"]." : ".$v["slug"]."<br>"; 
					foreach ($bigboss as $keyB => $valB) {
						$child = array("childId" => $keyB, "childType" => Person::COLLECTION);
						
						Link::connect($k, Project::COLLECTION, $keyB, Person::COLLECTION,Yii::app()->session["userId"], "contributors", true, null, null, null, null);
        				
        				Link::connect($keyB, Person::COLLECTION, $k, Project::COLLECTION, Yii::app()->session["userId"], "projects", true, null, null, null, null);


        				// Link::connect($projectId, Project::COLLECTION, $orgaId, Organization::COLLECTION,Yii::app()->session["userId"], "contributors", null, null, null, null,array("porteur"));
        				// Link::connect($orgaId, Organization::COLLECTION, $projectId, Project::COLLECTION, Yii::app()->session["userId"], "projects", null,null, null, null, array("porteur"));
					}
					$i++;
				}
			}
			$str .= $i." projets modifier";
			return $str;
		}else{
			return "Accès réservé au Big Bosses !";
		}
	}

	public function actionParentCterRole(){
		$str = "";
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]))){
			$projects=PHDB::find(Project::COLLECTION, array("source.key"=>"ctenat",
															"category"=>"cteR"));
			//Rest::json($projects); exit ;
			$i=0;
			$set = array();
			$listNameParent =  array();
			$sourceU = array("insertOrign" => "import",
							"key" => "ctenat",
							"keys" => array("ctenat") );
			
			$res = array();

			foreach($projects as $k => $v){
				if( !empty($v["parent"]) ){
					$contributors =array();
					$str .=  "<br>".$v["name"]." : ".$v["slug"]."<br>"; 
					foreach ($v["parent"] as $kP => $valP) {
						if($valP["type"] == Organization::COLLECTION){
							$str .=  "-----------".$kP." : ".$valP["name"]."<br>"; 
							Link::connect($k, Project::COLLECTION, $kP, Organization::COLLECTION,Yii::app()->session["userId"], "contributors", null, null, null, null,array(Ctenat::ROLE_PORTEUR_CTER));
        					Link::connect($kP, Organization::COLLECTION, $k, Project::COLLECTION, Yii::app()->session["userId"], "projects", null,null, null, null, array(Ctenat::ROLE_PORTEUR_CTER));
						}
					}
					$i++;
				}
			}
			$str .=  $i." projets modifier";
			return $str;
		}else{
			return "Accès réservé au Big Bosses !";
		}
	}

	public function actionParentActionsRole(){
		$str = "";
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]))){
			$answers=PHDB::find("answers", array("source.key"=>"ctenat"));
			//Rest::json($projects); exit ;
			$i=0;
			$set = array();
			$listNameParent =  array();
			$sourceU = array("insertOrign" => "import",
							"key" => "ctenat",
							"keys" => array("ctenat") );
			
			$res = array();
			$addTags = array( "actionPrincipal", "actionSecondaire", "cibleDDPrincipal");
			foreach($answers as $kA => $vA){
				if( !empty($vA["formId"]) ){
					$cter = PHDB::findOne(Project::COLLECTION, array("slug" => $vA["formId"]));
					if( !empty($vA["answers"]) && 
						!empty($cter) && 
						!empty($vA["answers"][$vA["formId"]]) &&
						!empty($vA["answers"][$vA["formId"]]["answers"]) &&
						!empty($vA["answers"][$vA["formId"]]["answers"]["project"]) &&
						!empty($vA["answers"][$vA["formId"]]["answers"]["project"]["id"]) &&
						!empty($vA["answers"][$vA["formId"]]["answers"]["organization"]) &&
						!empty($vA["answers"][$vA["formId"]]["answers"]["organization"]["id"]) ){
						$idP = $vA["answers"][$vA["formId"]]["answers"]["project"]["id"] ;
						$idO = $vA["answers"][$vA["formId"]]["answers"]["organization"]["id"] ;
						$str .=  $vA["formId"]."<br>"; 
						Link::connect($idP, Project::COLLECTION, $idO, Organization::COLLECTION,Yii::app()->session["userId"], "contributors", null, null, null, null,array("Porteur d'action"));
	        			Link::connect($idO, Organization::COLLECTION, $idP, Project::COLLECTION, Yii::app()->session["userId"], "projects", null,null, null, null, array("Porteur d'action"));

	        			Link::connect((String)$cter["_id"], Project::COLLECTION, $idO, Organization::COLLECTION,Yii::app()->session["userId"], "contributors", null, null, null, null,array("Porteur d'action"));
	        			Link::connect($idO, Organization::COLLECTION, (String)$cter["_id"], Project::COLLECTION, Yii::app()->session["userId"], "projects", null,null, null, null, array("Porteur d'action"));

	        			//var_dump($vA["priorisation"] );
	        			if(	!empty($vA["answers"][$vA["formId"]]["answers"]["caracter"]) &&
	        				!empty($vA["priorisation"]) && 
	        				$vA["priorisation"] == "selected"){
	        				//var_dump($vA["priorisation"] );
	        				//$project = PHDB::findOneById(Project::COLLECTION, $idP);
	        				$caracter = $vA["answers"][$vA["formId"]]["answers"]["caracter"];
	        				if( empty($cter["tags"]) )
								$cter["tags"] = array();
	        				foreach ($addTags as $kLT => $valLT) {
								if(is_array($caracter[$valLT])){
									foreach ($caracter[$valLT] as $kT => $valT) {
										if(!in_array($valT, $cter["tags"]))
											$cter["tags"][] = $valT ;
									}
								}else if(is_string($caracter[$valLT])){
									if(!in_array($caracter[$valLT], $cter["tags"]))
										$cter["tags"][] = $caracter[$valLT] ;
								}
							}

							if(!empty($cter["tags"])){

								PHDB::update( Project::COLLECTION,
								    array("_id"=>new MongoId((String)$cter["_id"])), 
								    array('$set' => array( "tags" => $cter["tags"] )));
							}
	        			}

						$i++;
					}
				}

				
			}
			$str .=  $i." projets modifier";
			return $str;
		}else{
			return "Accès réservé au Big Bosses !";
		}
	}

	public function actionRemoveAdminCter(){
		$str = "";
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]))){
			$projects=PHDB::find(Project::COLLECTION, array("source.key"=>"ctenat",
															"category"=>"cteR"));
			//Rest::json($projects); exit ;
			$i=0;
			$set = array();
			$listNameParent =  array();
			$sourceU = array("insertOrign" => "import",
							"key" => "ctenat",
							"keys" => array("ctenat") );
			$bigboss = PHDB::find(Person::COLLECTION, array("email" => array('$in' => array("antoine.daval@gmail.com", "fanny.bontemps@developpement-durable.gouv.fr", "elois.divol@developpement-durable.gouv.fr", "juliette.maitre@cerema.fr"))));
			//Rest::json($bigboss); exit ;
			foreach ($bigboss as $key => $value) {
				$str .= "bigbos ---------".$key."<br>"; 
			}
			$str .= "<br>";
			$res = array();

			// foreach($projects as $k => $v){
				
			// 	if(!empty($bigboss)){
			// 		foreach ($bigboss as $keyB => $valB) {
			// 			$child = array("childId" => $keyB, "childType" => Person::COLLECTION);
			// 			$res[] = Link::connectParentToChild( $k, Project::COLLECTION, $child, true, Yii::app()->session["userId"], "" ) ;
			// 		}
			// 	}
			// }

			foreach($projects as $k => $v){

				if( !empty($v["links"]) && !empty($v["links"]["contributors"]) ){
					$contributors =array();
					$str .= "<br>".$v["name"]." : ".$v["slug"]."<br>"; 
					foreach ($v["links"]["contributors"] as $kC => $valC) {
						if(	empty($bigboss[$kC]) && 
							$valC["type"] == Person::COLLECTION && 
							!empty($valC["isAdmin"]) && 
							$valC["isAdmin"] == true){
							unset($valC["isAdmin"]);
							$str .= "---------".$kC."<br>"; 
						}

						if( !empty($bigboss[$kC]))
							unset($valC["isInviting"]);
						$contributors[$kC] = $valC;
					}

					

					if(!empty($contributors)){
						$i++;
						$v["links"]["contributors"] = $contributors;
						PHDB::update( Project::COLLECTION,
									    array("_id"=>new MongoId( $k) ), 
										array('$set' => array( "links" => $v["links"] ) ) );
					}


				}
							
			}
			$str .= $i." projets modifier";
			$str .= json_encode($res);
			return $str;
		}else{
			return "Accès réservé au Big Bosses !";
		}

	}
	public function actionAddDispositifAnswer(){
		$where =
		array('$and' => array(
				array(
					'state' => array (
						'$nin' => 
						array ('uncomplete' ,'deleted')
					)
				),
				array(
					'status' =>  array ( 
							'$nin' =>  array ( 'uncomplete' ,'deleted' ,'deletePending' )
					)
				),
				array(
					'answers.action.project.0' => array('$exists' => true)
				),
				array(
					'$or' => array(
						array( 'source.keys' =>  array ('$in' => array('ctenat'))),
						array( 'reference.costum' =>  array ('$in' => array('ctenat')))
					)
				)
					));
		$allAnswers = PHDB::find(Answer::COLLECTION,$where,array("context"));
		$count = 0;
		$output = "";
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
			if( !empty($allAnswers) ){
				foreach ($allAnswers as $key => $value) {
					$dispositif = "";
					$dispositifId = "";
					if(!empty($value["context"])){
						$dispositifParent = "";
						foreach($value["context"] as $kContext => $vContext){
							if($kContext != "5ca1b2bb40bb4e9352ba351b"){
								$dispositifParent = $kContext;
								$dispositifParentType = $vContext["type"];
							}
						} 
						$territory = PHDB::findOneById($dispositifParentType, $dispositifParent, array("preferences", "links", "collection","name","dispositifId", "dispositif") ) ;  
						
						
						if(!empty($territory)){
							if(isset($territory["dispositif"]))
								$dispositif	= $territory["dispositif"];
							if(isset($territory["dispositifId"]))
								$dispositifId	= $territory["dispositifId"];
						}   
					}
					$set = [];
					$set['$set'] = [];
					if($dispositif != "")
						$set['$set']["answers.action.porteur.dispositif"] = $dispositif;
					if($dispositifId != "")
						$set['$set']["answers.action.porteur.dispositifId"] = $dispositifId;
						
					if(!empty($set['$set'])){
						$output .= "answers ". $key ." mis à jour<br>";
						PHDB::update(Answer::COLLECTION,
							array("_id" => new MongoId($key)), 
							$set
						);
						$count++;
					}
				}
			}
			$output .= $count." answers mis à jour";
		}else {
			$output .= "Vous n'êtes pas autorisé";
		}
		return $output;
		
	}
  /////// ------------------------ END BASH CTENAT ---------------------------------------------------//////////
  public function actionModifyCostumDB(){
        echo "<h1>Are Talking To Me</h1><br/><iframe src='https://giphy.com/embed/VW66IuyWkTdT7neytT' width='480' height='360' frameBorder='0' allowFullScreen></iframe><br/>".
            "<span>Pour expliquer, le système des json est finit maintenant ça se passe en db ...<br/> normalement tout passe par la db les json ne sont plus effectifs</span>";
		/*if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"])) 
			&& in_array(Yii::app()->session["user"]["slug"], ["bouboule", "oceatoon", "raphaelRiviere", "thomascraipeau", "nicoss"])){
			
				$retour = "<h1>Bonjour ".Yii::app()->session["user"]["username"]."</h1><br/>";
			$costums=PHDB::find(Costum::COLLECTION);
			$retour .= count($costums)." vont passer dans la moulinette ! Prêt ... Partez <br/><br/>";
			$i=0;
			$sansSlug=0;
			foreach($costums as $k => $v){
				if(@$v["slug"]){
					$retour .= "<br/>-------------------------------<br/>".
						"-------------------------------<br/>".
						"-------------------------------<br/>".
							"<h3>".@$v["slug"]." </h3>".
						"<br/>-------------------------------".
						"<br/>-------------------------------".
						"<br/>-------------------------------<br/>";
					$filepath="../../modules/costum/data/".$v["slug"].".json";
					if(file_exists($filepath)){
						$docsJSON = file_get_contents($filepath, FILE_USE_INCLUDE_PATH);
                    	$c = json_decode($docsJSON,true);
						if(!empty($c)){
							if(!isset($c["_id"])){
								PHDB::update(Costum::COLLECTION,
									array("_id" => $v["_id"]) , 
									array('$set' => $c)
								);
								$retour .= "<span style='width:100%; padding:30px; background:green; color:white; font-size:20px;border-radius:5px;'>Bien mis à jour</span>";
							}else{
								$retour .= "<span style='width:100%; padding:30px; background:red; color:white; font-size:20px;border-radius:5px;'>Celui là je lui péte le doigt avec son object ID dans le json</span>";
							}
						}else{
							$retour .= "<span style='width:100%; padding:30px; background:orange; color:white; font-size:20px;border-radius:5px;'>JSON défectueux (contient des fautes de frappes, des commentaires, mauvaise structure...)</span>";
						}
					}else{
						$retour .= "<span style='width:100%; padding:30px; background:orange; color:white; font-size:20px;border-radius:5px;'>JSON non trouvé ".$filepath."</span>";
					}
				}else{
					$retour .= "<br/>-------------------------------<br/><br/>-------------------------------<br/><br/>-------------------------------<br/>".
					"<span style='width:100%; padding:30px; background:orange; border-radius:5px;'>".$k." n'a pas de slug </span>".
					"<br/>-------------------------------<br/>";
				}
			}
		}else{
			$retour = "Sorry bidiboys";
		}
		return $retour;*/
	}
	//Refactor permettant de mettre la size des doc en bytes type string 
	// Pas encore Passez en prod et dev
	// A lancer une fois pour que ce soit stocké en int32 ou int64
	public function actionChangeSizeDocumentToBytesNumber(){
		$str="";
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
			$document=PHDB::find(Document::COLLECTION);
			$nbDoc=count($document);
			$str.= "Nombre de documents appelés : ".$nbDoc;
			$i=0;
			foreach($document as $key => $data){
				if(@$data["size"]){
					$size="";
					$str.= "<br/>".$data["_id"]."//".$data["size"]."///";
					$str.= gettype($data["size"]);
					if(gettype($data["size"])=="double"){
						$size = (int)$data["size"];
					}
					if (strstr($data["size"], 'M', true)){
						$size=((float)$data["size"])*1048576;
					} 
					else if(strstr($data["size"], 'K', true)){
						$size = (float)($data["size"])*1024;
					}
					$i++;
					if(@$size && !empty($size)){
						$str.= "new size : ".$size;
						PHDB::update(Document::COLLECTION,
								array("_id" => $data["_id"]) , 
								array('$set' => array("size" => (int)$size))	
			
						);
					}	
				}
			}
			$str.= "</br>Nombre de documents traités pour la size : ".$i; 
		}
		return $str;
	}
	//Refactor of contentKey
	//@param string contentKey type type.view.contentKey become contentKey
	//!!!!!!!!!!!! CAREFULLY THIS METHOD IS FOR COMMUNECTER AND NOT FOR GRANDDIR !!!!!!!!!!!!!!!!!//////
	// For the moment refactorContentKey change all contentKey to profil 
	// Function need to be change with an explode and $contentKey = $explode[2] for granddir
	public function actionRefactorContentKey(){
		$str="";
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
			$document=PHDB::find(Document::COLLECTION);
			$nbDoc=count($document);
			$str.= "Nombre de documents appelés : ".$nbDoc;
			$i=0;
			foreach($document as $key => $data){
				if(strstr($data["contentKey"],'.')){
					$str.= $data["contentKey"]."<br/>";
					PHDB::update(Document::COLLECTION,
						array("_id" => $data["_id"]) , 
						array('$set' => array("contentKey" => "profil"))	
					);
					$i++;
				}
			}
			$str.= "</br>Nombre de documents concerné par le refactor : ".$i;  
		}
		return $str;
	}
	// Washing of docmuent
	// Wash data with array in params @size which could be string
	// Wash data with no type or no id, represent the target of the document
	// Wash data with no contentKey
	public function actionWashIncorrectAndOldDataDocument(){
		$str="";
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
			$document=PHDB::find(Document::COLLECTION);
			$nbDoc=count($document);
			$str.= "Nombre de documents appelés : ".$nbDoc;
			$nbDocSizeIsArray=0;
			$nbDocNoTypeOrNoId=0;
			$nbDocNoContentKey=0;
			foreach($document as $key => $data){
				if(gettype($data["size"])=="array"){
					$str.= "<br/>//////// This document content an array for size : <br/>";
					//print_r($data);
					PHDB::remove(Document::COLLECTION,array("_id" => $data["_id"]));
					$nbDocSizeIsArray++;
				}
				if( !@$data["type"] || !@$data["id"] || empty($data["type"]) || empty($data["id"])){
					$str.= "<br/>//////// This document doesn't content any type or id : <br/>";
					//print_r($data);
					PHDB::remove(Document::COLLECTION,array("_id" => $data["_id"]));
					$nbDocNoTypeOrNoId++;
				}
				if( !@$data["contentKey"] || empty($data["contentKey"])){
					$str.= "<br/>//////// This document doesn't content any contentKey : <br/>";
					//print_r($data);
					PHDB::remove(Document::COLLECTION,array("_id" => $data["_id"]));
					$nbDocNoContentKey++;
				}
			}
			$str.= "</br>//////// <br/>Nombre de documents sans type ou id: ".$nbDocNoTypeOrNoId; 
			$str.= "</br>//////// <br/>Nombre de documents sans contentKey: ".$nbDocNoContentKey;
			$str.= "</br>//////// <br/>Nombre de documents avec size comme array: ".$nbDocSizeIsArray;  
		}
		return $str;
	}
	/* 
	* Upload image from media type url content
	*	Condition: if not from communevent
	*	News uploadNewsImage
	*   Update link @param media.content.image in news collection
	*/
	public function actionUploadImageFromMediaUrlNews(){
		$str="";
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
		  $news=PHDB::find(News::COLLECTION);
		  $i=0;
		  foreach($news as $key => $data){
			  if(@$data["media"] && @$data["media"]["content"] && @$data["media"]["content"]["image"] && !@$data["media"]["content"]["imageId"]){
				  	if (strstr($data["media"]["content"]["image"],"upload/communecter/news/")==false){
				  		sleep(1);
				  		$str.= $data["media"]["content"]["image"]."<br/>";
				  		$returnUrl=News::uploadNewsImage($data["media"]["content"]["image"],$data["media"]["content"]["imageSize"],$data["author"]);
				  		$newUrl= Yii::app()->baseUrl."/".$returnUrl;
				  		$str.= 'Nouvelle url <br/>'.$newUrl."<br/>/////////////<br/>";
				  		PHDB::update(News::COLLECTION,
							array("_id" => $data["_id"]) , 
							array('$set' => array("media.content.image" => $newUrl))			
						);
				  		$i++;
			 		}
				}
			}
			$str.= "nombre de news avec images provenant d'un autre site////////////// ".$i;
		}
		return $str;
	}
	/* 
	* update type french instead of key
	*/
	public function actionUpdateTypeRessources(){
		$str="";
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
		  $ress=PHDB::find(Ressource::COLLECTION);
		  $i=0;
		  foreach($ress as $key => $data){
			  $label="";
			  if(@$data["type"]=="compétence")
			  		$label="competence";
			  else if(@$data["type"]=="matériel")
			  	$label="material";
			  if($label!=""){
				  		$str.= '=>Ressoruce id : '.$key."<br/>last type : ".$data["type"]."///// New type : ".$label."<br/>";
				  		PHDB::update(Ressource::COLLECTION,
							array("_id" => $data["_id"]) , 
							array('$set' => array("type" => $label))			
						);
				  		$i++;
			 		}
			}
			$str.= "nombre de ressource updaté type to clé////////////// ".$i;
		}
		return $str;
	}
	/* 
	* Scope public in news not well formated (ancient news)
	*	Condition: if not from communevent
	*	News uploadNewsImage
	*   Update link @param media.content.image in news collection
	*/
	public function actionBashRepareBulshit(){
		$str="";
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
		  $news=PHDB::find(News::COLLECTION);
		  $nbNews=count($news);
		  $i=0;
		  $newsWrong=0;
		  $nbNewsGood=0;
		  foreach($news as $key => $data){
			  	if($data["scope"]["type"]=="public"){
				  	if(!@$data["scope"]["cities"][0]){
					  	$newScopeArray=array("type"=>"public","cities"=>array());
					  	if($data["type"]=="activityStream"){
						  	$object=PHDB::findOne($data["object"]["objectType"],array("_id"=>new MongoId($data["object"]["id"])));
								$newScopeArray["cities"][0]["codeInsee"]=$object["address"]["codeInsee"];
								$newScopeArray["cities"][0]["postalCode"]=$object["address"]["postalCode"];
								$newScopeArray["cities"][0]["geo"]=$object["geo"];
								PHDB::update(News::COLLECTION,
											array("_id" => $data["_id"]) , 
											array('$set' => array("scope" => $newScopeArray)	
							));
							$newsWrong++;
					  	}
					  	/*else{
							if($data["target"]["type"]=="pixels"){
								$author=PHDB::findOne(Person::COLLECTION,array("_id"=>new MongoId($data["author"])));
								$newScopeArray["cities"][0]["codeInsee"]=$author["address"]["codeInsee"];
								$newScopeArray["cities"][0]["postalCode"]=$author["address"]["postalCode"];
								$newScopeArray["cities"][0]["geo"]=$author["geo"];
							}else {
								$target=PHDB::findOne($data["target"]["type"],array("_id"=>new MongoId($data["target"]["id"])));
								$newScopeArray["cities"][0]["codeInsee"]=$target["address"]["codeInsee"];
								$newScopeArray["cities"][0]["postalCode"]=$target["address"]["postalCode"];
								$newScopeArray["cities"][0]["geo"]=$target["geo"];
							}
							PHDB::update(News::COLLECTION,
											array("_id" => $data["_id"]) , 
											array('$set' => array("scope" => $newScopeArray)	
							));
						}*/

					}
					else{
						$nbNewsGood++;
					}
				}
			}
			$str.= "nombre total de news: ".$nbNews."news";
			$str.= "nombre de news mauvaise: ".$newsWrong."news";
			$str.= "nombre de news good: ".$nbNewsGood."news";
		}
		return $str;
	}
	/* 
	* Scope public in news not well formated (ancient news)
	*	Condition: if not from communevent
	*	News uploadNewsImage
	*   Update link @param media.content.image in news collection
	*/
	public function actionBashNewsWrongScope(){
		$str="";
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
		  $news=PHDB::find(News::COLLECTION);
		  $i=0;
		  $nbCityCodeInsee=0;
		  $nbCityName=0;
		  $nbtodelete=0;
		  $nbCityNotFindName=0;
		  foreach($news as $key => $data){
			  if($data["scope"]["type"]=="public"){
				  if(@$data["scope"]["cities"]){
					  	$newScopeArray=array("type"=>"public","cities"=>array());
					  	if(@$data["scope"]["cities"][0] && gettype($data["scope"]["cities"][0])=="string"){

						  	  	$str.= "<br/>////////id News: ".$key. "/////////";				  	  						  	//print_r($data["scope"]);
						  	foreach($data["scope"]["cities"] as $value){
							  	if(is_numeric($value)){
								  	$str.= "<br/>ici numérique:".$value;
								  	$city=PHDB::findOne(City::COLLECTION, array("insee" => $value));
								  	$newScopeArray["cities"][0]["codeInsee"]=$value;
								  	$newScopeArray["cities"][0]["postalCode"]=$city["postalCodes"][0]["postalCode"];
								  	if(@$data["geo"]){
									  	$newScopeArray["cities"][0]["geo"]=$data["geo"];
								  	}else{
									  	$newScopeArray["cities"][0]["geo"]=$city["geo"];
								  	}
								  	$nbCityCodeInsee++;
							  	} else {
								  	$str.= "<br/>ici non numérique mais string: ".$value;
								  	if($value=="LA RIVIERE"){
									  	$newScopeArray["cities"][0]["codeInsee"]="97414";
									  	$newScopeArray["cities"][0]["postalCode"]="97421";
									  	$newScopeArray["cities"][0]["geo"]=array('@type' => 'GeoCoordinates','latitude'=>'-21.25833300','longitude'=>'55.44166700');
									  	$nbCityName++;
								  	}
								  	else{
								  	$city = PHDB::findOne(City::COLLECTION, array("alternateName" =>$value));
								  		if(!empty($city)){
									  		$newScopeArray["cities"][0]["codeInsee"]=$city["insee"];
									  		$newScopeArray["cities"][0]["postalCode"]=$city["postalCodes"][0]["postalCode"];
									  		$newScopeArray["cities"][0]["geo"]=$city["geo"];
								  		$nbCityName++;
								  		}else{
								  			$str.= "ici";
								  			$newScopeArray["cities"][0]="wrong";
								  			$nbCityNotFindName++;	
								  		}
								  	}
							  	}
							  	$str.= "<br/>===>News array scope: ///<br/>";
								//print_r($newScopeArray);
							  	$str.= "<br/>";
						  	}
						  	PHDB::update(News::COLLECTION,
								array("_id" => $data["_id"]) , 
								array('$set' => array("scope" => $newScopeArray)			
							));
							$i++;
					  	} else {
						  	if (!@$data["scope"]["cities"][0])
							{
							 	$str.= "<br/>/////////////////// PAS DE 00000 ////////////////////<br/>";
							 	$insee=false;
							 	foreach($data["scope"]["cities"] as $value){
								 	if(is_numeric($value)){
									 	$insee=$value;
								 	}	
							 	}
							 	if($insee){
							 		$str.= "<br/>ici numérique:".$value;
									  	$city=PHDB::findOne(City::COLLECTION, array("insee" => $value));
									  	$newScopeArray["cities"][0]["codeInsee"]=$value;
									  	$newScopeArray["cities"][0]["postalCode"]=$city["postalCodes"][0]["postalCode"];
									  	if(@$data["geo"]){
										  	$newScopeArray["cities"][0]["geo"]=$data["geo"];
									  	}else{
										  	$newScopeArray["cities"][0]["geo"]=$city["geo"];
									  	}
									  	$nbCityCodeInsee++;
									  	$i++;
									//print_r($newScopeArray);
									$str.= "<br/>";
									PHDB::update(News::COLLECTION,
										array("_id" => $data["_id"]) , 
										array('$set' => array("scope" => $newScopeArray)			
									));
							 	}
							}
							
					  	}
				  } 
				  else{
					  $str.= "<br/>////news to delete avec wrong scope/////<br/>";
					  //print_r($data["scope"]);
					  $nbtodelete++;
					  $str.= "<br/>";
					   PHDB::remove(News::COLLECTION, array("_id"=>$data["_id"]));
					   $i++;
				  }
			}
			}
			$str.= "nombre de news avec insee enregistré: ".$nbCityCodeInsee."news";
			$str.= "nombre de news avec name enregistré: ".$nbCityName."news";
			$str.= "nombre de news à supprimer: ".$nbtodelete."news";
			$str.= "nombre de news avec city non trouvé: ".$nbCityNotFindName."news";
			$str.= "nombre de news avec data publique not well formated: ".$i."news";
		}
		return $str;
	}
	/* First refactor à faire sur communecter.org 
	* Remove all id and type in and object target.id, target.type
	*	=> Modify target type city to target.id=author, target.type=Person::COLLECTION
	*	=> Add @params type string "news" OR "activityStream"
	*/
	public function actionRefactorNews(){
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
		  $news=PHDB::find(News::COLLECTION);
		  $i=0;
		  foreach($news as $key => $data){
			  if(@$data["type"] && $data["type"]!="activityStream"){
				  ////print_r($data["_id"]);
				  // add une target au lieu de id et type et type devient news
				  // pour les type city => la target devient l'auteur
				  if($data["type"]!="news"){
					  if(@$data["id"]){
					  	$parentType=$data["type"];
					  	$parentId=$data["id"];
					  	if($parentType=="city"){
						  $parentType=Person::COLLECTION;
						  $parentId=$data["author"];
					  	}
					  PHDB::update(News::COLLECTION,
						array("_id" => $data["_id"]) , 
						array('$set' => array("target.type" => $parentType,"target.id"=>$parentId, "type" => "news"),'$unset' => array("id"=>""))			
						);
					$i++;
					} else if($data["type"]=="pixels"){
						PHDB::update(News::COLLECTION,
							array("_id" => $data["_id"]) , 
							array('$set' => array("target.type" => "pixels","target.id"=>"", "type" => "news"),'$unset' => array("id"=>""))			
						);
						$i++;
					}
				}
				 // //print_r($data);
			  }
			  if(@$data["type"] && $data["type"]=="activityStream"){
				  if(@$data["target"]){
				 	 //adapter le vocubulaire de target pour qu'il soit comment au news type "news"
				  	// passe target.objectType à target.type
					 $parentType=$data["target"]["objectType"];
					 // $parentId=$data["id"];
						  PHDB::update(News::COLLECTION,
							array("_id" => $data["_id"]) , 
							array('$set' => array("target.type" => $parentType),'$unset' => array("target.objectType"=>""))			
						);
									$i++;
					}
			  }
			}
			return "nombre de news ////////////// ".$i;
		}
	}

   // Second refactor à faire sur communecter.org qui permet de netoyer les news sans scope
  public function actionWashingNewsNoScopeType(){
	  if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
	  $news=PHDB::find(News::COLLECTION);
	  foreach($news as $key => $data){
			  if(!@$data["scope"]["type"]){
			  ////print_r($data);
			  PHDB::remove(News::COLLECTION, array("_id"=>new MongoId($key)));
			  	
			}
			}
		}
	}
	
	// Second refactor à faire sur communecter.org qui permet de netoyer les news sans scope
 	public function actionUpdateTypeInCollectionsLinkToClassifieds(){
 		$str = "";
	  	if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
	  		$str .= "--------------------------------------------------------------------------------<br/>".
	  			"---------------------------------------DOCUMENTS-------------------------------<br/>".
	  			"--------------------------------------------------------------------------------<br/>";
	  		
	  		$documents=PHDB::find(Document::COLLECTION,array('$or'=>array(array("type"=>"classified"), array("type"=>"ressources"))));
	  		$documentNb=0;
	  		foreach ($documents as $key => $value) {
	  			$str .= "Document modified :".$key."<br/>";
	  			$str .= "type:".$value["type"]." devient classifieds<br/>";
	  			$value["type"]="classifieds";
	  			$value["folder"]=preg_replace("/ressources/", "classifieds", $value["folder"]);
	  			$value["folder"]=preg_replace("/classified/", "classifieds", $value["folder"]);
	  			$str .= "folder:".$value["folder"]."<br/>";
	  			$str .= "---------------------------------------------------<br/>";
	  			PHDB::update(Document::COLLECTION,
					array("_id" => $value["_id"]) , 
					array('$set' => array("type" => $value["type"], "folder"=>$value["folder"]))			
				);
				$documentNb++;
	  		}
	  		$str .= "Nombre de documents modifier : ".$documentNb."<br/><br/><br/>";
	  		$str .= "--------------------------------------------------------------------------------<br/>".
	  			"------------------------------------NOTIFS--------------------------------------<br/>".
	  			"--------------------------------------------------------------------------------<br/>";
	  		$activityStream=PHDB::find(ActivityStream::COLLECTION,array("type"=>"notifications",'$or'=>array(array("target.type"=>"classified"), array("target.type"=>"ressources"))));
	  		$activityStreamNb=0;
	  		foreach ($activityStream as $key => $value) {
	  			$str .= "Notifs modified :".$key."<br/>";
	  			$str .= "type:".$value["target"]["type"]." devient classifieds<br/>";
	  			$targetType="classifieds";
	  			$set=array("target.type"=>"classifieds");
	  			if(@$value["notify"] && @$value["notify"]["url"]){
		  			$url=preg_replace("/ressources/", "classifieds", $value["notify"]["url"]);
		  			$url=preg_replace("/classified/", "classifieds", $value["notify"]["url"]);
		  			$str .= "notify.url:".$url."<br/>";
		  			$set["notify.url"]=$url;
	  			}
	  			//print_r($set);
	  			$str .= "---------------------------------------------------<br/>";
	  			PHDB::update(ActivityStream::COLLECTION,
					array("_id" => $value["_id"]) , 
					array('$set' => $set)			
				);
				$activityStreamNb++;
	  		}
	  		$str .= "Nombre de notifications modifier : ".$activityStreamNb."<br/><br/><br/>";
	  		$str .= "--------------------------------------------------------------------------------<br/>".
	  			"---------------------------------COMMENTS------------------------------------------<br/>".
	  			"--------------------------------------------------------------------------------<br/>";
	  		$comments=PHDB::find(Comment::COLLECTION,array('$or'=>array(array("contextType"=>"classified"), array("contextType"=>"ressources"))));
	  		$commentsNb=0;
	  		foreach ($comments as $key => $value) {
	  			$str .= "Comment modified :".$key."<br/>";
	  			$str .= "contextType:".$value["contextType"]." devient classifieds<br/>";
	  			//$targetType="classifieds";
	  			$set=array("contextType"=>"classifieds");
	  			//print_r($set);
	  			$str .= "---------------------------------------------------<br/>";
	  			PHDB::update(Comment::COLLECTION,
					array("_id" => $value["_id"]) , 
					array('$set' => $set)			
				);
				$commentsNb++;
	  		}
	  		$str .= "Nombre de comments modifier : ".$commentsNb."<br/><br/><br/>";
	  		$str .= "--------------------------------------------------------------------------------<br/>".
	  			"---------------------------------NEWS------------------------------------------<br/>".
	  			"--------------------------------------------------------------------------------<br/>";
	  		$news=PHDB::find(News::COLLECTION,array('$or'=>array(array("object.type"=>"classified"), array("object.type"=>"ressources"))));
	  		$newsNb=0;
	  		foreach ($news as $key => $value) {
	  			$str .= "News modified :".$key."<br/>";
	  			$str .= "contextType:".$value["object"]["type"]." devient classifieds<br/>";
	  			//$targetType="classifieds";
	  			$set=array("object.type"=>"classifieds");
	  			//print_r($set);
	  			$str .= "---------------------------------------------------<br/>";
	  			PHDB::update(News::COLLECTION,
					array("_id" => $value["_id"]) , 
					array('$set' => $set)			
				);
				$newsNb++;
	  		}
	  		$str .= "Nombre de news modifier : ".$newsNb;
		}else
			$str .= "hi han dis l'ane";

		return $str;
	}
	// Refactor classifieds
  	public function actionUpdateDateInDB(){
	  	if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
		  	$classifieds=PHDB::find(Classified::COLLECTION);
		  	foreach($classifieds as $key => $v){
		  		PHDB::update(Classified::COLLECTION,
					array("_id" => $v["_id"]) , 
					array('$set' => array("modified"=>new MongoDate($v["modified"]["sec"])))			
				);
		  	}
		  	return "done mon petit";
		}
	}
	// Refactor classifieds
  	public function actionUpdateLineInDB(){
	  	if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
		  	$classifieds=PHDB::find(Classified::COLLECTION);
		  	foreach($classifieds as $key => $v){
		  		if(is_array($v["updated"])){
		  			PHDB::update(Classified::COLLECTION,
					array("_id" => $v["_id"]) , 
					array('$set' => array("updated"=> $v["updated"]["sec"]))			
					);
		  		}
		  		/*PHDB::update(Classified::COLLECTION,
					array("_id" => $v["_id"]) , 
					array('$set' => array("updated"=> strtotime ($v["updated"]["sec"]), "created"=>strtotime ($v["created"]["sec"])))			
				);*/
		  	}
		  	return "impressionnant";
		}
	}
	// Refactor classifieds
  	public function actionUpdateClassifiedAndMergeRessources(){
	  	if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
		  	$classifieds=PHDB::find(Classified::COLLECTION);
		  	$allClassifieds=[];
		  	foreach($classifieds as $key => $data){
		  		$data["_id"]=array('$oid'=>(string)$data["_id"]);
				if(@$data["section"]=="job"){
					$data["subtype"]=strtolower($data["type"]);
					$data["type"]="jobs";
					$data["section"]="offer";
					$data["category"]="joboffer";
					if(@$data["profilImageUrl"]) $data["profilImageUrl"]=preg_replace("/classified/", "classifieds", $data["profilImageUrl"]);
					if(@$data["profilThumbImageUrl"]) $data["profilThumbImageUrl"]=preg_replace("/classified/", "classifieds", $data["profilThumbImageUrl"]);
					if(@$data["profilMarkerImageUrl"]) $data["profilMarkerImageUrl"]=preg_replace("/classified/", "classifieds", $data["profilMarkerImageUrl"]);
					if(@$data["profilMediumImageUrl"]) $data["profilMediumImageUrl"]=preg_replace("/classified/", "classifieds", $data["profilMediumImageUrl"]);
				  	////print_r($data);
				  	//$str .= "----------------------------------------<br/><br/>";
				  	array_push($allClassifieds, $data);
				  	//PHDB::remove(News::COLLECTION, array("_id"=>new MongoId($key)));
				} else if($data["section"]=="Vendre" || $data["section"]=="forsale"){
					//$str .= "Classified type classifieds (for sale) :".$key."<br/><br/>";
					$data["category"]=strtolower($data["type"]);
					$data["type"]="classifieds";
					$data["section"]="forsale";
					$data["subtype"]=strtolower(@$data["subtype"]);
					////print_r($data);
					if(@$data["profilImageUrl"]) $data["profilImageUrl"]=preg_replace("/classified/", "classifieds", $data["profilImageUrl"]);
					if(@$data["profilThumbImageUrl"]) $data["profilThumbImageUrl"]=preg_replace("/classified/", "classifieds", $data["profilThumbImageUrl"]);
					if(@$data["profilMarkerImageUrl"]) $data["profilMarkerImageUrl"]=preg_replace("/classified/", "classifieds", $data["profilMarkerImageUrl"]);
					if(@$data["profilMediumImageUrl"]) $data["profilMediumImageUrl"]=preg_replace("/classified/", "classifieds", $data["profilMediumImageUrl"]);
					array_push($allClassifieds, $data);
				  	//$str .= "----------------------------------------<br/><br/>";
				}else if($data["section"]=="Louer" || $data["section"]=="forrent"){
					//$str .= "Classified type classifieds (for rent) :".$key."<br/><br/>";
					$data["category"]=strtolower($data["type"]);
					$data["type"]="classifieds";
					$data["section"]="forrent";
					$data["subtype"]=strtolower(@$data["subtype"]);
					////print_r($data);
					if(@$data["profilImageUrl"]) $data["profilImageUrl"]=preg_replace("/classified/", "classifieds", $data["profilImageUrl"]);
					if(@$data["profilThumbImageUrl"]) $data["profilThumbImageUrl"]=preg_replace("/classified/", "classifieds", $data["profilThumbImageUrl"]);
					if(@$data["profilMarkerImageUrl"]) $data["profilMarkerImageUrl"]=preg_replace("/classified/", "classifieds", $data["profilMarkerImageUrl"]);
					if(@$data["profilMediumImageUrl"]) $data["profilMediumImageUrl"]=preg_replace("/classified/", "classifieds", $data["profilMediumImageUrl"]);
				  	
					array_push($allClassifieds, $data);
					//$str .= "----------------------------------------<br/><br/>";
				}else if($data["section"]=="Donner" || $data["section"]=="donation" || $data["section"]=="Partager" || $data["section"]=="sharing"){
					//$str .= "Classified type ressources (offer) :".$key."<br/><br/>";
					$data["category"]="isToImplemant";
					$data["subtype"]="isToImplemant";
					$data["type"]="ressources";
					$data["section"]="offer";
					if(@$data["profilImageUrl"]) $data["profilImageUrl"]=preg_replace("/classified/", "classifieds", $data["profilImageUrl"]);
					if(@$data["profilThumbImageUrl"]) $data["profilThumbImageUrl"]=preg_replace("/classified/", "classifieds", $data["profilThumbImageUrl"]);
					if(@$data["profilMarkerImageUrl"]) $data["profilMarkerImageUrl"]=preg_replace("/classified/", "classifieds", $data["profilMarkerImageUrl"]);
					if(@$data["profilMediumImageUrl"]) $data["profilMediumImageUrl"]=preg_replace("/classified/", "classifieds", $data["profilMediumImageUrl"]);
				  	
					if(@$data["devise"]) unset($data["devise"]);
					////print_r($data);
					array_push($allClassifieds, $data);
					//$str .= "----------------------------------------<br/><br/>";
				}else if($data["section"]=="lookingfor" || $data["section"]=="Besoin"){
					//$str .= "Classified type ressources (need) :".$key."<br/><br/>";
					$data["category"]="isToImplemant";
					$data["subtype"]="isToImplemant";
					$data["type"]="ressources";
					$data["section"]="need";
					if(@$data["devise"]) unset($data["devise"]);
					////print_r($data);
					if(@$data["profilImageUrl"]) $data["profilImageUrl"]=preg_replace("/classified/", "classifieds", $data["profilImageUrl"]);
					if(@$data["profilThumbImageUrl"]) $data["profilThumbImageUrl"]=preg_replace("/classified/", "classifieds", $data["profilThumbImageUrl"]);
					if(@$data["profilMarkerImageUrl"]) $data["profilMarkerImageUrl"]=preg_replace("/classified/", "classifieds", $data["profilMarkerImageUrl"]);
					if(@$data["profilMediumImageUrl"]) $data["profilMediumImageUrl"]=preg_replace("/classified/", "classifieds", $data["profilMediumImageUrl"]);
				  	
					array_push($allClassifieds, $data);
					//$str .= "----------------------------------------<br/><br/>";
				}
				$str .= json_encode($data);
			}
			$ressources=PHDB::find(Ressource::COLLECTION);
			foreach($ressources as $key => $data){
				$data["_id"]=array('$oid'=>(string)$data["_id"]);
				//$str .= "Classified type ressources :".$key."<br/><br/>";
				$data["category"]=$data["type"];
				$data["type"]="ressources";
				if(@$data["profilImageUrl"]) $data["profilImageUrl"]=preg_replace("/ressources/", "classifieds", $data["profilImageUrl"]);
				if(@$data["profilThumbImageUrl"]) $data["profilThumbImageUrl"]=preg_replace("/ressources/", "classifieds", $data["profilThumbImageUrl"]);
				if(@$data["profilMarkerImageUrl"]) $data["profilMarkerImageUrl"]=preg_replace("/ressources/", "classifieds", $data["profilMarkerImageUrl"]);
				if(@$data["profilMediumImageUrl"]) $data["profilMediumImageUrl"]=preg_replace("/ressources/", "classifieds", $data["profilMediumImageUrl"]);
				  	
			  	////print_r($data);
			  	//$str .= "----------------------------------------<br/><br/>";
			  	array_push($allClassifieds, $data);
				// $str .= json_encode($data);
			}
			//$str .= "<br/><br/><br/><hr><hr/><h1>JSON CLASSIFIEDS GENERATE</h1><br/><br/><br/>";
			//$str .= json_encode($allClassifieds);
		}else{
			return "hello l'artiste !! What's up baby ?";
		}
	}
   // Troisième refactor à faire sur communecter.org qui permet de netoyer les news sans target
   	
   	public function actionWashingNewsNoTarget(){
	  	if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
	  		$news=PHDB::find(News::COLLECTION);
	  		$i=0;
	  		$nbNews=count($news);
			$str .= "Nombre de documents appelés : ".$nbNews;
	  		foreach($news as $key => $data){
			  if($data["type"]=="news" && !@$data["target"]){
				  $i++;
				  //print_r($data);
				  PHDB::remove(News::COLLECTION, array("_id"=>new MongoId($key)));
			 // PHDB::remove(News::COLLECTION, array("_id"=>new MongoId($key)));
			  	
				}
				else if ($data["type"]=="activityStream" && !@$data["target"]){
				$i++;
				  //print_r($data);
				PHDB::update(News::COLLECTION,
							array("_id" => $data["_id"]) , 
							array('$set' => array("target.type" => Person::COLLECTION,"target.id"=>$data["author"])));
				  //PHDB::update(News::COLLECTION, array("_id"=>new MongoId($key)));
			 // PHDB::remove(News::COLLECTION, array("_id"=>new MongoId($key)));
			  	
				}
			}
			return "nombre de news traitées ////////////// ".$i;
		}
	}
	// Delete news with object gantts and needs
	public function actionDeleteNewsGanttsNeeds(){
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
		$newsNeeds=PHDB::find(News::COLLECTION,array("type"=>"activityStream","object.objectType"=>"needs"));
		$newsGantts=PHDB::find(News::COLLECTION,array("type"=>"activityStream","object.objectType"=>"gantts"));  		$i=0;	
	  		foreach($newsNeeds as $key => $data){
			  //if(!@$data["target"]){
				  //print_r($data);
				  PHDB::remove(News::COLLECTION, array("_id"=>new MongoId($key)));
			 // PHDB::remove(News::COLLECTION, array("_id"=>new MongoId($key)));
			 	$i++;
				//}
			}
			foreach($newsGantts as $key => $data){
				//if(!@$data["target"]){
				//print_r($data);
				PHDB::remove(News::COLLECTION, array("_id"=>new MongoId($key)));
				// PHDB::remove(News::COLLECTION, array("_id"=>new MongoId($key)));
				$i++;
				//}
			}
			return "Nombre de news gantts ou needs suprimées : ".$i." news";
			
		}
	}
	// Quatrième refactor à faire sur communecter.org qui permet de netoyer les news dont la target n'existe pas
	public function actionWashingNewsTargetNotExist(){
	  	if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
	  		$news=PHDB::find(News::COLLECTION);
	  		foreach($news as $key => $data){
			  	if(@$data["target"]){
					if(!@$data["target"]["type"]){
						if(@$data["target"]["objectType"]){
							$parentType=$data["target"]["objectType"];
							PHDB::update(News::COLLECTION,
								array("_id" => $data["_id"]) , 
								array('$set' => array("target.type" => $parentType),
									'$unset' => array("target.objectType"=>""))			
							);
						} else{
							PHDB::remove(News::COLLECTION, array("_id"=>new MongoId($key))); 
						}
				  }
				  else if($data["target"]["type"]==Person::COLLECTION){
				  	$target = Person::getById($data["target"]["id"]);
				  	if (empty($target)){
					  	//print_r($data);
				  		PHDB::remove(News::COLLECTION, array("_id"=>new MongoId($key))); 
				  	}
				  }
				  else if($data["target"]["type"]==Event::COLLECTION){
				  	$target = Event::getById($data["target"]["id"]);
				  	if (empty($target)){
					  	//print_r($data);
				  		PHDB::remove(News::COLLECTION, array("_id"=>new MongoId($key))); 
				  	}
				  }
				  else if($data["target"]["type"]==Organization::COLLECTION){
				  	$target = Organization::getById($data["target"]["id"]);
				  	if (empty($target)){
					  	//print_r($data);
				  		PHDB::remove(News::COLLECTION, array("_id"=>new MongoId($key))); 
				  	}

				  }
				  else if($data["target"]["type"]==Project::COLLECTION){
				  	$target = Project::getById($data["target"]["id"]);
				  	if (empty($target)){
				  		//print_r($data);
				  		PHDB::remove(News::COLLECTION, array("_id"=>new MongoId($key))); 
				  	}
				  }	  
				}
			  	else {
				  	//print_r($data);
				  	PHDB::update(News::COLLECTION,
								array("_id" => $data["_id"]) , 
								array('$set' => array("target.type" => Person::COLLECTION,
												"target.id" => $data["author"])
									)
					);
			  	}
			}
		}
		return "done";
	}
// Cinquième refactor à faire sur communecter.org qui permet de netoyer les news type activityStream dont l'object n'existe pas
	public function actionWashingNewsObjectNotExist(){
	  	if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
	  		$news=PHDB::find(News::COLLECTION);
	  		$i=0;
	  		$nbNews=count($news);
			$str .= "Nombre de documents appelés : ".$nbNews;
	  		foreach($news as $key => $data){
		  		if($data["type"]=="activityStream"){
			  		if(@$data["object"]){
					  if($data["object"]["objectType"]==Event::COLLECTION){
					  	$target = Event::getById($data["target"]["id"]);
					  	if (empty($target)){
						  	//print_r($data);
						  	$i++;
					  		//PHDB::remove(News::COLLECTION, array("_id"=>new MongoId($key))); 
					  	}
					  }
					  else if($data["object"]["objectType"]==Organization::COLLECTION){
					  	$target = Organization::getById($data["target"]["id"]);
					  	if (empty($target)){
						  	//print_r($data);
						  						  	$i++;
					  		//PHDB::remove(News::COLLECTION, array("_id"=>new MongoId($key))); 
					  	}
		
					  }
					  else if($data["object"]["objectType"]==Project::COLLECTION){
					  	$target = Project::getById($data["target"]["id"]);
					  	if (empty($target)){
					  		//print_r($data);
					  							  	$i++;
					  		//PHDB::remove(News::COLLECTION, array("_id"=>new MongoId($key))); 
					  	}
					  }	  
					 }
			  	}
			}
			return "Nombre de news sans object traitées : ".$i." news";
		}
	}

	public function actionNewsPixels(){
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
			$news=PHDB::find(News::COLLECTION);
	  		$i=0;
	  		$nbNews=count($news);
			$str .= "Nombre de documents appelés : ".$nbNews;
			foreach($news as $key => $data){
				if($data["type"]=="pixels"){
					PHDB::update(News::COLLECTION,
						array("_id" => $data["_id"]) , 
						array('$set' => array("target.type" => "pixels","target.id"=>"", "type" => "news"),'$unset' => array("id"=>""))			
					);
					$i++;
				}
			}
			return "Nombre de news pixels traitées : ".$i." news";
		}
	}
	// VoteDown
  	public function actionRefactorModerateVoteDown(){
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
		  	$str .= "actionRefactorModerateVoteDown => ";
			$news=PHDB::find(News::COLLECTION, array('voteDown' => array('$exists' => 1),'refactorAction' => array('$exists' => 0)));
			$i=0;
			$str .= count($news)." News en base avec voteDown<br/>";
			foreach($news as $key => $data){
				$map = array();
				foreach ($data['voteDown'] as $j => $reason) {
					if(!is_array($reason))$map['voteDown.'.$reason] = array('date' => new MongoDate(time())); 
				}
				if(count($map)){
					$res = PHDB::update('news', array('_id' => $data['_id']), array('$set' => array('refactorNews' => new MongoDate(time()))));

					$res = PHDB::update('news', array('_id' => $data['_id']), array('$unset' => array('voteDown' => 1)));
					$res = PHDB::update('news', array('_id' => $data['_id']), array('$set' => $map, '$unset' => array('voteDownReason' => 1)));
					$i++;
				}
				elseif(isset($news['voteDownReason'])){
					$res = PHDB::update('news', array('_id' => $data['_id']), array('$unset' => array('voteDownReason' => 1)));
					$i++;
				}
			}

			return "nombre de news modifié => ".$i;
		}
	}

	// VoteUp
  	public function actionRefactorModerateVoteUp(){
  		$str = "";
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
		  	$str .= "actionRefactorModerateVoteUp => ";
			$news=PHDB::find(News::COLLECTION, array('voteUp' => array('$exists' => 1),'refactorNews' => array('$exists' => 0)));
			$i=0;
			$str .= count($news)." News en base avec voteUp<br/>";
			foreach($news as $key => $data){
				$map = array();
				foreach ($data['voteUp'] as $j => $reason) {
					if(!is_array($reason))$map['voteUp.'.$reason] = array('date' => new MongoDate(time())); 
				}
				if(count($map)){
					$res = PHDB::update('news', array('_id' => $data['_id']), array('$set' => array('refactorNews' => new MongoDate(time()))));
					$res = PHDB::update('news', array('_id' => $data['_id']), array('$unset' => array('voteUp' => 1)));
					$res = PHDB::update('news', array('_id' => $data['_id']), array('$set' => $map, '$unset' => array('voteUpReason' => 1)));
					$i++;
				}
				elseif(isset($news['voteUpReason'])){
					$res = PHDB::update('news', array('_id' => $data['_id']), array('$unset' => array('voteUpReason' => 1)));
					$i++;
				}
			}

			$str .= "nombre de news modifié => ".$i;
	  	}
	  	return $str;
	  }


	  // ReportAbuse
	public function actionRefactorModerateReportAbuse(){
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
		  	
		  	$i = 0;
			$news=PHDB::find(News::COLLECTION, array('reportAbuseReason' => array('$exists' => 1)));
		  	foreach($news as $key => $data){
				$res = PHDB::update('news', array('_id' => $data['_id']), array('$unset' => array('reportAbuseReason' => 1)));
				$res = PHDB::update('news', array('_id' => $data['_id']), array('$unset' => array('reportAbuseCount' => 1)));
				$res = PHDB::update('news', array('_id' => $data['_id']), array('$unset' => array('reportAbuse' => 1)));
				$i++;
			}

			return "actionRefactorModerateReportAbuse => ".count($news)." News en base avec reportAbuseReason<br/>";
		}
	}



	/*public function actionUpdateRegion(){
if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){

		$newsRegions = array(
						//array("new_code","new_name","former_code","former_name"),
						array("01","Guadeloupe","01","Guadeloupe"),
						array("02","Martinique","02","Martinique"),
						array("03","Guyane","03","Guyane"),
						array("04","La Réunion","04","La Réunion"),
						array("06","Mayotte","06","Mayotte"),
						array("11","Île-de-France","11","Île-de-France"),
						array("24","Centre-Val de Loire","24","Centre"),
						array("27","Bourgogne-Franche-Comté","26","Bourgogne"),
						array("27","Bourgogne-Franche-Comté","43","Franche-Comté"),
						array("28","Normandie","23","Haute-Normandie"),
						array("28","Normandie","25","Basse-Normandie"),
						array("32","Nord-Pas-de-Calais-Picardie","31","Nord-Pas-de-Calais"),
						array("32","Nord-Pas-de-Calais-Picardie","22","Picardie"),
						array("44","Alsace-Champagne-Ardenne-Lorraine","41","Lorraine"),
						array("44","Alsace-Champagne-Ardenne-Lorraine","42","Alsace"),
						array("44","Alsace-Champagne-Ardenne-Lorraine","21","Champagne-Ardenne"),
						array("52","Pays de la Loire","52","Pays de la Loire"),
						array("53","Bretagne","53","Bretagne"),
						array("75","Aquitaine-Limousin-Poitou-Charentes","72","Aquitaine"),
						array("75","Aquitaine-Limousin-Poitou-Charentes","54","Poitou-Charentes"),
						array("75","Aquitaine-Limousin-Poitou-Charentes","74","Limousin"),
						array("76","Languedoc-Roussillon-Midi-Pyrénées","73","Midi-Pyrénées"),
						array("76","Languedoc-Roussillon-Midi-Pyrénées","91","Languedoc-Roussillon"),
						array("84","Auvergne-Rhône-Alpes","82","Rhône-Alpes"),
						array("84","Auvergne-Rhône-Alpes","83","Auvergne"),
						array("93","Provence-Alpes-Côte d'Azur","93","Provence-Alpes-Côte d'Azur"),
						array("94","Corse","94","Corse")
					);

		foreach ($newsRegions as $key => $region){

			$str .= "News : (".$region[0].") ".$region[1]." ---- Ancien : (".$region[2].") ".$region[3]."</br>" ;

			$cities = PHDB::find(City::COLLECTION,array("region" => $region[2]));
			$res = array("result" => false , "msg" => "La région (".$region[0].") ".$region[1]." n'existe pas");
			
			if(!empty($cities)){

				/*$res = PHDB::updateWithOption( City::COLLECTION, 
									  	array("region"=> $region[2]),
				                        array('$set' => array(	"region" => $region[0],
				                        						"regionName" => $region[1])),
				                        array("multi"=> true)
				                    );
				foreach ($cities as $key => $city) {
					$res = PHDB::update( City::COLLECTION, 
									  	array("_id"=>new MongoId($key)),
				                        array('$set' => array(	"region" => $region[0],
				                        						"regionName" => $region[1]))
				                    );

				}
				
				var_dump($res);
				$str .= "</br></br></br>";

			}
			else
				$str .= "Result : ".$res["result"]." | ".$res["msg"]."</br></br></br>";

		}

	}*/

	public function actionUpdateUserName() {
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
			//add a suffle username for pending users event if they got one
			$pendingUsers = PHDB::find(Person::COLLECTION, array("pending" => true));
			$nbPendingUser = 0;
			foreach ($pendingUsers as $key => $person) {
				$res = PHDB::update( Person::COLLECTION, 
										  	array("_id"=>new MongoId($key)),
					                        array('$set' => array(	
					                        			"username" => substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyz"), 0, 32)), 
					                        			'$addToSet' => array("modifiedByBatch" => array("updateUserName" => new MongoDate(time()))))

					                    );
				$nbPendingUser++;
			}
			return "Number of pending user with username modified : ".$nbPendingUser;
		}
	}



	public function actionUpdatePreferences() {
		$str = "";
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
			$nbUser = 0;
			$preferencesUsers = array(
								"publicFields" => array(),
								"privateFields" => array("email", "streetAddress", "phone", "directory", "birthDate"),
								"isOpenData" => false );
			$users = PHDB::find(Person::COLLECTION, array());
			foreach ($users as $key => $person) {
				$person["modifiedByBatch"][] = array("updatePreferences" => new MongoDate(time()));
				$res = PHDB::update(Person::COLLECTION, 
											  	array("_id"=>new MongoId($key)),
						                        array('$set' => array(	"seePreferences" => true,
						                        						"preferences" => $preferencesUsers,
						                        						"modifiedByBatch" => $person["modifiedByBatch"])
						                        					)
						                    );

				if($res["ok"] == 1){
					$nbUser++;
				}else{
					$str .= "<br/> Error with user id : ".$key;
				}
			}

			$str .= "Number of user with preferences modified : ".$nbUser;
		}
		return $str;
	}

	public function actionUpdateCitiesBelgique() {
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
			/*$cities = PHDB::find(City::COLLECTION, array("country" => "BEL"));

			foreach ($cities as $key => $city) {
				$res = PHDB::update( City::COLLECTION, 
						  	array("_id"=>new MongoId($key)),
	                        array('$set' => array(	"country" => "BE",
													"insee" => substr($city["insee"], 0, 5)."*BE",
													"dep" => substr($city["dep"], 0, 2)."*BE",
													"region" => substr($city["region"], 0, 2)."*BE"))

	                    );
			}*/

			$types = array(Person::COLLECTION, Organization::COLLECTION, Project::COLLECTION, Event::COLLECTION);

			foreach ($types as $keyType => $type) {
				$elts = PHDB::find($type, array("address.addressCountry" => "BEL"));

				foreach ($elts as $key => $elt) {
					if(!empty($elt["address"]["codeInsee"])){
						$newAddress = $elt["address"];
						$newAddress["addressCountry"] = "BE";
						$newAddress["codeInsee"] = substr($newAddress["codeInsee"], 0, 5)."*BE";

						$res = PHDB::update($type, 
							  	array("_id"=>new MongoId($key)),
		                        array('$set' => array(	"address" => $newAddress ))
		                    );
					}
					
				}
			}
			return "good" ;
			
		}
	}


	public function actionCheckNameBelgique(){
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
			$cities = PHDB::find(City::COLLECTION, array("country" => "BE"));
			$nbcities = 0 ;
			foreach ($cities as $key => $city) {
				$name = $city["name"];
				$find = false ;
				if(count($city["postalCodes"]) == 1){
					

					foreach ($city["postalCodes"] as $keyCP => $cp) {
						if(trim($cp["name"]) != trim($name)){
							$find =true;
							$cp["name"] = $name ;
							$postalCodes[$keyCP] =  $cp ;
						}


					}

					if($find == true){
						$nbcities ++ ;
						$res = PHDB::update( City::COLLECTION, 
						  	array("_id"=>new MongoId($key)),
	                        array('$set' => array(	"postalCodes" => $postalCodes ))

	                    );
					}
				}			
			}
			return  "NB Cities : " .$nbcities."<br>" ;

		}
	}


	public function actionAddGeoPosition(){
		$str = "";
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
			$types = array(Person::COLLECTION, Organization::COLLECTION, Project::COLLECTION, Event::COLLECTION);
			$nbelement = 0 ;
			foreach ($types as $keyType => $type) {

				$elements = PHDB::find($type, array("geoPosition" => array('$exists' => 0), "geo" => array('$exists' => 1)));
				foreach ($elements as $key => $elt) {
					if(!empty($elt["geo"])){
						if(!empty($elt["geo"]["longitude"]) && !empty($elt["geo"]["latitude"])){
							$geoPosition = array("type"=>"Point", 
											"coordinates" => array(floatval($elt["geo"]["longitude"]), floatval($elt["geo"]["latitude"])));
							$elt["modifiedByBatch"][] = array("addGeoPosition" => new MongoDate(time()));
							$res = PHDB::update( $type, 
							  	array("_id"=>new MongoId($key)),
		                        array('$set' => array(	"geoPosition" => $geoPosition,
		                        						"modifiedByBatch" => $elt["modifiedByBatch"])), 
		                        array('upsert' => true ));
		                    $nbelement ++ ;
						}else{
							$str .=  $type." id : " .$key." : pas de longitude ou de latitude<br>" ;
						}	
					}else{
						$str .=  $type." id : " .$key." : pas de geo <br>" ;
					}


				}

			}		
			$str .=  "NB Element mis à jours: " .$nbelement."<br>" ;

		}
		return $str;
	}


	public function actionDeleteLinksHimSelf(){
		$str = "";
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
			$types = array(Person::COLLECTION, Organization::COLLECTION, Project::COLLECTION, Event::COLLECTION);
			$nbelement = 0 ;
			foreach ($types as $keyType => $type) {
				$elements = PHDB::find($type, array("links" => array('$exists' => 1)));
				foreach ($elements as $keyElt => $elt) {
					if(!empty($elt["links"])){
						$find = false;
						$newLinks = array();
						foreach(@$elt["links"] as $typeLinks => $links){
							if(array_key_exists ($keyElt , $links)){
								$find = true;
			                    unset($links[$keyElt]);
							}
							$newLinks[$typeLinks] = $links;
						}

						if($find == true){
							$nbelement ++ ;
							$elt["modifiedByBatch"][] = array("deleteLinksHimSelf" => new MongoDate(time()));
							$res = PHDB::update( $type, 
							  	array("_id"=>new MongoId($keyElt)),
		                        array('$set' => array(	"links" => $newLinks,
		                        						"modifiedByBatch" => $elt["modifiedByBatch"])));
							$str .= "Suppression du link pour le type : ".$type." et l'id ".$keyElt;
						}
					}
				}
			}		
			$str .=  "NB Element mis à jours: " .$nbelement."<br>" ;
		}
		return $str;
	}


	public function actionUpdateCitiesBelgiqueGeo() {
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
			ini_set('memory_limit', '-1');
			$cities = PHDB::find(City::COLLECTION, array("country" => "BE"));
			$nbelement= 0 ;
			foreach ($cities as $key => $city) {
				$find = false ;
				$newCPs = array();
				foreach ($city["postalCodes"] as $keyPC => $cp) {
					if(empty($cp["geo"])){
						$find = true ;
						$cp["geo"] = $city["geo"];
						$cp["geoPosition"] = $city["geoPosition"];
					}
					$newCPs[] = $cp;
				}
				if($find == true){
					$nbelement ++ ;
					$res = PHDB::update( City::COLLECTION, 
				  		array("_id"=>new MongoId($key)),
	                	array('$set' => array("postalCodes" => $newCPs)));
				}
			}
			return  "NB Element mis à jours: " .$nbelement."<br>" ;
		}
	}


	public function actionDeleteLinksDeprecated(){
		$str = "";
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
			$types = array(Person::COLLECTION, Organization::COLLECTION, Project::COLLECTION, Event::COLLECTION);
			$nbelement = 0 ;
			foreach ($types as $keyType => $type) {
				$elements = PHDB::find($type, array("links" => array('$exists' => 1)));
				foreach (@$elements as $keyElt => $elt) {
					if(!empty($elt["links"])){
						$find = false;
						$newLinks = array();
						foreach(@$elt["links"] as $typeLinks => $links){

							foreach(@$links as $keyLink => $link){
								if(!empty($link["type"])){
									$eltL = PHDB::find($link["type"], array("_id"=>new MongoId($keyLink)));
									if(empty($eltL)){
										$find = true;
					                    unset($links[$keyLink]);
									}
									$newLinks[$typeLinks] = $links;
								}
							}
						}
						if($find == true){
							$nbelement ++ ;
							$elt["modifiedByBatch"][] = array("deleteLinksDeprecated" => new MongoDate(time()));
							$res = PHDB::update( $type, 
							  	array("_id"=>new MongoId($keyElt)),
		                        array('$set' => array(	"links" => $newLinks,
		                        						"modifiedByBatch" => $elt["modifiedByBatch"])));
							$str .= "Suppression de link  deprecated pour le type : ".$type." et l'id ".$keyElt."<br>" ;
						}
					}
				}
			}		
			$str .=  "NB Element mis à jours: " .$nbelement."<br>" ;
		}
		return $str;
	}

	public function actionRefactorChartProjectData(){
		$str = "";
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
			$projects = PHDB::find(Project::COLLECTION, array("properties.chart" => array('$exists' => 1)));
			foreach($projects as $data){
				$str .= "////////// <br/>";
				$str .= (string)$data["_id"]."<br/>";
				$chart=array();
				$chart["open"]=array();
				foreach($data["properties"]["chart"] as $key => $value){
					$values=array("description"=>"", "value" => $value);
					$chart["open"][$key]=array();
					$chart["open"][$key]=$values;
					//$str .= $value."<br/>";
				}
				$str .= "NEW OBJECT<br/>";
				//print_r($chart);
				PHDB::update(Project::COLLECTION,
					array("_id" => new MongoId((string)$data["_id"])),
					array('$set' => array("properties.chart"=> $chart))
				);

				$str .= "////////// <br/>";
			}
		}
		return $str;
	}

	public function actionRemovePropertiesOfOrganizations(){
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
			$organizations = PHDB::find(Organization::COLLECTION, array("properties.chart" => array('$exists' => 1)));
			$nbOrgaDealWith=0;
			foreach($organizations as $data){
				PHDB::update(Organization::COLLECTION,
					array("_id" => new MongoId((string)$data["_id"])),
					array('$unset' => array("properties"=> ""))
				);
				$nbOrgaDealWith++;
			}	
			return "nombre d'organizations traitées : ".$nbOrgaDealWith;
		}
	}

	public function actionInseeDecoup(){
		$mapping=PHDB::FindOne(Mapping::COLLECTION,array("name"=>"Decoupage INSEE"),array("fields"));
		// $map=$mapping["fields"];
		$inseeCodes=array_keys($mapping["fields"]);
		$cities=PHDB::find(City::COLLECTION,array("insee"=>array('$in'=>$inseeCodes)));
		// var_dump(count($cities));exit;
		// var_dump(count($inseeCodes));exit;
		$query=array(
		'$or'=>array(
			   array("reference.costum"=>"franceTierslieux"),
			    array("source.key"=>"franceTierslieux"),
				array("tags"=>"TiersLieux")
		    )
		);
		$arr=[];
		$counter=0;
		$orga=PHDB::find(Organization::COLLECTION,$query);
		$highDensity=array("Paris","Lyon","Marseille");
		// var_dump(count($orga));exit;
		foreach($orga as $id=>$org){
			if(!empty($org["address"]["codeInsee"])){
				if(isset($mapping["fields"][$org["address"]["codeInsee"]])){
					$density=$mapping["fields"][$org["address"]["codeInsee"]];
				}else{
					$city=PHDB::findOne(City::COLLECTION,array("_id"=> new MongoId($org["address"]["localityId"])));
					if(isset($city["density"]["degréDensité"])){
						$density=$city["density"]["degréDensité"];
					}else{
					// (array_search( new MongoRegex("/" . explode(" ",$org["address"]["addressLocality"])[0] . "/i"),$highDensity)){
						
						$matches=preg_grep('/'.explode(" ",$org["address"]["addressLocality"])[0].'/i',$highDensity);
						// var_dump($matches);
						if(!empty($matches)){
							$density=4;

						}elseif(!array_search($org["address"]["localityId"],array_keys($arr))){
								array_push($arr,array($org["address"]["localityId"]=>$org["address"]["addressLocality"]));
							
						}
							// explode(" ",$org["address"]["addressLocality"])[0])
						// ,array_search( new MongoRegex("/" . explode(" ",$org["address"]["addressLocality"])[0] . "/i"),$highDensity));exit;

					// }elseif(isset($city["densite"])){
					// 	// if($de)

					}
						
				
				}
				if(!empty($density)){
					// var_dump($id,$density);exit;
    				PHDB::Update(
						Organization::COLLECTION,
    					array("_id"=>new MongoId($id)),
    					array('$set'=>
    					    array("address.inseeDensity"=>$density)
    					)
					);
					$counter++;
				}	
    		}
		}
		return "Nombre d'organisations avec densité Insee ajouté à l'adresse : ".$counter;
		// var_dump($arr);exit;

	
	}


	public function actionFormatSocialNetwork(){
		$query=array(
		   '$or'=>array(
			   array("reference.costum"=>"franceTierslieux"),
			    array("source.key"=>"franceTierslieux")
		    ),
			"socialNetwork"=>array('$exists'=>1)
		);	
		$orga=PHDB::find(Organization::COLLECTION,$query);
		// var_dump(count($orga));exit;
		$cter=0;
		$res=[];
		
		foreach($orga as $id=>$v){
			$formatedSocialNetwork=[];
			if(!empty($v["socialNetwork"])){
				// $formatedSocialNetwork=$v["socialNetwork"];
				$res[$id]=["former" => $v["socialNetwork"]];
				
			    foreach($v["socialNetwork"] as $i=>$net){
					
				    if(isset($net["platform"]) && is_string($net["platform"])){
					    if(isset($net["url"])){
							$labelArray=explode(" ",$net["platform"]);
  					        $nameSocNet=strtolower($labelArray[sizeof($labelArray)-1]);
                           $formatedSocialNetwork[$nameSocNet]=$net["url"];						
					    }
				    }
			    }
				
		    }

			if(!empty($formatedSocialNetwork) && $formatedSocialNetwork!==$v["socialNetwork"]){
				$res[$id]["new"]=$formatedSocialNetwork;
				$set=array('$set'=>array("socialNetwork"=>$formatedSocialNetwork));
			    PHDB::update(Organization::COLLECTION,array("_id"=>new MongoId($id)),$set);
			}

			

			
		}
		echo "<pre>";
		print_r($res);
		echo "</pre>";
	}
	public function actionFormatSurfaceArea(){
		$query=array(
		   '$or'=>array(
			   array("reference.costum"=>"franceTierslieux"),
			    array("source.key"=>"franceTierslieux")
		    ),
			"buildingSurfaceArea"=>new MongoRegex("/.*[a-zA-Z].*/"),
			// "name"=>new MongoRegex("/La Place des Ami.e.s/")
		);	
		$orga=PHDB::find(Organization::COLLECTION,$query);
		// var_dump($orga);exit;
		$cter=0;
		$surf=[];
		$sev=[];
		$single=[];
		$ha=[];
		$temp="";
		$repl=[];
		$count=0;

		foreach($orga as $id=>$v){
			$temp=$v["buildingSurfaceArea"];
			$surf[$v["name"]]=$v["buildingSurfaceArea"];
			$temp=str_replace(" ","",$temp);
			// var_dump($v["buildingSurfaceArea"]);exit;



			preg_match_all("/\d(ha|hectare)/",$temp,$ha);
			if(!empty($ha[0])){
				foreach($ha[0] as $i=>$h){
					// $ha[0][$i]=str_replace("ha","",$ha[0][$i]);
					// $ha[0][$i]=str_replace("hectare","",$ha[0][$i]);
					preg_match_all("/\d/",$h,$thousand);
					if(!empty($thousand[0][0])){
						$num=intval($thousand[0][0])*1000;
						$str=(string)$num;
						$temp=str_replace($thousand[0][0],$str,$temp);
						

						// $temp.=$num." ";
						// var_dump($out[0],$v["name"]);
					}
				}
				$ha[$v["name"]]=$v["buildingSurfaceArea"];
			}			
			
			
			
			preg_match_all("/\d{2,}/",$temp,$out);
			if(!empty($out[0])){
				$sev[$v["name"]]=$v["buildingSurfaceArea"];
				// var_dump($out[0],$v["name"]);
			}
			preg_match_all("/(\+|et)/",$v["buildingSurfaceArea"],$plus);
			if(!empty($plus[0])){
				if(count($out[0])>1){
				    $sum=array_sum($out[0]);
					$temp=(string)$sum;
					$add[$v["name"]]=$v["buildingSurfaceArea"];
					$add[$v["buildingSurfaceArea"]]=$sum;
				}
					
			// var_dump($out[0],$v["name"]);
			}
				// var_dump($v["buildingSurfaceArea"]);exit;
			


			
            // only one digit
			preg_match_all("/(?<!\S)\d(?!\S)/",$temp,$solo);
			if(count($solo[0])){
				$single[$v["name"]]=$v["buildingSurfaceArea"];
				var_dump($out[0],$v["name"]);
			}

			preg_match_all("/\d{1,}/",$temp,$out);
			if(!empty($out[0])){
				$temp=$out[0][0];
				// var_dump($out[0],$v["name"]);
			}
            
			$forced=
			[
				"LE 67 ateliers de créateurs" => "1400",
				"La Rayonne" => "1700",
				"kjhkb" => "0",
				"Projet de tiers-lieu l'Atelier des valeurs sociales et environnementales en cours de création" => "0",
				"TIERS LIEU FAYEN" => "0",
				"\"La Place des Ami.e.s\"" =>"200",
			];

			if(isset($forced[$v["name"]])){
				$temp=$forced[$v["name"]];
			}

			

			$set=array('$set'=>array("buildingSurfaceArea"=>$temp));
			if($v["buildingSurfaceArea"]!==$temp){
				$repl[]=[$v["buildingSurfaceArea"],$temp,$v["name"]];
				$count++;
				PHDB::update(Organization::COLLECTION,array("_id"=>new MongoId($id)),$set);
			}
			
		}
		echo $count. " buildingSurfaceArea mises à jours";
		echo "<pre>";
		print_r($repl);
		echo "</pre>";
		// var_dump($surf);exit;
		// echo "".count($orga)." tag 'Autre famille de tiers-lieux' mis à jour";
	}

	public function actionSpaceSize(){
		$query=array(
			'$or'=>array(
				array("reference.costum"=>"franceTierslieux"),
				 array("source.key"=>"franceTierslieux")
			 ),
			 "buildingSurfaceArea"=>array('$exists'=>1)
			 // "name"=>new MongoRegex("/La Place des Ami.e.s/")
		 );	
		 $orga=PHDB::find(Organization::COLLECTION,$query);
		 // var_dump($orga);exit;
		 
        $res=[];
		foreach($orga as $id=>$v){
			$size="";
			$value=$v["buildingSurfaceArea"];
			preg_match_all("/\s/",$value,$space);

			if(intval($v["buildingSurfaceArea"])<60){
				$size="Moins de 60m²";
			}else if(intval($v["buildingSurfaceArea"])<=200){
				$size="Entre 60 et 200m²";
			}else if(intval($v["buildingSurfaceArea"])>200){
				$size="Plus de 200m²";
			}
			if(!empty($size) && !isset($space[0][0])){
				$res[$id]=[$v["buildingSurfaceArea"]];
				$res[$id][]=$size;

				$commonVal=[];

				if(isset($v["tags"])){
					$commonVal=array_intersect($v["tags"],["Moins de 60m²","Entre 60 et 200m²","Plus de 200m²"]);
				    $res[$id][]=$commonVal;

				}

				if(!empty($commonVal)){
					PHDB::update( Organization::COLLECTION, 
					array("_id"=>new MongoId($id)),
					array('$pull' => array("tags"=> array('$in'=>["Moins de 60m²","Entre 60 et 200m²","Plus de 200m²"]))));
				}
				
				PHDB::update( Organization::COLLECTION, 
					array("_id"=>new MongoId($id)),
					array('$push' => array("tags"=> $size )));
			}  
		}		
		echo "<pre>";
		print_r($res);
		echo "</pre>";
    }

	public function actionFormatSiteSurfaceArea(){
		$query=array(
		   '$or'=>array(
			   array("reference.costum"=>"franceTierslieux"),
			    array("source.key"=>"franceTierslieux")
		    ),
			"siteSurfaceArea"=>new MongoRegex("/.*[a-zA-Z].*/")
			// "name"=>new MongoRegex("/La Place des Ami.e.s/")
		);	
		$orga=PHDB::find(Organization::COLLECTION,$query);
		// var_dump(count($orga));exit;
		$cter=0;
		$surf=[];
		$sev=[];
		$single=[];
		$ha=[];
		$temp="";
		$repl=[];
		$count=0;

		foreach($orga as $id=>$v){
			$temp=$v["siteSurfaceArea"];
			$surf[$v["name"]]=$v["siteSurfaceArea"];
			$temp=str_replace(" ","",$temp);
			$temp=str_replace(",",".",$temp);
			// var_dump($v["siteSurfaceArea"]);exit;



			preg_match_all("/\d(ha|hectare)/",$v["siteSurfaceArea"],$ha);
			if(!empty($ha[0])){
				foreach($ha[0] as $i=>$h){
					// var_dump($ha[0]);exit;
					// $ha[0][$i]=str_replace("ha","",$ha[0][$i]);
					// $ha[0][$i]=str_replace("hectare","",$ha[0][$i]);
					preg_match_all("/\d/",$h,$thousand);
					// var_dump($thousand);exit;
					if(!empty($thousand[0][0])){
						$num=intval($thousand[0][0])*1000;
						$str=(string)$num;
						str_replace($thousand[0][0],$str,$temp);
						

						// $temp.=$num." ";
						// var_dump($out[0],$v["name"]);
					}
				}
				$ha[$v["name"]]=$v["siteSurfaceArea"];
			}			
			
			
			
			preg_match_all("/\d{2,}/",$temp,$out);
			if(!empty($out[0])){
				$sev[$v["name"]]=$v["siteSurfaceArea"];
				// var_dump($out[0],$v["name"]);
			}
			preg_match_all("/(\+|et)/",$v["siteSurfaceArea"],$plus);
			if(!empty($plus[0])){
				if(count($out[0])>1){
				    $sum=array_sum($out[0]);
					$temp=(string)$sum;
					$add[$v["name"]]=$v["siteSurfaceArea"];
					$add[$v["siteSurfaceArea"]]=$sum;
				}
					
			// var_dump($out[0],$v["name"]);
			}
				// var_dump($v["siteSurfaceArea"]);exit;
			


			
            // only one digit
			preg_match_all("/(?<!\S)\d(?!\S)/",$temp,$solo);
			if(count($solo[0])){
				$single[$v["name"]]=$v["siteSurfaceArea"];
				// var_dump($out[0],$v["name"]);
			}

			preg_match_all("/\d{1,}/",$temp,$out);
			if(!empty($out[0])){
				$temp=$out[0][0];
				// var_dump($out[0],$v["name"]);
			}
            
			$forced=
			[
				"Atelier de Claret"=>"0",
				"Carte Blanche" =>"0",
				"kjhkb" => "0",
				"L'Intervalle" => "0",
				"L'Établi"=>"0",
				
				"Afpa - Village des solutions Golbey Epinal"=>"30000",
				"Nature &amp; Gîtes "=>"204000",
				"Projet de tiers-lieu l'Atelier des valeurs sociales et environnementales en cours de création" => "0",
				"Le Théâtre des Minuits"=>"130000",
				"le Nid"=>"0",
				"CPIE Sèvre et Bocage - Maison de la Vie Rurale" => "30000",
				"L'Ampère" =>"0",

				// "LE 67 ateliers de créateurs" => "1400",
				"La Rayonne" => "17000",
				"L'Attrium" => "0",
				"Les Ateliers du Vent" =>"0",
				// "Projet de tiers-lieu l'Atelier des valeurs sociales et environnementales en cours de création" => "0",
				"TIERS LIEU FAYEN" => "0",
				"La Manufacture Village des solutions du Puy en Velay" => "70000",
				"Afpa - Village des solutions Nancy Laxou" => "20000",
				"Village des solutions Afpa de Bourg en Bresse" => "45000",
				"Ecolieu du Plan du Pont" => "100000",
				" a riturnella " => "0",
				"Afpa - Village des solutions Strasbourg"=>"30000",
				"La Minoterie de Lésigny" =>"50000",
				"Afpa - Village des solutions Reims" => "30000",
				"Afpa - Village des solutions Metz" => "30000",
				"Espace Bon Rencontre" => "10000",
				"Le Refuge de Création du Rupt de Bamont" => "16000",
				"Tiers-lieu l'Orange Bleue" => "180000",
				"espace service jeunesse" => ""


				// "\"La Place des Ami.e.s\"" =>"200",
			];

			if(isset($forced[$v["name"]])){
				$temp=$forced[$v["name"]];
			}

			

			$set=array('$set'=>array("siteSurfaceArea"=>$temp));
			if($v["siteSurfaceArea"]!==$temp){
				$repl[]=[$v["siteSurfaceArea"],$temp,$v["name"]];
				$count++;
				PHDB::update(Organization::COLLECTION,array("_id"=>new MongoId($id)),$set);
			}
			
		}
		echo $count. " siteSurfaceArea mises à jours";
		echo "<pre>";
		print_r($repl);
		echo "</pre>";
		// var_dump($repl);exit;
		// var_dump($surf);exit;
		// echo "".count($orga)." tag 'Autre famille de tiers-lieux' mis à jour";
	}

	// public function actionTelephone(){
	// 	$query=array(
	// 	   '$or'=>array(
	// 		   array("reference.costum"=>"franceTierslieux"),
	// 		    array("source.key"=>"franceTierslieux")
	// 	    ),
	// 		"telephone"=>array('$exists'=>1),
	// 		// "name"=>new MongoRegex("/La Place des Ami.e.s/")
	// 	);	
	// 	$orga=PHDB::find(Organization::COLLECTION,$query);
	// 	var_dump(count($orga));exit;

	// 	$tel=[];
		
	// 	$temp="";


	// 	foreach($orga as $id=>$v){
	// 		$temp=$v["telephone"];
	// 		$tel[$v["name"]]=$v["telephone"];
	// 		$temp=str_replace(" ","",$temp);
	// 		$temp=str_replace(",",".",$temp);
	// 		// var_dump($v["siteSurfaceArea"]);exit;



	// 		preg_match_all("/\d(ha|hectare)/",$v["telephone"],$ha);
	// 	}
	// }			

	public function actionFormatTypePlace(){
		$query=array(
		   '$or'=>array(
			   array("reference.costum"=>"franceTierslieux"),
			    array("source.key"=>"franceTierslieux")
		    ),
			"extraTypePlace"=>array('$exists'=>1)
		);	
		$orga=PHDB::find(Organization::COLLECTION,$query);
		$cter=0;
		foreach($orga as $id=>$v){
			if(!isset($v["tags"])){
				$v["tags"]=[];
			}
			if(!in_array("Autre famille de tiers-lieux",$v["tags"])){
				array_push($v["tags"],"Autre famille de tiers-lieux");
			}
			
			$set=array('$set'=>array("tags"=>$v["tags"]));
			PHDB::update(Organization::COLLECTION,array("_id"=>new MongoId($id)),$set);
		}
		echo "".count($orga)." tag 'Autre famille de tiers-lieux' mis à jour";
	}
	public function actionFormatManageModel(){
		$query=array(
		   '$or'=>array(
			   array("reference.costum"=>"franceTierslieux"),
			    array("source.key"=>"franceTierslieux")
		    ),
			"extraManageModel"=>array('$exists'=>1)
		);	
		$orga=PHDB::find(Organization::COLLECTION,$query);
		$cter=0;
		foreach($orga as $id=>$v){
			if(!isset($v["tags"])){
				$v["tags"]=[];
			}
			if(!in_array("Autre mode de gestion",$v["tags"])){
				array_push($v["tags"],"Autre mode de gestion");
			}
			$set=array('$set'=>array("tags"=>$v["tags"]));
			PHDB::update(Organization::COLLECTION,array("_id"=>new MongoId($id)),$set);
		}
		echo "".count($orga)." tag 'Autre mode de gestion' mis à jour";
	}
	public function actionFormatTelephone(){
		$mapIndicTel=[
			"FR" => "33",
			"GP" => "590",
			"MQ" => "596",
			"GF" => "594",
			"RE" => "262",
			"PM" => "508",
			"YT" => "269",
			"WF" => "681",
			"PF" => "689",
			"NC" => "687"
		];
		$query=array(
			'$or'=>array(
				array("reference.costum"=>"franceTierslieux"),
				 array("source.key"=>"franceTierslieux")
			 ),
			//  '$or'=>array(
				// "telephone.fixe"=>array(new MongoRegex("/\D/"))
				//  array("telephone.mobile"=>array(new MongoRegex("/[-]/")))
			 
			 "telephone"=>array('$exists'=>1)
		 );	
		 $orga=PHDB::find(Organization::COLLECTION,$query);
		//  var_dump(count($orga));exit;
		//  var_dump($orga);exit;

		 $telephones=PHDB::distinct(Organization::COLLECTION,"telephone",$query);
		//  var_dump($telephones);exit;

		 $res=[];

		 foreach($orga as $id=>$v){
			$res[$id]=["former"=>$v["telephone"]];
			// var_dump($v["telephone"]);
			$phone=is_string($v["telephone"]) ? $v["telephone"] : 
			(isset($v["telephone"]["mobile"]) && isset($v["telephone"]["mobile"][0]) ? $v["telephone"]["mobile"][0] : 
			(isset($v["telephone"]["fixe"]) && isset($v["telephone"]["fixe"][0]) ? $v["telephone"]["fixe"][0] : ""));
			$phone = (strlen($phone)==9 && substr($phone,1,0)!="0") ? "0".$phone : str_replace("(0)","",$phone) ;
			$phone=(preg_match('/^[+0-9]/',$phone)) ? $phone : "";
			// var_dump($phone." ".$v["name"]);
			
			$numIndex=2;
		    $phone=preg_replace('~[/\s\.(.*?\-)]~','',$phone);
			// $phone=str_replace('(0)','',$phone);
			// var_dump($phone);
		// remplacer 00 par +
		// var_dump(substr($phone,0,2));
		    $phone=(substr($phone,0,2)=="00") ? "+".substr($phone,2) : 
			((substr($phone,0,1)!="+" && isset($v["address"]) && isset($v["address"]["addressCountry"]) && isset($mapIndicTel[$v["address"]["addressCountry"]])) 
			   ? "+".$mapIndicTel[$v["address"]["addressCountry"]].substr($phone,1) : $phone );
			
		if(substr($phone,0,1)=="+"){
			// var_dump(substr($phone,1,2));
			if(substr($phone,1,2)=="33"){
				$numIndex=3;
			}else{
				$numIndex=4;
			}
		}

		// $phone=substr($phone, $numIndex, 9);
		
		
		$mobOrfixeInd=substr($phone,$numIndex,1);
				$typeNumber= ($mobOrfixeInd=="6" || $mobOrfixeInd=="7") ? "mobile" : "fixe" ;	
				// var_dump($phone,$typeNumber);		
			    $telephone=[];
				$telephone[$typeNumber]=[$phone];
			// var_dump($telephone);
			$set=array('$set'=>array("telephone"=>$telephone));
			PHDB::update(Organization::COLLECTION,array("_id"=>new MongoId($id)),$set);
			$res[$id]["new"]=$telephone;
		}	
		echo "<pre>";
		print_r($res);
		echo "</pre>";
		
		echo "Numéros de téléphone à jour"; 	
	}

	public function actionDureeBail(){
		$questionPath="franceTierslieux1522023_1549_3.franceTierslieux1522023_1549_3le5si0qhpywv18p94ne";//DURE_BAIL 
		$query=array(
			 "answers.".$questionPath=>array('$exists'=>1),
			 
		);	

		

		$answers=PHDB::find(Form::ANSWER_COLLECTION,$query);

		$baux=PHDB::distinct(Form::ANSWER_COLLECTION,"answers.".$questionPath,$query);
		// var_dump($answers);exit;
		
		$res=[];
		foreach($answers as $ida=>$ans){
			$split=explode(".",$questionPath);
			// var_dump($split);exit;
			$res[$ida]=["former"=>$ans["answers"][$split[0]][$split[1]]];
			$ans["answers"][$split[0]][$split[1]]=preg_replace('~[-]~','',$ans["answers"][$split[0]][$split[1]]);
			$res[$ida]["new"]=$ans["answers"][$split[0]][$split[1]];

			$set=array('$set'=>array("answers.".$questionPath=>$ans["answers"][$split[0]][$split[1]]));
			PHDB::update(Form::ANSWER_COLLECTION,array("_id"=>new MongoId($ida)),$set);
			
		}
        echo "<pre>";
		print_r($res);
		echo "</pre>";
	}

	public function actionPublicEventCult(){
		$questionPath=[
			"franceTierslieux1922023_1231_5.franceTierslieux1922023_1231_5lebb61gnmph1vnin1a",//NB_PUBLIC_CULT
			"franceTierslieux1922023_1231_5.franceTierslieux1922023_1231_5lebb61gosevh7nembco",//NB_PERS_PROJ
			"franceTierslieux1922023_1231_5.franceTierslieux1922023_1231_5lebb61goil3uftiqkwo",//NB_BENEV
			"franceTierslieux1922023_1231_5.franceTierslieux1922023_1231_5lebb61go0yi3ov9tzuve"//NB_PERS_ASSO
		];	
		$res=[];
		foreach($questionPath as $ind=>$path){
		    $query=array(
			    "answers.".$path=>array('$exists'=>1),
			 
		    );	

		

		    $answers=PHDB::find(Form::ANSWER_COLLECTION,$query);

		    $baux=PHDB::distinct(Form::ANSWER_COLLECTION,"answers.".$path,$query);
		    // var_dump($answers);exit;
		
		    
		    foreach($answers as $ida=>$ans){
			    $split=explode(".",$path);
			    // var_dump($split);exit;
			    $res[$ida]=["former"=>$ans["answers"][$split[0]][$split[1]]];
			    $ans["answers"][$split[0]][$split[1]]=str_replace('.',',',$ans["answers"][$split[0]][$split[1]]);
				$ans["answers"][$split[0]][$split[1]]=preg_replace('~[-]~','',$ans["answers"][$split[0]][$split[1]]);
				
			    $res[$ida]["new"]=$ans["answers"][$split[0]][$split[1]];

				$set=array('$set'=>array("answers.".$path=>$ans["answers"][$split[0]][$split[1]]));
			    PHDB::update(Form::ANSWER_COLLECTION,array("_id"=>new MongoId($ida)),$set);
			
		    }
            echo "<pre>";
		    print_r($res);
		    echo "</pre>";
		}	
	}

	public function actionNbEtp(){
		$questionPath="franceTierslieux1922023_1231_6.franceTierslieux1922023_1231_6lebdout8uyagpr9ehyg";//NB_ETP 
		$query=array(
			 "answers.".$questionPath=>array('$exists'=>1),
			 
		);	
		$answers=PHDB::find(Form::ANSWER_COLLECTION,$query);

		$etp=PHDB::distinct(Form::ANSWER_COLLECTION,"answers.".$questionPath,$query);
		// echo "<pre>";
		//     print_r($etp);
		// echo "</pre>";exit;
		
		$res=[];
		foreach($answers as $ida=>$ans){
			$split=explode(".",$questionPath);
			// var_dump($split);exit;
			$res[$ida]=["former"=>$ans["answers"][$split[0]][$split[1]]];
			$ans["answers"][$split[0]][$split[1]]=preg_replace('~[-]~','',$ans["answers"][$split[0]][$split[1]]);
			$ans["answers"][$split[0]][$split[1]]=preg_replace('~\.~',',',$ans["answers"][$split[0]][$split[1]]);
			$ans["answers"][$split[0]][$split[1]]=preg_replace('~^,~','0,',$ans["answers"][$split[0]][$split[1]]);
			$res[$ida]["new"]=$ans["answers"][$split[0]][$split[1]];

			$set=array('$set'=>array("answers.".$questionPath=>$ans["answers"][$split[0]][$split[1]]));
			PHDB::update(Form::ANSWER_COLLECTION,array("_id"=>new MongoId($ida)),$set);
			
		}
        echo "<pre>";
		print_r($res);
		echo "</pre>";
	}

	public function actionStructureOccupante(){
		$questionPath="franceTierslieux1922023_1417_7.franceTierslieux1922023_1417_7lebeyrj9zthl8xraoim";//NB_ETP 
		$query=array(
			 "answers.".$questionPath=>array('$exists'=>1),
			 
		);	

		

		$answers=PHDB::find(Form::ANSWER_COLLECTION,$query);

		$occup=PHDB::distinct(Form::ANSWER_COLLECTION,"answers.".$questionPath,$query);
		// echo "<pre>";
		//     print_r($occup);
		// echo "</pre>";exit;
		
		$res=[];
		foreach($answers as $ida=>$ans){
			$split=explode(".",$questionPath);
			// var_dump($split);exit;
			$res[$ida]=["former"=>$ans["answers"][$split[0]][$split[1]]];
			$ans["answers"][$split[0]][$split[1]]=preg_replace('~[-]~','',$ans["answers"][$split[0]][$split[1]]);
			$ans["answers"][$split[0]][$split[1]]=preg_replace('~\.~',',',$ans["answers"][$split[0]][$split[1]]);
			$ans["answers"][$split[0]][$split[1]]=preg_replace('~^,~','0,',$ans["answers"][$split[0]][$split[1]]);
			$res[$ida]["new"]=$ans["answers"][$split[0]][$split[1]];

			$set=array('$set'=>array("answers.".$questionPath=>$ans["answers"][$split[0]][$split[1]]));
			PHDB::update(Form::ANSWER_COLLECTION,array("_id"=>new MongoId($ida)),$set);
			
		}
        echo "<pre>";
		print_r($res);
		echo "</pre>";
	}

	public function actionMultipleStructureOccupante(){
		// $questionPath=[
		// 	"franceTierslieux1922023_1231_6.franceTierslieux1922023_1231_6lebdout9qitprmvmmwt",//NB_PUBLIC_CULT
		// 	// "franceTierslieux1922023_1231_5.franceTierslieux1922023_1231_5lebb61gosevh7nembco",//NB_PERS_PROJ
		// 	// "franceTierslieux1922023_1231_5.franceTierslieux1922023_1231_5lebb61goil3uftiqkwo",//NB_BENEV
		// 	// "franceTierslieux1922023_1231_5.franceTierslieux1922023_1231_5lebb61go0yi3ov9tzuve"//NB_PERS_ASSO
		// ];	
		$questionPath="franceTierslieux1922023_1417_7.multiCheckboxPlusfranceTierslieux1922023_1417_7lebeyrj9evio2mudes";//contrat
		// $typeContrat=[
		// 	"CDD","CDDI","CDI","Contrat d'apprentissage","Contrats de professionnalisation","Stagiaire"
		// ];
		$query=array(
			 "answers.".$questionPath=>array('$exists'=>1),
			 
		);	

		

		$answers=PHDB::find(Form::ANSWER_COLLECTION,$query);

		$occup=PHDB::distinct(Form::ANSWER_COLLECTION,"answers.".$questionPath,$query);
		// echo "<pre>";
		//     print_r($contract);
		// echo "</pre>";exit;
		
		$res=[];
		$val=[];
		$former=[];
		// $change=0;
		foreach($answers as $ida=>$ans){
			$split=explode(".",$questionPath);
			// var_dump($ans["answers"][$split[0]][$split[1]]);exit;
			// foreach()
			$originalAns=$ans["answers"][$split[0]][$split[1]];
			$res[$ida]=["former"=>$ans["answers"][$split[0]][$split[1]]];
			foreach($ans["answers"][$split[0]][$split[1]] as $ind=>$struct){
				$split=explode(".",$questionPath);
				$structure=array_keys($struct)[0];
				
				if(isset($ans["answers"][$split[0]][$split[1]][$ind][$structure]["textsup"])){
					// var_dump($ans["answers"][$split[0]][$split[1]][$ind][$structure]["textsup"]);exit;
					
					$value=$ans["answers"][$split[0]][$split[1]][$ind][$structure]["textsup"];
					$val[]=$value;
					$former[]=$value;
					// 
			        preg_match_all("/(\d*\.\d*)|(\d*,\d*)|(\d*\-\d*)|(\d*\~\d*)|(\d+)/",$value,$out);
					// var_dump($out);exit;
					if(!empty($out[0][0])){
						$newVal=$out[0][0];
						$newVal=preg_replace("/-/",'',$newVal);
						$newVal=preg_replace("/\./",',',$newVal);
						$newVal=preg_replace("/^,/",'0,',$newVal);
						$newVal=preg_replace("/,$/",'',$newVal);
						$newVal=preg_replace("/~/",'',$newVal);
			        //     $ans["answers"][$split[0]][$split[1]]=preg_replace('~\.~',',',$ans["answers"][$split[0]][$split[1]]);
			        //    $ans["answers"][$split[0]][$split[1]]=preg_replace('~^,~','0,',$ans["answers"][$split[0]][$split[1]]);
					}else if($value=="0"){
						$newVal=$value;

					}else{
						$newVal="";
					}

					$ans["answers"][$split[0]][$split[1]][$ind][$structure]["textsup"]=$newVal;
					
					   
				    // }	
			        
			        // $res[$ida]["new"]=$ans["answers"][$split[0]][$split[1]][$ind][$cont]["textsup"];


				}
				    
			// var_dump($split);exit;
			        

			}
			$res[$ida]["new"]=$ans["answers"][$split[0]][$split[1]];
			$set=array('$set'=>array("answers.".$questionPath=>$ans["answers"][$split[0]][$split[1]]));
			PHDB::update(Form::ANSWER_COLLECTION,array("_id"=>new MongoId($ida)),$set);
			
			
		}
        echo "<pre>";
		print_r($res);
		echo "</pre>";	
	}
	// franceTierslieux1922023_1231_5lebb61gnmph1vnin1a

	public function actionRhContracts(){
		// $questionPath=[
		// 	"franceTierslieux1922023_1231_6.franceTierslieux1922023_1231_6lebdout9qitprmvmmwt",//NB_PUBLIC_CULT
		// 	// "franceTierslieux1922023_1231_5.franceTierslieux1922023_1231_5lebb61gosevh7nembco",//NB_PERS_PROJ
		// 	// "franceTierslieux1922023_1231_5.franceTierslieux1922023_1231_5lebb61goil3uftiqkwo",//NB_BENEV
		// 	// "franceTierslieux1922023_1231_5.franceTierslieux1922023_1231_5lebb61go0yi3ov9tzuve"//NB_PERS_ASSO
		// ];	
		$questionPath="franceTierslieux1922023_1231_6.multiCheckboxPlusfranceTierslieux1922023_1231_6lebdout9qitprmvmmwt";//contrat
		$typeContrat=[
			"CDD","CDDI","CDI","Contrat d'apprentissage","Contrats de professionnalisation","Stagiaire"
		];
		$query=array(
			 "answers.".$questionPath=>array('$exists'=>1),
			 
		);	

		

		$answers=PHDB::find(Form::ANSWER_COLLECTION,$query);

		$contract=PHDB::distinct(Form::ANSWER_COLLECTION,"answers.".$questionPath,$query);
		// echo "<pre>";
		//     print_r($contract);
		// echo "</pre>";exit;
		
		$res=[];
		$val=[];
		$former=[];
		// $change=0;
		foreach($answers as $ida=>$ans){
			$split=explode(".",$questionPath);
			// var_dump($ans["answers"][$split[0]][$split[1]]);exit;
			// foreach()
			$originalAns=$ans["answers"][$split[0]][$split[1]];
			$res[$ida]=["former"=>$ans["answers"][$split[0]][$split[1]]];
			foreach($ans["answers"][$split[0]][$split[1]] as $ind=>$contrat){
				$split=explode(".",$questionPath);
				$cont=array_keys($contrat)[0];
				
				if(isset($ans["answers"][$split[0]][$split[1]][$ind][$cont]["textsup"])){
					// var_dump($ans["answers"][$split[0]][$split[1]][$ind][$cont]["textsup"]);exit;
					
					$value=$ans["answers"][$split[0]][$split[1]][$ind][$cont]["textsup"];
					$val[]=$value;
					$former[]=$value;
					// 
			        preg_match_all("/(\d*\.\d*)|(\d*,\d*)|(\d*\-\d*)|(\d*\~\d*)|(\d+)/",$value,$out);
					// var_dump($out);exit;
					if(!empty($out[0][0])){
						$newVal=$out[0][0];
						$newVal=preg_replace("/-/",'',$newVal);
						$newVal=preg_replace("/\./",',',$newVal);
						$newVal=preg_replace("/^,/",'0,',$newVal);
						$newVal=preg_replace("/,$/",'',$newVal);
						$newVal=preg_replace("/~/",'',$newVal);
			        //     $ans["answers"][$split[0]][$split[1]]=preg_replace('~\.~',',',$ans["answers"][$split[0]][$split[1]]);
			        //    $ans["answers"][$split[0]][$split[1]]=preg_replace('~^,~','0,',$ans["answers"][$split[0]][$split[1]]);
					}else if($value=="0"){
						$newVal=$value;

					}else{
						$newVal="";
					}

					$ans["answers"][$split[0]][$split[1]][$ind][$cont]["textsup"]=$newVal;
					
					   
				    // }	
			        
			        // $res[$ida]["new"]=$ans["answers"][$split[0]][$split[1]][$ind][$cont]["textsup"];


				}
				    
			// var_dump($split);exit;
			        

			}
			$res[$ida]["new"]=$ans["answers"][$split[0]][$split[1]];
			$set=array('$set'=>array("answers.".$questionPath=>$ans["answers"][$split[0]][$split[1]]));
			PHDB::update(Form::ANSWER_COLLECTION,array("_id"=>new MongoId($ida)),$set);
			
			
		}
        echo "<pre>";
		print_r($res);
		echo "</pre>";	
	}

	public function actionModeleEcoNumbers(){
		$questionPath=[
			"franceTierslieux2022023_732_9.franceTierslieux2022023_732_9lecfwuxw4ol3ib2zcyc",//CA_22
			"franceTierslieux2022023_732_9.franceTierslieux2022023_732_9lecfwuxx9q1jro8u6xf",//PRODUITS_22
			"franceTierslieux2022023_732_9.franceTierslieux2022023_732_9lecfwuxx2a0p6im4uhn",//TOT_SUBV_PUB_22
			"franceTierslieux2022023_732_9.franceTierslieux2022023_732_9lecfwuxz95rar0lzc65",//FOND_PROPR_22
			"franceTierslieux2022023_732_9.franceTierslieux2022023_732_9lecfwuxzsgkob33wl6d",//RECETTE_23
			"franceTierslieux1922023_1231_6.franceTierslieux1922023_1231_6leh2y401jm987cubh8a"//PART_FEMME
		];	
		$res=[];
		foreach($questionPath as $ind=>$path){
		    $query=array(
			    "answers.".$path=>array('$exists'=>1),
			 
		    );	

		

		    $answers=PHDB::find(Form::ANSWER_COLLECTION,$query);

		    $baux=PHDB::distinct(Form::ANSWER_COLLECTION,"answers.".$path,$query);
		    // var_dump($answers);exit;
		
		    $virguleMillier=[];
		    foreach($answers as $ida=>$ans){
			    $split=explode(".",$path);
			    // var_dump($split);exit;
			    $res[$ida]=["former"=>$ans["answers"][$split[0]][$split[1]]];
				$newVal=$ans["answers"][$split[0]][$split[1]];
				$newVal=preg_replace("/-/",'',$newVal);
				preg_match_all("/\.\d{3,}/",$newVal,$thousandDot);
				if(!empty($thousandDot[0][0])){
					// $virguleMillier[]=$newVal;
					$newVal=preg_replace("/\./",'',$newVal);
					// $virguleMillier[]=$newVal;
				}
				// $newVal=preg_replace("/,\d{3,}/",'',$newVal);
				$newVal=preg_replace("/\./",',',$newVal);
				$newVal=preg_replace("/^,/",'0,',$newVal);
				$newVal=preg_replace("/,$/",'',$newVal);
				$newVal=preg_replace("/~/",'',$newVal);
			    $ans["answers"][$split[0]][$split[1]]=$newVal;
			    $res[$ida]["new"]=$ans["answers"][$split[0]][$split[1]];

				$set=array('$set'=>array("answers.".$path=>$ans["answers"][$split[0]][$split[1]]));
			PHDB::update(Form::ANSWER_COLLECTION,array("_id"=>new MongoId($ida)),$set);
			
		    }
            echo "<pre>";
		    print_r($res);
		    echo "</pre>";
		}	
	}


	
	
		

	public function actionLoyerBureauRecensementTL(){
		$path="answers.franceTierslieux1822023_1721_4.multiCheckboxPlusfranceTierslieux1822023_1721_4lea64x9zefdbjqbhl7l.Loyer / Participation aux frais.textsup";
		$query=array($path=>array('$exists'=>1));
        $answers=PHDB::find(Form::ANSWER_COLLECTION,$query);
		$nbAnswers=0;
		$arrTreatment=[];
		foreach($answers as $id=>$val){
			$indexLoyer=array_search("Loyer / Participation aux frais",array_keys($val["answers"]["franceTierslieux1822023_1721_4"]["multiCheckboxPlusfranceTierslieux1822023_1721_4lea64x9zefdbjqbhl7l"]));
	
			$ruledVal=$val["answers"]["franceTierslieux1822023_1721_4"]["multiCheckboxPlusfranceTierslieux1822023_1721_4lea64x9zefdbjqbhl7l"];
			$newFullVal=FranceTierslieux::answerRules("franceTierslieux1822023_1721_4lea64x9zefdbjqbhl7l",$ruledVal);
			$newValLoyer=$newFullVal[$indexLoyer]["Loyer / Participation aux frais"]["textsup"];
			
			// $set=array("answers.franceTierslieux1822023_1721_4."=>$newVal);
			$set=array('$set'=>array("answers.franceTierslieux1822023_1721_4.franceTierslieux1822023_1721_4lmg9ztxiwnjfpa9weqg"=>$newValLoyer));
			//  var_dump($ruledVal[$indexLoyer]["Loyer / Participation aux frais"]["textsup"],$newValLoyer);
			 PHDB::update(Form::ANSWER_COLLECTION,array("_id"=>new MongoId($id)),$set);
			$nbAnswers++;
			$arrTreatment[$ruledVal[$indexLoyer]["Loyer / Participation aux frais"]["textsup"]]="".$newValLoyer."";
		}
		// $msg= "Nombre de" 
		return print_r($arrTreatment);
		
		
	}	

	public function actionReferencingFtl(){
			$str = "";
			$csv = $_POST["file"];
			$csv=explode("\n",$csv);
		
			$ids=[];
			foreach($csv as $value){
				//var_dump($value);
				if (strpos($value, 'existe')!==false){
					
					$lines=explode("organizations,",$value);
					array_push($ids,$lines[1]);
				}							
			 }
			
			$elements=PHDB::findByIds(Organization::COLLECTION, $ids);			

			 if(!empty($elements)){
			 	$countRef=0;
			 	$countNew=0;
			 	foreach($elements as $data){
			 			$newreference = array();
			 			if(!empty($data["reference"])){	 				
							$newreference["costum"] = $data["reference"]["costum"];
								array_push($newreference["costum"],"franceTierslieux");
								try {
									$res = PHDB::update( Organization::COLLECTION, 
								  		array("_id"=>new MongoId($data["_id"])),
			                        	array('$set' => array(	"reference.costum" => $newreference["costum"])));
								} catch (MongoWriteConcernException $e) {
									$str .=("Erreur à la mise à jour de la référence existante de ".Organization::COLLECTION." avec l'id ".$data);
									die();
								}
			 					$countRef++;

			 				
			 			}
			 			else{
			 				try {
									$res = PHDB::update( Organization::COLLECTION, 
								  		array("_id"=>new MongoId($data["_id"])),
			                        	array('$set' => array("reference.costum"=> array("franceTierslieux"))));
									$countNew++;
								} catch (MongoWriteConcernException $e) {
									$str .=("Erreur de la création de la référence à jour de l'élément ".Organization::COLLECTION." avec l'id ".$data);
									die();
								}
			 					$countNew++;
			 			}
			 	}
			 	$str .= $countNew ." tiers-lieux avec nouvelle référence !";
			 	$str .= "\n".$countRef." tiers-lieux mis a jour avec référence en plus !" ;
			}		
		return $str;	
	}	


	public function actionRemoveOrgaBadlySourcedFtl(){
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
			$organizations = PHDB::find(Organization::COLLECTION, array(
				"source.key" => "FTLTest1"));
			$nbOrgaDealWith=0;
			$name=[];
			$id=[];
			$couple=[];
			$formatedId = [];
			$formatedId[]=[];
			foreach($organizations as $data){
			
				$formatedId =  get_object_vars(($data["_id"]));

				PHDB::remove(Organization::COLLECTION,array(
					"source.key" => "FTLTest1"));
				Slug::removeByParentIdAndType($formatedId['$id'],Organization::COLLECTION);
				$nbOrgaDealWith++;
				
				array_push($name, $data["name"]);
				array_push($id, $formatedId['$id']);

			}

			$couple = array_combine($name, $id);
			return "nombre de tiers-lieux supprimés ".$nbOrgaDealWith;
			//print_r($couple);
		}
	}

	public function actionRemoveOrgaFromFtl(){
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
			$organizations = PHDB::find(Organization::COLLECTION, array(
				"source.key" => "franceTierslieux"));
			$nbOrgaDealWith=0;
			$name=[];
			$id=[];
			$couple=[];
			$formatedId = [];
			$formatedId[]=[];
			foreach($organizations as $data){
			
				$formatedId =  get_object_vars(($data["_id"]));

				PHDB::remove(Organization::COLLECTION,array(
					"source.key" => "franceTierslieux"));
				Slug::removeByParentIdAndType($formatedId['$id'],Organization::COLLECTION);
				$nbOrgaDealWith++;
				
				array_push($name, $data["name"]);
				array_push($id, $formatedId['$id']);

			}

			$couple = array_combine($name, $id);
			return "nombre de tiers-lieux supprimés ".$nbOrgaDealWith;
			//print_r($couple);
		}
	}


	public function actionRemoveOrgaFromHubReunion(){
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
			$organizations = PHDB::find(Organization::COLLECTION, array(
				"source.key" => "hubreunion"));
			$nbOrgaDealWith=0;
			$name=[];
			$id=[];
			$couple=[];
			$formatedId = [];
			$formatedId[]=[];
			foreach($organizations as $data){
			
				$formatedId =  get_object_vars(($data["_id"]));

				PHDB::remove(Organization::COLLECTION,array(
					"source.key" => "hubreunion"));
				Slug::removeByParentIdAndType($formatedId['$id'],Organization::COLLECTION);
				$nbOrgaDealWith++;
				
				array_push($name, $data["name"]);
				array_push($id, $formatedId['$id']);

			}

			$couple = array_combine($name, $id);
			return "nombre de structures mednum supprimés ".$nbOrgaDealWith;
			//print_r($couple);
		}
	}

	public function actionRemoveDataFromTerramies(){
		$str = "";
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
			$organizations = PHDB::find(Organization::COLLECTION, array(
				"source.key" => "terramies"));
			

			$nbOrgaDealWith=0;
			$name=[];
			$id=[];
			$couple=[];
			$formatedId = [];
			$formatedId[]=[];
			foreach($organizations as $data){
			
				$formatedId =  get_object_vars(($data["_id"]));

				PHDB::remove(Organization::COLLECTION,array(
					"source.key" => "terramies"));
				Slug::removeByParentIdAndType($formatedId['$id'],Organization::COLLECTION);
				$nbOrgaDealWith++;
				
				array_push($name, $data["name"]);
				array_push($id, $formatedId['$id']);

			}
			$couple = array_combine($name, $id);
			$str .= "nombre d'orga supprimées ".$nbOrgaDealWith;
			//print_r($couple);


			$projects = PHDB::find(Project::COLLECTION, array(
				"source.key" => "terramies"));
			$nbOrgaDealWith=0;
			$name=[];
			$id=[];
			$couple=[];
			$formatedId = [];
			$formatedId[]=[];
			foreach($projects as $data){
			
				$formatedId =  get_object_vars(($data["_id"]));

				PHDB::remove(Project::COLLECTION,array(
					"source.key" => "terramies"));
				Slug::removeByParentIdAndType($formatedId['$id'],Project::COLLECTION);
				$nbOrgaDealWith++;
				
				array_push($name, $data["name"]);
				array_push($id, $formatedId['$id']);

			}
			$couple = array_combine($name, $id);
			$str .= "nombre de projets supprimés ".$nbOrgaDealWith;
			//print_r($couple);


			$news = PHDB::find(News::COLLECTION, array(
				"source.key" => "terramies"));

			$nbOrgaDealWith=0;
			foreach($news as $data){
				PHDB::remove(News::COLLECTION,array(
					"source.key" => "terramies"));
				
				$nbOrgaDealWith++;
			}
			$str .= "nombre de news supprimées ".$nbOrgaDealWith;
		}
		return $str;
	}

	public function actionFixBugCoutryReunion(){
		$str = "";
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
			$nbelement = 0 ;
			$elements = PHDB::find(Organization::COLLECTION, array("address.addressCountry" => "Réunion"));
			foreach (@$elements as $keyElt => $elt) {
				if(!empty($elt["address"]["postalCode"]) || !empty($elt["address"]["cp"])){
					$cpElt = (!empty($elt["address"]["postalCode"])?$elt["address"]["postalCode"]:$elt["address"]["cp"]);
					$where = array("postalCodes.postalCode" => $cpElt);
					$cities = PHDB::find("cities",$where);
					foreach (@$cities as $keyCity => $city) {
							$address = array(
						        "@type" => "PostalAddress",
						        "codeInsee" => $city["insee"],
						        "addressCountry" => $city["country"],
						        "postalCode" => $cpElt,
						        "streetAddress" => ((@$elt["address"]["streetAddress"])?trim(@$fieldValue["address"]["streetAddress"]):""),
						        "depName" => $city["depName"],
						        "regionName" => $city["regionName"],
						    	);

							$find = false;
					   		foreach ($city["postalCodes"] as $keyCp => $cp) {
					   			if($cp["postalCode"] == $cpElt){
					   				$address["addressLocality"] = $cp["name"];
					   				$geo = $cp["geo"];
					   				$geoPosition = $cp["geoPosition"];
					   				$find = true;
					   				break;
					   			}
					   		}

					   		if($find == false){
					   			$address["addressLocality"] = $city["alternateName"];
					   			$geo = $city["geo"];
					   			$geoPosition = $city["geoPosition"];
					   		}
						break;  	
					}
					
					$nbelement ++ ;
					$elt["modifiedByBatch"][] = array("fixBugCoutryReunion" => new MongoDate(time()));
					$res = PHDB::update( Organization::COLLECTION, 
					  	array("_id"=>new MongoId($keyElt)),
	                    array('$set' => array(	"address" => $address,
	                    						"geo" => $geo,
	                    						"geoPosition" => $geoPosition,
	                    						"modifiedByBatch" => $elt["modifiedByBatch"])));
					$str .= "Update orga : l'id ".$keyElt."<br>" ;
					
				}else{
					$nbelement ++ ;
					$elt["modifiedByBatch"][] = array("fixBugCoutryReunion" => new MongoDate(time()));
					$res = PHDB::update( Organization::COLLECTION, 
					  	array("_id"=>new MongoId($keyElt)),
	                    array('$unset' => array("address" => ""),
	                    		'$set' => array( "modifiedByBatch" => $elt["modifiedByBatch"])));
					$str .= "Update orga : l'id ".$keyElt."<br>" ;
				}
				
			
			}
					
			$str .=  "NB Element mis à jours: " .$nbelement."<br>" ;
		}
		return $str;
	}


	public function actionRefactorSource(){
		$str = "";
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
			$types = array(Person::COLLECTION, Organization::COLLECTION, Project::COLLECTION, Event::COLLECTION);
			$nbelement = 0 ;
			foreach ($types as $keyType => $type) {
				$elements = PHDB::find($type, array("source" => array('$exists' => 1)));
				if(!empty($elements)){
					foreach (@$elements as $keyElt => $elt) {

						if(!empty($elt["source"])){
							$newsource = array();
							if(!empty($elt["source"]["key"]) && empty($elt["source"]["keys"])){
								$newsource["insertOrign"] = "import" ;
								$newsource["key"] = $elt["source"]["key"];
								$newsource["keys"][] = $elt["source"]["key"];

								if(!empty($elt["source"]["url"]))
									$newsource["url"] = $elt["source"]["url"];
								if(!empty($elt["source"]["id"])){
									if(!empty($elt["source"]["id"]['$numberLong']))

										$newsource["id"] = $elt["source"]["id"]['$numberLong'];

									else
										$newsource["id"] = $elt["source"]["id"];
								}
								if(!empty($elt["source"]["update"]))
									$newsource["update"] = $elt["source"]["update"];
								
								$nbelement ++ ;
								$elt["modifiedByBatch"][] = array("RefactorSource" => new MongoDate(time()));

								try {
									$res = PHDB::update( $type, 
								  		array("_id"=>new MongoId($keyElt)),
			                        	array('$set' => array(	"source" => $newsource,
			                        							"modifiedByBatch" => $elt["modifiedByBatch"])));
								} catch (MongoWriteConcernException $e) {
									$str .=("Erreur à la mise à jour de l'élément ".$type." avec l'id ".$keyElt);
									die();
								}
								$str .= "Elt mis a jour : ".$type." et l'id ".$keyElt."<br>" ;


							}
						}
					}
				}
			}		
			$str .=  "NB Element mis à jours: " .$nbelement."<br>" ;
		}
		return $str;
	}

	/**
	 * Refactor events with no timezone depending on country
	 * Must be launch only once !
	 */
	public function actionAddTZOnEventDates(){
		$str = "";
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
			$nbelement = 0 ;
			$nbelementPassed = 0 ;
			$nbelementRE = 0 ;
			$nbelementBE = 0 ;
			$nbelementFR = 0 ;
			$nbelementNC = 0 ;
			$nbelementUnknown = 0 ;
			$nbelementDateString = 0 ;
			$timezoneArray = array("RE" => 4, "FR" => 1, "NC" => 11, "BE" => 1);

			$elements = PHDB::find(Event::COLLECTION, array());
			foreach (@$elements as $keyElt => $elt) {
				if (isset($elt["modifiedByBatch"])) {
					$alreadyUpdated = false;
					foreach ($elt["modifiedByBatch"] as $value) {
						if (isset($value["addTZOnEventDates"])) {
							$nbelementPassed++;
							$alreadyUpdated = true;
							break;
						}
					}
					if ($alreadyUpdated) continue;
				}
				if(empty($elt["address"]["addressCountry"]) || 
				   empty($timezoneArray[$elt["address"]["addressCountry"]])) {
					$str .= "Pas de country ou country inconnu pour l'événement : ".$keyElt."</br>";
					$nbelementUnknown++;
					continue;
				}
				$timezone = $timezoneArray[$elt["address"]["addressCountry"]];
				if (isset($elt["startDate"]) && isset($elt["endDate"]) && (gettype($elt["startDate"]) == "object" && gettype($elt["endDate"]) == "object")) {
					//Set TZ to UTC in order to be the same than Mongo
					$startDate = new DateTime(date(DateTime::ISO8601, $elt["startDate"]->sec));
					$startDate = $startDate->sub(new DateInterval("PT".$timezone."H"));
					$endDate = new DateTime(date(DateTime::ISO8601, $elt["endDate"]->sec));
					$endDate = $endDate->sub(new DateInterval("PT".$timezone."H"));
					//$startDate = $elt["startDate"]->toDateTime()->sub(new DateInterval("PT".$timezone."H"));
					//$endDate = $elt["endDate"]->toDateTime()->sub(new DateInterval("PT".$timezone."H"));
					${'nbelement'.$elt["address"]["addressCountry"]}++;
				//On en profite pour revoir les dates des événements qui sont en string ou sans date
				} else {
					$nbelementDateString++;
					$startDate = new DateTime();
					$startDate->sub(new DateInterval("P1D"));
					$endDate = new DateTime();
					$endDate->sub(new DateInterval("P2D"));
				}
				//update the event
				$elt["modifiedByBatch"][] = array("addTZOnEventDates" => new MongoDate(time()));
				PHDB::update( Event::COLLECTION, array("_id" => new MongoId($keyElt)), 
			                          array('$set' => array(
			                          		"startDate" => new MongoDate($startDate->getTimestamp()), 
			                          		"endDate" => new MongoDate($endDate->getTimestamp()),
			                        		"modifiedByBatch" => $elt["modifiedByBatch"])));
				$nbelement++;
			}
					
			$str .=  "Event Reunion mis à jours: " .$nbelementRE."<br>" ;
			$str .=  "Event France mis à jours: " .$nbelementFR."<br>" ;
			$str .=  "Event Belgique mis à jours: " .$nbelementBE."<br>" ;
			$str .=  "Event NC mis à jours: " .$nbelementNC."<br>" ;
			$str .=  "NB Element mis à jours: " .$nbelement."<br>" ;
			$str .=  "NB Element passé : " .$nbelementPassed."<br>" ;
			$str .=  "NB Element inconnu : " .$nbelementUnknown."<br>" ;
			$str .=  "NB Element date en string : " .$nbelementDateString."<br>" ;
		}
		return $str;
	}


	/*public function actionNameHtmlSpecialCaractere(){
	if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
		$types = array(Person::COLLECTION, Organization::COLLECTION, Project::COLLECTION, Event::COLLECTION);
		$nbelement = 0 ;
		foreach ($types as $keyType => $type) {
			$elements = PHDB::find($type, array("name" => array('$exists' => 1)));
			if(!empty($elements)){
				foreach (@$elements as $keyElt => $elt) {
					if(!empty($elt["name"])){
						$nbelement ++ ;
						$elt["modifiedByBatch"][] = array("NameHtmlSpecialCaractere" => new MongoDate(time()));
						try {
							$res = PHDB::update( $type, 
						  		array("_id"=>new MongoId($keyElt)),
	                        	array('$set' => array(	"name" => htmlspecialchars($elt["name"]),
	                        							"modifiedByBatch" => $elt["modifiedByBatch"])));
						} catch (MongoWriteConcernException $e) {
							$str .=("Erreur à la mise à jour de l'élément ".$type." avec l'id ".$keyElt);
							die();
						}
						$str .= "Elt mis a jour : ".$type." et l'id ".$keyElt."<br>" ;
					}
				}
			}
		}		
		$str .=  "NB Element mis à jours: " .$nbelement."<br>" ;
	}}*/


	public function actionNameHtmlSpecialCharsDecode(){
		$str = "";
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
			$types = array(Person::COLLECTION, Organization::COLLECTION, Project::COLLECTION, Event::COLLECTION);
			$nbelement = 0 ;
			foreach ($types as $keyType => $type) {
				$elements = PHDB::find($type, array("name" => array('$exists' => 1)));
				if(!empty($elements)){
					foreach (@$elements as $keyElt => $elt) {
						if(!empty($elt["name"])){
							$nbelement ++ ;
							$elt["modifiedByBatch"][] = array("NameHtmlSpecialCharsDecode" => new MongoDate(time()));
							try {
								$res = PHDB::update( $type, 
							  		array("_id"=>new MongoId($keyElt)),
		                        	array('$set' => array(	"name" => htmlspecialchars_decode($elt["name"]),
		                        							"modifiedByBatch" => $elt["modifiedByBatch"])));
							} catch (MongoWriteConcernException $e) {
								$str .=("Erreur à la mise à jour de l'élément ".$type." avec l'id ".$keyElt);
								die();
							}
							$str .= "Elt mis a jour : ".$type." et l'id ".$keyElt."<br>" ;
						}
					}
				}
			}		
			$str .=  "NB Element mis à jours: " .$nbelement."<br>" ;
		}
		return $str;
	}

	public function actionAddDepAndRegionAndCountryInAddress(){
		$str = "";
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){


			$types = array(Person::COLLECTION, Organization::COLLECTION, Project::COLLECTION, Event::COLLECTION);
			$nbelement = 0 ;
			$arrayDep = array("address.depName" => array('$exists' => 0));
			$arrayRegion = array("address.regionName" => array('$exists' => 0));
			$arrayCountry = array("address.addressCountry" => array('$exists' => 0));
			$arrayStreet = array("address.streetAddress" => array('$exists' => 0));
			$where = array('$and' => array(
							array("address" => array('$exists' => 1)), 
							array('$or' => array($arrayDep, $arrayRegion, $arrayCountry, $arrayStreet))
						));
			
			foreach ($types as $keyType => $type) {
				
				$elements = PHDB::find($type, $where);
				
				if(!empty($elements)){
					foreach (@$elements as $keyElt => $elt) {
						if(!empty($elt["name"])){
							$nbelement ++ ;
							$elt["modifiedByBatch"][] = array("AddDepAndRegionAndCountryInAddress" => new MongoDate(time()));
							$address = $elt["address"];

							if (isset($address["codeInsee"])) {
								$depAndRegion = City::getDepAndRegionByInsee($address["codeInsee"]);

								$address["depName"] = (empty($depAndRegion["depName"]) ? "" : $depAndRegion["depName"]);
								$address["regionName"] = (empty($depAndRegion["regionName"]) ? "" : $depAndRegion["regionName"]);
								$address["addressCountry"] = (empty($depAndRegion["country"]) ? "" : $depAndRegion["country"]);
								$address["streetAddress"] = (empty($elt["address"]["streetAddress"]) ? "" : $elt["address"]["streetAddress"]);
								try {
									$res = PHDB::update( $type, 
								  		array("_id"=>new MongoId($keyElt)),
			                        	array('$set' => array(	"address" => $address,
			                        							"modifiedByBatch" => $elt["modifiedByBatch"])));
								} catch (MongoWriteConcernException $e) {
									$str .=("Erreur à la mise à jour de l'élément ".$type." avec l'id ".$keyElt);
									die();
								}
								$str .= "Elt mis a jour : ".$type." et l'id ".$keyElt."<br>" ;
							} else {
								$str .= "Pas de mise a jour : ".$type." et l'id ".$keyElt."<br>" ;
							}
						}
					}
				}
			}		
			$str .=  "NB Element mis à jours: " .$nbelement."<br>" ;
		}
		return $str;
	}

	public function actionHtmlToMarkdown(){
		$str = "";
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
			$html = "<h3> TEst </h3>
					<p>Welcome to the demo:</p>
					<ol>
					<li>Write Markdown text on the left</li>
					<li>Hit the <strong>Parse</strong> button or <code>⌘ + Enter</code></li>
					<li>See the result to on the right</li>
					</ol>" ;

			$str .= $html ;

			$str .= "<br/><br/><br/><br/>";

			
			

			try {
	            //$mailError = new MailError($_POST);
	            $converter = new Htmlconverter();
				$mark = $converter->convert($html);
	        } catch (CTKException $e) {
	            Rest::sendResponse(450, "Webhook : ".$e->getMessage());
	            die;
	        }
			$str .= $mark;
		}
		return $str;
	}

	public function actionDescInHtml(){
		$str = "";
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
			$types = array(Person::COLLECTION, Organization::COLLECTION, Project::COLLECTION, Event::COLLECTION);
			$nbelement = 0 ;
			foreach ($types as $keyType => $type) {
				$elements = PHDB::find($type, array("description" => array('$exists' => 1)));
				if(!empty($elements)){
					foreach (@$elements as $keyElt => $elt) {
						if(!empty($elt["name"])){
							$nbelement ++ ;
							$elt["modifiedByBatch"][] = array("DescInHtml" => new MongoDate(time()));
							try {
								$res = PHDB::update( $type, 
							  		array("_id"=>new MongoId($keyElt)),
		                        	array('$set' => array(	"descriptionHTML" => true,
		                        							"modifiedByBatch" => $elt["modifiedByBatch"])));
							} catch (MongoWriteConcernException $e) {
								$str .=("Erreur à la mise à jour de l'élément ".$type." avec l'id ".$keyElt);
								die();
							}
							$str .= "Elt mis a jour : ".$type." et l'id ".$keyElt."<br>" ;
						}
					}
				}
			}		
			$str .=  "NB Element mis à jours: " .$nbelement."<br>" ;
		}
		return $str;
	}

	public function actionChangePhoneObjectToArray(){
		$str = "";
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
			$types = array(Person::COLLECTION, Organization::COLLECTION);
			$nbelement = 0 ;
			foreach ($types as $keyType => $type) {
				$elements = PHDB::find($type, array("telephone" => array('$exists' => 1)));
				if(!empty($elements)){
					foreach (@$elements as $keyElt => $elt) {
						if(!empty($elt["name"])){
							$nbelement ++ ;
							$elt["modifiedByBatch"][] = array("ChangePhoneObjectToArray" => new MongoDate(time()));
							
							if(!empty($elt["telephone"]["fixe"])) {
								$elt["telephone"]["fixe"] = json_decode(json_encode($elt["telephone"]["fixe"]), true);
							}

							if(!empty($elt["telephone"]["mobile"])){
								$elt["telephone"]["fixe"] = json_decode(json_encode($elt["telephone"]["mobile"]), true);
							}

							if(!empty($elt["telephone"]["fax"]) ){
								$elt["telephone"]["fax"] = json_decode(json_encode($elt["telephone"]["fax"]), true);
							}

							try {
								$res = PHDB::update( $type, 
							  		array("_id"=>new MongoId($keyElt)),
		                        	array('$set' => array(	"telephone" => $elt["telephone"],
		                        							"modifiedByBatch" => $elt["modifiedByBatch"])));
							} catch (MongoWriteConcernException $e) {
								$str .=("Erreur à la mise à jour de l'élément ".$type." avec l'id ".$keyElt);
								die();
							}
							$str .= "Elt mis a jour : ".$type." et l'id ".$keyElt."<br>" ;
						}
					}
				}
			}		
			$str .=  "NB Element mis à jours: " .$nbelement."<br>" ;
		}
		return $str;
	}

	public function actionAddGeoShapeMissing(){
		$str = "";
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
			$cities = PHDB::find(City::COLLECTION, array("geoShape" => array('$exists' => 0)));

			foreach ($cities as $key => $city) {
				$name = str_replace(" ", "+", trim($city["name"]));
				$url = "http://nominatim.openstreetmap.org/search?format=json&addressdetails=1&city=".$name."&countrycodes=FR&polygon_geojson=1&extratags=1" ;
				$nominatim = json_decode(file_get_contents($url), true);
				$find = false ;
				if(!empty($nominatim)){
					foreach ($nominatim as $key => $value) {
						if($value["osm_type"] == "relation" && !empty($value["geojson"]) && $find == false){
							$str .= $city["insee"]." : ". $city["name"]." : ".$value["osm_id"]."<br/>";
							$find = true ;

							$city["modifiedByBatch"][] = array("AddGeoShapeMissing" => new MongoDate(time()));
							try {
								$res = PHDB::update( City::COLLECTION, 
							  		array("_id"=>new MongoId((String)$city["_id"])),
		                        	array('$set' => array(	"geoShape" => $value["geojson"],
		                        							"osmID" => $value["osm_id"],
		                        							"modifiedByBatch" => $city["modifiedByBatch"])));
							} catch (MongoWriteConcernException $e) {
								$str .=("Erreur à la mise à jour de l'élément ".City::COLLECTION." avec l'id ".$key);
								die();
							}
						}
					}
				}
			}
		}
		return $str;
	}


	public function actionChangePrefObjectToArray(){
		$str = "";
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
			$types = array(Person::COLLECTION);
			$nbelement = 0 ;
			foreach ($types as $keyType => $type) {
				$elements = PHDB::find($type, array("preferences" => array('$exists' => 1)));
				if(!empty($elements)){
					foreach (@$elements as $keyElt => $elt) {
						if(!empty($elt["name"])){
							$nbelement ++ ;
							$elt["modifiedByBatch"][] = array("ChangePrefObjectToArray" => new MongoDate(time()));
							
							if(!empty($elt["preferences"]["publicFields"])) {
								$elt["preferences"]["publicFields"] = json_decode(json_encode($elt["preferences"]["publicFields"]), true);
							}

							if(!empty($elt["preferences"]["privateFields"])){
								$elt["preferences"]["privateFields"] = json_decode(json_encode($elt["preferences"]["privateFields"]), true);
							}

							try {
								$res = PHDB::update( $type, 
							  		array("_id"=>new MongoId($keyElt)),
		                        	array('$set' => array(	"preferences" => $elt["preferences"],
		                        							"modifiedByBatch" => $elt["modifiedByBatch"])));
							} catch (MongoWriteConcernException $e) {
								$str .=("Erreur à la mise à jour de l'élément ".$type." avec l'id ".$keyElt);
								die();
							}
							$str .= "Elt mis a jour : ".$type." et l'id ".$keyElt."<br>" ;
						}
					}
				}
			}		
			$str .=  "NB Element mis à jours: " .$nbelement."<br>" ;
		}
		return $str;
	}

	public function actionInitMultiScope(){
		$str = "";
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
			$types = array(Person::COLLECTION);
			$nbelement = 0 ;
			foreach ($types as $keyType => $type) {
				$elements = PHDB::find($type, array("multiscopes" => array('$exists' => 1)));
				if(!empty($elements)){
					foreach (@$elements as $keyElt => $elt) {
						if(!empty($elt["name"])){
							$nbelement ++ ;
							$elt["modifiedByBatch"][] = array("InitMultiScope" => new MongoDate(time()));
							
							try {
								$res = PHDB::update( $type, 
							  		array("_id"=>new MongoId($keyElt)),
		                        	array(	'$unset' 	=> array(	"multiscopes" => null),
		                        			'$set' 		=> array(	"modifiedByBatch" => $elt["modifiedByBatch"])));
							} catch (MongoWriteConcernException $e) {
								$str .=("Erreur à la mise à jour de l'élément ".$type." avec l'id ".$keyElt);
								die();
							}
							$str .= "Elt mis a jour : ".$type." et l'id ".$keyElt."<br>" ;
						}
					}
				}
			}		
			$str .=  "NB Element mis à jours: " .$nbelement."<br>" ;
		}
		return $str;
	}

	public function actionObjectObjectTypeNewsToObjectType(){
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
		  	// Check in mongoDB
		  	//// db.getCollection('news').find({"object.objectType": {'$exists':true}})
		  	// Check number of news to formated
		  	//// db.getCollection('news').find({"object.objectType": {'$exists':true}}).count()
		  	$news=PHDB::find(News::COLLECTION,array("object.objectType"=>array('$exists'=>true)));
		  	$nbNews=0;
		  	foreach($news as $key => $data){
		  		$newObject=array("id"=>$data["object"]["id"], "type"=> $data["object"]["objectType"]);
				PHDB::update(News::COLLECTION,
					array("_id" => $data["_id"]) , 
					array('$set' => array("object" => $newObject))
				);
		  		$nbNews++;
		  	}
		  	return "nombre de news traitées:".$nbNews." news";
		}
	}


	public function actionUpOldNotifications(){
	  	if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
	  	// Update notify.id 
	  	$notifications=PHDB::find(ActivityStream::COLLECTION,array("notify.id"=>array('$exists'=>true)));
	  	$nbNotifications=0;
	  	////print_r($notifications);
	  	foreach($notifications as $key => $data){
	  		////print_r($data["notify"]["id"]);
	  		$update=false;
	  		$newArrayId=array();
	  		foreach($data["notify"]["id"] as $val){
				if(gettype($val)=="string"){
					//$str .=($val);
	  				$newArrayId[$val]=array("isUnsee"=>true,"isUnread"=>true);
	  				$update=true;
	  			}
	  		}
	  		if($update){
	  			////print_r($newArrayId);
				PHDB::update(ActivityStream::COLLECTION,
					array("_id" => $data["_id"]) , 
					array('$set' => array("notify.id" => $newArrayId))
				);
				$nbNotifications++;
			}
	  	}
	  	return "nombre de notifs traitées:".$nbNotifications." notifs";
	  }
	}


  	public function actionSharedByRefactor(){
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
		  	// Check in mongoDB
		  	//// db.getCollection('news').find({"object.objectType": {'$exists':true}})
		  	// Check number of news to formated
		  	//// db.getCollection('news').find({"object.objectType": {'$exists':true}}).count()
		  	$news=PHDB::find(News::COLLECTION,array());
		  	$nbNews=0;
		  	foreach($news as $key => $data){
		  		if(@$data["targetIsAuthor"] && $data["targetIsAuthor"] == true ){
					PHDB::update(News::COLLECTION,
						array("_id" => $data["_id"]) , 
						array('$set' => array("sharedBy" => array(array('id'=> $data["target"]["id"],
	        					'type'=>$data["target"]["type"],
	        					'updated'=> $data["created"]))))
					);
				} else {
					PHDB::update(News::COLLECTION,
						array("_id" => $data["_id"]) , 
						array('$set' => array("sharedBy" => array(array('id'=> $data["author"],
	        					'type'=>"citoyens",
	        					'updated'=> $data["created"]))))
					);
				}
		  		$nbNews++;
		  	}
		  	return "nombre de news traitées:".$nbNews." news";
	  		
	  	}
	 }
	public function actionChangeEventFrenchType(){
		$str = "";
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
			$str .= "Traitement des événements avec le type 'exposition' ou 'concours'";
		  	$events=PHDB::find(Event::COLLECTION,array('$or' => array( array('type' => 'concours'), array('type' => 'exposition') )));
		  	$nbEvents=0;
		  	
		  	foreach($events as $key => $data){
		  		if($data["type"]=="exposition")
		  			$newType="exhibition";
		  		else if($data["type"]=="concours")
		  			$newType="contest";
				PHDB::update(Event::COLLECTION,
						array("_id" => $data["_id"]) , 
						array('$set' => array('type'=> $newType)));
				$nbEvents++;
				
		  	}
		  	$str .= "nombre de events traités:".$nbEvents." events";
	  		
	  	}
	  	return $str;
	 }
	 public function actionChangePoiType(){
	 	$str = "";
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
			$str .= "Traitement des p(zéro)is<br/>";
		  	$pois=PHDB::find(Poi::COLLECTION);
		  	$nbPois=0;
		  	
		  	foreach($pois as $key => $data){
		  		if(!@$data["type"]){
		  			$newType="other";
		  		}else{
			  		if($data["type"]=="poi")
			  			$newType="other";
			  		else if($data["type"]=="streetArts")
			  			$newType="streetArt";
			  		else if($data["type"]=="ficheBlanche")
			  			$newType="documentation";
			  		else if($data["type"]=="RessourceMaterielle")
			  			$newType="materialRessource";
			  		else
			  			$newType=$data["type"];
		  		}
				PHDB::update(Poi::COLLECTION,
						array("_id" => $data["_id"]) , 
						array('$set' => array('type'=> $newType)));
				$nbPois++;
				
		  	}
		  	$str .= "nombre de p0is traités:".$nbPois." pois";
	  		
	  	}else
	  		$str .= "jajajajajja: Tu as volé trop près du soleil Icare";

	  	return $str;
	 }

	public function actionCreatorUpdatedOnNotifications(){
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
		  	$notifications=PHDB::find(ActivityStream::COLLECTION,array("notify"=>array('$exists'=>true),"updated"=>array('$exists'=>false)));
		  	$nbNotifs=0;
		  	$nbNotifsDeleted=0;
		  	foreach($notifications as $key => $data){
		  		if(!@$data["created"] ){
		  			PHDB::remove(ActivityStream::COLLECTION, array("_id"=>new MongoId($key)));
		  			$nbNotifsDeleted++;
		  		}
		  		if(!@$data["updated"] && @$data["created"] ){
					PHDB::update(ActivityStream::COLLECTION,
						array("_id" => $data["_id"]) , 
						array('$set' => array('updated'=> $data["created"])));
					$nbNotifs++;
				}
				
		  	}
		  	return "nombre de notifs attendus = environs 15.3k".
		  		   "Nombre de notifs deleted car pas de created (normalement 383):".$nbNotifsDeleted." notifs<br/>".
		  		   "nombre de notifications traitées:".$nbNotifs." notifs";
	  		
	  	}
	 }


  	public function actionUpdateRegion(){
  		$str = "";
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){

			$newsRegions = array(
							//array("new_code","new_name","former_code","former_name"),
							array("01","Guadeloupe","01","Guadeloupe"),
							array("02","Martinique","02","Martinique"),
							array("03","Guyane","03","Guyane"),
							array("04","La Réunion","04","La Réunion"),
							array("06","Mayotte","06","Mayotte"),
							array("11","Île-de-France","11","Île-de-France"),
							array("24","Centre-Val de Loire","24","Centre"),
							array("27","Bourgogne-Franche-Comté","26","Bourgogne"),
							array("27","Bourgogne-Franche-Comté","43","Franche-Comté"),
							array("28","Normandie","23","Haute-Normandie"),
							array("28","Normandie","25","Basse-Normandie"),
							array("32","Nord-Pas-de-Calais-Picardie","31","Nord-Pas-de-Calais"),
							array("32","Nord-Pas-de-Calais-Picardie","22","Picardie"),
							array("44","Alsace-Champagne-Ardenne-Lorraine","41","Lorraine"),
							array("44","Alsace-Champagne-Ardenne-Lorraine","42","Alsace"),
							array("44","Alsace-Champagne-Ardenne-Lorraine","21","Champagne-Ardenne"),
							array("52","Pays de la Loire","52","Pays de la Loire"),
							array("53","Bretagne","53","Bretagne"),
							array("75","Aquitaine-Limousin-Poitou-Charentes","72","Aquitaine"),
							array("75","Aquitaine-Limousin-Poitou-Charentes","54","Poitou-Charentes"),
							array("75","Aquitaine-Limousin-Poitou-Charentes","74","Limousin"),
							array("76","Languedoc-Roussillon-Midi-Pyrénées","73","Midi-Pyrénées"),
							array("76","Languedoc-Roussillon-Midi-Pyrénées","91","Languedoc-Roussillon"),
							array("84","Auvergne-Rhône-Alpes","82","Rhône-Alpes"),
							array("84","Auvergne-Rhône-Alpes","83","Auvergne"),
							array("93","Provence-Alpes-Côte d'Azur","93","Provence-Alpes-Côte d'Azur"),
							array("94","Corse","94","Corse")
						);

			foreach ($newsRegions as $key => $region){

				$str .= "News : (".$region[0].") ".$region[1]." ---- Ancien : (".$region[2].") ".$region[3]."</br>" ;

				$cities = PHDB::find(City::COLLECTION,array("region" => $region[2]));
				$res = array("result" => false , "msg" => "La région (".$region[0].") ".$region[1]." n'existe pas");
				
				if(!empty($cities)){
					foreach ($cities as $key => $city) {
						$res = PHDB::update( City::COLLECTION, 
										  	array("_id"=>new MongoId($key)),
					                        array('$set' => array(	"region" => $region[0],
					                        						"regionName" => $region[1]))
					                    );
					}
					$str .= "</br></br></br>";
					$str .= "Result : ".$res["result"]." | ".$res["msg"]."</br></br></br>";
				}
				else
					$str .= "Result : ".$res["result"]." | ".$res["msg"]."</br></br></br>";

			}

		}
		return $str;
	}

	public function actionDepRefactorCitiesZones(){
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
			ini_set('memory_limit', '-1');
			$nbelement = 0 ;
			$dep = array();
			$cities = PHDB::find(City::COLLECTION, array("depName" => array('$exists' => 1)));
			if(!empty($cities)){
				foreach (@$cities as $keyElt => $city) {
					if(!empty($city["depName"]) && trim($city["depName"]) != "" && !in_array($city["depName"], $dep)){
						$dep[] = $city["depName"];
						$zone = Zone::createLevel($city["depName"], $city["country"], "4");
						if(!empty($zone)){
							$nbelement++;
							Zone::save($zone);
						}
					}
				}
			}
			
			return  "NB Element mis à jours: " .$nbelement."<br>" ;
		}
	}


	public function actionAddZeroPostalCode(){
		$str = "";
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
			ini_set('memory_limit', '-1');
			$nbelement = 0 ;
			$where = array( '$and' => array(
										array("address.postalCode" => array('$exists' => 1)),
										array("address.addressCountry" => array('$ne' => "BE") ) ),
							'$where' => "this.address.postalCode.length == 4" );

			$orgs = PHDB::find(Organization::COLLECTION, $where);

			if(!empty($orgs)){
				foreach (@$orgs as $keyElt => $org) {
					$res = PHDB::update( Organization::COLLECTION, 
										  	array("_id"=>new MongoId($keyElt)),
					                        array('$set' => array(	"address.postalCode" => "0".$org["address"]["postalCode"]))
					                    );
					$str .= $org["name"]." : ".$org["address"]["postalCode"]." > 0".$org["address"]["postalCode"]."<br>" ;
					$nbelement++;
				}
			}
			
			$str .=  "NB Element mis à jours: " .$nbelement."<br>" ;
		}
		return $str;
	}
	//Bash on document used for profil and banner of element
	//Used for delete in gallery to know directly when a current doc is used as profil and banner  
	//find doc used current for profil and banner
	public function actionAddCurrentToDoc(){
		$str = "";
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
			$docs=PHDB::find(Document::COLLECTION, array(
				'$or'=>array(
					array("contentKey"=>"banner"),
					array("contentKey"=>"profil")
					)));
			$totalProfil=0;
			$totalBanner=0;
			foreach($docs as $key => $data){
				if(@$data["id"] && $data["type"]!="city"){
					$element=Element::getElementSimpleById($data["id"], $data["type"],null,array("profilImageUrl","profilBannerUrl"));
					$docProfilUrl=Document::getDocumentFolderUrl($data)."/".$data["name"];
					if(!empty($element["profilImageUrl"]) && @$docProfilUrl && $docProfilUrl==$element["profilImageUrl"]){
						$str .= "Profil:".$key."<br>";
						PHDB::update(Document::COLLECTION,array("_id"=>new MongoId($key)),array('$set'=>array("current"=>true)));
						$totalProfil++;
					}
					if(!empty($element["profilBannerUrl"]) && @$docProfilUrl && $docProfilUrl==$element["profilBannerUrl"]){
						$str .= "banner:".$key."<br>";
						PHDB::update(Document::COLLECTION,array("_id"=>new MongoId($key)),array('$set'=>array("current"=>true)));
						$totalBanner++;
					}
				}
			}
			$str .= "Nombre de profil used actually by element:".$totalProfil."<br>";
			$str .= "Nombre de banner used actually by element:".$totalBanner;
		}else
			$str .= "connectoi crétin";

		return $str;
	}
	
	// -------------------- Fonction pour le refactor Cities/zones
	public function actionRegionBERefactorCitiesZones(){
		$str = "";
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
			ini_set('memory_limit', '-1');
			$nbelement = 0 ;
			$region = array();
			$cities = PHDB::find(City::COLLECTION, array("regionNameBel" => array('$exists' => 1)));
			if(!empty($cities)){
				foreach (@$cities as $keyElt => $city) {
					if(!empty($city["regionNameBel"]) && trim($city["regionNameBel"]) != "" && !in_array($city["regionNameBel"], $region)){
						
						$zone = Zone::createLevel($city["regionNameBel"], $city["country"], "2");

						
						if(!empty($zone)){
							$region[] = $city["regionNameBel"];
							$nbelement++;
							Zone::save($zone);
						}else{
							$str .= "Erreur: " .$city["regionNameBel"]." : City :".(String)$city["_id"]."<br>" ;

						}
					}
				}
			}
			
			foreach (@$region as $k => $v) {
				$str .= "Good: " .$v."<br>" ;
			}
			$str .= "NB Element mis à jours: " .$nbelement."<br>" ;
		}
		return $str;
	}



	public function actionRegionRefactorCitiesZones(){
		$str = "";
		//if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
			ini_set('memory_limit', '-1');
			$nbelement = 0 ;
			$erreur = array();
			$region = array();
			$aggregate = array(
			    array(
			        '$group' => array(
			            "_id" => array(	"regionName" => '$regionName', 
			            				"country" => '$country',
			            				"regionNameBel" => '$regionNameBel' ),
			        ),
			    ),
			);

			$cities = PHDB::aggregate( City::COLLECTION, $aggregate);
			

			if(!empty($cities["result"])){
				foreach (@$cities["result"] as $keyElt => $city) {				
					if(!empty($city["_id"]["regionName"]) && trim($city["_id"]["regionName"]) != ""){
						
						$exists = PHDB::findOne(Zone::COLLECTION, array('$and' => array(
													array("name" => $city["_id"]["regionName"] ) , 
													 array("level" => "3" ) ) ) );
						if($exists== null){
							$zone = Zone::createLevel($city["_id"]["regionName"], $city["_id"]["country"], "3", ((!empty($city["_id"]["regionNameBel"])) ? $city["_id"]["regionNameBel"] : null));
							if(!empty($zone)){
								$region[] = $city["_id"]["regionName"];
								$nbelement++;
								Zone::save($zone);
							}else{
								$erreur[] = $city["_id"]["regionName"];
							}
						}
						
					}
				}
			}

			foreach (@$region as $k => $v) {
				$str .=  "Good: " .$v."<br>" ;
			}
			
			foreach (@$erreur as $k => $v) {
				$str .=  "Erreur: " .$v." : City :".$k."<br>" ;
			}
			$str .=  "NB Element mis à jours: " .$nbelement."<br>" ;
		//}
		return $str;
	}

	public function actionDepRefactorCitiesZones2(){
		$str = "";
		//if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
			ini_set('memory_limit', '-1');
			$nbelement = 0 ;
			$erreur = array();
			$region = array();
			$aggregate = array(
			    array(
			        '$group' => array(
			            "_id" => array(	"depName" => '$depName', 
			            				"country" => '$country',
			            				"regionNameBel" => '$regionNameBel',
			            				"regionName" => '$regionName', ),
			        ),
			    ),
			);

			$cities = PHDB::aggregate( City::COLLECTION, $aggregate);
			

			if(!empty($cities["result"])){
				foreach (@$cities["result"] as $keyElt => $city) {				
					if( !empty($city["_id"]["depName"]) && trim($city["_id"]["depName"]) != ""){
						
						$exists = PHDB::findOne(Zone::COLLECTION, array('$and' => array(
													array("name" => $city["_id"]["depName"] ) , 
													 array("level" => "4" ) ) ) );
						if($exists== null){
							$zone = Zone::createLevel($city["_id"]["depName"], $city["_id"]["country"], "4", ((!empty($city["_id"]["regionNameBel"])) ? $city["_id"]["regionNameBel"] : null), ((!empty($city["_id"]["regionName"])) ? $city["_id"]["regionName"] : null));
							if(!empty($zone)){
								$region[] = $city["_id"]["depName"];
								$nbelement++;
								Zone::save($zone);
							}else{
								$erreur[] = $city["_id"]["depName"];
							}
						}
						
					}
				}
			}

			foreach (@$region as $k => $v) {
				$str .=  "Good: " .$v."<br>" ;
			}
			
			foreach (@$erreur as $k => $v) {
				$str .=  "Erreur: " .$v." : City :".$k."<br>" ;
			}
			$str .=  "NB Element mis à jours: " .$nbelement."<br>" ;
		//}
		return $str;
	}


	public function actionAddDepName(){
		$str = "";
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
			ini_set('memory_limit', '-1');
			$nbelement = 0 ;
			$region = array();
			$cities = PHDB::find(City::COLLECTION, array("depName" => array('$exists' => 0) ) );
			if(!empty($cities)){
				foreach (@$cities as $key => $city) {
					if(!empty($city["dep"])){
						$depName = PHDB::findOne(City::COLLECTION, array( '$and' => array( 
																			array( "dep" => $city["dep"] ),

																			array( '$or' => array(
																				array("depName" => array('$exists' => 1) ),
																				array("regionName" => array('$exists' => 1) ) ) ) 
																		) ) );
						if(!empty($depName)){
							$name = "";
							if(!empty($depName["depName"])){
								$name = $depName["depName"] ;
							}else if(!empty($depName["regionName"])){
							
								$name = ucfirst(strtolower($depName["regionName"])) ;
							}

							$res = PHDB::update( City::COLLECTION, 
										  	array("_id"=>new MongoId($key)),
					                        array('$set' => array(	"depName" => $name ))
					                    );
							$str .=  "Good: ".$city["name"]." ".$name."<br>" ;
							$nbelement++;
							
						}
						else
							$str .=  "Erreur: ".$city["name"]." ".$depName["depName"]."<br>" ;
					}

				}
			}

			$str .=  "NB Element mis à jours: " .$nbelement."<br>" ;
		}
		return $str;
	}



	public function actionLinkCityAndZone(){
		$str = "";
		//if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
			ini_set('memory_limit', '-1');
			ini_set('max_execution_time', 300);
			$nbelement = 0 ;
			$erreur = array();
			$region = array();
			$cities = PHDB::find(City::COLLECTION, array("modifiedByBatch.LinkCityAndZone" => 
															array('$exists' => 0)) );

			if(!empty($cities)){
				foreach (@$cities as $keyElt => $city) {
					//if($nbelement == 0){
						$set = array();			
						if(!empty($city["dep"]) && !empty($city["depName"])){
							$city["dep"] = Zone::getIdLevelByNameAndCountry($city["depName"], "4", $city["country"]);
							$set["dep"] = $city["dep"];
						}

						if(!empty($city["region"]) && !empty($city["regionName"])){
							$city["region"] = Zone::getIdLevelByNameAndCountry($city["regionName"], "3", $city["country"]);
							$set["region"] = $city["region"];
						}

						if(!empty($city["regionBel"])){
							$city["regionBel"] = Zone::getIdLevelByNameAndCountry($city["regionNameBel"], "2", $city["country"]);
							$set["regionBel"] = $city["regionBel"];
						}


						if($set ==  true){
							$city["modifiedByBatch"][] = array("LinkCityAndZone" => new MongoDate(time()));

							//var_dump($keyElt);
							if($city["country"] == "BE"){
								$res = PHDB::update( City::COLLECTION, 
										  	array("_id"=>new MongoId($keyElt)),
					                        array('$set' => array(	"dep" => $city["dep"],
					                        						"region" => $city["region"],
					                        						"regionBel" => $city["regionBel"],
					                        						"modifiedByBatch" => $city["modifiedByBatch"]))
					                    );
							}else{
								$res = PHDB::update( City::COLLECTION, 
										  	array("_id"=>new MongoId($keyElt)),
					                        array('$set' => array(	"dep" => $city["dep"],
					                        						"region" => $city["region"],
					                        						"modifiedByBatch" => $city["modifiedByBatch"]))
					                    );
							}
							
							$str .=  "Good: ".$city["name"]." ".$city["country"]." : ".$keyElt."<br>" ;
							$nbelement++;
						}
						else
							$str .=  "Erreur: ".$city["name"]." ".$city["country"]." : ".$keyElt."<br>" ;
					//}
					
				}
			}
			$str .=  "NB Element mis à jours: " .$nbelement."<br>" ;
		//}
		return $str;
	}




	// -------------------- Fonction pour le refactor Cities/zones
	public function actionFinalisation(){
		$str = "";
		//if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
			ini_set('memory_limit', '-1');
			$nbelement = 0 ;
			$region = array();
			$zones = PHDB::find(Zone::COLLECTION, array('$and' => array(
															array("level" => "3"),
															array("countryCode" => "FR",
															/*array("level2" => array('$exists' => 0)*/)
														)));
			if(!empty($zones)){
				foreach (@$zones as $keyElt => $zone) {
					$str .=  "Good: ".$zone["name"]." ".$zone["level"]."<br>" ;
						$nbelement++;
				}
			}

			$str .=  "NB Element mis à jours: " .$nbelement."<br>" ;
		//}
		return $str;
	}

	

	// -------------------- Fonction pour le refactor Cities/zones
	public function actionRenameRegion(){
		$str = "";
		//if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
			ini_set('memory_limit', '-1');
			$nbelement = 0 ;
			$regionName = array("Alsace-Champagne-Ardenne-Lorraine" => "Grand Est",
							"Nord-Pas-de-Calais-Picardie" => "Hauts-de-France",
							"Aquitaine-Limousin-Poitou-Charentes" => "Nouvelle-Aquitaine",
							"Languedoc-Roussillon-Midi-Pyrénées" => "Occitanie");
			
			foreach (@$regionName as $old => $new) {
				$zones = PHDB::find(Zone::COLLECTION, array("name" => $old));
				if(!empty($zones)){
					foreach (@$zones as $keyElt => $zone) {
						$res = PHDB::update( Zone::COLLECTION, 
									  	array("_id"=>new MongoId($keyElt)),
				                        array('$set' => array("name" => $new))
				        );
						$str .=  "Good: ".$zone["name"]." ".$zone["level"]."<br>" ;
							$nbelement++;
					}
				}

				$cities = PHDB::find(City::COLLECTION, array("regionName" => $old));
				if(!empty($cities)){
					foreach (@$cities as $keyElt => $city) {
						$res = PHDB::update( City::COLLECTION, 
									  	array("_id"=>new MongoId($keyElt)),
				                        array('$set' => array("regionName" => $new))
				        );
						$str .=  "Good: ".$city["name"]."<br>" ;
							$nbelement++;
					}
				}
			}

			$str .=  "NB Element mis à jours: " .$nbelement."<br>" ;
		//}
		return $str;
	}



	// -------------------- Fonction pour le refactor Cities/zones
	public function actionLevelStringToArray(){
		$str = "";
		//if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
			ini_set('memory_limit', '-1');
			$nbelement = 0 ;
			
			$zones = PHDB::find(Zone::COLLECTION, array());
			if(!empty($zones)){
				foreach (@$zones as $keyElt => $zone) {

					if(is_string($zone["level"])){
						$levels = array($zone["level"]);

						$res = PHDB::update( Zone::COLLECTION, 
									  	array("_id"=>new MongoId($keyElt)),
										array('$set' => array("level" => $levels))
						);

						$str .=  "Good: ".$zone["name"]."<br>" ;
							$nbelement++;
					}
				}
			}
			$str .=  "NB Element mis à jours: " .$nbelement."<br>" ;
		//}
		return $str;
	}


	public function actionCreateKeyZone(){
		$str = "";
		//if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
			ini_set('memory_limit', '-1');
			ini_set('max_execution_time', 300);
			$nbelement = 0 ;
			$erreur = array();
			$region = array();
			$zones = PHDB::find(Zone::COLLECTION, array("key" => array('$exists' => 0)) );

			if(!empty($zones)){
				foreach (@$zones as $keyElt => $zone) {
					//if($nbelement == 0){
						
						$key = Zone::createKey($zone);
						$res = PHDB::update( Zone::COLLECTION, 
									  	array("_id"=>new MongoId($keyElt)),
				                        array('$set' => array(	"key" => $key))
				        );
							
						$str .=  "Good: ".$zone["name"]." ".$zone["level"][0]." : ".$key."<br>" ;
						$nbelement++;
						
						
					//}
					
				}
			}
			$str .=  "NB Element mis à jours: " .$nbelement."<br>" ;
		return $str;
	}

	public function actionCreateKeyCity(){
		$str = "";
		//if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
			ini_set('memory_limit', '-1');
			ini_set('max_execution_time', 300);
			$nbelement = 0 ;
			$erreur = array();
			$region = array();
			$cities = PHDB::find(City::COLLECTION, 
				array( '$and' => array(
					array("dep" => array('$exists' => 1)),
					array("key" => array('$exists' => 0)))
				));

			if(!empty($cities)){
				foreach (@$cities as $keyElt => $city) {
					if(!empty($city["dep"]) && strlen($city["dep"]) > 3){
						$zone=PHDB::findOne(Zone::COLLECTION, array("_id"=>new MongoId($city["dep"])));
						if(!empty($zone)){
							
							$key = $zone["key"]."@".$keyElt;
							$res = PHDB::update( City::COLLECTION, 
										  	array("_id"=>new MongoId($keyElt)),
					                        array('$set' => array(	"key" => $key))
					        );
								
							$str .=  "Good: ".$zone["name"]." ".$zone["level"][0]." : ".$key."<br>" ;
							$nbelement++;
							
							
						}
					}
					
				}
			}
			$str .=  "NB Element mis à jours: " .$nbelement."<br>" ;
		return $str;
	}


	public function actionAddLevel3ToLevel4(){
		$str = "";
		//if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
			ini_set('memory_limit', '-1');
			$nbelement = 0 ;
			
			$zones = PHDB::find(Zone::COLLECTION, array('$and' => array(
															array("level" => "4" ),
															array("level3" =>  array('$exists' => 0)) )));
			if(!empty($zones)){
				foreach (@$zones as $keyElt => $zone) {
					$city=PHDB::findOne( City::COLLECTION, array( "dep"=> $keyElt ) );

					if(!empty($city["region"]) && !empty($city["regionName"])) {
						//$levels = array($zone["level"]);

						$res = PHDB::update( Zone::COLLECTION, 
									  	array("_id"=>new MongoId($keyElt)),
										array('$set' => array("level3" => $city["region"]))
						);

						$str .=  "Good: ".$zone["name"]." " .$city["regionName"] . " " .$city["region"] . "<br>" ;
							$nbelement++;
					}
				}
			}
			$str .=  "NB Element mis à jours: " .$nbelement."<br>" ;
			return $str;
		//}
	}


	public function actionOccitanie(){
		$str = "";
		//if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
			ini_set('memory_limit', '-1');
			$nbelement = 0 ;
			
			$cities = PHDB::find(City::COLLECTION, array("depName" => "Phillippeville" ));
			if(!empty($cities)){
				foreach (@$cities as $keyElt => $city) {
					$res = PHDB::update( City::COLLECTION, 
									  	array("_id"=>new MongoId($keyElt)),
										array('$set' => array("depName" => "Philippeville"))
						);

					$str .=  "Good: ".$city["name"]." " .$city["depName"] . " Philippeville <br>" ;
							$nbelement++;
					
				}
			}
			$str .=  "NB Element mis à jours: " .$nbelement."<br>" ;
			return $str;
		//}
	}

	public function actionAddIdDep(){
		$str = "";
		//if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
			ini_set('memory_limit', '-1');
			$nbelement = 0 ;
			
			$cities = PHDB::find(City::COLLECTION, array("dep" => null ));
			if(!empty($cities)){
				foreach (@$cities as $keyElt => $city) {
					$zone=PHDB::findOne( Zone::COLLECTION, array( "name"=>  $city["depName"]));

					if( !empty($zone)){
						$res = PHDB::update( City::COLLECTION, 
									  	array("_id"=>new MongoId($keyElt)),
										array('$set' => array("dep" => (String)$zone["_id"],
															"depName" => $zone["name"]))
						);

						$str .=  "Good: ".$city["name"]." " . $city["depName"] . "<br>" ;
							$nbelement++;
					}else{
						$str .=  "Error: ".$city["name"]." " . $city["depName"]. "<br>" ;
					}
					
					
				}
			}
			$str .=  "NB Element mis à jours: " .$nbelement."<br>" ;
		//}
		return $str;
	}

	public function actionAddIdRegion(){
		$str = "";
		//if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
			ini_set('memory_limit', '-1');
			$nbelement = 0 ;
			
			$cities = PHDB::find(City::COLLECTION, array("region" => null ));
			if(!empty($cities)){
				foreach (@$cities as $keyElt => $city) {
					$zone=PHDB::findOne( Zone::COLLECTION, array( "name"=>  $city["depName"]));

					if( !empty($zone)){
						$res = PHDB::update( City::COLLECTION, 
									  	array("_id"=>new MongoId($keyElt)),
										array('$set' => array("region" => (String)$zone["_id"],
															"regionName" => $zone["name"]))
						);

						$str .=  "Good: ".$city["name"]." " . $city["regionName"] . "<br>" ;
							$nbelement++;
					}else{
						$str .=  "Error: ".$city["name"]." " . $city["regionName"]. "<br>" ;
					}
					
					
				}
			}
			$str .=  "NB Element mis à jours: " .$nbelement."<br>" ;
		//}
		return $str;
	}

	public function actionReunion(){
		$str = "";
		//if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
			ini_set('memory_limit', '-1');
			$nbelement = 0 ;
			
			$cities = PHDB::find(City::COLLECTION, array("depName" => "REUNION" ));
			if(!empty($cities)){
				foreach (@$cities as $keyElt => $city) {
					
						$res = PHDB::update( City::COLLECTION, 
									  	array("_id"=>new MongoId($keyElt)),
										array('$set' => array(	"region" => "58be4af494ef47df1d0ddbcc",
																"regionName" => "Réunion",
																"dep" => "58be4af494ef47df1d0ddbcc",
																"depName" => "Réunion"))
						);

						$str .=  "Good: ".$city["name"]." " . $city["regionName"] . "<br>" ;
							$nbelement++;
					
					
					
				}
			}
			$str .=  "NB Element mis à jours: " .$nbelement."<br>" ;
		//}
		return $str;
	}

	public function actionBatchCities2(){
		$str = "";
		//if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
			ini_set('memory_limit', '-1');
			$nbelement = 0 ;
			
			$cities = PHDB::find(City::COLLECTION, array("key" => array('$exists' => 1) ));

			if(!empty($cities)){
				foreach (@$cities as $keyElt => $city) {
						$set = array();
						$newInfos = City::detailKeysLevels($city["key"]);
						
						if(!empty($newInfos)){
							$set["level1"] = $newInfos["level1"];
							$zone = Zone::getById($newInfos["level1"],array("name"));
							$set["level1Name"] = $zone["name"];
						}

						$unset = array( "key" => "",
										"depName" => "",
										"regionName" => "",
										"regionNameBel" => "",
										"dep" => "",
										"region" => "",
										"regionBel" => "",
						);

						$res = PHDB::update( City::COLLECTION, 
									  	array("_id"=>new MongoId($keyElt)),
										array(	'$unset' => $unset,
	                    						'$set' => $set)
						);

						$str .=  "Good: ".$city["name"]." " . $keyElt . "<br>" ;
							$nbelement++;			
				}
			}
			$str .=  "NB Element mis à jours: " .$nbelement."<br>" ;
		//}
		return $str;
	}

	public function actionBatchTranslate(){
		$str = "";
		ini_set('memory_limit', '-1');
		$nbelement = 0 ;
		
		$cities = PHDB::find(Zone::TRANSLATE, array("parentKey" => array('$exists' => 1) ));
		if(!empty($cities)){
			foreach (@$cities as $keyElt => $city) {
					$unset = array( "parentKey" => "");
					$res = PHDB::update( Zone::TRANSLATE, 
								  	array("_id"=>new MongoId($keyElt)),
									array('$unset' => $unset)
					);
					$str .=  "Good: ".$keyElt."<br>" ;
						$nbelement++;
			}
		}
		$str .=  "NB Element mis à jours: " .$nbelement."<br>" ;
		return $str;
	}

	public function actionBatchInterCountry() {
		ini_set('memory_limit', '-1');
		$where = array(	"level" => "1");
		$zones = PHDB::find(Zone::COLLECTION, $where);
		$nbelement = 0 ;
		foreach ($zones as $key => $zone) {
			$city = PHDB::findOne(City::COLLECTION, array("country" => $zone["countryCode"]));

			if(!empty($city)){
				$res = PHDB::update( Zone::COLLECTION, 
								  	array("_id"=>new MongoId($key)),
									array('$set' => array("hasCity" =>  true))
					);
				$nbelement++;
			}			
		}
		return  "NB Element mis à jours: " .$nbelement."<br>" ;
	}

	public function actionBatchOwnToHas() {
		ini_set('memory_limit', '-1');
		$where = array(	"ownACity" => true);
		$zones = PHDB::find(Zone::COLLECTION, $where);
		$nbelement = 0 ;
		foreach ($zones as $key => $zone) {
			$city = PHDB::findOne(City::COLLECTION, array("country" => $zone["countryCode"]));

			if(!empty($city)){
				$res = PHDB::update( Zone::COLLECTION, 
								  	array("_id"=>new MongoId($key)),
									array(	'$unset' 	=> array(	"ownACity" => null),
	                						'$set' 		=> array(	"hasCity" => true) ) );

				$nbelement++;
			}			
		}
		return  "NB Element mis à jours: " .$nbelement."<br>" ;
	}


	public function actionBatchZoneUnsetKey(){
		ini_set('memory_limit', '-1');
		$nbelement = 0 ;
		
		$cities = PHDB::find(Zone::COLLECTION, array("key" => array('$exists' => 1) ));
		if(!empty($cities)){
			foreach (@$cities as $keyElt => $city) {
					$unset = array( "key" => "");
					$res = PHDB::update( Zone::COLLECTION, 
								  	array("_id"=>new MongoId($keyElt)),
									array('$unset' => $unset)
					);
					//$str .=  "Good: ".$keyElt."<br>" ;
						$nbelement++;
			}
		}
		return  "NB Element mis à jours: " .$nbelement."<br>" ;
	}

	public function actionBatchTranslateTypeZone(){
		ini_set('memory_limit', '-1');
		$nbelement = 0 ;
		
		$zones = PHDB::find(Zone::COLLECTION, array("translateId" => array('$exists' => 1) ));
		if(!empty($zones)){
			foreach (@$zones as $keyElt => $zone) {
					$set = array( "parentType" => Zone::COLLECTION);
					$res = PHDB::update( Zone::TRANSLATE, 
								  	array("_id"=>new MongoId($zone["translateId"])),
									array('$set' => $set)
					);
					//$str .=  "Good: ".$keyElt."<br>" ;
						$nbelement++;
			}
		}
		return  "NB Element mis à jours: " .$nbelement."<br>" ;
	}


	public function actionBatchTranslateMissing(){
		$str = "";
		ini_set('memory_limit', '-1');
		$nbelement = 0 ;
		
		$cities = PHDB::find(City::COLLECTION, array("translateId" => array('$exists' => 0) ));
		if(!empty($cities)){
			foreach (@$cities as $keyElt => $city) {
				Zone::insertTranslate(	( String ) $city["_id"], City::COLLECTION, $city["country"], $city["name"], 
										(!empty($city["osmID"]) ? $city["osmID"] : null ), 
										(!empty($city["wikidataID"]) ? $city["wikidataID"] : null ) ) ;
				$nbelement++;
			}	
		}
		$str .=  "NB City mis à jours: " .$nbelement."<br>" ;

		$nbelement = 0 ;
		
		$zones = PHDB::find(Zone::COLLECTION, array("translateId" => array('$exists' => 0) ));
		if(!empty($zones)){
			foreach (@$zones as $keyElt => $zone) {
				Zone::insertTranslate(	( String ) $zone["_id"], Zone::COLLECTION, $zone["countryCode"], $zone["name"], 
										(!empty($zone["osmID"]) ? $zone["osmID"] : null ), 
										(!empty($zone["wikidataID"]) ? $zone["wikidataID"] : null ) ) ;
				$nbelement++;
			}
		}
		$str .=  "NB Zones mis à jours: " .$nbelement."<br>" ;
		return $str;
	}

	public function actionBatchInterElementCorrection() {
		//if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
			$nbelement = 0 ;
			$types = array(Person::COLLECTION , Organization::COLLECTION, Project::COLLECTION, 
							Event::COLLECTION, Poi::COLLECTION);

			foreach ($types as $keyType => $type) {
				$elts = PHDB::find($type, array('$and' => array(
									array("address" => array('$exists' => 1)),
									array("address.localityId" => array('$exists' => 1)),
									array("address.level1" => array('$exists' => 0))
						)));

				foreach ($elts as $key => $elt) {
					$newAddress = $elt["address"];
					unset($newAddress["localityId"]);
					$res = PHDB::update($type, 
							array("_id"=>new MongoId($key)),
							array('$set' => array("address" => $newAddress))
					);
					$nbelement++;
				}
			}
			return  "NB Element mis à jours: " .$nbelement."<br>" ;
			
		//}
	}

	public function actionBatchInterElement() {
		//if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
			$nbelement = 0 ;
			$types = array(Person::COLLECTION , Organization::COLLECTION, Project::COLLECTION, 
							Event::COLLECTION, Poi::COLLECTION);

			foreach ($types as $keyType => $type) {
				$elts = PHDB::find($type, array('$and' => array(
									array("address" => array('$exists' => 1)),
									array("address.localityId" => array('$exists' => 0))
						)));

				foreach ($elts as $key => $elt) {
					if(!empty($elt["address"]["codeInsee"]) || !empty($elt["address"]["postalCode"])){

						if( !empty($elt["address"]["codeInsee"]) )
							$city = PHDB::findOne(City::COLLECTION, array("insee" => $elt["address"]["codeInsee"]));
						else
							$city = PHDB::findOne(City::COLLECTION, array("postalCodes.postalCode" => $elt["address"]["postalCode"]));
						//var_dump($city);
						if(!empty($city)){
							$newAddress = $elt["address"];

							if(empty($elt["address"]["codeInsee"])){
								$newAddress["codeInsee"] = $city["insee"];
								$newAddress["addressCountry"] = $city["country"];
								$newAddress["streetAddress"] = "";
								if(!empty($city["postalCodes"]))
									foreach ($city["postalCodes"] as $keycp => $valuecp) {
										if($valuecp["postalCode"] == $elt["address"]["postalCode"])
											$newAddress["addressLocality"] = $valuecp["name"];
									}

								if(!empty($newAddress["addressLocality"]))
									$newAddress["addressLocality"] = $city["name"];
							}

							if(!empty($city["level1"])){
								$newAddress["level1"] = $city["level1"];
								$newAddress["level1Name"] = $city["level1Name"];
							}

							if(!empty($city["level2"])){
								$newAddress["level2"] = $city["level2"];
								$newAddress["level2Name"] = $city["level2Name"];
							}

							if(!empty($city["level3"])){
								$newAddress["level3"] = $city["level3"];
								$newAddress["level3Name"] = @$city["level3Name"];
							}

							if(!empty($city["level4"])){
								$newAddress["level4"] = $city["level4"];
								$newAddress["level4Name"] = @$city["level4Name"];
							}

							$newAddress["localityId"] = (String)$city["_id"];
							unset($newAddress["key"]);
							unset($newAddress["depName"]);
							unset($newAddress["regionName"]);
							unset($newAddress["regionNameBel"]);

							$set = array("address" => $newAddress);

							if(!empty($elt["addresses"])){
								$newAdd = array();
								foreach ($elt["addresses"] as $keyAddresses => $address) {

									if(!empty($address["address"]["codeInsee"])){
										$cityAdd = PHDB::findOne(City::COLLECTION, array("insee" => $address["address"]["codeInsee"]));
										if(!empty($city)){
											if(!empty($cityAdd["level1"])){
												$address["address"]["level1"] = $cityAdd["level1"];
												$address["address"]["level1Name"] = $cityAdd["level1Name"];
											}

											if(!empty($cityAdd["level2"])){
												$address["address"]["level2"] = $cityAdd["level2"];
												$address["address"]["level2Name"] = $cityAdd["level2Name"];
											}

											if(!empty($cityAdd["level3"])){
												$address["address"]["level3"] = $cityAdd["level3"];
												$address["address"]["level3Name"] = $cityAdd["level3Name"];
											}

											if(!empty($cityAdd["level4"])){
												$address["address"]["level4"] = $cityAdd["level4"];
												$address["address"]["level4Name"] = $cityAdd["level4Name"];
											}
											$address["localityId"] = (String)$cityAdd["_id"];
											unset($address["key"]);
											unset($address["depName"]);
											unset($address["regionName"]);
											unset($address["regionNameBel"]);
											$newAdd[] = $address;
										}else{
											$str .=  "Error addresses: ".$elt["name"]." " . $type. " " . $key. "<br>" ;
										}

									}else{
										$str .=  "Error Insee: ".$elt["name"]." " . $type. " " . $key. "<br>" ;
									}
									
								}
								if(!empty($newAdd))
									$set["addresses"] = $newAdd;
							}
							if(!empty($newAddress['localityId'])){
								$res = PHDB::update($type, 
										array("_id"=>new MongoId($key)),
										array('$set' => $set)
								);
							}
							$nbelement++;
						}
					}else{
						$res = PHDB::update($type, 
									array("_id"=>new MongoId($key)),
									array('$unset' => array("address" => ""))
							);
						$nbelement++;
					}					
				}
			}
			return  "NB Element mis à jours: " .$nbelement."<br>" ;
			
		//}
	}


	public function actionBatchInterMultiScope() {

		$elts = PHDB::find(Person::COLLECTION, array('$and' => array(
									array("multiscopes" => array('$exists' => 1)))
						));
		$nbelement = 0 ;
		if(!empty($elts)){
			foreach ($elts as $key => $value) {
				$res = PHDB::update(Person::COLLECTION, 
						array("_id"=>new MongoId($key)),
						array(	'$unset' 	=> array(	"multiscopes" => null),
	                			'$set' 		=> array(	"inter" => true) ) );
				$nbelement++;
			}
		}
		
		return  "NB Element mis à jours: " .$nbelement."<br>" ;			
					
	}


	public function actionBatchInterNews() {
		$str = "";
		ini_set('memory_limit', '-1');
		//$news = PHDB::find(News::COLLECTION, array("scope" => array('$exists' => 1)));

		$news = PHDB::find(News::COLLECTION, array('$and' => array(
									array("scope" => array('$exists' => 1)),
									array("scope.localities" => array('$exists' => 0)))
						));


		$nbelement = 0 ;
		foreach ($news as $key => $new) {
			if(!empty($new["scope"])){
				
				$listKey = array();

				if(!empty($new["scope"]["cities"])){

					foreach ($new["scope"]["cities"] as $keyS => $valueS) {
						$newsKey = array();

						if(!empty($valueS["codeInsee"])){
							$city = PHDB::findOne(City::COLLECTION, array("insee" => $valueS["codeInsee"]));
							if(!empty($city)){
								$newsKey["parentId"] = (String) $city["_id"] ;
								$newsKey["parentType"] = City::COLLECTION ;

								if(!empty($valueS["postalCode"])){
									$newsKey["postalCode"] = $valueS["postalCode"];
								}else{
									$newsKey["name"] = $valueS["addressLocality"];
								}

								if(!empty($valueS["geo"]))
									$newsKey["geo"] = $valueS["geo"];

								$newsKey = array_merge($newsKey, Zone::getLevelIdById((String) $city["_id"], $city, City::COLLECTION) ) ;
							}
						}

						if(!empty($newsKey))
							$listKey[] = $newsKey;
					}
				}

				if(!empty($new["scope"]["departements"])){

					foreach ($new["scope"]["departements"] as $keyS => $valueS) {

						$renameDep = array(	"REUNION" => "Réunion",
											"Phillippeville" => "Philippeville" );

						$nameD = (!empty($renameDep[$valueS["name"]])? $renameDep[$valueS["name"]] : $valueS["name"] ) ;

						$zone = PHDB::findOne(Zone::COLLECTION, array("name" => $nameD));
						$newsKey = array();
						if(!empty($zone)){
							$newsKey["name"] = $zone["name"];
							$newsKey["parentId"] = (String) $zone["_id"] ;
							$newsKey["parentType"] = Zone::COLLECTION ;
							if(!empty($zone["geo"]))
									$newsKey["geo"] = $zone["geo"];
							$newsKey = array_merge($newsKey, Zone::getLevelIdById((String) $zone["_id"], $zone, Zone::COLLECTION) ) ;
							$listKey[] = $newsKey;
						}
					}
				}

				if(!empty($new["scope"]["regions"])){

					foreach ($new["scope"]["regions"] as $keyS => $valueS) {

						$renameRegion = array(	"Alsace-Champagne-Ardenne-Lorraine" => "Grand Est",
												'Nord-Pas-de-Calais-Picardie' => "Hauts-de-France",
												"Aquitaine-Limousin-Poitou-Charentes" => "Nouvelle-Aquitaine",
												"Languedoc-Roussillon-Midi-Pyrénées" => "Occitanie",
												"La Réunion" => "Réunion" );

						$nameR = (!empty($renameRegion[$valueS["name"]])? $renameRegion[$valueS["name"]] : $valueS["name"] ) ;

						$zone = PHDB::findOne(Zone::COLLECTION, array("name" => $nameR));
						$newsKey = array();
						if(!empty($zone)){
							$newsKey["parentId"] = (String) $zone["_id"] ;
							$newsKey["parentType"] = Zone::COLLECTION ;
							$newsKey["name"] = $zone["name"];
							if(!empty($zone["geo"]))
									$newsKey["geo"] = $zone["geo"];
							$newsKey = array_merge($newsKey, Zone::getLevelIdById((String) $zone["_id"], $zone, Zone::COLLECTION) ) ;
							$listKey[] = $newsKey;
						}
					}
				}
				$newscope = array("type" => $new["scope"]["type"], "localities" => $listKey);
				$set = array("scope" => $newscope);
				$res = PHDB::update(News::COLLECTION, 
						array("_id"=>new MongoId($key)),
						array('$set' => $set)
				);
				$nbelement++;
			}else{
				$str .=  "Error: ". $key. "<br>" ;
			}
		}

		$str .=  "NB Element mis à jours: " .$nbelement."<br>" ;
		return $str;
	}



	public function actionBatchInterInit() {
		$str = "";
		ini_set('memory_limit', '-1');
		//if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
			$nbelement = 0 ;
			$types = array(Person::COLLECTION , Organization::COLLECTION, Project::COLLECTION, Event::COLLECTION, Poi::COLLECTION);

			foreach ($types as $keyType => $type) {
				$elts = PHDB::find($type, array('$and' => array(
												array("address" => array('$exists' => 1)),
												array("address.addressCountry" => 
														array('$nin' => array("PM", "MQ", "YT", "GP", "GF", "RE", "FR", "NC", "BE") ) )
						) ) );

				foreach ($elts as $key => $elt) {
					if(!empty($elt["address"]["codeInsee"])){
						
							$unset = array("address" => "");
							if(!empty($elt["addresses"]))
								$unset["addresses"] = "";
							if(!empty($elt["multiscopes"]))
								$unset["multiscopes"] = "";	
							
							// $res = PHDB::update($type, 
							// 		array("_id"=>new MongoId($key)),
							// 		array('$unset' => $unset)
							// );
							$nbelement++;
					}else{
						$str .=  "Error: ".$elt["name"]." " . $type. " " . $key. "<br>" ;
					}
					
				}
			}

			$str .=  "NB Element mis à jours: " .$nbelement."<br>" ;

			$zones = PHDB::find(Zone::COLLECTION, array("countryCode" => 
															array('$nin' => 
																	array("PM", "MQ", "YT", "GP", "GF", "RE", "FR", "NC", "BE"))));
			$nbelement = 0 ;
			$nbelementNew = 0 ;
			foreach ($zones as $key => $value) {
				$news = PHDB::find(News::COLLECTION, array('$and' => array(
													array("scope.localities" => array('$exists' => 1)),
													array("scope.localities.parentId" => $key))));


				foreach ($news as $keyNew => $valueNew) {
					// $res = PHDB::update(News::COLLECTION, 
					// 				array("_id"=>new MongoId($keyNew)),
					// 				array('$set' => array("scope.localities" => array() ) )

					// 		);
					$nbelementNew++;

				}
				$str .=  "Good: zone " . $key. "<br>" ;
				//PHDB::remove(Zone::COLLECTION, array("_id"=>new MongoId($key)));
				$nbelement++;
			}

			$str .=  "NB Zone mis à jours: " .$nbelement."<br>" ;
			$cities = PHDB::find(City::COLLECTION, array("country" => array('$nin' => array("PM", "MQ", "YT", "GP", "GF", "RE", "FR", "NC", "BE"))));

			$nbelement = 0 ;
			foreach ($cities as $key => $value) {
				$news = PHDB::find(News::COLLECTION, array('$and' => array(
													array("scope.localities" => array('$exists' => 1)),
													array("scope.localities.parentId" => $key))));


				foreach ($news as $keyNew => $valueNew) {
					// $res = PHDB::update(News::COLLECTION, 
					// 				array("_id"=>new MongoId($keyNew)),
					// 				array('$set' => array("scope.localities" => array()))
					// 		);
					$nbelementNew++;
				}
				$str .=  "Good: cities " . $key. "<br>" ;
				//PHDB::remove(City::COLLECTION, array("_id"=>new MongoId($key)));
				$nbelement++;
			}

			$str .=  "NB City mis à jours: " .$nbelement."<br>" ;
			
			$str .=  "NB New mis à jours: " .$nbelementNew."<br>" ;
		//}
		return $str;
	}




	public function actionLevel234(){
		$str = "";
		//if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
			ini_set('memory_limit', '-1');
			$nbelement = 0 ;
			
			$cities = PHDB::find(City::COLLECTION, array('$or' => array(
														array('level2' => array('$exists' => 0)),
														array('level3' => array('$exists' => 0)),
														array('level4' => array('$exists' => 0))
												)));
			//$cities = PHDB::find(City::COLLECTION, array('level3' => array('exists' => 0)));
			//var_dump($cities);
			if(!empty($cities)){
				foreach (@$cities as $keyElt => $city) {
					$set = array();
					if(empty($city["level2"]) && !empty($city["regionBel"])){
						$set["level2"] = $city["regionBel"];
						if(!empty($city["regionNameBel"]))
							$set["level2Name"] = $city["regionNameBel"];
					}

					if(empty($city["level3"]) && !empty($city["region"])){
						$set["level3"] = $city["region"];

						if(!empty($city["regionName"]))
							$set["level3Name"] = $city["regionName"];
					}

					if(empty($city["level4"]) && !empty($city["dep"])){
						$set["level4"] = $city["dep"];
						if(!empty($city["depName"]))
							$set["level4Name"] = $city["depName"];
					}

					if(!empty($set)){
						$res = PHDB::update( City::COLLECTION, 
									  	array("_id"=>new MongoId($keyElt)),
										array('$set' => $set)
						);

						$str .=  "Good: ".$city["name"]." <br>" ;
						$nbelement++;
					}
				}
			}else{
				$str .=  "Error" ;

			}
			$str .=  "NB Element mis à jours: " .$nbelement."<br>" ;
			return $str;
		//}
	}


	public function actionNamePays(){
		$str = "";
		ini_set('memory_limit', '-1');
		$nbelement = 0 ;
		
		$zones = PHDB::find(City::COLLECTION, array('$and' => array(
													array('translateId' => array('$exists' => 0)),
													array('$or' => array(
														array('osmID' => array('$exists' => 1)),
														array('wikidataID' => array('$exists' => 1)))))));
		// $zones = PHDB::find(Zone::COLLECTION, array('$and' => array(
		// 											array('translateId' => array('$exists' => 0)),
		// 											array('$or' => array(
		// 												array('osmID' => array('$exists' => 1)),
		// 												array('wikidataID' => array('$exists' => 1)))))));
		
		if(!empty($zones)){

			foreach ($zones as $key => $zone) {
				
				$translate = array();
				$info = array();
				if(!empty($zone["osmID"])){
					$zoneNominatim =  json_decode(file_get_contents("http://nominatim.openstreetmap.org/lookup?format=json&namedetails=1&osm_ids=R".$zone["osmID"]), true);
				
					if(!empty($zoneNominatim) && !empty($zoneNominatim[0]["namedetails"])){
						
						
						foreach ($zoneNominatim[0]["namedetails"] as $keyName => $valueName) {
							$arrayName = explode(":", $keyName);
							if(!empty($arrayName[1]) && $arrayName[0] == "name" && strlen($arrayName[1]) == 2){
								$translate[strtoupper($arrayName[1])] = $valueName;
							}
						}
					}
				}

				if(!empty($zone["wikidataID"])){

					$zoneWiki =  json_decode(file_get_contents("https://www.wikidata.org/wiki/Special:EntityData/".$zone["wikidataID"].".json"), true);
					
					if(!empty($zoneWiki) && !empty($zoneWiki["entities"][$zone["wikidataID"]]["labels"])){
						foreach ($zoneWiki["entities"][$zone["wikidataID"]]["labels"] as $keyName => $valueName) {
							
							if(strlen($keyName) == 2){
								$translate[strtoupper($keyName)] = $valueName["value"];
							}
						}
					}
				}

				if(!empty($translate)){
					$info["countryCode"] = $zone["country"];
					//$info["countryCode"] = $zone["countryCode"];
					$info["parentId"] = $key;
					$info["parentType"] = City::COLLECTION;
					//$info["parentType"] = Zone::COLLECTION;
					$info["parentKey"] = $zone["key"];
					$info["translates"] = $translate;
					Yii::app()->mongodb->selectCollection("translates")->insert( $info);
					PHDB::update(City::COLLECTION, 
								array("_id"=>new MongoId($key)),
								array('$set' => array("translateId" => (string)$info["_id"]))
					);

					$nbelement++;
				}else {
					$str .=  "Error ".$zone["name"]." ".$key." <br/>" ;
				}
			}

			$str .=  "NB Element mis à jours: " .$nbelement."<br>" ;
		}
		return $str;
	}

	public function actionRemoveTrans(){
		ini_set('memory_limit', '-1');
		$nbelement = 0 ;
		
		//$zones = PHDB::find(City::COLLECTION, array('translateId' => array('$exists' => 0)));
		$zones = PHDB::find(Zone::COLLECTION, array('translateId' => array('$exists' => 1)));
		
		if(!empty($zones)){

			foreach ($zones as $key => $zone) {
				
					PHDB::update(Zone::COLLECTION, 
								array("_id"=>new MongoId($key)),
								array('$unset' => array("translateId" => ""))
					);

					$nbelement++;
				
			}

			return  "NB Element mis à jours: " .$nbelement."<br>" ;
		}
	}


	public function actionRefactorTranslate(){
		ini_set('memory_limit', '-1');
		$nbelement = 0 ;
		
		$translates = PHDB::find(Zone::TRANSLATE, array('origin' => array('$exists' => 0)));
		
		if(!empty($translates)){

			foreach ($translates as $key => $translate) {

				$countries = array("MQ", "YT", "GP", "GF", "RE", "NC");
				
				if( in_array($translate["countryCode"], $countries))
					$origin = $translate["translates"]["FR"] ;
				else if (!empty($translate["translates"][$translate["countryCode"]]))
					$origin = $translate["translates"][$translate["countryCode"]] ;
				else if (!empty($translate["translates"]["EN"]))
					$origin = $translate["translates"]["EN"] ;
				else if (!empty($translate["translates"]["FR"]))
					$origin = $translate["translates"]["FR"] ;

				if(!empty($origin)){
					$newsT = array();
					foreach ($translate["translates"] as $keyT => $valueT) {
						if($valueT != $origin)
							$newsT[$keyT] = $valueT;
					}

					PHDB::update(Zone::TRANSLATE, 
								array("_id"=>new MongoId($key)),
								array('$set' => array(	"origin" => $origin,
														"translates" => $newsT))
					);

					$nbelement++;
				}
				
				//break;
			}

			return  "NB Element mis à jours: " .$nbelement."<br>" ;
		}
	}


	public function actionRefactorCountries(){
		$str = "";
		ini_set('memory_limit', '-1');
		$nbelementUpdate = 0 ;
		$nbelementCreate= 0 ;
		$nbelementError = 0 ;
		$countries = json_decode(file_get_contents("../../modules/co2/data/countries.json", FILE_USE_INCLUDE_PATH), true );
		//var_dump($countries);
		foreach ($countries as $key => $value) {
			//var_dump(json_encode($value));
			//$str .= json_encode($value);

			$zone = PHDB::findOne(Zone::COLLECTION, array('$and' => array(
														array('countryCode' => $value["cca2"]),
														array('level' => "1") ) ) );

			if(!empty($zone)){
				// $set = array();
				// if(!empty($value["cca3"]))
				// 	$set["cca3"] = $value["cca3"];
				// if(!empty($value["callingCode"]))
				// 	$set["callingCode"] = $value["callingCode"];

				// if(!empty($set)){
				// 	PHDB::update(Zone::TRANSLATE, 
				// 					array("_id"=>new MongoId((String) $zone["_id"])),
				// 					array('$set' => $set)
				// 		);
				// 	$nbelementUpdate++;
				// }

			} else {
				$str .=  "todo: " .$value["cca2"]." : ".$value["name"]["common"]."<br>" ;
				// $level1 = Zone::createLevel($value["name"]["common"], $value["cca2"], "1");

				// if(!empty($level1)){
				// 	if(!empty($value["cca3"]))
				// 	$level1["cca3"] = $value["cca3"];
				// 	if(!empty($value["callingCode"]))
				// 		$level1["callingCode"] = $value["callingCode"];
				// 	//$str .= json_encode($level1 );
				// 	$savelevel1 = Zone::save($level1);
		  //   		if($savelevel1["result"] == true)
		  //   			$nbelementCreate++;
		  //   		else{
		  //   			$nbelementError++;
		  //   			$str .=  "Error1: " .$value["cca2"]."<br>" ;
		  //   		}
				// }else{
				// 	$nbelementError++;
		  //    			$str .=  "Error2: " .$value["cca2"]."<br>" ;
				// }
				
			}

			//break;
		}

		$str .=  "NB Element mis à jours: " .$nbelementUpdate."<br>" ;
		$str .=  "NB Element created: " .$nbelementCreate."<br>" ;
		$str .=  "NB Element error: " .$nbelementError."<br>" ;
		return $str;
	}

	// -------------------- Fin des foncction pour le refactor Cities/zones
		// -------------------- Slugify everything
	public function actionSlugifyCitoyens(){
		$str = "";
		ini_set('memory_limit', '-1');
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
			$slugExist=array();
			$unwanted_array = array(    'Š'=>'S', 'š'=>'s', 'Ž'=>'Z', 'ž'=>'z', 'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A', 'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E',
                            'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I', 'Ï'=>'I', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O', 'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U',
                            'Ú'=>'U', 'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss', 'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a', 'å'=>'a', 'æ'=>'a', 'ç'=>'c',
                            'è'=>'e', 'é'=>'e', 'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i', 'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o', 'ô'=>'o', 'õ'=>'o',
                            'ö'=>'o', 'ø'=>'o', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'þ'=>'b', 'ÿ'=>'y' );
			//foreach($typeEl as $type){
				
				$res=PHDB::find(Person::COLLECTION);
				$str .= "//////////".count($res)." Citoyens/////////////////<br>";
				$count=0;
				$del=0;
				foreach ($res as $key => $value) {
					if((@$value["username"] && !empty($value["username"])) || (@$value["name"] && !empty($value["name"]))){
						// replace non letter or digits by -
						if(@$value["username"]){
							$string=$value["username"];
							$createUsername=false;
						}else{
							$string=$value["name"];
							$createUsername=true;
						}
						$str="";
						$value=explode(" ",$string);
						$i=0;
						foreach($value as $v){
							$text = strtr( $v, $unwanted_array );
							//$text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
			  				$text = preg_replace('~[^\\pL\d]+~u', '', $text);

				  			// trim
				  			$text = trim($text, '-');

				 			// transliterate
				  			$text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

				  			// lowercase
				  			$text = strtolower($text);
				  			if($i>0)
				  				$text = ucfirst($text);

				  			// remove unwanted characters
				  			$text = preg_replace('~[^-\w]+~', '', $text);
				  			$str.=$text;
				  			$i++;
			  			}	
			  			if(in_array($str, $slugExist)){
			 				$v = 1; // $i est un nombre que l'on incrémentera. 
			 				$inc=true;
							while($inc==true) 
							{ 
							  	$inc=in_array($str.$v, $slugExist);
							  	//$str .= $inc;
								if(!$inc)
									$str=$str.$v;
								else
							  		$v ++ ;
							}
						}
						if(@$createUsername && $createUsername==true){
							$str .= "doooooooit entry username////";
							PHDB::update(
							Person::COLLECTION,
							array("_id"=>new MongoId($key)),
							array('$set'=>array("username"=>$str)));
						}
						array_push($slugExist, $str);
						$str .=  $str."<br>";
						//INSERT IN SLUG COLLECTION
						$insertArrayRef = array("name"=>$str,"id"=>$key,"type"=>Person::COLLECTION);
						Yii::app()->mongodb->selectCollection(Slug::COLLECTION)->insert($insertArrayRef);
						//INSERT SLUG ENTRY IN ELEMENT
						PHDB::update(
							Person::COLLECTION,
							array("_id"=>new MongoId($key)),
							array('$set'=>array("slug"=>$str)));
						$count++;
					} else {
						PHDB::remove(
							Person::COLLECTION,
							array("_id"=>new MongoId($key)));
						$del++;
					}
		 		}
		 		$str .= "/////////////".$count." citoyens traités (comme des sauvages)//////////<br>";
		 		$str .= "/////////////".$del." citoyens zigouillés, lapidés, déchiquetés, oubliés, mis au bucher //////////<br>";
		}else 
			$str .= "Bois du rebBull t'auras des ailles";
		return $str;
	}
	public function actionSlugifyElement(){
		$str = "";
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
			ini_set('memory_limit', '-1');
			ini_set('max_execution_time', 300);
			$slugcitoyens=PHDB::find(Slug::COLLECTION);
			$typeEl=array("organizations","projects","events");
			$slugExist=array();
			foreach($slugcitoyens as $data){
				array_push($slugExist,$data["name"]);
			}
			$unwanted_array = array(    'Š'=>'S', 'š'=>'s', 'Ž'=>'Z', 'ž'=>'z', 'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A', 'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E',
                            'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I', 'Ï'=>'I', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O', 'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U',
                            'Ú'=>'U', 'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss', 'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a', 'å'=>'a', 'æ'=>'a', 'ç'=>'c',
                            'è'=>'e', 'é'=>'e', 'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i', 'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o', 'ô'=>'o', 'õ'=>'o',
                            'ö'=>'o', 'ø'=>'o', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'þ'=>'b', 'ÿ'=>'y' );
			foreach($typeEl as $type){
				
				$res=PHDB::find($type);
				$str .= "//////////".count($res)." ".$type."/////////////////<br>";
				$count=0;
				foreach ($res as $key => $value) {
					if(@$value["name"] && !empty($value["name"])){
						// replace non letter or digits by -
						$str="";
						if(strlen($value["name"])>50)
							substr($value["name"],50);
						
						$value=explode(" ",$value["name"]);
						$i=0;
						foreach($value as $v){
							$text = strtr( $v, $unwanted_array );
							//$text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
			  				$text = preg_replace('~[^\\pL\d]+~u', '', $text);

				  			// trim
				  			$text = trim($text, '-');

				 			// transliterate
				  			$text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

				  			// lowercase
				  			$text = strtolower($text);
				  			if($i>0)
				  				$text = ucfirst($text);

				  			// remove unwanted characters
				  			$text = preg_replace('~[^-\w]+~', '', $text);
				  			$str.=$text;
				  			$i++;
			  			}	
			  			if(in_array($str, $slugExist)){
			 			//if(!Slug::check(array("slug"=>$str,"type"=>Organization::COLLECTION,"id"=>$key))){
			 				$v = 1; // $i est un nombre que l'on incrémentera. 
			 				$inc=true;
			 				//$str .= "ouuuuuuuuuiiii";
							while($inc==true) 
							{ 
								//$inc=Slug::check(array("slug"=>$str.$i,"type"=>Organization::COLLECTION,"id"=>$key));
							  	//$str .= $i . "<br />";
							  	$inc=in_array($str.$v, $slugExist);
							  	//$str .= $inc;
							  	$str .= "ca bloque la ".$str.$v;
								if(!$inc)
									$str=$str.$v;
								else
							  		$v ++ ;
							}
						}
						array_push($slugExist, $str);
						$str .=  $key."////".$type."/////".$str."<br>";
						//INSERT IN SLUG COLLECTION
						$insertArrayRef = array("name"=>$str,"id"=>$key,"type"=>$type);
						Yii::app()->mongodb->selectCollection(Slug::COLLECTION)->insert($insertArrayRef);
						//INSERT SLUG ENTRY IN ELEMENT
						PHDB::update(
							$type,
							array("_id"=>new MongoId($key)),
							array('$set'=>array("slug"=>$str)));
						$count++;
					}
		 		}
		 		$str .= "////////////////".$count." ".$type." traités (comme des animaux) ///////";
			}
		}else 
			$str .= "Tout le monde t'as vu !! reste bien tranquille";
		return $str;
	}
	public function actionInsertFoldersElement(){
		$str = "";
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
			ini_set('memory_limit', '-1');
			ini_set('max_execution_time', 300);
			//$slugcitoyens=PHDB::find(Slug::COLLECTION);
			$typeEl=array("organizations","projects","events","citoyens");
			//$slugExist=array();
			//foreach($slugcitoyens as $data){
			//	array_push($slugExist,$data["name"]);
			//}
			foreach($typeEl as $type){
				
				$res=PHDB::find($type,array("documents"=>array('$exists'=>true)));
				$str .= "//////////".count($res)." ".$type."/////////////////<br>";
				$count=0;
				foreach ($res as $key => $value) {
					
						// replace non letter or digits by -
					var_dump($value["documents"]);	
					$folders=[];
					if(@$value["documents"]["image"]){
						$folders=array_merge($folders,self::prepareFolder($type, $key, $value["documents"]["image"], "image"));	
					}
					if(@$value["documents"]["file"]){
						$folders=array_merge($folders,self::prepareFolder($type, $key, $value["documents"]["file"], "file"));	
					}
					var_dump($folders);
					$count++;
					
					
		 		}
		 		$str .= "////////////////".$count." ".$type." traités (comme des animaux) ///////";
			}
		}else 
			$str .= "Tout le monde t'as vu !! reste bien tranquille";
		return $str;
	}
	public function actionCreateFoldersPathAndDocumentPath(){
		$str = "";
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
			ini_set('memory_limit', '-1');
			ini_set('max_execution_time', 300);	
			$res=PHDB::find(Folder::COLLECTION);
			$str .= "//////////".count($res)." folders vont être épiés traités envoyer à l'abatoire/////////////////<br>";
			$count=0;
			foreach ($res as $key => $value) {
				$docs=PHDB::find(Document::COLLECTION, 
					array("id"=>$value["contextId"], "type"=>$value["contextType"], "doctype"=>$value["docType"], "collection"=>$value["name"]));
				$folderPath=Folder::getFolderPath($value);
				self::createfolder($folderPath);
				$str .= "=>>>>>>> folder path : ".$folderPath."<br/><hr>";
				foreach($docs as $id => $v){
					$pathDocument=Document::getDocumentPath($v);
					$newPath=$folderPath."/".$v["name"];
					$str .= $id." :: ".$pathDocument."</br>";
					$str .= "Move file to :: ".$newPath."</br>";
					if(file_exists ($pathDocument))
						rename($pathDocument, $newPath);
					$count++;
				}
				//var_dump($folders);
				
	 		}
	 		$str .= "////////////////".$count." documents sous de la source de la kilienne ///////";
			
		}else 
			$str .= "Tout le monde t'as vu !! reste bien tranquille";
		return $str;
	}
	// PACTE BASH COLLECTIF LOCAUX
	public function actionUpdateGroupAndDeleteDouble(){
		$str = "";
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
			ini_set('memory_limit', '-1');
			ini_set('max_execution_time', 300);	
			$group=PHDB::find(Organization::COLLECTION, 
					array("source.key"=>"siteDuPactePourLaTransition"));
			$countGeo=0;
			$countDelete=0;
			$countBgCity=0;
			$scopesChecked=[];
			foreach($group as $id => $v){
				if(@$v["scope"]){
					foreach($v["scope"] as $k => $ref){
						$keyScope=$ref["city"];
						$checkIfExist=$k;
					}
					$city = City::getById($keyScope, array("postalCodes", "geo", "geoPosition"));
					/*$str .= "------- update geo position :".$v["name"]." <br/>";
					var_dump($city["geo"]);$str .= "<br/>";var_dump($city["geoPosition"]);
					PHDB::update(
							Organization::COLLECTION,
							array("_id"=>new MongoId($id)),
							array('$set'=>array("geo"=>$city["geo"],"geoPosition"=>$city["geoPosition"])));
					$str .= "<br/>---------------";
					$countGeo++;*/
					if(count($city["postalCodes"])>1){
						/*$checkIfExist=$keyScope."cities".$city["postalCodes"][0]["postalCode"];
						$name=ucfirst(strtolower($city["postalCodes"][0]["name"]));
						$groupName="Collectif local ".$name;
						$checkIfGroupExist=PHDB::findOne(Organization::COLLECTION, 
							array("source.key"=>"siteDuPactePourLaTransition", "scope.".$checkIfExist => array('$exists' => true )));
						if(!empty($checkIfGroupExist) && $id!=(string)$checkIfGroupExist["_id"]){
							$str .= "group exists: delete group".$v["name"]."<br/>";
							$countDelete++;
							PHDB::remove(Organization::COLLECTION,array("_id" => new MongoId($id)));
						}else{
							$str .= "update group: with good locality ".$v["name"]." to ".$groupName."<br/>";
							var_dump($v["scope"]);$str .= "<br/>";
							foreach($v["scope"] as $k => $data){
								$scope2formate=$data;
							}
							$newScope=array($checkIfExist=> $scope2formate);
							$newScope[$checkIfExist]["cityName"]=$city["postalCodes"][0]["name"];
							$newScope[$checkIfExist]["postalCode"]=$city["postalCodes"][0]["postalCode"];
							$set=array("scope"=>$newScope, "name"=>$groupName, "email"=>"pacte-".mb_strtolower($city["postalCodes"][0]["postalCode"])."@listes.transition-citoyenne.org");
							PHDB::update(
								Organization::COLLECTION,
								array("_id"=>new MongoId($id)),
								array('$set'=>$set));
							$countBgCity++;
							var_dump($set);$str .= "<br/>";
						}*/
					}else{
						if(!in_array($checkIfExist, $scopesChecked)){
							$checkIfGroupExist=PHDB::find(Organization::COLLECTION, 
								array("source.key"=>"siteDuPactePourLaTransition", "scope.".$checkIfExist => array('$exists' => true )));
							if(!empty($checkIfGroupExist)){
								array_push($scopesChecked, $checkIfExist);
							 	foreach($checkIfGroupExist as $kk => $c){
							 		if($id!=$kk){
										$str .= "group exists: delete group".$c["name"]."<br/>";
										PHDB::remove(Organization::COLLECTION,array("_id" => new MongoId($kk)));
							 		}
								}
							}	
						}
						//$countDelete++;
							//PHDB::remove(Organization::COLLECTION,array("_id" => new MongoId($id)));
					}
					
				}else{
					$str .= "delete :".$v["name"]."<br/>";
					PHDB::remove(Organization::COLLECTION,array("_id" => new MongoId($id)));
					
				}
	 		}
	 		$str .= "////////////////".$countGeo." groupe avec ajout des points de geo position ///////";
	 		$str .= "////////////////".$countBgCity." groupes ayant des doublons sur les grosse villes à multi cp ///////";
	 		$str .= "////////////////".$countDelete." groupes étant en doublon ///////";
			
		}else 
			$str .= "Tout le monde t'as vu !! reste bien tranquille";
		return $str;
	}
	public function actionCreateThumbPath(){
		$str = "";
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
			ini_set('memory_limit', '-1');
			ini_set('max_execution_time', 300);	
			$docs=PHDB::find(Document::COLLECTION, 
					array("doctype"=>"image", "folderId"=>array('$exists'=>true)));
			//$str .= "=>>>>>>> folder path : ".$folderPath."<br/><hr>";
			$count=0;
			foreach($docs as $id => $v){
				$folderPath=Folder::getFolderPath(Folder::getById($v["folderId"]))."/thumb";
				self::createfolder($folderPath);
				
   				$pathDocument=Yii::app()->params['uploadDir'].$v["moduleId"]."/".$v["folder"]."/thumb/".$v["name"];
				$newPath=$folderPath."/".$v["name"];
				$str .= $id." :: ".$pathDocument."</br>";
				$str .= "Move file to :: ".$newPath."</br>";
				if(file_exists ($pathDocument))
					rename($pathDocument, $newPath);
				$count++;
				//var_dump($folders);
				
	 		}
	 		$str .= "////////////////".$count." documents sous de la source de la kilienne ///////";
			
		}else 
			$str .= "Tout le monde t'as vu !! reste bien tranquille";
		return $str;
	}
	public function actionUpdateDocsAndDeleteCollection(){
		$str = "";
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
			ini_set('memory_limit', '-1');
			ini_set('max_execution_time', 300);	
			$res=PHDB::find(Folder::COLLECTION);
			$str .= "//////////".count($res)." folders ont été épiés traités envoyer à l'abatoire/////////////////<br>";
			$count=0;
			foreach ($res as $key => $value) {
				$docs=PHDB::find(Document::COLLECTION, 
					array("id"=>$value["contextId"], "type"=>$value["contextType"], "doctype"=>$value["docType"], "collection"=>$value["name"]));
				foreach($docs as $id => $v){
					$str .= $id." :: ".$key."</br>";
					$set=array('$set'=>array("folderId"=>$key), '$unset'=>array("collection"=>true));
					PHDB::update(
							Document::COLLECTION,
							array("_id"=>new MongoId($id)),
							$set);
					$count++;
				}
				
	 		}
	 		$str .= "////////////////".$count." documents updaté avec le folderId ///////";
	 		$typeEl=array("organizations","projects","events","citoyens");
	 		$count=0;
			foreach($typeEl as $type){
				
				$el=PHDB::find($type,array("documents"=>array('$exists'=>true)));
				//$str .= "//////////".count($res)." ".$type."/////////////////<br>";
				foreach ($el as $key => $value) {

					PHDB::update(
							$type,
							array("_id"=>new MongoId($key)),
							array('$unset'=>array("documents"=>true)));	
					$count++;
					
					
		 		}
			}
	 		$str .= "////////////////".$count." elements ou l'on a supprimer l'arbre documentations ///////";
			
		}else 
			$str .= "Tout le monde t'as vu !! reste bien tranquille";
		return $str;
	}
	public function actionStatsPacteEmails(){
		$str = "";
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
			$listPeopleId=array();
			$countTotalVotes=0;
			$countTotalAmendements=0;
			$props=PHDB::find('proposals', array("source.key"=>"pactePourLaTransition"));
			foreach($props as $k => $v){
        		if(!empty($v["votes"]) ){
            		foreach($v["votes"] as $k => $ids){
                		if(@$v["votes"][$k] && !empty($v["votes"][$k])){
                    		$countTotalVotes=($countTotalVotes+count($v["votes"][$k]));
                    		foreach($ids as $id){
	                        	if(!in_array($id, $listPeopleId))
	                          		array_push($listPeopleId, $id); 
	                    	}
                		}
             		} 
        		}

        		if(!empty($v["amendements"]) ){
        			$countTotalAmendements=($countTotalAmendements+count($v["amendements"])); 
        		}
     		}
  			$str .= "<span class='pull-left'>nombre de votes :".$countTotalVotes."</span><br/>----------------<br/>";
  			$str .= "nombre de personnes votantes:".count($listPeopleId)."<br/>------------------<br/>";
  			$str .= "nombre d'amendements :".$countTotalAmendements."<br/>------------------<br/>";
    		$listPeopleEmail=[];
  			foreach($listPeopleId as $k => $id){
       			$citoyen=PHDB::findOne('citoyens',array("_id"=>new MongoId($id)));
       			if(@$citoyen["email"]){
                	$str .= $citoyen["email"].",<br/>"; 
            	} 
        	}
		}else 
			$str .= "Tout le monde t'as vu !! reste bien tranquille";
		return $str;
	}
	public static function createfolder($folderPath){
		$str = "";
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
			$str .= "/////////////create folder//////////////////////////<br/>";

			$upload_dir = Yii::app()->params['uploadDir']."communecter/";
			$folderPath=str_replace ( $upload_dir , "" , $folderPath ); 
			$folderPathExp=explode("/", $folderPath);
			foreach($folderPathExp as $folder){
				$upload_dir .= $folder.'/';
	            $str .= $upload_dir."<br/>";
	            if( !file_exists ( $upload_dir ) )
	               mkdir ( $upload_dir,0775 );
			 
	        }
	    }else{
	    	$str .= "Bah alors champion ? douché q'tin reconno";
	    }
	    return $str;
	}
	public static function actionChangeFilesCTESurveyDatas(){
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
			$el=PHDB::find(Document::COLLECTION,array("keySurvey"=>array('$exists'=>true)));
				//$str .= "//////////".count($res)." ".$type."/////////////////<br>";
				$count=0;
				foreach ($el as $key => $value) {
					$arraySetted=array('$unset'=>array("keySurvey"=>""), '$set'=>array("surveyId"=>'cte'));
					////print_r($value);
					PHDB::update(
							Document::COLLECTION,
							array("_id"=>new MongoId($key)),
							$arraySetted);
					$count++;
					
					
		 		}
		 	return $count." documents qui sont passés à la casserole";
	    }else{
	    	return "Bah alors champion ? douché q'tin reconno";
	    }
	}

	public static function prepareFolder($type, $id, $value, $docType, $parentId=null){
		$foldarray=[];
		foreach($value as $key => $subs){
			if($key != "updated"){
				$newFolder=array("name"=>$key, "docType" => $docType, "contextId"=>$id, "contextType"=> $type, "created"=> @$subs["updated"], "updated"=>@$subs["updated"]);
				$newFolder["count"]=PHDB::count(Document::COLLECTION, array("type"=>$type, "id"=>$id, "collection"=>$key));
				if(!empty($parentId))
					$newFolder["parentId"]=$parentId;
				Yii::app()->mongodb->selectCollection(Folder::COLLECTION)->insert(  
                    $newFolder);
				array_push($foldarray, $newFolder);
				if(count($subs)>1)
					$foldarray=array_merge($foldarray,self::prepareFolder($type, $id, $subs, $docType, (string)$newFolder["_id"]));	
			}
		}
		return $foldarray;

	}
	public function actionParentObjectInsteadOfString(){
		$str = "";
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
			ini_set('memory_limit', '-1');
			ini_set('max_execution_time', 300);
			//$slugcitoyens=PHDB::find(Slug::COLLECTION);
			$typeEl=array("organizations","events","projects", "classifieds", "poi");
			foreach($typeEl as $type){
				$where=array("parentId"=>array('$exists'=>true));
				if($type==Event::COLLECTION)
					$where=array('$or'=>array(
							$where,
							array("organizerId"=>array('$exists'=>true))
						));
				$res=PHDB::find($type,$where);
				$str .= "//////////".count($res)." ".$type." avec parent/////////////////<br><br>";
				$count=0;
				foreach($res as $key => $value){
					$set=array();
					$unset=array();
					if(@$value["parentId"] && !@$value["parent"]){
						if(!@$value["parentType"] && $type==Event::COLLECTION) 
							$parentType= Event::COLLECTION; 
						else
							$parentType=$value["parentType"];
						$set["parent"]=array($value["parentId"] => array("type"=>$parentType, "name"=>$value["name"]));

					}
					if(@$value["parentId"]){
						$unset["parentId"]=true;
						$unset["parentType"]=true;
					}
					if(@$value["organizerId"] && $value["organizerId"]!="dontKnow" && !@$value["organizer"] && @$value["organizerType"]){
						$set["organizer"]=array($value["organizerId"] => array("type"=>$value["organizerType"]));
					}
					if(@$value["organizerId"]){
						$unset["organizerId"]=true;
						$unset["organizerType"]=true;
					}
					$str .= "// ".$count." - id : ".$key."<br/>";
					/*//print_r($set);
					$str .= "<br/>";
					//print_r($unset);*/
					
					if(!empty($set) || !empty($unset)){
						$update=array();
						if(!empty($set)){
							$update['$set']=$set;
						}

						if(!empty($unset)){
							$update['$unset']=$unset;
						}
						//print_r($update);
						$str .= "<br/>";
						$str .= "<br/>";
						$str .= "<br/>";
						PHDB::update(
								$type,
								array("_id"=>new MongoId($key)),
								$update);
						$count++;
					}
				}
				$str .= "////////////////".$count." ".$type." ont été traités (comme des sauvages alors que nous sommes des sauvages) ///////<br><br>";
			}
		}else 
			$str .= "Tout le monde t'as vu !! reste bien tranquille";
		return $str;
	}
	public function actionReverseParentToDDA(){
		$str = "";
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
			ini_set('memory_limit', '-1');
			ini_set('max_execution_time', 300);
			//$slugcitoyens=PHDB::find(Slug::COLLECTION);
			$typeEl=array(Resolution::COLLECTION, Proposal::COLLECTION, Room::COLLECTION, Room::COLLECTION_ACTIONS);
			foreach($typeEl as $type){
				$where=array("parent"=>array('$exists'=>true));
				$res=PHDB::find($type,$where);
				$str .= "//////////".count($res)." ".$type." avec parent/////////////////<br><br>";
				$count=0;
				foreach($res as $key => $value){
					$set=array();
					$unset=array();
					
					if(isset($value["parent"])){
						$unset["parent"]=true;
						foreach($value["parent"] as $k => $v){
							$set=array("parentId"=>$k, "parentType"=>$v["type"]);
						}
					
					}
					$str .= "// ".$count." - id : ".$key."<br/>";
					/*//print_r($set);
					$str .= "<br/>";
					//print_r($unset);*/
					
					if(!empty($set) || !empty($unset)){
						$update=array();
						if(!empty($set)){
							$update['$set']=$set;
						}

						if(!empty($unset)){
							$update['$unset']=$unset;
						}
						//print_r($update);
						$str .= "<br/>";
						$str .= "<br/>";
						$str .= "<br/>";
						PHDB::update(
								$type,
								array("_id"=>new MongoId($key)),
								$update);
						$count++;
					}
				}
				$str .= "////////////////".$count." ".$type." ont été traités (comme des sauvages alors que nous sommes des sauvages) ///////<br><br>";
			}
		}else 
			$str .= "Tout le monde t'as vu !! reste bien tranquille";
		return $str;
	}
	public function actionUpdateSlugifyElement(){
		$str = "";
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
			ini_set('memory_limit', '-1');
			ini_set('max_execution_time', 300);
			//$slugcitoyens=PHDB::find(Slug::COLLECTION);
			$typeEl=array("organizations","projects","events","citoyens");
			//$slugExist=array();
			//foreach($slugcitoyens as $data){
			//	array_push($slugExist,$data["name"]);
			//}
			$unwanted_array = array(    'Š'=>'S', 'š'=>'s', 'Ž'=>'Z', 'ž'=>'z', 'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A', 'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E',
                            'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I', 'Ï'=>'I', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O', 'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U',
                            'Ú'=>'U', 'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss', 'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a', 'å'=>'a', 'æ'=>'a', 'ç'=>'c',
                            'è'=>'e', 'é'=>'e', 'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i', 'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o', 'ô'=>'o', 'õ'=>'o',
                            'ö'=>'o', 'ø'=>'o', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'þ'=>'b', 'ÿ'=>'y' );
			foreach($typeEl as $type){
				
				$res=PHDB::find($type,array("slug"=>array('$exists'=>false)));
				$str .= "//////////".count($res)." ".$type."/////////////////<br>";
				$count=0;
				foreach ($res as $key => $value) {
					if(@$value["name"] && !empty($value["name"])){
						// replace non letter or digits by -
						$str="";
						if(strlen($value["name"])>50)
							substr($value["name"],50);
						
						$value=explode(" ",$value["name"]);
						$i=0;
						foreach($value as $v){
							$text = strtr( $v, $unwanted_array );
							//$text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
			  				$text = preg_replace('~[^\\pL\d]+~u', '', $text);

				  			// trim
				  			$text = trim($text, '-');

				 			// transliterate
				  			$text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

				  			// lowercase
				  			$text = strtolower($text);
				  			if($i>0)
				  				$text = ucfirst($text);

				  			// remove unwanted characters
				  			$text = preg_replace('~[^-\w]+~', '', $text);
				  			$str.=$text;
				  			$i++;
			  			}	
			  			if(in_array($str, $slugExist)){
			 			//if(!Slug::check(array("slug"=>$str,"type"=>Organization::COLLECTION,"id"=>$key))){
			 				$v = 1; // $i est un nombre que l'on incrémentera. 
			 				$inc=true;
			 				//$str .= "ouuuuuuuuuiiii";
							while($inc==true) 
							{ 
								//$inc=Slug::check(array("slug"=>$str.$i,"type"=>Organization::COLLECTION,"id"=>$key));
							  	//$str .= $i . "<br />";
							  	$inc=in_array($str.$v, $slugExist);
							  	//$str .= $inc;
							  	$str .= "ca bloque la ".$str.$v;
								if(!$inc)
									$str=$str.$v;
								else
							  		$v ++ ;
							}
						}
						array_push($slugExist, $str);
						$str .=  $key."////".$type."/////".$str."<br>";
						//INSERT IN SLUG COLLECTION
						$insertArrayRef = array("name"=>$str,"id"=>$key,"type"=>$type);
						Yii::app()->mongodb->selectCollection(Slug::COLLECTION)->insert($insertArrayRef);
						//INSERT SLUG ENTRY IN ELEMENT
						PHDB::update(
							$type,
							array("_id"=>new MongoId($key)),
							array('$set'=>array("slug"=>$str)));
						$count++;
					}
		 		}
		 		$str .= "////////////////".$count." ".$type." traités (comme des animaux) ///////";
			}
		}else 
			$str .= "Tout le monde t'as vu !! reste bien tranquille";
		return $str;
	}
	public function actionRelaunchInvitation(){
		$str = "";
		ini_set('memory_limit', '-1');
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){		
			$res=PHDB::find(Person::COLLECTION,array("pending"=>array('$exists'=>true)), array("name", "language", "invitedBy", "email"));
			$i=0;
			$v=0;
			$languageUser = Yii::app()->language;
			foreach($res as $key => $value){
				if(DataValidator::email($value["email"])=="" && !empty($value["language"])){

					$str .= $key." : ".$value["name"]." : ".$value["language"]." <br/> ";
					Yii::app()->language = $value["language"];
					Mail::relaunchInvitePerson($value);
					$i++;
				}else{
					$v++;
				}
			}
			$str .= $i." mails envoyé pour relancer l'inscription<br>";
			$str .= $v." utilisateur non inscrit (validé) qui ont un mail de marde<br>";
			Yii::app()->language = $languageUser ;
		}else 
			$str .= "Pas d'envoie pour toi ma cocote !! Tu vas aller au four plutot";
		return $str;
	}

	public function actionMailMaj(){
		ini_set('memory_limit', '-1');
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){		
			Mail::mailMaj();
		}else 
			$str .= "Pas d'envoie pour toi ma cocote !! Tu vas aller au four plutot";
		return $str;
	}


	public function actionRegionList(){
		$str = "";
		//if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
			ini_set('memory_limit', '-1');
			$nbelement = 0 ;
			$erreur = array();
			$region = array();
			$aggregate = array(
			    array(
			        '$group' => array(
			            "_id" => array(	"level3Name" => '$level3Name',
			        					"country" => '$country'),
			        ),
			    ),
			);

			$cities = PHDB::aggregate( City::COLLECTION, $aggregate);
			
			//var_dump($cities);
			if(!empty($cities["result"])){
				foreach (@$cities["result"] as $keyElt => $city) {
					if(!empty($city["_id"]["level3Name"]) && (!empty($city["_id"]["country"]) && $city["_id"]["country"] == "FR") )		
						$str .= '"'.$city["_id"]["level3Name"].'"<br/>';
				}
			}
		//}
		return $str;
	}
	public function actionActivityStreamDDA(){
		$str = "";
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
			ini_set('memory_limit', '-1');
			$nbNews = 0 ;
			$newsDDA = PHDB::find( News::COLLECTION, array("type"=>"activityStream", 
				'$or'=>array(
					array("object.type"=>"proposals"),
					array("object.type"=>"resolutions"),
					array("object.type"=>"actions"),
					array("object.type"=>"rooms")
				)));
			//var_dump($cities);
			if(!empty($newsDDA)){
				foreach (@$newsDDA as $key => $v) {
					//if(!empty($city["_id"]["level3Name"]) && (!empty($city["_id"]["country"]) && $city["_id"]["country"] == "FR") )		
					$str .= "<br/>------------------------<br/>".
						"object:".$v["object"]["type"]."<br/>".
						"target:".$v["target"]["type"]."<br/>".
						"share:<br>";
						//print_r($v["sharedBy"]);
					$nbNews++;
				}
			}
			$str .= "Nombre news traitées : ".$nbNews;
		}else
			$str .= "sorry for you, you are not an agent C007";
		return $str;
	}

	public function actionPublicEvent(){
		$str = "";
		ini_set('memory_limit', '-1');
		$nbelement = 0 ;
		$where = array("public" => array('$exists' => 0));
		$fields = array("name");

		$events = PHDB::find( Event::COLLECTION, $where, $fields);
		
		foreach ($events as $key => $value) {
			// if(!empty($value["name"]))
			// 	$str .= $key." : ".$value["name"]."<br/>";

			$res = PHDB::update(Event::COLLECTION, 
			  	array("_id"=>new MongoId($key)),
                array('$set' => array("public" => true ))
            );
            $nbelement++;
		}	

		$str .= $nbelement." elements mis a jours";
		return $str;
	}
	public function actionRemoveMarkerPathRessourcesClassified(){
		$str = "";
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
			ini_set('memory_limit', '-1');
			$nbRessources = 0 ;
			$nbClass=0;
			$ressources = PHDB::find( Ressource::COLLECTION);
			
			foreach ($ressources as $key => $value) {
				// if(!empty($value["name"]))
				// 	$str .= $key." : ".$value["name"]."<br/>";
				if (@$value["profilMarkerImageUrl"]){
					$res = PHDB::update(Ressource::COLLECTION, 
				  		array("_id"=>new MongoId($key)),
	                	array('$unset' => array("profilMarkerImageUrl" => "" ))
	            	);
	            	$nbRessources++;
	            }
			}	
			$class = PHDB::find( Classified::COLLECTION);
			
			foreach ($class as $key => $value) {
				// if(!empty($value["name"]))
				// 	$str .= $key." : ".$value["name"]."<br/>";
				if (@$value["profilMarkerImageUrl"]){
					$res = PHDB::update(Classified::COLLECTION, 
				  		array("_id"=>new MongoId($key)),
	                	array('$unset' => array("profilMarkerImageUrl" => "" ))
	            	);
	            	$nbClass++;
	            }
			}	
			$str .= $nbClass. " miss à pjour";
			$str .= $nbRessources." yepa";
		}else
			$str .= "salut toi !!!!!";
		return $str;
	}


	public function actionIrisUpdateInter(){
		$str = "";
		ini_set('memory_limit', '-1');
		$nbelement = 0 ;
		$nbelementtotal = 0 ;
		$where = array("level1" => array('$exists' => 0));
		$fields = array("name","insee");
		$iris = PHDB::find( "zones_iris", $where, $fields);
		
		foreach ($iris as $key => $value) {
			// if(!empty($value["name"]))
			 	$str .= $key." : ".$value["name"]."<br/>";

			 	if(!empty($value["insee"])){
			 		$city = PHDB::findOne( "cities", array("insee" => $value["insee"]) , array("_id","insee", "name", "level1", "level2", "level1Name", "level3Name", "level4Name", "level2Name", "level3", "level4"));

			 		$set =array("localityId" => (String) $city["_id"], 
			 					"level1" => $city["level1"], 
			 					"level2" => $city["level2"], 
			 					"level1Name" => $city["level1Name"], 
			 					"level3Name" => $city["level3Name"], 
			 					"level4Name" => $city["level4Name"], 
			 					"level2Name" => $city["level2Name"], 
			 					"level3" => $city["level3"], 
			 					"level4" => $city["level4"]);
				 	if(!empty("city")){
				 		$res = PHDB::update("zones_iris", 
						  	array("_id"=>new MongoId($key)),
			                array('$set' => $set)
			            );
				 		$nbelement++;
				 	}
			 	}
            $nbelementtotal++;
            break;
		}
		$str .= $nbelement." iris mis a jours / ".$nbelementtotal;
		return $str;
	}

	public function actionUpdateTranslatewithNAmeCP(){
		$str = "";
		ini_set('memory_limit', '-1');
		$nbelement = 0 ;
		$nbelementtotal = 0 ;
		$where = array("postalCodes" => array('$exists' => 1));
		$fields = array("name","insee", "postalCodes", "translateId");
		$cities = PHDB::find("cities", $where, $fields);
		
		foreach ($cities as $key => $value) {
			// if(!empty($value["name"]))
			 	//$str .= $value["translateId"]." : ".$value["name"]."<br/>";

			 	if(!empty($value["postalCodes"])){
			 		$cpTranslate = array();
			 		foreach ($value["postalCodes"] as $keyCP => $valueCP) {
			 			$cpTranslate[$keyCP]["origin"] = ucfirst(strtolower($valueCP["name"]));
			 			$cpTranslate[$keyCP]["postalCode"] = $valueCP["postalCode"];
			 		}
			 		$set =array("postalCodes" => $cpTranslate);
				 	//var_dump($set);
				 	//$str .= "<br/>";
			 		$res = PHDB::update(Zone::TRANSLATE, 
					  	array("_id"=>new MongoId($value["translateId"])),
		                array('$set' => $set)
		            );
			 		$nbelement++;
				 	
			 	}
            //break;
		}
		return $nbelement." trnalate mis a jours / ";
	}


	public function actionUpdateMultiScope(){
		ini_set('memory_limit', '-1');
		$nbelement = 0 ;
		$nbelementtotal = 0 ;
		$where = array("multiscopes" => array('$exists' => 1), "modifiedByBatch.updateMultiScope" => array('$exists' => 0));
		$fields = array("multiscopes", "name", "modifiedByBatch");
		$person = PHDB::find(Person::COLLECTION, $where, $fields);
		
		foreach ($person as $key => $value) {

		 	if(!empty($value["multiscopes"])){
		 		$newML = array();
		 		foreach ($value["multiscopes"] as $keyCP => $valueCP) {
		 			$newS = $valueCP;
		 			

		 			if($newS["type"] == "cp"){
		 				$city = PHDB::findOne(City::COLLECTION, array("postalCodes.postalCode" =>$newS["name"]), array("country"));

		 				if(!empty($city)){
		 					$newS["postalCode"] = $newS["name"];
		 					$newS["countryCode"] = $city["country"];
		 					unset($newS["cp"]);	
		 					$newML[$newS["postalCode"].$newS["countryCode"].$newS["type"]] = $newS;
		 				}
		 				
		 			}else if(!empty($newS["level"])){
		 				$zone = PHDB::findOneById(Zone::COLLECTION, $keyCP,array("countryCode") );

		 				if(!empty($zone)){
		 					$newS["id"] = $keyCP;
		 					$newS["countryCode"] = $zone["countryCode"];
		 					$newML[$newS["id"].$newS["type"]] = $newS;
		 				}
		 				
		 			}else{

		 				$city = PHDB::findOneById(City::COLLECTION, $keyCP,array("country") );

		 				if(!empty($city)){
		 					$newS["id"] = $keyCP;
		 					$newS["type"] = City::COLLECTION;
		 					$newS["countryCode"] = $city["country"];
		 					if(!empty($newS["cp"])){
		 						$newS["postalCode"] = $newS["cp"];
		 						unset($newS["cp"]);			
		 					}
		 					$newML[(String)$city["_id"].$newS["type"]] = $newS;
		 				}
		 			}
		 		}

		 		$set[] =$newML ;
		 		$value["modifiedByBatch"][] = array("updateMultiScope" => new MongoDate(time()));
		 		$set =array("multiscopes" => $newML,
		 					"modifiedByBatch" => $value["modifiedByBatch"]);
		 					 
		 		$res = PHDB::update(Person::COLLECTION, 
				  	array("_id"=>new MongoId($key)),
	                array('$set' => $set)
	            );

		 		$nbelement++;
		 	}
		}
		return $nbelement." multiscopes mis a jours / ";

		//$str .= json_encode($set);
	}


	public function actionUpdateZoneMissingNameLevel1(){
		$str = "";
		ini_set('memory_limit', '-1');
		$nbelement = 0 ;
		$nbelementtotal = 0 ;
		$where = array("level1" => array('$exists' => 1), "level1Name" => array('$exists' => 0));
		$fields = array("level1", "name", "level");
		$zones = PHDB::find(Zone::COLLECTION, $where, $fields);
		$names = array();
		foreach ($zones as $key => $value) {
			//$str .= $key."<br/>";
			if(!in_array("1", $value["level"])){
				$str .= $key." : ".$value["name"]."<br/>";
				if(empty($names[$value["level1"]])){
					$nameLevel1 = Zone::getById($value["level1"], array("name"));
					$names[(String)$nameLevel1["_id"]] = $nameLevel1["name"];
				}
				
				$set = array("level1Name" => $names[$value["level1"]]);

				$res = PHDB::update(Zone::COLLECTION, 
				  	array("_id"=>new MongoId($key)),
	                array('$set' => $set)
	            );

				$nbelement++;
			}
		}
		$str .= $nbelement." trnalate mis a jours / ";
		return $str;
	}

	public function actionUpdateZoneMissingNameLevel2(){
		$str = "";
		ini_set('memory_limit', '-1');
		$nbelement = 0 ;
		$nbelementtotal = 0 ;
		$where = array("level2" => array('$exists' => 1), "level2Name" => array('$exists' => 0));
		$fields = array("level2", "name", "level");
		$zones = PHDB::find(Zone::COLLECTION, $where, $fields);
		$names = array();
		foreach ($zones as $key => $value) {

			if(!in_array("1", $value["level"])){
				$str .= $key." : ".$value["name"]."<br/>";
				if(empty($names[$value["level2"]])){
					$nameLevel2 = Zone::getById($value["level2"], array("name"));
					$names[(String)$nameLevel2["_id"]] = $nameLevel2["name"];
				}
				
				$set = array("level2Name" => $names[$value["level2"]]);

				$res = PHDB::update(Zone::COLLECTION, 
				  	array("_id"=>new MongoId($key)),
	                array('$set' => $set)
	            );

				$nbelement++;
			}
		}
		$str .= $nbelement." trnalate mis a jours / ";
		return $str;
	}

	public function actionUpdateZoneMissingNameLevel3(){
		$str = "";
		ini_set('memory_limit', '-1');
		$nbelement = 0 ;
		$nbelementtotal = 0 ;
		$where = array("level3" => array('$exists' => 1), "level3Name" => array('$exists' => 0));
		$fields = array("level3", "name", "level");
		$zones = PHDB::find(Zone::COLLECTION, $where, $fields);
		$names = array();
		foreach ($zones as $key => $value) {

			if(!in_array("1", $value["level"])){
				$str .= $key." : ".$value["name"]."<br/>";
				if(empty($names[$value["level3"]])){
					$nameLevel3 = Zone::getById($value["level3"], array("name"));
					$names[(String)$nameLevel3["_id"]] = $nameLevel3["name"];
				}
				
				$set = array("level3Name" => $names[$value["level3"]]);

				$res = PHDB::update(Zone::COLLECTION, 
				  	array("_id"=>new MongoId($key)),
	                array('$set' => $set)
	            );

				$nbelement++;
			}
		}
		$str .= $nbelement." trnalate mis a jours / ";
		return $str;
	}

	public function actionBatchCorrectionLevel($id = null) {
		ini_set('memory_limit', '-1');
		//if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) ) && !empty($id) ){
			$nbelement = 0 ;
			$nbCity = 0;
			$nbN = 0;

			$zone = PHDB::findOneById( Zone::COLLECTION, $id);
			$where = array();
			$whereElt = array();

			if(in_array("4", $zone["level"])){
				$newLevel = "4";
				$where["level4"] = $id;
				$whereElt["address.level4"] = $id;
				$whereNews = array('$or' => array(
									array("scope.localities.level4" => $id),
									array('$and' => array(
										array("scope.localities.parentId" => $id),
										array("scope.localities.parentType" => Zone::COLLECTION),
									))
							));

				if(!empty($zone["level3"])){
					$set["level3"] = $zone["level3"];
					$set["level3Name"] = $zone["level3Name"];
					$setElt["address.level3"] = $zone["level3"];
					$setElt["address.level3Name"] = $zone["level3Name"];
				}
				
				if(!empty($zone["level2"])){
					$set["level2"] = $zone["level2"];
					$set["level2Name"] = $zone["level2Name"];
					$setElt["address.level2"] = $zone["level2"];
					$setElt["address.level2Name"] = $zone["level2Name"];
				}

				if(!empty($zone["level1"])){
					$set["level1"] = $zone["level1"];
					$set["level1Name"] = $zone["level1Name"];
					$setElt["address.level1"] = $zone["level1"];
					$setElt["address.level1Name"] = $zone["level1Name"];
				}

			}
			else if(in_array("3", $zone["level"])){
				$newLevel = "3";
				$where["level3"] = $id;
				$whereElt["address.level3"] = $id;
				$whereNews = array('$or' => array(
									array("scope.localities.level3" => $id),
									array('$and' => array(
										array("scope.localities.parentId" => $id),
										array("scope.localities.parentType" => Zone::COLLECTION),
									))
							));
				if(!empty($zone["level2"])){
					$set["level2"] = $zone["level2"];
					$set["level2Name"] = $zone["level2Name"];
					$setElt["address.level2"] = $zone["level2"];
					$setElt["address.level2Name"] = $zone["level2Name"];
				}
				
				if(!empty($zone["level1"])){
					$set["level1"] = $zone["level1"];
					$set["level1Name"] = $zone["level1Name"];
					$setElt["address.level1"] = $zone["level1"];
					$setElt["address.level1Name"] = $zone["level1Name"];
				}
			}
			else if(in_array("2", $zone["level"])){
				$newLevel = "2";
				$where["level2"] = $id;
				$whereElt["address.level2"] = $id;
				$whereNews = array('$or' => array(
									array("scope.localities.level2" => $id),
									array('$and' => array(
										array("scope.localities.parentId" => $id),
										array("scope.localities.parentType" => Zone::COLLECTION),
									))
							));
				
				if(!empty($zone["level1"])){
					$set["level1"] = $zone["level1"];
					$set["level1Name"] = $zone["level1Name"];
					$setElt["address.level1"] = $zone["level1"];
					$setElt["address.level1Name"] = $zone["level1Name"];
				}
			}

			if(!empty($zone) && !empty($where) && !empty($whereElt)){
				$cities = PHDB::find( City::COLLECTION, $where);

				if(!empty($cities )){
					foreach ($cities as $keyC => $city) {
						//$str .= $keyC." : ".$city["name"]."<br/>";
						$nbCity++;

						$res = PHDB::update(City::COLLECTION, 
											array("_id"=>new MongoId($keyC)),
											array('$set' => $set)
									);
					}
				}

				$types = array(Person::COLLECTION , Organization::COLLECTION, Project::COLLECTION, Event::COLLECTION, Poi::COLLECTION);

				foreach ($types as $keyType => $type) {
					$elts = PHDB::find($type, $whereElt );
					$str .="**************".$type."**************<br/>";
					if(!empty($elts)){

						foreach ($elts as $keyE => $elt) {
							$str .= $keyE." : ".$elt["name"]."<br/>";
							$nbelement++;
							$newAd = $elt["address"];
							if($newLevel == "4"){
								if(!empty($zone["level3"])){
									$newAd["level3"] = $zone["level3"];
									$newAd["level3Name"] = $zone["level3Name"];
								}
								if(!empty($zone["level2"])){
									$newAd["level2"] = $zone["level2"];
									$newAd["level2Name"] = $zone["level2Name"];
								}
								if(!empty($zone["level1"])){
									$newAd["level1"] = $zone["level1"];
									$newAd["level1Name"] = $zone["level1Name"];
								}
							}
							
							if($newLevel == "3"){
								if(!empty($zone["level2"])){
									$newAd["level2"] = $zone["level2"];
									$newAd["level2Name"] = $zone["level2Name"];
								}
								if(!empty($zone["level1"])){
									$newAd["level1"] = $zone["level1"];
									$newAd["level1Name"] = $zone["level1Name"];
								}
							}

							if($newLevel == "2"){
								if(!empty($zone["level1"])){
									$newAd["level1"] = $zone["level1"];
									$newAd["level1Name"] = $zone["level1Name"];
								}
							}
							$setElt = array("address" => $newAd);

							$res = PHDB::update($type, 
											array("_id"=>new MongoId($keyE)),
											array('$set' => $setElt)
									);
						}
					}
				}

				//$str .="**************NEWS**************<br/>";
				$news = PHDB::find( News::COLLECTION, $whereNews);

				if(!empty($news )){
					foreach ($news as $keyN => $new) {
						$str .= $keyN." : ".(!empty($new["text"]) ? $new["text"] : "")."<br/>";

						if(!empty($new["scope"]["localities"])){
							$loc = array();
							foreach ($new["scope"]["localities"] as $key => $value) {
								if(	( !empty($value["level".$newLevel]) && $value["level".$newLevel] == $id ) ||
									( !empty($value["parentId"]) && !empty($value["parentType"]) && 
										$value["parentId"] == $id && $value["parentType"] == Zone::COLLECTION )) {
									if($newLevel == "4"){
										if(!empty($zone["level3"]))
											$value["level3"] = $zone["level3"];
										if(!empty($zone["level2"]))
											$value["level2"] = $zone["level2"];
										if(!empty($zone["level1"]))
											$value["level1"] = $zone["level1"];
									}
									
									if($newLevel == "3"){
										if(!empty($zone["level2"]))
											$value["level2"] = $zone["level2"];
										if(!empty($zone["level1"]))
											$value["level1"] = $zone["level1"];
									}

									if($newLevel == "2"){
										if(!empty($zone["level1"]))
											$value["level1"] = $zone["level1"];
									}

									$loc[] = $value;
								}
							}
							$setNew["scope"] = $new["scope"] ;
							//var_dump($setNew);
							$setNew["scope"]["localities"] = $loc;
							
							$str .= "<br/>";
							$res = PHDB::update( News::COLLECTION, 
												array("_id"=>new MongoId($keyN)),
												array('$set' => $setNew));
							$nbN++;
						}
						
					}
				}
			}
			$str .=  "NB city mis à jours: " .$nbCity."<br>" ;
			$str .=  "NB Element mis à jours: " .$nbelement."<br>" ;
			$str .=  "NB new mis à jours: " .$nbN."<br>" ;
			return $str;
		// }else{
		// 	$str .= "here";
		// }
	}




	public function actionUpdateScopeNews(){
		ini_set('memory_limit', '-1');
		$nbelement = 0 ;
		$nbelementtotal = 0 ;
		$where = array("scope.localities" => array('$exists' => 1), "modifiedByBatch.UpdateScopeNews" => array('$exists' => 0));
		$fields = array("scope", "modifiedByBatch");
		$news = PHDB::find(News::COLLECTION, $where, $fields);
			
		foreach ($news as $key => $value) {
			//$str .= $key." : ".$value["name"]."<br/>";
			$set = array();
		 	if(!empty($value["scope"]["localities"]) ){
		 		$newML = array();
		 		foreach ($value["scope"]["localities"] as $keyscope => $scope) {
		 			$newS = $scope;
		 			//var_dump(count($value["scope"]["localities"]));
		 			//$str .=  City::COLLECTION." ".City::CONTROLLER." ";
		 			if( !empty($newS["parentType"]) && 
		 				($newS["parentType"] == City::COLLECTION || $newS["parentType"] == City::CONTROLLER) ) {
		 				$city = PHDB::findOneById(City::COLLECTION, $newS["parentId"] );
		 				if(!empty($city)){
		 					//$newS["postalCode"] = $newS["postalCode"];
		 					if(!empty($city["postalCodes"]) && !empty( $newS["postalCode"] )){
					 			if( count($city["postalCodes"]) == 1 ) {
					 				$newS["name"] = $city["name"];
					 			}else{
					 				foreach ($city["postalCodes"] as $keyCP => $valueCP) {
					 					if( $valueCP["postalCode"] == $newS["postalCode"] && empty($newS["name"] ) ) {
					 						$newS["name"] = $city["name"];
					 					}
					 				}
					 			}
					 		}

					 		if(empty($newS["name"] ))
					 			$newS["name"] = $city["name"];



		 					$newS["countryCode"] = $city["country"];
		 					$newS = array_merge($newS, Zone::getLevelIdById($newS["parentId"], $city, Zone::COLLECTION) ) ;
		 					$newML[] = $newS;
		 				}
		 				
		 			}else if( !empty($newS["parentType"]) && $newS["parentType"] == Zone::COLLECTION ) {
		 				$zone = PHDB::findOneById(Zone::COLLECTION, $newS["parentId"] );
		 				if(!empty($zone)){
		 					$newS["countryCode"] = $zone["countryCode"];
		 					$newS = array_merge($newS, Zone::getLevelIdById($newS["parentId"], $zone, Zone::COLLECTION) ) ;
		 					$newML[] = $newS;
		 				}
		 				
		 			}else{

		 				$where = array( "postalCodes.postalCode"=>strval(@$newS["postalCode"]));
						$city = City::getWhereFindOne($where, $fields=null);
						if(!empty($city)){
							$newS["countryCode"] = $city["country"];
							$newS = array_merge($newS, Zone::getLevelIdById((String) $city["_id"], $city, City::COLLECTION) ) ;
							$newML[] = $newS;
							
						}

		 			}

		 			
		 		}

		 		// if(!empty($newML))
		 		// 	$set[$key] =$newML ;
		 		$value["modifiedByBatch"][] = array("UpdateScopeNews" => new MongoDate(time()));

		 		$value["scope"]["localities"] = $newML;
		 		$set =array("scope" => $value["scope"],
		 					"modifiedByBatch" => $value["modifiedByBatch"]);
		 					 
		 		$res = PHDB::update(News::COLLECTION, 
				  	array("_id"=>new MongoId($key)),
	                array('$set' => $set)
	            );
		 		$nbelement++;
		 	}
		}
		return $nbelement." news mis a jours / ";

		//$str .= json_encode($set);
	}



	public function actionUpdateCPMisingImport(){
		$str = "";
		ini_set('memory_limit', '-1');
		$nbelement = 0 ;
		$nbelementtotal = 0 ;
		$where = array("source" => array('$exists' => 1), "address.postalCode" => array('$exists' => 0));
		$fields = array("name","address");
		$orgas = PHDB::find(Organization::COLLECTION, $where, $fields);
		
		foreach ($orgas as $key => $value) {
		 	if(!empty($value["address"]) && empty($value["address"]["postalCode"])){
		 		$city = PHDB::findOneById( City::COLLECTION, $value["address"]["localityId"], array("postalCodes") );
		 		
		 		$newAddress = $value["address"];
		 		if(!empty($city) && !empty($city["postalCodes"])){
		 			if( count($city["postalCodes"]) == 1 ) {
		 				$newAddress["postalCode"] = $city["postalCodes"][0]["postalCode"];
		 			}else{
		 				foreach ($city["postalCodes"] as $keyCP => $valueCP) {
		 					$name = strtoupper(str_replace(array("-","é"), array(" ","e"), $newAddress["addressLocality"]));
		 					if(strtoupper($valueCP["name"]) == $name){
		 						$str .=  $valueCP["postalCode"]." CP : <br/>";
		 						$newAddress["postalCode"] = $valueCP["postalCode"];
		 					}
		 				}
		 			}
		 		}
		 		$set =array("address" => $newAddress);
		 		$str .= $key." Name : ".$value["name"]." : ".(!empty($newAddress["postalCode"]) ? $newAddress["postalCode"] : "NULL")."<br/>";

		 		$res = PHDB::update(Organization::COLLECTION, 
				  	array("_id"=>new MongoId($key)),
	                array('$set' => $set)
	            );
	            $str .= $nbelement." multiscopes mis a jours / ";
		 		$nbelement++;
		 	}
		}
		return $str;
	}

	public function actionBatchLanguage(){
		$str = "";
		ini_set('memory_limit', '-1');
		$nbelement = 0 ;
		$nbelementError = 0 ;
		$where = array("language" => array('$exists' => 0));
		$fields = array("name", "address", "invitedBy");
		$persons = PHDB::find(Person::COLLECTION, $where, $fields);
		$fr = array("FR", "RE", "NC", "GP", "GF", "MQ", "BE", "YT", "CA","CH");
		$en = array("EN", "DE", "BG","FI","ES", "CG","IT");
		//$en = array("EN");
		foreach ($persons as $key => $value) {
			$set = array();
			
			if(!empty($value["address"]) && !empty($value["address"]["addressCountry"])){
				
				if(in_array($value["address"]["addressCountry"], $fr))
					$set["language"] = "fr";
				else if(in_array($value["address"]["addressCountry"], $en))
					$set["language"] = strtolower($value["address"]["addressCountry"]);
				else
					$set["language"] = "fr";

			}else if( !empty($value["invitedBy"]) ) {

				$set["language"] = 'fr';
				$personInvite = PHDB::findOneById(Person::COLLECTION, $value["invitedBy"], array("address") );
				if(!empty($personInvite["address"]) && !empty($personInvite["address"]["addressCountry"])){		
					if(in_array($personInvite["address"]["addressCountry"], $fr))
						$set["language"] = "fr";
					else if(in_array($personInvite["address"]["addressCountry"], $en))
						$set["language"] = strtolower($personInvite["address"]["addressCountry"]);
					else 
						$set["language"] = "fr";
				}
			}else {
				$set["language"] = 'fr';
			}


			if(!empty($set)){
				
				if(!empty($value["name"]))
					$str .= $key." Name : ".$value["name"]." : ".$set["language"]."<br/>";
				else
					$str .= $key." NOT : ".$set["language"]."<br/>";
				
				$res = PHDB::update(Person::COLLECTION, 
						array("_id"=>new MongoId($key)),
						array('$set' => $set)
				);
		 		$nbelement++;
			}else{
				$str .= $key." Name : ".$value["name"]." : ERROR <br/>";
				$nbelementError++;
			}	
		}
		$str .= $nbelement." language mis a jours / ";
		$str .= $nbelementError." language error / ";
		return $str;
	}


	public function actionBatchCountryMissing(){
		ini_set('memory_limit', '-1');
		$nbelement = 0 ;
		$nbelementError = 0 ;
		$where = array("address.addressCountry" => array('$exists' => 0), "address.addressCountry" => "");
		$fields = array("name", "address", "invitedBy");
		$persons = PHDB::find(Person::COLLECTION, $where, $fields);
		$set=array();
		foreach ($persons as $key => $value) {
			//$str .= $key." Name! : ".$value["name"]." ".$value["address"]["codeInsee"]." : <br/>";
			$set=array();
			if(!empty($value["address"]["addressLocality"]) && $value["address"]["addressLocality"] == "ST GILLES LES BAINS"){
				$city = PHDB::findOneById(City::COLLECTION, "54c0965cf6b95c141800a518");
				$add = $value["address"];
				//var_dump($city);
				if(!empty($city)){
					unset($add["regionName"]);
					unset($add["depName"]);

					$add["addressCountry"] = $city["country"];
					$add["level1"] = $city["level1"];
					$add["level1Name"] = $city["level1Name"];
					if(!empty($city["level2"])){
						$add["level2"] = $city["level2"];
						$add["level2Name"] = $city["level2Name"];
					}

					if(!empty($city["level3"])){
						$add["level3"] = $city["level3"];
						$add["level3Name"] = $city["level3Name"];
					}

					if(!empty($city["level4"])){
						$add["level4"] = $city["level4"];
						$add["level4Name"] = $city["level4Name"];
					}

					if(empty($add["localityId"])){
						$add["localityId"] = (String) $city["_id"];
					}
				}
				$set["address"] = $add;
			}else if(!empty($value["address"]["codeInsee"])){
				$city = PHDB::findOne(City::COLLECTION, array("insee" => $value["address"]["codeInsee"]));
				$add = $value["address"];
				//var_dump($city);
				if(!empty($city)){
					//$str .= $key." Name! : ".$value["name"]." ".$value["address"]["codeInsee"]." : <br/>";
					unset($add["regionName"]);
					unset($add["depName"]);

					$add["addressCountry"] = $city["country"];
					$add["level1"] = $city["level1"];
					$add["level1Name"] = $city["level1Name"];
					if(!empty($city["level2"])){
						$add["level2"] = $city["level2"];
						$add["level2Name"] = $city["level2Name"];
					}

					if(!empty($city["level3"])){
						$add["level3"] = $city["level3"];
						$add["level3Name"] = $city["level3Name"];
					}

					if(!empty($city["level4"])){
						$add["level4"] = $city["level4"];
						$add["level4Name"] = $city["level4Name"];
					}

					if(empty($add["localityId"])){
						$add["localityId"] = (String) $city["_id"];
					}
					

				}

				$set["address"] = $add;
			}

			if(!empty($set)){
				$str .= $key." Name : ".$value["name"]." <br/>";
				
				$res = PHDB::update(Person::COLLECTION, 
						array("_id"=>new MongoId($key)),
						array('$set' => $set)
				);
		 		$nbelement++;
			}else{
				$str .= $key." Name : ".$value["name"]." : ERROR <br/>";
				$nbelementError++;
			}
		}



		//$str .= json_encode($set);	
		$str .= $nbelement." multiscopes mis a jours / ";
		return $str;
	}


	public function actionOrganizationMissing(){
		$str = "";
		$orga = PHDB::find(Organization::COLLECTION, array(	"type" => array('$exists' => false) ) );
		$i = 0 ;
		$v = 0;
		foreach ($orga as $key => $value) {
			$str .= date("d / m / y", $value["created"])." ; ".$value["name"];

			if( !empty($value["source"]) ){
				$str .= " ; IMPORT";
				$i++;
			}else{
				$str .= " ; DYNFORM";
				$v++;
			}
			
			$str .= " ; ".Yii::app()->getRequest()->getBaseUrl(true)."/#page.type.organizations.id.".$key."<br/>";

			$res = PHDB::update(Organization::COLLECTION, 
					array("_id"=>new MongoId($key)),
					array('$set' => array("type" => "Group"))
			);
		}
		$str .= $i." importé <br/>";
		$str .= $v." dynform <br/>";
		return $str;
	}


	public function actionPoiGeoFormat(){
		$str = "";
		//{ $and : [ {"geo" : {$exists : 1} }, {"geo.@type" : {$ne : "GeoCoordinates"} } ] }
		$where = array('$and' => array(
					array("geo" => array('$exists' => 1)),
					array("geo.@type" => array('$ne' => "GeoCoordinates"))
				));
		$poi = PHDB::find(Poi::COLLECTION, $where);
		$i = 0 ;
		foreach ($poi as $key => $value) {
			
			if(!empty($value["geo"])){
				$geo = SIG::getFormatGeo($value["geo"]["coordinates"][1], $value["geo"]["coordinates"][0]);

				$res = PHDB::update(Poi::COLLECTION, 
						array("_id"=>new MongoId($key)),
						array('$set' => array("geo" => $geo))
				);
				$i++;
			}			
		}
		
		$str .= $i." poi updaté <br/>";
		return $str;
	}

	public function actionRemovePendingUser() {
		$str = "";
		$nbUser = 0;
		$users = PHDB::find(Person::COLLECTION, 
							array("pending" => true), 
							array("pending", "modifiedByBatch"));
		foreach ($users as $key => $person) {
			$person["modifiedByBatch"][] = array("RemovePendingUser" => new MongoDate(time()));
			$res = PHDB::update(Person::COLLECTION, 
										  	array("_id"=>new MongoId($key)),
					                        array(	'$set' => array( "modifiedByBatch" => $person["modifiedByBatch"]),
					                        		'$unset' => array("pending"=>"") )
					                    );

			if($res["ok"] == 1){
				$nbUser++;
			}else{
				$str .= "<br/> Error with user id : ".$key;
			}
		}

		$str .= "Number of user with preferences modified : ".$nbUser;
		return $str;
	}


	public function actionInviteDigitalReunion() {
		$nbUser = 0;
		$orga = PHDB::find(Organization::COLLECTION, 
							array("source.key" => "digitalreunion"));

		foreach ($orga as $key => $value){
			$child = array();
			$child[] = array( 	"childId" => $key,
								"childType" => Organization::COLLECTION,
								"childName" => $value["name"],
								"roles" => array() );
			//var_dump($child);
			$res["organizations"][] = Link::multiconnect($child, "577e4ad740bb4e9c6e10130d", Organization::COLLECTION);

			$res["update"][] = PHDB::update(Organization::COLLECTION, 
										  	array("_id"=>new MongoId($key)),
					                        array(	'$set' => array( "tags" => array("Digital Réunion"))));

		}

		//577e4ad740bb4e9c6e10130d

		return Rest::json($res); exit;
	}

	public function actionPendingMissing() {
		$str = "";
		$nbUser = 0;
		$person = PHDB::find(Person::COLLECTION, 
							array("pwd" => array('$exists' => 0), "pending" => array('$exists' => 0)));

		foreach ($person as $key => $value){
			$str .= $key." <br/> ";
			$nbUser++;
			$res["update"][] = PHDB::update(Person::COLLECTION, 
										  	array("_id"=>new MongoId($key)),
					                        array(	'$set' => array( "pending" => true)));

		}
		$str .= $nbUser;
		return $str;
		//577e4ad740bb4e9c6e10130d

		//Rest::json($res); exit;
	}

	public function actionRemoveLevel3Mising() {
		$str = "";
		$nbUser = 0;
		$city = PHDB::find(City::COLLECTION, 
							array("level3" => array('$exists' => 1), "level3Name" => array('$exists' => 0)));

		foreach ($city as $key => $value){
			$str .= $key." ".$value["name"]." <br/> ";
			$nbUser++;
			PHDB::update(City::COLLECTION, 
						  	array("_id"=>new MongoId($key)),
	                        array(	'$unset' => array( "level3"=> "")));

		}
		$str .= $nbUser;
		return $str;
	}


	public function actionUnifierAnswers() {
		$nbAnswer = 0;
		$answers = PHDB::find(	Form::ANSWER_COLLECTION, 
								array("modifiedByBatch.UnifierAnswers" => array('$exists' => 0)) );

		$unique = array();


		$deleted = array();
		foreach ($answers as $key => $answer) {

			if(empty($unique[$answer["user"]])){
				$unique[$answer["user"]] = array(
					"user" => $answer["user"],
					"name" => $answer["name"],
					"email" => @$answer["email"],
					"formId" => @$answer["parentSurvey"],
					"created" => time(),
					"session" => $answer["session"],
					"answers" => array()
				);
			}

			if( empty($unique[$answer["user"]]["created"]) && 
				!empty($answer["created"]) ){
				$unique[$answer["user"]]["created"] = $answer["created"];
			}

			if( empty($unique[$answer["user"]]["formId"]) && 
				!empty($answer["parentSurvey"]) )
				$unique[$answer["user"]]["formId"] = $answer["parentSurvey"];


			if( empty($unique[$answer["user"]]["email"]) && 
				!empty($answer["email"]) )
				$unique[$answer["user"]]["email"] = $answer["email"];


			if( !empty($answer["answers"]) && 
				!empty($answer["formId"]) && 
				in_array($answer["formId"], array("cte1", "cte2", "cte3") ) ) {
				$unique[$answer["user"]]["answers"][$answer["formId"]]["answers"] = $answer["answers"] ;
				$unique[$answer["user"]]["answers"][$answer["formId"]]["created"] = $answer["created"] ;
			}


			if( !empty($answer["step"]) && 
				!empty($answer["formId"]) && 
				$answer["formId"] ==  "cte") {

				$unique[$answer["user"]]["step"] = $answer["step"] ;
			}

			if( !empty($answer["categories"]) && 
				!empty($answer["formId"]) && 
				$answer["formId"] ==  "cte") {

				$unique[$answer["user"]]["categories"] = $answer["categories"] ;
			}

			if( !empty($answer["comment"]) && 
				!empty($answer["formId"]) && 
				$answer["formId"] ==  "cte") {

				$unique[$answer["user"]]["comment"] = $answer["comment"] ;
			}


			if( !empty($answer["risks"]) && 
				!empty($answer["formId"]) && 
				$answer["formId"] ==  "cte") {

				$unique[$answer["user"]]["risks"] = $answer["risks"] ;
			}

			if( !empty($answer["eligible"]) && 
				!empty($answer["formId"]) && 
				$answer["formId"] ==  "cte") {

				$unique[$answer["user"]]["eligible"] = $answer["eligible"] ;
			}

			$unique[$answer["user"]]["modifiedByBatch"][] = array("UnifierAnswers" => new MongoDate(time()));

			$deleted[] = new MongoId($key) ;
		}

		//Rest::json($unique);

		PHDB::remove( Form::ANSWER_COLLECTION , array( "_id" => array('$in' => $deleted) ) );

		foreach ($unique as $key => $value) {
			Yii::app()->mongodb->selectCollection( Form::ANSWER_COLLECTION)->insert( $value );
		}

		return "Good" ;
	}

	public function actionLinkParentForm() {
		$nbUser = 0;
		$form = Form::getById("cte");
		$orga=PHDB::findOne($form ["parentType"],array("_id"=>new MongoId($form ["parentId"])), array("name", "links"));
		$res = array();
		if(!empty($orga["links"]["projects"] ) ) {
			foreach ($form["links"]["projectExtern"] as $key => $value){
				
				if(empty($orga["links"]["projects"][$key] ) ) {
					$child = array();
					$child[] = array( 	"childId" => (String) $form["parentId"],
										"childType" => $form["parentType"],
										"childName" => $orga["name"],
										"roles" =>  (!empty($value["roles"]) ? $value["roles"] : array()) );

					$res[] = Link::multiconnect($child, $key, $value["type"]);
				
					$nbUser++;
				}
			}
		}
		$result = array("nb" => $nbUser,
						"res" => $res);
		return Rest::json($result);
	}

	public function actionStepAnswers() {
		$nbAnswer = 0;
		$answers = PHDB::find(	Form::ANSWER_COLLECTION, 
								array("modifiedByBatch.StepAnswers" => array('$exists' => 0)) );

		$res = array();
		foreach ($answers as $key => $answer) {

			if(empty($answer["step"])){
				$res [] = PHDB::update(Form::ANSWER_COLLECTION, 
						  	array("_id"=>new MongoId($key)),
	                        array(	'$set' => array( "step"=> "dossier")));
			}

		}

		return Rest::json($res);
	}

	public function actionUpdatePreferencesSendMail() {
		$str = "";
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
			$nbUser = 0;
			$users = PHDB::find(Person::COLLECTION, array("modifiedByBatch.updatePreferencesSendMail" => array('$exists' => 0)));
			foreach ($users as $key => $person) {
				$person["modifiedByBatch"][] = array("updatePreferencesSendMail" => new MongoDate(time()));
				$person["preferences"]["sendMail"] = true;
				$res = PHDB::update(Person::COLLECTION, 
											  	array("_id"=>new MongoId($key)),
						                        array('$set' => array(	"preferences" => $person["preferences"],
						                        						"modifiedByBatch" => $person["modifiedByBatch"])
						                        					)
						                    );

				if($res["ok"] == 1){
					$nbUser++;
				}else{
					$str .= "<br/> Error with user id : ".$key;
				}
			}

			$str .= "Number of user with preferences modified : ".$nbUser;
		}
		return $str;
	}


	public function actionUpdatePreferencesSendMailUserPending() {
		$str = "";
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
			$nbUser = 0;
			$users = PHDB::find(Person::COLLECTION, array("modifiedByBatch.UpdatePreferencesSendMailUserPending" => array('$exists' => 0), "roles.tobeactivated" => true));

			//Rest::json($users); exit;
			foreach ($users as $key => $person) {
				$person["modifiedByBatch"][] = array("UpdatePreferencesSendMailUserPending" => new MongoDate(time()));
				$person["preferences"]["sendMail"] = false;
				$res = PHDB::update(Person::COLLECTION, 
											  	array("_id"=>new MongoId($key)),
						                        array('$set' => array(	"preferences" => $person["preferences"],
						                        						"modifiedByBatch" => $person["modifiedByBatch"])
						                        					)
						                    );

				if($res["ok"] == 1){
					$nbUser++;
				}else{
					$str .= "<br/> Error with user id : ".$key;
				}
			}

			$str .= "Number of user with preferences modified : ".$nbUser;
		}
		return $str;
	}


	public function actionSearchLinkMissingType() {
		$str = "";
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
			$nbUser = 0;
			$users = PHDB::find(Person::COLLECTION,
								array('$and' => array(
									array("links" => array('$exists' => 1)),
									array('$or' => array(
										array("links.events.type" => array('$exists' => 0)),
										array("links.follows.type" => array('$exists' => 0)),
										array("links.memberOf.type" => array('$exists' => 0)),
										array("links.projects.type" => array('$exists' => 0))

									))
								)));




			//Rest::json($users); exit;
			foreach ($users as $key => $person) {
				//var_dump($value); $str .= "<br><br>";
				if(!empty($person["links"])){
					$newlinks = $person["links"];
					$up = false;
					foreach (@$person["links"] as $connectKey => $links) {
						if(in_array($connectKey,["follows", "memberOf", "projects", "events"])){
							foreach($links as $keyLinks => $value){
								//var_dump($value); $str .= "<br><br>";
								if(empty($value["type"])){
									//Rest::json($newlinks[$connectKey]); exit;
									//
									$str .= $key." ".$person["name"]." > links.".$connectKey.".".$keyLinks." <br/>";

									$type = null ;
									if($connectKey == "events"){
										$elt = Element::getSimpleByTypeAndId(Event::COLLECTION, $keyLinks,array("name"));
										$type = Event::COLLECTION;
									} else if($connectKey == "projects"){
										$elt = Element::getSimpleByTypeAndId(Project::COLLECTION, $keyLinks,array("name"));
										$type = Project::COLLECTION;
									}else{

										$elt = Element::getSimpleByTypeAndId(Organization::COLLECTION, $keyLinks,array("name"));
										$type = Organization::COLLECTION;

										if(empty($elt)){
											$elt = Element::getSimpleByTypeAndId(Person::COLLECTION, $keyLinks,array("name"));
											$type = Person::COLLECTION;
										}

										if(empty($elt)){
											$elt = Element::getSimpleByTypeAndId(Event::COLLECTION, $keyLinks,array("name"));
											$type = Event::COLLECTION;
										}

										if(empty($elt)){
											$elt = Element::getSimpleByTypeAndId(Project::COLLECTION, $keyLinks,array("name"));
											$type = Project::COLLECTION;
										}

										if(empty($elt)){
											$type = null;
										}

									}

									if(!empty($elt) && !empty($type)){
										$up = true;
										$value["type"] = $type;
										$newlinks[$connectKey][$keyLinks] = $value;
									}
									

								}
								
								


								
							}
						}
					}

					if($up === true){
						$person["modifiedByBatch"][] = array("SearchLinkMissingType" => new MongoDate(time()));
						//Rest::json($newlinks); exit;

						$res = PHDB::update(Person::COLLECTION, 
									array("_id"=>new MongoId($key)),
									array('$set' => array("links" => $newlinks )) );

						if($res["ok"] == 1){
							$nbUser++;
						}else{
							$str .= "<br/> Error with user id : ".$key;
						}
					}
					
				}
				
			}

			$str .= "Number of user with preferences modified : ".$nbUser;
		}
		return $str;
	}

	public function actionHotfixAddressCPMissing() {
		$str = "";
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
			$nbUser = 0;
			$types = array(Organization::COLLECTION, Project::COLLECTION, Event::COLLECTION);

			foreach ($types as $keyT => $valT) {
				$elts = PHDB::find($valT, 
					array("modifiedByBatch.HotfixAddressCPMissing" => array('$exists' => 0), 
							"address.addressCountry" => "FR",
							"address.postalCode" => array('$exists' => 0)));

				//Rest::json($elts); exit;
				foreach ($elts as $keyE => $elt) {
					$elt["modifiedByBatch"][] = array("HotfixAddressCPMissing" => new MongoDate(time()));

					$city = City::getById($elt["address"]["localityId"], array("postalCodes"));
					$cp = "NUUUUUUUULLLLLLL";
					foreach ($city["postalCodes"] as $keyPC => $valPC) {
						$cp = $valPC["postalCode"];
						break;
					}


					$elt["address"]["postalCode"] = $cp ;
					$str .= "<br/>  ".$keyE." : ".$valT." : ".$elt["name"]." :: ".$cp;
					$res = PHDB::update($valT, 
									  	array("_id"=>new MongoId($keyE)),
				                        array('$set' => array(	"address" => $elt["address"],
				                        						"modifiedByBatch" => $elt["modifiedByBatch"])
				                        					)
				                    );

					if($res["ok"] == 1){
						$nbUser++;
					}else{
						$str .= "<br/> Error with user id : ".$key;
					}
				}
			}
			

			$str .= "Number of user with preferences modified : ".$nbUser;
		}
		return $str;
	}

	public function actionIviaticTagGeo() {
		$str = "";
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
			$nbUser = 0;
			$tags = array();
			$elts = PHDB::find(Organization::COLLECTION, 
				array("modifiedByBatch.IviaticTagGeo" => array('$exists' => 0), 
						"source.key" => "iviatic" ) );

			//Rest::json($elts); exit;
			foreach ($elts as $keyE => $elt) {
				$elt["modifiedByBatch"][] = array("IviaticTagGeo" => new MongoDate(time()));


				$tagsI = array("NORD", "PAS-DE-CALAIS", "SOMME", "OISE", "AISNE");

// "Hors région"
// "MEL"

				// if( !empty($elt["address"]["level4Name"]) && !in_array($elt["address"]["level4Name"], $tags) ){

				// 	if( in_array($elt["address"]["level4Name"], $tagsI) )
				// 		$tags[] = $elt["address"]["level4Name"] ;
				// 	else{
				// 		//$tags[] = "Hors région";
				// 	}

				// 	if( $elt["address"]["level4Name"] == "NORD")
				// 		$tags[] = "MEL";
				// }

				if( !empty($elt["address"]["level4Name"]) ){

					if( in_array($elt["address"]["level4Name"], $tagsI) )
						$elt["tags"][] = $elt["address"]["level4Name"] ;
					else{
						$elt["tags"][] = "Hors région";
					}

					if( $elt["address"]["level4Name"] == "NORD")
						$elt["tags"][] = "MEL";
				}

				//$str .= "<br/>  ".$keyE." : ".$valT." : ".$elt["name"]." :: ".$cp;
				$res = PHDB::update(Organization::COLLECTION, 
								  	array("_id"=>new MongoId($keyE)),
								  	array('$set' => array(	"tags" => $elt["tags"],
								  							"modifiedByBatch" => $elt["modifiedByBatch"] ) ) );

				if($res["ok"] == 1){
					$nbUser++;
				}else{
					$str .= "<br/> Error with user id : ".$key;
				}
			}
			
			//Rest::json($tags); exit;
			$str .= "Number of user with preferences modified : ".$nbUser;
		}
		return $str;
	}


	public function actionPublicProposal() {
		$str = "";
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
			$nbUser = 0;
			$elts = PHDB::find(	Proposal::COLLECTION, 
								array( "modifiedByBatch.PublicProposal" => array('$exists' => 0), 
										"parentType" => "citoyens") );

			foreach ($elts as $keyE => $elt) {
				$elt["modifiedByBatch"][] = array("PublicProposal" => new MongoDate(time()));
				$elt["preferences"] = array();
				$elt["preferences"]["private"]=false;
				$nbUser++;
				$res = PHDB::update(Proposal::COLLECTION, 
									array("_id"=>new MongoId($keyE)),
									array('$set' => array(	"preferences" => $elt["preferences"],
															"modifiedByBatch" => $elt["modifiedByBatch"] ) ) );

				if($res["ok"] == 1){
					$nbUser++;
				}else{
					$str .= "<br/> Error with user id : ".$key;
				}
			}
			
			//Rest::json($tags); exit;
			$str .= "Number of user with preferences modified : ".$nbUser;
		}
		return $str;
	}

	public function actionReferenceFail() {
		$str = "";
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
			$nbUser = 0;
			$mails = PHDB::find(	Cron::COLLECTION, 
								array( 	"status" => "fail", 
										"tpl" => "referenceEmailInElement") );
			//Rest::json($mails); exit;
			foreach ($mails as $keyE => $elt) {
				$res = PHDB::update(Cron::COLLECTION, 
									array("_id"=>new MongoId($keyE)),
									array('$set' => array(	"status" => "pending" ) ) );

				if($res["ok"] == 1){
					$nbUser++;
				}else{
					$str .= "<br/> Error with user id : ".$key;
				}
			}
			
			//Rest::json($tags); exit;
			$str .= "Number of mails with status modified : ".$nbUser;
		}
		return $str;
	}

	
	public function actionContactToContacts() {
		$str = "";
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
			$nbUser = 0;
			$col = array(
				Organization::COLLECTION,
				Project::COLLECTION,
				Event::COLLECTION,
				Poi::COLLECTION,
			Form::COLLECTION,
				Form::ANSWER_COLLECTION,
				Survey::COLLECTION,
				Proposal::COLLECTION,
				Classified::COLLECTION,
				Action::COLLECTION,
			);
			$eltsReferenceEmail = array() ;
			foreach ($col as $k => $v) {
				

				$where3 = array("contact" => array('$exists' => 1) );
				$resElt3 = PHDB::find($v, $where3 , array("name", "contact"));

				//Rest::json($resElt3); exit ;
				foreach ($resElt3 as $key => $value) {
					$res = PHDB::update($v, 
									array("_id"=>new MongoId($key)),
									array('$set' => array("contacts" => $value["contact"]),
										  '$unset' => array("contact"=>"") ) );

					if($res["ok"] == 1){
						$nbUser++;
					}else{
						$str .= "<br/> Error with user id : ".$key;
					}
				}
			}
			
			//Rest::json($tags); exit;
			$str .= "Number of contacts modified : ".$nbUser;
		}
		return $str;
	}


	public function actionRemoveTranslateEpciOld() {
		$str = "";
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
			$nbUser = 0;
			$where3 = array("parentType" => "zones" );
			$resElt3 = PHDB::find(Zone::TRANSLATE, $where3);

			//Rest::json($resElt3); exit ;
			foreach ($resElt3 as $key => $value) {
				
				$exist = PHDB::findOneById($value["parentType"] ,$value["parentId"]);

				if(empty($exist)){
					//var_dump($exist);
					PHDB::remove(Zone::TRANSLATE,array("_id" => new MongoId($key)));
					$nbUser++;
				}
			}
					//Rest::json($tags); exit;
			$str .= "Number of contacts modified : ".$nbUser;
		}
		return $str;
	}

	public function actionCressReunion() {
		$str = "";
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
			$nbUser = 0;
			$where3 = array("source.key" => array('$in' => array("CRESS1", "CRESS2", "CRESS3", "CRESS4") ) );
			$resElt3 = PHDB::find(Organization::COLLECTION, $where3 , array("name", "source"));

			//Rest::json($resElt3); exit ;
			foreach ($resElt3 as $key => $value) {

				$value["source"]["key"] = "cressReunion";
				$value["source"]["keys"][] = "cressReunion";
				$res = PHDB::update(Organization::COLLECTION, 
								array("_id"=>new MongoId($key)),
								array('$set' => array("source" => $value["source"]) ) );

				if( $res["ok"] == 1 ){
					$nbUser++;
				} else {
					$str .= "<br/> Error with user id : ".$key;
				}
			}
			//Rest::json($tags); exit;
			$str .= "Number of contacts modified : ".$nbUser;
		}
		return $str;
	}

	public function actionTranslateEpci() {
		$str = "";
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
			$nbUser = 0;
			$where3 = array("epci" => array('$exists' => 1) );
			$resElt3 = PHDB::find(Zone::COLLECTION, $where3);

			//Rest::json($resElt3); exit ;
			foreach ($resElt3 as $key => $value) {
				$where2 = array("parentType" => Zone::COLLECTION, "parentId" => $key);
				$exist = PHDB::findOne(Zone::TRANSLATE,$where2, array("name") );

				if(!empty($exist)){
						$res = PHDB::update(Zone::TRANSLATE, 
										array("_id"=>new MongoId((String)$exist["_id"]) ),
										array('$set' => array("epci" => $value["epci"]) ) );

						$res = PHDB::update(Zone::COLLECTION, 
										array("_id"=>new MongoId($key) ),
										array('$set' => array("translateId" => (String)$exist["_id"]) ) ) ;
						$nbUser++;
					//Rest::json($tags); exit;
					$str .= "Number of contacts modified : ".$nbUser;
				}
			}
		}
		return $str;
	}

	public function actionTagESSCressReunion() {
		$str = "";
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
			$nbUser = 0;
			$where3 = array('$and' => array(
								array("source.key" => array('$in' => array("cressReunion") )),
							array( "tags" => array('$nin' => array("ESS")  ) ) ) );


			$resElt3 = PHDB::find(Organization::COLLECTION, $where3 , array("name", "source", "tags"));

			//Rest::json($resElt3); exit ;
			foreach ($resElt3 as $key => $value) {

				$value["tags"][] = "ESS";
				$res = PHDB::update(Organization::COLLECTION, 
								array("_id"=>new MongoId($key)),
								array('$set' => array("tags" => $value["tags"]) ) );

				if( $res["ok"] == 1 ){
					$nbUser++;
				} else {
					$str .= "<br/> Error with user id : ".$key;
				}
			}
			//Rest::json($tags); exit;
			$str .= "Number of contacts modified : ".$nbUser;
		}
		return $str;
	}

	public function actionEluCtenat() {
		$str = "";
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
			$nbUser = 0;
			$where3 = array("source.key" => "ctenat" );
			$resElt3 = PHDB::find(Project::COLLECTION, $where3 , array("name", "links"));

			//Rest::json($resElt3); exit ;
			foreach ($resElt3 as $key => $value) {

				if(!empty($value["links"]) && !empty($value["links"]["contributors"])){
					foreach ($value["links"]["contributors"] as $kC => $vC) {

						if(empty($vC["roles"]) && (empty($vC["isAdmin"]) || $vC["isAdmin"] == false) ){
							$value["links"]["contributors"][$kC]["roles"][] = "Elu";
						}else if(	!empty($vC["roles"]) && 
									(empty($vC["isAdmin"]) || $vC["isAdmin"] == false) &&
									!in_array("Elu", $vC["roles"]) ){
							$value["links"]["contributors"][$kC]["roles"][] = "Elu";
						}
					}
					//$nbUser++;
					$res = PHDB::update(Project::COLLECTION, 
									array("_id"=>new MongoId($key)),
									array('$set' => array("links" => $value["links"]) ) );

					if( $res["ok"] == 1 ){
						$nbUser++;
					} else {
						$str .= "<br/> Error with user id : ".$key;
					}
				}

			}
			//Rest::json($tags); exit;
			$str .= "Number of project modified : ".$nbUser;
		}
		return $str;
	}


	public function actionHotfixAddressLocalityId() {
		$str = "";
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
			$nbUser = 0;
			$where = array("address.localityId" => "0" );
			$elts = PHDB::find(Person::COLLECTION, $where , array("name", "address"));

			return Rest::json($elts); exit ;
			foreach ($elts as $id => $elt) {

				
				$nbUser++;
				// $res = PHDB::update(Project::COLLECTION, 
				// 				array("_id"=>new MongoId($key)),
				// 				array('$set' => array("links" => $value["links"]) ) );

				// if( $res["ok"] == 1 ){
				// 	$nbUser++;
				// } else {
				// 	$str .= "<br/> Error with user id : ".$key;
				// }
				

			}
			//Rest::json($tags); exit;
			$str .= "Number of project modified : ".$nbUser;
		}
		return $str;
	}

	public function actionFinancterLabel() {
		$str = "";
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
			$answers = PHDB::find("answers", array("source.key" => "ctenat",
													"answers" => array('$exists' => 1) ) ) ;
			//Rest::json($answers); exit;
			$resAnswers = array();
			$j = 0;
			$k = 0;
			$i = 0;
			foreach ($answers as $keyA => $answer) {

				if(!empty($answer["answers"]) && 
					!empty($answer["formId"]) && 
					!empty($answer["answers"][$answer["formId"]]) && 
					!empty($answer["answers"][$answer["formId"]]["answers"]) && 
					!empty($answer["answers"][$answer["formId"]]["answers"]["murir"])&& 
					!empty($answer["answers"][$answer["formId"]]["answers"]["murir"]["planFinancement"]) ){
					$pL = $answer["answers"][$answer["formId"]]["answers"]["murir"]["planFinancement"] ;
					$newPL = array();
					

					foreach ($pL as $keyPL => $valPL) {
						if(!empty($valPL["financer"]) && $valPL["financer"] == "public"){
							$valPL["financerType"] = "colfinanceur";
							$valPL["financer"] = "";
							$i++;
						}else if(!empty($valPL["financer"]) && $valPL["financer"] == "private"){
							$valPL["financerType"] = "acteursocioeco";
							$valPL["financer"] = "";
							$k++;
						} else if(empty($valPL["financerType"])){
							$valPL["financerType"] = $valPL["financer"];
							$valPL["financer"] = "";
							$j++;
						}
						$newPL[] = $valPL;
					}

					$resAnswers[$keyA] = $newPL;
					//$str .= $keyA."<br>";
					PHDB::update( "answers",
							array("_id"=>new MongoId($keyA)),
							array('$set' => array( "answers.".$answer["formId"].".answers.murir.planFinancement" => $newPL ) ) );
				}
			}
			$str .= "public : ".$i;
			$str .= "private : ".$k;
			$str .= "type : ".$j;
			//Rest::json($resAnswers); exit;

		}else{
			$str .= "NO NO nooooo";
		}
		return $str;
	}

	public function actionFinancterLIST($debug=null) {
		$str = "";
		$res = array();
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
			
			foreach (PHDB::distinct( "answers", "formId",array()) as $i => $slug) {
				foreach ( PHDB::find("answers", [ "answers.".$slug.".answers.financement" => ['$exists'=>1] ] )  as $id => $a ) 
				{ 
					$res[$id]= $a;
				}
			}
		}
		return Rest::json($res); exit;
	}

	public function actionFinancter($debug=null) {
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
			foreach (PHDB::distinct( "answers", "formId",array()) as $i => $slug) {
				$str .= "<b>".$slug."</b><br/>";
				
				$str .= PHDB::count("answers", [ "answers.".$slug.".answers.financement" => ['$exists'=>1] ] )." tag financement<br/>";
				$str .= PHDB::count("answers", [ "answers.".$slug.".answers.murir.budget" => ['$exists'=>1] ] )." budget<br/>";
				$str .= PHDB::count("answers", [ "answers.".$slug.".answers.murir.planFinancement" => ['$exists'=>1] ] )." plan financement<br/>";


				foreach ( PHDB::find("answers", [ "answers.".$slug.".answers.financement" => ['$exists'=>1] ] )  as $id => $a ) 
				{ 
					// //$str .= $id."<br/>";
					if( isset( $a[ "answers"][ $slug][ "answers"][ "financement"] ) && !$debug )
					{
						$keys = array_keys($a[ "answers"][ $slug ][ "answers"][ "financement"]);
						//$str .= "<br/><br/>".$a["_id"]."<br/>";
						foreach ($keys as $fi => $fk ) 
						{
							if(!empty($a[ "answers"][ $slug ][ "answers"][ "financement"][$fk]["total"]))
							{
								if($fk == 'depensesfonctionnement' )
									PHDB::update( "answers",
										array("_id"=>new MongoId($a["_id"])),
										array('$push' => array( "answers.".$slug.".answers.murir.budget" => [
				                            "nature" => "fonctionnement",
				                            "poste" => "Dépenses totales de fonctionnement",
				                            "amount2019" => "",
				                            "amount2020" => $a[ "answers"][ $slug ][ "answers"][ "financement"][$fk]["total"],
				                            "amount2021" => "",
				                            "amount2022" => ""
				                        ] ) ) );
								else if($fk == 'depensesinvestissement' )
									PHDB::update( "answers",
										array("_id"=>new MongoId($a["_id"])),
										array('$push' => array( "answers.".$slug.".answers.murir.budget" => [
				                            "nature" => "investissement",
				                            "poste" => "Dépenses totales d'investissement",
				                            "amount2019" => "",
				                            "amount2020" => $a[ "answers"][ $slug ][ "answers"][ "financement"][$fk]["total"],
				                            "amount2021" => "",
				                            "amount2022" => ""
				                        ] ) ) );
								else if($fk == 'financeursnonprecises' )
									PHDB::update( "answers",
									array("_id"=>new MongoId($a["_id"])),
									array('$push' => array("answers.".$slug.".answers.murir.planFinancement" => [
				                            "financerType" => "autre",
				                            "title" => "Financements Autre",
				                            "financer" => "",
				                            "amount2019" => "",
				                            "amount2020" => $a[ "answers"][ $slug ][ "answers"][ "financement"][$fk]["total"],
				                            "amount2021" => "",
				                            "amount2022" => ""
				                        ]) ) );
								else if($fk == 'autofinancement' )
									PHDB::update( "answers",
									array("_id"=>new MongoId($a["_id"])),
									array('$push' => array("answers.".$slug.".answers.murir.planFinancement" => [
				                            "financerType" => "acteursocioeco",
				                            "title" => "Autofinancement Porteur projet",
				                            "financer" => "",
				                            "amount2019" => "",
				                            "amount2020" => $a[ "answers"][ $slug ][ "answers"][ "financement"][$fk]["total"],
				                            "amount2021" => "",
				                            "amount2022" => ""
				                        ]) ) );
								else if($fk == 'financeur')
									PHDB::update( "answers",
									array("_id"=>new MongoId($a["_id"])),
									array('$push' => array("answers.".$slug.".answers.murir.planFinancement" => [
				                            "financerType" => "acteursocioeco",
				                            "title" => "Divers financements privés",
				                            "financer" => "",
				                            "amount2019" => "",
				                            "amount2020" => $a[ "answers"][ $slug ][ "answers"][ "financement"][$fk]["total"],
				                            "amount2021" => "",
				                            "amount2022" => ""
				                        ]) ) );
								else if($fk == 'banque')
									PHDB::update( "answers",
									array("_id"=>new MongoId($a["_id"])),
									array('$push' => array("answers.".$slug.".answers.murir.planFinancement" => [
				                            "financerType" => "acteursocioeco",
				                            "title" => "Banque",
				                            "financer" => "",
				                            "amount2019" => "",
				                            "amount2020" => $a[ "answers"][ $slug ][ "answers"][ "financement"][$fk]["total"],
				                            "amount2021" => "",
				                            "amount2022" => ""
				                        ]) ) );
								else if($fk == 'colautofinancement)' )
									PHDB::update( "answers",
									array("_id"=>new MongoId($a["_id"])),
									array('$push' => array("answers.".$slug.".answers.murir.planFinancement" => [
				                            "financerType" => "colfinanceur",
				                            "title" => "Autofinancement Collectivité",
				                            "financer" => "",
				                            "amount2019" => "",
				                            "amount2020" => $a[ "answers"][ $slug ][ "answers"][ "financement"][$fk]["total"],
				                            "amount2021" => "",
				                            "amount2022" => ""
				                        ]) ) );
								else if($fk == 'colfinanceur' )
									PHDB::update( "answers",
									array("_id"=>new MongoId($a["_id"])),
									array('$push' => array("answers.".$slug.".answers.murir.planFinancement" => [
				                            "financerType" => $fk,
				                            "title" => "Financements collectivités",
				                            "financer" => "",
				                            "amount2019" => "",
				                            "amount2020" => $a[ "answers"][ $slug ][ "answers"][ "financement"][$fk]["total"],
				                            "amount2021" => "",
				                            "amount2022" => ""
				                        ]) ) );
								else if($fk == 'dep')
									PHDB::update( "answers",
									array("_id"=>new MongoId($a["_id"])),
									array('$push' => array("answers.".$slug.".answers.murir.planFinancement" => [
				                            "financerType" => "departement",
				                            "title" => "Total Financements Département",
				                            "financer" => "",
				                            "amount2019" => "",
				                            "amount2020" => $a[ "answers"][ $slug ][ "answers"][ "financement"][$fk]["total"],
				                            "amount2021" => "",
				                            "amount2022" => ""
				                        ]) ) );
								else if($fk == 'region')
									PHDB::update( "answers",
									array("_id"=>new MongoId($a["_id"])),
									array('$push' => array("answers.".$slug.".answers.murir.planFinancement" => [
				                            "financerType" => $fk,
				                            "title" => "Total Financements Région",
				                            "financer" => "",
				                            "amount2019" => "",
				                            "amount2020" => $a[ "answers"][ $slug ][ "answers"][ "financement"][$fk]["total"],
				                            "amount2021" => "",
				                            "amount2022" => ""
				                        ]) ) );
								else if($fk == 'etat')
									PHDB::update( "answers",
									array("_id"=>new MongoId($a["_id"])),
									array('$push' => array("answers.".$slug.".answers.murir.planFinancement" => [
				                            "financerType" => $fk,
				                            "title" => "Divers Financements Etat",
				                            "financer" => "",
				                            "amount2019" => "",
				                            "amount2020" => $a[ "answers"][ $slug ][ "answers"][ "financement"][$fk]["total"],
				                            "amount2021" => "",
				                            "amount2022" => ""
				                        ]) ) );
								else if($fk == 'ademe')
									PHDB::update( "answers",
									array("_id"=>new MongoId($a["_id"])),
									array('$push' => array("answers.".$slug.".answers.murir.planFinancement" => [
				                            "financerType" => $fk,
				                            "title" => "Total Financements ADEME",
				                            "financer" => "",
				                            "amount2019" => "",
				                            "amount2020" => $a[ "answers"][ $slug ][ "answers"][ "financement"][$fk]["total"],
				                            "amount2021" => "",
				                            "amount2022" => ""
				                        ]) ) );
								else if($fk == 'caisseconsignations' )
									PHDB::update( "answers",
									array("_id"=>new MongoId($a["_id"])),
									array('$push' => array("answers.".$slug.".answers.murir.planFinancement" => [
				                            "financerType" => "bdt",
				                            "title" => "Total financements Banque des territoires",
				                            "financer" => "",
				                            "amount2019" => "",
				                            "amount2020" => $a[ "answers"][ $slug ][ "answers"][ "financement"][$fk]["total"],
				                            "amount2021" => "",
				                            "amount2022" => ""
				                        ]) ) );
								else if($fk == 'europe')
									PHDB::update( "answers",
									array("_id"=>new MongoId($a["_id"])),
									array('$push' => array("answers.".$slug.".answers.murir.planFinancement" => [
				                            "financerType" => $fk,
				                            "title" => "Total Financement Europe",
				                            "financer" => "",
				                            "amount2019" => "",
				                            "amount2020" => $a[ "answers"][ $slug ][ "answers"][ "financement"][$fk]["total"],
				                            "amount2021" => "",
				                            "amount2022" => ""
				                        ]) ) );
								else if($fk == 'chambresconsulaires' )
									PHDB::update( "answers",
									array("_id"=>new MongoId($a["_id"])),
									array('$push' => array("answers.".$slug.".answers.murir.planFinancement" => [
				                            "financerType" => "acteursocioeco",
				                            "title" => "Chambre consulaire",
				                            "financer" => "",
				                            "amount2019" => "",
				                            "amount2020" => $a[ "answers"][ $slug ][ "answers"][ "financement"][$fk]["total"],
				                            "amount2021" => "",
				                            "amount2022" => ""
				                        ]) ) );
								else if($fk == 'agencedeleau' )
									PHDB::update( "answers",
									array("_id"=>new MongoId($a["_id"])),
									array('$push' => array("answers.".$slug.".answers.murir.planFinancement" => [
				                            "financerType" => "agenceLeau",
				                            "title" => "Total Financements Agence de l'eau",
				                            "amount2019" => "",
				                            "amount2020" => $a[ "answers"][ $slug ][ "answers"][ "financement"][$fk]["total"],
				                            "amount2021" => "",
				                            "amount2022" => ""
				                        ]) ) );
								else if($fk == 'fondations' )
									PHDB::update( "answers",
									array("_id"=>new MongoId($a["_id"])),
									array('$push' => array("answers.".$slug.".answers.murir.planFinancement" => [
				                            "financerType" => "acteursocioeco",
				                            "title" => "Financement provenant de Fondations",
				                            "amount2019" => "",
				                            "amount2020" => $a[ "answers"][ $slug ][ "answers"][ "financement"][$fk]["total"],
				                            "amount2021" => "",
				                            "amount2022" => ""
				                        ]) ) );
							}
							// else 
							// 	$str .= $fk." is empty value<br/>";
							
						}
						PHDB::update( "answers",
										array("_id"=>new MongoId($a["_id"])),
										array('$rename' => array( "answers.".$slug.".answers.financement" => "financementFait" ) ) );
						$str .= "<br/>financement changed<br/>";
					}
					// else 
					// 	$str .= "no Finance<br/>";

				} 
				$str .= "-----------------------------------<br/>";
			}
			//Rest::json($elts); exit ;
			//Rest::json($tags); exit;
			//$str .= "Number of project modified : ".$nbUser;
		}
		return $str;
	}


	public function actionHotfixSendMail(){

  		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
            $elts=PHDB::find(Person::COLLECTION, 
            		array("preferences.sendMail" => array('$exists' => 0)),
            		array("name", "email", "pending", "roles", "preferences", "invitedBy", "created"));
        	$res["countTotal"] = 0;
        	$res["countinvitedBy"] = 0;
        	$res["countpending"] = 0;
        	$res["countroles"] = 0;
        	$res["person"] = array();
        	foreach ($elts as $id => $elt) {
        		$valid = Mail::authorizationMail($elt['email']);
                if(!empty($elt["roles"]) && $valid === true){
                	$res["countTotal"]++;
	                if(!empty($elt["pending"]))
	                	$res["countpending"]++;

	                if(!empty($elt["invitedBy"])){
	                	$res["countinvitedBy"]++;
	                }
	                if(!empty($elt["created"]))
	                	$elt["created"] = date("d/m/Y", $elt["created"]->sec);
	                PHDB::update(Person::COLLECTION,
		                    array("_id" => $elt["_id"]),
		                    array('$set' => array("preferences.sendMail" => true))
		                );
	                $res["person"][$id] = $elt;
                }
        	}
            return Rest::json($res); exit;
        }
    }

    public function actionNAGroupToNGO(){
  		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
            $elts=PHDB::find(Organization::COLLECTION, 
            		array("source.key" => "notragora",
            				"type" => "Group"),
            		array("name", "type"));
        	$res["countTotal"] = 0;
        	$res["group"] = array();
        	foreach ($elts as $id => $elt) {
                	$res["countTotal"]++;
	                PHDB::update(Organization::COLLECTION,
		                    array("_id" => $elt["_id"]),
		                    array('$set' => array("type" => "NGO"))
		                );
	                $res["group"][$id] = $elt;
        	}
            return Rest::json($res); exit;
        }
    }

    public function actionBashRaffinerie(){
  		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) ) ) {
            $raffinerie=PHDB::findOne(Organization::COLLECTION, 
            		array("slug" => "laRaffinerie3"));
        	$poles = array("Jardin","Sport","Economique","Culture","Collaboratif","Les communs","CA");
        	 //Rest::json($raffinerie); exit;
        	$res["project"] = array();
        	foreach ($raffinerie["links"]["projects"] as $id => $val) {
        		$project=PHDB::findOneById(Project::COLLECTION, $id, array("name", "tags"));
        		$newsTags = array();
        		foreach ($project["tags"] as $keyT => $valT) {
        			if(in_array($valT, $poles))
        				$newsTags[] = trim($valT);
        		}


        		foreach ($project["tags"] as $keyT => $valT) {
        			if(!in_array($valT, $newsTags))
        				$newsTags[] = trim($valT);
        		}

        		PHDB::update(Project::COLLECTION,
		                    array("_id" => $project["_id"]),
		                    array('$set' => array("tags" => $newsTags))
		                );
        		$res["project"][$id] = array(
        			"name" => $project["name"],
        			"newsTags" => $newsTags,
        			"oldTags" => $project["tags"]
        		) ;
        	}
            return Rest::json($res); exit;
        }
    }

    public function actionNAOrgaCategory(){
  		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
            $elts=PHDB::find(Organization::COLLECTION, 
            		array(	"source.key" => "notragora",
            				"categoryNA" => array('$exists' => 0)),
            		array("name", "type", "tags"));
        	$res["countTotal"] = 0;
        	$res["both"] = array();
        	$res["group"] = array();
        	$res["supports"] = array();
        	$res["producors"] = array();
        	$res["partner"] = array();
        	foreach ($elts as $id => $elt) {

        			$categoryNA = array();
                	$res["countTotal"]++;
                	$newTags = array();
                	if(!empty($elt["tags"])){
                		$elt["oldTags"] = $elt["tags"];

                		if( in_array("producors", $elt["tags"]) ){
	                		$categoryNA[] = "producors";
	                		array_splice($elt["tags"], array_search('producors', $elt["tags"]), 1);
	                	}

	                	if( in_array("supports", $elt["tags"]) ){
	                		$categoryNA[] = "supports";
	                		array_splice($elt["tags"], array_search('supports', $elt["tags"]), 1);
	                	}

	                	if( in_array("partner", $elt["tags"]) ){
	                		$categoryNA[] = "partner";
	                		array_splice($elt["tags"], array_search('partner', $elt["tags"]), 1);
	                	}


	                	$newTags = $elt["tags"];
                	}

                	if(!empty($categoryNA))
                		$elt["categoryNA"] = $categoryNA;
                	else
                		$elt["categoryNA"][] = "group";
                	
                	PHDB::update(Organization::COLLECTION,
						array("_id" => $elt["_id"]),
						array('$set' => array( "categoryNA" => $elt["categoryNA"],
										"tags" => $newTags ))
					);

                	if( in_array("producors", $categoryNA) ){
                		$res["producors"][$id] = $elt;
                	} if( in_array("supports", $categoryNA) ){
                		$res["supports"][$id] = $elt;
                	} else if( in_array("partner", $categoryNA) ){
                		$res["partner"][$id] = $elt;
                	} else
                		$res["group"][$id] = $elt;

					
	                
        	}
            return Rest::json($res); exit;
        }
    }

    public function actionTestLastEvent(){
    	if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
	  		$events = Element::getLastEvents("organizations", "5e203fc98b509c36568b4584", 4, "2016-01-23T00:00:00+04:00");
	  		return Rest::json($events); exit;
	  	}
	}

	public function actionAddZonePF(){
		//$level1 = Zone::createLevel("PF", "Polynésie française", "1");
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
			$url = "https://nominatim.openstreetmap.org/details.php?osmtype=R&osmid=3412620&class=boundary&format=json&email=".Yii::app()->params["contactEmail"];
			$nominatim = SIG::getUrl($url);
			//var_dump(json_decode($nominatim)); exit;
			$nominatim = json_decode($nominatim, true);
			$zone=array();
			$info=array();
			$translates=array();
			if(!empty($nominatim)){
				$zone["name"] = $nominatim["localname"];
				$zone["countryCode"] = "PF";
				$zone["level"] = array("1");
				// $zone["geo"] = SIG::getFormatGeo($nominatim["lat"], $nominatim["lon"]);
				// $zone["geoPosition"] = SIG::getFormatGeoPosition($nominatim["lat"], $nominatim["lon"]);
				$lat = $nominatim["geometry"]["coordinates"][1];
				$lon = $nominatim["geometry"]["coordinates"][0];
				$zone["geo"] = SIG::getFormatGeo($lat, $lon);
				$zone["geoPosition"] = SIG::getFormatGeoPosition($lat, $lon);
				//$zone["geoShape"] = $nominatim["geojson"];
				if(!empty($nominatim["osm_id"]))
					$zone["osmID"] = $nominatim["osm_id"];
				if(!empty($nominatim["extratags"]["wikidata"]))
					$zone["wikidataID"] = $nominatim["extratags"]["wikidata"];

				if(!empty($nominatim["names"])){
					foreach ($nominatim["names"] as $keyName => $valueName) {
						$arrayName = explode(":", $keyName);
						if(!empty($arrayName[1]) && $arrayName[0] == "name" && strlen($arrayName[1]) == 2 && $nominatim["localname"] != $valueName){
							$translates[strtoupper($arrayName[1])] = $valueName;
						}
					}	
				}
				$info["countryCode"] = $zone["countryCode"];
				
				$info["parentType"] = Zone::COLLECTION;
				$info["translates"] = $translates;
				$info["origin"] = $nominatim["localname"];
			}
			$res = array(
				"zone" =>$zone,
				"info" =>$info
			);
			// PHDB::insert(Zone::COLLECTION, $zone );
			// $info["parentId"] = (String) $zone["_id"];
			// PHDB::insert(Zone::TRANSLATE, $info);
			// PHDB::update(Zone::COLLECTION, 
			// 			array("_id"=>new MongoId((String) $zone["_id"])),
			// 			array('$set' => array("translateId" => (String)$info["_id"]))
			// );
			return Rest::json($res);
		}
	}

	public function actionCitySaintPierreEtMichellon(){
		//$level1 = Zone::createLevel("PF", "Polynésie française", "1");
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
			$city = PHDB::find(City::COLLECTION, array('postalCodes.postalCode'=> '97500'));
			return Rest::json($city); exit;
		}
	}
	
	public function actionBashforeachActiveTags() {
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
	    $existingTags = Tags::getActiveTags();
	      foreach($existingTags as $tag) {
			Tags::insertActiveTags($tag);
		  }
		 return "create tags collection";
		}
  	}

  	public function actionRecurentEventsTimeZone() {
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
	    	$events=PHDB::find(Event::COLLECTION, 
	            		array(
	            			"openingHours" => array('$exists' => 1),
	            			"timeZone" => array('$exists' => 0)
	            		),
	            		array("name", "openingHours", "startDate", "endDate", "created", "creator", "source", "address.addressCountry"));
	    	$count = array(
	    		"source" => array(),
	    		"notSource" => 0,
	    		"FR" => 0,
	    		"RE" => 0,
	    		"all" => 0,
	    		"notAddress" => 0,
	    		"fuseau" => array()
	    	);


	    	$timezone = array(
	    		"FR" => "Europe/Paris",
				"BE" => "Europe/Brussels",
				"RE" => "Indian/Reunion",
				"ES" => "Europe/Madrid",
				"MX" => "America/Mexico_City",
				"MQ" => "America/Martinique",
				"AT" => "Europe/Vienna",
				"DE" => "Europe/Berlin",
				"CL" => "America/Santiago",
				"CH" => "Europe/Zurich",
				"IT" => "Europe/Rome",
				"GF" => "America/Cayenne",
				"MC" => "Europe/Monaco",
				"GP" => "America/Guadeloupe"
	    	);
	    	//Rest::json($count); exit;
	    	foreach ($events as $key => $value) {
	    		if( !empty($value["address"]) && !empty($value["address"]["addressCountry"]) ){
	    			if(empty($count[$value["address"]["addressCountry"]]) )
	    				$count[$value["address"]["addressCountry"]] = 0 ;
	    			$count[$value["address"]["addressCountry"]]++;


	    			// if(empty($count["fuseau"][$value["address"]["addressCountry"]]))
	    			// 	$count["fuseau"][$value["address"]["addressCountry"]] = geoip_time_zone_by_country_and_region($value["address"]["addressCountry"]) ;


	    			if(!empty($timezone[$value["address"]["addressCountry"]])){
	    				$cc = date(DateTime::ISO8601, $value["created"] ) ;
	    				//date_default_timezone_set ( $timezone[$value["address"]["addressCountry"]] );
	    				$mars = new DateTimeZone($timezone[$value["address"]["addressCountry"]]);
	    				$events[$key]["created_at"] = new DateTime($cc) ;
	    				$events[$key]["created_at"]->setTimezone($mars);
	    				//$events[$key]["created_atFormat"] = $events[$key]["created_at"]->format(DateTime::ISO8601);
	    				$events[$key]["timeZone"] = $timezone[$value["address"]["addressCountry"]] ;
	    				$ts = $events[$key]["created_at"]->getTimestamp();
						$events[$key]["created_at"] = new MongoDate($ts);
	    			}

	    		}else{
	    			if(!empty($value["source"])){
		    			if(empty($count["source"][$value["source"]["key"]]))
		    				$count["source"][$value["source"]["key"]] = 0;
		    			$count["source"][$value["source"]["key"]]++;

		    			if($value["source"]["key"] == "hva"){
		    				$events[$key]["timeZone"] = $timezone["FR"];
		    			}else if($value["source"]["key"] == "lapossession"){
		    				$events[$key]["timeZone"] = $timezone["RE"];
		    			}

		    		}else {
		    			$count["notSource"]++;
		    			$events[$key]["timeZone"] = $timezone["FR"];
		    		}

	    			$count["notAddress"]++;
	    		}
	    		$count["all"]++;
	    		

	    		
	    		if(!empty($events[$key]["timeZone"])){
	    			PHDB::update(Event::COLLECTION,
						array("_id" => $events[$key]["_id"]),
						array('$set' => array( "timeZone" => $events[$key]["timeZone"]) )
					);
	    		}
	    		

	    	}
	    	$result = array(
	    		"count" => $count,
	    		"events" => $events,

	    	);
	    	return Rest::json($result); 
	    }
  	}


  	public function actionEventsTimeZone() {
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
			date_default_timezone_set('UTC');
	    	$events=PHDB::find(Event::COLLECTION, 
	            		array(
	            			"openingHours" => array('$exists' => 0),
	            			"timeZone" => array('$exists' => 0)
	            		),
	            		array("name", "openingHours", "startDate", "endDate", "creator", "created", "source", "address.addressCountry"));
	    	$count = array(
	    		"source" => array(),
	    		"notSource" => 0,
	    		"FR" => 0,
	    		"RE" => 0,
	    		"all" => 0,
	    		"notAddress" => 0,
	    		"address" => 0,
	    		"fuseau" => array()
	    	);

	    	$timezone = array(
	    		"FR" => "Europe/Paris",
				"BE" => "Europe/Brussels",
				"RE" => "Indian/Reunion",
				"ES" => "Europe/Madrid",
				"MX" => "America/Mexico_City",
				"MQ" => "America/Martinique",
				"AT" => "Europe/Vienna",
				"DE" => "Europe/Berlin",
				"CL" => "America/Santiago",
				"CH" => "Europe/Zurich",
				"IT" => "Europe/Rome",
				"GF" => "America/Cayenne",
				"MC" => "Europe/Monaco",
				"GP" => "America/Guadeloupe"
	    	);

	    	//Rest::json($count); exit;
	    	foreach ($events as $key => $value) {
	    		if( !empty($value["address"]) && !empty($value["address"]["addressCountry"]) ){
	    			if(empty($count[$value["address"]["addressCountry"]]))
	    				$count[$value["address"]["addressCountry"]] = 0 ;
	    			$count[$value["address"]["addressCountry"]]++;


	    			// if(empty($timezone[$value["address"]["addressCountry"]]))
	    			// 	$timezone[$value["address"]["addressCountry"]] = geoip_time_zone_by_country_and_region($value["address"]["addressCountry"]) ;

	    			if(!empty($timezone[$value["address"]["addressCountry"]])){
	    				// $mars = new DateTimeZone($timezone[$value["address"]["addressCountry"]]);
	    				// $cc = date(DateTime::ISO8601, $value["startDate"]->sec ) ;
	    				// $events[$key]["startDateTest"] = new DateTime($cc) ;
	    				// $events[$key]["startDateNew"] = new DateTime($cc) ;
	    				// $events[$key]["startDateNew"]->setTimezone($mars);
						$events[$key]["timeZone"] = $timezone[$value["address"]["addressCountry"]] ;

	    			}

	    		} else {
	    			if(!empty($value["source"])){
		    			if(empty($count["source"][$value["source"]["key"]]))
		    				$count["source"][$value["source"]["key"]] = 0;
		    			$count["source"][$value["source"]["key"]]++;

		    			if($value["source"]["key"] == "journalInsoumisChambery"||
		    				$value["source"]["key"] == "collectifEssArrageois" ||
		    				$value["source"]["key"] == "lesPepitesEss" ||
		    				$value["source"]["key"] == "maillage" ||
		    				$value["source"]["key"] == "emploiCsc" ||
		    				$value["source"]["key"] == "CollectifEcocitoyenDeMontreuilSurMer" ||
		    				$value["source"]["key"] == "meuseCampagnes" ){
		    				$events[$key]["timeZone"] = $timezone["FR"];
		    			}else if(	$value["source"]["key"] == "lapossession" ||
		    						$value["source"]["key"] == "ctenat" || 
		    						$value["source"]["key"] == "hva" ||
		    						$value["source"]["key"] == "laRaffinerie3"){
		    				$events[$key]["timeZone"] = $timezone["RE"];
		    			}else if( $value["source"]["key"] == "tepozencomun"){
		    				$events[$key]["timeZone"] = $timezone["MX"];
		    			}

		    		}else{
		    			if(!empty($events[$key]["creator"])){
    						$creator=PHDB::findOneById(Person::COLLECTION, 
			            		$events[$key]["creator"],
			            		array("name", "address.addressCountry"));
    						$events[$key]["endDateTest"] = $creator;

    						if( !empty($creator["address"]) && 
    							!empty($creator["address"]["addressCountry"]) &&
    							!empty($timezone[$creator["address"]["addressCountry"]])){
			    				$events[$key]["timeZone"] = $timezone[$creator["address"]["addressCountry"]] ;
			    			}
			    		}
				    	
		    			$count["notSource"]++;
		    		}
	    			$count["notAddress"]++;
	    		}
	    		$count["all"]++;
	    		
	    		
	    		if(!empty($events[$key]["timeZone"])){
	    			PHDB::update(Event::COLLECTION,
						array("_id" => $events[$key]["_id"]),
						array('$set' => array( "timeZone" => $events[$key]["timeZone"] ) )
					);
	    		}
	    		

	    	}
	    	$result = array(
	    		"count" => $count,
	    		"timezone" => $timezone,
	    		//"events" => $events,

	    	);
	    	return Rest::json($result); 
	    }
  	}

  	public function actionEventsTest() {
  		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
	  		$date = new DateTime("2020-05-08T15:30:00+04:00");
	  		$newD = new DateTime();
	  		$timezoneEvent = new DateTimeZone($date->getTimezone()->getName());
	  		$format = $date->format(DateTime::ISO8601);
	  		$date2 = new DateTime($format);
	  		$res = array(
	  			"date" => $date,
	  			"newD" => $newD,
	  			"timezoneEvent" => $timezoneEvent,
	  			"format" => $date2
	  		);
	  		return Rest::json($res); exit;
	  		//return Rest::json($date->getTimezone()->getName());
	  	}
  	}


  	public function actionPolynesie() {
  		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
	  		$date = new DateTime("2020-05-08T15:30:00+04:00");
	  		$newD = new DateTime();
	  		$timezoneEvent = new DateTimeZone($date->getTimezone()->getName());
	  		$format = $date->format(DateTime::ISO8601);
	  		$date2 = new DateTime($format);
	  		$res = array(
	  			"date" => $date,
	  			"newD" => $newD,
	  			"timezoneEvent" => $timezoneEvent,
	  			"format" => $date2
	  		);
	  		return Rest::json($res); exit;
	  		//return Rest::json($date->getTimezone()->getName());
	  	}
  	}

  	public function actionCountryMissing(){
  		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
			$countries = Zone::getListCountry();

			$cs = OpenData::$phCountries;
			$tabC = array();
			foreach ($cs as $key => $value) {
				$t = false;
				foreach ($countries as $k => $val) {
					if($val["countryCode"] == $key)
						$t = true;
				}
				

				if($t === false)
					$tabC[$key] = $value;
			}

			foreach ($tabC as $key => $val) {
				$params = array(
					"text" => $val,
					"countryCode" => $key,
					"namedetails" => true,
					"extratags" => true
				);
				$tabC[$key] = array("text" => $val,"url" => Nominatim::getAddressByNominatim($params));
				// $tabC[$key] = array("api" => Nominatim::getAddressByNominatim($params),
				// 					"zones" => null);
				// if(!empty($tabC[$key]["api"])){
				// 	foreach ($tabC[$key]["api"] as $keyAPI => $valAPI) {
				// 		if( !empty($valAPI["class"]) && $valAPI["class"] == "boundary" && empty($valAPI["zones"])){
				// 			$tabC[$key]["zones"] = Nominatim::createLevel($valAPI["lat"], $valAPI["lon"], $key, "1", null, $valAPI) ;
				// 		}
				// 	}
				// }
				//break;		
			}
			
			return Rest::json($tabC);
		}
	}

	public function actionCitiesGeoShapeMissingNotFR(){
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
			$cities = PHDB::find(City::COLLECTION, 
						array( "geoShape" => array('$exists' => 0),
								 "country" => array('$ne' => "FR")), 
					array("name", "country", "geo", "osmID", "translateId"));

			

			foreach ($cities as $key => $city) {
				$cityNominatim = null;
				if(!empty($city["osmID"])){
					$url = "https://nominatim.openstreetmap.org/details.php?osmtype=N&osmid=".$city["osmID"]."&format=json";
					$url .= "&email=".Yii::app()->params["contactEmail"];
					$url .= "&polygon_geojson=1";
					$cityNominatim = json_decode( SIG::getUrl($url), true ) ;
				}

				if(empty($cityNominatim) || !empty($cityNominatim["error"]) ){
					$cityNominatim =  Nominatim::getCityByLatLon($city["geo"]["latitude"], $city["geo"]["longitude"]);
				}

				if(!empty($cityNominatim) && !empty($cityNominatim["geojson"])){
					PHDB::update(City::COLLECTION, 
						array("_id"=>new MongoId($key)),
						array('$set' => array("geoShape" => $cityNominatim["geojson"] ) )
					);
					$cities[$key]["geoShape"] = true;
				}
				
			}
			
			return Rest::json($cities);
		}
	}

	// public function actionTestShanghai(){
		// $url = "https://nominatim.openstreetmap.org/details.php?osmtype=R&osmid=913067&class=boundary&format=json";
		// $url .= "&email=".Yii::app()->params["contactEmail"];
		// $url .= "&polygon_geojson=1";
  //       $url .= "&extratags=1";
  //       $url .= "&namedetails=1";
		// $cityNominatim = json_decode( SIG::getUrl($url), true ) ;
	// 	Rest::json($cityNominatim); exit;
	// 	$city = Nominatim::createCity(array("countryCode" => "CN"), Yii::app()->session["userId"], $cityNominatim);
	// 	Rest::json($city);
	// }

	public function actionCitiesGeoShapeMissingPolynesie(){
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
			$cities = PHDB::find(City::COLLECTION, 
						array( "geoShape" => array('$exists' => 0),
						"depName" => "Polynesie",), 
					array("name", "country", "geo", "osmID", "translateId"));

			$level1 = Zone::getCountryByCountryCode("PF");


			// foreach ($cities as $key => $city) {
			// 	$geoShape = null;

			// 	$cityNominatim =  Nominatim::getCityByLatLon($city["geo"]["latitude"], $city["geo"]["longitude"]);

			// 	if(!empty($cityNominatim) && !empty($cityNominatim["geojson"])){
			// 		$geoShape = $cityNominatim["geojson"];
			// 	}

			// 	if(!empty($cityNominatim["osm_id"])){
			// 		PHDB::update(City::COLLECTION, 
			// 			array("_id"=>new MongoId($key)),
			// 			array('$set' => array(	
			// 					"geoShape" => $geoShape,
			// 					"country" => "PF",
			// 					"osmID" => $cityNominatim["osm_id"],
			// 					"level1" =>  (String)$level1["_id"],
			// 					"level1Name" => $level1["name"],
			// 			) )
			// 		);
			// 		$cities[$key]["	geoShape"] = true;
			// 	}
				
				
			// }
			
			return Rest::json($cities);
		}
	}

	public function actionCitiesGeoShapeMissing(){
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
			$cities = PHDB::find(City::COLLECTION, 
						array( "geoShape" => array('$exists' => 0) ), 
					array("name", "country", "geo", "osmID", "translateId"));
			return Rest::json($cities); exit;
			$res = array();
			// foreach ($cities as $key => $city) {
				
			// 	if(strstr($city["name"], 'Arrondissement') == false){
			 		$cityNominatim =  Nominatim::getCityByLatLon($city["geo"]["latitude"], $city["geo"]["longitude"]);

			// 		if( !empty($cityNominatim) && !empty($cityNominatim["geojson"]) && !empty($cityNominatim["osm_id"]) ){
			// 			PHDB::update(City::COLLECTION, 
			// 				array("_id"=>new MongoId($key)),
			// 				array('$set' => array(	
			// 						"geoShape" => $cityNominatim["geojson"],
			// 						"osmID" => $cityNominatim["osm_id"],
			// 				) )
			// 			);
			// 			$cities[$key]["geoShape"] = true;
			// 		}
			// 		$res[$key] = $city;
			// 	}
			// }
			return Rest::json($res);
		}
	}
	
  	public function actionCressMissingSiret(){
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
			$orga = PHDB::find(Organization::COLLECTION,
								array(  "siren" => array('$exists' => 1),
										"source.new" => array('$exists' => 0),
										"source.key" => "cressReunion"),
								array("name", "siren","siret", "arrondissement", "address.streetAddress", "address.addressLocality"));
			foreach ($orga as $key => $value) {
				$res[] = array( "siren" => $value["siren"],
								"siret" => $value["siret"],
								"name" => $value["name"],
								// "arrondissement" => $value["arrondissement"],
								// "rue" => $value["address"]["streetAddress"],
								// "ville" => $value["address"]["addressLocality"] 
							);
			}
			Rest::csv($res); exit;
		}
	}

	public function actionRemoveSourceNotNewCress(){
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
	        $orgs = PHDB::find(  Organization::COLLECTION, 
	                            array( "source.key" => "cressReunion",
	                                    "source.new" => array('$exists' => 0)),
	                            array("name", "source") );

	        if(!empty($orgs)){
	            foreach ($orgs as $key => $value) {
	                if(!empty($value["source"]["keys"])){
	                    $new =array();
	                    foreach ($value["source"]["keys"] as $k => $v) {
	                        if($v != "cressReunion")
	                            $new[] = $v;
	                    }
	                    $value["source"]["keys"] = $new;
	                }

	                if(!empty($value["source"]["key"]) && $value["source"]["key"] == "cressReunion")
	                    $value["source"]["key"] = "";

	                PHDB::update(Organization::COLLECTION,
	                    array("_id"=>new MongoId($key) ) , 
	                    array('$set' => array("source" => $value["source"]) )
	                ); 
	            }
	        }
	        //return $params;
	    }
    }



    public function actionUpdateSendMailPreferences(){
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
	        $person = PHDB::find(  Person::COLLECTION, 
	                            array( "preferences.sendMail" => true ),
	                            array("name", "source", "preferences.sendMail") );
			$i = 0;
	        if(!empty($person)){
	            foreach ($person as $key => $value) {
	                if(!empty($value["preferences"]["sendMail"]) && $value["preferences"]["sendMail"] === true ){
	                    $new =array();
	                    $value["preferences"]["sendMail"] = array(
	                    	"source" => array()
	                    );

	                    if( !empty($value["source"]["key"]))
	                    	$value["preferences"]["sendMail"]["source"][] = $value["source"]["key"];
	                    else
	                    	$value["preferences"]["sendMail"]["source"][] = "communecter";
	                	
	                    PHDB::update(Person::COLLECTION,
		                    array("_id"=>new MongoId($key) ) , 
		                    array('$set' => array("preferences" => $value["preferences"]) )
		                );
	                    $i++;
	                }
	            }
	        }
	        return "Il y a eu ".$i. " update ";
	    }

    }

    public function actionRefactorPolynesie(){
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
			$zonePF = Zone::getCountryByCountryCode("PF");
			//Rest::json($zonePF);exit;
			if(empty($zonePF)){
				$paramsPF = array(
					"id" => "3412620",
					"type" => "R"
				);
				$polynesieNominatim = Nominatim::getElementByIdNominatim($paramsPF);
				//Rest::json($polynesieNominatim); exit;
				if(!empty($polynesieNominatim["geometry"]) ){
					$polynesieNominatim["lat"] = $polynesieNominatim["geometry"]["coordinates"][1];
					$polynesieNominatim["lon"] = $polynesieNominatim["geometry"]["coordinates"][0];
					//Rest::json($polynesieNominatim); exit;

					Nominatim::createLevel($polynesieNominatim["lat"], $polynesieNominatim["lon"], "PF", "1", array(), $polynesieNominatim);
					$zonePF = Zone::getCountryByCountryCode("PF");
					
				}
				
			}


			$zoneWF = Zone::getCountryByCountryCode("WF");
			//Rest::json($zoneWF);exit;
			if(empty($zoneWF)){
				$paramsPF = array(
					"id" => "290162",
					"type" => "R"
				);
				$wfNominatim = Nominatim::getElementByIdNominatim($paramsPF);
				if(!empty($wfNominatim["geometry"]) ){
					$wfNominatim["lat"] = $wfNominatim["geometry"]["coordinates"][1];
					$wfNominatim["lon"] = $wfNominatim["geometry"]["coordinates"][0];
					//Rest::json($wfNominatim); exit;

					Nominatim::createLevel($wfNominatim["lat"], $wfNominatim["lon"], "WF", "1", array(), $wfNominatim);
					$zoneWF = Zone::getCountryByCountryCode("WF");
					
				}
				
			}


			// Ville Wallis et futumas : Sigave, Alo, Uvea
			$i = 0 ;
			if(!empty($zonePF) && !empty($zoneWF)){
				$cities = PHDB::find(City::COLLECTION, array( "depName" => "Polynesie" ) );
				$citiesWallisFutuma = array("Sigave", "Alo", "Uvea", "Ile-de-Clipperton");
				foreach ($cities as $keyCity => $city) {
					//Rest::json($city); exit;
					// $cityNominatim =  Nominatim::getCityByLatLon($city["geo"]["latitude"], $city["geo"]["longitude"]);
					if(!in_array($city["name"], $citiesWallisFutuma)){
						$nameCity = null;
						foreach ($city["postalCodes"] as $keyPC => $valPC) {
							if(empty($nameCity) && !empty($valPC['postalCode'])){
								$nameCity = $city["name"]."+".$valPC['postalCode'];
								break;
							}
						}

						if(!empty($nameCity)){
							$paramsCityNom = array(
								"text" => $nameCity,
								"extratags" => true,
								"geoShape" => true,
								"namedetails" => true,
								'state' => "Polynésie Française"
							);

							$cityNominatim = Nominatim::getAddressByNominatim($paramsCityNom);

							if(empty($cityNominatim)){
								$paramsCityNom["text"] = $city["name"];
								$cityNominatim = Nominatim::getAddressByNominatim($paramsCityNom);
							}
							//Rest::json($cityNominatim); exit;
							foreach ($cityNominatim as $keyCN => $valueCN) {
								if(!empty($valueCN["osm_id"]) && !empty($valueCN["address"]["state"]) && $valueCN["address"]["state"] === "Polynésie Française"){

									$set = array(
										"geo" => SIG::getFormatGeo($valueCN["lat"], $valueCN["lon"]),
										"geoPosition" => SIG::getFormatGeoPosition($valueCN["lat"], $valueCN["lon"]),
										"level1" => (String) $zonePF["_id"],
										"level1Name" => $zonePF["name"],
										"osmID" => $valueCN["osm_id"],
										"geoShape" => $valueCN["geojson"],
										"country" => "PF",
										"depName" => "",
										"regionName" => ""

									);
									$newCP = array();
									foreach ($city["postalCodes"] as $keyPC => $valPC) {
										$city["postalCodes"][$keyPC]["geo"] = $set["geo"];
										$city["postalCodes"][$keyPC]["geoPosition"] = $set["geoPosition"];
									}
									$set["postalCodes"] = $city["postalCodes"];
									PHDB::update(City::COLLECTION, 
										array("_id"=>new MongoId($keyCity)),
										array('$set' => $set )
									);
									$i++;
									//Rest::json($set); exit;

									break;
								}
							}
						}
					} else {
						$nameCity = null;
						if($city["name"] === "Uvea")
							$nameCity = "Île Uvéa";
						else
							$nameCity = $city["name"];

						if(!empty($nameCity)){
							$paramsCityNom = array(
								"text" => $nameCity,
								"extratags" => true,
								"geoShape" => true,
								"namedetails" => true
							);

							$cityNominatim = Nominatim::getAddressByNominatim($paramsCityNom);

							// if(empty($cityNominatim)){
							// 	$paramsCityNom["text"] = $city["name"];
							// 	$cityNominatim = Nominatim::getAddressByNominatim($paramsCityNom);
							// }
							//Rest::json($cityNominatim); exit;
							foreach ($cityNominatim as $keyCN => $valueCN) {
								if(!empty($valueCN["osm_id"]) && !empty($valueCN["address"]["archipelago"]) && $valueCN["address"]["archipelago"] === "Wallis-et-Futuna"){

									$set = array(
										"geo" => SIG::getFormatGeo($valueCN["lat"], $valueCN["lon"]),
										"geoPosition" => SIG::getFormatGeoPosition($valueCN["lat"], $valueCN["lon"]),
										"level1" => (String) $zoneWF["_id"],
										"level1Name" => $zoneWF["name"],
										"osmID" => $valueCN["osm_id"],
										"geoShape" => $valueCN["geojson"],
										"country" => "WF",
										"depName" => "",
										"regionName" => ""

									);
									$newCP = array();
									foreach ($city["postalCodes"] as $keyPC => $valPC) {
										$city["postalCodes"][$keyPC]["geo"] = $set["geo"];
										$city["postalCodes"][$keyPC]["geoPosition"] = $set["geoPosition"];
									}
									$set["postalCodes"] = $city["postalCodes"];
									PHDB::update(City::COLLECTION, 
										array("_id"=>new MongoId($keyCity)),
										array('$set' => $set )
									);
									$i++;
									//Rest::json($set); exit;

									break;
								}
							}
						}
					}
				}
			}

			return "il y a eu ".$i." cities modifier"; exit;
		}
	}

	public function actionRefactorPolynesieElt(){
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
			$zonePF = Zone::getCountryByCountryCode("PF");
			$zoneWF = Zone::getCountryByCountryCode("WF");


			$cities = PHDB::find(City::COLLECTION, array(
					'$or' => array(
						array('level1' => (String) $zoneWF["_id"]),
						array('level1' => (String) $zonePF["_id"])
					)
				), array("name", "alternateName", "country", "level1", "level1Name", "level2", "level2Name", "level3", "level3Name", "level4", "level4Name") );

			//Rest::json($cities);
			$idCities = array();
			foreach ($cities as $key => $value) {
				$idCities[] = $key;
			}
			$cols = array(Person::COLLECTION , Organization::COLLECTION, Project::COLLECTION, Event::COLLECTION, Poi::COLLECTION);

			foreach ($cols as $keycol => $col) {
				$elts = PHDB::find($col, array( "address.localityId" => array('$in' => $idCities ) , array("name", "address") ) );
				$i = 0 ;
				// Rest::json($elts); exit;
				// $str .="**************".$col."**************<br/>";
				// $str .= "il y a eu ".count($elts)." elts modifier<br/><br/>";
				if(!empty($elts)){
					foreach ($elts as $keyElt => $valueElt) {
						if(!empty($valueElt["address"])){
							unset($valueElt["address"]["level1"]);
							unset($valueElt["address"]["level1Name"]);
							unset($valueElt["address"]["level2"]);
							unset($valueElt["address"]["level2Name"]);
							unset($valueElt["address"]["level3"]);
							unset($valueElt["address"]["level3Name"]);
							unset($valueElt["address"]["level4"]);
							unset($valueElt["address"]["level4Name"]);
							$goodCity = $cities[$valueElt["address"]["localityId"]];
							$valueElt["address"]["localityId"] = (String)$goodCity["_id"] ;
							$valueElt["address"]["addressCountry"] = $goodCity["country"] ;
							if( !empty($goodCity["level1"]) ){
								$valueElt["address"]["level1"] = $goodCity["level1"];
								$valueElt["address"]["level1Name"] = (!empty($goodCity["level1Name"]) ? $goodCity["level1Name"] :  $goodCity["name"]);
							}

							if( !empty($goodCity["level2"]) ){
								$valueElt["address"]["level2"] = $goodCity["level2"];
								$valueElt["address"]["level2Name"] = (!empty($goodCity["level2Name"]) ? $goodCity["level2Name"] :  $goodCity["name"]);
							}

							if( !empty($goodCity["level3"]) ){
								$valueElt["address"]["level3"] = $goodCity["level3"];
								$valueElt["address"]["level3Name"] = (!empty($goodCity["level3Name"]) ? $goodCity["level3Name"] :  $goodCity["name"]);
							}

							if( !empty($goodCity["level4"]) ){
								$valueElt["address"]["level4"] = $goodCity["level4"];
								$valueElt["address"]["level4Name"] = (!empty($goodCity["level4Name"]) ? $goodCity["level4Name"] :  $goodCity["name"]);
							}

							$elts[$keyElt]["address"] = $valueElt["address"];

							PHDB::update($col, 
								array("_id"=>new MongoId($keyElt)),
								array('$set' => array('address' => $valueElt["address"]) )
							);

						}
					}
					//Rest::json($elts); exit;
				}
			}
			return "Mise à jour faite.";
		}
	}


	public function actionRefactorReunion(){
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
			$badReunion = PHDB::findOne(Zone::COLLECTION, array("name" => "La Réunion"));
			$goodReunion = PHDB::findOne(Zone::COLLECTION, array("name" => "Réunion"));
			$badCities = PHDB::find(City::COLLECTION, array( "level3" => (String)$badReunion["_id"] ), array("name", "alternateName" ) );
			$speciale = array(
				"Sainte-Clotilde" => "Saint-Denis",
				"Le Tampon" => "Tampon",
				"La Possession" => "Possession",
				"Les Avirons" => "Avirons" );

			$compareCity = array();
			$pascityCity = array();
			foreach ($badCities as $keyBC => $valueBC) {
				$nameCity = (!empty($speciale[$valueBC["name"]]) ? $speciale[$valueBC["name"]] : $valueBC["name"] );

				$citiesPotentiel = PHDB::findOne(City::COLLECTION, array( "name" => $nameCity, "country" => "RE" ), array("name", "alternateName", "country", "level1", "level1Name", "level2", "level2Name", "level3", "level3Name", "level4", "level4Name" ) );
				if(!empty($citiesPotentiel)){
					$compareCity[$keyBC] = $valueBC ;
					$compareCity[$keyBC]["compare"] = $citiesPotentiel ;
				}else{
					$pascityCity[$keyBC] = $valueBC ;
				}
				
			}

			$test = array(
				"pascityCity" => $pascityCity,
				"compareCity" => $compareCity
			);
			//Rest::json($test); exit;
			$cols = array(Person::COLLECTION , Organization::COLLECTION, Project::COLLECTION, Event::COLLECTION, Poi::COLLECTION);

			foreach ($cols as $keycol => $col) {
				$elts = PHDB::find($col, array( "address.level3" => (String)$badReunion["_id"] ), array("name", "address") );
				$i = 0 ;
				// $str .="**************".$col."**************<br/>";
				// $str .= "il y a ".count($elts)." elts <br/><br/>";
				if(!empty($elts)){
					foreach ($elts as $keyElt => $valueElt) {
						if(!empty($valueElt["address"])){
							unset($valueElt["address"]["level1"]);
							unset($valueElt["address"]["level1Name"]);
							unset($valueElt["address"]["level2"]);
							unset($valueElt["address"]["level2Name"]);
							unset($valueElt["address"]["level3"]);
							unset($valueElt["address"]["level3Name"]);
							unset($valueElt["address"]["level4"]);
							unset($valueElt["address"]["level4Name"]);


							$goodCity = $compareCity[$valueElt["address"]["localityId"]]["compare"];
							$valueElt["address"]["localityId"] = (String)$goodCity["_id"] ;
							$valueElt["address"]["addressCountry"] = $goodCity["country"] ;
							if( !empty($goodCity["level1"]) ){
								$valueElt["address"]["level1"] = $goodCity["level1"];
								$valueElt["address"]["level1Name"] = (!empty($goodCity["level1Name"]) ? $goodCity["level1Name"] :  $goodCity["name"]);
							}

							if( !empty($goodCity["level2"]) ){
								$valueElt["address"]["level2"] = $goodCity["level2"];
								$valueElt["address"]["level2Name"] = (!empty($goodCity["level2Name"]) ? $goodCity["level2Name"] :  $goodCity["name"]);
							}

							if( !empty($goodCity["level3"]) ){
								$valueElt["address"]["level3"] = $goodCity["level3"];
								$valueElt["address"]["level3Name"] = (!empty($goodCity["level3Name"]) ? $goodCity["level3Name"] :  $goodCity["name"]);
							}

							if( !empty($goodCity["level4"]) ){
								$valueElt["address"]["level4"] = $goodCity["level4"];
								$valueElt["address"]["level4Name"] = (!empty($goodCity["level4Name"]) ? $goodCity["level4Name"] :  $goodCity["name"]);
							}

							$elts[$keyElt]["address"] = $valueElt["address"];

							PHDB::update($col, 
								array("_id"=>new MongoId($keyElt)),
								array('$set' => array('address' => $valueElt["address"]) )
							);

						}
					}
					//Rest::json($elts); exit;
				}
			}
			return "Mise à jour faite.";
		}
	}

	public function actionRefactorSaintPierreMiquelon(){
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
			$zoneSPM = Zone::getCountryByCountryCode("PM");
			//Rest::json($zoneSPM);exit;
			if(empty($zoneSPM)){
				$paramsPF = array(
					"id" => "3406826",
					"type" => "R"
				);
				$spmNominatim = Nominatim::getElementByIdNominatim($paramsPF);
				if(!empty($spmNominatim["geometry"]) ){
					$spmNominatim["lat"] = $spmNominatim["geometry"]["coordinates"][1];
					$spmNominatim["lon"] = $spmNominatim["geometry"]["coordinates"][0];
					//Rest::json($spmNominatim); exit;

					Nominatim::createLevel($spmNominatim["lat"], $spmNominatim["lon"], "PM", "1", array(), $spmNominatim);
					$zoneSPM = Zone::getCountryByCountryCode("PM");
					
				}
				
			}

			if(!empty($zoneSPM)){

				$existsSaintPierre = PHDB::findOne(City::COLLECTION, array( "osmID" => 3404440 ) );
				//Rest::json($existsSaintPierre); exit;
				if(empty($existsSaintPierre)){
					$paramsSPM = array(
						"id" => "3404440",
						"type" => "R",
						"geoShape" => true
					);
					$saintPierreNominatim = Nominatim::getElementByIdNominatim($paramsSPM);

					$paramsCitySP = array(
						"countryCode" => "PM",
						"lat" => $saintPierreNominatim["centroid"]["coordinates"][1],
						"lon" => $saintPierreNominatim["centroid"]["coordinates"][0]
					);
					$saintPierreNominatim["lat"] = $saintPierreNominatim["centroid"]["coordinates"][1];
					$saintPierreNominatim["lon"] = $saintPierreNominatim["centroid"]["coordinates"][0];
					Nominatim::createCityByNominatim($paramsCitySP, Yii::app()->session["userId"], $saintPierreNominatim);
				}
					
				$existsMiquelon = PHDB::findOne(City::COLLECTION, array( "osmID" => 3404439 ) );
				
				if(empty($existsMiquelon)){
					$paramsML= array(
						"id" => "3404439",
						"type" => "R",
						"geoShape" => true
					);
					$miquelonNominatim = Nominatim::getElementByIdNominatim($paramsML);
					$paramsCityM = array(
						"countryCode" => "PM",
						"lat" => $miquelonNominatim["centroid"]["coordinates"][1],
						"lon" => $miquelonNominatim["centroid"]["coordinates"][0]
					);
					$miquelonNominatim["lat"] = $miquelonNominatim["centroid"]["coordinates"][1];
					$miquelonNominatim["lon"] = $miquelonNominatim["centroid"]["coordinates"][0];
					Nominatim::createCityByNominatim($paramsCityM, Yii::app()->session["userId"], $miquelonNominatim);
					$existsMiquelon = PHDB::findOne(City::COLLECTION, array( "osmID" => 3404439 ) );
				}

				$badMiquelon = PHDB::findOne(City::COLLECTION, array( "osmID" => array('$exists' => 0 ), "country" => "PM" ) );
				//Rest::json($badMiquelon); exit;

				$cols = array(Person::COLLECTION , Organization::COLLECTION, Project::COLLECTION, Event::COLLECTION, Poi::COLLECTION);

				foreach ($cols as $keycol => $col) {
					$elts = PHDB::find($col, array( "address.localityId" => (String)$badMiquelon["_id"] ), array("name", "address") );
					//Rest::json($elts); exit;
					$i = 0 ;
					// $str .="**************".$col."**************<br/>";
					// $str .= "il y a ".count($elts)." elts <br/><br/>";
					if(!empty($elts)){
						foreach ($elts as $keyElt => $valueElt) {
							if(!empty($valueElt["address"])){
								unset($valueElt["address"]["level1"]);
								unset($valueElt["address"]["level1Name"]);
								unset($valueElt["address"]["level2"]);
								unset($valueElt["address"]["level2Name"]);
								unset($valueElt["address"]["level3"]);
								unset($valueElt["address"]["level3Name"]);
								unset($valueElt["address"]["level4"]);
								unset($valueElt["address"]["level4Name"]);


								// $goodCity = $compareCity[$valueElt["address"]["localityId"]]["compare"];
								$valueElt["address"]["localityId"] = (String)$existsMiquelon["_id"] ;
								$valueElt["address"]["addressCountry"] = $existsMiquelon["country"] ;
								if( !empty($existsMiquelon["level1"]) ){
									$valueElt["address"]["level1"] = $existsMiquelon["level1"];
									$valueElt["address"]["level1Name"] = (!empty($existsMiquelon["level1Name"]) ? $existsMiquelon["level1Name"] :  $existsMiquelon["name"]);
								}

								if( !empty($existsMiquelon["level2"]) ){
									$valueElt["address"]["level2"] = $existsMiquelon["level2"];
									$valueElt["address"]["level2Name"] = (!empty($existsMiquelon["level2Name"]) ? $existsMiquelon["level2Name"] :  $existsMiquelon["name"]);
								}

								if( !empty($existsMiquelon["level3"]) ){
									$valueElt["address"]["level3"] = $existsMiquelon["level3"];
									$valueElt["address"]["level3Name"] = (!empty($existsMiquelon["level3Name"]) ? $existsMiquelon["level3Name"] :  $existsMiquelon["name"]);
								}

								if( !empty($existsMiquelon["level4"]) ){
									$valueElt["address"]["level4"] = $existsMiquelon["level4"];
									$valueElt["address"]["level4Name"] = (!empty($existsMiquelon["level4Name"]) ? $existsMiquelon["level4Name"] :  $existsMiquelon["name"]);
								}

								$elts[$keyElt]["address"] = $valueElt["address"];

								PHDB::update($col, 
									array("_id"=>new MongoId($keyElt)),
									array('$set' => array('address' => $valueElt["address"]) )
								);

							}
						}
						//Rest::json($elts); exit;
					}
				}

			}
			return "Mise à jour faite.";
		}
	}

	public function actionRemoveBadZonesAndCities(){
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
			$badReunion = PHDB::findOne(Zone::COLLECTION, array("name" => "La Réunion"));
			if(!empty($badReunion)){
				$badCities = PHDB::find(City::COLLECTION, array( "level3" => (String)$badReunion["_id"] ), array("name", "alternateName" ) );
				if(!empty($badCities)){
					foreach ($badCities as $keyBC => $valueBC) {
						PHDB::remove(City::COLLECTION, array("_id"=>new MongoId($keyBC)) );
					}
				}
				

				PHDB::remove(Zone::COLLECTION, array("_id"=>new MongoId( (String)$badReunion["_id"] )) );
			}

			$badMiquelon = PHDB::findOne(City::COLLECTION, array( "osmID" => array('$exists' => 0 ), "country" => "PM" ) );
			if(!empty($badMiquelon))
				PHDB::remove(City::COLLECTION, array("_id"=>new MongoId((String)$badMiquelon["_id"])) );

			return "Mise à jour faite.";
		}
	}

	public function actionRefactorCitiesPolynesieWF(){
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
			$zonePF = Zone::getCountryByCountryCode("PF");
			
			if(!empty($zonePF )){
				$citiesPF = PHDB::find(City::COLLECTION, array('level1' => (String) $zonePF["_id"]), array("name", 'translateId') );
				if(!empty($citiesPF )){
					foreach ($citiesPF as $keyPF => $valuePF) {
						PHDB::update(City::COLLECTION, 
							array("_id"=>new MongoId($keyPF)),
							array('$set' => array('country' => "PF") )
						);

						PHDB::update(Zone::TRANSLATE, 
							array("_id"=>new MongoId($valuePF['translateId'])),
							array('$set' => array('countryCode' => "PF") )
						);
					}
				}
			}

			

			$zoneWF = Zone::getCountryByCountryCode("WF");
			if(!empty($zoneWF )){
				$citiesWF = PHDB::find(City::COLLECTION, array('level1' => (String) $zoneWF["_id"]), array("name", "translateId") );
				if(!empty($citiesWF )){
					foreach ($citiesWF as $keyWF => $valueWF) {
						PHDB::update(City::COLLECTION, 
							array("_id"=>new MongoId($keyWF)),
							array('$set' => array('country' => "WF") )
						);

						PHDB::update(Zone::TRANSLATE, 
							array("_id"=>new MongoId($valueWF['translateId'])),
							array('$set' => array('countryCode' => "WF") )
						);
					}
				}
			}

			return "Mise à jour faite.";
		}
	}
	public function actionUpdateLinksMembersAndMemberOf(){
		$allOrga = PHDB::find(Organization::COLLECTION);
		$allCitoyens = PHDB::find(Person::COLLECTION);
		$count = 0;
		foreach ($allOrga as $key => $value) {
	/**** members to memberOf ****/
			if (isset($value["links"]["members"])) {
				foreach ($value["links"]["members"] as $k => $m) {

					$roles=(isset($m["roles"])) ? $m["roles"] : "";
            		$isAdmin=(@$m["isAdmin"])? $m["isAdmin"] : false; 
            		$pendingAdmin=(@$m[Link::IS_ADMIN_PENDING]) ? $m[Link::IS_ADMIN_PENDING] : false;
            		$isPending=(@$m[Link::TO_BE_VALIDATED]) ? $m[Link::TO_BE_VALIDATED] : false; 
            		$isInviting=false;
            		if(@$m[Link::IS_INVITING])
            			$isInviting=true;
            		if(isset($m[Link::IS_ADMIN_INVITING]))
            			$isInviting="admin";
            		Link::connect($k, $m["type"], $key , Organization::COLLECTION, Yii::app()->session["userId"], "memberOf",$isAdmin, $pendingAdmin,$isPending, $isInviting, $roles);
            		$count++;
				}
			}
	/**** memberOf to members ***/
			if(isset($value["links"]["memberOf"])) {
				foreach ($value["links"]["memberOf"] as $kO => $mO) {
					$roles=(isset($mO["roles"])) ? $mO["roles"] : "";
            		$isAdmin=(@$mO["isAdmin"])? $mO["isAdmin"] : false; 
            		$pendingAdmin=(@$mO[Link::IS_ADMIN_PENDING]) ? $vco[Link::IS_ADMIN_PENDING] : false;
            		$isPending=(@$mO[Link::TO_BE_VALIDATED]) ? $mO[Link::TO_BE_VALIDATED] : false; 
            		$isInviting=false;
            		if(@$mO[Link::IS_INVITING])
            			$isInviting=true;
            		if(isset($mO[Link::IS_ADMIN_INVITING]))
            			$isInviting="admin";
            		Link::connect($kO, $mO["type"], $key , Organization::COLLECTION, Yii::app()->session["userId"], "members",$isAdmin, $pendingAdmin,$isPending, $isInviting, $roles);
            		$count++;
				}

			}
		}

		foreach ($allCitoyens as $kCi => $vCi) {
			if(isset($vCi["links"]["memberOf"])) {
				foreach ($vCi["links"]["memberOf"] as $kcm => $vcm) {
					$roles=(isset($vcm["roles"])) ? $vcm["roles"] : "";
            		$isAdmin=(@$vcm["isAdmin"])? $vcm["isAdmin"] : false; 
            		$pendingAdmin=(@$vcm[Link::IS_ADMIN_PENDING]) ? $vcm[Link::IS_ADMIN_PENDING] : false;
            		$isPending=(@$vcm[Link::TO_BE_VALIDATED]) ? $vcm[Link::TO_BE_VALIDATED] : false; 
            		$isInviting=false;
            		if(@$vcm[Link::IS_INVITING])
            			$isInviting=true;
            		if(isset($vcm[Link::IS_ADMIN_INVITING]))
            			$isInviting="admin";
            		Link::connect($kcm, $vcm["type"], $kCi , Person::COLLECTION, Yii::app()->session["userId"], "members",$isAdmin, $pendingAdmin,$isPending, $isInviting, $roles);
            		$count++;
				}

			}
		}
		return "Mise à jour de " .$count ." donnée(s)" ;
	}
	public function actionUpdateLinksProjectsAndContributors(){
		$str = "";
		$allProjects = PHDB::find(Project::COLLECTION);
		$colElts = array(Person::COLLECTION, Organization::COLLECTION);
		$count = 0;
		# Contributors to Projects
		$str .= "Contributors to Projects<br/>";
		foreach ($allProjects as $key => $value) {
			$str .= ">>>>> PROJECT  - ".$key."<br/>";
			if (isset($value["links"]["contributors"])) {
				foreach ($value["links"]["contributors"] as $kco => $vco) {
					$roles=(isset($vco["roles"])) ? $vco["roles"] : "";
            		$isAdmin=(@$vco["isAdmin"])? $vco["isAdmin"] : false; 
            		$pendingAdmin=(@$vco[Link::IS_ADMIN_PENDING]) ? $vco[Link::IS_ADMIN_PENDING] : false;
            		$isPending=(@$vco[Link::TO_BE_VALIDATED]) ? $vco[Link::TO_BE_VALIDATED] : false; 
            		$isInviting=false;
            		if(@$vco[Link::IS_INVITING])
            			$isInviting=true;
            		if(isset($vco[Link::IS_ADMIN_INVITING]))
            			$isInviting="admin";

            		$str .= $vco["type"]." - ".$kco."<br/>";
					Link::connect($kco, $vco["type"], $key , Project::COLLECTION, Yii::app()->session["userId"], "projects",$isAdmin, $pendingAdmin,$isPending, $isInviting, $roles);
					$count++;
				}
			}
		}
	# Projects to Contributors
		$str .= "Projects to Contributors<br/>";
		foreach ($colElts as $ke => $ve) {
			$allElts = PHDB::find($ve);
			foreach ($allElts as $kOneElt => $vOneElt) {		
			$str .= ">>>>>".$ve." - ".$kOneElt."<br/>";		
				if (isset($vOneElt["links"]["projects"])) {
					foreach ($vOneElt["links"]["projects"] as $kp => $vProject) {
						$roles=(isset($vProject["roles"])) ? $vProject["roles"] : "";
						$isAdmin=(@$vProject["isAdmin"])? $vProject["isAdmin"] : false; 
						$pendingAdmin=(@$vProject[Link::IS_ADMIN_PENDING]) ? $vProject[Link::IS_ADMIN_PENDING] : false;
						$isPending=(@$vProject[Link::TO_BE_VALIDATED]) ? $vProject[Link::TO_BE_VALIDATED] : false; 
						$isInviting=false;
						if(@$vProject[Link::IS_INVITING])
							$isInviting=true;
						if(isset($vProject[Link::IS_ADMIN_INVITING]))
							$isInviting="admin";
						if($vOneElt["collection"] == Organization::COLLECTION) {
							$str .= $vProject["type"]." - ".$kp."<br/>";			
								Link::connect($kp,  $vProject["type"], $kOneElt , $ve, Yii::app()->session["userId"], "contributors",$isAdmin, $pendingAdmin,$isPending, $isInviting, $roles);
								$count++;
						}
					}

				}
			}

		}
		$str .= "Mise à jour de " .$count ." donnée(s)" ;
	return $str;
	}

	public function actionMigrateCssCodeToCssFile(){
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
			$str = "";
			$allOrga = PHDB::find(Organization::COLLECTION);
			$count = 0;
			foreach ($allOrga as $key => $value) {
				if(isset($value["costum"]["css"]["cssCode"])) {
					$params=array(
						"contextId"=>(String)$value["_id"], 
						"css"=>$value["costum"]["css"]["cssCode"], 
						"collection"=>"cssFile",
						"created"=>time(),
						"updated"=>time(),
						"migratedBy"=> Yii::app()->session["userId"]
					);
					
					$id = (String)$value["_id"] ;
					$cssFile = PHDB::find("cssFile", array("contextId"=>$id ));
					
					if(empty($cssFile)){
						Yii::app()->mongodb->selectCollection("cssFile")->insert($params);
						$count++;
					}
				}
			}

		}

		$str .= "Migration de " .$count ."cssCode vers cssFile" ;
		return $str;
	}

	public function actionAddPageToBlockChild(){
		$blockChildren = PHDB::find(Cms::COLLECTION, [
			"type" => "blockChild",
			"page" => [
				'$exists' => false
			],
			"blockParent" => [
				'$exists' => true
			]
		]);

		$getPageOfBlock = function($block) use (&$getPageOfBlock){
			if(isset($block["page"]))
				return $block["page"];

			if(isset($block["blockParent"]))
				return $getPageOfBlock(PHDB::findOne(Cms::COLLECTION, [
					"_id" => new MongoId($block["blockParent"])
				]));

			return null;

		};

		foreach($blockChildren as $childId => $child){
			$page = $getPageOfBlock($child);
			if(isset($page)){
				PHDB::update(Cms::COLLECTION, [
					"_id" => new MongoId($childId)
				], [
					'$set' => [
						"page" => $page
					]
				]);
			}
		}

		return "L'ajout de l'entrée 'page' pour les blocks est terminé.";
	}
	public function actionImportFormation(){
		$allFormation = json_decode(file_get_contents("../../modules/costum/json/smarterre/formation3.json"), true);
		foreach($allFormation as $kf => $vf){
			/** Ajout formation */
			$formation = [
				"collection" => "projects",
				"name" => $vf["formation"]["name"],
				"codeCARIF" => $vf["formation"]["codeCARIF"],
				"objectives" => $vf["formation"]["objectives"],
				"program" => $vf["formation"]["program"],
				"levels" => $vf["formation"]["levels"],
				"urlsDoc" => [$vf["formation"]["url"]],
				"created" => new MongoDate(time()),
				"category" => "formation",
				"preferences" => [
					"isOpenData" => true,
					"isOpenEdition" => true
				],
				"tags" => ["Formation"],
				"source" => [
					"insertOrign" => "import",
					"key" => "reunionEducation",
					"keys" => [ 
						"reunionEducation"
					]
				],
			];
			$res = Element::save($formation);

			/** Session */
			if(isset($vf["sessions"])){
				foreach($vf["sessions"] as $ks => $vs){
					$session = [
						"collection" => "events",
						"type" =>"course",
						"category" => "sessionFormation",
						"name" => $vs["sessionTitle"],
						"description" => $vs["sessionDetails"],
						"source" => [
							"insertOrign" => "import",
							"key" => "reunionEducation",
							"keys" => [ 
								"reunionEducation"
							]
						],
					];
					preg_match('/du (\d{2}\/\d{2}\/\d{4}) au (\d{2}\/\d{2}\/\d{4})/', $vs["sessionDate"], $matches);
					if (count($matches) === 3) {
						$startDate =  DateTime::createFromFormat('d/m/Y', $matches[1]);
    					$endDate = DateTime::createFromFormat('d/m/Y', $matches[2]);
						$session["startDate"] = $startDate->format('Y-m-d\TH:i:sP');
						$session["endDate"] = $endDate->format('Y-m-d\TH:i:sP');
					}
					$session["organizer"] =[
						"".$res["map"]["_id"] => [
							type => "projects",
							name => $res["map"]["name"]
						]
					];
					$session["links"] = [
						"organizer" => [
							"".$res["map"]["_id"] => [
								type => "projects"
							]
						]
					];
					if(isset($vs["sessionPlace"])){
						$city = PHDB::findOne("cities",array(
							"alternateName" => $vs["sessionPlace"],
							"country" => "RE",
						));
						if($city){
							$session["geo"] = $city["geo"];
							$session["geoPosition"] = $city["geoPosition"];
							$session["address"] = [
								"addressLocality" => $vs["sessionPlace"],
								"codeInsee" =>  $city["insee"],
								"@type" => "PostalAddress",
								"addressCountry" => "RE",
								"streetAddress" => "",
								"postalCode" => $city["postalCodes"][0]["postalCode"],
								"level1" => $city["level1"],
								"level1Name" => $city["level1Name"],
								"level3" => $city["level3"],
								"level3Name" => $city["level3Name"],
								"level4" => $city["level4"],
								"level4Name" => $city["level4Name"],
								"localityId" => (String)$city["_id"]

							];
						}
					}

					$resSession = Element::save($session);

					PHDB::update("events", array("_id" => new MongoId($resSession["map"]["_id"])), array('$set'=>array(
						"parent.".$res["map"]["_id"] => array(
							type => "projects",
							name => $res["map"]["name"]
						)
					)));
					PHDB::update("projects", array("_id" => new MongoId($res["map"]["_id"])), array('$set'=>array(
						"links.events.".$resSession["map"]["_id"] => array("type"=>"events")
					)));
				}
			}
		}
	}
	
	public function actionAddTagsMetiers() {
		$allMetiers = PHDB::find("metiers");
		$output = "";
		if (!empty($allMetiers)) {
			$output .= "<h1>".count($allMetiers)." metiers qui va être à jour</h1>";
			$i = 0;
			foreach($allMetiers as $key => $value) {
				$tags = [];
				if (isset($value["conditionsOfWork"])) {
					if (isset($value["conditionsOfWork"]["contractTypes"])) {
						$contactTypes = $value["conditionsOfWork"]["contractTypes"];
						foreach($contactTypes as $keyCType => $valueCType) {
							if (isset($valueCType["percentage"]) && isset($valueCType["type"])) {
								if ($valueCType["percentage"] > 0) {
									array_push($tags, $valueCType["type"]);
								}
							}
						}
					}
					if (isset($value["conditionsOfWork"]["workTime"])) {
						$workTimeType = $value["conditionsOfWork"]["workTime"];
						foreach($workTimeType as $keyWorkTime => $valueWorkTime) {
							if (isset($valueWorkTime["percentage"]) && isset($valueWorkTime["type"])) {
								if ($valueWorkTime["percentage"] > 0) {
									array_push($tags, $valueWorkTime["type"]);
								}
							}
						}
					}
				}
				$i++;
				PHDB::update("metiers", array("_id" => new MongoId($value["_id"])), array('$set' => array("tags" => $tags)));
			}
		}
		$output .= "<h1 style='color: blue'> => ".$i." metiers à jour </h1>";
		return $output;
	}
}