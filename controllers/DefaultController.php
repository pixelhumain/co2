<?php

namespace PixelHumain\PixelHumain\modules\co2\controllers;

use CommunecterController;
use Event;
use Organization;
use PHDB;
use PixelHumain\PixelHumain\components\ThemeHelper;
use Project;
use Yii;
use IpSpam;

/**
 * DefaultController.php
 *
 * OneScreenApp for Communecting people
 *
 * @author: Tibor Katelbach <tibor@pixelhumain.com>
 * Date: 14/03/2014
 */
class DefaultController extends CommunecterController {

    
    public function beforeAction($action)
  	{

      parent::initPage();
      
		  return parent::beforeAction($action);
  	}
    /**
     * Home page
     */

	public function actionIndex($src=null) 
	{
    	//Yii::app()->theme = $theme;   
      //Yii::app()->session["theme"] = $theme; 
      //Yii::app()->theme = "notragora";
      //Yii::app()->theme = "CO2";

      // http://127.0.0.1/ph/network?network=tierslieuxlille
      // http://127.0.0.1/ph/network/default/index/src/tierslieuxlille

		if(IpSpam::isBanned(@$_SERVER["REMOTE_ADDR"]))
			return $this->renderPartial("co2.views.default.unTpl",array("msg"=>"Problème d'Adresse IP Bloquée", "btn"=>"<a href='mailto:contact@communecter.org' target='_blank' style='font-size:18px;text-transform: uppercase;text-decoration:none;font-weight: bold; background-color: #9fbd38!important;padding: 4px;padding-top: 4px!important;border: 2px solid #9fbd38;border-radius: 4px;color: #fff;'>Contactez-Nous</a>","text"=> "Votre adresse IP a été identifiée comme source de spam et a été temporairement bloquée par notre système de sécurité. <br> Pour rétablir l'accès, veuillez contacter notre administrateur système qui vous guidera à travers le processus de vérification et de résolution.<br> Nous nous excusons pour tout inconvénient que cela pourrait causer et vous remercions de votre coopération pour maintenir un environnement en ligne sûr et fiable."));
      
	    if(@$_GET["network"] ){
	      $this->redirect(Yii::app()->createUrl("/network/default/index?src=".$_GET["network"]));
      }
      if( @$src ){
        ThemeHelper::setWebsiteTheme(ThemeHelper::NETWORK);
        Yii::app()->params['networkParams'] = $src;
        Yii::app()->session["theme"] = "network";
        Yii::app()->session["networkParams"] = $src;
        return $this->render("index");
      }
      else if(@Yii::app()->session["theme"] == "network" ){
        ThemeHelper::setWebsiteTheme(ThemeHelper::NETWORK);
        Yii::app()->params['networkParams'] = Yii::app()->session["networkParams"];
        return $this->render("index");
      }else{
//        $this->layout = "//layouts/mainSearch";
        $this->layout = "mainSearch";
       // return $this->render("index", array());
        if(Yii::app()->request->isAjaxRequest)
            return $this->renderPartial("../app/welcome", array(), true);
        else 
            return $this->render("../app/welcome" , array());
      }
	   
  }

  public function actionTwoStepRegister() 
  {
    $this->layout = "//layouts/mainSearch";
    return $this->renderPartial("two_step_register");
  }
  /*public function actionAgenda() 
  {
    $this->renderPartial("agenda");
  }*/

  /*public function actionLive($type=null) 
  {
    $stream = array();
    $now = array();
    if( !$type || $type == "dda" ){
      $stream = array_merge( $stream, ActionRoom::getAllRoomsActivityByTypeId( Person::COLLECTION, Yii::app()->session['userId'] ) );  
    }
    if( !$type || $type == Project::COLLECTION ){
      $stream = array_merge( $stream, Element::getActive( Project::COLLECTION ) );  
    }
    if( !$type || $type == Event::COLLECTION ){
      $stream = array_merge( $stream, Element::getActive( Event::COLLECTION ) );  
    }
    if( !$type || $type == Organization::COLLECTION ){
      $stream = array_merge( $stream, Element::getActive( Organization::COLLECTION ) );  
    }
    function mySort($a, $b){ 
          if( isset($a['updated']) && isset($b['updated']) ){
              return (strtolower(@$b['updated']) > strtolower(@$a['updated']));
          }else{
              return false;
          }
      }
      
      usort($stream,"mySort");
    $this->renderPartial("live", array( "stream"=>$stream,
                                        "now"=>$now,
                                        "type"=>$type ));
  }*/

  //public function actionNews() 
  //{
  //  $this->renderPartial("news");
  //}

  //public function actionDirectory() 
  //{

    //$this->renderPartial("directory");
  //}
	//public function actionDirectoryjs(){

		// if( Yii::app()->session["theme"] != "CO2" && 
		// 	!empty(Yii::app()->params["overWrite"]) && 
		// 	!empty(Yii::app()->params["overWrite"]["views"]["directoryjs"])){
		// 	$this->renderPartial(Yii::app()->session["theme"].".views.".Yii::app()->params["overWrite"]["views"]["directoryjs"]);
		// }else{
		//	$this->renderPartial("directoryjs");
		// }

		
	//}

  public function actionLang() 
  {
    return $this->render("index");
  }

  public function actionImg($n) 
  {
    return $this->module->getAssetsUrl()."/images/".$n;
  }

  /*public function actionHome() 
  {
    //$this->layout = "//layouts/mainSearch";

    //Get the last global statistics
    $stats = Stat::getWhere(array(),null,1);
    if(is_array($stats)) $stats = array_pop($stats);
    $tpl = "home";
    if(Yii::app()->theme != "ph-dori")
    	$tpl = "//layouts/default/home";

   // $tpl=(@$_GET["tpl"]) ? $_GET["tpl"]: "home";
    $this->renderPartial($tpl, array("stats"=>$stats));
  }*/
  public function actionApropos() 
  {
    //$this->layout = "//layouts/mainSearch";
    $tpl = "apropos";
    if(Yii::app()->theme != "ph-dori")
      $tpl = "//layouts/default/apropos";

   // $tpl=(@$_GET["tpl"]) ? $_GET["tpl"]: "home";
    return $this->renderPartial($tpl);
  }
  public function actionLogin() 
  {
    $this->layout = "//layouts/mainSearch";
    return $this->renderPartial("login");
  }
  public function actionRender($url){
    if(!empty($url)){
      $params=(isset($_POST)) ? $_POST : [];
      return $this->renderPartial($url, $params,true);
    }
  }
  public function actionView($page,$dir=null,$layout=null) 
  {
    if(@$dir){
      
      if( strpos($dir,"docs") !== false )
        $dir = "../".$dir;

      if(strpos($dir,"|")){
        $dir=str_replace("|", "/", $dir);
      }
      $page = $dir."/".$page;
	
    }
    
    if(Yii::app()->request->isAjaxRequest || $layout=="empty"){
      $this->layout = "//layouts/empty";
      return $this->renderPartial($page, null,true);
    }
    else {
      //$this->sidebar2 = Menu::$infoMenu;
      return $this->render($page);
    }
  }
  public function actionTrad()
  {
    return $this->renderPartial("co2.views.translation.trad");
  }
  
    public function actionSwitch($lang)
    {
        $this->layout = "//layouts/empty";
        Yii::app()->session["lang"] = $lang;
        $this->redirect(Yii::app()->createUrl("/".$this->module->id));
    }

    public function actionSitemap($sourceKey=null,$host=null)
    {

        $list = null;

        //dont reference private project to search engines
        $p = array(
                '$or' =>array( 
                     array( "preferences.private"=> array( '$exists' => true, '$in' => array( "false", false ) )),
                    array( "preferences.private"=>array( '$exists' => false))
                ) 
            ) ;


        //reduce search to sourceKey
        if( isset($sourceKey )){
            $list = array();
            $p["source.key"] = $sourceKey;          
            $list = array_merge(
                PHDB::find ( Organization::COLLECTION , $p , array("slug","updated") ),
                PHDB::find ( Project::COLLECTION , $p , array("slug","updated") ),
                PHDB::find ( Event::COLLECTION , $p , array("slug","updated") )
            );
        } else {
          //value only Organizations, Projects and Events
          $list = array_merge(
              PHDB::find ( Organization::COLLECTION , $p , array("slug","updated") ),
              PHDB::find ( Project::COLLECTION , $p , array("slug","updated") ),
              PHDB::find ( Event::COLLECTION , $p , array("slug","updated") )
          );
        }

        
        
        //var_dump($list);
        return $this->renderPartial("sitemap",array(
            "list"=>$list,
            "host"=>(isset($host)) ? $host : "www.communecter.org",
            ));
    }

	public function actionRemoveData($type=null){
		$this->layout = "//layouts/empty";

		$list = PHDB::find( "slugs" , array(), array("name", "slug") );

		return $this->render("sitemap",array("list"=>$list));
	}
}