<?php

namespace PixelHumain\PixelHumain\modules\co2\controllers;

use CommunecterController;

/**
 * ElementController.php
 *
 * @author: Tibor Katelbach <tibor@pixelhumain.com>
 * Date: 15/08/13
 */
class ElementController extends CommunecterController {
    const moduleTitle = "Element";
    
  public function beforeAction($action) {
    parent::initPage();
    return parent::beforeAction($action);
  }
  public function actions()
  {
      return array(
      'updatepathvalue'       =>     \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\element\UpdatePathValuedAction::class,
          'unsetpath'             => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\element\UnsetPath::class,
          'updatefield' 				  => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\element\UpdateFieldAction::class,
          'updatefields' 				  => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\element\UpdateFieldsAction::class,
          'updateblock'           => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\element\UpdateBlockAction::class,
          'updatesettings'        => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\element\UpdateSettingsAction::class,
          'updatestatus'          => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\element\UpdateStatusAction::class,
          'detail'                => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\element\DetailAction::class,
          'newhome'                => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\element\NewHomeAction::class,
          'getalllinks'           => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\element\GetAllLinksAction::class,
          'getroleslist'           => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\element\GetRolesListAction::class,
          'geturls'               => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\element\GetUrlsAction::class,
          'getcuriculum'          => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\element\GetCuriculumAction::class,
          'getcontacts'           => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\element\GetContactsAction::class,
          'directory'             => 'citizenToolKit.controllers.element.DirectoryAction',
          'addmembers'            => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\element\AddMembersAction::class,
          'aroundme'              => 'citizenToolKit.controllers.element.AroundMeAction',
          'save'                  => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\element\SaveAction::class,
          'savecontact'           => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\element\SaveContactAction::class,
          'saveurl'               => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\element\SaveUrlAction::class,
          'delete'                => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\element\DeleteAction::class,
          'get'                   => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\element\GetAction::class,
          'getcontext'                   => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\element\GetContextAction::class,
          'notifications'         => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\element\NotificationsAction::class,
          'about'                 => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\element\AboutAction::class,
          'getinvitedme'                 => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\element\GetInvitedMeAction::class,
          'getdatadetail'         => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\element\GetDataDetailAction::class,
          'stopdelete'            => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\element\StopDeleteAction::class,
          'getthumbpath'          => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\element\GetThumbPathAction::class,
          'getcommunexion'        => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\element\GetCommunexionAction::class,
          'getdatabyurl'          => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\element\GetDataByUrlAction::class,
          'network'               => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\element\NetworkAction::class,
          'getnetworks'           => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\element\GetNetworksAction::class,
          'invoice'               => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\element\InvoiceAction::class,
          'invite'                => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\element\InviteAction::class,
          'answerinvite'          => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\element\AnswerInviteAction::class,
          'list'                  => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\element\ListAction::class,
          'askdata'               => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\element\AskDataAction::class,
          'remove'                => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\element\RemoveAction::class,
          'deletedata'            => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\element\DeleteDataAction::class,
          'getlastevents'         => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\element\GetLastEventsAction::class,
          'getexternalnetwork'    => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\element\GetExternalNetwork::class,
          'mergedatadoublon'      => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\element\MergeDataAction::class,
          'getdoublon'            => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\element\GetDataDoublonAction::class,
          'existselement'         => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\element\ExistsElementAction::class,
          "select2list"           => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\element\Select2ListAction::class,
          "join_element"          => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\element\JoinElementAction::class,
      );
  }
}