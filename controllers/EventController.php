<?php

namespace PixelHumain\PixelHumain\modules\co2\controllers;

use CommunecterController;

/**
 * EventController.php
 *
 * tous ce que propose le PH en terme de gestion d'evennement
 *
 * @author: Tibor Katelbach <tibor@pixelhumain.com>
 * Date: 15/08/13
 */
class EventController extends CommunecterController {
	const moduleTitle = "Évènement";

	public function beforeAction($action) {
		parent::initPage();
		return parent::beforeAction($action);
	}
	public function actions() {
		return array(
			"attendees"	=>	\PixelHumain\PixelHumain\modules\citizenToolKit\controllers\event\AttendeesAction::class,
			// 'getcalendar'   				=> 'citizenToolKit.controllers.event.GetCalendarAction',
			//'calendarview'				  => 'citizenToolKit.controllers.event.CalendarViewAction',
		);
	}
}
