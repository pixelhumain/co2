<?php

namespace PixelHumain\PixelHumain\modules\co2\controllers;

use CommunecterController;
class ExportController extends CommunecterController {

	public function beforeAction($action) {
		parent::initPage();
		return parent::beforeAction($action);
	}
	public function actions() {
		return array(
			'index'	=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\export\IndexAction::class,
			'pdfelement'	=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\export\PdfElementAction::class,
			'csvelement'	=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\export\CsvElementAction::class,
			'csv'	=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\export\CsvAction::class,
		);
	}
}
