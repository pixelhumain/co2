<?php

namespace PixelHumain\PixelHumain\modules\co2\controllers;
use CommunecterController;

/**
 * FolderController.php
 *
 * @author: Bouboule <clement.damiens@gmail.com>
 * Date: 7/29/15
 * Time: 12:25 AM
 */
class FolderController extends CommunecterController {
  

	public function beforeAction($action) {
	    parent::initPage();
	    return parent::beforeAction($action);
	}

	public function actions()
	{
	    return array(
	        'list'    => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\folder\ListAction::class,
	        'crud'    => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\folder\CrudAction::class,
	    );
	}
}