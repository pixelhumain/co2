<?php

namespace PixelHumain\PixelHumain\modules\co2\controllers;

use CommunecterController;

/**
 * FundingController.php
 *
 * @author: oceatoon@gmail.com
 * Date: 25/7/15
 * Time: 11:25 PM
 */
class FundingController extends CommunecterController {
  

	public function beforeAction($action) {
	    return parent::beforeAction($action);
	}

	public function actions()
	{
	    return array(
	        'index' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\funding\RequestAction::class,
	    );
	}
}