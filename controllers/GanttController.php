<?php

namespace PixelHumain\PixelHumain\modules\co2\controllers;

use CommunecterController;

/**
 * DiscussController.php
 *
 *
 * @author: Sylvain Barbot
 * Date: 24/06/2015
 */
class GanttController extends CommunecterController {

    public function beforeAction($action) {
    	parent::initPage();
    	return parent::beforeAction($action);
  	}
  	public function actions()
	{
	    return array(
	        'index'       		=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\gantt\IndexAction::class,
			'savetask'  		=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\gantt\SaveTaskAction::class,
			'removetask'   		=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\gantt\RemoveTaskAction::class,
			'generatetimeline'  => 'citizenToolKit.controllers.gantt.GenerateTimelineAction',
			'addtimesheetsv'	=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\gantt\AddTimesheetSvAction::class,
	    );
	}
}