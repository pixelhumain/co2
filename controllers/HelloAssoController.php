<?php

namespace PixelHumain\PixelHumain\modules\co2\controllers;

use CommunecterController;

class HelloAssoController extends CommunecterController {
	public function beforeAction($action) {
		return parent::beforeAction($action);
	}
	public function actions() {
		return ([
			"campagne-tl"	=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\helloasso\CampagneTlAction::class,
			"error"			  => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\helloasso\ErrorAction::class,
			"return"		  => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\helloasso\ReturnAction::class,
		]);
	}
}
