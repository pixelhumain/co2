<?php

namespace PixelHumain\PixelHumain\modules\co2\controllers;

use CommunecterController;

class InteroperabilityController extends CommunecterController {
  public function beforeAction($action) {
    return parent::beforeAction($action);
  }

  public function actions()
  {
    return array(
      'index'       => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\interoperability\IndexAction::class,
      'wiki'      	=> 'citizenToolKit.controllers.interoperability.WikiAction',
      'datagouv'        => 'citizenToolKit.controllers.interoperability.DatagouvAction',
      'osm'  => 'citizenToolKit.controllers.interoperability.OsmAction',
      'osm-contribution'  => 'citizenToolKit.controllers.interoperability.OsmContribution',
      'ods'     => 'citizenToolKit.controllers.interoperability.OdsAction',
      'get' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\interoperability\GetAction::class,
      'copedia' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\interoperability\CopediaAction::class,
      'co-osm' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\interoperability\COSMAction::class,
      'co-osm-getnode' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\interoperability\OSMGetNodeAction::class,
      'co-osm-push-tag' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\interoperability\OSMPushTagAction::class,
      'wikitoco' => 'citizenToolKit.controllers.interoperability.WikiToCoAction',
      'pushtypewikidata' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\interoperability\PushTypeWikidataAction::class,
      'wikidata-put-description' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\interoperability\WikidataPutDescriptionAction::class,
      'wikidata-put-claim' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\interoperability\WikidataPutClaimAction::class,
      'proposeopendatasource' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\interoperability\ProposeOpenDataSourceAction::class,
      'validateproposeinterop' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\interoperability\ValidateProposeInteropAction::class,
      'rejectproposeinterop' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\interoperability\RejectProposeInteropAction::class,
    );
  }
}