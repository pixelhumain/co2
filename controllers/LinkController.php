<?php

namespace PixelHumain\PixelHumain\modules\co2\controllers;

use CommunecterController;

/**
 * LinkController.php
 *
 * Manage Links between Organization, Person, Projet and Event
 *
 * @author: Sylvain Barbot <sylvain@pixelhumain.com>
 * Date: 05/05/2015
 */
class LinkController extends CommunecterController {

	public function beforeAction($action) {
		parent::initPage();
		return parent::beforeAction($action);
	}
	public function actions()
	{
	    return array(
	        'removemember'     	=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\link\RemoveMemberAction::class,
	        "removerole"		=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\link\RemoveRoleAction::class,
			'removecontributor' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\link\RemoveContributorAction::class,
			'disconnect' 		=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\link\DisconnectAction::class,

			'linkchildparent' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\link\LinkChildParentAction::class,
			
			//New Actions
			'connect' 			=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\link\ConnectAction::class,
			'loadcontextdatalinks' 			=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\link\LoadContextDataLinksAction::class,
			'multiconnect' 		=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\link\MultiConnectAction::class,
			'follow' 			=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\link\FollowAction::class,
			'validate' 			=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\link\ValidateAction::class,
			'verify'    		=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\link\VerifyAttendeeAction::class,
			'favorite' 			=> 'citizenToolKit.controllers.link.FavoriteAction',
			'updateadminlink'	=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\link\UpdateAdminLinkAction::class,
			'validateinvitationbymail'	=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\link\ValidateInvitationByMailAction::class,
			'createinvitationlink' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\link\CreateInvitationLinkAction::class,
            'linksekitia' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\link\EkitiaLinkAction::class
	    );
	}
}