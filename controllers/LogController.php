<?php

namespace PixelHumain\PixelHumain\modules\co2\controllers;

use CommunecterController;
use Yii;

/**
 * LogController.php
 *
 * @author: Childéric THOREAU <childericthoreau@gmail.com>
 * Date: 7/29/15
 * Time: 12:25 AM
 */
class LogController extends CommunecterController {

	public function actions()
	{
	    return array(
	        'monitoring'    	 	 => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\log\MonitoringAction::class,
	        //'cleanup'    			 => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\log\CleanUpAction::class,
	    );
	}

	public function actionDbaccess() 
	{
    	return Yii::app()->session["dbAccess"];
  	}
  	public function actionClear() 
	{
    	Yii::app()->session["dbAccess"] = 0;
    	return Yii::app()->session["dbAccess"];
  	}
}