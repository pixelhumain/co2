<?php

namespace PixelHumain\PixelHumain\modules\co2\controllers;

use CommunecterController;
use PixelHumain\PixelHumain\modules\co2\components\CommunecterException;

/**
 * ActionController.php
 *
 * @author: Sylvain Barbot <sylvain.barbot@gmail.com>
 * Date: 7/29/15
 * Time: 12:25 AM
 */
class MailmanagementController extends CommunecterController {
  

	public function beforeAction($action) {
	    //Check hook come from mailgun
	    $mailgunCheck = true;
	    if (! $mailgunCheck) {
	    	//TODO SBAR : add notification for SuperAdmin

	    	throw new CommunecterException("It seems that the hook has been launch by someone else than mailgun");
	    }
	    return parent::beforeAction($action);
	}

	

	public function actions()
	{
	    return array(
	        'updatetopending'    => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\mailmanagement\UpdateToPendingAction::class,
	        'droppedmail'    => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\mailmanagement\DroppedMailAction::class,
	        'createandsend'    => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\mailmanagement\CreateAndSendAction::class,
	        'mailfeedback'    => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\mailmanagement\MailFeedBackAction::class,
	        'removedata'    => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\mailmanagement\RemoveDataAction::class,
	        'askdata'    => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\mailmanagement\AskDataAction::class,
	        'getdata'    => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\mailmanagement\GetDataAction::class,
	        'relaunchinvitation'    => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\mailmanagement\RelaunchInvitationAction::class,
	        'schedule'    => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\mailmanagement\ScheduleAction::class
	    );
	}
}