<?php

namespace PixelHumain\PixelHumain\modules\co2\controllers;

use CController;

/**
 * MigrationController.php
 *
 * @author: Sylvain Barbot <sylvain.barbot@gmail.com>
 * Date: 25/03/2016
 * Time: 16:14 
 */
class MigrationController extends CController {
  

	public function beforeAction($action) {
	    return parent::beforeAction($action);
	}

	public function actions()
	{
	    return array(
	        'citiespostalcodes'       			=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\migration\CitiesPostalCodesAction::class,
	    );
	}
}