<?php

namespace PixelHumain\PixelHumain\modules\co2\controllers;

use CommunecterController;

/**
 * DefaultController.php
 *
 * OneScreenApp for Communecting people
 *
 * @author: Tibor Katelbach <tibor@pixelhumain.com>
 * Date: 14/03/2014
 */
class NetworkController extends CommunecterController {

    
    public function beforeAction($action){
    	parent::initPage();
    	return parent::beforeAction($action);
    }

    public function actions(){
	    return array(
	        'get'     => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\network\GetAction::class,
	    );
	}

	public function actionSimplyDirectory(){
		//$params = self::getParams(@$_GET['params']);
		$this->layout = "//layouts/mainSearch";
		return $this->render("simplyDirectory");
	}
}
 ?>