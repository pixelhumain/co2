<?php

namespace PixelHumain\PixelHumain\modules\co2\controllers;

use CommunecterController;
use Document;
use Exception;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\NewsLetter;
use Rest;

class NewsletterController extends CommunecterController {

    public function actions(){
	    return [];
	}

    public function actionSaveTemplate(){
        try{    
            NewsLetter::saveTemplate(
                $_SESSION["userId"],
                $_POST["context"]["type"],
                $_POST["context"]["id"],
                $_POST["template"]
            );
        }catch(Exception $e){
            return Rest::json(["error"=>true, "message" => $e->getMessage()]);
        }
    }

    public function actionGetTemplate(){
        return Rest::json(NewsLetter::getTemplate());
    }

    public function actionUpdateTemplate($id){
        try{
            NewsLetter::updateTemplate($_SESSION["userId"], $id, $_POST["template"]);
        }catch(Exception $e){
            return Rest::json(["error"=>true, "message" => $e->getMessage()]);
        }
    }

    public function actionDeleteTemplate($id){
        try{
            NewsLetter::deleteTemplate($_SESSION["userId"], $id);
        }catch(Exception $e){
            return Rest::json(["error"=>true, "message" => $e->getMessage()]);
        }
    }
}
 ?>