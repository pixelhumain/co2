<?php

namespace PixelHumain\PixelHumain\modules\co2\controllers;

use CommunecterController;

/**
 * NotificationController.php
 *
 * @author: Tibor Katelbach <oceatoon@gmail.com>
 * Date: 8/09/15
 * Time: 10:00 AM
 */
class NotificationController extends CommunecterController {
  

	public function beforeAction($action) {
	    parent::initPage();
	    return parent::beforeAction($action);
	}

	public function actions()
	{
	    return array(
	        'getnotifications'    => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\notification\GetAction::class,
	        'marknotificationasread'    => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\notification\RemoveAction::class,
	        'removeall'    => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\notification\RemoveAllAction::class,
	        'update'    => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\notification\UpdateAction::class,
	    );
	}
}