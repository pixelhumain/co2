<?php


namespace PixelHumain\PixelHumain\modules\co2\controllers;

use CacheHelper;
use CommunecterController;
use DataValidator;
use MongoId;
use PaymentMethod;
use Rest;

class PaymentMethodController extends CommunecterController {
    public function actionSave(){
        $data = DataValidator::clearUserInput($_POST);
        $data = PaymentMethod::save($data);
        return Rest::json($data);
    }
    public function actionDelete(){
        $data = DataValidator::clearUserInput($_POST);
        if(isset($data["id"])){
            $result = PaymentMethod::removeById($data["id"]);
            return Rest::json(["result" => true, "msg" => "Le moyen de paiement a bien été supprimé", "data" => $data]);
        } else {
            return Rest::json(["result" => false, "msg" => "L'identifiant du moyen de paiement est obligatoire"]);
        }
    }
    public function actionGetpayments(){
        $data = DataValidator::clearUserInput($_POST);
        $costum = CacheHelper::getCostum(null, null, $data["costumSlug"]);
        $payments = PaymentMethod::getByElement(["id" => $costum["contextId"], "type" => $costum["contextType"]]);
        return Rest::json($payments);
    }
}