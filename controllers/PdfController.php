<?php

namespace PixelHumain\PixelHumain\modules\co2\controllers;
use CommunecterController;

/**
 * PdfController.php
 *
 * @author: Raphael R
 * Date: 09/2017
 */
class PdfController extends CommunecterController {
  

	public function beforeAction($action) {
	    parent::initPage();
	    return parent::beforeAction($action);
	}

	public function actions()
	{
	    return array(
	        'create'     		=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\pdf\CreateAction::class,
	    );
	}    
}