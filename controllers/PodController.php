<?php

namespace PixelHumain\PixelHumain\modules\co2\controllers;

use CommunecterController;

    /**
 * PodController.php
 *
 */

class PodController extends CommunecterController {

	public function beforeAction($action) {
		parent::initPage();
		return parent::beforeAction($action);
	}

	public function actions()
	{
	    return array(
	        'photovideo'     	=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\pod\PhotoVideoAction::class,
	        'fileupload'     	=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\pod\FileUploadAction::class,
	        'activitylist'     	=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\pod\ActivityListAction::class,
	        //'preferences'     	=> 'citizenToolKit.controllers.pod.preferencesAction',
	    );
	}
	public function actionCircuit(){
    	return $this->renderPartial("circuit", array(), true);
 	}
 	public function actionSliderMedia(){
    	return $this->renderPartial("sliderMedia", array("images"=>@$_POST["images"], "medias"=>@$_POST["medias"]), true);
 	}
 	public function actionPreferences(){
    	return $this->renderPartial("../pod/preferences" , array() );
	}
}
?>