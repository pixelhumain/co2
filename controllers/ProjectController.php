<?php


namespace PixelHumain\PixelHumain\modules\co2\controllers;

use CommunecterController;

/**
 * ActionLocaleController.php
 *
 * tous ce que propose le PH pour les associations
 * comment agir localeement
 *
 * @author: Tibor Katelbach <tibor@pixelhumain.com>
 * Date: 15/08/13
 */
class ProjectController extends CommunecterController {
  	const moduleTitle = "Projet";
  	public function beforeAction($action)
  	{
		parent::initPage();
		return parent::beforeAction($action);
  	}
  	public function actionIndex() 
    {
	    return $this->render("index");
	}
	public function actions()
	{
		return array(
		// captcha action renders the CAPTCHA image displayed on the contact page
		'captcha'=>array(
		 	'class'=>'CCaptchaAction',
		 	'backColor'=>0xFFFFFF,
		),
		'get'   					=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\project\GetAction::class
		

		);
	}	

}