<?php

namespace PixelHumain\PixelHumain\modules\co2\controllers;

use CommunecterController;

/**
 * DefaultController.php
 *
 * OneScreenApp for Communecting people
 *
 * @author: Tibor Katelbach <tibor@pixelhumain.com>
 * Date: 14/03/2014
 */
class SearchController extends CommunecterController {

  public function beforeAction($action) {
      return parent::beforeAction($action);
  }

	public function actions(){
		return array(
			'globalautocomplete'      	=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\search\GlobalAutoCompleteAction::class,
			'agenda'      	=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\search\AgendaAction::class,
			'globalautocompleteadmin'      	=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\search\GlobalAutoCompleteAdminAction::class,
			'simplyautocomplete'        => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\search\SimplyAutoCompleteAction::class,
			'searchmemberautocomplete'  => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\search\SearchMembersAutoCompleteAction::class,
			'getshortdetailsentity'     => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\search\GetShortDetailsEntityAction::class,
			'index'                     => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\search\IndexAction::class,
			'mainmap'                   => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\search\MainMapAction::class,
			'geteventsforcalendar'      => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\search\GetEventsForCalendarAction::class,
			'searchnextevent'      => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\search\SearchNextEventAction::class,
			'address'      => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\search\AddressAction::class,
			'getzone'      => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\search\GetZoneAction::class,
			'searchbadgefinder' 				=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\search\SearchBadgeFinderAction::class,
		);
	}
}