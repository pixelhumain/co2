<?php
    
    namespace PixelHumain\PixelHumain\modules\co2\controllers;

use Answer;
use Authorisation;
use CommunecterController;
use Form;
use MongoId;
use PHDB;
use Yii;

    class SetAnswersVoteCountController extends CommunecterController {
        
        public function beforeAction($action) {
            return parent::beforeAction($action);
        }

        /**
         * http://communecter74-dev/co2/setAnswersVoteCount/SetAllCount
         */
        public function actionSetAllCount() {
            if (!Authorisation::isUserSuperAdmin(Yii::app()->session["userId"])) {
                echo "<h1 style='color:red;text-align:center'>SUPER ADMIN SEULEMENT !!</h1>";
                return;
            }   
            $this->setAllCount();
        }

        private function setAllCount() {
            $answers = PHDB::find(
                Answer::COLLECTION, 
                ["vote" => ['$exists' => true]], 
                ["vote"]
            );
            $count = 0;
            foreach ($answers as $ansKey => $ansValue) {
                $loveCount = 0;
                foreach ($ansValue["vote"] as $key => $value) {
                    if($value["status"] == "love") {
                        $loveCount++;
                    }
                }
                if (PHDB::update(Answer::COLLECTION, ["_id" => new MongoId($ansKey)], [
                    '$set' => [
                        "allVoteCount.love" => $loveCount
                    ]
                ])) {
                    $count++;
                }
            }
            echo "<h2 style='color:green;'> OK - $count answers updated (vote count)</h2>";
        }
        
    }