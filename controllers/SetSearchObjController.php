<?php
    
    namespace PixelHumain\PixelHumain\modules\co2\controllers;

use Answer;
use Authorisation;
use Cms;
use CommunecterController;
use Form;
use MongoId;
use PHDB;
use Yii;

    class SetSearchObjController extends CommunecterController {
        
        public function beforeAction($action) {
            return parent::beforeAction($action);
        }

        /**
         * http://communecter74-dev/co2/setSearchObj/DefConfig/id
         */
        public function actionDefConfig($id) {
            if (!Authorisation::isUserSuperAdmin(Yii::app()->session["userId"])) {
                echo "<h1 style='color:red;text-align:center'>SUPER ADMIN SEULEMENT !!</h1>";
                return;
            }   
            $this->setDefaultConfig($id);
        }

        private function setDefaultConfig($id) {
            $searchObjBlock = PHDB::findOneById(Cms::COLLECTION, $id, ["parent"]);
            $form = PHDB::findOne(Form::COLLECTION, ["parent." . (!empty($searchObjBlock["parent"]) ? array_keys($searchObjBlock["parent"])[0] : "null") => ['$exists' => true], "type" => "aap"], ["name", "config"]);
            PHDB::update(Cms::COLLECTION, ["_id" => new MongoId($id)], [
                '$set' => [
                    "query" => [
                        "formId" => (!empty($form["_id"]) ? (string) $form["_id"] : null),
                        "indexStep" => 10,
                        "types" => [Answer::COLLECTION]
                    ], 
                    "moreQueryConfig.url" => "aapProposalDir",
                    "displayConfig.designType" => "communDesign",
                    "header" => [
                        "left" => [
                            "count" => true
                        ],
                        "right" => [
                            "map" => false,
                            "add" => false,
                        ]
                    ],
                    "filters" => [
                        "filterDesign" => [
                            "displayType" => "column"
                        ],
                        "filtersPersonal" => [
                            "filterPersonal" => [
                                [
                                    "label" => Yii::t("common", "Search by name"),
                                    "field" => "answers.aapStep1.titre",
                                    "view" => "text",
                                    "events" => "text",
                                    "options" => '{"icon" : "fa fa-search", "customClass" : "set-width to-sticky to-set-value visible-xs","inputAlias" : ".filter-alias .main-search-bar-addon"}',
                                ],
                                [
                                    "label" => "theme",
                                    "field" => "answers.aapStep1.tags",
                                    "view" => "megaMenuAccordion",
                                    "type" => "filters",
                                    "events" => "filters",
                                    "options" => '{"remove0":false,"activateCounter":true,"countFieldPath":"answers.aapStep1.tags","classDom":"scrollable-row","classList":"label-test-to-ellipsis","name":"'. (Yii::t('common', 'search by theme')) .'"}',
                                    "lists" => "dynamic",
                                    "dynamic" => "dynamicContent.6438366673d20a0de1533c77.tags.list"
                                ],
                                [
                                    "label" => Yii::t("common", "Retained") . " " . Yii::t("common", "or") . " " . Yii::t("common", "Rejected"),
                                    "field" => "acceptation",
                                    "view" => "accordionList",
                                    "type" => "filters",
                                    "events" => "filters",
                                    "options" => '{"keyValue":false,"classDom":"scrollable-row","classList":"label-test-to-ellipsis","name":"'. (Yii::t("common", "Retained") . " " . Yii::t("common", "or") . " " . Yii::t("common", "Rejected")) .'"}',
                                    "lists" => "keyValue",
                                    "listObject" => [
                                        [
                                            "objectkey" => "retained",
                                            "objectvalue" => Yii::t("common", "Retained")
                                        ],
                                        [
                                            "objectkey" => "rejected",
                                            "objectvalue" => Yii::t("common", "Rejected")
                                        ],
                                        [
                                            "objectkey" => "pending",
                                            "objectvalue" => Yii::t("common", "Pending")
                                        ],
                                    ] 
                                ],
                                [
                                    "label" => Yii::t("common", "Not seen") . " " . Yii::t("common", "or") . " " . Yii::t("common", "Seen"),
                                    "field" => "views",
                                    "view" => "accordionList",
                                    "type" => "filters",
                                    "events" => "filters",
                                    "options" => '{"keyValue":false,"classDom":"scrollable-row","classList":"label-test-to-ellipsis","name":"'. (Yii::t("common", "Not seen") . " " . Yii::t("common", "or") . " " . Yii::t("common", "Seen")) .'"}',
                                    "lists" => "keyValue",
                                    "listObject" => [
                                        [
                                            "objectkey" => "notSeen",
                                            "objectvalue" => Yii::t("common", "Not seen")
                                        ],
                                        [
                                            "objectkey" => "seen",
                                            "objectvalue" => Yii::t("common", "Seen")
                                        ]
                                    ] 
                                ],
                                [
                                    "label" => "Urgent",
                                    "field" => "answers.aapStep1.urgency",
                                    "view" => "accordionList",
                                    "type" => "filters",
                                    "events" => "filters",
                                    "options" => '{"classDom":"scrollable-row","classList":"label-test-to-ellipsis","name":"Urgent"}',
                                    "lists" => "keyValue",
                                    "listObject" => [
                                        [
                                            "objectkey" => "Urgent",
                                            "objectvalue" => "Urgent"
                                        ]
                                    ] 
                                ],
                                [
                                    "label" => Yii::t("common", "Status"),
                                    "field" => "status",
                                    "view" => "accordionList",
                                    "type" => "filters",
                                    "events" => "filters",
                                    "options" => '{"keyValue":false,"classDom":"scrollable-row","classList":"label-test-to-ellipsis","name":"'. (Yii::t("common", "Status")) .'"}',
                                    "lists" => "keyValue",
                                    "listObject" => [
                                        [
                                            "objectkey" => "progress",
                                            "objectvalue" => "En cours"
                                        ],
                                        [
                                            "objectkey" => "vote",
                                            "objectvalue" => "En evaluation"
                                        ],
                                        [
                                            "objectkey" => "funded",
                                            "objectvalue" => "En financement"
                                        ],
                                        [
                                            "objectkey" => "call",
                                            "objectvalue" => "Appel à participation"
                                        ],
                                        [
                                            "objectkey" => "newaction",
                                            "objectvalue" => "Nouvelle proposition"
                                        ],
                                        [
                                            "objectkey" => "prevalided",
                                            "objectvalue" => "Pré validé"
                                        ],
                                        [
                                            "objectkey" => "validproject",
                                            "objectvalue" => "Projet validé"
                                        ],
                                        [
                                            "objectkey" => "voted",
                                            "objectvalue" => "Voté"
                                        ],
                                        [
                                            "objectkey" => "finished",
                                            "objectvalue" => "Términé"
                                        ],
                                        [
                                            "objectkey" => "suspended",
                                            "objectvalue" => "Suspendu"
                                        ],
                                        [
                                            "objectkey" => "underconstruction",
                                            "objectvalue" => "En construction"
                                        ],
                                        [
                                            "objectkey" => "projectstate",
                                            "objectvalue" => "En projet"
                                        ],
                                        [
                                            "objectkey" => "notified",
                                            "objectvalue" => "Notification vue"
                                        ],
                                        [
                                            "objectkey" => "notificationSent",
                                            "objectvalue" => "Notification envoyée"
                                        ],
                                        [
                                            "objectkey" => "unnotified",
                                            "objectvalue" => "Non notifié"
                                        ],
                                        [
                                            "objectkey" => "renewed",
                                            "objectvalue" => "Reconduit"
                                        ],
                                    ] 
                                ],
                                [
                                    "label" => Yii::t("common", "Projectstate") . " " . Yii::t("common", "or") . " " . Yii::t("common", "proposal"),
                                    "field" => "inproject",
                                    "view" => "accordionList",
                                    "type" => "filters",
                                    "events" => "filters",
                                    "options" => '{"keyValue":false,"classDom":"scrollable-row","classList":"label-test-to-ellipsis","name":"'. Yii::t("common", "Projectstate") . " " . Yii::t("common", "or") . " " . Yii::t("common", "proposal") .'"}',
                                    "lists" => "keyValue",
                                    "listObject" => [
                                        [
                                            "objectkey" => "inproject",
                                            "objectvalue" => Yii::t("common", "Projectstate")
                                        ],
                                        [
                                            "objectkey" => "inproposal",
                                            "objectvalue" => Yii::t("common", "proposal")
                                        ]
                                    ] 
                                ],
                                [
                                    "label" => Yii::t("common", "Sort by"),
                                    "field" => "sortBy",
                                    "view" => "accordionList",
                                    "type" => "sortBy",
                                    "events" => "sortBy",
                                    "options" => '{"classDom":"scrollable-row","classList":"label-test-to-ellipsis","name":"'. Yii::t("common", "Sort by") .'","isRadioButton":{"answers.aapStep1.titre": ["answers.aapStep2.allVotes", "created", "updated"], "answers.aapStep2.allVotes": ["answers.aapStep1.titre", "answers.aapStep2.allVotes", "created", "updated"], "created": ["answers.aapStep1.titre", "answers.aapStep2.allVotes", "created", "updated"], "updated": ["answers.aapStep1.titre", "answers.aapStep2.allVotes", "created", "updated"]}}',
                                    "lists" => "keyValue",
                                    "listObject" => [
                                        [
                                            "objectkey" => "Ordre alphabetique",
                                            "objectvalue" => '{"answers.aapStep1.titre": 1}'
                                        ],
                                        [
                                            "objectkey" => "Plus de votes",
                                            "objectvalue" => '{"answers.aapStep2.allVotes": -1}'
                                        ],
                                        [
                                            "objectkey" => "Moins de votes",
                                            "objectvalue" => '{"answers.aapStep2.allVotes": 1}'
                                        ],
                                        [
                                            "objectkey" => "Date de création croissant",
                                            "objectvalue" => '{"created": 1}'
                                        ],
                                        [
                                            "objectkey" => "Date de création décroissant",
                                            "objectvalue" => '{"created": -1}'
                                        ],
                                        [
                                            "objectkey" => "Date de mise à jour croissant",
                                            "objectvalue" => '{"updated": 1}'
                                        ],
                                        [
                                            "objectkey" => "Date de mise à jour décroissant",
                                            "objectvalue" => '{"updated": -1}'
                                        ],
                                    ] 
                                ],
                            ]
                        ]
                    ]
                ]
            ]);
            echo "<h2 style='color:green;'> OK - Default config set with success</h2>";
        }
        
    }