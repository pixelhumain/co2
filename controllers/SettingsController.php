<?php

namespace PixelHumain\PixelHumain\modules\co2\controllers;
	use CommunecterController;
    use Element;
    use Authorisation;
    use Person;
	use Yii;

    /**
 * PodController.php
 *
 */

class SettingsController extends CommunecterController {

	public function beforeAction($action) {
		parent::initPage();
		return parent::beforeAction($action);
	}

	public function actions()
	{
	    return array(
	      //'preferences'     	=> 'citizenToolKit.controllers.pod.preferencesAction',
	    );
	}
	public function actionRedirect(){
		return $this->renderPartial("redirect");
	}
	public function actionNotificationsAccount(){
		$element=Element::getElementSimpleById(Yii::app()->session["userId"], Person::COLLECTION, null, array("preferences"));
    	return $this->renderPartial("notificationsAccount", array("preferences"=>$element["preferences"]), true);
 	}
 	public function actionConfidentiality($type=null,$id=null, $modal=false){
 		$element=Element::getElementSimpleById($id, $type, null, array("preferences","name"));
 		$params=array(  "element" => @$element, 
			"type" => $type,
			"id" => $id, 
			"edit" => true,
			"openEdition" => false,
			"modal"=>$modal
		);

		if ( Yii::app()->session["userId"]  && Authorisation::canDeleteElement( $id, $type, Yii::app()->session["userId"]) ) {
			
			return $this->renderPartial("confidentialityPanel", $params, true);	
		}else{
			if(($msgNotAuthorized = Element::getCostumMsgNotAuthorized()) != "")
            	return $msgNotAuthorized;
		}
 	}
 	/*public function actionConfidentialityAccount(){
    	echo $this->renderPartial("confidentialityAccount", array(), true);
	 }*/
	 public function actionChatManager($type=null,$id=null){
 		$element=Element::getElementSimpleById($id, $type, null, array("preferences","name","tools","slug", "hasRC", "rocketchatMultiEnabled"));
 		$params=array(  "element" => @$element, 
			"type" => $type,
			"id" => $id, 
			"edit" => true,
			"openEdition" => false
		);
    	return $this->renderPartial("chatManager", $params, true);
 	}
 	public function actionConfidentialityCommunity(){
    	return $this->renderPartial("confidentialityCommunity", array(), true);
 	}
 	public function actionNotificationsCommunity(){
    	return $this->renderPartial("notificationsCommunity", array(), true);
 	}

 	public function actionMyAccount($type=null,$id=null){
 		$element=Element::getElementSimpleById($id, $type, array(), array("_id", "preferences","name", "slug", "collection"));
		if(!isset($element)){
			$element=Element::getElementSimpleById(Yii::app()->session["userId"], $type, array(), array("_id", "preferences","name", "slug", "collection"));
			$element["id"] = Yii::app()->session["userId"];
			$element["type"] = $type;
		}else{
			$element["id"] = $id;
			$element["type"] = $type;
		}
		
 		$params=array(  "element" => @$element, 
						"type" => $element["type"],
						"id" => $element["id"],
		);
		if ( Yii::app()->session["userId"]  && Authorisation::canDeleteElement( $id, $type, Yii::app()->session["userId"]) ) {
			return $this->renderPartial("myAccount", $params, true);
			
		}else{
			return  '<div class="col-lg-offset-1 col-lg-10 col-xs-12 margin-top-50 text-center text-red">	
			 <i class="fa fa-lock fa-4x "></i><br/>
			 <h1 class="">'.Yii::t("organization", "Unauthorized Access.").'</h1>
		 </div>';

		}
 	}

 	public function actionIndex($page=null){
 		$params=array(
 			"page"=>@$page,
 			"to"=>@$_GET["slug"]
 			);
     	return $this->renderPartial("index", $params,true);
	}
}
?>