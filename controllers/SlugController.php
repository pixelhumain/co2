<?php

namespace PixelHumain\PixelHumain\modules\co2\controllers;
use CommunecterController;

/**
 * ActionController.php
 *
 * @author: Sylvain Barbot <sylvain.barbot@gmail.com>
 * Date: 7/29/15
 * Time: 12:25 AM
 */
class SlugController extends CommunecterController {
  

	public function beforeAction($action) {
	    parent::initPage();
	    return parent::beforeAction($action);
	}

	public function actions()
	{
	    return array(
	        'check'    => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\slug\CheckAction::class,
	        'checkandcreate'    => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\slug\CheckAndCreateSlugAction::class,
	        'getinfo'  => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\slug\GetInfoAction::class,
	    );
	}
}