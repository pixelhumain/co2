<?php

namespace PixelHumain\PixelHumain\modules\co2\controllers;

use CommunecterController;
use PixelHumain\PixelHumain\modules\co2\controllers\oauth\AuthHandler;
use PixelHumain\PixelHumain\modules\co2\controllers\oauth\AuthAction;
use PixelHumain\PixelHumain\modules\co2\controllers\oauth\ServicesAction;

class SsoController extends CommunecterController {
  

	public function beforeAction($action) {
	    parent::initPage();
	    return parent::beforeAction($action);
	}
    
    public function actions()
    {
        return [
            'auth' => [
                'class' => AuthAction::className(),
                'successCallback' => [$this, 'onAuthSuccess'],
            ],
            'services'  => ServicesAction::class,
        ];
    }

    public function onAuthSuccess($client)
    {
        (new AuthHandler($client))->handle();
    }
}