<?php

namespace PixelHumain\PixelHumain\modules\co2\controllers;

use CommunecterController, Yii, Rest, PHDB, CacheHelper;
use Element, yii\base\Response;
use MongoId;
use Stripe\Customer;
use \Stripe\Stripe, \Stripe\Account, \Stripe\Refund, \Stripe\Checkout\Session, \Stripe\Event;
use Stripe\Subscription;
use Stripe\Webhook;
use Stripe\Exception\SignatureVerificationException;
use Stripe\Exception\UnexpectedValueException;
use Stripe\PaymentIntent;

/**
 * StripePaymentController.php
 * 
 * @author: Dady Christon <devchriston@gmail.com>
 */
class StripePaymentController extends CommunecterController {

    public function beforeAction($action) 
    {
		return parent::beforeAction($action);
  	}

    public function setApiKey($contextSecretKey)
    {
        // Configuration de la clé secrète Stripe
        //Stripe::setApiKey(Yii::$app->params['stripe'][$context]['secretKey']);
        Stripe::setApiKey($contextSecretKey??"sk_test_51Oo3fFIynzIQ6hylTQiUi26CeTCWSB8VYbKwizkmPGuI8fvsGvaX00jtahiehP0FwSJhavWPsddZnyAlSdM4I9Gs00MkvTdPv6");
    }

    public function actionCreatePaymentIntent()
    {
        if(isset(Yii::app()->session["userId"])){
            // Récupération des données de payement;
            $contextId = Yii::$app->request->post("contextId");
            $amount = Yii::$app->request->post("amount");
            //$reason = Yii::$app->request->post("reason", "Droit Adhésion"); // name of paid product, membership, template ...
            //$email = Yii::$app->request->post("email", Yii::app()->session["userEmail"]); // email
            //$type = Yii::$app->request->post("type", "membership"); // type: membership, subscription
            
            $paymentConfig = PHDB::findOne("paymentMethods", ["parent.id" => $contextId, "type" => "stripe"]);
            // Clé API secrète Stripe
            if(isset($paymentConfig) && isset($paymentConfig["config"]["stripeSecretKey"])){
                self::setApiKey($paymentConfig["config"]["stripeSecretKey"]);
            }else{
                return [
                    'error' => "Contacter l'administrateur pour configurer la clé API",
                ];
            }

            $currency = 'eur';

            try {
                // Créez une intention de paiement
                $paymentIntent = PaymentIntent::create([
                    'amount' => floatval($amount)*100,
                    'currency' => $currency,
                    'payment_method_types' => ['card'],
                ]);
                return Rest::json([
                    'clientSecret' => $paymentIntent->client_secret,
                ]);
            } catch (\Exception $e) {
                Yii::error("Erreur de création de l'intention de paiement : " . $e->getMessage());
                return [
                    'error' => $e->getMessage(),
                ];
            }
        }
    }

    public function actionProcessPayment()
    {
        if(isset(Yii::app()->session["userId"])){
            // Récupération des données de payement
            $contextId = Yii::$app->request->post("contextId", "test");
            $amount = Yii::$app->request->post("amount", 200,0);
            $reason = Yii::$app->request->post("reason", "Droit Adhésion"); // name of paid product, membership, template ...
            $email = Yii::$app->request->post("email", Yii::app()->session["userEmail"]); // email
            $type = Yii::$app->request->post("type", "membership"); // type: membership, subscription

            // Configuration de la clé secrète Stripe
            self::setApiKey("test");

            try {// Perform a simple API request to retrieve Stripe account information
                Account::retrieve();
            } catch (\Stripe\Exception\AuthenticationException $e) {
                // Erreur d'authentification (e.g., invalid API key)
                return $this->renderPartial('/payment/stripe/error', array("message" => "Authentication error: ".$e->getMessage())); // Rediriger vers une page d'erreur
            } catch (\Stripe\Exception\ApiConnectionException $e) {
                // Problème de réseau (e.g., unable to connect to Stripe's servers)
                return $this->renderPartial('/payment/stripe/error', array("message" => "Network error: " . $e->getMessage())); // Rediriger vers une page d'erreur
            } catch (\Stripe\Exception\ApiErrorException $e) {
                // If there's a general API error
                return $this->renderPartial('/payment/stripe/error',array("message" => "API error: " . $e->getMessage()));
            }


            $costum = CacheHelper::getCostum();
            $costumSlug = array_key_first(Yii::app()->session["costum"]);
            
            $data = array(
                "name" => Yii::app()->session["user"]["name"],
                "creator" => Yii::app()->session["userId"],
                "email" => $email,
                "amount" => $amount*100,
                "type" => $type,
                "reason" => $reason,
                "collection" => "payments",
                "status" => "attempt",
                "source" => array(
                    "keys" => [$costumSlug],
                    "key" => $costumSlug,
                    'insertOrigin' => "costum"
                )
            );

            if(!empty($costum)){
                $data["parent"] = array(
                    "id" => $costum["contextId"],
                    "type" => $costum["contextType"],
                    "name" => [$costum["name"]],
                    "slug" => $costum["slug"]
                );
            }
            
            $payment = PHDB::insert("payments", $data);
            
            // Création de la session de paiement avec Stripe Checkout
            try {
                $session = Session::create([
                    'payment_method_types' => ['card'],
                    'line_items' => [[
                        'price_data' => [
                            'currency' => 'EUR', // Changez selon votre devise
                            'unit_amount' => $amount*100, //MONTANT_EN_CENTIMES,
                            'product_data' => [
                                'name' => $reason,
                                'description' => "description du open atlas",
                                //'image' => "description du open atlas",
                                //'type' => "membership",
                            ],
                        ],
                        'quantity' => 1,
                    ]],
                    'mode' => 'payment',
                    'success_url' => Yii::$app->request->hostInfo . '/co2/stripe-payment/success?slug='.$costumSlug,
                    'cancel_url' => Yii::$app->request->hostInfo . '/co2/stripe-payment/cancel?slug='.$costumSlug,
                ]);


                Yii::$app->session->set("urlOrigin", Yii::$app->request );
                // Rediriger vers la page de paiement de Stripe
                return $this->redirect($session->url);
            } catch (\Exception $e) {
                var_dump("Error", $e->getMessage());
                Yii::$app->session->setFlash('error', $e->getMessage());
                return $this->renderPartial('/payment/stripe/error'); // Rediriger vers une page d'erreur
            }
        }else{
            return $this->renderPartial(
                '/payment/stripe/error', 
                array(
                    "message" => "Votre session a expiré, Il faut vous vous connectez."
                )
            ); // Rediriger vers une page d'erreur
        }
    }

    public function actionSubscribe(){
        if ($_POST) {
            $email = $_POST['email'];
            $token = $_POST['stripeToken'];

            $amount = $_POST['amount'];
            $description = $_POST['description'];

            require_once('path/to/stripe-php/init.php');
            \Stripe\Stripe::setApiKey('VOTRE_CLE_SECRETE_STRIPE');

            try {
                $charge = \Stripe\Charge::create([
                    'amount' => 5000, // Montant en cents (ici, 50,00€)
                    'currency' => 'eur',
                    'description' => 'Adhésion',
                    'source' => $token,
                    'receipt_email' => $email,
                ]);
                Yii::app()->user->setFlash('success', 'Paiement réussi ! Merci pour votre adhésion.');
                $this->redirect(array('site/success'));
            } catch (\Stripe\Exception\CardException $e) {
                Yii::app()->user->setFlash('error', 'Le paiement a échoué : ' . $e->getMessage());
                $this->redirect(array('site/failure'));
            }
        } else {
            $this->render('subscribe');
        }
    }

    public function actionRefund($chargeId)
    {
        // Configuration de la clé secrète Stripe
        self::setApiKey("test");

        // Émission du remboursement
        try {
            $refund = Refund::create([
                'charge' => $chargeId,
            ]);

            Yii::$app->session->setFlash('success', 'Remboursement effectué avec succès.');
            return $this->renderPartial('/payment/stripe/success', array("message"=> "Remboursement effectué avec succès.", "result" => $refund)); // Rediriger vers une autre page après le remboursement
        } catch (\Exception $e) {
            Yii::$app->session->setFlash('error', $e->getMessage());
            return $this->renderPartial('/payment/stripe/error', array("message"=> "Erreur : ".$e->getMessage())); // Rediriger vers une autre page après le remboursement
        }
    }

    public function actionSuccess($session_id=null)
    {
        // Vous pouvez récupérer l'identifiant de session à partir de la requête
        $sessionId = Yii::$app->request->get('session_id');
        $session = Yii::$app->session;
        $paymentData = $session->get('paymentData');
        $paymentIntentId = Yii::$app->request->get('payment_intent');
        $sessionPayment = Yii::$app->request->get('session_payment', null);
        
        //var_dump($paymentIntentId);
        //var_dump($sessionPayment);
        //exit();

        if( $session_id != null ){
            // Récupérer l'événement lié à la session de paiement
            $event = Event::retrieve($sessionId);

            // Vérifier si l'événement est de type 'checkout.session.completed'
            if ($event->type == 'checkout.session.completed') {
                // Le paiement a réussi, vous pouvez traiter les données ici
                $message = "Paiement réussi !";
                $data = array(
                    "message" => $message
                );
                $payload = @file_get_contents('php://input');

                $costum = CacheHelper::getCostum();
                $data = array(
                    "data" => $payload,
                    "status" => "success"
                );

                if(!empty($costum)){
                    $data["source"] = array(
                        "keys" => [$costum["slug"]],
                        "key" => $costum["slug"],
                    );

                    $data["parent"] = array(
                        "id" => $costum["contextId"],
                        "type" => $costum["contextType"],
                        "name" => [$costum["name"]],
                        "slug" => $costum["slug"]
                    );
                }

                PHDB::update("payments", 
                    array(
                        "_id" => new MongoId($sessionPayment)
                    ),
                    $data
                );

                var_dump("réponse", $payload);

                Yii::$app->session->setFlash('success', 'Paiement réussi!');
                return $this->render('/payment/stripe/success', array( "message" => $message, "result" => $data)); // Rediriger vers une page de confirmation de paiement
            } else {
                Yii::$app->session->setFlash('error', 'Erreur lors du paiement');
                return $this->renderPartial('/payment/stripe/error', array("message" => "Erreur lors du paiement")); // Rediriger vers une page d'erreur
            }
        }else{
            return $this->renderPartial('/payment/stripe/error', array("message" => "Votre session a expiré")); // Rediriger vers une page d'erreur
        }
    }

    public function actionCancel()
    {
        // Le paiement a été annulé par l'utilisateur
        Yii::$app->session->setFlash('warning', 'Paiement annulé par l\'utilisateur.');
        return $this->renderPartial('/payment/stripe/cancel', array("message" => "Vous avez annulé le paiement.")); // Rediriger vers une page d'annulation de paiement
    }

    public function actionWebhook()
    {
        $endpoint_secret = 'your_stripe_webhook_secret';  // Replace with your webhook secret
        $payload = @file_get_contents('php://input');
        $sig_header = $_SERVER['HTTP_STRIPE_SIGNATURE'];

        try {
            $event = Webhook::constructEvent($payload, $sig_header, $endpoint_secret);
        } catch (UnexpectedValueException $e) {
            // Invalid payload
            Yii::$app->response->statusCode = 400;
            return 'Invalid payload';
        } catch (SignatureVerificationException $e) {
            // Invalid signature
            Yii::$app->response->statusCode = 400;
            return 'Invalid signature';
        }

        // Handle the event
        switch ($event['type']) {
            case 'payment_intent.succeeded':
                $paymentIntent = $event['data']['object']; // contains a StripePaymentIntent
                // Then define and call a method to handle the successful payment intent.
                $this->handlePaymentIntentSucceeded($paymentIntent);
                break;
            case 'payment_method.attached':
                $paymentMethod = $event['data']['object']; // contains a StripePaymentMethod
                // Then define and call a method to handle the successful attachment of a PaymentMethod.
                $this->handlePaymentMethodAttached($paymentMethod);
                break;
            // ... handle other event types
            default:
                Yii::$app->response->statusCode = 400;
                return 'Unhandled event type';
        }

        Yii::$app->response->statusCode = 200;
        return 'Success';
    }
}