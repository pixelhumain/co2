<?php

namespace PixelHumain\PixelHumain\modules\co2\controllers;
use CommunecterController;

/**
 * SiteController.php
 *
 * @author: antonio ramirez <antonio@clevertech.biz>
 * Date: 7/23/12
 * Time: 12:25 AM
 */
class TemplateController extends CommunecterController {
  

	public function beforeAction($action) {
	    parent::initPage();
	    return parent::beforeAction($action);
	}

	public function actions()
	{
	    return array(
	    	"get"=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\template\GetAction::class,
	        'use'  => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\template\UseAction::class,
	        'delete'  => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\template\DeleteAction::class,

	    );
	}
}