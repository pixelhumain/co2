<?php
class TranslateController extends CommunecterController {
  
	protected function beforeAction($action) {
		return parent::beforeAction($action);
	}
 	
 	//http://communecter74-dev/co2/translate/translate2lang/lang/PL
 	public function actionTranslate2lang($lang, $file=null) {
 		$str = "";
 		ob_start();
 		if($_SERVER['SERVER_NAME'] == "communecter74-dev" ) {
	 		$deepLKey = "4907e40c-29b7-21ab-adb2-ee956be835e5:fx";

	 		$str .= "<br><b>************ TRANSLATE TO $lang ************</b><br>";
	 		$ctAll = 0;
	 		$tradCt = 0;

	 		//create destination folder
	 		if(!file_exists("../../pixelhumain/ph/protected/messages/".$lang)) {
				$str .= mkdir("../../pixelhumain/ph/protected/messages/".$lang);
				$str .= "mkdir FOLDER pixelhumain/ph/protected/messages/$lang <br/><br/>";
	 		}
			else 
				$str .= "folder $lang EXIST<br>";

	 		//get each message in folder
	 		foreach ( scandir( "../../pixelhumain/ph/protected/messages/fr" ) as $i => $f ) {
	        //$str .= str_replace(".php", "", $f)."<br/>";
	        	if($file == null || $file == str_replace(".php", "", $f) )
	        	{
		            $str .= "<b>*********************".$f."</b><br/>";
		            $ct = 0;
		            $txtTrad = '';
			        if(  $f != "." && $f != ".." ){

			        	//laod the mùessage file
			       		 $res = include("../../pixelhumain/ph/protected/messages/fr/".$f);
			       		 
			       		 //build array of translatable strings
			       		 foreach ( $res as $k => $v ) {
			       		 	//$str .= "<b>EN : ".$k."</b><br/>";
			       		 	$txtTrad .= '&text='.$k;
			       		 	$ct += strlen($k);
			       		 	$tradCt++;
			       		 }

			       		// $str .= "<b>$lang : ".$txtTrad."</b><br/><br/>";


			       		 // DEEPL REQUEST TO API WITH LIST OF TRANSLATIONs
			       		 $ch = curl_init();

						curl_setopt($ch, CURLOPT_URL, 'https://api-free.deepl.com/v2/translate');
						curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
						curl_setopt($ch, CURLOPT_POST, 1);
						curl_setopt($ch, CURLOPT_POSTFIELDS, "auth_key=".$deepLKey.$txtTrad."&target_lang=".$lang);
						curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/x-www-form-urlencoded']);

						$result = curl_exec($ch);
						if (curl_errno($ch)) {
						    $str .= 'Error:' . curl_error($ch);
						}
						curl_close($ch);
						
						$curlRes = json_decode($result, true); // Decode the word
		//var_dump($result);exit;
			       		 if( isset($curlRes["translations"])){
				       		 $ctix = 0;
				       		 $lb = "";
				       		 $txt = 'return array(';
				       		 $str .= "php return array(<br>";
				       		 foreach ( $res as $k => $v ) {
				       		 	$str .= "\"$k\" => \"".$curlRes["translations"][$ctix]["text"]."\",<br/>";
				       		 	$txt .= '"'.$k.'"=>"'.$curlRes["translations"][$ctix]["text"].'",'.$lb;
				       		 	$ctix++;
				       		 }
				       		 $txt .= '); ?>';
				       		 $str .= '); ?><br/><br/>';

				       		 //$str .= "<pre>".$txt."</pre>"; 

				       		  file_put_contents("../../pixelhumain/ph/protected/messages/".$lang."/".$f , $txt );
							$str .= "added file : messages/$lang/$f , check exists : ".file_exists("../../pixelhumain/ph/protected/messages/".$lang."/".$f )."<br/><br/>";
			       		 	 $str .= "count : ".$ct."<br/><br/>";
			       		 } else 
			       		 	$str .= "<h3 style='color:red'>curl empty</h3>";
			       		 $ctAll += $ct;
			       	}
			    }
			}
		    $str .= "count : ".$ctAll."<br/><br/>";
		} else 
			$str .= "only works localy";
         return ob_get_clean();
         return $str;
 	}
 	

}