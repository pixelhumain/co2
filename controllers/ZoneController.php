<?php

namespace PixelHumain\PixelHumain\modules\co2\controllers;
use CommunecterController;

/**
 * SiteController.php
 *
 * @author: antonio ramirez <antonio@clevertech.biz>
 * Date: 7/23/12
 * Time: 12:25 AM
 */
class ZoneController extends CommunecterController {
  

	public function beforeAction($action) {
	    parent::initPage();
	    return parent::beforeAction($action);
	}

	public function actions()
	{
	    return array(
	        'getscopebyids'       			=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\zone\GetScopeByIdsAction::class,
	        'getzone'       			=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\zone\GetZoneAction::class,
	        'getinfo'       			=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\zone\GetZoneInfoAction::class,
            'updatedepartement'       		=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\zone\UpdateDepartementAction::class,
            'updateregion'       		=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\zone\UpdateRegionAction::class,
	        'removedoublons'       		=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\zone\RemoveDoublonsAction::class,
            'removeoldepci'             => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\zone\RemoveOldEPCIAction::class,
            'updatesirenepci'             => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\zone\UpdateSirenEPCIAction::class,
            'addnewepci'             => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\zone\AddNewEPCIAction::class,
            'updatescope'       		=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\zone\UpdateScopeEPCI::class,
            'updatetype'       		=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\zone\UpdateTypeAction::class,
            'admin'       		=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\zone\ZoneAdminAction::class,
            'updateprovincemada'       		=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\zone\UpdateMadaProvinceAction::class,
            'updateregionmada'       		=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\zone\UpdateRegionMadaAction::class,
            'adddistrictmada'       		=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\zone\AddDistrictAction::class,
            'addcommunemada'       		=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\zone\AddCommuneMadaAction::class,
            'updatev7vdistrict'       		=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\zone\UpdateDistrictV7VAction::class,
            'updatev7vcommune'       		=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\zone\UpdateCommuneV7VAction::class,
            'updatetranslatemada'       		=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\zone\UpdateTranslateZoneMadaAction::class,
            'update'                        => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\zone\UpdateZoneAction::class,
            'getshape'                        => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\zone\GetZoneShapeAction::class,
            'zoneratacher'                  => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\zone\GetZoneReattachAction::class,
            'getcities'                  => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\zone\GetCitiesAction::class,
            'updatecities'                  => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\zone\UpdateCitiesAction::class,
            'savecities'                =>  \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\zone\AddCitiesAction::class,
            "updatecountrycode"             => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\zone\UpdateCountryCodeForFranceIle::class,
            "updatedepparis"             => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\zone\UpdateDepartementParisAction::class,
            "checkelement"             => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\zone\CheckElementAction::class,
            "combinezone"             => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\zone\MergeZonetoZoneAction::class,
            // Belgique Geoshape
            "belgiqueregion"             => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\zone\BelgiqueRegionAction::class,
            "belgiqueprovince"             => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\zone\BelgiqueProvinceAction::class,
            "belgiquearrondissement"             => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\zone\BelgiqueArrondissementAction::class,
            "migratebelgiquedata"           => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\zone\MigrateDataBelgique::class,
            // Batiment La Possesion Hackaton 
            "batimentslapossesion" => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\zone\InsertionBatimentsLaPossesionAction::class,
            "findbatiment" => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\zone\hackathon\FindBatimentAction::class,
            "findbatimentbounds" => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\zone\hackathon\FindBatimentBoundsAction::class,
	    );
	}
}