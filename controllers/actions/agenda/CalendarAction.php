<?php

namespace PixelHumain\PixelHumain\modules\co2\controllers\actions\agenda;

use Authorisation;
use Organization;
use Person;
use PHDB;
use Event;
use DateTime;
use DateTimeZone;
use Form;
use HtmlHelper;
use Liliumdev\ICalendar\ZCiCal;
use MongoId;
use MongoRegex;
use PixelHumain\PixelHumain\components\Action;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\UtilsHelper;
use Project;
use Rest;
use Yii;

class CalendarAction extends Action
{
    public function run($request = '', $contextid = '', $contexttype = '', $form = '', $input = '', $standalone = false)
    {
        $output = json_encode([]);
        $parameters = [
            'module' => $this->getController()->getModules(),
            'type'   => $_POST['type'],
            'id'     => $_POST['id']
        ];
        switch ($request)
        {
            case 'event_sources':
                $search = $_POST['search'] ?? '';
                $output = [
                    'agendas' => [
                        'communecter' => [],
                        'icalendar'   => [],
                        'openagenda'  => []
                    ],
                    'events'  => [
                        'communecter' => [],
                        'icalendar'   => [],
                        'openagenda'  => []
                    ],
                    'other'   => []
                ];
                $agendas = PHDB::findOneById($_POST['collection'], $_POST['id'], [
                    'name',
                    'links.events',
                    'agengas'
                ]);
                $events = $agendas['links']['events'] ?? [];
                $agendas = $agendas['agendas'] ?? [];

                $communecter_color = self::get_random_color();
                // paramètrage de communecter
                if (!empty($events))
                {
                    $communecterEventKeys = array_keys($events);

                    // Get subelements events
                    $dbSubElements = PHDB::find(Organization::COLLECTION, ['parent.' . $_POST['id'] . '.type' => $_POST['collection']], ['links.events']);
                    foreach ($dbSubElements as $dbSubElement)
                    {
                        $dbSubElementEvents = array_keys($dbSubElement['links']['events'] ?? []);
                        foreach ($dbSubElementEvents as $dbSubElementEvent)
                        {
                            UtilsHelper::push_array_if_not_exists($dbSubElementEvent, $communecterEventKeys);
                        }
                    }

                    $dbSubElements = PHDB::find(Project::COLLECTION, ['parent.' . $_POST['id'] . '.type' => $_POST['collection']], ['links.events']);
                    foreach ($dbSubElements as $dbSubElement)
                    {
                        $dbSubElementEvents = array_keys($dbSubElement['links']['events'] ?? []);
                        foreach ($dbSubElementEvents as $dbSubElementEvent)
                        {
                            UtilsHelper::push_array_if_not_exists($dbSubElementEvent, $communecterEventKeys);
                        }
                    }
                    // Get subelements events
                    $whereEvent = [
                        '_id' => [
                            '$in' => array_map(function ($id)
                            {
                                return new MongoId($id);
                            }, $communecterEventKeys)
                        ]
                    ];
                    if (!empty($search))
                    {
                        $whereEvent['name'] = UtilsHelper::mongo_regex($search);
                    }
                    $dbEventDatabaseList = PHDB::find(Event::COLLECTION, [
                        'slug',
                        'name',
                        'startDate',
                        'endDate',
                        'timeZone',
                        'type'
                    ]);

                    foreach ($dbEventDatabaseList as $eventId => $dbEventDatabase)
                    {
                        // traitement des dates
                        $start_datetime = new DateTime();
                        $end_datetime = new DateTime();
                        $start_datetime->setTimestamp(UtilsHelper::get_as_timestamp(['startDate'], $dbEventDatabase));
                        $end_datetime->setTimestamp(UtilsHelper::get_as_timestamp(['endDate'], $dbEventDatabase));
                        if (!empty($dbEventDatabase['timeZone']))
                        {
                            $start_datetime->setTimezone(new DateTimeZone($dbEventDatabase['timeZone']));
                            $end_datetime->setTimezone(new DateTimeZone($dbEventDatabase['timeZone']));
                        }

                        $output['events']['communecter'][] = [
                            'id'         => $eventId,
                            'parent'     => 'communecter_agenda',
                            'group'      => 'Communecter',
                            'slug'       => $dbEventDatabase['slug'],
                            'name'       => $dbEventDatabase['name'],
                            'start_date' => $start_datetime->format('c'),
                            'end_date'   => $end_datetime->format('c'),
                            'color'      => $communecter_color
                        ];
                    }
                    if (count($output['events']['communecter']) > 0)
                    {
                        $output['agendas']['communecter'][] = [
                            'id'    => 'communecter_agenda',
                            'slug'  => 'communecter',
                            'group' => 'Communecter',
                            'name'  => $agendas['name'],
                            'color' => $communecter_color
                        ];
                    }
                }

                // paramètrage de l'openagenda
                if (!empty($agendas['openagenda']))
                {
                    $request = curl_init('https://api.openagenda.com/v2/me/agendas?key=' . $agendas['openagenda']['api_key']);
                    curl_setopt($request, CURLOPT_RETURNTRANSFER, true);
                    $response = json_decode(curl_exec($request), true);
                    curl_close($request);
                    if (empty($response['error']))
                    {
                        if (!empty($agendas['openagenda']['urls']))
                        {
                            $slugs = [];
                            foreach ($agendas['openagenda']['urls'] as $url)
                            {
                                $exploded = explode('/', $url);
                                $domain_index = array_search(Yii::app()->language, $exploded);
                                $domain_index = $domain_index === false ? array_search('openagenda.com', $exploded) : $domain_index;
                                $slugs[] = $domain_index === false ? $exploded[0] : explode('?', $exploded[$domain_index + 1])[0];
                            }
                            $slugs = array_unique($slugs);
                            $url = 'https://api.openagenda.com/v2/agendas?key=' . $agendas['openagenda']['api_key'] . '&slug[]=' . implode('&slug[]=', $slugs);
                            $public = curl_init($url);
                            curl_setopt($public, CURLOPT_RETURNTRANSFER, true);
                            $public_agendas = json_decode(curl_exec($public), true);
                            curl_close($public);
                            $response['items'] = array_merge($response['items'], $public_agendas['agendas']);
                        }
                        foreach ($response['items'] as $item)
                        {
                            $color = $this->get_random_color();

                            // recevoir la liste des events de l'agenda en question
                            $request = curl_init('https://api.openagenda.com/v2/agendas/' . $item['uid'] . '/events?key=' . $agendas['openagenda']['api_key']);
                            curl_setopt($request, CURLOPT_RETURNTRANSFER, true);
                            $events = json_decode(curl_exec($request), true);
                            curl_close($request);
                            $event_count = 0;
                            if (!empty($events['success']))
                            {
                                foreach ($events['events'] as $event)
                                {

                                    if (empty($search) || preg_match("/$search/i", $event['slug']) || preg_match("/$search/i", $event['title'][Yii::app()->language]))
                                    {
                                        $output['events']['openagenda'][] = [
                                            'id'         => $event['uid'],
                                            'parent'     => $item['uid'],
                                            'group'      => $item['title'] . ' (OpenAgenda)',
                                            'slug'       => $event['slug'],
                                            'name'       => $event['title'][Yii::app()->language],
                                            'start_date' => $event['lastTiming']['begin'],
                                            'end_date'   => $event['lastTiming']['end'],
                                            'color'      => $color
                                        ];
                                        $event_count++;
                                    }
                                }
                            }

                            if ($event_count > 0)
                            {
                                $output['agendas']['openagenda'][] = [
                                    'id'    => $item['uid'],
                                    'slug'  => $item['slug'],
                                    'group' => 'OpenAgenda',
                                    'name'  => $item['title'],
                                    'color' => $color
                                ];
                            }
                        }
                    }

                    if (!empty($agendas['openagenda']['slugs']))
                    {
                        $public = curl_init('https://api.openagenda.com/v2/agendas?key=' . $agendas['openagenda']['api_key'] . '' . implode('&slug[]=', $agendas['openagenda']['slugs']));
                        curl_setopt($public, CURLOPT_RETURNTRANSFER, true);
                    }
                }

                // intérrogation sur les icalendar distants
                if (!empty($agendas['icalendars']))
                {
                    $color = $this->get_random_color();

                    foreach ($agendas['icalendars'] as $source)
                    {
                        $response = file_get_contents($source);
                        $color = $this->get_random_color();
                        $agenda = [
                            'id'    => "icalendar$source",
                            'color' => $color
                        ];

                        if ($response !== FALSE)
                        {
                            $ical_object = new ZCiCal($response);
                            $event_count = 0;
                            $timezone = date_default_timezone_get();
                            foreach ($ical_object->tree->child as $node)
                            {
                                if ($node->getName() === 'VTIMEZONE')
                                {
                                    foreach ($node->data as $key => $value)
                                    {
                                        if ($key == 'TZID')
                                        {
                                            $timezone = $value->getValues();
                                            break;
                                        }
                                    }
                                    break;
                                }
                                else if ($node->getName() === 'VEVENT')
                                {
                                    foreach ($node->data as $key => $value)
                                    {
                                        if ($key == 'TZID')
                                        {
                                            $timezone = $value->getValues();
                                            break;
                                        }
                                    }
                                    break;
                                }
                            }
                            foreach ($ical_object->tree->child as $node)
                            {
                                if ($node->getName() === 'VEVENT')
                                {
                                    $event = [];
                                    foreach ($node->data as $key => $value)
                                    {
                                        switch ($key)
                                        {
                                            case 'SUMMARY':
                                                $event['name'] = $value->getValues();
                                                break;
                                            case 'UID':
                                                $event['id'] = $value->getValues();
                                                break;
                                            case 'DTEND':
                                                $end_datetime = new DateTime();
                                                if (!empty($value->parameter['value']) && $value->parameter['value'] === 'DATE') $end_datetime->setTimeZone(new DateTimeZone('UTC'));
                                                else $end_datetime->setTimezone(new DateTimeZone($timezone));
                                                $end_datetime->setTimestamp(strtotime($value->getValues()));
                                                $event['end_date'] = $end_datetime->format('c');
                                                break;
                                            case 'DTSTART':
                                                $start_datetime = new DateTime();
                                                if (!empty($value->parameter['value']) && $value->parameter['value'] === 'DATE') $start_datetime->setTimeZone(new DateTimeZone('UTC'));
                                                else $start_datetime->setTimezone(new DateTimeZone($timezone));
                                                $start_datetime->setTimestamp(strtotime($value->getValues()));
                                                $event['start_date'] = $start_datetime->format('c');
                                                break;
                                        }
                                    }
                                    if (empty($search) || preg_match("/$search/i", $event['name']))
                                    {
                                        $output['events']['icalendar'][] = [
                                            'id'         => $event['id'],
                                            'parent'     => $agenda['id'],
                                            'group'      => 'ICalendar',
                                            'slug'       => '',
                                            'name'       => $event['name'],
                                            'start_date' => $event['start_date'],
                                            'end_date'   => $event['end_date'],
                                            'color'      => $agenda['color']
                                        ];
                                        $event_count++;
                                    }
                                }
                            }

                            if ($event_count > 0 && count($output['agendas']['icalendar']) == 0)
                            {
                                $output['agendas']['icalendar'][] = [
                                    'id'    => 'icalendar',
                                    'slug'  => 'icalendar',
                                    'group' => 'ICalendar',
                                    'name'  => 'ICalendar',
                                    'color' => $color
                                ];
                            }
                        }
                    }
                }
                $output = Rest::json($output);
                break;
            case 'source_management_html':
                $output = $this->getController()->renderPartial('co2.views.agenda.source_manager.' . $_GET['type'], $parameters);
                break;
            case 'html':
                $links_types = [
                    'contributors',
                    'creator',
                    'attendees',
                    'organizer'
                ];

                if (isset($_POST['collection']) && isset($_POST['id']))
                {
                    $dbAgendas = PHDB::findOneById($_POST['collection'], $_POST['id'], [
                        'name',
                        'links',
                        'agendas'
                    ]);
                }

                // Authorization start
                $can_read = Authorisation::canSee($_POST["collection"], $_POST["id"]);
                $can_edit = false;
                if (!empty(Yii::app()->session['userId']))
                    $can_edit = Authorisation::canEdit(Yii::app()->session['userId'], $_POST['id'], $_POST['collection']);
                // Authorization end

                if ($_POST['collection'] === Event::COLLECTION)
                    $events = $dbAgendas['links']['subEvents'] ?? [];
                else
                    $events = $dbAgendas['links']['events'] ?? [];
                $agendas = $dbAgendas['agendas'] ?? [];
                $agenda_list = [
                    'icalendar'   => [],
                    'openagenda'  => [],
                    'communecter' => []
                ];
                $events_list = [
                    'icalendar'   => [],
                    'openagenda'  => [],
                    'communecter' => []
                ];

                $communecter_color = $this->get_random_color();
                // paramètrage de communecter
                if ($can_read && !empty($events))
                {
                    $agenda_list['communecter'][] = [
                        'id'    => 'communecter_agenda',
                        'slug'  => 'communecter',
                        'group' => 'Communecter',
                        'name'  => $dbAgendas['name'],
                        'color' => $communecter_color
                    ];
                    $communecterEventKeys = array_keys($events);
                    if ($_POST['collection'] === Event::COLLECTION)
                    {
                        $index = array_search($_POST['id'], $communecterEventKeys);
                        if ($index !== false)
                            array_splice($communecterEventKeys, $index, 1);
                        $communecterEventKeys = array_values($communecterEventKeys);
                    }
                    else
                    {
                        // Get subelements events
                        foreach ([Organization::COLLECTION, Project::COLLECTION] as $element)
                        {
                            $dbSubElements = PHDB::find(
                                $element,
                                ['parent.' . $_POST['id'] . '.type' => $_POST['collection']],
                                ['links.events']
                            );
                            foreach ($dbSubElements as $dbSubElement)
                            {
                                $dbSubElementEvents = array_keys($dbSubElement['links']['events'] ?? []);
                                foreach ($dbSubElementEvents as $dbSubElementEvent)
                                    UtilsHelper::push_array_if_not_exists($dbSubElementEvent, $communecterEventKeys);
                            }
                        }
                    }
                    // Get subelements events
                    $db_communecter_events = PHDB::findByIds(Event::COLLECTION, $communecterEventKeys, [
                        'slug',
                        'name',
                        'startDate',
                        'endDate',
                        'timeZone',
                        'type',
                        'links',
                        'preferences.private'
                    ]);
                    foreach ($db_communecter_events as $key => $db_communecter_event)
                    {
                        if (empty($db_communecter_event['name']) || empty($db_communecter_event['slug'])) continue;
                        // traitement des dates
                        $start_datetime = new DateTime();
                        $end_datetime = new DateTime();
                        $start_datetime->setTimestamp(UtilsHelper::get_as_timestamp(['startDate'], $db_communecter_event));
                        $end_datetime->setTimestamp(UtilsHelper::get_as_timestamp(['endDate'], $db_communecter_event));
                        if (!empty($db_communecter_event['timeZone']))
                        {
                            $start_datetime->setTimezone(new DateTimeZone($db_communecter_event['timeZone']));
                            $end_datetime->setTimezone(new DateTimeZone($db_communecter_event['timeZone']));
                        }

                        $is_admin = false;
                        if (!empty(Yii::app()->session['userId']))
                        {
                            foreach ($links_types as $links_type)
                            {
                                if (!empty($db_communecter_event['links'][$links_type][Yii::app()->session['userId']]['isAdmin']) && filter_var($db_communecter_event['links'][$links_type][Yii::app()->session['userId']]['isAdmin'], FILTER_VALIDATE_BOOL) === true)
                                {
                                    $is_admin = true;
                                    break;
                                }
                            }
                        }

                        $events_list['communecter'][] = [
                            'id'         => $key,
                            'parent'     => 'communecter_agenda',
                            'group'      => $dbAgendas['name'] . ' (Communecter)',
                            'slug'       => $db_communecter_event['slug'],
                            'name'       => $db_communecter_event['name'],
                            'start_date' => $start_datetime->format('c'),
                            'end_date'   => $end_datetime->format('c'),
                            'url'        => '#page.type.events.id.' . $key,
                            'color'      => $communecter_color,
                            'is_admin'   => $is_admin
                        ];
                    }
                }

                // paramètrage de l'openagenda
                if ($can_read && !empty($agendas['openagenda']))
                {
                    $request = curl_init('https://api.openagenda.com/v2/me/agendas?key=' . $agendas['openagenda']['api_key']);
                    curl_setopt($request, CURLOPT_RETURNTRANSFER, true);
                    $response = json_decode(curl_exec($request), true);
                    curl_close($request);

                    if (!empty($response['success']))
                    {
                        if (!empty($agendas['openagenda']['urls']))
                        {
                            $slugs = [];
                            foreach ($agendas['openagenda']['urls'] as $url)
                            {
                                $exploded = explode('/', $url);
                                $domain_index = array_search(Yii::app()->language, $exploded);
                                $domain_index = $domain_index === false ? array_search('openagenda.com', $exploded) : $domain_index;
                                $slugs[] = $domain_index === false ? $exploded[0] : explode('?', $exploded[$domain_index + 1])[0];
                            }
                            $slugs = array_unique($slugs);
                            $url = 'https://api.openagenda.com/v2/agendas?key=' . $agendas['openagenda']['api_key'] . '&slug[]=' . implode('&slug[]=', $slugs);
                            $public = curl_init($url);
                            curl_setopt($public, CURLOPT_RETURNTRANSFER, true);
                            $public_agendas = json_decode(curl_exec($public), true);
                            curl_close($public);
                            $response['items'] = array_merge($response['items'], $public_agendas['agendas']);
                        }
                        foreach ($response['items'] as $item)
                        {
                            $color = $this->get_random_color();
                            $agenda_list['openagenda'][] = [
                                'id'    => $item['uid'],
                                'slug'  => $item['slug'],
                                'group' => 'OpenAgenda',
                                'name'  => $item['title'],
                                'color' => $color
                            ];

                            $url = 'https://api.openagenda.com/v2/agendas/' . $item['uid'] . '/events?key=' . $agendas['openagenda']['api_key'] . '&size=';
                            // recevoir la liste des events de l'agenda en question
                            $request = curl_init($url . '0');
                            curl_setopt($request, CURLOPT_RETURNTRANSFER, true);
                            $events = json_decode(curl_exec($request), true);
                            curl_close($request);

                            if (!empty($events['success']))
                            {

                                $request = curl_init($url . $events['total']);
                                curl_setopt($request, CURLOPT_RETURNTRANSFER, true);
                                $events = json_decode(curl_exec($request), true);
                                curl_close($request);

                                foreach ($events['events'] as $event)
                                {
                                    $events_list['openagenda'][] = [
                                        'id'         => $event['uid'],
                                        'parent'     => $item['uid'],
                                        'group'      => $item['title'] . ' (OpenAgenda)',
                                        'slug'       => $event['slug'],
                                        'name'       => $event['title'][Yii::app()->language],
                                        'start_date' => $event['lastTiming']['begin'],
                                        'end_date'   => $event['lastTiming']['end'],
                                        'url'        => 'https://openagenda.com/' . $item['slug'] . '/events/' . $event['slug'] . '?lang=' . Yii::app()->language,
                                        'color'      => $color
                                    ];
                                }
                            }
                        }
                    }
                }

                // intérrogation sur les icalendar distants
                if ($can_read && !empty($agendas['icalendars']))
                {
                    $color = $this->get_random_color();
                    $agenda_list['icalendar'][] = [
                        'id'    => 'icalendar',
                        'slug'  => 'icalendar',
                        'group' => 'ICalendar',
                        'name'  => 'ICalendar',
                        'color' => $color
                    ];
                    foreach ($agendas['icalendars'] as $source)
                    {
                        $response = file_get_contents($source);
                        $color = $this->get_random_color();

                        if ($response !== FALSE)
                        {
                            $ical_object = new ZCiCal($response);

                            $agenda = [
                                'id'    => "icalendar$source",
                                'slug'  => "icalendar$source",
                                'group' => 'ICalendar',
                                'color' => $color
                            ];

                            $timezone = date_default_timezone_get();
                            foreach ($ical_object->tree->child as $node)
                            {
                                if (empty($node)) continue;

                                if ($node->getName() === 'VTIMEZONE')
                                {
                                    foreach ($node->data as $key => $value)
                                    {
                                        if ($key == 'TZID')
                                        {
                                            $timezone = $value->getValues();
                                            break;
                                        }
                                    }
                                    break;
                                }
                                else if ($node->getName() === 'VEVENT')
                                {
                                    foreach ($node->data as $key => $value)
                                    {
                                        if ($key == 'TZID')
                                        {
                                            $timezone = $value->getValues();
                                            break;
                                        }
                                    }
                                    break;
                                }
                            }
                            if (empty($agenda['name']))
                                $agenda = $agenda_list['icalendar'][0];
                            foreach ($ical_object->tree->child as $node)
                            {
                                if (empty($node)) continue;

                                if ($node->getName() === 'VEVENT')
                                {
                                    $event = [];
                                    foreach ($node->data as $key => $value)
                                    {
                                        switch ($key)
                                        {
                                            case 'SUMMARY':
                                                $event['name'] = $value->getValues();
                                                break;
                                            case 'URL':
                                                $event['url'] = $value->getValues();
                                                break;
                                            case 'UID':
                                                $event['id'] = $value->getValues();
                                                break;
                                            case 'DTEND':
                                                $end_datetime = new DateTime();
                                                if (!empty($value->parameter['value']) && $value->parameter['value'] === 'DATE') $end_datetime->setTimeZone(new DateTimeZone('UTC'));
                                                else $end_datetime->setTimezone(new DateTimeZone($timezone));
                                                $end_datetime->setTimestamp(strtotime($value->getValues()));
                                                $event['end_date'] = $end_datetime->format('c');
                                                break;
                                            case 'DTSTART':
                                                $start_datetime = new DateTime();
                                                if (!empty($value->parameter['value']) && $value->parameter['value'] === 'DATE') $start_datetime->setTimeZone(new DateTimeZone('UTC'));
                                                else $start_datetime->setTimezone(new DateTimeZone($timezone));
                                                $start_datetime->setTimestamp(strtotime($value->getValues()));
                                                $event['start_date'] = $start_datetime->format('c');
                                                break;
                                        }
                                    }
                                    $events_list['icalendar'][] = [
                                        'id'         => $event['id'],
                                        'parent'     => $agenda['id'],
                                        'group'      => 'ICalendar',
                                        'slug'       => '',
                                        'name'       => $event['name'],
                                        'start_date' => $event['start_date'],
                                        'end_date'   => $event['end_date'],
                                        'url'        => $event['url'] ?? '',
                                        'color'      => $agenda['color']
                                    ];
                                }
                            }
                        }
                    }
                }

                // envoyer en paramètre les agendas
                $parameters['agenda_list'] = $agenda_list;
                $parameters['events'] = $events_list;
                $parameters['agendas'] = $agendas;
                $parameters['standalone'] = $standalone;
                $parameters['can_edit'] = $can_edit;

                if (!empty($form))
                    $formElem = PHDB::findOneById(Form::COLLECTION, $form);
                if (!empty($formElem))
                    $parameters['form'] = $formElem;
                if ($standalone)
                {
                    if (isset(Yii::app()->session["userId"]))
                        $output = $this->getController()->renderPartial('co2.views.agenda.calendar', $parameters);
                    else
                    {
                        if (!empty($formElem["temporarymembercanreply"]) && ($formElem["temporarymembercanreply"] == "true" || $formElem["temporarymembercanreply"] == true))
                        {
                            $connexionmodeParams = [
                                "id"       => 'new',
                                "answer"   => array(),
                                "mode"     => "w",
                                "form"     => $formElem,
                                "redirect" => "#codate.calendar.request.html.contextid." . $contextid . ".contexttype." . $contexttype . ".form." . $form . ".input." . $input . ".standalone." . $standalone
                            ];
                            if ($standalone && !empty($formElem["tplformconnexionmode"]))
                            {
                                $tplc = $formElem["tplformconnexionmode"];
                            }
                            else
                            {
                                $tplc = (!empty($formElem["tpl"])) ? $formElem["tpl"] : "survey.views.default.connexionmode";
                            }
                            $output = $this->getController()->renderPartial($tplc, $connexionmodeParams);
                        }
                        else if (!empty($formElem["disconnectedPage"]) && $formElem["disconnectedPage"] == "connexionModal")
                        {

                            if (Yii::app()->request->isAjaxRequest)
                            {

                                $output = "<style> #coformlogin form { display : contents; }</style><div id='coformlogin'>";
                                HtmlHelper::registerCssAndScriptsFiles(array('/js/default/loginRegister.js'), Yii::app()->getModule("co2")->getAssetsUrl());

                                $output .= $this->getController()->renderPartial('webroot.themes.CO2.views.layouts.loginRegisterForCoform');
                                $output .= "<div>";
                                return $output;
                            }
                            else
                            {

                                $output = "<style #coformlogin .form { display : contents; }></style><div id='coformlogin'>";
                                $output .= $this->getController()->render('webroot.themes.CO2.views.layouts.loginRegister');
                                $output .= "<div>";
                                return $output;
                            }
                        }
                        else
                        {
                            if (Yii::app()->request->isAjaxRequest)
                                return $this->getController()->renderPartial("co2.views.default.unTpl", array(
                                    "msg"    => Yii::t("survey", "You are not allowed to access to this answer ! Reason : not logged"),
                                    "icon"   => "fa-lock",
                                    "reason" => "unlogged"
                                ));
                            else
                                return $this->getController()->render("co2.views.default.unTpl", array(
                                    "msg"  => Yii::t("survey", "You are not allowed to access to this answer ! Reason : not logged"),
                                    "icon" => "fa-lock", "reason" => "unlogged"
                                ));
                        }
                    }
                }
                else
                    $output = $this->getController()->renderPartial('co2.views.agenda.calendar', $parameters);
                break;
        }
        return $output;
    }

    public static function get_random_color()
    {
        return '#' . substr('0' . base_convert(rand(0, 255), 10, 16), -2) . substr('0' . base_convert(rand(0, 255), 10, 16), -2) . substr('0' . base_convert(rand(0, 255), 10, 16), -2);
    }
}
