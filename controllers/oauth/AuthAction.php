<?php
namespace PixelHumain\PixelHumain\modules\co2\controllers\oauth;

use Yii;
use yii\helpers\Url;

class AuthAction extends \yii\authclient\AuthAction
{
    public $redirectView='@modules/co2/views/oauth/redirect.php';

    public function init()
    {
        //parent::init();
        //$this->user = Instance::ensure($this->user, User::className());

            $this->setSuccessUrl(Url::home());
            $this->setCancelUrl(Url::to(Url::base(true)."#panel=box-login"));

    }
}