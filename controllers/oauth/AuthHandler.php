<?php
namespace PixelHumain\PixelHumain\modules\co2\controllers\oauth;

use Yii;
use yii\authclient\ClientInterface;
use yii\helpers\ArrayHelper;
use Person;
use yii\helpers\Url;
/**
 * AuthHandler handles successful authentication via Yii auth component
 */
class AuthHandler
{
    /**
     * @var ClientInterface
     */
    private $client;

    public function __construct(ClientInterface $client)
    {
        $this->client = $client;
    }

    public function handle()
    {
        $attributes = $this->client->getUserAttributes();
        
        if($this->client->getId() == "ademe" || $this->client->getId() == "tierslieuxorg" || $this->client->getId() == "fabmobio" || $this->client->getId() == "tierslieuxoccitanie"){
        $email = ArrayHelper::getValue($attributes, 'email');
        $name = ArrayHelper::getValue($attributes, 'name');
        $username = ArrayHelper::getValue($attributes, 'preferred_username');
        if(empty($username)){
            $username = ArrayHelper::getValue($attributes, 'username');
        }
        $id = ArrayHelper::getValue($attributes, 'sub');
        } else {
            // service pas configuré ici
            Yii::$app->getSession()->setFlash('errorAuth', $this->client->getId().": service pas configuré");
            //Yii::$app->response->redirect(Url::to(Url::base(true)."#panel=box-login"), '302')->send();
            //return $this->redirect(Url::to(Url::base(true)."#panel=box-login"),false);
            return;
        }

        $serviceDecrypt = ["serviceName" => $this->client->getId(), "serviceData" => [ "id" => (string)$id, "email" => (string)$email, "username" => (string)$username, "name" => (string)$name]];

        $res = Person::serviceOauth($serviceDecrypt);

        // var_dump($res);
        // exit;
        if($res["result"] == true) {

            $res["account"]["serviceName"] = $this->client->getId();
            Person::saveUserSessionData($res["account"]);
            Person::updateLoginHistory((String) $res["account"]["_id"]);

        } else {
            // error retour
            // var_dump($res);
            // exit;
            Yii::$app->getSession()->setFlash('errorAuth', $this->client->getId().": ".$res["msg"]);
            // "?error=".$res["msg"].
            //Yii::$app->response->redirect(Url::to(Url::base(true)."#panel=box-login"), '302')->send();
            //return $this->redirect(Url::to(Url::base(true)."#panel=box-login"), false);
            return;
        }
    }

    public function redirect($url, $enforceRedirect = true)
    {

        $viewFile = Yii::getAlias('@modules/co2/views/oauth/redirect.php');
        
        $viewData = [
            'url' => $url,
            'enforceRedirect' => $enforceRedirect
        ];

        $response = Yii::$app->getResponse();
        $response->content = Yii::$app->getView()->renderFile($viewFile, $viewData);

        return $response;
    }

}