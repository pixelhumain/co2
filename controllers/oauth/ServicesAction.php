<?php
namespace PixelHumain\PixelHumain\modules\co2\controllers\oauth;

use Yii;
use CTKException;

class ServicesAction extends \PixelHumain\PixelHumain\components\Action {
    public function run() {
        $controller = $this->getController();
        $service = $_GET["authclient"] ?? null;

        if(!isset($service) || empty($service)){
            throw new CTKException("Authclient is mandatory");
        }
        if(Yii::$app->authClientCollection->hasClient($service)){
        $authClient = Yii::$app->authClientCollection->getClient($service);
        $authClient->setReturnUrl(Yii::$app->urlManager->createAbsoluteUrl(["/co2/sso/auth?authclient=".$service]));
        $url = $authClient->buildAuthUrl();

        Yii::$app->response->redirect($url, '302')->send();
        } else {
            throw new CTKException("Authclient not found");
        }
    }
}
