# Refactor cache costum


## Dans les view
Remplacé : 
```
Yii::app()->session["costum"]
Yii::app()->session['costum']
```
Par : `$this->costum`

----

Remplacé : 
```
Yii::app()->session['paramsConfig'];  
Yii::app()->session["paramsConfig"]; 
```
Par : `$this->appConfig`

## Dans les controller

Utiliser
```
$controller = $this->getController();
$controller->costum
$controller->appConfig
```


## Dans les function static
Utiliser
```
$costum = CacheHelper::getCostum();
$appConfig = CacheHelper::get("appConfig");
```

 