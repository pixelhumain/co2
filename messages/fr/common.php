<?php

return array(
	'Participate' => 'Participer',
	'Quit' => 'Quitter',
	'No events to show'	=> 'Aucun événement à afficher',
	"Searching" => "Recherche en cours",
	"WELCOME" => "BIENVENUE",
	"ADD SOMETHING" => "AJOUTER",

	"PEOPLE"		=> "CITOYENS",
	"ORGANIZATIONS" => "ORGANISATIONS",
	"PROJECTS" 		=> "PROJETS",
	"EVENTS" 		=> "ÉVÉNEMENTS",
	"FRIENDS"		=> "AMIS",
	"CITIES" 			=> "COMMUNES",
	"ADD SOMETHING" 	=> "AJOUTER",
	"HELP US : BUGS, IDEAS" => "AIDEZ NOUS : BUG, IDEE",
	"CONNECT" 			=> "CONNECTER",
	"LOGOUT" 			=> "DÉCONNECTER",
	"REGISTER"			=> "S'INSCRIRE",

	"WHAT"		=> "COMMUNECTER ?",
	"WHY"		=> "POURQUOI ?",
	"WHO"		=> "QUI ?",
	"FOR WHO"	=> "POUR QUI ?",
	"BY WHO" 	=> "PAR QUI ?",
	"HOW"		=> "COMMENT ?",
	"WHEN"		=> "QUAND ?",
	"WHERE"		=> "OÙ ?",
	"HELP US"	=> "AIDEZ-NOUS ?",
	"GET INVOLVED" => "PARTICIPEZ !",

	"Required Fields" => "Champs obligatoires",
	"Change password" => "Changez le mot de passe",

	"The current user is not valid : please login." => "Cet utilisateur n'est pas loggué",
	"Incomplete Request." => "Demande Incomplète",

	"Oops! You are stuck at " => "Oops! Vous etes bloqué en ",
	"Unfortunately the page you were looking for could not be found." => "Malheureusement la page que vous cherchez est introuvable",
	"It may be temporarily unavailable, moved or no longer exist." => "Elle est peut etre temporairement indisponible, déplacé ou n'existe plus.",
	"Check the URL you entered for any mistakes and try again." => "Vérifier l'URL et rééssayez",
	"Return home" => "Retour Acceuil",

	"Follow this person" => "Suivre cette personne",
	"Follow this organization" => "Suivre cette organisation",
	"Follow this project" => "Suivre ce projet",
	"Unfollow" => "Ne plus suivre",

	"Documentation" => "Documentation",
	"contribute to" => "contribue à",


	"Save Processing"=> "Enregistrement en cours",
	"Understanding newspaper and news stream"=> "Comprendre le journal et le fil d&apos;actualité",

	"Short description" => "Description courte",

	"Remove" => "Supprimer",
	"Update" => "Modifier",
	"Edit" => "Éditer",
	"Chat Settings" => "de Messagerie",
	
	"Add a block in the page" => "Ajouter un bloc à la page",
	"Choose a template" => "Choisir un modèle",
	"Save as new template" => "Enregistrer comme modèle",
	"Forsake" => "Abandonner",
	"Preview" => "Apercevoir",
	"Go to communecter" => "aller sur communecter",
	"App (Menu)" => "App (Menu)",
	"Coform" => "Co-Formulaire",
	"Advenced settings" => "Paramètres avancés",
	"Click on the button below to search" => "Cliquer sur le bouton ci-dessous pour rechercher",
	"Search and add" => "Rechercher et ajouter",
	"Enter the name of the item you are looking for" => "Saisir le nom de l'élément que vous recherchez",
	"Select from list" => "Sélectionner dans la liste",
	"Only one" => "Un seulement"
);