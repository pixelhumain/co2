<?php
	$cssAndScriptFilesModule = array(
	'/js/default/profilSocial.js'
	);
	HtmlHelper::registerCssAndScriptsFiles($cssAndScriptFilesModule, Yii::app()->getModule( "co2" )->getAssetsUrl());
?>
<?php 
    $rolesLists = [];
  
?>
<script>
    var hashUrlPage = location.hash
    var canEdit = aapObj.canEdit;
    var canParticipate = aapObj.canParticipate;
    var contextData = aapObj.context;
    var elementData = aapObj.context;
    let elementId = "<?= isset($elementId) && !empty($elementId) ? $elementId : "" ;?>";
    let elementType = "<?= isset($elementType) && !empty($elementType) ? $elementType : "" ;?>";
    let elementContext = <?= isset($elemContext) && !empty($elemContext) ? json_encode($elemContext) : json_encode("") ;?>;
    if(notEmpty(elementId) && notEmpty(elementType)) {
        contextData["id"] = elementId;
        contextData["type"] = elementType;
    }
    if(notEmpty(elementType) && elementType == "projects") {
        contextData["avancement"] = <?= isset($elemContext["properties"]["avancement"]) ? json_encode($elemContext["properties"]["avancement"]) : json_encode(""); ?>
    }
    mylog.log("check elementContext", elementContext)
    if(typeof elementContext == "object" && Object.keys(elementContext).length > 0) {
        contextData = {...contextData,...elementContext}
        mylog.log("check elementContext", elementContext)
    }
    pageProfil.params = {
        "view": "detail",
        "subview": "",
        "dir": "detail",
        "elemContainer" : "#about-element-container",
        "key": ""
    }
    pageProfil.initCallB = () => {
        // $(".load-community").off().on("click",function(){ 
        //     $(".load-community").removeClass("active");
        //     $(this).addClass("active");
        //     coInterface.showLoader("#communitySearchContent");
        //     pageProfil.views.directory();
        // });
        if(typeof aapObj != "undefined" && aapObj.context && aapObj.context.name) {
            setTitle("", "", aapObj.context.name)
        }
    }
    pageProfil.init();
</script>