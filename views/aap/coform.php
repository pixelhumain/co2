<style type="text/css">
    #aapcoform {
        padding: 50px;
        background-color: #ededed;
    }

    #coformheader , #coformcontainer {
        background-color: #fff;
    }

    #coformheader {
        border-top-left-radius: 50px;
        border-top-right-radius: 50px;
        padding-top: 40px !important;
    }

    #coformcontainer {
        background-color: #fff;
        border-bottom-left-radius: 50px;
        border-bottom-right-radius: 50px;
        padding-bottom: 60px;
    }

    .colorgreenAap{
        color: #9bc129 !important;
    }

    .coformheader .coformtitle{
        font-size: 30px;
    }

    @media (max-width: 767px) {
        #aapcoform {
            padding: 10px 5px !important;
        }
    }
</style>

<div id="aapcoform">
    <div id="coformheader" class="container">
        <?php
            $coformtitle = "";
            if (!empty($answer["results"][$answerId]['answers']['aapStep1']['titre'])){
                $coformtitle = $answer["results"][$answerId]['answers']['aapStep1']['titre'];
            }
        ?>
        <h2 class="coformtitle margin-20"><?= $coformtitle ?></h2>
        <?php
            if (!empty($answer["results"][$answerId]['answers']['aapStep1']['titre'])){
        ?>
                <div class="row margin-20" >
                    <div class="col-md-9 col-sm-9">
                        <div> Parent : <span class="colorgreenAap"> <?= $context["name"] ?> </span></div>
                        <div> Déposé par : <b> <?= @$answer["users"][array_keys($answer["users"])[0]]["name"] ?> </b></div>
                        <div> Le : <?= date("d-m-Y H:i:s" , @$answer["results"][array_keys($answer["results"])[0]]["created"]) ?> </div>
                    </div>
                    <div class="col-md-3 col-sm-3">
                        <div> Statut : <b> <?= implode( " , ", @$answer["users"][array_keys($answer["users"])[0]]["status"])  ?> </b></div>
                        <div> <i class="fa fa-users"></i> <span class="colorgreenAap"> <?= @count(array_keys($answer["users"])) ?> </span> Contributeur(s) </div>
                    </div>
                </div>
        <?php
            }
        ?>
    </div>
    <div id="coformcontainer" class="container">
        <i class="fa fa-spinner fa-3x fa-spin" ></i>
    </div>
</div>
<script type="text/javascript">
    var formId = "<?php echo !empty($form["_id"]) ? (string)$form["_id"] : "null" ?>";
    var answerId = "<?php echo !empty($answerId) ? $answerId : "null" ?>";
    const renderThisStep = "<?php echo !empty($renderThisStep) ? $renderThisStep : "" ?>"
    $(document).ready(function() {
        coInterface.showLoader("#coformcontainer");
        ajaxPost("#coformcontainer", baseUrl+'/survey/answer/answer/id/'+answerId+'/form/'+formId + ( renderThisStep ? "/step/" + renderThisStep : "" ),
            { url : window.location.href },
            function(data){

            },"html");
    });
</script>
