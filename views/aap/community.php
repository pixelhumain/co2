<?php
	$cssAndScriptFilesModule = array(
	'/js/default/profilSocial.js'
	);
	HtmlHelper::registerCssAndScriptsFiles($cssAndScriptFilesModule, Yii::app()->getModule( "co2" )->getAssetsUrl());
?>
<style>
    #filterContainerInside{
        overflow-y:unset !important;
    }
</style>
<div class="container">
    <div id="central-container" class="padding-10"></div>
</div>
<?php 
    $rolesLists = [];
    $elementCount = [];
    $el = $context; 
    if (isset($el["links"]["members"])) {
        $elementCount[Link::$linksTypes[$el["collection"]][Person::COLLECTION] . "Active"] = 0;
        $elementCount["admin"] = 0;
        foreach ($el["links"]["members"] as $e => $v) {
            if (isset($v["roles"])) {
                foreach ($v["roles"] as $role) {
                    if (!empty($role)) {
                        if (!isset($rolesLists[$role])) {
                            $rolesLists[$role] = array("count" => 1, "label" => $role);
                        } else
                            $rolesLists[$role]["count"]++;
                    }
                }   
            }
            if (isset($v[Link::IS_INVITING])) {
                if (isset($elementCount[Link::IS_INVITING]))
                    $elementCount[Link::IS_INVITING]++;
                else
                    $elementCount[Link::IS_INVITING] = 1;
            } else if (isset($v[Link::TO_BE_VALIDATED]) || isset($v[Link::IS_ADMIN_PENDING])) {
                if (isset($elementCount["toBeValidated"]))
                    $elementCount["toBeValidated"]++;
                else
                    $elementCount["toBeValidated"] = 1;
            } else {
                $elementCount[Link::$linksTypes[$el["collection"]][Person::COLLECTION] . "Active"]++;
                if (isset($v["isAdmin"])) {
                    $elementCount["admin"]++;
                }
            }
        }
    }
    if($el["collection"] == Event::COLLECTION)
        $paramsGetInfoDetail["typesList"]=Event::$types;
    else if($el["collection"] == Organization::COLLECTION)
        $paramsGetInfoDetail["typesList"]=Organization::$types;
    $paramsGetInfoDetail["element"] = $el;
    $paramsGetInfoDetail["type"] = $el["collection"];
    $paramsGetInfoDetail["edit"] = Authorisation::canEditItem(Yii::app()->session["userId"], $el["collection"], (string)$el["_id"]);
    $paramsGetInfoDetail["openEdition"] = Authorisation::isOpenEdition((string)$el["_id"], $el["collection"], @$el["preferences"]);
    $getInfoDetail = Element::getInfoDetail($paramsGetInfoDetail, $el, $el["collection"], (string)$el["_id"]);
    $invitedMe = (isset($getInfoDetail["invitedMe"])) ? $getInfoDetail["invitedMe"] : [];
  
?>
<script>
    var hashUrlPage = location.hash
    var canEdit = aapObj.canEdit;
    var canParticipate = aapObj.canParticipate;
    var contextData = aapObj.context;
    contextData.rolesLists =  <?= json_encode($rolesLists) ?>;
    rolesList =  <?= json_encode(array_keys($rolesLists)) ?>;
    contextData.counts =  <?= json_encode($elementCount) ?>;
    invitedMe = <?= json_encode($invitedMe); ?>;
    var elementData = aapObj.context;
    pageProfil.params = {
        "view": "directory",
        "subview": "",
        "dir": "members",
        "key": ""
    }
    pageProfil.directory.initEvents = function(){
        $(".load-community").off().on("click",function(){ 
            $(".load-community").removeClass("active");
            $(this).addClass("active");
            coInterface.showLoader("#communitySearchContent");
            pageProfil.views.directory();
        });
    }
    pageProfil.init();
</script>