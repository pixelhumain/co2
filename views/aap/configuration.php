<div class="d-flex no-padding">
    <div class="container-fluid no-padding mx-0" id="coformcontainer" style="width: 100%;"></div>
    <div class="coformbuilder-side-content coformbuilder-right-content">
        <button class="btn-toggle-coformbuilder-sidepanel d-none" data-target="right"><i class="fa fa-chevron-left" aria-hidden="true"></i></button>
        <div id="coformRightPanel" style="width: 100%; height:100%;"></div>
    </div>
</div>


<script type="text/javascript">
    $(function(){
        coInterface.showLoader("#coformcontainer");
        let currentView = "parentform";
        if(typeof aapObj != "undefined" && aapObj?.common?.getQuery) {
            const currentQuery = aapObj.common.getQuery();
            currentQuery["aapview"] ? currentView = currentQuery["aapview"] : ""
        }
        ajaxPost("#coformcontainer", baseUrl+'/survey/answer/answer/form/'+aapObj.form._id.$id+'/mode/fa/aapview/'+currentView,
            { url : window.location.href },
            function(data){
                let dropdownSubMenuDom = $('.dropdown-menu.drop-down-sub-menu').filter(function(){
                    return $(this).is(':visible');
                })
                let stickTop = 0;
                if (dropdownSubMenuDom.length > 0) {
                    dropdownSubMenuDom.each(function(){
                        var bcr = this.getBoundingClientRect();
                        stickTop = Math.max(bcr.bottom, stickTop);
                    });
                } else {
                    $('#mainNav').each(function(){
                        var bcr = this.getBoundingClientRect();
                        stickTop = Math.max(bcr.bottom, stickTop);
                    });
                }
                if($("#confignav").is(":visible"))
                    $('.coformbuilder-side-content').css('top', $("#confignav").height() + 20 + stickTop);
            },"html");
    });
</script>




