<?php
$cssJS = array(
 '/plugins/gridstack/css/gridstack.min.css',
 '/plugins/gridstack/js/gridstack.js',
 '/plugins/gridstack/js/gridstack.jQueryUI.min.js'

);
HtmlHelper::registerCssAndScriptsFiles($cssJS, Yii::app()->request->baseUrl);
?>

<div class="list-aap-container" id="aapformcont"></div>

<script type="text/javascript">
    coInterface.showLoader("#aapformcont",trad.currentlyloading);
    $(document).ready(function() {
        $('.grid-stack').gridstack({
            cellHeight: 100,
            verticalMargin: 10,
            horizontalMargin: 10,
            disableResize: true,
            removable: true
        });
        ajaxPost("#aapformcont", baseUrl+'/survey/answer/ocecoorgadashboard/formparentid/'+aapObj.form['_id']['$id'],
        {
            urlR : "<?php echo $_SERVER['REQUEST_URI']?>"
        },
        function(data){

        },"html");
    });
</script>
