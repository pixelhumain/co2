<?php
    HtmlHelper::registerCssAndScriptsFiles(['/css/aap/proposal.css'], $this->module->assetsUrl);
?>
<div class="no-padding aap-list-container">
    <div class="row">
        <div class="col-md-12 no-padding">
            <div class="col-xs-3 menu-filters-lg filter" id="filterContainerAapOceco" style="display: none;">
                <div id='filterContainerL' class='aap-filter menu-verticale searchObjCSS'></div>
            </div>
            <div class="col-xs-12 col-md-8 filter-container">
                <button type="button" class="btn btn-default showHide-filters-xs btn-aap-tertiary hidden-xs" style="height: fit-content;"
                        id="show-filters-lg">
                    <span class="filter-xs-count topbar-badge badge animated bounceIn badge-tranparent badge-success"></span>
                    <i class="fa fa-sliders"></i>
                    <small class="pl-2"><?php echo Yii::t("form", "Filters") ?></small>
                </button>
                <div class='headerSearchContainerL no-padding col-xs-12'></div>
                <div class='bodySearchContainer margin-top-30'>
                    <div class='col-xs-12 no-padding' id='dropdown_search'>
                    </div>
                    <div class='padding-top-20 col-xs-12 text-left footerSearchContainerL'></div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(function(){
        function proposition_init_filters() {
            var filterPrms = aapObj.paramsFilters.proposal(aapObj);
            filterPrms.results.renderView = {
                single : true,
                view : "aapObj.directory.proposalObservatory"
            };
            filterPrms.defaults.indexStep = "all";
            filterPrms.defaults.fields = ["context","user","answers"];
            aapObj.filterSearch.proposal = searchObj.init(filterPrms);
            aapObj.filterSearch.proposal.search.init(aapObj.filterSearch.proposal);
        }
        proposition_init_filters();
        aapObj.events.proposal(aapObj)
        aapObj.events.common(aapObj);
        aapObj.common.switchFilter()
    })
</script>