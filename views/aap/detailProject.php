<?php
	$JsCss = [
		'/plugins/kanban/kanban.css',
		'/plugins/kanban/kanban.js',
		'/plugins/jquery-confirm/jquery-confirm.min.css',
		'/plugins/jquery-confirm/jquery-confirm.min.js'
	];
	HtmlHelper::registerCssAndScriptsFiles(
		[
			"/css/blockcms/jkanban.css",
			"/js/blockcms/jkanban.js"
		],
		Yii::app()->getModule('costum')->assetsUrl
	);
	HtmlHelper::registerCssAndScriptsFiles(
		[
			"/js/kanban.js",
			"/js/aap/detailProject.js",
			"/js/aap/top_contributor_filter.js",
			"/css/kanban.css",
			'/css/aap/detail.css'
		],
		Yii::app()->getModule('co2')->assetsUrl
	);
	HtmlHelper::registerCssAndScriptsFiles($JsCss, Yii::app()->getBaseUrl(true));
	$showFilter = $showFilter ?? true;
?>
<style>
	.kanban-no-task-action, .kanban-doing-task-action, .kanban-done-task-action {
		cursor: default;
	}
    .kanban-no-task-action {
        background-color: #777 !important;
        color: white;
    }

    .kanban-doing-task-action {
        background-color: #d9534f !important;
        color: white;
    }

    .kanban-done-task-action {
        background-color: #9bc125 !important;
        color: white;
    }
</style>
<div class="container-fluid propsDetailPanelContainer">
    <div id="action-modal-preview"></div>
    <div class="row">
		<?php if ($showFilter === true || $showFilter === 'true') { ?>
            <div class="col-sm-2 pos-sticky allPropsPanel aap-nav-panel no-padding hidden-xs">
                <h4 class="d-flex justify-content-between col-xs-12 pt-2 pb-32 border-bottom-default">
                    <span class="title propsPanelTitle">
                        <span class="topbar-badge badge animated bounceIn badge-tranparent badge-success proposal-count-pastille"
                              id="proposalResultCount"></span>
                        <?php echo Yii::t("common", "Projects") ?>
                    </span>
                    <i id="btnHideFilterProps" class="fa fa-angle-double-left cursor-pointer custom-font pr-2"></i>
                </h4>
                <div class="accordion" id="proposalDetailAccord">
                    <div class="panel proposListPanel panel-default mb-0">
                        <!-- <div class="panel-title collapse-title pb-0 d-flex justify-content-between mb-0" id="headingTwo" data-toggle="collapse"
								data-target="#listPropsPanel" aria-expanded="false" aria-controls="listPropsPanel">
								<a href="javascript:;" class="collapse-title pb-0">Tous les projets de l'organisation</a>
								<i class="fa fa-caret-up"></i>
							</div> -->
                        <div id="propsDetail" class="filterContainerD filter-alias custom-filter searchBar-filters pull-left set-width mx-4 mb-4">
                            <input type="text" type="text" class="form-control pull-left text-center alias-main-search-bar search-bar"
                                   data-field="text"
                                   placeholder="<?php echo Yii::t('common', "Search by") ?> <?php echo ucfirst(Yii::t('common', "Project name")) ?>">
                            <span class="text-white input-group-addon pull-left main-search-bar-addon " data-field="text" data-icon="fa-search">
                                <i class="fa fa-search"></i>
                            </span>
                        </div>
                        <div id="listPropsPanel" class="mt-2 in col-xs-12" style="height: 62rem; overflow-y: auto" aria-labelledby="headingTwo"
                             data-parent="#proposalDetailAccord">
                            <div class="aap-results-container no-padding">
                                <div class="col-xs-12 row no-padding" id="dropdown_search_detail" style="min-height: 45vh; position: relative;"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-2 pos-sticky allFilterPanel aap-nav-panel no-padding hidden-xs" style="display: none;">
                <h4 class="d-flex justify-content-between col-xs-12 pt-2 pb-32 border-bottom-default">
                    <span class="title propsPanelTitle"><?php echo Yii::t("form", "Filters") ?> </span>
                    <i id="btnHideFilterOnly" class="fa fa-angle-double-left cursor-pointer custom-font pr-2 showHide-filters-xs">
                        <span class="filter-xs-count topbar-badge badge animated bounceIn badge-tranparent badge-success filter-pastille"
                              id="detailFilterCount"></span>
                    </i>
                </h4>
                <div class="accordion px-0" id="proposalFilterAccord">
                    <div class="panel filterPanel panel-default mb-0">
                        <!-- <div class="panel-title collapse-title showHide-filters-xs pb-0 d-flex justify-content-between mb-0" id="filterL"
                            data-toggle="collapse" data-target="#filterLCollapse" aria-expanded="true" aria-controls="filterLCollapse">
                            <a href="javascript:;" class="collapse-title pb-0"><?php echo Yii::t("form", "Filters") ?></a>
                            <i class="fa fa-caret-up"></i>
                            <span class="filter-xs-count topbar-badge badge animated bounceIn badge-tranparent badge-success"></span>
                        </div> -->

                        <div id="filterLCollapse" class="in" aria-labelledby="filterL" data-parent="#proposalFilterAccord" style="height: 73.7rem;">
                            <div id="filterContainerD" class="aap-filter"></div>
                        </div>
                    </div>
                </div>
            </div>
		<?php } ?>
        <div class="<?php echo filter_var($showFilter, FILTER_VALIDATE_BOOLEAN) ? 'col-sm-10' : 'col-sm-12' ?> col-xs-12 propsDetailPanel no-padding">
            <!-- Nav tabs -->
            <ul class="project-nav nav nav-tabs aap-nav-tabs pos-sticky bg-aap-white" role="tablist">
				<?php if (filter_var($showFilter, FILTER_VALIDATE_BOOLEAN)) { ?>
                    <li class="hidden-xs show-hide-filter">
                        <a href="javascript:;" class="openPropsList tooltips showHide-filters-xs" data-toggle="tooltip" data-html="true" data-placement="right"
                           data-original-title="<?php echo Yii::t("common", "Show/hide filter and projects") ?>">
                            <span class="filter-xs-count topbar-badge badge animated bounceIn badge-tranparent badge-success" id="detailFilterCountInBtn"></span>
                            <i class="fa fa-sliders"></i>
                        </a>
                    </li>
				<?php } ?>
                <li class="kanban-tab">
                    <a class="need-reload" href="#action" role="tab" data-toggle="tab" data-project="<?= $projectId ?? '' ?>"
                       data-answer="<?= $answerId ?? '' ?>">Kanban</a>
                </li>
                <li class="plan-tab">
                    <a class="need-reload" href="#plan" role="tab" data-toggle="tab" data-project="<?= $projectId ?? '' ?>"
                       data-answer="<?= $answerId ?? '' ?>"><?= ucfirst(Yii::t('common', 'planning')) ?></a>
                </li>
				<li class="about-elem-tab">
                    <a class="need-reload text-ellipsis" href="#aboutelem" role="tab" data-toggle="tab" data-project="<?= $projectId ?? '' ?>"
                       data-answer="<?= $answerId ?? '' ?>"><?= ucfirst(Yii::t('common', 'About')) ?></a>
                </li>
				<?php if ((empty($stepAccess["disabledStepsId"]) || !in_array("aapStep3", $stepAccess["disabledStepsId"])) && (empty($stepAccess["hiddenStepsId"]) || !in_array("aapStep3", $stepAccess["hiddenStepsId"]))) : ?>
                    <li class="has-proposal fund-tab">
                        <a class="need-reload" id="fundings" href="#funding" role="tab" data-toggle="tab" data-project="<?= $projectId ?? '' ?>"
                           data-answer="<?= $answerId ?? '' ?>"><?= ucfirst(Yii::t('common', 'funding')) ?></a>
                    </li>
				
				<?php
				endif;
					if (isset($form["coremu"]) && filter_var($form["coremu"], FILTER_VALIDATE_BOOLEAN)) : ?>
                        <li class="has-proposal">
                            <a class="need-reload" href="#contribution" role="tab" data-toggle="tab" data-project="<?= $projectId ?? '' ?>"
                               data-answer="<?= $answerId ?? '' ?>">Contributions</a>
                        </li>
					<?php endif; ?>
                <li class="has-proposal summary-tab">
                    <a class="need-reload" href="#summary" role="tab" data-toggle="tab" data-project="<?= $projectId ?>"
                       data-answer="<?= $answerId ?? '' ?>"><?= Yii::t('common', 'Summary') ?></a>
                </li>
                <?php if (!empty(Yii::app()->session['userId'])) { ?>
                <li class="has-proposal evaluation-tab">
                    <a class="need-reload" href="#evaluation" role="tab" data-toggle="tab" data-project="<?= $projectId ?>"
                       data-answer="<?= $answerId ?? '' ?>"><?= Yii::t('common', 'Evaluation') ?></a>
                </li>
                <?php } ?>

				<?php //if (Authorisation::isElementMember($projectId, Project::COLLECTION, Yii::app()->session['userId'])) { ?>
                    <li class="pull-right invite-tab">
                        <a href="javascript:;" class="pull-right text-center link-banner btn-invite-proposal dropup text-ellipsis" data-placement="bottom"
                           data-original-title="Inviter des contributeurs au projet" data-answerid="" data-projectid="<?= $projectId ?>">
                            <i class="fa fa-user-plus a-icon-banner"></i>
                            <span class="title-link-banner hidden-sm">
                                <?= Yii::t("common", "Invite") ?>
                            </span>
                        </a>
                    </li>
				<?php //} ?>
                <li class="pull-right comment-tab">
                    <a href="javascript:;" id="btn-comment-<?= $answerId ?? '' ?>" class="pull-right text-center btn-comment-proposal link-banner text-ellipsis"
                       data-toggle="tooltip" data-placement="bottom" data-original-title="<?= Yii::t("comment", "comment") ?>">
                        <i class='fa fa-2xx fa-commenting'></i>
                        <span class="title-link-banner hidden-sm contributor-filter">
                            <?= Yii::t("comment", "comment") ?>
                        </span>
                    </a>
                </li>
                <li class="pull-right contributor-project-filter dropdown" id="contributor-filter-container"></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <div class="tab-pane aap-list-container" id="action">
                    <div id="kanban-container1" style="height: 685px;">
                        <div id="aap-kanban"></div>
                    </div>
                </div>
                <div class="tab-pane aap-list-container need-reload" id="plan"></div>
				<div class="tab-pane aap-list-container" id="aboutelem">
					<div id="about-element-container" class="mt-4" style="height: 685px;">
                    </div>
				</div>
                <div class="tab-pane aap-list-container" id="funding"></div>
                <div class="tab-pane aap-list-container" id="contribution"></div>
                <div class="tab-pane aap-list-container" id="summary"></div>
                <div class="tab-pane aap-list-container" id="evaluation"></div>
            </div>
        </div>
    </div>
</div>
<script>
	(function ($) {
		var fromObservatory = <?= json_encode($fromObservatory ?? null)  ?>;
		var modal_answer_id = <?= json_encode($answerId ?? null)  ?>;
		var modal_project_id = <?= json_encode($projectId ?? null)  ?>;
		var showFilter = <?= isset($showFilter) ? json_encode($showFilter) : "true"  ?>;
		const isInsideModal = <?= isset($insideModal) ? json_encode(filter_var($insideModal, FILTER_VALIDATE_BOOLEAN)) : json_encode(false)  ?>;
		var defaultView = JSON.parse(JSON.stringify(<?= json_encode($aapview ?? 'action') ?>));
		var queryParams = aapObj.common.getQuery();
		if (!notEmpty(queryParams.projectId) && notEmpty(modal_project_id))
			queryParams.projectId = modal_project_id
		if (notEmpty(queryParams.projectId)) {
			function project_init_filters() {
				//var filterPrms = aapObj.paramsFilters.projectDetail(aapObj, isInsideModal);
				// if(queryParams?.userId)
				//     filterPrms.defaults.filters[`links.contributors.${queryParams?.userId}`] = {'$exists' : true};
				
				var filterPrms = aapObj.paramsFilters.projectDetail(aapObj);
				var filterProposals = aapObj.paramsFilters.proposalDetail(aapObj);
				filterPrms.defaults.filters = filterProposals.defaults.filters;
				// filterPrms.defaults.filters['project.id'] = {'$exists' : true};
                filterPrms.defaults?.filters?.['project.id'] ? delete filterPrms.defaults.filters['project.id'] : "";
                filterPrms.defaults?.filters?.['status'] ? delete filterPrms.defaults.filters['status'] : "";
                filterPrms.defaults?.filters?.['answers.aapStep1.year'] ? delete filterPrms.defaults.filters['answers.aapStep1.year'] : "";
				filterPrms.filters = filterProposals.filters;
				filterPrms.filters.avancement = {
					view : "accordionList",
					type : "filters",
					field : "properties.avancement",
					name : ucfirst(trad.advancement),
					event : "filters",
					keyValue : false,
					list : avancementProject
				}

                if(filterPrms.filters?.avancement?.list) {
                    var avancementClone = {};
                    for([avcKey, avcVal] of Object.entries(filterPrms.filters.avancement.list)) {
                        if( avcKey == "") 
                            avancementClone["notSpecified"] = avcVal;
                        else
                            avancementClone[avcKey] = avcVal;
                    }
                    notEmpty(avancementClone) ? filterPrms.filters.avancement.list = JSON.parse(JSON.stringify(avancementClone)) : "";
                }

				if (queryParams?.projectUserId) {
					filterPrms.defaults.filters['$or'] = {
						user : userId,
						["links.contributors." + userId] : {$exists : true}
					}
				}

				filterPrms.toActivate = aapObj.filterSearch.project?.search?.obj && aapObj.filterSearch?.activeFilters && Object.keys(aapObj.filterSearch?.activeFilters).length > 0 ? JSON.parse(JSON.stringify(aapObj.filterSearch.activeFilters)) : {}
				aapObj.filterSearch?.activeFilters ? aapObj.filterSearch.activeFilters = {} : "";
				aapObj.filterSearch.projectDetail = searchObj.init(filterPrms);
				aapObj.filterSearch.project?.search?.obj && Object.keys(filterPrms.toActivate).length > 0 ? aapObj.filterSearch.projectDetail.search.obj = JSON.parse(JSON.stringify(aapObj.filterSearch.project?.search?.obj)) : ""
				if (aapObj.filterSearch.projectDetail?.pInit?.filters?.text?.field) {
					aapObj.filterSearch.projectDetail.search.obj.textPath = aapObj.filterSearch.projectDetail?.pInit?.filters?.text?.field;
				}
				$("#propsDetail .alias-main-search-bar").val(aapObj.filterSearch.projectDetail?.search?.obj?.text)
				aapObj.filterSearch.projectDetailInitialized = JSON.parse(JSON.stringify(aapObj.filterSearch.projectDetail?.search?.obj));
				aapObj.filterSearch.projectDetail.search.init(aapObj.filterSearch.projectDetail);
			}

			if (notNull(modal_project_id)) {
				queryParams.projectId = modal_project_id;
				queryParams.modal_answer_id = modal_answer_id;
			}
			aapObj.common.switchProjectToProposalDetail(aapObj, queryParams);
			if (typeof queryParams.aapview === 'undefined') {
				// $('#plan').addClass('active');
				$('a[role=tab][data-toggle=tab]:first').tab('show');
				aapObj.common.loadProjectDetail(aapObj, queryParams.projectId, 'action');
				history.pushState(null, null, aapObj.common.buildUrlQuery(document.location.href, {
					aapview : 'action'
				}, aapObj.common.getQueryStringObject()));
			}
			$('.btn-invite-proposal').attr('data-answerid', queryParams?.answerId)
			aapObj.events.common(aapObj);
			if (parseBool(showFilter))
				project_init_filters();
		}
		$('.tab-contributor').tooltip();
	})(jQuery)
</script>