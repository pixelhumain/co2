<?php
	
	use PixelHumain\PixelHumain\modules\citizenToolKit\components\UtilsHelper;
	
	HtmlHelper::registerCssAndScriptsFiles(['/css/aap/detail.css'], $this->module->assetsUrl);
    $showFilter = $showFilter ?? true;
    /* if(empty($answerId)){
             echo Yii::t("common","Proposal")." ".Yii::t("common","empty");
             echo '<style>.breadcrumb.aap-breadcrumb{display:none}</style>';
         }else{*/
?>
<div class="container-fluid propsDetailPanelContainer">
    <div class="row">
        <?php if ($showFilter === true || $showFilter === 'true') { ?>
            <div class="col-sm-2 pos-sticky allPropsPanel aap-nav-panel no-padding hidden-xs">
                <h4 class="d-flex justify-content-between col-xs-12 pt-2 pb-32 border-bottom-default">
                    <span class="title propsPanelTitle">
                        <span class="topbar-badge badge animated bounceIn badge-tranparent badge-success proposal-count-pastille" id="proposalResultCount"></span>
                        <?php echo Yii::t("common", "The proposals") ?> 
                    </span>
                    <i id="btnHideFilterProps" class="fa fa-angle-double-left cursor-pointer pr-2 custom-font"></i>
                </h4>
                <div class="accordion" id="proposalDetailAccord">
                    <div class="panel proposListPanel panel-default mb-0">
                        <!-- <div class="panel-title collapse-title pb-0 d-flex justify-content-between mb-0" id="headingTwo" data-toggle="collapse"
                                data-target="#listPropsPanel" aria-expanded="false" aria-controls="listPropsPanel">
                                <a href="javascript:;" class="collapse-title pb-0">Toutes les propositions de l'organisation</a>
                                <i class="fa fa-caret-up"></i>
                            </div> -->
                        <div id="propsDetail" class="filterContainerD filter-alias custom-filter searchBar-filters pull-left set-width mx-4 mb-4">
                            <input
                                    type="text"
                                    class="form-control pull-left text-center alias-main-search-bar search-bar"
                                    data-field="text"
                                    data-text-path="answers.aapStep1.titre"
                                    placeholder="<?php echo ucfirst(Yii::t('common', "Search by proposal title")) ?>"
                            >
                            <span
                                    class="text-white input-group-addon pull-left main-search-bar-addon "
                                    data-field="text"
                                    data-icon="fa-search"
                            >
                                <i class="fa fa-search"></i>
                            </span>
                        </div>
                        <div id="listPropsPanel" class="mt-2 in col-xs-12" style="height: 62rem; overflow-y: auto" aria-labelledby="headingTwo"
                             data-parent="#proposalDetailAccord">
                            <div class="aap-results-container no-padding">
                                <div class="col-xs-12 row no-padding" id="dropdown_search_detail" style="min-height: 45vh; position: relative;"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-2 pos-sticky allFilterPanel aap-nav-panel no-padding hidden-xs" style="display: none;">
                <h4 class="d-flex justify-content-between col-xs-12 pt-2 pb-32 border-bottom-default">
                    <span class="title propsPanelTitle"><?php echo Yii::t("form", "Filters") ?> </span>
                    <i id="btnHideFilterOnly" class="fa fa-angle-double-left cursor-pointer pr-2 custom-font showHide-filters-xs">
                        <span class="filter-xs-count topbar-badge badge animated bounceIn badge-tranparent badge-success filter-pastille"
                              id="detailFilterCount"></span>
                    </i>
                </h4>
                <div class="accordion px-0" id="proposalFilterAccord">
                    <div class="panel filterPanel panel-default mb-0">
                        <!-- <div class="panel-title collapse-title showHide-filters-xs pb-0 d-flex justify-content-between mb-0" id="filterL"
                            data-toggle="collapse" data-target="#filterLCollapse" aria-expanded="true" aria-controls="filterLCollapse">
                            <a href="javascript:;" class="collapse-title pb-0"><?php echo Yii::t("form", "Filters") ?></a>
                            <i class="fa fa-caret-up"></i>
                            <span class="filter-xs-count topbar-badge badge animated bounceIn badge-tranparent badge-success"></span>
                        </div> -->

                        <div id="filterLCollapse" class="in" aria-labelledby="filterL" data-parent="#proposalFilterAccord" style="height: 73.7rem;">
                            <div id="filterContainerD" class="aap-filter"></div>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
        <div class="<?php echo filter_var($showFilter, FILTER_VALIDATE_BOOLEAN) ? 'col-sm-10' : 'col-sm-12' ?> col-xs-12 propsDetailPanel no-padding">
            <ul class="nav nav-tabs aap-nav-tabs pos-sticky bg-aap-white" role="tablist">
                <?php if (filter_var($showFilter, FILTER_VALIDATE_BOOLEAN)) { ?>
                    <li class="hidden-xs" style="margin-left: 3px;">
                        <a href="javascript:;" class="openPropsList tooltips showHide-filters-xs" data-toggle="tooltip" data-html="true" data-placement="right"
                           data-original-title="<?php echo Yii::t("common", "Show/hide filter and proposals") ?>">
                            <span class="filter-xs-count topbar-badge badge animated bounceIn badge-tranparent badge-success" id="detailFilterCountInBtn"></span>
                            <i class="fa fa-sliders"></i>
                        </a>
                    </li>
                <?php } ?>
                <li>
                    <a href="#proposition-summary" role="tab" data-toggle="tab"
                       data-answer="<?= $answerId ?? '' ?>"><?= Yii::t('common', 'Summary') ?></a>
                </li>
                
                <?php if (!empty(Yii::app()->session["userId"]) && (empty($stepAccess["disabledStepsId"]) || !in_array("aapStep2", $stepAccess["disabledStepsId"])) && (empty($stepAccess["hiddenStepsId"]) || !in_array("aapStep2", $stepAccess["hiddenStepsId"]))) { ?>
                    <li><a class="need-reload" href="#proposition-evaluation" role="tab" data-toggle="tab"
                           data-answer="<?= $answerId ?? '' ?>"><?= Yii::t('common', 'Evaluation') ?></a></li>
                <?php }
                    
                    if ((empty($stepAccess["disabledStepsId"]) || !in_array("aapStep3", $stepAccess["disabledStepsId"])) && (empty($stepAccess["hiddenStepsId"]) || !in_array("aapStep3", $stepAccess["hiddenStepsId"]))) { ?>
                        <li><a class="need-reload" href="#proposition-funding" role="tab" data-toggle="tab"
                               data-answer="<?= $answerId ?? '' ?>"><?= ucfirst(Yii::t('common', 'funding')) ?></a></li>
                    <?php }
                    
                    if (isset($form["coremu"]) && filter_var($form["coremu"], FILTER_VALIDATE_BOOLEAN)) { ?>
                        <li><a class="need-reload" href="#proposition-contribution" role="tab" data-toggle="tab" data-answer="<?= $answerId ?? '' ?>">Contributions</a>
                        </li>
                    <?php } ?>

                <li class="pull-right invite-tab">
                    <a href="javascript:;" class="pull-right text-center btn-invite-proposal link-banner dropup"
                       data-placement="bottom" data-original-title="Inviter des contributeurs au projet" data-answerid="<?= $answerId ?>"
                       data-projectid="">
                        <i class="fa fa-user-plus a-icon-banner"></i>
                        <span class="title-link-banner hidden-sm">
                            <?= Yii::t("common", "Invite") ?>
                        </span>
                    </a>
                </li>
                <li class="pull-right">
                    <a href="javascript:;" id="btn-comment-<?= $answerId ?? '' ?>" class="pull-right text-center btn-comment-proposal link-banner"
                       data-toggle="tooltip" data-placement="bottom" data-original-title="<?= Yii::t("comment", "comment") ?>">
                        <i class='fa fa-2xx fa-commenting'></i>
                        <span class="title-link-banner hidden-sm">
                            <?= Yii::t("comment", "comment") ?>
                        </span>
                    </a>
                </li>
                <li class="pull-right pt-2 btn-switch-resume">
                    <button class="edit-proposal btn btn-aap-primary pull-right" data-view="detailed">
                        <i class="fa fa-pencil-square-o" id="proposition-summary-i"></i>
                        <span id="proposition-summary-span"> <?= Yii::t("common", "Modify proposal") ?> </span>
                    </button>
                </li>
	            <li class="pull-right pt-2 pr-2">
                    <?php if(!empty(Yii::app()->session["userId"])){ ?>
		                <button class="<?= UtilsHelper::className(['btn btn-aap-primary pdf-download', 'disabled' => empty($answerId)]) ?>" data-answer="<?= $answerId ?>"><i class="fa fa-file-pdf-o"></i></button>
	                <?php } ?>
                </li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <div class="tab-pane aap-list-container" id="proposition-summary">
                    <!-- <div class="row" style="margin: 10px 0">
                        <button class="edit-proposal btn btn-aap-primary pull-right" data-view="detailed">
                            <i class="fa fa-pencil-square-o" id="proposition-summary-i"></i>
                            <span id="proposition-summary-span"> Modifier la proposition </span>
                        </button>
                    </div> -->
                    <div id="proposition-summary-container" class="padding-top-10">
                    </div>
                </div>

                <div class="tab-pane aap-list-container padding-top-10" id="proposition-evaluation">

                </div>
                <div class="tab-pane aap-list-container padding-top-10" id="proposition-funding">

                </div>
                <?php if (isset($form["coremu"]) && filter_var($form["coremu"], FILTER_VALIDATE_BOOLEAN)) { ?>
                    <div class="tab-pane aap-list-container padding-top-10" id="proposition-contribution">

                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
    <script>
        (function (W, $) {
            var modal_answer_id = <?= isset($answerId) ? json_encode($answerId) : "null"  ?>;
            var queryParams = aapObj.common.getQuery();
            const isInsideModal = <?= isset($insideModal) ? json_encode(filter_var($insideModal, FILTER_VALIDATE_BOOLEAN)) : json_encode(false)  ?>;
            var showFilter = <?= isset($showFilter) ? json_encode($showFilter) : "true"  ?>;
            if (!notEmpty(queryParams.answerId) && notEmpty(modal_answer_id)) {
                queryParams.answerId = modal_answer_id
            }
            function proposition_init_filters() {
                var filterPrms = aapObj.paramsFilters.proposalDetail(aapObj, isInsideModal);
                if (queryParams?.userId) {
                    filterPrms.defaults.filters['$or'] = {
                        user : userId,
                        ["links.contributors." + userId] : {$exists : true}
                    }
                    // filterPrms.defaults.filters.user = userId;
                }
                if (!notEmpty(queryParams.answerId) && filterPrms.results) {
                    var definedEventFunc = () => {};
                    if (typeof filterPrms.results.events == "function") {
                        definedEventFunc = filterPrms.results.events;
                    }
                    filterPrms.results.events = function () {
                        var allDataResKeys = null;
                        if (aapObj.filterSearch?.proposalDetail?.results?.datares) {
                            allDataResKeys = Object.keys(aapObj.filterSearch.proposalDetail.results.datares);
                            var tabsDom = $('.nav.nav-tabs.aap-nav-tabs a[role=tab][data-toggle=tab], .btn-aap-primary.pdf-download');
                            tabsDom.attr("data-answer", allDataResKeys[0])
                            tabsDom.removeClass("disabled")
                            const urlQuery = {
                                ...queryParams, 
                                answerId : allDataResKeys[0],
                                aapview : 'summary'
                            }
                            var newHash = aapObj.common.buildUrlQuery('#detailProposalAap', urlQuery, aapObj.common.getQueryStringObject())
                            history.replaceState({}, null, document.location.origin + document.location.pathname + newHash)
                            $(`.nav.nav-tabs.aap-nav-tabs a[role=tab][data-toggle=tab][data-href=#proposition-summary]`).parent().addClass("active");
                        }
                        definedEventFunc()
                        if (allDataResKeys) {
                            if (allDataResKeys.length > 0) {
                                $(`.aap-results-container #dropdown_search_detail .props-item#propItem${allDataResKeys[0]}`).trigger("click");
                            }
                        }
                    }
                }
                filterPrms.toActivate = aapObj.filterSearch.proposal?.search?.obj && aapObj.filterSearch?.activeFilters && Object.keys(aapObj.filterSearch?.activeFilters).length > 0 ? JSON.parse(JSON.stringify(aapObj.filterSearch.activeFilters)) : {}
                aapObj.filterSearch?.activeFilters ? aapObj.filterSearch.activeFilters = {} : "";
                aapObj.filterSearch.proposalDetail = searchObj.init(filterPrms);
                aapObj.filterSearch.proposal?.search?.obj && Object.keys(filterPrms.toActivate).length > 0 ? aapObj.filterSearch.proposalDetail.search.obj = JSON.parse(JSON.stringify(aapObj.filterSearch.proposal?.search?.obj)) : ""

                if (aapObj.filterSearch.proposalDetail?.pInit?.filters?.text?.field) {
                    aapObj.filterSearch.proposalDetail.search.obj.textPath = aapObj.filterSearch.proposalDetail?.pInit?.filters?.text?.field;
                    $("#propsDetail .alias-main-search-bar").val(aapObj.filterSearch.proposalDetail?.search?.obj?.text)
                }
                aapObj.filterSearch.proposalDetailInitialized = JSON.parse(JSON.stringify(aapObj.filterSearch.proposalDetail?.search?.obj));
                aapObj.filterSearch.proposalDetail.search.init(aapObj.filterSearch.proposalDetail);
            }
            if (showFilter == true || showFilter == 'true') {
                proposition_init_filters();
            }
            if (notEmpty(queryParams.answerId)) {
                const hashes = document.location.hash.split('.');
                const index_of = hashes.indexOf('answerId');

                var proposition_id = queryParams && queryParams['answerId'] ? queryParams['answerId'] : hashes[index_of + 1];
                if (notNull(modal_answer_id)) {
                    proposition_id = modal_answer_id;
                }

                aapObj.common.switchProposalToProjectDetail(aapObj, queryParams);

                $("#filterContainerInside .count-result-xs").css({'cssText' : `display: block !important;`})
                if (typeof queryParams.aapview === 'undefined') {
                    $('#proposition-summary').addClass('active');
                    $('a[role=tab][data-toggle=tab]:first').tab('show');
                    aapObj.common.loadProposalDetail(aapObj, queryParams.answerId, 'summary');
                    if(location.hash.includes("Aap"))
                        history.pushState(null, null, aapObj.common.buildUrlQuery(document.location.href, {aapview : 'summary'}, aapObj.common.getQueryStringObject()));
                } else {
                    aapObj.common.loadProposalDetail(aapObj, queryParams.answerId, queryParams.aapview);
                }
                $('.btn-invite-proposal').attr('data-projectid', queryParams?.projectId)
                // $('.btn-invite-proposal').attr('href', '#element.invite.type.answers.id.' + proposition_id);

                aapObj.events.common(aapObj);
            }
        })(window, jQuery)
    </script>
</div>
<?php //} ?>
