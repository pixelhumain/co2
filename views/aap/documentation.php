<?php 
    HtmlHelper::registerCssAndScriptsFiles([
        '/js/codoc/api.js',
        '/js/codoc/utils.js',
        '/js/codoc/main.js',
    ], $this->module->getParentAssetsUrl());
?>
<div class="page-content container">
    <div class="sub-header">
        <div class="custom-breadcrumb"></div>
        <div class="actions-container hidden">
            <?php if(isset($_SESSION["userId"]) && Authorisation::isUserSuperAdmin($_SESSION["userId"])){ ?>
            <button type="button" class="btn-content-action d-none" data-action="save"><i class="fa fa-check" aria-hidden="true"></i></button>
            <button type="button" class="btn-content-action d-none" data-action="cancel"><i class="fa fa-undo" aria-hidden="true"></i></button>
            <button type="button" class="btn-content-action d-none" data-action="edit"><i class="fa fa-pencil" aria-hidden="true"></i></button>
            <?php } ?>
        </div>
    </div>
    <div class="doc-container">
        <div id="md-viewer" class="markdown-body">

        </div>
    </div>
</div>
<script>
    $(function(){
        codoc.actions.fetchFileRaw("10 - OCECO/droits_acces_aap.md");
        codoc.preview.init();
        codoc.editor.init();
    })
</script>