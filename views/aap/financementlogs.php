<?php
HtmlHelper::registerCssAndScriptsFiles(array(
    //'/plugins/jsontotable/JSON-to-Table.min.1.0.0.js',
    '/plugins/jqueryTablesorter/jquery.tablesorter.min.js',
    '/plugins/jqueryTablesorter/jquery.tablesorter.widgets.js',
    '/plugins/jqueryTablesorter/widget-output.min.js',
    '/plugins/Sortable/Sortable.min.js',
    '/plugins/jqueryTablesorter/theme.ice.min.css',
    '/plugins/select2/select2v4/select2v4.min.css',
), Yii::app()->request->baseUrl);
HtmlHelper::registerCssAndScriptsFiles(array(
    '/js/financementlog.js',
    '/css/financementlog.css',
), Yii::app()->getModule(Survey::MODULE)->getAssetsUrl());
?>
<script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
<div class="col-md-12 col-sm-12 col-xs-12">
    <h3 class="financerlogtitle"> Financement </h3>
</div>
<div id="fiterBtnGroup" class="col-md-12 col-sm-12 col-xs-12 no-padding">

</div>
<div id="headerselect" class="col-md-12 col-sm-12 col-xs-12 no-padding" style="display: none">
    <div class="form-group">
        <select id="select2v4" multiple="multiple" class="form-control" style="width: 100%;"></select>
    </div>
</div>
<div id="financementFilTable" class="financertable col-md-12 col-sm-12 col-xs-12 tgledv contentInformationCoremu no-padding">

</div>
<div style="display: none" id="financementTable" class="financertable col-md-12 col-sm-12 col-xs-12 tgledv contentInformationCoremu no-padding">

</div>
<div style="display: none" id="financementStat" class="financertable col-md-12 col-sm-12 col-xs-12 tgledv contentInformationCoremu no-padding">

</div>
<script type="text/javascript">
    jQuery(document).ready(function() {
        var fLog = financementlogObj.action.init();
        fLog.action.loadData(financementlogObj,null,aapObj.form._id.$id);
    });
</script>