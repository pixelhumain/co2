<?php
    $params = array(
        "form"              => $form,
        "context"           => $context,
        "page"              => $page,
        "pageDetail"        => $pageDetail,
        "canEdit"           => $canEdit,
        "canParticipate"    => $canParticipate,
        "canSee"            => $canSee,
        "status"            => $status,
        "inputs"            => $inputs,
        "aapview"           => $aapview ?? null,
        "subOrganizations"  => $subOrganizations ?? null,
        "answer"            => $answer ?? null,
        "answerId"          => $answerId ?? null,
        "project"           => $project ?? null,
        "projectId"         => $projectId ?? null,
        "session"           => $session ?? null,
        "aapSession"        => $aapSession ?? null,
        "showMap"           => @$showMap,
        "defaultProject"    => $defaultProject,
        "defaultAnswer"     => $defaultAnswer,
        "defaultProposal"   => $defaultProposal,
        "myDefaultProject"  => $myDefaultProject ?? null,
        "myDefaultProposal" => $myDefaultProposal ?? null,
        "stepAccess"        => $stepAccess ?? null,
        "fonds"             => $fonds,
        "quartiers"         => $quartiers,
        "associations"      => $associations,
        "renderThisStep"    => $renderThisStep ?? "",
        "admin_projects"    => $admin_projects ?? [],
        "haveAnswer"        => $haveAnswer ?? false,
        //"financors" => $financors
    );

    $allowedDisconnectedPage = [
        "proposal",
        "detailProposal",
        "project",
        "detailProject",
        "projects",
        "observatory"
    ];
    if (isset($incompatible)) {
        echo $this->renderPartial("co2.views.aap.partials.incopatible", ["params" => $params]);
    } else if (!empty(Yii::app()->session["userId"]) || (!empty($form["DisconnectedView"]) && $form["DisconnectedView"])) {
        if ($listAccess || (!empty($form["DisconnectedView"]) && $form["DisconnectedView"] && in_array($page , $allowedDisconnectedPage))) {
            echo $this->renderPartial("co2.views.aap.partials.header", ["params" => $params]);
            
            if (!empty($form["params"]["app"][$page]) && $form["params"]["app"][$page]) {
                echo $this->renderPartial("costum.views.custom." . $context["slug"] . ".aap." . $page, $params);
            } else {
                echo $this->renderPartial("co2.views.aap." . $page, $params);
            }
            echo $this->renderPartial("co2.views.aap.partials.footer", $params);
        } else {
            $params["msg"] = Yii::t("survey", "You are not allowed to access. You don't have the right");
            $params["icon"] = "fa-lock";
            echo $this->renderPartial("co2.views.default.unTpl", $params);
        }
    } else { ?>
        <div class="col-xs-12 text-center font-montserrat">
            <h5 class="letter-red">
                <i class="fa fa-ban"></i> <?php echo Yii::t("cooperation", "You are not logged"); ?><br>
                <small><?php echo Yii::t("cooperation", "Please login to go further"); ?></small>
                <br><br>
                <button class="btn btn-link bg-green-k" data-toggle="modal" data-target="#modalLogin">
                    <i class="fa fa-sign-in"></i> <?php echo Yii::t("cooperation", "I'm logging in"); ?>
                </button>
                <button class="btn btn-link bg-blue-k margin-left-10" data-toggle="modal" data-target="#modalRegister">
                    <i class="fa fa-plus-circle"></i> <?php echo Yii::t("cooperation", "I create my account"); ?>
                </button>
                <br><br>
                <small class="letter-blue"><i class="fa fa-check"></i> <?php echo Yii::t("cooperation", "free registration"); ?></small>
            </h5>
        </div>
        <script>
            $(function () {
                setTitle(JSON.parse(JSON.stringify(<?= json_encode($params['context']['name']) ?>)));
                $('.proposalDropdown,' +
                    '.projectDropdown,' +
                    '.projectDropdown+a,' +
                    '.organismAap a.lbh-menu-app:nth-child(2),' +
                    '.organismAap a.lbh-menu-app:nth-child(3),' +
                    '#menuTopRight .cosDyn-app').remove();
            });
        </script>
    <?php }
?>
