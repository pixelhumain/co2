<?php
    $JsCss = [
        '/plugins/kanban/kanban.css',
        '/plugins/kanban/kanban.js',
        '/plugins/jquery-confirm/jquery-confirm.min.css',
        '/plugins/jquery-confirm/jquery-confirm.min.js'
    ];
    HtmlHelper::registerCssAndScriptsFiles(
        [
            "/css/blockcms/jkanban.css",
            "/js/blockcms/jkanban.js"
        ],
        Yii::app()->getModule('costum')->assetsUrl
    );
    HtmlHelper::registerCssAndScriptsFiles(
        ["/js/kanban.js", "/js/aap/detailProject.js", "/css/kanban.css", '/css/aap/detail.css'],
        Yii::app()->getModule('co2')->assetsUrl
    );
    HtmlHelper::registerCssAndScriptsFiles($JsCss, Yii::app()->getBaseUrl(true));
    $showFilter = $showFilter ?? true;
?>
<style>
    .header-coremu {
        padding-left : 20px;
    }
    .filtre {
        display : flex;
        padding : 0 20px;
    }
    .filtre .answer-content {
        padding-right : 20px !important;
    }
    .filtre .depense-content {
        padding-right : 20px !important;
    }
    .filtre .person-content {
        padding-right : 20px !important;
    }
    .griser {
        cursor: not-allowed !important;
        background-color: #eee !important;
        opacity: 1 !important;
    }
    #kanbancoremu .kanban-footer-card .card-action{
        font-size : 14px !important;
    }
</style>

<div class="kanban-coremu" data-name="kanban">
    <div class="header-coremu">
        <h4>kanban en corému</h4>
    </div>
    <!-- <div id="kanbancoremu">
    </div> -->
</div>
<div class="container-fluid propsDetailPanelContainer">
    <div id="action-modal-preview"></div>
    <div class="row">
        <?php if ($showFilter === true || $showFilter === 'true') { ?>
            <div class="col-sm-2 pos-sticky allPropsPanel aap-nav-panel no-padding hidden-xs">
                <h4 class="d-flex justify-content-between col-xs-12 pt-2 pb-32 border-bottom-default">
                    <span class="title propsPanelTitle"><?php echo Yii::t("common", "Projects") ?></span>
                    <i id="btnHideFilterProps" class="fa fa-angle-double-left cursor-pointer custom-font pr-2"></i>
                </h4>
                <div class="accordion" id="proposalDetailAccord">
                    <div class="panel proposListPanel panel-default mb-0">
                        <div id="propsDetail" class="filterContainerD filter-alias custom-filter searchBar-filters pull-left set-width mx-4 mb-4">
                            <input type="text" type="text" class="form-control pull-left text-center alias-main-search-bar search-bar" data-field="text" placeholder="<?php echo Yii::t('common', "Search by") ?> <?php echo ucfirst(Yii::t('common', "Project name")) ?>">
                            <span class="text-white input-group-addon pull-left main-search-bar-addon " data-field="text" data-icon="fa-search">
                                <i class="fa fa-search"></i>
                            </span>
                        </div>
                        <div id="listPropsPanel" class="mt-2 in col-xs-12" style="height: 62rem; overflow-y: auto" aria-labelledby="headingTwo" data-parent="#proposalDetailAccord">
                            <div class="aap-results-container no-padding">
                                <div class="col-xs-12 row no-padding" id="dropdown_projet" style="min-height: 45vh; position: relative;">
                                
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-2 pos-sticky allFilterPanel aap-nav-panel no-padding hidden-xs" style="display: none;">
                <h4 class="d-flex justify-content-between col-xs-12 pt-2 pb-32 border-bottom-default">
                    <span class="title propsPanelTitle"><?php echo Yii::t("form", "Filters") ?> </span>
                    <i id="btnHideFilterOnly" class="fa fa-angle-double-left cursor-pointer custom-font pr-2 showHide-filters-xs">
                        <span class="filter-xs-count topbar-badge badge animated bounceIn badge-tranparent badge-success filter-pastille" id="detailFilterCount"></span>
                    </i>
                </h4>
                <div class="accordion px-0" id="proposalFilterAccord">
                    <div class="panel filterPanel panel-default mb-0">
                        <!-- <div class="panel-title collapse-title showHide-filters-xs pb-0 d-flex justify-content-between mb-0" id="filterL"
                            data-toggle="collapse" data-target="#filterLCollapse" aria-expanded="true" aria-controls="filterLCollapse">
                            <a href="javascript:;" class="collapse-title pb-0"><?php echo Yii::t("form", "Filters") ?></a>
                            <i class="fa fa-caret-up"></i>
                            <span class="filter-xs-count topbar-badge badge animated bounceIn badge-tranparent badge-success"></span>
                        </div> -->

                        <div id="filterLCollapse" class="in" aria-labelledby="filterL" data-parent="#proposalFilterAccord" style="height: 73.7rem;">
                            <div id="filterContainerD" class="aap-filter"></div>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
            <div class="<?php echo filter_var($showFilter, FILTER_VALIDATE_BOOLEAN) ? 'col-sm-10' : 'col-sm-12' ?> col-xs-12 propsDetailPanel no-padding">
                <!-- Nav tabs -->
                
                <ul class="nav nav-tabs aap-nav-tabs  pos-sticky bg-aap-white zi-2" role="tablist">
                    <?php if (filter_var($showFilter, FILTER_VALIDATE_BOOLEAN)) { ?>
                        <li class="hidden-xs">
                            <a href="javascript:;" class="openPropsList tooltips" data-toggle="tooltip" data-html="true" data-placement="right" data-original-title="<?php echo Yii::t("common", "Show/hide filter and projects") ?>">
                                <i class="fa fa-sliders"></i>
                            </a>
                        </li>
                    <?php } ?>
                    <li class="active">
                        <a class="need-reload" href="#action" role="tab" data-toggle="tab" data-project="<?= $projectId ?? '' ?>" data-answer="<?= $answerId ?? '' ?>">Kanban</a>
                    </li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div class="tab-pane aap-list-container active" id="action">
                        <div id="kanban-container1" style="height: 685px;">
                            <div class="filtre">
                                <!-- <div class="answer-content">
                                    <label for=""><i class="fa fa-tasks"> </i> Projet(s) :</label></br>
                                    <select class="" id="answerId">
                                        <option value="">Selectionner un projet</option>
                                    </select>
                                </div> -->
                                <div class="depense-content">
                                    <label for=""><i class="fa fa-check-square-o"> </i> Etape(s) :</label></br>
                                    <select class="griser" id="depenseId" disabled="disabled">
                                        <option value="">Les etapes du projet</option>
                                    </select>
                                </div>
                                <div class="person-content">
                                    <label for=""><i class="fa fa-user"> </i> Candidat(s) :</label></br>
                                    <select class="griser" id="personId" disabled="disabled">
                                        <option value="">Les candidats</option>
                                    </select>
                                </div>
                            </div>
                            <div id="kanbancoremu">

                            </div>
                        </div>
                    </div>
                    <!-- <div class="tab-pane aap-list-container need-reload" id="plan"></div>
                    <div class="tab-pane aap-list-container" id="funding"></div>
                    <div class="tab-pane aap-list-container" id="contribution"></div>
                    <div class="tab-pane aap-list-container" id="summary"></div>
                    <div class="tab-pane aap-list-container" id="evaluation"></div> -->
                </div>
            </div>
    </div>
</div>

<script>   
$(function () {
    alert('aaaz');
    $("#propsDetail.filterContainerD .alias-main-search-bar").off().on("keyup", function (e) {
        if (e.keyCode != 13) {
            aapObj.common.filterItems($(this), false, "#dropdown_projet", "a.props-item", "#filterContainerD");
        }

    })
    $("#btnHideFilterProps").off('click').on('click', function (e) {
        e.stopImmediatePropagation();
        let selector = ".allPropsPanel";
        $(".allPropsPanel").is(":visible") && $(".allFilterPanel").is(":visible") ? selector = ".aap-nav-panel" : "";
        $(selector).hide(300, function () {
            $(".propsDetailPanel").removeClass('col-xs-10 col-xs-8 col-sm-10 col-sm-8').addClass('col-xs-12');
            $(".openPropsList i").removeClass('fa-sliders fa-times').addClass('fa-list')
        });
    });
    $("#btnHideFilterOnly").off('click').on('click', function (e) {
        e.stopImmediatePropagation();
        $(".allFilterPanel").hide(300, function () {
            $(".propsDetailPanel").removeClass('col-xs-12 col-sm-12 col-sm-8').addClass('col-xs-10 col-sm-10');
            $(".openPropsList i").removeClass('fa-times').addClass('fa-sliders')
        });
    });
    $(".openPropsList").off('click').on('click', function (e) {
        alert('aaaz');
        e.stopImmediatePropagation();
        $(".tooltip").remove();
        $(this).removeAttr("aria-describedby")
        if ($(".allPropsPanel").is(":visible") && $(".allFilterPanel").is(":visible")) {
            $(".aap-nav-panel").hide(300, function () {
                $(".propsDetailPanel").removeClass('col-xs-12 col-xs-8 col-sm-12 col-sm-8').addClass('col-xs-12');
                $(".openPropsList i").removeClass('fa-sliders fa-times').addClass('fa-list')
            });
        }
        // else if ($(".allPropsPanel").is(":visible")) {
        //     $(".propsDetailPanel").removeClass('col-xs-10 col-sm-10').addClass('col-xs-8 col-sm-8');
        //     $(".openPropsList i").removeClass('fa-sliders').addClass('fa-times');
        //     $(".allFilterPanel").show(300);
        // } 
        else {
            $(".propsDetailPanel").removeClass('col-xs-12 col-sm-12').addClass('col-xs-10 col-sm-10')
            $(".openPropsList i").removeClass('fa-list').addClass('fa-sliders')
            $(".allPropsPanel").show(300);
        }
    });
    var queryParams = aapObj.common.getQuery();
    mylog.log(queryParams, 'queryParams')
    var showFilter = <?= isset($showFilter) ? json_encode($showFilter) : "true"  ?>;
    const isInsideModal = <?= isset($insideModal) ? json_encode(filter_var($insideModal, FILTER_VALIDATE_BOOLEAN)) : json_encode(false)  ?>;
    var defaultView = JSON.parse(JSON.stringify(<?= json_encode($aapview ?? 'action') ?>));
    $('#depenseId').select2({
        width : '250px'
    });
    $('#personId').select2({
        width : '250px'
    });
    var headerscoremu = [
        { id: 'totraite', label: 'A traiter' , editable : false,
            menus: []
        },
        { id: 'validated', label: 'Valider' , editable : false,
            menus: []
        },
        { id: 'notvalidate', label: 'Non valider' , editable : false,
            menus: []
        }
    ]
    var datacoremu = []
    var answercoremu = {}
    var answerInfo = {}
    
    function getAnswer(){
        let params = {
            'filters[form]' : aapObj.form._id.$id,
            'filters[answers.aapStep1.titre][$exists]' : 'true',
            'searchType[]' : "answers",
            'notSourceKey' : "true",
            'indexStep' : "0",
            'fields[]' : ['name', 'tags', 'extraInfo', 'answers'],
            'costumSlug' : costum.slug,
            'filters[status][$not]' : "finish",
        }

        if(typeof aapObj.form != "undefined" && typeof aapObj.form.actualSession != "undefined") {
            params['filters[answers.aapStep1.year]'] = aapObj.form.actualSession
            params['filters[project.id][$exists]'] = 'false'
        }

        ajaxPost(
            null,
            baseUrl+"/" + moduleId + "/search/globalautocomplete",
            params,
            function(data){
                answercoremu = data.results;
            },null,null, {async:false}
        )
        mylog.log(answercoremu , Object.keys(answercoremu).length, 'answercoremu')
        let typee = '';
        $.map( answercoremu , function( valeur, keys ) { 
            typee = '';
            if(typeof valeur.answers != "undefined" && typeof valeur.answers.aapStep1 != "undefined" && typeof valeur.answers.aapStep1.titre != "undefined"){
                
                if(typeof valeur.answers.aapStep1.depense != "undefined" && Array.isArray(valeur.answers.aapStep1.depense)){

                    for(let i = 0; i < valeur.answers.aapStep1.depense.length ; i++){ 
                        if (typeof valeur.answers.aapStep1.depense[i].estimates != 'undefined' && valeur.answers.aapStep1.depense[i].estimates != null){
                            $.map(valeur.answers.aapStep1.depense[i].estimates, function(val, key){
                                if(key.length == 24 && typeof val.AssignBudgetArray != 'undefined' && val.AssignBudgetArray != null){
                                    if(Array.isArray(val.AssignBudgetArray)) {
                                        for(let j = 0 ; j < val.AssignBudgetArray.length ; j++) {
                                           
                                            if (typeof val.AssignBudgetArray[j].check == 'undefined') {
                                                typee = '#d71717'
                                            } 
                                            if(typeof val.AssignBudgetArray[j].check != 'undefined' && (val.AssignBudgetArray[j].check == 'false' || val.AssignBudgetArray[j].check == false || val.AssignBudgetArray[j].check == 'non')) {
                                                typee = '#d71717'
                                            } 
                                        }
                                    } else {
                                        $.map(val.AssignBudgetArray, function(v , k) {
                                            if(typeof v.check == 'undefined') {
                                                typee = '#d71717'
                                            } 
                                            if(typeof v.check != 'undefined' && (v.check == 'false' || v.check == false || v.check == 'non')) {
                                                typee = '#d71717';
                                            } 
                                        })

                                    }
                                    
                                } 
                            })
                        } 
                    }
                    
                }
                       
                let iteme = `
                            <a class="col-xs-12 props-item item-projet d-flex justify-content-between" 
                                id="propItem${keys}" href="javascript:;" 
                                data-title="${valeur.answers.aapStep1.titre}" data-targety="${keys}" 
                                data-answer-id="${keys}" style="color : ${typee}">
                                <span class="text-ellipsis hover-marquee">${valeur.answers.aapStep1.titre}</span>
                            </a>`
                $('#dropdown_projet').append(iteme)
            }
        })
        
    }
    getAnswer()

    $('.item-projet').on('click', function(){
        $('.item-projet').removeClass('active');
        let projetId = $(this).data('targety');
        $(this).addClass('active');

        $('#depenseId').attr('disabled', 'disabled');
        $('#depenseId').addClass('griser');
        let html = '<option value="">Aucune etape dans cet projet</option>'
        $('#depenseId').html(html);
        $('#depenseId').select2({
            width : '250px'
        });
        getDepense(projetId)
        getAllAction(projetId);
    })

    function getDepense(idAnswer){        
        $.map( answercoremu , function( valeur, keys ) { 
            if(keys == idAnswer) {
                let html = '<option value="">Selectionner une etape</option>'
                if(typeof valeur.answers != "undefined" && typeof valeur.answers.aapStep1 != "undefined" && typeof valeur.answers.aapStep1.depense != "undefined" && Array.isArray(valeur.answers.aapStep1.depense)){
                    for(let i = 0; i < valeur.answers.aapStep1.depense.length ; i++){ 
                        let type = ''
                        if(typeof valeur.answers.aapStep1.depense[i].estimates != 'undefined' && valeur.answers.aapStep1.depense[i].estimates != null) {
                            $.map(valeur.answers.aapStep1.depense[i].estimates, function(val, key){
                                if(key.length == 24 && typeof val.AssignBudgetArray != 'undefined' && val.AssignBudgetArray != null){
                                    if(Array.isArray(val.AssignBudgetArray)) {
                                        for(let j = 0 ; j < val.AssignBudgetArray.length ; j++) {
                                            
                                            if (typeof val.AssignBudgetArray[j].check == 'undefined') {
                                                type = '#d71717'
                                            } 
                                            if(typeof val.AssignBudgetArray[j].check != 'undefined' && (val.AssignBudgetArray[j].check == 'false' || val.AssignBudgetArray[j].check == false || val.AssignBudgetArray[j].check == 'non')) {
                                                type = '#d71717'
                                            } 
                                        }
                                    } else {
                                        $.map(val.AssignBudgetArray, function(v , k) {
                                            if(typeof v.check == 'undefined') {
                                                type = '#d71717'
                                            } 
                                            if(typeof v.check != 'undefined' && (v.check == 'false' || v.check == false || v.check == 'non')) {
                                                type = '#d71717';
                                            } 
                                        })

                                    }
                                    
                                } 
                            })
                        }
                        if (typeof valeur.answers.aapStep1.depense[i].poste != 'undefined' ){
                            let poste = valeur.answers.aapStep1.depense[i].poste
                            html += '<option value="'+poste+'" style ="color : '+type+' !important">'+poste+'</option>'
                            $('#depenseId').html(html)
                            $('#depenseId').select2({
                                width : '250px'
                            });
                        }
                    }
                    if (html != '<option value="">Selectionner une etape</option>') {
                        $('#depenseId').removeAttr('disabled');
                        $('#depenseId').removeClass('griser');
                        $('#personId').html('<option value="">Les candidats dans une etape</option>');
                        $('#personId').select2({
                            width : '250px'
                        });
                        $('#personId').attr('disabled', 'disabled');
                        $('#personId').addClass('griser');
                    }
                    
                }
            }
        })
    }

    $('#answerId').on('change', function(){
        $('#depenseId').attr('disabled', 'disabled');
        $('#depenseId').addClass('griser');
        let html = '<option value="">Aucune etapes dans cet projet</option>'
        $('#depenseId').html(html);
        $('#depenseId').select2({
            width : '250px'
        });
        let id = $('#answerId').val();
        getDepense(id)
        getAllAction(id)
    })

    function getPerson(idAnswer, idDepense){  
        let html = '<option value="">Selectionner un candidat</option>'      
        $.map( answercoremu , function( valeur, keys ) { 
            if(keys == idAnswer) {
                if(typeof valeur.answers != "undefined" && typeof valeur.answers.aapStep1 != "undefined" && typeof valeur.answers.aapStep1.depense != "undefined" && Array.isArray(valeur.answers.aapStep1.depense)){
                    for(var i = 0; i < valeur.answers.aapStep1.depense.length ; i++){ 
                        if (typeof valeur.answers.aapStep1.depense[i].poste != 'undefined' && valeur.answers.aapStep1.depense[i].poste == idDepense && typeof valeur.answers.aapStep1.depense[i].estimates != 'undefined' && valeur.answers.aapStep1.depense[i].estimates != null){
                            $.map(valeur.answers.aapStep1.depense[i].estimates, function(v, k){
                                if(k.length == 24 && v != null &&  typeof v != 'undefined' && typeof v.name != 'undefined') {
                                    html += `<option value="${k}">${v.name}</option>`
                                    $('#personId').html(html)
                                    $('#personId').select2({
                                        width : '250px'
                                    });
                                    $('#personId').removeAttr('disabled');
                                    $('#personId').removeClass('griser');
                                }
                            })
                            
                        }
                    }
                    
                }
            }
        })
        
    }
    
    $('#dropdown_projet a:first').addClass('active')
    if($('#dropdown_projet a:first').hasClass('active')) {
        getAllAction($('#dropdown_projet .active').data('targety'))
        getDepense($('#dropdown_projet .active').data('targety'))
    }
    
    $('#depenseId').on('change', function(){
        $('#personId').attr('disabled', 'disabled');
        $('#personId').addClass('griser');
        let html = '<option value="">Aucun candidat dans cette etape</option>'
        $('#kanbancoremu').html('')
        $('#personId').html(html);
        $('#personId').select2({
            width : '250px'
        });
        let answerId = $('#dropdown_projet .active').data('targety')
        let depenseId = $('#depenseId').val();
        
        getPerson(answerId, depenseId)
        getAction(answerId, depenseId)
    })

    function getDataCoremu(idAnswer, idDepense, idPerson){ 
        datacoremu = []       
        $('#kanbancoremu').kanban('destroy')
        $.map( answercoremu , function( valeur, keys ) { 
            if(keys == idAnswer) {
                let html = '<option value="">Selectionner un person</option>'
                if(typeof valeur.answers != "undefined" && typeof valeur.answers.aapStep1 != "undefined" && typeof valeur.answers.aapStep1.depense != "undefined" && Array.isArray(valeur.answers.aapStep1.depense)){
                    for(var i = 0; i < valeur.answers.aapStep1.depense.length ; i++){ 
                        if (typeof valeur.answers.aapStep1.depense[i].poste != 'undefined' && valeur.answers.aapStep1.depense[i].poste == idDepense && typeof valeur.answers.aapStep1.depense[i].estimates != 'undefined' && valeur.answers.aapStep1.depense[i].estimates != null){
                            $.map(valeur.answers.aapStep1.depense[i].estimates, function(val, key){
                                if(key == idPerson && typeof val.AssignBudgetArray != 'undefined' && val.AssignBudgetArray != null){
                                    if(Array.isArray(val.AssignBudgetArray)) {
                                        for(let j = 0 ; j < val.AssignBudgetArray.length ; j++) {
                                            let objet = {
                                                _id : val.AssignBudgetArray[j].label,
                                                id : val.AssignBudgetArray[j].label,
                                                title : "<b>"+val.AssignBudgetArray[j].label+"</b>",
                                                header : 'totraite',
                                                uid : key,
                                                posdepense : i,
                                                posarray : j,
                                                idaction : keys,
                                                etape : valeur.answers.aapStep1.depense[i].poste,
                                                html : true,
                                                name : val.AssignBudgetArray[j].label,
                                                actions : [
                                                    {
                                                        'icon': 'fa fa-money' ,
                                                        'bstooltip' : {
                                                            'text' : '', 
                                                            'position' : 'left'
                                                        }, 
                                                        'action': '',
                                                        'badge' : val.AssignBudgetArray[j].price
                                                    },
                                                ]
                                            }
                                            if(typeof val.AssignBudgetArray[j].check != 'undefined' && (val.AssignBudgetArray[j].check == 'true' || val.AssignBudgetArray[j].check == true)) {
                                                objet.header = 'validated'
                                            } 
                                            if(typeof val.AssignBudgetArray[j].check != 'undefined' && (val.AssignBudgetArray[j].check == 'false' || val.AssignBudgetArray[j].check == false || val.AssignBudgetArray[j].check == 'non')) {
                                                objet.header = 'notvalidate'
                                            } 
                                            if(typeof val.AssignBudgetArray[j].hourlyRate != 'undefined' && val.AssignBudgetArray[j].hourlyRate != '') {
                                                let hourlyRate = {
                                                    'icon': '' ,
                                                    'bstooltip' : {
                                                        'text' : 'argent par heure', 
                                                        'position' : 'left'
                                                    }, 
                                                    'action': '',
                                                    'badge' : val.AssignBudgetArray[j].hourlyRate+' <i class="fa fa-euro"></i>/h'
                                                }
                                                objet.actions.push(hourlyRate)
                                            }
                                            if(typeof val.AssignBudgetArray[j].hour != 'undefined' && val.AssignBudgetArray[j].hour != '' && val.AssignBudgetArray[j].hour != '0' && val.AssignBudgetArray[j].hour != 0) {
                                                let hour = {
                                                    'icon': '' ,
                                                    'bstooltip' : {
                                                        'text' : 'heure', 
                                                        'position' : 'left'
                                                    }, 
                                                    'action': '',
                                                    'badge' : val.AssignBudgetArray[j].hour+' h'
                                                }
                                                objet.actions.push(hour)
                                            }
                                            datacoremu.push(objet)
                                            mylog.log(datacoremu, 'datacoremu')
                                        }
                                        $('#kanbancoremu').html('')
                                        loadKanban()
                                    } else {
                                        $.map(val.AssignBudgetArray, function(v , k) {
                                            let objet = {
                                                _id : v.label,
                                                id : v.label,
                                                title : "<b>"+v.label+"</b>",
                                                uid : key,
                                                posdepense : i,
                                                posarray : k,
                                                idaction : keys,
                                                etape : valeur.answers.aapStep1.depense[i].poste,
                                                header : 'totraite',
                                                html : true,
                                                name : v.label,
                                                actions : [
                                                    {
                                                        'icon': 'fa fa-money' ,
                                                        'bstooltip' : {
                                                            'text' : 'Prix', 
                                                            'position' : 'left'
                                                        }, 
                                                        'action': '',
                                                        'badge' : v.price
                                                    },
                                                ]
                                            }
                                            if(typeof v.check != 'undefined' && (v.check == 'true' || v.check == true)) {
                                                objet.header = 'validated'
                                            } 
                                            if(typeof v.check != 'undefined' && (v.check == 'false' || v.check == false || v.check == 'non')) {
                                                objet.header = 'notvalidate'
                                            } 
                                            if(typeof v.hourlyRate != 'undefined' && v.hourlyRate != '') {
                                                let hourlyRate = {
                                                    'icon': '' ,
                                                    'bstooltip' : {
                                                        'text' : 'argent par heure', 
                                                        'position' : 'left'
                                                    }, 
                                                    'action': '',
                                                    'badge' : v.hourlyRate+' <i class="fa fa-euro"></i>/h'
                                                }
                                                objet.actions.push(hourlyRate)
                                            }
                                            if(typeof v.hour != 'undefined' && v.hour != '' && v.hour != '0' && v.hour != 0) {
                                                let hour = { 
                                                    'icon': '' ,
                                                    'bstooltip' : {
                                                        'text' : 'heure', 
                                                        'position' : 'left'
                                                    }, 
                                                    'action': '',
                                                    'badge' : v.hour+' h'
                                                }
                                                objet.actions.push(hour)
                                            }
                                            datacoremu.push(objet)
                                            mylog.log(datacoremu, 'datacoremu')
                                        })
                                        $('#kanbancoremu').html('')
                                        loadKanban()
                                    }
                                    
                                } else if (key == idPerson && typeof val.AssignBudgetArray == 'undefined') {
                                    let vide = `<div class="text-center"><h2>Pas de corémunération</h2></div>`
                                    $('#kanbancoremu').html(vide)
                                }
                            })
                        }
                    }
                    
                }
            }
        })
    }

    $('#personId').on('change', function(){
        let answerId = $('#dropdown_projet .active').data('targety')
        let depenseId = $('#depenseId').val();
        let personId = $(this).val();
        getDataCoremu(answerId, depenseId, personId)
    })

    function getAllAction(idAnswer){ 
        datacoremu = []       
        $('#kanbancoremu').kanban('destroy')
        $.map( answercoremu , function( valeur, keys ) { 
            if(keys == idAnswer) {
                let html = '<option value="">Selectionner un person</option>'
                if(typeof valeur.answers != "undefined" && typeof valeur.answers.aapStep1 != "undefined" && typeof valeur.answers.aapStep1.depense != "undefined" && Array.isArray(valeur.answers.aapStep1.depense)){
                    for(var i = 0; i < valeur.answers.aapStep1.depense.length ; i++){ 
                        mylog.log(i, 'iii')
                        if (typeof valeur.answers.aapStep1.depense[i].poste != 'undefined' && typeof valeur.answers.aapStep1.depense[i].estimates != 'undefined' && valeur.answers.aapStep1.depense[i].estimates != null){
                            $.map(valeur.answers.aapStep1.depense[i].estimates, function(val, key){
                                if(key.length == 24 && typeof val.AssignBudgetArray != 'undefined' && val.AssignBudgetArray != null){
                                    if(Array.isArray(val.AssignBudgetArray)) {
                                        for(let j = 0 ; j < val.AssignBudgetArray.length ; j++) {
                                            let objet = {
                                                _id : val.AssignBudgetArray[j].label,
                                                id : val.AssignBudgetArray[j].label,
                                                title : "<b>"+val.AssignBudgetArray[j].label+"</b>",
                                                header : 'totraite',
                                                uid : key,
                                                posdepense : i,
                                                posarray : j,
                                                idaction : keys,
                                                etape : valeur.answers.aapStep1.depense[i].poste,
                                                html : true,
                                                name : val.AssignBudgetArray[j].label,
                                                actions : [
                                                    {
                                                        'icon': 'fa fa-check-square-o' ,
                                                        'bstooltip' : {
                                                            'text' : '', 
                                                            'position' : 'left'
                                                        }, 
                                                        'action': '',
                                                        'badge' : valeur.answers.aapStep1.depense[i].poste
                                                    },
                                                ]
                                            }
                                            if(typeof val.AssignBudgetArray[j].check != 'undefined' && (val.AssignBudgetArray[j].check == 'true' || val.AssignBudgetArray[j].check == true)) {
                                                objet.header = 'validated'
                                            } 
                                            if(typeof val.AssignBudgetArray[j].check != 'undefined' && (val.AssignBudgetArray[j].check == 'false' || val.AssignBudgetArray[j].check == false || val.AssignBudgetArray[j].check == 'non')) {
                                                objet.header = 'notvalidate'
                                            } 
                                            datacoremu.push(objet)
                                            mylog.log(datacoremu, 'datacoremu')
                                        }
                                        
                                    } else {
                                        $.map(val.AssignBudgetArray, function(v , k) {
                                            let objet = {
                                                _id : v.label,
                                                id : v.label,
                                                title : "<b>"+v.label+"</b>",
                                                header : 'totraite',
                                                html : true,
                                                uid : key,
                                                posdepense : i,
                                                posarray : k,
                                                idaction : keys,
                                                etape : valeur.answers.aapStep1.depense[i].poste,
                                                name : v.label,
                                                actions : [
                                                    {
                                                        'icon': 'fa fa-check-square-o' ,
                                                        'bstooltip' : {
                                                            'text' : '', 
                                                            'position' : 'left'
                                                        }, 
                                                        'action': '',
                                                        'badge' : valeur.answers.aapStep1.depense[i].poste
                                                    },
                                                ]
                                            }
                                            if(typeof v.check != 'undefined' && (v.check == 'true' || v.check == true)) {
                                                objet.header = 'validated'
                                            } 
                                            if(typeof v.check != 'undefined' && (v.check == 'false' || v.check == false || v.check == 'non')) {
                                                objet.header = 'notvalidate'
                                            } 
                                            datacoremu.push(objet)
                                            mylog.log(datacoremu, 'datacoremu')
                                        })
                                        
                                    }
                                    
                                } 
                            })
                        }
                    }
                    
                }
            }
        })
        loadKanban()
    }

    function getAction(idAnswer, idDepense){ 
        datacoremu = []       
        $('#kanbancoremu').kanban('destroy')
        $.map( answercoremu , function( valeur, keys ) { 
            if(keys == idAnswer) {
                let html = '<option value="">Selectionner un person</option>'
                if(typeof valeur.answers != "undefined" && typeof valeur.answers.aapStep1 != "undefined" && typeof valeur.answers.aapStep1.depense != "undefined" && Array.isArray(valeur.answers.aapStep1.depense)){
                    for(var i = 0; i < valeur.answers.aapStep1.depense.length ; i++){ 
                        if (typeof valeur.answers.aapStep1.depense[i].poste != 'undefined' && valeur.answers.aapStep1.depense[i].poste == idDepense && typeof valeur.answers.aapStep1.depense[i].estimates != 'undefined'){
                            $.map(valeur.answers.aapStep1.depense[i].estimates, function(val, key){
                                if(key.length == 24 && typeof val.AssignBudgetArray != 'undefined' && val.AssignBudgetArray != null){
                                    if(Array.isArray(val.AssignBudgetArray)) {
                                        for(let j = 0 ; j < val.AssignBudgetArray.length ; j++) {
                                            let objet = {
                                                _id : val.AssignBudgetArray[j].label,
                                                id : val.AssignBudgetArray[j].label,
                                                title : "<b>"+val.AssignBudgetArray[j].label+"</b>",
                                                header : 'totraite',
                                                uid : key,
                                                posdepense : i,
                                                posarray : j,
                                                idaction : keys,
                                                etape : valeur.answers.aapStep1.depense[i].poste,
                                                html : true,
                                                name : val.AssignBudgetArray[j].label,
                                                actions : [
                                                    {
                                                        'icon': 'fa fa-user' ,
                                                        'bstooltip' : {
                                                            'text' : '', 
                                                            'position' : 'left'
                                                        }, 
                                                        'action': '',
                                                        'badge' : val.name
                                                    },
                                                ]
                                            }
                                            if(typeof val.AssignBudgetArray[j].check != 'undefined' && (val.AssignBudgetArray[j].check == 'true' || val.AssignBudgetArray[j].check == true) ) {
                                                objet.header = 'validated'
                                            } 
                                            if(typeof val.AssignBudgetArray[j].check != 'undefined' && (val.AssignBudgetArray[j].check == 'false' || val.AssignBudgetArray[j].check == false || val.AssignBudgetArray[j].check == 'non')) {
                                                objet.header = 'notvalidate'
                                            } 
                                            datacoremu.push(objet)
                                            mylog.log(datacoremu, 'datacoremu')
                                        }
                                    } else {
                                        $.map(val.AssignBudgetArray, function(v , k) {
                                            let objet = {
                                                _id : v.label,
                                                id : v.label,
                                                title : "<b>"+v.label+"</b>",
                                                header : 'totraite',
                                                html : true,
                                                uid : key,
                                                posdepense : i,
                                                posarray : k,
                                                idaction : keys,
                                                etape : valeur.answers.aapStep1.depense[i].poste,
                                                name : v.label,
                                                actions : [
                                                    {
                                                        'icon': 'fa fa-user' ,
                                                        'bstooltip' : {
                                                            'text' : '', 
                                                            'position' : 'left'
                                                        }, 
                                                        'action': '',
                                                        'badge' : val.name
                                                    },
                                                ]
                                            }
                                            if(typeof v.check != 'undefined' && (v.check == 'true' || v.check == true)) {
                                                objet.header = 'validated'
                                            } 
                                            if(typeof v.check != 'undefined' && (v.check == 'false' || v.check == false || v.check == 'non')) {
                                                objet.header = 'notvalidate'
                                            } 
                                            datacoremu.push(objet)
                                            mylog.log(datacoremu, 'datacoremu')
                                        })
                                        
                                    }
                                    
                                }
                            })
                        }
                    }
                    
                }
            }
        })
        loadKanban()
    }

    mylog.log(answerInfo , 'answerInfo')
    function loadKanban(){
        mylog.log('loadKanban')
        let kanbanDomcoremu = $('#kanbancoremu')
        .kanban({
            headers : headerscoremu,
            data : datacoremu,
            editable: false,
            language : mainLanguage,
            canAddCard: false,
            editable : true,
            canAddColumn: false,
            canEditCard: false,
            canEditHeader: false,
            canMoveCard: true,
            canMoveColumn: true,
            readonlyHeaders: [],
            copyWhenDragFrom: [],
            endpoint : `${baseUrl}/plugins/kanban/`,
            defaultColumnMenus: [''],
            onRenderDone(){ 
                $('.kanban-list-card-action').css({
                    'display' : 'none'
                })
            },
            onCardDrop(dropped, update){
                console.log(dropped,update, 'dropped')
                if(dropped.target != dropped.origin) {
                    let params = {
                        'pos' : dropped.data.posdepense,
                        'uid' : dropped.data.uid,
                        'collection' : 'answers',
                        'id' : dropped.data.idaction,
                        'key' : 'depense',
                        'form' : 'aapStep1',
                        'id' : dropped.data.idaction,
                        'assignBudget' : {},
                        'unumber' : dropped.data.posarray,
                        'action' : 'false',
                        'value' : '',
                        'path' : 'answers.aapStep1.depense.'+dropped.data.posdepense+'.estimates.'+dropped.data.uid+'.AssignBudgetArray.'+dropped.data.posarray+'.check',
                        'costumSlug' : costum.slug,
                        'costumEditMode' : 'false',
                    }

                    if(dropped.target == 'totraite') {
                        params.value = ''
                    } else if(dropped.target == 'notvalidate') {
                        params.value = 'non'
                    } else if(dropped.target == 'validated') {
                        params.value = 'true'
                    } 

                    $.map( answercoremu , function( valeur, keys ) { 
                        if(keys == dropped.data.idaction) {
                            if(typeof valeur.answers != "undefined" && typeof valeur.answers.aapStep1 != "undefined" && typeof valeur.answers.aapStep1.depense != "undefined" && Array.isArray(valeur.answers.aapStep1.depense)){
                                if (typeof valeur.answers.aapStep1.depense[dropped.data.posdepense].estimates != 'undefined'){
                                    $.map(valeur.answers.aapStep1.depense[dropped.data.posdepense].estimates, function(val, key){
                                        if(key == dropped.data.uid && typeof val.AssignBudgetArray != 'undefined' && val.AssignBudgetArray != null){
                                            if(Array.isArray(val.AssignBudgetArray)) {
                                                for(let j = 0 ; j < val.AssignBudgetArray.length ; j++) {
                                                    params["assignBudget"][j] = val.AssignBudgetArray[j]
                                                }
                                            } else {
                                                params["assignBudget"] = val.AssignBudgetArray
                                            }
                                        }
                                    })
                                }
                            }
                        }
                    })

                    dataHelper.path2Value(params, function (__data) {
                        if (__data['result']) {
                            toastr.success('Action deplacer avec succes', {
                                closeButton : true,
                                preventDuplicates : true,
                                timeOut : 3000
                            });
                        }
                    },baseUrl);
                    mylog.log(params, 'params')
                }
            }
        })
    }
})
</script>