<div class="container-fluid myactions-container">

</div>
<script>
    coInterface.showLoader(".myactions-container", trad.currentlyloading);
    $(function () {
        if (notEmpty(aapObj.defaultProject)) {
            var hashParams = {
                context : aapObj.context.slug,
                formid : aapObj.form._id.$id,
                projectId : aapObj.defaultProject._id.$id,
                answerId : notEmpty(aapObj.defaultAnswer) ? aapObj.defaultAnswer._id.$id : '',
                userId : notEmpty(userId) ? userId : ''
            };
            urlCtrl.loadByHash(aapObj.common.buildUrlQuery('#detailProjectAap', hashParams, aapObj.common.getQueryStringObject()));
        } else {
            $('.myactions-container').html(`
            <h2 class="text-center aap-text-primary">${"vous n'avez aucun projet associé à "}${aapObj.context.name}</h2>`
            );
        }
    })
</script>