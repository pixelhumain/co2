<?php
    $params = [
        "form" => $form,
        "context" => $context,
        "page" => $page,
        "pageDetail" => $pageDetail,
        "canEdit" => $canEdit,
        "canParticipate" => $canParticipate,
        "canSee" => $canSee,
        "status"  => $status,
        "inputs" => $inputs,
        "aapview" => $aapview ?? null,
        "subOrganizations" => $subOrganizations ?? null,
        "answer" => $answer ?? null,
        "answerId" => $answer ?? null,
        "project" => $project ?? null,
        "projectId" => $projectId ?? null,
        "session" => $session ?? null,
        "myProjects" => true
    ];
	echo $this->renderPartial("co2.views.aap.projects",$params);
?>