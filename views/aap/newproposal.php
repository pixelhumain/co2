<script>
    $(function(){
        const new_hash = `coformAap.context.${aapObj.context.slug}.formid.${aapObj.form._id.$id}.answerId.new${document.location.href.indexOf('?') > -1 ? '?' + document.location.href.split('?')[1] : ''}`;
        history.pushState(null, document.title, `${document.location.origin}${document.location.pathname}#${new_hash}`);
        urlCtrl.loadByHash(location.hash);
    })
</script>