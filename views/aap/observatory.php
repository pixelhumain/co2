<?php
$graphConfig = !empty($form["GraphObservatory"]) ? $form["GraphObservatory"] : [];
$cssJS = array(
    '/plugins/Chart.js/chart.4.4.0.min.js',
    "/plugins/Chart.js/chartjs-plugin-datalabels.2.0.0.js",
    '/plugins/Chart.js/chartjs-chart-sankey.0.12.0.js'
);
HtmlHelper::registerCssAndScriptsFiles($cssJS, Yii::app()->request->baseUrl);
if (is_file($this->getViewFile("costum.views.custom.".$context["slug"].".observatory"))) { 
    echo $this->renderPartial("costum.views.custom.".$context["slug"].".observatory",array("config"=>$graphConfig,"slug"=>$context["slug"]));
}else{
    echo $this->renderPartial("co2.views.aap.partials.obs",array("config"=>$graphConfig,"slug"=>$context["slug"]));
}


?>


