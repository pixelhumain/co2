<?php

use PixelHumain\PixelHumain\modules\citizenToolKit\components\UtilsHelper;

$tab_list = [
	['id'	=> 'aactl-commun', 'name'	=> Yii::t("common", "Commons")],
	['id'	=> 'aactl-tl', 'name'	=> 'TiersLieux']
];
$proposal_list = $proposal_list ?? [];
HtmlHelper::registerCssAndScriptsFiles([
	'/js/aap/jointl.js',
	"/css/franceTierslieux/answersDirectory.css",
	"/js/franceTierslieux/answerDirectory.js",
	"/js/franceTierslieux/dataviz/getElement.js",
], Yii::app()->getModule('costum')->getAssetsUrl());

// HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFiles, Yii::app()->getModule( "costum" )->getAssetsUrl());
?>
<style type="text/css">
	.dashboard-aap .close-modal .lr,
	.dashboard-aap .close-modal .lr .rl {
		height: 50px;
		width: 1px;
		background: white;
	}

	.dashboard-aap .close-modal:hover .lr,
	.dashboard-aap .close-modal:hover .lr .rl {
		width: 2px;
	}

	.dashboard-aap .close-modal .lr {
		margin-left: 28px;
		transform: rotate(45deg);
		-ms-transform: rotate(45deg);
		-webkit-transform: rotate(45deg);
	}

	.dashboard-aap .close-modal .lr .rl {
		transform: rotate(90deg);
		-ms-transform: rotate(90deg);
		-webkit-transform: rotate(90deg);

	}

	.dashboard-aap .close-modal {
		position: absolute;
		top: 5px;
		right: 5px;
		width: 55px;
		height: 50px;
		z-index: 1;
		cursor: pointer;
	}

	.dashboard-aap .profil-img {
		display: inline-block;
		width: 100%;
		height: 100%;
		object-fit: cover;
		border-radius: 5px;
		border: 2px solid white;
	}

	.dashboard-aap .btn-dash-link {
		color: white;
		font-size: 18px;
		padding: 5px 10px;
		display: block;
		text-decoration: underline;
	}

	.dashboard-aap .btn-dash-link:hover {
		color: black;
		background-color: white;
	}

	.tilsCTENAT {
		border-radius: 5px;
		padding: 10px;
		color: white;
	}

	.tilsCTENAT:hover {
		background-color: rgba(255, 255, 255, 0.88);
		color: #22252A;
	}

	.tilsCTENAT:hover .btn-dash-link {
		color: inherit;
	}

	.tilsCTENAT .img-tils {
		border-radius: 5px;
		border: 2px solid white;
		width: 90px;
		height: 90px;
		object-fit: cover;
	}

	.tilsCTENAT .title-tils {
		font-size: 18px;
		color: white;
		font-weight: 800;
	}

	.tilsCTENAT:hover .title-tils {
		color: #22252A;
	}

	.showAllList,
	.reduceList,
	.linkTODoc {
		color: #45c7dd;
		cursor: pointer;
	}

	.linkTODoc:hover,
	.tilsCTENAT .refLink {
		color: #45c7dd;
	}

	a.btn-logout-Dash {
		background-color: white;
		color: red;
		border: 1px solid red;
		text-transform: none;
		font-size: 16px;
		font-weight: 700;
	}

	a.btn-logout-Dash:hover {
		background-color: red;
		color: white;
		border: 1px solid white;
	}

	.aac-tl.panel {
		background-color: #22252A;
	}

	.aac-tl.panel .panel-title {
		cursor: pointer;
		user-select: none;
	}

	.aac-tl.tab-content {
		padding-top: 10px;
	}

	.aac-tl.nav.nav-tabs>li>a {
		font-weight: 600;
		background-color: var(--aap-primary-color);
		border-radius: 8px;
		text-decoration: none;
		border: none !important;
		font-size: 1.6rem;
	}

	.aac-tl.nav.nav-tabs>li:not(.active)>a {
		background-color: var(--green-light);
		color: var(--text-black);
	}

	@media screen and (min-width: 768px) {
		.dashboard-aap .profile-container {
			position: sticky;
			top: 50px;
		}
	}

	<?php if ($profile['mode'] === true) { ?>.dashboard-aap {
		background-color: rgb(4 11 62 / 90%) !important
	}

	<?php } ?>
</style>
<div class="col-xs-12 padding-top-50">
	<div class="close-modal" data-dismiss="modal">
		<div class="lr">
			<div class="rl">
			</div>
		</div>
	</div>
	<div class="col-md-3 col-sm-4 col-xs-12 profile-container">
		<img class="img-responsive profil-img" src="<?= $profile['image'] ?>">
		<a href="#@<?= $profile['slug'] ?>" class="lbh btn-dash-link col-xs-12">Page profil</a>
		<?php $themeParams = $this->appConfig; ?>
		<div class="about-identity text-center">
			<i class='fa fa-language text-green fs-18'></i>
			<div class="dropdown" style="display: inline-block;">
				<a href="#" class="dropdown-toggle btn-language padding-5 text-white" style="padding-top: 7px !important;" data-toggle="dropdown" role="button">
					<img src="<?php echo Yii::app()->getRequest()->getBaseUrl(true); ?>/images/flags/<?php echo Yii::app()->language ?>.png" width="22" /> <?php echo Yii::t("common", $themeParams["languages"][Yii::app()->language]); ?><span class="caret"></span></a>
				<ul class="dropdown-menu arrow_box dropdown-languages-nouser" role="menu" style="">
					<?php foreach ($themeParams["languages"] as $lang => $label)
					{ ?>
						<li><a href="javascript:;" onclick="coInterface.setLanguage('<?php echo $lang ?>', true, true)"><img src="<?php echo Yii::app()->getRequest()->getBaseUrl(true); ?>/images/flags/<?php echo $lang ?>.png" width="25" /> <?php echo Yii::t("common", $label) ?></a></li>
					<?php } ?>
				</ul>
			</div>
		</div>
		<?php if ($profile['mode'] === false) { ?>
			<a href="javascript:;" class="btn btn-logout-Dash col-xs-12 logoutBtn" style="margin-top: 10px">
				<i class="fa fa-sign-out"></i> <?= Yii::t("common", "Log Out") ?>
			</a>
		<?php } ?>
	</div>
	<div class="col-md-9 col-sm-8 col-xs-12">
		<h1 class="text-white col-xs-12" style="margin-top:0px;"><?php if ($profile['mode'] == false) { ?><?php } ?><?= $profile['name'] ?></h1>
		<ul class="aac-tl nav nav-tabs" role="tablist">
			<?php foreach ($tab_list as $tab_item): ?>
				<li class="<?= UtilsHelper::className(['active' => $tab_item['id'] === 'aactl-commun']) ?>"><a href="#tab-pane-<?= $tab_item['id'] ?>" role="tab" data-toggle="tab"><?= $tab_item['name'] ?></a></li>
			<?php endforeach; ?>
		</ul>
		<div class="aac-tl tab-content">
			<?php
			foreach ($tab_list as $tab_item):
				$i = 0;
			?>
				<div class="<?= UtilsHelper::className(['tab-pane', 'active' => $tab_item['id'] === 'aactl-commun']) ?>" id="tab-pane-<?= $tab_item['id'] ?>">
					<div class="panel-group" id="accordion-<?= $tab_item['id'] ?>">
						<?php
						$proposals = array_filter($proposal_list, fn($p) => $p['tab'] === $tab_item['id']);
						$data_length = 0;
						foreach ($proposals as $section => $proposal) :
							$data_length += count($proposal['data']);
							if (count($proposal['data']) > 0) :
						?>
								<div class="aac-tl panel panel-default">
									<div class="panel-heading" data-toggle="collapse" data-parent="#accordion-<?= $tab_item['id'] ?>" href="#aactl-<?= $section ?>">
										<h4 class="panel-title">
											<span>
												<?= $proposal['title'] ?>
											</span>
										</h4>
									</div>
									<div id="aactl-<?= $section ?>" class="panel-collapse collapse">
										<div class="panel-body">
											<?php
											foreach ($proposal['data'] as $data) :
												$i++;
											?>
												<div class="col-xs-12 tilsCTENAT shadow2">
													<div class="col-xs-12 col-sm-3 col-md-2">
														<img class="img-responsive img-tils" src="<?= $data['image'] ?>" alt="<?= str_replace('"', "'", $data['name']) ?>">
													</div>
													<div class="col-xs-12 col-sm-9 col-md-10 padding-5">
														<a href="<?= $data['url'] ?>" class="lbh col-xs-12 title-tils no-padding elipsis <?= isset($data["className"]) ? $data["className"] : "" ?>" <?= isset($data["className"]) ? "data-tl='" . htmlspecialchars(json_encode($data)) . "'" : "" ?>><?= $data['name'] ?></a><br>
														<?= $data["other"] ?? ""  ?>
														<div><?= $data['description'] ?></div>
													</div>
												</div>
											<?php endforeach; ?>
										</div>
									</div>
								</div>
							<?php
							endif;
						endforeach;
						if (!$data_length && $tab_item['id'] === 'aactl-tl'):
							?>
							<div class="join-tls-invite">
								<p style="color: white;"><?= Yii::t("common", "You don't belong to any Tiers-lieu yet. Click on the button below to join a Tiers-lieu and become an administrator") ?>.</p>
								<button class="btn btn-default"><?= Yii::t("common", "Joining a “Tiers-lieu”") ?></button>
							</div>
						<?php elseif (!$data_length && $tab_item['id'] === 'aactl-commun'): ?>
							<p style="color: white;"><?= Yii::t("common", "Here you'll find the commons you've liked, the commons you've funded and the commons you've contributed to") ?>.</p>
						<?php endif; ?>
					</div>
				</div>
			<?php endforeach; ?>
		</div>
	</div>
</div>

<div id="custom-prev-div" style="display: block;"></div>
<script type="text/javascript">
	jQuery(document).ready(function() {
		$(document).on('keydown', function(e) {
			e.stopImmediatePropagation();
			if (e.key === 'Escape') {
				$(".dashboard-aap").hide(200);
			}
		});
		coInterface.bindEvents();
		coInterface.bindLBHLinks();
		var $win = $(document); // or $box parent container
		var $box = $(".dashboard-aap");
		var $lbhBtn = $(".lbh, .lbhp, .close-modal");
		//var $log = $(".log");
		$lbhBtn.on('click', function() {
			var that;

			that = this;
			$box.hide(200);
			$lbhBtn.off("click", that);
		});
		$(".cosDyn-buttonList,.commonButtonHtml,.menu-button").on("click", function() {
			var that;

			that = this;
			$box.hide(200);
			$(".cosDyn-buttonList,.commonButtonHtml,.menu-button").off("click", that);
		});
		$(document.documentElement).on("keyup", function(e) {
			var that;

			that = this;
			if (typeof e.key === "string" && e.key.toLowerCase() === "escape") {
				$box.hide(200);
				$(document.documentElement).off("keyup", that);
			}
		});
		$('.join-tls-invite .btn').on('click', function() {
			var that;

			that = this;
			join_tl();
			$box.hide(200);
			$('.join-tls-invite .btn').off("click", that);
		})
		$(".btn-tl-link").off("click").on("click", function(e) {
			e.preventDefault();
			e.stopImmediatePropagation();
			let data = $(this).data("tl");
			showAllNetwork.loadPreview(null, data.image, data.name, data.id, 'tiersLieux', typeof data.network != "undefined" ? data.network : '', {}, '', function() {}, data.slug);
		});
		$(".btn-network-link").off("click").on("click", function(e) {
			e.preventDefault();
			e.stopImmediatePropagation();
			let data = $(this).data("tl");
			mylog.log("Data tl from network", data);
			showAllNetwork.loadPreview(null, data.image, data.name, '', 'reseaux', '', '', data.level3, function() {}, data.slug);
		});
	});
</script>