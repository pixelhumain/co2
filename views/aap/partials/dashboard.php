<style type="text/css">
    .dashboard-aap .close-modal .lr,
    .dashboard-aap .close-modal .lr .rl {
        height: 50px;
        width: 1px;
        background: white;
    }

    .dashboard-aap .close-modal:hover .lr,
    .dashboard-aap .close-modal:hover .lr .rl {
        width: 2px;
    }

    .dashboard-aap .close-modal .lr {
        margin-left: 28px;
        transform: rotate(45deg);
        -ms-transform: rotate(45deg);
        -webkit-transform: rotate(45deg);
    }

    .dashboard-aap .close-modal .lr .rl {
        transform: rotate(90deg);
        -ms-transform: rotate(90deg);
        -webkit-transform: rotate(90deg);

    }

    .dashboard-aap .close-modal {
        position: absolute;
        top: 5px;
        right: 5px;
        width: 55px;
        height: 50px;
        z-index: 1;
        cursor: pointer;
    }

    .dashboard-aap .profil-img {
        display: inline-block;
        width: 100%;
        height: 100%;
        object-fit: cover;
        border-radius: 5px;
        border: 2px solid white;
    }

    .dashboard-aap .btn-dash-link {
        color: white;
        font-size: 18px;
        padding: 5px 10px;
        border-bottom: 1px solid white;
    }

    .dashboard-aap .btn-dash-link:hover {
        color: black;
        background-color: white;
    }

    .tilsCTENAT {
        border-radius: 5px;
        padding: 10px;
        color: white;
    }

    .tilsCTENAT:hover {
        background-color: rgba(255, 255, 255, 0.88);
        color: #22252A;
    }

    .tilsCTENAT .img-tils {
        border-radius: 5px;
        border: 2px solid white;
        width: 90px;
        height: 90px;
    }

    .tilsCTENAT .title-tils {
        font-size: 18px;
        color: white;
        font-weight: 800;
    }

    .tilsCTENAT:hover .title-tils {
        color: #22252A;
    }

    .showAllList, .reduceList, .linkTODoc {
        color: #45c7dd;
        cursor: pointer;
    }

    .linkTODoc:hover, .tilsCTENAT .refLink {
        color: #45c7dd;
    }

    a.btn-logout-Dash {
        background-color: white;
        color: red;
        border: 1px solid red;
        text-transform: none;
        font-size: 16px;
        font-weight: 700;
    }

    a.btn-logout-Dash:hover {
        background-color: red;
        color: white;
        border: 1px solid white;
    }
    
    @media screen and (min-width: 768px) {
        .dashboard-aap .profile-container {
            position: sticky;
            top: 50px;
        }
    }
</style>
<div class="col-xs-12 padding-top-50">
    <div class="close-modal" data-dismiss="modal">
        <div class="lr">
            <div class="rl">
            </div>
        </div>
    </div>
    <div class="col-md-3 col-sm-4 col-xs-12 profile-container">
		<?php $imgPath = (isset(Yii::app()->session["user"]["profilMediumImageUrl"]) && !empty(Yii::app()->session["user"]["profilMediumImageUrl"])) ? Yii::app()
		                                                                                                                                                  ->createUrl(Yii::app()->session["user"]["profilMediumImageUrl"]) : Yii::app()->getModule('co2')->getParentAssetsUrl() . "/images/thumb/default_citoyens.png"; ?>
        <img class="img-responsive profil-img" src="<?php echo $imgPath ?>">
        <a href="#@<?php echo Yii::app()->session["user"]["slug"] ?>" class="lbh btn-dash-link col-xs-12">Mon profil</a>
        <a href="javascript:;" class="btn btn-logout-Dash col-xs-12 logoutBtn" style="margin-top: 10px">
            <i class="fa fa-sign-out"></i> Déconnexion
        </a>
    </div>
    <div class="col-md-9 col-sm-8 col-xs-12">
        <h1 class="text-white col-xs-12" style="margin-top:0px;">Bonjour <?php echo Yii::app()->session["user"]["name"]; ?></h1>
		<?php
			$rolesMessage = $rolesMessage ?? [];
			$roles = $roles ?? [];
			foreach ($rolesMessage as $roleKey => $roleMessage) {
				if (in_array($roleKey, $roles)) { ?>
                    <div class="col-xs-12 margin-bottom-20 no-padding">
                        <h4 class='col-xs-12 text-white'><?= $roleMessage['title'] ?></h4>
                        <span class="col-xs-12 text-white"><?= $roleMessage['description'] ?></span>
                    </div>
				<?php }
			}
		?>
        <h4 class='col-xs-12 text-white'>Propositions dont vous faites parties</h4>
		<?php
			if (count($proposals) > 0):
				foreach ($proposals as $proposal):
					?>
                    <div class="col-xs-12 tilsCTENAT shadow2">
                        <div class="col-xs-12 col-sm-3 col-md-2">
                            <img class='img-responsive img-tils' src="<?= $proposal['image'] ?>">
                        </div>
                        <div class="col-xs-12 col-sm-9 col-md-10 padding-5">
                            <a href="#detailProposalAap.context.<?= $context ?>.formid.<?= $form ?>.answerId.<?= $proposal['id'] ?>.aapview.summary"
                               class="lbh col-xs-12 title-tils no-padding elipsis"><?= $proposal['name'] ?></a><br>
                            <div><?= $proposal['description'] ?></div>
                        </div>
                    </div>
				<?php
				endforeach;
			else: ?>
                <span class="col-xs-12 text-white">Vous ne faites partie d'aucune proposition</span>
			<?php
			endif;
		?>
        <h4 class='col-xs-12 text-white'>Projets dont vous faites parties</h4>
		<?php
			if (count($projects) > 0):
				foreach ($projects as $project):
					?>
                    <div class="col-xs-12 tilsCTENAT shadow2">
                        <div class="col-xs-12 col-sm-3 col-md-2">
                            <img class='img-responsive img-tils' src="<?= $project['image'] ?>">
                        </div>
                        <div class="col-xs-12 col-sm-9 col-md-10 padding-5">
                            <a href="#detailProjectAap.context.<?= $context ?>.formid.<?= $form ?>.projectId.<?= $project['id'] ?>.aapview.action"
                               class="lbh col-xs-12 title-tils no-padding elipsis"><?= $project['name'] ?></a><br>
                            <div>
								<?= $project['description'] ?>
                            </div>
                        </div>
                    </div>
				<?php
				endforeach;
			else: ?>
                <span class="col-xs-12 text-white">Vous ne faites partie d'aucun projet</span>
			<?php
			endif;
		?>
        <h4 class='col-xs-12 text-white'>Vos propositions favoris</h4>
        <?php
			if (count($proposalsliked) > 0):
				foreach ($proposalsliked as $proposal):
					?>
                    <div class="col-xs-12 tilsCTENAT shadow2">
                        <div class="col-xs-12 col-sm-3 col-md-2">
                            <img class='img-responsive img-tils' src="<?= $proposal['image'] ?>">
                        </div>
                        <div class="col-xs-12 col-sm-9 col-md-10 padding-5">
                            <a href="#detailProposalAap.context.<?= $context ?>.formid.<?= $form ?>.answerId.<?= $proposal['id'] ?>.aapview.summary"
                               class="lbh col-xs-12 title-tils no-padding elipsis"><?= $proposal['name'] ?></a><br>
                            <div><?= $proposal['description'] ?></div>
                        </div>
                    </div>
				<?php
				endforeach;
			else: ?>
                <span class="col-xs-12 text-white">Vous ne faites partie d'aucune proposition</span>
			<?php
			endif;
		?>
    </div>
</div>
<script type="text/javascript">
	jQuery(document).ready(function () {
		coInterface.bindEvents();
		coInterface.bindLBHLinks();
		var $win = $(document); // or $box parent container
		var $box = $(".dashboard-aap");
		var $lbhBtn = $(".lbh, .lbhp, .close-modal");
		//var $log = $(".log");

		$win.on("click.Bst", function (event) {
			if ($box.has(event.target).length == 0 //checks if descendants of $box was clicked
				&&
				!$box.is(event.target) //checks if the $box itself was clicked
			) {
				$(".dashboard-aap").hide(200);
			} else {
				if ($lbhBtn.has(event.target).length != 0 //checks if descendants of $box was clicked
					||
					$lbhBtn.is(event.target)) {
					$(".dashboard-aap").hide(200);
				}
				//alert("you clicked inside the box");
			}
		});
		$(".showAllList").click(function () {
			$(this).parent().find(".reduceList").show(200);
			$(this).hide();
			$(this).parent().find(".tilsCTENAT").each(function () {
				if (!$(this).is(":visible")) {
					$(this).show(200);
				}
			});

		});
		$(".reduceList").click(function () {
			$(this).parent().find(".showAllList").show(200);
			$(this).hide();
			cntInc = 0;
			$(this).parent().find(".tilsCTENAT").each(function () {
				if (cntInc >= 2) {
					$(this).hide(200);
				}
				cntInc++;
			});
		});
	});
</script>