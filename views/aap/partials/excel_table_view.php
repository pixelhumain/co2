<?php
$headers = $headers ?? [];
$bodies = $bodies ?? [];
$sources = $sources ?? ['status' => []];
$formTitle = $formTitle ?? '';
?>
<style>
	.select2-container {
		width: 100%;
		height: 100%;
	}

	.select2-container .select2-choices {
		border: none;
		border-radius: 0;
		height: 100%;
	}

	.select2-container.select2-container-active .select2-choices {
		border: none;
		background: none;
	}

	.select2-container .select2-choices .select2-search-choice {
		line-height: 8px;
	}

	.select2-container:not(.aapstatus-container) .select2-choices .select2-search-choice {
		padding: 0 5px 0 19px !important;
	}

	#handsontable-column-reorder+div.dropdown-menu {
		max-height: 200px;
		overflow-y: auto;
	}

	#handsontable-column-reorder+div.dropdown-menu ul {
		margin: 0;
		padding: 5px 10px;
	}

	#handsontable-column-reorder+div.dropdown-menu ul>li {
		list-style: none;
	}

	.reorder-item {
		display: flex;
		flex-direction: row;
		justify-content: space-between;
		align-items: center;
		background: white;
		padding: 0 10px;
	}

	.reorder-item:hover {
		background: #ddd;
		border-radius: 8px;
	}

	.reorder-label {
		font-size: 14px;
	}
</style>
<div class="dropdown" style="margin-bottom: 10px;">
	<button class="btn btn-default dropdown-toggle" type="button" id="handsontable-column-reorder" data-toggle="dropdown">
		Ranger les colonnes
		<span class="caret"></span>
	</button>
	<div class="dropdown-menu" role="menu" aria-labelledby="handsontable-column-reorder">
		<div class="btn-group pull-right" style="margin-right: 5px;">
			<button type="button" id="show-all-columns" class="btn btn-default">Tout sélectionner</button>
			<button type="button" id="hide-all-columns" class="btn btn-default">Effacer la sélection</button>
		</div>
		<ul style="clear: both;">
			<?php foreach (array_keys($headers) as $order => $header_key) : ?>
				<?php if ($header_key === 'ID') continue; ?>
				<li class="order-elements" role="presentation" data-order="<?= $order ?>">
					<div class="reorder-item">
						<div class="reorder-label">
							<div class="checkbox">
								<label>
									<input type="checkbox" value="<?= $order ?>" checked="checked"> <?= $headers[$header_key] ?>
								</label>
							</div>
						</div>
						<span class="reorder-drag"><span class="fa fa-arrows"></span></span>
					</div>
				</li>
			<?php endforeach; ?>
		</ul>
	</div>
</div>
<div class="btn-group" style="margin-bottom: 10px;">
	<div class="btn btn-default" id="hot-csv-export"><?= Yii::t('common', 'Export to CSV') ?></div>
</div>
<div id="handsontable-container"></div>
<script>
	coInterface.showLoader('#handsontable-container');
	(function(handsontable) {
		var headers = JSON.parse(JSON.stringify(<?= json_encode($headers) ?>));
		var bodies = JSON.parse(JSON.stringify(<?= json_encode($bodies) ?>));
		var authorizations = {
			read: JSON.parse(JSON.stringify(<?= json_encode($canRead) ?>)),
			edit: JSON.parse(JSON.stringify(<?= json_encode($canEdit) ?>))
		};
		var readonly_cols = JSON.parse(JSON.stringify(<?= json_encode($readonly_cols) ?>));
		var data_sources = JSON.parse(JSON.stringify(<?= json_encode($sources) ?>));
		var data = [];
		$.each(bodies, function(_, body) {
			if (authorizations.read[body['ID']])
				data.push(body);
			else {
				function recursivelyRestrict(element) {
					var temp = {};
					if (!['undefined', 'object'].includes(typeof element)) {
						temp = '[hidden information]';
					} else {
						$.each(element, function(key) {
							if (['string', 'number'].includes(typeof element[key]) || element[key] === null)
								temp[key] = '[hidden information]';
							else
								temp[key] = recursivelyRestrict(element[key]);
						});
					}
					return temp;
				}

				var temporary = {};
				$.each(body, function(key) {
					temporary[key] = recursivelyRestrict(body[key]);
				});
				data.push(temporary);
			}
		});

		class Select2Editor extends handsontable.editors.BaseEditor {
			init() {
				this.options = {};
				var self = this;
				$.each(data_sources.status, function(_key, _value) {
					self.options[_key] = _value;
				});
			}

			prepare(_row, _col, _prop, _td, _originalValue, _cellProperties) {
				// Invoke the original method...
				super.prepare(_row, _col, _prop, _td, _originalValue, _cellProperties);
				this.select_dom = document.createElement('select');
				this.select_dom.multiple = true;

				var self = this;
				// add all options
				$.each(this.options, function(_id, _label) {
					var option_dom = document.createElement('option');
					option_dom.value = _id;
					option_dom.textContent = _label;
					self.select_dom.appendChild(option_dom);
				});
			}

			setValue(_value) {
				var origin_selected = _value ? _value.split(' ; ') : [];
				var final_value = [];
				if (origin_selected.length) {
					$.each(data_sources.status, function(_key, _label) {
						if (origin_selected.includes(_label)) {
							final_value.push(_key);
						}
					});
				}
				$(this.select_dom).val(final_value);
				$(this.select_dom).trigger('change');
			}

			open() {
				handsontable.dom.empty(this.TD);
				this.TD.appendChild(this.select_dom);
				$(this.select_dom).select2();
			}

			focus() {
				$(this.select_dom).select2('open');
			}

			getValue() {
				var output = [];
				var values = $(this.select_dom).val();
				if (values && values.length) {
					$.each(values, function(__, _value) {
						if (data_sources.status[_value]) {
							output.push(data_sources.status[_value]);
						}
					});
				}
				return output.join(' ; ');
			}

			close() {
				$(this.select_dom).select2('destroy');
				this.select_dom.remove();
			}
		}

		class TagEditor extends Select2Editor {
			init() {
				this.options = {};
				var self = this;
				$.each(data_sources.tag_lists, function(__, _value) {
					self.options[_value] = _value;
				});
			}

			setValue(_value) {
				var origin_selected = _value ? _value.split(' ; ') : [];
				$(this.select_dom).val(origin_selected);
				$(this.select_dom).trigger('change');
			}

			getValue() {
				var values = $(this.select_dom).val();
				return values && values.length ? values.join(' ; ') : '';
			}
		}

		function is_expense_post_column(_header) {
			if (typeof _header !== 'string')
				return (false);
			var match = _header.match(/^answers\.aapStep1\.depense\.[0-9]{1,2}\.poste$/);
			return Array.isArray(match) && match.length > 0;
		}

		function is_expense_amount_column(_header) {
			if (typeof _header !== 'string') {
				return false;
			}
			var match = _header.match(/^answers\.aapStep1\.depense\.[0-9]{1,2}\.price$/);
			return Array.isArray(match) && match.length > 0;
		}

		function is_financer_line_column(_header) {
			if (typeof _header !== 'string') {
				return false;
			}
			var match = _header.match(/^answers\.aapStep1\.depense\.[0-9]{1,2}\.financer\.[0-9]{1,2}\.line$/);
			return Array.isArray(match) && match.length > 0;
		}

		function is_financer_amount_column(_header) {
			if (typeof _header !== 'string')
				return (false);
			var match = _header.match(/^answers\.aapStep1\.depense\.[0-9]{1,2}\.financer\.[0-9]{1,2}\.amount$/);
			return Array.isArray(match) && match.length > 0;
		}

		$(function() {
			var container = document.getElementById('handsontable-container');
			var virtualContainer = document.createElement('div');
			var columns = [];
			$.each(headers, function(__key, __header) {
				var column = {
					title: __header,
					data: __key
				};
				if (__key.match(/^answers\.aapStep1\.depense\.[0-9]{1,2}.price$/)) {
					column.type = 'numeric';
					column.typeFormat = {
						pattern: '0 0,00 $',
						culture: 'fr-FR',
					};
				} else {
					switch (__key) {
						case 'status':
							column.editor = Select2Editor;
							column.minWidth = 200;
							break;
						case 'answers.aapStep1.urgency':
							column.type = 'checkbox';
							column.checkedTemplate = 'Urgent';
							column.uncheckedTemplate = '';
							column.className = 'htCenter';
							break;
						case 'answers.aapStep1.tags':
							column.editor = TagEditor;
							column.minWidth = 200;
							break;
					}
				}
				columns.push(column);
			});
			var hot = new handsontable(virtualContainer, {
				data,
				columns,
				rowHeaders: true,
				colHeaders: true,
				manualColumnMove: true,
				manualColumnResize: true,
				manualRowResize: true,
				autoRowSize: false,
				filters: true,
				fillHandle: false,
				dropdownMenu: ['filter_by_condition', 'filter_by_value', 'filter_action_bar'],
				hiddenColumns: {
					// cacher la colonne correspondant à l'ID
					columns: [0]
				},
				height: 'auto',
				licenseKey: 'non-commercial-and-evaluation', // for non-commercial use only
				afterChange: function(change, source) {
					if (source === 'loadData')
						return;
					var update = change[0];
					if (update[2] === update[3]) {
						return;
					}
					var id = hot.getSourceDataAtRow(update[0])['ID'];
					let value = update[3];
					let setType = null;
					if (is_expense_amount_column(update[1]) || is_financer_amount_column(update[1])) {
						value = parseInt(value, 10);
						value = isNaN(value) ? 0 : value;
						setType = 'int';
					} else {
						switch (update[1]) {
							case 'status':
								value = value.split(' ; ').filter(_filter => typeof data_sources.status_reverse[_filter] !== 'undefined').map(_map => data_sources.status_reverse[_map]);
								break;
							case 'answers.aapStep1.tags':
								value = value.split(' ; ');
								break;
							case 'answers.aapStep1.urgency':
								value = value ? ['Urgent'] : null
								break;
						}
					}
					var path2Value = {
						id,
						collection: 'answers',
						path: update[1],
						setType,
						value
					};

					dataHelper.path2Value(path2Value, function() {
						toastr.success('enregistré');
					});
				}
			});

			hot.addHook('afterColumnMove', function(columns, target) {
				var order_element_dom = $('.order-elements');
				var target_dom = order_element_dom.eq(target - 1);
				var moved_dom = order_element_dom.filter(_index => columns.map(_column => _column - 1).includes(_index));

				var move_forward = columns.slice(-1) > target;
				if (move_forward)
					target_dom.before(moved_dom);
				else
					target_dom.after(moved_dom);
			});

			var renderNumber = 0;
			hot.addHook('afterRender', function() {
				if (renderNumber === 0) {
					renderNumber++;
					$(container).empty().append(virtualContainer);
				}
			});

			hot.updateSettings({
				// Les colonnes des dépenses vides ne sont pas éditables
				cells: function(_row, _col, _prop) {
					var cell_property = {};
					var is_post_column = is_expense_post_column(_prop);
					var is_price_column = is_expense_amount_column(_prop);
					var is_line_column = is_financer_line_column(_prop);
					var is_amount_column = is_financer_amount_column(_prop);
					var is_readonly_col = prop => typeof prop === "string" && readonly_cols.includes(prop);
					var valid_cell = data => typeof data !== 'undefined' && data !== null && data !== '';

					var data = hot.getSourceDataAtRow(_row);
					var dataAtCell = hot.getDataAtCell(_row, _col);
					var next_data = hot.getDataAtCell(_row, _col + 1);
					var previous_data = hot.getDataAtCell(_row, _col - 1);

					if (is_readonly_col(_prop))
						cell_property.readOnly = true;
					if (data === null || data.ID && !authorizations.edit[data['ID']])
						cell_property.readOnly = true;
					else if ((is_price_column || is_post_column) && (!valid_cell(dataAtCell) && (is_price_column && !valid_cell(previous_data) || is_post_column && !valid_cell(next_data)))) {
						cell_property.readOnly = true;
						cell_property.renderer = function(instance, td, row, col, prop, value, cellProperties) {
							handsontable.renderers.TextRenderer.apply(this, arguments);
							td.style.backgroundColor = '#ddd'; // Couleur d'arrière-plan pour la cellule
						};
					} else if ((is_line_column || is_amount_column) && (!valid_cell(dataAtCell) && (is_amount_column && !valid_cell(previous_data) || is_line_column && !valid_cell(next_data)))) {
						cell_property.readOnly = true;
						cell_property.renderer = function(instance, td, row, col, prop, value, cellProperties) {
							handsontable.renderers.TextRenderer.apply(this, arguments);
							td.style.backgroundColor = '#ddd'; // Couleur d'arrière-plan pour la cellule
						};
					}

					return cell_property;
				}
			});

			$('#handsontable-column-reorder+div.dropdown-menu ul').sortable({
				handle: '.reorder-drag',
				update: function() {
					var self = $(this);
					var new_order = [0];
					self.find('.order-elements').each(function() {
						new_order.push(parseInt(this.dataset.order, 10));
					})
					setTimeout(function() {
						hot.updateSettings({
							columns: new_order.map(_index => columns[_index])
						});
					}, 800);
				}
			});
			$('.dropdown-menu').on('click', function(_event) {
				_event.stopPropagation();
			});
			$('#show-all-columns').on('click', function() {
				$('.reorder-item input[type=checkbox]').prop('checked', true);
				var plugin = hot.getPlugin('hiddenColumns');
				var indexes = Array.from({
					length: columns.length - 1
				}, (__, _index) => _index + 1);
				plugin.showColumns(indexes);
				hot.render();
			});
			$('#hide-all-columns').on('click', function() {
				$('.reorder-item input[type=checkbox]').prop('checked', false);
				var plugin = hot.getPlugin('hiddenColumns');
				var indexes = Array.from({
					length: columns.length - 1
				}, (__, _index) => _index + 1);
				plugin.hideColumns(indexes);
				hot.render();
			});

			$('.reorder-item input[type=checkbox]').on('change', function() {
				var self = $(this);
				var plugin = hot.getPlugin('hiddenColumns');
				if (self.is(':checked'))
					plugin.showColumn(parseInt(self.val()));
				else
					plugin.hideColumn(parseInt(self.val()));
				hot.render();
			});

			$('#hot-csv-export').on('click', function(event) {
				event.preventDefault();
				var exportPlugin = hot.getPlugin('exportFile');
				exportPlugin.downloadFile('csv', {
					bom: true,
					columnDelimiter: ',',
					columnHeaders: true,
					exportHiddenColumns: false,
					exportHiddenRows: false,
					fileExtension: 'csv',
					filename: JSON.parse(JSON.stringify(<?= json_encode($formTitle) ?>)) + moment().format(' YYYY-MM-DD_HH-mm-ss'),
					mimeType: 'text/csv',
					rowDelimiter: '\r\n',
					rowHeaders: false
				});
			})
		})
	})(Handsontable)
</script>