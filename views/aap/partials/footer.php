<?php
    $config = file_get_contents("../../modules/co2/data/aap.json", FILE_USE_INCLUDE_PATH);
    $config = json_decode($config, true);
?>
<footer style="margin-top: 99px;" class="">
    <div class="container-fluid bg-darkk">
        <div class="row">
            <div class="col-xs-12">
                <h4><a href="#communityAap" class="aap-communityy hidden  text-white lbh h"><?= Yii::t("common", "Community") ?></a></h4>
            </div>
        </div>
    </div>
</footer>

<script>
    $(function () {
        var config = <?= json_encode($config) ?>;
        $.each(config.app, function (k, v) {
            var hrefDom = $(`#mainNav .lbh-menu-app[href*='${k}']`);
            var href = hrefDom.attr('href');
            var aapView = sessionStorage.getItem('aapview');
            if (notEmpty(href) && !href.includes("context") && !href.includes("formid")) {
                var standardViews = ['detailed', 'minimalist'];
                var selectedView = aapView && standardViews.indexOf(aapView) >= 0 ? aapView : standardViews[0];
                var newHref = href + ".context." + aapObj.context.slug + ".formid." + aapObj.form._id.$id;
                var linkViewsList = ['myprojectsAap', 'proposalAap', 'myproposalsAap', 'projectsAap'];
                if (notEmpty(aapView) && linkViewsList.some(function (someLinkView) {
                    return href.indexOf(someLinkView) >= 0;
                })) newHref += `.aapview.${selectedView}`;
                if(typeof userId != "undefined" && userId && href.includes('#myproposalsAap'))
                    newHref += `.userId.${userId}`;
                if(typeof userId != "undefined" && userId && href.includes("#myprojectsAap"))
                    newHref += `.projectUserId.${userId}`;
                if(href.includes('#myactionsAap') && aapObj.myDefaultProject === null) {
                    hrefDom.hide();
                } else {
                    hrefDom.attr("href", newHref);
                }
            }
            if(k == "#proposalAap" || k == "#myproposalsAap" || k == "#projectsAap" || k == "#myprojectsAap") {
                var hashParams = {
                    context: aapObj.context && aapObj.context.slug ? aapObj.context.slug : null,
                    formid: aapObj.form && aapObj.form._id ? aapObj.form._id.$id : null,
                };
                if(notEmpty(aapView) && aapView == "kanban") {
                    if(k == "#proposalAap" && aapObj.defaultProposal != null && aapObj.defaultProposal._id) {
                        hashParams.answerId = aapObj.defaultProposal._id.$id;
                        if(notEmpty(aapObj.defaultProposal.project) && aapObj.defaultProposal.project.id)
                            hashParams.projectId = aapObj.defaultProposal.project.id
                        hrefDom.attr("href", aapObj.common.buildUrlQuery('#detailProposalAap',hashParams, {}))
                    }
                    if(k == "#myproposalsAap" && aapObj.myDefaultProposal != null && aapObj.myDefaultProposal._id) {
                        hashParams.answerId = aapObj.myDefaultProposal._id.$id;
                        if(aapObj.myDefaultProposal.project && aapObj.myDefaultProposal.project.id)
                            hashParams.projectId = aapObj.myDefaultProposal.project.id;
                        if(typeof userId != "undefined" && userId)
                            hashParams.userId = userId;
                        hrefDom.attr("href", aapObj.common.buildUrlQuery('#detailProposalAap',hashParams, {}))
                    }
                    if(k == "#projectsAap" && aapObj.defaultProject != null && aapObj.defaultProject._id) {
                        hashParams.projectId = aapObj.defaultAnswer != null && aapObj.defaultAnswer._id && aapObj.defaultAnswer.project && aapObj.defaultAnswer.project.id ? aapObj.defaultAnswer.project.id : aapObj.defaultProject._id.$id;
                        hashParams.aapview = "action";
                        if(notEmpty(aapObj.defaultProject.answer) && aapObj.defaultProject.answer.$id)
                            hashParams.answerId = aapObj.defaultProject.answer.$id;
                        
                        hrefDom.attr("href", aapObj.common.buildUrlQuery('#detailProjectAap',hashParams, {}))
                    }
                    if(k == "#myprojectsAap" && aapObj.myDefaultProject != null && aapObj.myDefaultProject._id) {
                        hashParams.projectId = aapObj.myDefaultProject._id.$id;
                        hashParams.aapview = "action";
                        if(notEmpty(aapObj.myDefaultProject.answer) && aapObj.myDefaultProject.answer.$id)
                            hashParams.answerId = aapObj.myDefaultProject.answer.$id;
                        if(typeof userId != "undefined" && userId)
                            hashParams.projectUserId = userId;
                        hrefDom.attr("href", aapObj.common.buildUrlQuery('#detailProjectAap',hashParams, {}))
                    }
                } else {
                    hashParams.aapview = notEmpty(aapView) ? aapView : "detailed";
                    if(k == "#proposalAap") {
                        if(hrefDom.length < 1) 
                            hrefDom = $(`#mainNav .lbh-menu-app[href*='#detailProposalAap']`).filter(`:not([href*='.userId.'])`)
                        hrefDom.attr("href", aapObj.common.buildUrlQuery('#proposalAap',hashParams, {}))
                    }
                    if(k == "#myproposalsAap") {
                        if(hrefDom.length < 1) 
                            hrefDom = $(`#mainNav .lbh-menu-app[href*='#detailProposalAap']`).filter(`[href*='.userId.']`)
                        if(typeof userId != "undefined" && userId)
                            hashParams.userId = userId;
                        hrefDom.attr("href", aapObj.common.buildUrlQuery('#myproposalsAap',hashParams, {}))
                    }
                    if(k == "#projectsAap") {
                        if(hashParams.aapview == "table")
                            hashParams.aapview = "minimalist"
                        if(hrefDom.length < 1) 
                            hrefDom = $(`#mainNav .lbh-menu-app[href*='#detailProjectAap']`).filter(`:not([href*='.projectUserId.'])`)
                        hrefDom.attr("href", aapObj.common.buildUrlQuery('#projectsAap',hashParams, {}))
                    }
                    if(k == "#myprojectsAap") {
                        if(hashParams.aapview == "table")
                            hashParams.aapview = "minimalist"
                        if(hrefDom.length < 1) 
                            hrefDom = $(`#mainNav .lbh-menu-app[href*='#detailProjectAap']`).filter(`[href*='.projectUserId.']`)
                        if(typeof userId != "undefined" && userId)
                            hashParams.projectUserId = userId;
                        hrefDom.attr("href", aapObj.common.buildUrlQuery('#myprojectsAap',hashParams, {}))
                    }
                }
            }

        });
        if(aapObj.myDefaultProject === null) {
            $('#menuTopLeft .cosDyn-app .projectDropdown a:nth-child(2) .fa-trello').hide();
        }
        if(aapObj.myDefaultProposal === null) {
            $('#menuTopLeft .cosDyn-app .proposalDropdown a:nth-child(2) .fa-trello').hide();
        }

        htmlExists('a[href^="#newproposalAap"]', function (sel) {
            // if (typeof aapObj.session !== 'undefined' && notEmpty(aapObj.session) && typeof aapObj.session.status === 'string' && aapObj.session.status === "include" && (!aapObj.form.oneAnswerPerPers || !aapObj.haveAnswer)) {
            //     sel.show();
            // } else {
            //     sel.hide();
            // }
            if(!notEmpty(aapObj?.form?.onlymemberaccess) || aapObj?.form?.onlymemberaccess == false) {
                if (typeof aapObj.session !== 'undefined' && notEmpty(aapObj.session) && typeof aapObj.session.status === 'string' && aapObj.session.status === "include" && (!aapObj.form.oneAnswerPerPers || !aapObj.haveAnswer)) {
                    sel.show();
                } else {
                    sel.hide();
                }
            } else {
                if(aapObj.canEdit || aapObj.context?.links?.members?.[userId]) {
                    if (typeof aapObj.session !== 'undefined' && notEmpty(aapObj.session) && typeof aapObj.session.status === 'string' && aapObj.session.status === "include" && (!aapObj.form.oneAnswerPerPers || !aapObj.haveAnswer)) {
                        sel.show();
                    } else {
                        sel.hide();
                    }
                } else {
                    sel.hide();
                }
            }
        });
        $('div.custom-tooltip.tooltip-content').remove();
        aapObj.common.breadcrumbHtml(aapObj);
        aapObj.directory.menuRightDropdownHtml(aapObj);
        aapObj.directory.proposalNotSeenCounter(aapObj);
        aapObj.common.changeImageLoader(aapObj);
        aapObj.common.changeLogo(aapObj);
        aapObj.common.session.initYear(aapObj);
        setTitle(aapObj.context.name);
    })
</script>
