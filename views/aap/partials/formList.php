<script>
    $(function(){
        var params = <?= json_encode($params,true) ?>;
        if(notEmpty(params)){
            coInterface.showLoader(".pageContent");
            var new_hash = `${location.hash}.context.${params.context}.formid.${params._id.$id}`;
            history.pushState(null, document.title, `${document.location.origin}${document.location.pathname}${new_hash}`);
            urlCtrl.loadByHash(location.hash);
        }else{
            if(notNull(costum) && exists(costum.contextId) && typeof aapObj === 'object')
                aapObj.common.loadAapOrganismPanel();
        }
    })
</script>