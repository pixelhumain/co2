<?php
$css_js = [
	'/css/flex-layout.css',
	'/plugins/pnotify/core/dist/PNotify.css',
	// '/plugins/pnotify/mobile/dist/PNotifyMobile.css',
	'/plugins/pnotify/bootstrap4/dist/BrightTheme.css',
	'/plugins/pnotify/custom/Custom-style.css',
	'/plugins/pnotify/core/dist/PNotify.js',
	'/plugins/pnotify/animate/dist/PNotifyAnimate.js',
	'/plugins/pnotify/custom/Custom-call.js',
];
HtmlHelper::registerCssAndScriptsFiles($css_js, Yii::app()->request->baseUrl);
$css_js = [
	'/css/space-layout.css',
];
HtmlHelper::registerCssAndScriptsFiles($css_js, Yii::app()->getModule(Survey::MODULE)->getAssetsUrl());
$css_js = [
	'/css/bootstrap-ext-card.css',
	'/css/bootstrap-ext-button.css',
	'/css/bootstrap-ext-progressbar.css',
	'/js/bootstrap-ext-progressbar.js',
	'/js/bootstrap-ext-button.js'
];
HtmlHelper::registerCssAndScriptsFiles($css_js, Yii::app()->getModule('co2')->getAssetsUrl());
$css_js = [
	'/plugins/bootstrap-datetimepicker/xdsoft/jquery.datetimepicker.full.min.js',
	'/plugins/bootstrap-datetimepicker/xdsoft/jquery.datetimepicker.min.css'
];
HtmlHelper::registerCssAndScriptsFiles($css_js, Yii::app()->request->baseUrl);
//HtmlHelper::registerCssAndScriptsFiles(['/plugins/rater/rater-js.js'], Yii::app()->request->baseUrl);

$pagekey = array_keys($params["pageDetail"])[0];
$pageTitle = "";
$info = "";
$html = "";
if (!empty($params['aapview']) && $params['aapview'] === 'table') {
	$css_js = [
		'/plugins/handsometable/js/handsontable.full.min.js',
		'/plugins/handsometable/css/handsontable.full.min.css',
	];
	HtmlHelper::registerCssAndScriptsFiles($css_js, Yii::app()->request->baseUrl);
}
?>
<style>
	.detail-view-container {
		text-align: right;
		margin-top: 10px;
	}

	.associate-project-container {
		display: inline-block;
		vertical-align: middle;
		width: 300px;
	}
</style>
<?php
$haveSpecificJs = file_exists("../../modules/costum/assets/js/" . $this->costum["slug"] . "/" . $this->costum["slug"] . ".js");
if ($haveSpecificJs) HtmlHelper::registerCssAndScriptsFiles(["/js/" . $this->costum["slug"] . "/" . $this->costum["slug"] . ".js"], Yii::app()
	->getModule('costum')->assetsUrl);
switch ($pagekey) {
	case '#contributionAap':
	case '#proposalAap':
	case '#myproposalsAap':
		$info .= "<h4>" . Yii::t('common', 'Call for projects') . " - " . $params['context']['name'] . " - <span class='" . $params['session']['class'] . "'>" . $params['session']["msg"] . "</span></h4>";
		$btnDepose = ``;
		if (
			$params["canEdit"] ||
			(
				$params['session']['status'] == "include" &&
				(empty($params["form"]["onlymemberaccess"]) || ($params["form"]["onlymemberaccess"] == true && isset($params["context"]["links"]["members"][Yii::app()->session["userId"]])))
			)
		)
			$btnDepose = '<button class="btn btn-aap-secondary add-proposition" type="button">
                                    <i class="fa fa-plus-square-o"></i> ' . Yii::t("common", "Submit a proposal") . '
                                </button>';
		$oneAnswerAllowed = !empty($params['form']['oneAnswerPerPers']) && filter_var($params['form']['oneAnswerPerPers'], FILTER_VALIDATE_BOOLEAN);
		if (!empty(Yii::app()->session["userId"]) && !empty($params['session']['status']) && $params['session']['status'] === 'include' && (!$oneAnswerAllowed || !$params['haveAnswer'])) {
			$html .= '
                <div class="column-flex">
                    <div class="flex-item aap-page-info-right">' . $btnDepose . '</div>
                </div>';
		}
		break;
	case '#detailProposalAap':
		$pageTitle = $params["answer"]["answers"]["aapStep1"]["titre"] ?? "";
		$mapButton = '
		<button class="btn btn-default btn-tooltip change-list-view" data-view="map" data-toggle="tooltip" data-placement="top" data-original-title="' . Yii::t('common', 'Disable/Enable map view') . '">
            <i class="fa fa-map-marker fw600"></i>
        </button>
		';
		$html = '<a href="javascript:;" id="proposition-detailProject" class="btn btn-aap-link" data-project="">' . Yii::t('common', "Associated project") . '<i class="fa fa-long-arrow-right ml-2"></i></a>';
		if ($params['canEdit']) {
			$html .= '
			<div class="associate-project-container">
			<select name="associate-project" style="width: 200px; display: inline;">
			<option></option>';
			foreach ($params["admin_projects"] as $project)
				$html .= '<option value="' . $project["id"] . '">' . $project["name"] . '</option>';
			$html .= '</select></div>';
			$html .= '<a href="javascript:;" id="proposal-generate-project" class="btn btn-aap-primary margin-left-5" data-answer="">'
				. Yii::t("survey", "Generate project") .
				'</a>';
		}
		$html .= '
		<div class="detail-view-container">
			<div class="medium-view">
                <span class="hidden">Vue</span>
                <div class="btn-group">
                    <button class="btn btn-default btn-tooltip change-list-view hidden-xs active" data-view="kanban" data-toggle="tooltip" data-placement="top" data-original-title="Kanban">
                        <i class="fa fa-trello"></i>
                    </button>
                    <button class="btn btn-default btn-tooltip change-list-view hidden-xs" data-view="detailed" data-toggle="tooltip" data-placement="top" data-original-title="' . Yii::t('common', 'Detailed view') . '">
                        <i class="fa fa-th-list"></i>
                    </button>
                    <button class="btn btn-default btn-tooltip change-list-view" data-view="minimalist" data-toggle="tooltip" data-placement="top" data-original-title="' . Yii::t('common', 'Compact view') . '">
                        <i class="fa fa-bars fw600 rotate90"></i>
                    </button>
                    ' . (!empty($showMap) && filter_var($showMap, FILTER_VALIDATE_BOOL) ? $mapButton : '') . '
                    <button class="btn btn-default btn-tooltip change-list-view" data-view="table" data-toggle="tooltip" data-placement="top" data-original-title="' . Yii::t('common', 'Table view') . '">
                        <i class="fa fa-table"></i>
                    </button>
                </div>
            </div>
         </div>';
		break;
	case '#detailProjectAap':
		$pageTitle = @$params["answer"]["answers"]["aapStep1"]["titre"];
		$mapButton = '
		<button class="btn btn-default btn-tooltip change-list-view" data-view="map" data-toggle="tooltip" data-placement="top" data-original-title="' . Yii::t('common', 'Disable/Enable map view') . '">
            <i class="fa fa-map-marker fw600"></i>
        </button>
		';
		$html = '<a href="javascript:;" id="project-detailProposition" class="btn btn-aap-link" data-answer="">'
			. Yii::t('common', "Associated proposal") .
			'<i class="fa fa-long-arrow-right ml-2"></i>
                     </a>';
		$html .= '
		<div class="detail-view-container">
			<div class="medium-view">
                <span class="hidden">Vue</span>
                <div class="btn-group">
                    <button class="btn btn-default btn-tooltip change-list-view hidden-xs active" data-view="kanban" data-toggle="tooltip" data-placement="top" data-original-title="Kanban">
                        <i class="fa fa-trello"></i>
                    </button>
                    <button class="btn btn-default btn-tooltip change-list-view hidden-xs" data-view="detailed" data-toggle="tooltip" data-placement="top" data-original-title="' . Yii::t('common', 'Detailed view') . '">
                        <i class="fa fa-th-list"></i>
                    </button>
                    <button class="btn btn-default btn-tooltip change-list-view" data-view="minimalist" data-toggle="tooltip" data-placement="top" data-original-title="' . Yii::t('common', 'Compact view') . '">
                        <i class="fa fa-bars fw600 rotate90"></i>
                    </button>
                    ' . (!empty($showMap) && filter_var($showMap, FILTER_VALIDATE_BOOL) ? $mapButton : '') . '
                </div>
            </div>
         </div>';
		break;
	case '#projectsAap':
	case '#myprojectsAap':
		if (!empty(Yii::app()->session["userId"])) {
			$info .= "<h4>" . ucfirst(Yii::t('common', 'projects list')) . " - " . $params['context']['name'];
			$html .= '
            <button class="btn btn-aap-secondary create-project" type="button">
                <i class="fa fa-rocket"></i> ' . Yii::t("common", "Create project") . '
            </button>
            ';
		}
		break;
}

if ($pagekey != "#configurationAap") {
?>
	<div class="container-fluid aap-header">
		<ul class="breadcrumb aap-breadcrumb" style="padding-left : 0"></ul>
		<div class="menu-hider">
			<button class="btn btn-default show pull-right">
				<span class="show-menus"><?= Yii::t('common', 'Show menus') ?> <span class="fa fa-eye"></span></span>
				<span class="hide-menus"><?= Yii::t('common', 'Hide menus') ?> <span class="fa fa-eye-slash"></span></span>
			</button>
		</div>
	</div>

	<?php if (!empty($pageTitle) || !empty($info) || !empty($html)): ?>
		<div class="row aap-list-container aap-page-info">
			<div class="aap-page-info-left">
				<h3 class="title no-margin">
					<span class="main"> <?= $pageTitle ?></span>
					<span class="status"> <?= $info ?> </span>
				</h3>
			</div>
			<?php if (!empty($html)): ?>
				<div class="<?= in_array($pagekey, ['#detailProposalAap', '#detailProjectAap']) ? '' : 'aap-page-info-right' ?>">
					<?= $html ?>
				</div>
			<?php endif ?>
		</div>
<?php endif;
}
?>
<script>
	var params = JSON.parse(JSON.stringify(<?= json_encode($params) ?>));
	var paramsv2 = {};
	if (params.answerId)
		paramsv2.answer = params.answerId;
	if (params.context)
		paramsv2.context = {
			slug: params.context.slug,
			id: params.context._id.$id,
			type: params.context.collection
		};
	params.requiredInputs = {};
	if (params?.inputs) {
		for ([inputIndex, inputVal] of Object.entries(params.inputs)) {
			const formKey = inputVal.step ? inputVal.step : inputIndex;
			params.requiredInputs[inputIndex] = {
				formParent: inputVal?.formParent,
				isSpecific: inputVal?.isSpecific,
				type: inputVal?.type,
				updated: inputVal?.updated,
				"_id": inputVal?._id,
				inputs: {},
				step: formKey
			}
			if (inputVal.inputs) {
				for ([index, value] of Object.entries(inputVal.inputs)) {
					if (value.isRequired && value.isRequired == true) {
						params.requiredInputs[inputIndex].inputs[index] = value
					}
					if (value.type && value.type == "tpls.forms.aap.invite") {
						params.hasInviteInputs = true
					}
				}
			}
		}
	}
	params?.inputs?.invite ? params.hasInviteInputs = true : "";
	var aapObj = aapObj.init(params);
	g_aap.set(paramsv2);
	var page = aapObj.page + "Aap";
	$(function() {
		$('select[name="associate-project"]').select2({
			placeholder: "Sélectionner un projet",
			width: "100%",
			allowClear: true
		}).select2("val", "").trigger("change");
	})
</script>