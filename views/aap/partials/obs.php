<?php
use PixelHumain\PixelHumain\modules\citizenToolKit\models\Aap;
$graphLabels = Aap::aapGraph($slug);
$cssJS = array(
 '/plugins/Chart.js/chart.4.4.0.min.js',
 "/plugins/Chart.js/chartjs-plugin-datalabels.2.0.0.js",
 '/plugins/Chart.js/chartjs-chart-sankey.0.12.0.js',
 '/plugins/Chart.js/chartjs-adapter-date-fns.bundle.min.js',
 '/plugins/Chart.js/chartjs-chart-matrix@1.1.1.js'
);
HtmlHelper::registerCssAndScriptsFiles($cssJS, Yii::app()->request->baseUrl);

/*function createChartPanel($idCanvas,Array $params = []){
    $tag = $params["tag"] ?? '';
    $containerStyle = $params["containerStyle"] ?? '';
    $bodyStyle = $params["bodyStyle"] ?? '';
    $html = $params["html"] ?? '';
    $col = $params["col"] ?? 6;
    $chartsExpand = <<<EOF
        <div class="col-md-1">
            <div class="panel-toolbar pull-right" role="menu">
                <a href="javascript:;" class="btn btn-panel fullscreen" data-key="on" data-toggle="tooltip" data-original-title="Fullscreen">
                    <i class="fa fa-expand"></i>
                </a>
                <a href="javascript:;" class="btn btn-panel fullscreen" data-key="off" data-toggle="tooltip" data-original-title="Fullscreen" style="display:none">
                    <i class="fa fa-compress"></i>
                </a>
            </div>
        </div>
    EOF;

    $idContainer = is_array($idCanvas) ? join("-", $idCanvas) : $idCanvas."-container";
    $panel = <<<EOF
        <div class="col-lg-$col col-md-$col col-sm-12 margin-top-10 $tag" id="$idContainer">
            <div class="card card-stats" style="$containerStyle">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-11">
                            $html
                        </div>
                        $chartsExpand 
                    </div>
                </div>
                <div class="card-content" style="$bodyStyle"> 
        EOF;
                if(is_array($idCanvas)){
                    foreach ($idCanvas as $kc=> $c) {
                        $hide = $kc !== 0 ? "style='display:none'" : "";
                        $panel .= "<canvas id=".$c." $hide ></canvas>";
                    }
                }else{
                    $panel .= "<canvas id=".$idCanvas."></canvas>";
                }
    $panel .= <<<EOF
                </div>
            </div>
        </div> 
    EOF;
    return $panel;
}*/

function createChartPanel($idCanvas,$params = []){
    if(!empty($params["isActive"])){
        $tag = $params["tag"] ?? '';
        $containerStyle = $params["containerStyle"] ?? '';
        $bodyStyle = $params["bodyStyle"] ?? '';
        $html = $params["html"] ?? '';
        $col = $params["col"] ?? 6;
        $chartsExpand = <<<EOF
            <div class="col-md-1">
                <div class="panel-toolbar pull-right" role="menu">
                    <a href="javascript:;" class="btn btn-panel fullscreen" data-key="on" data-toggle="tooltip" data-original-title="Fullscreen">
                        <i class="fa fa-expand"></i>
                    </a>
                    <a href="javascript:;" class="btn btn-panel fullscreen" data-key="off" data-toggle="tooltip" data-original-title="Fullscreen" style="display:none">
                        <i class="fa fa-compress"></i>
                    </a>
                </div>
            </div>
        EOF;

        $canvas = "";
        if(is_array($idCanvas)){
            foreach ($idCanvas as $kc=> $c) {
                $hide = $kc !== 0 ? "style='display:none'" : "";
                $canvas .= "<canvas id=".$c." $hide ></canvas>";
            }
        }else{
            $canvas .= "<canvas id=".$idCanvas."></canvas>";
        }

        $idContainer = is_array($idCanvas) ? join("-", $idCanvas) : $idCanvas."-container";
        $panel = <<<EOF
            <div class="col-lg-$col col-md-$col col-sm-12 margin-top-10 $tag" id="$idContainer">
                <div class="card card-stats" style="$containerStyle">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-11">
                                $html
                            </div>
                            $chartsExpand 
                        </div>
                    </div>
                    <div class="card-content" style="$bodyStyle">
                        $canvas
                    </div>
                </div>
            </div>
        EOF;
        return $panel;
    }
    
}
?>

<style>
    .tochecked {
        display: none;
    }

    .table td {
        text-align: center;
    }

    .text-uppercase {
        text-transform: lowercase !important;
    }

    canvas {
        margin: 10px 10px 10px 10px;
    }

    .panel-toolbar {
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
    }

    .fullscreen-active {
        position: fixed;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        width: 100%;
        height: 100%;
        z-index: 100000
    }

    .canvas-fullscreen-active {
        width: 100%;
        height: 90%;
    }

    .card {
        border: 1px solid #eeefee !important;
    }

    .card-title {
        font-size: 18px;
    }

    .graphe.fullscreenActive {
        position: fixed;
        top: 80px;
        left: 0;
        align-items: center;
        justify-content: center;
        width: 100%;
        height: 100%;
        transition: visibility 0s, opacity 0.5s;
        z-index: 100;
    }

    a.fullscreenA {
        width: 2.3rem !important;
        height: 2.3rem !important;
    }

    canvas.fullscreenActive {
        height: 100% !important;
        width: 70% !important;
    }

    div.fullscreenActive {
        height: 100% !important;
        width: 100% !important;
    }

    #expenditureFinancingPaymentHistory-filter {
        width: 112px;
    }

    .observatory-aap .historyName {
        text-align: left !important;
        width: 70%;
    }

    .observatory-aap .historyDate {
        text-align: right !important;
        width: 30%
    }

    .observatory-aap .historyTable {
        border: 1px solid #ddd;
    }

    .observatory-aap #filterContainerInside {
        background-color: transparent;
    }

    .breadcrumb.aap-breadcrumb {
        display: none;
    }

    .observatory-aap .pointer {
        cursor: pointer;
    }

    .observatory-aap .date-between .form-group {
        position: relative;
        display: flex;
        flex-direction: row;
        justify-content: center;
        align-items: center;
    }

    .observatory-aap .date-between {
        position: relative;
        display: flex;
        flex-direction: row;
        align-items: center;
        margin-top: 9px;
    }

    .switch.checkFilter input[type=checkbox]:checked+label::before {
        background-color: #9BC125;
    }

    #checkProject:hover+#hovercheckFilter {
        display: inline-block;
    }
</style>

<div class="container-fluid observatory-aap">
    <div class="row filter-container">
        <div class="padding-bottom-20">
            <div class="col-md-12 no-padding">
                <div id='filterContainerObs' class='searchObjCSS'></div>
                <div class='headerSearchIncommunityObs no-padding col-xs-12 hidden'></div>
                <div class='bodySearchContainerObs margin-top-30 hidden'>
                    <div class='no-padding col-xs-12' id='dropdown_search'>
                    </div>
                </div>
                <div class='padding-top-20 col-xs-12 text-left footerSearchContainer hidden'></div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-3 col-md-6 col-sm-6">
            <div class="card card-stats">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-8 card-title"><i class="fa fa-lightbulb-o">&nbsp;</i><b>Proposition</b></div>
                        <div class="panel-toolbar pull-right" style="margin-right:4%">
                            <div class="switch checkFilter">
                                <input id="checkProposition" class="cmn-toggle cmn-toggle-round-flat checkbox" type="checkbox" checked>
                                <label for="checkProposition"></label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-content">
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table align-items-center mb-0" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7"> En financement</th>
                                        <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">A voter</th>
                                        <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">A financer</th>
                                        <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            <h4 id="inFinancingProposal" class="metric-number cursor-pointer" data-action="infinancing">0</h4>
                                        </td>
                                        <td>
                                            <h4 id="voteProposal" class="metric-number cursor-pointer" data-action="tobevoted">0</h4>
                                        </td>
                                        <td>
                                            <h4 id="toBeFinancedProposal" class="metric-number cursor-pointer" data-action="tobefinanced">0</h4>
                                        </td>
                                        <td>
                                            <h4 id="totalProposal">0</h4>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="col-lg-2 col-md-6 col-sm-6">
            <div class="card card-stats">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-8 card-title"><i class="fa fa-cogs"></i>&nbsp; <b>Projet</b></div>
                        <div class="panel-toolbar pull-right" style="margin-right:4%">
                            <div class="switch checkFilter">
                                <input id="checkProject" class="cmn-toggle cmn-toggle-round-flat checkbox" type="checkbox" checked>
                                <label for="checkProject"></label>
                                <span id="hovercheckFilter" class="tooltips-menu-btn" style="display: none;">Activer/désactiver</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-content">
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table align-items-center mb-0" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7"> Total</th>
                                        <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">En cours</th>
                                        <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Terminé</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            <h4 id="totalProject">0</h4>
                                        </td>
                                        <td>
                                            <h4 class="metric-number cursor-pointer" id="runningProject" data-action="inprogress">0</h4>
                                        </td>
                                        <td>
                                            <h4 class="metric-number cursor-pointer" id="doneProject" data-action="finished">0</h4>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-3 col-md-6 col-sm-6">
            <div class="card card-stats">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-8 card-title"><i class="fa fa-tasks">&nbsp;</i><b>Actions</b></div>
                        <div class="panel-toolbar pull-right" style="margin-right:4%">
                            <div class="switch checkFilter">
                                <input id="checkActions" class="cmn-toggle cmn-toggle-round-flat checkbox" type="checkbox" checked>
                                <label for="checkActions"></label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-content">
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table align-items-center mb-0" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7"> Total</th>
                                        <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">À faire</th>
                                        <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">En cours</th>
                                        <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Terminé</th>
                                        <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Délai dépassé</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            <h4 id="totalActions" class="metric-number pointer">0</h4>
                                        </td>
                                        <td>
                                            <h4 id="todoActions" class="metric-number pointer">0</h4>
                                        </td>
                                        <td>
                                            <h4 id="runningActions" class="metric-number pointer">0</h4>
                                        </td>
                                        <td>
                                            <h4 id="doneActions" class="metric-number pointer">0</h4>
                                        </td>
                                        <td>
                                            <h4 id="deadlinePassedActions" class="metric-number pointer">0</h4>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-4 col-md-6 col-sm-6">
            <div class="card card-stats">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-8 card-title"><i class="fa fa-money">&nbsp;</i><b>Financements (en euro)</b></div>
                        <div class="panel-toolbar pull-right" style="margin-right:4%">
                            <div class="switch checkFilter">
                                <input id="checkFinancement" class="cmn-toggle cmn-toggle-round-flat checkbox" type="checkbox" checked>
                                <label for="checkFinancement"></label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-content">
                    <div class="row">

                        <div class="col-md-12">
                            <table class="table align-items-center mb-0" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7"> Montant sollicité</th>
                                        <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Montant financé</th>
                                        <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Reste à financé </th>
                                        <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Fonds dépensés</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            <h4 id="amountRequested">0</h4>
                                        </td>
                                        <td>
                                            <h4 id="amountFinanced">0</h4>
                                        </td>
                                        <td>
                                            <h4 id="amountRemaining">0</h4>
                                        </td>
                                        <td>
                                            <h4 id="amountSpent">0</h4>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <?php 
        $jours_off = '';
        if(!isset($config["dayOffPerPersonne"])  || !empty($config["dayOffPerPersonne"]))
            $jours_off = '<a href="javascript:;" class="btn btn-default blocHeatMap " data-id="jourOff">Jours off</a>';
    
    $dayOffPerPersonne = <<<EOF
    <div class="btn-group">
            <div class="btn-group" style="display:flex">  
                <div class="dropdown">
                    <button class="btn btn-default dropdown-toggle" id="btn-user-names" type="button" data-toggle="dropdown">Utilisateurs
                        <span class="caret"></span>             
                    </button>
                    <ul class="dropdown-menu" id="ListofUsers"></ul> 
                </div>
                <div class="dropdown">
                    <button type="button" id="btn-date-names" class="btn btn-default off" id="btn-list-date"  data-toggle="dropdown">Année(s)
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" id="Listofdate"></ul> 
                </div>
            
                <a href="javascript:;" class="btn btn-default blocHeatMap active" data-id="Activite">Activité(s)</a>
                $jours_off
            </div>
        </div>
    EOF;

        echo createChartPanel(["calendarOffAll" , "calendarActivity"], [
            "html" => $dayOffPerPersonne,
            "bodyStyle" => "min-height: 200px;",
            "containerStyle" => "max-height: 98vh; overflow-y: auto",
            "col" => 12,
            "isActive" =>   !isset($config["calendarActivity"]) || !empty($config["calendarActivity"]),
        ]);
        
            $htmlActionActivityDayWeekMonth = <<<EOF
                <div class="btn-group" role="group" aria-label="Basic example">
                    <div class="btn-group">
                        <button class="btn btn-default dropdown-toggle" id="btn-user-name" type="button" data-toggle="dropdown">Utilisateurs
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu" id="ListofUser"></ul>   
                    </div>
                    <button type="button" class="btn btn-default action-ativity" data-key="yesterday">Hier</button>
                    <button type="button" class="btn btn-default action-ativity" data-key="today">Aujourd'hui</button>
                    <button type="button" class="btn btn-default action-ativity active" data-key="thisweek">Cette semaine</button>
                    <button type="button" class="btn btn-default action-ativity" data-key="thismonth">Ce mois</button>
                    <button type="button" class="btn btn-default action-ativity off" data-key="showTwoDate">Entre 2 dates</button>
                </div>
                <div class="date-between hidden">
                    <div class="form-group no-margin">
                        <label>Entre</label>
                        &nbsp;<input type="date" class="form-control" id="date1">
                    </div>
                    <div class="form-group no-margin">
                        <label> et </label>&nbsp;
                        <input type="date" class="form-control" id="date2">
                    </div>
                    <a href="javascript:;" class="action-ativity btn btn-aap-primary margin-left-5" data-key="twodates">
                        <i class="fa fa-search"></i>
                    </a>
                </div>
            EOF;
            
           echo createChartPanel("actionActivityDayWeekMonth", [
                "html" => $htmlActionActivityDayWeekMonth,
                "tag" => "checkActions",
                "bodyStyle" => "min-height: 400px;",
                "containerStyle" => "max-height: 98vh; overflow-y: auto",
                "col" => 6,
                "isActive" => !isset($config["actionActivityDayWeekMonth"]) || !empty($config["actionActivityDayWeekMonth"])
            ]);

            $allTime = Yii::t("common","All the time");
            $year = Yii::t("common","Year");
            $htmlExpenditureFinancingPaymentHistory = <<<EOF
                <div class="btn-group">
                    <a href="javascript:;" class="btn btn-default expenditureFinancingPaymentHistory active" data-id="all">$allTime</a>
                    <a href="javascript:;" class="btn btn-default expenditureFinancingPaymentHistory" data-id="year">$year</a>
                </div>
            EOF;
            
            echo createChartPanel("expenditureFinancingPaymentHistory", [
                "html" => $htmlExpenditureFinancingPaymentHistory, "tag" => "checkFinancement",
                "bodyStyle" => "min-height: 400px;",
                "containerStyle" => "max-height: 98vh; overflow-y: auto",
                "col" => 6,
                "isActive" => !isset($config["expenditureFinancingPaymentHistory"]) || !empty($config["expenditureFinancingPaymentHistory"])
            ]);
            
            echo createChartPanel("actionPerPersonByYear", [
                "tag" => "checkActions",
                "bodyStyle" => "min-height: 400px;",
                "containerStyle" => "max-height: 98vh; overflow-y: auto",
                "col" => 6,
                "isActive" => !isset($config["actionPerPersonByYear"]) || !empty($config["actionPerPersonByYear"])
            ]);

            $taskPerPerson = <<<EOF
                <div class="btn-group">
                    <a href="javascript:;" class="btn btn-default TaskPerPerson active" data-id="runningTaskPerPerson">En cours</a>
                    <a href="javascript:;" class="btn btn-default TaskPerPerson" data-id="todoTaskPerPerson">A faire</a>
                    <a href="javascript:;" class="btn btn-default TaskPerPerson" data-id="doneTaskPerPerson">Terminé</a>
                </div>
            EOF;
            
            echo createChartPanel(["runningTaskPerPerson","todoTaskPerPerson","doneTaskPerPerson"], [
                "html" => $taskPerPerson, "tag" => "checkActions",
                "bodyStyle" => "min-height: 400px;",
                "containerStyle" => "max-height: 98vh; overflow-y: auto",
                "col" => 6,
                "isActive" => !isset($config["runningTaskPerPerson"]) || !isset($config["todoTaskPerPerson"]) || !isset($config["doneTaskPerPerson"]) ||
                !empty($config["runningTaskPerPerson"]) || !empty($config["todoTaskPerPerson"]) || !empty($config["doneTaskPerPerson"])
            ]);

            $taskPerProject = <<<EOF
                <div class="btn-group">
                    <a href="javascript:;" class="btn btn-default TaskPerProject active" data-id="runningTaskPerProject">En cours</a>
                    <a href="javascript:;" class="btn btn-default TaskPerProject" data-id="todoTaskPerProject">A faire</a>
                    <a href="javascript:;" class="btn btn-default TaskPerProject" data-id="doneTaskPerProject">Terminé</a>
                </div>
            EOF;
            
            echo createChartPanel(["runningTaskPerProject","todoTaskPerProject","doneTaskPerProject"], [
                "html" => $taskPerProject, "tag" => "checkActions",
                "bodyStyle" => "min-height: 400px;",
                "containerStyle" => "max-height: 98vh; overflow-y: auto",
                "col" => 6,
                "isActive" => !isset($config["runningTaskPerProject"]) || !isset($config["todoTaskPerProject"]) || !isset($config["doneTaskPerProject"]) ||
                !empty($config["runningTaskPerProject"]) || !empty($config["todoTaskPerProject"]) || !empty($config["doneTaskPerProject"])
            ]);
         
           

            echo createChartPanel("dayOffPerPersonne", [
                
                "bodyStyle" => "min-height: 400px;",
                "containerStyle" => "max-height: 98vh; overflow-y: auto",
                "col" => 6,
                "isActive" => !isset($config["dayOffPerPersonne"]) || !empty($config["dayOffPerPersonne"]),
            ]);

            

            echo createChartPanel("financingFlows", [
                "tag" => "checkFinancement",
                "bodyStyle" => "position: relative;min-height:600px",
"containerStyle" => "max-height: 98vh; overflow-y: auto",
                "col" => 12,
                "isActive" => !isset($config["financingFlows"]) || !empty($config["financingFlows"])
            ]);
        ?>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="detailsModal" style="z-index: 111111">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="forAction">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" id="menuModal">
                    <li class="active"><a data-toggle="tab" href="#onglet1">Actions</a></li>
                    <li><a data-toggle="tab" href="#onglet2">Proposition</a></li>
                    <li><a data-toggle="tab" href="#onglet3">Contribution</a></li>
                    <li><a data-toggle="tab" href="#onglet4">Tâche</a></li>
                </ul>

                <!-- Contenu des onglets -->
                <div class="tab-content">
                    <div id="onglet1" class="tab-pane fade in active">
                        <table class="table table-hover historyTable">
                            <thead>
                                <tr>
                                    <th class="historyName">Tâche</th>
                                    <th class="historyDate">Date</th>
                                </tr>
                            </thead>
                            <tbody id="actions">

                            </tbody>
                        </table>
                    </div>
                    <div id="onglet2" class="tab-pane fade">
                        <table class="table table-hover historyTable">
                            <thead>
                                <tr>
                                    <th class="historyName">Tâche</th>
                                    <th class="historyDate">Date</th>
                                </tr>
                            </thead>
                            <tbody id="proposition">

                            </tbody>
                        </table>
                    </div>
                    <div id="onglet3" class="tab-pane fade">
                        <table class="table table-hover historyTable">
                            <thead>
                                <tr>
                                    <th class="historyName">Tâche</th>
                                    <th class="historyDate">Date</th>
                                </tr>
                            </thead>
                            <tbody id="contribution">

                            </tbody>
                        </table>
                    </div>
                    <div id="onglet4" class="tab-pane fade">
                        <table class="table table-hover historyTable">
                            <thead>
                                <tr>
                                    <th class="historyName">Tâche</th>
                                    <th class="historyDate">Date</th>
                                </tr>
                            </thead>
                            <tbody>
                            <tbody id="taches">

                            </tbody>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
            <div class="modal-body" id="forDayoff">
            </div>
        </div>
    </div>
</div>

<script>
    var queryParams = aapObj.common.getQuery();
    var mychart = null;
    var observatoryObj = {
        formId: queryParams.formid,
        context: queryParams.context,
        projectData : {},
        proposalData : {},
        detaildayoff : {},
        id_user : '',
        graphLabels : <?= json_encode($graphLabels) ?>,
        fObj:{},
        init: function(obj) {
            obj.filters(obj);
            obj.events(obj);
        },
        initCharts: function(obj) {

        },
        initMetrics: function(obj) {

        },
        convertTimeStampToDate: function(timestampInSeconds) {

            const months = [
                'Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin',
                'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Decembre'
            ];

            const date = new Date(timestampInSeconds * 1000);
            const year = date.getFullYear();
            const month = months[date.getMonth()]; // Months are 0-based
            const day = String(date.getDate()).padStart(2, '0');
            const hours = String(date.getHours()).padStart(2, '0');
            const minutes = String(date.getMinutes()).padStart(2, '0');
            const seconds = String(date.getSeconds()).padStart(2, '0');

            const formattedDate = `${day}-${month}-${year} ${hours}:${minutes}`;

            return formattedDate;
        },
        metrics: {
            "combineMetrics": function(obj, params) {
                ajaxPost(
                    "",
                    baseUrl + '/co2/aap/observatory/request/combineMetrics', {
                        formId: params.formId,
                        context: params.context
                    },
                    function(data) {
                        $("#totalProject").html(data.projectMetrics.total);
                        $("#runningProject").html(data.projectMetrics.inprogress);
                        $("#doneProject").html(data.projectMetrics.finished);
                        if(data.projectMetrics.finished <= 0) {
                            $("#doneProject").removeClass('cursor-pointer').addClass('cursor-pointer').removeClass("cursor-pointer")
                        }
                        if(data.projectMetrics.inprogress <= 0) {
                            $("#runningProject").removeClass('cursor-pointer').addClass('cursor-pointer').removeClass("cursor-pointer")
                        }
                        observatoryObj.projectData = {
                            inprogress : data.projectMetrics.inprogressData,
                            finished : data.projectMetrics.finishedData
                        }

                        $("#totalActions").removeClass('pointer').addClass('pointer').html(data.actionMetrics.total);
                        $("#todoActions").removeClass('pointer').addClass('pointer').html(data.actionMetrics.todo);
                        $("#runningActions").removeClass('pointer').addClass('pointer').html(data.actionMetrics.running);
                        $("#doneActions").removeClass('pointer').addClass('pointer').html(data.actionMetrics.done);
                        $("#deadlinePassedActions").removeClass('pointer').addClass('pointer').html(data.actionMetrics.deadlinePassed);

                        $("#totalProposal").html(data.proposalMetrics.total);
                        $("#voteProposal").removeClass('cursor-pointer').addClass('cursor-pointer').html(data.proposalMetrics.vote);
                        $("#toBeFinancedProposal").removeClass('cursor-pointer').addClass('cursor-pointer').html(data.proposalMetrics.toBefinanced);
                        $("#inFinancingProposal").removeClass('cursor-pointer').addClass('cursor-pointer').html(data.proposalMetrics.inFinancing);

                        if(data.proposalMetrics.vote <= 0)
                            $("#voteProposal").off("click").removeClass("cursor-pointer")
                        if(data.proposalMetrics.toBefinanced <= 0)
                            $("#toBeFinancedProposal").off("click").removeClass("cursor-pointer")
                        if(data.proposalMetrics.inFinancing <= 0)
                            $("#inFinancingProposal").off("click").removeClass("cursor-pointer")

                        if(data.actionMetrics.total <= 0)
                            $("#totalActions").off("click").removeClass("pointer")
                        if(data.actionMetrics.todo <= 0)
                            $("#todoActions").off("click").removeClass("pointer")
                        if(data.actionMetrics.running <= 0)
                            $("#runningActions").off("click").removeClass("pointer")
                        if(data.actionMetrics.done <= 0)
                            $("#doneActions").off("click").removeClass("pointer")
                        if(data.actionMetrics.deadlinePassed <= 0)
                            $("#deadlinePassedActions").off("click").removeClass("pointer")

                        if (typeof aapObj?.form?.params.onlyAdminCanSeeList != "undefined" && aapObj.form?.params.onlyAdminCanSeeList == true) {
                            if(!aapObj.canEdit) {
                                $("#totalProject,#runningProject,#doneProject,#totalActions,#todoActions,#runningActions,#doneActions,#deadlinePassedActions,#totalProposal,#voteProposal,#toBeFinancedProposal,#inFinancingProposal").off("click").removeClass("cursor-pointer pointer")
                            }
                        }
                        observatoryObj.proposalData = {
                            infinancing : data.proposalMetrics.infinancingData,
                            tobevoted : data.proposalMetrics.tobevotedData,
                            tobefinanced : data.proposalMetrics.tobefinancedData
                        }

                        $("#amountRequested").html(addSeparatorMillier(data.financingMetrics.amountRequested) + ' \u20ac');
                        $("#amountFinanced").html(addSeparatorMillier(data.financingMetrics.amountFinanced) + ' \u20ac');
                        $("#amountSpent").html(addSeparatorMillier(data.financingMetrics.amountSpent) + ' \u20ac');
                        $("#amountRemaining").html(addSeparatorMillier(data.financingMetrics.amountRemaining) + ' \u20ac');
                    }
                );
            }
        },
        charts: {
            "taskPerPerson": function(obj, params) {
//if(document.getElementById('taskPerPerson'))
                ajaxPost(
                    "",
                    baseUrl + '/co2/aap/observatory/request/taskPerPerson', {
                        formId: params.formId,
                        context: params.context
                    },
                    function (data) {
                        const status = {
                            'todo' : {id : "todoTaskPerPerson",label : "Action et sous taches à faire par personne"},
                            'running': {id : "runningTaskPerPerson",label : "Action et sous taches en cours par personne"},
                            'done' : {id : "doneTaskPerPerson",label : "Action et sous taches terminé par personne"},
                        };

                        var todoTaskPers = 
                        {   'todo' : "",
                            'running': "",
                            'done' : "",
                        };
 
                        $.each(status,(ks,vs)=>{
                            $.each(data[ks],function(k,v){
                                if(!exists(trad[v.id])) trad[v.id] = v.name
                                if(v.actionValue == 0 && v.subTaskValue == 0) delete data[ks][k];
                            })

                            const labels = Object.values(data[ks]).map(item => item.id);
                            const datasets = [
                                {
                                    label: 'Action',
                                    data: Object.values(data[ks]).map(item => item.actionValue),
                                    backgroundColor: "green",
                                    stack: 'Stack 0',
                                },
                                {
                                    label: 'Sous taches',
                                    data: Object.values(data[ks]).map(item => item.subTaskValue),
                                    backgroundColor: "red",
                                    stack: 'Stack 1',
                                },
                            ];
                            todoTaskPers[ks] = obj.chartTypes.barStacked({
                                id: vs.id,
                                labels: labels,
                                data: datasets,
                                label: vs.label,
                            });

                            document.getElementById(vs.id).onclick = function(evt) {
                                var activePoints = todoTaskPers[ks].getElementsAtEventForMode(evt, 'point', todoTaskPers[ks].options);
                                var firstPoint = activePoints[0];
                                var personneId = todoTaskPers[ks].data.labels[firstPoint.index];
                                var projectId = "";

                                ajaxPost(
                                    "",
                                    baseUrl +  '/co2/aap/observatory/request/detailKanbanPerPerson',{
                                        formId: params.formId,
                                        context: params.context,
                                        personneId: personneId

                                    },
                                    function (data)
                                    {
                                        if(data.length > 0)
                                        {
                                            projectId = data[0]; 
                                            const query = aapObj.common.getQuery(aapObj);
                                            history.pushState({}, null, aapObj.common.buildUrlQuery(document.location.href, {aapview : "action", "userId":personneId,"projectId":projectId}, aapObj.common.getQueryStringObject()));
                                            aapObj.common.modalProjectDetail(aapObj, projectId);
                                            aapObj.common.loadProjectDetail(aapObj, queryParams.projectId, queryParams.aapview);
                                        }
                                        else{
                                            alert('Pas de kanban');
                                        }
                                    }
                                );
                            };
                        })
                    }
                );
            },
            "taskPerProject": function(obj, params) {
                //if(document.getElementById('taskPerProject'))
                ajaxPost(
                    "",
                    baseUrl + '/co2/aap/observatory/request/taskPerProject', {
                        formId: params.formId,
                        context: params.context
                    },
                    function (data) {
                        const status = {
                            'todo' : {id : "todoTaskPerProject",label : "Action et sous taches à faire par project"},
                            'running': {id : "runningTaskPerProject",label : "Action et sous taches en cours par project"},
                            'done' : {id : "doneTaskPerProject",label : "Action et sous taches terminé par project"},
                        };

                        var tdTaskPerProject = {
                            'todo' : "",
                            'running': "",
                            'done' : "",
                        };

                        $.each(status,(ks,vs)=>{
                            $.each(data[ks],function(k,v){
                                if(!exists(trad[v.id])) trad[v.id] = v.name
                                if(v.actionValue == 0 && v.subTaskValue == 0) delete data[ks][k];
                            })

                            const labels = Object.values(data[ks]).map(item => item.id);
                            const datasets = [
                                {
                                    label: 'Action',
                                    data: Object.values(data[ks]).map(item => item.actionValue),
                                    backgroundColor: "green",
                                    stack: 'Stack 0',
                                },
                                {
                                    label: 'Sous taches',
                                    data: Object.values(data[ks]).map(item => item.subTaskValue),
                                    backgroundColor: "red",
                                    stack: 'Stack 1',
                                },
                            ];
                            tdTaskPerProject[ks] = obj.chartTypes.barStacked({
                                id: vs.id,
                                labels: labels,
                                data: datasets,
                                label: vs.label,
                            });

                            document.getElementById(vs.id).onclick = function(evt) {
                                var activePoints = tdTaskPerProject[ks].getElementsAtEventForMode(evt, 'point', tdTaskPerProject[ks].options);
                                var firstPoint = activePoints[0];
                                var projectId = tdTaskPerProject[ks].data.labels[firstPoint.index];

                                ajaxPost(
                                    "",
                                    baseUrl +  '/co2/aap/observatory/request/testProjetWithFinancement',{
                                        formId: params.formId,
                                        context: params.context,
                                        projectId: projectId

                                    },
                                    function (data)
                                    {
                                        const query = aapObj.common.getQuery(aapObj);
                                        history.pushState({}, null, aapObj.common.buildUrlQuery(document.location.href, {aapview : "action", "projectId":projectId}, aapObj.common.getQueryStringObject()));
                                        aapObj.common.modalProjectDetail(aapObj, projectId);
                                        if(data.nombre < 1)
                                        {
                                            $("#fundings").hide();
                                        }
                                        else{
                                            $("#fundings").show();
                                        }
                                    }
                                );

                                
                            };
                        })
                    }
                );
            },
            "actionPerPersonByYear": function(obj, params) {
                //if(document.getElementById('actionPerPersonByYear'))
                ajaxPost(
                    "",
                    baseUrl + '/co2/aap/observatory/request/actionPerPersonByYear', {
                        formId: params.formId,
                        context: params.context
                    },
                    function(data) {
                        var the_chart = null;
                        if(data?.membersData) {
                            $.each(data.membersData, function(k, v) {
                                if (!exists(trad[k])) trad[k] = v.name
                            });

                            the_chart = obj.chartTypes.barStacked({
                                id: "actionPerPersonByYear",
                                labels: data.years,
                                data: Object.values(data.membersData),
                                label: obj.graphLabels['actionPerPersonByYear'],

                            });

                            document.getElementById("actionPerPersonByYear").onclick = function(evt) {
                            var activePoints = the_chart.getElementsAtEventForMode(evt, 'point', the_chart.options);
                            var firstPoint = activePoints[0];
                            var label = the_chart.data.labels[firstPoint.index];
                            var value = the_chart.data.datasets[firstPoint.datasetIndex]._id;
                            ajaxPost(
                                "",
                                baseUrl + '/co2/aap/observatory/request/userHistories', {
                                    formId: obj.formId,
                                    personneId: value,
                                    context: obj.context
                                },
                                function(data) {
                                    $("#menuModal").show()
                                    $("#forDayoff").hide();
                                    $('#forAction').show();                                   
                                    $('#detailsModal').modal('show');
                                    $(".modal-title").html(data.citoyen + " historiques");

                                    var content = "";
                                    $.each(data.action, function(k, v) {
                                        content += '<tr><td class="historyName historyTable" data-id=' + v._id + '>' + v.name + '</td><td class="historyDate historyTable">' + obj.convertTimeStampToDate(v.created) + '</td></tr>';
                                    })
                                    $("#actions").html(content);


                                    var content = "";
                                    $.each(data.proposition, function(k, v) {
                                        content += '<tr><td class="historyName historyTable" data-id=' + v._id + '>' + v.answers.aapStep1.titre + '</td><td class="historyDate historyTable">' + obj.convertTimeStampToDate(v.created) + '</td></tr>';
                                    })
                                    $("#proposition").html(content);

                                    content = "";
                                    var content = "";
                                    $.each(data.tache, function(k, v) {
                                        content += '<tr><td class="historyName historyTable" data-id=' + v._id + '>' + v.name + '</td><td class="historyDate historyTable">' + obj.convertTimeStampToDate(v.created) + '</td></tr>';
                                    })
                                    $("#taches").html(content);

                                    content = "";
                                    var content = "";
                                    $.each(data.contribution, function(k, v) {
                                        content += '<tr><td class="historyName historyTable" data-id=' + v._id + '>' + v.name + '</td><td class="historyDate historyTable">' + obj.convertTimeStampToDate(v.created) + '</td></tr>';
                                    })
                                    $("#contribution").html(content);
                                }
                            );

                        };
                        }

                        

                        var listLi = '<li><input type="text" style="width:100%;heigth:100%" id="graph-search" autofocus></li>';
                        listLi += '<li><a href="javascript:;" class="user-graph" data-value="">Tout le monde</a></li>';      
                        $.each(data.membersData, function(key){
                            listLi += '<li><a href="javascript:;" class="user-graph" data-value="'+key+'">'+trad[key]+'</a></li>';
                        });
                        var styles = {
                            "width": "auto",
                            "height": "20em",
                            "line-height": "2em",
                            "border": "1px solid #ccc",
                            "padding": 0,
                            "margin": 0,
                            "overflow": "scroll",
                            "overflow-x": "hidden",
                        };

                        $("#ListofUser").html(listLi);
                        $("#ListofUser").css(styles);

                        $('.user-graph').click(function(){
                            const filters = observatoryObj.fObj.search.obj.filters;
                            const contexts = notEmpty(filters?.context) ? filters?.context : [aapObj.context.id];
                            const text = $(this).html()+'&nbsp;<span class="caret"></span>';
                            const params = {
                                formId: obj.formId,
                                context: contexts,
                                id_user: $(this).data('value'),
                            }
                            obj.activity_section(obj, params);
                            $("#btn-user-name").html(text);
                            if($('button[data-key="showTwoDate"]').hasClass('active') == true && $('.date-between').hasClass('hidden') == false)
                            {
                                $('a[data-key="twodates"]').click();
                            }

                        });

                        $('#graph-search').on('input',function(){
                            var userg = $('.user-graph');
                            userg.css('display', 'none');
                            var text = $(this).val().toUpperCase(); // valeur à chercher
                            const regex = new RegExp(text);
                            $.each(userg, function(){
                                var nom = $(this).text().toUpperCase();
                                var tag = $(this);
                                if(regex.test(nom) == true)
                                {
                                    tag.show();
                                }
                            });
                        });
                    }
                );
            },
            "financingFlows": function(obj, params) {
                //if(document.getElementById('financingFlows'))
                ajaxPost(
                    "",
                    baseUrl + '/co2/aap/observatory/request/financingFlows', {
                        formId: params.formId,
                        context: params.context
                    },
                    function(data) {
                        /*data = {
                            "colors": {
                                "Technopole de La Réunion- Incubateur Régional": "#18C82B",
                                "MEIR , écosystème de l'Innovation Réunionaise": "#14BFAD",
                                "Tibor Katelbach": "#437952"
                            },
                            "labels": [
                                "Technopole de La Réunion- Incubateur Régional",
                                "MEIR , écosystème de l'Innovation Réunionaise",
                                "Tibor Katelbach"
                            ],
                            "flows": {
                                "Technopole de La Réunion- Incubateur Régional---MEIR , écosystème de l'Innovation Réunionaise": {
                                    "from": "Technopole de La Réunion- Incubateur Régional",
                                    "to": "MEIR , écosystème de l'Innovation Réunionaise",
                                    "flow": 26000
                                },
                                "MEIR , écosystème de l'Innovation Réunionaise---Tibor Katelbach": {
                                    "from": "MEIR , écosystème de l'Innovation Réunionaise",
                                    "to": "Tibor Katelbach",
                                    "flow": 13000
                                }
                            }
                        }*/
                        // if(notEmpty(data.flows))
                        obj.chartTypes.sankey({
                            id: "financingFlows",
                            labels: data.labels,
                            data: Object.values(data.flows),
                            colors: data.colors,
                            label: obj.graphLabels['financingFlows'],

                        });
                    }
                );
            },
            "expenditureFinancingPaymentHistory": function(obj, params) {
                if(document.getElementById('expenditureFinancingPaymentHistory'))
                ajaxPost(
                    "",
                    baseUrl + '/co2/aap/observatory/request/expenditureFinancingPaymentHistory', {
                        formId: params.formId,
                        context: params.context,
                        filter: params.filter
                    },
                    function(data) {
                        expFinPayment = obj.chartTypes.line({
                            id: "expenditureFinancingPaymentHistory",
                            labels: Object.keys(data.depense),
                            data: [{
                                    label: 'Financement demandé',
                                    data: Object.values(data.depense),
                                    borderColor: "red",
                                    backgroundColor: "red",
                                },
                                {
                                    label: 'Financé',
                                    data: Object.values(data.financement),
                                    borderColor: "green",
                                    backgroundColor: "green",
                                },
                                {
                                    label: 'Payement',
                                    data: Object.values(data.payment),
                                    borderColor: "blue",
                                    backgroundColor: "blue",
                                }
                            ],
                            label: obj.graphLabels['expenditureFinancingPaymentHistory'],

                        })

                        document.getElementById("expenditureFinancingPaymentHistory").onclick = function(evt) {
                            var activePoints = expFinPayment.getElementsAtEventForMode(evt, 'point', expFinPayment.options);
                            var firstPoint = activePoints[0];

                        };
                    }
                );
            },
            "weekActionActivity": function(obj, params) {
                if(document.getElementById('actionActivityDayWeekMonth'))
                ajaxPost(
                    "",
                    baseUrl + '/co2/aap/observatory/request/weekActionActivity', {
                        formId: params.formId,
                        context: params.context,
                        filter: params.filter,
                        id_user: (params.id_user) ? params.id_user : null,
                    },
                    function(data) {
                        obj.chartTypes.line({
                            id: "actionActivityDayWeekMonth",
                            labels: Object.keys(data.done),
                            data: [{
                                    label: 'Tache à faire',
                                    data: Object.values(data.todo),
                                    borderColor: "red",
                                    backgroundColor: "red",
                                },
                                {
                                    label: 'Tache en cours',
                                    data: Object.values(data.running),
                                    borderColor: "blue",
                                    backgroundColor: "blue",
                                },
                                {
                                    label: 'Tache términé',
                                    data: Object.values(data.done),
                                    borderColor: "green",
                                    backgroundColor: "green",
                                }
                            ],
                            label: "Activité de cette semaine",
                        })
                    }
                );
            },
            "betweenTwoDateActivity": function(obj, params) {
            if(document.getElementById('actionActivityDayWeekMonth'))
                ajaxPost(
                    "",
                    baseUrl + '/co2/aap/observatory/request/betweenTwoDateActivity', {
                        formId: params.formId,
                        context: params.context,
                        date1: params.date1,
                        date2: params.date2,
                        id_user: (params.id_user) ? params.id_user : null,
                    },
                    function(data) {
                        ActionActDWM = obj.chartTypes.line({
                            id: "actionActivityDayWeekMonth",
                            labels: Object.keys(data.done),
                            data: [{
                                    label: 'Tache à faire',
                                    data: Object.values(data.todo),
                                    borderColor: "red",
                                    backgroundColor: "red",
                                    status: "todo",
                                },
                                {
                                    label: 'Tache en cours',
                                    data: Object.values(data.running),
                                    borderColor: "blue",
                                    backgroundColor: "blue",
                                    status: "running",
                                },
                                {
                                    label: 'Tache términé',
                                    data: Object.values(data.done),
                                    borderColor: "green",
                                    backgroundColor: "green",
                                    status: "done",
                                }
                            ],
                            label: "Activité entre deux dates",
                        })

                        document.getElementById("actionActivityDayWeekMonth").onclick = function(evt, id_user) {
                            var activePoints = ActionActDWM.getElementsAtEventForMode(evt, 'point', ActionActDWM.options);
                            var firstPoint = activePoints[0];
                            
                            var date = ActionActDWM.data.labels[firstPoint.index];
                            var status = ActionActDWM.data.datasets;

                            var clickedDataset = ActionActDWM.data.datasets[firstPoint.datasetIndex];
                            var clickedLabel = clickedDataset.status;
                            var nbTask = clickedDataset.data[firstPoint.index];
                            var id_user = (params.id_user) ? params.id_user : null;

                            let tradStatus = {
                                "todo": 'tache à faire',
                                "running": 'tache en cours',
                                "done": 'tache términé'
                            };
                            if(nbTask > 0)
                            {
                                ajaxPost(
                                    "",
                                    baseUrl+'/co2/aap/observatory/request/oneDateActivitylist',
                                    {
                                        formId : params.formId,
                                        context : params.context,
                                        date: date, 
                                        status: clickedLabel,
                                        id_user: id_user
                                    },
                                    function(data){
                                        $("#menuModal").hide();
                                        $("#forDayoff").hide();
                                        $('#forAction').show();
                                        $('#detailsModal').modal('show');
                                        $(".modal-title").html(tradStatus[clickedDataset.status] + " le " + date);
                                        var content = '';
                                    
                                        $.each(data, function(k, v) {
                                            content += '<tr><td class="historyName historyTable" data-id=' + v._id + '>' + v.name + ' par '+v.name_creator+'</td><td class="historyDate historyTable">' + obj.convertTimeStampToDate(v.created) + '</td></tr>';
                                        })
                                        $("#actions").html(content);
                                    }
                                );
                            }
                            
                        };
                    }
                );
            },
            "oneDayActionActivity" : function(obj,params){
                if(document.getElementById('actionActivityDayWeekMonth'))
                ajaxPost(
                    "",
                    baseUrl+'/co2/aap/observatory/request/oneDayActionActivity',
                    {
                        formId : params.formId,
                        context : params.context,
                        filter : params?.filter ?? "today",
                        id_user: (params.id_user) ? params.id_user : null,
                    },
                    function (data) {
                        $.each(data.done,function(k,v){
                            
                            trad[k] = v.label;
                            mylog.log(k,v,trad[k],"mkv")
                        })
                        obj.chartTypes.line({
                            id : "actionActivityDayWeekMonth",
                            labels : Object.keys(data.done),
                            data : [
                                {
                                    label: 'Tache à faire',
                                    data: Object.values(data.todo).map(v => v.value),
                                    borderColor: "red",
                                    backgroundColor: "red",
                                },
                                {
                                    label: 'Tache en cours',
                                    data: Object.values(data.running).map(v => v.value),
                                    borderColor: "blue",
                                    backgroundColor: "blue",
                                },
                                {
                                    label: 'Tache términé',
                                    data: Object.values(data.done).map(v => v.value),
                                    borderColor: "green",
                                    backgroundColor: "green",
                                }
                            ],
                            label : "Activité aujourd'hui",
                            click : function(x,y){
                                alert(x);
                            }
                        })
                    }
                );
            },
            "calendarOffAll":  function(obj,params){
                ajaxPost(
                    "",
                    baseUrl + '/co2/aap/observatory/request/dayOffPerPersonne', {
                        formId: params.formId,
                        context: params.context
                    },
                    function(data) {
                        obj.chartTypes.heatMap({
                                id: "calendarOffAll",
                                labels: 'nb personne absent : ',
                                label: obj.graphLabels['calendarOffAll'],
                                data: data?.calendrier
                            });
                    });
                }, 
            "dayOffPerPersonne": function(obj,params){
                ajaxPost(
                    "",
                    baseUrl + '/co2/aap/observatory/request/dayOffPerPersonne', {
                        formId: params.formId,
                        context: params.context
                    },
                    function(data) {
                        var dayoffchart = null;
                        obj.detaildayoff = data.details;
                        if(data?.membersData) {
                            var  listLi = '<li><a href="javascript:;" class="user-graphs" data-value="">Tout le monde</a></li>';  
                                $.each(data.membersData, function(k, v){
                                    trad[k] = v.name
                                    listLi += '<li><a href="javascript:;" class="user-graphs" data-value="'+v.label+'">'+trad[k]+'</a></li>';
                            });
                            
                            var listDate = '<li><a href="javascript:;" class="date-graphs" data-value="">Par défaut</a></li>';

                            $.each(data.years, function(k, v){
                                    
                                    listDate += '<li><a href="javascript:;" class="date-graphs" data-value="'+v+'">'+v+'</a></li>';
                            });
                            
                            dayoffchart = obj.chartTypes.barStacked({
                                id: "dayOffPerPersonne",
                                labels: data.years,
                                data: Object.values(data.membersData),
                                label: obj.graphLabels['dayOffPerPersonne'],

                            });

                                var styles = {
                                "width": "auto",
                                "height": "20em",
                                "line-height": "2em",
                                "border": "1px solid #ccc",
                                "padding": 0,
                                "margin": 0,
                                "overflow": "scroll",
                                "overflow-x": "hidden",
                            };

                            var detaisPersonne = null;

                            document.getElementById("dayOffPerPersonne").onclick = function(evt) {
                                var activePoints = dayoffchart.getElementsAtEventForMode(evt, 'point', dayoffchart);
                                var firstPoint = activePoints[0];
                                var personneId = dayoffchart.data.datasets[firstPoint.datasetIndex].label.toString();
                                ajaxPost(
                                "",
                                baseUrl + '/co2/aap/observatory/request/dayOffPerPersonne', {
                                    formId: params.formId,
                                    context: params.context,
                                    id_user: personneId,
                                },
                                function(data) {
                                        var content = '<table class="table align-items-center mb-0" cellspacing="0"><thead><tr><th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7"> Déscription</th><th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Date de début</th><th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Date de fin</th><th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Total</th></tr></thead><tbody>';
                                        $.each(data.details, (k, v) => {
                                            const sd = new Date(v.startDate );
                                            const ed = new Date(v.endDate );
                                            var startDate = sd.getDate()+' '+ sd.toLocaleDateString('fr-FR', {month: 'long'})+' '+ sd.getFullYear();
                                            var endDate = ed.getDate()+' '+ ed.toLocaleDateString('fr-FR', {month: 'long'})+' '+ ed.getFullYear();
                                            content += '<tr><td class="historyName historyTable" data-id=' + k+ '>' + v.name +'</td><td class="historyDate historyTable"> du ' + startDate + '</td class="historyDate historyTable"><td> jusqu\' au '+endDate+'</td><td>'+Object.keys(v.list_jour).length+' jour(s)</td></tr>';
                                        });
                                        content += '</tbody></table>'
                                        $('#detailsModal').modal('show');
                                        $('#forAction').hide();
                                        $("#forDayoff").show();
                                        $(".modal-title").html('les jours offs de '+data.membersData[0].name);
                                        $('#forDayoff').html(content);

                                        obj.chartTypes.heatMap({
                                            id: "calendarOffAll",
                                            labels: 'nb personne absent : ',
                                            label: obj.graphLabels['calendarOffAll'],
                                            data: data?.calendrier
                                        });
                                });
                
                            };
                        }

                       
                        

                        $("#ListofUsers").html(listLi);
                        $("#ListofUsers").css(styles);
                        $("#Listofdate").html(listDate);
                        $("#Listofdate").css(styles);
                        
                        

                        $('.user-graphs').click(function(){
                            const filters = observatoryObj.fObj.search.obj.filters;
                            const contexts = notEmpty(filters?.context) ? filters?.context : [aapObj.context.id];
                            const text = $(this).html()+'&nbsp;<span class="caret"></span>';
                            obj.id_user = $(this).data('value');
                            $("#btn-user-names").html(text);
                            ajaxPost(
                            "",
                            baseUrl + '/co2/aap/observatory/request/dayOffPerPersonne', {
                                formId: obj.formId,
                                context: contexts,
                                id_user: obj.id_user,
                            },
                            function(data) {

                                    obj.chartTypes.heatMap({
                                    id: "calendarOffAll",
                                    labels: 'nb personne absent : ',
                                    label: obj.graphLabels['calendarOffAll'],
                                    data: data?.calendrier
                                });
                            });
                            const d = new Date();
                            let today = new Date(d.getFullYear(), d.getMonth(), d.getDate(), 0, 0, 0, 0);
                            let start = new Date(new Date().setDate(today.getDate() - 365));
                
                            ajaxPost(
                            "",
                            baseUrl + '/co2/aap/observatory/request/betweenTwoDateActivity', {
                                formId: obj.formId,
                                context: contexts,
                                id_user: obj.id_user,
                                date1: start.getFullYear()+"-"+start.getMonth()+"-"+ start.getDate(),
                                date2: today.getFullYear()+"-"+today.getMonth()+"-"+ today.getDate(),
                                id_user: obj.id_user,
                            },
                            function(data) {
                                console.log('hre');
                                console.log(data);
                                obj.chartTypes.heatMap({
                                        id: "calendarActivity",
                                        labels: 'nombre d\'activité bouclé : ',
                                        label: obj.graphLabels['calendarActivity'],
                                        data: data?.calendrier
                                    });
                            });



                        });

                        $('.date-graphs').click(function(){
                            const filters = observatoryObj.fObj.search.obj.filters;
                            const contexts = notEmpty(filters?.context) ? filters?.context : [aapObj.context.id];
                            const text = $(this).html()+'&nbsp;<span class="caret"></span>';
                            const year =  $(this).data('value');
                            $("#btn-date-names").html(text);
                            ajaxPost(
                            "",
                            baseUrl + '/co2/aap/observatory/request/dayOffPerPersonne', {
                                formId: obj.formId,
                                context: contexts,
                                id_user: obj.id_user,
                            },
                            function(data) {
                                    obj.chartTypes.heatMap({
                                    id: "calendarOffAll",
                                    labels: 'nb personne absent : ',
                                    label: obj.graphLabels['calendarOffAll'],
                                    data: data?.calendrier, 
                                    year: year
                                });
                            });

                            const d = new Date();
                            let today = new Date(year+1, 0, 1, 0, 0, 0, 0);
                            let start = new Date(year, 0, 1, 0, 0, 0, 0);
                
                            ajaxPost(
                            "",
                            baseUrl + '/co2/aap/observatory/request/betweenTwoDateActivity', {
                                formId: obj.formId,
                                context: contexts,
                                id_user: obj.id_user,
                                date1: start.getFullYear()+"-"+start.getMonth()+"-"+ start.getDate(),
                                date2: today.getFullYear()+"-"+today.getMonth()+"-"+ today.getDate(),
                                id_user: obj.id_user,
                            },
                            function(data) {
                                console.log('hre');
                                console.log(data);
                                obj.chartTypes.heatMap({
                                        id: "calendarActivity",
                                        labels: 'nombre d\'activité bouclé : ',
                                        label: obj.graphLabels['calendarActivity'],
                                        data: data?.calendrier
                                    });
                            });

                        });

                        

                        

                    })
            },
            "calendarActivity": function(obj,params)
            {
                const d = new Date();
                let today = new Date(d.getFullYear(), d.getMonth(), d.getDate(), 0, 0, 0, 0);
                let start = new Date(new Date().setDate(today.getDate() - 365));
                ajaxPost(
                    "",
                    baseUrl + '/co2/aap/observatory/request/betweenTwoDateActivity', {
                        formId: params.formId,
                        context: params.context,
                        date1: start.getFullYear()+"-"+start.getMonth()+"-"+ start.getDate(),
                        date2: today.getFullYear()+"-"+today.getMonth()+"-"+ today.getDate(),
                        id_user: (params.id_user) ? params.id_user : null,
                    },
                    function(data) {
                        console.log('hre');
                        console.log(data);
                        obj.chartTypes.heatMap({
                                id: "calendarActivity",
                                labels: 'nombre d\'activité bouclé : ',
                                label: obj.graphLabels['calendarActivity'],
                                data: data?.calendrier
                            });
                    });
            },

        },
        chartTypes: {
            bar: function(opt) {
                const ctx = document.getElementById(opt.id);
                const chartData = {
                    labels: opt.labels,
                    datasets: [{
                        label: opt.label,
                        data: opt.data,
                        borderWidth: 0,
                        backgroundColor: 'rgb(155 193 37)',
                    }]
                }
                const chartOptions = {
                    indexAxis: 'y',
                    responsive: true,
                    scales: {
                        x: {},
                        y: {
                            beginAtZero: true,
                            ticks: {
                                callback: function(value, index, values) {
                                    var value = chartData.labels[index];
                                    return exists(trad[value]) ? trad[value] : value;
                                }
                            }
                        }
                    },
                    plugins: {
                        title: {
                            display: true,
                            text: opt.label.toUpperCase(),
                            fullSize : true,
                            font: {
                                size: 16
                            }
                        },
                        datalabels: {
                            formatter: (value, ctx) => {
                                return value
                            },
                            anchor: 'end',
                            align: 'end',
                            color: '#000',
                            font: {
                                //size : 25,
                                weight: 'bold'
                            }
                        },
                        tooltip: {
                            enabled: true,
                            callbacks: {
                                label: function(context) {
                                    var label = context.dataset.label || '';
                                    if (label) {
                                        label += ': ';
                                    }
                                    label += context.parsed.x;
                                    return label;
                                },
                                title: function(context) {
                                    const key = context[0].label;
                                    return exists(trad[key]) ? trad[key] : key; // Customize the tooltip title
                                },
                            }
                        }
                    }
                }


                if (exists(observatoryObj[opt.id])) observatoryObj[opt.id].destroy();
                observatoryObj[opt.id] = new Chart(ctx, {   
                    plugins: [ChartDataLabels],
                    type: 'bar',
                    data: chartData,
                    options: chartOptions
                });
                return observatoryObj[opt.id];
            },
            barStacked: function(opt) {
                const ctx = document.getElementById(opt.id);
                const chartData = {
                    labels: opt.labels,
                    datasets: opt.data
                };
                const chartOptions = {
                    indexAxis: 'y',
                    plugins: {
                        title: {
                            display: true,
                            text: opt.label.toUpperCase(),
                            fullSize : true,
                            font: {
                                size: 16
                            }
                        },
                        tooltip: {
                            enabled: true,
                            callbacks: {
                                label: function(context) {
                                    var label = exists(trad[context.dataset.label]) ? trad[context.dataset.label] : context.dataset.label;
                                    label += ': ' + context.parsed.x;
                                    return label;
                                },
                                title: function(context) {
                                    const key = context[0].label;
                                    return exists(trad[key]) ? trad[key] : key; // Customize the tooltip title
                                },
                            }
                        }
                    },
                    responsive: true,
                    interaction: {
                        intersect: false,
                    },
                    scales: {
                        x: {
                            stacked: true,
                        },
                        y: {
                            stacked: true,
                            ticks: {
                                callback: function(value, index, values) {
                                    var value = chartData.labels[index];
                                    return exists(trad[value]) ? trad[value] : value;
                                }
                            }
                        },
                        onClick: (e) => {

                        }
                    }
                }
                const config = {
                    type: 'bar',
                    data: chartData,
                    options: chartOptions
                };

                if (exists(observatoryObj[opt.id])) observatoryObj[opt.id].destroy();
                observatoryObj[opt.id] = new Chart(ctx, config);
                return observatoryObj[opt.id];
            },
            line: function(opt) {
                const ctx = document.getElementById(opt.id);
                const chartData = {
                    labels: opt.labels,
                    datasets: opt.data,
                };
                const chartOptions = {
                    responsive: true,
                    scales: {
                        x: {
                            beginAtZero: true,
                            ticks: {
                                callback: function(value, index, values) {
                                    var value = chartData.labels[index];
                                    return exists(trad[value]) ? trad[value] : value;
                                }
                            }
                        }
                    },
                    plugins: {
                        legend: {
                            position: 'top',
                        },
                        title: {
                            display: true,
                            text: opt.label.toUpperCase(),
                            fullSize : true,
                            font: {
                                size: 16
                            }
                        }
                    },
                    tooltip: {
                        enabled: true,
                        callbacks: {
                            label: function(context) {
                                var label = context.dataset.label || '';
                                if (label) {
                                    label += ': ';
                                }
                                label += context.parsed.x;
                                return label;
                            },
                            title: function (context) {
                                const key = context[0].label;
                                return exists(trad[key]) ? trad[key] : key ; // Customize the tooltip title
                            },
                        }
                    },
                    onClick: (e) => {
                        const canvasPosition = Chart.helpers.getRelativePosition(e, observatoryObj[opt.id]);
                        const dataX = observatoryObj[opt.id].scales.x.getValueForPixel(canvasPosition.x);
                        const dataY = observatoryObj[opt.id].scales.y.getValueForPixel(canvasPosition.y);
                        //opt.click(dataX,dataY)
                    }
                };

                if (exists(observatoryObj[opt.id])) observatoryObj[opt.id].destroy();
                observatoryObj[opt.id] = new Chart(ctx, {
                    type: 'line',
                    data: chartData,
                    options: chartOptions
                });
                return observatoryObj[opt.id];
            },
            sankey: function(opt) {
                $("#"+opt.id).show();
                $("#"+opt.id).parent().css("min-height", 600)
                const ctx = document.getElementById(opt.id).getContext('2d');
                if(opt.data.length > 0) {
                    const colors = opt.colors;

                    function getColor(name) {
                        return colors[name] || "green";
                    }
                    const chartData = {
                        labels: opt.labels,
                        datasets: [{
                            data: opt.data,
                            colorFrom: (c) => getColor(c.dataset.data[c.dataIndex].from),
                            colorTo: (c) => getColor(c.dataset.data[c.dataIndex].to),
                            borderWidth: 2,
                            borderColor: 'black',
                            size: 'max'
                        }, ],
                    };
                    const lastNodeX = 50; // Adjust the X-coordinate as needed
                    const lastNodeY = 50;
                    const options = {
                        responsive: true,
                        maintainAspectRatio: false,
                        plugins: {
                            title: {
                                display: true,
                                text: opt.label.toUpperCase(),
                                fullSize : true,
                                font: {
                                    size: 16
                                }
                            },
                            sankey: {
                                node: {
                                    width: 30, // Adjust the width of nodes as needed
                                },
                            },
                        },
                        layout: {
                            padding: {
                                top: 10, // Adjust the top padding for the last node
                                bottom: 10, // Adjust the bottom padding for the last node
                            },
                        },
                    };


                    if (exists(observatoryObj[opt.id])) observatoryObj[opt.id].destroy();
                    observatoryObj[opt.id] = new Chart(ctx, {
                        type: 'sankey',
                        data: chartData,
                        options: options,
                    });
                    return observatoryObj[opt.id];
                } else {
                    $("#"+opt.id).parent().find(".nodatamsg").remove()
                    if(exists(observatoryObj[opt.id])) observatoryObj[opt.id].destroy();
                    $("#"+opt.id).hide();
                    $("#"+opt.id).parent().css("min-height", "")
                    $("#"+opt.id).parent().append(`
                        <div class="text-center nodatamsg col-xs-12 no-padding">
                            <h4 class="text-center">${opt.label}</h4> 
                            <h5 class="text-center">Aucun données à afficher</h5>
                        </div>
                    `);
                }
            }, 
            heatMap: function(opt){
                const ctx = document.getElementById(opt.id);

                const d = new Date();
                let today = new Date(d.getFullYear(), d.getMonth(), d.getDate(), 0, 0, 0, 0);
                const data2 = new Array();
                let end = today;
                let dt = new Date(new Date().setDate(end.getDate() - 365));

                if(opt.year  !== undefined && opt.year != '')
                {
                   
                    end = new Date(opt.year, 11, 30, 0, 0, 0, 0);
                    dt = new Date(opt.year, 0, 1, 0, 0, 0, 0);
                }

                while(dt <= end)
                {
                    const iso = dt.toISOString().substr(0, 10);
                    let wd = dt.getDay(); 
                        wd = (wd + 6)%7+1;
                        wd =  '' + wd;
                    if(dt.getDay() > 0 && dt.getDay() <6)
                    {
                        let vals = {
                                x: iso,
                                y: wd,
                                d: iso,
                                v: 0 
                            };
                        if(opt.data.hasOwnProperty(iso))
                        {
                            vals  = opt.data[iso];
                        }
                        data2.push(vals);
                    }
                    
                    
                    dt = new Date(dt.setDate(dt.getDate() + 1))
                }

                        
                        const datas = {
                            datasets: [{
                                label: 'les jours off',
                                data : data2,
                                backgroundColor(c){
                                    const value = c.dataset.data[c.dataIndex].v;
                                    var alpha = (value==0) ? 0.0 : (value >= 1 && value < 2) ? 0.4: (value >= 2 && value <= 4) ? 0.6: (value >= 5 && value <= 7) ? 0.8 : 1 ;
                                    return `rgba(51, 15, 228, ${alpha})`;
                                },
                                borderColor: 'rgba(26, 11, 102)',
                                borderRadius: 1,
                                borderWidth: 1, 
                                hoverBackgroundColor: 'rgba(255, 255, 51, 0.2)',
                                hoverBorderColor: 'rgba(255, 128, 0, 1)',
                                width(c){
                                const a = c.chart.chartArea || {};
                                return (a.right - a.left) / 53 - 1;
                                },
                                height(c){
                                const a = c.chart.chartArea || {};
                                return (a.bottom - a.top) / 5 - 1;
                                }
                            }]
                        }


                const scales = {
                    y: {
                        type : 'time',
                        offset: true,
                        time: {
                        unit: 'day',
                        round: 'day',
                        isoWeek: 1,
                        parser: 'i',
                        displayFormats: {
                                day : 'iii'
                            }
                        },
                        reverse: true,
                        position: 'right',
                        ticks: {
                        maxRotation: 0,
                        autoSkip: true,
                        padding: 1,
                            font: {
                                size: 11
                            }
                        }, 
                        grid: {
                            display: false,
                            drawBorder: false,
                            tickLength: 0
                        }
                    }, 
                    x:{
                            type:'time',
                            position: 'bottom',
                            offset: true,
                            time:{
                            unit: 'week',
                            round: 'week',
                            isoWeekDay: 1,
                            displayFormats: {
                                week: 'MMM dd'
                            }
                            },
                            ticks:{
                            maxRotation: 0,
                            autoSkip: true,
                            font:{
                                size: 11
                            },
                            grid:{
                                display: false,
                                drawBorder: false,
                                //tickLength: 0
                            }
                            }
                        }
                    }



                if (exists(observatoryObj[opt.id])) observatoryObj[opt.id].destroy();
                observatoryObj[opt.id] = new Chart(ctx, {
                    type : 'matrix', 
                    data: datas, 
                    options:{
                        maintainAspectRatio: false, 
                        scales :scales, 
                        plugins:{
                                legend:{
                                    display: false
                                },
                                title: {
                                    display: true,
                                    text: opt.label.toUpperCase(),
                                    fullSize : true,
                                    font: {
                                        size: 16
                                    }
                                },
                                tooltip: {
                                        enabled: true,
                                        callbacks: {
                                            label: function(context) {
                                                var label = opt.labels+ context.raw.v || '';
                                                return label;
                                            },
                                            title: function (context) {
                                                const d = new Date(context[0].raw.d);
                                               return d.getDate()+' '+ d.toLocaleDateString('fr-FR', {month: 'long'})+' '+ d.getFullYear();
                                            },
                                    }
                                }
                        },
                        layout: {
                            padding: {
                            bottom: 13,
                            }
                        }

                    }
                });
                $('#'+opt.id).css({"maxHeight":"180px"});
                $('#'+opt.id).toggle();
                return observatoryObj[opt.id];
            }
        },
        events: function(obj) {
            $(".btn-panel.fullscreen").off().on('click', function() {
                if ($(this).data("key") == "on") {
                    $(this).parent().parent().parent().parent().parent().addClass("fullscreen-active");
                    $(this).parent().parent().parent().parent().next().addClass("canvas-fullscreen-active");
                    $(this).hide();
                    $(this).next().show();
                } else {
                    $(this).parent().parent().parent().parent().parent().removeClass("fullscreen-active");
                    $(this).parent().parent().parent().parent().next().removeClass("canvas-fullscreen-active");
                    $(this).hide();
                    $(this).prev().show();
                }
            })

            $(".TaskPerPerson").off().on('click',function(){
                $('.TaskPerPerson').removeClass('active');
                $(this).addClass('active');
                var data = $(this).data("id");
                switch (data) {
                    case "runningTaskPerPerson":
                        //obj.charts.runningTaskPerPerson(obj,{reload:true});
                        $("#todoTaskPerPerson,#doneTaskPerPerson").hide();
                        $("#runningTaskPerPerson").show();
                        break;
                    case "todoTaskPerPerson":
                        //obj.charts.todoTaskPerPerson(obj,{reload:true});
                        $("#runningTaskPerPerson,#doneTaskPerPerson").hide();
                        $("#todoTaskPerPerson").show();
                        break;
                    case "doneTaskPerPerson":
                        //obj.charts.doneTaskPerPerson(obj,{reload:true});
                        $("#runningTaskPerPerson,#todoTaskPerPerson").hide();
                        $("#doneTaskPerPerson").show();
                        break;
                    default:
                        break;
                }
            })

            $(".TaskPerProject").off().on('click',function(){
                $('.TaskPerProject').removeClass('active');
                $(this).addClass('active');
                var data = $(this).data("id");
                switch (data) {
                    case "runningTaskPerProject":
                        //obj.charts.runningTaskPerProject(obj,{reload:true});
                        $("#todoTaskPerProject,#doneTaskPerProject").hide();
                        $("#runningTaskPerProject").show();
                        break;
                    case "todoTaskPerProject":
                        //obj.charts.todoTaskPerProject(obj,{reload:true});
                        $("#runningTaskPerProject,#doneTaskPerProject").hide();
                        $("#todoTaskPerProject").show();
                        break;
                    case "doneTaskPerProject":
                        //obj.charts.doneTaskPerProject(obj,{reload:true});
                        $("#runningTaskPerProject,#todoTaskPerProject").hide();
                        $("#doneTaskPerProject").show();
                        break;
                    default:
                        break;
                }
            })

            $(".blocHeatMap").off().on('click',function(){
                $('.blocHeatMap').removeClass('active');
                $(this).addClass('active');
                var data = $(this).data("id");
                switch (data) {
                    case "jourOff":
                        $("#calendarActivity").hide();
                        $("#calendarOffAll").show();
                        break;
                    case "Activite":
                        $("#calendarOffAll").hide();
                        $("#calendarActivity").show();
                        break;
                    default:
                        break;
                }

            })

            $('.observatory-aap .filter-container').css({
                "position": "sticky",
                "top": ($("#mainNav").height() + 10) + "px",
                "z-index": 100,
                "background": 'var(--aap-primary-color)',
                "margin-bottom": "13px"
            })

            var listgraph = localStorage.getItem('listgraph');

            if (!listgraph) {
                let newlistgraph = {
                    "checkProject": 1,
                    "checkActions": 1,
                    "checkFinancement": 1,
                    "checkProposition": 1
                };

                localStorage.setItem('listgraph', JSON.stringify(newlistgraph));
                //console.log(newlistgraph);
            } else {
                listgraph = JSON.parse(listgraph);
                $.each(listgraph, function(index, value) {
                    var bool = true;
                    if (value != 1) {
                        bool = false;
                    }
                    $("#" + index).prop('checked', bool);

                    if ($("#" + index).prop('checked')) {
                        $("." + index).removeClass("tochecked");
                        listgraph[index] = 1;

                    } else {

                        listgraph[index] = 0;
                        $("." + index).addClass("tochecked");
                    }
                })
            }

            $('.checkbox').click(function() {
                var listgraph = JSON.parse(localStorage.getItem('listgraph'));
                $.each(listgraph, function(index, value) {
                    if ($("#" + index).prop('checked')) {
                        $("." + index).removeClass("tochecked");
                        listgraph[index] = 1;

                    } else {

                        listgraph[index] = 0;
                        $("." + index).addClass("tochecked");
                    }
                })
                localStorage.setItem('listgraph', JSON.stringify(listgraph));
                //console.log(listgraph)

            });

        },
        commons: {
            bindMetricEvents: function(obj) {
                $('#totalActions, #todoActions, #runningActions, #doneActions, #deadlinePassedActions').off("click").on("click", function() {
                    history.pushState({}, null, aapObj.common.buildUrlQuery(document.location.href, {aapview : "action"}, aapObj.common.getQueryStringObject()));
                    const action = $(this).attr('data-action');
                    const post = {
                        projectId : aapObj?.defaultProject?.["_id"]?.["$id"] ? aapObj.defaultProject["_id"]["$id"] : null,
                        showFilter : true,
                        answerId: 'default',
                        insideModal : true
                    };
                    const url = baseUrl + '/co2/aap/getviewbypath/path/co2.views.aap.detailProject';
                    dialogContent.open({
                        modalContentClass : "modal-custom-lg",
                        isRemoteContent : true,
                        shownCallback : () => {},
                        hiddenCallback : () => {
                            history.pushState({}, null, aapObj.common.buildUrlQuery(document.location.href, {projectId : "", aapview : "", answerId : "", userId: ""}, aapObj.common.getQueryStringObject()));
                            aapObj.filterSearch.projectDetail = {}
                            $("#dialogContent #dialogContentBody").empty()
                            $("#openModal .modal-content").removeClass("aap-modal-md");
                            $("#openModal .modal-content .close-modal").removeClass("aap-close-modal");
                            $('#openModalContent').removeClass("container container-fluid").addClass("container");
                        }
                    });
                    ajaxPost(null, url, post,
                        function (data) {
                            $("#dialogContent #dialogContentBody").empty().html(data)
                            coInterface.bindLBHLinks();
                        }, null, null, {
                            // async : false
                        }
                    )
                    // ajaxPost(null, url, post,
                    //     function (data) {
                    //         smallMenu.open(data)
                    //         $("#openModal").css({
                    //             cssText : `
                    //             top: 0 !important;
                    //             display: block !important;
                    //             overflow: hidden !important;
                    //             display: block !important;
                    //             background-color: rgba(3, 3, 3, 0.62) !important;`
                    //         });
                    //         $("#openModal .modal-content").addClass("aap-modal-md");
                    //         $("#openModal .modal-content .close-modal").addClass("aap-close-modal");
                    //         $('#openModalContent').removeClass("container").addClass("container-fluid");
                    //         $('#openModal').on('hidden.bs.modal', function () {
                    //             // $('#openModalContent').empty();
                    //             if($(this).find(".modal-content.aap-modal-md").length > 0) {
                    //                 history.pushState({}, null, aapObj.common.buildUrlQuery(document.location.href, {projectId : "", aapview : "", answerId : "", userId: ""}, aapObj.common.getQueryStringObject()));
                    //                 aapObj.filterSearch.projectDetail = {}
                    //                 $(".modal-content.aap-modal-md .propsDetailPanelContainer").empty();
                    //                 $("#openModal .modal-content").removeClass("aap-modal-md");
                    //                 $("#openModal .modal-content .close-modal").removeClass("aap-close-modal");
                    //                 $('#openModalContent').removeClass("container container-fluid").addClass("container");
                    //             }
                    //         })
                    //     }, null, null, {
                    //         async : false
                    //     }
                    // );
                })

                $('#runningProject, #doneProject').off('click').on('click', function() {
                    const action = $(this).attr('data-action');
                    let projectsId = {
                        inprogress : null,
                        finished : null
                    }
                    if(typeof observatoryObj?.projectData != "undefined") {
                        if(notEmpty(observatoryObj.projectData?.inprogress) && Object.keys(observatoryObj.projectData.inprogress).length > 0) {
                            projectsId.inprogress = Object.keys(observatoryObj.projectData.inprogress)[0]
                        }
                        if(notEmpty(observatoryObj.projectData?.finished) && Object.keys(observatoryObj.projectData.finished).length > 0) {
                            projectsId.finished = Object.keys(observatoryObj.projectData.finished)[0]
                        }
                    }
                    if($(this).html()*1 > 0) {
                        history.pushState({}, null, aapObj.common.buildUrlQuery(document.location.href, {aapview : "action"}, aapObj.common.getQueryStringObject()));
                        const post = {
                            projectId : projectsId.inprogress ? projectsId.inprogress : aapObj?.defaultProject?.["_id"]?.["$id"] ? aapObj.defaultProject["_id"]["$id"] : null,
                            answerId : "default",
                            showFilter : true,
                            insideModal : true
                        };
                        if(action != "inprogress" && projectsId.finished)
                            post.projectId = projectsId.finished
                        const searchObjToActivate = {
                            obj : {
                                    "text": "",
                                    "nbPage": 0,
                                    "indexMin": 0,
                                    "indexStep": 20,
                                    "count": false,
                                    "scroll": true,
                                    "scrollOne": false,
                                    "tags": [],
                                    "initType": "",
                                    "filters": {
                                        "form": aapObj?.form?.["_id"]?.["$id"] ? aapObj.form["_id"]["$id"] : "",
                                        "answers.aapStep1.titre": {
                                            "$exists": true
                                        },
                                        "properties.avancement": action == "inprogress" ? [
                                            "notSpecified",
                                            "idea",
                                            "concept",
                                            "started",
                                            "development",
                                            "testing",
                                            "mature"
                                        ] : [
                                            "finished",
                                            "abandoned"
                                        ]
                                    },
                                    "types": [
                                        "projects"
                                    ],
                                    "countType": [],
                                    "locality": {},
                                    "forced": {
                                        "filters": {}
                                    },
                                    "fediverse": false,
                                    "tagsPath": "answers.aapStep1.tags",
                                    "fields": [],
                                    "notSourceKey": true,
                                    "sortBy": {
                                        "updated": -1
                                    },
                                    "textPath": "answers.aapStep1.titre"

                                }
                        }
                        aapObj.filterSearch.activeFilters = action == "inprogress" ? {
                            "0notSpecified": {
                                "key": "notSpecified",
                                "value": "notSpecified",
                                "type": "filters",
                                "field": "properties.avancement"
                            },
                            "1idea": {
                                "key": "idea",
                                "value": "idea",
                                "type": "filters",
                                "field": "properties.avancement"
                            },
                            "2concept": {
                                "key": "concept",
                                "value": "concept",
                                "type": "filters",
                                "field": "properties.avancement"
                            },
                            "3started": {
                                "key": "started",
                                "value": "started",
                                "type": "filters",
                                "field": "properties.avancement"
                            },
                            "4development": {
                                "key": "development",
                                "value": "development",
                                "type": "filters",
                                "field": "properties.avancement"
                            },
                            "5testing": {
                                "key": "testing",
                                "value": "testing",
                                "type": "filters",
                                "field": "properties.avancement"
                            },
                            "6mature": {
                                "key": "mature",
                                "value": "mature",
                                "type": "filters",
                                "field": "properties.avancement"
                            }
                        } : {
                            "0abandoned": {
                                "key": "abandoned",
                                "value": "abandoned",
                                "type": "filters",
                                "field": "properties.avancement"
                            },
                            "1finished": {
                                "key": "finished",
                                "value": "finished",
                                "type": "filters",
                                "field": "properties.avancement"
                            }
                        }
                        if(!aapObj.filterSearch.project?.search) {
                            aapObj.filterSearch.project.search = searchObjToActivate
                        } else {
                            aapObj.filterSearch.project.search.obj = searchObjToActivate.obj
                        }
                        const url = baseUrl + '/co2/aap/getviewbypath/path/co2.views.aap.detailProject';
                        dialogContent.open({
                            modalContentClass : "modal-custom-lg",
                            isRemoteContent : true,
                            shownCallback : () => {},
                            hiddenCallback : () => {
                                history.pushState({}, null, aapObj.common.buildUrlQuery(document.location.href, {projectId : "", aapview : "", answerId : "", userId: ""}, aapObj.common.getQueryStringObject()));
                                aapObj.filterSearch.projectDetail = {}
                                $("#dialogContent #dialogContentBody").empty()
                                $("#openModal .modal-content").removeClass("aap-modal-md");
                                $("#openModal .modal-content .close-modal").removeClass("aap-close-modal");
                                $('#openModalContent').removeClass("container container-fluid").addClass("container");
                            }
                        });
                        ajaxPost(null, url, post,
                            function (data) {
                                $("#dialogContent #dialogContentBody").empty().html(data)
                                coInterface.bindLBHLinks();
                            }, null, null, {
                                // async : false
                            }
                        )
                    }
                });

                $('#inFinancingProposal, #voteProposal, #toBeFinancedProposal').off('click').on('click', function() {
                    const action = $(this).attr('data-action');
                    let view = "funding";
                    
                    
                    let answersId = {
                        infinancing : null,
                        tobevoted : null,
                        tobefinanced : null
                    }
                    if(typeof observatoryObj?.proposalData != "undefined") {
                        if(notEmpty(observatoryObj.proposalData?.infinancing) && Object.keys(observatoryObj.proposalData.infinancing).length > 0) {
                            answersId.infinancing = Object.keys(observatoryObj.proposalData.infinancing)[0]
                        }
                        if(notEmpty(observatoryObj.proposalData?.tobevoted) && Object.keys(observatoryObj.proposalData.tobevoted).length > 0) {
                            answersId.tobevoted = Object.keys(observatoryObj.proposalData.tobevoted)[0]
                        }
                        if(notEmpty(observatoryObj.proposalData?.tobefinanced) && Object.keys(observatoryObj.proposalData.tobefinanced).length > 0) {
                            answersId.tobefinanced = Object.keys(observatoryObj.proposalData.tobefinanced)[0]
                        }
                    }
                    const post = {
                        answerId : answersId.infinancing ? answersId.infinancing : aapObj?.defaultProposal?.["_id"]?.["$id"] ? aapObj.defaultProposal["_id"]["$id"] : null,
                        showFilter : true,
                        insideModal : true
                    };
                    let filterObj = {
                        obj : {
                            'answers.aapStep1.titre' : {
                                '$exists': true
                            },
                            'form' : aapObj.form?._id?.$id,
                            'answers.aapStep1.depense.financer' : {
                                '$exists': true
                            }
                        },
                        toActivate : {
                            "status": {
                                "key": "funded",
                                "value": "funded",
                                "type": "filters",
                                "field": "status"
                            }
                        }
                    };
                    switch (action) {
                        case "tobevoted":
                            view = "evaluation";
                            answersId.tobevoted ? post.answerId = answersId.tobevoted : "";
                            filterObj.obj = {
                                'answers.aapStep1.titre' : {
                                    '$exists': true
                                },
                                'status' : "vote",
                                'form' : aapObj.form?._id?.$id,
                                "project.id" : {
                                    "$exists" : false
                                }
                            };
                            filterObj.toActivate = {
                                "status": {
                                    "key": "vote",
                                    "value": "vote",
                                    "type": "filters",
                                    "field": "status"
                                }
                            };
                            break;
                    
                        case "tobefinanced":
                            answersId.tobefinanced ? post.answerId = answersId.tobefinanced : "";
                            filterObj.obj = {
                                'answers.aapStep1.titre' : {
                                    '$exists': true
                                },
                                'form' : aapObj.form?._id?.$id,
                                'answers.aapStep1.depense' : {
                                    '$exists': true
                                },
                                'answers.aapStep1.depense.financer' : {
                                    '$exists': false
                                }
                            };
                            break;
                        default:
                            view = "funding"
                            break;
                    }
                    history.pushState({}, null, aapObj.common.buildUrlQuery(document.location.href, {aapview : view}, aapObj.common.getQueryStringObject()));
                    const searchObjToActivate = {
                        obj : {
                                "text": "",
                                "nbPage": 0,
                                "indexMin": 0,
                                "indexStep": 20,
                                "count": false,
                                "scroll": true,
                                "scrollOne": false,
                                "tags": [],
                                "initType": "",
                                "filters": filterObj.obj,
                                "types": [
                                    "answers"
                                ],
                                "countType": [],
                                "locality": {},
                                "forced": {
                                    "filters": {}
                                },
                                "tagsPath": "answers.aapStep1.tags",
                                "fields": [],
                                "notSourceKey": true,
                                "sortBy": {
                                    "updated": -1
                                }
                            }
                    }
                    aapObj.filterSearch.activeFilters = filterObj.toActivate
                    if(!aapObj.filterSearch.proposal?.search) {
                        aapObj.filterSearch.proposal.search = searchObjToActivate
                    } else {
                        aapObj.filterSearch.proposal.search.obj = searchObjToActivate.obj
                    }
                    const url = baseUrl + '/co2/aap/getviewbypath/path/co2.views.aap.detailProposal';
                    dialogContent.open({
                        modalContentClass : "modal-custom-lg",
                        isRemoteContent : true,
                        shownCallback : () => {},
                        hiddenCallback : () => {
                            history.pushState({}, null, aapObj.common.buildUrlQuery(document.location.href, {projectId : "", aapview : "", answerId : "", userId: ""}, aapObj.common.getQueryStringObject()));
                            aapObj.filterSearch.projectDetail = {}
                            $("#dialogContent #dialogContentBody").empty()
                            $("#openModal .modal-content").removeClass("aap-modal-md");
                            $("#openModal .modal-content .close-modal").removeClass("aap-close-modal");
                            $('#openModalContent').removeClass("container container-fluid").addClass("container");
                        }
                    });
                    ajaxPost(null, url, post,
                        function (data) {
                            $("#dialogContent #dialogContentBody").empty().html(data)
                            coInterface.bindLBHLinks();
                        }, null, null, {
                            // async : false
                        }
                    )
                    // ajaxPost(null, url, post,
                    //     function (data) {
                    //         smallMenu.open(data)
                    //         $("#openModal").css({
                    //             cssText : `
                    //             top: 0 !important;
                    //             display: block !important;
                    //             overflow: hidden !important;
                    //             display: block !important;
                    //             background-color: rgba(3, 3, 3, 0.62) !important;`
                    //         });
                    //         $("#openModal .modal-content").addClass("aap-modal-md");
                    //         $("#openModal .modal-content .close-modal").addClass("aap-close-modal");
                    //         $('#openModalContent').removeClass("container").addClass("container-fluid");
                    //         $('#openModal').on('hidden.bs.modal', function () {
                    //             // $('#openModalContent').empty();
                    //             if($(this).find(".modal-content.aap-modal-md").length > 0) {
                    //                 history.pushState({}, null, aapObj.common.buildUrlQuery(document.location.href, {projectId : "", aapview : "", answerId : "", userId: ""}, aapObj.common.getQueryStringObject()));
                    //                 aapObj.filterSearch.proposal = {}
                    //                 $(".modal-content.aap-modal-md .propsDetailPanelContainer").empty();
                    //                 $("#openModal .modal-content").removeClass("aap-modal-md");
                    //                 $("#openModal .modal-content .close-modal").removeClass("aap-close-modal");
                    //                 $('#openModalContent').removeClass("container container-fluid").addClass("container");
                    //             }
                    //         })
                    //     }, null, null, {
                    //         async : false
                    //     }
                    // );
                });
            }
        },
        filters: function(obj) {
            const so = {};
            if(aapObj?.context?.oceco?.subOrganization)
                $.each(aapObj.subOrganizations, function(k, v) {
                    so[k] = v.name
                    trad[k] = v.name
                });
            if (so?.[aapObj.context._id.$id]) delete so[aapObj.context._id.$id]

            const filters = {}
            if(aapObj?.context?.oceco?.subOrganization){
                filters["subOrganism"] = {
                    view: "dropdownList",
                    type: "filters",
                    multiple: false,
                    field: "context",
                    name: trad["Sub organization"],
                    event: "filters",
                    keyValue: false,
                    typeList: "object",
                    list: so,
                }
            }
            if(Object.keys(filters).length===0)
                $('#filterContainerObs').parent().parent().hide();

            const paramsFilterObs = {
                urlData: "",
                container: "#filterContainerObs",
                header: {
                    dom: ".headerSearchIncommunityObs",
                    options: {
                        left: {
                            classes: 'col-xs-8 elipsis no-padding',
                            group: {
                                count: true,
                                types: true
                            }
                        }
                    },
                },
                defaults: {
                    filters: {},
                },
                results: {
                    events: function(fObj) {
                        observatoryObj.fObj = fObj;
                        const filters = fObj.search.obj.filters;
                        const contexts = notEmpty(filters?.context) ? filters?.context : [aapObj.context.id]
                        const params = {
                            formId: obj.formId,
                            context: contexts
                        }
                        obj.metrics.combineMetrics(obj, params);
                        obj.charts.taskPerPerson(obj, params);
                        obj.charts.taskPerProject(obj, params);
                        obj.charts.actionPerPersonByYear(obj, params);
                        obj.charts.dayOffPerPersonne(obj, params);
                        obj.charts.calendarOffAll(obj, params);
                        obj.charts.calendarActivity(obj, params);
                        obj.charts.financingFlows(obj, params);
                        obj.charts.expenditureFinancingPaymentHistory(obj, {
                            ...params,
                            filter: "all"
                        });
                        obj.charts.weekActionActivity(obj, params);
                        obj.commons.bindMetricEvents(obj);

                        $(".expenditureFinancingPaymentHistory").off().on('click',function(){
                            $(".expenditureFinancingPaymentHistory").removeClass('active');
                            $(this).addClass('active');
                            var filter = $(this).data("id");
                            obj.charts.expenditureFinancingPaymentHistory(obj, {
                                formId: obj.formId,
                                context: contexts,
                                filter: filter
                            });
                        });
                        obj.activity_section(obj, params);
                        
                    },
                },
                filters: filters
            };

            observatoryObj["searchObj"] = searchObj.init(paramsFilterObs);
            observatoryObj["searchObj"].search.init(observatoryObj["searchObj"]);
        },
        activity_section: function(obj, params){

            obj.charts.weekActionActivity(obj, params);
            $('.action-ativity').off().on('click',function(){
                const btn = $(this);
                const key = $(this).data('key');
                $('.action-ativity').removeClass('active');
                if(btn.attr('data-key') != "twodates")
                {
                    btn.addClass('active');
                    $('.date-between').addClass('hidden')
                }else{
                    $('.action-ativity[data-key=showTwoDate]').addClass('active');
                }
                
                
                    switch (key) {
                        case "twodates":
                            var date1 = $('#date1').val();
                            var date2 = $('#date2').val();
                            if(notEmpty(date1) && notEmpty(date2)){
                                obj.charts.betweenTwoDateActivity(obj,{
                                    ...params,
                                    date1 : date1,
                                    date2 : date2,

                                });
                            }
                            break;
                        case "thisweek":
                            obj.charts.weekActionActivity(obj,params);
                            break;
                        case "thismonth":
                                obj.charts.betweenTwoDateActivity(obj,{
                                    ...params,
                                    filter : 'thismonth'
                                });
                            break;
                        case "showTwoDate" :
                            if(btn.hasClass("off")){
                                btn.addClass("on").removeClass("off");
                            $('.date-between').removeClass('hidden')
                            }else{
                                btn.addClass("on").addClass("off");
                                $('.date-between').addClass('hidden')
                            }
                            break;
                        case "today" :
                            obj.charts.oneDayActionActivity(obj,params);
                            break;
                        case "yesterday" :
                            obj.charts.oneDayActionActivity(obj,{
                                ...params,
                                filter : "yesterday"
                            });
                        default:
                            break;
                    }
                })
            },
    }

    $(document).ready(function() {
        observatoryObj.init(observatoryObj);
    })
</script>