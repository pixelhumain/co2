<style>
    .container-oc{
        position: relative;
        display: flex;
        flex-direction: row;
        gap : 10px;
        width :48%;
        padding-bottom: 5px;
    }
    .name-oc:hover{
        text-decoration: none;
    }
    .dropdown-menu-oc{
        max-height: 300px;
        overflow-y: auto;
    }
    .li-dropdown-search-aap-oc{
        position: sticky;
        top: 0;
        padding : 5px 10px
    }
    .container-image-oc{
        position: relative;
    }
    .container-desc-oc{
        position: relative;
        width: 100%;
    }
    .container-rest-oc{
        position: relative;
    }
    #dropdown_search_OC{
        display: flex;
        flex-wrap: wrap;
        flex-direction: row;
        gap: 10px;
    }
    .create-sub-organization-oc{
        /* display: none; */
    }
    .container-oc:hover .create-sub-organization-oc{
        display: initial
    }
    @media screen and (max-width:588px) {
        .container-oc{
            width: 99%;
        }
    }

    .panel-body-oc-sub-orga,
    .panel-body-oc-parent,
    .panel-body-oc-nearby-sub-orga{
        display: flex;
        gap: 12px;
        margin-top: 12px;
        flex-wrap: wrap;
        text-align: left;
    }

    #accordion-oc .panel-title a{
        text-decoration: none;
        cursor:default;
    }
</style>

<div class="container">
    <div class="row">
        <h1><?php echo Yii::t("common", "Choose an organization") ?></h1>
        <div class="col-xs-12">
                <button class="btn btn-success pull-right btn-block" id="add-new-subOrg">
                    Ajouter une sous-organisation
                </button>
        </div>
        <div class="panel-group margin-top-50" id="accordion-oc">
            <div class="panel panel-default panel-collapse-oc-1">
                <div class="panel-heading">
                <h4 class="panel-title">

                    <a data-toggle="collapsee" data-parent="#accordion-oc" hreff="#collapse-oc-1" href="javascript:;">
                        <?// echo Yii::t("common", "Sub organization") ?>
                        Liste de sous organisation de 
                    </a>
                </h4>
                </div>
                <div id="collapse-oc-1" class="panel-collapse collapse in">
                    <div class="panel-body panel-body-oc-sub-orga">
                        
                    </div>
                </div>
            </div>
            <div class="panel panel-default panel-collapse-oc-2 margin-top-35">
                <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapsee" data-parent="#accordion-oc" hreff="#collapse-oc-2" href="javascript:;">
                        Parent
                    </a>
                </h4>
                </div>
                <div id="collapse-oc-2" class="panel-collapse collapse in">
                    <div class="panel-body panel-body-oc-parent">
                        
                    </div>
                </div>
            </div>

            <div class="panel panel-default panel-collapse-oc-3 margin-top-35 hidden">
                <div class="panel-heading">
                <h4 class="panel-title">

                    <a data-toggle="collapsee" data-parent="#accordion-oc" hreff="#collapse-oc-3" href="javascript:;">
                        <?// echo Yii::t("common", "Sub organization") ?>
                        Liste de sous organisation voisine 
                    </a>
                </h4>
                </div>
                <div id="collapse-oc-1" class="panel-collapse collapse in">
                    <div class="panel-body panel-body-oc-nearby-sub-orga">
                        
                    </div>
                </div>
            </div>

            <div class="panel panel-default hidden">
                <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion-oc" href="#collapse-oc-3">
                        <?= Yii::t("common", "Global search") ?>
                    </a>
                </h4>
                </div>
                <div id="collapse-oc-3" class="panel-collapse collapse">
                    <div class="panel-body">
                        <div class="col-md-12 no-padding">
                            <div class="col-xs-12 filter" id="filterContainerAapOceco">
                                <div id='filterContainerOrganismChooser' class='searchObjCSS'></div>
                            </div>
                            <div class="col-xs-12 filter-container">
                                <div class='headerSearchContainerOrganismChooser no-padding col-xs-12'></div>
                                <div class='bodySearchContainer margin-top-30'>
                                    <div class='col-xs-12 no-padding text-left' id='dropdown_search_OC'>
                                    </div>
                                    <div class='padding-top-20 col-xs-12 text-left footerSearchContainerOrganismChooser'></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(function(){
        var ocObj = {
            isAllEmpty : true,
            title :  function(params){
                var html = "";
                var host = `${baseUrl}/co2/co/index/slug/${params.configSlug}`;

                html += `<a href="javascript:;" class="name-oc" data-context="${params.parentSlug}" data-form="${params?._id?.$id}" data-host="${escapeHtml(host)}">
                            <h5>Parent : ${params.parentName}</h5>
                            <h6>Form : ${params.name}</h6>
                            <small>${params.configSlug}</small>
                        </a><br />`;
                    if(params.parentType=="organizations" && notEmpty(params.configSlug) && params.configSlug != "proceco" && params.configSlug == params.parentSlug){
                        html +=`<a href="javascript:;" data-id-parent=${params.parentId} data-slug-parent=${params.parentSlug} data-type-parent=${params.parentType} data-config-slug="${params.configSlug}" class="btn create-sub-organization-oc btn-xs btn-aap-primary pull-right">
                                    <i class="fa fa-plus"></i> ${trad["Sub organization"]}
                                </a>`;
                    } 
                return html;
            },
            search : function(){
                $('.dropdown-search-aap-oc').on("keyup",function(){
                    var id = $(this).data('id');
                    setTimeout(() => {
                        var searchKey = $(this).val();
                        $('#dropdown-menu-oc-'+id+' .dropdown-item-oc').each(function() {
                            var txt = $(this).find('.name-oc').text();
                            if (txt.match(new RegExp(searchKey, "gi"))) {
                                $(this).show(500);$(this).next(".divider").show(500)
                            } else {
                                $(this).hide(500);$(this).next(".divider").hide(500)
                            }
                        })
                    }, 1000);
                })
            },
            createSubOrganism : function(){
                $(".create-sub-organization-oc").off().on('click',function(){
                    var configSlug = $(this).data("config-slug");
                    if(notEmpty(configSlug)){
                        var newSubOrg = bootbox.dialog({ 
                            title: 'Créer sous-organisation',
                            message: `<div>
                                <form action="" id="form-new-suborg">
                                    <div class="form-group">
                                        <label for="">${trad.Name}</label>
                                        <input type="text" required class="form-control" maxlength="20" id="suborg-name" />
                                    </div>
                                    <div class="form-group">
                                        <label for="">${tradDynForm.shortDescription}</label>
                                        <input type="text" class="form-control" maxlength="50" required id="suborg-shortDescription"/>
                                    </div>
                                    <div class="form-group">
                                        <button class="btn btn-success">${trad.save}</button>
                                    </div>
                                </form>
                                </div>`,
                            size: 'small',
                            onEscape: true,
                            backdrop: true,
                        });
                        newSubOrg.init(function(){
                            $("#form-new-suborg").on('submit',function(e){
                                e.preventDefault();
                                ajaxPost(null, baseUrl + "/co2/aap/sousorga/action/add/slug/"+configSlug,
                                    {
                                        name : $("#suborg-name").val(),
                                        shortDescription : $("#suborg-shortDescription").val()
                                    },
                                    function (data) {
                                        if(newSubOrg.modal('hide')){
                                            var host = `${baseUrl}/co2/co/index/slug/${data.data.configSlug}#proposalAap.context.${data.data.formParentSlug}.formid.${data.data.formId}`;
                                            window.open(host);
                                        }
                                    }, null
                                );
                            })
                        });
                    }
                })
            },
            getSubOrganism : function(slug,nearby=false){
                var url = baseUrl + "/co2/aap/sousorga/action/listSubOrga/slug/"+slug;
                var post = { };
                
                nearby ? coInterface.showLoader(".panel-body-oc-nearby-sub-orga")  : coInterface.showLoader(".panel-body-oc-sub-orga"); 
                ajaxPost(null,url, post,
                    function(data) {
                        var html = "";
                        $.each(data,function(k,params){
                            var aapList = "";
                            if(notEmpty(params.aap)){
                                if(Object.values(params.aap).length === 1){
                                    const v = params.aap[0];
                                    aapList += `<a href="javascript:;" class="btn btn-aap-primary btn-xs aap-chooser-switcher" data-hash="#proposalAap.context.${v.context}.formid.${v.formId}">${v.formName}</a>`
                                }else{
                                    $.each(params.aap,function(k,v){
                                        aapList += `
                                        <li>
                                            <a href="javascript:;" data-hash="#proposalAap.context.${v.context}.formid.${v.formId}" class="aap-chooser-switcher">${v.formName}</a>
                                        </li>`;
                                    })
                                }
                            
                                html+=`
                                <div class="container-oc shadow2 margin-bottom-10">
                                    <div class="container-image-oc">
                                        <img src="${params?.profilMediumImageUrl ? params?.profilMediumImageUrl : defaultImage}" width="70" height="70" alt="" />
                                    </div>
                                    <div class="container-desc-oc">
                                        <a href="#page.type.${params.collection}.id.${k}" class="name-sub-orga-oc lbh-preview-element" data-context="" data-form="" data-host="">
                                            <h6>${params.name}</h6>
                                        </a>`;
                                        if(Object.values(params.aap).length !== 1){
                                            html+=`
                                            <div class="dropdown">
                                                <button class="btn btn-aap-primary btn-xs dropdown-toggle" type="button" data-toggle="dropdown">Voir l'appel à projet
                                                <span class="caret"></span></button>
                                                <ul class="dropdown-menu">
                                                    ${aapList}
                                                </ul>
                                            </div>`;
                                        }else{
                                            html+= aapList
                                        }
                                    html+=`   
                                    </div>
                                    <div class="container-rest-oc">

                                    </div>
                                </div>
                                `;
                            }
                        })
                        if(nearby){
                            if(notEmpty(html))
                                $('.panel-body-oc-nearby-sub-orga').html(html);
                            else
                                $('.panel-body-oc-nearby-sub-orga').html('<h5 class="h5 text-center">Sous organisation voisine <?= Yii::t("common", "empty") ?></h5>');
                        }else{
                            if(notEmpty(html))
                                $('.panel-body-oc-sub-orga').html(html);
                            else
                                $('.panel-body-oc-sub-orga').html('<h5 class="h5 text-center">Sous organisation <?= Yii::t("common", "empty") ?></h5>');
                        }


                        $(".aap-chooser-switcher").off().on('click',function(){
                            var new_hash = $(this).data("hash");
                            history.pushState(null, document.title, location.origin+location.pathname+new_hash);
                            location.reload();
                            //window.open(location.origin+location.pathname+new_hash);
                        })
                        coInterface.bindLBHLinks();
                    }
                );
            },
            getParentOrganism : function(slug){
                var url = baseUrl + "/co2/aap/sousorga/action/listParentOrga/slug/"+slug;
                var post = { };
                coInterface.showLoader(".panel-body-oc-parent");
                ajaxPost(null,url, post,
                    function(data) {
                        var html = "";
                        $.each(data,function(k,params){
                            var aapList = "";
                            mylog.log(params.slug,"sssssssss")
                            if(costum.slug === params.slug){
                                $('.panel-collapse-oc-3').removeClass('hidden')
                                ocObj.getSubOrganism(costum.slug,true);
                            }
                            if(notEmpty(params.aap)){
                                $.each(params.aap,function(k,v){
                                    aapList += `
                                    <li>
                                        <a href="javascript:;" data-hash="#proposalAap.context.${v.context}.formid.${v.formId}" class="aap-chooser-switcher">${v.formName}</a>
                                    </li>`;
                                })
                            }
                            html+=`
                            <div class="container-oc shadow2 margin-bottom-10">
                                <div class="container-image-oc">
                                    <img src="${params?.profilMediumImageUrl ? params?.profilMediumImageUrl : defaultImage}" width="70" height="70" alt="" />
                                </div>
                                <div class="container-desc-oc">
                                    <a href="#page.type.${params.collection}.id.${k}" class="name-sub-orga-oc lbh-preview-element" data-context="" data-form="" data-host="">
                                        <h6>${params.name}</h6>
                                    </a>
                                    <div class="dropdown">
                                        <button class="btn btn-aap-primary btn-xs dropdown-toggle" type="button" data-toggle="dropdown">Voir l'appel à projet
                                        <span class="caret"></span></button>
                                        <ul class="dropdown-menu">
                                            ${aapList}
                                        </ul>
                                    </div>
                                </div>
                                <div class="container-rest-oc">

                                </div>
                            </div>
                            `;
                        })

                        if(notEmpty(html))
                            $('.panel-body-oc-parent').html(html);
                        else
                            $('.panel-body-oc-parent').html('<h5 class="h5 text-center">Organisation parent <?= Yii::t("common", "empty") ?></h5>');
                        
                        $(".aap-chooser-switcher").off().on('click',function(){
                            var new_hash = $(this).data("hash");
                            history.pushState(null, document.title, location.origin+location.pathname+new_hash);
                            location.reload();
                            //window.open(location.origin+location.pathname+new_hash,self);
                        })
                        coInterface.bindLBHLinks();
                    }
                );
            },
            events : function(){
                $('.oc-show-parent-so').off().on('click',function(){
                    var key = $(this).data('key');
                    switch (key) {
                        case "parent":
                            ocObj.getParentOrganism(aapObj?.context?.slug);
                            break;
                        case "sub-orga":
                            ocObj.getSubOrganism(aapObj?.context?.slug);
                            break;
                        default:
                            break;
                    }
                })
                $("#collapse-oc-1").on("show.bs.collapse", function(){
                    ocObj.getSubOrganism(aapObj?.context?.slug);
                });
                $("#collapse-oc-2").on("show.bs.collapse", function(){
                    ocObj.getParentOrganism(aapObj?.context?.slug);
                });
                ocObj.getSubOrganism(aapObj?.context?.slug);
                ocObj.getParentOrganism(aapObj?.context?.slug);
                $('a[hreff="#collapse-oc-1"]').text("Liste des sous-organisations de "+$('.aap-organism-chooser span').text());
                $('a[hreff="#collapse-oc-2"]').text("Liste des organisations parents de "+$('.aap-organism-chooser span').text());
                $('a[hreff="#collapse-oc-3"]').text("Liste des organisations voisines de "+$('.aap-organism-chooser span').text());
                ocObj.createSubOrganism();

                if(!aapObj?.context?.oceco?.subOrganization){
                    $("#add-new-subOrg").parent().remove();
                }
            },
            createSubOrganism : function(){
                if(aapObj?.context?.oceco?.subOrganization){
                    $("#add-new-subOrg").off().on('click',function(){
                        if(costum != null){
                            var newSubOrgDialog = bootbox.dialog({ 
                                    title: 'Créer sous-organisation',
                                    message: `<div>
                                        <form action="" id="form-new-subOrg">
                                            <div class="form-group">
                                                <label for="">${trad.Name}</label>
                                                <input type="text" required class="form-control" maxlength="20" id="subOrg-name" />
                                            </div>
                                            <div class="form-group">
                                                <label for="">${tradDynForm.shortDescription}</label>
                                                <input type="text" class="form-control" maxlength="50" required id="subOrg-shortDescription"/>
                                            </div>
                                            <div class="form-group">
                                                <button class="btn btn-success">${trad.save}</button>
                                            </div>
                                        </form>
                                        </div>`,
                                    size: 'small',
                                    onEscape: true,
                                    backdrop: true,
                                })
                                newSubOrgDialog.init(function(){
                                    if(costum != null){
                                        $("#form-new-subOrg").on('submit',function(e){
                                            e.preventDefault();
                                            ajaxPost(null, baseUrl + "/co2/aap/sousorga/action/add/slug/"+aapObj?.context?.slug,
                                                {
                                                    name : $("#subOrg-name").val(),
                                                    shortDescription : $("#subOrg-shortDescription").val()
                                                },
                                                function (data) {
                                                    if(data.result){
                                                        toastr.success(data.msg);
                                                        newSubOrgDialog.modal('hide');
                                                        ocObj.events();
                                                    }
                                                }, null
                                            );
                                        })
                                    }
                                });
                        }
                    })
                }
            }
        }
        ocObj.events();

        // directory.organismChooser = function (params){
        //     var html = "";
        //     var host = `${baseUrl}/co2/co/index/slug/${params.slug}`;
        //     if(!debug && exists(costum.host)){
        //         host = costum.host;
        //     }
        //     html+=`
        //     <div class="container-oc shadow2 margin-bottom-10">
        //         <div class="container-image-oc">
        //             <img src="${params?.parentProfilImageUrl ? params?.parentProfilImageUrl : defaultImage}" width="70" height="70" alt="" />
        //         </div>
        //         <div class="container-desc-oc">
        //             ${ocObj.title(params)}
        //         </div>
        //         <div class="container-rest-oc">
        //         </div>
        //     </div>
        //     `;
        //     return html;
        // }

        // if(notEmpty(aapObj?.context?.slug)){
        //     ocObj.getSubOrganism(aapObj?.context?.slug);
        //     ocObj.getParentOrganism(aapObj?.context?.slug);
        // }
        // ocObj.events();
        // var paramsFilterOC = {
        //     container: "#filterContainerOrganismChooser",
        //     urlData  : baseUrl + "/co2/aap/directoryorganismchooser/slug/" + costum.contextSlug,
        //     footerDom : ".footerSearchContainerOrganismChooser",
        //     interface: {
        //         events: {
        //             page: true,
        //             //scroll: true,
        //             //scrollOne : true
        //         }
        //     },
        //     /*loadEvent: {
        //         default: "scroll"
        //         },*/
        //     filters     : {
        //         text: true
        //     },
        //     notSourceKey: true,
        //     defaults    : {
        //         notSourceKey: true,
        //         indexStep   : "20",
        //         types       : ["forms"],
        //         forced      : {
        //             filters: {}
        //         },
        //         filters     : {
        //             /*'$or' : {
        //                 "parentId" : costum.contextId,
        //             }*/
        //             type : "aap",
        //             config : {'$exists' : true}
        //         },
        //         sortBy      : {
        //             "updated": -1
        //         },
        //         fields      : [
        //             "name","collection","slug","profilImageUrl","parent","description","parentId","config"
        //         ],
        //     },
        //     header      : {
        //         dom    : ".headerSearchContainerOrganismChooser",
        //         options: {
        //             left: {
        //                 classes: "col-xs-12 no-padding",
        //                 group  : {
        //                     count: true,
        //                     //map: true
        //                 }
        //             }
        //         },
        //         views  : {
        //             map: function (fObj, v) {
        //                 /*return  '<button class="btn-show-map-search pull-right" style="" title="'+trad.showmap+'" alt="'+trad.showmap+'">'+
        //                     '<i class="fa fa-map-marker"></i> '+trad.map+
        //                     '</button>';*/
        //                 return "";
        //             }
        //         }
        //     },
        //     events      : {},
        //     results     : {
        //         dom       : "#dropdown_search_OC",
        //         smartGrid : false,
        //         renderView: "directory.organismChooser",
        //         events: function () {
        //             $("#dropdown_search_OC .processingLoader").remove();
        //             $('.name-oc').off().on('click',function(){
        //                 var context = $(this).data('context');
        //                 var formId = $(this).data('form');
        //                 var host = $(this).data('host');
        //                 if(formId != "undefined"){
        //                     var newHash = `#proposalAap.context.${context}.formid.${formId}`;
        //                     history.pushState(null, document.title, `${host}${newHash}`);
        //                     location.reload(location.href);
        //                 }else{
        //                     toastr.error(trad["This organization does not have an oceco/call for projects form"]);
        //                 }
        //             })
        //             ocObj.search();
        //             ocObj.createSubOrganism();
        //             coInterface.bindLBHLinks();
        //         }
        //     }
        // };
        // var filterSearchOC = searchObj.init(paramsFilterOC);
        // filterSearchOC.search.init(filterSearchOC);
    })
</script>