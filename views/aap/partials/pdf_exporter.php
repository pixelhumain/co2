<?php 
    $answer = PHDB::findOneById(Form::ANSWER_COLLECTION,$params,['form']);
    $form = PHDB::findOneById(Form::COLLECTION,$answer["form"],['params.pdf_exporter']);
    if(!empty($form["params"]["pdf_exporter"]))
        echo $this->renderPartial($form["params"]["pdf_exporter"],["params"=> $params]);
    else
        echo "<h3>".Yii::t("common","PDF export soon available")."</h3>";
?>