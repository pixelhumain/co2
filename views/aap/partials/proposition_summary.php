<?php
    
    use PixelHumain\PixelHumain\modules\citizenToolKit\components\UtilsHelper;
    
    $proposition = UtilsHelper::array_first($results);
    $aap_step1 = $proposition['answers']['aapStep1'] ?? [];
    $parent = UtilsHelper::array_first($elements);
    $depositor = UtilsHelper::array_first($users);
    $parser = new Parsedown();
    $hide_infos = ['titre', 'description', 'depense', 'budget', 'year', 'tags', 'image'];
    
    if (!function_exists('get_fa_file')) {
        function get_fa_file(string $filename): string {
            $splited = explode('.', $filename);
            if ($splited) {
                $extension = strtolower(end($splited));
                $document_maps = [
                    'file-word-o'       => ['doc', 'docx', 'dot'],
                    'file-powerpoint-o' => ['ppt', 'pptx', 'odp'],
                    'file-excel-o'      => ['xls', 'xlsx', 'ods'],
                    'file-pdf-o'        => ['pdf']
                ];
                foreach ($document_maps as $document_icon => $document_map) {
                    if (in_array($extension, $document_map)) return $document_icon;
                }
            }
            return 'file';
        }
    }
    
    $css_js = [
        '/css/bootstrap-ext-card.css',
        '/css/bootstrap-ext-button.css',
        '/css/bootstrap-ext-progressbar.css',
        '/js/bootstrap-ext-progressbar.js',
        '/js/bootstrap-ext-button.js'
    ];
    HtmlHelper::registerCssAndScriptsFiles($css_js, Yii::app()->getModule('co2')->getAssetsUrl());
?>
<style>
    .card {
        margin-bottom: 20px;
    }
</style>
<div class="proposition-summary">
    <div class="proposal-compact"></div>

    <?php
        $count = 0;
        $moreInfo = "";
        if (gettype($inputs) == "array" && !empty(array_values($inputs)[0]['step']) && array_values($inputs)[0]['step'] == 'aapStep1' && !empty(array_values($inputs)[0]['inputs'])) 
            foreach (array_values($inputs)[0]['inputs'] as $db_input_key => $db_input_content) {
                if (!empty($aap_step1[$db_input_key]) && !in_array($db_input_key, $hide_infos)) {
                    $count += 1;
                    if (gettype($aap_step1[$db_input_key]) === 'string')
                        $moreInfo .= '<li class="list-group-item"><b>'. $db_input_content['label'] .'</b>: '. $aap_step1[$db_input_key] .'</li>';
                    elseif (gettype($aap_step1[$db_input_key]) === 'array' && range(0, count($aap_step1[$db_input_key]) - 1) === array_keys($aap_step1[$db_input_key])) {
                        $moreInfo .= '<li class="list-group-item">'
                                    .'<b>'. $db_input_content['label'] .'</b><br>';
                            foreach ($aap_step1[$db_input_key] as $aap_step_array_value) {
                                $moreInfo .= '- '. $aap_step_array_value .'<br>';
                            }
                        $moreInfo .= '</li>';
                    }
                } elseif ($db_input_content['type'] == 'tpls.forms.cplx.finder' && isset($aap_step1['finder'.$db_input_key]) && !empty($aap_step1['finder'.$db_input_key]) && !in_array($db_input_key, $hide_infos)) {
                    $count += 1;
                    $moreInfo .= '<li class="list-group-item d-flex flex-wrap" style="flex-wrap: wrap">'
                                .'<b>'. $db_input_content['label'] . '</b><br>';
                        foreach ($aap_step1['finder'.$db_input_key] as $aap_step_key => $aap_step_finder_value) {
                            $moreInfo .= '<div class="col-xs-12 element-finder">'
                                            .'<img src="'.Yii::app()->getRequest()->getBaseUrl(true).(isset($aap_step_finder_value["img"]) ? $aap_step_finder_value["img"] : "/assets/cc923626/images/thumbnail-default.jpg").'" class="img-circle pull-left margin-right-10" height="35" width="35">'
                                            .'<span class="info-contact pull-left margin-right-5">'
                                                .'<span class="name-contact text-dark text-bold">'. $aap_step_finder_value["name"] .'</span><br>'
                                                .'<span class="cp-contact text-light pull-left">'. $aap_step_finder_value["type"] .'</span>'
                                            .'</span>'
                                        .'</div>';
                        }
                    $moreInfo .= '</li>';
                }
            }
        if ($count > 0):
    ?>
            <div class="card card-show-divider">
                <div class="card-header">
                    <h4>
                        <span><?= Yii::t("common", "More informations") ?></span>
                        <span style="float: right">
                            <span class="fa fa-info"></span>
                        </span>
                    </h4>
                </div>
                    <div class="card-body">
                        <ul class="list-group">
                            <?= $moreInfo ?>
                        </ul>
                    </div>
            </div>
    <?php endif ?>

    <div class="card card-show-divider detail-description">
        <div class="card-header">
            <h4><?= Yii::t("form","Description") ?></h4>
        </div>
        <div class="card-body">
            <?php if (!empty($aap_step1['description'])): ?>
                <div class="description-body detail-content">
                    <?= $parser->parse(!empty($aap_step1['description']) ? '<div class="p-3">' . $parser->parse($aap_step1['description']) . '</div>' : '<div class="p-3">' . $aap_step1['description'] . '</div>') ?>
                </div>
            <?php else: ?>
                <div class="description-body">
                    <p class="list-group-item"><?= Yii::t("common", "No description") ?></p>
                </div>
            <?php endif ?>
        </div>
    </div>
    <div class="card card-show-divider">
        <div class="card-header">
            <h4>
                <span><?= Yii::t("common", "Themes") ?></span>
            </h4>
        </div>
        <div class="card-body">
            <div class="tag-body tag-temes">
                <?php if (!empty($aap_step1['tags'])): foreach ($aap_step1['tags'] as $thematique): ?>
                    <span class="tag-theme"><?= $thematique ?></span>
                <?php endforeach; else: ?>
                    <p><?= Yii::t("common", "No theme") ?></p>
                <?php endif ?>
            </div>
        </div>
    </div>
    <div class="card card-show-divider">
        <div class="card-header">
            <h4>
                <span><?= Yii::t("cms", "Documents") ?></span>
                <span style="float: right">
                    <i class="fa fa-file"></i>
                </span>
            </h4>
        </div>
        <div class="card-body document-body">
            <?php if (!empty($allDocuments)): foreach ($allDocuments as $document): ?>
                <div class="doc-view-container">
                    <div class="doc-icon">
                        <span class="fa fa-<?= get_fa_file($document['name']) ?>"></span>
                    </div>
                    <div class="doc-name"><?= $document['name'] ?></div>
                    <div class="doc-actions">
                        <a title="<?= str_replace('"', ' ', $document['name']) ?>"
                           href="<?= Yii::app()->getBaseUrl(true) . '/upload/communecter/' . $document['folder'] . '/' . $document['name'] ?>"
                           download="<?= $document['name'] ?>"
                           class="btn btn-default"><span class="fa fa-download"></span></a>
                    </div>
                </div>
            <?php endforeach; else: ?>
                <p><?= Yii::t("common", "No document") ?></p>
            <?php endif ?>
        </div>
    </div>
</div>
<script>
    $(function () {
        var results = <?= json_encode(get_defined_vars()["_params_"]) ?>;
        var answerId = Object.keys(results['results'])[0];
        var answer = aapObj.prepareData.proposal(aapObj, results["results"][answerId], results);

        $('.proposition-summary .proposal-compact').html(
            aapObj.directory.proposition_template_html_compact(aapObj, answer)
        )

        aapObj.events.proposal(aapObj);
        aapObj.events.proposalStatus(aapObj);
        aapObj.events.proposalDropdown(aapObj);
        coInterface.bindLBHLinks();
        $('.proposal-compact .proposition-collapse-container .collapse').collapse("hide");
        $(".proposal-compact .proposition-detail").remove();
        $('.proposition-name').off();
        $('#openModal .propsDetailPanelContainer .propsDetailPanel').removeClass("col-sm-10");
    })
</script>
