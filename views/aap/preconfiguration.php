<?php
$cssJS = array(
    '/plugins/swiper/swiper.min.css',
    '/plugins/swiper/swiper.js',
    '/plugins/bootstrap-datetimepicker/xdsoft/jquery.datetimepicker.full.min.js',
    '/plugins/bootstrap-datetimepicker/xdsoft/jquery.datetimepicker.min.css',
    '/plugins/bootstrap-tagsinput/src/bootstrap-tagsinput.css'
);
HtmlHelper::registerCssAndScriptsFiles($cssJS, Yii::app()->request->baseUrl);
?>
<style>
    @font-face{
        font-family: "montserrat";
        src: url("<?php echo Yii::app()->getModule("costum")->getAssetsUrl() ?>/font/co/montserrat-regular.woff") format("woff"),
        url("<?php echo Yii::app()->getModule("costum")->getAssetsUrl() ?>/font/co/montserrat-regular.ttf") format("ttf")
    }.mst{font-family: 'montserrat'!important;}

    @font-face{
        font-family: "CoveredByYourGrace";
        src: url("<?php echo Yii::app()->getModule("costum")->getAssetsUrl() ?>/font/co/CoveredByYourGrace.ttf")
    }.cbyg{font-family: 'CoveredByYourGrace'!important;}
    .co-popup-preconfig-container{
        width: 100%;
        height: 100vh;
        position: fixed;
        top: 0;
        left: 0;
        background-color: rgba(0, 0, 0, .5);
        display: flex;
        justify-content: center;
        align-items: center;
        color: #2c3e50;
        z-index: 9999999;
    }

    .co-popup-preconfig {
        min-width: 700px;
        min-height: 400px;
        background-color: white;
        border-radius: 10px;
        position: relative;
        font-family: "montserrat" !important;
    }

    .co-popup-preconfig-content , .co-popup-preconfig-finish {
        transition: all 0.7s 0.3s cubic-bezier(0.5, 0.005, 0.075, 0.985), transform 0.1s 0.7s, -webkit-transform 0.1s;
        width: 700px;
        height: 387px;
    }

    .co-popup-preconfig-header {
        padding-top: 20px;
        text-align: center;
    }

    .co-popup-preconfig-header h2 {
        font-weight: bold;
    }

    .co-popup-preconfig-header p{
        font-size: 14px;
        line-height: 1.8;
    }

    .co-popup-preconfig-header p a {
        text-decoration: none;
        font-weight: bold;
        color: #9fbd38;
        text-decoration: underline;
    }

    .co-popup-preconfig-actions {
        display: flex;
        flex-direction: row;
        padding-top: 50px;
        align-items: center;
        justify-content: center;
        margin: 0;
        padding: 0;
        margin-top: 20px;
    }
    .co-popup-preconfig-actions li{
        display: flex;
        margin-bottom: 5px;
    }

    .co-popup-preconfig-actions li a {
        text-decoration: none;
        padding: 10px 20px;
        border-radius: 4px;
        color: #2c3e50;
        font-size: 18px;
    }

    .co-popup-preconfig-actions li a span {
        padding-right: 10px;
    }

    .co-popup-preconfig-actions li:first-child a {
        background-color: #9fbd38;
        color: white;
        font-weight: bold;
    }

    .co-popup-preconfig-actions button {
        width: fit-content;
        margin-top: 10px;
        background-color: transparent;
        font-size: 18px;
        color: #2c3e50;
    }

    .co-popup-preconfig-background {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
    }

    .co-popup-preconfig-content , .co-popup-preconfig-finish {
        transition: all 0.7s 0.3s cubic-bezier(0.5, 0.005, 0.075, 0.985), transform 0.1s 0.7s, -webkit-transform 0.1s;
        position: relative;
        padding: 20px;
    }

    .co-popup-preconfig-particle {
        width: 100%;
        height: 100%;
    }

    .ap-starter-btn-close {
        background-color: #2c3e50;
        color: white !important;
        margin-left: 5px;
    }

    .co-popup-preconfig-header {
        background-color: #fff !important;
    }

    @media (max-width: 720px) {
        .co-popup-preconfig-actions {
            flex-direction: column;
        }
    }

    .co-popup-preconfig-question .wrapper {
        display:flex;
        flex-direction:column;
    }
    .co-popup-preconfig-question .grid-row {
        margin-bottom: 1em
    }
    .co-popup-preconfig-question .grid-row, .grid-header {
        display:flex;
        /*   flex: 1 0 auto; */
        /*   height: auto; */
    }

    .co-popup-preconfig-question .grid-header {
        align-items: flex-end;
    }
    .co-popup-preconfig-question .header-item {
        width:100px;
        text-align:center;
        /*   border:1px solid transparent; */
    }
    .co-popup-preconfig-question .header-item:nth-child(1) {
        width:180px;
    }
    .co-popup-preconfig-question .subtitle {
        font-size: 0.7em;
    }
    .co-popup-preconfig-question .flex-item:before {
        content: '';
        padding-top:26%;
    }
    .co-popup-preconfig-question .flex-item {
        display:flex;
        /*   flex-basis:25%; */
        width:100px;
        border-bottom:1px solid #ccc;
        justify-content: center;
        align-items:center;
        /*   text-align:left; */
        font-size: 1em;
        font-weight:normal;
        color:#999;
    }
    .co-popup-preconfig-question .flex-item:nth-child(1) {
        border:none;
        font-size:1.15em;
        color:#000;
        width:180px;
        justify-content: left;
    }

    .co-popup-preconfig-question [type="radio"], .co-popup-preconfig-question [type="checkbox"] {
        border: 0;
        clip: rect(0 0 0 0);
        height: 1px; margin: -1px;
        overflow: hidden;
        padding: 0;
        position: absolute;
        width: 1px;
    }

    .co-popup-preconfig-question  label {
        /*display: block;*/
        cursor: pointer;
    }

    .co-popup-preconfig-question [type="radio"] + span:before{
        content: '';
        display: inline-block;
        width: 1.5em;
        height: 1.5em;
        vertical-align: -0.25em;
        border-radius: 50%;
        border: 0.125em solid #fff;
        box-shadow: 0 0 0 0.15em #555;
        transition: 0.5s ease all;
    }

    .co-popup-preconfig-question [type="checkbox"] + span:before{
        content: '';
        display: inline-block;
        width: 1.5em;
        height: 1.5em;
        vertical-align: -0.25em;
        border-radius:.25em;
        border: 0.125em solid #fff;
        box-shadow: 0 0 0 0.15em #555;
        transition: 0.5s ease all;
    }

    .co-popup-preconfig-question [type="checkbox"] + span:before ,
    .co-popup-preconfig-question [type="radio"] + span:before {
        margin-right: 0.75em;
    }

    .co-popup-preconfig-question [type="radio"]:checked + span:before,
    .co-popup-preconfig-question [type="checkbox"]:checked + span:before{
        background: var(--aap-primary-color);
        box-shadow: 0 0 0 0.25em #666;
    }

    .co-popup-preconfig-question  [type="radio"]:focus span:after {
        content: '\0020\2190';
        font-size: 1.5em;
        line-height: 1;
        vertical-align: -0.125em;
    }

    .co-popup-preconfig-question fieldset {
        font-size: 1em;
        border: 2px solid #000;
        padding: 2em;
        border-radius: 0.5em;
        margin-bottom: 20px;
    }

    .swiper-slide-next {
        display : none !important;
    }

    .co-popup-preconfig-menu {
        background-color: var(--aap-primary-color);
        height: 100%;
        border-top-left-radius: 10px;
        border-bottom-left-radius: 10px;
    }

    .co-popup-preconfig-question-container {
        height: 600px;
        width: 1000px;
    }

    /*preconfig Timeline */
    ul.timeline.aap-preconfigListStepSwipping {
        transition: all 0.7s 0.3s cubic-bezier(0.5, 0.005, 0.075, 0.985), transform 0.1s 0.7s, -webkit-transform 0.1s;
        list-style-type: none;
        position: relative;
        padding: 0px;
    }
    ul.timeline.aap-preconfigListStepSwipping:before {
        content: ' ';
        background: #fff;
        display: inline-block;
        position: absolute;
        left: 10px;
        width: 2px;
        height: 100%;
        z-index: 400;
    }
    ul.timeline.aap-preconfigListStepSwipping > li {
        margin: 10px 0;
        padding-left: 30px;
        float: none;
    }
    ul.timeline.aap-preconfigListStepSwipping > li:before {
        content: ' ';
        background: #fff;
        display: inline-block;
        position: absolute;
        border-radius: 50%;
        border: 3px solid var(--aap-primary-color);
        left: 1px;
        width: 18px;
        height: 18px;
        z-index: 400;
    }

    ul.timeline.aap-preconfigListStepSwipping > li.active:before {
        content: ' ';
        background: var(--aap-primary-color);
        display: inline-block;
        position: absolute;
        border-radius: 50%;
        border: 3px solid var(--aap-secondary-color);
        left: 1px;
        width: 18px;
        height: 18px;
        z-index: 400;
    }


    .timeline.aap-preconfigListStepSwipping > li .name-step {
        display: block;
        max-width: 100%;
        margin-bottom: 5px;
        font-weight: 700;
        font-size: 16px;
        color: #fff;
        text-align: left;
    }
    .timeline.aap-preconfigListStepSwipping > li > p {
        font-size: 14px;
        color: #eee;
        grid-template-rows: min-content 1fr;
    }
    .timeline.aap-preconfigListStepSwipping > li > hr {
        width: 100%;
        margin-bottom: 0px;
    }
    .aap-preconfigcontain-info-costum {
        margin-top: 30px;
        margin-bottom: 30px;
    }
    .aap-preconfigcontain-info-costum img.logo-info {
        width: 75px;
        height: 75px;
        border: solid 2px rgba(255, 255, 255);
        border-radius: 50%;
    }
    .aap-preconfigcontain-info-costum .title-info {
        font-size: 18px;
        color: #fff;
    }

    .co-popup-preconfig-question .swiper-slide-:not(:last-child){
        visibility: hidden;
    }

    .co-popup-preconfig-question .swiper-slide-:not(:last-child) div{
        display: none;
    }

    .li-start-preconfig{
        background-color : var(--aap-primary-color);
        border-radius: 4px;
    }

    input.checkbox-preconfig-start:checked + div div + div ul .li-start-preconfig {
        transition: all 0.7s cubic-bezier(0.5, 0.005, 0.075, 0.985);
        width: 100%;
        left: 0;
        height: 100%;
    }

    .li-start-preconfig {
        transition: all 0.7s 0.3s cubic-bezier(0.5, 0.005, 0.075, 0.985), transform 0.1s 0.7s, -webkit-transform 0.1s;
        width: 260px;
        left: 0;
        height: 100%;
    }

    input.checkbox-preconfig-start:checked + div div + div ul .li-abord-preconfig {
        transition: all 0.7s cubic-bezier(0.5, 0.005, 0.075, 0.985);
        width: 0%;
        opacity: 0;
    }

    .li-abord-preconfig {
        transition: all 0.7s 0.3s cubic-bezier(0.5, 0.005, 0.075, 0.985), transform 0.1s 0.7s, -webkit-transform 0.1s;
        width: 190px;
        opacity: 1;
    }

    input.checkbox-preconfig-start:checked + div.co-popup-preconfig-content {
        transition: all 0.7s cubic-bezier(0.5, 0.005, 0.075, 0.985);
        width: 1000px;
        height: 600px;
        padding: 0;
    }

    .li-abord-preconfig {
        transition: all 0.7s 0.3s cubic-bezier(0.5, 0.005, 0.075, 0.985), transform 0.1s 0.7s, -webkit-transform 0.1s;
        width: 190px;
        opacity: 1;
    }

    input.checkbox-preconfig-start:checked + div div + div ul .li-start-preconfig a ,
    input.checkbox-preconfig-start:checked + div div + div ul .li-abord-preconfig ,
    input.checkbox-preconfig-start:checked + div div .co-popup-preconfig-header-h2 ,
    input.checkbox-preconfig-start:checked + div div .co-popup-preconfig-header-h4 ,
    input.checkbox-preconfig-start:checked + div div .co-popup-preconfig-header-p
    {
        transition: all 0.7s cubic-bezier(0.5, 0.005, 0.075, 0.985);
        opacity: 0;
    }

    input.checkbox-preconfig-start:checked + div div .co-popup-preconfig-header-logo
    {
        transition: all 0.7s cubic-bezier(0.5, 0.005, 0.075, 0.985);
        float: left;
        margin-left: 130px;
    }

    .li-start-preconfig a,
    .li-abord-preconfig ,
    .co-popup-preconfig-header-h2 ,
    .co-popup-preconfig-header-h4 ,
    .co-popup-preconfig-header-p
    {
        transition: all 0.7s 0.3s cubic-bezier(0.5, 0.005, 0.075, 0.985), transform 0.1s 0.7s, -webkit-transform 0.1s;
        opacity: 1;
    }

    input.checkbox-preconfig-start:checked + div div + div .co-popup-preconfig-actions {
        transition: all 0.7s cubic-bezier(0.5, 0.005, 0.075, 0.985);
        width: 33.33333333%;
        left: 0;
        height: 100%;
        margin : 0;
    }

    input.checkbox-preconfig-start:checked + div div + div.co-popup-preconfig-header-action {
        transition: all 0.7s cubic-bezier(0.5, 0.005, 0.075, 0.985);
        height: 100%;
    }

    co-popup-preconfig-header-action {
        height: 45px;
    }

    input.checkbox-preconfig-start:checked + div div.co-popup-preconfig-header {
        transition: all 0.7s cubic-bezier(0.5, 0.005, 0.075, 0.985);
        background-color: transparent !important;
        position: absolute;
    }

    .co-popup-preconfig-actions {
        transition: all 0.7s 0.3s cubic-bezier(0.5, 0.005, 0.075, 0.985), transform 0.1s 0.7s, -webkit-transform 0.1s;
        width: 100%;
        left: 0;
        height: 100%;
    }

    .co-popup-preconfig-header-logo img.logo-info {
        width: 75px;
        height: 75px;
        border: solid 2px var(--aap-primary-color);
        border-radius: 50%;
    }

    .co-popup-preconfig-menu legend {
        font-size: 25px;
    }

    .co-popup-preconfig-center {
        margin: 0;
        top: 50%;
        -ms-transform: translateY(-50%);
        transform: translateY(-50%);
    }

    .swiper-slide .wrapper {
        padding: 20px 50px;
        text-align: left;
    }

    .co-popup-preconfig-title {
        font-size: 30px;
        font-weight: bold;
        color : var(--aap-secondary-color)
    }

    .co-popup-preconfig-subtitle {
        font-size: 18px;
        font-weight: bold;
        color : var(--aap-secondary-color)
    }

    .preconfig-info-has-tooltip:hover .preconfig-info-tooltip,
    .preconfig-info-has-tooltip:focus .preconfig-info-tooltip,
    .preconfig-info-has-tooltip.hover .preconfig-info-tooltip {
        opacity: 1;
        rotate(0deg);
        pointer-events: inherit;
    }

    .preconfig-info-tooltip {
        display: block;
        position: absolute;
        padding: 10px 30px;
        border-radius: 5px;
        background: var(--aap-primary-color);
        text-align: center;
        color: white;
        opacity: 0;
        pointer-events: none;
        z-index: 5;
        bottom : 30%;
        transform: translate(-50%, 0px);
    }

    .preconfig-info-tooltip:hover {
        opacity: 1;
        pointer-events: inherit; }
    .preconfig-info-tooltip img {
        max-height: 200px;
    }
    .preconfig-info-tooltip:after {
        content: '';
        display: block;
        margin: 0 auto;
        widtH: 0;
        height: 0;
        border: 5px solid transparent;
        border-top: 5px solid rgba(0, 0, 0, 0.75);
        position: absolute;
        left : 50%;
    }

    .createActivationStepRow {
        padding: 0px 30px;
    }

</style>
<div class="co-popup-preconfig-container">
    <div class="co-popup-preconfig">
        <input type="checkbox" class="checkbox-preconfig-start hide">
        <div class="co-popup-preconfig-content">
            <div class="co-popup-preconfig-header">
                <div class="co-popup-preconfig-header-logo">
                </div>
                <h2 class="co-popup-preconfig-header-h2 co-popup-preconfig-title">Bienvenue</h2>
                <span class="co-popup-preconfig-header-h4 co-popup-preconfig-subtitle">Si c'est votre première visite, nous vous proposons ce guide afin de vous aider à configurer votre plateforme </span>
                <p class="co-popup-preconfig-header-p">Souhaitez-vous suivre le guide ou préférez-vous passer directement dans l'action et revenir sur la configuration plus tard dans le menu <i class="fa fa-cog" ></i> configuration </p>
            </div>
            <div class="co-popup-preconfig-header-action">
                <ul class="co-popup-preconfig-actions">
                    <li class="li-start-preconfig"><a href="javascript:;" class="ap-starter-btn-start-preconfig"><span><i class="fa fa-cog" aria-hidden="true"></i></span> Configurer maintenant</a></li>
                    <li class="li-abord-preconfig"><a href="javascript:;" class="ap-starter-btn-close"> Passer cette étape </a></li>
                </ul>
            </div>

            <div class="caption-container">
                <div class="caption-title"></div>
                <div class="caption"></div>
            </div>
        </div>
        <div class="co-popup-preconfig-question" style="display: none;">
            <div class="co-popup-preconfig-question-container" >
                <div class="col-md-4 co-popup-preconfig-menu "">
                    <div class="center aap-preconfigcontain-info-costum">

                    </div>
                    <ul class="timeline aap-preconfigListStepSwipping">
                        <li class="active" data-actualstep="activationstep">
                            <div class="name-step">Ouverture aux réponses</div>
                            <p> </p>
                        </li>
                    </ul>
                </div>
                <div class="col-md-8 padding-10 center co-popup-preconfig-center" >
                    <div class="swiper" id="swiper">
                        <div class="swiper-wrapper">

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="co-popup-preconfig-finish" style="display: none;">
            <div class="co-popup-preconfig-header margin-top-20 padding-20">
                <div class="co-popup-preconfig-title">Votre plateforme est maintenant prêt...</div>
                <span class="co-popup-preconfig-subtitle">Félicitations pour la configuration de votre plateforme !
                    Nous vous recommandons de personnaliser votre communauté ou plongez directement dans la navigation ! Parcourez les différentes sections et commencer à ajouter vos proposition et projets</h4>
                <p>Vous pouvez configurer le plateforme à tout moment en cliquant sur le menu <i class="fa fa-cog" ></i> configuration</p>

            </div>
            <div class="">
                <ul class="co-popup-preconfig-actions">
                    <li><a href="javascript:;" class="btn-aap-secondary ap-starter-btn-community"> Gérer ma communauté </a></li>
                    <li><a href="javascript:;" class="ap-starter-btn-close"> Terminer </a></li>
                </ul>
            </div>

            <div class="caption-container">
                <div class="caption-title"></div>
                <div class="caption"></div>
            </div>
        </div>
    </div>
</div>

<script>
    let swiper ;
</script>
