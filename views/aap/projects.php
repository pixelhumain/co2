<?php
    $css_js = [
        '/css/bootstrap-ext-card.css',
        '/css/bootstrap-ext-button.css',
        '/css/bootstrap-ext-progressbar.css',
        '/js/bootstrap-ext-progressbar.js',
        '/js/bootstrap-ext-button.js',
        '/css/aap/proposal.css'
    ];
    HtmlHelper::registerCssAndScriptsFiles($css_js, Yii::app()->getModule('co2')->getAssetsUrl());
?>
<style>
    .divider {
        width: 100%;
        border-top: 1px solid #ccc;
    }

    .mt-2 {
        margin-top: 20px;
    }

    .mb-2 {
        margin-bottom: 20px;
    }

    #dropdown_search {
        width: 100%;
        min-height: 1px !important;
        display: block !important;
        flex-wrap: nowrap !important;
        align-items: normal !important;
        margin: 0 !important;
        height: max-content !important;
    }

    #dropdown_search > .card {
        margin-bottom: 20px;
    }

    .proposition-actions-container {
        align-items: center;
    }

    .text-primary {
        color: #9bc125;
    }
</style>

<div class="no-padding aap-list-container">
    <div class="row">
        <div class="col-md-12 no-padding">
            <div class="col-xs-3 menu-filters-lg filter" id="filterContainerAapOceco" style="display: none;">
                <div class="filterContainerL filterShown filter-alias custom-filter searchBar-filters pull-left mx-4 col-xs-12 no-padding hidden-xs">
                    <input
                            type="text"
                            class="form-control pull-left text-center alias-main-search-bar search-bar"
                            data-field="text"
                            placeholder="Recherche par nom du projet"
                    >
                    <span
                            class="text-white input-group-addon pull-left main-search-bar-addon "
                            data-field="text"
                            data-icon="fa-search"
                    >
                        <i class="fa fa-search"></i>
                    </span>
                </div>
                <h4 class="d-flex justify-content-between col-xs-12 pt-2 pb-32 border-bottom-default">
                    <span class="title propsPanelTitle"><?php echo Yii::t("form", "Filters") ?></span>
                    <i id="btnHideFilter" class="fa fa-angle-double-left cursor-pointer pr-2 custom-font showHide-filters-xs"></i>
                </h4>
                <div id='filterContainerL' class='aap-filter menu-verticale searchObjCSS'></div>

            </div>
            <div class="col-xs-12 col-md-12  filter-container">
                <div class="row filter-list-container-grid" id="filter-list-container">
                    <div class="no-padding">
                        <div class="filterContainerL filterHidden filter-alias custom-filter searchBar-filters pull-left mx-4 hidden-xs">
                            <input
                                    type="text"
                                    class="form-control pull-left text-center alias-main-search-bar search-bar"
                                    data-field="text"
                                    placeholder="Recherche par nom du projet"
                            >
                            <span
                                    class="text-white input-group-addon pull-left main-search-bar-addon "
                                    data-field="text"
                                    data-icon="fa-search"
                            >
                                <i class="fa fa-search"></i>
                            </span>
                        </div>
                        <button type="button" class="btn btn-default showHide-filters-xs btn-aap-tertiary mt-2 py-3 hidden-xs"
                                style="height: fit-content; font-size: 1.5rem"
                                id="show-filters-lg">
                            <span class="filter-xs-count topbar-badge badge animated bounceIn badge-tranparent badge-success"></span>
                            <i class="fa fa-sliders"></i>
                            <small class="pl-2"><?php echo Yii::t("form", "Filters") ?></small>
                        </button>
                    </div>
                    <div class='headerSearchContainerL no-padding'></div>
                    <div class="view-container">
                        <div class="medium-view">
                            <span class="hidden">Vue</span>
                            <div class="btn-group">
                                <button class="btn btn-default btn-tooltip change-list-view hidden-xs" data-view="kanban" data-toggle="tooltip"
                                        data-placement="top" title="Kanban">
                                    <i class="fa fa-trello fs17"></i>
                                </button>
                                <button class="btn btn-default btn-tooltip change-list-view hidden-xs" data-view="detailed" data-toggle="tooltip"
                                        data-placement="top" title="<?= Yii::t('common', 'Detailed view') ?>">
                                    <i class="fa fa-th-list fs17"></i>
                                </button>
                                <button class="btn btn-default btn-tooltip change-list-view" data-view="minimalist" data-toggle="tooltip"
                                        data-placement="top" title="<?= Yii::t('common', 'Compact view') ?>">
                                    <i class="fa fa-bars fs17 fw600 rotate90"></i>
                                </button>
                                <?php if (!empty($showMap) && filter_var($showMap, FILTER_VALIDATE_BOOL)): ?>
                                    <button class="btn btn-default btn-tooltip change-list-view hidden-xs" data-view="map" data-toggle="tooltip"
                                        data-placement="top" title="<?= Yii::t('common', 'Disable/Enable map view') ?>">
                                        <i class="fa fa-map-marker fs17 fw600"></i>
                                    </button>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="small-view">
                            <div class="dropdown">
                                <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown">
                                    Vue
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu pull-right" role="menu" aria-labelledby="dropdownMenu1">
                                    <li role="presentation" class="change-list-view" data-view="detailed">
                                        <a role="menuitem" tabindex="-1" data-target="#">
                                            <i class="fa fa-th-list"></i> <?= Yii::t('common', 'Detailed') ?>
                                        </a>
                                    </li>
                                    <li role="presentation" class="change-list-view" data-view="minimalist">
                                        <a role="menuitem" tabindex="-1" data-target="#">
                                            <i class="fa fa-bars rotate90"></i> <?= Yii::t('common', 'Compact') ?>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class='bodySearchContainer margin-top-30 col-xs-12'>
                        <div class='col-xs-12 no-padding project-container' id='dropdown_search'>
                        </div>
                        <div class='padding-top-20 col-xs-12 text-left footerSearchContainerL'></div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-xs-12 mapOfResults">
                <div id="mapOfResultsAnswL"></div>
            </div>
        </div>
    </div>
</div>

<script>
    $(function () {
        var myProjects = <?= $myProjects ?? "false" ?>;

        function proposition_init_filters() {
            //var filterPrms = aapObj.paramsFilters.project(aapObj);
            aapObj.showMyMap = notNull(localStorage.getItem("showMyMap")) ? (localStorage.getItem("showMyMap") === 'true' && aapObj.showMap) : false;

            var filterPrms = aapObj.paramsFilters.project(aapObj);
            var filterProposals = aapObj.paramsFilters.proposal(aapObj);
            filterPrms.defaults.filters = filterProposals.defaults.filters;
            // filterPrms.defaults.filters['project.id'] = {'$exists' : true};
            filterPrms.defaults?.filters?.['project.id'] ? delete filterPrms.defaults.filters['project.id'] : "";
            filterPrms.defaults?.filters?.['status'] ? delete filterPrms.defaults.filters['status'] : "";
            filterPrms.defaults?.filters?.['answers.aapStep1.year'] ? delete filterPrms.defaults.filters['answers.aapStep1.year'] : "";
            filterPrms.filters = filterProposals.filters;
            filterPrms.filters.avancement = {
                view : "accordionList",
                type : "filters",
                field : "properties.avancement",
                name : ucfirst(trad.advancement),
                event : "filters",
                keyValue : false,
                list : avancementProject
            }
            if(filterPrms.filters?.avancement?.list) {
				var avancementClone = {};
				for([avcKey, avcVal] of Object.entries(filterPrms.filters.avancement.list)) {
					if( avcKey == "") 
						avancementClone["notSpecified"] = avcVal;
					else
						avancementClone[avcKey] = avcVal;
				}
				notEmpty(avancementClone) ? filterPrms.filters.avancement.list = JSON.parse(JSON.stringify(avancementClone)) : "";
			}

            if (aapObj.showMyMap) {
                const default_option = {
                    container : "#mapOfResultsAnswL",
                    activePopUp : true,
                    zoom:14,
                    centerOneZoom:14,
                    mapOpt : {
                        zoom : 0,
                        latLon : ["-21", "55"],
                        zoomControl : true,
                        doubleClick : true,
                    },
                    mapCustom : {
                        tile : "mapbox",
                        markers:{
                            getMarker:function(){
                                return modules.map.assets + '/images/markers/ngo-marker-default.png'
                            }
                        },
                    },
                    elts : [],
                    buttons : {
                        fullScreen : {
                            allow : true
                        }
                    }
                };
                var mapAnsw = new CoMap(default_option);
                default_option.container = '.map-container';
                modal_map = CoMap(default_option);
            } else {
                $('.mapOfResults, #aap-map-modal').remove();
                $.each(aapObj.common.range(1, 12), function (__, __col) {
                    $('.filter-container').removeClass(`col-md-${__col}`);
                });
                if (aapObj.showMyMap) $('.filter-container').addClass("col-md-6");
                else $('.filter-container').addClass("col-md-12");
            }

            if (myProjects == 1 && notNull(userId) && userId != '') {
                filterPrms.defaults.filters['$or'] = {
                    user : userId,
                    ["links.contributors." + userId] : {$exists : true}
                }
            }
            filterPrms.toActivate = aapObj.filterSearch.project?.search?.obj && aapObj.filterSearch?.activeFilters && Object.keys(aapObj.filterSearch?.activeFilters).length > 0 ? JSON.parse(JSON.stringify(aapObj.filterSearch.activeFilters)) : {}
            aapObj.filterSearch.project = searchObj.init(filterPrms);
            aapObj.filterSearch.lastSearchObjProject?.search?.obj && Object.keys(filterPrms.toActivate).length > 0 ? (aapObj.filterSearch.project.search.obj = JSON.parse(JSON.stringify(aapObj.filterSearch.lastSearchObjProject?.search?.obj)), aapObj.filterSearch.project.search.obj.forced.page = true) : ""
            aapObj.filterSearch.projectInitialized = JSON.parse(JSON.stringify(aapObj.filterSearch.project?.search?.obj));
            if (aapObj.filterSearch.project?.pInit?.filters?.text?.field) {
                aapObj.filterSearch.project.search.obj.textPath = aapObj.filterSearch.project?.pInit?.filters?.text?.field
            }

            if (aapObj.showMyMap) {
                aapObj.filterSearch.project.results.addToMap = function (fObj, results,data) {
                    console.log(results,data,"resultsxxx");
                    var elts = [];
                    var popupLabel = "";
                    $.each(aapObj.inputs, function (k, v) {
                        if (v?.inputs?.adress?.label) {
                            popupLabel = v?.inputs?.adress?.label;
                            return false;
                        }
                    })
                    $.each(data.data.answer, function (resultKey, result) {
                        if (result.answers) {
                            $.each(result.answers, function (k, answer) {
                                if (exists(answer.adress) && exists(answer.adress.geo))
                                    elts.push({
                                        geo : answer.adress.geo,
                                        name : `<h6 class="text-center">${popupLabel}</h6>${trad.projectName} : <b>${answer?.titre}</b>`,
                                    })
                            })
                        }
                    })
                    mapAnsw.addElts(elts);
                    // modal_map.addElts(elts);
                };
            }

            const query = aapObj.common.getQuery(aapObj);
            if (query["text"]) {
                $(".filter-alias .alias-main-search-bar").val(query["text"]);
            }

            aapObj.filterSearch.project.search.init(aapObj.filterSearch.project);
            aapObj.filterSearch.lastSearchObjProposal.search.obj = {}
            aapObj.filterSearch.activeFilters = {}
            aapObj.filterSearch.project.search.obj?.forced?.page ? delete aapObj.filterSearch.project.search.obj.forced.page : "";

        }

        proposition_init_filters(aapObj.form);
        aapObj.events.common(aapObj);
        aapObj.events.proposal(aapObj);

        $('.create-project').on('click', function () {
            aapObj.dynform.createProject(aapObj);
        });
        $('.view-container').on('mouseover', function (event) {
            $(this).find('[data-toggle=tooltip]').tooltip();
        });
    })
</script>