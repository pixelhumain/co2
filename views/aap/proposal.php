<?php
    HtmlHelper::registerCssAndScriptsFiles(['/css/aap/proposal.css'], $this->module->assetsUrl);
    if (!empty($form['params']['csv_exporter']))
        echo $this->renderPartial($form['params']['csv_exporter'], ['form' => (string)$form['_id']])
?>
<div class="no-padding aap-list-container">
    <div class="row">
        <div class="col-md-12 no-padding">
            <div class="col-xs-3 menu-filters-lg filter" id="filterContainerAapOceco" style="display: none;">
                <div class="filterContainerL filterShown filter-alias custom-filter searchBar-filters pull-left mx-4 col-xs-12 no-padding hidden-xs">
                    <input
                            type="text"
                            class="form-control pull-left text-center alias-main-search-bar search-bar"
                            data-field="text"
                            data-text-path="answers.aapStep1.titre"
                            placeholder="<?php echo Yii::t('common', "Search by") ?> <?php echo ucfirst(Yii::t('common', "Search by proposal title")) ?>"
                    >
                    <span
                            class="text-white input-group-addon pull-left main-search-bar-addon "
                            data-field="text"
                            data-icon="fa-search"
                    >
                        <i class="fa fa-search"></i>
                    </span>
                </div>
                <h4 class="d-flex justify-content-between col-xs-12 pt-2 pb-32 border-bottom-default">
                    <span class="title propsPanelTitle"><?php echo Yii::t("form", "Filters") ?></span>
                    <i id="btnHideFilter" class="fa fa-angle-double-left cursor-pointer pr-2 custom-font showHide-filters-xs"></i>
                </h4>
                <div id='filterContainerL' class='aap-filter menu-verticale searchObjCSS'></div>
            </div>
            <div class="col-xs-12 col-md-12 filter-container">
                <div class="row filter-list-container-grid" id="filter-list-container">
                    <div class="no-padding">
                        <div class="filterContainerL filterHidden filter-alias custom-filter searchBar-filters pull-left mx-4 hidden-xs">
                            <input
                                    type="text"
                                    class="form-control pull-left text-center alias-main-search-bar search-bar"
                                    data-field="text"
                                    data-text-path="answers.aapStep1.titre"
                                    placeholder="<?= Yii::t('common', 'Search by proposal title') ?>"
                            >
                            <span
                                    class="text-white input-group-addon pull-left main-search-bar-addon "
                                    data-field="text"
                                    data-icon="fa-search"
                            >
                                <i class="fa fa-search"></i>
                            </span>
                        </div>
                        <button type="button" class="btn btn-default showHide-filters-xs btn-aap-tertiary mt-2 py-3 hidden-xs"
                                style="height: fit-content; font-size: 1.5rem"
                                id="show-filters-lg">
                            <span class="filter-xs-count topbar-badge badge animated bounceIn badge-tranparent badge-success"></span>
                            <i class="fa fa-sliders"></i>
                            <small class="pl-2"><?php echo Yii::t("form", "Filters") ?></small>
                        </button>
                    </div>
                    <div class='headerSearchContainerL no-padding'></div>
                    <div class="view-container">
                        <div class="medium-view">
                            <span class="hidden">Vue</span>
                            <div class="btn-group">
                                <button class="btn btn-default btn-tooltip change-list-view hidden-xs" data-view="kanban" data-toggle="tooltip" data-placement="top" data-original-title="Kanban">
                                    <i class="fa fa-trello fs17"></i>
                                </button>
                                <button class="btn btn-default btn-tooltip change-list-view hidden-xs" data-view="detailed" data-toggle="tooltip" data-placement="top" data-original-title="<?= Yii::t('common', 'Detailed view') ?>">
                                    <i class="fa fa-th-list fs17"></i>
                                </button>
                                <button class="btn btn-default btn-tooltip change-list-view" data-view="minimalist" data-toggle="tooltip" data-placement="top" data-original-title="<?= Yii::t('common', 'Compact view') ?>">
                                    <i class="fa fa-bars fs17 fw600 rotate90"></i>
                                </button>
                                <?php if (!empty($showMap) && filter_var($showMap, FILTER_VALIDATE_BOOL)): ?>
                                    <button class="btn btn-default btn-tooltip change-list-view" data-view="map" data-toggle="tooltip" data-placement="top" data-original-title="<?= Yii::t('common', 'Disable/Enable map view') ?>">
                                        <i class="fa fa-map-marker fs17 fw600"></i>
                                    </button>
                                <?php endif; ?>
                                <button class="btn btn-default btn-tooltip change-list-view" data-view="table" data-toggle="tooltip" data-placement="top" data-original-title="<?= Yii::t('common', 'Table view') ?>">
                                    <i class="fa fa-table fs17"></i>
                                </button>
                            </div>
                        </div>
                        <div class="small-view">
                            <div class="dropdown">
                                <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown">
                                    Vue
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu pull-right" role="menu" aria-labelledby="dropdownMenu1">
                                    <li role="presentation" class="change-list-view" data-view="detailed">
                                        <a role="menuitem" tabindex="-1" data-target="#">
                                            <i class="fa fa-th-list"></i> <?= Yii::t('common', 'Detailed view') ?>
                                        </a>
                                    </li>
                                    <li role="presentation" class="change-list-view" data-view="minimalist">
                                        <a role="menuitem" tabindex="-1" data-target="#">
                                            <i class="fa fa-bars rotate90"></i> <?= Yii::t('common', 'Compact view') ?>
                                        </a>
                                    </li>
                                    <?php if (!empty($showMap) && filter_var($showMap, FILTER_VALIDATE_BOOL)): ?>
                                        <li role="presentation" class="change-list-view" data-view="map">
                                            <a role="menuitem" tabindex="-1" data-target="#">
                                                <i class="fa fa-map-marker"></i> <?= Yii::t('common', 'Disable/Enable map view') ?>
                                            </a>
                                        </li>
                                    <?php endif; ?>
                                    <li role="presentation" class="change-list-view" data-view="table">
                                        <a role="menuitem" tabindex="-1" data-target="#">
                                            <i class="fa fa-table"></i> <?= Yii::t('common', 'Table view') ?>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="bodySearchContainer margin-top-30 col-xs-12 row">
                        <div class='col-xs-12 no-padding' id='dropdown_search'>
                        </div>
                        <div class='padding-top-20 col-xs-12 text-left footerSearchContainerL'></div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-xs-12 mapOfResults">
                <div id="mapOfResultsAnswL"></div>
            </div>
        </div>
    </div>
</div>

<script>
    $(function () {
        function proposition_init_filters() {
            aapObj.showMyMap = notNull(localStorage.getItem("showMyMap")) ? (localStorage.getItem("showMyMap") === 'true' && aapObj.showMap) : false;
            var myProposal = <?= $myProposal ?? "false" ?>;
            let modal_map;
            if (aapObj.showMyMap) {
                const default_option = {
                    container : "#mapOfResultsAnswL",
                    activePopUp : true,
                    zoom:14,
                    centerOneZoom:14,
                    mapOpt : {
                        zoom : 0,
                        latLon : ["-21", "55"],
                        zoomControl : true,
                        doubleClick : true,
                    },
                    mapCustom : {
                        tile : "mapbox",
                        markers:{
                            getMarker:function(){
                                return modules.map.assets + '/images/markers/ngo-marker-default.png'
                            }
                        },
                    },
                    elts : [],
                    buttons : {
                        fullScreen : {
                            allow : true
                        }
                    }
                };
                var mapAnsw = new CoMap(default_option);
                default_option.container = '.map-container';
                modal_map = CoMap(default_option);
            } else {
                $('.mapOfResults, #aap-map-modal').remove();
                $.each(aapObj.common.range(1, 12), function (__, __col) {
                    $('.filter-container').removeClass(`col-md-${__col}`);
                });
                if (aapObj.showMyMap) $('.filter-container').addClass("col-md-6");
                else $('.filter-container').addClass("col-md-12");
            }
            var filterPrms = aapObj.paramsFilters.proposal(aapObj);

            if (myProposal == 1 || (typeof aapObj.forms != "undefined" && typeof aapObj.forms.membersCanSeeResume != "undefined" && aapObj.forms.membersCanSeeResume)) {
                filterPrms.defaults.filters['$or'] = {
                    user : userId,
                    ["links.contributors." + userId] : {$exists : true}
                }
                if(filterPrms?.defaults?.filters?.["answers.aapStep1.titre"]) {
                    delete filterPrms?.defaults?.filters?.["answers.aapStep1.titre"]
                }
            }
            filterPrms.toActivate = aapObj.filterSearch.proposal?.search?.obj && aapObj.filterSearch?.activeFilters && Object.keys(aapObj.filterSearch?.activeFilters).length > 0 ? JSON.parse(JSON.stringify(aapObj.filterSearch.activeFilters))  : {}

            aapObj.filterSearch.proposal = searchObj.init(filterPrms);
            aapObj.filterSearch.lastSearchObjProposal?.search?.obj && Object.keys(filterPrms.toActivate).length > 0 ? (aapObj.filterSearch.proposal.search.obj = JSON.parse(JSON.stringify(aapObj.filterSearch.lastSearchObjProposal?.search?.obj)), aapObj.filterSearch.proposal.search.obj.forced.page = true) : ""
            aapObj.filterSearch.proposalInitialized = JSON.parse(JSON.stringify(aapObj.filterSearch.proposal?.search?.obj));

            if (aapObj.showMyMap) {
                aapObj.filterSearch.proposal.results.addToMap = function (fObj, results) {
                    var elts = [];
                    var popupLabel = "";
                    $.each(aapObj.inputs, function (k, v) {
                        if (v?.inputs?.adress?.label) {
                            popupLabel = v?.inputs?.adress?.label;
                            return false;
                        }
                    })
                    $.each(results, function (resultKey, result) {
                        if (result.answers) {
                            $.each(result.answers, function (k, answer) {
                                if (exists(answer.adress) && exists(answer.adress.geo))
                                    elts.push({
                                        geo : answer.adress.geo,
                                        name : `<h6 class="text-center">${popupLabel}</h6>${trad.projectName} : <b>${answer?.titre}</b>`,
                                    })
                            })
                        }
                    })
                    mapAnsw.addElts(elts);
                    //modal_map.addElts(elts);
                };
            }
            if (aapObj.filterSearch.proposal?.pInit?.filters?.text?.field) {
                aapObj.filterSearch.proposal.search.obj.textPath = aapObj.filterSearch.proposal?.pInit?.filters?.text?.field;
            }
            const query = aapObj.common.getQuery(aapObj);
            if (query["text"]) {
                $(".filter-alias .alias-main-search-bar").val(query["text"]);
            }
            aapObj.filterSearch.proposal.search.init(aapObj.filterSearch.proposal);
            aapObj.filterSearch.lastSearchObjProposal.search.obj = {}
            aapObj.filterSearch.activeFilters = {}
            aapObj.filterSearch.proposal.search.obj?.forced?.page ? delete aapObj.filterSearch.proposal.search.obj.forced.page : ""
        }

        proposition_init_filters()
        aapObj.events.proposal(aapObj)
        aapObj.events.common(aapObj);
        aapObj.common.switchFilter();
    })
</script>
