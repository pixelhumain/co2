<style>
    .overflow-hidden {
        overflow: hidden !important;
    }

    .ap-guide-wrapper {
        width: 100%;
        height: 100%;
        position: fixed;
        top: 0;
        left: 0;
        z-index: 9999999;
        display: flex;
        align-items: center;
        justify-content: center;
        background-color: rgba(0, 0, 0, .5);
        animation: fade-in-animation .5s ease-in-out forwards;
    }

    .ap-guide-container {
        width: 75%;
        background-color: white;
        position: relative;
        border-radius: 4px;
        padding: 40px;
    }

    .ap-guide-step-body {
        display: flex;
        width: 100%;
    }

    .ap-guide-description, .ap-guide-illustation {
        width: 50%;
    }

    .ap-guide-illustation {
        display: flex;
        justify-content: center;
        align-items: center;
    }

    .ap-guide-illustation img {
        width: 90%;
        height: auto;
    }

    .ap-guide-step-title {
        margin: 0px 0px 40px 0px;
        font-size: 32px;
        line-height: 1.3;
    }

    .ap-guide-step-title span {
        color: #9fbd38;
        font-weight: bold;
    }

    .ap-guide-step-description {
        font-size: 18px;
        line-height: 1.8;
    }

    .ap-guide-step-description a {
        display: inline-block;
        padding: 2px 10px;
        border: 2px solid white;
        background-color: #9fbd38;
        border-radius: 50px;
        text-decoration: none;
        color: white;
        font-size: 14px;
        transition: .5s;
    }

    .ap-guide-step-description a:hover {
        background-color: white;
        color: #9fbd38;
        border-color: #9fbd38;
    }

    .ap-guide-step-description span {
        white-space: nowrap;
        padding: 2px 6px;
        color: white;
        background-color: #9fbd38;
        border-radius: 20px;
        font-size: 16px;
        font-weight: bold;
    }

    .ap-guide-step-description-search span {
        font-weight: bold;
        color: #9fbd38;
    }

    .ap-guide-step-description-subscription span {
        width: 40px;
        height: 40px;
        display: inline-block;
        text-align: center;
        border: 2px solid #9fbd38;
        border-radius: 100%;
    }

    .ap-guide-step-action{
        display: flex;
        align-items: center;
        justify-content: space-between;
        padding-top: 20px;
    }

    .ap-guide-step-action a {
        text-decoration: none;
        color: #333;
        font-size: 18px;
        font-weight: 500;
        transition: .5s;
    }

    .ap-guide-step-action a:first-child {
        opacity: .7;
    }

    .ap-guide-step-action a:first-child:hover{
        opacity: 1;
        color: #9fbd38;
    }

    .ap-guide-step-action a:last-child {
        padding: 8px 15px;
        background-color: #9fbd38;
        color: white;
        border-radius: 4px;
        border: 2px solid white;
    }

    .ap-guide-step-action a:last-child:hover {
        border-color: #9fbd38;
        color: #9fbd38;
        background: white;
    }

    @keyframes fade-in-animation {
        from {
            opacity: 0;
        }
        to {
            opacity: 1;
        }
    }

    @media (max-width: 720px) {
        .ap-guide-step-body {
            flex-direction:column;
        }

        .ap-guide-illustation, .ap-guide-description {
            width: 100%;
        }

        .ap-guide-container {
            width: 100%;
            padding: 20px;
            max-height: 70vh;
            overflow: auto;
        }

        .ap-guide-step-title {
            margin-bottom: 20px;
            font-size: 2em;
        }

        .ap-guide-step-action {
            flex-direction: column-reverse;
        }

        .ap-guide-step-action a {
            display: block;
            margin-bottom: 10px;
        }
    }
</style>
<?php 
    $ASSET_URL = $this->module->assetsUrl;
?>
<div class="ap-guide-wrapper">
    <div class="ap-guide-container carousel slide" data-ride="carousel">
        <div class="carousel slide"  data-ride="carousel" id="ap-guide-carousel">
            <div class="carousel-inner">
                <div class="ap-guide-step item active">
                    <div class="ap-guide-step-header">
                        <h1 class="ap-guide-step-title">Comment activer <span>Activitypub ?</span></h1>
                    </div>
                    <div class="ap-guide-step-body">
                        <div class="ap-guide-illustation">
                            <img src="<?= $ASSET_URL ?>/images/activitypub/guide_setting.png" alt="">
                        </div>
                        <div class="ap-guide-description">
                            <p class="ap-guide-step-description">
                                Pour pouvoir utiliser les fonctionnalités d'ActivityPub, vous devez d'abord l'activer. Pour ce faire, 
                                rendez-vous sur votre page de profil, accédez à vos paramètres de confidentialité, 
                                puis activez l'option ActivityPub en cliquant sur le bouton correspondant dans la section appropriée.
                            </p>
                        </div>
                    </div>
                    <div class="ap-guide-step-action">
                        <a href="javascript:;" class="ap-guide-btn-close">Je veux arrêter ce tutoriel</a>
                        <a href="javascript:;" class="ap-guide-btn-lbh" data-hash="#@<?= $_SESSION["user"]["slug"] ?>.view.settings">Activer maintennant <i class="fa fa-chevron-right" aria-hidden="true"></i></a>
                    </div>
                </div>
                <div class="ap-guide-step item">
                    <div class="ap-guide-step-header">
                        <h1 class="ap-guide-step-title">Comment gérer vos communautés externes avec <span>Activitypub</span> ?</h1>
                    </div>
                    <div class="ap-guide-step-body">
                        <div class="ap-guide-illustation">
                            <img src="<?= $ASSET_URL ?>/images/activitypub/guide_community.png" alt="">
                        </div>
                        <div class="ap-guide-description">
                            <p class="ap-guide-step-description">
                                Une fois que vous avez activé la fonctionnalité ActivityPub sur votre compte, 
                                vous êtes maintenant prêt à profiter de ses avantages. Cependant, 
                                il est essentiel de gérer vos communautés externes, 
                                c'est-à-dire vos réseaux au sein du Fediverse.</br> 
                                Pour ce faire, il vous suffit de vous rendre dans le menu 
                                <a href="#"><i class="fa fa-share" aria-hidden="true"></i> Réseau externe</a> 
                                situé dans la section "Communauté" de votre page de profil.
                            </p>
                        </div>
                    </div>
                    <div class="ap-guide-step-action">
                        <a href="javascript:;" class="ap-guide-btn-close">Je veux arrêter ce tutoriel</a>
                        <a href="javascript:;" class="ap-guide-btn-lbh" data-hash="#@<?= $_SESSION["user"]["slug"] ?>.view.directory.dir.externalNetwork">Gérer mes communautés <i class="fa fa-chevron-right" aria-hidden="true"></i></a>
                    </div>
                </div>
                <div class="ap-guide-step item">
                    <div class="ap-guide-step-header">
                        <h1 class="ap-guide-step-title">Rechercher un entité dans <span>Activitypub</span> ?</h1>
                    </div>
                    <div class="ap-guide-step-body">
                        <div class="ap-guide-illustation">
                            <img src="<?= $ASSET_URL ?>/images/activitypub/guide_search.png" alt="">
                        </div>
                        <div class="ap-guide-description">
                            <p class="ap-guide-step-description">
                                Une des caractéristiques principales d'ActivityPub est la capacité de rechercher des entités sur d'autres plateformes. 
                                Pour ce faire, il vous suffit de saisir le nom de l'entité que vous souhaitez rechercher dans la barre de recherche en suivant ce modèle : 
                                <span>@nom_entité@domaine</span>, par exemple: <span>oceatoon@mastodon.social.</span>
                            </p>
                        </div>
                    </div>
                    <div class="ap-guide-step-action">
                        <a href="javascript:;" class="ap-guide-btn-close">Je veux arrêter ce tutoriel</a>
                        <a href="javascript:;" class="ap-guide-btn-search">Rechercher <i class="fa fa-chevron-right" aria-hidden="true"></i></a>
                    </div>
                </div>
                <div class="ap-guide-step item">
                    <div class="ap-guide-step-header">
                        <h1 class="ap-guide-step-title">Suivre un entité dans <span>Activitypub</span> ?</h1>
                    </div>
                    <div class="ap-guide-step-body">
                        <div class="ap-guide-illustation">
                            <img src="<?= $ASSET_URL ?>/images/activitypub/guide_add_member.png" alt="">
                        </div>
                        <div class="ap-guide-description">
                            <p class="ap-guide-step-description">
                                Pour recevoir les activités (articles, événements, etc.) d'une entité externe, vous devez l'ajouter à votre communauté en vous abonnant à elle. Il vous suffit donc de rechercher cette entité, puis de cliquer sur l'icône <span><i class="fa fa-user-plus"></i></span>.
                            </p>
                        </div>
                    </div>
                    <div class="ap-guide-step-action">
                        <a href="javascript:;" class="ap-guide-btn-close">Je veux arrêter ce tutoriel</a>
                        <a href="javascript:;" class="ap-guide-btn-next">Suivant <i class="fa fa-chevron-right" aria-hidden="true"></i></a>
                    </div>
                </div>
                <div class="ap-guide-step item">
                    <div class="ap-guide-step-header">
                        <h1 class="ap-guide-step-title">Organiser les activités en utilisant les <span>tags</span></h1>
                    </div>
                    <div class="ap-guide-step-body">
                        <div class="ap-guide-illustation">
                            <img src="<?= $ASSET_URL ?>/images/activitypub/guide_tags.png" alt="">
                        </div>
                        <div class="ap-guide-description">
                            <p class="ap-guide-step-description">
                                Plus votre communauté s'agrandit, plus vous recevrez des activités provenant de différentes plateformes. Pour faciliter l'organisation et le filtrage de vos flux, vous pouvez attribuer des tags <span><i class="fa fa-tags" aria-hidden="true"></i></span> à chaque membre de votre communauté.
                            </p>
                        </div>
                    </div>
                    <div class="ap-guide-step-action">
                        <a href="javascript:;" class="ap-guide-btn-close">Je veux arrêter ce tutoriel</a>
                        <a href="javascript:;" class="ap-guide-btn-next">Suivant <i class="fa fa-chevron-right" aria-hidden="true"></i></a>
                    </div>
                </div>
                <div class="ap-guide-step item">
                    <div class="ap-guide-step-header">
                        <h1 class="ap-guide-step-title">Consulter les <span>activités</span> provenant d'autres plateformes</h1>
                    </div>
                    <div class="ap-guide-step-body">
                        <div class="ap-guide-illustation">
                            <img src="<?= $ASSET_URL ?>/images/activitypub/guide_activity.png" alt="">
                        </div>
                        <div class="ap-guide-description">
                            <p class="ap-guide-step-description">
                                Félicitations ! Tout est prêt maintenant, et vous pouvez consulter les activités envoyées par vos <span>abonnements</span>. 
                                Vos propres activités seront également visibles pour les personnes <span>abonnées</span> à vous. Les activités reçues sont identifiées par le logo de la plateforme d'origine, qui se trouve en bas de chaque liste.
                            </p>
                        </div>
                    </div>
                    <div class="ap-guide-step-action">
                        <a href="javascript:;"></a>
                        <a href="javascript:;" class="ap-guide-btn-finished">Terminer</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>