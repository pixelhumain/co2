<style>
    .co-popup-activitypub-container{
        width: 100%;
        height: 100vh;
        position: fixed;
        top: 0;
        left: 0;
        background-color: rgba(0, 0, 0, .7);
        display: flex;
        justify-content: center;
        align-items: center;
        color: #2c3e50;
        z-index: 9999999;
    }

    .co-popup-activitypub {
        width: 700px;
        min-height: 400px;
        background-color: white;
        border-radius: 10px;
        position: relative;
    }

    .fediverse-logo{
        width: 150px;
        height: 150px;
        background-color: white;
        border-radius: 100%;
        position: absolute;
        top: -75px;
        right: 40px;
        text-align: center;
        line-height: 150px;
    }

    .fediverse-logo img{
        width: 90%;
    }

    .co-popup-activitypub-header {
        padding-top: 80px;
        text-align: center;
    }

    .co-popup-activitypub-header h1 {
        font-weight: bold;
    }

    .co-popup-activitypub-header p{
        font-size: 18px;
        line-height: 1.8;
    }

    .co-popup-activitypub-header p a {
        text-decoration: none;
        font-weight: bold;
        color: #9fbd38;
        text-decoration: underline;
    }

    .co-popup-activitypub-actions {
        display: flex;
        flex-direction: row;
        padding-top: 50px;
        align-items: center;
        justify-content: center;
        margin: 0;
        padding: 0;
        margin-top: 20px;
    }
    .co-popup-activitypub-actions li{
        display: flex;
        margin-bottom: 5px;
    }

    .co-popup-activitypub-actions li a {
        text-decoration: none;
        padding: 10px 20px;
        border-radius: 4px;
        color: #2c3e50;
        font-size: 18px;
    }

    .co-popup-activitypub-actions li a span {
        padding-right: 10px;
    }

    .co-popup-activitypub-actions li:first-child a {
        background-color: #9fbd38;
        color: white;
        font-weight: bold;
    } 

    .co-popup-activitypub-actions button {
        width: fit-content;
        margin-top: 10px;
        background-color: transparent;
        font-size: 18px;
        color: #2c3e50;
    }

    .co-popup-activitypub-background {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
    }

    .co-popup-activitypub-content {
        position: relative;
        padding: 20px;
    }

    .co-popup-activitypub-particle {
        width: 100%;
        height: 100%;
    }

    .ap-starter-btn-close {
        background-color: #2c3e50;
        color: white !important;
        margin-left: 5px;
    }

    @media (max-width: 720px) {
        .co-popup-activitypub-actions {
            flex-direction: column;
        }
    }
</style>
<div class="co-popup-activitypub-container">
    <div class="co-popup-activitypub">
        <div class="co-popup-activitypub-background"><div id="co-popup-activitypub-particle"></div></div>
        <div class="co-popup-activitypub-content">
            <div class="fediverse-logo"><img src="<?= $this->module->assetsUrl ?>/images/Fediverse_logo.svg" alt=""></div>
            <div class="co-popup-activitypub-header">
                <h1>C'est fantastique !!</h1>
                <p>Désormais, <span>Communecter</span> fait partie du <a href="#">Fediverse</a> en implémentant le protocole <a href="#">ActivityPub</a>.</p>
            </div>
            <div>
                <ul class="co-popup-activitypub-actions">
                    <li><a href="javascript:;" class="ap-starter-btn-start-guide"><span><i class="fa fa-paper-plane-o" aria-hidden="true"></i></span> Commencer</a></li>
                    <li><a href="javascript:;" class="ap-starter-btn-close">Je ferai plus tard</a></li>
                    <li><a href="javascript:;" class="btn btn-light ap-starter-btn-finished">Ne me demande plus</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<script>
    $(function() {
        $.getScript("<?php echo $this->module->assetsUrl; ?>/js/particles.min.js", function(){
            particlesJS('co-popup-activitypub-particle', {
                "particles": {
                "number": {
                    "value": 100,
                    "density": {
                    "enable": true,
                    "value_area": 800
                    }
                },
                "color": {
                    "value": "#9fbd38"
                },
                "shape": {
                    "type": "circle",
                    "stroke": {
                    "width": 0,
                    "color": "#36c3eb"
                    },
                    "polygon": {
                    "nb_sides": 5
                    },
                    "image": {
                    "width": 100,
                    "height": 100
                    }
                },
                "opacity": {
                    "value": 0.5,
                    "random": false,
                    "anim": {
                    "enable": false,
                    "speed": 1,
                    "opacity_min": 0.1,
                    "sync": false
                    }
                },
                "size": {
                    "value": 5,
                    "random": true,
                    "anim": {
                    "enable": false,
                    "speed": 40,
                    "size_min": 0.1,
                    "sync": false
                    }
                },
                "line_linked": {
                    "enable": true,
                    "distance": 150,
                    "color": "#3f4e58",
                    "opacity": 0.4,
                    "width": 1
                },
                "move": {
                    "enable": true,
                    "speed": 4,
                    "direction": "none",
                    "random": false,
                    "straight": false,
                    "out_mode": "out",
                    "attract": {
                    "enable": false,
                    "rotateX": 600,
                    "rotateY": 1200
                    }
                }
                },
                "interactivity": {
                "detect_on": "canvas",
                "events": {
                    "onhover": {
                    "enable": true,
                    "mode": "repulse"
                    },
                    "onclick": {
                    "enable": true,
                    "mode": "push"
                    },
                    "resize": true
                },
                "modes": {
                    "grab": {
                    "distance": 400,
                    "line_linked": {
                        "opacity": 1
                    }
                    },
                    "bubble": {
                    "distance": 400,
                    "size": 40,
                    "duration": 2,
                    "opacity": 8,
                    "speed": 3
                    },
                    "repulse": {
                    "distance": 200
                    },
                    "push": {
                    "particles_nb": 4
                    },
                    "remove": {
                    "particles_nb": 2
                    }
                }
                },
                "retina_detect": true,
                "config_demo": {
                "hide_card": false,
                "background_color": "#b61924",
                "background_image": "",
                "background_position": "50% 50%",
                "background_repeat": "no-repeat",
                "background_size": "cover"
                }
            }
            );

        });
    })
</script>