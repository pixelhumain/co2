<?php
$cssAnsScriptFilesModule = array(
    '/plugins/jquery-simplePagination/jquery.simplePagination.js',
	'/plugins/jquery-simplePagination/simplePagination.css'
);
HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, Yii::app()->getRequest()->getBaseUrl(true));
$cssJS = array(
    '/js/dataHelpers.js',
    //'/js/default/directory.js'
);
HtmlHelper::registerCssAndScriptsFiles($cssJS, Yii::app()->getModule( Yii::app()->params["module"]["parent"] )->getAssetsUrl() );

$cssJS = array(
    '/plugins/jquery.dynForm.js',
    '/plugins/select2/select2.min.js' , 
);
HtmlHelper::registerCssAndScriptsFiles($cssJS, Yii::app()->request->baseUrl);

   /* $layoutPath = 'webroot.themes.'.Yii::app()->theme->name.'.views.layouts.';
$me = isset(Yii::app()->session['userId']) ? Person::getById(Yii::app()->session['userId']) : null;

echo $this->renderPartial( $layoutPath.'modals.'.Yii::app()->params["CO2DomainName"].'.mainMenu', array("me"=>$me) );*/
?>

<style type="text/css">
	.round{
		border-radius: 100%;
		width: 250px;
		height: 250px;
		padding-top: 70px;
		border-color: #333;
 	}
</style>
<div class="panel panel-white col-lg-offset-1 col-lg-10 col-xs-12 no-padding">
	<div class="col-md-12 col-sm-12 col-xs-12 text-center">
		<h1><?php echo "Liste des membres "?> <!-- <a href="<?php //echo Yii::app()->getRequest()->getBaseUrl(true) ?>/survey/co/index/id/<?php //echo $form["id"] ?>"><i class="fa fa-arrow-circle-right"></i></a> --> </h1>
		<br/>
		<div id="" class="" style="width:80%;  display: -webkit-inline-box;">
	    	<input type="text" class="form-control" id="input-search-table" 
					placeholder="Rechercher une information dans le tableau">
	    </div>
    </div>
	<div class="pageTable col-md-12 col-sm-12 col-xs-12 padding-20 text-center"></div>
	<div class="panel-body">
		<div>
			<?php if(Authorisation::isInterfaceAdmin()){ ?>

				<a href="#element.invite.type.<?php echo $type ?>.id.<?php echo $id ?>" class="btn btn-success btn-xs pull-right margin-10 lbhp"><i class="fa fa-user-plus"></i> Ajouter des membres et autres</a>
			<?php } ?>
			<table class="table table-striped table-bordered table-hover  directoryTable" id="panelAdmin">
				<thead>
					<tr>
						<th>Nom</th>
						<th>Type</th>
						<th>Email</th>
						<!--<th>Identifiant</th>-->
						<th>Roles</th>
						<th>Organisation</th>
						<th>Admin</th>
						<th>Statut</th>
						<?php if(Authorisation::isInterfaceAdmin()){ ?> 
							<th>Action</th>
						<?php } ?>
					</tr>
				</thead>
				<tbody class="directoryLines">
				</tbody>
			</table>
			
		</div>
	</div>
	<div class="pageTable col-md-12 col-sm-12 col-xs-12 padding-20"></div>
</div>

<script type="text/javascript">

	var contextData = {id : "<?php echo $id ?>", type : "<?php echo $type ?>", connectTo : "<?php echo $connectTo ?>"};
	var community =<?php echo json_encode($results); ?>;

	var allOrgaMmber = [];

	var isInterfaceAdmin=<?php echo json_encode(Authorisation::isInterfaceAdmin()) ?>; 				
	jQuery(document).ready(function() {
		coInterface.bindLBHLinks();
		//bindAnwserList();
		//var rolesList=
		if(typeof community != "undefined"){
			initViewTableCommunity(community);
		}

		$(".disconnectConnection").click(function(){
			var $this=$(this); 
			disconnectTo(	contextData.type,
							contextData.id, 
							$this.data("id"),
							$this.data("type"), 
							$this.data("connection"),
							function() { $("#lineMember"+$this.data("id")).fadeOut(); });
		});

		$(".acceptAsBtn").off().on("click",function () {
			links.validate(contextData.type, contextData.id, $(this).data("id"), $(this).data("type"), $(this).data("connect-validation"));
		});


		
	    $("#input-search-table").on("keyup", function() {
		    var value = $(this).val().toLowerCase();
		    $("#panelAdmin tr.line").filter( function() {
				$(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
				//countLine();
		    });
		});

	   
	});

	// function countLine(){
	// 	var nbLine = $("#panelAdmin tr.line").filter(function() {
	// 			    return $(this).css('display') !== 'none';
	// 			}).length ;
	// 	$("#nbLine").html(nbLine);
	// }


	function startAdminSearch(initPage){
		//$("#second-search-bar").val(search);
	    $('#panelAdmin .directoryLines').html("Recherche en cours. Merci de patienter quelques instants...");
	    var data = {
	    	//ifForm : form._id.$id,
	    	text : $("#input-search-table").val(),
	    }
	    ajaxPost(
	        null,
	        baseUrl+"/"+moduleId+"/co/searchadminmembers/",
	        searchAdmin,
	        function(data){ 
	             initViewTable(data);
	        },
	        function(xhr, status, error){
	            $("#searchResults").html("erreur");
	        }
	    );
	}

	function initViewTableCommunity(data){
		
		$('#panelAdmin .directoryLines').html("");
		
		$.each(data, function(key, value){
			entry=buildDirectoryLine(key, value);
			
			$("#panelAdmin .directoryLines").append(entry);
			bindMembers();
		});
		//bindAnwserList();
	}

	function bindMembers(){
		mylog.log("bindMembers");
		$(".removeAdmin").off().click(function(e){
			var params = {
		    	parentId : contextData.id,
		    	parentType : contextData.type,
		   		childId : $(this).data("id"),
				childType : $(this).data("type"),
				connect : contextData.connectTo,
				isAdmin : false,
			};
			ajaxPost(
		        null,
		        baseUrl+"/"+moduleId+"/link/updateadminlink/",
		        params,
		        function(data){ 
			        $("#admin"+params.childId+params.childType).html("Non");
			        $("#removeAdmin"+params.childId).remove();
			        var action = '<li id="addAdmin'+key+'"><a href="javascript:;" data-id="'+params.childId+'" data-type="'+params.childType+'" class="margin-right-5 addAdmin"><span class="fa-stack"><i class="fa fa-user fa-stack-1x"></i><i class="fa fa-check fa-stack-2x stack-right-bottom text-danger"></i></span>Ajouter en tant que admin</a></li>' ;
			        $("#actionMenu"+params.childId).append(action);
		        },
		        function(xhr, status, error){
		            $("#searchResults").html("erreur");
		        }
		    );
	    });

	    $(".addAdmin").off().click(function(e){
			var params = {
		    	parentId : contextData.id,
		    	parentType : contextData.type,
		   		childId : $(this).data("id"),
				childType : $(this).data("type"),
				connect : contextData.connectTo,
				isAdmin : true,
			};
			ajaxPost(
		        null,
		        baseUrl+"/"+moduleId+"/link/updateadminlink/",
		        params,
		        function(data){ 
			        $("#admin"+params.childId+params.childType).html("Oui");
		        },
		        function(xhr, status, error){
		            $("#searchResults").html("erreur");
		        }
		    );
	    });


	    $(".updateRoles").off().click(function(e){
			var id = $(this).data("id");
			var name = $(this).data("name");
			var type = $(this).data("type");
			mylog.log("updateRoles", id, type, name);
			//if( typeof form.links.members[id] != "undefined" ){

			var roles = ( ( typeof community[id].roles != "undefined" ) ? community[id].roles : [] ) ;
			var linkType = ( typeof  links.linksTypes  != "undefined" && 
							 typeof  links.linksTypes[contextData.type]  != "undefined" &&
							 typeof  links.linksTypes[contextData.type][type]  != "undefined" ) ? links.linksTypes[contextData.type][type] : "members" ;
			updateRoles(id, type, name, linkType, roles);
			//}

	    });

	    $(".connectToOrg").off().click(function(e){
			var id = $(this).data("id");
			var type = $(this).data("type");
			var name = $(this).data("name");
			var email = $(this).data("email");
			mylog.log("connectToOrg", id, type, name);


			//if( typeof form.links.members[id] != "undefined" ){

				
				connectToOrg(id, type, name, email);
			//}

	    });
	}

	function updateRoles(childId, childType, childName, connectType, roles) {
		mylog.log("network community updateRoles", rolesList);
		var formRole = {
				saveUrl : baseUrl+"/"+moduleId+"/link/removerole/",
				dynForm : {
					jsonSchema : {
						title : tradDynForm.modifyoraddroles+"<br/>"+childName,// trad["Update network"],
						icon : "fa-key",
						onLoads : {
							sub : function(){
								//bindDesc("#ajaxFormModal");
							}
						},
						beforeSave : function(){
							mylog.log("beforeSave");
					    	//removeFieldUpdateDynForm(contextData.type);
					    	var r = [];
					    	$('#s2id_roles li').each(function(i,li){ 
					    		
					    		if(typeof $(li).children("div").html()!="undefined") 
									r.push($(li).children("div").html()) 
								})
					    	$("#roles").val( r.join() ) ;
					    },
						afterSave : function(data){
							mylog.log("afterSave",data);
							dyFObj.closeForm();
							//loadDataDirectory(connectType, "user", true);
							community[data.memberid].roles = data.roles ;
							var str = "";
							if( typeof data.roles != "undefined") {
								$.each(data.roles, function(kR, vR){
									str += vR+" <br/>";
								});
							}
							mylog.log("afterSave", "#role"+childId+childType, str);
							$("#role"+childId+childType).html(str);
							//changeHiddenFields();
						},
						properties : {
							contextId : dyFInputs.inputHidden(),
							contextType : dyFInputs.inputHidden(), 
							roles : dyFInputs.tags(rolesList, tradDynForm["addroles"] , tradDynForm["addroles"], 0),
							childId : dyFInputs.inputHidden(), 
							childType : dyFInputs.inputHidden(),
							connectType : dyFInputs.inputHidden()
						}
					}
				}
			};

			var dataUpdate = {
		        contextId : contextData.id,
		        contextType : contextData.type,
		        childId : childId,
		        childType : childType,
		        connectType : connectType,
			};

			if(notEmpty(roles))
				dataUpdate.roles = roles;
			dyFObj.openForm(formRole, "sub", dataUpdate);		
	}

	function buildDirectoryLine(key, value){
		
		actions = "";
		str = '<tr id="lineMember'+key+'" class="line">';
			str += '<td>'+value.name+'</td>';
			str += '<td>'+value.type+'</td>';
			str += '<td>'+((typeof value.email != "undefined") ? value.email : "<i>Non renseigné</i>")+'</td>';
			//str += '<td>'+key+'</td>';
			
			//if(typeof form.links != "undefined" && typeof form.links.members != "undefined"
			//	&& typeof form.links.members[key] != "undefined"){
				actions += '<li><a href="javascript:;" data-id="'+key+'" data-type="'+value.type+'" data-name="'+value.name+'" class="margin-right-5 updateRoles"><span class="fa-stack"><i class="fa fa-user fa-stack-1x"></i><i class="fa fa-check fa-stack-2x stack-right-bottom text-danger"></i></span>Modifier les roles</a></li>';

				str += '<td id="role'+key+value.type+'">';
				if( typeof value.roles != "undefined") {
					$.each(value.roles, function(kR, vR){
						str += vR+"<br/>";
					});
				}
				str += '</td>';

				str += '<td>';

				var allCommunity = <?php echo json_encode($results); ?>;

				var allOrgaMember = {};

				$.each(allCommunity, function(kCom, vCom){
					if (vCom.type == "organizations") {
						allOrgaMember[kCom] = vCom.name;
					}
				});

				$.each(allOrgaMember, function(kL, vL){
					if( typeof value.links != "undefined" && typeof value.links.memberOf != "undefined" && typeof value.links.memberOf[kL] != "undefined") {str += vL+"<br/>";
					}
				});

				str += '</td>';
				actions += '<li id="connectToOrg'+key+'"><a href="javascript:;" data-id="'+key+'" data-name="'+value.name+'" data-email="'+value.email+'" data-type="'+value.type+'" class="margin-right-5 connectToOrg"><span class="fa-stack"><i class="fa fa-users fa-stack-1x"></i><i class="fa fa-check fa-stack-2x stack-right-bottom text-danger"></i></span>Ajouter dans une Organisation</a></li>';
				str += '<td id="admin'+key+value.type+'">';
				if( value.type == "<?php echo Person::COLLECTION ; ?>"){
					if( typeof value.isAdmin != "undefined" && 
						value.isAdmin == true) {
						str += " Oui ";
						actions += '<li id="removeAdmin'+key+'"><a href="javascript:;" data-id="'+key+'" data-type="'+value.type+'" class="margin-right-5 removeAdmin"><span class="fa-stack"><i class="fa fa-user-times"></i></i></span>Supprimer de l\'admin</a></li>';
					}else{
						str += " Non ";
						actions += '<li id="addAdmin'+key+'"><a href="javascript:;" data-id="'+key+'" data-type="'+value.type+'" class="margin-right-5 addAdmin"><span class="fa-stack"><i class="fa fa-user fa-stack-1x"></i><i class="fa fa-check fa-stack-2x stack-right-bottom text-danger"></i></span>Ajouter en tant que admin</a></li>';
					}
				}else{
					str += " ";
				}
				str += '</td>';
				str += '<td id="status'+key+value.type+'">';
				if( value.type == "<?php echo Person::COLLECTION ; ?>"){
					if( typeof value.toBeValidated != "undefined" && 
						value.toBeValidated == true) {
						str += " En attende de validation ";
						actions += '<li id="acceptAsBtn'+key+'"><a href="javascript:;" '+
									'data-id="'+key+'" data-type="'+value.type+'" '+
									'data-connect-validation="toBeValidated"'+
									'data-connection="members"'+
									'class="margin-right-5 acceptAsBtn"><span class="fa-stack"><i class="fa fa-user-times"></i></i></span>Valider</a></li>';

						// html +="<button class='btn btn-default btn-xs acceptAsBtn'"+ 
					 //            " data-type='"+data.type+"' data-id='"+data.id+"' data-connect-validation='toBeValidated' data-parent-hide='2'"+
					 //            " style='bottom:"+(30*countBtn)+"px'>"+
					 //            "<i class='fa fa-user'></i> "+trad["acceptas"+data.edit]+
					 //          "</button> ";
					}else{
						str += " Membre ";
					}
				}else{
					str += " ";
				}
				str += '</td>';
				if(isInterfaceAdmin){
				str += '<td class="center">';

				actions += '<li><a href="javascript:;" data-id="'+key+'" data-type="'+value.type+'" data-name="'+value.name+'" data-connection="members" data-parent-hide="'+2+'" class="margin-right-5 disconnectConnection"><span class=""><i class="fa fa-trash"></i></i></span>Supprimer le lien</a></li>';


					str += '<div class="btn-group">'+
								'<a href="#" data-toggle="dropdown" class="btn btn-danger dropdown-toggle btn-sm"><i class="fa fa-cog"></i> <span class="caret"></span></a>'+
								'<ul class="dropdown-menu pull-right dropdown-dark" role="menu">'+
									actions+
								'</ul></div>';
				str += '</td>';
				}
			str += '</td>';
		str += '</tr>';
		return str;
	}

	function connectToOrg(childId, childType, childName, childEmail){
		// mylog.log("updateRoles", rolesList);
		var dataSearch={filters:{}};
		dataSearch.filters["links.members."+contextData.id]= {'$exists': 1};

		var dataUpdate = {
		       contextId : contextData.id,
		        contextType : contextData.type,
		        childId : childId,
		        childType : childType,
		        childName : childName,
		        childEmail : childEmail,
		        connectType : "members"
		};
		
		var formCTo = {
				saveUrl : baseUrl+"/"+moduleId+"/link/linkchildparent/",
				dynForm : {
					jsonSchema : {
						// title : tradDynForm.modifyoraddroles+"<br/>"+childName,// trad["Update network"],
						title : "Associer à une Organisation",
						icon : "fa-key",
						afterSave : function(data){

							//mylog.log("updateRoles", id, type, name);

							var roles = ( ( typeof community[childId].roles != "undefined" ) ? community[childId].roles : [] ) ;

							$.each(Object.keys(finder.object.parent), function(comK, comV){
								// $.extend(roles,community[comV]["roles"]);
								if(roles.indexOf(community[comV]["roles"]) === -1){
							        roles.push(community[comV]["roles"]);
							    }
							});

							mylog.log("azeee",finder);

							var updRoleData = new Object();

							updRoleData["roles"] = roles.toString();
							updRoleData["contextId"] = contextData.id;
							updRoleData["contextType"] =  contextData.type;
							
							updRoleData["childId"] =  childId;
							updRoleData["childType"] = childType;
							updRoleData["connectType"] = "members";

							$("#role"+childId+childType).html(roles.toString());


							ajaxPost(
						        "",
						        baseUrl+"/"+moduleId+"/link/removerole",
						        updRoleData,
						        function (data) {
						         }, "html"
					        );

							dyFObj.closeForm();
							urlCtrl.loadByHash(location.hash);


							//loadDataDirectory(connectType, "user", true);
							
							//changeHiddenFields();
						},
						properties : {
							contextId : dyFInputs.inputHidden(),
							contextType : dyFInputs.inputHidden(), 
							childId : dyFInputs.inputHidden(), 
							childType : dyFInputs.inputHidden(),
							childName : dyFInputs.inputHidden(),
							childEmail : dyFInputs.inputHidden(),
							connectType : dyFInputs.inputHidden(),
							organizations : dyFInputs.inputHidden(),
							parent : {
								inputType : "finder",
		                        label : "Organisation",
		                        //multiple : true,
					           	initType: ["organizations"],
				    			multiple : true,
				    			search : dataSearch,
				    			initBySearch : true,
				    			initMe:false,
		                        initContext:false,
		                        initContacts:false
		                    }
						}
					}
				}
			};

			dyFObj.openForm(formCTo, "sub", dataUpdate);
	}

	function disconnectTo(parentType,parentId,childId,childType,connectType, callback, linkOption, msg) { 
		var messageBox = (notNull(msg)) ? msg : trad["removeconnection"+connectType];
		$(".disconnectBtnIcon").removeClass("fa-unlink").addClass("fa-spinner fa-spin");
		var formData = {
			"childId" : childId,
			"childType" : childType, 
			"parentType" : parentType,
			"parentId" : parentId,
			"connectType" : connectType,
		};
		if(typeof linkOption != "undefined" && linkOption)
			formData.linkOption=linkOption;
		bootbox.dialog({
	        onEscape: function() {
	            $(".disconnectBtnIcon").removeClass("fa-spinner fa-spin").addClass("fa-unlink");
	        },
	        message: '<div class="row">  ' +
	            '<div class="col-md-12"> ' +
	            '<span>'+messageBox+' ?</span> ' +
	            '</div></div>',
	        buttons: {
	            success: {
	                label: "Ok",
	                className: "btn-primary",
	                callback: function () {
	                	ajaxPost(
					        null,
					        baseUrl+"/"+moduleId+"/link/disconnect",
					        formData,
					        function(data){ 
						        if ( data && data.result ) {
									typeConnect=(formData.parentType==  "citoyens") ? "people" : formData.parentType;
									idConnect=formData.parentId;
									if(formData.parentId==userId){
										typeConnect=(formData.childType==  "citoyens") ? "people" : formData.childType;
										idConnect=formData.childId;
									
									}
									// if(typeof removeFloopEntity() != "undefined")
									// 	removeFloopEntity(idConnect, typeConnect);
									toastr.success("Le lien a été supprimé avec succès");
									if (typeof callback == "function") 
										callback();
									else
										urlCtrl.loadByHash(location.hash);
								} else {
								   toastr.error("You leave succesfully");
								}
					        }
					    );
	                }
	            },
	            cancel: {
	            	label: trad["cancel"],
	            	className: "btn-secondary",
	            	callback: function() {
	            		$(".disconnectBtnIcon").removeClass("fa-spinner fa-spin").addClass("fa-unlink");
	            	}
	            }
	        }
	    });      
	};
</script> 

