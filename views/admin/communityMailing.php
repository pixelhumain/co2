<?php 
	$emailList = array();
	$members = array();

	if(!isset($id)){
		$id = $this->costum["contextId"];
	}

	if(!isset($type)){
		$type = $this->costum["contextType"];
	}

	//if($this->costum["contextType"]==$type){
	$linkPath = "links.memberOf.".$id;
	
	if($type!="organizations"){
		$linkPath = "links.$type.".$id;
	}

	$members = PHDB::find(
		Citoyen::COLLECTION,
		array('$or' => array(
			array("links.projects.".$id => ['$exists'=>1]),
			array("links.memberOf.".$id => ['$exists'=>1]),
			array("links.events.".$id => ['$exists'=>1]),
		)),
		["name", "email", "links.events.".$id, "links.projects.".$id, "links.memberOf.".$id]
	);

	foreach ($members as $key => $value) {
		$mail = $value["email"];
		$receiveMailStatus = true;

		if($type=="organizations"){
			$type = "memberOf";
		}

		if(isset($value["links"][$type][$id]["isAdmin"]) && isset($value["links"][$type][$id]["roles"])){
			$roles = $value["links"][$type][$id]["roles"];
			array_push($roles, "Administrateur");
		}else if(isset($value["links"][$type][$id]["isAdmin"])){
			$roles = ["Administrateur"];
		}else if(isset($value["links"][$type][$id]["roles"])){
			$roles = $value["links"][$type][$id]["roles"];
		}else{
			$roles = ["Autres membres"];
		}

		if(isset($value["links"][$type][$id]["receiveMail"]["status"])){
			if($value["links"][$type][$id]["receiveMail"]["status"]=="false"){
				$receiveMailStatus = false;
			}
		}

		array_push($emailList, [$mail, $roles, $receiveMailStatus, $value["name"]]);
	}
?>

<style>
	#mailing{
		margin-top: 55px;
	}
	#mailing .form-control{
		box-shadow: none !important;
		outline: none !important;
		background: #fefefe;
		padding: 20px 10px;
	}

	#mailing textarea.form-control{
		padding: 10px 10px !important;
	}

	#txtMsg, select.form-control{
		height: 100px !important
	}

	#mailing h3{
		margin-bottom: 2%;
		text-align: center;
		color: #ACCD5C;
	}
	#mailing .icon-title{
		font-size: 48pt;
		color: #1E56A2;
	}
	#mailing option{
		padding: 0.4em 0.2em;
	}
	#mailing select{
		padding: 0.2em 0em !important;
	}
	#mailing .contact-image{
		text-align: center;
	}
	#mailing .btnSend{
       	-webkit-transition: 0.5s ease;
       	-moz-transition: 0.5s;
       	-ms-transition: 0.5s;
       	-o-transition: 0.5s;
       	transition: 0.5s;
       	width: 50%;
       	cursor: pointer;
       	text-transform: uppercase;
		border-radius: 5rem;
		padding: 0.6em 0.6em 0.5em 0.6em;
		background: #FFF;
		border: 3px solid #1E56A2;
		font-weight: 800;
		color: #1E56A2;
	}
	#mailing .btnSend:hover {
        background:#1E56A2;
        color:white;
    }
	#mailing .btnSend:focus{
		outline: none;
	}

	#listEmailGrid{
		max-height: 200px;
		overflow-y: auto;
		display: flex;
		flex-wrap: wrap;
	}
	
	.email-badge{
		border-radius: 20px;
		padding: 1px 8px;
		margin: 2px;
		font-weight: bold;
	}

	#mailing {
        background-color: rgb(0,0,0); 
        background-color: rgba(0,0,0,0.8); 
        backdrop-filter: blur(2px);
        -webkit-backdrop-filter: blur(2px);
    }
</style>

<!-- Modal Mailing-->
<div id="mailing" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="contact-image">
				<br/>
				<span class="fa fa-envelope icon-title"></span>
			</div>
			<div>
				<h3>COMMUNAUTÉ MAILING</h3>
				<!--p class="text-center">Séléctionner le destinataire(s) de votre email.</p -->
			</div>
	
			<div class="row">
				<div class="col-md-10 col-md-offset-1">
					<div class="form-group checkbox">
    					<label>
							<input id="mailType" name="mailType" value="invitation" type="checkbox" />	Email d'invitation à rejoindre la communauté
    					</label>
					</div>
					<div class="row form-group">
						<div class="col-md-6 recipientSelect">
							<div class="form-group">
								<select multiple name="txtRole" id="txtRole" class="form-control">
									<option value="">Tout le Membres</option>
								</select>
							</div>
						</div>
						<div class="col-md-6 recipientSelect">
							<div class="form-group">
								<select multiple name="txtTo" id="txtTo" class="form-control">
								</select>
								<small>Maintenir Ctrl pour selection multiple</small>
							</div>
						</div>
						<div class="col-md-12 recipientTextArea">
							<textarea id="txtTo" name="txtTo" class="form-control margin-bottom-5 activeMarkdown" placeholder="Copier la liste des e-mails ici" rows="2" required></textarea>
							<div id="listEmailGrid" class=""></div>
						</div>
					</div>	
					<div class="form-group">
						<input id="txtObj" name="txtObj" class="form-control activeMarkdown" placeholder="Objet du mail *" required />
					</div>
					<div class="form-group emailContent">
						<textarea id="txtMsg" name="txtMsg" markdown="true" class="form-control activeMarkdown" placeholder="Votre Message *" rows="5" required></textarea>
					</div>
					<div class="form-group text-center">
						<button id="btnSend" class="btnSend" data-dismiss="modal">Envoyer</button>
						<!--button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button-->
					</div>
				</div>
			</div>
		</div>
	</div>
</div>



<script type="text/javascript">
	
jQuery(document).ready(function() {
	$(".headerSearchright").prepend('<a data-toggle="modal" data-target="#mailing" class="btn btn-primary"><i class="fa fa-envelope"></i> Envoyer un mail</a>&nbsp; &nbsp;');

	if(costum.htmlConstruct.adminPanel.menu.community.recipientTextArea){
		$(".recipientSelect").remove();
	}else{
		$(".recipientTextArea").remove()
	}

	// Mailing
	var emailMembers = <?php echo (isset($emailList) ? json_encode($emailList) : "[]" ) ?>;
	var rolesMembers = emailMembers.map(function(v){return v[1]});

	var emailList = emailMembers.map(function(v){return v[0]});
	
	var selectedMailByRole = [];
	var listInvitation = {};


	for (let index = 0; index < rolesMembers.length; index++){
		for(let j = 0; j < rolesMembers[index].length; j++){
			var optionExists = ($("#txtRole option[value='"+rolesMembers[index][j].trim()+"']").length > 0);
			if(!optionExists && rolesMembers[index][j]!=""){
				var opt = new Option(rolesMembers[index][j], rolesMembers[index][j]);
				$(opt).html(rolesMembers[index][j]);
				$("#txtRole").append(opt);
			}
		}
	}

	$("#mailType").on("change", function(){
		$(".emailContent").toggle();
	})

	$("#txtRole").on("change", function(){
		var selectedMembers;
		selectedMembers = emailMembers.filter((member)=>{
			var ok = false;
			for (var i = $(this).val().length - 1; i >= 0; i--) {
				if(member[2] && $(this).val()[i]==""){
					ok = true;
					break;
				}

				if(member[2] && member[1].includes($(this).val()[i])){
					ok = true;
					break;
				}
			}
			return ok;
		});
		
		$("#txtTo").empty();
		for(let index = 0; index < selectedMembers.length; index++){
			var element =  selectedMembers[index];
			$("#txtTo").append('<option value="'+element[0]+'" selected="'+element[2]+'">'+element[3]+'</option>');
		}

		selectedMailByRole = selectedMembers.map(function(ar){return ar[0]});
	});

	/*$("#txtTo").on("change", function(){
		selectedMailByRole = $(this).val();
	});*/

	$("#txtTo").on("input", function(){
		let input = $(this).val().replaceAll('"', "");
		let emails = input.split(/[\s,;+/]+/).filter( e => e.trim() !== "");
		let validEmails = [];
		let invalidEmails = [];
		listInvitation = {};
		$.each(emails, function(index, email) {
			if(valideEmail(email)) {
				validEmails.push(email);
				selectedMailByRole = validEmails.join(",");
				listInvitation["listInvitation."+slugify(email, email)] = {
					"email": email, 
					"status": "Non lu"
				}
			} else {
				invalidEmails.push(email);
			}
		});
		displayEmails(validEmails, invalidEmails);
	})

	function valideEmail(email) {
		var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		return re.test(email);
	}

	function displayEmails(validEmails, invalidEmails) {
		let validEmailsStr = validEmails.join(", ");
		let invalidEmailsStr = invalidEmails.join(", ");
		let html = "";
		$.each(validEmails, function(index, email) {
			html += '<span class="email-badge bg-success">' + email + '</span>';
		});

		$.each(invalidEmails, function(index, email) {
			html += '<span class="email-badge bg-danger invalid" onclick="boldText(this)">' + email + '</span>';
		});

		$("#listEmailGrid").html(html);
	}

	$(".email-badge.invalid").on("click", function(){
		//alert("here we go");
		let emailToEdit = $(this).text();
		let currentEmails = $("#txtTo").val();
		let updatedEmails = currentEmails.split(/[\s,;+/]+/).filter( e => e.trim() !== emailToEdit);
		$("#txtTo").val(updatedEmails+ (updatedEmails?', ': "") + emailToEdit).focus();
	});
	
	function boldText(el) {
		//alert("here we go"+JSON.stringify($(el).attr("class")));
		var start = txtarea.selectionStart;
		var end = txtarea.selectionEnd;
		var sel = txtarea.value.substring(start, end);
		var finText = txtarea.value.substring(0, start) + '[b]' + sel + '[/b]' + txtarea.value.substring(end);
		txtarea.value = finText;
		txtarea.focus();
	}

	// Send email
	$("#btnSend").on("click", function(){
			var isValid = false;
			var mailType = "basic";

			if (($("#txtMsg").val() != "" || ($("#txtMsg").val() == "" && $("#mailType").val()=="invitation")) && selectedMailByRole.length!=0 && $("#txtObj").val()!=""){
				isValid = true;
			}

			if($("#mailType").val()=="invitation"){
				mailType = "invitation";
			}

			if(isValid){
				var paramsmail = {
					tpl : mailType,
					tplObject : $("#txtObj").val(),
					tplMail : selectedMailByRole,
					html:"<p style='white-space:pre-line'>"+$("#txtMsg").val()+"</p><br>",
					btnRedirect : {
						hash : "#email-feedback",
						label : "cliquer ici si vous ne voulez plus recevoir de mail de cette organisation."
					},
					language: costum.language||"fr"
				};

				if(mailType == "invitation"){
					paramsmail.target = {
						type:costum.contextType,
						id:costum.contextId,
						name: costum.title
					}
					paramsmail.invitorId = userId;
					paramsmail.invitorName = userConnected.name;
					paramsmail.urlRedirect = baseUrl+"/costum/co/index/slug/"+costum.contextSlug;
					paramsmail.urlValidation = baseUrl+"/costum/co/index/slug/"+costum.contextSlug;
				}

				if (notNull(costum) && exists(costum.admin) && exists(costum.admin.email) && costum.admin.email != ""){
					paramsmail.replyTo = costum.admin.email;
    			}

				ajaxPost(
					null,
					baseUrl+"/co2/mailmanagement/createandsend",
					paramsmail,
					function(data){ 
						$("#txtMsg").val("");
						toastr.success("Votre mail a été bien envoyé");
						dataHelper.path2Value(
							{
								id:costum.contextId,
								collection:costum.contextType,
								path:"allToRoot",
								value:listInvitation,
								// updateCache : true
							}, function(){});
					},
					function(data){
						toastr.error("Une problème s'est produite, envoie du mail est intérrompu");
					}
				);
			}else{
				toastr.warning("Votre email n'est pas envoyé. le mail n'a pas d'objet ou aucun déstinataire séléctionné ou vous n'avez pas écrit de message. Veuillez vérifier les champs et réessayer.");
			}
	});

	$("#txtMsg").on("change", function(){
		if($(this).val()!=""){
			$("#btnSend").attr("disabled", false);
		}else if(mailType!="invitation"){
			$("#btnSend").attr("disabled", true);
		}
	});

	
});
</script>
