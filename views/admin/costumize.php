<?php 
    $costumizeJSON = file_get_contents("../../modules/costum/data/costumize.json", FILE_USE_INCLUDE_PATH);
	$defaultsConf = file_get_contents("../../modules/costum/data/defaultCms/defaultCostumize.json", FILE_USE_INCLUDE_PATH);
 ?>
<script>
	var costumizeJson = JSON.parse(<?php echo json_encode($costumizeJSON) ?>);
	var defaultConfig = JSON.parse(<?php echo json_encode($defaultsConf) ?>);
	var hasCostumJson = false;
	defaultConfig = {
        slug : "costumize",
        ...defaultConfig
    };
	<?php 
		$resultQuery = Costum::getBySlug("costumize");
		if(!empty($resultQuery)){
	 ?>
	 hasCostumJson = true;
	<?php } ?>

	 // $(".costumize").on("click",function() {
	 // 	var costumizeSlug = $(this).data("costumize-slug");
	 // 	costumize(costumizeSlug);
	 // });

	function costumize(costumizeSlug=null,defaultCms=null,options={},costumConfig={}){
	  if(exists(contextData) && exists(contextData.costum) && exists(contextData.costum.slug)){
		if(exists(options.withbootbox) && options.withbootbox ){
			if(exists(options.type) && options.type=="aap")
				customizeCostum(defaultConfig,defaultCms,options);
			location.reload();
			
		}else
			bootbox.confirm({
				title: "<h5 class='text-center letter-green-k'><i><u>"+contextData.name+"</u></i> <?php echo Yii::t('common', 'already has a costum') ?> </h5>",
				message: "<?php echo Yii::t('common', 'You will be redirected to the COstumization interface of') ?> <i><u>"+contextData.name+"</u></i> !",
				buttons: {
					cancel: {
						label: '<i class="fa fa-times"></i> '+trad.cancel
					},
					confirm: {
						label: '<i class="fa fa-check"></i> '+trad.yes
					}
				},
				callback: function (result) {
					if(result) {
						if (userConnected != null && contextData.role == "admin")
							window.location.href = baseUrl+'/costum/co/index/slug/'+contextData.slug+'/edit/true';
						else
							window.location.href = baseUrl+'/costum/co/index/slug/'+contextData.slug
					}
				}
			});  
	    }else{
	    	if(Object.keys(costumConfig).length>0 ){
	    		mylog.log("costumConfig",costumConfig);
	    		customizeCostum(costumConfig,defaultCms,options);
	    	}
	  		else if(costumizeSlug){
		  		ajaxPost(
			        null,
			        baseUrl+"/costum/costumgenerique/getJson",
			        {slug:costumizeSlug},
			        function(data){ 
			          	mylog.log("jsonFile",data);
			          	costumizeJson=data;
			  		}

	  			);	  		
	  		}
	  		else{
 				customizeCostum(defaultConfig,defaultCms,options);
 			}	
	  	}
	}

	function customParams(){
		var sectionDyf={};
		sectionDyf.colorParams = {
	        "jsonSchema" : {    
	            "title" : "Couleurs principales",
	            "icon" : "fa-cog",
	            "properties" : {
	                "main1" : {
	                    label : "Couleur principale",
	                    inputType : "colorpicker"
	                },
	                "main2" : {
	                    label : "Couleur secondaire",
	                    inputType : "colorpicker"
	                }
	            },
	            save : function (data) {
	            	 mylog.log("color",data);
	            	 tplCtx.value={};
	            	 $.each( sectionDyf.colorParams.jsonSchema.properties , function(k,val) {
                            tplCtx.value[k] = $("#"+k).val();
                        mylog.log("value",'.'+k+val.inputType,tplCtx.value[k]);
                    });
                    mylog.log("save color tplCtx",tplCtx);
	                if(typeof tplCtx.value == "undefined")
	                    toastr.error('value cannot be empty!');
	                else {
	                    dataHelper.path2Value( tplCtx, function(params) { 
	                        paramsCat();
	                    } );
	                }

	            }
	        }
    	};
		tplCtx = {};
		tplCtx.id = contextData.id;
		tplCtx.collection = contextData.collection;
		tplCtx.path = "costum.colors";
        dyFObj.openForm( sectionDyf.colorParams,null, {collection:"organizations"});
    }

    function paramsCat(){
		var sectionDyf={};
		sectionDyf.catParams = {
	        "jsonSchema" : {    
	            "title" : "Réseau thématique",
	            "icon" : "fa-cog",
	            "properties" : {
	            	"info" : {
						"inputType" : "custom",
						"html":"<p class='text-"+typeObj["reseau"].color+"'>"+
						//"Faire connaître votre Organisation n'a jamais été aussi simple !<br>" +
						"Si votre réseau de tiers-lieux regroupent des structures spécialisées dans une ou plusieures thématiques...<hr>" +
					 "</p>",
					},
	                "themes" : {
	                    "label" : "Thématiques de votre réseau",
	                    "inputType" : "select",
	                    "placeholder":"Choisir la(les) thématique(s)",
                        "list":"typePlace",
                        "groupOptions":false,
                        "groupSelected":false,
                        "optionsValueAsKey":true,
                        "select2":{
                           "multiple":true
                        }  
	                }
	            },
	            save : function (data) {
	            	 mylog.log("categorie",data);
						tplCtx.value={};
                        tplCtx.value = data.themes;
                        if(typeof tplCtx.value=="string"){
                        	tplCtx.value=tplCtx.value.split();
                        }
                        //var valLen=Object.keys(data.themes).length;
                        mylog.log("Themes to save",data.themes)
                        var urlTags=data.themes.toString();
                        mylog.log("tags url",urlTags);
	                if(typeof tplCtx.value == "undefined")
	                    toastr.error('value cannot be empty!');
	                else {
	                    tplCtx.updatePartial=true;
	                     //tplCtx.arrayForm = true;
	                    dataHelper.path2Value( tplCtx, function(params) { 
	                        $("#ajax-modal").modal('hide');
	                        toastr.success("Bien ajouté");
	                         window.location.href = baseUrl+'/costum/co/index/slug/'+contextData.slug+'#?tags='+urlTags;
	                    } );
	                }
	            }
	        }
    	};
		tplCtx = {};
		tplCtx.id = contextData.id;
		tplCtx.collection = contextData.collection;
		tplCtx.path = "tags";
        dyFObj.openForm(sectionDyf.catParams,null, null);

    }		

	function customizeCostum(defaultConfig,defaultCms,options=null){
		var tplCtx = {};
		var defaultCostumizeObj = {
			costum: defaultConfig
		};
		if(exists(options.type) && options.type=="aap")
			defaultCostumizeObj.costum.type = "aap";

		mylog.log("defaultCostumizeObj",defaultCostumizeObj);
		tplCtx.id = contextData.id;
		tplCtx.collection = contextData.collection;
		tplCtx.path ="allToRoot";
		tplCtx.value = defaultCostumizeObj;
		tplCtx.format = true;
		if(exists(options.withbootbox) && options.withbootbox==true){
			dataHelper.path2Value(tplCtx,function(response){
				mylog.log("valiny",response);
				$("#ajax-modal").modal('hide');
				if(response.result){
					if(notNull(defaultCms))
						addDefaultCms(defaultCms);
					else{
						toastr.success("Votre costum est créé");
						window.location.href = baseUrl+'/costum/co/index/slug/'+contextData.slug;
					}
				}else{

				}
			})
		} else {
			bootbox.confirm({
				title: "<h5 class='text-center letter-green-k'><i><u style='text-transform:capitalize'>'"+contextData.name+"'</u></i> <?php echo Yii::t('common', 'does not yet have a costum') ?> </h5>",
				message: "<h6 class='text-center'><?php echo Yii::t('common', 'Please create your costum !') ?></h6>",
				buttons: {
					cancel: {
						label: '<i class="fa fa-times"></i> '+trad.cancel
					},
					confirm: {
						label: '<i class="fa fa-check"></i> <?php echo Yii::t('common', 'Create') ?>'
					}
				},
				callback: function (result) {
					if(result){
						dataHelper.path2Value(tplCtx,function(response){
							mylog.log("valiny",response);
							$("#ajax-modal").modal('hide');
							if(response.result){
								if(notNull(defaultCms))
									addDefaultCms(defaultCms);
								else{
									toastr.success("Votre costum est créé");
									if (userConnected != null && contextData.role == "admin")
										window.location.href = baseUrl+'/costum/co/index/slug/'+contextData.slug+'/edit/true';
									else
										window.location.href = baseUrl+'/costum/co/index/slug/'+contextData.slug
								}
							}else{

							}
						})
					}
				}
			});
		}
	}

function getRandomString(length) {
    var chars = "ABCDEFabcdef0123456789";
    var randS = "";
    while(length > 0) {
        randS += chars.charAt(Math.floor(Math.random() * chars.length));
        length--;
    }
    return randS;
}

function addDefaultCms(params){
	ajaxPost(
		null,
		baseUrl+"/co2/cms/insert",
		params,
		function(data){
			if(data.result){
				toastr.success("bloc cms par défaut ajouté");
				toastr.success("Votre costum est créé");
			}
				
			else
				toastr.error("bloc cms par défaut non ajouté")
		}
	)
}
</script>