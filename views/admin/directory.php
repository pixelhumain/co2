
<div id="adminDirectory" class="col-xs-12 no-padding"></div>

<script type="text/javascript">
var contextElt = <?php echo json_encode(@$context)  ?>;
var panelAdmin = <?php echo json_encode($panelAdmin) ?>;
mylog.log("panelAdmin",panelAdmin);
var filterAdmin = {};
var paramsFilterAdmin= {};
jQuery(document).ready(function() {
	var paramsFilterAdmin= {
	 	container : "#filters-nav-admin",
	 	loadEvent: {
	 		default : "admin",
	 		options : {
	 			results : {},
				initType : panelAdmin.types,
				panelAdmin : panelAdmin
	 		}
	 	},
	 	results : {
	 		multiCols : false
	 	},
		header : {
			dom : ".headerSearchContainerAdmin",
			options : {
				left : {
					classes : 'col-xs-8 elipsis no-padding',
					group:{
						count : true
					}
				}
			}
		},
		urlData : baseUrl+"/co2/search/globalautocompleteadmin"
	};
	if(notNull(contextElt) && notEmpty(contextElt)){
		paramsFilterAdmin.urlData+="/type/"+contextElt.type+"/id/"+contextElt.id;
	}
	if(typeof panelAdmin != "undefined" && typeof panelAdmin.paramsFilter != "undefined"){
		if(typeof panelAdmin.paramsFilter.filters != "undefined")
			paramsFilterAdmin.filters = panelAdmin.paramsFilter.filters;
		if(typeof panelAdmin.paramsFilter.defaults != "undefined")
			paramsFilterAdmin.defaults = panelAdmin.paramsFilter.defaults;
			if(typeof panelAdmin.paramsFilter.header != "undefined")
			paramsFilterAdmin.header = panelAdmin.paramsFilter.header;
	}

	if(typeof panelAdmin != "undefined" && typeof panelAdmin.context != "undefined"){
		
		paramsFilterAdmin.loadEvent.options["context"] = panelAdmin.context;
	}
	if(costum!=null && typeof costum[costum.slug] != "undefined" && typeof costum[costum.slug].aDirectory!="undefined" && typeof costum[costum.slug].aDirectory.actions != "undefined"){
		$.each(costum[costum.slug].aDirectory.actions, function(key, value){
			panelAdmin.actions[key] = value;
		});
	}
	// Admin structure simple d'admin pour qu'elle s'adapte au header du searchObj	
	if( typeof panelAdmin.csv != "undefined" || 
		typeof panelAdmin.pdf != "undefined"|| 
		typeof panelAdmin.invite != "undefined"){
		paramsFilterAdmin.header.options.right = {
			classes : 'col-xs-4 text-right no-padding',
			group : { }
		}

		if(typeof panelAdmin.csv != "undefined")
			paramsFilterAdmin.header.options.right.group.csv = panelAdmin.csv;
		if(typeof panelAdmin.pdf != "undefined")
			paramsFilterAdmin.header.options.right.group.pdf = panelAdmin.pdf;
		if(typeof panelAdmin.invite != "undefined")
			paramsFilterAdmin.header.options.right.group.invite = panelAdmin.invite;
		if(typeof panelAdmin.kanban != "undefined"){
			paramsFilterAdmin.header.options.right.group.kanban = panelAdmin.kanban;
		}
		/*if(typeof panelAdmin.community != "undefined")
			paramsFilterAdmin.header.options.right.group.community = panelAdmin.community;*/
	}
	// Configure searchObj for admin and construct central table 
	filterAdmin = searchObj.init(paramsFilterAdmin);

	// Search results for admin initialisation
	var myResult = filterAdmin.search.init(filterAdmin);
});
</script>
