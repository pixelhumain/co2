<?php
	//var_dump("exit"); exit;
	$cssAnsScriptFilesModule = array(
	    '/js/admin/panel.js',
	    '/js/admin/admin_directory.js',
	);
  	HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, $this->module->assetsUrl);

  	$adminConstruct=$this->appConfig["adminPanel"];
	if(isset($this->costum)){
		$slugContext=isset($this->costum["assetsSlug"]) ? $this->costum["assetsSlug"] : $this->costum["slug"] ;
		$cssJsCostum=array();
		if(isset($adminConstruct["js"])){
			$jsCostum=($adminConstruct["js"]===true) ? "admin.js" : $adminConstruct["js"];
			array_push($cssJsCostum, '/js/'.$slugContext.'/'.$jsCostum);
		}
		if(isset($adminConstruct["css"]))
			array_push($cssJsCostum, '/css/'.$slugContext.'/admin.css');
		if(!empty($cssJsCostum))
			HtmlHelper::registerCssAndScriptsFiles($cssJsCostum, Yii::app()->getModule( "costum" )->getAssetsUrl());
	}
	$logo = (@$this->costum["logo"]) ? $this->costum["logo"] : Yii::app()->theme->baseUrl."/assets/img/LOGOS/CO2/logo-min.png";
	

?>
<!-- start: PAGE CONTENT -->
<style type="text/css">
	#content-view-admin, #goBackToHome{
		display: none;
	}
	#content-admin-panel{
		min-height:700px;
	}
	.menuAdmin{
		display: flex;
		flex-direction: row;
		flex-wrap: wrap;
		gap: 20px;
		justify-content: center;
		align-items: center;
	}
	/*.menu-admin-item{
		width: calc(95% / 3);
		border: 1px solid;
    	text-align: center;
	}*/

	.menu-admin-item{
		cursor: pointer;
		display: flex;
		flex-direction: column;
		justify-content: center;
		align-items: center;
		gap: 5px;
		padding: 5px;
		text-decoration: none;
	}

	@media (max-width: 768px) {
		.menu-admin-item{
			width: 100% !important;
		}
	}
	@media (max-width: 1261px) {
		.menu-admin-item{
			width: calc(95% / 2);
		}
	}
	@media (max-width: 767px){
		.links-main-menu {
			font-size: inherit;
		}
	}

    /*menu*/
    .admin-menu-item {
        text-align: left;
        padding: 10px 20px 10px 30px;
        /* border-bottom: 1px solid; */
    }
    .admin-menu-item.active, .admin-menu-item:hover {
        background-color: #d4e1ea;
    }
    .admin-menu-item.active a, .admin-menu-item:hover a{
        color: #495392;
        text-decoration: none;
    }
    .admin-menu-item a {
        font-size: 20px;
        display: block;
        width: 100%;
    }
    .admin-menu-item a i {
        font-size: 24px;
        margin-right: 10px;
    }
    .admin-menu-lists {
        list-style: none;
        padding: 0;
    }

    /* modal*/
    #modal-view-stat {
        top: 50px;
        background-color: rgb(0 0 0 / 70%);
    }

    /* contain-admin-add */
    .contain-admin-add .btn-link h6 i {
        font-size: 35px;
        width: 70px;
        height: 70px;
        padding: 15px;
        border-radius: 50%;
        border: 2px solid;
        margin-bottom: 15px;
    }
    .contain-admin-add .btn-link h6 {
        font-size: 16px
    }

    .contain-admin-add .btn-link:hover {
        border: 1px solid;
        border-radius: 15px;
        border-left: 4px solid;
    }
    #view-content-admin .admin-menuLeft {
        /*box-shadow: 2px 0 20px #0a0a0a4f;*/
        position: fixed;
        height: 100vh;
        padding-right: 0;
        transition: all .5s ease-in-out;
        overflow-y: auto;
        padding-bottom: 150px;
        box-shadow: 0 4px 5px 0 rgba(0, 0, 0, .6);
    }
    .admin-menu-xs {
        display: none;
        cursor: pointer;
    }

    .card-counter{
        box-shadow: 2px 2px 10px #5a7490;
        margin: 5px;
        padding: 20px 20px;
        background-color: #fff;
        height: 100px;
        border-radius: 10px;
        transition: .3s linear all;
    }

    .card-counter:hover{
        box-shadow: 4px 4px 20px #DADADA;
        transition: .3s linear all;
    }

    .card-counter.primary{
        border: 1px solid #007bff;
        border-left: 5px solid #007bff;
    }

    .card-counter.danger{
        background-color: #ef5350;
        color: #FFF;
    }

    .card-counter.success{
        background-color: #66bb6a;
        color: #FFF;
    }

    .card-counter.info{
        background-color: #26c6da;
        color: #FFF;
    }

    .card-counter i{
        font-size: 5em;
        opacity: 0.2;
    }

    .card-counter .count-numbers{
        position: absolute;
        right: 35px;
        top: 20px;
        font-size: 36px;
        display: block;
    }

    .card-counter .count-name{
        position: absolute;
        right: 35px;
        top: 65px;
        font-style: italic;
        text-transform: capitalize;
        opacity: 0.5;
        display: block;
        font-size: 18px;
        color: #007bff;
        font-weight: bold;
    }

    @media screen and (max-width: 767px) {
        .admin-menuLeft {
            position: absolute;
            left: -767px;
            width: 100%;
            z-index: 99999;
            background: white !important;
            width: 90%;
            box-shadow: 0 4px 5px 0 rgba(0, 0, 0, .6);
        }

        .admin-menuLeft.active {
            left: 0px;
        }

        .admin-menu-xs/*.menu-left*/ {
            display: block;
            position: fixed;
            z-index: 100000000;
            background: #d4e1ea;
            color: #495392;
            border-radius: 0px 10px 10px 0px;
            transform: translate(50%, 100%);
            padding: 15px 5px 15px 5px;
            writing-mode: vertical-lr;
            margin-left: -15px;
        }
        .admin-menu-item a i {
            font-size: 20px;
        }
        .admin-menu-item a {
            font-size: 16px;
        }

    }
    
    /*Table*/
    #panelAdmin th, #panelAdmin td {
        border: none;
        vertical-align: middle !important;
        /*min-width: 10vw;*/
        padding: 0.5em;
        font-size: 14px;
    }
    #panelAdmin {
        border: none;
    }
     .panel-body div table.table-striped {
         table-layout: auto !important;
         word-wrap: normal !important;
     }
    #panelAdmin thead tr {
        border-radius: 8px 8px 0px 0px;
        box-shadow: 0px 0px 0px 5px #2b3b51;
        /*background: #2b3b51;*/
    }
    #panelAdmin tbody tr {
        /*border-radius: 8px;
        box-shadow: 0px 0px 0px 5px white;*/
        background: white;
        color: #2b3b51;
        border-bottom: #eee solid;
    }
    #panelAdmin thead {
        /*color: white;*/
        font-size: 16px;
    }
    #panelAdmin thead::after, #panelAdmin tbody tr::after {
        height: 1em;
        display: table-row;
        content: '';
        /* background-color: #f0f1f7; */
    }
    #content-admin-panel #btn-backToHome {
        display: block!important;
    }
</style>
<div class="col-xs-12" id="view-content-admin">
    <div class="row">
        <div class="admin-menu-xs ">
            <i class="fa fa-list-alt"></i> Menu d'administration
        </div>
        <div class="col-xs-12 col-sm-3 bg-white admin-menuLeft co-scroll">
            <?php if($authorizedAdmin || Role::isSourceAdmin(Role::getRolesUserId(Yii::app()->session["userId"]))){?>
            <ul class="admin-menu-lists">
                <?php
                if(!empty($menu)){
                    foreach($adminConstruct["menu"] as $key => $v) {
                        $show=(isset($v["show"])) ? $v["show"] : true;
                        $show= (isset($v["costumAdmin"]) && !Authorisation::isInterfaceAdmin()) ? false : $show;
                        $show=(isset($v["super"]) && !Authorisation::isInterfaceAdmin()) ? false : $show;
                        if($show){
                            $dataAttr="";
                            $dataAttr=(isset($v["dataHref"])) ? "data-href='".$v["dataHref"]."' " : "";
                            $dataAttr.=(isset($v["view"]) && !empty($v["view"])) ? "data-view='".$v["view"]."' " : "";
                            $dataAttr.=(isset($v["action"]) && !empty($v["action"])) ? "data-action='".$v["action"]."' " : "";
                            ?>
                            <li class="admin-menu-item <?= @$v["view"] == $view ? "active" :  "" ?>">
                                <a href="javascript:;" class="menu-admin-item btnNavAdmin <?php /*echo @$v["class"]*/ ?>" id="<?php echo @$v["id"] ?>" <?php echo $dataAttr; ?> style="cursor:pointer;">
                                    <i class="fa fa-<?php echo @$v["icon"] ?> fa-2x"></i>
                                    <span><?php echo Yii::t("admin", @$v["label"]); ?></span>
                                </a>
                            </li>
                            <?php
                        }
                    } ?>

                    <?php if(Authorisation::isInterfaceAdmin() && Authorisation::isUserSuperAdmin(Yii::app()->session["userId"]) && !isset($this->costum)){ ?>
                        <li class="admin-menu-item <?= $view == "spamobservatoire" ? "active" :  "" ?>">
                            <a href="javascript:;" class="menu-admin-item btnNavAdmin" data-view="spamobservatoire" style="cursor:pointer;">
                                <i class="fa fa-exclamation-circle fa-2x"></i>
                                <span>Spam Observatoire</span>
                            </a>
                        </li>
                        <li class="admin-menu-item  <?= $view == "zoneadmin" ? "active" :  "" ?>">
                            <a href="javascript:;" class="menu-admin-item btnNavAdmin " data-view="zoneadmin" style="cursor:pointer;">
                                <i class="fa fa-globe fa-2x"></i>
                                <span>Administration des zones</span>
                            </a>
                        </li>
                    <?php }
                }
                ?>
            </ul>
            <?php } ?>
        </div>

        <div class="col-sm-9 col-sm-offset-3 col-xs-12 " id="content-admin-panel">

            <?php if($authorizedAdmin || Role::isSourceAdmin(Role::getRolesUserId(Yii::app()->session["userId"]))){
                $title=(@Yii::app()->session["userIsAdmin"]) ? Yii::t("common","Administration portal") : Yii::t("common","Public administration portal");
                ?>
            <div class="col-md-12 col-sm-12 col-xs-12" id="navigationAdmin">
                <div class="col-sm-12 col-xs-12 text-center" style="background: #d4e1ea;color: #495392;">
                    <!-- <img src="<?php echo $logo ?>" height="100"><br/> -->
                     <h3><?php echo $title ?></h3>
                </div>


                <div class="col-xs-12  col-sm-offset-1 col-sm-10 padding-50  links-main-menu"
                     id="div-admin-select-create">
                    <?php
                    //Filtering button add element if costum
                    if(isset($adminConstruct["add"]) && $adminConstruct["add"]){ ?>
                        <br>
                        <div class="col-xs-12 col-sm-12  shadow2 bg-white ">

                           <h5 class="text-center margin-top-15 infoPanelAddContent">
                            <i class="fa fa-plus-circle"></i> <?php echo Yii::t("form","Add items") ?>
                            <br>
                            <small><?php echo Yii::t("form","What kind of content will you create ?") ?></small>
                           </h5>

                            <div class="col-md-12 col-sm-12 col-xs-12 contain-admin-add"><hr></div>
                        </div>
                    <?php } else { ?>
                        <br>
                        <br>
                        <div class="col-xs-12 col-sm-12 no-padding text-center ">
                            <img src="<?php echo Yii::app()->theme->baseUrl.'/assets/img/admin.png'; ?>" class="" style="width: 80%">
                        </div>
                            <?php }  ?>
                    <!--<div class="col-xs-12  col-sm-offset-1 col-sm-10  shadow2 bg-white margin-top-35">
                        <div class="menuAdmin padding-top-10 padding-bottom-10">
                        <?php /*
                        if(!empty($menu)){
                            foreach($adminConstruct["menu"] as $key => $v) {
                                $show=(isset($v["show"])) ? $v["show"] : true;
                                $show= (isset($v["costumAdmin"]) && !Authorisation::isInterfaceAdmin()) ? false : $show;
                                $show=(isset($v["super"]) && !Authorisation::isInterfaceAdmin()) ? false : $show;
                                if($show){
                                    $dataAttr="";
                                    $dataAttr=(isset($v["dataHref"])) ? "data-href='".$v["dataHref"]."' " : "";
                                    $dataAttr.=(isset($v["view"]) && !empty($v["view"])) ? "data-view='".$v["view"]."' " : "";
                                    $dataAttr.=(isset($v["action"]) && !empty($v["action"])) ? "data-action='".$v["action"]."' " : "";
                                        */?>
                                    <a href="javascript:;" class="menu-admin-item btnNavAdmin <?php /*echo @$v["class"] */?>" id="<?php /*echo @$v["id"] */?>" <?php /*echo $dataAttr; */?> style="cursor:pointer;">
                                        <i class="fa fa-<?php /*echo @$v["icon"] */?> fa-2x"></i>
                                        <span><?php /*echo Yii::t("admin", @$v["label"]); */?></span>
                                    </a>
                                <?php /*
                                }
                            } */?>

                            <?php /*if(Authorisation::isInterfaceAdmin() && Authorisation::isUserSuperAdmin(Yii::app()->session["userId"])){ */?>
                                <a href="javascript:;" class="menu-admin-item btnNavAdmin text-red" data-view="spamobservatoire" style="cursor:pointer;">
                                    <i class="fa fa-exclamation-circle fa-2x"></i>
                                    <span>Spam Observatoire</span>
                                </a>
                            <?php /*}
                            if(Authorisation::isInterfaceAdmin() && Authorisation::isUserSuperAdmin(Yii::app()->session["userId"])){ */?>
                                <a href="javascript:;" class="menu-admin-item btnNavAdmin text-red" data-view="zoneadmin" style="cursor:pointer;">
                                    <i class="fa fa-globe fa-2x"></i>
                                    <span>Administration des zones</span>
                                </a>
                                <a href="javascript:;" class="menu-admin-item btnNavAdmin text-primary" data-view="instanceadmin" style="cursor:pointer;">
                                    <i class="fa fa-clone fa-2x"></i>
                                    <span>Administration des instances</span>
                                </a>
                            <?php /*}
                            }
                        */?>
                        </div>
                    </div>-->
                </div>
            </div>
            <!--<div class="col-md-12 col-sm-12 col-xs-12 no-padding" id="goBackToHome">
                <a href="javascript:;" class="btnNavAdmin  col-md-12 col-sm-12 col-xs-12 padding-20 text-center bg-orange" data-view="index" style="font-size:20px;"><i class="fa fa-home"></i> <?php /*echo Yii::t("common", "Back to admin home") */?></a>
            </div>-->
            <div id="content-view-admin" class="col-md-12 col-sm-12 col-xs-12 no-padding"></div>
            <?php }else{ ?>
            <div class="col-md-12 col-sm-12 col-xs-12 text-center margin-top-50">
                    <img src="<?php echo $logo ?>"
                                 class="" height="100"><br/>
                     <h3><?php echo Yii::t("common","Administration portal") ?></h3>
                </div>
                <div class="alert-danger text-center padding-30 padding-bottom-50"><strong><?php echo Yii::t("common","You are not authorized to acces adminastrator panel ! <br/>Connect you or contact us in order to become admin system") ?></strong></div>
            <?php } ?>
        </div>
    </div>
</div>
<div class="modal fade" id="modal-view-stat" tabindex="-1" role="dialog" aria-labelledby="modalLabelLarge" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <!--<h4 class="modal-title" id="modalLabelLarge">Modal Title</h4>-->
            </div>

            <div class="modal-body col-xs-12" id="content-view-stat" style="background-color: #ffffff"></div>
        </div>
    </div>
</div>
<!-- end: PAGE CONTENT-->
<script type="text/javascript">
	var superAdmin="<?php echo @Yii::app()->session["userIsAdmin"] ?>";
	var sourceAdmin="<?php echo @Yii::app()->session["userIsAdminPublic"] ?>";
	var authorizedAdmin=<?php echo json_encode(@$authorizedAdmin) ?>;
	var edit=true;
	var paramsAdmin= <?php echo json_encode($adminConstruct) ?>;
	var subView=<?php echo (isset($_POST["subView"])) ? json_encode(true) : json_encode(false); ?>;
	adminPanel.params={
		hashUrl : "#admin",
    	view : "<?php echo @$_GET['view']; ?>",
    	action : null,
		dir: "<?php echo @$_GET['dir']; ?>",
		subView:false
	};
	if(notEmpty(subView)){
	 	adminPanel.params.view = "<?php echo @$_GET['subview']; ?>";
	 	adminPanel.params.subView = true;
	 }
	jQuery(document).ready(function() {
		mylog.log("render","--- co2/views/admin/index.php", authorizedAdmin, !authorizedAdmin);
		if(!authorizedAdmin){
			//urlCtrl.loadByHash("");
			bootbox.dialog({message:'<div class="alert-danger text-center"><strong><?php echo Yii::t("common","You are not authorized to acces adminastrator panel ! <br/>Connect you or contact us in order to become admin system") ?></strong></div>'});
		}
		adminPanel.init();
        $(".admin-menu-xs").off().on('click', function() {
            $(".admin-menuLeft").toggleClass("active");
        })
	});
</script>
<!-- end: PAGE CONTENT-->

