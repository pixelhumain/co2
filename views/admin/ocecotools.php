<style>
	#oceco-tools-coevent-modal {
		z-index: 999999;
	}

	.otc-select2-item {
		display: flex;
		gap: 10px;
	}

	.otc-select2-image {
		width: 40px;
		height: 40px;
		min-width: 40px;
		overflow: hidden;
		min-height: 40px;
		border-radius: 50%;
	}

	.otc-select2-image img {
		width: 100%;
		height: 100%;
		object-fit: cover;
	}

	.otc-select2-info {
		font-size: 16px;
	}

	.otc-select2-type {
		font-size: 14px;
	}

	.otc-select2-info>* {
		display: block;
	}

	.otc-select2-name {
		font-weight: bold;
	}

	.otc-select2-search {
		text-decoration: underline;
		font-style: italic;
	}
</style>
<div class="modal fade" id="oceco-tools-coevent-modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title">Coevent</h4>
			</div>
			<div class="modal-body">
				<form action="#!" id="oceco-tools-coevent-form">
					<input type="hidden" id="otc-context-type">
					<div class="form-group">
						<label for="otc-context">Contexte</label>
						<input type="text" hidden id="otc-context">
					</div>
					<div class="form-group">
						<label for="otc-parent-event">Evénement parent</label>
						<select id="otc-parent-event">
							<option value="" selected></option>
						</select>
					</div>
					<div class="form-group" id="create-event-group">
						<label for="oct-create-event">Vous avez besoin de créer un événement ?</label><br>
						<button id="oct-create-event" class="btn btn-primary">Créer un événement</button><br>
						<label class="error"></label>
					</div>
					<div class="form-group" id="otc-page-name-group">
						<label for="otc-page-name">Nom de la page</label>
						<input type="text" class="form-control" id="otc-page-name">
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
				<button type="button" class="btn btn-primary" id="oct-save"><span class="fa fa-plus"></span> Créer</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script>
	"use strict";

	function create_costum_page(args) {
		return new Promise(function(resolve, reject) {
			if (typeof args.hash !== "string" ||
				typeof args.event !== "string" ||
				typeof args.id !== "string" ||
				typeof args.collection !== "string")
				reject();
			var path2value = {
				id: args.id,
				collection: args.collection,
				path: "costum.app.#" + args.hash,
				value: {
					hash: "#app.view",
					coevent: true,
					event: args.event,
					urlExtra: "/page/" + args.hash,
					name: {}
				},
				setType: [{
					path: "coevent",
					type: "boolean"
				}]
			};
			if (typeof args.name === "string")
				path2value.value.name[mainLanguage] = args.name;
			dataHelper.path2Value(path2value, resolve);
		});
	}

	function get_event_from_context(args) {
		var url = baseUrl + "/costum/coevent/get_events/request/element_event";
		var elementEvents = {};
		return new Promise(function(resolve, reject) {
			ajaxPost(null, url, args, resolve, reject, "json")
		});
	}

	function classic_form(args) {
		var self = this;
		const otContextForm = {
			jsonSchema: {
				title: "Contexte",
				description: "",
				icon: "fa-question",
				properties: {
					name: {
						inputType: "text",
						label: args.label
					},
					parent: {
						inputType: "finder",
						label: "Sélectionner un contexte",
						initMe: false,
						buttonLabel: "Rechercher un element",
						placeholder: "Rechercher un element",
						initContext: false,
						initType: ["organizations", "projects"],
						initBySearch: true,
						initContacts: false,
						openSearch: true,
						filters: {
							'$or': {
								["links.members." + userId + ".isAdmin"]: {
									"$exists": true
								},
								["links.contributors." + userId + ".isAdmin"]: {
									"$exists": true
								},
								["costum.type"]: {
									"$exists": false
								}
							}
						},
						multiple: false,
						rules: {
							required: true
						},
						noResult: { 
							label: "Créez l'organisation",
							action: function () {
								var customForm = {
									"beforeBuild" : {
										"properties" : {
											
										}    
									}, 
									"afterSave" : function(data) {
										console.log("data orga", data)
										valueOrga = {}
										valueOrga[data.id] = {
											_id: {
												$id: data.id
											},
											name: data.map.name,
											type: "organizations"
										};
									}
								};
								var extendedForm=customForm;
								dyFObj.openForm("organization",null,null, null, extendedForm);
								$(".bootbox").modal('hide');
							}
						}
					}
				},
				onLoads: {
					onload: function() {

					}
				},
				beforeBuild: function() {},
				save: function(formData) {
					var today = new Date();
					delete formData.collection;
					delete formData.scope;
					args.callback(formData);
				}
			}
		}
		if (typeof contextId != "undefined" && typeof contextType != "undefined" && typeof contextName != "undefined") {
			self.contextData = {
				parent: {}
			}
			self.contextData["parent"][contextId] = {
				type: contextType,
				name: contextName
			};
		}
		dyFObj.openForm(otContextForm, null, self.contextData, null, null, {
			type: "bootbox"
		});
	}

	function coevent_form(args) {
		var context_input_dom = $("#otc-context");
		var event_select_dom = $("#otc-parent-event");
		var page_name_group_dom = $("#otc-page-name-group");
		var context_type_input_dom = $("#otc-context-type");
		var create_event_button_dom = $("#oct-create-event");
		var creae_event_group_dom = $("#create-event-group");
		var create_btn_dom = $("#oct-save");

		function enable_spin(state) {
			create_btn_dom.prop("disabled", state);
			if (state)
				create_btn_dom.find(".fa")
				.removeClass("fa-plus")
				.addClass("fa-spinner")
				.addClass("fa-spin");
			else
				create_btn_dom.find(".fa")
				.addClass("fa-plus")
				.removeClass("fa-spinner")
				.removeClass("fa-spin");
		}

		event_select_dom.build_event_select = function(events) {
			var new_event_id = "new_event";
			var self = this;
			var _events = [];
			if (typeof events === "object" && Array.isArray(events))
				_events = events.slice();
			_events.unshift({
				id: new_event_id,
				name: "Créer un événement"
			});
			if (self.data("select2"))
				self.select2("destroy");
			while (self.children().length > 0)
				self.children().last().remove();
			self.append("<option value=\"\" selected></option>");
			$.each(_events, function(i, event) {
				self.append(
					$("<option>")
					.attr("value", event.id)
					.text(event.name)
					.data("storage", event)
				);
			});
			self.select2({
				width: "100%",
				formatResult: function(item) {
					if (item.id === new_event_id) {
						return ("<div style=\"text-align: center;\"><button id=\"oct-create-event\" class=\"btn btn-primary\">Créer un événement</button></div>");
					}
					var regex = new RegExp(self.data('select2').search.val(), 'ig');
					var copy = $.extend(true, {}, $(item.element[0]).data("storage"));
					copy.text = copy.name.replace(regex, function(match) {
						return ("<span class=\"otc-select2-search\">" + match + "</span>");
					});
					var start_date = copy.startDate.format("LL");
					var start_hour = copy.startDate.format("HH:mm");
					var end_date = copy.endDate.format("LL");
					var end_hour = copy.endDate.format("HH:mm");
					if (end_hour === "00:00") {
						var clone = copy.endDate.clone().subtract(1, "minutes");
						end_date = clone.format("YYYY-MM-DD");
						end_hour = clone.format("HH:mm");
					}
					if (start_date !== end_date) {
						copy.interval = start_date;
						if (start_hour !== "00:00")
							copy.interval += " (" + start_hour + ")";
						copy.interval += " - " + end_date + " (" + end_hour + ")";
					} else
						copy.interval = start_date + " (" + start_hour + " - " + end_hour + ")";
					return (dataHelper.printf(
						"<div class=\"otc-select2-item\">" +
						"	<div class=\"otc-select2-info\">" +
						"		<span class=\"otc-select2-name\">{{text}}</span>" +
						"		<span class=\"otc-select2-type\">{{interval}}</span>" +
						"	</div>" +
						"</div>",
						copy
					));
				}
			}).off("click").on("click", function() {
				if (self.val() === new_event_id) {
					self.val("").trigger("change");
					create_event_button_dom.trigger("click");
					return (true);
				}
			});
		};
		var modal_dom = $("#oceco-tools-coevent-modal");
		var search_length = 5;
		var elements = ["organizations", "projects"];
		if (context_input_dom.data("select2"))
			context_input_dom.select2("destroy");
		context_input_dom.select2({
			width: "100%",
			minimumInputLength: 1,
			ajax: {
				url: baseUrl + "/co2/element/select2list",
				dataType: "json",
				type: "post",
				cache: true,
				data: function(term, page) {
					return ({
						search: term,
						page: page,
						elements: elements,
						length: search_length,
						user: userId
					});
				},
				results: function(data) {
					var results = data.results.map(function(result) {
						return ({
							id: result.id,
							text: result.name,
							image: result.image,
							type: result.type,
							slug: result.slug
						});
					});
					return ({
						results: results,
						more: data.more
					});
				}
			},
			formatResult: function(item) {
				var regex = new RegExp(context_input_dom.data('select2').search.val(), 'ig');
				var copy = $.extend({}, item);
				copy.match = copy.text.replace(regex, function(match) {
					return ("<span class=\"otc-select2-search\">" + match + "</span>");
				});
				return (dataHelper.printf(
					"<div class=\"otc-select2-item\">" +
					"	<div class=\"otc-select2-image\">" +
					"		<img src=\"{{image}}\" alt=\"{{text}}\">" +
					"	</div>" +
					"	<div class=\"otc-select2-info\">" +
					"		<span class=\"otc-select2-name\">{{match}}</span>" +
					"		<span class=\"otc-select2-type\">{{type}}</span>" +
					"	</div>" +
					"</div>",
					copy
				));
			}
		}).off("change").on("change", function(e) {
			var self = $(this);
			var data = self.select2("data");
			create_event_button_dom.data(data).nextAll(".error").hide();
			context_type_input_dom.val(data.type);
			get_event_from_context(data).then(function(events) {
				var mapped = Object.keys(events).map(function(key) {
					return ({
						id: key,
						name: events[key].name,
						startDate: moment.unix(events[key].startDate.sec),
						endDate: moment.unix(events[key].endDate.sec),
					})
				});
				event_select_dom.build_event_select(mapped);
			});
			dataHelper.element_has_costum(data).then(function(exists) {
				if (exists)
					page_name_group_dom.show();
				else
					page_name_group_dom.hide().find(".form-control").val("");
			});
		});
		// initialize when modal opens
		event_select_dom.build_event_select();
		page_name_group_dom.hide().find(".form-control").val("");
		modal_dom.modal("show");
		context_type_input_dom.val("");
		creae_event_group_dom.hide();
		enable_spin(false);
		// initialize when modal opens
		modal_dom.off("hidden.bs.modal").on("hidden.bs.modal", function() {
			context_input_dom.select2("destroy").val("");
			event_select_dom.select2("destroy");
		});

		$("#oceco-tools-coevent-form").off("submit").on("submit", function(e) {
			var form_valid;
			var has_costum;

			e.preventDefault();
			has_costum = page_name_group_dom.is(":visible");
			form_valid = context_input_dom.val() !== "";
			form_valid = form_valid && event_select_dom.val() !== "";
			form_valid = form_valid && (!has_costum || page_name_group_dom.find(".form-control").val() !== "");

			if (form_valid) {
				enable_spin(true);
				var context_data = {
					id: context_input_dom.val(),
					collection: context_type_input_dom.val(),
					firstStepper: false
				};
				if (!has_costum) {
					coInterface.create_costum(context_data).then(function(response) { 
						if (response.result) {
							create_costum_page($.extend({}, context_data, {
								hash: "welcome",
								event: event_select_dom.val(),
								name: "welcome"
							})).then(function(response) {
								if (typeof response === "object" && response.result) {
									var context_data = context_input_dom.select2("data");
									location.href = baseUrl + "/costum/co/index/slug/" + context_data.slug + "/edit/true#welcome?text=coevent";
								}
							});
						}
					});
				} else {
					var page_link = page_name_group_dom.find(".form-control").val()
						.toLowerCase()
						.replace(/\s/g, "-")
						.normalize("NFD")
						.replace(/[\u0300-\u036f]/g, "");
					var name = {};
					create_costum_page($.extend({}, context_data, {
						hash: page_link,
						event: event_select_dom.val(),
						name: page_name_group_dom.find(".form-control").val()
					})).then(function(response) {
						if (typeof response === "object" && response.result) {
							var context_data = context_input_dom.select2("data");
							location.href = baseUrl + "/costum/co/index/slug/" + context_data.slug + "/edit/true#" + page_link + "?text=coevent";
						}
					});
				}
			}
		});
		$("#oct-save").off("click").on("click", function() {
			var self = $(this);
			if (self.prop("disabled"))
				return (true);
			$("#oceco-tools-coevent-form").trigger("submit");
		});
		if (typeof contextId !== "undefined" && typeof contextType !== "undefined" && typeof contextName !== "undefined")
			$.ajax({
				url: baseUrl + "/co2/element/select2list",
				dataType: "json",
				type: "post",
				data: {
					id: contextId,
					type: contextType
				},
				success: function(response) {
					if (response.results.length) {
						context_input_dom.select2("data", {
							id: response.results[0].id,
							text: response.results[0].name,
							image: response.results[0].image,
							type: response.results[0].type,
							slug: response.results[0].slug
						});
						context_input_dom.val(contextId).trigger("change");
					}
				}
			});
		create_event_button_dom.off("click").on("click", function() {
			var self = $(this);
			var data = $.extend({}, {
				id: null,
				type: null
			}, self.data());
			if (notEmpty(data.id) && notEmpty(data.type)) {
				var default_value = {
					organizer: {}
				};
				default_value.organizer[data.id] = {
					name: data.text,
					type: data.type,
					profilThumbImageUrl: data.image
				};
				default_value.organizer[data.id]._id = {
					$id: data.id
				};
				var build = {
					afterSave: function(data) {
						dyFObj.onclose = function() {
							event_select_dom.build_event_select([{
								id: data.id,
								name: data.map.name,
								startDate: moment.unix(data.map.startDate.sec),
								endDate: moment.unix(data.map.endDate.sec),
							}]);
							event_select_dom.val(data.id).trigger("change");
							modal_dom.css("z-index", "");
						}
						dyFObj.closeForm();
					}
				}
				dyFObj.openForm("event", null, default_value, null, build);
				dyFObj.onclose = function() {
					modal_dom.css("z-index", "");
				}
				modal_dom.css("z-index", "99999");
			} else
				self.nextAll(".error").show().text("Vous devez sélectionner un contexte");
		});
	}

	const ocecotoolsObj = {
		init: function(otObj) {
			var copyOtObj = Object.assign({}, otObj);
			copyOtObj.events(copyOtObj);
		},
		events: function(otObj) {
			$(document).on("click", '.btn-new-form', function() {
				$(".btn-ocecotools[data-id='createNewForm']").trigger('click')
			})

			$(".btn-ocecotools").off().on("click", function(e) {
				let btn = $(this);
				var action = btn.data("id");
				otObj.formConfig(otObj, action, function(data) {
					if (typeof data.parent != "undefined") {
						if (action === "createNewOcecoForm") {
							otObj.generateOceco(otObj, data, "createOceco")
						}
						else if (action === "createAac") {
							otObj.generateOceco(otObj, data, "createAac");
						} else if (action === "createNewForm") {
							otObj.generateForm(otObj, data);
						} else {
							otObj.generateForm(otObj, data,
								[{
									id: btn.data("id"),
									label: btn.data("label"),
									type: btn.data("path")
								}]
							);
						}
					} else {
						bootbox.alert(trad["Choose context"]);
					}
				})
			})
		},
		generateOceco: function(otObj, prms, action) {
			ajaxPost(
				null,
				baseUrl + "/co2/aap/generateformoceco/", {
					"elId": Object.keys(prms.parent)[0],
					"elType": prms.parent[Object.keys(prms.parent)[0]]["type"],
					"createOceco": true,
					"formName": prms.name,
					"action" : action
				},
				function(data) {
					if (typeof data["aap link"] != "undefined") {
						window.open(data["aap link"], '_self');
					}
				}, null, null, {
					async: false
				}
			);
		},
		generateForm: function(otObj, prms, inputs) {
			ajaxPost(
				null,
				baseUrl + "/co2/aap/generateformoceco/", {
					"elId": Object.keys(prms.parent)[0],
					"elType": prms.parent[Object.keys(prms.parent)[0]]["type"],
					"createForm": true,
					"inputs": inputs,
					"formName": prms.name
				},
				function(data) {
					if (typeof data["config link"] != "undefined") {
						window.open(data["config link"], '_self');
						location.reload();
					}
				}, null, null, {
					async: false
				}
			);
		},
		formConfig: function(otObj, action, callback) {
			switch (action) {
				case "createNewOcecoForm":
					classic_form.call(otObj, {
						label: "Nom de l'oceco",
						callback: callback
					});
					break;
				case "createCoevent":
					coevent_form.call(otObj, {});
					break;
				case "createAac":
					classic_form.call(otObj, {
						label: "Nom du appel à commun",
						callback: callback
					})
					break;
				
				default:
					classic_form.call(otObj, {
						label: "Nom du formulaire",
						callback: callback
					})
					break;
			}
		}
	}
	ocecotoolsObj.init(ocecotoolsObj);
</script>