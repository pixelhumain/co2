<?php 

use CAction, Element, Rest, Yii, MongoId, Citoyen, Log, Person, Authorisation;
use PHDB;
?>

<?php
$cssAnsScriptFilesModuleMap = array( 
    '/leaflet/leaflet.css',
    '/leaflet/leaflet.js',
    '/css/map.css',
    '/markercluster/MarkerCluster.css',
    '/markercluster/MarkerCluster.Default.css',
    '/markercluster/leaflet.markercluster.js',
    '/js/map.js',
);

HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModuleMap, Yii::app()->getModule( Map::MODULE )->getAssetsUrl() );

$keyTpl ="spamObservatoire";
$paramsData = [ 
    "title" => "Liste des adresse IP Spam",
];




?>

<style type="text/css">
    .title-spam-observatoire{
        text-align: center;
    }

    .header-spam-observatoire{
        margin-bottom: 100px !important;
    }

    .main-container-spam-observatoire{
        margin-left:  5%;
        margin-right:  5%;
    }

    .map-container-spam-observatoire{
        margin-bottom: 100px;
    }
    
    .container-liste-spam--spam-observatoire , .container-liste-spam--spam-observatoire .table{
        margin-left:  5%;
        margin-right:  5%;
    }

    .container-filters-menu-spam-observatoire{
        margin-left: 5%;
    }

    .searchBar-filters-spam-observatoire{
        margin-bottom:  25px;
    }

    .dropdown-spam-observatoire {
        /* height: 57px; */
        margin-bottom: 5px;
        margin-top: 5px;
        margin-right: 10px;
        padding-top: 0px;
        padding-bottom: 0px;
        display: inline-block;
        float: left;
    }

    .container-filters-menu-spam-observatoire .searchBar-filters-spam-observatoire .input-group-addon {
        background-color: #9fbd38!important;
    }

    .dropdown-spam-observatoire .btn-menu,  .container-filters-menu-spam-observatoire .search-bar{
        border: 1px solid #9fbd38!important;
    }

    .dropdown-spam-observatoire .btn-menu {
        padding: 10px 15px;
        font-size: 16px;
        border: 1px solid #6b6b6b;
        color: #6b6b6b;
        top: 0px;
        position: relative;
        border-radius: 20px;
        text-decoration: none;
        background: white;
        line-height: 20px;
        display: inline-block;
        margin-left: 5px;
    }

    .dropdown-spam-observatoire .dropdown-menu {
        position: absolute;
        overflow-y: visible !important;
        top: 50px;
        left: -55px;
        width: 250px;
        border-radius: 2px;
        border: 1px solid #6b6b6b;
        padding: 0px;
    }

    .ville-filters-spam-observatoire .dropdown.open .dropdown-menu, .pays-filters-spam-observatoire .dropdown.open .dropdown-menu,  .region-filters-spam-observatoire .dropdown.open .dropdown-menu{
        display: block;
    }

    .dropdown-spam-observatoire .dropdown-menu .list-filters {
        max-height: 300px;
        overflow-y: scroll;
    }

    .dropdown-spam-observatoire .dropdown-menu {
        font-size: 14px;
        text-align: left;
        list-style: none;
    }

    .container-filters-menu-spam-observatoire .dropdown-menu .list-filters button:hover{
        background-color: #e8e8e8;
    }

    .main-container-spam-observatoire .container-liste-spam--spam-observatoire .tabling-ipSpam thead tr th,  .main-container-spam-observatoire .container-liste-spam--spam-observatoire .tabling-ipSpam thead tr, .main-container-spam-observatoire .container-liste-spam--spam-observatoire .tabling-ipSpam thead{
        background-color: #6b6b6b!important;
    }
    
    
</style>


<div class="container-spam-observatoire col-md-12">
    <div class="header-spam-observatoire" >
        <h2 class=" title title-spam-observatoire Oswald sp-text img-text-bloc" data-field="title">
            <?php echo $paramsData["title"] ?>
        </h2>

        <div class="panel-group container-filters-menu-spam-observatoire">
            <div class="searchBar-filters searchBar-filters-spam-observatoire pull-left ">
                <input type="text" class="form-control pull-left text-center main-search-bar search-bar search-ip" data-field="text" data-text-path="" placeholder="Que recherchez-vous ?">
                <span class="text-white input-group-addon pull-left main-search-bar-addon " data-field="text" data-icon="fa-arrow-circle-right"><i class="fa fa-arrow-circle-right"></i></span>
            </div>

            <?php if(isset($regions["result"]) && !empty($regions["result"])) { ?>
                <div class="region-filters-spam-observatoire">
                    <li class="dropdown dropdown-spam-observatoire">
                        <a href="javascript:;" class="dropdown-toggle menu-button btn-menu" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-label-xs="types" data-placement="bottom">
                            Régions <i class="fa fa-angle-down margin-left-5"></i>
                        </a>
                        <div class="dropdown-menu arrow_box" style="overflow-y: auto;" aria-labelledby="dropdownTypes">
                            <div class="list-filters">
                                <?php foreach($regions["result"] as $region) { 
                                    if(isset($region["region"]) && !empty($region["region"])) { ?>
                                        <button type="button" class="btn padding-10 col-xs-12 btn-filter-type-change--spam-observatoire" data-type="region" data-key="<?= $region["region"] ?>">
                                            <span class="elipsis label-filter "><?= $region["region"] ?></span>
                                            <span class="badge text-white bg-turq"><?= $region["count"] ?></span>
                                        </button>

                                <?php } } ?>
                            </div>
                        </div>
                    </li>
                </div>
            <?php } ?>

            <?php if(isset($citys["result"]) && !empty($citys["result"])) { ?>
                <div class="ville-filters-spam-observatoire">
                    <li class="dropdown dropdown-spam-observatoire">
                        <a href="javascript:;" class="dropdown-toggle menu-button btn-menu" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-label-xs="types" data-placement="bottom">
                            Villes <i class="fa fa-angle-down margin-left-5"></i>
                        </a>
                        <div class="dropdown-menu arrow_box" style="overflow-y: auto;" aria-labelledby="dropdownTypes">
                            <div class="list-filters">
                                <?php foreach($citys["result"] as $city) { 
                                    if(isset($city["city"]) && !empty($city["city"])) { ?>

                                        <button type="button" class="btn padding-10 col-xs-12 btn-filter-type-change--spam-observatoire" data-type="city" data-key="<?= $city["city"] ?>">
                                            <span class="elipsis label-filter "><?= $city["city"] ?></span>
                                            <span class="badge text-white bg-turq"><?= $city["count"] ?></span>
                                        </button>

                                <?php } } ?>
                            </div>
                        </div>
                    </li>
                </div>
            <?php } ?>

            <?php if(isset($countrys["result"]) && !empty($countrys["result"])) { ?>
                <div class="pays-filters-spam-observatoire">
                    <li class="dropdown dropdown-spam-observatoire">
                        <a href="javascript:;" class="dropdown-toggle menu-button btn-menu" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-label-xs="types" data-placement="bottom">
                            Pays <i class="fa fa-angle-down margin-left-5"></i>
                        </a>
                        <div class="dropdown-menu arrow_box" style="overflow-y: auto;" aria-labelledby="dropdownTypes">
                            <div class="list-filters">
                                <?php foreach($countrys["result"] as $country) { 
                                    if(isset($country["country"]) && !empty($country["country"])) { ?>
                                        <button type="button" class="btn padding-10 col-xs-12 btn-filter-type-change--spam-observatoire" data-type="country" data-key="<?= $country["country"] ?>">
                                            <span class="elipsis label-filter "><?= $country["country"] ?></span>
                                            <span class="badge text-white bg-turq"><?= $country["count"] ?></span>
                                        </button>

                                <?php } } ?>
                            </div>
                        </div>
                    </li>
                </div>
            <?php } ?>

            <div id="activeFilters-spam-observatoire">
                <div id="activeFilters" class="col-xs-12 no-padding">

                </div>

            <div class="no-padding col-xs-12" style="top: inherit; left: inherit; right: inherit;">
                <div class="col-xs-12">
                    <div class="col-xs-8 elipsis no-padding">
                        <h4 class="pull-left count-result-spam-observatoire"><i class="fa fa-angle-down"></i> <?= count($ipSpams); ?> résultats </h4>
                    </div>
                    <div class="col-xs-4 text-right no-padding container-btn-shift-spam-observatoire">
                        <button type="button" class="btn-show-in-carte-spam-observatoire btn-show-map hidden-xs" title="Afficher sur la carte" alt="Afficher sur la carte">
                            <i class="fa fa-map-marker"></i> Carte
                        </button>
                    </div>
                </div>
            </div>
        </div>

        
    </div>

    <div class="main-container-spam-observatoire">
        <div class="map-container-spam-observatoire"  style="display:none; ">
            <div style="height: 500px;" class="col-md-12 mapBackground no-padding divMap" id="divMap"></div>

            </div>
        </div>

        <div class="container-liste-spam--spam-observatoire">
            <table class="tabling-ipSpam table  table-hover col-xs-12">
                <thead style="background-color: #9fbd38!important;">
                    <tr>
                        <th class="col title">Adresse IP</th>
                        <th class="col key-app">Region</th>
                        <th class="col key-app">Ville</th>
                        <th class="col key-app">Pays</th>
                    </tr>
                </thead>
                <tbody id="tbody-tab-liste-ipSpam-spam-observatoire">
                    <?php foreach($ipSpams as $ipSpam) { ?>
                    <tr>
                        <td> <?= @$ipSpam["ip"] ?></td>
                        <td> <?= @$ipSpam["region"] ?></td>
                        <td> <?= @$ipSpam["city"] ?>  </td>
                        <td> <?= @$ipSpam["country"] ?></td>
                    </tr>

                    <?php } ?>

                </tbody>
            </table>
        </div>
    </div>
</div>
   



    





<script type="text/javascript">

    var filterListe = {};
    var allIpSpam = <?php echo json_encode( $ipSpams ); ?>;

    var mapAbout = {};
    var searchIp = "";

    function addElementInTable(val) {
        var str = "";
        if(typeof val.ip != "undefined"){
            var ip = val.ip;
            var region = (typeof val.region != "undefined") ? val.region : "";
            var city = (typeof val.city != "undefined") ? val.city : "" ;
            var country = (typeof val.country != "undefined") ? val.country : "";
            str += "<tr>" +
                    "<td>" + ip  + "</td>" +   
                    "<td>" + region  + "</td>" +
                    "<td>" + city + "</td>" +
                    "<td>" +  country + "</td>" +
                "</tr>";
        }
        return str;
    }

    function addFilter(type, value) {
        if(typeof filterListe[type] == "undefined")
            filterListe[type] = [];

        var indexOfvalue = filterListe[type].indexOf(value);
        if(indexOfvalue != -1){
            removeFilter(type, value, indexOfvalue);
        }
        else{
            filterListe[type].push(value);

            $("#activeFilters-spam-observatoire #activeFilters").append(`
                <div class="filters-activate tooltips" data-position="bottom" data-title="Effacer" data-type="`+type+`" data-key="`+value+`">
                    <i class="fa fa-times-circle"></i>
                    <span class="activeFilters" data-type="`+type+`" data-key="`+value+`">`+value+`</span>
                </div>
            `);
            bindClickBtnDeleteFilter();
        }

        refreshListe();
    }

    function showMap(){
        var dataMap = []; 
        
        $.each(allIpSpam, function(key, val){
            if(typeof val.loc != "undefined"){
                var localisation = val.loc.split(",");
                if(localisation.length > 1){
                    var data = val;
                    data["name"] = val.ip;
                    data["collection"] = "ipSpam";
                    data["geo"] = {
                        "@type" : "GeoCoordinates",
                        "latitude" : localisation[0],
                        "longitude" : localisation[1]
                    };

                    dataMap.push(data);
                    /* mapAbout.addMarker({
                        elt:{
                            geo:{
                                "@type" : "GeoCoordinates",
                                "latitude" : localisation[0],
                                "longitude" : localisation[1]
                            }
                        }
                    }) */
                }
            }
            
        });

        var paramsMapContent = {
            container : "divMap",
            activeCluster : true,
            zoom : 3,
            activePopUp : true,
            elts : dataMap
        };

        
        mapAbout = mapObj.init(paramsMapContent);
        //mapAbout.addElts(dataMap);
        if(mapAbout != null)
            mapAbout.hideLoader();
    };

    function btncChangeValueInInputSearchIp() {
        $(".container-filters-menu-spam-observatoire .search-ip").off().on("change", function(){
            searchIp = $(this).val();
            refreshListe();
        });
    }

    function bindClickBtnDeleteFilter() {
        $("#activeFilters-spam-observatoire #activeFilters .filters-activate").off().on("click", function(){
            var type = $(this).data("type");
            var value = $(this).data("key");

            removeFilter(type, value);
            refreshListe();
        });
    }

    function bindClickBtnShowAnnuaire(){
        $(".btn-show-in-annuaire-spam-observatoire").off().on("click", function(){
            $(".map-container-spam-observatoire").hide();
            $(".container-liste-spam--spam-observatoire").show();
            $(".container-btn-shift-spam-observatoire").html(`
                <button type="button" class="btn-show-in-carte-spam-observatoire btn-show-map hidden-xs" title="Afficher sur la carte" alt="Afficher sur la carte">
                    <i class="fa fa-map-marker"></i> Carte
                </button>
            `);
            bindClickBtnShowCarte();
        });
        
    }

    function bindClickBtnShowCarte(){
        $(".btn-show-in-carte-spam-observatoire").off().on("click", function(){
            $(".container-liste-spam--spam-observatoire").hide();
            $(".map-container-spam-observatoire").show();
            
            $(".container-btn-shift-spam-observatoire").html(`
                <button type="button" class="btn-show-in-annuaire-spam-observatoire btn-show-map hidden-xs" title="Afficher sur le tableau" alt="Afficher sur le tableau">
                    <i class="fa fa-table"></i> Tableau
                </button>
            `);
            refreshListe();
            bindClickBtnShowAnnuaire();
        });
    }

    function bindClickBtnFilterTypeChange() {
        $(".btn-filter-type-change--spam-observatoire").off().on("click", function(){
            var type = $(this).data("type");
            var value = $(this).data("key");

            ($(this).hasClass("active")) ? $(this).removeClass("active") : $(this).addClass("active");

            addFilter(type, value);
        });
    }

    function refreshListe() {
        var count = 0;
        var result = {};

        if((typeof filterListe["region"] != "undefined" && filterListe["region"] != null && filterListe["region"].length > 0) || (typeof filterListe["city"] != "undefined" && filterListe["city"] != null && filterListe["city"].length > 0) || (typeof filterListe["country"] != "undefined" && filterListe["country"] != null && filterListe["country"].length > 0) || searchIp != "") {
            $.each(allIpSpam, function(key, ipSpam){
                if(searchIp != ""){
                    if(searchIp == ipSpam.ip){
                        result[count] = ipSpam;
                        count++;
                    }
                }
                else if  (   (typeof ipSpam.region != "undefined" && typeof filterListe["region"] != "undefined" && filterListe["region"] != null &&  filterListe["region"].includes(ipSpam.region)) 
                        || (typeof ipSpam.city != "undefined" && typeof filterListe["city"] != "undefined" && filterListe["city"] != null &&  filterListe["city"].includes(ipSpam.city)) 
                        ||(typeof ipSpam.country != "undefined" && typeof filterListe["country"] != "undefined" && filterListe["country"] != null &&  filterListe["country"].includes(ipSpam.country))  
                    )
                {
                    result[count] = ipSpam;
                    count++;
                }
            });
        }
        else {
            result = allIpSpam;
            $.each(allIpSpam, function(key, ipSpam){
                    count++;
            });
        }
        

        countString = (count == 0 || count == 1) ? count + " résultat"  : count + " résultats";
        $(".count-result-spam-observatoire").html(`<i class="fa fa-angle-down"></i> `+countString);

        refreshTableau(result);
        refreshMap(result);
    }

    function refreshMap(dataToShow) {
        $("#divMap").html("");
        var dataMap = []; 
        var paramsMapContent = {
            container : "divMap",
            activeCluster : true,
            zoom : 5,
            activePopUp : true
        };

        
        mapAbout = mapObj.init(paramsMapContent);
        $.each(dataToShow, function(key, val){
            if(typeof val.loc != "undefined"){
                var localisation = val.loc.split(",");
                if(localisation.length > 1){
                    var data = val;
                    data["name"] = val.ip;
                    data["collection"] = "ipSpam";
                    data["geo"] = {
                        "@type" : "GeoCoordinates",
                        "latitude" : localisation[0],
                        "longitude" : localisation[1]
                    };

                    dataMap.push(data);
                    /* mapAbout.addMarker({
                        elt:{
                            geo:{
                                "@type" : "GeoCoordinates",
                                "latitude" : localisation[0],
                                "longitude" : localisation[1]
                            }
                        }
                    }) */
                }
            }
            
        });
        mapAbout.addElts(dataMap);
        mapAbout.hideLoader();
    }

    function refreshTableau(data) {
        $("#tbody-tab-liste-ipSpam-spam-observatoire").html("");

        $.each(data,function(key, val){
            $("#tbody-tab-liste-ipSpam-spam-observatoire").append(addElementInTable(val));
        });
    }

    function removeFilter(type, value, indexOfvalue = null) {
        if(indexOfvalue == null && typeof filterListe[type] != "undefined" && filterListe[type] != null)
            indexOfvalue = filterListe[type].indexOf(value);

        if(typeof filterListe[type] != "undefined" && filterListe[type] != null && typeof filterListe[type][indexOfvalue] != "undefined" && filterListe[type][indexOfvalue] == value) {
            filterListe[type].splice(indexOfvalue, 1);
            $("#activeFilters-spam-observatoire #activeFilters").find(".filters-activate[data-key="+value+"]").remove();
            
            if($(".btn-filter-type-change--spam-observatoire[data-key="+value+"]").hasClass("active")) 
                $(".btn-filter-type-change--spam-observatoire[data-key="+value+"]").removeClass("active");
        }
    }
    

    jQuery(document).ready(function(){
        bindClickBtnShowCarte();
        bindClickBtnFilterTypeChange();
        btncChangeValueInInputSearchIp();
        showMap();
    });

    
</script>