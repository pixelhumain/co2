<?php

$cssAnsScriptFilesModule = array(
	'/plugins/floating-menu/floating-menu.css',
	'/plugins/floating-menu/floating-menu.min.js'
);
HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, Yii::app()->request->baseUrl);

HtmlHelper::registerCssAndScriptsFiles([
    '/css/coInput/co-input.css',
    '/js/coInput/co-input.js',
    '/js/coInput/co-input-types-css.js'
], Yii::$app->getModule('co2')->assetsUrl);
?>

<style>
    .co-input-switcher-tab-content .tab-content .form-group select, .co-input-switcher-tab-content .tab-content .form-group input {
        background: transparent;
        color: black;
    }
    #content-view-admin {
        padding-left: 55px;
    }
    .container-zone .zone-header .zone-title {
        text-align: center;
    }
    .container-zone .zone-content #countryList{
        overflow-y: auto;
        height: 65vh;
        margin: 0;
        padding: 0;
        list-style: none;
        padding-right: 10px;
    }
    .container-zone .zone-content #countryList li{
        padding: 10px;
        margin: 5px 0;
        cursor: pointer;
        border: 1px solid #666666;
        border-radius: 10px;
    }
    .container-zone .zone-content #countryList li:hover, .container-zone .zone-content #countryList li.active{
        background: white;
        border: none;
        box-shadow: 0px 0px 20px 0px rgb(0 0 0 / 16%);
    }
    #zoneList {
        list-style: none;
        margin: 0;
        padding: 0;
        height: 60vh;
        overflow-y: auto;
    }
    #zoneList li {
        padding: 10px;
        background-color: #e0e0e0;
        margin-bottom: 5px;
        border-radius: 5px;
        transition: background-color 0.3s;
    }
    #zoneList li:hover{
        background-color: #ccc;
        cursor: pointer;
    }
    #zoneList li.active, #affiche-container .zone-item.active {
        background-color: white;
        box-shadow: 0 4px 8px #07051a;
        border-radius: 4px;
        transition: box-shadow 0.3s ease;
        margin-bottom: 15px;
    }
    .zone-content .searchData{
        position: relative;
        width: 100%;
        margin: 0 auto 10px;
        height: 55px;
        border-radius: 20px;
        padding: 5px;
        box-sizing: border-box;
        border: 4px solid #07051a;
    }
    .zone-content .searchData .searchInput{
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 47.5px;
        line-height: 30px;
        outline: 0;
        border: 0;
        font-size: 1.2em;
        border-radius: 20px;
        padding: 0 10px;
    }
    .zone-content .searchData .fa{
        background: #07051a;
        color: white;
        box-sizing: border-box;
        padding: 15px;
        width: 49px;
        height: 49px;
        position: absolute;
        top: 0px;
        right: 0px;
        border-radius: 30%;
        text-align: center;
        font-size: 1.2em;
        transition: all 1s;
    }

    .zone-content .searchZoneContainer{
        position: relative;
        width: 85%;
        margin: 0 auto 10px;
        height: 50px;
        border-radius: 20px;
        padding: 5px;
        box-sizing: border-box;
        border: 4px solid #07051a;
    }
    .zone-content .searchZoneContainer .searchInput{
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 42.5px;
        line-height: 30px;
        outline: 0;
        border: 0;
        font-size: 1em;
        border-radius: 20px;
        padding: 0 10px;
    }
    .zone-content .searchZoneContainer .fa{
        background: #07051a;
        color: white;
        box-sizing: border-box;
        padding: 15px;
        width: 42.5px;
        height: 42.5px;
        position: absolute;
        top: 0px;
        right: 0px;
        border-radius: 30%;
        text-align: center;
        font-size: 1.2em;
        transition: all 1s;
    }
    button {
        font-family: Arial, sans-serif;
        font-size: 16px;
        padding: 10px 15px;
        border: none;
        border-radius: 5px;
        cursor: pointer;
        transition: background-color 0.3s ease, transform 0.3s ease;
    }

    .btn-zone-ratache {
        background-color: #4CAF50; 
        color: white; 
        box-shadow: 0 4px 6px rgba(0, 0, 0, 0.1);
        margin-left: 25px; 
        padding: 4px 7px !important;
    }

    .btn-zone-ratache:hover {
        background-color: #45a049; 
        transform: scale(1.05);
    }

    .btn-zone-ratache:active {
        background-color: #3e8e41; 
        transform: scale(0.95);
    }

    .btn-zone-ratache:focus {
        outline: none; 
        box-shadow: 0 0 5px #4CAF50;
    }
    #affiche-container {
        padding: 20px;
        border: 1px solid #ccc;
        border-radius: 8px;
        background-color: #f9f9f9;
        margin-top: 20px;
    }

    #affiche-container h4 {
        text-align: center;
        color: #333;
        margin-bottom: 20px;
    }

    #affiche-container .label_zone {
        cursor: pointer;
        font-weight: bold;
        color: #555;
        padding: 10px;
        border-bottom: 2px solid #ddd;
        margin-bottom: 10px;
        position: relative;
    }

    #affiche-container .label_zone:hover {
        background-color: #e6e6e6;
    }

    #affiche-container .label_zone::after {
        content: '▼';
        position: absolute;
        right: 10px;
        font-size: 12px;
        transition: transform 0.3s;
    }

    #affiche-container .label_zone.open::after {
        content: '▲';
        transform: rotate(360deg);
    }

    #affiche-container .ul_zone {
        list-style-type: none;
        padding: 0;
        margin: 0 0 20px 0;
        display: none; 
        transition: max-height 0.5s ease-in-out, padding 0.5s ease-in-out;
        max-height: 0; 
        padding-right: 10px;
    }

    #affiche-container .ul_zone.visible {
        display: block;
        overflow-y: auto;  
        overflow-x: hidden; 
        max-height: 200px;
        padding: 10px;
    }
    #affiche-container .zone-item {
        padding: 10px;
        background-color: #e0e0e0;
        margin-bottom: 5px;
        border-radius: 5px;
        transition: background-color 0.3s;
    }

    #affiche-container .zone-item:hover {
        background-color: #ccc;
        cursor: pointer;
    }
    #information-container {
        font-family: Arial, sans-serif;
        color: #333;
        background-color: #f9f9f9;
        padding: 20px;
        border-radius: 8px;
        box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1);
        max-width: 600px;
    }
    .profile-header {
        display: flex;
        align-items: center;
        background-color: #4CAF50;
        color: white;
        padding: 15px;
        border-radius: 8px 8px 0 0;
    }
    .profile-info {
        margin-left: 10px;
    }
    .profil-name {
        margin: 0;
        font-size: 24px;
        font-weight: bold;
    }
    .profil-description {
        margin: 5px 0 0;
        font-size: 16px;
    }
    .zone-info, .address-info {
        margin-top: 20px;
        background-color: white;
        padding: 15px;
        border-radius: 8px;
        border: 1px solid #ddd;
    }
    .zone-info h3, .address-info h3 {
        margin-top: 0;
        font-size: 18px;
        border-bottom: 2px solid #4CAF50;
        padding-bottom: 5px;
    }
    .zone-info p, .address-info p {
        margin: 10px 0;
        line-height: 1.5;
    }
    strong {
        font-weight: bold;
        color: #4CAF50;
    }
    /* #information-container div:hover {
        box-shadow: 0 4px 8px rgba(0, 0, 0, 0.2);
        transition: box-shadow 0.3s ease;
    } */
    .btn-change-country {
        margin-left: 10px;
        cursor: pointer;
    }
    .profile-button {
        display: inline-block;
        margin: 10px 5px 0 0;
        padding: 8px 12px;
        background-color: #ffffff;
        color: #4CAF50;
        font-size: 14px;
        font-weight: bold;
        text-align: center;
        border-radius: 5px;
        cursor: pointer;
        border: 1px solid #4CAF50;
        transition: background-color 0.3s, color 0.3s;
    }

    .profile-button i {
        margin-right: 5px;
    }

    .profile-button:hover {
        background-color: #4CAF50;
        color: white;
    }

    .profile-button:active {
        background-color: #45a049;
    }

    .btn-group{
        width: 100%;
    }

    .btn-group-vertical>.btn, .btn-group>.btn {
        width: 33.33%;
    }

    /* Responsivité pour les petits écrans */
    @media (max-width: 600px) {
        .profile-header {
            flex-direction: column;
            align-items: flex-start;
        }

        .profile-button {
            width: 100%;
            text-align: center;
            margin-top: 5px;
        }
    }
    .leaflet-popup-content {
        min-width: 300px !important;
    }

    .zone-modal{
        margin: 45px 0 auto;
    }
    .zone-modal .modal-dialog {
        margin: 20px 0 auto;
        width: 100%;
    }
    .zone-modal .modal-dialog .btn-close-modal{
        border-radius: 50px;
        border: 4px solid #f5f5f5;
        font-size: 16pt;
    }
</style>
<div class="container-zone">
    <div class="zone-header">
        <h3 class="zone-title">Administration des zones 
            <span id="countrySelected"></span> 
            <span class="btn-change-country hidden" title="Changer le Pays" id="change-country"><i class="fa fa-exchange"></i></span>
        </h3>
    </div>
    <div class="zone-content">
        <div class="row">
            <div class="col-md-4 col-lg-3 col-sm-6" id="countryContainer">
                <h3 class="text-center">Liste des pays</h3>
                <div class="searchData">
                    <input type="text" class="searchInput" name="search" id="search" placeholder="Search Country">
                    <i class="fa fa-search"></i>
                </div>
                <ul id="countryList">
                    <!-- <?php foreach ($pays as $key => $itemPays) { ?>
                        <li data-key="<?= $itemPays["countryCode"] ?>"><?= $itemPays["name"] ?></li>
                    <?php } ?> -->
                </ul>
            </div>
            <div class="col-md-4 col-lg-3 col-sm-6 hidden" id="zoneList-container">
                <div class="form-group">
                    <label for="">Niveau de zone</label>
                    <select name="level" id="level" class="form-control">

                    </select>
                </div>
                <button class="profile-button" id="navigate-map"><i class="fa fa-map"></i> Naviger en utilisant la carte</button>
                <hr>
                <div class="zoneContainer hidden">
                    <div class="searchZoneContainer">
                        <input type="text" class="searchInput" name="searchZone" id="searchZone" placeholder="Search Zone">
                        <i class="fa fa-search"></i>
                    </div>
                    <p>Liste des zones niveau <span id="titleLevel"></span> d'(e) <span id="countryLevel"></span></p>
                    <ul id="zoneList">
                    </ul>
                </div>
            </div>
            <div class="col-md-4 col-lg-5 col-sm-6 hidden" id="information-container">
                
            </div>
            <div class="col-md-4 col-lg-4 col-sm-6 hidden" id="editZone-container">
                
            </div>
            <div class="col-md-4 col-lg-4 col-sm-6 hidden" id="affiche-container">
    
            </div>
        </div>
    </div>
    <div class="map-content hidden">
        <div class="row">
            <div class="col-xs-12" id="map-content-zone" style="height: 75vh;">

            </div>
        </div>
    </div>
    <div class="modal fade zone-modal">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-body">
                    <button data-dismiss="modal" class="btn btn-default pull-right btn-close-modal">
                        <i class="fa fa-times"></i>
                    </button>
                    <div class="row">
                        <div class="col-md-4 col-lg-5 col-sm-6 hidden" id="information-container">
                        
                        </div>
                        <div class="col-md-4 col-lg-4 col-sm-6 hidden" id="editZone-container">
                            
                        </div>
                        <div class="col-md-4 col-lg-4 col-sm-6 hidden" id="affiche-container">
                
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://cdn.jsdelivr.net/npm/@turf/turf@7/turf.min.js"></script>
<script>
    var ajaxStart = false;
    $(document).ajaxStart(function() {
        ajaxStart = true;
    });

    $(document).ajaxStop(function() {
        ajaxStart = false;
    });
    let countryList = <?= json_encode($pays ?? []) ?>;
    var adminZone = {
        country: {},
        containerCountry : "#countryList",
        selected: undefined,
        zones: {},
        level: undefined,
        init: function(pInit){
            if(pInit.country){
                this.country = $.extend({}, pInit.country, true);
            }
            if(pInit.container){
                this.containerCountry = pInit.container;
            }
            this.views.init(this);
            this.events.init(this);
        },
        helpers: {
            getLevelCorrespondance : function(level, zone){
                let niveau = {};
                if(zone.countryCode == "MG"){
                    niveau = {
                        "2" : "Province",
                        "3" : "Région",
                        "4" : "District",
                        "5" : "Niveau 5"
                    };
                }else if(zone.countryCode == "BE"){
                    niveau = {
                        "2" : "Région",
                        "3" : "Province",
                        "4" : "Arrondissement",
                        "5" : "Niveau 5"
                    };
                }else if(zone.countryCode == "FR"){
                    niveau = {
                        "2" : "Province",
                        "3" : "Région",
                        "4" : "Département",
                        "5" : "EPCI"
                    };
                }else{
                    niveau = {
                        "2" : "Niveau 2",
                        "3" : "Niveau 3",
                        "4" : "Niveau 4",
                        "5" : "Niveau 5"
                    };
                }
                niveau["1"] = "Pays";
                return niveau[level];
            },
            getElements: function(countryCode, getdata, parentId, level, callback = () => {}, extraParams = {}){
                ajaxPost(
                    null,
                    baseUrl+"/co2/zone/getshape",
                    {
                        countryCode : countryCode,
                        level : getdata,
                        parentId : parentId,
                        parentLevel: level
                    },
                    function(success){
                        callback(success, getdata);
                    },
                    null,
                    "json",
                    {
                        ...extraParams
                    }
                );
            },
            getNextLevel: function(zoneObj, level){
                let defaultLevel = ["2", "3", "4", "5", "cities", "QPV"];
                if(zoneObj.selected == "FR" || zoneObj.selected == "RE" || zoneObj.selected == "GF"  || zoneObj.selected == "YT"  || zoneObj.selected == "MQ"  || zoneObj.selected == "GP"){
                   defaultLevel.shift();
                }else if(zoneObj.selected == "MG"){
                    defaultLevel = defaultLevel.filter(item => (item !== "5" && item != "QPV"));
                }
                let index = defaultLevel.indexOf(level);
                mylog.log("Level, index, defaultLevel", level, index, defaultLevel, zoneObj.selected);
                if(index != -1 && (index +1 <= defaultLevel.length - 1)){
                    return defaultLevel[index+1];
                }
                return level;
            },
            getPreviousLevel: function(zoneObj, level){
                let defaultLevel = ["2", "3", "4", "5", "cities", "QPV"];
                if(zoneObj.selected == "FR" || zoneObj.selected == "RE" || zoneObj.selected == "GF"  || zoneObj.selected == "YT"  || zoneObj.selected == "MQ"  || zoneObj.selected == "GP"){
                   defaultLevel.shift();
                }else if(zoneObj.selected == "MG"){
                    defaultLevel = defaultLevel.filter(item => (item !== "5" && item != "QPV"));
                }
                let index = defaultLevel.indexOf(level);
                mylog.log("Level, index, defaultLevel", level, index, defaultLevel, zoneObj.selected);
                if(index != -1 && (index - 1 >= 0)){
                    return defaultLevel[index-1];
                }
                return level;
            },
            isLastLevel: function(zoneObj, level){
                let defaultLevel = ["2", "3", "4", "5", "cities", "QPV"];
                if(zoneObj.selected == "FR" || zoneObj.selected == "RE" || zoneObj.selected == "GF"  || zoneObj.selected == "YT"  || zoneObj.selected == "MQ"  || zoneObj.selected == "GP"){
                   defaultLevel.shift();
                }else if(zoneObj.selected == "MG"){
                    defaultLevel = defaultLevel.filter(item => (item !== "5" && item != "QPV"));
                }
                return (defaultLevel.indexOf(level) == defaultLevel.length - 1);
            },
            isFirstLevel: function(zoneObj, level){
                let defaultLevel = ["2", "3", "4", "5", "cities", "QPV"];
                if(zoneObj.selected == "FR" || zoneObj.selected == "RE" || zoneObj.selected == "GF"  || zoneObj.selected == "YT"  || zoneObj.selected == "MQ"  || zoneObj.selected == "GP"){
                   defaultLevel.shift();
                }else if(zoneObj.selected == "MG"){
                    defaultLevel = defaultLevel.filter(item => (item !== "5" && item != "QPV"));
                }
                return (defaultLevel.indexOf(level) == 0);
            },
        },
        events: {
            init: function (zoneObj) {
                this.search(zoneObj);
                this.country(zoneObj);
                this.zone(zoneObj);
            },
            search: function(zoneObj){
                $("#search").on("keyup", function(){
                    let search = $(this).val().trim();
                    let result = '';
                    $.each(Object.fromEntries(Object.entries(zoneObj.country).filter(([key, value]) => value.name.toLowerCase().includes(search.toLowerCase()))), function(k, v){
                        result += `<li data-key="${v.countryCode}" ${typeof zoneObj.selected != "undefined" && zoneObj.selected == v.countryCode ? "class='active'" : ""}>${v.name}</li>`;
                    });
                    $(zoneObj.containerCountry).html(result);
                    zoneObj.events.country(zoneObj);
                });
            },
            searchZone: function(zoneObj){
                $("#searchZone").on("keyup", function(){
                    let search = $(this).val().trim();
                    let result = '';
                    $.each(Object.fromEntries(Object.entries(zoneObj.zones).filter(([key, value]) => value.name.toLowerCase().includes(search.toLowerCase()))), function(k, zone){
                        result += zoneObj.views.zone(zoneObj, zone);
                    });
                    $("#zoneList").html(result);
                    zoneObj.events.zoneMenu(zoneObj);
                    zoneObj.events.zoneInfo(zoneObj);
                    // zoneObj.events.country(zoneObj);
                });
            },
            country : function(zoneObj){
                $("#countryList > li").click(function(){
                    $("#level").html('');
                    if(zoneObj.selected != $(this).data("key")){
                        $("#countryList > li").removeClass("active");
                        zoneObj.selected = $(this).data("key");
                        var tempZone = [];
                        tempZone.countryCode = zoneObj.selected;
                        var optionNiveau = `<option value="" disabled>Selectioner un niveau</option>`;
                        for(var i = 2; i < 6; i++) {
                            let label = zoneObj.helpers.getLevelCorrespondance(i, tempZone);
                            optionNiveau += `<option value="${i}">${label}</option>`;
                        }

                        $("#level").html(optionNiveau);
                        
                        $(this).addClass("active");
                        $("#countryLevel").text($(this).text());
                        $("#countrySelected").text($(this).text());
                        $("#countrySelected").attr("data-country", zoneObj.selected);
                        $("#level").val('');
                        $("#zoneList-container").removeClass("hidden");
                        $(".zoneContainer").addClass("hidden");
                        $("#countryContainer").addClass("hidden");
                        $("#affiche-container").addClass('hidden');
                        $("#change-country").removeClass("hidden");
                        $("#search").val('');
                        $("#search").trigger("keyup");
                        zoneObj.events.map(zoneObj);
                    }
                });

                $("#change-country").on("click", function(){
                    $("#countryContainer").removeClass("hidden");
                    $("#affiche-container").addClass('hidden');
                    $("#information-container").addClass('hidden');
                    $("#editZone-container").addClass('hidden');
                    $("#zoneList-container").addClass("hidden");
                    $("#countrySelected").html("");
                    $(".zone-content").removeClass("hidden");
                    $(".map-content").addClass("hidden");
                    zoneObj.selected = '';
                    $(this).addClass('hidden');
                });
            },
            zone: function(zoneObj){
                $("#level").on("change", function(){
                    var country = zoneObj.selected;
                    zoneObj.level = $(this).val();
                    $("#titleLevel").text($(this).val());
                    $("#affiche-container").addClass('hidden');
                    $("#information-container").addClass('hidden');
                    ajaxPost(
                        null,
                        baseUrl+"/co2/zone/getzone",
                        {
                            countryCode : country,
                            level: [$(this).val()]
                        },
                        function(data){
                            $(".zoneContainer").removeClass("hidden");
                            let str = "";
                            zoneObj.zones = data;
                            if(Object.keys(data).length > 0){
                                $.each(data, function(k, v){
                                    str += zoneObj.views.zone(zoneObj, v);
                                });
                            }else{
                                str = `<li>Il n'y a pas des resultats</li>`;
                            }
                            $("#zoneList").html(str);
                            zoneObj.events.searchZone(zoneObj)
                            zoneObj.events.zoneInfo(zoneObj)
                        }
                    );
                });
            },
            zoneInfo: function(zoneObj){
                $(".zone-item").on("click", function(){
                    let _id = $(this).attr("data-key");
                    let _section = $(this).attr("data-section");
                    let _level = $(this).attr("data-level");
                    let zone = $.extend({}, zoneObj.zones[_id], true);
                    $(".zone-item").removeClass("active");
                    $("#zone_"+_id).addClass("active");
                    $("#editZone-container").addClass('hidden');
                    zoneObj.views.info(zoneObj, zone, _section, _level);
                    zoneObj.events.zoneMenu(zoneObj);
                });
            },
            zoneMapInfo: function(zoneObj){
                $(".zone-modal .zone-item").on("click", function(){
                    let _id = $(this).attr("data-key");
                    let _section = $(this).attr("data-section");
                    let _level = $(this).attr("data-level");
                    let zone = $.extend({}, zoneObj.zones[_id], true);
                    $(".zone-item").removeClass("active");
                    $("#zone_"+_id).addClass("active");
                    $(".zone-modal #editZone-container").addClass('hidden');
                    zoneObj.views.info(zoneObj, zone, _section, _level);
                    zoneObj.events.zoneMapMenu(zoneObj);
                });
            },
            zoneMenu: function(zoneObj){
                $("#edit-info").on("click", function(){
                    let _id = $(this).attr("data-key");
                    let zone = $.extend({}, zoneObj.zones[_id], true);
                    $("#editZone-container").html(`<form id='zone-edti-form' style='padding:0px 10px;'>
                                    <div class="container-form-zone col-xs-12"></div>
                                </form>`);
                    $("#affiche-container").addClass('hidden');
                    $("#editZone-container").removeClass("hidden");
                    var myCoInput = {
                        container : ".container-form-zone",
                        inputs :[
                            {
                                type : "inputSimple",
                                options: {
                                    label : "Nom",
                                    name : "name",
                                    defaultValue: zone.name
                                },
                            },
                            {
                                type : "selectMultiple",
                                options : {
                                    name : "level",
                                    label : "Niveau du zone",
                                    options : [
                                        {label : "Niveau 1", value : "1"},
                                        {label : "Niveau 2", value : "2"},
                                        {label : "Niveau 3", value : "3"},
                                        {label : "Niveau 4", value : "4"},
                                        {label : "Niveau 5", value : "5"},
                                        // {label : "Niveau 6", value : "6"},
                                    ],
                                    defaultValue: zone.level
                                }
                            },
                            {
                                type : "inputSimple",
                                options: {
                                    label : "Identification sur OpenStreetMap",
                                    name : "osmID",
                                    defaultValue: zone.osmID
                                },
                            },
                            {
                                type : "inputSimple",
                                options: {
                                    label : "Identification sur Wikidata",
                                    name : "wikidataID",
                                    defaultValue: zone.wikidataID
                                },
                            },
                        ],
                        onchange:function(name, value, payload){
                            zone[name] = typeof value == "string" ? value.trim() : value;
                            $("#zone-edti-form #btn-submit-form").attr('disabled', JSON.stringify(zone) == JSON.stringify(zoneObj.zones[_id]));
                        }
                    }
                    
                    new CoInput(myCoInput);
                    $("#zone-edti-form").append(`<div class="container-form-zone col-xs-12" id='divMapLocality' style='height: 300px;'></div>`);
                    var paramsMarker = {
                        elt : {
                            id : 0,
                            type : "zones",
                            geo : {
                                latitude : 0,
                                longitude: 0
                            }
                        },
                        opt : {
                            draggable: true
                        },
                        center : true
                    };
                    if(typeof zone.geo != "undefined" || typeof zone.geoShape != "undefined"){
                        if(typeof zone.geo == "undefined" && typeof zone.geoPosition != "undefined"){
                            zone.geo = {
                                "@type" : "GeoCoordinates",
                                latitude :  zone.geoPosition.coordinates[1],
                                longitude : zone.geoPosition.coordinates[0]
                            }
                        }
                        paramsMarker.elt.geo.latitude = zone.geo.latitude;
                        paramsMarker.elt.geo.longitude = zone.geo.longitude;
                    }
                    var paramsMap = {
                        container : "divMapLocality",
                        latLon : [ 39.74621, -104.98404],
                        activeCluster : false,
                        zoom : 16,
                        activePopUp : false
                    }
                    let map = mapObj.init(paramsMap);

                    map.addMarker(paramsMarker);
                    map.markerList[0].on('drag', function (e) {
                        var latLonMarker = map.markerList[0].getLatLng();
                        //mylog.log('marker drag event', latLonMarker);
                        zone.latitude = latLonMarker.lat;
                        zone.longitude = latLonMarker.lng;
                        $("#zone-edti-form #btn-submit-form").attr('disabled', JSON.stringify(zone) == JSON.stringify(zoneObj.zones[_id]));
                    });
                    map.hideLoader();
                    $("#zone-edti-form").append(`<div class="zones-tabs-footer"><div class="col-xs-12 no-padding" style="margin-top:15px;margin-bottom:15px;padding-bottom: 50px !important">
                        <a href="javascript:;" class="btn btn-danger col-xs-12 " id="btn-submit-form" disabled="true"><i class="fa fa-save"></i> `+tradCms.register+`</a>
                    </div></div>`);
                    $("#zone-edti-form #btn-submit-form").click(function() {
                        var tplCtx = {
                            id : _id,
                            collection : 'zones',
                            path : 'allToRoot',
                            value : {},
                        }
                        $.each(zone, function(k, v){
                            if(k == "_id"){
                                return;
                            }
                            tplCtx.value[k] = v;
                        });
                        ajaxPost(
                            null,
                            baseUrl+"/co2/zone/update",
                            tplCtx,
                            function(success){
                                let str = '';
                                if(zone.longitude && zone.latitude){
                                    zone.geo = {
                                        "@type" : "GeoCoordinates",
                                        latitude :  zone.latitude,
                                        longitude : zone.longitude
                                    }
                                    zone.geoPosition = {
                                        "type": "Point",
                                        "float": "true",
                                        "coordinates": [
                                            zone.longitude,
                                            zone.latitude
                                        ]
                                    }
                                    delete zone.latitude;
                                    delete zone.longitude;
                                }
                                zoneObj.zones[_id] = $.extend({}, zone, true);
                                $.each(zoneObj.zones, function(k, v){
                                    str += zoneObj.views.zone(zoneObj, v);
                                });
                                $("#zoneList").html(str);
                                zoneObj.events.searchZone(zoneObj)
                                zoneObj.events.zoneMenu(zoneObj)
                                zoneObj.events.zoneInfo(zoneObj)
                                $("#zone-edti-form #btn-submit-form").attr('disabled', JSON.stringify(zone) == JSON.stringify(zoneObj.zones[_id]));
                                toastr.success("Zone modifié avec succès");
                                $("#editZone-container").addClass("hidden");
                            }
                        );
                    });
                });
                $("#merge-to").on("click", function(){
                    let _id = $(this).attr("data-key");
                    let level = $(this).data("level");
                    let zone = $.extend({}, zoneObj.zones[_id], true);
                    $("#editZone-container").html(`<form id='zone-edti-form' style='padding:0px 10px;'>
                                    <div class="container-form-zone col-xs-12"></div>
                                </form>`);
                    $("#affiche-container").addClass('hidden');
                    $("#editZone-container").removeClass("hidden");
                    let options = [];
                    let zoneSelected = "";
                    $.each(Object.fromEntries(Object.entries(zoneObj.zones).filter(([key, value]) => key != _id)),function(k, v){
                        options.push({label: v.name, value: k});
                    });
                    var myCoInput = {
                        container : ".container-form-zone",
                        inputs :[
                           {
                                type : "select",
                                options : {
                                    name : "zone",
                                    label : "Choisir une zone pour la fusion",
                                    isSelect2: true,
                                    options : options,
                                }
                            },
                        ],
                        onchange:function(name, value, payload){
                            zoneSelected = value;
                            $("#zone-edti-form #btn-submit-form").attr('disabled', zoneSelected == "" || zoneSelected == null);
                        }
                    }
                    
                    new CoInput(myCoInput);
                    $("#zone-edti-form").append(`<div class="zones-tabs-footer"><div class="col-xs-12 no-padding" style="margin-top:15px;margin-bottom:15px;padding-bottom: 50px !important">
                        <a href="javascript:;" class="btn btn-danger col-xs-12 " id="btn-submit-form" disabled="true"><i class="fa fa-save"></i> `+tradCms.register+`</a>
                    </div></div>`);
                    $("#zone-edti-form #btn-submit-form").click(function() {
                        ajaxPost(
                            null,
                            baseUrl+"/co2/zone/combinezone",
                            {
                                zoneMerge : _id,
                                zoneDestination : zoneSelected,
                                level: level
                            },
                            function (data) {
                                if(data.success){
                                    let str = '';
                                    delete zoneObj.zones[_id];
                                    $.each(zoneObj.zones, function(k, v){
                                        str += zoneObj.views.zone(zoneObj, v);
                                    });
                                    $("#zoneList").html(str);
                                    zoneObj.events.searchZone(zoneObj)
                                    zoneObj.events.zoneMenu(zoneObj)
                                    zoneObj.events.zoneInfo(zoneObj)
                                    $("#zone-edti-form #btn-submit-form").attr('disabled', false);
                                    $("#information-container").addClass('hidden');
                                    toastr.success(data.msg);
                                    $("#editZone-container").addClass("hidden");
                                    $(`.zone-item[data-key=${zoneSelected}]`).trigger("click");
                                }else{
                                    toastr.error(data.msg);
                                }
                            },
                            null,
                            null,
                            {async:false}
                        );
                    });
                });
                $("#edit-parent").on("click", function(){
                    let _id = $(this).attr("data-key");
                    let level = parseInt($(this).attr("data-level")) - 1;
                    let zone = $.extend({}, zoneObj.zones[_id], true);
                    let levelSelected = "";
                    $("#editZone-container").html(`<form id='zone-edti-form' style='padding:0px 10px;'>
                        <div class="container-form-zone col-xs-12"></div>
                    </form>`);
                    $("#affiche-container").addClass('hidden');
                    $("#editZone-container").removeClass("hidden");
                    let zoneData = {};
                    let parentOptions = [];

                    ajaxPost(
                        null,
                        baseUrl+"/co2/zone/getzone",
                        {
                            countryCode : zoneObj.selected,
                            level: [level]
                        },
                        function(data){
                            zoneData = data;
                            if(Object.keys(data).length > 0){
                                $.each(data, function(k, v){
                                    parentOptions.push({label: v.name, value: k});
                                });
                            }
                        },
                        null,
                        null,
                        {async:false}
                    );
                    var myCoInput = {
                        container : ".container-form-zone",
                        inputs :[
                            {
                                type : "select",
                                options : {
                                    name : "level",
                                    label : "Niveau du zone",
                                    options : [
                                        {label : "Niveau 1", value : "1"},
                                        {label : "Niveau 2", value : "2"},
                                        {label : "Niveau 3", value : "3"},
                                        {label : "Niveau 4", value : "4"},
                                        {label : "Niveau 5", value : "5"},
                                        // {label : "Niveau 6", value : "6"},
                                    ],
                                    defaultValue: level.toString()
                                }
                            },
                            {
                                type : "select",
                                options: {
                                    label : "Liste des parents",
                                    name : "parent",
                                    options : parentOptions,
                                    defaultValue : zone[`level${level}`]
                                },
                            },
                        ],
                        onchange:function(name, value, payload){
                            if(name == "level"){
                                levelSelected = value;
                                let zoneParams = {
                                    countryCode : zoneObj.selected,
                                    level: [value]
                                };

                                if(value == "1"){
                                    delete zoneParams.countryCode;
                                }
                                ajaxPost(
                                    null,
                                    baseUrl+"/co2/zone/getzone",
                                    zoneParams,
                                    function(data){
                                        zoneData = data;
                                        parentOptions = [];
                                        new Promise((resolve, reject) => {
                                            if(Object.keys(data).length > 0){
                                                $.each(data, function(k, v){
                                                    parentOptions.push({label: v.name, value: k});
                                                });
                                            }
                                            resolve();
                                        }).then((valuePromise) => {
                                            myCoInput.inputs[0].options.defaultValue = value;
                                            myCoInput.inputs[1].options.options = parentOptions;
                                            new CoInput(myCoInput);
                                        })
                                    }
                                );
                            }
                            if(name == "parent"){
                                $.each(zone, function(index, valeur){
                                    if(index.indexOf("level") > -1 && (index != "level")){
                                        delete zone[index];
                                    }
                                });
                                $.each(zoneData[value], function(k, v){
                                    if(k.indexOf("level") > -1 && (k != "level")){
                                        zone[k] = v;
                                    }
                                });
                                if(levelSelected != ""){
                                    zone[`level${levelSelected}`] = value;
                                    zone[`level${levelSelected}Name`] = zoneData[value].name;
                                }else{
                                    zone[`level${level}`] = value;
                                    zone[`level${level}Name`] = zoneData[value].name;
                                }
                                mylog.log('Zone edited', zone);
                            }
                            $("#zone-edti-form #btn-submit-form").attr('disabled', JSON.stringify(zone) == JSON.stringify(zoneObj.zones[_id]));
                        }
                    };
                    new CoInput(myCoInput);

                    $("#zone-edti-form").append(`<div class="zones-tabs-footer"><div class="col-xs-12 no-padding" style="margin-top:15px;margin-bottom:15px;padding-bottom: 50px !important">
                        <a href="javascript:;" class="btn btn-danger col-xs-12 " id="btn-submit-form" disabled="true"><i class="fa fa-save"></i> `+tradCms.register+`</a>
                    </div></div>`);
                    $("#zone-edti-form #btn-submit-form").click(function() {
                        var tplCtx = {
                            id : _id,
                            collection : 'zones',
                            path : 'allToRoot',
                            value : {},
                        }
                        $.each(zone, function(k, v){
                            if(k == "_id"){
                                return;
                            }
                            tplCtx.value[k] = v;
                        });
                        ajaxPost(
                            null,
                            baseUrl+"/co2/zone/update",
                            tplCtx,
                            function(success){
                                let str = '';
                                zoneObj.zones[_id] = $.extend({}, zone, true);
                                $.each(zoneObj.zones, function(k, v){
                                    str += zoneObj.views.zone(zoneObj, v);
                                });
                                $("#zoneList").html(str);
                                zoneObj.events.searchZone(zoneObj)
                                zoneObj.events.zoneMenu(zoneObj)
                                zoneObj.events.zoneInfo(zoneObj)
                                $("#zone-edti-form #btn-submit-form").attr('disabled', JSON.stringify(zone) == JSON.stringify(zoneObj.zones[_id]));
                                toastr.success("Zone modifié avec succès");
                                $("#editZone-container").addClass("hidden");
                            }
                        );
                    });
                });
                $("#edit-contour").on("click", function(){
                    let _id = $(this).attr("data-key");
                    let zone = $.extend({}, zoneObj.zones[_id], true);
                    $("#editZone-container").html(`<form id='zone-edti-form' style='padding:0px 10px;'>
                                    <div class="container-form-zone col-xs-12"></div>
                                </form>`);
                    $("#affiche-container").addClass('hidden');
                    $("#editZone-container").removeClass("hidden");
                    let myCoInput = {
                        container : ".container-form-zone",
                        inputs: [
                            {
                                type : "inputSwitcher",
                                options : {
                                    name : "switch",
                                    label : "GeoJSON du contour",
                                    tabs: [
                                        {
                                            value:"json",  
                                            label: "Texte",
                                            inputs: [
                                                {
                                                    type: "textarea",
                                                    options: {
                                                        name: "text",
                                                        viewPreview : true
                                                    }
                                                }
                                            ]
                                        },
                                        {
                                            value:"file"  , 
                                            label: "Fichier",   
                                            inputs: [
                                                {
                                                    type : "inputFile",
                                                    options: {
                                                        name : "jsonfile",
                                                        viewPreview : true
                                                    }
                                                }
                                            ]
                                        }
                                    ]
                                }
                            },
                        ],
                        onchange:function(name, value, payload){
                            if(name == "text"){
                                try {
                                    let geoJsonObj = JSON.parse(value);
                                    if(typeof geoJsonObj != "undefined" && ((typeof geoJsonObj.geometry != "undefined" && typeof geoJsonObj.geometry.type != "undefined" && typeof geoJsonObj.geometry.coordinates != "undefined") || (typeof geoJsonObj.type != "undefined" && typeof geoJsonObj.coordinates != "undefined"))){
                                        if(typeof geoJsonObj.geometry != "undefined"){
                                            zone.geoShape = geoJsonObj.geometry;
                                        }else{
                                            zone.geoShape = geoJsonObj;
                                        }
                                    }else{
                                        toastr.error("Veuillez inserer un GeoJSON valide pour modifier les contours du zone");
                                    }
                                } catch (error) {
                                    toastr.error("Veuillez inserer de texte json valide");
                                }
                            }
                            if(name == "jsonfile"){
                                let fileReader = new FileReader();
                                fileReader.onload = function(){
                                    try {
                                        let geoJsonObj = JSON.parse(fileReader.result);
                                        if(typeof geoJsonObj != "undefined" && ((typeof geoJsonObj.geometry != "undefined" && typeof geoJsonObj.geometry.type != "undefined" && typeof geoJsonObj.geometry.coordinates != "undefined") || (typeof geoJsonObj.type != "undefined" && typeof geoJsonObj.coordinates != "undefined"))){
                                            if(typeof geoJsonObj.geometry != "undefined"){
                                                zone.geoShape = geoJsonObj.geometry;
                                            }else{
                                                zone.geoShape = geoJsonObj;
                                            }
                                        }else{
                                            toastr.error("Veuillez inserer un GeoJSON valide pour modifier les contours du zone");
                                        }
                                    } catch (error) {
                                        toastr.error("Veuillez inserer un fichier json valide");
                                    }
                                }
                                fileReader.readAsText(value);
                            }
                            $("#zone-edti-form #btn-submit-form").attr('disabled', JSON.stringify(zone) == JSON.stringify(zoneObj.zones[_id]));
                        }
                    };
                    new CoInput(myCoInput);
                    $("#zone-edti-form").append(`<div class="zones-tabs-footer"><div class="col-xs-12 no-padding" style="margin-top:15px;margin-bottom:15px;padding-bottom: 50px !important">
                        <a href="javascript:;" class="btn btn-danger col-xs-12 " id="btn-submit-form" disabled="true"><i class="fa fa-save"></i> `+tradCms.register+`</a>
                    </div></div>`);
                    $("#zone-edti-form #btn-submit-form").click(function() {
                        var tplCtx = {
                            id : _id,
                            collection : 'zones',
                            path : 'allToRoot',
                            value : {},
                        }
                        $.each(zone, function(k, v){
                            if(k == "_id"){
                                return;
                            }
                            tplCtx.value[k] = v;
                        });
                        ajaxPost(
                            null,
                            baseUrl+"/co2/zone/update",
                            tplCtx,
                            function(success){
                                let str = '';
                                if(zone.longitude && zone.latitude){
                                    zone.geo = {
                                        "@type" : "GeoCoordinates",
                                        latitude :  zone.latitude,
                                        longitude : zone.longitude
                                    }
                                    zone.geoPosition = {
                                        "type": "Point",
                                        "float": "true",
                                        "coordinates": [
                                            zone.longitude,
                                            zone.latitude
                                        ]
                                    }
                                    delete zone.latitude;
                                    delete zone.longitude;
                                }
                                zoneObj.zones[_id] = $.extend({}, zone, true);
                                $.each(zoneObj.zones, function(k, v){
                                    str += zoneObj.views.zone(zoneObj, v);
                                });
                                $("#zoneList").html(str);
                                zoneObj.events.searchZone(zoneObj)
                                zoneObj.events.zoneMenu(zoneObj)
                                $("#zone-edti-form #btn-submit-form").attr('disabled', JSON.stringify(zone) == JSON.stringify(zoneObj.zones[_id]));
                                toastr.success("Zone modifié avec succès");
                                $("#editZone-container").addClass("hidden");
                            }
                        );
                    });
                });
                $("#info-coherence").on("click", function(){
                    let _id = $(this).attr("data-key");
                    let _level = $(this).attr("data-level");
                    let _name = $(this).attr("data-name");
                    let params = {
                        "zoneId" : _id,
                        "level" : _level,
                        "name" : _name
                    }
                    urlCtrl.openPreview("/view/url/co2.views.zones.zoneCoherence", {"params": params});
                });
            },
            zoneMapMenu: function(zoneObj){
                $(".zone-modal #edit-info").on("click", function(){
                    let _id = $(this).attr("data-key");
                    let zone = $.extend({}, zoneObj.zones[_id], true);
                    $(".zone-modal #editZone-container").html(`<form id='zone-edti-form' style='padding:0px 10px;'>
                                    <div class="container-form-zone col-xs-12"></div>
                                </form>`);
                    $(".zone-modal #affiche-container").addClass('hidden');
                    $(".zone-modal #editZone-container").removeClass("hidden");
                    var myCoInput = {
                        container : ".container-form-zone",
                        inputs :[
                            {
                                type : "inputSimple",
                                options: {
                                    label : "Nom",
                                    name : "name",
                                    defaultValue: zone.name
                                },
                            },
                            {
                                type : "selectMultiple",
                                options : {
                                    name : "level",
                                    label : "Niveau du zone",
                                    options : [
                                        {label : "Niveau 1", value : "1"},
                                        {label : "Niveau 2", value : "2"},
                                        {label : "Niveau 3", value : "3"},
                                        {label : "Niveau 4", value : "4"},
                                        {label : "Niveau 5", value : "5"},
                                        // {label : "Niveau 6", value : "6"},
                                    ],
                                    defaultValue: zone.level
                                }
                            },
                            {
                                type : "inputSimple",
                                options: {
                                    label : "Identification sur OpenStreetMap",
                                    name : "osmID",
                                    defaultValue: zone.osmID
                                },
                            },
                            {
                                type : "inputSimple",
                                options: {
                                    label : "Identification sur Wikidata",
                                    name : "wikidataID",
                                    defaultValue: zone.wikidataID
                                },
                            },
                        ],
                        onchange:function(name, value, payload){
                            zone[name] = typeof value == "string" ? value.trim() : value;
                            $(".zone-modal #zone-edti-form #btn-submit-form").attr('disabled', JSON.stringify(zone) == JSON.stringify(zoneObj.zones[_id]));
                        }
                    }
                    
                    new CoInput(myCoInput);
                    $(".zone-modal #zone-edti-form").append(`<div class="container-form-zone col-xs-12" id='divMapLocality' style='height: 300px;'></div>`);
                    var paramsMarker = {
                        elt : {
                            id : 0,
                            type : "zones",
                            geo : {
                                latitude : 0,
                                longitude: 0
                            }
                        },
                        opt : {
                            draggable: true
                        },
                        center : true
                    };
                    if(typeof zone.geo != "undefined" || typeof zone.geoPosition != "undefined"){
                        if(typeof zone.geo == "undefined" && typeof zone.geoPosition != "undefined"){
                            zone.geo = {
                                "@type" : "GeoCoordinates",
                                latitude :  zone.geoPosition.coordinates[1],
                                longitude : zone.geoPosition.coordinates[0]
                            }
                        }
                        paramsMarker.elt.geo.latitude = zone.geo.latitude;
                        paramsMarker.elt.geo.longitude = zone.geo.longitude;
                    }
                    var paramsMap = {
                        container : "divMapLocality",
                        latLon : [ 39.74621, -104.98404],
                        activeCluster : false,
                        zoom : 16,
                        activePopUp : false
                    }
                    let map = mapObj.init(paramsMap);

                    map.addMarker(paramsMarker);
                    map.markerList[0].on('drag', function (e) {
                        var latLonMarker = map.markerList[0].getLatLng();
                        zone.latitude = latLonMarker.lat;
                        zone.longitude = latLonMarker.lng;
                        $(".zone-modal #zone-edti-form #btn-submit-form").attr('disabled', JSON.stringify(zone) == JSON.stringify(zoneObj.zones[_id]));
                    });
                    map.hideLoader();
                    $(".zone-modal #zone-edti-form").append(`<div class="zones-tabs-footer"><div class="col-xs-12 no-padding" style="margin-top:15px;margin-bottom:15px;padding-bottom: 50px !important">
                        <a href="javascript:;" class="btn btn-danger col-xs-12 " id="btn-submit-form" disabled="true"><i class="fa fa-save"></i> `+tradCms.register+`</a>
                    </div></div>`);
                    $(".zone-modal #zone-edti-form #btn-submit-form").click(function() {
                        var tplCtx = {
                            id : _id,
                            collection : 'zones',
                            path : 'allToRoot',
                            value : {},
                        }
                        $.each(zone, function(k, v){
                            if(k == "_id"){
                                return;
                            }
                            tplCtx.value[k] = v;
                        });
                        ajaxPost(
                            null,
                            baseUrl+"/co2/zone/update",
                            tplCtx,
                            function(success){
                                if(zone.longitude && zone.latitude){
                                    zone.geo = {
                                        "@type" : "GeoCoordinates",
                                        latitude :  zone.latitude,
                                        longitude : zone.longitude
                                    }
                                    zone.geoPosition = {
                                        "type": "Point",
                                        "float": "true",
                                        "coordinates": [
                                            zone.longitude,
                                            zone.latitude
                                        ]
                                    }
                                    delete zone.latitude;
                                    delete zone.longitude;
                                }
                                zoneObj.zones[_id] = $.extend({}, zone, true);
                                zoneObj.events.zoneMapMenu(zoneObj)
                                $(".zone-modal #zone-edti-form #btn-submit-form").attr('disabled', JSON.stringify(zone) == JSON.stringify(zoneObj.zones[_id]));
                                toastr.success("Zone modifié avec succès");
                                $(".zone-modal #editZone-container").addClass("hidden");
                            }
                        );
                    });
                });

                $(".zone-modal #edit-parent").on("click", function(){
                    let _id = $(this).attr("data-key");
                    let level = parseInt($(this).attr("data-level")) - 1;
                    let zone = $.extend({}, zoneObj.zones[_id], true);
                    let levelSelected = "";
                    $(".zone-modal #editZone-container").html(`<form id='zone-edti-form' style='padding:0px 10px;'>
                        <div class="container-form-zone col-xs-12"></div>
                    </form>`);
                    $(".zone-modal #affiche-container").addClass('hidden');
                    $(".zone-modal #editZone-container").removeClass("hidden");
                    let zoneData = {};
                    let parentOptions = [];

                    ajaxPost(
                        null,
                        baseUrl+"/co2/zone/getzone",
                        {
                            countryCode : zoneObj.selected,
                            level: [level]
                        },
                        function(data){
                            zoneData = data;
                            if(Object.keys(data).length > 0){
                                $.each(data, function(k, v){
                                    parentOptions.push({label: v.name, value: k});
                                });
                            }
                        },
                        null,
                        null,
                        {async:false}
                    );
                    var myCoInput = {
                        container : ".container-form-zone",
                        inputs :[
                            {
                                type : "select",
                                options : {
                                    name : "level",
                                    label : "Niveau du zone",
                                    options : [
                                        {label : "Niveau 1", value : "1"},
                                        {label : "Niveau 2", value : "2"},
                                        {label : "Niveau 3", value : "3"},
                                        {label : "Niveau 4", value : "4"},
                                        {label : "Niveau 5", value : "5"},
                                        // {label : "Niveau 6", value : "6"},
                                    ],
                                    defaultValue: level.toString()
                                }
                            },
                            {
                                type : "select",
                                options: {
                                    label : "Liste des parents",
                                    name : "parent",
                                    options : parentOptions,
                                    defaultValue : zone[`level${level}`]
                                },
                            },
                        ],
                        onchange:function(name, value, payload){
                            if(name == "level"){
                                levelSelected = value;
                                let zoneParams = {
                                    countryCode : zoneObj.selected,
                                    level: [value]
                                };

                                if(value == "1"){
                                    delete zoneParams.countryCode;
                                }
                                ajaxPost(
                                    null,
                                    baseUrl+"/co2/zone/getzone",
                                    zoneParams,
                                    function(data){
                                        zoneData = data;
                                        parentOptions = [];
                                        new Promise((resolve, reject) => {
                                            if(Object.keys(data).length > 0){
                                                $.each(data, function(k, v){
                                                    parentOptions.push({label: v.name, value: k});
                                                });
                                            }
                                            resolve();
                                        }).then((valuePromise) => {
                                            myCoInput.inputs[0].options.defaultValue = value;
                                            myCoInput.inputs[1].options.options = parentOptions;
                                            new CoInput(myCoInput);
                                        })
                                    }
                                );
                            }
                            if(name == "parent"){
                                $.each(zone, function(index, valeur){
                                    if(index.indexOf("level") > -1 && (index != "level")){
                                        delete zone[index];
                                    }
                                });
                                $.each(zoneData[value], function(k, v){
                                    if(k.indexOf("level") > -1 && (k != "level")){
                                        zone[k] = v;
                                    }
                                });
                                if(levelSelected != ""){
                                    zone[`level${levelSelected}`] = value;
                                    zone[`level${levelSelected}Name`] = zoneData[value].name;
                                }else{
                                    zone[`level${level}`] = value;
                                    zone[`level${level}Name`] = zoneData[value].name;
                                }
                                mylog.log('Zone edited', zone);
                            }
                            $(".zone-modal #zone-edti-form #btn-submit-form").attr('disabled', JSON.stringify(zone) == JSON.stringify(zoneObj.zones[_id]));
                        }
                    };
                    new CoInput(myCoInput);

                    $(".zone-modal #zone-edti-form").append(`<div class="zones-tabs-footer"><div class="col-xs-12 no-padding" style="margin-top:15px;margin-bottom:15px;padding-bottom: 50px !important">
                        <a href="javascript:;" class="btn btn-danger col-xs-12 " id="btn-submit-form" disabled="true"><i class="fa fa-save"></i> `+tradCms.register+`</a>
                    </div></div>`);
                    $(".zone-modal #zone-edti-form #btn-submit-form").click(function() {
                        var tplCtx = {
                            id : _id,
                            collection : 'zones',
                            path : 'allToRoot',
                            value : {},
                        }
                        $.each(zone, function(k, v){
                            if(k == "_id"){
                                return;
                            }
                            tplCtx.value[k] = v;
                        });
                        ajaxPost(
                            null,
                            baseUrl+"/co2/zone/update",
                            tplCtx,
                            function(success){
                                zoneObj.zones[_id] = $.extend({}, zone, true);
                                zoneObj.events.zoneMapMenu(zoneObj)
                                $(".zone-modal #zone-edti-form #btn-submit-form").attr('disabled', JSON.stringify(zone) == JSON.stringify(zoneObj.zones[_id]));
                                toastr.success("Zone modifié avec succès");
                                $(".zone-modal #editZone-container").addClass("hidden");
                            }
                        );
                    });
                });
                $(".zone-modal #edit-contour").on("click", function(){
                    let _id = $(this).attr("data-key");
                    let zone = $.extend({}, zoneObj.zones[_id], true);
                    $(".zone-modal #editZone-container").html(`<form id='zone-edti-form' style='padding:0px 10px;'>
                                    <div class="container-form-zone col-xs-12"></div>
                                </form>`);
                    $(".zone-modal #affiche-container").addClass('hidden');
                    $(".zone-modal #editZone-container").removeClass("hidden");
                    let myCoInput = {
                        container : ".container-form-zone",
                        inputs: [
                            {
                                type : "inputSwitcher",
                                options : {
                                    name : "switch",
                                    label : "GeoJSON du contour",
                                    tabs: [
                                        {
                                            value:"json",  
                                            label: "Texte",
                                            inputs: [
                                                {
                                                    type: "textarea",
                                                    options: {
                                                        name: "text",
                                                        viewPreview : true
                                                    }
                                                }
                                            ]
                                        },
                                        {
                                            value:"file"  , 
                                            label: "Fichier",   
                                            inputs: [
                                                {
                                                    type : "inputFile",
                                                    options: {
                                                        name : "jsonfile",
                                                        viewPreview : true
                                                    }
                                                }
                                            ]
                                        }
                                    ]
                                }
                            },
                        ],
                        onchange:function(name, value, payload){
                            if(name == "text"){
                                try {
                                    let geoJsonObj = JSON.parse(value);
                                    if(typeof geoJsonObj != "undefined" && ((typeof geoJsonObj.geometry != "undefined" && typeof geoJsonObj.geometry.type != "undefined" && typeof geoJsonObj.geometry.coordinates != "undefined") || (typeof geoJsonObj.type != "undefined" && typeof geoJsonObj.coordinates != "undefined"))){
                                        if(typeof geoJsonObj.geometry != "undefined"){
                                            zone.geoShape = geoJsonObj.geometry;
                                        }else{
                                            zone.geoShape = geoJsonObj;
                                        }
                                    }else{
                                        toastr.error("Veuillez inserer un GeoJSON valide pour modifier les contours du zone");
                                    }
                                } catch (error) {
                                    toastr.error("Veuillez inserer de texte json valide");
                                }
                            }
                            if(name == "jsonfile"){
                                let fileReader = new FileReader();
                                fileReader.onload = function(){
                                    try {
                                        let geoJsonObj = JSON.parse(fileReader.result);
                                        if(typeof geoJsonObj != "undefined" && ((typeof geoJsonObj.geometry != "undefined" && typeof geoJsonObj.geometry.type != "undefined" && typeof geoJsonObj.geometry.coordinates != "undefined") || (typeof geoJsonObj.type != "undefined" && typeof geoJsonObj.coordinates != "undefined"))){
                                            if(typeof geoJsonObj.geometry != "undefined"){
                                                zone.geoShape = geoJsonObj.geometry;
                                            }else{
                                                zone.geoShape = geoJsonObj;
                                            }
                                        }else{
                                            toastr.error("Veuillez inserer un GeoJSON valide pour modifier les contours du zone");
                                        }
                                    } catch (error) {
                                        toastr.error("Veuillez inserer un fichier json valide");
                                    }
                                }
                                fileReader.readAsText(value);
                            }
                            $(".zone-modal #zone-edti-form #btn-submit-form").attr('disabled', JSON.stringify(zone) == JSON.stringify(zoneObj.zones[_id]));
                        }
                    };
                    new CoInput(myCoInput);
                    $(".zone-modal #zone-edti-form").append(`<div class="zones-tabs-footer"><div class="col-xs-12 no-padding" style="margin-top:15px;margin-bottom:15px;padding-bottom: 50px !important">
                        <a href="javascript:;" class="btn btn-danger col-xs-12 " id="btn-submit-form" disabled="true"><i class="fa fa-save"></i> `+tradCms.register+`</a>
                    </div></div>`);
                    $(".zone-modal #zone-edti-form #btn-submit-form").click(function() {
                        var tplCtx = {
                            id : _id,
                            collection : 'zones',
                            path : 'allToRoot',
                            value : {},
                        }
                        $.each(zone, function(k, v){
                            if(k == "_id"){
                                return;
                            }
                            tplCtx.value[k] = v;
                        });
                        ajaxPost(
                            null,
                            baseUrl+"/co2/zone/update",
                            tplCtx,
                            function(success){
                                zoneObj.zones[_id] = $.extend({}, zone, true);
                                zoneObj.events.zoneMapMenu(zoneObj)
                                $(".zone-modal #zone-edti-form #btn-submit-form").attr('disabled', JSON.stringify(zone) == JSON.stringify(zoneObj.zones[_id]));
                                toastr.success("Zone modifié avec succès");
                                $(".zone-modal #editZone-container").addClass("hidden");
                            }
                        );
                    });
                });
            },
            map : function(zoneObj){
                $("#navigate-map").off().on('click', function(){
                    let level = "";
                    if(zoneObj.selected != "FR" && zoneObj.selected != "RE" && zoneObj.selected != "GF"  && zoneObj.selected != "YT"  && zoneObj.selected != "MQ"  && zoneObj.selected != "GP"){
                        level ="2";
                    }else{
                        level="3";
                    }

                    function callBack(data, newLevel){
                        if(Object.keys(data).length > 0){
                            let contains = Object.values(data).every(obj => ("geoShape" in obj));
                            if(contains){
                                level = newLevel;
                                map.getOptions().data[newLevel] = data;
                                zoneObj.zones = data;
                                map.clearMap();
                                map.addElts(data);
                                map.fitBounds();
                            }else{
                                map.hideLoader();
                                toastr.error("Les "+newLevel+" dans cet élement n'ont pas de contour");
                            }
                        }else{
                            map.hideLoader();
                            toastr.error("Il n'y a pas de zone "+newLevel+" sur cet élément");
                        }
                    }
                    function beforeSend(){
                        map.showLoader();
                    }
                    $(".zone-content").addClass("hidden");
                    $(".map-content").removeClass("hidden");
                    var map = new CoMap({
                        container : "#map-content-zone",
                        mapCustom: {
                            addElts: function(_context, data){
                                var geoJson = [];
                                $.each(data, function(k, v){
                                    if(typeof v.geoShape != "undefined"){
                                        var data = $.extend({}, v);
                                        var geoData = {
                                                type: "Feature",
                                                properties: {
                                                    name: v.name
                                                },
                                                geometry: v.geoShape
                                        };
                                        delete data.name;
                                        delete data.geoShape;
                                        $.each(data, function(key,value) {
                                            if(typeof geoData.properties[key] == 'undefined'){
                                                geoData.properties[key] = value;
                                            }
                                        })
                                        geoJson.push(geoData);
                                    }
                                })
                        
                                _context.getOptions().geoJSONlayer = L.geoJSON(geoJson, {
                                    onEachFeature: function(feature, layer){
                                        layer.on("mouseover", function(){
                                            this.setStyle({
                                                "fillOpacity" : 1,
                                                "fillColor" : "#0000FF"
                                            })
                                        });
                                        layer.on("mouseout", function(){
                                            this.setStyle({
                                                "fillOpacity" : 0.2,
                                                "fillColor" : "#0000FF"
                                            });
                                            // this.closePopup();
                                        });
                                        layer.on("click", function(){
                                            _context.addPopUp(layer, feature, true);
                                            let elt = this.feature.properties;
                                            $(".map-zoom-plus").off().on("click", function(){
                                                let newLevel = zoneObj.helpers.getNextLevel(zoneObj, level);
                                                zoneObj.helpers.getElements(zoneObj.selected, newLevel, elt._id.$id, level, callBack,  {beforeSend: beforeSend});
                                            });
                                            $(".map-zoom-minus").off().on("click", function(){
                                                level = zoneObj.helpers.getPreviousLevel(zoneObj, level);
                                                map.clearMap();
                                                map.addElts(map.getOptions().data[level]);
                                                map.fitBounds();
                                            });
                                            $(".map-info").off().on("click", function(){
                                                let _id = elt._id.$id;
                                                let _section = "zone";
                                                let _level = level;
                                                let zone = map.getOptions().data[level][_id];
                                                $(".zone-item").removeClass("active");
                                                $("#zone_"+_id).addClass("active");
                                                $("#editZone-container").addClass('hidden');
                                                zoneObj.views.mapinfo(zoneObj, zone, _section, _level);
                                                zoneObj.events.zoneMapMenu(zoneObj);
                                                $(".zone-modal").modal("show");
                                            });
                                        });
                                    },
                                    style: {
                                        color: "#0000FF",
                                        opacity: 0.5
                                    }
                                });
                                _context.getMap().addLayer(_context.getOptions().geoJSONlayer);
                                _context.hideLoader();
                                _context.getMap().invalidateSize();
                            },
                            getPopup: function(data){
                                // mylog.log('data', data);
                                var id = data._id ? data._id.$id:data.id;
                                var eltName = data.title ? data.title: ((typeof data.properties != "undefined" && typeof data.properties.name != "undefined") ? data.properties.name : data.name);
                                var popup = "";
                                popup += "<div class='padding-5' id='popup" + id + "'>";
                                popup += "<h3 style='margin:0; text-align:center;margin-left : 5px; font-size:18px;'>" + eltName + "</h3>";
                                popup += `<div class="btn-group" role="group" style="margin-top: 15px">
                                    <button type="button" class="btn btn-default map-zoom-minus" ${zoneObj.helpers.isFirstLevel(zoneObj, level) ? "disabled" : ""}><i class="fa fa-minus" aria-hidden="true"></i></button>
                                    <button type="button" class="btn btn-default map-info ${level == "cities" || level == "QPV" ? "hidden" : ""}"><i class="fa fa-info" aria-hidden="true"></i></button>
                                    <button type="button" class="btn btn-default map-zoom-plus" ${zoneObj.helpers.isLastLevel(zoneObj, level) ? "disabled" : ""}><i class="fa fa-plus" aria-hidden="true"></i></button>
                                </div>`;
                                popup += '</div>';
                                return popup;
                            },
                        },
                        elts: {},
                    });
                    zoneObj.helpers.getElements(zoneObj.selected, level, "","", callBack, {beforeSend: beforeSend});
                })
            }
        },
        views: {
            init: function(zoneObj){
                let str = '';
                $.each(zoneObj.country, function(k,v){
                    str += zoneObj.views.country(zoneObj, v); 
                });
                $(zoneObj.containerCountry).html(str);
            },
            country: function(zoneObj, country){
                return `<li data-key="${country.countryCode}" ${typeof zoneObj.selected != "undefined" && zoneObj.selected == country.countryCode ? "class='active'" : ""}>${country.name}</li>`;
            },
            zone : function(zoneObj, zone){
                return `<li class="zone-item" data-key='${zone._id.$id}' data-section="zone" id='zone_${zone._id.$id}' data-fm-floatTo='bottom' data-level='${zoneObj.level}' data-country='${zone.countryCode}'>${zone.name} (${zone.level4Name ?? zone.level3Name ?? zone.level2Name ?? ""})</li>`
            },
            default: function(){
                return `<li>Il n'y a pas des resultats</li>`;
            },
            info(zoneObj ,zone, section, level){
                // $("#information-container").html(`<form id='zone-edti-form' style='padding:0px 10px;'>
                //     <div class="container-form-zone col-xs-12"></div>
                // </form>`);
                var levelString = '';
                zone.level.forEach((element) => {
                    levelString += `${zoneObj.helpers.getLevelCorrespondance(element, zone)}, `
                });
                $("#editZone-container").addClass("hidden");
                $("#information-container").removeClass("hidden");
                if(section == "zone") 
                    $("#affiche-container").addClass('hidden');
                var str = `<div class="profile-header">
                    <div class="profile-info">
                        <h2 class="profil-name">${zone.name}</h2>
                        <p class="profil-description">${zone.name} est un(e) ${levelString.trim()} de ${$("#countrySelected").text()}</p>
                        <span class="profile-button" data-key="${zone._id.$id}" id="edit-info"><i class="fa fa-edit"></i> Modifier</span> 
                        <span class="profile-button" data-key="${zone._id.$id}" data-level="${level}" id="merge-to"><i class="fa fa-arrows-alt"></i> Fusionner avec une zone</span> 
                        <span class="profile-button" data-key="${zone._id.$id}" data-level="${level}" id="edit-parent"><i class="fa fa-exchange"></i> Changer le parent</span> 
                        <span class="profile-button" data-key="${zone._id.$id}" id="edit-contour"><i class="fa fa-map"></i> Changer le contour</span>
                    </div>
                </div>
                <div class="zone-info">
                    <h3>Information sur la zone</h3>
                    <p><strong>Identification sur OpenStreetMap : </strong> ${zone.osmID ?? "Non renseigné"}</p>
                    <p><strong>Identification sur Wikidata : </strong> ${zone.wikidataID ?? "Non renseigné"}</p>
                    <p><strong>Niveau du zone : </strong> ${levelString.trim(",")}</p>
                    <div class="zone-info-supl">
                    </div>
                </div>
                <div class="address-info">
                    <h3>Information sur l'adresse</h3>
                    <p><strong>Latitude : </strong> ${typeof zone.geo != "undefined" && typeof zone.geo.latitude != "undefined" ? zone.geo.latitude : "Non renseigné"}</p>
                    <p><strong>Longitude : </strong> ${typeof zone.geo != "undefined" && typeof zone.geo.longitude != "undefined" ? zone.geo.longitude : "Non renseigné"}</p>
                    <div class="address-info-supl"></div>
                </div>`;
                $("#information-container").html(str);
                if((level == "4" || level == "5") && (zone.countryCode == "FR" || zone.countryCode == "RE" || zone.countryCode == "GP" || zone.countryCode == "YT" || zone.countryCode == "GF" || zone.countryCode == "MQ")) {
                    $(".profile-info").append(`<span class="profile-button" data-name="${zone.name}" data-key="${zone._id.$id}" data-level="${level}" id="info-coherence"><i class="fa fa-exchange"></i> Cohérence de données </span>`);
                }
                ajaxPost(
                    null,
                    baseUrl+"/co2/zone/getinfo/id/"+(zone._id.$id)+"/level/"+level,
                    null,
                    function(success){
                        $("#information-container .zone-info-supl").html('');
                        $("#information-container .zone-info-supl").append(`<p><strong>Elements rattachés : </strong> <a href="${baseUrl}/#search?scopeType=open&zones=${zone._id.$id}" target="_blank">${success.all}</a></p>`);
                        var afficheHtml = `<p><strong>Zones rattachées : </strong> ${success.zone}`;
                        afficheHtml += success.zone > 0 ? `<button class="btn-zone-ratache" id="info-zoneRatacher"> Afficher </button></p>` : `</p>`;
                        afficheHtml += `<p><strong>Villes rattachées : </strong> ${success.city}`;
                        // afficheHtml += success.city > 0 ? `<button class="btn-zone-ratache" id="info-cityRattache"> Afficher </button></p>` : `</p>`;
                        $("#information-container .zone-info-supl").append(afficheHtml);
                        $("#info-zoneRatacher").on('click', function(){
                            zoneObj.views.affiche(zoneObj, zone._id.$id, zone.name, level);
                        });
                        $("#information-container .address-info-supl").html('');
                        if(typeof success.geoShape != "undefined" && typeof success.geoShape.geoShape != "undefined"){
                            $("#information-container .address-info-supl").append(`<div class="zone-shape" style="width: 100%">
                                <p><strong>Contour</strong></p>
                                <div id="zoneshape-map" style="height: 250px"></div>
                            </div>`);
                            
                            let customMap=(typeof paramsMapCO.mapCustom!="undefined") ? paramsMapCO.mapCustom : {tile : "mapbox"};
                            let map = new CoMap({
                                container : "#zoneshape-map",
                                activePopUp : true,
                                mapOpt:{
                                    zoomControl:false
                                },
                                mapCustom:customMap,
                                elts : {
                                    
                                }
                            });
                            let elts = {};
                            elts[zone._id.$id] = success.geoShape;
                            zoneObj.zones[zone._id.$id].geoShape = success.geoShape.geoShape;
                            if(typeof zone.geo != "undefined" || typeof zone.geoPosition != "undefined"){
                                let paramsMarker = {
                                    elt : {
                                        id : 0,
                                        type : "zones",
                                        geo : {
                                            latitude : 0,
                                            longitude: 0
                                        }
                                    },
                                };
                                if(typeof zone.geo == "undefined" && typeof zone.geoPosition != "undefined"){
                                    zone.geo = {
                                        "@type" : "GeoCoordinates",
                                        latitude :  zone.geoPosition.coordinates[1],
                                        longitude : zone.geoPosition.coordinates[0]
                                    }
                                }
                                paramsMarker.elt.geo.latitude = zone.geo.latitude;
                                paramsMarker.elt.geo.longitude = zone.geo.longitude;
                                elts[0] = paramsMarker.elt;
                            }else if(typeof zoneObj.zones[zone._id.$id].geoShape != "undefined"){
                                let centroid = turf.centroid(zoneObj.zones[zone._id.$id].geoShape);
                                let paramsMarker = {
                                    elt : {
                                        id : 0,
                                        type : "zones",
                                        geo : {
                                            latitude : 0,
                                            longitude: 0
                                        }
                                    },
                                };
                                zoneObj.zones[zone._id.$id].geo = {
                                    "@type" : "GeoCoordinates",
                                    latitude :  centroid.geometry.coordinates[1],
                                    longitude : centroid.geometry.coordinates[0]
                                };
                                paramsMarker.elt.geo.latitude = centroid.geometry.coordinates[1];
                                paramsMarker.elt.geo.longitude = centroid.geometry.coordinates[0];
                                elts[0] = paramsMarker.elt;
                            }
                            map.addElts(elts);
                        }
                    }
                );
            },
            affiche: function(zoneObj, zoneId, zoneName, level){
                let params = {
                    "zoneId" : zoneId,
                    "level" : level
                }
                $("#affiche-container").html(`<h4>ZONE RATTACHEES D'(E) ${zoneName}</h4>`)
                ajaxPost(
                    null,
                    baseUrl + "/co2/zone/zoneratacher",
                    params,
                    function(result) {
                        $("#affiche-container").removeClass("hidden");
                        $("#editZone-container").addClass("hidden");
                        var htmlContent = {};

                        if (Object.keys(result).length > 0) {
                            $.each(result, function(key, value) {
                                zoneObj.zones[key] = value;
                                var level = value.level['0'];
                            
                                if (!htmlContent[level]) {
                                    htmlContent[level] = { 
                                        html: '',
                                        zone: ''
                                    };
                                }
                                htmlContent[level].html += `<li class="zone-item" data-section="ratacher" id="zone_${key}" data-key='${key}' data-level="${level}" data-country="${value.countryCode}" data-fm-floatTo='bottom'>${value.name}</li>`;
                                htmlContent[level].zone = value;
                            });
                        }

                        $.each(htmlContent, function(level, value) {
                            var listId = `#zoneRatacher${level}`;
                            var labelId = `#label_zoneRatache${level}`;

                            if (value.html !== '') {
                                if ($(labelId).length === 0) {
                                    var labelNiveau = zoneObj.helpers.getLevelCorrespondance(level, value.zone);
                                    $("#affiche-container").append(`
                                        <p class="label_zone" id="label_zoneRatache${level}"> ${labelNiveau}</p>
                                        <ul class="ul_zone" id="zoneRatacher${level}"></ul>
                                    `);
                                }
                                $(labelId).removeClass("hidden");
                                $(listId).html(value.html);
                            }
                        });

                        $(".label_zone").click(function() {
                            var targetId = $(this).attr("id").replace("label_zoneRatache", "zoneRatacher");
                            var targetList = $("#" + targetId);
                            
                            if (targetList.hasClass("visible")) {
                                targetList.removeClass("visible").css('max-height', '0').css('padding', '0 10px');
                                $(this).removeClass("open");
                            } else {
                                targetList.addClass("visible").css('max-height', '500px').css('padding', '10px');
                                $(this).addClass("open");
                            }
                        });

                        zoneObj.events.zoneInfo(zoneObj);
                    }
                );
            },
            mapinfo(zoneObj ,zone, section, level){
                // $("#information-container").html(`<form id='zone-edti-form' style='padding:0px 10px;'>
                //     <div class="container-form-zone col-xs-12"></div>
                // </form>`);
                var levelString = '';
                zone.level.forEach((element) => {
                    levelString += `${zoneObj.helpers.getLevelCorrespondance(element, zone)}, `
                });
                $(".zone-modal #editZone-container").addClass("hidden");
                $(".zone-modal #information-container").removeClass("hidden");
                if(section == "zone") 
                    $(".zone-modal #affiche-container").addClass('hidden');
                var str = `<div class="profile-header">
                    <div class="profile-info">
                        <h2 class="profil-name">${zone.name}</h2>
                        <p class="profil-description">${zone.name} est un(e) ${levelString.trim()} de ${$("#countrySelected").text()}</p>
                        <span class="profile-button" data-key="${zone._id.$id}" id="edit-info"><i class="fa fa-edit"></i> Modifier</span> 
                        <span class="profile-button" data-key="${zone._id.$id}" data-level="${level}" id="edit-parent"><i class="fa fa-exchange"></i> Changer le parent</span> 
                        <span class="profile-button" data-key="${zone._id.$id}" id="edit-contour"><i class="fa fa-map"></i> Changer le contour</span>
                    </div>
                </div>
                <div class="zone-info">
                    <h3>Information sur la zone</h3>
                    <p><strong>Identification sur OpenStreetMap : </strong> ${zone.osmID ?? "Non renseigné"}</p>
                    <p><strong>Identification sur Wikidata : </strong> ${zone.wikidataID ?? "Non renseigné"}</p>
                    <p><strong>Niveau du zone : </strong> ${levelString.trim(",")}</p>
                    <div class="zone-info-supl">
                    </div>
                </div>
                <div class="address-info">
                    <h3>Information sur l'adresse</h3>
                    <p><strong>Latitude : </strong> ${typeof zone.geo != "undefined" && typeof zone.geo.latitude != "undefined" ? zone.geo.latitude : "Non renseigné"}</p>
                    <p><strong>Longitude : </strong> ${typeof zone.geo != "undefined" && typeof zone.geo.longitude != "undefined" ? zone.geo.longitude : "Non renseigné"}</p>
                    <div class="address-info-supl"></div>
                </div>`;
                $(".zone-modal #information-container").html(str);
                ajaxPost(
                    null,
                    baseUrl+"/co2/zone/getinfo/id/"+(zone._id.$id)+"/level/"+level,
                    null,
                    function(success){
                        $(".zone-modal #information-container .zone-info-supl").html('');
                        $(".zone-modal #information-container .zone-info-supl").append(`<p><strong>Elements rattachés : </strong> <a href="${baseUrl}/#search?scopeType=open&zones=${zone._id.$id}" target="_blank">${success.all}</a></p>`);
                        var afficheHtml = `<p><strong>Zones rattachées : </strong> ${success.zone}`;
                        afficheHtml += success.zone > 0 ? `<button class="btn-zone-ratache" id="info-zoneRatacher"> Afficher </button></p>` : `</p>`;
                        afficheHtml += `<p><strong>Villes rattachées : </strong> ${success.city}`;
                        // afficheHtml += success.city > 0 ? `<button class="btn-zone-ratache" id="info-cityRattache"> Afficher </button></p>` : `</p>`;
                        $(".zone-modal #information-container .zone-info-supl").append(afficheHtml);
                        $(".zone-modal #info-zoneRatacher").on('click', function(){
                            zoneObj.views.mapaffiche(zoneObj, zone._id.$id, zone.name, level);
                        });
                        $(".zone-modal #information-container .address-info-supl").html('');
                        if(typeof success.geoShape != "undefined" && typeof success.geoShape.geoShape != "undefined"){
                            $("#information-container .address-info-supl").append(`<div class="zone-shape-modal" style="width: 100%">
                                <p><strong>Contour</strong></p>
                                <div id="zoneshape-map-modal" style="height: 250px"></div>
                            </div>`);
                            
                            let customMap=(typeof paramsMapCO.mapCustom!="undefined") ? paramsMapCO.mapCustom : {tile : "mapbox"};
                            let map = new CoMap({
                                container : "#zoneshape-map-modal",
                                activePopUp : true,
                                mapOpt:{
                                    zoomControl:false
                                },
                                mapCustom:customMap,
                                elts : {
                                    
                                }
                            });
                            let elts = {};
                            elts[zone._id.$id] = success.geoShape;
                            zoneObj.zones[zone._id.$id].geoShape = success.geoShape.geoShape;
                            if(typeof zone.geo != "undefined" || typeof zone.geoPosition != "undefined"){
                                let paramsMarker = {
                                    elt : {
                                        id : 0,
                                        type : "zones",
                                        geo : {
                                            latitude : 0,
                                            longitude: 0
                                        }
                                    },
                                };
                                if(typeof zone.geo == "undefined" && typeof zone.geoPosition != "undefined"){
                                    zone.geo = {
                                        "@type" : "GeoCoordinates",
                                        latitude :  zone.geoPosition.coordinates[1],
                                        longitude : zone.geoPosition.coordinates[0]
                                    }
                                }
                                paramsMarker.elt.geo.latitude = zone.geo.latitude;
                                paramsMarker.elt.geo.longitude = zone.geo.longitude;
                                elts[0] = paramsMarker.elt;
                            }else if(typeof zoneObj.zones[zone._id.$id].geoShape != "undefined"){
                                let centroid = turf.centroid(zoneObj.zones[zone._id.$id].geoShape);
                                let paramsMarker = {
                                    elt : {
                                        id : 0,
                                        type : "zones",
                                        geo : {
                                            latitude : 0,
                                            longitude: 0
                                        }
                                    },
                                };
                                zoneObj.zones[zone._id.$id].geo = {
                                    "@type" : "GeoCoordinates",
                                    latitude :  centroid.geometry.coordinates[1],
                                    longitude : centroid.geometry.coordinates[0]
                                };
                                paramsMarker.elt.geo.latitude = centroid.geometry.coordinates[1];
                                paramsMarker.elt.geo.longitude = centroid.geometry.coordinates[0];
                                elts[0] = paramsMarker.elt;
                            }
                            map.addElts(elts);
                        }
                    }
                );
            },
            mapaffiche: function(zoneObj, zoneId, zoneName, level){
                let params = {
                    "zoneId" : zoneId,
                    "level" : level
                }
                $(".zone-modal #affiche-container").html(`<h4>ZONE RATTACHEES D'(E) ${zoneName}</h4>`)
                ajaxPost(
                    null,
                    baseUrl + "/co2/zone/zoneratacher",
                    params,
                    function(result) {
                        $(".zone-modal #affiche-container").removeClass("hidden");
                        $(".zone-modal #editZone-container").addClass("hidden");
                        var htmlContent = {};

                        if (Object.keys(result).length > 0) {
                            $.each(result, function(key, value) {
                                zoneObj.zones[key] = value;
                                var level = value.level['0'];
                            
                                if (!htmlContent[level]) {
                                    htmlContent[level] = { // Initialisation correcte de l'objet pour le niveau
                                        html: '',
                                        zone: ''
                                    };
                                }
                                htmlContent[level].html += `<li class="zone-item" data-section="ratacher" id="zone_${key}" data-key='${key}' data-level="${level}" data-country="${value.countryCode}" data-fm-floatTo='bottom'>${value.name}</li>`;
                                htmlContent[level].zone = value;
                            });
                        }

                        $.each(htmlContent, function(level, value) {
                            var listId = `#zoneRatacher${level}`;
                            var labelId = `#label_zoneRatache${level}`;

                            if (value.html !== '') {
                                if ($(labelId).length === 0) {
                                    var labelNiveau = zoneObj.helpers.getLevelCorrespondance(level, value.zone);
                                    $(".zone-modal #affiche-container").append(`
                                        <p class="label_zone" id="label_zoneRatache${level}"> ${labelNiveau}</p>
                                        <ul class="ul_zone" id="zoneRatacher${level}"></ul>
                                    `);
                                }
                                $(labelId).removeClass("hidden");
                                $(listId).html(value.html);
                            }
                        });

                        $(".label_zone").click(function() {
                            var targetId = $(this).attr("id").replace("label_zoneRatache", "zoneRatacher");
                            var targetList = $("#" + targetId);
                            
                            if (targetList.hasClass("visible")) {
                                targetList.removeClass("visible").css('max-height', '0').css('padding', '0 10px');
                                $(this).removeClass("open");
                            } else {
                                targetList.addClass("visible").css('max-height', '500px').css('padding', '10px');
                                $(this).addClass("open");
                            }
                        });

                        // zoneObj.events.zoneMapInfo(zoneObj);
                    }
                );
            },
            map: function(zoneObj, zone){

            }
        }
    };
    $(function(){
        adminZone.init({
            country: countryList,
            container: "#countryList"
        });
    });
</script>