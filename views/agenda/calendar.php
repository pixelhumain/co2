<?php
$blockKey = $blockKey ?? sha1(date('c'));
$links_types = [
	'members',
	'contributors',
	'creator',
	'attendees',
	'organizer'
];
$css_js = [
	'/css/flex-layout.css',
	'/css/rinelfi/modal.css',
	'/js/rinelfi/modal.js',
	'/plugins/xdan.datetimepicker/jquery.datetimepicker.full.min.js',
	'/plugins/xdan.datetimepicker/jquery.datetimepicker.min.css',
	'/plugins/jquery-confirm/jquery-confirm.min.css',
	'/plugins/jquery-confirm/jquery-confirm.min.js'
];
HtmlHelper::registerCssAndScriptsFiles($css_js, Yii::app()->request->baseUrl);
$css_js = array(
	'/css/codate/codate-inside.css',
	// '/js/codate/codateInsideObj.js'
);
HtmlHelper::registerCssAndScriptsFiles($css_js, Yii::app()->getModule('co2')->getAssetsUrl());
$css_js = [
	'/css/inputs-style.css',
	'/css/space-layout.css',
	'/css/codate/codate.css'
];
HtmlHelper::registerCssAndScriptsFiles($css_js, Yii::app()->getModule(Survey::MODULE)->getAssetsUrl());
$connected_user = Yii::app()->session['userId'] ?? '';
?>
<style>
	#calendar_container {
		width: 100%;
	}

	/**
     * CPL container
     */
	#cpl_container {
		min-width: 300px;
		width: 300px;
	}

	#cpl {
		display: flex;
		flex-direction: column;
		max-height: 594px;
		overflow-y: auto;
		gap: 10px;
	}

	.cpl,
	#cpl-select {
		background-color: white;
		padding: 9px 10px;
		display: flex;
		align-items: center;
		gap: 5px;
	}

	#cpl-search {
		position: relative;
	}

	#cpl-search>input,
	#cpl-search>input:focus {
		border: 1px solid #cacaca;
		border-radius: 20px;
		line-height: 1.8rem;
		padding: 10px 10px 10px 35px;
		width: 100%;
		outline: none;
	}

	#cpl-search>.fa {
		position: absolute;
		top: 50%;
		left: 10px;
		transform: translateY(-50%);
		color: #cacaca;
		font-size: 2rem;
	}

	.cpl-text {
		user-select: none;
		cursor: default;
		display: block;
	}

	.card {
		display: flex;
		flex-direction: column;
		border: 1px solid #cfcfcf;
		border-radius: 10px;
	}

	.card>div:first-child {
		border-radius: 10px 10px 0 0;
	}

	.card>div:last-child {
		border-radius: 0 0 10px 10px;
	}

	.card-header {
		padding: 0.75rem 1.25rem;
		margin-bottom: 0;
		background-color: #f7f7f7;
		border-bottom: 1px solid rgba(0, 0, 0, .125);
		display: flex;
		flex-direction: row;
		justify-content: space-between;
		align-items: center;
	}

	.card-header>.card-title {
		font-size: 18px;
	}

	.card-body {
		padding: 1.25rem;
		background-color: #fff;
	}

	.cpl-text {
		overflow: hidden;
		text-overflow: ellipsis;
		white-space: nowrap;
	}

	.cpl-actions {
		display: flex;
		flex-direction: row;
		position: relative;
	}

	.checkmark {
		display: block;
		height: 25px;
		width: 25px;
		background-color: #fff;
	}

	.checkmark:after,
	.checkmark:before {
		content: "";
		position: absolute;
		display: none;
	}

	/* Show the checkmark when checked */
	.cpl-check:checked~.checkmark:after {
		display: block;
	}

	.cpl-check:indeterminate~.checkmark:before {
		display: block;
	}

	/* Style the checkmark/indicator */
	.checkmark:after {
		left: 10px;
		top: 7px;
		width: 5px;
		height: 10px;
		border: solid #fff;
		border-width: 0 3px 3px 0;
		-webkit-transform: rotate(45deg);
		-ms-transform: rotate(45deg);
		transform: rotate(45deg);
	}

	.checkmark:before {
		left: 50%;
		top: 50%;
		width: 15px;
		height: 1px;
		border: solid #fff;
		border-width: 0 0 3px 0;
		-webkit-transform: translateX(-50%) translateY(50%);
		-ms-transform: translateX(-50%) translateY(50%);
		transform: translateX(-50%) translateY(50%);
	}

	.cpl-check {
		position: absolute;
		opacity: 0;
		cursor: pointer;
		height: 0;
		width: 0;
	}

	.cpl-check~.checkmark {
		border: 3px solid #000;
	}

	.cpl-check:checked~.checkmark,
	.cpl-check:indeterminate~.checkmark {
		background-color: #000;
	}

	.cpl-text .description {
		display: block;
		color: rgba(0, 0, 0, .5);
		font-weight: normal;
	}

	#custom_tooltip {
		display: none;
		position: fixed;
		border: 1px solid #2c2c2c;
		background-color: #2c2c2c;
		padding: 5px 15px;
		border-radius: 10px;
		color: white;
		font-size: 1.2rem;
		font-weight: bold;
		z-index: 11;
	}

	#custom_tooltip.show {
		display: block;
	}

	ul.ks-cboxtags li label::before,
	a.invite-community::before {
		display: inline-block;
		text-rendering: auto;
		-webkit-font-smoothing: antialiased;
		font: normal normal normal 14px/1 FontAwesome;
		font-weight: 900;
		font-size: 16px;
		padding: 2px 2px 2px 2px;
		transition: transform .3s ease-in-out;
	}

	ul.ks-cboxtags li label.create-codate::before {
		content: "\f067";
	}

	ul.ks-cboxtags li label.vote-codate::before {
		content: "\f1d8";
	}

	ul.ks-cboxtags li label.invite-community::before {
		content: "\f0f3";
	}

	.cdt-text-span {
		background-color: #f9f9f9;
		display: flex;
		align-items: center;
		gap: 5px;
		margin-bottom: 5px;
		font-weight: 700;
		margin-left: -4%;
		padding: 3px;
	}

	.cdt-text-info {
		font-size: 12px;
		font-weight: 400;
		color: gray;
	}

	.xdsoft_datetimepicker {
		z-index: 1000001 !important
	}

	.times-content .dropdown-menu {
		margin: -2px 0 !important;
	}

	li span.custom-font {
		font-size: 14px !important;
		;
	}

	.fc-past {
		cursor: not-allowed !important;
	}

	/**
     * CPL container
     */
</style>
<div class="rin-modal">
	<div class="rin-modal-dialog">
		<div class="rin-modal-header">
			<div class="rin-modal-actions">
				<a class="rin-modal-action" href="#" target="" id="redirector">
					<span class="fa fa-eye"></span>
				</a>
				<div class="rin-modal-action delete">
					<span class="fa fa-trash"></span>
				</div>
				<div class="rin-modal-action rin-modal-close">
					<span class="fa fa-times"></span>
				</div>
			</div>
		</div>
		<div class="rin-modal-body">
			<div style="display: flex; flex-direction: row; align-items: flex-start; margin: 5px 5px 0; gap: 5px;">
				<span style="width: 30px; height: 30px;border-radius: 3px;" id="event-color-nocostum"></span>
				<span style="flex: 1;">
					<span style="font-size: 30px;line-height: 1;" id="event-name-nocostum"></span><br>
					<span id="event-date-nocostum"></span>
				</span>
			</div>
			<div style="display: flex; flex-direction: row; align-items: flex-start; margin: 5px 5px 0; gap: 5px;">
				<span style="font-size: 30px;" class="fa fa-calendar"></span>
				<span style="flex: 1;">
					<span style="font-size: 20px;" id="event-group-nocostum"></span><br>
					<span id="event-author-nocostum"></span>
				</span>
			</div>
		</div>
	</div>
</div>
<div class="col-xs-12" style="padding-top: 50px;">
	<?php
	if (isset($form) && $form["name"]) { ?>
		<h4>Codate <?= $form["name"] ?></h4>
	<?php
	} ?>
	<div style="display: flex; flex-direction: row;gap:20px;align-items: flex-start;" id="agFedereCodInside">
		<?php
		if (isset($standalone) && $standalone === false) { ?>
			<div style="min-width: 300px;width: 300px;">
				<div id="cpl_container" class="card view_type" style="min-width: 300px;width: 300px;">
					<div class="card-header">
						<h3 class="card-title"><?= Yii::t('common', 'Calendars') ?></h3>
						<?php if ($can_edit) : ?>
							<div class="dropdown">
								<button class="btn btn-default dropdown-toggle" type="button" id="add_agenda_source<?= $blockKey ?>" data-toggle="dropdown">
									<span class="fa fa-plus"></span>
								</button>
								<ul class="dropdown-menu" role="menu" aria-labelledby="add_agenda_source<?= $blockKey ?>">
									<li role="presentation"><a role="menuitem" tabindex="-1" href="#openagenda">OpenAgenda</a></li>
									<li role="presentation"><a role="menuitem" tabindex="-1" href="#icalendar">iCalendar</a></li>
									<li role="presentation"><a role="menuitem" tabindex="-1" href="#communecter">Communecter</a></li>
									<li role="presentation">
										<a role="menuitem" tabindex="-1" class="create-codate codate-create-button" href="javascript:;">Créer
											codate</a>
									</li>
								</ul>
							</div>
						<?php endif; ?>
					</div>
					<div class="card-body" id="cpl">
						<div id="cpl-search">
							<i class="fa fa-search"></i>
							<input type="search" placeholder="Rechercher dans calendrier" id="agenda_search_filter">
						</div>
						<div id="cpl-select">
							<div id="custom_tooltip"></div>
							<div class="cpl-actions">
								<input class="cpl-check" type="checkbox" id="cpl_filter_select_all" checked>
								<span class="checkmark"></span>
							</div>
							<label for="cpl_filter_select_all" class="cpl-text">Sélectionner tout</label>
						</div>
					</div>
				</div>
				<div class="col-xs-3 input-panel ins-card" style="min-width: 300px;width: 300px;">
					<div class="ins-card-header d-flex justify-content-between">
						<span>Enquête par date</span>
						<div class="checkboxContainer" id="codInsideBtnAction">
						</div>
					</div>
					<div class="ins-card-body" id="codInsideInput">

					</div>
				</div>
			</div>
		<?php
		} else { ?>
			<div style="min-width: 20px;width: 20px;">
			</div>
		<?php
		} ?>
		<div id="calendar_container">
			<div id="agenda_calendar_view"></div>
		</div>
	</div>
</div>

<script type="text/javascript">
	(function($, W) {
			var _selected_event = {
				id: null,
				group: null
			};
			var _standalone = <?= json_encode($standalone) ?>;
			var _events = JSON.parse(JSON.stringify(<?= json_encode($events) ?>));
			var _agenda_list = JSON.parse(JSON.stringify(<?= json_encode($agenda_list) ?>));
			var _input_timeout = null;
			var _agendas = JSON.parse(JSON.stringify(<?= json_encode($agendas) ?>));
			var _forms = {
				openagenda: {
					jsonSchema: {
						title: 'Open Agenda',
						description: 'une description',
						icon: 'fa-question',
						properties: {
							api_key: {
								inputType: 'text',
								label: 'Clef d\'API',
								value: _agendas && _agendas['openagenda'] && _agendas['openagenda']['api_key'] ? _agendas['openagenda']['api_key'] : '',
								rules: {
									required: true
								}
							},
							public_agenda: {
								inputType: 'custom',
								html: '<label class="col-xs-12 text-left control-label no-padding"><i class="fa fa-chevron-circle-right"></i> Autre agenda public</label>'
							},
							openagenda_urls: {
								label: 'Slugs ou urls',
								inputType: 'array',
								value: _agendas && _agendas['openagenda'] && _agendas['openagenda']['urls'] ? _agendas['openagenda']['urls'] : []
							}
						},
						save: function(__form_value) {
							delete __form_value['collection'];
							delete __form_value['scope'];
							var agenda = {
								api_key: __form_value['api_key'],
								urls: getArray('.openagenda_urlsarray')
							}
							var parameters = {
								id: contextId,
								collection: contextData['collection'],
								path: 'agendas.openagenda',
								value: agenda

							};
							dataHelper.path2Value(parameters, function(response) {
								document.location.reload();
							})
						}
					}
				},
				icalendar: {
					jsonSchema: {
						title: 'iCalendar',
						description: 'une description',
						icon: 'fa-question',
						properties: {
							icalendar_urls: {
								label: 'Urls',
								inputType: 'array',
								value: _agendas && _agendas['icalendars'] ? _agendas['icalendars'] : []
							}
						},
						save: function(__form_value) {
							delete __form_value['collection'];
							delete __form_value['scope'];

							var parameters = {
								id: contextId,
								collection: contextData['collection'],
								path: 'agendas.icalendars',
								value: getArray('.icalendar_urlsarray')

							};
							dataHelper.path2Value(parameters, function() {
								document.location.reload();
							})
						}
					}
				}
			}

			function perform_search(__element) {
				if (_input_timeout != null) {
					clearTimeout(_input_timeout);
				}
				if (__element.value.length > 0) {
					_input_timeout = setTimeout(function() {
						var regex = new RegExp(__element.value, 'gi');
						var openagenda_events = _events['openagenda'].filter(function(__event) {
							var in_agenda = _agenda_list['openagenda'].filter(function(__agenda) {
								return __event['parent'] === __agenda['id'];
							}).filter(function(__agenda) {
								return __agenda['name'].match(regex) || __agenda['group'].match(regex);
							}).length > 0;
							var statement = __event['name'].match(regex) || __event['group'].match(regex) || in_agenda;
							return statement;
						});
						var icalendar_events = _events['icalendar'].filter(function(__event) {
							var in_agenda = _agenda_list['icalendar'].filter(function(__agenda) {
								return __event['parent'] === __agenda['id'];
							}).filter(function(__agenda) {
								return __agenda['name'].match(regex) || __agenda['group'].match(regex);
							}).length > 0;
							var statement = __event['name'].match(regex) || __event['group'].match(regex) || in_agenda;
							return statement;
						});
						var communecter_events = _events['communecter'].filter(function(__event) {
							var in_agenda = _agenda_list['communecter'].filter(function(__agenda) {
								return __event['parent'] === __agenda['id'];
							}).filter(function(__agenda) {
								return __agenda['name'].match(regex) || __agenda['group'].match(regex);
							}).length > 0;
							var statement = __event['name'].match(regex) || __event['group'].match(regex) || in_agenda;
							return statement;
						});

						var openagenda_agenda = _agenda_list['openagenda'].filter(function(__agenda) {
							var in_event = _events['openagenda'].filter(function(__event) {
								return __event['parent'] === __agenda['id'];
							}).filter(function(__event) {
								return __event['name'].match(regex) || __event['group'].match(regex);
							}).length > 0;
							var statement = __agenda['name'].match(regex) || __agenda['group'].match(regex) || in_event;
							return statement;
						});
						var icalendar_agenda = _agenda_list['icalendar'].filter(function(__agenda) {
							var in_event = _events['icalendar'].filter(function(__event) {
								return __event['parent'] === __agenda['id'];
							}).filter(function(__event) {
								return __event['name'].match(regex) || __event['group'].match(regex);
							}).length > 0;
							var statement = __agenda['name'].match(regex) || __agenda['group'].match(regex) || in_event;
							return statement;
						});
						var communecter_agenda = _agenda_list['communecter'].filter(function(__agenda) {
							var in_event = _events['communecter'].filter(function(__event) {
								return __event['parent'] === __agenda['id'];
							}).filter(function(__event) {
								return __event['name'].match(regex) || __event['group'].match(regex);
							}).length > 0;
							var statement = __agenda['name'].match(regex) || __agenda['group'].match(regex) || in_event;
							return statement;
						});
						load_filter({
							openagenda: openagenda_agenda,
							communecter: communecter_agenda,
							icalendar: icalendar_agenda
						});
						load_calendar({
							openagenda: openagenda_events,
							communecter: communecter_events,
							icalendar: icalendar_events
						});
					}, 800);
				} else {
					load_filter();
					load_calendar();
				}
			}

			function load_filter(__agendas) {
				var agendas = __agendas ? __agendas : _agenda_list;
				$('.cpl').remove();
				for (var key in agendas) {
					$.each(agendas[key], function(_, __agenda) {
						$('#cpl').append(
							'<label class="cpl" for="' + __agenda['id'] + '">' +
							'<div class="cpl-actions">' +
							'<input class="cpl-check" type="checkbox" value="' + __agenda['id'] + '" id="' + __agenda['id'] + '" checked>' +
							'<span class="checkmark" style="background-color: ' + __agenda['color'] + '; border-color: ' + __agenda['color'] + ';"></span>' +
							'</div>' +
							'<span class="cpl-text" data-start="" data-end="" data-id="' + __agenda['id'] + '">' +
							__agenda['name'] +
							'<small class="description">Groupe : ' + __agenda['group'] + '</small>' +
							'</span>' +
							'</label>');
					})
				}
			}

			function load_calendar(__event) {
				var openagenda_event = __event && typeof __event['openagenda'] != 'undefined' ? __event['openagenda'] : _events['openagenda'];
				var communecter_event = __event && typeof __event['communecter'] != 'undefined' ? __event['communecter'] : _events['communecter'];
				var icalendar_event = __event && typeof __event['icalendar'] != 'undefined' ? __event['icalendar'] : _events['icalendar'];
				var merged = openagenda_event.concat(communecter_event, icalendar_event);

				if ($("#agenda_calendar_view").children().length > 0) {
					var render = merged.filter(function(__event) {
						return $('.cpl-check:checked[value=' + __event['parent'] + ']').length > 0;
					}).map(function(__event) {
						return {
							id: __event['id'],
							title: __event['name'],
							simple_event: true,
							group: __event['group'],
							parent: __event['parent'],
							start: moment(__event['start_date']).format(),
							end: moment(__event['end_date']).format(),
							url: __event['url'],
							backgroundColor: __event['color'] + ' !important',
							is_admin: typeof __event['is_admin'] !== 'undefined' ? __event['is_admin'] : false
						};
					});

					var openagenda = _events['openagenda'] ? _events['openagenda'] : [];
					var communecter = _events['communecter'] ? _events['communecter'] : [];
					var icalendar = _events['icalendar'] ? _events['icalendar'] : [];
					var to_deletes = openagenda.concat(communecter, icalendar);

					$.each(to_deletes, (_, __to_delete) => $("#agenda_calendar_view").fullCalendar('removeEvents', __to_delete['id']));
					$("#agenda_calendar_view").fullCalendar('addEventSource', render);
					W.agenda.fixDayClick(W.agenda);
				} else {
					if (codateObj.container != "#agFedereCodInside") {
						W.agenda = codateObj.init({
							container: "#agFedereCodInside",
							calendarContainer: "#agenda_calendar_view",
							options: {
								events: merged.filter(function(__event) {
									return $('.cpl-check:checked[value=' + __event['parent'] + ']').length > 0;
								}).map(function(__event) {
									return {
										id: __event['id'],
										title: __event['name'],
										simple_event: true,
										group: __event['group'],
										parent: __event['parent'],
										start: moment(__event['start_date']).format(),
										end: moment(__event['end_date']).format(),
										url: __event['url'],
										backgroundColor: __event['color'] + ' !important',
										is_admin: typeof __event['is_admin'] !== 'undefined' ? __event['is_admin'] : false
									};
								})
							}
						});
					}
				}
			}

			function add_source(__type) {
				if (__type === 'communecter') {
					dyFObj.openForm('event', null, null, null, {
						afterSave: function(__data) {
							var user = {
								id: userConnected['_id']['$id'],
								collection: userConnected['collection'] ? userConnected['collection'] : 'citoyens'
							};
							var event = {
								id: __data['map']['_id']['$id'],
								collection: __data['map']['collection']
							};
							var aftersave = {
								parent: {},
								links: {
									attendees: {},
									events: {},
									subEvents: {},
								}
							};
							var set_types = [{
								path: 'links.attendees.' + user.id + '.isAdmin',
								type: 'boolean'
							}];
							aftersave.parent[contextData.id] = {
								type: contextData.type,
								name: contextData.name
							};
							aftersave.links.attendees[user.id] = {
								type: user.collection,
								isAdmin: true
							};
							if (event.collection === 'events') {
								aftersave.links.subEvents[__data.id] = {
									type: 'events'
								}
								delete aftersave.links.events;
							} else {
								aftersave.links.events[__data.id] = {
									type: 'events'
								}
								delete aftersave.links.subEvents;
							}

							const after_event = {
								id: event.id,
								collection: event.collection,
								path: 'allToRoot',
								value: aftersave,
								setType: set_types
							};
							const after_citizen = {
								id: user.id,
								collection: user.collection,
								path: `links.${event.collection}.${event.id}`,
								value: {
									type: event.collection,
									isAdmin: true
								},
								setType: [{
									path: 'isAdmin',
									type: 'boolean'
								}]
							};
							dataHelper.path2Value(after_event, function(response) {
								if (response['result']) {
									dataHelper.path2Value(after_citizen, function(response) {
										if (response['result']) {
											dyFObj.closeForm();
											W.location.reload();
										}
									});
								}
							});
						}
					}, {
						type: 'bootbox'
					});
				} else {
					dyFObj.openForm(_forms[__type], null, null, null, null, {
						type: "bootbox",
						notCloseOpenModal: true,
					});
				}
			}

			$(function() {
				load_filter();
				load_calendar();

				var event_count = 0;
				$.each(Object.keys(_events), function(_, __key) {
					event_count += _events[__key].length;
				});
				if (event_count === 0) {
					$('#cpl-search,#cpl-search').hide();
					$('#cpl').append('<b class="text-center color-gray">Aucun élément à afficher</b>');
				}

				$('#add_agenda_source<?= $blockKey ?>').next('ul').find('a').on('click', function(__e) {
					__e.preventDefault();
					if (__e.target.getAttribute('href').substr(1) && __e.target.getAttribute('href').substr(1) != 'avascript:;') {
						add_source(__e.target.getAttribute('href').substr(1));
					}
				});
				$('#cpl_filter_select_all').on('change', function(__event) {
					$('#cpl_container .cpl-check').each(function(_, __element) {
						__element['checked'] = __event['target']['checked'];
						var checkmark = $(__element).next('.checkmark');
						checkmark.css('background-color', __element['checked'] ? checkmark.css('border-color') : '#fff');
					});
					load_calendar();
				});
				$('#cpl_container').on('change', '.cpl .cpl-check', function(__e) {
					var checkmark = $(this).next('.checkmark');
					checkmark.css('background-color', __e.target.checked ? checkmark.css('border-color') : '#fff');
					var select_all = document.getElementById('cpl_filter_select_all');
					select_all.checked = __e.target.checked;
					select_all.indeterminate = false;
					$('.cpl .cpl-check').each(function(_, __element) {
						if (__element.checked !== __e.target.checked) {
							select_all.checked = false;
							select_all.indeterminate = true;
						}
					});
					load_calendar();
				});
				$('#calendar_container').on('calendar.reload', function() {
					var agenda_search_filter = $('#agenda_search_filter');
					var index_of_deleted = _events['communecter'].findIndex(function(__event) {
						__event['id'] === $('.rin-modal .delete').data('id');
					});
					_events['communecter'].splice(index_of_deleted, 1);
					$("#agenda_calendar_view").fullCalendar('removeEvents', $('.rin-modal .delete').data('id'));
				});
				$('#agenda_search_filter').on('input', function() {
					perform_search(this);
				});
			});
		}

	)
	(jQuery, window)
</script>