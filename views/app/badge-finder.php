<?php

    $cssAnsScriptFilesModule = array(
        '/css/element/badge.css'
    );
    HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, $this->module->getParentAssetsUrl());

    $page = (@$page && !empty($page)) ? $page : "badge";
    //var_dump($this);exit;

$searchParamsViews=(isset($this->appConfig["pages"]["#".$page]["filterObj"])) ? $this->appConfig["pages"]["#".$page]["filterObj"] : "co2.views.app.filters.badge-finder";
     echo $this->renderPartial($searchParamsViews, array("page"=>$page, "issuers"=>$issuers, "appConfig"=>$this->appConfig["pages"]["#".$page]));

?>

<div class="about-section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="single_menu page_title col-xs-12">
                    <div class="img-div">
                        <img class="img-responsive" src="<?php echo $this->module->assetsUrl; ?>/images/badge.png" alt="black coffee">
                    </div>
                    <div class="menu_content">
                        <h4><?php echo Yii::t("badge", "All badges") ?> <span>(<span id="countbadges"><?= $badgeCount ?> </span>)</span> </h4>
                        <p>
                            <?php echo Yii::t("badge", "Badges are based on tags, and related to possible actions.") ?><br />
                            <?php echo Yii::t("badge", "They help build communities around tags. People or Organizations cary tags, and can be found in the search engine. Badges will be credentials confirmed by peers. Giving access to features and actions available on a certain thematic. Badges can tell who you are, what you do, or want to do, find people or organizations alike.") ?><br />
                        </p>
                        <div class="pulse-btn btn " >
                            <a class="btn custom-btn-nav" href="javascript:dyFObj.openForm('badge')">
                                <i class="fa fa-bookmark"></i>
                                <?php echo Yii::t("badge", "Add badge")?>
                            </a>
                        </div>
                    </div>
                </div>
            </div>


        </div>
        <div class="row">
            <div class="col-xs-12">
                <div id='filterContainer' class='searchObjCSS'></div>
                <div class='headerSearchIncommunity no-padding col-xs-12'></div>
                <div class='bodySearchContainer margin-top-30'>
                    <div class='no-padding col-xs-12' id='dropdown_search'></div>
                </div>
                <div class='no-padding col-xs-12 text-left footerSearchContainer smartgrid-slide-element'></div>
            </div>
        </div>
    </div>
</div>

<script>
    directory.badgeElementPanelHtml = function(params){
		mylog.log("badgeElementPanelHtml","Params",params);
		
		var dateStr="";
		if(typeof params.updated != "undefined" && notNull(params.updated))
      		dateStr += /*'<div class="dateUpdated dateUpdated-sm date-position">'+*/directory.showDatetimePost(params.collection, params.id, params.updated,30)/*+'</div>'*/;
		else if(typeof params.created != "undefined" && notNull(params.created))
      		dateStr += /*'<div class="dateUpdated dateUpdated-sm date-position">'+*/directory.showDatetimePost(params.collection, params.id, params.created,30)/*+'</div>'*/;
		var str=''; 
		str +='<div id="entity_'+params.collection+'_'+params.id+'" class="col-lg-4 col-md-4 col-sm-6 col-xs-12 searchEntityContainer '+params.containerClass+'">'+
				'<div class="item-slide">'+
						'<div class="entityCenter" style="position: absolute;">'+
							'<span><i class="fa fa-'+params.icon+' bg-'+params.color+'"></i></span>'+
						'</div>'+
						'<div class="img-back-card">'+
							params.imageProfilHtml +
							'<div class="text-wrap searchEntity">'+
								'<h4 class="entityName">'+
									'<a href="'+params.hash+'" class="uppercase '+params.hashClass+'">'+params.name+'</a>'+
								'</h4>'+
								'<div class="small-infos-under-title text-center">';
									if (typeof params.type != 'undefined') {
		str +=							'<div class="text-center entityType">'+	
											'<span class="text-white">'+((typeof tradCategory[params.type] != "undefined") ? tradCategory[params.type] : params.type)+'</span>'+
										'</div>';
									}
									if(notEmpty(params.statusLinkHtml))
		str+=							params.statusLinkHtml;	
									if(notEmpty(params.rolesHtml))
		str += 							'<div class=\'rolesContainer elipsis\'>'+params.rolesHtml+'</div>';
									if (notNull(params.localityHtml)) {
		str +=							'<div class="entityLocality no-padding">'+
											'<span>'+params.localityHtml+'</span>'+
										'</div>';
									}
		str +=					'</div>'+
								'<div class="entityDescription"></div>'+
							'</div>'+
						'</div>'+
						//hover
						'<div class="slide-hover co-scroll">'+
							'<div class="text-wrap">'+
								'<h4 class="entityName">'+
									'<a href="'+params.hash+'" class="'+params.hashClass+'">'+params.name+'</a>'+
								'</h4>';
								if (typeof params.type != 'undefined') {
		str +=						'<div class="entityType">'+	
										'<span class="text-white">'+((typeof tradCategory[params.type] != "undefined") ? tradCategory[params.type] : params.type)+'</span>'+
									'</div>';
								}
								if(notEmpty(params.statusLinkHtml))
		str+=							params.statusLinkHtml;	
								if(notEmpty(params.rolesHtml))
		str += 						'<div class=\'rolesContainer\'>'+params.rolesHtml+'</div>';
								if(typeof params.edit  != 'undefined' && notNull(params.edit))
		str += 						directory.getAdminToolBar(params);

								if (notNull(params.localityHtml)) {
		str +=						'<div class="entityLocality text-center">'+
										'<span> '+params.localityHtml+'</span>'+
									'</div>';
								}
//		str +=					'<hr>';
		str +=					'<p class="p-short">'+params.descriptionStr+'</p>';
 
								if(typeof params.id != 'undefined' && typeof params.collection != 'undefined') 
        str +=                  '<div style="text-align: center;"> <a class="btn btn-success" onclick="handleButtonAssign(event,\''+ params.id +'\');"><?php echo Yii::t("badge", "Assign") ?></a> </div>'
		str+=				'</div>'+
					'</div>'+
				'</div>'+
			'</div>';
		return str;
	};
</script>

<script> 

        
    function handleButtonAssign(event, badgeId){
        dyFObj.openForm("assignbadge",null, {badgeId: badgeId});
    }
</script>