<?php

    $cssAnsScriptFilesModule = array(
    '/css/element/badge.css'
    );
    HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, $this->module->getParentAssetsUrl());

	$myContacts = array();
	if(isset(Yii::app()->session['userId']))
		$myContacts = Person::getPersonLinksByPersonId(Yii::app()->session['userId']);

    function checkExisteAndAdmin($arr, $key) {

        // is in base array?
        if (array_key_exists($key, $arr)) {
            if($arr[$key]["isAdmin"] == true){
                return true;
            }
        }
    
        // check arrays contained in this array
        foreach ($arr as $element) {
            if (is_array($element)) {
                if (checkExisteAndAdmin($element, $key)) {
                    return true;
                }
            }
            
        }
    
        return false;
    }
?>
<script>
    var contextData = <?php echo json_encode( Element::getElementForJS(@$element, @$type) ); ?>;
    var contextId = contextData._id["$id"];
    var isEndorseBadge = false;
</script>

<script>
    directory.badgePanelHtml  = function(params){
		mylog.log("badgePanelHtml","Params",params);
        if(typeof params['badges'] == "undefined"){
            return "";
        }
        const elementId = params._id["$id"];

        <?php if(isset($element["type"]) && $element["type"] == "byReference") { ?>
            var imageUrl = '<?= @$element['assertion']['image'] ?>';
        <?php }else { ?>
            var imageUrl = '<?= @$element['profilMediumImageUrl'] ?>';
            if(imageUrl){
                var index = imageUrl.indexOf('/medium');
                imageUrl = imageUrl.substring(0, index);
                imageUrl += '/assertions/' + elementId + ".png";
            }
        <?php } ?>
            mylog.log("badge assertions URL", imageUrl);

        const attente = Boolean(params['badges'][contextId]['attenteRecepteur']);
        const toConfirm = Boolean(params['badges'][contextId]['attenteEmetteur']);
        const confirmed = !attente && !toConfirm;
        const revoked =  Boolean(params['badges'][contextId]['revoke'])

		var dateStr="";
		if(typeof params.updated != "undefined" && notNull(params.updated))
      		dateStr += /*'<div class="dateUpdated dateUpdated-sm date-position">'+*/directory.showDatetimePost(params.collection, params.id, params.updated,30)/*+'</div>'*/;
		else if(typeof params.created != "undefined" && notNull(params.created))
      		dateStr += /*'<div class="dateUpdated dateUpdated-sm date-position">'+*/directory.showDatetimePost(params.collection, params.id, params.created,30)/*+'</div>'*/;
		var str='';	
		str +='<div id="entity_'+params.collection+'_'+params.id+'" class="col-lg-4 col-md-4 col-sm-6 col-xs-12 searchEntityContainer '+params.containerClass+'">'+
				'<div class="item-slide">'+
						'<div class="entityCenter" style="position: absolute;">'+
							'<span><i class="fa fa-'+params.icon+' bg-'+params.color+'"></i></span>'+
						'</div>'+
						'<div class="img-back-card">'+
							params.imageProfilHtml +
							'<div class="text-wrap searchEntity">'+
								'<h4 class="entityName">'+
									'<a href="'+params.hash+'" class="uppercase '+params.hashClass+'">'+params.name+'</a>'+
								'</h4>'+
								'<div class="small-infos-under-title text-center">';
									if (typeof params.type != 'undefined') {
		str +=							'<div class="text-center entityType">'+	
											'<span class="text-white">'+((typeof tradCategory[params.type] != "undefined") ? tradCategory[params.type] : params.type)+'</span>'+
										'</div>';
									}
		str +=					'</div>'+
								'<div class="entityDescription"></div>'+
							'</div>'+
						'</div>'+
						//hover
						'<div class="slide-hover co-scroll">'+
							'<div class="text-wrap">'+
								'<h4 class="entityName">'+
									'<a href="'+params.hash+'" class="'+params.hashClass+'">'+params.name+'</a>'+
								'</h4>';

		str +=					'<p class="p-short">'+params.descriptionStr+'</p>';
        if(confirmed){
            str +=                  '<div class="text-center"><button onclick="openDetails(\''+elementId+'\', \''+ params.collection +'\' , \''+ contextId +'\')" class="btn btn-success" > <?php echo Yii::t("badge", "View details") ?> </button></div>'
        }
        <?php if(!Authorisation::badgeIsAdminEmetteur($element["_id"])){?>
            if(!revoked){
                if(attente && toConfirm){
                    str +=					'<p class="p-short"> <?php echo Yii::t("badge", "Waiting for award and issuer confirmation"); ?> </p>';
                }
                else if(attente){
                    str +=					'<p class="p-short"> <?php echo Yii::t("badge", "Waiting for award confirmation"); ?> </p>';
                }else if(toConfirm){
                    str +=					'<p class="p-short"> <?php echo Yii::t("badge", "Waiting for issuer confirmation"); ?> </p>';
                }
            }else{
                str +=                      '<p class="p-short"> <?php echo Yii::t("badge", "Badge revoked"); ?> </p>';
            }
        <?php }else{?>
            if(!revoked){
                if(attente && !toConfirm){
                    str +=					'<p class="p-short"><?php echo Yii::t("badge", "Waiting for award confirmation"); ?></p>';
                }
                if(toConfirm){
                    str +=                  '<div style="text-align: center;"><button onclick="confirmBadge(event, \''+params.id+'\', \''+params.collection+'\');" data-id="<?= $element["_id"] ?>" data-confirm="true" class="btn btn-success">Confirmer</button></div><div style="text-align: center;"><button onclick="confirmBadge(event, \''+params.id+'\', \''+params.collection+'\');" data-id="<?= $element["_id"] ?>" data-confirm="false" class="btn btn-danger">Réfuser</button></div>'
                }
                if(!toConfirm){
                    str +=                  '<div style="text-align: center;"><button onclick="revokeBadge(event, \''+params.id+'\', \''+params.collection+'\');" data-id="<?= $element["_id"] ?>" class="btn btn-danger"><?php echo Yii::t("badge", "Revoke") ?></button></div><div style="text-align: center;"></div>' 
                }
            }else{
                str +=                      '<p class="p-short"> <?php echo Yii::t("badge", "Badge revoked"); ?> </p>';
            }

        <?php } ?>
        
            
		str+=				'</div>'+
					'</div>'+
				'</div>'+
			'</div>';
		return str;
	}
    directory.badgePanelHtmlFullWidth = function(params){
		mylog.log("badgePanelHtml","Params",params);
        if(typeof params['badges'] == "undefined"){
            return "";
        }
        const elementId = params._id["$id"];

        <?php if(isset($element["type"]) && $element["type"] == "byReference") { ?>
            var imageUrl = '<?= @$element['assertion']['image'] ?>';
        <?php }else { ?>
            var imageUrl = '<?= @$element['profilMediumImageUrl'] ?>';
            if(imageUrl){
                var index = imageUrl.indexOf('/medium');
                imageUrl = imageUrl.substring(0, index);
                imageUrl += '/assertions/' + elementId + ".png";
            }
        <?php } ?>
            mylog.log("badge assertions URL", imageUrl);

        const attente = Boolean(params['badges'][contextId]['attenteRecepteur']);
        const toConfirm = Boolean(params['badges'][contextId]['attenteEmetteur']);
        const confirmed = !attente && !toConfirm;
        const revoked =  Boolean(params['badges'][contextId]['revoke'])

		var dateStr="";
		if(typeof params.updated != "undefined" && notNull(params.updated))
      		dateStr += /*'<div class="dateUpdated dateUpdated-sm date-position">'+*/directory.showDatetimePost(params.collection, params.id, params.updated,30)/*+'</div>'*/;
		else if(typeof params.created != "undefined" && notNull(params.created))
      		dateStr += /*'<div class="dateUpdated dateUpdated-sm date-position">'+*/directory.showDatetimePost(params.collection, params.id, params.created,30)/*+'</div>'*/;
		var str='';	
            str += `<div id="entity_${params.collection}_${params.id}" class="detailBadgeCard searchEntityContainer smartgrid-slide-element mt-20 col-lg-4 col-md-4 col-sm-6 col-xs-12">
            <div class="  card detailBadge">
            <div  class=" col-lg-4 col-md-4 col-sm-3 col-xs-3 card-img-top no-padding">
                <div >
                <div class="entityCenter" style="position: absolute;">
							<span><i class="fa fa-${params.icon} bg-${params.color}"></i></span>'
						</div>
                    <a href="${params.hash}.view.detail" class="${params.hashClass}">
                    <div class="content-img">
                    ${params.imageProfilHtml}
                    </div>
                    </a>
                </div>
                </div>
                <div class="card-body no-padding  col-lg-8 col-md-8 col-sm-9 col-xs-9">`;
                    <?php if(!Authorisation::badgeIsAdminEmetteur($element["_id"])){?>
                        if(!revoked){
                            if(attente && toConfirm){
                                str +=`<span class="discount-holder warning"><span>${tradBadge.waitingIssuerConfirmation}</span></span>`;
                            }
                            else if(attente){
                                str +=`<span class="discount-holder warning"><span> ${tradBadge.waitingAwardCofirmation}</span></span>`;
                            }else if(toConfirm){
                                str +=`<span class="discount-holder warning"><span> ${tradBadge.waitingIssuerConfirmation}</span></span>`;
                            }
                        }else{
                            str +=`<span class="discount-holder danger"><span>  ${tradBadge.badgeRevoked}</span></span>`;
                        }
                    <?php }else{?>
                        if(!revoked){
                            if(attente && !toConfirm){
                                str +=`<span class="discount-holder warning"><span> ${tradBadge.waitingAwardCofirmation} </span></span>`;
                            }                            
                        }else{
                            str +=`<span class="discount-holder danger"><span>   ${tradBadge.badgeRevoked} </span></span>`;
                        }

                    <?php } ?>
                    
                    str+= `<div class="detail-box ">
                        <a href="${params.hash}.view.detail" class="${params.hashClass}">
                            <h4>${params.name} </h4>
                        </a>`;               
                        str += `<div class="entityLocality">`
                            if (notNull(params.localityHtml)) {     
                                str += `<span> ${params.localityHtml}</span>`
                            }
                        str += `</div>`;
                    
                        str+= `<div><span>`;
                            if(confirmed){
                                str +='<button onclick="openDetails(\''+elementId+'\', \''+ params.collection +'\' , \''+ contextId +'\')" class="btn openDetails" > <?php echo Yii::t("badge", "View details") ?> </button>';
                                str +='<button onclick="openEndorse(event, \''+ contextId +'\', \''+elementId+'\', \''+ params.collection +'\' ,)" class="btn openDetails" > <?php echo Yii::t("badge", "View endorsements") ?> </button>';
                            }   
                            <?php
                            if(Authorisation::badgeIsAdminEmetteur($element["_id"])){?>
                                if(!revoked){                            
                                    if(toConfirm){
                                        str += '<button style="font-size: 12px;" onclick="viewNarratives(\''+elementId+'\', \''+ params.collection +'\' , \''+ contextId +'\');" data-id="<?= $element["_id"] ?>" class="btn openDetails">'+tradBadge.viewDetails+'</button>'

                                        str += '<button style="font-size: 12px;" onclick="confirmBadge(event, \''+params.id+'\', \''+params.collection+'\');" data-id="<?= $element["_id"] ?>" data-confirm="true" class="btn btn-success">'+tradBadge.confirm+'</button>'
                                        str += '<button style="text-align: center; font-size: 12px; "onclick="confirmBadge(event, \''+params.id+'\', \''+params.collection+'\');" data-id="<?= $element["_id"] ?>" data-confirm="false" class="btn btn-danger">'+trad.refuse+'</button>'
                                    }
                                    if(!toConfirm){
                                        str += '<button style="font-size: 12px;" onclick="revokeBadge(event, \''+params.id+'\', \''+params.collection+'\');" data-id="<?= $element["_id"] ?>" class="btn btn-danger">'+tradBadge.revoke+'</button></div>' 
                                    }
                                }else{
                                    str += '<button style="font-size: 12px;" onclick="resetBadge(event, \''+params.id+'\', \''+params.collection+'\');" data-id="<?= $element["_id"] ?>" class="btn btn-danger"><?php echo Yii::t("common", "Reset") ?></button></div>' 
                                }
                            <?php } ?>
                        str += ` </span></div>
                    </div>
                </div>
            </div>
            </div>
            `
            ;	

		return str;
	}
    function openDetails(elementId, elementCollection, badgeId){
        console.log(elementId, elementCollection, badgeId);
        coInterface.showLoader("#details-content");
        getAjax("#details-content", baseUrl + '/co2/badges/details/element/' + elementId + "/collection/" + elementCollection + "/badge/" + badgeId, null, 'html');
        $("#modal-details").modal("show");
    }

    function viewNarratives(elementId, elementCollection, badgeId){
        console.log(elementId, elementCollection, badgeId);
        coInterface.showLoader("#details-content");
        getAjax("#details-content", baseUrl + '/co2/badges/details/narrative/true/element/' + elementId + "/collection/" + elementCollection + "/badge/" + badgeId, null, 'html');
        $("#modal-details").modal("show");
    }


    function endorse(elementId, elementCollection, badgeId){
        dyFObj.openForm("endorsement", null, {
            claimBadge: badgeId,
            claimElementId: elementId,
            claimElementType: elementCollection
        })
    }
    function openEndorse(event, badgeId, elementId, elementType){
        event.stopPropagation();
        event.preventDefault();
        $("#modal-endosser").data("badge", badgeId);
        $("#modal-endosser").data("elementId", elementId);
        $("#modal-endosser").data("elementType", elementType);
        getAjax("#endosser-content", baseUrl + '/co2/badges/endorsement-modal/badge/' + badgeId + '/id/' + elementId + '/type/' + elementType, null, 'html');
        $("#modal-endosser").modal("show");
    }
    $(document).ready(function() {
    //Set the carousel options
        $('#quote-carousel').carousel({
            pause: true,
            interval: 4000,
        });
    });
    var dynFormIEndorse = {
        "beforeBuild": {
            "properties": {
                "issuer": {
                    "inputType": "finder",
                    "label": "<?= Yii::t("badge", "Endorse as") ?>",
                    "multiple": false,
                    "initMe": true,
                    "rules": { "required": true, "lengthMin": [1] },
                    "initType": [],
                    "initBySearch": true,
                    "openSearch": true,
                    "initContacts": false,
                }
            }
        }
    };
</script>



<div class="container-fluid header-badge no-padding">
<?php if(isset($element["type"]) && $element["type"] == "byReference") { ?>

    <section class="about-section">
        <div class="container">
            <div class="row">
                <h1 class="text-center">
                    Badge
                </h1>
            </div>
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 about_img text-center">
                    <div class="about_img-inner">
                        <img class="img-responsive margin-auto" src="<?= @$element['badgeClass']['image'] ?>" alt="Badges Image">
                    </div>
                </div>
                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 about-text">
                    <div class="section-heading space-overflow">
                        <h1 class="section-title"><span><?= $element['name'] ?></span></h1>
                            <?php if(isset($element['issuer'])) {
                                $emeteur = '<a href="'. $element["issuer"]["url"] .'"> '.  $element["issuer"]["name"] .' </a>';
                            ?>
                            <div class="section-description creator"><?php echo Yii::t("badge", "Issuer") ?> : <?= $emeteur ?></div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php }else{ ?>
    <section class="about-section">
        <div class="container">
            <div class="row">
                <h1 class="text-center">
                    <?= (isset($element['isParcours']) && $element['isParcours'] != "false") ? Yii::t("badge", "Pathway") : "Badge" ?>
                </h1>
            </div>
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 about_img margin-top-20">
                    <div class="about_img-inner">
                        <img class="img-responsive margin-auto" src="<?= @$element['profilMediumImageUrl'] ?>" alt="Badges Image">
                    </div>
                    <div class="col-xs-12 text-center margin-top-20">
                        <?php if(isset(Yii::app()->session["userId"]) && Authorisation::badgeIsAdminEmetteur($element['_id'])){ ?>
                            <button id="btn-modifier-badge" class="btn custom-btn-nav"><i class="fa fa-edit"></i> <?php echo Yii::t("common", "Edit") ?> </button>
                            <script>
                                $('#btn-modifier-badge').off().on('click', () => {
                                    dyFObj.editElement('badges', contextId);
                                });
                            </script>
                        <?php }?>
                        <?php if(  isset(Yii::app()->session["userId"]) && (Authorisation::badgeIsAdminEmetteur($element['_id']) || (isset($element['preferences']) && isset($element['preferences']['private']) && !($element['preferences']['private'])))){ ?>
                            <button id="btn-assign-badge" class="btn custom-btn-nav"><i class="fa fa-chain"></i> <?php echo Yii::t("badge", "Assign") ?> </button>

                            <script>
                                $("#btn-assign-badge").off().on('click',function(e){
                                    e.preventDefault();
                                    dyFObj.openForm("assignbadge",null, {badgeId: contextId});
                                });
                            </script>
                        <?php } ?>
                        <?php if(isset(Yii::app()->session["userId"]) && Authorisation::badgeIsAdminEmetteur($element['_id'])){ ?>
                            <button id="btn-delete-badge" class="btn custom-btn-nav"><i class="fa fa-trash"></i> <?php echo Yii::t("common", "Delete") ?> </button>

                            <script>
                                $("#btn-delete-badge").off().on('click',function(e){
                                    e.preventDefault();
                                    var urlToSend = baseUrl+"/"+moduleId+"/element/delete/type/badges/id/<?= $element['_id'] ?>";
                                    bootbox.confirm(trad.areyousuretodelete,
                                    function(result) 
                                    {
                                    if (!result) {
                                        if(typeof btnClick !="undefined" && btnClick != null)
                                        btnClick.empty().html('<i class="fa fa-trash"></i>');
                                        return;
                                    } else {
                                        ajaxPost(
                                                    null,
                                                    urlToSend,
                                                    {},
                                                    function(data){ 
                                                        if ( data && data.result ) {
                                                toastr.info("élément effacé");
                                                urlCtrl.loadByHash( "#badge" );
                                            } else {
                                                toastr.error("something went wrong!! please try again.");
                                            }
                                        });
                                    }
                                    });
                                });
                            </script>
                        <?php } ?>
                        <button id="btn-i-endorse" class="btn custom-btn-nav"><i class="fa fa-thumbs-o-up"></i> <?php echo Yii::t("badge", "I endorse it") ?> </button>
                        <button id="btn-endorse" class="btn custom-btn-nav"><i class="fa fa-thumbs-o-up"></i> <?php echo Yii::t("badge", "Endorse via another element") ?> </button>

                        <script>
                            $("#btn-endorse").off().on('click',function(e){
                                e.preventDefault();
                                dyFObj.openForm("endorsement",null, {
                                    claimElementId: contextId,
                                    claimElementType: contextData.collection
                                }, null, {
                                    "beforeBuild": {
                                        "properties": {
                                            issuer: {
                                                inputType: "finder",
                                                label: "<?= Yii::t("badge", "Endorse as") ?>",
                                                multiple: false,
                                                initMe: false,
                                                rules: { required: true, lengthMin: [1] },
                                                initType: ["organizations", "projects"],
                                                initBySearch: true,
                                                openSearch: true,
                                                initContacts: false
                                            }
                                        }
                                    }
                                });
                            });
                            $("#btn-i-endorse").off().on('click',function(e){
                               e.preventDefault();
                                dyFObj.openForm("endorsement",null, {
                                    claimElementId: contextId,
                                    claimElementType: contextData.collection
                                }, null, dynFormIEndorse);
                            });
                        </script>
                    </div>
                </div>
                <div class="col-lg-8 col-md-8 col-sm-7 col-xs-12 about-text">
                    <div class="">
                        <div class="section-heading space-overflow">
                        <h1 class="section-title"><span><?= $element['name'] ?></span></h1>
                        <div class="row">
                    <?php if(isset($element['preferences']) && isset($element['preferences']['private'])){ ?>
                            <div class="col-xs-12 col-md-4 col-sm-4">
                                 <div class="section-description preferences"><i class="fa fa-unlock-alt"></i> <span class="label-preferences"><?php echo Yii::t("badge", "Public") ?> :</span> <?= $element['preferences']['private'] ? Yii::t("common", "No") : Yii::t("common", "Yes") ?></div>
                            </div>
                      <?php } ?>
                            <div class="col-xs-12 col-md-8 col-sm-8">
                                <div class="section-description creator"><i class="fa fa-user-circle"></i> <span class="label-preferences"><?php echo Yii::t("badge", "Made by") ?> :</span> <?= $element['creator']['name'] ?></div>
                            </div>
                        </div>
                    <?php if(isset($element['issuer']) && count($element['issuer']) > 0) {
                        $emeteur = '';
                        foreach($element['issuer'] as $id => $value){
                            $emeteur .= '<a class="lbh" href="#page.type.'.$value["type"].'.id.'.$id.'"> '. $value["name"] .' </a>';
                        }
                    ?>
                        <div class="section-description creator"><i class="fa fa-arrow-circle-o-right"></i> <span class="label-preferences"><?php echo Yii::t("badge", "Issuer") ?> :</span> <?= $emeteur ?></div>
                    <?php } ?>

                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="section-description ">
                                    <i class="fa fa-star"></i>
                                    <span class="label-preferences"><?php echo Yii::t("badge", "Criteria"); ?> :</span>
                                </div>
                                <div class="markdown moreText"><?php if(isset($element["criteria"]) && isset($element["criteria"]["narrative"])) echo $element["criteria"]["narrative"]?></div>
                            </div>

                        </div>
                        <?php if(isset($element["description"])){ ?>
                                    <!--<h4></h4>-->
                            <div class="section-description ">
                                <i class="fa fa-file-text-o"></i>
                                <span class="label-preferences"><?php echo Yii::t("common", "Description"); ?>:</span>
                            </div>
                            <div class="markdown moreText margin-bottom-25 margin-top-20"><?php echo $element["description"] ?></div>
                        <?php } ?>


                    </div>
                </div>
            </div>
        </div>
    </section>
<?php } ?>
<?php if(isset($endorsements) && !empty($endorsements)){ ?>
<div class="container-fluid">
  <div class='row'>
    <div class='col-md-offset-2 col-md-8'>
      <div class="carousel slide" data-ride="carousel" id="quote-carousel">
        <!-- Bottom Carousel Indicators -->
        <ol class="carousel-indicators">
            <?php for ($i=0; $i < count($endorsements); $i++) {?>
                <li data-target="#quote-carousel" data-slide-to="<?= $i ?>" <?= $i == 0 ? "class='active'": "" ?>></li>
            <?php }?>
        </ol>
        
        <!-- Carousel Slides / Quotes -->
        <div class="carousel-inner">
          <?php 
          for ($i=0; $i < count($endorsements); $i++) { 
            $value = $endorsements[array_keys($endorsements)[$i]]; 
            $issuerId = array_keys($value["issuer"])[0]; 
            $issuer = $value["issuer"][$issuerId];
            ?>
                <div class="item <?= $i == 0 ? "active": "" ?>">
                    <blockquote>
                        <div class="row">
                            <!-- <div class="col-sm-3 text-center">
                                <img class="img-circle" src="http://www.reactiongifs.com/r/overbite.gif" style="width: 100px;height:100px;">
                            </div>
                             -->
                            <div class="col-sm-12">
                                <p><?= $value["endorsementComment"] ?></p>
                                <?php if((Yii::app()->session['userId'] == $issuerId) || (checkExisteAndAdmin($myContacts, $issuerId))) { ?>
                                    <script>
                                        <?php
                                            if(Yii::app()->session['userId'] == $issuerId) { ?>
                                            isEndorseBadge = true;
                                        <?php } ?>
                                    </script>
                                    <a class="edit-endorse" data-id="<?= (String)$value['_id'] ?>" type="button">
                                        <i class="fa fa-pencil"></i>
                                    </a>
                                <?php } ?>
                                <a class="lbh-preview-element" href="#page.type.<?= $issuer["type"] ?>.id.<?= $issuerId ?>"><small><?= $issuer["name"] ?></small></a>
                                
                            </div>
                        </div>
                    </blockquote>
                </div>
            <?php } ?>
        </div>
        
        <script>
            $(".edit-endorse").off().on('click',function(e){
                e.preventDefault();
                let id = $(this).data('id');
                dyFObj.editElement("endorsements", id, {
                    claimElementId: contextId,
                    claimElementType: contextData.collection
                });
            });
        </script>
        <!-- Carousel Buttons Next/Prev -->
        <a data-slide="prev" href="#quote-carousel" class="left carousel-control"><i class="fa fa-chevron-left"></i></a>
        <a data-slide="next" href="#quote-carousel" class="right carousel-control"><i class="fa fa-chevron-right"></i></a>
      </div>                          
    </div>
  </div>
</div>

<?php } ?>

</div>
<div class="container-fluid body-badge">


<div class="container-badge">
    <div class="row">
    <div id="menuCommunity" class="badgeCommunity col-md-12 col-sm-12 col-xs-12 numberCommunity">
    <a href="javascript:;" data-view="all" class="uppercase load-community active"><i class="fa fa-bookmark"></i> <span class="hidden-xs"><?php echo Yii::t("badge", "All") ?></span></a>
    <?php if(!(isset($element["type"]) && $element["type"] == "byReference")) { ?>
        <a href="javascript:;" data-view="confirmed" class="uppercase load-community"><i class="fa fa-check"></i> <span class="hidden-xs"><?php echo Yii::t("badge", "Confirmed") ?></span></a>
        <a href="javascript:;" data-view="attente" class="uppercase load-community"><i class="fa fa-clock-o"></i> <span class="hidden-xs"><?php echo Yii::t("badge", "Pending") ?></span>
        <div class="indicateur" style="display: none;">10</div>
        </a>
        <a href="javascript:;" data-view="revoked" class="uppercase load-community"><i class="fa fa-times"></i> <span class="hidden-xs"><?php echo Yii::t("badge", "Revoked") ?></span></a>
        <?php if(Authorisation::badgeIsAdminEmetteur($element['_id'])){?> 
            <script>
                function putCountsAttente(){
                    getAjax(null, baseUrl +'/co2/badges/count-attente/badge/' + contextId, data => {
                        mylog.log("COUNT", data.attente);
                        const confirm = data['to-confirm'];
                        const attente = data.attente;
                        if(confirm == 0){
                            $('a[data-view="confirm"] .indicateur').hide();
                        }else{
                            $('a[data-view="confirm"] .indicateur').show();
                            $('a[data-view="confirm"] .indicateur').text(confirm);
                        }
                        if(attente == 0){
                            $('a[data-view="attente"] .indicateur').hide();
                        }else{
                            $('a[data-view="attente"] .indicateur').show();
                            $('a[data-view="attente"] .indicateur').text(attente);
                        }
                    })
                }
                $(() => {
                      putCountsAttente();
                })
            </script>
            <a href="javascript:;" data-view="confirm" class="uppercase load-community"><i class="fa fa-square-o"></i> <span class="hidden-xs"><?php echo Yii::t("badge", "To be confirmed") ?></span><div class="indicateur" style="display: none;">10</div></a>
        <?php } ?>
        <?php if(isset($element['isParcours']) && $element['isParcours'] != "false"){ ?>
            <a href="javascript:;" data-view="parcours" class="uppercase load-community"><i style="transform: rotateZ(-90deg);" class="fa fa-sitemap"></i> <span class="hidden-xs"><?php echo Yii::t("badge", "Pathway") ?></span></a>
        <?php } ?>
        </div>
    <?php } ?>
    </div>
    <hr id="badge-separator">
    <div class="row">
        <div id="main-container" class="col-md-12">
        </div>
    </div>
</div>
</div>
<div class="portfolio-modal modal fade in" id="modal-details" tabindex="-1" role="dialog" aria-hidden="false" style="z-index: 99999; padding-left: 0px; top: 56px; display: none; left: 0px;">
    <div class="modal-content padding-top-15">
        <div class="close-modal" data-dismiss="modal">
            <div class="lr">
                <div class="rl">
                </div>
            </div>
        </div>
        

        <div class="container">
            <div id="details-content" style="height: 100%; width: 100%">

            </div>
        </div>
    </div>
</div>

<div class="portfolio-modal modal fade in" id="modal-endosser" tabindex="-1" role="dialog" aria-hidden="false" style="z-index: 99999; display: none;">
<div class="modal-content padding-top-15">
    <div class="close-modal" data-dismiss="modal">
        <div class="lr">
            <div class="rl">
            </div>
        </div>
    </div>
    <div id="endosser-content" style="height: 100%; width: 100%">
    </div>
</div>
</div>

<script>
    
    $(function() {
        inintDescs();
        AddReadMore();
        const hash = hashT[0];
        const str = '.views.';
        var index = hash.indexOf(str);
        if(index > -1){
            var indexStart = index + str.length;
            var indexStop = hash.indexOf('.', indexStart);
            var tmpStr
            if(indexStop > -1){
                tmpStr  = hash.substring(indexStart, indexStop);
            }else{
                tmpStr  = hash.substring(indexStart);
            }
            loadView(tmpStr, index, indexStop);
        }else{
            loadView('all');
        }
        if(isEndorseBadge){
            $("#btn-i-endorse").hide();
        }
    });
    function loadView(view, index = -1){
        var url = baseUrl + '/' + moduleId + '/badges/views/view/' + view + '/badge/' + contextId;
        getAjax('#main-container', url, null, "html");
        var oldHash = getHash();
        if(index > -1){
            var newHash = window.location.href.replace(location.hash, "") + oldHash.substring(0, index) + '.views.' + view;
            window.history.pushState("", document.title, newHash);
        }else{
            window.history.replaceState("", document.title, window.location.href.replace(location.hash, "") + getHash() + '.views.' + view);
        }
        $('a.load-community').removeClass("active");
        $('a[data-view="'+view+'"]').addClass('active');
    }

    $('a.load-community').off().on('click', (e) => {
        e.preventDefault();
        var hash = getHash();
        const str = '.views.';
        var index = hash.indexOf(str);
        const view = $(e.currentTarget).data("view");
        loadView(view,index);
    });

    function getHash(){
        var hash = location.hash;
        if(hash.indexOf("?") >= 0){
			hash=hash.split("?");
			hash=hash[0];
		}
        return hash;
    }

    function confirmBadge(event, awardId, awardType){
        event.preventDefault();
        event.stopPropagation();
        var target = event.currentTarget;
        var value = $(target).data('confirm');
        var badgeId = $(target).data('id');
        var url = baseUrl + '/co2/badges/confirm/badge/' + badgeId + '/award/' + awardId + '/type/' + awardType + '/confirm/' + value + 
        '/confirmType/emetteur';
        $.ajax({
            type:"GET",
            url: url,
            success:function(data) {
                if(value){
                    toastr.success("Badges assigné avec succes");
                }else{
                    toastr.success("Badges refusé avec succes");
                }
                if(filterGroupbadge){
                    filterGroupbadge.search.init(filterGroupbadge);
                }
                <?php if(Authorisation::badgeIsAdminEmetteur($element['_id'])){?> 
                    putCountsAttente();
                <?php } ?>
            },
            error:function (xhr, ajaxOptions, thrownError){
                toastr.error("Erreur lors de l'assignation des badges");
            }
        });
    }
    function revokeBadge(event, awardId, awardType){
        event.preventDefault();
        event.stopPropagation();
        var target = event.currentTarget;
        var value = $(target).data('confirm');
        var badgeId = $(target).data('id');
        const data = {
            badgeId,
            awardId,
            awardType
        }
        dyFObj.openForm("revokebadge", null, data, null, {
            afterSave: (data) => dyFObj.commonAfterSave(data, () => {
                <?php if(Authorisation::badgeIsAdminEmetteur($element['_id'])){?> 
                    putCountsAttente();
                <?php } ?>
            })
        });
    }
    function resetBadge(event, awardId, awardType){
        event.preventDefault();
        event.stopPropagation();
        var target = event.currentTarget;
        var badgeId = $(target).data('id');
        var url = baseUrl + '/co2/badges/cancel-revoke/badge/' + badgeId + '/award/' + awardId + '/type/' + awardType;
        $.ajax({
            type:"GET",
            url: url,
            success:function(data) {
                if(data.result){
                    toastr.success("Badges reinitialisé");
                }else{
                    toastr.error("Erreur lors de la reinitialisation du badges");
                }
                if(filterGroupbadge){
                    filterGroupbadge.search.init(filterGroupbadge);
                }
                <?php if(Authorisation::badgeIsAdminEmetteur($element['_id'])){?> 
                    putCountsAttente();value
                <?php } ?>
            },
            error:function (xhr, ajaxOptions, thrownError){
                toastr.error("Erreur lors de l'assignation des badges");
            }
        });
    }
    function inintDescs() {
        $.each($(".markdown"), function(k,v){
            descHtml = dataHelper.markdownToHtml($(v).html()); 
            $(v).html(descHtml);
        });
        
    }
    function AddReadMore() {
        var showChar = 250;
        var ellipsestext = "...";
        var moretext = "Lire la suite";
        var lesstext = "Lire moins";
        $('.moreText').each(function(k,v) {
            var content =  $(v).html();
            var textcontent =  $(v).text();
            if (textcontent.length > showChar) {
                var c = textcontent.substr(0, showChar);
                var html = '<span class="container-text "><p>' + c  + '<span class="moreelipses">' + ellipsestext + '</span></p></span><div class="morecontent">' + content + '</div>';
                $(v).html(html);
                $(v).after('<a href="" class="morelink">' + moretext + '</a>');
            }

        });

        $(".morelink").click(function() {
            if ($(this).hasClass("less")) {
                $(this).removeClass("less");
                $(this).html(moretext);
                $(this).prev().children('.morecontent').fadeToggle(500, function(){
                $(this).prev().fadeToggle(500);
                });
            
            } else {
                $(this).addClass("less");
                $(this).html(lesstext);
                $(this).prev().children('.container-text').fadeToggle(500, function(){
                $(this).next().fadeToggle(500);
                });
            }
            return false;
        });
    }
    

</script>