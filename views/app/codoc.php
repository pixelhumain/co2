<link rel="stylesheet" href="https://uicdn.toast.com/editor/latest/toastui-editor.min.css" />

<link rel="stylesheet" href="<?= $assetsUrl ?>/css/codoc/index.css">
<link rel="stylesheet" href="<?= $assetsUrl ?>/css/codoc/theme-switcher.css">
<link rel="stylesheet" href="<?= $assetsUrl ?>/css/codoc/markdown-light.css">
<link rel="stylesheet" href="<?= $assetsUrl ?>/css/codoc/loader.css">

<div class="codoc-container">
    <div class="loader-container">
        <div class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
        <p>chargement...</p>
    </div>

    <nav class="codoc-nav">
        <div class="nav-header">
            <a href="/#codoc"><span><i class="fa fa-book" aria-hidden="true"></i> </span><span>CO</span><span>Doc.</span></a>
        </div>
        <div class="nav-body">
            
        </div>
    </nav>
    <div class="page-content">
        <div class="sub-header">
            <div class="custom-breadcrumb"></div>
            <div class="actions-container">
                <?php if(isset($_SESSION["userId"]) && Authorisation::isUserSuperAdmin($_SESSION["userId"])){ ?>
                <button type="button" class="btn-content-action d-none" data-action="save"><i class="fa fa-check" aria-hidden="true"></i></button>
                <button type="button" class="btn-content-action d-none" data-action="cancel"><i class="fa fa-undo" aria-hidden="true"></i></button>
                <button type="button" class="btn-content-action d-none" data-action="edit"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                <?php } ?>
            </div>
        </div>
        <div class="doc-container">
            <div id="md-viewer" class="markdown-body">
    
            </div>
        </div>
    </div>
</div>

<script src="https://uicdn.toast.com/editor/latest/toastui-editor-all.min.js"></script>

<script src="<?= $assetsUrl ?>/js/codoc/api.js"></script>
<script src="<?= $assetsUrl ?>/js/codoc/utils.js"></script>
<script src="<?= $assetsUrl ?>/js/codoc/main.js"></script>

