<?php

use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\utils\Utils;
// $cssAnsScriptFilesTheme = array(
//     '/plugins/showdown/showdown.min.js',
// );
// HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesTheme, Yii::app()->request->baseUrl);
$isActivitypubEnabled  = Utils::isActivitypubEnabled();
?>
<style type="text/css">
	/*#filters-nav{
		right: 0px;
	    text-align: center;
	    padding-left: 0px;
	    box-shadow: none;
	    border-bottom: none;
	}
	#filters-nav > *{
		display: inline-grid;
		margin-left: 5px;
    	margin-right: 5px;
	}
	#filters-nav .searchBar-filters{
	    width: 100%;
	    padding-left: 15%;
	    padding-right: 15%;
	    margin-bottom: 15px;
	    display: inline-flex;
	}
	#activeFilters{
		margin:0px; 
		display: block;
		text-align: left;
	}*/

	.fc-unthemed .fc-popover {
		position: absolute;
		top: 30px !important;
		left:
			40% !important;
	}

	/* Masquer la case à cocher */
	#check-filter-fediverse {
		position: absolute;
		left: -9999px;
	}

	/* Style personnalisé pour l'étiquette */
	#check-filter-fediverse+label {
		display: inline-block;
		margin-left: 5px;
		padding: 5px 10px;
		background-color: #ccc;
		color: #000;
		cursor: pointer;
	}

	/* Style personnalisé pour l'étiquette lorsque la case à cocher est cochée */
	#check-filter-fediverse:checked+label {
		background-color: #007bff;
		color: #fff;
	}
</style>
<script type="text/javascript">
	var pageApp = <?php echo json_encode(@$page); ?>;
	var isActivitypubActivate = <?php echo $isActivitypubEnabled == true ? "true" : "false"; ?>;
	var appConfig = <?php echo json_encode(@$appConfig); ?>;
	var eventList = <?php echo json_encode(Event::$types); ?>;
	var paramsFilter = {
		container: "#filters-nav",
		urlData: baseUrl + "/co2/search/agenda",
		loadEvent: {
			default: "agenda"
		},
		defaults: {
			types: ["events"],
			filters: {}
		},

		header: {
			options: {
				left: {
					classes: 'col-xs-8 elipsis no-padding',
					group: {
						count: true
					}
				},
				right: {
					classes: 'col-xs-4 text-right no-padding',
					group: {
						fediverse: true,
						calendar: true,
						map: true
					}
				}
			}
		},
		results: {
			renderView: "directory.eventPanelHtml",
			smartGrid : true
		},
		filters: {
			text: true,
			scope: true,
			type: {
				name: trad.category,
				view: "dropdownList",
				event: "filters",
				type: "type",
				keyValue: false,
				list: eventList
			}
		}
	};

	// si l'utisateur n'a pas activé activitypub, il ne pas necessaire d'affiche le filtre

	if (!isActivitypubActivate) {
		delete paramsFilter.header.options.right.group.fediverse;
	}
	if (typeof appConfig.calendarOptions != "undefined") {
		paramsFilter.agenda = {
			options: {
				calendarConfig: appConfig.calendarOptions
			}
		}
	}
	if (notNull(costum) && typeof costum.filters != "undefined" && typeof costum.filters.sourceKey != "undefined")
		paramsFilter.defaults.sourceKey = costum.slug;

	if (typeof appConfig.filters != "undefined") {
		//alert(appConfig.filters.costumContext);
		if (typeof appConfig.filters.costumContext != "undefined" && appConfig.filters.costumContext)
			paramsFilter.defaults.filters['organizer.' + costum.contextId] = {
				'$exists': 1
			};

	}
	if (typeof appConfig.results != "undefined")
		$.extend(paramsFilter.results, appConfig.results);
	if (typeof appConfig.extendFilters != "undefined") {
		$.extend(paramsFilter.filters, appConfig.extendFilters);
	}
	var filterSearch = {};
	jQuery(document).ready(function() {
		mylog.log("AGENDA filters.php");
		filterSearch = searchObj.init(paramsFilter);
		if (wsCO) {

			wsCO.on('new-event', function() {
				filterSearch.search.init(filterSearch);
			})
		}
	});
</script>