
<script type="text/javascript">

    
	var appConfig=<?php echo json_encode(@$appConfig); ?>;
	var paramsFilterBadge = {
            urlData: baseUrl + "/co2/search/globalautocomplete",
            container: "#filterContainer",
            loadEvent:{
                default:"scroll"
            },
            header: {
                dom: ".headerSearchIncommunity",
                options: {
                    left: {
                        classes: 'col-xs-8 elipsis no-padding',
                        group: {
                            count: true,
                            types: true
                        }
                    }
                },
            },
            defaults: {
                indexStep: 10,
                notSourceKey: true,
                types: ["badges"],
                forced: {
                    filters: {
                        'preferences.private':  false
                    }
                }
            },
            results: {
                dom: ".bodySearchContainer",
                smartGrid: true,
                renderView: "directory.badgeElementPanelHtml",
                map: {
                    active: false
                }
            },
            filters: {
                text: true,
                // category : {
                //     name : trad.category,
                //     view : "dropdownList",
                //     event : "filters",
                //     type : "category",
                //     multiple: true,
                //     keyValue: true,
                //     list : [
                //         "domainAction",
                //         "cibleDD",
                //         "strategy"
                //     ]
	 		    // },
                isParcours : {
                    name : "Parcours ?",
                    view : "dropdownList",
                    type : "filters",
                    event : "filters",
                    action : "filters",
                    keyValue: false,
                    field: "isParcours",
                    list : {
                        "true" : trad.yes,
                        "false" : trad.no
                    }
	 		    },
                 issuer : {
                    name : "<?php echo Yii::t("badge","Filter by issuer"); ?>",
                    view : "dropdownList",
                    type : "filters",
                    event : "exists",
                    action : "filters",
                    typeList : "object",
                    keyValue: false,
                    list : <?php echo json_encode($issuers) ?>
	 		    },
            }
        };

    mylog.log("appConfig",appConfig);    

	if(typeof appConfig.filters != "undefined"){
        if(typeof appConfig.filters.sourceKey != "undefined"){
            paramsFilterBadge.defaults.sourceKey=appConfig.filters.sourceKey;
            if(typeof paramsFilterBadge.defaults.notSourceKey!="undefined"){
                delete paramsFilterBadge.defaults.notSourceKey;
            }
        }
    }
	
	var filterGroupbadge={}; 
	jQuery(document).ready(function(){
		mylog.log('filterSearch paramsFilter', paramsFilterBadge);
        if(Object.keys($("#activeFilters .filters-activate[data-type='types']")).length >0 )
            $("#activeFilters .filters-activate[data-type='types']").fadeOut().remove();
		filterGroupbadge = searchObj.init(paramsFilterBadge);
		filterGroupbadge.search.init(filterGroupbadge);
	});
</script>