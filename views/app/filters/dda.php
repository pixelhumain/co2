<?php 
	$filliaireCategories = CO2::getContextList("filliaireCategories"); 
?>
<style type="text/css">
	#filters-nav{
		right: 0px;
	    text-align: center;
	    padding-left: 0px;
	    /*padding-top: 30px !important;*/
	    box-shadow: none;
	    border-bottom: none;
	}
	#filters-nav > *{
		display: inline-grid;
		margin-left: 5px;
    	margin-right: 5px;
	}
	#filters-nav .searchBar-filters{
	    width: 100%;
	    padding-left: 15%;
	    padding-right: 15%;
	    margin-bottom: 15px;
	    display: inline-flex;
	}
	#activeFilters{
		margin:0px; 
		display: block;
		text-align: left;
	}

	.fc-unthemed .fc-popover {
	   	position: absolute;
	    top: 30px !important;
	    left: 40% !important;
	}
</style>
<script type="text/javascript">
	var pageApp=<?php echo json_encode(@$page); ?>;
	var appConfig=<?php echo json_encode(@$appConfig); ?>;
	var filliaireCategories=<?php echo json_encode(@$filliaireCategories); ?>;
	var paramsFilter= {
	 	container : "#filters-nav",
	 	defaults : {
 			types : ["proposals"],
 			fields : ["status", "producer", "amendementActivated", "amendementAndVote", "amendementDateEnd", "voteActivated", "voteDateEnd", "majority", "voteAnonymous", "answers", "votes", "voteCanChange"]
 		}, 
 		results : {
 			renderView : "directory.coopPanelHtml"
 		},
	 	filters : {
	 		text : true,
	 		scope : true,
	 		themes : {
	 			lists : filliaireCategories
	 		}
	 	}
	};
	if(typeof appConfig.results != "undefined")
		$.extend(paramsFilter.results, appConfig.results);
	if(typeof appConfig.extendFilters != "undefined")
		$.extend(paramsFilter.filters, appConfig.extendFilters);
	var filterSearch={};
	jQuery(document).ready(function() {
		mylog.log("DDA filters.php");
		filterSearch = searchObj.init(paramsFilter);
	});
</script>