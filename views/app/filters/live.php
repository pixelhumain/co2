<?php 
    $cssAnsScriptFilesModule = array(
    	'/css/filters.css',
	);
    HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, $this->module->getParentAssetsUrl());
?> 
<style type="text/css">
	#filters-nav{
		right: 0px;
	    text-align: center;
	    padding-left: 0px;
	    /*padding-top: 30px !important;*/
	    box-shadow: none;
	    border-bottom: none;
	}
	#filters-nav > *{
		display: inline-grid;
		margin-left: 5px;
    	margin-right: 5px;
	}
	#filters-nav .searchBar-filters{
	    width: 100%;
	    padding-left: 15%;
	    padding-right: 15%;
	    margin-bottom: 15px;
	    display: inline-flex;
	}
	#activeFilters{
		margin:0px; 
		display: block;
		text-align: left;
	}
</style>
<?php 
	$filliaireCategories = CO2::getContextList("filliaireCategories"); 
?>
<script type="text/javascript">
	var pageApp=<?php echo json_encode(@$page); ?>;
	var filliaireCategories=<?php echo json_encode(@$filliaireCategories); ?>;
	var paramsFilter= {
	 	container : "#filters-nav",
	 	interface : {
	 		events : {
	 			scroll : true,
	 			scrollOne : true
	 		}
	 	},
	 	filters : {
	 		text : true,
	 		scope : true,
	 		themes : {
	 			filliaireCategories : filliaireCategories
	 		},
	 		types : {
	 			lists : ["NGO", "LocalBusiness", "Group", "GovernmentOrganization", "Cooperative" ,"projects", "citoyens", "poi"]
	 		}
	 	}
	};
	var filterSearch={};
	jQuery(document).ready(function() {
		mylog.log('filterSearch paramsFilter', paramsFilter);
		filterSearch = searchObj.init(paramsFilter);
	});
</script>