<?php 
	use Yii;

	$filliaireCategories = CO2::getContextList("filliaireCategories"); 
	$odd = CO2::getContextList("odd"); 
	$costumAssets = Yii::app()->getModule('costum')->assetsUrl;
	$cssAnsScriptFilesModule = array(
		'/js/default/activitypub.js'
	);
	HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, $this->module->assetsUrl);
?>
<script type="text/javascript">
	var pageApp=<?php echo json_encode(@$page); ?>;
	var appConfig=<?php echo json_encode(@$appConfig); ?>;
	var filliaireCategories=<?php echo json_encode(@$filliaireCategories); ?>;
	var oddList=<?php echo json_encode(@$odd); ?>;
	var listInit=["NGO", "LocalBusiness", "Group", "GovernmentOrganization", "Cooperative" ,"projects", "citoyens", "poi"];
	var defaultFilters = {'$or':{}};
	if(typeof costum != "undefined" && notNull(costum)){
		defaultFilters['$or']["parent."+costum.contextId+".type"] = costum.contextType;
	    defaultFilters['$or']["source.keys"] = costum.slug;
	    defaultFilters['$or']["links.projects."+costum.contextId+".type"] = costum.contextType;
	    defaultFilters['$or']["links.memberOf."+costum.contextId+".type"] = costum.contextType;
		if (!notNull(appConfig))
			appConfig = costum.appConfig.pages[`#${pageApp}`];
	}

	var paramsFilter= {
		container : "#filters-nav",
	 	interface : {
	 		events : {
	 			scroll : true,
	 			scrollOne : true
	 		}
	 	},
	 	defaults:{
		    notSourceKey:true,
            fieldShow : [
                "name","address","tags", "shortDescription","description","role","collection","type","created"
            ]
        },
	 	loadEvent:{
	 		default:"scroll"
	 	},
		header : {
			options : {
				left : {
					classes : 'col-xs-8 elipsis no-padding',
					group:{
						count : true,
						types : true
					}
				},
				right : {
					classes : 'col-xs-4 text-right no-padding',
					group : {
						switchView : true,
						map : true
					}
				}
			}
		},
	 	results : {
	 		map : {
	 			show : false
	 		}
	 	},
	 	filters : {
	 		/*"top": {
	 			container : "#filters-nav",
			 	lists: {*/
			 		text : true,
			 		scope : true,
			 		themes : {
			 			lists : filliaireCategories
			 		},
			 		types : {
			 			lists : ["NGO", "LocalBusiness", "Group", "GovernmentOrganization", "Cooperative" ,"projects", "citoyens", "poi"]
			 		},
					odd : {
						lists : oddList	
					},
			 	//}
		 	//}
	 	}
	};
	if(userConnected && typeof userConnected.roles != "undefined" && typeof userConnected.roles.superAdmin != "undefined" && userConnected.roles.superAdmin){
		
		paramsFilter.filters.profilImageUrl = {
			view : "dropdownList",
			type : "filters",
			field : "profilImageUrl",
			name : "Photo de profil",
			multiple : true,
			event : "exists",
			keyValue : false,
			list :{
				"true" : "Avec photo de profil",
				"false" : "Sans photo de profil"
			}
	 	}
		paramsFilter.filters.address = {
			view : "dropdownList",
			type : "filters",
			field : "address",
			name : "Adresse",
			event : "exists",
			keyValue : false,
			list :{
				"true" : "Avec adresse",
				"false" : "Sans adresse"
			}
	 	}
		paramsFilter.filters.links = {
			view : "dropdownList",
			type : "filters",
			field : "links",
			name : "Links",
			event : "exists",
			keyValue : false,
			list :{
				"true" : "Avec links",
				"false" : "Sans links"
			}
	 	}
		paramsFilter.filters.doublon = {
			view : "dropdownList",
			type : "doublon",
			field : "doublon",
			name : "Doublons",
			event : "doublon",
			keyValue : false,
			list :{
				"true" : "Doublons"
			}
	 	}
		paramsFilter.filters.tobeactivated = {
			view : "dropdownList",
			type : "filters",
			field : "roles.tobeactivated",
			name : "Compte non validé",
			event : "filters",
			keyValue : false,
			list :{
				"true" : "Compte non validé"
			}
	 	}
	}
	if(typeof appConfig != "undefined" && appConfig != null && typeof appConfig.useFilters != "undefined" && !appConfig.useFilters)
		paramsFilter.filters={};
	if(notNull(appConfig) && typeof appConfig.loadEvent != "undefined" && notNull(appConfig.loadEvent)){
		paramsFilter.loadEvent=appConfig.loadEvent;
	}

	/* Initialise les parametres des filtres */
	mylog.log("appconfig", appConfig);
	if(notNull(appConfig) &&  typeof appConfig.searchObject != "undefined"){
		if(typeof appConfig.searchObject.indexStep != "undefined")
			paramsFilter.defaults.indexStep=appConfig.searchObject.indexStep;
		if(typeof appConfig.searchObject.sortBy != "undefined")
			paramsFilter.defaults.sortBy=appConfig.searchObject.sortBy;
		if(typeof appConfig.searchObject.filters != "undefined"){
			paramsFilter.defaults.filters=appConfig.searchObject.filters;
			if(typeof appConfig.searchObject.filters.types != "undefined"){
				paramsFilter.defaults.types=appConfig.searchObject.types;
				if(typeof paramsFilter.filters.types != "undefined")
					paramsFilter.filters.types.lists=appConfig.searchObject.types;
			}
		}
		if(typeof appConfig.searchObject.sourceKey != "undefined")
			paramsFilter.defaults.sourceKey=appConfig.searchObject.sourceKey;
		
		/*if(typeof appConfig.searchObject.notSourceKey == "undefined")
			paramsFilter.defaults.notSourceKey=appConfig.searchObject.notSourceKey;*/

	}
	if(notNull(appConfig) && typeof appConfig.sourceKey=="undefined" && typeof costum !="undefined"){
		paramsFilter.defaults.notSourceKey = true
	}else if(notNull(appConfig) && appConfig.sourceKey==true && typeof costum != "undefined"){
		paramsFilter.defaults.sourceKey = costum.contextSlug
	}else if(notNull(appConfig) && typeof appConfig.sourceKey!="undefined"){
		paramsFilter.defaults.sourceKey = appConfig.sourceKey
	}
	if(notNull(appConfig) && typeof appConfig.extendFilters != "undefined"){
		$.extend(paramsFilter.filters, appConfig.extendFilters);
	}
	if(notNull(appConfig) && typeof appConfig.filters != "undefined"){
		if(typeof appConfig.filters.types != "undefined"){
			paramsFilter.defaults.types=appConfig.filters.types;
			if(typeof paramsFilter.filters.types != "undefined")
				paramsFilter.filters.types.lists=appConfig.filters.types;
		}
		if(typeof appConfig.filters.type != "undefined")
			paramsFilter.defaults.type=appConfig.filters.type;
		if(typeof appConfig.filters.category != "undefined")
			paramsFilter.defaults.category=appConfig.filters.category;
		if(typeof appConfig.filters.tags != "undefined")
			paramsFilter.defaults.tags=appConfig.filters.tags;
		if(typeof appConfig.filters.forced != "undefined")
			paramsFilter.defaults.forced=appConfig.filters.forced;
	}


	if(notNull(costum) && typeof costum.filters != "undefined" && typeof costum.filters.sourceKey != "undefined"){
		//paramsFilter.defaults.sourceKey=costum.slug;
	}

	if(notNull(appConfig) &&  typeof appConfig.results != "undefined")
		paramsFilter.results=appConfig.results;
	if(notNull(costum)){
		paramsFilter.defaults["filters"] = defaultFilters;
	}
	
	var filterSearch={};
		
	jQuery(document).ready(function(){
		mylog.log('filterSearch paramsFilter', paramsFilter);
		filterSearch = searchObj.init(paramsFilter);
	});
</script>