<?php
	$costum = Costum::getCostumCache();
	$orgaAnnuary = PHDB::find(Organization::COLLECTION, array( "source.key" => $costum["slug"])) ?? [];
	$allTags = [];
	$newAllTags = [];
	foreach ($orgaAnnuary as $array) { 
		$allTags = $allTags + $array["tags"];
	}
	$allTags = array_unique($allTags);
	foreach ($allTags as $value) {
		$newAllTags[$value] = $value;
	}
?>

<style>
	.card-xtrem-group {
		display: inline-block;
		justify-content: center;
		margin: 20px;
		/* margin-top: 10%; */
	}
	.card-xtrem-group .card {
		background: #fff;
		width: 330px;
		height: 420px;
		display: flex;
		justify-content: center;
		flex-direction: column;
		/* box-shadow: 1px 1px 1px 0 rgba(0, 0, 0, 0.4); */
		box-shadow: rgba(99, 99, 99, 0.2) 0px 2px 8px 0px;
		position: relative;
		cursor: pointer;
		border-radius: 15px;
	}

	.card-xtrem-group .card:hover .card-xtrem-badge-outside {
		background: #fff;
	}
	.card-xtrem-group .card .card-xtrem-badge {
		width: 4em;
		height: 4em;
		background: #fff;
		border-radius: 50%;
		position: absolute;
		transition: 0.3s ease-in;
		left: -18px;
		top: -22px;
		background-size: contain;
		display: flex;
		justify-content: center;
		align-items: center;
	}
	/* .card-xtrem-group .card .card-xtrem-badge-outside {
	width: 4.5em;
	height: 4.5em;
	background: #EF8275;
	border-radius: 50%;
	position: absolute;
	transition: 0.3s ease-in;
	left: 38.8%;
	top: 44.2%;
	} */
	.card-xtrem-group .card div {
		transition: transform 0.35s ease-in-out;
	}
	.card-xtrem-group .card .card-xtrem-image {
		background-size: cover;
		height: 100%;
		width: 100%;
	}
	.card-xtrem-group .card .card-xtrem-desc {
		height: 100%;
		margin: 0.5em;
	}
	.card-xtrem-group .card .card-xtrem-desc h1 {
		padding: 0 1em;
		font-size: 1.7rem;
		z-index: 2;
		text-align: center;
	}
	.card-xtrem-group .card .card-xtrem-desc p {
		font-weight: 100;
		font-size: 0.85rem;
		text-align: center;
	}
	/* .card-xtrem-group .card .card-xtrem-desc a {
	font-size: 1rem;
	color: #0f82ff;
	} */
	.badge-xtrem-icon {
		color: white;
		font-size: 27px;
	}
	.xtremElementTag {
		cursor: pointer;
		position: relative;
		margin: 3px 3px 3px 0;
		display: inline-block;
		background: #658097;
		color: white;
		padding: 5px 10px;
		border-radius: 19px;
		font-size: 15px;
	}
</style>

<div class=" col-md-12">
<div class='headerSearchIncommunity no-padding col-xs-12' style="margin-top: 90px;"></div>
<div id='filterContainer' class='searchObjCSS'></div>
	<div class='bodySearchContainer'>
		<div class="content"></div>
		<div class='no-padding col-xs-12' id='dropdown_search'>
		</div>
		<div class='no-padding col-xs-12 text-left footerSearchContainer smartgrid-slide-element'></div>
	</div>
</div>

<script>
	directory.panelXtrem =  function(params){
		var str = '';
		mylog.log("sadkhcgdsk", params)
		str += `
		<div class="card-xtrem-group">
			<div class="card">
			<div class="card-xtrem-badge-outside"></div>
			<div  class="card-xtrem-badge bg-${params.color}">
				<i class="fa fa-${params.icon} badge-xtrem-icon"></i>
			</div>
			<div class="card-xtrem-image">
				<a href="${params.hash}" class="container-img-profil card-xtrm-image ${params.hashClass}">${params.imageProfilHtml}</a>
			</div>
			<div class="card-xtrem-desc">
				<h1>${params.name}<h1>
				<p>${(params.shortDescription == null ) ? '' : params.shortDescription }</p>`;
				if (typeof params.tags !== "undefined") {
					$.each(params.tags, function (e, v) {
						str += `
														<span class="xtremElementTag" onclick="modalShowAll.actions.filterTag('${v}')">
															 ${v}
														</span>
													`;
					});
				}
			str += `	${(typeof params.id != 'undefined' && typeof params.collection != 'undefined') ? directory.socialToolsHtml(params) : ""}
			</div>
			</div>
		</div>
		`;
		return str;
	}

    let directoryList = {
        xtremOrgaList: function () {
				var paramsFIlter = {
					urlData: baseUrl + "/co2/search/globalautocomplete",
					container: "#filterContainer",
					header: {
						dom: ".headerSearchIncommunity",
						options: {
							left: {
								classes: 'col-xs-8 elipsis no-padding',
								group: {
									count: true,
									types: true
								}
							}
						},
					},
					defaults: {
						notSourceKey: true,
						types : ["organizations"],
						filters: {
							"source.key": contextSlug
						}
					},
					results: {
						dom: ".content",
						// smartGrid: true,
						renderView: "directory.panelXtrem",
						map: {
							active: false
						}
					},
					filters : {
						text : true,
						types : {
							lists : ["NGO", "LocalBusiness", "Group", "GovernmentOrganization", "Cooperative"]
						},
						badges : {
							view : "dropdownList",
				            type : "filters",
				            action : "filters",
				            typeList : "object",							
							name : "Badge", 
				            event : "inArray",
							value : true,
							field : "tags",
							list : <?= json_encode($newAllTags) ?>
						},
						role : {
							view : "dropdownList",
				            type : "filters",
				            action : "filters",
				            typeList : "object",							
							name : "Rôle", 
				            event : "filters",
							field : "role",
							value : true,
							list : {
								"admin" : {
                                    label:  trad.administrator,
                                    value : "admin",
                                },
								"member" : {
                                    label:  trad.Member,
                                    value : "member",
                                },
								"creator" : {
                                    label:  trad.justCitizen,
                                    value : "creator",
                                },
							}
							
						}

					}
				
				}
			
				let filterXtrem = searchObj.init(paramsFIlter);
				filterXtrem.search.init(filterXtrem);
			}
    }
    directoryList.xtremOrgaList();
	$(".footerSearchContainer").hide();
</script>