<?php 

    // $cssAnsScriptFilesTheme = array(
    //     '/plugins/showdown/showdown.min.js',
    // );
    // HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesTheme, Yii::app()->request->baseUrl);
    
?>
<style type="text/css">
    .social-main-container{
        padding-top: 20px !important;
    }
    #home .timeline > li.timeline-inverted > .timeline-badge{
        display: none;
    }
    #home .timeline > li > .timeline-panel{
        width: 100%
    }
    #home .timeline > li.timeline-inverted > .timeline-panel:before, #home .timeline > li.timeline-inverted > .timeline-panel:after{
        border-right-width:0px;
    }
    #home .timeline::before{
        width: 0px;
    }
    .menu-left-home, #notif-column{
        position: fixed;
    }
    #notif-column{
        right: 0;
    }

    .menu-left-home{
        left: 52px;
    }
    .content-home-user-info .link-user-menu{
        text-decoration: none;
    }
    .content-home-user-info .img-profil{
        border-radius:10px;
    }
    .content-home-user-info .username{
        font-size: 20px;
        vertical-align: bottom;
        text-decoration: underline;
    }
    .content-home-user-info .username:hover{
        color:#4285f4!important;
    }
    .content-home-user-info .label-gammification{
        text-transform: uppercase; 
    }
    .content-home-user-info .label-gammification, .content-home-user-info .points-gammification{
        vertical-align: sub;
    }
    #btnGamificationInfos:hover{
        cursor: pointer;
    }
    .contribute-panel .title-rubrique{
        font-size: 16px;
        color:#777;
        padding-top: 5px;
        padding-bottom: 5px;
        margin-bottom: 5px !important;
        float: left;
    }
    /*.contribute-panel .btn-open-form{
        margin-bottom: 3px;
    }*/
    .contribute-panel .btn-open-form i{
        font-size: 15px;
        vertical-align: baseline;
        width: 25px;
        height: 25px;
        text-align: center;
        padding: 5px;
        border-radius: 100%;
        color: white !important;

    }
    .breadcrumb-gammification .content-level{
        display: inline-block;
        width: 60px;
        text-align: center;
        margin:0px 15px; 
        position: relative;
    }
    .breadcrumb-gammification .content-level.arrow{
        top: -25px;
    }
    .breadcrumb-gammification .content-level.arrow .fa{
        font-size: 35px;
    }
    .breadcrumb-gammification .content-level .fa-question-circle{
        font-size: 55px;
        width: 60px;
    }
    .breadcrumb-gammification .label-level-game{
        font-variant: small-caps;
    }
    .breadcrumb-gammification .mylevel{
        font-variant: small-caps;
        font-weight: bolder;
    }
    #home-floop.floopDrawer{
        display: block;
        width: 28%;
        box-shadow: none;
        border: 1px solid #ccc; 
        border-top: none;
    }
    #home-floop.floopDrawer .floopHeader{
        box-shadow: none;
    }
    .button-on-air{
        line-height: 18px;
        text-align: left;
        max-width: 100%;
        margin-bottom: 5px;
    }
    .button-on-air input{
        margin-top: 3px;
    }
    .button-on-air label{
        margin-bottom: 0px;
    }
    .button-flux-news{
        display: none;
    }
    .show-flux-news{
        cursor: pointer;
    }
    .show-flux-news i.badge.showIcon{
        display: inline-block;
        width: 25px;
        /* float: left; */
        font-weight: bolder;
        background-color: white;
        color: #2c3e50;
        border: 2px solid #2c3e50;
        padding: 2px 0px 2px 0px;
        height: 25px;
        margin-right: 5px;

    }
    .addFluxNews{
        font-size: 13px;
        border-radius: 5px;
        padding: 3px 8px;
    }
    @media (max-width: 767px){
       
    }
</style>
<div class="col-md-12 col-sm-12 col-xs-12 no-padding social-main-container">
	<div class="" id="onepage">
		<div class="hidden-xs hidden-sm hidden-md col-lg-2 menu-left-home content-home-user-info">
            <?php 
                $userThumb=Element::getElementSimpleById( Yii::app()->session["userId"], Person::COLLECTION, null, array("profilThumbImageUrl"=>1));
                $profilThumbImageUrl = $this->module->getParentAssetsUrl().'/images/thumb/default_citoyens.png';
                if(@$userThumb["profilThumbImageUrl"])
                    $profilThumbImageUrl=Yii::app()->getRequest()->getBaseUrl(true).$userThumb["profilThumbImageUrl"];
                $pointGammif=Gamification::calcPoints( Person::COLLECTION, Yii::app()->session["userId"] );
                $labelBadgeGammif=Gamification::badge(  Yii::app()->session["userId"], $pointGammif );
                $iconBadgeGammif=Gamification::badge(  Yii::app()->session["userId"], $pointGammif, $this->module->getParentAssetsUrl() );
            ?>     
            <a href="#page.type.<?php echo Person::COLLECTION ?>.id.<?php echo Yii::app()->session["userId"] ?>" class="link-user-menu lbh text-dark no-margin margin-bottom elipsis" data-toggle="dropdown">
                <img class="img-profil" id="" width="25" height="25" src="<?php echo $profilThumbImageUrl ?>" alt="image">            
                <span class="username"><?php echo "@".Yii::app()->session["user"]["username"] ?></span> 
            </a>
            <div id="btnGamificationInfos" class="col-xs-12 valueAbout no-padding margin-bottom-10 btnGamificationInfos margin-top-10">
                <span style="line-height: 25px;"> 
                    <img class="" width="25" height="25" src="<?php echo $iconBadgeGammif ?>" alt="image">
                    <span class="label-gammification"><?php echo $labelBadgeGammif ?></span>
                    <span class="points-gammification">&middot; <span class="letter-red"><?php echo $pointGammif."</span>"." ".Yii::t("common","points"); ?></span>
                </span>  
            </div>
            <div class="col-xs-12 no-padding channel-large-view">
                <h4 class="text-dark bold"><i class="fa fa-rss"></i> <?= Yii::t("home", "My news feeds") ?></h4>
                <div class="order-channel">
                <?php foreach($live as $k => $v){ ?>
                    <div class="col-xs-12 button-on-air btn bg-white radius-20 shadow2 elipsis" data-sort="<?php echo $v["order"] ?>">
                      <input type="radio" class="margin-right-5 fluxNews-<?php echo $k ?>" name="flux-news-full" data-url="<?php echo $v["url"] ?>" value="<?php echo $k ?>"
                            <?php echo (!empty($v["active"])) ? "checked" : ""; ?>>
                      <label for="<?php echo $k ?>"><?php echo $v["name"] ?></label>
                    </div>
               <?php  } ?>
               </div>
                <a href="javascript:;" class="addFluxNews letter-blue"><i class="fa fa-plus"></i> <?= Yii::t("home", "Add feeds") ?></button>
            </div>
            <!--<div class="contribute-panel col-xs-12 no-padding margin-top-20">
                <span class="title-rubrique"><?php echo Yii::t("common","Create your network")?></span>
                <a href="#element.invite" class="lbhp btn-open-form col-xs-12 no-padding margin-bottom-10"><i class="fa fa-user-plus bg-yellow"></i> <?php echo Yii::t("common", "Invite your friend") ?></a>
                <a href="javascript:;" data-form-type="organization" class="btn-open-form col-xs-12 no-padding margin-bottom-10"><i class="fa fa-users bg-green"></i> <?php echo Yii::t("common", "Add an organization") ?></a>
                <a href="javascript:;" data-form-type="project" class="btn-open-form col-xs-12 no-padding"><i class="fa fa-lightbulb-o bg-purple"></i> <?php echo Yii::t("common", "Reference a project") ?></a>
            </div>
             <div class="contribute-panel col-xs-12 no-padding margin-top-20">
                <span class="title-rubrique"><?php echo Yii::t("common", "Useful links") ?></span>
                <a href="https://doc.co.tools/books/2---utiliser-loutil" target="_blank" class="col-xs-12 no-padding">#<?php echo Yii::t("common","User guide") ?></a><br/>
                <a href="#page.type.<?php echo Person::COLLECTION ?>.id.<?php echo Yii::app()->session["userId"] ?>.view.settings" class="lbh margin-top-10 col-xs-12 no-padding">#<?php echo Yii::t("common", "Settings") ?></a><br/>
                <a href="mailto:<?php echo Yii::app()->params["contactEmail"] ?>" class="margin-top-10 col-xs-12 no-padding">#<?php echo Yii::t("common", "Contact us") ?></a><br/>
            </div>-->

            <!--<div class="contribute-panel">
            <a href="javascript:;" class="margin-top-10 col-xs-12 no-padding">Concours des assos</a><br/>-->
        </div>
        <div class="col-xs-12 col-sm-9 col-md-7 col-lg-7 col-md-offset-1 col-lg-offset-2" id="home">
            <div class="content-home-user-info hidden-lg col-xs-12 no-padding margin-bottom-10">
                <a href="#page.type.<?php echo Person::COLLECTION ?>.id.<?php echo Yii::app()->session["userId"] ?>" class="link-user-menu lbh text-dark no-margin margin-bottom" data-toggle="dropdown">
                    <img class="img-profil" id="" width="35" height="35" src="<?php echo $profilThumbImageUrl ?>" alt="image">            
                    <span class="username" style="font-size:25px;"><?php echo "@".Yii::app()->session["user"]["username"] ?></span> 
                </a>
                <strong class="margin-right-5 margin-left-5" style="font-size: 20px;vertical-align: middle;text-align: center;width: 20px;display: inline-block;"> • </strong>
                <div id="btnGamificationInfos" class="valueAbout no-padding margin-bottom-10 margin-top-10 btnGamificationInfos" style="display: initial;">
                    <span style="line-height: 25px;"> 
                        <img class="" width="25" height="25" src="<?php echo $iconBadgeGammif ?>" alt="image">
                        <span class="label-gammification"><?php echo $labelBadgeGammif ?></span>
                        <span class="points-gammification">&middot; <span class="letter-red"><?php echo $pointGammif."</span>"." ".Yii::t("common","points"); ?></span>
                    </span>  
                </div>
            </div>
             <div class="col-xs-12 no-padding hidden-lg margin-bottom-15 channel-small-view">
                <h4 class="text-dark bold show-flux-news cursor text-left">
                    <i class="badge showIcon fa fa-angle-down"></i>
                    <i class="fa fa-rss"></i> <?= Yii::t("home", "My news feeds") ?>
                    <span class="badge bold bg-dark"><?php echo count($live); ?></span>
                    <a href="javascript:;" class="addFluxNews bg-green btn text-white tooltips pull-right" data-toggle="tooltip" data-placement="left" 
                        title="<?php echo Yii::t("common","Manage your news streams"); ?>"><i class="fa fa-plus" ></i></a>
                </h4>
                <div class="order-channel">
                <?php foreach($live as $k => $v){ ?>
                    <div class="col-xs-12 button-on-air btn bg-white radius-20 shadow2 elipsis button-flux-news" data-sort="<?php echo $v["order"] ?>">
                      <input type="radio" class="margin-right-5 fluxNews-<?php echo $k ?>" name="flux-news-sm" data-url="<?php echo $v["url"] ?>" value="<?php echo $k ?>"
                      <?php echo (!empty($v["active"])) ? "checked" : ""; ?>>
                      <label for="<?php echo $k ?>"><?php echo $v["name"] ?></label>
                    </div>
                <?php  } ?>
                </div>
            </div>
            <div class="col-xs-12 no-padding">
            </div>
            <div class="col-xs-12 col-lg-10 col-lg-offset-1 no-padding" id="central-container">
            </div>
        </div>
        <div class="col-md-3 col-lg-3 col-sm-3 hidden-xs" id="notif-column">
            <!-- <div class="content-home-user-info visible-sm">
                <a href="#page.type.<?php echo Person::COLLECTION ?>.id.<?php echo Yii::app()->session["userId"] ?>" class="link-user-menu lbh text-dark no-margin margin-bottom elipsis" data-toggle="dropdown">
                    <img class="img-profil" id="" width="25" height="25" src="<?php echo $profilThumbImageUrl ?>" alt="image">            
                    <span class="username"><?php echo "@".Yii::app()->session["user"]["username"] ?></span> 
                </a>
                <div id="btnGamificationInfos" class="col-xs-12 valueAbout no-padding margin-bottom-10 btnGamificationInfos margin-top-10">
                    <span style="line-height: 25px;"> 
                        <img class="" width="25" height="25" src="<?php echo $iconBadgeGammif ?>" alt="image">
                        <span class="label-gammification"><?php echo $labelBadgeGammif ?></span>
                        <span class="points-gammification">&middot; <span class="letter-red"><?php echo $pointGammif."</span>"." ".Yii::t("common","points"); ?></span>
                    </span>  
                </div>
            </div>-->
            
            <div id="home-floop" class="floopDrawer"></div>
            <!-- WHAT WHAT NNN?? -->
            <!--<div id="territorial-notif-column">
                <?php if(@$element["custom"] && @$element["custom"]["pubTpl"])
                    echo $this->renderPartial($element["custom"]["pubTpl"]); ?>
            </div>-->
        </div>
	</div>
</div>
<div class="modal fade" role="dialog" id="modalExplainGamification" style="background: rgba(0,0,0,0.4);" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-green-k text-white">
                <h4 class="modal-title"><!--<i class="fa fa-info-circle"></i>--> <?php echo Yii::t("docs"," Gamification (ou ludification) Citoyenne") ?></h4>
            </div>
            <div class="modal-body center text-dark"> 
                <span class="text-short-explain">
                    <?php echo Yii::t("docs", "Win points <span class='letter-red'>using {website}</span>.<br/><span class='letter-red'>Each link</span> you did with people, organizations, projects, events <span class='letter-red'>improve your score</span>.<br/> Later will be added <span class='letter-red'>exchanges of ressources</span>, <span class='letter-red'>the number of posts</span> and <span class='letter-red'>your activity on public policy</span>", array("{website}"=> ucfirst(Yii::app()->name))) ?><br/>
                </span>
                <div class="content-user-game-info margin-top-20">
                    <?php $s=($pointGammif>1) ? "s" : ""; $labelTodayScore=Yii::t("docs", "You have today <span class='letter-red'>{points} point".$s."</span>", array("{points}"=>$pointGammif)) ?>
                    <span class="bold"><?php echo $labelTodayScore ?></span><br/><br/>
                    <div class="breadcrumb-gammification">
                        <?php $level=Gamification::getAllLevelAndCurent($pointGammif, Yii::app()->session["userId"], $this->module->getParentAssetsUrl());
                          //  print_r($level);
                            foreach($level as $key => $v){ ?>
                                <div class="content-level <?php if(@$v["current"]) echo "current" ?>">
                                    <?php if(@$v["current"]){
                                        echo "<span class='mylevel letter-red'>".Yii::t("common", "You are")."</span>";
                                        $nextStep=$v["nextStep"];
                                    } ?>
                                    <span class="label-level-game"><?php echo @$v["label"]; ?></span>
                                    <img src="<?php echo @$v["icon"]; ?>"/>
                                    <span class="nb-points"><?php echo @$v["points"]; ?> pt<?php if($v["points"] > 1) echo "s"; ?></span>
                                </div>
                            <?php }
                            if($nextStep !== false){ ?>
                                <div class="content-level arrow">
                                    <i class="fa fa-2x fa-arrow-right"/>
                                </div>
                                <div class="content-level">
                                <span class="label-level-game"><?php echo Yii::t("common", "Next to"); ?></span>
                                    <i class="fa fa-2x fa-question-circle"/>
                                    <span class="nb-points"><?php echo @$nextStep; ?> pts</span>
                                </div>
                           <?php }
                        ?>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" >

var type = "<?php echo Person::COLLECTION; ?>";
var id = "<?php echo Yii::app()->session["userId"]; ?>";
var view = "<?php echo @$view; ?>";
var indexStepGS = 20;
var dateLimit = 0;
var isLiveBool=true;
var fluxNews=<?php echo json_encode($live) ?>;
jQuery(document).ready(function() {
    $('.channel-small-view .order-channel').sortDivs();
    $('.channel-large-view .order-channel').sortDivs();
    
    /*$('.channel-small-view .button-on-air').sortDivs();
    $('.channel-large-view .button-on-air').sort(function (a, b) {
          var contentA =parseInt( $(a).attr('data-sort'));
          var contentB =parseInt( $(b).attr('data-sort'));
          return (contentA < contentB) ? -1 : (contentA > contentB) ? 1 : 0;
   })*/
    $(".btnGamificationInfos").off().on("click",function(){
       // $("#modalExplainGamification").modal("show"); 
       urlCtrl.modalExplain({
            icon: "info-circle",
            title : $("#modalExplainGamification .modal-title").text(),
            html : $("#modalExplainGamification .modal-body").html(),
           
        });
    });
    $(".addFluxNews").off().on("click",function(){
        urlCtrl.modalExplain({
            icon: "rss",
            title : trad.manageNewsStream,
            text : trad.explainFavNewsStream,
            link : "#live",
            targetB:false,
            linkClass : "lbh",
            linkLabel : trad.goToLive
        });
    });

    floopDrawer.init({
        dom : '#home-floop',
        options :{    
            status : true, 
            answer : true, 
            actions : {
                chat : true,
                fluxNews:true,
                disconnect : true,
                costum: true
            }
        }
        
    });
    
	//initPageInterface();
    newsObj.loadLive("/news/co/index?type=citoyens&id="+userId+"&isLive=true&nbCol=1&inline=true");
    $(".show-flux-news").click(function(){
        if($(".button-flux-news").is(":visible")){
            $(".button-flux-news").fadeOut(500);
            $(this).find("i.fa-angle-up").removeClass("fa-angle-up").addClass("fa-angle-down");
        }else{
            $(".button-flux-news").fadeIn(500);
            $(this).find("i.fa-angle-down").removeClass("fa-angle-down").addClass("fa-angle-up");
        }
    });
    $(".button-on-air").off().on("click", function(){
        if($(this).hasClass("button-flux-news")){
            $(".button-flux-news").fadeOut(500);
            $(".show-flux-news").find("i.fa-angle-up").removeClass("fa-angle-up").addClass("fa-angle-down");
        }
        $radio=$(this).find("input[type='radio']");
        $radioValue=$radio.val();
        $(".fluxNews-"+$radioValue).attr("checked", true);
        newsObj.loadLive($radio.data("url"), $radio.val(), fluxNews);       
    });
});
function getSorted(selector, attrName) {
    return $($(selector).toArray().sort(function(a, b){
        var aVal = parseInt(a.getAttribute(attrName)),
            bVal = parseInt(b.getAttribute(attrName));
        return aVal - bVal;
    }));
}

var colNotifOpen = true;
/*function loadLiveNow () {
    var level = {} ;
    if( notNull(userConnected.address)) {
        mylog.log("loadLiveNow2", userConnected.address);
        if(notNull(userConnected.address.level4)){
            mylog.log("loadLiveNow3", userConnected.address.level4);
            level[userConnected.address.level4] = {id : userConnected.address.level4, type : "level4", name : userConnected.address.level4Name } ;
        } else if(notNull(userConnected.address.level3)){
            level[userConnected.address.level3] = {id : userConnected.address.level3, type : "level3", name : userConnected.address.level3Name } ;
        } else if(notNull(userConnected.address.level2)){
            level[userConnected.address.level2] = {id : userConnected.address.level2, type : "level2", name : userConnected.address.level2Name } ;
        } else if(notNull(userConnected.address.level1)){
            level[userConnected.address.level1] = {id : userConnected.address.level1, type : "level1", name : userConnected.address.level1Name } ;
        }
    }
    mylog.log("loadLiveNow4", level);
    if( jQuery.isEmptyObject(level) ) {
        //alert("Vous n'êtes pas communecté ?");
    } //else{
   /* var searchParams = {
      "tpl":"/pod/nowList",
      "searchLocality" : level,
      "indexMin" : 0, 
      "indexMax" : 30 
    };

    ajaxPost( "#territorial-notif-column", baseUrl+'/'+moduleId+'/element/getdatadetail/type/citoyens/id/'+userId+'/dataName/liveNow?tpl=nowList',
                    searchParams, function() { 
                    coInterface.bindLBHLinks();
     } );*/
    //}
//}


//function toogleNotif(open){
  //  if(typeof open == "undefined") open = false;
    
   // if(open==false){
        //$('#notif-column').removeClass("col-md-3 col-sm-3 col-lg-3").addClass("hidden");
        //$('#central-container').removeClass("col-md-9 col-lg-9").addClass("col-md-12 col-lg-12");
    //}else{
        //$('#notif-column').addClass("col-md-3 col-sm-3 col-lg-3").removeClass("hidden");
        //$('#central-container').addClass("col-sm-12 col-md-9 col-lg-9").removeClass("col-md-12 col-lg-12");
    //}

    //colNotifOpen = open;
//}
//function initPageInterface(){
  //  $(".btnGamificationInfos").off().on("click",function(){
    //    $("#modalExplainGamification").modal("show"); 
    //});
	/*$("#second-search-bar").addClass("input-global-search");

    $("#main-btn-start-search, .menu-btn-start-search").click(function(){
        startGlobalSearch(0, indexStepGS);
    });

    $("#second-search-bar").keyup(function(e){ 
        $("#input-search-map").val($("#second-search-bar").val());
        $("#second-search-xs-bar").val($("#second-search-bar").val());
        if(e.keyCode == 13){
            searchObject.text=$(this).val();
            myScopes.type="open";
            myScopes.open={};
            //urlCtrl.loadByHash("#search");
            startGlobalSearch(0, indexStepGS);
         }
    });
    $("#second-search-xs-bar").keyup(function(e){ 
        $("#input-search-map").val($("#second-search-xs-bar").val());
        $("#second-search-bar").val($("#second-search-xs-bar").val());
        if(e.keyCode == 13){
            searchObject.text=$(this).val();
            myScopes.type="open";
            myScopes.open={};
            //urlCtrl.loadByHash("#search");
            startGlobalSearch(0, indexStepGS);
         }
    });
     $("#second-search-bar-addon, #second-search-xs-bar-addon").off().on("click", function(){
        $("#input-search-map").val($("#second-search-bar").val());
        searchObject.text=$("#second-search-bar").val();
        myScopes.type="open";
        myScopes.open={};
            //urlCtrl.loadByHash("#search");
            startGlobalSearch(0, indexStepGS);
    });
    
    $("#input-search-map").keyup(function(e){ 
        $("#second-search-bar").val($("#input-search-map").val());
        if(e.keyCode == 13){
            startGlobalSearch(0, indexStepGS);
         }
    });

    $("#menu-map-btn-start-search").click(function(){
        $("#second-search-bar").val($("#input-search-map").val());
        startGlobalSearch(0, indexStepGS);
    });

    $(".social-main-container").mouseenter(function(){
    	$(".dropdown-result-global-search").hide();
    });

    $(".tooltips").tooltip();
   
    $('.sub-menu-social').affix({
      offset: {
          top: 320
      }
    });*/
    //$(".dropdown-result-global-search").hide();
    

//}

</script>