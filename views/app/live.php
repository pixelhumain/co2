<?php 
    $page = "live";
?>

<style>
     
 #noMoreNews {
        position: relative;
        padding: 0px 40px;
        bottom: 0px;
        width: 100%;
        text-align: center;
        background: white;
    }
    #newsstream .loader,
    #noMoreNews{
       border-radius: 50px;
        margin-left: auto;
        margin-right: auto;
        display: table;
        padding: 15px;
        margin-top: 15px;
    }
</style>
<div class="row bg-white live-container">
<?php 
    //$this->renderPartial($layoutPath.'headers/pod/'.$CO2DomainName.'/dayQuestion', array());
?>

    <div class="col-md-12 col-sm-12 col-xs-12 bg-white top-page" id="" style="padding-top:0px!important;">
    	<div class="col-lg-1 col-md-1 hidden-sm hidden-xs text-right hidden-xs" id="sub-menu-left"></div>

    	<div class="col-lg-10 col-md-12 col-sm-12 col-xs-12 margin-top-10">
    		<div id="newsstream"></div>
    	</div>	
    </div>
</div>


<script type="text/javascript" >
//searchObject.initType="news";
var titlePage = "<?php echo @$this->appConfig["pages"]["#live"]["subdomainName"]; ?>";
var liveParams =<?php echo json_encode(@$this->appConfig["pages"]["#live"]); ?>;
var pageApp = "live"; 
directory.appKeyParam=(location.hash.indexOf("?") >= 0) ? location.hash.split("?")[0] : location.hash;
jQuery(document).ready(function() {
	$(".subsub").hide();
	/*$('#btn-start-search').click(function(e){
		startNewsSearch(true);
    });

   searchInterface.initSearchParams();*/
	startNewsSearch(true);


});
function startNewsSearch(isFirst){
    var urlLive = "/news/co/index/type/city/isLive/true";
    var dataSearchLive={search:true};
    if(typeof liveParams != "undefined" && notNull(liveParams)){
       if(typeof liveParams.slug != "undefined" && notNull(costum)){
            urlLive = "/news/co/index/type/"+costum.contextType+"/id/"+costum.contextId;
            dataSearchLive=null; 
       }else if(typeof contextData!="undefined" && notNull(contextData)){
            urlLive = "/news/co/index/type/"+contextData.collection+"/id/"+contextData._id.$id;
            dataSearchLive=null;
       }else if(typeof costum!="undefined" && notNull(costum)){
            urlLive = "/news/co/index/type/"+costum.contextType+"/id/"+costum.contextId;
            dataSearchLive=null;
       }
       
       if(typeof liveParams.filters!="undefined" && typeof liveParams.filters.types!="undefined"){
            dataSearchLive = {searchTypes : liveParams.filters.types};
       }

       if(typeof liveParams.formCreate != "undefined")
            urlLive += "/formCreate/false";
    
        if(typeof liveParams.setParams != "undefined"){
            members = (typeof liveParams.setParams.members != "undefined") ? liveParams.setParams.members : false;

            myScopes.type="open";
            myScopes.open=[];
            if(typeof liveParams.setParams.source != "undefined") 
                urlLive += "/source/"+liveParams.setParams.source;
            else
                urlLive += "/source/"+costum.contextSlug;
        }
    }
    coInterface.showLoader("#newsstream");
    coInterface.simpleScroll(0, 500);
    ajaxPost("#newsstream",baseUrl+"/"+urlLive, dataSearchLive, function(news){ 
        const arr = document.querySelectorAll('img.lzy_img')
			arr.forEach((v) => {
				imageObserver.observe(v);
			});
        spinSearchAddon();});
}

</script>