<?php
    $ASSETS_URL = Yii::app()->getModule("co2")->getAssetsUrl();
?>

<style>
    #menuLeft{
        display:none;
    }

<?php
if (isset($appMap["embeded"])){
    if(isset($appMap["embeded"]["hideHeader"]) && $appMap["embeded"]["hideHeader"]=="true"){ ?>

    #mainNav{
        display:none !important;
    }
    .main-container{
        padding-top:0px !important;
    }
<?php } ?> 
<?php if(isset($appMap["embeded"]["hideFooter"]) && $appMap["embeded"]["hideFooter"]=="true"){ ?>

    footer{
        display:none !important;
        margin-top:0px;
    }
    .app-map-main-container{
        height:calc(100vh) !important;
    }

<?php } ?>    

<?php if(isset($appMap["embeded"]["hideMenuRight"]) && $appMap["embeded"]["hideMenuRight"]=="true"){ ?>

    #menuRight{
        display:none !important;
    }

<?php } ?>  
<?php if(isset($appMap["embeded"]["hideMenuBottom"]) && $appMap["embeded"]["hideMenuBottom"]=="true"){ ?>

    #menuBottom{
        display:none !important;
    }

<?php }} ?>  

    .app-map-main-container{
        width:100%;
        height: calc(100vh - 53px);
        background:red;
        position: relative;
    }

    .app-map-side-nav{
        position: fixed;
        width: 350px;
        height: calc(100vh - 53px);
        bottom: 0px;
        background: #ffffffab;
        left: -350px;
        transition: .3s;
        z-index: 100;
        padding: 20px 10px;
    }
    .app-map-side-nav.active{
        left:0;
        overflow-y: auto;
    }

    .app-map-side-nav.active > .btn-toogle-app-map-side-nav{
        position:fixed;
    }
    .app-map-side-nav.active .fa-chevron-left{
        display:block;
    }
    .app-map-side-nav.active .fa-chevron-right{
        display:none;
    }
    .app-map-side-nav .fa-chevron-left{
        display:none;
    }
    .app-map-side-nav .fa-chevron-right{
        display:block;
    }

    .btn-toogle-app-map-side-nav{
        position: absolute;
        left: 350px;
        top:50px;
        height:70px;
        background: #9fbd38;
        color:white;
        border: none;
        width: 30px;
        text-align:center;
    }

    #app-map-container{
        width:100%;
        height:100%;
    }

    .app-map-tile-switcher{
        position: absolute;
        bottom: 20px;
        left: 370px;
        display: flex;
        margin:0;
        padding:0;
    }
    .app-map-tile-switcher li{
        list-style: none;
        width: 70px;
        height: 70px;
        margin-right: 10px;
        border-radius: 4px;
        cursor: pointer;
    }
    .app-map-tile-switcher li img{
        width:100%;
        height:100%;
        object-fit:cover;
        border-radius: 4px;
    }
    .app-map-tile-switcher li:first-child{
        border: 2px solid #9fbd38;
    }
    .app-map-tile-switcher li:not(:first-child){
        position: relative;
        left: -10px;
        visibility: hidden;
        opacity: 0;
        transition: .2s;
    }
    .app-map-tile-switcher li.active{
        left:0;
        visibility: visible;
        opacity: 1;
    }

    .app-map-controller{
        margin: 0;
        padding: 0;
        position: absolute;
        top: 50px;
        right: 20px;
        z-index: 1;
        display: flex;
        flex-direction: column;
    }
    .app-map-controller li{
        list-style: none;
        cursor: pointer;
        width: 25px;
        height: 25px;
        display: block;
        background-color: white;
        text-align: center;
        line-height: 25px;
        margin-bottom: 5px;
        border-radius: 4px;
    }

    .app-map-no-data{
        width:100%;
        height: 100%;
        background-color: white;
        display: flex;
        align-items: center;
        flex-direction: column;
        text-align: center;
        margin-top: 100px;
    }
    .app-map-no-data img{
        width: 200px;
    }
    .app-map-no-data p{
        font-size: 18px;
        color: #a0a0a0;
        margin-top: 15px;
    }

    .app-map-no-data a{
        margin-top: 30px;
        font-size: 16px;
        background: #9fbd38;
        padding: 10px 15px;
        border-radius: 4px;
        color: white;
        font-weight: bold;
    }

    .app-map-info{
        position: absolute;
        bottom: 20px;
        right: 20px;
        border-radius: 100%;
        width: 55px;
        height: 55px;
        background-color: white;
        transition: .3s;
        z-index: 1;
    }
    .app-map-info.active{
        width: 500px;
        border-radius: 4px;
        padding: 20px;
        height: fit-content;
    }
    .app-map-info .app-map-info-active{
        display: none;
    }
    .app-map-info .app-map-info-inactive{
        display: block;
    }
    .app-map-info.active .app-map-info-active{
        display: flex;
        width: 100%;
        align-items: center;
    }
    .app-map-info.active .app-map-info-inactive{
        display: none;
    }

    .app-map-info h1{
        font-size: 18px;
        margin: 0;
        display: flex;
        justify-content: space-between;
        align-items: flex-start;
    }
    .app-map-info-active h1 button{
        border: none;
        background: white;
        padding: 0;
        color: #616161;
    }
    .app-map-info p{
        margin-top: 10px;
        color: #707070;
        font-size: 16px;
    }
    .app-map-info a{
        padding: 5px 10px;
        background: #9fbd38;
        border-radius: 2px;
        margin-top: 10px;
        color: white;
        display: block;
        width: fit-content;
        margin: auto;
    }
    .app-map-info-icon{
        width: 200px;
        height: 100%;
        display: flex;
        justify-content: center;
        align-items: center;
        padding-right: 20px;
    }
    .app-map-info-icon i{
        font-size: 40px;
        color:#9fbd38;
    }
    .app-map-info-inactive{
        width: 100%;
        height: 100%;
    }
    .app-map-info-inactive button{
        width: 100%;
        height: 100%;
        border-radius: 100%;
        border: none;
        font-size: 25px;
        color:white;
        background-color: #9fbd38;
    }
</style>

<div class="app-map-main-container">
    <?php if($appMap){ ?>
    <div id="app-map-container"></div>
    <div class="app-map-side-nav">
        <div id="app-map-filters" class="searchObjCSS"></div>
        <button class="btn-toogle-app-map-side-nav">
            <i class="fa fa-chevron-left" aria-hidden="true"></i>
            <i class="fa fa-chevron-right" aria-hidden="true"></i>
        </button>

        <ul class="app-map-tile-switcher"></ul>
    </div>
    <ul class="app-map-controller">
        <li data-action="zoom-center"><i class="fa fa-arrows-alt" aria-hidden="true"></i></li>
        <li data-action="zoom-in"><i class="fa fa-plus" aria-hidden="true"></i></li>
        <li data-action="zoom-out"><i class="fa fa-minus" aria-hidden="true"></i></li>
    </ul>
    <?php if(isset($appMap["info"]) && $appMap["info"]["show"]){ ?>
    <div class="app-map-info active">
        <div class="app-map-info-active">
            <div class="app-map-info-icon">
                <p><i class="fa fa-map-o" aria-hidden="true"></i></p>
            </div>
            <div style="width: 100%;">
                <h1><?= $appMap["info"]["title"] ?> <button><i class="fa fa-window-minimize" aria-hidden="true"></i></button></h1>
                <p>
                    <?= $appMap["info"]["description"] ?? "" ?>
                </p>
               <!--  <a href="#">Visiter notre site</a> -->
            </div>
        </div>
        <div class="app-map-info-inactive">
            <button><i class="fa fa-info" aria-hidden="true"></i></button>
        </div>
    </div>
    <?php } ?>
    <?php }else{ ?>
        <div class="app-map-no-data">
            <img src="<?= $ASSETS_URL ?>/images/empty-data.png" alt="">
            <p><?php echo Yii::t('common', 'Impossible to load the map')?>.<br><?php echo Yii::t('common', 'No data was found')?>.<?php echo Yii::t('common', 'Please specify a valid data source')?>.</p>
            <a href="/"><?php echo Yii::t('common', 'Go to home page')?></a>
        </div>
    <?php } ?>
</div>
<script>
    var removeInfo= <?php echo json_encode( (isset($appMap["embeded"]) && isset($appMap["embeded"]["hideInfo"])) ? $appMap["embeded"]["hideInfo"] : false ); ?>;
    jQuery(document).ready(function() {
        if(removeInfo){
            $(".app-map-info").removeClass("active");
        }    
    });
        
    var TILES = [
        {
            name:"mapnik",
            image:"<?= $ASSETS_URL ?>/images/tile-mapnik.png"
        },
        {
            name:"satelite",
            image:"<?= $ASSETS_URL ?>/images/tile-satelite.png"
        },
        {
            name:"toner",
            image:"<?= $ASSETS_URL ?>/images/tile-toner.png"
        }
    ];

    var appMapConfig = <?= json_encode($appMap) ?>;

    function switchMapTile(comap, tile){
        comap.setTile(tile)
        $(".app-map-tile-switcher").html("")
        var firstTile = "", restTile = ""
        TILES.forEach(function(item){
            if(item.name == tile)
                firstTile = `<li data-tile="${item.name}"><img src="${item.image}" alt=""></li>`
            else
                restTile += `<li data-tile="${item.name}"><img src="${item.image}" alt=""></li>`
        })
        $(".app-map-tile-switcher").html(firstTile + restTile)
        
        $(".app-map-tile-switcher li:first-child").click(function(){
            $(".app-map-tile-switcher li:not(:first-child)").toggleClass("active")
        })
        $(".app-map-tile-switcher li:not(:first-child)").click(function(){
            switchMapTile(comap, $(this).data("tile"))
        })

        if($("#menuBottom").length>0){
            var switchMargin= $(document).height()-$("#menuBottom").offset().top;
            $(".app-map-tile-switcher").css("bottom",switchMargin+20+"px");
        }
    }

    function getMapFilter(){
        if(appMapConfig){
            var customMap=(typeof paramsMapCO.mapCustom!="undefined") ? paramsMapCO.mapCustom : {tile : "maptiler"};
            var appMap = new CoMap({
                container : "#app-map-container",
                activePopUp : true,
                mapOpt:{
                    zoomControl:false
                },
                activePreview : true,
                mapCustom:customMap,
                elts : {}
            })
            var filterConfig = $.extend({}, appMapConfig.filterConfig, {
                container:"#app-map-filters",
                layoutDirection:"horizontal",
                mapCo:appMap,
                results:{
                    multiCols:false,
                    map:{
                        active:true
                    }
                },
                mapContent:{
                    hideViews: ["btnHideMap"]
                }
            })

            var filterSearch = searchObj.init(filterConfig);
            filterSearch.search.init(filterSearch);

            $(".btn-toogle-app-map-side-nav").click(function(){
                $(".app-map-side-nav").toggleClass("active")
            })

            $(".app-map-controller li").click(function(){
                var action = $(this).data("action")
                switch(action){
                    case "zoom-center":
                        appMap.getMap().setZoom(appMap.getOptions().mapOpt.zoom)
                    break;
                    case "zoom-in":
                        appMap.getMap().zoomIn()
                    break;
                    case "zoom-out":
                        appMap.getMap().zoomOut()
                    break;
                }
            })

            $(".app-map-info-inactive button").click(function(){
                $(".app-map-info").addClass("active")
            })
            $(".app-map-info-active h1 button").click(function(){
                $(".app-map-info").removeClass("active")
            })

            switchMapTile(appMap, "mapnik")
        }
    }

    $(function(){
        mylog.log("appMapConfig",appMapConfig);
        if(notNull(costum) && typeof costum.assetsSlug!="undefined" &&
        typeof appMapConfig!="undefined" &&  typeof appMapConfig.filterConfig!="undefined" &&  typeof appMapConfig.filterConfig.js=="string"){
            mylog.log("appMapConfig.filterConfig.js",appMapConfig.filterConfig.js);
            lazyLoad(assetPath+'/js/'+costum.assetsSlug+'/'+appMapConfig.filterConfig.js,null,
                function(){
                    getMapFilter();
                }
            );
        }
        else{
            getMapFilter();   
        }

        if(notNull(costum) && typeof appMapConfig!="undefined" &&  typeof appMapConfig.info!="undefined" &&  typeof appMapConfig.info.downloadApi!="undefined" && appMapConfig.info.downloadApi){
            mylog.log("appMapConfig download api",appMapConfig.info.downloadApi);
            var urlApi = "https://communecter.org/api/organization/get";
            var level3Id=(typeof costum.address!="undefined" && typeof costum.address.level3!="undefined" ) ? costum.address.level3 : null;
            var level3Param="/level3/"+level3Id;
            var source= (typeof costum.assetsSlug!="undefined" && costum.assetsSlug=="reseauTierslieux") ? "franceTierslieux" : costum.slug ;
            var sourceParam="/key/"+source;
            var format=(typeof costum.assetsSlug!="undefined" && ["franceTierslieux","reseauTierslieux"].includes(costum.assetsSlug)) ? "ftl" : null;
            var formatParams="/format/"+format;
            var title= (typeof costum.title!="undefined") ? slugify(costum.title) : null;
            urlApi=urlApi+level3Param+sourceParam+formatParams+"/extraFormat/csv/fileName/"+title;
            var linkDownload="<div><a href='"+urlApi+"' target='_blank'>Télécharger les données</a></div>";
            $(".app-map-info-active").after(linkDownload);
        }
             
    })
</script>