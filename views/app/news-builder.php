<?php $assetsUrl = $this->module->assetsUrl; ?>
<script>
    var BASE_URL_IMAGE = "<?= $assetsUrl ?>/images/news-builder",
        contextData = <?= json_encode($contextData) ?>,
        context = {
            type:contextData.type,
            id: contextData.id
        }

</script>
<?php
    /* library */
    HtmlHelper::registerCssAndScriptsFiles([
        "https://cdn.jsdelivr.net/npm/spectrum-colorpicker2/dist/spectrum.min.css",
        "https://cdn.jsdelivr.net/npm/spectrum-colorpicker2/dist/spectrum.min.js",

        "https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css",
        "https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"
    ]);

    HtmlHelper::registerCssAndScriptsFiles([
        "/js/news-builder/common/lodash.min.js",
        "/js/news-builder/common/mjml-browser.min.js"
    ], $assetsUrl);
    /* end library */

    HtmlHelper::registerCssAndScriptsFiles([
        '/css/coInput/co-input.css',

        "/css/news-builder/index.css",
        "/css/news-builder/accordion.css",
        "/css/news-builder/cnb-inputs.css"
    ], $assetsUrl);

    HtmlHelper::registerCssAndScriptsFiles([
        '/js/coInput/co-input.js',
        '/js/coInput/co-input-types-css.js',
        
        "/js/news-builder/common/jquery-accordion.js",
        "/js/news-builder/common/constant.js",
        "/js/news-builder/common/utils.js",
        "/js/news-builder/common/state.js",
        "/js/news-builder/component/tools/blocks.js",
        "/js/news-builder/component/tools/layers.js",
        "/js/news-builder/component/tools/resources.js",
        "/js/news-builder/component/page.js",
        "/js/news-builder/component/customizer.js",
        "/js/news-builder/index.js",
    ], $assetsUrl);

?>
<div id="news-builder-loader">
    <img src="<?= $assetsUrl."/images/news-builder/loader.gif" ?>" alt="" srcset="">
</div>
<div class="news-builder">
    <header>
        <div class="header-left-news-builder">
            <a href="#@<?= $contextData["slug"] ?>" target="_blank"><span>News</span><span>Builder</span> - <i><?= $contextData["name"] ?></i></a>
        </div>
        <div class="header-right-news-builder">
            <button data-action="open-template"><i class="fa fa-folder-open" aria-hidden="true"></i> Ouvrir un modèle</button>
            <button data-action="preview">
                <span class="show-label"><i class="fa fa-eye" aria-hidden="true"></i> Aperçu</span>
                <span class="hide-label"><i class="fa fa-eye-slash" aria-hidden="true"></i> Fermer l'aperçu</span>
            </button>
            <button data-action="save-news"><i class="fa fa-floppy-o" aria-hidden="true"></i> Enregistrer</button>
            <button data-action="send-news"><i class="fa fa-paper-plane" aria-hidden="true"></i> Envoyer</button>
        </div>
    </header>
    <div class="news-builder-container">
        <div class="side-navigation">
            <div class="side-navigation-header">
                <ul>
                    <li class="active" data-tab="cnb-draganddrop-block"><a href="javascript:;">Composants</a></li>
                    <li data-tab="cnb-layers-tab"><a href="javascript:;">Couches</a></li>
                    <li data-tab="cnb-ressources-panel"><a href="javascript:;">Ressources</a></li>
                </ul>
            </div>
            <div class="side-navigation-body">
                <div class="cnb-sidenav-tab active" id="cnb-draganddrop-block">
                    
                </div>
                <div class="cnb-sidenav-tab" id="cnb-ressources-panel">
                    
                </div>
                <div class="cnb-sidenav-tab" id="cnb-layers-tab">
                    
                </div>
            </div>
        </div>
        <div class="page-container" id="cnb-page">
        </div>
        <div class="right-navigation">
            
        </div>
        <div class="cnb-modal-fullwidth"></div>
    </div>
</div>

<div id="cnb-modal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
      </div>
    </div>
  </div>
</div>