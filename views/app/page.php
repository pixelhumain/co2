<?php 
    
    // $cssAnsScriptFilesTheme = array(
    //     '/plugins/showdown/showdown.min.js',
    //     '/plugins/to-markdown/to-markdown.js'
    // );
    // HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesTheme, Yii::app()->request->baseUrl);

    $layoutPath = 'webroot.themes.'.Yii::app()->theme->name.'.views.layouts.';
    $feedbacktypelist = PHDB::findOne(Lists::COLLECTION, array('name' => 'feedbacktypelist'));

?>
<style type="text/css">
    #feedbackocecodiv  {
        height: 0px;
        width: 85px;
        position: fixed;
        right: 0;
        bottom: 20%;
        z-index: 1000;
        transform: rotate(-90deg);
        -webkit-transform: rotate(-90deg);
    }

    #feedbackocecodiv a {
        display: flex;
        background: #00495d;
        height: 52px;
        width: 155px;
        text-align: center;
        color: #fff;
        /*font-family: Arial, sans-serif;*/
        font-size: 20px;
        font-weight: lighter;
        text-decoration: none;
        border-top-left-radius: 15px;
        border-top-right-radius: 15px;
    }

    #feedbackocecodiv a div.icofdb{
        width: 30%;
        background-color: #a7ba4f;
        padding-top: 10px;
        border-top-left-radius: 15px;
    }

    #feedbackocecodiv a div.icofdb object {
        -webkit-transform: rotate(90deg);
    }

    #feedbackocecodiv a div.lblfdb{
        width: 70%;
        padding-top: 10px;
    }

    #feedbackocecodiv a:hover {
        background:#00495d;
    }

    .iconfeedbackbtn {
        /*background: url('your-image-url.jpg');*/
        height: 20px;
        width: 20px;
        display: inline;
        /* Other styles here */
    }
</style>
<div id="feedbackocecodiv" class="feedbackocecodiv">

</div>

<div class="col-md-12 col-sm-12 col-xs-12 no-padding social-main-container">
	<div class="" id="onepage">
		<?php
            if( !empty($this->costum) && 
                !empty($this->costum['htmlConstruct']) && 
                !empty($this->costum['htmlConstruct']['element']) &&
                !empty($this->costum['htmlConstruct']['element']['urlTpl']) &&
                !empty($this->costum['htmlConstruct']['element']['urlTpl'][$type]) &&
                (
                    (isset($element["type"]) && !empty($this->costum['htmlConstruct']['element']['urlTpl'][$element["type"]])) ||
                    !empty($this->costum['htmlConstruct']['element']['urlTpl']["default"])
                )
            ) {


                $tpl = $this->costum['htmlConstruct']['element']['urlTpl'][$type] ;
                $url = ( !empty($element["type"]) && !empty($tpl[$element["type"]]) ? $tpl[$element["type"]] : $tpl["default"] ) ;
                $params = array("element"=>$element , 
                                "id" => @$id,
                                "type" => @$type,
                                "page" => "page",
                                "canEdit"=>$canEdit,
                                "openEdition" => $openEdition,
                                "linksBtn" => $linksBtn,
                                "isLinked" => $isLinked,
                                "countStrongLinks" => $countStrongLinks,
                                "countInvitations" => $countInvitations,
                                "pageConfig" => $pageConfig,
                                "addConfig" => $addConfig );
                echo $this->renderPartial($url,$params);
            } else if( in_array($type,[Person::COLLECTION,Event::COLLECTION,Project::COLLECTION,Organization::COLLECTION]) ){
        			$params = array("element"=>$element , 
                                    "badgesToShow" => isset($badgesToShow) ? $badgesToShow : [],
                                    "id" => @$id,
                                    "type" => @$type,
        							"page" => "page",
        							"canParticipate"=>@$canParticipate,
                                    "canSee"=>@$canSee,
                                    
                                    "canEdit"=>@$canEdit,
        							"openEdition" => @$openEdition,
        							"linksBtn" => $linksBtn,
        							"isLinked" => $isLinked,
        							"controller" => $controller,
        							"countStrongLinks" => $countStrongLinks,
        							"countInvitations" => $countInvitations,
        							"pageConfig" => $pageConfig,
                                    "addConfig" => $addConfig );

                    if(@$members) $params["members"] = $members;
                    if(@$invitedMe) $params["invitedMe"] = $invitedMe;
                    echo $this->renderPartial('co2.views.element.profilSocial', $params );
                } else if($type == News::COLLECTION){
                    $params = array("element"=>$element , 
                                    "page" => "page",
                                    "type" => $type,
                                    "controller" => $controller,
                                    );

                    if(@$members) $params["members"] = $members;
                    if(@$invitedMe) $params["invitedMe"] = $invitedMe;

                    echo $this->renderPartial('news.views.co.standalone', $params );
                }
                else if($type == Classified::COLLECTION){
                    $params = array("element"=>$element , 
                                    "page" => "page",
                                    "type" => $type,
                                    "controller" => $controller,
                                    );

                    if(@$members) $params["members"] = $members;
                    if(@$invitedMe) $params["invitedMe"] = $invitedMe;

                    echo $this->renderPartial('eco.views.co.standalone', $params );
                    
                }else if($type == Poi::COLLECTION){
                        $params = array(

                            "element"=>$element , 
                            "badgesToShow" => isset($badgesToShow) ? $badgesToShow : [],
                            "id" => @$id,
                            "type" => @$type,
                            "preview" => false,
                            "page" => "page",
                            "canParticipate"=>@$canParticipate,
                            "canSee"=>@$canSee,
                            
                            "canEdit"=>@$canEdit,
                            "openEdition" => @$openEdition,
                            "linksBtn" => $linksBtn,
                            "isLinked" => $isLinked,
                            "controller" => $controller,
                            "countStrongLinks" => $countStrongLinks,
                            "countInvitations" => $countInvitations,
                            "pageConfig" => $pageConfig,
                            "addConfig" => $addConfig
                        );
                        // echo $this->renderPartial('co2.views.poi.standalone', $params );
                        $typePreview = "poi";
                        if (
                            isset($controller->costum["htmlConstruct"])
                            && isset($controller->costum["htmlConstruct"]["preview"])
                            && isset($controller->costum["htmlConstruct"]["preview"][$typePreview])
                        ) {
                            echo $this->renderPartial($controller->costum["htmlConstruct"]["preview"][$typePreview], $params);
                        } else {
                            echo $this->renderPartial('../poi/preview', $params);
                        }
                
                }else if($type == Answer::COLLECTION){
                    $params = array("answer"=>$element , 
                                    "page" => "page",
                                    "type" => $type,
                                    "form"=>$answer["form"],
                                    "canEdit"=>$canEdit,
                                    "controller" => $controller);
                    $params = Form::getDataForm($params);
                    $params["canEdit"]=false;
                    $params["mode"]="r";
                    echo $this->renderPartial('survey.views.tpls.forms.formWizard', $params );
                }
		?>
	</div>
</div>
<script type="text/javascript" >

    var feedbacktypelist = <?php echo json_encode(@$feedbacktypelist['list']) ?>;

    jQuery(document).ready(function() {
        // initScopeMenu();
        setTimeout(function(){
            if(
                typeof contextId != "undefined" &&
                contextId != null &&
                typeof contextData != "undefined" &&
                contextData != null &&
                typeof contextData["type"] != "undefined" &&
                typeof contextData["type"] != "undefined" &&
                typeof contextData["type"] != null &&
                (contextData["type"] == "projects" || contextData["type"] == "organizations")

            ) {
                ajaxPost(
                    null,
                    baseUrl + "/survey/form/checkfeedback",
                    {
                        id : contextId ,
                        type : contextData["type"]
                    },
                    function (data) {
                        if( typeof data.value != "undefined" && ( data.value == true || data.value == "true" && typeof userId != "undefined") ){
                            var feedbackocecodivbtn = '<a href="javascript:;" id="feedbackocecodivbtn" class="feedbackocecodivbtn tooltips" data-toggle="tooltip" data-original-title="feedback" data-placement="left"><div class="icofdb" ><object data="<?php echo Yii::app()->getModule('co2')->assetsUrl; ?>/images/logos/user-profile-message.svg" width="30" height="30"> </object></div> <div class="lblfdb"> FeedBack </div></a>';

                            if (typeof data.el != "undefined" &&
                                typeof data.el.preferences != "undefined" &&
                                typeof data.el.preferences.feedback != "undefined" &&
                                data.el.preferences.feedback == true
                            ) {
                                $('#feedbackocecodiv').html(feedbackocecodivbtn);

                                $(".feedbackocecodivbtn").off().on("click", function () {

                                    var feedbackDyf = {
                                        jsonSchema : {
                                            title : "Type de feedback",
                                            icon : "fa-send",
                                            properties : {
                                                "typefeedback" : {
                                                    "label": "Type",
                                                    "placeholder": "Type",
                                                    "inputType": "select",
                                                    "noOrder": true,
                                                    "options": feedbacktypelist,
                                                    "rules": {"required": true}
                                                }
                                            },
                                            save : function () {

                                                smallMenu.openAjaxHTML(baseUrl + '/survey/answer/index/id/new/mode/w/form/' + Object.values(data.forms)[0]["_id"]["$id"] + '/feedback/true/typefeedback/'+$("#typefeedback").val());
                                            }
                                        }
                                    };
                                    dyFObj.openForm(feedbackDyf);
                                });
                            } else {
                                $('#feedbackocecodiv').html("");
                            }
                        }
                    }
                );
            } else {
                $('#feedbackocecodiv').html("");
            }

        }, 2000);

    });

/*var type = "<?php echo $type; ?>";
var id = "<?php echo $id; ?>";
var view = "<?php echo @$view; ?>";
var indexStepGS = 20;
*/

</script>