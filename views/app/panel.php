<style>
    .panel-container .main-center-container {
        box-shadow: 3px 3px 8px 1px rgba(0, 0, 0, .2);
        height: 100%;
        padding-top: 2rem;
        border-radius: 8px;
    }
</style>
<div class="col-md-12 col-sm-12 panel-container col-xs-12 no-padding social-main-container">
    <div id="onepage" class="bs-mt-5 main-center-container container">
        <?= $this->renderPartial($url,["params" => $params]); ?>
    </div>
</div>