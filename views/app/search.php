<?php 
    $cssAnsScriptFilesModule = array(
    '/assets/css/default/responsive-calendar.css',
    '/assets/css/default/search.css',
    '/assets/js/comments.js'
    );
    HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, Yii::app()->theme->baseUrl);

    

//     $cssAnsScriptFilesModule = array(
//     '/js/default/responsive-calendar.js',
// //    '/js/default/search.js',
//  //   '/js/news/index.js',
//     );
//     HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, $this->module->getParentAssetsUrl());
 
    

    $layoutPath = 'webroot.themes.'.Yii::app()->theme->name.'.views.layouts.';

    $maxImg = 5;
    $page = (@$page && !empty($page)) ? $page : "search";
    if(!@$type){  $type = "all"; }

    if(@$type=="events")    { $page = "agenda"; $maxImg = 7; }
    if(@$type=="classifieds"){ $page = "annonces"; $maxImg = 1; }
    if(@$type=="proposals") { $page = "dda";}

    $filliaireCategories = CO2::getContextList("filliaireCategories");
    $directoryParams=$this->appConfig["directory"];
    if(@$this->appConfig["pages"]["#".$page]["directory"])
        $directoryParams=$this->appConfig["pages"]["#".$page]["directory"];
    if( isset($this->costum["app"]["#".$page]["tagsList"]) ){
        if(is_string($this->costum["app"]["#".$page]["tagsList"]) && isset($this->costum[$this->costum["app"]["#".$page]["tagsList"]])){
            $nameParamsTags = $this->costum["app"]["#".$page]["tagsList"];
            $tagsList = $this->costum[$this->costum["app"]["#".$page]["tagsList"]];
        }else{
            $nameParamsTags = "tags";
            $tagsList = $this->costum["app"]["#".$page]["tagsList"] ;
        }
        
    }else if( isset($this->costum["tags"]) ){
        $nameParamsTags = "tags" ;
        $tagsList = $this->costum["tags"];
    }







?>

<style>

    #dropdown_search{
        margin-top:0px;
    }

    .container{
        padding-bottom:0px !important;
    }
    .simple-pagination{
        padding: 5px 0px;
        border-bottom: 1px solid #e9e9e9;
      /*  font-family: "Montserrat", "Helvetica Neue", Helvetica, Arial, sans-serif;*/
    }

    .simple-pagination li a, .simple-pagination li span {
        border: none;
        box-shadow: none !important;
        background: none !important;
        color: #707478 !important;
        font-size: 13px !important;
        font-weight: 500;
    }
    .simple-pagination li.active span{
        color: #d9534f !important;
        font-size: 24px !important; 
    }

    .simple-pagination li a.next,
    .simple-pagination li a.prev{
        color:#223f5c !important;
    }
    
</style>
<script>
    var appConfigPageFilters = <?= json_encode(@$this->appConfig["pages"]["#".$page]["filters"]["answers"]); ?>
</script>
<?php //if(isset($this->appConfig["pages"]["#".$page]["useFilter"]) && isset($this->appConfig["pages"]["#".$page]["filterObj"])){
    $searchParamsViews=(isset($this->appConfig["pages"]["#".$page]["filterObj"])) ? $this->appConfig["pages"]["#".$page]["filterObj"] : "co2.views.app.filters.search";
     echo $this->renderPartial($searchParamsViews, array("page"=>$page, "appConfig"=>@$this->appConfig["pages"]["#".$page]));
//} ?>
<!-- <div id="mapContent" class="col-md-12 col-sm-12 col-xs-12" style="display: none; height: 600px"></div> -->
<div class="col-md-12 col-sm-12 col-xs-12 bg-white shadow" 
     id="search-content" style="min-height:700px;">
	<div class="col-md-12 col-sm-12 col-xs-12 no-padding app-<?php echo $page ?>" id="page">
        <?php 

        if(isset($this->appConfig["pages"]["#".$page]["header"])){
            //var_dump($this->appConfig["pages"]["#".$page]["header"]);
            echo $this->renderPartial($this->appConfig["pages"]["#".$page]["header"], array("page"=>$page,"pageDetail"=>$this->appConfig["pages"]["#".$page]));
        }
        if(isset($this->costum["htmlConstruct"]["directory"]["filters"]["tagsList"]) && isset($tagsList)
                ){ 

                if( !empty($this->costum["paramsData"]) &&
                    !empty($this->costum["paramsData"][$nameParamsTags])  ){
                    $paramsTags = $this->costum["paramsData"][$nameParamsTags];
                }
                $tagsVertical = false ;
                if(!empty($this->costum["htmlConstruct"]["directory"]["filters"]["tagsList"]["rendering"]) && $this->costum["htmlConstruct"]["directory"]["filters"]["tagsList"]["rendering"] == "vertical"){
                    $tagsVertical = true ;
                }
                
        ?> 



        <div class="col-md-10 col-sm-10 col-xs-12 text-left col-sm-offset-1 col-md-offset-1 text-center margin-top-20 tags-cloud">
           
            <?php
                $colLi = "";
                if( $tagsVertical === true){
                    $colLi .= " col-xs-12 ";
                }

                $tagsHTML = " <a class='btn btn-link ".$colLi." bg-purple elipsis btn-tags-unik'
                    data-k='' href='javascript:;'>".Yii::t("common","See all")."</a>" ;
                foreach($tagsList as $v){    
                    if(!empty($paramsTags[$v]["color"])){
                        $color = (!empty($paramsTags[$v]["color"]) ? "background-color : ".$paramsTags[$v]["color"].";" : "");
                        $class = "";
                    }else{
                        $class = "bg-purple";
                        $color = "";
                    }

                    $class .=$colLi;
                    $tagsHTML .= '<a class="btn btn-link '.$class.' elipsis  btn-tags-unik" data-k="'.$v.'" href="javascript:;" style="'.$color.'" >'.$v.'</a>';

                   // $tagsHTML .= "<a class='btn btn-link ".$class." elipsis  btn-tags-unik'
                    //    data-k='".$v."' href='javascript:;' style='".$color."' >".$v."</a>";

                    if( !empty($tagsVertical) && $tagsVertical === true){
                        $tagsHTML .= "<br/>";
                    }
                } 


                if( empty($tagsVertical)) {
                    echo $tagsHTML;
                }
            ?>
        </div> 
        
        <?php } 
        
        //top page Title 
        if( isset( $this->costum[ "app" ][ "#".$page ]["title"] ) ){
        ?>
            <h3 class="title-section col-sm-8"><?php echo $this->costum[ "app" ][ "#".$page ]["title"] ?></h3>
        <?php 
        }

        if(!empty($directoryParams["header"])){ 
            //$classHeader =($this->appConfig["appRendering"]=="vertical") ? "no-padding col-xs-12" : "no-padding col-md-10 col-sm-10 col-xs-12 text-left col-sm-offset-1 col-md-offset-1";
            $classHeader = "col-xs-12";
            if(!empty($directoryParams["header"]["class"]))
                $classHeader =  $directoryParams["header"]["class"];
            ?>
            <div class="headerSearchContainer no-padding <?php echo $classHeader; ?>"></div>

        <?php } ?>
        <?php 
        
        /*if(  isset($this->appConfig["pages"]["#".$page]["calendar"]) && 
            isset($this->appConfig["pages"]["#".$page]["calendar"]["renderingCalendar"]) &&
			$this->appConfig["pages"]["#".$page]["calendar"]["renderingCalendar"] == "top" ) {
            //$this->renderPartial('co2.views.app.calendar', array());
            ?>
            <div class='calendarSearch col-xs-12'></div>
            <?php
		}*/

        //$classBodySearch = ($this->appConfig["appRendering"]=="vertical") ? "col-xs-12" : "col-md-10 col-sm-10 col-xs-12 text-left col-sm-offset-1 col-md-offset-1";
        $classBodySearch = "col-xs-12" ;
        if( !empty($tagsVertical) && $tagsVertical === true){
            $classBodySearch = "col-md-10 col-sm-10 col-xs-12";
            echo '<div class="tagsMenuSearch col-md-2 col-xs-12">';
                echo $tagsHTML ;
            echo '</div>' ;
        }

        if( !empty($directoryParams["body"]["class"]) )
            $classBodySearch = $directoryParams["body"]["class"];

        ?>

        <div class="<?php echo $classBodySearch ?> bodySearchContainer margin-top-10 <?php echo $page ?>">
            <div class="no-padding col-xs-12" id="dropdown_search">
            </div>
            <div class="no-padding col-xs-12 text-left footerSearchContainer smartgrid-slide-element"></div>   
        </div>
    </div>
<?php 
echo $this->renderPartial('co2.views.modals.pageCreate', array());
// if( !empty($this->appConfig["pages"]["#".$page]["calendar"]) && 
// 	!empty($this->appConfig["pages"]["#".$page]["calendar"]["renderingCalendar"]) &&
// 	$this->appConfig["pages"]["#".$page]["calendar"]["renderingCalendar"] == "bottom" ) {
// 	$this->renderPartial('co2.views.app.calendar', array());
// }
//$this->renderPartial($layoutPath.'footer', array(  "page" => $page)); ?>


<script type="text/javascript" >

var type = "<?php echo @$type ? $type : 'all'; ?>";
var typeInit = "<?php echo @$type ? $type : 'all'; ?>";
directory.appKeyParam="#<?php echo $page ?>";//(location.hash.indexOf("?") >= 0) ? location.hash.split("?")[0] : location.hash;
var pageCount=false;
//searchObject.count=true;
//searchObject.initType=typeInit;
<?php if(@$type=="events"){ ?>
 /* var STARTDATE = new Date();
  var ENDDATE = new Date();
  var startWinDATE = new Date();
  var agendaWinMonth = 0;
  var dayCount = 0;*/
<?php } ?>
//var scrollEnd = false;
/*if(searchObject.initType=="events") var categoriesFilters=<?php //echo json_encode(Event::$types) ?>;
if(searchObject.initType=="all"){
  var categoriesFilters={
    "persons" : { "key": "persons", "icon":"user", "label":"people","color":"yellow"}, 
    "NGO" : { "key": "NGO", "icon":"group", "label":"NGOs","color":"green-k"}, 
    "LocalBusiness" : { "key": "LocalBusiness", "icon":"industry", "label":"LocalBusiness","color":"azure"}, 
    "Group" : { "key": "Group", "icon":"circle-o", "label":"Groups","color":"turq"}, 
    "GovernmentOrganization" : { "key": "GovernmentOrganization", "icon":"university", "label":"services publics","color":"red"},
    "Cooperative" : { "key": "Cooperative", "icon":"university", "label":"Cooperative","color":"green"},
    "projects" : { "key": "projects", "icon":"lightbulb-o", "label":"projects","color":"purple"}, 
    "events" : { "key": "events", "icon":"calendar", "label":"events","color":"orange"}, 
    "poi" : { "key": "poi", "icon":"map-marker", "label":"points of interest","color":"green-poi"}, 
    //"place" : { "key": "place", "icon":"map-marker", "label":"points of interest","color":"brown"},
    //"places" : { "key": "places", "icon":"map-marker", "label":"Places","color":"brown"}, 
    "classifieds" : { "key": "classified", "icon":"bullhorn", "label":"classifieds","color":"azure"}, 
     
    //"ressources" : { "key": "ressources", "icon":"cubes", "label":"Ressource","color":"vine"} 
    //"services" : { "key": "services", "icon":"sun-o", "label":"services","color":"orange"}, "circuits" : { "key": "circuits", "icon":"ravelry", "label":"circuits","color":"orange"},
  };
}*/

//var filliaireCategories = <?php //echo json_encode($filliaireCategories); ?>;
//var currentKFormType = "";

jQuery(document).ready(function() {
      //    initKInterface();
    if(typeof appConfig != "undefined" && typeof appConfig.showMap != "undefined" && appConfig.showMap){
        if(typeof filterSearch.results == "undefined")
            filterSearch.results = {}
        if(typeof filterSearch.results.map == "undefined")
            filterSearch.results.map = {}
        if(typeof filterSearch.results.map != "undefined" && typeof filterSearch.results.map.active != "undefined" && !filterSearch.results.map.active) 
            filterSearch.results.map.active = true
    }
    if(typeof filterSearch != "undefined")
        filterSearch.search.init(filterSearch);
    
    //else{
        /*initCountType();

        loadingData = false; 
        if(typeof costum != "undefined" && notNull(costum) && typeof costum.app != "undefined" && typeof directory.appKeyParam != "undefined" && typeof costum.app[directory.appKeyParam] != "undefined" && typeof costum.app[directory.appKeyParam].searchObject != "undefined" && typeof costum.app[directory.appKeyParam].searchObject.indexStep != "undefined")
            searchObject.indexStep=costum.app[directory.appKeyParam].searchObject.indexStep;

        if(typeof costum != "undefined" && notNull(costum) && typeof costum.app != "undefined" && typeof directory.appKeyParam != "undefined" && typeof costum.app[directory.appKeyParam] != "undefined" && typeof costum.app[directory.appKeyParam].searchObject != "undefined" && typeof costum.app[directory.appKeyParam].searchObject.sortBy != "undefined")
            searchObject.sortBy=costum.app[directory.appKeyParam].searchObject.sortBy;

        if(typeof costum != "undefined" && notNull(costum) && typeof costum.app != "undefined" && typeof directory.appKeyParam != "undefined" && typeof costum.app[directory.appKeyParam] != "undefined" && typeof costum.app[directory.appKeyParam].searchObject != "undefined" && typeof costum.app[directory.appKeyParam].searchObject.filters != "undefined")
            searchObject.filters=costum.app[directory.appKeyParam].searchObject.filters;

        if(typeof costum != "undefined" && notNull(costum) && typeof costum.app != "undefined" && typeof directory.appKeyParam != "undefined" && typeof costum.app[directory.appKeyParam] != "undefined" && typeof costum.app[directory.appKeyParam].searchObject != "undefined" && typeof costum.app[directory.appKeyParam].searchObject.links != "undefined"){


            var followers = [];
            $.each(costum.app[directory.appKeyParam].searchObject.links,function(k,v){
                if (costum.contextId != "undefinied") {
                   followers.push({type : v , id : costum.contextId}); 
                   
                }
            });
            searchObject.links = followers;
        }

        if(typeof costum != "undefined" && notNull(costum) && typeof costum.app != "undefined" && typeof costum.app[directory.appKeyParam] != "undefined" && typeof costum.app[directory.appKeyParam].searchObject != "undefined" && typeof costum.app[directory.appKeyParam].searchObject.sourceKey != "undefined"){
            searchObject.sourceKey=costum.app[directory.appKeyParam].searchObject.sourceKey;
        }

        searchInterface.init(type);*/
        /*if(type=="events"){
            if(window.location.href.indexOf("#agenda") == -1)
            	window.history.pushState({},"", window.location.href+"#agenda");
            calculateAgendaWindow(0);


            if(typeof calendar != "undefined" && type=="events"){
                calendar.init("#profil-content-calendar");

                var endDate = moment(STARTDATE).set("month", moment(STARTDATE).get("month")+1).valueOf();
                var secondEndDate = Math.floor(endDate / 1000);
                calendar.searchInCalendar(searchObject.startDate, secondEndDate);
            }

            
        }

        startSearch(searchObject.indexMin, null, searchCallback);

        $(".tooltips").tooltip();*/
    //}
});


/* -------------------------
AGENDA
----------------------------- */

<?php /*if(@$type == "events"){ ?>

//var calendarInit = false;


<?php } */ ?>

/* -------------------------
END AGENDA
----------------------------- */

</script>