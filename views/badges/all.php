<div id='filterContainer' class='searchObjCSS'></div>
    <div class='headerSearchIncommunity no-padding col-xs-12'>
    </div>
    <div class='badgeSearchContainer   margin-top-50'>
    <div class='no-padding col-xs-12' id='dropdown_search'></div>
    </div>
    <div class='no-padding col-xs-12 text-left footerSearchContainer smartgrid-slide-element'></div>
</div>

<script>
    var filterGroupbadge
    $(function() {
        var paramsFilterBadge = {
            urlData: baseUrl + "/co2/search/globalautocomplete",
            container: "#filterContainer",
            header: {
                dom: ".headerSearchIncommunity",
                options: {
                    left: {
                        classes: 'col-xs-8 elipsis no-padding',
                        group: {
                            count: true,
                            types: true
                        }
                    },
                    right :{
                        classes : 'col-xs-4 text-right no-padding',
                        group : {
                            map : true
                        }
                    }
                },
            },
            defaults: {
                notSourceKey: true,
                types: ["organizations", "projects", "citoyens"],
                forced: {
                    filters: {
                        "badges.<?= (string) $element["_id"] ?>" : {"$exists" : true}
                    }
                },
                fields: ['badges'],
            },
            results: {
                dom: ".badgeSearchContainer #dropdown_search",
                smartGrid: true,
                renderView: "directory.badgePanelHtmlFullWidth",
                map: {
                    active: false
                }
            },
            filters: {
                text: true,
                types :{
                    lists :["citoyens", 
                    "projects",
					"NGO",
					"LocalBusiness",
					"Group",
					"GovernmentOrganization",
					"Cooperative"]
                }
            }
        };
        filterGroupbadge = searchObj.init(paramsFilterBadge);
        filterGroupbadge.search.init(filterGroupbadge);

        coInterface.bindLBHLinks();
       
    })
</script>