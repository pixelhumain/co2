<?php 
    $assets = [
        'css/badge-creator.css',
        'js/badge-creator.js',
    ];
    HtmlHelper::registerCssAndScriptsFiles($assets,Yii::app()->getModule('co2')->getAssetsUrl() . '/');
?>

<div>
<div id="content">
        <div id="control-panel">
            <div id="element-container">

            </div>
        </div>
        <div id="main">
            <div id="header-control">
                <div id="attributes">
                    
                    <div><button id="btn-bold" class="btn btn-default btn-text-attribute"><i class="fa fa-bold"></i></button></div>
                    <div><button id="btn-italic" class="btn btn-default btn-text-attribute"><i class="fa fa-italic"></i></button></div>
                    <div><button id="btn-underline" class="btn btn-default btn-text-attribute"><i class="fa fa-underline"></i></button></div>

                    <div id="btn-group-align" class="btn-group">
                        <button id="btn-align" type="button" class="btn btn-default dropdown-toggle btn-text-attribute" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          <i class="fa fa-align-center"></i>
                        </button>
                        <ul class="dropdown-menu">
                          <li id="align-container">
                              <div>
                                  <div><button id="btn-align-left" class="btn btn-default btn-text-attribute"><i class="fa fa-align-left"></i></button></div>
                                  <div><button id="btn-align-right" class="btn btn-default btn-text-attribute"><i class="fa fa-align-right"></i></button></div>
                                  <div><button id="btn-align-center" class="btn btn-default btn-text-attribute"><i class="fa fa-align-center"></i></button></div>
                                  <div><button id="btn-align-justify" class="btn btn-default btn-text-attribute"><i class="fa fa-align-justify"></i></button></div>
                              </div>
                          </li>
                        </ul>
                      </div>
                    <div id="btn-group-vertical-align" class="btn-group">
                        <button id="btn-vertical-align" type="button" class="btn btn-default dropdown-toggle btn-text-attribute" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <svg width="20" height="30" version="1.1" viewBox="0 0 7.9375 7.9375">
                                <g stroke="#000" stroke-width=".26458">
                                 <rect x="1.0013" y="3.2283" width="5.88" height=".23446" rx=".44309" ry=".21463"/>
                                 <rect x="1.0303" y="4.2579" width="5.88" height=".23446" rx=".44309" ry=".21463"/>
                                </g>
                               </svg>
                        </button>
                        <ul class="dropdown-menu">
                          <li id="align-container">
                              <div>
                                <div><button id="btn-align-top" class="btn btn-default btn-text-attribute">
                                    <svg width="20" height="30" version="1.1" viewBox="0 0 7.9375 7.9375">
                                        <g stroke="#000" stroke-width=".26458" style="transform: translateY(-2.5px);">
                                         <rect x="1.0013" y="3.2283" width="5.88" height=".23446" rx=".44309" ry=".21463"/>
                                         <rect x="1.0303" y="4.2579" width="5.88" height=".23446" rx=".44309" ry=".21463"/>
                                        </g>
                                       </svg>
                                </button></div>
                                <div><button id="btn-align-middle" class="btn btn-default btn-text-attribute">
                                    <svg width="20" height="30" version="1.1" viewBox="0 0 7.9375 7.9375">
                                        <g stroke="#000" stroke-width=".26458">
                                         <rect x="1.0013" y="3.2283" width="5.88" height=".23446" rx=".44309" ry=".21463"/>
                                         <rect x="1.0303" y="4.2579" width="5.88" height=".23446" rx=".44309" ry=".21463"/>
                                        </g>
                                       </svg>
                                </button></div>
                                <div><button id="btn-align-bottom" class="btn btn-default btn-text-attribute">
                                    <svg width="20" height="30" version="1.1" viewBox="0 0 7.9375 7.9375">
                                        <g stroke="#000" stroke-width=".26458" style="transform: translateY(2.5px);">
                                         <rect x="1.0013" y="3.2283" width="5.88" height=".23446" rx=".44309" ry=".21463"/>
                                         <rect x="1.0303" y="4.2579" width="5.88" height=".23446" rx=".44309" ry=".21463"/>
                                        </g>
                                       </svg>
                                </button></div>
                              </div>
                          </li>
                        </ul>
                      </div>
                </div>
                <div></div>
                <div id="container-button-navigation">
                    <button class="btn btn-success mx-5px" id="btn-download-png"><?php echo Yii::t("badge", "Download as png") ?></button>
                    <button class="btn btn-success mx-5px" id="btn-png-into-uploader"><?php echo Yii::t("common", "Save") ?></button>
                    <button class="btn btn-success mx-5px" id="btn-export-artboard"><?php echo Yii::t("badge", "Save Artboard") ?></button>
                    <button class="btn btn-success mx-5px" id="btn-import-artboard"><?php echo Yii::t("badge", "Import Artboard") ?></button>
                    <button class="btn btn-danger mx-5px" id="btn-cancel"><?php echo Yii::t("badge", "Quit") ?></button>
                    <button class="btn btn-navigation mx-5px" disabled="true" id="btn-undo">
                        <i class="fa fa-undo" ></i>
                    </button>
                    <button class="btn btn-navigation mx-5px" disabled="true" id="btn-redo">
                        <i class="fa fa-repeat" ></i>
                    </button>
                </div>
            </div>
            <div id="main-content">
                
                <div id="svg-handler-container">
                    <div class="svg-container">
                        
                    </div>
                    <div id="handler" style="display: none;">
                        <div id="handle-tl"></div>
                        <div id="handle-tr"></div>
                        <div id="handle-br"></div>
                        <div id="handle-bl"></div>
                        <div id="handle-ct"></div>
                        <div id="handler-other-controller">
                            <div id="btn-up"><i class="fa fa-arrow-up"></i></div>
                            <div id="btn-down"><i class="fa fa-arrow-down"></i></div>
                            <div id="btn-clone"><i class="fa fa-clone text-primary"></i></div>
                            <div id="btn-trash"><i class="fa fa-trash text-danger"></i></div>
                        </div>
                    </div>
                    <ul id="contextMenu" class="dropdown-menu" role="menu" style="display:none;" >
                        <li><input id="fileUpload" type="file" accept="image/*" style="display: none;"><a tabindex="-1" href="#" id="btn-import-image">Importer une image</a></li>
                        <li><a id="btn-copy-image" tabindex="-1" href="#">Copier l'image</a></li>
                        <li><a id="btn-insert-text" tabindex="-1" href="#">Inserer texte</a></li>
<!-- 
                        <li class="divider"></li>
                        <li><a tabindex="-1" href="#">TO CHANGE</a></li>
-->
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <script>
        const art = Artboard.getInstance();

        var controlPanel = document.querySelector("#control-panel");
        var elementContainer = controlPanel.querySelector("#element-container");
        var element = null;
        
        <?php
            
            $dir = dirname(__FILE__) . '/../../assets/images/badges-components';
            if (is_dir($dir)) {
                if ($dh = opendir($dir)) {
                    while (($file = readdir($dh)) !== false) {
                        if(strpos($file, ".svg") !== false){
                            $path = $dir . "/" . $file;
                            if(!is_dir($path)){
        ?>
                                element = document.createElement("div");
                                element.setAttribute("class", "element");
                                element.innerHTML = `<?= file_get_contents($path) ?>`
                                elementContainer.appendChild(element);
                                element.addEventListener("click", function (event) {
                                    const svg = event.currentTarget.querySelector("svg");
                                    Artboard.getInstance().stackControl.do(new ImportationCommand(svg, true));
                                });
        <?php               }
                        }
                    }
                    closedir($dh);
                }
            }
        ?>


        var startDragPoint = null;

        document.addEventListener('paste', function(event){
            var items = (event.clipboardData || event.originalEvent.clipboardData).items;
            console.log(JSON.stringify(items)); // will give you the mime types
            for (index in items) {
              var item = items[index];
              if (item.kind === 'file') {
                var file = item.getAsFile();
                getBase64(file).then(importIntoArtboard);
              }
            }
        });
        function getBase64(file) {
            return new Promise((resolve, reject) => {
                    var reader = new FileReader();
                    reader.readAsDataURL(file);
                    reader.onload = function () {
                        const base64 = reader.result;
                        const image = new Image();
                        image.onload = function () {
                            const width = image.width;
                            const height = image.height;
                            resolve({file, base64, width, height});
                        }
                        image.src = base64;
                    };
                reader.onerror = function (error) {
                    reject('Error: ', error);
                };
            })
        }

        const importIntoArtboard = ({file, base64, width, height}) => {
            if(file.type == 'image/svg+xml'){
                const div = document.createElement('div');
                const index = base64.indexOf("base64,")
                const data = base64.substring(index + 7);
                div.innerHTML = atob(data);
                const svg = div.querySelector("svg");
                if(svg.getAttribute("viewBox") == null){
                    svg.setAttribute("viewBox", `0 0 ${width} ${height}`);
                }
                if(width > 400){
                    svg.setAttribute("width", 400);
                }
                if(height > 400){
                    svg.setAttribute("height", 400);
                }
                Artboard.getInstance().stackControl.do(new ImportationCommand(svg));
            }else{
                const svg = document.createElementNS(NS, "svg");
                svg.setAttribute("viewBox", `0 0 ${width} ${height}`);
                if(width > 400){
                    svg.setAttribute("width", 400);
                }
                if(height > 400){
                    svg.setAttribute("height", 400);
                }
                const image = document.createElementNS(NS, "image");
                image.setAttribute("href", base64);
                svg.appendChild(image);
                Artboard.getInstance().stackControl.do(new ImportationCommand(svg));
            }
        };

        const NS = 'http://www.w3.org/2000/svg';
        const input = document.querySelector("#fileUpload");
        input.addEventListener('change', (e2) => {
            if(input.files && input.files.length > 0){
                const file = input.files[0];
                console.log();
                getBase64(file).then(importIntoArtboard).then((_) => {
                    input.value = "";
                })
            }
        });
        
        document.querySelector("#btn-import-image").addEventListener('click', (e) => {
            e.preventDefault();
            document.querySelector("#contextMenu").style.display = "none";
            const input = document.querySelector("#fileUpload");
            input.click();
        })
        document.querySelector("#btn-copy-image").addEventListener('click', (e) => {
            e.preventDefault();
            document.querySelector("#contextMenu").style.display = "none";
            SvgUtils.copyIntoClipboard();
        })
        document.querySelector("#btn-insert-text").addEventListener('click', (e) => {
            e.preventDefault();
            document.querySelector("#contextMenu").style.display = "none";
            Artboard.getInstance().stackControl.do(new InsertTextCommand());
        })
        document.querySelector("#btn-cancel").addEventListener("click",e => {
            e.preventDefault();
            document.querySelector("#ajax-modal #badge-creator-container").style.display = "none";
        })
        $(function() {
            const old = localStorage.getItem("badge-creator");
            Artboard._isAttachedElementEvents = false;
            Artboard._isAttachedContextualMenuEvents = false;
            Artboard.getInstance()._init(old);
        })
    </script>
</div>