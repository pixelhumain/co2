<?php
    $badgesKeys = array();
    if(isset($el["badges"])){
        $badgesKeys = array_keys($el["badges"]);
    }
?>
<div class=" col-md-12">
    <div id='filterContainer' class='searchObjCSS'></div>
    <div class='headerSearchIncommunity no-padding col-xs-12'></div>
    <div class='bodySearchContainer margin-top-30'>
        <div class='no-padding col-xs-12' id='dropdown_search'>
        </div>
        <div class='no-padding col-xs-12 text-left footerSearchContainer smartgrid-slide-element'></div>
    </div>
</div>
<script>
    $(function(){
        var paramsFilterBadge= {
            urlData : baseUrl+"/co2/search/globalautocomplete",
            container : "#filterContainer",
            header : {
                dom : ".headerSearchIncommunity",
                options : {
                    left : {
                        classes : 'col-xs-8 elipsis no-padding',
                        group:{
                            count : true,
                            types : true
                        }
                    }
		        },
            },
            defaults : {
                notSourceKey : true,
                types : ["badges"],
                forced: {
                    filters: {
                        'creator' : <?= json_encode($el["_id"]) ?>["$id"]
                    }
                }
            },
            results : {
                dom:".bodySearchContainer",
                smartGrid : true,
                renderView :"directory.elementPanelHtml",
                map : {
            	    active : false
                }
            },
            filters : {
                text : true
            }
        };
        var filterGroupbadge = searchObj.init(paramsFilterBadge);
        mylog.log("mitia",filterGroupbadge);
        filterGroupbadge.search.init(filterGroupbadge);
    })
</script>