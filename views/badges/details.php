<?php
HtmlHelper::registerCssAndScriptsFiles([
    '/plugins/json-format-highlight/json-format-highlight.js'
],Yii::app()->request->baseUrl);
?>

<style>
    /* USER PROFILE PAGE */
    #details-modal-content .card {
  margin-top: 20px;
  padding: 30px;
  background-color: rgba(214, 224, 226, 0.2);
  -webkit-border-top-left-radius: 5px;
  -moz-border-top-left-radius: 5px;
  border-top-left-radius: 5px;
  -webkit-border-top-right-radius: 5px;
  -moz-border-top-right-radius: 5px;
  border-top-right-radius: 5px;
  -webkit-box-sizing: border-box;
  -moz-box-sizing: border-box;
  box-sizing: border-box;
}
#details-modal-content .card.hovercard {
  position: relative;
  padding-top: 0;
  overflow: hidden;
  text-align: center;
  background-color: #fff;
  background-color: white;
}
#details-modal-content .card.hovercard .card-background {
  height: 130px;
}
#details-modal-content .card-background img {
  -webkit-filter: blur(25px);
  -moz-filter: blur(25px);
  -o-filter: blur(25px);
  -ms-filter: blur(25px);
  filter: blur(25px);
  margin-left: -100px;
  margin-top: -200px;
  min-width: 130%;
}
#details-modal-content .card.hovercard .useravatar {
  position: absolute;
  top: 15px;
  left: 0;
  right: 0;
}
#details-modal-content .card.hovercard .useravatar img {
  width: 100px;
  height: 100px;
  max-width: 100px;
  max-height: 100px;
  -webkit-border-radius: 50%;
  -moz-border-radius: 50%;
  border-radius: 50%;
  border: 5px solid rgba(255, 255, 255, 0.5);
}
#details-modal-content .card.hovercard .card-info {
  position: absolute;
  bottom: 14px;
  left: 0;
  right: 0;
}
#details-modal-content .card.hovercard .card-info .card-title {
  padding: 0 5px;
  font-size: 20px;
  line-height: 1;
  color: #262626;
  background-color: rgba(255, 255, 255, 0.1);
  -webkit-border-radius: 4px;
  -moz-border-radius: 4px;
  border-radius: 4px;
}
#details-modal-content .card.hovercard .card-info {
  overflow: hidden;
  font-size: 12px;
  line-height: 20px;
  color: #737373;
  text-overflow: ellipsis;
}
#details-modal-content .card.hovercard .bottom {
  padding: 0 20px;
  margin-bottom: 17px;
}
#details-modal-content .btn-pref .btn {
  -webkit-border-radius: 0 !important;
}
</style>
<script>
    $(document).ready(function() {
        $(".btn-pref .btn").click(function () {
            $(".btn-pref .btn").removeClass("btn-primary").addClass("btn-default");
            $(this).removeClass("btn-default").addClass("btn-primary");   
        });
        
        var formatted = jsonFormatHighlight(<?= json_encode($assertion) ?>,{
            keyColor: '#f92672',
            numberColor: 'blue',
            stringColor: '#f8f8f2',
            trueColor: '#00cc00',
            falseColor: '#ff8080',
            nullColor: 'cornflowerblue'
        });
        $("#json-container").html(formatted);
        $("#btn-download").off().on('click', (e) => {
            const a = document.createElement("a");
            a.href = "<?= $assertion["image"] ?>";
            a.target = "_blank"
            a.click();
        })
    });
</script>

<?php if(isset($narrative) && $narrative != true) { ?>

<div id="details-modal-content" class="col-sm-12">
    <div class="btn-pref btn-group btn-group-justified btn-group-lg" role="group" aria-label="...">
        <?php if($badgeElement["isParcours"] == "true") {?>
        <div class="btn-group" role="group">
            <button type="button" id="progress" class="btn btn-primary" href="#tab4" data-toggle="tab"><span class="fa fa-tasks"></span>
                <div class="hidden-xs">Progression</div>
            </button>
        </div>
        <?php } ?>
        <div class="btn-group" role="group">
            <button type="button" id="json" class="btn <?= $badgeElement["isParcours"] == "true" ? "btn-default" : "btn-primary" ?>" href="#tab1" data-toggle="tab"><span class="fa fa-code" <?= $badgeElement["isParcours"] == "true" ? "aria-hidden=\"true\"" : "" ?>></span>
                <div class="hidden-xs">JSON</div>
            </button>
        </div>
        <div class="btn-group" role="group">
            <button type="button" id="favorites" class="btn btn-default" href="#tab2" data-toggle="tab"><span class="fa fa-globe" aria-hidden="true"></span>
                <div class="hidden-xs">URL</div>
            </button>
        </div>
        <div class="btn-group" role="group">
            <button type="button" id="following" class="btn btn-default" href="#tab3" data-toggle="tab"><span class="fa fa-image" aria-hidden="true"></span>
                <div class="hidden-xs">Image</div>
            </button>
        </div>
    </div>

        <div class="well">
      <div class="tab-content">
      <?php if($badgeElement["isParcours"] == "true") {?>
            <div class="tab-pane fade in active" id="tab4">
                
            </div>
            <script>
                getAjax("#tab4", baseUrl + '/co2/badges/parcours/element/<?= (string) $awardElement["_id"] ?>/collection/<?= (string) $awardElement["collection"] ?>/badge/<?= (string) $badgeElement["_id"] ?>', null, 'html');
            </script>
        <?php } ?>
        <div class="tab-pane fade <?= $badgeElement["isParcours"] == "true" ? "" : "in active" ?>" id="tab1">
            <pre style="text-align: left;background: #272822;" id="json-container"></pre>
        </div>
        <div class="tab-pane fade" id="tab2">
            <a href="<?= $assertion["id"] ?>"><?= $assertion["id"] ?></a>
        </div>
        <div class="tab-pane fade" id="tab3">
            <div class="col-sm-12">
                <img id="image-badge" alt="" src="<?= $assertion["image"] ?>" />
            </div>
            <button style="margin-top: 20px;" id="btn-download" class="btn btn-success">Download images</button>
            <?php if(!(isset($awardElement["badges"][(string) $badgeElement["_id"]]["byReference"]) && $awardElement["badges"][(string) $badgeElement["_id"]]["byReference"])) { ?>
                <button style="margin-top: 20px;" id="btn-rebuild" class="btn btn-success">Rebuild image</button>
                <script>
                    $("#btn-rebuild").off().on('click', (e) => {
                        $.ajax({
                            url         : baseUrl + '/' + moduleId + "/badges/rebuild",
                            data: {
                                badgeId: "<?= (string) $badgeElement["_id"] ?>",
                                awardId: "<?= (string) $awardElement["_id"] ?>",
                                awardCollection: "<?= (string) $awardElement["collection"] ?>",
                            },
                            type: 'POST'
                        }).done(function(data){
                            if(data.result){
                                const src = $("#image-badge").attr("src").split("?")[0];
                                $("#image-badge").attr("src", src + `?v=${new Date().getTime()}`);
                                toastr.success("Images rebuilded");
                            }else{
                                toastr.error("Images rebuild error");
                            }
                        });
                    })
                </script>
            <?php } ?>
        </div>
      </div>
    </div>  
</div>

<?php if(isset($awardElement["badges"][(string) $badgeElement["_id"]]["byReference"]) && $awardElement["badges"][(string) $badgeElement["_id"]]["byReference"]) { ?>
    <?php if(isset($awardElement["badges"][(string) $badgeElement["_id"]]["lastVerification"])){ ?>
        <p> <b>Dernière verification :</b> <?= $awardElement["badges"][(string) $badgeElement["_id"]]["lastVerification"]->toDateTime()->format(DateTime::COOKIE) ?></p>
    <?php } ?>
    <button class="btn btn-success" onclick="verify(event);">Reverify <i id="verif-spin" style="display: none; color:white;" class='fa fa-spin fa-spinner'></i> </button>
    <script>
        function verify(event) {
            $(event.currentTarget).attr("disabled", "true");
            $(event.currentTarget).find("#verif-spin").css("display", "inline-block");
            var form_data = new FormData();
            form_data.append("url", "<?= $assertion["id"] ?>")
            form_data.append("type", "url");
            form_data.append('contextId', "<?= (string) $awardElement["_id"] ?>");
            form_data.append('contextCollection', "<?= (string) $awardElement["collection"] ?>");
            form_data.append('badgeId', "<?= (string) $badgeElement["_id"] ?>");
            $.ajax({
                url         : baseUrl + '/' + moduleId + "/badges/validator",
                data        : form_data,
                processData : false,
                contentType : false,
                type: 'POST'
            }).done(function(data){
                if(data.result){
                    coInterface.showLoader("#details-content");
                    toastr.success("Badge valide");
                    getAjax("#details-content", baseUrl + '/co2/badges/details/element/<?= (string) $awardElement["_id"] ?>/collection/<?= (string) $awardElement["collection"] ?>/badge/<?= (string) $badgeElement["_id"] ?>', null, 'html');
                }else{
                    toastr.error("Ce badge n'est plus valide");
                    urlCtrl.loadByHash(location.hash);
                }
            });
        }
    </script>
<?php } ?>
<?php if(isset($awardElement["badges"][(string) $badgeElement["_id"]]["narative"])) { ?>
    <div class="col-sm-12">
        <h3>Narative</h3>
        <p id="narative">
            
        </p>
        <script>
            $("p#narative").html(dataHelper.markdownToHtml("<?= $awardElement["badges"][(string) $badgeElement["_id"]]["narative"] ?>"));
        </script>
    </div>
<?php } ?>

<?php if(isset($awardElement["badges"][(string) $badgeElement["_id"]]["revoke"]) && $awardElement["badges"][(string) $badgeElement["_id"]]["revoke"]){?>
    <?php if(!empty($revokeAuthor)) { ?> 
        <h3><?= Yii::t("badge", "Badge revoked by") ?></h3>
        <p><?= $revokeAuthor ?> </p>
    <?php } ?>
        <h3><?= Yii::t("badge", "Revocation reason") ?></h3>
        <p id="reason">
        </p>
        <script>
            $("p#reason").html(dataHelper.markdownToHtml("<?=$awardElement["badges"][(string) $badgeElement["_id"]]["revokeReason"] ?>"));
        </script>
<?php } ?>

<?php } ?>

<?php if(isset($awardElement["badges"][(string) $badgeElement["_id"]]["evidences"])) { ?>
    <div class="col-sm-12">
        <h3><?= Yii::t("badge","Evidences") ?></h3>
        <?php foreach($awardElement["badges"][(string) $badgeElement["_id"]]["evidences"] as $i => $evidence){?> 
        <h4><?= Yii::t("badge","Evidence") ?> <?= $i + 1 ?></h4>
        <a href="<?= $evidence["url"] ?>"></a>
        <p id="evidence-<?= $i ?>">
        </p>
        <script>
            $("p#evidence-<?= $i ?>").html(dataHelper.markdownToHtml("<?= $evidence["narative"] ?>"));
        </script>
        <?php }?>
    </div>
<?php } ?>