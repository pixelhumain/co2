<?php 
    $assets = [
        'css/endorsement.css',
    ];
    HtmlHelper::registerCssAndScriptsFiles($assets,Yii::app()->getModule('co2')->getAssetsUrl() . '/');

	if(isset(Yii::app()->session['userId']))
    $myContacts = Person::getPersonLinksByPersonId(Yii::app()->session['userId']);
    function checkExisteAndAdmin($arr, $key) {

        // is in base array?
        if (array_key_exists($key, $arr)) {
            if($arr[$key]["isAdmin"] == true){
                return true;
            }
        }
    
        // check arrays contained in this array
        foreach ($arr as $element) {
            if (is_array($element)) {
                if (checkExisteAndAdmin($element, $key)) {
                    return true;
                }
            }
            
        }
    
        return false;
    }
?>

<script>
    var isExisteEndorse = false;
</script>

<div class="endosser-header">
    <button class="btn btn-success btn-i-endorse" onclick="iEndorse()"><i class="fa fa-thumbs-o-up"></i> <?= Yii::t("badge", "I endorse it") ?></button>
    <button class="btn btn-success" onclick="endorse()"><i class="fa fa-thumbs-o-up"></i> <?= Yii::t("badge", "Endorse via another element") ?></button>
</div>
<div class="endosser-list endosser-contain">
        <?php 
          if(!empty($endorsements)){
            for ($i=0; $i < count($endorsements); $i++) { 
                $value = $endorsements[array_keys($endorsements)[$i]]; 
                $issuerId = array_keys($value["issuer"])[0]; 
                $issuer = $value["issuer"][$issuerId];
                $image = $issuer["image"] ?? $this->module->assetsUrl."/images/thumbnail-default.jpg";
        ?>

    <div class="single-menu endosser-item" style="position: relative;">
        <img src="<?= $image ?>" class="hidden-xs" alt="">
        <div class="menu-content">
            <a class="lbh-preview-element link-issuerName" href="#page.type.<?= $issuer["type"] ?>.id.<?= $issuerId ?>">
                <img src="<?= $image ?>" class="visible-xs"> <h4><?= $issuer["name"] ?></h4>
            </a>
            <div class="endosser-comment">
                <?= isset($value["endorsementComment"]) ? $value["endorsementComment"] : "" ?>
                <?php if((Yii::app()->session['userId']== $issuerId) || (checkExisteAndAdmin($myContacts, $issuerId))) { ?>

                    <script>
                        <?php
                            if(Yii::app()->session['userId'] == $issuerId) { ?>
                            isExisteEndorse = true;
                        <?php } ?>
                    </script>
                    <a class="edit-endorses" data-id="<?= (String)$value['_id'] ?>" type="button">
                        <i class="fa fa-pencil"></i>
                    </a>
                <?php } ?>
            </div>
        </div>
    </div>
    <?php }
        }else{ ?>
            <?= Yii::t("badge", "This badge doesn't have any endorsements"); ?>
        <?php } ?>
</div>

<script>
    var dynFormIEndorse = {
        "beforeBuild": {
            "properties": {
                "issuer": {
                    "inputType": "finder",
                    "label": "<?= Yii::t("badge", "Endorse as") ?>",
                    "multiple": false,
                    "initMe": true,
                    "rules": { "required": true, "lengthMin": [1] },
                    "initType": [],
                    "initBySearch": true,
                    "openSearch": true,
                    "initContacts": false,
                }
            }
        }
    };
    coInterface.bindLBHLinks();
    if(isExisteEndorse){
        $(".btn-i-endorse").hide();
    }
    $(".edit-endorses").off().on('click',function(e){
        e.preventDefault();
        let id = $(this).data('id');
        dyFObj.editElement("endorsements", id);
    });
    function endorse(){
        dyFObj.openForm("endorsement", null, {
            claimBadge:  $("#modal-endosser").data("badge"),
            claimElementId: $("#modal-endosser").data("elementId"),
            claimElementType: $("#modal-endosser").data("elementType")
        }, null, {
            "beforeBuild": {
                "properties": {
                    issuer: {
                        inputType: "finder",
                        label: "<?= Yii::t("badge", "Endorse as") ?>",
                        multiple: false,
                        initMe: false,
                        rules: { required: true, lengthMin: [1] },
                        initType: ["organizations", "projects"],
                        initBySearch: true,
                        openSearch: true,
                        initContacts: false
                    }
                }
            }
        })
    }
    function iEndorse() {
        dyFObj.openForm("endorsement", null, {
            claimBadge:  $("#modal-endosser").data("badge"),
            claimElementId: $("#modal-endosser").data("elementId"),
            claimElementType: $("#modal-endosser").data("elementType")
        }, null, dynFormIEndorse);
    }
</script>