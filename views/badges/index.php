<?php 

//assets from ph base repo
$cssJS = array(
    
    '/plugins/jquery.dynForm.js',
    
    '/plugins/jQuery-Knob/js/jquery.knob.js',
    '/plugins/jQuery-Smart-Wizard/js/jquery.smartWizard.js',
    '/plugins/jquery.dynSurvey/jquery.dynSurvey.js',

    '/plugins/jquery-validation/dist/jquery.validate.min.js',
    '/plugins/select2/select2.min.js' , 
    // '/plugins/moment/min/moment.min.js' ,
    // '/plugins/moment/min/moment-with-locales.min.js',

    // '/plugins/bootbox/bootbox.min.js' , 
    // '/plugins/blockUI/jquery.blockUI.js' , 
    
    '/plugins/bootstrap-fileupload/bootstrap-fileupload.min.js' , 
    '/plugins/bootstrap-fileupload/bootstrap-fileupload.min.css',
    '/plugins/jquery-cookieDirective/jquery.cookiesdirective.js' , 
    '/plugins/ladda-bootstrap/dist/spin.min.js' , 
    '/plugins/ladda-bootstrap/dist/ladda.min.js' , 
    '/plugins/ladda-bootstrap/dist/ladda.min.css',
    '/plugins/ladda-bootstrap/dist/ladda-themeless.min.css',
    '/plugins/animate.css/animate.min.css',
); 

HtmlHelper::registerCssAndScriptsFiles($cssJS, Yii::app()->request->baseUrl);
$cssJS = array(
    '/js/dataHelpers.js',
//    '/js/sig/geoloc.js',
//    '/js/sig/findAddressGeoPos.js',
    '/js/default/loginRegister.js'
);
HtmlHelper::registerCssAndScriptsFiles($cssJS, Yii::app()->getModule( "co2" )->getAssetsUrl() );
$cssJS = array(
'/assets/css/default/dynForm.css',
'/assets/js/comments.js',
);
HtmlHelper::registerCssAndScriptsFiles($cssJS, Yii::app()->theme->baseUrl);
 ?>
<div class="container" style="margin-top:70px;">

	<div class="col-xs-12">

		<h1 class="text-azure">All Badges ( <?php echo count(array_keys($badges)) ?> )</h1>
		Badges are based on tags, and related to possible actions.<br/> 
		They help build communities around tags. People or Organizations cary tags, and can be found in the search engine. Badges will be credentials confirmed by peers. Giving access to features and actions available on a certain thematic. <br>
		Badges can tell who you are, what you do, or want to do, find people or organizations alike.
	</div>
	<div class="col-xs-12">
		<div id="input-sec-search" class="hidden-xs col-sm-8 col-sm-offset-2  margin-top-5">
            <input type="text" style="width:80%;" class="form-control input-global-search" id="second-search-bar" placeholder="Search tags and badges by name before creating a new one ...">
            <span class="text-dark input-group-addon pull-left second-search-bar-addon" id="second-search-bar-addon">
                <i class="fa fa-search"></i>
            </span>
            <br><br><br>
			<?php if(Yii::app()->session["userId"]){ ?>
			<a class="btn btn-danger" href="javascript:dyFObj.openForm('badge')"><i class="fa fa-plus-circle "></i> Add a Badge</a> 
			<a class="btn btn-success" href="javascript:dyFObj.openForm('badge')"><i class="fa fa-plus-circle "></i> Add a Parcours</a> 
			<a class="btn btn-warning" href="javascript:;" id="filterParcours">Les Parcours</a> 
			<?php foreach ($category as $k => $v) { 
				?>
				<a class="btn btn-primary categories" data-category="<?= $v ?>" href="javascript:;"><?= $v ?></a> 
				<?php } ?>
			<a class="btn btn-default" href='javascript:;' onclick='reset()'>All</a> 
			<?php } ?>
        </div>
	</div>
	<div class="card-columns col-xs-12 padding-15 ">
		
		

		<?php foreach ($badges as $k => $v) { 
			if(!@$v["profilThumbImageUrl"]){
				
				if( empty($v["icon"]) )
					$icon = "fa-times text-red";
				 else 
					$icon = ( is_array(@$v["icon"] ) ) ? @$v["icon"][0] : $v["icon"];
			}
		?>
		<div class="badgeC card col-md-4 col-xs-6 text-center" data-id="<?php echo @$v["_id"]?>" data-tag="<?php echo @$v["tag"]?>" data-category="<?php echo @$v["category"]?>" data-is-parcours="<?php echo @$v["isParcours"]?>">
			<div class="card-body padding-15 margin-bottom-5 " style="border: 1px solid MidnightBlue;border-radius: 10px;">
				<div class="card-title bold text-dark">
					<h3 class="entityName">
						<?php if(@$v["profilThumbImageUrl"]){ ?>
						<img height="80"  src="<?php echo $v["profilThumbImageUrl"]; ?>">
						<?php } else { 
							$color = (@$v["color"]) ? $v["color"] : "black";
						?>
						<i class="fa fa-3x <?php echo $icon ?> margin-bottom-5" style="color:<?php echo $color; ?>"></i>
						<?php } ?>
						<br><?php echo @$v["name"] ?></h3>
				</div>
				<a href="/co2#search?text=#<?php echo @$v["tag"] ?>" class="lbh badge letter-red bg-white tagsContainer" ><?php echo "#".@$v["tag"] ?></a>
				<div class="margin-top-10 rounded-bottom mdb-color lighten-3 text-center pt-3">
				    <ul class="list-unstyled list-inline font-small">
				      <li class="list-inline-item pr-2"><i class="fa fa-group pr-1"> </i> <?php echo @$v["count"]?> </li>
				    </ul>
				  </div>
			</div>
		</div>
		<?php } ?>
		
	</div>
</div>
<?php 

/*
if( in_array($targetType, array(Organization::COLLECTION, Project::COLLECTION)) )  {
	    	Notification::constructNotification(ActStr::VERB_ADD, array("id" => Yii::app()->session["userId"],"name"=> Yii::app()->session["user"]["name"]), array("type"=>$targetType,"id"=> $targetId), array("id"=>$id,"type"=> $collection), $collection);
	    }
		ActivityStream::saveActivityHistory( ActStr::VERB_CREATE, $id, $collection, self::getCommonByCollection($collection), $params["name"] ) ;  
    	return $res;
*/

/*

	    // if( ){
	    // 	Notification::constructNotification(ActStr::VERB_ADD, array("id" => Yii::app()->session["userId"],"name"=> Yii::app()->session["user"]["name"]), array("type"=>$params["parentType"],"id"=> $params["parentId"]), array("id"=>(string)$params["_id"],"type"=> $collection), $collection);
	    // }


		ActivityStream::saveActivityHistory( ActStr::VERB_CREATE, $id, $collection, "organization", (@$params["name"] ) ? $params["name"] : @$params["label"] ) ;
                
    	return $res;
*/ ?>
<script type="text/javascript">

	jQuery(document).ready(function() {

		$('#second-search-bar').off().on("keyup",function() { 
			searchBadge ( $(this).val() );
	   	});

		$("a.categories").off().on("click", function() {
			const category = $(this).data("category");
			$.each( $(".badgeC") ,function (i,k) { 
              	if( $(this).data("category") == category)
                    $(this).removeClass('hide');
            	else
                    $(this).addClass('hide');
            });
		})

		$("#filterParcours").off().on("click", filterIsParcours)
		function filterIsParcours(){
			$.each( $(".badgeC") ,function (i,k) { 
              	if($(this).data("is-parcours"))
                    $(this).removeClass('hide');
            	else
                    $(this).addClass('hide');
            });
		}

		$(".badgeC").off().on("click",function (){
			window.location.href = "/co2/badges/index/tree/true/id/" + $(this).data("id")
		});
	});

	function searchBadge ( searchVal) { 
        mylog.log("searchDir searchVal",searchVal);           
        if(searchVal.length>2 ){
            $.each( $(".badgeC") ,function (i,k) { 
                      var found = null;
              if( $(this).find(".entityName").text().search( new RegExp( searchVal, "i" ) ) >= 0 || 
                  $(this).find(".tagsContainer").text().search( new RegExp( searchVal, "i" ) ) >= 0 )
                {
                  found = 1;
                }
                
                if(found)
                    $(this).removeClass('hide');
                else
                    $(this).addClass('hide');
            });
        } else
            $(".badgeC").removeClass("hide");
    }
	function reset(){
		$(".badgeC").removeClass("hide");
		return false;
	}
</script>