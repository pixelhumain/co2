  <script src="https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.13.4/underscore-min.min.js" integrity="sha512-5B2sl+/Nbe4Q1KY3csUeHRxjTPJJvCtGfNnIWVSShvIkeFhfRa54cGRkovghfyzoDNaj5cyvAGNfCEagZGikVg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

  <?php
    $cssAnsScriptFilesModule = array(    
      '/plugins/drawflow/drawflow.min.js',
      '/plugins/drawflow/drawflow.min.css',
    );
    HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule,Yii::app()->request->baseUrl);
    $cssJS = array(
      '/css/parcours-editor.css',
    );
    HtmlHelper::registerCssAndScriptsFiles($cssJS, Yii::app()->getModule( Yii::app()->params["module"]["parent"] )->getAssetsUrl() );
  ?>

  <div class="wrapper">
      <div id="drawflow">
        <div id="head-buttons">
          <button type="button" class="btn btn-add" onclick="handleAdd();">Add</button>
          <button type="button" class="btn btn-quit" onclick="quitEditor()">Quit</button>
        </div>
        <!-- <div class="btn-export" onclick="console.log({ title: 'Export',
        html: '<pre><code>'+JSON.stringify(editor.export(), null,4)+'</code></pre>'
        })">Save</div>
        <div class="btn-clear" onclick="editor.clearModuleSelected()">Clear</div>
        -->
        <div class="btn-lock">
          <i id="lock" class="fa fa-lock" onclick="editor.editor_mode='fixed'; changeMode('lock');"></i>
          <i id="unlock" class="fa fa-unlock" onclick="editor.editor_mode='edit'; changeMode('unlock');" style="display:none;"></i>
        </div>
        <div class="bar-zoom">
          <i class="fa fa-search-minus" onclick="editor.zoom_out()"></i>
          <i class="fa fa-search" onclick="editor.zoom_reset()"></i>
          <i class="fa fa-search-plus" onclick="editor.zoom_in()"></i>
        </div>
      </div>
  </div>

  <script>
    var editor;
    var isParcoursLoaded = false;
    var tree;
    var maxX = Number.NEGATIVE_INFINITY;
    const nodeSize = 80;
    $(() => {

      var id = document.getElementById("drawflow");
      editor = new Drawflow(id);
      editor.reroute = true;
      editor.reroute_fix_curvature = true;
      editor.force_first_input = false;

      editor.start();
      editor.on('nodeCreated', function(id) {
        console.log("Node created " + id);
        const heads = editor.getNodesFromName('head');
        if(heads.length > 1){
          editor.removeNodeId('node-' + id);
          console.error("YOU CAN ONLY HAVE ONE HEAD");
        }
      })
      
      
      editor.on('nodeRemoved', function(id) {
        console.log("Node removed " + id);
      })
      
      editor.on('nodeSelected', function(id) {
        console.log("Node selected " + id);
      })
      
      editor.on('moduleCreated', function(name) {
        console.log("Module Created " + name);
      })
      
      editor.on('moduleChanged', function(name) {
        console.log("Module Changed " + name);
    })
    
    editor.on('connectionCreated', function(connection) {
      console.log('Connection created');
      console.log(connection);
      const connections = editor.drawflow.drawflow.Home.data[connection.input_id].inputs[connection.input_class].connections;
      if(connections.length > 1){
        toastr.error("Vous ne pouvez pas creer cette relation");
        editor.removeSingleConnection(connection.output_id, connection.input_id, connection.output_class, connection.input_class);
      }else if(isParcoursLoaded){
        //CHECK IF NORMAL ADD
        ajaxPost(null, baseUrl + '/' + moduleId + '/badges/parcoursadder', {
          badge: connection.input_id,
          parent: connection.output_id
        }, (data) => {
          if(data.result){
            toastr.success("Relation ajoutée");
            window.parcoursChanged = true;
          }else{
            toastr.error("Erreur lors de l'ajout de la relation");
          }
        });
      }
    })
    
    editor.on('connectionRemoved', function(connection) {
      console.log('Connection removed');
      //CHECK IF NORMAL ADD
      ajaxPost(null, baseUrl + '/' + moduleId + '/badges/parcoursdeleter', {
          child: connection.input_id,
          parent: connection.output_id
        }, (data) => {
          if(data.result){
            toastr.success("Relation supprimé");
            window.parcoursChanged = true;
          }else{
            toastr.error("Erreur lors de la suppression de la relation");
          }
        });

      console.log(connection);
    })
    editor.on('nodeMoved', function(id) {
      console.log("Node moved " + id);
    })
    
    editor.on('zoom', function(zoom) {
      console.log('Zoom level ' + zoom);
    })
    
    editor.on('translate', function(position) {
      console.log('Translate x:' + position.x + ' y:'+ position.y);
    })
    
    editor.on('addReroute', function(id) {
      console.log("Reroute added " + id);
    })
    
    editor.on('removeReroute', function(id) {
      console.log("Reroute removed " + id);
    })
    /* LOAD PARCOURS TREE */
    tree = <?= json_encode($tree) ?>;
    tree = d3.hierarchy(tree);
    tree.x = 500;
    tree.y = 500;
    d3.tree()
      .size([2000, 2000])
      .nodeSize([120, 120])(tree);
    const descendants = tree.descendants();
    setTimeout(() => {
      const head = descendants.shift();
      const x = head.y + 100;
      if(x > maxX){
        maxX = x;
      }
      addNodeToDrawFlow(head.data, x, head.x + editor.precanvas.clientHeight / 2, true);
      for(const descendant of  descendants){
        const x = descendant.y + 100;
        if(x > maxX){
          maxX = x;
        }
        addNodeToDrawFlow(descendant.data, x, descendant.x + editor.precanvas.clientHeight / 2);
        for(const parent of Object.keys(descendant.data.parent)){
          editor.addConnection(parent, descendant.data.id,"output_1" , "input_1");
        }
      }
      isParcoursLoaded = true;
    }, 200)
  })
  function addNodeToDrawFlow(data, pos_x, pos_y, isHead = false) {
      if(editor.editor_mode === 'fixed') {
        return false;
      }
      console.log(data);
      pos_x = pos_x * ( editor.precanvas.clientWidth / (editor.precanvas.clientWidth * editor.zoom)) - (editor.precanvas.getBoundingClientRect().x * ( editor.precanvas.clientWidth / (editor.precanvas.clientWidth * editor.zoom)));
      pos_y = pos_y * ( editor.precanvas.clientHeight / (editor.precanvas.clientHeight * editor.zoom)) - (editor.precanvas.getBoundingClientRect().y * ( editor.precanvas.clientHeight / (editor.precanvas.clientHeight * editor.zoom)));
      if(isHead){
        var head = `
        <div style="height: 100%; width: 100%; display: flex; justify-content: center; align-items: center; position: relative; padding: 10px; aspect-ratio: 1"><img src="${data.profilMediumImageUrl}" style="height: 90%; width: 90%; pointer-events: none;"><div class="badge-graph-label" style="position: absolute; top: 100%; width: 100%; text-align: center;">${data.name}</div><button class="button-delete-badge-parcours btn btn-danger" style="position: absolute; top: -10px; display: none; height: 30px; width: 30px; padding: 3px 5px;"><i class="fa fa-trash" style="pointer-events: none;"></i></button></div>
        `;
        editor.addNode(data.id, 'head', 0, 1, pos_x, pos_y, 'head', {}, head );
      }else{
        var node = `
        <div style="height: 100%; width: 100%; display: flex; justify-content: center; align-items: center; position: relative; padding: 10px; aspect-ratio: 1"><img src="${data.profilMediumImageUrl}" style="height: 90%; width: 90%; pointer-events: none;"><div class="badge-graph-label" style="position: absolute; top: 100%; width: 100%; text-align: center;">${data.name}</div><button class="button-delete-badge-parcours btn btn-danger" style="position: absolute; top: -10px; display: none; height: 30px; width: 30px; padding: 3px 5px;"><i class="fa fa-trash" style="pointer-events: none;"></i></button></div>
        `;
        editor.addNode(data.id, 'node', 1, 1, pos_x, pos_y, 'node', {}, node );
      }
    }
    
    var transform = '';
    function showpopup(e) {
      e.target.closest(".drawflow-node").style.zIndex = "9999";
      e.target.children[0].style.display = "block";
      transform = editor.precanvas.style.transform;
      editor.precanvas.style.transform = '';
      editor.precanvas.style.left = editor.canvas_x +'px';
      editor.precanvas.style.top = editor.canvas_y +'px';
      editor.editor_mode = "fixed";
      
    }
    
    function closemodal(e) {
      e.target.closest(".drawflow-node").style.zIndex = "2";
      e.target.parentElement.parentElement.style.display  ="none";
      editor.precanvas.style.transform = transform;
      editor.precanvas.style.left = '0px';
      editor.precanvas.style.top = '0px';
      editor.editor_mode = "edit";
    }
    
    function changeModule(event) {
      var all = document.querySelectorAll(".menu ul li");
      for (var i = 0; i < all.length; i++) {
        all[i].classList.remove('selected');
      }
      event.target.classList.add('selected');
    }
    
    function changeMode(option) {
      
      //console.log(lock.id);
      if(option == 'lock') {
        lock.style.display = 'none';
        unlock.style.display = 'block';
      } else {
        lock.style.display = 'block';
        unlock.style.display = 'none';
      }
      
    }
    function handleAdd(){
      bootbox.dialog({ 
        title: "<?php echo Yii::t("badge", "Badge adding mode") ?>",
        message: "<?php echo Yii::t("badge", "Existing badge or new badge") ?>",
        size: 'large',
        onEscape: true,
        backdrop: true,
        buttons: {
            existing: {
                label: "<?php echo Yii::t("badge", "Existing") ?>",
                className: 'btn-primary',
                callback: function(){
                    mylog.log("parcours existing")   
                    const jsonSchemaParcours = {
                      jsonSchema : {
                        title : tradBadge.addExistingBadge,
                        icon : " bookmark-o",
                        type : "object",
                        onLoads : {
                          onload : function(data){
                            dyFInputs.setSub("bg-azure");
                                  $(".parentfinder").hide();
                          }
                        },
                          save: function(formData){
                              // LOAD FULL TREE
                              loadTreeAsync(Object.keys(formData.badge)[0])
                              $("#ajax-modal").modal("hide");
                          },
                          afterSave : function(data,callB){
                              // dyFObj.commonAfterSave(data, callB);
                              
                              mylog.log("afterSave PARCOURS BADGE", data);
                          },
                        properties : {
                              badge : {
                                  inputType : "finder",
                                  label : tradBadge.addBadge,
                                  multiple: false,
                                  initMe:false,
                                  placeholder:tradBadge.addBadge,
                                  rules : { required : true}, 
                                  initType: ["badges"],
                                  initBySearch: true,
                                  openSearch :true,
                                  initContacts: false,
                                  initContext: false,
                                  filters: {}
                              }
                        }
                    }
                  };
                  jsonSchemaParcours.jsonSchema.properties.badge.filters['_id'] = { $nin : Object.keys(editor.drawflow.drawflow.Home.data) };
                  console.log(jsonSchemaParcours.jsonSchema.properties, "json_decode")
                  dyFObj.openForm(jsonSchemaParcours); 
                }
            },
            new: {
                label: "<?php echo Yii::t("badge", "New") ?>",
                className: 'btn-info',
                callback: function(){
                    mylog.log("parcours new")
                    var dyfBadge={
                        "beforeBuild":{
                          "properties":{
                            "parent" : {
                              "values": {
                                        }
                            }, 
                                    "isParcours": {
                                        "checked": true,
                                        params: {
                                            onText:"Text On", 
                                            offText:"Text Off", 
                                            onLabel : "Label On",
                                            offLabel: "Label Off",
                                        }
                                    }
                          }
                        },
                            afterSave: (data) => {
                                dyFObj.commonAfterSave(data, () => {
                                    urlCtrl.loadByHash(location.hash);
                                });
                            }
                      };
                    // dyfBadge.beforeBuild.properties.parent.values[d.parent.data.id] = {};
                    // dyfBadge.beforeBuild.properties.parent.values[d.parent.data.id]["name"] = d.parent.data.name;
                    // dyfBadge.beforeBuild.properties.parent.values[d.parent.data.id]["type"] = "badges";
                    dyFObj.openForm("badge", null, null, null, dyfBadge);
                }
            }
        }
      })
    }
    function quitEditor(){
        $('#parcours-edit-modal').modal("hide");
        $("#parcours-editor-container").html("");
        if(window.parcoursChanged){
          var hash = getHash();
          const str = '.views.';
          var index = hash.indexOf(str);
          const view = "parcours"
          loadView(view,index);
          delete window.parcoursChanged;
        }
    }
    function loadTreeAsync(id){
      if(!id) return;
      getAjax(null, baseUrl + '/' + moduleId + '/badges/parcoursedit/badge/' + id +'/format/json', (data) => {
        console.log(data);
        isParcoursLoaded = false;
        let tree = data.tree;
        tree = d3.hierarchy(tree);
        tree.x = 1000;
        tree.y = 1000;
        d3.tree()
          .size([2000, 2000])
          .nodeSize([120, 120])(tree);
        const descendants = tree.descendants();
        const offset = maxX + nodeSize + 20;
        setTimeout(() => {
          const head = descendants.shift();
          const x = head.y + offset;
          if(x > maxX){
            maxX = x;
          }
          addNodeToDrawFlow(head.data, x, head.x + editor.precanvas.clientHeight / 2, false);
          for(const descendant of  descendants){
            addNodeToDrawFlow(descendant.data, descendant.y + offset, descendant.x + editor.precanvas.clientHeight / 2);
            for(const parent of Object.keys(descendant.data.parent)){
              editor.addConnection(parent, descendant.data.id,"output_1" , "input_1");
            }
          }
          isParcoursLoaded = true;
        }, 200)
      })
    }
    </script>
    