    <div class="row">
        <div id="graph-container" class="col-xs-12">

        </div>
    </div>
<style>
    pre {outline: 1px solid #ccc; padding: 5px; margin: 25px; }
.string { color: green; }
.number { color: darkorange; }
.boolean { color: blue; }
.null { color: magenta; }
.key { color: red; }
</style>

<!--
<pre id="res">

</pre>
 -->

<?php 
$graphAssets = [
    '/js/graph.js', '/css/graph.css'
  ];
  HtmlHelper::registerCssAndScriptsFiles(
    $graphAssets,
    Yii::app()->request->baseUrl . Yii::app()->getModule("graph")->getAssetsUrl()
  );
  if($badgeAssigned == null){
      $badges = Badge::getBadgeChildrenTree($badge["_id"]);
  }else{
      $badges = Badge::getBadgeChildrenTree($badge["_id"], $badgeAssigned);
  }
?>

<script>
    var data;
    $(function() {
        data = <?= json_encode($badges) ?>;
            var graph = new BadgeGraph(data, -1, {id: "root", label: "<?= Yii::t("common", "Community")?>"}, false);
            graph.setOnClickNode((e,d) => {
            console.log(d.data.id);
            var dyfBadge={
					"beforeBuild":{
						"properties":{
							"parent" : {
								"values": {
                                }
							}, 
                            "isParcours": {
                                "checked": true,
                                params: {
                                    onText:"Text On", 
                                    offText:"Text Off", 
                                    onLabel : "Label On",
                                    offLabel: "Label Off",
                                }
                            }
						}
					}
				};
            dyfBadge.beforeBuild.properties.parent.values[d.parent.data.id] = {};
            dyfBadge.beforeBuild.properties.parent.values[d.parent.data.id]["name"] = d.parent.data.name;
            dyfBadge.beforeBuild.properties.parent.values[d.parent.data.id]["type"] = "badges";
            dyFObj.openForm("badge", null, null, null, dyfBadge);
        })
        graph.setLabelFunc((d,i,n) => {
			return d.data.name;
		})
        graph.draw("#graph-container");
        function syntaxHighlight(json) {
            if (typeof json != 'string') {
                json = JSON.stringify(json, undefined, 2);
            }
            json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
            return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
                var cls = 'number';
                if (/^"/.test(match)) {
                    if (/:$/.test(match)) {
                        cls = 'key';
                    } else {
                        cls = 'string';
                    }
                } else if (/true|false/.test(match)) {
                    cls = 'boolean';
                } else if (/null/.test(match)) {
                    cls = 'null';
                }
                return '<span class="' + cls + '">' + match + '</span>';
            });
        }
        // document.querySelector("#res").innerHTML = syntaxHighlight(data);
    })
</script>