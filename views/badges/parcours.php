<div class="container">
    <div class="row">
        <div id="graph-container" class="col-xs-12">
            <?php if(Authorisation::badgeIsAdminEmetteur($element['_id'])){?>
                <button class="btn btn-sm btn-success" id="btn-edit"><i class="fa fa-edit"></i></button>
            <?php } ?>
        </div>
    </div>
</div>



<div class="portfolio-modal modal fade in" id="parcours-edit-modal" tabindex="-1" role="dialog" aria-hidden="false" style="padding-left: 0px; top: 56px;z-index: 20000">
    <div class="modal-content padding-top-15">
    <div id="parcours-editor-container" style="display: flex; justify-content:center; align-items: center; height: calc(100vh - 56px); width: 100vw; top: 56px; left: 0px; position: fixed; background-color: white; z-index: 8;"></div>
    </div>
</div>


<style>
    #graph-container button#btn-edit{
        position: absolute;
        top: 10px;
        right: 10px;
    }
</style>

<?php 
$graphAssets = [
    '/plugins/d3/d3.v6.min.js', '/js/graph.js', '/css/graph.css'
  ];
  HtmlHelper::registerCssAndScriptsFiles(
    $graphAssets,
    Yii::app()->request->baseUrl . Yii::app()->getModule("graph")->getAssetsUrl()
  );
  $badges = Badge::getBadgeChildrenTree($element["_id"]);
?>
<script>
    <?php if(Authorisation::badgeIsAdminEmetteur($element['_id'])){?>
        const button = document.querySelector('#btn-edit');
        button.addEventListener('click', (e) => {
            e.stopPropagation();
            e.preventDefault();
            $('#parcours-edit-modal').modal("show");
            coInterface.showLoader("#parcours-editor-container");
            getAjax("#parcours-editor-container", baseUrl + '/co2/badges/parcoursedit/badge/' + contextId, null, 'html');
        })        
    <?php } ?>

    $(function() {
        var data = <?= json_encode($badges) ?>;
        var graph = new BadgeGraph(data, -1, {id: "root", label: "<?= Yii::t("common", "Community")?>"}, false);
        graph.setOnLabelClick((e,d) => {
            e.stopPropagation();
            e.preventDefault();
            urlCtrl.loadByHash("#page.type.badges.id." + d.data.id);
        })
        graph.setLabelFunc((d,i,n) => {
			return d.data.name;
		})
        graph.draw("#graph-container");
    })
</script>