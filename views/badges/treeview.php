<?php 

//assets from ph base repo
$cssJS = array(
    
    '/plugins/jquery.dynForm.js',
    
    '/plugins/jQuery-Knob/js/jquery.knob.js',
    '/plugins/jQuery-Smart-Wizard/js/jquery.smartWizard.js',
    '/plugins/jquery.dynSurvey/jquery.dynSurvey.js',

    '/plugins/jquery-validation/dist/jquery.validate.min.js',
    '/plugins/select2/select2.min.js' , 
    // '/plugins/moment/min/moment.min.js' ,
    // '/plugins/moment/min/moment-with-locales.min.js',

    // '/plugins/bootbox/bootbox.min.js' , 
    // '/plugins/blockUI/jquery.blockUI.js' , 
    
    '/plugins/bootstrap-fileupload/bootstrap-fileupload.min.js' , 
    '/plugins/bootstrap-fileupload/bootstrap-fileupload.min.css',
    '/plugins/jquery-cookieDirective/jquery.cookiesdirective.js' , 
    '/plugins/ladda-bootstrap/dist/spin.min.js' , 
    '/plugins/ladda-bootstrap/dist/ladda.min.js' , 
    '/plugins/ladda-bootstrap/dist/ladda.min.css',
    '/plugins/ladda-bootstrap/dist/ladda-themeless.min.css',
    '/plugins/animate.css/animate.min.css',
); 

HtmlHelper::registerCssAndScriptsFiles($cssJS, Yii::app()->request->baseUrl);
$cssJS = array(
    '/js/dataHelpers.js',
//    '/js/sig/geoloc.js',
//    '/js/sig/findAddressGeoPos.js',
    '/js/default/loginRegister.js'
);
HtmlHelper::registerCssAndScriptsFiles($cssJS, Yii::app()->getModule( "co2" )->getAssetsUrl() );
$cssJS = array(
'/assets/css/default/dynForm.css',
'/assets/js/comments.js',
);
HtmlHelper::registerCssAndScriptsFiles($cssJS, Yii::app()->theme->baseUrl);
 ?>

<?php 
$graphAssets = [
    '/plugins/d3/d3.v6.min.js', '/js/graph.js', '/css/graph.css'
  ];
  HtmlHelper::registerCssAndScriptsFiles(
    $graphAssets,
    Yii::app()->request->baseUrl . Yii::app()->getModule("graph")->getAssetsUrl()
  );
?>

<style>
    pre {outline: 1px solid #ccc; padding: 5px; margin: 25px; }
.string { color: green; }
.number { color: darkorange; }
.boolean { color: blue; }
.null { color: magenta; }
.key { color: red; }
</style>

<div id="graph-container">

</div>

<pre id="res">

</pre>



<script>
    function init() {
        var data = <?= json_encode($badges) ?>;

        var graph = new BadgeGraph(data, -1, {id: "root", label: "<?= Yii::t("common", "Community")?>"}, true);
        graph.setLabelFunc((d,i,n) => {
			return d.data.name;
		})
    graph.setOnClickNode((e,d) => {
            console.log(d.data.id);
            var dyfBadge={
					"beforeBuild":{
						"properties":{
							"parent" : {
								"values": {
                                }
							}, 
                            "isParcours": {
                                "checked": true,
                                params: {
                                    onText:"Text On", 
                                    offText:"Text Off", 
                                    onLabel : "Label On",
                                    offLabel: "Label Off",
                                }
                            }
						}
					}
				};
            dyfBadge.beforeBuild.properties.parent.values[d.parent.data.id] = {};
            dyfBadge.beforeBuild.properties.parent.values[d.parent.data.id]["name"] = d.parent.data.name;
            dyfBadge.beforeBuild.properties.parent.values[d.parent.data.id]["type"] = "badges";
            dyFObj.openForm("badge", null, null, null, dyfBadge);
        })
        graph.draw("#graph-container");
        function syntaxHighlight(json) {
            if (typeof json != 'string') {
                json = JSON.stringify(json, undefined, 2);
            }
            json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
            return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
                var cls = 'number';
                if (/^"/.test(match)) {
                    if (/:$/.test(match)) {
                        cls = 'key';
                    } else {
                        cls = 'string';
                    }
                } else if (/true|false/.test(match)) {
                    cls = 'boolean';
                } else if (/null/.test(match)) {
                    cls = 'null';
                }
                return '<span class="' + cls + '">' + match + '</span>';
            });
        }
        document.querySelector("#res").innerHTML = syntaxHighlight(data);
    }
    $(init)
</script>