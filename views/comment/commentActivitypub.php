<style>
    .textarea-new-comment{
		max-width: 100%;
		min-width: 100%;
		vertical-align: top;
		font-size:13px;
	}
	.textarea-new-comment:focus {
		outline-style: solid;
		outline-width: 1px;
		outline-color: grey;
	}
	.footer-comments{
        margin-right: -10px;
        margin-left: -10px;
        margin-top: -5px;
        padding: 10px;
        background-color: rgba(231, 231, 231, 0.62);
	}
	.content-comment{
		max-width:85%;
		min-height: 35px;
	}
	.comment-container-white{
		background-color: rgba(231, 231, 231, 0.62);
    	border-radius: 35px;
    	padding: 7px 20px;
    	margin-bottom: 5px;
	}
	.answerCommentContainer {
	    margin-left: 45px;
	    margin-top: 5px;
	}

	.ctnr-txtarea{
		position: absolute;
		right:10px;
		left:50px;
	}

	.answerCommentContainer .ctnr-txtarea{
		left:40px!important;
	}

	.content-comment .tool-action-comment{
		display: none;
	}
	.content-comment:hover .tool-action-comment{
		display: inherit;
	}
	.content-comment .fa-reply{
		font-size:14px;
		margin-right:5px;
		margin-left:5px;

	}
	.text-comment{
		word-wrap: break-word;
		white-space: pre-line;
		width: 100%;
	}
    .text-comment p{
        font-size: 18px !important;
    }
	.content-new-comment .mentions{
		padding: 10px !important;
    	font-size: 13px !important;
	}
	.content-update-comment .mentions{
		padding: 10px !important;
    	font-size: 14px !important;
	}
	.date-com{
		font-size: 12px;
	}
</style>
<?php

use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\Config;

	$userConnected = $_SESSION["user"];
?>

<div class="footer-comments row">
    <div class="col-md-12 col-sm-12 col-xs-12 no-padding margin-top-15 container-txtarea">
        <img src="<?= @$userConnected["profilImageUrl"] ?>"
            class="img-responsive pull-left img-circle" style="margin-right:6px;height:32px; width:32px;">

        <div class="content-new-comment" style="height: 34px;">
            <div class="ctnr-txtarea">
                <div class="mentions-input-box">
                    <div class="mentions">
                        <div></div>
                    </div><textarea rows="1"
						id="textarea-comment-<?= $idComment ?>"
                        style="height: 52px; overflow: hidden; overflow-wrap: break-word; resize: horizontal;"
                        class="form-control"
                        placeholder="Votre commentaire..." data-mentions-input="true"></textarea>
                    <div
                        style="position: absolute; display: none; overflow-wrap: break-word; white-space: pre-wrap; border-color: rgb(220, 220, 220); border-style: solid; border-width: 1px; font-weight: 400; width: 490px; font-family: Lato, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; line-height: 18.5714px; font-size: 13px; padding: 9px;">
                        &nbsp;</div>
                    <div class="mentions-autocomplete-list" style="display: none;"></div>
                </div>
                <input type="hidden" id="argval" value="">
            </div>
        </div>
    </div>

    <div>
        <?php
			foreach($comments as $comment){
				$isExternalAuthor = parse_url($comment["author"]["id"], PHP_URL_HOST) !== Config::HOST();
		?>
        <div class="col-xs-12 no-padding margin-top-5 item-comment  bg-white-comment" id="item-comment-624d2c6753bb4c78120d8214">
            <img src="<?= @$comment["author"]["avatar"] ?>" class="img-responsive pull-left img-circle" style="margin-right:5px; margin-top:10px; height:32px;width: 32px;">
            <span class="pull-left content-comment col-xs-12 no-padding">
                <span class="text-black pull-left col-xs-12 comment-container-white">
                    <div style="display: flex; flex-direction:column;">
						<a href="<?= $comment["author"]["id"] ?>" class="text-dark pull-left" style="width:fit-content;">
							<strong><?= $comment["author"]["name"] ?></strong>
						</a>
						<?php if($isExternalAuthor){ ?>
						<span style="font-size: 14px;color:#74797d; line-height: 1;margin-bottom:10px;"><?= $comment["author"]["address"] ?></span>
						<?php } ?>
					</div>
                    <span class="pull-right date-com">
                        <div class="date-updated-comment" data-updated="<?= $comment["created"]->sec ?>"></div>
                    </span>
                    <span class="text-comment text-left pull-left"><?= $comment["content"] ?></span>
                </span>
            </span>
        </div>
        <?php } ?>
    </div>
</div>

<script>
	var parentId = "<?= $idComment ?>";
    $(function(){
        $(".date-updated-comment").each(function(){
            $(this).html(directory.showDatetimePost(null, null, $(this).data("updated")))
        })

		var shiftKeyDown = false
		$("#textarea-comment-<?= $idComment ?>").keydown(function(e){
			var text = $(this).val();
			var isEnterKey = e.keyCode === 13,
				isNotEmpty = text.replaceAll([" ", "\n"], "") != "";

			if(e.keyCode == 16)
				shiftKeyDown = true;

			if(isEnterKey && isNotEmpty && !shiftKeyDown){
				$.post(`${baseUrl}/api/activitypub/comment`, {
					parentId:parentId,
					comment:text
				}, function(){
					showCommentsTools(parentId, "activitypub");
				})
				return false;
			}

		}).keyup(function(e){
			if(e.keyCode == 16)
				shiftKeyDown = false;
		})
    })
</script>
