<style>
    .discussions-content {
        min-height: 80vh;
        display: flex;
    }
    .discussions-content .panel-left-content {
        padding-left: 0;
        padding-right: 0;
    }
    ul.discuList {
        position: relative;
        overflow-y: auto;
        -moz-box-shadow: 0px 0px 3px -1px #656565;
        -webkit-box-shadow: 0px 0px 3px -1px #656565;
        -o-box-shadow: 0px 0px 3px -1px #656565;
        box-shadow: 0px 0px 3px -1px #656565;
        overflow-x: hidden;
        padding: 5px;
    }
    .panel-left li.discuLi {
        min-height: 50px;
        padding: 10px;
        background-color: rgba(231, 231, 231, 0.62);
        border-radius: 10px;
        margin-bottom: 5px;
        display: flex;
        align-items: center;
    }

    .panel-left li.discuLi.non-lus {
        background-color: rgba(197, 225, 197, 0.3);
        font-weight: 800;
        box-shadow: rgba(0, 0, 0, 0.1) 0px 2px 3px;
    }

    .discussions-content .panel-left li.discuLi[data-notseen]::after {
        content: attr(data-notseen);
        position: absolute;
        top: 7px;
        right: 5px;
        background-color: red;
        color: white;
        border-radius: 50%;
        padding: 1px 4px;
        font-size: 12px;
        font-weight: bold;
    }
    .panel-left a.question {
        padding-top: 0px !important;
        background-color: transparent;
        color: #354535;
        font-size: 13px;
        font-weight: 100;
        text-align: left;
    }
    .panel-left .non-lus a.question .message {
        font-size: 13px;
        font-weight: 800;
    }
    .panel-left .questions-list a .label {
        display: flex;
        opacity: 0.7;
        font-size: 20px !important;
        border-radius: 30px !important;
        height: 40px;
        margin-top: 5px;
        height: 35px;
        width: 35px;
        margin-left: -1px;
        background-color: #56c557;
        align-content: center;
        justify-content: center;
        align-items: center;
    }
    .panel-left .message, .panel-left .time {
        width: 100%;
    }
    .discussions-content .panel-left,
    .discussions-content .panel-right {
        flex-grow: 1;
        border: solid 1px #ddd;
        bottom: 1rem;
    }
    .discussions-content .panel-left {
        border-start-start-radius: 8px;
        border-end-start-radius: 8px;
    }
    .discussions-content .panel-right {
        border-start-end-radius: 8px;
        border-end-end-radius: 8px;
    }
    .discussions-content .title-panel {
        display: block;
        width: 100%;
        padding: 5px 10px;
        /* margin-bottom: 1.5rem; */
        border-bottom: solid 1px #ddd;
    }
    .discussions-content .title-panel h4 {
        text-transform: none !important;
    }
    .discussions-content .content-header {
        display: block;
        width: 100%;
        padding: 5px 10px;
        margin-bottom: .5rem;
        border-bottom: solid 1px #ddd;
    }
    .discussions-content .panel-left li.discuLi.active {
        background-color: #56c557;
    }
    .discussions-content .panel-left li.discuLi.active a.question {
        color: #ffff;
        font-weight: 600;
    }
    .discussions-content .comments-content .content-header {
        margin-bottom: 1.5rem;
    }
    .discussions-content .content-header h4 {
        text-transform: none !important;
        margin: 0;
    }
    .discussions-content .panel-left .questions-list {
        max-height: 60vh;
        overflow-y: auto;
    }
    .discussions-content .answer-content .content-body {
        max-height: 10vh;
        overflow-y: auto;
        font-size: 1.5rem;
    }
    .discussions-content .answer-content .content-body.xl-height {
        max-height: 20vh;
    }
    .discussions-content .comments-content .content-body {
        max-height: 50vh;
        overflow-y: auto;
    }
    .discussions-content .comments-content .content-body.with-question-comment {
        max-height: 60vh;
    }
    .discussions-content .panel-right .answer-content .content-body .coforminput.questionBlock .form-group label h4,
    .discussions-content .panel-right .answer-content .content-body .coforminput.questionBlock label h4,
    .discussions-content .panel-right .answer-content .content-body .coforminput.questionBlock .funding_header h4 {
        display: none !important;
    }
    .discussions-content .panel-right .answer-content .content-body .empty-answer {
        font-size: 16px;
        font-weight: 600;
        color: #8b8b8b;
    }
    .discussions-content .panel-right .panel-right-title {
        width: 100%;
        gap: 1rem;
        padding-right: 7%;
    }
    .discussions-content .panel-right-loader {
        position: absolute;
        width: 100%;
        height: 72vh;
        background-color: #ffff;
        bottom: 0;
        top: 0;
        left: 0;
        right: 0;
        z-index: 10;
        display: flex;
        justify-content: center;
        align-items: center;
    }
    .discussions-content .proposal-container {
        padding: 15px;
        border-radius: 16px;
        box-shadow: 0 0 10px rgba(0, 0, 0, .2);
        display: flex;
        flex-direction: row;
        gap: 10px;
        flex-wrap: wrap;
        background-color: white;
    }
    .discussions-content .proposal-container .proposal-image-container {
        border-radius: 8px;
        overflow: hidden;
        width: 14rem;
        height: auto;
        max-height: 15rem;
        position: relative;
    }
    .discussions-content .proposal-image-container img {
        width: 100%;
        height: 100%;
        object-fit: cover;
    }
    .discussions-content .proposal-container .proposal-info-container {
        flex: 1;
    }
    .discussions-content .proposal-info-container .proposal-name {
        text-transform: none !important;
        font-size: 19px !important;
        margin-top: 0 !important;
    }
    .discussions-content .proposal-info-container .depositor-image {
        display: inline-block;
        overflow: hidden;
        width: 35px;
        height: 35px;
        border-radius: 50%;
        vertical-align: middle;
    }
    .discussions-content .item-comment.highlight-this, 
    .discussions-content .lblComment.has-newanswer {
        border: solid 2px #00d75ea1;
    }
    .discussions-content .lblComment.has-newanswer {
        padding: 2px 3px;
    }
    .move-mt-1 {
        margin-top: -1.5rem !important;
    }
    .modal-custom .close-modal {
        top: 2.5rem;
    }
    .discussions-content .refresh-comment-content {
        position: absolute;
        display: block;
        text-align: center;
        font-size: 1.5rem;
        background-color: #293c51;
        color: #ffff;
        font-weight: 600;
        border-radius: 6px;
        left: 50%;
        top: 8%;
        transform: translate(-50%, -50%);
        box-shadow: 1px 2px 6px rgba(0, 0, 0, .5);
        z-index: 10;
        cursor: pointer;
        padding: 4px 8px;
        transition: all .3s;
    }
    .discussions-content .refresh-comment-content:hover {
        background-color: #1f2b38;
    }
    .discussions-content .connected-users {
        display: block;
        text-align: right;
        white-space: nowrap;
        position: relative;
        width: 90%;
        right: 0%;
        overflow-x: auto;
        max-width: 90%;
    }
    .discussions-content .connected-users .user-bulle {
        display: inline-block;
        cursor: default !important;
        width: 3.5rem;
        min-width: 3.5rem;
        height: 3.5rem;
        border-radius: 50px;
        text-align: center;
        align-content: center;
        background-color: #f0f0f0;
    }
    .discussions-content .connected-users .user-bulle img {
        width: 35px;
        height: 35px;
        border-radius: 50%;
        margin: 0px;
        padding: 1px;
        object-fit: cover;
        margin-top: 0px;
        transition: all .3s;
    }
    .discussions-content .connected-users .user-bulle.writing img {
        padding: 0px 0px !important;
        border: #56c557 3px solid !important;
    }
    .discussions-content .rubric-connected-users {
        display: flex;
        gap: .2rem;
        position: absolute;
        bottom: 0;
        right: .5rem;
        width: 40%;
        justify-content: end;
    }
    .discussions-content .rubric-connected-users .user-bulle {
        display: block;
        padding: 0px;
        border-radius: 50px;
        text-align: center;
        align-content: center;
        background-color: #f0f0f0;
        width: 18px;
        height: 18px;
    }
    .discussions-content .rubric-connected-users .user-bulle img {
        width: 18px;
        height: 18px;
        border-radius: 50%;
        margin: 0px;
        padding: 0px;
        object-fit: cover;
        margin-top: 0px;
        transition: all .3s;
        top: -.5rem;
        position: relative;
    }
    .dis-flex {
        display: flex !important;
    }
    .discussions-content .panel-left-content .panel-left-action {
        justify-content: center;
    }
    .discussions-content .panel-left .panel-left-loader {
        position: absolute;
        top: 0;
        bottom: 0;
        background-color: #ffff;
        z-index: 1010;
        display: none;
        transition: all .3s;
    }
    .discussions-content .panel-left .new-discussion-form {
        position: absolute;
        top: 0;
        bottom: 0;
        background-color: #ffff;
        z-index: 1000;
        display: none;
        transition: all .3s;
    }
    .discussions-content .discussion-button {
        text-decoration: none;
        line-height: 1;
        border-radius: 1.5rem;
        overflow: hidden;
        position: relative;
        box-shadow: 10px 10px 20px rgba(0,0,0,.05);
        background-color: #fff;
        color: #121212;
        border: none;
        cursor: pointer;
    }

    .discussions-content .button-decor {
        position: absolute;
        inset: 0;
        background-color: var(--clr);
        transform: translateX(-100%);
        transition: transform .3s;
        z-index: 0;
    }

    .discussions-content .button-content {
        display: flex;
        align-items: center;
        font-weight: 600;
        position: relative;
        overflow: hidden;
    }

    .discussions-content .button__icon {
        width: 48px;
        height: 40px;
        background-color: var(--clr);
        display: grid;
        place-items: center;
    }

    .discussions-content .button__text {
        display: inline-block;
        transition: color .2s;
        padding: 2px 1.5rem 2px;
        padding-left: .75rem;
        overflow: hidden;
        white-space: nowrap;
        text-overflow: ellipsis;
        max-width: 85%;
    }

    .discussions-content .discussion-button:hover .button__text {
        color: #fff;
    }

    .discussions-content .discussion-button:hover .button-decor {
        transform: translate(0);
    }
    .discussions-content .panel-left .new-discussion-form .panel-left-title {
        align-items: center;
        gap: .5rem;
    }
    .discussions-content .new-discussion-form .hideNewDiscuForm {
        right: 2%;
        position: absolute;
    }
    .discussions-content .discussion-category-icon {
        position: absolute;
        left: 0;
        margin-top: -2.8rem;
        transition: all .3s;
    }
    .discussions-content .discuLi.active .discussion-category-icon {
        left: -0.4rem;
        margin-top: -4.1rem;
        background-color: #ffffff;
        border-radius: 10px;
        padding: 0px 5px;
        box-shadow: 1px 2px 5px 1px rgb(0 0 0 / 36%);
    }
    .discussions-content .btn-action-container {
        position: absolute;
        opacity: 0;
        transition: all .6s;
        display: none;
    }

    .discussions-content .discuLi:hover .btn-action-container {
        display: flex;
        right: 20%;
        opacity: 1;
        padding: 5px 5px;
        border-radius: 8px;
        box-shadow: rgb(0 0 0 / 39%) 1px 1px 5px;
        z-index: 10;
        background-color: #eeeeee;
    }

    @media screen and (max-width: 767px) {
        .discussions-content .panel-left {
            display: none;
        }
        .discussions-content .panel-right {
            border-radius: 0;
        }
        .discussions-content .panel-right .answer-content .content-body {
            max-height: 30vh;
        }
        .discussions-content .comments-content .content-body {
            max-height: 40vh;
        }

        .discussions-content .panel-left.shown {
            position: fixed;
            z-index: 100000;
            background-color: #fff;
            right: 0;
            left: 0;
            width: 98%;
            top: 1vh;
        }
        .modal-custom .discussions-content .panel-left.shown {
            z-index: 110;
        }
        .discussions-content .proposal-container .proposal-image-container {
            width: 100%;
        }
        .discussions-content .connected-users {
            right: 7%;
        }
        .discussions-content .connected-users .user-bulle {
            min-width: 3rem;
            width: 3rem;
            height: 3rem;
        }
        .discussions-content .connected-users .user-bulle img {
            width: 30px;
            height: 30px;
        }
        
    }
</style>

<div class="discussions-content col-xs-12 bs-px-0 bs-mx-0 row" data-torefreshfrom-notif="<?= isset($params["answer"]["_id"]) ? (string) $params["answer"]["_id"] : "" ?>" data-refreshmethode="updateCommentVisual" data-refreshargs='{"answerId": "<?= isset($params["answer"]["_id"]) ? (string) $params["answer"]["_id"] : "" ?>"}'>
    <div class="col-xs-3 panel-left bs-px-0">
        <div class="col-xs-12 bs-px-0 panel-left-loader">
        </div>
        <div class="col-xs-12 bs-px-0 new-discussion-form">
            <span class="title-panel panel-left-title dis-flex">
                <i class="fa fa-send fa-2xx"></i>
                <h4 class="new-title"><?= Yii::t("common", "New discussion") ?></h4>
                <h4 class="edit-title hide">Modifier</h4>
                <span class="pull-right hideNewDiscuForm cursor-pointer"> <i class="fa fa-times fa-2x"></i> </span>
            </span>
            <div class="col-xs-12 panel-left-content bs-mt-2">
                <div class="col-xs-12 form-body">
                    <div class="form-group">
                        <label for="discu-name">
                            <?= Yii::t("common", "Discussion name") ?>
                        </label>
                        <input type="text" class="form-control" placeholder="Saisir nom..." id="discu-name">
                    </div>
                    <div class="form-group">
                        <label for="discu-privilege">
                            <?= Yii::t("common", "Discussion type") ?>
                        </label>
                        <select class="form-control" id="discu-privilege">
                            <option value="authorAll"><?= Yii::t("common", "Publique") ?></option>
                            <option value="authorAdmin"><?= Yii::t("common", "Private ") ?></option>
                            <option class="isAdminOnly" value="private"><?= ucfirst(Yii::t("common", "admins only")) ?></option>
                        </select>
                    </div>
                </div>
                <div class="col-xs-12 bs-px-0 bs-mt-2 dis-flex panel-left-action">
                    <button class="discussion-button bs-p-0 create-discu" style="--clr: #56c557;">
                        <span class="button-decor"></span>
                        <div class="button-content">
                            <div class="button__icon">
                            <svg viewBox="0 0 50 50" fill="none" xmlns="http://www.w3.org/2000/svg" width="24">
                                <circle opacity="0.5" cx="25" cy="25" r="23" fill="url(#icon-payments-cat_svg__paint0_linear_1141_21101)"/>
                                <mask id="icon-add-cat_svg__a" fill="#fff">
                                    <path fill-rule="evenodd" clip-rule="evenodd" d="M34.42 15.93c.382-1.145-.706-2.234-1.851-1.852l-18.568 6.189c-1.186.395-1.362 2-.29 2.644l5.12 3.072a1.464 1.464 0 001.733-.167l5.394-4.854a1.464 1.464 0 011.958 2.177l-5.154 4.638a1.464 1.464 0 00-.276 1.841l3.101 5.17c.644 1.072 2.25.896 2.645-.29L34.42 15.93z"/>
                                </mask>
                                <g transform="translate(2.2, 2.2) scale(0.45)">
                                    <path d="M 50 20 L 50 80 M 20 50 L 80 50" stroke="#fff" stroke-linecap="round" stroke-width="7" fill="none"/>
                                </g>
                                <defs>
                                    <linearGradient id="icon-add-cat_svg__paint0_linear_1141_21101" x1="25" y1="2" x2="25" y2="48" gradientUnits="userSpaceOnUse">
                                    <stop stop-color="#fff" stop-opacity="0.71"/>
                                    <stop offset="1" stop-color="#fff" stop-opacity="0"/>
                                    </linearGradient>
                                </defs>
                            </svg>
                            </div>
                            <span class="button__text">Créer discussion</span>
                        </div>
                    </button>
                    <button class="discussion-button bs-p-0 update-discu hide" style="--clr: #56c557;">
                        <span class="button-decor"></span>
                        <div class="button-content">
                            <div class="button__icon">
                                <svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" viewBox="0 0 512 512">
                                    <circle opacity="0.5" cx="25" cy="25" r="23" fill="url(#icon-payments-cat_svg__paint0_linear_1141_21101)"/>
                                    <mask id="icon-edit-cat_svg__a" fill="#fff">
                                        <path fill-rule="evenodd" clip-rule="evenodd" d="M34.42 15.93c.382-1.145-.706-2.234-1.851-1.852l-18.568 6.189c-1.186.395-1.362 2-.29 2.644l5.12 3.072a1.464 1.464 0 001.733-.167l5.394-4.854a1.464 1.464 0 011.958 2.177l-5.154 4.638a1.464 1.464 0 00-.276 1.841l3.101 5.17c.644 1.072 2.25.896 2.645-.29L34.42 15.93z"/>
                                    </mask>
                                    <path fill="white" d="m410.3 231l11.3-11.3l-33.9-33.9l-62.1-62.1l-33.9-33.9l-11.3 11.3l-22.6 22.6L58.6 322.9c-10.4 10.4-18 23.3-22.2 37.4L1 480.7c-2.5 8.4-.2 17.5 6.1 23.7s15.3 8.5 23.7 6.1l120.3-35.4c14.1-4.2 27-11.8 37.4-22.2l199.2-199.2zM160 399.4l-9.1 22.7c-4 3.1-8.5 5.4-13.3 6.9l-78.2 23l23-78.1c1.4-4.9 3.8-9.4 6.9-13.3l22.7-9.1v32c0 8.8 7.2 16 16 16h32zM362.7 18.7l-14.4 14.5l-22.6 22.6l-11.4 11.3l33.9 33.9l62.1 62.1l33.9 33.9l11.3-11.3l22.6-22.6l14.5-14.5c25-25 25-65.5 0-90.5l-39.3-39.4c-25-25-65.5-25-90.5 0zm-47.4 168l-144 144c-6.2 6.2-16.4 6.2-22.6 0s-6.2-16.4 0-22.6l144-144c6.2-6.2 16.4-6.2 22.6 0s6.2 16.4 0 22.6"/>
                                </svg>
                            </div>
                            <span class="button__text">Modifier discussion</span>
                        </div>
                    </button>
                </div>
            </div>
        </div>
        <span class="title-panel panel-left-title">
            <h4></h4>
             <i class="fa fa-comments fa-2x"></i>
             <span class="visible-xs pull-right hideLeftPanel"> <i class="fa fa-times fa-lg"></i> </span>
        </span>
        <div class="col-xs-12 panel-left-content">
            <ul class="questions-list discuList col-md-12 col-sm-12 col-xs-12 cs-scroll">
            </ul>
            <?php if ($params["canEditDiscussion"]) { ?>
                <div class="col-xs-12 bs-px-0 bs-mt-4 dis-flex panel-left-action">
                    <button class="discussion-button create-new-discu bs-p-0" style="--clr: #56c557;">
                        <span class="button-decor"></span>
                        <div class="button-content">
                            <div class="button__icon">
                                <svg viewBox="0 0 50 50" fill="none" xmlns="http://www.w3.org/2000/svg" width="24">
                                    <circle opacity="0.5" cx="25" cy="25" r="23" fill="url(#icon-payments-cat_svg__paint0_linear_1141_21101)"></circle>
                                    <mask id="icon-payments-cat_svg__a" fill="#fff">
                                        <path fill-rule="evenodd" clip-rule="evenodd" d="M34.42 15.93c.382-1.145-.706-2.234-1.851-1.852l-18.568 6.189c-1.186.395-1.362 2-.29 2.644l5.12 3.072a1.464 1.464 0 001.733-.167l5.394-4.854a1.464 1.464 0 011.958 2.177l-5.154 4.638a1.464 1.464 0 00-.276 1.841l3.101 5.17c.644 1.072 2.25.896 2.645-.29L34.42 15.93z">
                                        </path>
                                    </mask>
                                    <path fill-rule="evenodd" clip-rule="evenodd" d="M34.42 15.93c.382-1.145-.706-2.234-1.851-1.852l-18.568 6.189c-1.186.395-1.362 2-.29 2.644l5.12 3.072a1.464 1.464 0 001.733-.167l5.394-4.854a1.464 1.464 0 011.958 2.177l-5.154 4.638a1.464 1.464 0 00-.276 1.841l3.101 5.17c.644 1.072 2.25.896 2.645-.29L34.42 15.93z" fill="#fff"></path>
                                    <path d="M25.958 20.962l-1.47-1.632 1.47 1.632zm2.067.109l-1.632 1.469 1.632-1.469zm-.109 2.068l-1.469-1.633 1.47 1.633zm-5.154 4.638l-1.469-1.632 1.469 1.632zm-.276 1.841l-1.883 1.13 1.883-1.13zM34.42 15.93l-2.084-.695 2.084.695zm-19.725 6.42l18.568-6.189-1.39-4.167-18.567 6.19 1.389 4.166zm5.265 1.75l-5.12-3.072-2.26 3.766 5.12 3.072 2.26-3.766zm2.072 3.348l5.394-4.854-2.938-3.264-5.394 4.854 2.938 3.264zm5.394-4.854a.732.732 0 01-1.034-.054l3.265-2.938a3.66 3.66 0 00-5.17-.272l2.939 3.265zm-1.034-.054a.732.732 0 01.054-1.034l2.938 3.265a3.66 3.66 0 00.273-5.169l-3.265 2.938zm.054-1.034l-5.154 4.639 2.938 3.264 5.154-4.638-2.938-3.265zm1.023 12.152l-3.101-5.17-3.766 2.26 3.101 5.17 3.766-2.26zm4.867-18.423l-6.189 18.568 4.167 1.389 6.19-18.568-4.168-1.389zm-8.633 20.682c1.61 2.682 5.622 2.241 6.611-.725l-4.167-1.39a.732.732 0 011.322-.144l-3.766 2.26zm-6.003-8.05a3.66 3.66 0 004.332-.419l-2.938-3.264a.732.732 0 01.866-.084l-2.26 3.766zm3.592-1.722a3.66 3.66 0 00-.69 4.603l3.766-2.26c.18.301.122.687-.138.921l-2.938-3.264zm11.97-9.984a.732.732 0 01-.925-.926l4.166 1.389c.954-2.861-1.768-5.583-4.63-4.63l1.39 4.167zm-19.956 2.022c-2.967.99-3.407 5.003-.726 6.611l2.26-3.766a.732.732 0 01-.145 1.322l-1.39-4.167z" fill="#fff" mask="url(#icon-payments-cat_svg__a)"></path>
                                    <defs>
                                        <linearGradient id="icon-payments-cat_svg__paint0_linear_1141_21101" x1="25" y1="2" x2="25" y2="48" gradientUnits="userSpaceOnUse">
                                            <stop stop-color="#fff" stop-opacity="0.71"></stop>
                                            <stop offset="1" stop-color="#fff" stop-opacity="0"></stop>
                                        </linearGradient>
                                    </defs>
                                </svg>
                            </div>
                            <span class="button__text"><?= Yii::t("common", "New discussion") ?></span>
                        </div>
                    </button>
                </div>
            <?php } ?>
        </div>
    </div>
    <div class="col-xs-9 panel-right bs-px-0">
        <span class="title-panel panel-right-title dis-flex">
            <span class="visible-xs bs-mr-3 bs-py-2 showLeftPanel"><i class="fa fa-bars fa-2x"></i></span>
            <h4><?= Yii::t("common", "Discussions") ?></h4>
            <div class="pull-left col-xs-12 bs-px-0 connected-users cs-scroll scroll-small">
                <!-- <span class="user-bulle">
                    <i class="fa fa-users">
                    </i>
                </span> -->
            </div>
        </span>
        <div class="col-xs-12 panel-right-content bs-px-0">
            <div class="panel-right-loader hide"></div>
            <div class="col-xs-12 answer-content bs-mb-4 bs-px-0"></div>
            <div class="col-xs-12 comments-content bs-px-0">
                <span class="refresh-comment-content" style="display: none;">
                    <i class="fa fa-refresh"></i>
                    Actualiser
                </span>
                <div class="html-container col-xs-12 bs-px-0">

                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    discussionComments.params = JSON.parse(JSON.stringify(<?= json_encode($params) ?>));
    discussionComments.click = 0;
    if ($(".discussions-content .connected-users #user-"+userId).length < 1) {
        $(".discussions-content .connected-users").append(`
            <a href="javascript:" class="user-bulle" id="user-${userId}">
                <img src="${userConnected && userConnected.profilThumbImageUrl ? userConnected.profilThumbImageUrl : modules.co2.url + "/images/thumb/default_citoyens.png"}" alt="${ (typeof discussionComments.common.getInitials == "function" ? discussionComments.common.getInitials(userConnected && userConnected.name ? userConnected.name : "user connected") : "US") }">
            </a>
        `)
    }
    const thisPhp = {
        answer: JSON.parse(JSON.stringify(<?= json_encode($params["answer"]) ?>)),
    }
    $(function() {
        discussionComments.common.showPanelRightLoader();
        if (((typeof isInterfaceAdmin != "undefined" && !isInterfaceAdmin) || typeof isInterfaceAdmin == "undefined") && ((typeof isSuperAdmin != "undefined" && !isSuperAdmin) || typeof isSuperAdmin == "undefined")) {
            $(".discussions-content .isAdminOnly").remove()
        }
        if (discussionComments.params?.steps) {
            $(".discussions-content .panel-left-content .questions-list").html(discussionComments.common.buildCommentedQuestionList(typeof discussionComments.params?.orderedQuestion != "undefined" && notEmpty(discussionComments.params.orderedQuestion) ? discussionComments.params.orderedQuestion : {}));
            discussionComments.common.bindDiscussionContentEvent()

            if (!$(".discussions-content .panel-left-content .questions-list li.discuLi").first().hasClass("empty-question")) {
                var clickInterval = setInterval(() => {
                    if ($(".discussions-content .panel-left-content li.discuLi").is(":visible") || ($(window).width() < 768 && $(".discussions-content .panel-left-content li.discuLi").length > 0)) {
                        if ($(`.discussions-content .panel-left-content li.discuLi[data-toselect-comment="${ discussionComments.params.toBeSelectedComment }"]`).length > 0) {
                            $(`.discussions-content .panel-left-content li.discuLi[data-toselect-comment="${ discussionComments.params.toBeSelectedComment }"]`).trigger("click");
                        } else {
                            $(".discussions-content .panel-left-content li.discuLi").first().trigger("click");
                        }
                        clearInterval(clickInterval);
                    }
                }, 600);
            } else {
                $(".discussions-content .panel-right .panel-right-content .answer-content").html(`
                    <span class="content-header">
                        <h4>Contexte</h4>
                    </span>
                    <div class="col-xs-12 bs-px-3 content-body">
                        Aucune question commentée
                    </div>
                `);
                $(".discussions-content .panel-right .panel-right-content .comments-content .html-container").html(`
                    <span class="content-header">
                        <h4>Commentaires</h4>
                    </span>
                    <div class="col-xs-12 bs-px-3 content-body">
                        Aucun commentaire
                    </div>
                `);
            }
        }
        $(".showLeftPanel, .hideLeftPanel").off("click").on("click", discussionComments.common.toggleLeftPanel)
        if (typeof discussionSocket != "undefined" && discussionComments.params.answer && discussionComments.params.answer._id && currentUser) {
            discussionSocket.emitEvent(wsCO, "join_discussion_room", {userId: userId, userName: currentUser.name})
            var connectedInterval = setInterval(() => {
                if ($(".discussions-content").length < 1 && $(".aac-tl-comments").length < 1) {
                    clearInterval(connectedInterval)
                    discussionSocket.emitEvent(wsCO, "user_leave_discussion", {userId: userId, userName: currentUser.name})
                } else if ($(".aac-tl-comments").length > 0) {
                    clearInterval(connectedInterval)
                }
            }, 700);
        }
        if (discussionComments.members.active[thisPhp.answer._id.$id]) {
            $.each(discussionComments.members.active[thisPhp.answer._id.$id], function(userIndex, userValue) {
                if (userIndex != userId && $(".discussions-content .connected-users #user-"+userIndex).length < 1 && userValue.htmlContent) {
                    $(".discussions-content .connected-users").append(userValue.htmlContent)
                }
            })
        }
    });

</script>