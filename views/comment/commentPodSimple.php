<?php 
	$cssAnsScriptFilesModule = array(
		
		//'/js/comments.js',
	);
	HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, $this->module->assetsUrl);

	function multiple10($nb, $total){ //error_log("multiple1000 : " . $nb. "  - ".$total);
		for($i=0;$i<$total;$i+=10){ 
			if($i==$nb) error_log( "multiple10 : " . $i);
			if($i==$nb) return true;
		}
		return false;
	}
	if(empty($path) && !empty($context["actionPath"])){
		$path = $context["actionPath"]; 
	}
?>	
<style>
	.textarea-new-comment{
		max-width: 100%;
		min-width: 100%;
		vertical-align: top;
		font-size:13px;
	}
	.textarea-new-comment:focus {
		outline-style: solid;
		outline-width: 1px;
		outline-color: grey;
	}
	.footer-comments{
		<?php if($contextType != "actionRooms" && $contextType != "surveys" && $contextType != "actions"){ ?>
		margin-right: -10px;
		margin-left: -10px;
		margin-top: -5px;
		padding: 10px;
		<?php } ?> 
		background-color: rgba(231, 231, 231, 0.62);
	}
	.content-comment{
		max-width:85%;
		min-height: 35px;
	}
	.comment-container-white{
		background-color: rgba(231, 231, 231, 0.62);
    	border-radius: 35px;
    	padding: 7px 20px;
    	margin-bottom: 5px;
	}
	.answerCommentContainer {
	    margin-left: 45px;
	    margin-top: 5px;
	}
	
	.ctnr-txtarea{
		position: absolute;
		right:10px; 
		left:50px;
	}

	.answerCommentContainer .ctnr-txtarea{
		left:40px!important;
	}

	.content-comment .tool-action-comment{
		display: none;
	}
	.content-comment:hover .tool-action-comment{
		display: inherit;
	}
	.content-comment .fa-reply{
		font-size:14px;
		margin-right:5px;
		margin-left:5px;

	}
	.text-comment{
		word-wrap: break-word; 
		white-space: pre-line;
		width: 100%;
	}
	.content-new-comment .mentions{
		padding: 10px !important;
    	font-size: 13px !important;
	}
	.content-update-comment .mentions{
		padding: 10px !important;
    	font-size: 14px !important;
	}
	.date-com{
		font-size: 12px;
	}
</style>
<!-- /////////////////////////// QUESTION FROM BOUBOULE : IS STILL USED @Tango ? //////////////////////////// -->
<?php if($contextType == "actionRooms"){ ?>
<div class='row'>
	<?php 
	  	$icon = (@$context["status"] == ActionRoom::STATE_ARCHIVED) ? "download" : "comments";
      	$archived = (@$context["status"] == ActionRoom::STATE_ARCHIVED) ? "<span class='text-small helvetica'>(ARCHIVED)</span>" : "";
      	$color = (@$context["status"] == ActionRoom::STATE_ARCHIVED) ? "text-red " : "text-dark";
    ?>
    <div class='col-md-8'>
		<h3 class=" <?php echo $color;?>" style="color:rgba(0, 0, 0, 0.8);">
	      <i class="fa fa-angle-right"></i> "<?php echo $context["name"].$archived; ?>"
	  	</h3>
	</div>

	<?php
		if($contextType == "actionRooms" && $context["type"] == ActionRoom::TYPE_DISCUSS){
			echo "<div class='col-md-4'>";
			/*$this->renderPartial('../pod/fileupload', array("itemId" => (string)$context["_id"],
				  "type" => ActionRoom::COLLECTION,
				  "resize" => false,
				  "contentId" => Document::IMG_PROFIL,
				  "editMode" => $canComment,
				  "image" => $images,
				   "parentType" => $parentType,
				   "parentId" => $parentId, 
			)); */
		}
		echo "</div>";
	
    ?>
</div>
<?php } ?>
<!-- /////////////////////////// END QUESTION //////////////////////////// -->

<div class="footer-comments row">

	<?php //image profil user connected + input new comment
		$profilThumbImageUrlUser = ""; 

		if(isset(Yii::app()->session["userId"])){
			$me = Person::getMinimalUserById(Yii::app()->session["userId"]);
			$profilThumbImageUrlUser = Element::getImgProfil($me, "profilThumbImageUrl", $this->module->assetsUrl); 
	?>

			
			<div class="col-md-12 col-sm-12 col-xs-12 no-padding margin-top-15 container-txtarea">
				<img src="<?php echo $profilThumbImageUrlUser; ?>" class="img-responsive pull-left img-circle" 
					 style="margin-right:6px;height:32px; width:32px;">

				<div id="container-txtarea-<?php echo $idComment; ?>" class="content-new-comment">
					<div style="" class="ctnr-txtarea">
						<textarea rows="1" style="height:1em;" class="form-control textarea-new-comment" 
								  id="textarea-new-comment<?php echo $idComment; ?>" placeholder="<?php echo Yii::t("common","Your comment") ?>..."></textarea>
						<input type="hidden" id="argval" value=""/>
					</div>
				</div>
			</div>
	<?php } ?>

	<div id="comments-list-<?php echo $idComment; ?>">

		<?php 
			$assetsUrl = $this->module->assetsUrl;
			function showCommentTree($comments, $assetsUrl, $idComment, $canComment, $level, $parentType=null){
				$count = 0;
				$hidden = 0;
				$hiddenClass = "";
				$nbTotalComments = sizeOf($comments);

				if($nbTotalComments == 0 && $level == 1) { echo "<span class='noComment'>".Yii::t("comment", "No comment")."</span>"; }
				if($nbTotalComments == 0) return;

				foreach ($comments as $key => $comment) { 
					/*echo "<pre>";
					var_dump($comment["author"]);
				   echo "</pre>";exit;*/
			 		$count++; 
					$profilThumbImageUrl = Element::getImgProfil($comment["author"], "profilThumbImageUrl", $assetsUrl); 
					if($hidden > 0) $hiddenClass = "hidden hidden-".$hidden;
					
					$classArgument = "";
					if(@$comment["argval"] == "up") $classArgument = "bg-green-comment";
					if(@$comment["argval"] == "down") $classArgument = "bg-red-comment";
					if(@$comment["argval"] == "") $classArgument = "bg-white-comment";
					//var_dump($comment["author"]);
					$slug = Slug::getByTypeAndId(Person::COLLECTION, $comment["author"]["id"]);
					if(@$slug == null){
						$slug = "page.type.".Person::COLLECTION.".id.".$comment["author"]["id"];
					}else{ $slug = "@".$slug["name"]; }
		?>
					<div class="col-xs-12 no-padding margin-top-5 item-comment <?php echo $hiddenClass.' '.$classArgument; ?>" 
						 id="item-comment-<?php echo $comment["_id"]; ?>">

						<img src="<?php echo $profilThumbImageUrl; ?>" class="img-responsive pull-left img-circle" 
							 style="margin-right:5px; margin-top:10px; height:32px;width: 32px;">
					
						<span class="pull-left content-comment col-xs-12 no-padding">						
							<span class="text-black pull-left col-xs-12 comment-container-white">
								<a href="#<?php echo @$slug; ?>" class="text-dark pull-left">
									<strong><?php echo $comment["author"]["name"]; ?></strong>
									<?php 
										if(!empty($comment["author"]["isAdmin"]))
											echo '<small><i>'."Admin";
										if(!empty($comment["author"]["roles"]))
											echo ',</i></small>';
										else
											echo '</i></small>';
									?>
									<?php 
									if(!empty($comment["author"]["roles"]))
										echo '<small><i>'.join(", ",$comment["author"]["roles"]).'</i></small>';
									?>
									
								</a> 
								<span class="timeline-comments-<?php echo $comment["_id"] ?> pull-right date-com"></span>
								<?php if(@$comment["rating"]){ ?>
									<div class="br-wrapper br-theme-fontawesome-stars pull-left margin-left-10">
                						<select id="ratingComments<?php echo $comment["_id"]; ?>" class="ratingComments">
                						    <option value="1">1</option>
						                    <option value="2">2</option>
						                    <option value="3">3</option>
						                    <option value="4">4</option>
						                    <option value="5">5</option>
						                  </select>
						                </div> 
								<?php } ?><br>
								<span class="text-comment text-comment-<?php echo $comment["_id"] ?>  text-left pull-left <?php echo (@$comment['reportAbuseCount']&&$comment['reportAbuseCount']>=5)?'text-red-light-moderation':'' ?>" data-parent-id="<?php echo $idComment ?>"><?php echo $comment["text"]; ?></span>
							</span>
							<!--<br>
							<small class="pull-right date-com">
								<i class="fa fa-clock-o"></i> 
								<?php echo Translate::pastTime(date(@$comment["created"]), "timestamp"); ?>
							</small>
								-->
							<br>
							<small class="bold">
							<?php if( /*@Yii::app()->session["userId"] && */ !@$comment["rating"]){ ?>
								 <div class="col-xs-12 pull-left no-padding" id="footer-comments-<?php echo @(string)$comment["_id"]; ?>" style="padding-left: 15px !important;"></div>
							<?php	} ?>
							</small>
						</span>
						<div id="comments-list-<?php echo $comment["_id"]; ?>" data-size="<?= sizeOf($comment["replies"]) ?>" class="hidden pull-left col-md-11 col-sm-11 col-xs-11 no-padding answerCommentContainer">
							<?php if(sizeOf($comment["replies"]) > 0) //recursive for answer (replies)
									showCommentTree($comment["replies"], $assetsUrl, $comment["_id"], $canComment, $level+1, $comment["contextType"]);  ?>
						</div>
					</div>
		<?php 		if(multiple10($count, $nbTotalComments)){ $hidden = $count; ?>
		<?php			$hiddenClass = ($hidden > 10) ? "hidden hidden-".($hidden-10) : ""; ?>
						<div class="pull-left margin-top-5 <?php echo $hiddenClass; ?> link-show-more-<?php echo ($hidden-10); ?>">
							<a class="" href="javascript:" onclick="showMoreComments('<?php echo $idComment; ?>', <?php echo $hidden; ?>);">
								<i class="fa fa-angle-down"></i> <?php echo Yii::t("comment","Show more comments") ?>
							</a>
						</div>
		<?php 		} //if (multiple10 ?>

		

		<?php 	} //$.each ?>
		<?php 	if($hidden > 0){ ?>
					<div class="pull-right margin-top-5">
						<a class="" href="javascript:" onclick="hideComments('<?php echo $idComment; ?>', <?php echo $level; ?>);">
							<i class="fa fa-angle-up"></i> Masquer
						</a>
					</div>
		<?php 	} ?>
		<?php	}//function()
		?>

		<?php showCommentTree($comments, $assetsUrl, $idComment, $canComment, 1, $contextType); ?>
					
	</div><!-- id="comments-list-<?php echo $idComment; ?>" -->

</div><!-- class="footer-comments" -->

<!-- ------------------------------------------------------------------------------------------------------------------------------------- -->

<script type="text/javascript" >
	var contextType = "<?php echo $contextType; ?>";
	var idComment = "<?php echo $idComment; ?>";
	var comments = <?php echo json_encode($comments); ?>;
	
	var context = <?php echo json_encode($context)?>;
	var canComment = <?php echo @$canComment ?>;
	var profilThumbImageUrlUser = "<?php echo @$profilThumbImageUrlUser; ?>";
	var isUpdatedComment=false;
	var contextPath="<?php echo @$path; ?>";
	// mylog.log("context");
	// mylog.dir(context);
	// mylog.log("comments");
	// mylog.dir(comments);

	function setCommentsDate (commentsList) {
		$.each(commentsList, function(i,v){
			const returnDate = directory.showDatetimePost(v.collection, i, (v.postedDate ? v.postedDate : v.created));
			$(".timeline-"+v.collection+"-"+i).html(returnDate);
			if(typeof v.rating != "undefined"){
				$("#ratingComments"+i).barrating({
					theme: 'fontawesome-stars',
					'readonly': true
				});
				$("#ratingComments"+i).barrating("set", v.rating);
	      		//$("#ratingComments"+i).barrating();
			}else{
				if(typeof v.replies != "undefined"){
					setCommentsDate(v.replies)
				}
				mentionAndLinkify(i,v);
		    }
		});
	}
	jQuery(document).ready(function() {
		initCommentsTools(comments, "comments", canComment);
		var idTextArea = '#textarea-new-comment<?php echo $idComment; ?>';
		bindEventTextArea(idTextArea, idComment, contextType, false, "", null, contextPath);
		bindEventActions();

		mylog.log(".comments-list-<?php echo $idComment; ?> .text-comment");
		/*$("#comments-list-<?php echo $idComment; ?> .text-comment").each(function(){
			idComment=$(this).data("id");
			idParent=$(this).data("parent-id");
			textComment=$(this).html();
			//if(typeof idParent != "undefined"){
			//	comments[idComment]=comments[idParent].replies[idComment];
			//}
			
	        //}
			textComment = linkify(textComment);
			$(this).html(textComment);
		});*/
		setCommentsDate(comments)
		// $.each(comments, function(i,v){
		// 	var returnDate = directory.showDatetimePost(v.collection, i, (v.postedDate ? v.postedDate : v.created));
		// 	$(".timeline-"+v.collection+"-"+i).html(returnDate);
		// 	if(typeof v.rating != "undefined"){
		// 		$("#ratingComments"+i).barrating({
		// 			theme: 'fontawesome-stars',
		// 			'readonly': true
		// 		});
		// 		$("#ratingComments"+i).barrating("set", v.rating);
	    //   		//$("#ratingComments"+i).barrating();
		// 	}else{
		// 		if(typeof v.replies != "undefined"){
		// 			$.each(v.replies, function(e, reply){
		// 				const timelineDate = directory.showDatetimePost(reply.collection, e, (reply.postedDate ? reply.postedDate : reply.created));
		// 				$(".timeline-"+reply.collection+"-"+e).html(timelineDate);
		// 				comments[e]=reply;
		// 				mentionAndLinkify(e,reply);
		// 			});
		// 		}
		// 		mentionAndLinkify(i,v);
				
		//     }
		// });

		$(".tooltips").tooltip();
		
	});

	function mentionAndLinkify(objectId,object, getText){
		textComment=object.text;
		if(typeof(object.mentions) != "undefined"){
		    textComment = mentionsInit.addMentionInText(textComment, object.mentions);
		}
		textComment = linkify(textComment);
		if(notNull(getText) && getText)
			return textComment;
		else
			$(".text-comment-"+objectId).html(textComment);
	}

	function bindEventActions(){

		//Abuse process
		/*$('.commentReportAbuse').off().on("click",function(){
			id=$(this).data("id");
			if($(this).data("voted")=="true")
				toastr.info("<?php //echo Yii::t("common", "Remove your last opinion before") ?>");
			else{	
				if($(".commentVoteUp[data-id='"+id+"']").hasClass("text-green") || $(".commentVoteDown[data-id='"+id+"']").hasClass("text-orange")){
					toastr.info("<?php //echo Yii::t("common", "You can't make any actions on this comment after reporting abuse !") ?>");
				}
				else{
					reportAbuse($(this), $(this).data("contextid"));
				}
			}
		});*/
		$('.deleteComment').off().on("click",function(){
			actionAbuseComment($(this), "<?php echo Comment::STATUS_DELETED ?>", "");
		});
	}
	

	

	

	



	/*function reportAbuse(comment, contextId) {
		// mylog.log(contextId);
		var message = "<div id='reason' class='radio'>"+
			"<h3 class='margin-top-10'>Pour quelle raison signalez-vous ce contenu ?</h3>" +
			"<hr>" +
			"<label><input type='radio' name='reason' value='Propos malveillants' checked>Propos malveillants</label><br>"+
			"<label><input type='radio' name='reason' value='Incitation et glorification des conduites agressives'>Incitation et glorification des conduites agressives</label><br>"+
			"<label><input type='radio' name='reason' value='Affichage de contenu gore et trash'>Affichage de contenu gore et trash</label><br>"+
			"<label><input type='radio' name='reason' value='Contenu pornographique'>Contenu pornographique</label><br>"+
		  	"<label><input type='radio' name='reason' value='Liens fallacieux ou frauduleux'>Liens fallacieux ou frauduleux</label><br>"+
		  	"<label><input type='radio' name='reason' value='Mention de source erronée'>Mention de source erronée</label><br>"+
		  	"<label><input type='radio' name='reason' value='Violations des droits auteur'>Violations des droits d\'auteur</label><br><br>"+
		  	"<input type='text' class='form-control' style='text-align:left;' id='reasonComment' placeholder='Laisser un commentaire...'/><br>"+
		  	"Votre signalement sera envoyé aux administrateurs du réseau,<br> qui le traiteront conformément aux <a href='javascript:'>conditions d'utilisations</a><br>" + 
			"<span class='text-red'><i class='fa fa-info-circle'></i> Tout signalement est définitif, vous ne pourrez pas l'annuler</span><br>" +
			"<hr>" +
			"<span class=''><i class='fa fa-arrow-right'></i> Le contenu sera signalé par un <i class='fa fa-flag text-red'></i> s'il fait l'objet d'au moins 2 signalements</span><br>" +
			"<span class='text-red-light'><i class='fa fa-arrow-right'></i> Le contenu sera masqué s'il fait l'objet d'au moins 5 signalements</span><br>" +
			"<span class=''><i class='fa fa-arrow-right'></i> Le contenu sera supprimé par les administrateurs s'il enfreint les conditions d'utilisations</span>" +
			"</div>";
		var boxComment = bootbox.dialog({
		  message: message,
		  //title: '<?php //echo Yii::t("comment","You are going to declare this comment as abuse : please fill the reason ?") ?>',
		  title: '<span class="text-red"><i class="fa fa-flag"></i> <?php //echo Yii::t("comment","Signaler un abus") ?>',
		  buttons: {
		  	annuler: {
		      label: "Annuler",
		      className: "btn-default",
		      callback: function() {
		        mylog.log("Annuler");
		      }
		    },
		    danger: {
		      label: "Envoyer le signalement",
		      className: "btn-danger",
		      callback: function() {
		      	// var reason = $('#reason').val();
		      	var reason = $("#reason input[type='radio']:checked").val();
		      	var reasonComment = $("#reasonComment").val();
		      	actionAbuseComment(comment, "<?php //echo Action::ACTION_REPORT_ABUSE ?>", reason, reasonComment);
				disableOtherAction(comment.data("id"), '.commentReportAbuse');
				//copyCommentOnAbuseTab(comment);
				return true;
		      }
		    },
		  }
		});

		boxComment.on("shown.bs.modal", function() {
		  $.unblockUI();
		});

		boxComment.on("hide.bs.modal", function() {
		  $.unblockUI();
		});
	}*/

	function confirmDeleteComment(id, $this){
		// mylog.log(contextId);
		var message = "<?php echo Yii::t("comment","Do you want to delete this comment") ?> ?";
		var boxComment = bootbox.dialog({
		  message: message,
		  title: '<?php echo Yii::t("comment","You are going to delete this comment : are your sure ?") ?>', //Souhaitez-vous vraiment supprimer ce commentaire ?
		  buttons: {
		  	annuler: {
		      label: trad.cancel,
		      className: "btn-default",
		      callback: function() {
		        mylog.log("Annuler");
		      }
		    },
		    danger: {
		      label: trad.delete,
		      className: "btn-primary",
		      callback: function() {
		      	deleteComment(id,$this);
				return true;
		      }
		    },
		  }
		});

		boxComment.on("shown.bs.modal", function() {
		  $.unblockUI();
		});

		boxComment.on("hide.bs.modal", function() {
		  $.unblockUI();
		});
	}

	function deleteComment(id,$this){
		ajaxPost(
			null,
			baseUrl+'/'+moduleId+"/comment/delete/id/"+id,
			null,
			function(data){ 
		        if (data.result) {  
		        	mylog.log(data);           
					toastr.success("<?php echo Yii::t("common","Comment successfully deleted")?>");
					//liParent=$this.parents().eq(2);
		        	//liParent.fadeOut();
		        	$("#item-comment-"+id).html("");
		        	$('.nbComments').html((parseInt($('.nbComments').html()) || 0) - 1);
		        	if (notEmpty(data.comment) && notEmpty(data.comment.contextType) && data.comment.contextType=="news"){
						$(".newsAddComment[data-id='"+data.comment.contextId+"']").children().children(".nbNewsComment").text(parseInt($('.nbComments').html()) || 0 );
					}
					if (typeof discussionSocket != "undefined" && typeof discussionSocket.emitEvent != "undefined" && userId, typeof userConnected != "undefined" && userConnected.name) {
						discussionSocket.emitEvent(wsCO, "change_comment", {userId: userId, userConnected: userConnected.name, onUpdate: "deleteComment", commentId: id, parentCommentId: (data?.elt?.parentId ? data.elt.parentId : null), deletedCommentId: id});
					}
					if(typeof aapObj != "undefined" && aapObj.common && aapObj.common.getQuery) {
						const urlQuery = aapObj.common.getQuery();
						if(urlQuery["preview"]) {
							if(urlQuery["preview"].indexOf(".") > -1) {
								subUrlQuery = {};
								subUrlQueryArray = urlQuery["preview"].replace(/\%20/g, " ").split(".");
								subUrlQueryArray.forEach((item, index) => {
									if(subUrlQueryArray[index+1] && notEmpty(subUrlQueryArray[index+1])) {
										if(notEmpty(item))
											subUrlQuery[item] = subUrlQueryArray[index+1]
										else 
											subUrlQuery["title"] = subUrlQueryArray[index+1]
									}
								})
								if(subUrlQuery["answers"]) {
									ajaxPost(null, typeof coWsConfig != "undefined" && coWsConfig.pingRefreshViewUrl ? coWsConfig.pingRefreshViewUrl : null, {
										action : "commentUpdated",
										answerId : subUrlQuery["answers"],
										userSocketId : aapObj.userSocketId ? aapObj.userSocketId : (typeof coWsData != "undefined" && coWsData && coWsData.id ? coWsData.id : null),
										emiterSocketId: (typeof coWsData != "undefined" && coWsData && coWsData.id ? coWsData.id : null),
										responses : data,
									}, function (data) {
											if(aapObj.directory && aapObj.directory.propositionCollapseHtml && aapObj.events && aapObj.events.proposalCollapseItems && $(`#proposal-container-detailed-${subUrlQuery["answers"]} .proposition-collapse-item[data-id="${subUrlQuery["answers"]}"]`).is(":visible")) {
												const html = aapObj.directory.propositionCollapseHtml(aapObj, subUrlQuery["answers"]);
												$(`.proposition-collapse-item[data-id="${subUrlQuery["answers"]}"]`).empty().html(html);
												aapObj.events.proposalCollapseItems(aapObj);
											}
									}, null, {contentType : 'application/json'});
								}
							}
						}
					}
				} else {
		            toastr.error("Quelque chose a buggé"); //j'adore cette alert ;) !
		        }
		    }
		  ); 
	}

	function editComment(idComment){
		// mylog.log(contextId);

		if(userId != ""){
			isUpdatedComment=true;
			var commentContent = comments[idComment].text;
			var message = "<div id='container-txtarea-"+idComment+"' class='content-update-comment'>"+
							"<textarea id='textarea-edit-comment"+idComment+"' class='form-control' placeholder='"+trad.modifyyourcomment+"'>"+commentContent+
							"</textarea>"+
						  "</div>";
			var boxComment = bootbox.dialog({
			  message: message,
			  title: '<?php echo Yii::t("comment","Update your comment"); ?>', //Souhaitez-vous vraiment supprimer ce commentaire ?
			  buttons: {
			  	annuler: {
			      label: trad.cancel,
			      className: "btn-default",
			      callback: function() {
			      	isUpdatedComment=false;
			      }
			    },
			    enregistrer: {
			      label: trad.save,
			      className: "btn-success",
			      callback: function() {
			      	updateComment(idComment,$("#textarea-edit-comment"+idComment).val(), "#textarea-edit-comment"+idComment);
					isUpdatedComment=false;
					return true;
			      }
			    },
			  }
			});

			boxComment.on("shown.bs.modal", function() {
			  $.unblockUI();
			  bindEventTextArea('#textarea-edit-comment'+idComment, idComment, contextType, false, "", comments[idComment]);
			});

			boxComment.on("hide.bs.modal", function() {
			  $.unblockUI();
			});
		}
		
	}



</script>