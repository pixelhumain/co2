<?php 
	$cssAnsScriptFilesModule = array(
		
		//'/js/comments.js',
	);
	HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, $this->module->assetsUrl);

	function multiple10($nb, $total){ //error_log("multiple1000 : " . $nb. "  - ".$total);
		for($i=0;$i<$total;$i+=10){ 
			if($i==$nb) error_log( "multiple10 : " . $i);
			if($i==$nb) return true;
		}
		return false;
	}
	if(empty($path) && !empty($context["actionPath"])){
		$path = $context["actionPath"]; 
	}
?>
<?php if(empty($readOnly)){ //if using specific css ?> 
<style>
	.textarea-new-comment{
		max-width: 100%;
		min-width: 100%;
		vertical-align: top;
		font-size:13px;
	}
	.textarea-new-comment:focus {
		outline-style: solid;
		outline-width: 1px;
		outline-color: grey;
	}
	.footer-comments{
		<?php if($contextType != "actionRooms" && $contextType != "surveys" && $contextType != "actions"){ ?>
		margin-right: -10px;
		margin-left: -10px;
		margin-top: -5px;
		padding: 10px;
		<?php } ?> 
		background-color: rgba(231, 231, 231, 0.62);
	}
	.content-comment{
		max-width:85%;
		min-height: 35px;
	}
	.comment-container-white{
		background-color: rgba(231, 231, 231, 0.62);
    	border-radius: 35px;
    	padding: 7px 20px;
    	margin-bottom: 5px;
	}
	.answerCommentContainer {
	    margin-left: 45px;
	    margin-top: 5px;
	}
	
	.ctnr-txtarea{
		position: absolute;
		right:10px; 
		left:50px;
	}

	.answerCommentContainer .ctnr-txtarea{
		left:40px!important;
	}

	.content-comment .tool-action-comment{
		display: none;
	}
	.content-comment:hover .tool-action-comment{
		display: inherit;
	}
	.content-comment .fa-reply{
		font-size:14px;
		margin-right:5px;
		margin-left:5px;

	}
	.text-comment{
		word-wrap: break-word; 
		white-space: pre-line;
		width: 100%;
	}
	.content-new-comment .mentions{
		padding: 10px !important;
    	font-size: 13px !important;
	}
	.content-update-comment .mentions{
		padding: 10px !important;
    	font-size: 14px !important;
	}
	.date-com{
		font-size: 12px;
	}
</style>
<?php } ?>

<?php if($contextType == "actionRooms"){ ?>
<div class='row'>
	<?php 
	  	$icon = (@$context["status"] == ActionRoom::STATE_ARCHIVED) ? "download" : "comments";
      	$archived = (@$context["status"] == ActionRoom::STATE_ARCHIVED) ? "<span class='text-small helvetica'>(ARCHIVED)</span>" : "";
      	$color = (@$context["status"] == ActionRoom::STATE_ARCHIVED) ? "text-red " : "text-dark";
    ?>
    <div class='col-md-8'>
		<h3 class=" <?php echo $color;?>" style="color:rgba(0, 0, 0, 0.8);">
	      <i class="fa fa-angle-right"></i> "<?php echo $context["name"].$archived; ?>"
	  	</h3>
	</div>
</div>
<?php } ?>
<!-- /////////////////////////// END QUESTION //////////////////////////// -->

<div class="footer-comments row">

	<?php //image profil user connected + input new comment
		$profilThumbImageUrlUser = ""; 

		if(isset(Yii::app()->session["userId"])){
			$me = Person::getMinimalUserById(Yii::app()->session["userId"]);
			$profilThumbImageUrlUser = Element::getImgProfil($me, "profilThumbImageUrl", $this->module->assetsUrl); 
	?>
	<?php } ?>

	<div id="comments-list-<?php echo $idComment; ?>">

		<?php 
			$assetsUrl = $this->module->assetsUrl;
			function showCommentTree($comments, $assetsUrl, $idComment, $canComment, $level, $parentType=null){
				$count = 0;
				$hidden = 0;
				$hiddenClass = "";
				$nbTotalComments = sizeOf($comments);

				if($nbTotalComments == 0 && $level == 1) { echo "<span class='noComment'>".Yii::t("comment", "No comment")."</span>"; }
				if($nbTotalComments == 0) return;

				foreach ($comments as $key => $comment) { 
			 		$count++; 
					$profilThumbImageUrl = Element::getImgProfil($comment["author"], "profilThumbImageUrl", $assetsUrl); 
					if($hidden > 0) $hiddenClass = "hidden hidden-".$hidden;
					
					$classArgument = "";
					if(@$comment["argval"] == "up") $classArgument = "bg-green-comment";
					if(@$comment["argval"] == "down") $classArgument = "bg-red-comment";
					if(@$comment["argval"] == "") $classArgument = "bg-white-comment";

					$slug = Slug::getByTypeAndId(Person::COLLECTION, $comment["author"]["id"]);
					if(@$slug == null){
						$slug = "page.type.".Person::COLLECTION.".id.".$comment["author"]["id"];
					}else{ $slug = "@".$slug["name"]; }
		?>
					<div class="col-xs-12 no-padding margin-top-5 item-comment <?php echo $hiddenClass.' '.$classArgument; ?>" 
						 id="item-comment-<?php echo $comment["_id"]; ?>">

						<img src="<?php echo $profilThumbImageUrl; ?>" class="img-responsive pull-left img-circle" 
							 style="margin-right:5px; margin-top:10px; height:32px;width: 32px;">
					
						<span class="pull-left content-comment col-xs-12 no-padding">						
							<span class="text-black pull-left col-xs-12 comment-container-white">
								<a href="#<?php echo @$slug; ?>" class="text-dark pull-left" style="font-size:14px">
									<strong><?php echo $comment["author"]["name"]; ?></strong>
									<?php 
										if(!empty($comment["author"]["isAdmin"]))
											echo '<small><i>'."Admin";
										if(!empty($comment["author"]["roles"]))
											echo ',</i></small>';
										else
											echo '</i></small>';
									?>
									<?php 
									if(!empty($comment["author"]["roles"]))
										echo '<small><i>'.join(", ",$comment["author"]["roles"]).'</i></small>';
									?>
									
								</a> 
								<span class="timeline-comments-<?php echo $comment["_id"] ?> pull-right date-com"></span>
								<br>
								<span class="text-comment text-comment-<?php echo $comment["_id"] ?>  text-left pull-left <?php echo (@$comment['reportAbuseCount']&&$comment['reportAbuseCount']>=5)?'text-red-light-moderation':'' ?>" data-parent-id="<?php echo $idComment ?>" style="font-size:12px"> 
									<?php echo $comment["text"]; ?>
								</span>
							</span>
							<!--<br>
							<small class="pull-right date-com">
								<i class="fa fa-clock-o"></i> 
								<?php echo Translate::pastTime(date(@$comment["created"]), "timestamp"); ?>
							</small>
								-->
							
							<small class="bold">
							<?php if( /*@Yii::app()->session["userId"] && */ !@$comment["rating"]){ ?>
								 <div class="col-xs-12 pull-left no-padding footer-comments-items" id="footer-comments-<?php echo @(string)$comment["_id"]; ?>" style="padding-left: 15px !important;"></div>
							<?php	} ?>
							</small>
						</span>
						<table style="width:100%">
							<tr>
								<td style="width:auto">
									<div id="comments-list-<?php echo $comment["_id"]; ?>" class="hidden pull-left col-md-11 col-sm-11 col-xs-11 no-padding answerCommentContainer">
										<?php if(sizeOf($comment["replies"]) > 0) //recursive for answer (replies)
												showCommentTree($comment["replies"], $assetsUrl, $comment["_id"], $canComment, $level+1, $comment["contextType"]);  ?>
									</div>
								</td>
							</tr>
						</table>
					</div>
		<?php 	} //$.each ?>
		<?php	}//function()
		?>

		<?php showCommentTree($comments, $assetsUrl, $idComment, $canComment, 1, $contextType); ?>
					
	</div><!-- id="comments-list-<?php echo $idComment; ?>" -->

</div><!-- class="footer-comments" -->
