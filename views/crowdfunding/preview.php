<style>
	#modal-preview-coop{
		overflow: auto;
		z-index: 1000 !important;
	}
	#poi .title{
		padding-bottom: 15px !important;
    	border-bottom: 1px solid rgba(150,150,150, 0.3);
	}
	.short-description{
		font-size:20px;
		text-align: justify;
	}
	.campaign-detail{
		text-align: justify;
		color:#05323e;

	}

	.linkedWith{
		font-size:14px;
		font-style:italic;
	}

	.caption{
		    width: 50px;
		    height: 20px;
		    border-radius: 5px;
	}

	.caption-label{
		color:#05323e;
		padding-left:5px;
	}

	.campaign-detail.activeMarkdown p, .campaign-detail.activeMarkdown li{
		font-size: 14px !important;
	}

	.campaign-detail a{
		text-decoration: underline;
	}

	.campaign-detail a:hover{
		font-weight: 700;
	}	

	.tags-content .badge.tags-poi-preview{
		padding: 3px 15px!important;
	    margin-right: 5px;
	}

	.donationTable td{
		padding: 5px;
    	border-bottom: solid;
	}

	.container-img-card{
		width: 100%;
	    float: left;
	    min-height: 400px;
	    max-height: 400px;
	    background: white;
	    
	    z-index: -1;
	   /* background-image: url(/upload/communecter/crowdfunding/615194a…/previewdokos.jpg?_=1632737583);
	    background-repeat: no-repeat;
	    background-size: contain;
	    height: 420px;*/
	    
	}
	.img-responsive{
	    z-index: inherit;
	    /*display: inline-block;*/
	    max-height: inherit;
	    min-height: inherit;
	    margin:auto;
	}

</style>
<?php


if($element["type"]=="campaign"){
	//projet parent
	$camp=$element;
	//var_dump($camp);exit;
	reset($camp["parent"]);
	$parentKey=key($camp["parent"]);
	$project=PHDB::findOne(Project::COLLECTION,array("_id"=>New MongoId($parentKey)));
	//var_dump($project);
	//var_dump($project);
	$imgProj = isset($project["profilMediumImageUrl"]) ? $project["profilMediumImageUrl"] :  Yii::app()->getModule('costum')->assetsUrl."/images/templateCostum/no-banner.jpg";
	//Yii::app()getModule('costum')->assetsUrl."/images/templateCostum/no-banner.jgg"
	$imgCamp=isset($camp["profilImageUrl"]) ? $camp["profilImageUrl"] : $imgProj ; 
	$shortDescProj = isset($project["shortDescription"]) ? $project["shortDescription"] : Yii::t("common","No description registred");

					$url = isset($camp["urls"]) ? $camp["urls"][0] : "";
					$videoId = "";
					$src = "";

					//var_dump($url);
					
					if (strpos($url,"=") != false) {
						$videoId = array_reverse(explode("=",$url))[0];
					} else {
						$videoId = array_reverse(explode("/",$url))[0];
					}

					//var_dump($videoId);

					if (strpos($url,"vimeo") != false) {
						$src = "https://player.vimeo.com/video/".$videoId;
					}
					if (strpos($url,"youtu") != false) {
						$src = "https://www.youtube.com/embed/".$videoId;
					}
					if (strpos($url,"dailymotion") != false) {
						$src = "https://www.dailymotion.com/embed/video/".$videoId;
					}
					if (strpos($url,"peertube.communecter") != false) {
						$src = "https://peertube.communecter.org/videos/embed/".$videoId;
					}

					//var_dump($src);
	//$imgCamp="";
	// $shortDescProj="";
	// var_dump($project["profilMediumImageUrl"]);
	// var_dump(Yii::app()->getModule('costum')->assetsUrl);
	// var_dump($imgCamp);
	// campagne
	
	
	$iban = isset($camp["iban"]) ? $camp["iban"] : "" ;
	$donationPlatform = isset($camp["donationPlatform"]) ? $camp["donationPlatform"] : "" ;
	$directDonation= isset($camp["directDonation"]) ? $camp["directDonation"] : "false";

	$Parsedown = new Parsedown();
	//$Parsedown->text('Hello _Parsedown_!');
	$campaignDesc=isset($camp["description"]) ? $Parsedown->text($camp["description"]) : Yii::t("common","No description registred");


	$currency_symbol = "€";
	$rest = $camp["goal"] - $camp["donationNumber"] - $camp["pledgeNumber"];
	$rest_txt = (string)$rest . $currency_symbol;
	$donations_txt = (string)$camp["donationNumber"] . $currency_symbol;
	$pledges_txt = (string)$camp["pledgeNumber"] . $currency_symbol;
	$requested_text = (string)$camp["goal"] . $currency_symbol;

	// Compute progress percentages

	$donations_perc = round($camp["donationNumber"] / $camp["goal"] * 100);
	$pledges_perc = "";
	if ($rest <= 0) {
		$pledges_perc = 100 - $donations_perc;
	} else {
		$pledges_perc = round($camp["pledgeNumber"] / $camp["goal"] * 100);
	}


	// Clamp progress percentages
	$min_perc = 1;
	if ($donations_perc <= $min_perc) {
	    $donations_perc = $min_perc;
	    $pledges_perc = min($pledges_perc, 100 - $min_perc);
	}
	if ($pledges_perc <= $min_perc) {
	    $pledges_perc = $min_perc;
	    $donations_perc = min($donations_perc, 100 - $min_perc);
	}

	$donations_perc_txt = (string)$donations_perc;
	$pledges_perc_txt = (string)$pledges_perc;


	// $eltId=key($element['parent']);
	// $eltType= $element['parent'][$eltId]['type'];
	// $eltName= $element['parent'][$eltId]['name'];
}	
	?>

<div class="margin-bottom-50 col-xs-12 no-padding">
	<div class="col-xs-12 no-padding">
		<?php if($element["type"]=="campaign"){ ?>
			
				
			<div class="container-img-card">
				<div class="pull-right center margin-10" style="position: absolute;right: 5px;top: 5px;">
				<button class="btn btn-default pull-right btn-close-preview">
					<i class="fa fa-times"></i>
				</button>
				<?php 

				if( $element["creator"] == Yii::app()->session["userId"] || 
						  Authorisation::canEditItem( Yii::app()->session["userId"], "crowdfunding", $id, @$element["parentType"], @$element["parentId"] ) ){ ?>
					
					<button class="btn btn-default pull-right margin-right-10 text-red deleteThisBtn" 
							data-type="crowdfunding" data-id="<?php echo $id ?>">
						<i class=" fa fa-trash"></i>
					</button>
					<button class="btn btn-default pull-right margin-right-10 btn-edit-preview" data-type="crowdfunding" data-id="<?php echo $id ?>" 
					data-subtype="<?php echo $element["type"] ?>">
						<i class="fa fa-pencil"></i>
					</button>
				<?php } ?>
			</div>
				<img src="<?php echo $imgCamp ?>" id="bannerPreview" class='img-responsive bg-light' style=""/>
			</div>	
			<div class="campaign-detail container col-xs-12">
			    <div class="row">
			        <div class="col-xs-12">
			            <!-- <h1><?php echo $camp["parent"][$parentKey]["name"] ?><br>
			                <small><?php echo $camp["name"]?></small>
			            </h1> -->
			            <p class="description">
			            	<?php echo $shortDescProj ;?>
			            </p>
			        </div>
			    </div>

			    <p class="linkedWith text-yellow-k"><?php echo Yii::t("common","Campaign linked to") ?> : <a target="_blank" href="#page.type.<?php echo @$project['collection'] ?>.id.<?php echo (string)$project['_id'] ?>.view.directory.dir.crowdfunding"> <?php echo @$project['name'] ?></a></p>
			    <div class="campaign-progress">
							<div class="text-right" style="color:#05323e;">
								<strong><?php echo Yii::t("common","Amount to be funded")?> : <?php echo $rest_txt ?> / <?php echo $requested_text ?></strong>
							</div>
							<div class="progress tooltips addDonation" data-toggle="tooltip" data-placement="top" title="" data-original-title="Ajuster le montant" data-id="<?php echo $parentKey ?>"
							   data-name="<?php echo $camp['parent'][$parentKey]['name'] ?>"
							   data-type="<?php echo $camp['parent'][$parentKey]['type'] ?>"
							   data-id-camp="<?php echo $camp['_id'] ?>"
							   data-name-camp="<?php echo $camp['name']?>" data-iban="<?php echo $iban ?>" data-donationPlatform="<?php echo $donationPlatform ?>"
							   data-directdonation="<?php echo $directDonation?>" data-donationamount="<?php echo $camp['donationNumber'] ?>" data-pledgeamount="<?php echo $camp['pledgeNumber'] ?>" data-goal="<?php echo $camp['goal']?>" data-amount="">
								<div class="progress-bar donation-bar" role="progressbar"
									 style="width: <?php echo $donations_perc_txt ?>%"
									 aria-valuenow="<?php echo $donations_perc_txt ?>"
									 aria-valuemin="0" aria-valuemax="100"></div>
								<div class="progress-bar pledge-bar" role="progressbar"
									 style="width: <?php echo $pledges_perc_txt?>%"
									 aria-valuenow="<?php echo$pledges_perc_txt ?>"
									 aria-valuemin="0" aria-valuemax="100"></div>
							</div>
							<div class="col-xs-12">
			                    <div class="col-xs-12" style="padding-top: 5px"><div class="caption donation-label pull-left"></div><span class="caption-label pull-left"> <?php echo Yii::t("common","Donations")?> : <?php echo $donations_txt ?></span></div>
			                    <div class="col-xs-12" style="padding-top: 5px"><div class="caption pledge-label pull-left"></div><span  class="caption-label pull-left"> <?php echo Yii::t("common","Pledges")?> : <?php echo $pledges_txt ?></span></div>
							</div>
				</div>
				
				
				<div class="text-center">
							<a href="javascript:;" class="margin-top-20 btn btn-lg btn-primary addDonation"
							   data-id="<?php echo $parentKey ?>"
							   data-name="<?php echo $camp['parent'][$parentKey]['name'] ?>"
							   data-type="<?php echo $camp['parent'][$parentKey]['type'] ?>"
							   data-id-camp="<?php echo $camp['_id'] ?>"
							   data-name-camp="<?php echo $camp['name']?>" data-iban="<?php echo $iban ?>" data-donationPlatform="<?php echo $donationPlatform ?>"
							   data-directdonation="<?php echo $directDonation?>" data-donationamount="<?php echo $camp['donationNumber'] ?>" data-pledgeamount="<?php echo $camp['pledgeNumber'] ?>" data-goal="<?php echo $camp['goal']?>" data-logopath="<?php echo $this->module->assetsUrl ?>"><?php echo Yii::t("common","I donate for this resource")?></a>
				</div>
				<?php if(!empty($url)){ ?>
					<div class="col-xs-12 col-sm-10 col-sm-offset-1 col-lg-6 col-lg-offset-3 margin-top-20 margin-bottom-20" >
							<iframe width="100%" height="281" src="<?php echo $src ?>" frameborder="0" allowfullscreen=""></iframe>
					</div>
				<?php } ?>
				<div class="row">
					<div class="description col-xs-12">
						
							<!-- <h2 style="color:#0b5394;">Description de la campagne</h2> -->
							<p><?php echo $campaignDesc ?></p>
						
						
						<div class="text-center">
							<a href="javascript:;" class="btn btn-lg btn-primary addDonation"
							   data-id="<?php echo $parentKey ?>"
							   data-name="<?php echo $camp['parent'][$parentKey]['name'] ?>"
							   data-type="<?php echo $camp['parent'][$parentKey]['type'] ?>"
							   data-id-camp="<?php echo $camp['_id'] ?>"
							   data-name-camp="<?php echo $camp['name']?>" data-iban="<?php echo $iban ?>" data-donationPlatform="<?php echo $donationPlatform ?>"
							   data-directdonation="<?php echo $directDonation?>" data-donationamount="<?php echo $camp['donationNumber'] ?>" data-pledgeamount="<?php echo $camp['pledgeNumber'] ?>" data-goal="<?php echo $camp['goal']?>" data-logopath="<?php echo $this->module->assetsUrl ?>"><?php echo Yii::t("common","I donate for this resource")?></a>
						</div>
					</div>
				</div>
			</div>
		<?php }else{ 
			//var_dump($element);

			
			$civility=(isset($element["civility"])) ? $element["civility"] : Yii::t("common","No description registred");
			if($civility=="0")
				$civility="Monsieur";
			else if($civility=="1")
				$civility="Madame";
			else $civility="Autre";
			$surname=(isset($element["surname"])) ? $element["surname"] : Yii::t("common","No description registred");
			$donatorName=(isset($element["donatorName"])) ? $element["donatorName"] : Yii::t("common","No description registred");
			$email=(isset($element["email"])) ? $element["email"] : Yii::t("common","No description registred");
			$amount=(isset($element["amount"])) ? $element["amount"] : Yii::t("common","No description registred");
			$invoice=(isset($element["invoice"]) && $element["invoice"]=="true") ? Yii::t("common","Yes") : Yii::t("common","No");
			$behalf=(isset($element["behalf"])) ? $element["behalf"] : Yii::t("common","No description registred");
			$behalf= ($behalf=="true") ? Yii::t("common","Yes") : Yii::t("common","No");
			$invoiceDetails=(isset($element["invoiceDetails"])) ? $element["invoiceDetails"] : Yii::t("common","No description registred");
			$invoiceName=(isset($element["invoiceName"])) ? $element["invoiceName"] : Yii::t("common","No description registred");
			$invoiceAddress=(isset($element["invoiceAddress"])) ? $element["invoiceAddress"] : Yii::t("common","No description registred");
			$invoiceSiret=(isset($element["invoiceSiret"])) ? $element["invoiceSiret"] : Yii::t("common","No description registred");
			$publicDonationData=(isset($element["publicDonationData"])) ? $element["publicDonationData"] : Yii::t("common","No description registred");
			$publicDonationData= ($publicDonationData=="true") ? Yii::t("common","Yes") : Yii::t("common","No");
			?>
			<table class="col-xs-offset-2 col-xs-8 donationTable">
				<tr><td>Civilité</td><td><?php echo $civility ?></td></tr>
				<tr><td>Nom</td><td><?php echo $surname ?></td></tr>
				<tr><td>Prénom</td><td><?php echo $donatorName ?></td></tr>
				<tr><td>e-mail</td><td><?php echo $email ?></td></tr>
				<tr><td>Montant du don</td><td><?php echo $amount ?></td></tr>
				<tr><td>Demande de facture</td><td><?php echo $invoice ?></td></tr>
				<tr><td>Au nom d'une structure</td><td><?php echo $behalf ?></td></tr>
				<tr><td>Détails de facturation</td><td><?php echo $invoiceDetails ?></td></tr>
				<tr><td>Nom de la structure</td><td><?php echo $invoiceName ?></td></tr>
				<tr><td>Adresse du siège sociale</td><td><?php echo $invoiceAddress ?></td></tr>
				<tr><td>Siret</td><td><?php echo $invoiceSiret ?></td></tr>
				<tr><td>Données pouvant être diffusées</td><td><?php echo $publicDonationData ?></td></tr>
			</table>	
		<?php } ?>	
		
	</div>
</div>

<script type="text/javascript">

	var crowdAlone=<?php echo json_encode($element); ?>;

	jQuery(document).ready(function() {	
		setTitle("", "", crowdAlone.name);
		// urlCtrl.bindModalPreview = function(){
		// 	urlCtrl.previewCurrentlyLoading=false;
		// 	$(".btn-close-preview, .deleteThisBtn").click(function(e){
		// 		e.stopPropagation();
		// 		urlCtrl.closePreview();
		// 		directory.bindBtnElement();
		// 	});

		// 	$(".main-container").off().on("click",function() {
		// 		alert("tyo");
		// 		if( $("#modal-preview-coop").css("display") == "block" ){
		// 			//e.stopPropagation();
		// 			urlCtrl.closePreview();
					
		// 		}
		// 	});
		// };

		directory.bindBtnElement();

		// $(".main-container").off().on("click",function() {
		// 	alert("yo");
		// 	if( $("#modal-preview-coop").css("display") == "block" ){
		// 		urlCtrl.closePreview();	
		// 		directory.bindBtnElement();
		// 	}
		// });

		// $(".addDonation").off().on("click",function(){
		// 	var directDonation=$(this).data("directdonation");
		// 	var dataEdit={};
		// 	if (typeof contextData =="undefined" || !notNull(contextData)){
		// 		dataEdit.receiver={};
		// 		var id=$(this).data("id");
		// 		dataEdit.receiver[id]={};
		// 		dataEdit.receiver[id].type = $(this).data("type");
		// 		dataEdit.receiver[id].name = $(this).data("name");

		// 	}

		// 	dataEdit.parent={};
		// 	var idCamp=$(this).data("id-camp");
		// 	dataEdit.parent[idCamp] = {};
		// 	dataEdit.parent[idCamp].type = "crowdfunding";
		// 	dataEdit.parent[idCamp].name = $(this).data("name-camp");
			
		// 	if(directDonation=="true"){
		// 	    urlCtrl.modalExplain({
		//       		icon : "money",
		//       		name : "DON / PROMESSE DE DON",
		//       		//title : "heyeyeehey",
		//       		text : "<ul style='list-style-type:none'><li>Don : soutenir directement la campagne de financement</li><br><li>Promesse de don : Différer le don et s'engager à le verser ultérieurement lorsque vous serez sollicité.e</li></ul>",
		//       		link : {
		//       			pledge : {
		//       				label : "Promesse",
		//       				icon : "money",
		//       				link : "javascript:;",
		//       				onclick : 'dyFObj.openForm("donation",null,{"type":"pledge"})',
		//       				dataExtend : dataEdit
		//       			},
		//       			donation : {
		//       				label : "Dons",
		//       				icon : "dollar",	      			
		//       				link : "javascript:;",
		//       				onclick : 'dyFObj.openForm("donation")'
		//       			}
		//       		}
		//       	})
		//     }  	
	  
			
		// });
			
		
	});
</script>