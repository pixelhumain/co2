<style>
	#modal-preview-coop{
		overflow: auto;
		z-index: 1000 !important;
	}
	#poi .title{
		padding-bottom: 15px !important;
    	border-bottom: 1px solid rgba(150,150,150, 0.3);
	}
	.short-description{
		font-size:20px;
		text-align: justify;
	}
	.description-preview{
		text-align: justify;
	}
	.description-preview.activeMarkdown p, .description-preview.activeMarkdown li{
		font-size: 14px !important;
	}
	.tags-content .badge.tags-poi-preview{
		padding: 3px 15px!important;
	    margin-right: 5px;
	}

	.donationTable td{
		padding: 5px;
    	border-bottom: solid;
	}
</style>
<?php

// var_dump($element);exit;
if($element["type"]=="campaign"){
	$camp=$element;

	reset($camp["parent"]);
	$parentKey=key($camp["parent"]);
	$iban = isset($camp["iban"]) ? $camp["iban"] : "" ;
	$donationPlatform = isset($camp["donationPlatform"]) ? $camp["donationPlatform"] : "" ;
	$directDonation= isset($camp["directDonation"]) ? $camp["directDonation"] : "false";
	//$campaignDesc=isset($campaign["description"]) ? $campaign["description"] : "Aucune description n'a été renseignée";


	$currency_symbol = "€";
	$rest = $camp["goal"] - $camp["donationNumber"] - $camp["pledgeNumber"];
	$rest_txt = (string)$rest . $currency_symbol;
	$donations_txt = (string)$camp["donationNumber"] . $currency_symbol;
	$pledges_txt = (string)$camp["pledgeNumber"] . $currency_symbol;
	$requested_text = (string)$camp["goal"] . $currency_symbol;

	// Compute progress percentages
	$donations_perc = round($camp["donationNumber"] / $camp["goal"] * 100);
	$pledges_perc = "";
	if ($rest <= 0) {
		$pledges_perc = 100 - $donations_perc;
	} else {
		$pledges_perc = round($camp["pledgeNumber"] / $camp["goal"] * 100);
	}

	// Clamp progress percentages
	$min_perc = 2;
	if ($donations_perc <= $min_perc) {
	    $donations_perc = $min_perc;
	    $pledges_perc = min($pledges_perc, 100 - $min_perc);
	}
	if ($pledges_perc <= $min_perc) {
	    $pledges_perc = $min_perc;
	    $donations_perc = min($donations_perc, 100 - $min_perc);
	}

	$donations_perc_txt = (string)$donations_perc;
	$pledges_perc_txt = (string)$pledges_perc;

	// $eltId=key($element['parent']);
	// $eltType= $element['parent'][$eltId]['type'];
	// $eltName= $element['parent'][$eltId]['name'];
}	
	?>

<div class="margin-top-25 margin-bottom-50 col-xs-12">
	<div class="col-xs-12 no-padding">
		<?php if($element["type"]=="campaign"){ ?>
			<button class="btn btn-default pull-right btn-close-preview" style="margin-top:-15px;">
				<i class="fa fa-times"></i>
			</button>
			<?php 

			if( $element["creator"] == Yii::app()->session["userId"] || 
					  Authorisation::canEditItem( Yii::app()->session["userId"], "crowdfunding", $id, @$element["parentType"], @$element["parentId"] ) ){ ?>
				
				<button class="btn btn-default pull-right margin-right-10 text-red deleteThisBtn" 
						data-type="crowdfunding" data-id="<?php echo $id ?>" style="margin-top:-15px;">
					<i class=" fa fa-trash"></i>
				</button>
				<button class="btn btn-default pull-right margin-right-10 btn-edit-preview" data-type="crowdfunding" data-id="<?php echo $id ?>" 
				data-subtype="<?php echo $element["type"] ?>" style="margin-top:-15px;">
					<i class="fa fa-pencil"></i>
				</button>
			<?php } ?>
			<div class="campaign-detail container col-xs-12">
			    <div class="row">
			        <div class="col-xs-12">
			            <h1><?php echo $camp["parent"][$parentKey]["name"] ?><br>
			                <small><?php echo $camp["name"]?></small>
			            </h1>
			        </div>
			    </div>
				<div class="row">
					<div class="description col-xs-12">
						<?php if(isset($campaign["description"])){	?>
							<h2>Description de la campagne</h2>
							<p><?php echo  $campaign["description"] ?></p>
						<?php }else{ ?>
							<p>Aucune description n'a été renseignée.</p>
						<?php } ?>	
						<div class="campaign-progress">
							<div class="text-right">
								<strong>Reste à financer : <?php echo $rest_txt ?> / <?php echo $requested_text ?></strong>
							</div>
							<div class="progress">
								<div class="progress-bar donation-bar" role="progressbar"
									 style="width: <?php echo $donations_perc_txt ?>%"
									 aria-valuenow="<?php echo $donations_perc_txt ?>"
									 aria-valuemin="0" aria-valuemax="100"></div>
								<div class="progress-bar pledge-bar" role="progressbar"
									 style="width: <?php echo $pledges_perc_txt?>%"
									 aria-valuenow="<?php echo$pledges_perc_txt ?>"
									 aria-valuemin="0" aria-valuemax="100"></div>
							</div>
							<div>
			                    <div><span class="label donation-label">Dons : <?php echo $donations_txt ?></span></div>
			                    <div><span class="label pledge-label">Promesses : <?php echo $pledges_txt ?></span></div>
							</div>
						</div>
						<div class="text-center">
							<a href="javascript:;" class="btn btn-lg btn-primary addDonation"
							   data-id="<?php echo $parentKey ?>"
							   data-name="<?php echo $camp['parent'][$parentKey]['name'] ?>"
							   data-type="<?php echo $camp['parent'][$parentKey]['type'] ?>"
							   data-id-camp="<?php echo $camp['_id'] ?>"
							   data-name-camp="<?php echo $camp['name']?>" data-iban="<?php echo $iban ?>" data-donationPlatform="<?php echo $donationPlatform ?>"
							   data-directdonation="<?php echo $directDonation?>">Je cofinance cette campagne !</a>
						</div>
					</div>
				</div>
			</div>
		<?php }else{ 
			//var_dump($element);
			$civility=(isset($element["civility"])) ? $element["civility"] : "non renseigné";
			$surname=(isset($element["surname"])) ? $element["surname"] : "non renseigné";
			$donatorName=(isset($element["donatorName"])) ? $element["donatorName"] : "non renseigné";
			$email=(isset($element["email"])) ? $element["email"] : "non renseigné";
			$amount=(isset($element["amount"])) ? $element["amount"] : "non renseigné";
			$invoice=(isset($element["invoice"]) && $element["invoice"]=="1") ? "Oui" : "Non";
			?>
			<table class="col-xs-offset-2 col-xs-8 donationTable">
				<tr><td>Civilité</td><td><?php echo $civility ?></td></tr>
				<tr><td>Nom</td><td><?php echo $surname ?></td></tr>
				<tr><td>Prénom</td><td><?php echo $donatorName ?></td></tr>
				<tr><td>e-mail</td><td><?php echo $email ?></td></tr>
				<tr><td>Montant du don</td><td><?php echo $amount ?></td></tr>
				<tr><td>Demande de facture</td><td><?php echo $invoice ?></td></tr>
			</table>	
		<?php } ?>	
		
	</div>
</div>

<script type="text/javascript">

	var crowdAlone=<?php echo json_encode($element); ?>;

	jQuery(document).ready(function() {	
		setTitle("", "", crowdAlone.name);
		directory.bindBtnElement();

		// $(".addDonation").off().on("click",function(){
		// 	var directDonation=$(this).data("directdonation");
		// 	var dataEdit={};
		// 	if (typeof contextData =="undefined" || !notNull(contextData)){
		// 		dataEdit.receiver={};
		// 		var id=$(this).data("id");
		// 		dataEdit.receiver[id]={};
		// 		dataEdit.receiver[id].type = $(this).data("type");
		// 		dataEdit.receiver[id].name = $(this).data("name");

		// 	}

		// 	dataEdit.parent={};
		// 	var idCamp=$(this).data("id-camp");
		// 	dataEdit.parent[idCamp] = {};
		// 	dataEdit.parent[idCamp].type = "crowdfunding";
		// 	dataEdit.parent[idCamp].name = $(this).data("name-camp");
			
		// 	if(directDonation=="true"){
		// 	    urlCtrl.modalExplain({
		//       		icon : "money",
		//       		name : "DON / PROMESSE DE DON",
		//       		//title : "heyeyeehey",
		//       		text : "<ul style='list-style-type:none'><li>Don : soutenir directement la campagne de financement</li><br><li>Promesse de don : Différer le don et s'engager à le verser ultérieurement lorsque vous serez sollicité.e</li></ul>",
		//       		link : {
		//       			pledge : {
		//       				label : "Promesse",
		//       				icon : "money",
		//       				link : "javascript:;",
		//       				onclick : 'dyFObj.openForm("donation",null,{"type":"pledge"})',
		//       				dataExtend : dataEdit
		//       			},
		//       			donation : {
		//       				label : "Dons",
		//       				icon : "dollar",	      			
		//       				link : "javascript:;",
		//       				onclick : 'dyFObj.openForm("donation")'
		//       			}
		//       		}
		//       	})
		//     }  	
	  
			
		// });
			
		
	});
</script>