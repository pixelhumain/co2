<style type="text/css">
	.social-banner, .error-500,.social-question{
		color: white;
		text-transform: uppercase;
		font-weight: 800;
		font-size: 50px;
		float: left;
		width: 100%;
		text-align: center;
	}
	.error-500{
		font-size: 60px;
		color: red;
	}
	.social-question{
		width: 60%;
		margin-left: 20%;
		font-weight: 100;
		font-size: 40px;
		text-transform: initial;
		font-style: italic;
		margin-top: 30px;
		margin-bottom: 30px;
		quotes: "„" "“" "‚" "‘";
	}
</style>
<div class="main" style="background-color: black; padding-top: 15%; position: fixed; top:0;bottom:0px;left:0px;right:0px;">

<span class="text-white col-xs-12 uppercase text-center error-500" style="font-size: 60px"> Error 500</span><br/><br/>
<italic class="text-white col-xs-12 uppercase text-center social-question">Est-ce que quand on a les moyens, tous les moyens sont bons ?</italic><br/><br/>
<span class="text-white col-xs-12 uppercase text-center social-banner">Communecter is bleeding</span>
</div> 