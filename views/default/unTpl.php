<?php
    $documentTitle = $context['name'] ?? Yii::t('common', 'Communecter, the free and connected social network');
?>
<style>
	/* .pageContent {
		display: flex;
		align-items: center;
		justify-content: center;
	} */

	.content-unauthorised-page {
		display: flex;
		align-items: center;
		justify-content: center;
		text-align: center;
		flex-direction: column;
		position: relative;
	}

	.content-unauthorised-page .cloud {
		position: absolute;
		min-width: 750px;
		height: auto;
	}

	.text-unauthorized-access {
		z-index: 2000;
		padding: 0px 50px;
	}

	.text-unauthorized-access .miniCloud {
		position: absolute;
	}

	.text-unauthorized-access .miniCloud:nth-child(1) {
		top: -130px;
		left: 200px;
		width: 140px;
  		animation: slideinleft 250s linear infinite;
	}

	.text-unauthorized-access .miniCloud:nth-child(2) {
		top: -90px;
		left: 900px;
		width: 90px;
		height: auto;
  		animation: slideinright 150s linear infinite;
	}

	.text-unauthorized-access .miniCloud:nth-child(3) {
		width: 130px;
		height: auto;
		right: 0;
		bottom: -80px;
		height: auto;
  		animation: slideinright 200s linear infinite;
	}

	.text-unauthorized-access .handStop {
		margin: 40px 0;
		animation: fadein 1000ms infinite;
	}

	.text-unauthorized-access h1 {
		font-weight: 800;
		font-size: 20px;
		letter-spacing: 4px;
		color: #ea4335;
		margin: 0;
	}

	.text-unauthorized-access h3 {
		font-size: 15px;
		font-weight: 300;
	}

	.text-unauthorized-access button {
		margin-top: 10px;
		padding: 10px 20px;
		font-weight: 800;
		min-width: 150px;
		color: #fff;
		background-color: #9fbd38;
		text-transform: uppercase;
	}

	.text-unauthorized-access button:hover {
		background-color: #fff;
		border: 1px solid #9fbd38;
		color: #9fbd38;
	}

	@keyframes fadein {
		0% {
			opacity: 0;
		}

		100% {
			opacity: 1;
		}
	}

	@keyframes slideinleft {
		0% {
			transform: translate(0);
		}

		50% {
			transform: translateX(900px);
		}

		100% {
			transform: translateX(0);
		}
	}

	@keyframes slideinright {
		0% {
			transform: translateX(0);
		}

		50% {
			transform: translateX(-900px);
		}

		100% {
			transform: translateX(0);
		}
	}
</style>

<div class="content-unauthorised-page">
	<img src="<?php echo Yii::app()->getModule("co2")->assetsUrl ?>/images/illustration_unauth_access.svg" alt="cloud" class="cloud">
	<div class="text-center text-unauthorized-access">
		<img src="<?php echo Yii::app()->getModule("co2")->assetsUrl ?>/images/mini_cloud.svg" alt="mini cloud" class="miniCloud" />
		<img src="<?php echo Yii::app()->getModule("co2")->assetsUrl ?>/images/mini_cloud.svg" alt="mini cloud" class="miniCloud" />
		<img src="<?php echo Yii::app()->getModule("co2")->assetsUrl ?>/images/mini_cloud.svg" alt="mini cloud" class="miniCloud" />
		<img src="<?php echo Yii::app()->getModule("co2")->assetsUrl ?>/images/hand_stop.png" alt="hand_stop" class="handStop" />
		<h1><?php if(isset($msg)){echo $msg;}else{echo "Please Login First";} ?> </h1>
		<!-- <i class='fa <?php //echo $icon ?> fa-4x '></i><br/> -->
		<p style="font-size:20px;"><?php if(isset($text)){echo $text;}else{echo Yii::t('survey', 'You must sign in to access in this features');} ?></p>
		<?php if(isset($btn)){echo $btn;} ?>
	</div>
</div>

<script>
	$(function(){
		if (!userId)
			$('#modalLogin').modal("show");
        setTitle(JSON.parse(JSON.stringify(<?= json_encode($documentTitle) ?>)));
	})
</script>