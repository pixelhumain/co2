<style type="text/css">
	.legacyMentions h4{
		color:#6d802e;
	}
</style>
<div class="col-xs-12 legacyMentions margin-top-50" style="text-align: justify; padding:0% 20%;">
	<h4>Toute personne peut s’exprimer librement sur la plateforme sous réserve de respecter la présente charte.
	</h4><br/><br/><br/>

	<b>En m’inscrivant sur la plateforme:</b><br/><br/>

	- J’adhère à la démarche de participation:je cherche à améliorer les idées, projets, propositions formulées, en donnant mon avis, et propose moi-même de nouvelles solutions ;<br/>
	- J’atteste de ma pleine et entière adhésion aux valeurs et aux règles du débat démocratique.<br/><br/><br/>
	<b>En conséquence, je m’engage en tant que participant:</b><br/><br/>

	- À ne publier aucune information volontairement erronée, tronquée, ou hors sujet ;<br/>
	- À reconnaître à chacun le droit d’avoir une opinion différente de la mienne et à la respecter ;<br/>
	- À n’exprimer, diffuser, ou partager aucun contenu offensant ou contraire à la loi ;<br/>
	- À signaler aux modérateurs tous les contenus en infraction avec la présente Charte.<br/><br/><br/>

	<b>Les contributions dont le comportement est contraire à cette Charte sont susceptibles d’être modérées ou supprimées sans préavis.<br/> 
	L’équipe de modération se réserve donc le droit de supprimer:</b><br/><br/>

	- Les messages à vocation publicitaire, promotionnelle ou commerciale ;<br/>
	- Les contributions prosélytes (politique, sectaire, religieuse, sexuelle, etc.) - sont considérées comme prosélytes les contributions qui invitent à un acte de mobilisation (signer une pétition, participer à une manifestation, etc.) ou qui ont pour vocation de susciter l'adhésion ;<br/>
	- Les contributions portant atteinte à autrui, c'est-à-dire attaquant une personne ou un groupe de personnes en raison de leurs caractéristiques propres ;<br/>
	- Les contributions dont le propos est injurieux, grossier, diffamatoire, irrespectueux, agressif, violent, raciste,xénophobe,homophobe, ou faisant l’apologie des crimes de guerre ;<br/>
	- Les contributions renvoyant des sites internet ou des contenus dont la teneur ne respecterait pas la présente charte.<br/><br/><br/><br/>
	Chaque personne peut contribuer tant qu’elle s’engage à respecter au préalable les règles de la Charte. En cas de violation grave ou répétée de la Charte, l’utilisateur est passible de voir son compte suspendu ou supprimé.
</div>