
<style type="text/css">
	.legacyMentions h3{
		color:#6d802e;
		text-decoration: underline;
	}
</style>
<div class="col-xs-12 legacyMentions" style="text-align: justify; padding:0% 15%;">
	<h3>Politique de confidentialité et données personnelles</h3><br/>
	Lorsque des données présentes sur ce site ont un caractère nominatif, les utilisateurs doivent en faire un usage conforme aux réglementations en vigueur et aux recommandations de la Commission nationale de l'informatique et des libertés (CNIL).<br/><br/>
	Conformément à la loi Informatique et Liberté 78-17 du 6 janvier 1978 modifiée, vous disposez d'un droit d'opposition (art. 38), d'accès (art. 39), de rectification ou de suppression (art. 40) des données qui vous concernent. Vous pouvez exercer ce droit en vous adressant à l'association Open Atlas.<br/><br/>
	<b/>Contact mail: contact@communecter.org</b><br/>
	<br/>Ou<br/><br/>
	Par voie postale :<br/>
	Association Open Atlas<br/>
	56 rue Andy<br/>
	97460, ST PAUL<br/>
	La réunion (fr) <br/>


	Toutes les données personnelles qui sont recueillies sont traitées avec la plus stricte confidentialité. En particulier, Open Atlas s’engage à respecter la confidentialité des messages e-mails transmis au moyen d’une messagerie électronique.<br/><br/>

	<h4>Les mails électroniques envoyés par le système de communecter.org sont paramétrables dans leur ensemble</h4>
	
	Les adresses électroniques recueillies dans le cadre des formulaires d'abonnement ne sont utilisées que pour l'envoi des informations pour lesquelles l'usager s'est expressément abonné. Il est possible à tout moment de se désabonner en ligne.<br/><br/>
	Toutes ces configurations sont accesibles et désactivables sur le profil de l'utilisateur.<br/>
	Dans ce même espace, vous retrouverez toutes les informations et la donnée référents à votre compte utilisateur ainsi que la fonctionnalité de suppression de compte:<br/>
	<a href="<?php echo $this->module->assetsUrl ?>/images/CGU/settings_user.png" class="thumb-info" data-lightbox="all">
		<img src="<?php echo $this->module->assetsUrl ?>/images/CGU/settings_user.png" class="col-xs-12 col-lg-6 margin-top-20"/>
	</a>
	<a href="<?php echo $this->module->assetsUrl ?>/images/CGU/menu_user_settings.png" class="thumb-info" data-lightbox="all">
		<img src="<?php echo $this->module->assetsUrl ?>/images/CGU/menu_user_settings.png" class="col-xs-12 col-lg-6 margin-top-20"/>
	</a>
	 
	<h3 class="margin-top-20">Informations sur les cookies</h3>
	Le site utilise certains cookies uniquement destinés à l'ergonomie et l'usage de la plateforme.<br/><br/>
	<h4>Mesure d'audience</h4>
	La navigation des usagers sur le communecter (pages d'entrée, durée et sources des visites, fréquence des visites, etc.) sont analysé par enregistrement en base de donnée et de manière anonyme.<br/><br/>
	<h4>les traceurs destinés à l’authentification auprès d’un service et la persistance de la connexion d'un utilisateur</h4>
	les traceurs destinés à garder en mémoire le contenu d’un panier d’achat sur un site marchand ou à facturer, à l’utilisateur, le(s) produit(s) et/ou service(s) acheté(s) ;
	<h4>Personnalisation de l'interface et des préférences de recherches géographiques ou thématiques en mode déconnecté</h4>

	<h4>Paramétrage</h4>
	Ces traceurs ne peuvent pas, techniquement, être désactivés depuis le site. Vous pouvez néanmoins vous opposer à l'utilisation de ces traceurs, exclusivement en paramétrant votre navigateur. Ce paramétrage dépend du navigateur que vous utilisez, mais il est en général simple à réaliser : en principe, vous pouvez soit activer une fonction de navigation privée soit uniquement interdire ou restreindre les traceurs (cookies).<br/><br/>
	Attention, il se peut que des traceurs aient été enregistrés sur votre périphérique avant le paramétrage de votre navigateur : dans ce cas il est possible de supprimer les cookies depuis les options de votre navigateur.<br/><br/>

	<h4>Géolocalisation</h4>
	L’utilisateur se verra informé si communecter.org propose un service de géolocalisation permettant une recherche intéractive autour de lui. Ainsi l'application demandera l'accord à l'utilisateur afin d'utiliser les
	technologies de géolocalisation.
	Les données collectées peuvent inclure des coordonnées GPS, l’adresse IP du
	terminal utilisé, ou des informations concernant le lieu où se trouve l’utilisateur
	lorsqu’il utilise le Site Internet.
	Conformément à la Loi Informatique et libertés du 6 janvier 1978, l’utilisateur
	peut exercer les droits prévus à l’article 4.4 des présent

	<h3>Accessibilité du site</h3>
	Le site Communecter.org  utilise des outils Open Source, et a été conçu pour être pleinement accessible, en accord avec les principes d'accessibilité de contenu web.<br/><br/>
	Nous utilisons HTML 5 et CSS 3 conformément aux spécifications édictées par le W3C car nous accordons une grande importance à la facilité d'utilisation et à l'accessibilité. Nous avons pour philosophie de nous adresser à tous nos visiteurs quels qu'ils soient, et dans cette optique nous appliquons du mieux possible le principe de séparation entre le code HTML et la présentation CSS.
</div>