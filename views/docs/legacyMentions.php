
<style type="text/css">
	.legacyMentions h3{
		color:#6d802e;
		text-decoration: underline;
	}
</style>
<div class="col-xs-12 legacyMentions" style="text-align: justify; padding:0% 15%;">
	<div class="col-xs-12 col-sm-6">
		<h3>Informations Éditeurs</h3><br/>
		<h4>Service gestionnaire</h4>

		Open Atlas<br/>
		Association de loi 1901<br/>
		<a href="https://doc.co.tools/books/1---le-projet/page/lassociation-open-atlas" style="color:red" target="_blank">Voir les status officiels de l'association </a><br/>
		56 rue Andy<br/>
		97460, ST PAUL<br/>
		La réunion (fr) <br/>
		SIRET : 513 381 830 00027<br/>
		Contact:  contact@communecter.org<br/>
		<br/>
		<h4>Direction et coordination du projet:</h4> le collectif indépendant Open R&D assure le développement du code chez Open Atlas animé par Tibor Katelbach et Clément Damiens.<br/><br/>
		<h4>Gestion éditoriale et graphisme:</h4> Association Open Atlas <br/>

	</div>
	<div class="col-xs-12 col-sm-6">
		<h3>Informations techniques</h3><br/>
		<h4>Logiciel libre internet :</h4>
		www.communecter.org<br/><br/>
		<h4>Hébergement du site</h4>
		OVH SAS <br/>
		2, rue Kellermann, 59100 Roubaix.<br/>
		<br/>
		<h4>DPO:</h4> Association Open Atlas<br/>
		Référent : Thomas Craipeau<br/>
		Contact : admin@co.tools<br/>
		<br/>
	</div>
	<div class="col-xs-12 margin-top-20">
		<h3>Propriété intellectuelle</h3><br/>

		Les contenus présentés sur ce site sont soumis à la législation relative au droit des informations publiques et sont couverts par le droit d'auteur. Toute réutilisation des vidéos, des photographies, des créations graphiques, des illustrations et des lexiques, ainsi que de l'ensemble des contenus éditoriaux produits pour l'animation éditoriale du site est conditionnée à l'accord de l'auteur. Le répertoire des informations publiques de l'association Open Atlas précise la charte de réutilisation des informations publiques.<br/><br/>

		Conformément au droit public de la propriété intellectuelle et notamment selon l'article L122-5 du Code de la propriété intellectuelle, les ''documents officiels'' sont librement réutilisables.<br/><br/>

		Le contenu produit par l'association Open Atlas est sous licence Creative Commons CC BY-SA ainsi que le code constituant le logiciel. Ainsi Les ressources ainsi développées par Open Atlas sont réutisable et duplicable à des fins non commerciales.

		La réutilisation du contenu non commerciale, et notamment pédagogique, est autorisée à la condition de respecter l'intégrité des informations et de n'en altérer ni le sens, ni la portée, ni l'application et d'en préciser l'origine et la date de publication.<br/>

		Les informations ne peuvent être utilisées à des fins commerciales ou promotionnelles sans l'autorisation expresse et l'obtention d'une licence de réutilisation des informations publiques. Est considérée comme réutilisation à des fins commerciales ou promotionnelles, l'élaboration à partir des informations publiques, d'un produit ou d'un service destiné à être mis à disposition de tiers, à titre gratuit ou onéreux.<br/><br/>
		L'utilisation des marques déposées utilisées sur ce site sur tout autre support ou réseau est interdite.
		<br/><br/>


		<h3>Liens hypertextes</h3>

		Tout site public ou privé est autorisé à établir, sans autorisation préalable, un lien vers les informations diffusées par Open Atlas. En revanche, les pages du site ne doivent pas être imbriquées à l’intérieur des pages d’un autre site.<br/><br/>
		L’autorisation de mise en place d’un lien est valable pour tout support, à l’exception de ceux diffusant des informations à caractère polémique, pornographique, xénophobe ou pouvant, dans une plus large mesure porter atteinte à la sensibilité du plus grand nombre.<br/><br/>

		<h3>Crédits photographiques</h3>
		Les photos et les graphismes présents sur ce site proviennent de sources différentes et sont fournies par les utilisateurs et restent de leur propriété.<br/>
		Toute image publiée sur le site est automatiquement considérée comme CC-BY-SA.<br/><br/>


		<h3>Responsabilité de l'association</h3>

		Les informations proposées sur ce site le sont au titre de service rendu au public. Malgré tout le soin apporté à l’actualisation des textes officiels et à la vérification des contenus, les documents mis en ligne ne sauraient engager la responsabilité de l'association.<br/><br/>

		Les informations et/ou documents disponibles sur ce site sont susceptibles d’être modifiés à tout moment, et peuvent faire l’objet de mises à jour.<br/><br/>

		Open Atlas ne pourra en aucun cas être tenu responsable de tout dommage de quelque nature qu’il soit résultant de l’interprétation ou de l’utilisation des informations et/ou documents disponibles sur ce site.<br/><br/>

		<h3>Les services liés à communecter.org</h3><br/>
		Communecter.org propose un panel d'outils complémentaire.<br/>
		<b>Ses applications mobiles:</b>
		<ul>
			<li><b>Comobi</b>, communecter mobile</li>
			<li><b>Oceco</b>, système de gestion et suivi de projets</li>
		</ul>
		<b>Stack d'outils open source:</b>
		<ul>
			<li>Wekan</li>
			<li>Rocket Chat</li>
			<li>Nextcloud</li>
			<li>Cotools</li>
			<li>Hackmd</li>
			<li>Peertube</li>
			<li>Jitsi</li>
		</ul>
		<h3>L'OCDB - Open Collective Data Base</h3>
		Une base de collective et ouverte avec un service d'API et des protocoles de connexion décentralisé<br/>
		Le but est de promouvoir et faire de la pédagogie sur la données ouvertes et d'accompagner nos partenaires à développer leur politique dans ce sens.<br/>
		Bien sûr, Open Atlas s'attache à l'indépendance des données personnelles de chaque utilisateur et sa non-utilisation.<br/>

		<a href="#confidentialityRules" style="color:red;" target="_blank">Retrouvez ici l'ensemble de la politique de confidentialité des données relatives aux utilisateurs</a><br/>


		<h3>Disponibilité du site</h3>
		L’éditeur s’efforce de permettre l’accès au site 24 heures sur 24, 7 jours sur 7, sauf en cas de force majeure ou d’un événement hors du contrôle de Open Atlas, et sous réserve des éventuelles pannes et interventions de maintenance nécessaires au bon fonctionnement du site et des services.<br/><br/>
		Par conséquent, Open Atlas ne peut garantir une disponibilité du site et/ou des services, une fiabilité des transmissions et des performances en terme de temps de réponse ou de qualité. Il n’est prévu aucune assistance technique vis-à-vis de l’utilisateur que ce soit par des moyens électronique ou téléphonique.<br/><br/>
		La responsabilité de l’éditeur ne saurait être engagée en cas d’impossibilité d’accès à ce site et/ou d’utilisation des services.<br/><br/>
		L'association Open Atlas peut être amené à interrompre le site ou une partie des services, à tout moment sans préavis, le tout sans droit à indemnités. L’utilisateur reconnaît et accepte que Open Atlas ne soit pas responsable des interruptions, et des conséquences qui peuvent en découler pour l’utilisateur ou tout tiers.<br/><br/>


		<h3>Droit applicable</h3>

		Quel que soit le lieu d’utilisation, le présent site est régi par le droit français. En cas de contestation éventuelle, et après l’échec de toute tentative de recherche d’une solution amiable, les tribunaux français seront seuls compétents pour connaître de ce litige.<br/><br/>
		Pour toute question relative aux présentes conditions d’utilisation du site, vous pouvez nous par mail ou par voie postal aux coordonnées indiquées en début de document<br/>


		<h3> Acceptation des mentions légales et conditions d’utilisation </h3>
		L’utilisateur reconnaît avoir pris connaissance des conditions d’utilisation, au moment de sa connexion vers le site du communecter et déclare expressément les accepter sans réserve.<br/><br/>

		<h3> Modifications des conditions générales d’utilisation</h3>
		Open Atlas se réserve la possibilité de modifier, à tout moment et sans préavis, les présentes conditions d’utilisation afin de les adapter aux évolutions du site et/ou de son exploitation.
	</div>
</div>