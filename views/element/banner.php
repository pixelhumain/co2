<?php

 $bannerConfig = $this->appConfig["element"]["banner"]; ?>
<style>
	#uploadScropResizeAndSaveImage i {
		position: inherit !important;
	}

	#uploadScropResizeAndSaveImage .close-modal .lr,
	#uploadScropResizeAndSaveImage .close-modal .lr .rl {
		z-index: 1051;
		height: 75px;
		width: 1px;
		background-color: #2C3E50;
	}

	#uploadScropResizeAndSaveImage .close-modal .lr {
		margin-left: 35px;
		transform: rotate(45deg);
		-ms-transform: rotate(45deg);
		-webkit-transform: rotate(45deg);
	}

	#uploadScropResizeAndSaveImage .close-modal .rl {
		transform: rotate(90deg);
		-ms-transform: rotate(90deg);
		-webkit-transform: rotate(90deg);
	}

	#uploadScropResizeAndSaveImage .close-modal {
		position: absolute;
		width: 75px;
		height: 75px;
		background-color: transparent;
		top: 25px;
		right: 25px;
		cursor: pointer;
	}

	.blockUI,
	.blockPage,
	.blockMsg {
		padding-top: 0px !important;
	}

	#banner_element:hover {
		color: #0095FF;
		background-color: white;
		border: 1px solid #0095FF;
		border-radius: 3px;
		margin-right: 2px;
	}

	#banner_element {
		background-color: #0095FF;
		color: white;
		border-radius: 3px;
		margin-right: 2px;
		display: none;
	}

	.header-address {
		font-size: 14px;
		padding-left: 5px;
	}

	#contentBanner img {
		min-height: 280px;
	}

	.badgePH {
		padding: 5px 10px;
	}

	#contentBanner {
		min-height: 280px;
	}

	.modify-campaign:hover {
		cursor: pointer;
		text-decoration: underline;
	}

	.btn-crowd:hover {
		cursor: pointer;
		text-decoration: underline;
		font-weight: 900;
	}

	.crowdfunding-badge {
		display: block;
		/* background: red; */
		color: white;
		/* height: 30px; */
		/* position: fixed; */
		/* top: 25%; */
		/* width: 10%; */
		padding: 5px;
		right: 0px;
		/* width: 200px; */
		border-radius: 10px 0 0 10px;
		z-index: 100;
		/* background-color: #3f4e58; */
		text-align: center;
		border: solid 1px white;
		/*max-width: 33%;*/
		/* position: relative; */
		float: right;
		margin-right: -1px;
	}

	.close-campaign-popup:hover {
		cursor: pointer;
	}
</style>

<?php


$campaign = null;
if (isset($element["preferences"]["crowdfunding"]) && $element["preferences"]["crowdfunding"] == true) {
	$campaign = Crowdfunding::getCampaignByParentId($element["_id"]);
}

$thumbAuthor = filter_var($element["profilImageUrl"], FILTER_VALIDATE_URL) ? HtmlHelper::assetsUrlImg(@$element['profilImageUrl']):$element['profilImageUrl'];
// $thumbAuthor =  @$element['profilImageUrl'] ? Yii::app()->createUrl('/'.@$element['profilImageUrl']) : "";
$icon = (isset($element["type"]) && Element::getFaIcon($element["type"]) !== false) ? Element::getFaIcon($element["type"]) : Element::getFaIcon($element["collection"]);
$iconColor = (isset($element["type"]) && Element::getColorIcon($element["type"]) !== false) ? Element::getColorIcon($element["type"]) : Element::getColorIcon($element["collection"]);
if (isset($pageConfig["banner"]) && !empty($pageConfig["banner"])) {
	if (is_string($pageConfig["banner"])) {
		// if($pageConfig["banner"]==""){
		// }
		// else{
		echo $this->renderPartial(
			$pageConfig["banner"],
			array(
				"iconColor" => $iconColor,
				"icon" => $icon,
				"campaign" => @$campaign,
				"pageConfig" => $pageConfig,
				"linksBtn" => $linksBtn,
				"invitedMe" => $invitedMe,
				"element" => $element,
				"badgesToShow" => $badgesToShow,
				"edit" => $edit,
				"openEdition" => $openEdition
			)
		);
		//	}
	} else {
?>
		<div class="col-xs-12 col-md-12 col-sm-12 col-lg-12 text-left no-padding pull-left" id="col-banner">

			<?php echo $this->renderPartial("co2.views.element.modalBanner", array(
				"edit" => $edit,
				"openEdition" => $openEdition,
				"profilBannerUrl" => filter_var(@$element["profilBannerUrl"], FILTER_VALIDATE_URL) ?  HtmlHelper::assetsUrlImg(@$element["profilBannerUrl"]): @$element["profilBannerUrl"],
				"element" => $element
			)); ?>
			<div id="contentBanner" class="col-md-12 col-sm-12 col-xs-12 no-padding">
				<?php
				if (@$element["profilBannerUrl"] && !empty($element["profilBannerUrl"])) {
					$imgHtml = '<img class="col-md-12 col-sm-12 col-xs-12 no-padding img-responsive" alt="' . Yii::t("common", "Banner") . '"
				src="' . (filter_var($element["profilBannerUrl"], FILTER_VALIDATE_URL) ?   HtmlHelper::assetsUrlImg(@$element["profilBannerUrl"]) : $element["profilBannerUrl"]). '">';
					if (@$element["profilRealBannerUrl"] && !empty($element["profilRealBannerUrl"])) {
						$imgHtml = '<a href="' . (filter_var($element["profilBannerUrl"], FILTER_VALIDATE_URL) ?  HtmlHelper::assetsUrlImg(@$element["profilBannerUrl"]) : $element["profilBannerUrl"] ). '"
							class="thumb-info"  
							data-title="' . Yii::t("common", "Cover image of") . " " . $element["name"] . '"
							data-lightbox="all">' .
							$imgHtml .
							'</a>';
					}
					echo $imgHtml;
				} else if (isset($element["collection"]) && !empty($element["collection"]) && $element["collection"] == "events") {
					if (isset($element["profilBannerUrl"]) && !empty($element["profilBannerUrl"])) {
						$imgHtml = '<img class="col-md-12 col-sm-12 col-xs-12 no-padding img-responsive" alt="' . Yii::t("common", "Banner") . '"
						src="' . (filter_var($element["profilBannerUrl"], FILTER_VALIDATE_URL) ?    HtmlHelper::assetsUrlImg($element["profilBannerUrl"]): $element["profilBannerUrl"]) . '">';
						if (isset($element["profilRealBannerUrl"]) && !empty($element["profilRealBannerUrl"])) {
							$imgHtml = '<a href="' .(filter_var($element["profilBannerUrl"], FILTER_VALIDATE_URL) ? HtmlHelper::assetsUrlImg($element["profilBannerUrl"]) : $element["profilBannerUrl"]) . '"
									class="thumb-info"  
									data-title="' . Yii::t("common", "Cover image of") . " " . $element["name"] . '"
									data-lightbox="all">' .
								$imgHtml .
								'</a>';
						}
						echo $imgHtml;
					} else if (isset($element["parent"]) && !empty($element["parent"])) {
						foreach ($element["parent"] as $parent) {
							$bannerImgUrl = PHDB::findOne("" . $parent["type"] . "", ["name" => $parent["name"]]);
						}
						if (isset($bannerImgUrl["profilBannerUrl"]) && !empty($bannerImgUrl["profilBannerUrl"])) {
							$imgHtml = '<img class="col-md-12 col-sm-12 col-xs-12 no-padding img-responsive" alt="' . Yii::t("common", "Banner") . '" src="' . (filter_var($element["profilBannerUrl"], FILTER_VALIDATE_URL) ?  HtmlHelper::assetsUrlImg($bannerImgUrl["profilBannerUrl"]): $bannerImgUrl["profilBannerUrl"] ). '">';
							if (isset($bannerImgUrl["profilRealBannerUrl"]) && !empty($bannerImgUrl["profilRealBannerUrl"])) {
								$imgHtml = '<a href="' . (filter_var($element["profilBannerUrl"], FILTER_VALIDATE_URL) ?    HtmlHelper::assetsUrlImg($bannerImgUrl["profilBannerUrl"]) : $bannerImgUrl["profilBannerUrl"]). '"
									class="thumb-info"  
									data-title="' . Yii::t("common", "Cover image of") . " " . $parent["name"] . '"
									data-lightbox="all">' .
									$imgHtml .
									'</a>';
							}
							echo $imgHtml;
						} else {
							$url = Yii::app()->theme->baseUrl . '/assets/img/background-onepage/' . $element["collection"] . '.png';
							echo '<img class="col-md-12 col-sm-12 col-xs-12 no-padding img-responsive" alt="' . Yii::t("common", "Banner") . '" src="' . $url . '">';
						}
					} else {
						$url = Yii::app()->theme->baseUrl . '/assets/img/background-onepage/' . $element["collection"] . '.png';
						echo '<img class="col-md-12 col-sm-12 col-xs-12 no-padding img-responsive" alt="' . Yii::t("common", "Banner") . '" src="' . $url . '">';
					}
				} else {
					if (isset($pageConfig["banner"]["img"]) && !empty($pageConfig["banner"]["img"])){
						$url = ($pageConfig["banner"]["img"]=="none") ? "" : Yii::app()->getModule("costum")->assetsUrl . $pageConfig["banner"]["img"];
					}
						
					else if (in_array($element["collection"], [Event::COLLECTION, Project::COLLECTION, Person::COLLECTION, Organization::COLLECTION]))
						$url = Yii::app()->theme->baseUrl . '/assets/img/background-onepage/' . $element["collection"] . '.png';
					else
						$url = Yii::app()->theme->baseUrl . '/assets/img/background-onepage/connexion-lines.jpg';

					$setImg=(!empty($url)) ? '<img class="col-md-12 col-sm-12 col-xs-12 no-padding img-responsive" alt="' . Yii::t("common", "Banner").'" src="' . $url . '">' : '';

					echo $setImg;
				} ?>

			</div>


			<?php if (isset($pageConfig["banner"]["badges"]) && !empty($pageConfig["banner"]["badges"])) { ?>
				<div class="section-badges pull-right no-padding">


					<div class="no-padding">
						<?php if (isset($pageConfig["banner"]["costumize"]) && !empty($pageConfig["banner"]["costumize"]) && (is_string($pageConfig["banner"]["costumize"]) || (isset($pageConfig["banner"]["costumize"]["slug"])) || isset($pageConfig["banner"]["costumize"]["config"])) && ($edit || $openEdition) && !empty(Yii::app()->session["userId"]) && ($element["collection"] == Organization::COLLECTION || $element["collection"] == Project::COLLECTION)) { ?>
							<?php
							$show = true;
							$costumizeSlug = is_string($pageConfig["banner"]["costumize"]) ? $pageConfig["banner"]["costumize"] : (isset($pageConfig["banner"]["costumize"]["slug"]) ? $pageConfig["banner"]["costumize"]["slug"] : (isset($pageConfig["banner"]["config"]) && isset($pageConfig["banner"]["costumize"]["config"]["slug"]) ? $pageConfig["banner"]["costumize"]["config"]["slug"] : ""));

							if (isset($pageConfig["banner"]["costumize"]["restricted"])) {
								if (isset($pageConfig["banner"]["costumize"]["restricted"]["types"]) && !in_array($element["collection"], $pageConfig["banner"]["costumize"]["restricted"]["types"]))
									$show = false;
								if (isset($pageConfig["banner"]["costumize"]["restricted"]["category"]) && (!isset($element["category"]) || !in_array($element["category"], $pageConfig["banner"]["costumize"]["restricted"]["category"])))
									$show = false;
							}
							if ($show) {
								$costum = $this->costum;
							?>
								<div class="badgePH pull-left hidden-xs" data-title="Only reserved to the community">
									<a class='costumize' data-costumize-slug='<?php is_string($pageConfig["banner"]["costumize"]) ? $pageConfig["banner"]["costumize"] : (isset($pageConfig["banner"]["costumize"]["slug"]) ? $pageConfig["banner"]["costumize"]["slug"] : "") ?>' data-costumize-parent='<?php echo !empty($costum) && isset($costum["slug"]) ? $costum["slug"] : "" ?>' data-costumize-config='<?php echo json_encode(isset($pageConfig["banner"]["costumize"]["config"]) ? $pageConfig["banner"]["costumize"]["config"] : "")   ?>'>
										<span class="pull-right tooltips " data-toggle="tooltip" data-placement="bottom" title="Créer votre costum">
											<img src="<?php echo Yii::app()->getModule("co2")->assetsUrl; ?>/images/logo_costum.png" style="width: 25px;margin-top: -4px">
										</span>
									</a>
								</div>
						<?php
							}
						}
						?>
						<?php if (isset($bannerConfig["preferences"]) && $bannerConfig["preferences"] && (!empty($element["preferences"]["isOpenEdition"]) || !empty($element["preferences"]["isOpenData"]) || !empty($element["preferences"]["private"]))) { ?>

							<?php if (!empty($element["preferences"]["private"])) { ?>
								<div class="badgePH pull-left" data-title="Only reserved to the community">
									<a href="javascript:;" class="editConfidentialityBtn">
										<span class="pull-right tooltips text-red" data-toggle="tooltip" data-placement="bottom" title="<?php echo Yii::t("common", "Only reserved to the community") ?>" style="font-size: 13px; line-height: 30px;">
											<i class="fa fa-lock" style="font-size: 17px;"></i>
											<?php echo Yii::t("common", "Private") ?>
										</span>
									</a>
								</div>
							<?php } ?>

							<?php if (Preference::isOpenData($element["preferences"])) { ?>
								<?php //if( Badge::checkBadgeInListBadges("opendata", $element["badges"]) ){
								?>
								<div class="badgePH pull-left" data-title="OPENDATA">
									<?php if ($edit) { ?><a href='javascript:;' class="openConfidentialSettings pull-left tooltips" data-toggle="tooltip" data-placement="left" title="<?php echo Yii::t("common", "Keep data open and usable by all") ?>" style="font-size: 13px; line-height: 30px;"><?php } ?>
										<span class="fa-stack opendata" style="width:17px">
											<i class="fa fa-database main fa-stack-1x" style="font-size: 20px;"></i>
											<i class="fa fa-share-alt  mainTop fa-stack-1x text-white" style="text-shadow: 0px 0px 2px rgb(15,15,15);"></i>
										</span>
										<?php echo Yii::t("common", "Open data") ?>
										<?php if ($edit) { ?></a><?php } ?>
								</div>
							<?php } ?>

							<?php if (Preference::isOpenEdition($element["preferences"])) { ?>
								<div class="badgePH pull-left" data-title="OPENEDITION">
									<?php if ($edit) { ?>
										<a href="javascript:;" class="openConfidentialSettings btn-show-activity">
										<?php } ?>
										<span class="pull-right tooltips" data-toggle="tooltip" data-placement="left" title="<?php echo Yii::t("common", "All users can participate and modify informations") ?>" style="font-size: 13px; line-height: 30px;">
											<i class="fa fa-creative-commons" style="font-size: 17px;"></i>
											<?php echo Yii::t("common", "Open edition") ?>
										</span>
										<?php if ($edit) { ?></a><?php } ?>
								</div>
							<?php } ?>

						<?php
						} ?>
					</div>
				</div>
			<?php }
			?>

			<?php if (isset($pageConfig["banner"]["headerInfos"]) && !empty($pageConfig["banner"]["headerInfos"])) {
				if (is_string($pageConfig["banner"]["headerInfos"])) {
					echo $this->renderPartial(
						$pageConfig["banner"]["headerInfos"],
						array(
							"iconColor" => $iconColor,
							"icon" => $icon,
							"element" => $element,
							"pageConfig" => $pageConfig,
							"linksBtn" => $linksBtn,
							"invitedMe" => $invitedMe,
							"element" => $element,
							"edit" => $edit,
							"openEdition" => $openEdition
						)
					);
				} else { ?>
					<div class="col-xs-12 col-sm-12 col-md-12 contentHeaderInformation <?php if (@$element["profilBannerUrl"] && !empty($element["profilBannerUrl"])) echo "backgroundHeaderInformation" ?>">
						<div class="col-xs-12 col-sm-9 col-md-9 col-lg-10 text-white pull-right">
							<?php if (@$element["status"] == "deletePending") { ?>
								<h4 class="text-left padding-left-15 pull-left no-margin letter-red"><?php echo Yii::t("common", "Being suppressed") ?></h4><br>
							<?php } ?>
							<h4 class="text-left padding-left-15 pull-left no-margin">
								<span id="nameHeader">
									<div class="pastille-type-element bg-<?php echo $iconColor; ?> pull-left">

									</div>
									<i class="fa fa-<?php echo $icon; ?> pull-left margin-top-5"></i>
									<div class="name-header pull-left"><?php echo @$element["name"]; ?></div>
								</span>
								<?php if ($element["collection"] == Event::COLLECTION && isset($element["type"])) {
									$typesList = Event::$types;
								?>
									<span id="typeHeader" class="margin-left-10 pull-left">
										<i class="fa fa-x fa-angle-right pull-left"></i>
										<div class="type-header pull-left">
											<?php if (isset($typesList[$element["type"]])) echo Yii::t("category", $typesList[$element["type"]]) ?>
										</div>
									</span>
								<?php } ?>
							</h4>


						</div>



						<?php
						$classAddress = ((@$element["address"]["postalCode"] || @$element["address"]["addressLocality"] || @$element["tags"]) ? "" : "hidden"); ?>
						<div class="header-address-tags col-xs-12 col-sm-9 col-md-9 col-lg-10 pull-right margin-bottom-5 <?php echo $classAddress; ?>">
							<?php if (!empty($element["address"]["addressLocality"])) { ?>
								<div class="header-address badge letter-white bg-red margin-left-5 pull-left">
									<?php
									echo !empty($element["address"]["streetAddress"]) ? "<i class='fa fa-map-marker'></i> " . $element["address"]["streetAddress"] . ", " : "";
									echo !empty($element["address"]["postalCode"]) ?
										$element["address"]["postalCode"] . ", " : "";
									echo $element["address"]["addressLocality"];
									?>
								</div>
								<?php $classCircleO = (!empty($element["tags"]) ? "" : "hidden"); ?>
								<span id="separateurTag" class="margin-right-10 margin-left-10 text-white pull-left <?php echo $classCircleO; ?>" style="font-size: 10px;line-height: 20px;">
									<i class="fa fa-circle-o"></i>
								</span>

							<?php } ?>
							<div class="header-tags pull-left">
								<?php
								if (@$element["tags"]) {
									foreach ($element["tags"] as $key => $tag) { ?>
										<a href="#search?text=#<?php echo $tag; ?>" class="badge letter-red bg-white lbh" style="vertical-align: top;">#<?php echo $tag; ?></a>
								<?php }
								} ?>
							</div>
						</div>



						<div class="col-xs-12 col-sm-9 col-md-9 col-lg-10 pull-right">
							<span class="pull-left text-white" id="shortDescriptionHeader"><?php echo ucfirst(substr(trim(@$element["shortDescription"]), 0, 180)); ?>
							</span>
						</div>

						<?php if (($edit || $openEdition) && !empty(Yii::app()->session["userId"])) {
							$href = (isset($bannerConfig["editButton"])
								&& isset($bannerConfig["editButton"]["dynform"])
								&& $bannerConfig["editButton"]["dynform"]) ? "javascript:dyFObj.editElement('" . Element::getControlerByCollection($element["collection"]) . "', '" . (string)$element["_id"] . "');" : "javascript:;";
						?>
							<div class="col-xs-12 col-sm-9 col-md-9 col-lg-10 pull-right">
								<a href="<?php echo $href ?>" class="pull-left btn letter-blue bg-white" id="btnHeaderEditInfos">
									<i class="fa fa-pencil"></i> <?php echo Yii::t("common", "Edit information") ?>
								</a>
							</div>

						<?php } ?>


						<?php if (in_array($element["collection"], [Event::COLLECTION, Poi::COLLECTION, Project::COLLECTION])) {
							if (@$element['parent'] || @$element['organizer']) { ?>
								<div class="section-date pull-right">
									<?php if ($element["collection"] == Event::COLLECTION) { ?>
										<div class="event-infos-header" style="font-size: 14px;font-weight: none;"></div>
									<?php } ?>
									<div style="font-size: 14px;font-weight: none;">
										<div id="parentHeader">
											<?php if (@$element['parent']) {
												$count = count($element["parent"]);
												$msg = ($element["collection"] == Event::COLLECTION) ? Yii::t("common", "Planned on") : Yii::t("common", "Carried by");
												echo $msg . " : ";
												foreach ($element['parent'] as $key => $v) {
													$heightImg = ($count > 1) ? 35 : 25;
													$imgPath = (@$v["profilThumbImageUrl"] && !empty($v["profilThumbImageUrl"])) ? Yii::app()->createUrl('/' . $v["profilThumbImageUrl"]) : $this->module->assetsUrl . '/images/thumb/default_' . $v["type"] . '.png' ?>
													<a href="#page.type.<?php echo $v['type']; ?>.id.<?php echo $key; ?>" class="lbh tooltips" <?php if ($count > 1) echo 'data-toggle="tooltip" data-placement="left" title="' . $v["name"] . '"' ?>>
														<img src="<?php echo $imgPath ?>" class="img-circle" width='<?php echo $heightImg ?>' height='<?php echo $heightImg ?>' />
														<?php if ($count == 1) echo $v['name']; ?>
													</a>

												<?php } ?> <br> <?php } ?>
										</div>
										<div id="organizerHeader">
											<?php if (@$element['organizer']) {
												$count = count($element["organizer"]);
												echo Yii::t("common", "Organized by") . " : ";
												foreach ($element['organizer'] as $key => $v) {
													$heightImg = ($count > 1) ? 35 : 25;
													$imgPath = (@$v["profilThumbImageUrl"] && !empty($v["profilThumbImageUrl"])) ? Yii::app()->createUrl('/' . $v["profilThumbImageUrl"]) : $this->module->assetsUrl . '/images/thumb/default_' . $v["type"] . '.png' ?>
													<a href="#page.type.<?php echo $v['type']; ?>.id.<?php echo $key; ?>" class="lbh tooltips" <?php if ($count > 1) echo 'data-toggle="tooltip" data-placement="left" title="' . $v["name"] . '"' ?>>
														<img src="<?php echo $imgPath ?>" class="img-circle" width='<?php echo $heightImg ?>' height='<?php echo $heightImg ?>' />
														<?php if ($count == 1) echo $v['name']; ?>
													</a>

											<?php }
											} ?>
										</div>
									</div>
								</div>
						<?php }
						} ?>

					</div>
			<?php
				}
			} ?>
		</div>
<?php
	}
} ?>

<script>
	jQuery(document).ready(function() {
		$('[data-toggle="tooltip"]').tooltip()
		var campaign = <?php echo json_encode($campaign) ?>;
		mylog.log("campaign", campaign);

		$(".modify-campaign").click(function() {

			dyFObj.openForm("crowdfunding", null, campaign);
		});


		$(".crowdfunding").click(function() {
			dyFObj.openForm("crowdfunding", null, null);
		});


		$(".close-campaign-popup").click(function() {
			$(this).parent().css("display", "none")
		});




		$(".costumize").off().on("click", function() {
			pageProfil.actions.costumize();
		});
		//// flashing below doesn't work---------
		// var hovering = false;

		// function FaireClignoterImage() {
		//     if (hovering) {
		//         return;
		//     }
		//     else{
		//     	$(".btn-crowd").fadeOut(200).delay(300).fadeIn(200); 
		// 		setInterval(FaireClignoterImage(),700);
		//     }
		// }

		// $(".crowdfunding-badge").hover(
		//     function() {
		//         hovering = true;
		//     },
		//     function() {
		//         hovering = false;
		//         FaireClignoterImage();
		//     }
		// );

	});
</script>