<?php

use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\utils\Utils;

 $bannerConfig=$this->appConfig["element"]["banner"]; ?>
<style>
	#uploadScropResizeAndSaveImage i{
		position: inherit !important;
	}
	#uploadScropResizeAndSaveImage .close-modal .lr,
	#uploadScropResizeAndSaveImage .close-modal .lr .rl{
		z-index: 1051;
		height: 75px;
		width: 1px;
		background-color: #2C3E50;
	}
	#uploadScropResizeAndSaveImage .close-modal .lr{
		margin-left: 35px;
		transform: rotate(45deg);
		-ms-transform: rotate(45deg);
		-webkit-transform: rotate(45deg);
	}
	#uploadScropResizeAndSaveImage .close-modal .rl{
		transform: rotate(90deg);
		-ms-transform: rotate(90deg);
		-webkit-transform: rotate(90deg);
	}
	#uploadScropResizeAndSaveImage .close-modal {
		position: absolute;
		width: 75px;
		height: 75px;
		background-color: transparent;
		top: 25px;
		right: 25px;
		cursor: pointer;
	}
	.blockUI, .blockPage, .blockMsg{
		padding-top: 0px !important;
	} 

	#banner_element:hover{
	    color: #9fbd38;
	    background-color: white;
	    border:1px solid #9fbd38;
	    border-radius: 3px;
	    margin-right: 2px;
	}
	a.btn-favorite-link{
		display: none;
	}
	#banner_element{
	    background-color: #9fbd38;
	    color: white;
	    border-radius: 3px;
	    margin-right: 2px;
	    display:none;
	}
	.header-address{
		font-size: 14px;
		padding-left: 5px;
	}
	#contentBanner img{
		min-height:280px;
	}
	.badgePH{
		padding: 10px;
	}
	#contentBanner{
		min-height: 280px;
	}
	.a-icon-banner{
		font-size: 25px;
	}

	.link-banner{
		padding: 0px 5px 0px 5px;
		color: white;
	}
	.bg-dark-green{
    background-color: #4caf50;
}
	.link-banner{
		max-width: 80px;	
	}
	.link-banner:hover{
		color: #9fbd38;
	}
	.title-link-banner{
		font-size: 13px;
	}
	@media (min-width: 768px) and (max-width: 991px){
		.link-banner {
		    padding: 15px 6px;
		}
		.tag-list li.adress {
			color: #3f4e58;
		}
	}
	.tag-contain ul.tag-list li {
	    border: 0px!important;
	    padding-left: 3px!important;
	    padding-right: 3px!important;
	    background-color: #fff;
	    color: #0566a1;
	    border-radius: 12px;
	    font-size: 12px;
	    padding: 5px 12px;
	    border-radius: 8px;
	    margin-right: 5px;
	    margin-top: 5px;
	}
	.tag-contain .tag-list li.adress {
	    font-size: 14px;
	    background-color: transparent;
	    padding: 5px 12px;
	}
	#btnHeaderEditInfos {
    padding: 0px!important;
    margin-top: 5px;
    margin-left: 0px!important;
	}
	.section-badges .dropdown-menu>li>a {
    color: #0566a1 !important;
	}
	.section-badges .dropdown-menu>li>a:hover {
    background: #0566a1 !important;
    color: white!important;
	}

	.section-badges .dropdown-menu {
		padding: 0px;
	}
	.icon-before-name{
    border-radius: 50%;
    width: 28px;
    height: 28px;
    font-size: 18px;
    color: white;
    text-align: center;
    line-height: 28px;
    margin-right: 10px;
    display: inline-block;
}
.event-infos-header small.pull-right {
    float: none!important;
    margin-left: 10px;
    color: #4caf50!important;
}
.event-infos-header .text-red{
	color: #4caf50!important;
}
#paramsMenu li ul li.text-left {
    text-align: left;
}
.visible-xs .event-infos-header h3 small{
	color: #fff;
}
.visible-xs .event-infos-header span.uppercase{
	color: #fff;
}
.boxBtnLink a.menu-linksBtn, .boxBtnLink .nav.navbar-nav, .boxBtnLink .dropdow, .AddFriendBtn {
    width: auto;
}
.AddFriendBtn a.menu-AddFriendBtn {
	padding: 9.3px;
    border: none;
    text-transform: none;
    margin: 0px;
}
.statuInvitation, #parentHeader, #organizerHeader  {
	padding: 10px;
}

@media (max-width: 767px){
	ul>li>ul.dropdown-menu{
	right: 0px;
    left: inherit;
    top: 40px;
	}
	.navbar-nav .open .dropdown-menu {
    position: absolute!important;
    background-color: #fff;
	}
	.section-badges .boxBtnLink, .section-badges .AddFriendBtn{
		border-radius: 30px;
	}
}

/***** BUTTON INVITER *******/
#col-banner .containInvitation, #parentHeader, #organizerHeader{
	 top: 60px;
    position: absolute;
    z-index: 1;
    right: 15px;
    margin-top: 10px;
    background-color: #3f4e58;
    opacity: 0.8;
    border-radius: 15px;
    color: white;
    text-align: center;
}
#col-banner .containInvitation a.lbh {
	color: #9fbd38;
}

#col-banner .containInvitation a.btn {
    font-weight: 500;
    font-size: 14px;
    letter-spacing: 1px;
    display: inline-block;
    padding: 9px 22px!important;
    border-radius: 50px!important;
    line-height: 1;
    color: #fff;
    /*background-color: transparent*/
}
#col-banner .containInvitation a.btn-accept {
	    border: 2px solid #1bbca3;
}
#col-banner .containInvitation a.btn-accept:hover {
	background: #fff;
}
#col-banner .containInvitation a.btn-refuse{
	border: 2px solid #E33551;
}
#col-banner .containInvitation a.btn-refuse:hover{
	background: #fff;
}
@media (max-width: 767px){
	#col-banner .containInvitation{
		display: none;
	}
}

#badge-container {
	pointer-events: none;
	position: absolute;
	z-index: 4;
	display: flex;
	justify-content: end;
	margin-right: 10px;
	right: 100%;
}

.banner-image-badge-container {
	pointer-events: all;
	cursor: pointer;
	background-color: white;
	padding: 7px;
	border-radius: 10px;
	box-shadow: 0px 0px 6px 0px #404545;
	margin-right: 10px;
	width: 50px;
	height: 50px;
}

.banner-image-badge {
	height: 100%;
	width: auto;
}

.dropdown-menu {
	    display: none;
	}
	.dropdown:hover .dropdown-menu {
	    display: block;
	}

</style>

<?php 
$thumbAuthor =  @$element['profilImageUrl'] ? 
Yii::app()->createUrl('/'.@$element['profilImageUrl']) : "";
$isFromFediverse = Utils::isFediverseShareLocal($element);
$isLocalActor = boolval(Utils::isLocalActor());
?>

<?php
if ($isFromFediverse) {
	if (!$isLocalActor) {
		$edit = false;
	}
}
function renderRealBanner($url){
	if (filter_var($url, FILTER_VALIDATE_URL)) {
		return $url;
	}else{
		return Yii::app()->createUrl('/'.$url);
	}
}
?>

<div class="col-xs-12 col-md-12 col-sm-12 col-lg-12 text-left no-padding" id="col-banner">
	<?php echo $this->renderPartial("co2.views.element.modalBanner", array(
			"edit" => $edit,
			"openEdition" => $openEdition,
			"profilBannerUrl"=> @$element["profilBannerUrl"],
			"element"=> $element)); ?>
	<div id="contentBanner" class="col-md-12 col-sm-12 col-xs-12 no-padding">
		<?php 
			if (@$element["profilBannerUrl"] && !empty($element["profilBannerUrl"])) {	
				$imgHtml='<img class="col-md-12 col-sm-12 col-xs-12 no-padding img-responsive" alt="'.Yii::t("common","Banner").'"
					src="'.renderRealBanner($element["profilBannerUrl"]).'">';
				if (@$element["profilRealBannerUrl"] && !empty($element["profilRealBannerUrl"])){
					$imgHtml='<a href="'.Yii::app()->createUrl('/'.$element["profilRealBannerUrl"]).'"
								class="thumb-info"  
								data-title="'.Yii::t("common","Cover image of")." ".$element["name"].'"
								data-lightbox="all">'.
								$imgHtml.
							'</a>';
				}
				echo $imgHtml;
			} else if (isset($element["collection"]) && !empty($element["collection"]) && $element["collection"] == "events") {
				if (isset($element["profilBannerUrl"]) && !empty($element["profilBannerUrl"])) {
					$imgHtml = '<img class="col-md-12 col-sm-12 col-xs-12 no-padding img-responsive" alt="' . Yii::t("common", "Banner") . '"
					src="' . HtmlHelper::assetsUrlImg($element["profilBannerUrl"]) . '">';
						if (isset($element["profilRealBannerUrl"]) && !empty($element["profilRealBannerUrl"])) {
							$imgHtml = '<a href="' . HtmlHelper::assetsUrlImg($element["profilBannerUrl"]) . '"
								class="thumb-info"  
								data-title="' . Yii::t("common", "Cover image of") . " " . $element["name"] . '"
								data-lightbox="all">' .
								$imgHtml .
								'</a>';
						}
						echo $imgHtml;
				} else if (isset($element["parent"]) && !empty($element["parent"])) {
					foreach($element["parent"] as $parent) {
						if (!empty($parent['name']))
						$bannerImgUrl = PHDB::findOne("".$parent["type"]."", ["name" => $parent["name"]]);
					}
					if (isset($bannerImgUrl["profilBannerUrl"]) && !empty($bannerImgUrl["profilBannerUrl"])) {
						$imgHtml = '<img class="col-md-12 col-sm-12 col-xs-12 no-padding img-responsive" alt="' . Yii::t("common", "Banner") . '" src="' . HtmlHelper::assetsUrlImg($bannerImgUrl["profilBannerUrl"]) . '">';
						if (isset($bannerImgUrl["profilRealBannerUrl"]) && !empty($bannerImgUrl["profilRealBannerUrl"])) {
							$imgHtml = '<a href="' . HtmlHelper::assetsUrlImg($bannerImgUrl["profilBannerUrl"]) . '"
								class="thumb-info"  
								data-title="' . Yii::t("common", "Cover image of") . " " . $parent["name"] . '"
								data-lightbox="all">' .
								$imgHtml .
								'</a>';
						}
						echo $imgHtml;
					} else {
						$url = Yii::app()->theme->baseUrl . '/assets/img/background-onepage/' . $element["collection"] . '.png';
						echo '<img class="col-md-12 col-sm-12 col-xs-12 no-padding img-responsive" alt="' . Yii::t("common", "Banner") . '" src="' . $url . '">';
					}
				} else {
					$url = Yii::app()->theme->baseUrl . '/assets/img/background-onepage/' . $element["collection"] . '.png';
						echo '<img class="col-md-12 col-sm-12 col-xs-12 no-padding img-responsive" alt="' . Yii::t("common", "Banner") . '" src="' . $url . '">';
				}
			} else {
				if(isset($pageConfig["banner"]["img"]) && !empty($pageConfig["banner"]["img"]))
					$url=Yii::app()->getModule( "costum" )->assetsUrl.$pageConfig["banner"]["img"];
				else if(in_array($element["collection"], [Event::COLLECTION, Project::COLLECTION, Person::COLLECTION, Organization::COLLECTION]))
					$url=Yii::app()->theme->baseUrl.'/assets/img/background-onepage/'.$element["collection"].'.png';
				else		
					$url=Yii::app()->theme->baseUrl.'/assets/img/background-onepage/connexion-lines.jpg';	
		
				echo '<img class="col-md-12 col-sm-12 col-xs-12 no-padding img-responsive" alt="'.Yii::t("common","Banner").'"
					src="'.$url.'">';
			} 
		?>
	</div>

	
	<div class="btn-group section-badges pull-right no-padding" >
		
	<?php if (isset($badgesToShow) && !empty($badgesToShow)) { ?>
	<div id="badge-container">
		<?php
		foreach ($badgesToShow  as $key => $value) { ?>
			<div class="lbh banner-image-badge-container" data-hash="#page.type.badges.id.<?= $key ?>" data-toggle="tooltip" data-placement="bottom" title="<?= $value["name"] ?>">
				<img class="banner-image-badge" src="<?php echo @$value["image"] ?>" />
			</div>
		<?php } ?>
	</div>
	<?php } ?>

		
	<!-- Btn Link -->	
		<div class="pull-left no-padding" style="">
		<?php  
 			if(@Yii::app()->session["userId"] && @Yii::app()->session["userId"] != (string)$element["_id"]){ ?>
			<div class="pull-left boxBtnLink no-padding bg-dark-green" style="border-radius: 30px; display: flex;">
        	<?php  echo $this->renderPartial('co2.views.element.menus.links', 
    			array(  "linksBtn" => $linksBtn,
    					"element"   => $element,
    					"openEdition" => $openEdition ) 
    			); 
    		?>
			</div>
		<?php } ?>
		
		</div>
<!-- END Btn Link -->	

<!-- Edite les information -->
		<?php if(($edit || $openEdition ) && !empty(Yii::app()->session["userId"]) && ($element["collection"]==Organization::COLLECTION || $element["collection"]==Project::COLLECTION || $element["collection"]==Event::COLLECTION)){ ?>
			<div class="badgePH pull-left hidden-xs" data-title="Only reserved to the community">
				<a href="javascript:pageProfil.actions.costumize();" class="" >
					<span class="pull-right tooltips " data-toggle="tooltip" data-placement="bottom" title="<?php echo Yii::t("common","Create your costume") ?>" >
						<img src="<?php echo Yii::app()->getModule("co2")->assetsUrl; ?>/images/logo_costum.png" style="width: 25px;margin-top: -4px">
					</span>
				</a>	
			</div>

		<?php } ?>
<!-- END Edite les information -->	
		
		
 			<?php if (!empty($element["preferences"]["private"])) { ?>
						<div class="badgePH pull-left hidden-xs" data-title="Only reserved to the community">
							<a href="javascript:;" class="editConfidentialityBtn">
							<span class="pull-right tooltips text-red" data-toggle="tooltip" data-placement="bottom" 
									title="<?php echo Yii::t("common","Private") ?>" >
								<i class="fa fa-lock" style="font-size: 17px;"></i> 
								
							</span>
							</a>
						</div>
					<?php } ?>

			<?php if(!empty($element["preferences"]["isOpenData"])){?>
				<div class="badgePH pull-left hidden-xs" data-title="OPENDATA">
					<?php if($edit){ ?>
						<a href='javascript:;' class="openConfidentialSettings pull-left tooltips" data-toggle="tooltip" data-placement="bottom" 
					title="<?php echo Yii::t("common","Open data") ?>"><?php } ?>
						<span class="fa-stack opendata" style="margin-top: -28px!important;height: auto;">
							<i class="fa fa-database main fa-stack-1x" style="font-size: 20px;"></i>
								<i class="fa fa-share-alt  mainTop fa-stack-1x text-white" 
								style="text-shadow: 0px 0px 2px rgb(15,15,15);"></i>
									</span> 
								<?php if($edit) { ?></a><?php } ?>
							</div>
					<?php //} 
					} ?>

					<?php if (!empty($element["preferences"]["isOpenEdition"])) { ?>
						<div class="badgePH pull-left hidden-xs" data-title="OPENEDITION">
							<?php if($edit){ ?>
							<a href="javascript:;" class="openConfidentialSettings btn-show-activity">
							<?php } ?>
							<span class="pull-right tooltips" data-toggle="tooltip" data-placement="bottom" 
									title="<?php echo Yii::t("common","Open edition") ?>">
								<i class="fa fa-creative-commons"  style="font-size: 17px;"></i> 
								
							</span>
							<?php if($edit){ ?></a><?php } ?>
						</div>
					<?php } ?>


<!-- Edite les information -->
		<?php if( ( $edit || $openEdition ) && !empty(Yii::app()->session["userId"])){ 
			$href = "javascript:;";
			$class = "";
			$dataView = "";
			if(isset($bannerConfig["editButton"]) 
				&& isset($bannerConfig["editButton"]["dynform"]) 
				&& $bannerConfig["editButton"]["dynform"])  
					$href= "javascript:dyFObj.editElement('".Element::getControlerByCollection($element["collection"])."', '".(string)$element["_id"]."');" ;
			else {
				$class = "ssmla";
				$dataView = "data-view='detail'"; 
			}
			?>
			<div class="badgePH pull-left hidden-xs" data-title="Only reserved to the community">
				<a href="<?php echo $href ?>" class="<?php echo $class ?>" <?php echo $dataView ?> >
					<span class="pull-right tooltips " data-toggle="tooltip" data-placement="bottom" title="<?php echo Yii::t("common", "Edit information") ?>" >
						<i class="fa fa-pencil"  style="font-size: 17px;"></i> 
					</span>
				</a>	
			</div>

		<?php } ?>
<!-- END Edite les information -->

	</div>

	

<!-- BUTTON ACCEPET INVITATION AND REJETE INVITATION  
	Yii::t("common","I co-fund")
	<div class="section-btn-ivite pull-right">
		<a href="#" class="btn-invite-accept animated fadeInUp">
			Accepter
		</a>
		<a href="#" class="btn-invite-accept animated fadeInUp">
			Refuser
		</a>
	</div>	
	-->
	
		<div class="animated bounceInRight">
			<?php if (@Yii::app()->session["userId"] && Yii::app()->session["userId"] != (is_array($invitedMe) && $invitedMe["invitorId"] ? $invitedMe["invitorId"] : null)) { ?>
				<?php echo $this->renderPartial('co2.views.element.menus.answerInvite', 
		    			array(  "invitedMe"      => $invitedMe,
		    					"element"   => $element
		    					) 
		    			); 
		    }else if (@Yii::app()->session["userId"] && Yii::app()->session["userId"] == (is_array($invitedMe) && $invitedMe["invitorId"] ? $invitedMe["invitorId"] : null)) {?>
				<div class="containInvitation">
					<div class="statuInvitation">
						<?php echo Yii::t("common", "Friend request sent") ?>
						<?php 
						$inviteCancel="Cancel";
						$option=null;
						$msgRefuse=Yii::t("common","Are you sure to cancel this invitation");
							echo 
							'<br>'.
							'<a class="btn btn-xs tooltips btn-refuse margin-left-5" href="javascript:links.disconnect(\''.$element["collection"].'\',\''.(string)$element["_id"].'\',\''.Yii::app()->session["userId"].'\',\''.Person::COLLECTION.'\',\''.Element::$connectTypes[$element["collection"]].'\',null,\''.$option.'\',\''.$msgRefuse.'\')" data-placement="bottom" data-original-title="'.Yii::t("common","Not interested by the invitation").'">'.
								'<i class="fa fa-remove"></i> '.Yii::t("common",$inviteCancel).
							'</a>';

						 ?>
					</div>
				</div>
		    <?php }?>	
		</div>
	
		<?php if(@$element['parent']){  ?>
			<div id="parentHeader"  style="top: 45px;">
			<?php	
				$count=count($element["parent"]);
				$msg = ($element["collection"]==Event::COLLECTION) ? Yii::t("common","Planned on") : Yii::t("common","Carried by") ;
				echo "<span class='text-green'>".$msg. " : </span>";
				foreach($element['parent'] as $key =>$v){
					$heightImg=($count>1) ? 35 : 25;
					$imgPath = (@$v["profilThumbImageUrl"] && !empty($v["profilThumbImageUrl"])) ? Yii::app()->createUrl('/'.$v["profilThumbImageUrl"]) : $this->module->assetsUrl.'/images/thumb/default_'.$v["type"].'.png' ?> 
					<a href="#page.type.<?php  echo $v['type']; ?>.id.<?php  echo $key; ?>" 
						class="lbh tooltips text-white"
						<?php if($count>1) echo 'data-toggle="tooltip" data-placement="left" title="'.$v["name"].'"' ?>> 
						<img src="<?php echo $imgPath ?>" class="img-circle" width='<?php echo $heightImg ?>' height='<?php echo $heightImg ?>' />
						<?php  if ($count==1 && !empty($v['name'])) echo $v['name']; ?>
					</a>
					 
				<?php } ?> <br> 
			</div>
		<?php } ?>
		
	
		
		<?php if(@$element['organizer']){ ?>
			<div id="organizerHeader" style="top: 95px;">
				<?php
					$count=count($element["organizer"]);
					echo "<span class='text-green'>".Yii::t("common","Organized by"). " : </span>";
					foreach($element['organizer'] as $key =>$v){
						$heightImg=($count>1) ? 35 : 25;
						$imgPath = (@$v["profilThumbImageUrl"] && !empty($v["profilThumbImageUrl"])) ? Yii::app()->createUrl('/'.$v["profilThumbImageUrl"]) : $this->module->assetsUrl.'/images/thumb/default_'.$v["type"].'.png' ?> 
					<a href="#page.type.<?php  echo $v['type']; ?>.id.<?php  echo $key; ?>" 
							class="lbh tooltips text-white"
							<?php if($count>1) echo 'data-toggle="tooltip" data-placement="left" title="'.$v["name"].'"' ?>> 
							<img src="<?php echo $imgPath ?>" class="img-circle" width='<?php echo $heightImg ?>' height='<?php echo $heightImg ?>' />
							<?php  if ($count==1) echo $v['name']; ?>
						</a>
						 
				<?php } ?> 
			</div>
		<?php } ?>

		

	
	<div class="col-xs-12 col-sm-12 col-md-12  hidden-xs contentHeaderInformation <?php if(@$element["profilBannerUrl"] && !empty($element["profilBannerUrl"])) echo "backgroundHeaderInformation" ?>">

	    	<div class="col-xs-12 col-sm-8 col-md-9 col-lg-9 text-white pull-right">
				<div class="col-xs-12 col-sm-7 col-md-6 text-left">
					<?php if (@$element["status"] == "deletePending") { ?> 
						<h4 class="text-left padding-left-15 pull-left no-margin letter-red"><?php echo Yii::t("common","Being suppressed") ?></h4><br>
					<?php } ?>
					<h4 class="text-left">
						<span id="nameHeader">
							<span class="bg-<?php echo $iconColor; ?> pull-left icon-before-name">
								<i class="fa fa-<?php echo $icon; ?> text-center"></i>
							</span> 
							<div class="name-header">
								<?php echo @$element["name"]; ?>
								<?php if (isset($element["fromActivityPub"]) && isset($element["attributedTo"])) { ?>
								<div style="padding-left:25px">
									<img style="width:18px;height:18px;margin-left: 5px;" src="https://www.fediverse.to/static/images/icon.png" alt="" width="32" height="32">
									<span style="font-size: 13px;"><?php echo Utils::getLinkToAdress($element["attributedTo"]);?></span>
									</div>
								<?php } ?>
							</div>
						</span>
						<?php if($element["collection"]==Event::COLLECTION && !empty($element["type"])){ 
								$typesList=Event::$types;
								if(isset($typesList[$element["type"]])){
						?>
							<span id="typeHeader" class="margin-left-10 pull-left">
								<i class="fa fa-x fa-angle-right pull-left"></i>
								<div class="type-header pull-left">
							 		<?php echo Yii::t("category", $typesList[$element["type"]]) ?>
							 	</div>
							</span>
						<?php }} ?>
					</h4>

					<div class="col-md-12 hidden-sm col-lg-12 tag-contain hidden-xs no-pading">
				        <ul class="tag-list no-padding">
				        	<?php if(!empty($element["address"]["addressLocality"])){ ?>
				        	<li class="adress-banner-element padding-left-0">
				        		<?php
				        		echo "<i class='fa fa-map-marker'></i> ";
									echo !empty($element["address"]["streetAddress"]) ? $element["address"]["streetAddress"].", " : "";
									echo !empty($element["address"]["postalCode"]) ? $element["address"]["postalCode"].", " : "";
									echo $element["address"]["addressLocality"];
								?>
				        	</li>
				        	<?php $classCircleO = (!empty($element["tags"]) ? "" : "hidden" ); ?>
										<span id="separateurTag" class="margin-right-10 margin-left-10 text-white pull-left <?php echo $classCircleO ; ?>" style="font-size: 10px;line-height: 35px; color: #9fbd38!important;">
											<i class="fa fa-circle-o"></i>
										</span>
									
								<?php } 
				            
				                if(!empty($element["tags"])){
				                	$countTags=count($element["tags"]);
				                	$nbTags=1;
				                    foreach ($element["tags"]  as $key => $tag) {
				                    	if($nbTags < 5){
					                        echo '<li class="tag-banner-element" style="margin-top: 3px;"><i class="fa fa-hashtag"></i>&nbsp;'.$tag.'</li>';
					                    	$nbTags++;
				                    	}else{
				                    		if(($countTags-$nbTags) > 0) echo "<li class='indicate-more-tags'>+".($countTags-$nbTags)."</li>";
				                    		break;
				                    	}
				                    }
				                } ?>
				        </ul>
				    </div>
				</div>	
				<div class="col-xs-12 col-sm-5 col-md-6 text-right">
				
					
					<?php if (($edit==true || $openEdition==true) && @Yii::app()->session["userId"]){ ?>
						<ul class="nav navbar-nav pull-right" id="paramsMenu">
							<li class="dropdown dropdown-profil-menu-params">
								<a href="javascript:;" type="button" class="pull-right text-center link-banner">
									<i class="fa fa-ellipsis-v a-icon-banner"></i> 
									<!--<span class="title-link-banner"></span>-->
						  		</a>
						  		<ul class="dropdown-menu arrow_box menu-params">
						  			<?php 
						  				$view=($element["collection"]!=Person::COLLECTION) ? "settingsCommunity" : "settings";
						  				$label=($element["collection"]!=Person::COLLECTION) ? "Parameters" : "My parameters";
						  			?>
				                	<li class="text-left">
										<a href="javascript:;" class="bg-white ssmla " data-view="<?php echo $view ?>">
											<i class="fa fa-cogs"></i> <?php echo Yii::t("common", $label) ?>
										</a>
									</li>
				                	<li class="text-left">
										<a href="javascript:;" class="bg-white ssmla " data-action="qrCode">
											<i class="fa fa-qrcode"></i> <?php echo Yii::t("common", "QR Code") ?>
										</a>
									</li>
				                	<li class="text-left">
										<a href="javascript:;" class="bg-white ssmla " data-action="chatSettings">
											<i class="fa fa-comments"></i> <?php echo Yii::t("common", "Chat settings") ?>
										</a>
									</li>
				                	<li class="text-left">
										<a href="javascript:;" class="bg-white ssmla " data-view="md">
											<i class="fa fa-file-text-o"></i> <?php echo Yii::t("common", "Markdown version") ?>
										</a>
									</li>
									<li class="text-left">
										<a href="javascript:;" class="bg-white ssmla " data-action="finder">
											<i class="fa fa-folder"></i> Finder
										</a>
									</li>
				                	<li class="text-left">
										<a href="javascript:;" class="bg-white ssmla " data-view="conode">
											<img src="<?php echo Yii::app()->getModule("co2")->assetsUrl; ?>/images/graph/community.png" alt="" width="20"> Graph de Communauté
										</a>
									</li>
				                	<?php if($element["collection"] == Person::COLLECTION){ ?>
									<li class="text-left">
										<a href="javascript:;" class="bg-white ssmla " data-action="mindmap">
											<i class="fa fa-sitemap"></i> <?php echo Yii::t("common", "Mindmap view") ?>
										</a>
									</li>
									<?php } ?>
				                	<li class="text-left">
										<a href="javascript:;" class="bg-white ssmla " data-action="downloadData">
											<i class="fa fa-download"></i> <?php echo Yii::t("common", "Download data") ?>
										</a>
									</li>
				                	<li class="text-left">
										<a href="javascript:;" class="bg-white ssmla " data-action="printOut">
											<i class="fa fa-print"></i> <?php echo Yii::t("common", "Print out") ?>
										</a>
									</li>
				                </ul>
				            </li>
				        </ul>

					
						<a href="javascript:;" class="ssmla pull-right text-center link-banner" data-action="chatmanager">
							<i class="fa fa-comments a-icon-banner"></i> 
							<span class="title-link-banner hidden-sm"><br><?php echo Yii::t("common", "Chat") ?></span>
						</a>
							<?php if (Authorisation::isElementMember((string)$element["_id"],(string)$element["collection"],Yii::app()->session["userId"])){ ?>
						
								<a href="javascript:;" class="ssmla pull-right text-center link-banner" data-action="create">
									<i class="fa fa-plus-circle a-icon-banner"></i>
									<span class="title-link-banner hidden-sm"><br><?php echo Yii::t("common", "Create") ?></span> 
								</a>
								<?php 
							}
						}
					?>
					<!--
					<a href="#" class="pull-right text-center link-banner">
						<i class="fa fa-plus-circle a-icon-banner"></i> <br>
						<span class="title-link-banner">Ajouter</span>
					</a>
					
					<a href="javascript:;" class="pull-right text-center link-banner">
						<i class="fa fa-user-plus a-icon-banner"></i> <br>
						<span class="title-link-banner">Inviter</span>
					</a>
					-->

					<?php 
						$urlLink = "#element.invite.type.".$element["collection"].".id.".(string)$element["_id"];
						$inviteLink = "people";
						if ($element["collection"] == Organization::COLLECTION) 
							$inviteLink = "members";
						else if ($element["collection"] == Project::COLLECTION ) 
							$inviteLink = "contributors";
						$whereConnect= ($element["collection"]!=Person::COLLECTION) ? 'to the '.Element::getControlerByCollection($element["collection"]) : ""; 
						$classHref=(isset($class) && !empty($class)) ? $class : "text-red"; 
						$classHref.=(isset($tooltip) && !empty($tooltip)) ? " tooltips" : ""; 
					?>

					<?php //if (($edit==true || $openEdition==true) && @Yii::app()->session["userId"]){ ?> 
					<?php if (Authorisation::isElementMember((string)$element["_id"],(string)$element["collection"],Yii::app()->session["userId"])){ ?>
						<a 	href="<?php echo $urlLink ;?>" 
							class="pull-right text-center link-banner lbhp "
							data-placement="bottom" 
							data-original-title="<?php echo Yii::t("common","Invite {what} {where}",array("{what}"=> Yii::t("common",$inviteLink),"{where}"=>Yii::t("common", $whereConnect))); ?>" > 
					        <i class="fa fa-user-plus a-icon-banner"></i> 
					        <span class="title-link-banner hidden-sm"> <br><?php echo Yii::t("common","Invite") ?></span> 
					    </a>
					
					<?php }

					if(in_array($element["collection"],[Organization::COLLECTION,Event::COLLECTION,Project::COLLECTION])){ 
						if($edit==true || ( $openEdition==true && Yii::app()->session["userId"] != null ) ){ ?>
						<a href="javascript:;" class="ssmla pull-right text-center link-banner " data-action="share">
							<i class="fa fa-share-alt a-icon-banner"></i> 
							<span class="title-link-banner hidden-sm"><br><?php echo Yii::t("common", "Share") ?>
							</span>
						</a>
					<?php }
					} ?>
					<?php
						$collections = [
						    Organization::COLLECTION => 'validRssOrga',
						    Project::COLLECTION => 'validRssProjects',
						    Event::COLLECTION => 'validRssEvents',
						];
						$defaultClass = 'validRssCitoyens';
						$validClass = $collections[$element["collection"]] ?? $defaultClass;
						
						$options = [
						    'classifieds' => 'Annonce',
						    'events' => 'Evenement',
						    'news' => 'Journal',
						    'poi' => 'POI',
						    'projects' => 'Projets',
						];
						if (!in_array($element["collection"], [Organization::COLLECTION, Project::COLLECTION, Event::COLLECTION])) {
						    $options['organizations'] = 'Organisation';
						}
						?>
						<div class="dropdown pull-right" style="cursor: pointer;">
						    <div
						        class="text-center link-banner dropdown-toggle" 
						        id="rss-dropdown" 
						        role="button"
						        aria-haspopup="true" 
						        aria-expanded="false" 
						        href="#" 
						        aria-controls="rss-menu"
						    >
						        <i class="fa fa-rss a-icon-banner" aria-hidden="true"></i>
						        <span class="title-link-banner hidden-sm"><br>RSS</span>
							</div>
						    <ul class="dropdown-menu" id="rss-menu" role="menu" aria-labelledby="rss-dropdown">
						        <?php foreach ($options as $value => $label): ?>
						            <li class="text-dark" style="margin-left: 5px; cursor: pointer;">
						                <label>
						                    <input 
						                        type="checkbox" 
						                        value="<?= $value ?>" 
						                        id="rss-option-<?= htmlspecialchars($value) ?>" 
						                        aria-labelledby="label-rss-option-<?= htmlspecialchars($value) ?>"
						                    > 
						                    <span style="cursor: pointer;" id="label-rss-option-<?= htmlspecialchars($value) ?>"><?= htmlspecialchars($label) ?></span>
						                </label>
						            </li>
						        <?php endforeach; ?>
						        <li>
						            <button 
						                class="btn btn-block <?= htmlspecialchars($validClass) ?>" 
						                style="background-color: #9fbd38;" 
						                aria-label="Valider les options sélectionnées pour le flux RSS"
						            >
						                OK
						            </button>
						        </li>
						    </ul>
						</div>

				</div>
		</div>
			<?php
			if($campaign!=null){
				$campId=isset($campaign['_id']) ? (string)$campaign['_id'] : "";
		 		if(isset($element["preferences"]["crowdfunding"]) && $element["preferences"]["crowdfunding"]==true){ ?>
		 		<div class="col-xs-12 no-padding">	
		 			<div class="crowdfunding-badge text-center" style="">
		 				<span class="pull-right close-campaign-popup" style="position:relative;color:white;font-size:14px"><i class="fa fa-times"></i></span>
		 				<div style="display:table">
			 				<span class="padding-5" style="float:left;font-size:45px;"><i class="fa fa-money"></i></span>
			 				<!-- <span style="font-size:14px;"><?php echo Yii::t("common","Now in crowdfunding campaign");?></span></br> -->
			 				<a style="display:table-cell;vertical-align: middle;width:250px;" href="#page.type.crowdfunding.id.<?php echo $campId ?>" class="lbh-preview-element letter-yellow">		<span class="btn-crowd letter-c" style="font-weight:bolder;font-size:18px;"><?php echo Yii::t("common","Crowdfunding campaign running now"); ?><br>
			 				</span>
			 				</a>
			 			</div>		
		 				<?php if ($edit==true){?>
		 					<span class="modify-campaign letter-yellow"><i class="fa fa-pencil"></i> <?php echo Yii::t("common","Edit the campaign") ?></span></a>
		 				<?php } ?>	
		 			</div>	
		 		</div>	

			 <?php
				}
			}
				
			
			?>

		</div>

		<div class="col-xs-12 visible-xs contentHeaderInformation <?php if(@$element["profilBannerUrl"] && !empty($element["profilBannerUrl"])) echo "backgroundHeaderInformation" ?>" style="padding: 15px 15px;">
			<h5 class="text-center padding-left-15 no-margin text-white">
						<span id="nameHeader">
							<span class="bg-<?php echo $iconColor; ?>  icon-before-name">
								<i class="fa fa-<?php echo $icon; ?> text-center"></i>
							</span> 
							<span class="name-header"><?php echo @$element["name"]; ?></span>
						</span>
						<?php if($element["collection"]==Event::COLLECTION && isset($element["type"])){ 
								$typesList=Event::$types;
								if(isset($typesList[$element["type"]])){
						?>
							<span id="typeHeader" class="margin-left-10 pull-left">
								<i class="fa fa-x fa-angle-right pull-left"></i>
								<div class="type-header pull-left">
							 		<?php echo Yii::t("category", $typesList[$element["type"]]) ?>
							 	</div>
							</span>
						<?php }} ?>
					</h5>
					<div class="col-xs-12 text-center text-white">
			        	<?php if(!empty($element["address"]["addressLocality"])){ ?>
			        	<span class="adress" style="margin: 5px;">
			        		<?php
			        		echo "<i class='fa fa-map-marker'></i> ";
								echo !empty($element["address"]["streetAddress"]) ? $element["address"]["streetAddress"].", " : "";
								echo !empty($element["address"]["postalCode"]) ? $element["address"]["postalCode"].", " : "";
								echo $element["address"]["addressLocality"];
							?>
			        	</span>
			        	<?php } ?>
					</div>

		        	<div class="col-xs-12 text-center">
						<?php
						 if(in_array($element["collection"],[Event::COLLECTION,Project::COLLECTION, Organization::COLLECTION])) { ?>
							<div class="event-infos-header"></div>
						<?php } ?>
					</div>
		        	
		</div>

			<!-- DESCRIPTION MENU TOP HORIZONTAL -->

		<div class="col-lg-12 col-md-12 col-sm-12 social-sub-header hidden-xs no-padding">
			<div id=menuTopDesc class="col-md-offset-4 col-sm-offset-4 col-lg-offset-3 col-md-8 col-sm-8 col-lg-9 menuTopDesc">
			<div class="col-md-12 col-sm-12 col-lg-12 text-left padding-10 hidden-xs shortDescTop text-dark-blue elipsis">
				<?php echo @$element["shortDescription"]; ?>	
			</div>
			<div class="col-sm-12 tag-contain visible-sm hidden-xs">
				        <ul class="tag-list no-padding">
				        	<?php if(!empty($element["address"]["addressLocality"])){ ?>
				        	<li class="tag adress padding-left-0">
				        		<?php
				        		echo "<i class='fa fa-map-marker'></i> ";
									echo !empty($element["address"]["streetAddress"]) ? $element["address"]["streetAddress"].", " : "";
									echo !empty($element["address"]["postalCode"]) ? $element["address"]["postalCode"].", " : "";
									echo $element["address"]["addressLocality"];
								?>
				        	</li>
				        	<?php $classCircleO = (!empty($element["tags"]) ? "" : "hidden" ); ?>
										<span id="separateurTag" class="margin-right-10 margin-left-10 text-white pull-left <?php echo $classCircleO ; ?>" style="font-size: 10px;line-height: 35px; color: #9fbd38!important;">
											<i class="fa fa-circle-o"></i>
										</span>
									
								<?php } 
				            
				                if(!empty($element["tags"])){
				                	$countTags=count($element["tags"]);
				                	$nbTags=1;
				                    foreach ($element["tags"]  as $key => $tag) {
				                    	if($nbTags < 5){
					                        echo '<li class="tag" style="margin-top: 3px;"><i class="fa fa-hashtag"></i>&nbsp;'.$tag.'</li>';
					                    	$nbTags++;
				                    	}else{
				                    		if(($countTags-$nbTags) > 0) echo "<li class='indicate-more-tags'>+".($countTags-$nbTags)."</li>";
				                    		break;
				                    	}
				                    }
				                }else{
				                    
				                } ?>
				        </ul>
			<div class="pull-left">
				<?php
				if(in_array($element["collection"],[Event::COLLECTION,Project::COLLECTION, Organization::COLLECTION])) { ?>
					<div class="event-infos-header" style="text-align: left;"></div>
				<?php } ?>
			</div>
		</div>
	</div>
<script>
	var selectedValues = [];
	$("input[type='checkbox']").change(function () {
	    var value = $(this).val();
	    if ($(this).is(":checked")) {
	        selectedValues.push(value);
	    } else {
	        var index = selectedValues.indexOf(value);
	        if (index !== -1) {
	            selectedValues.splice(index, 1);
	        }
	    }
	});
	$(".btn-block").click(function () {
	    var action = $(this).attr("class").split(' ').find(c => c.startsWith('validRss'));
	    var typeMap = {
	        validRssOrga: "organization",
	        validRssProjects: "project",
	        validRssEvents: "event",
	        validRssCitoyens: "person"
	    };
	    var endpoint = typeMap[action];
	    if (endpoint) {
	        window.open(`${baseUrl}/api/${endpoint}/get/id/${elementId}/format/rss/rssParametre/${selectedValues}`);
	    }
	});
</script>
		

<!-- END DESCRIPTION MENU TOP HORIZONTAL -->
	