
<div class="popup-costum"></div>
<div>
    <div id="modal-list-element">
        <div class="modal-list-element-btn ">
            <button id="btn-hide-modal-list-element"><i class="fa fa-times" aria-hidden="true"></i></button>
        </div>
        <div class="">
            <div class="modal-list-element-header ">
                <div class="modal-filter-template"></div>
            </div>
            <div id="modal-element-header" class="col-md-12"></div>
            <div class="modal-list-element-body ">
                <div class="modal-list-element-list ">
                    <div id="modal-element-results"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<style>
    .co-popup-costum-container {
        width: 100%;
        height: 100vh;
        position: fixed;
        top: 0;
        left: 0;
        background-color: rgba(0, 0, 0, .7);
        display: flex;
        justify-content: center;
        align-items: center;
        color: #2c3e50;
        z-index: 99999;
    }
    .co-popup-costum {
        width: 700px;
        min-height: 260px;
        background-color: white;
        border-radius: 10px;
        position: relative;
        opacity: 0;
        animation: myAnim 1s ease 1s 1 normal forwards;
    }
    .co-popup-costum-background {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
    }
    .costum-logo {
        width: 150px;
        height: 150px;
        background-color: white;
        border-radius: 100%;
        position: absolute;
        top: -75px;
        right: 40px;
        text-align: center;
        line-height: 150px;
    }
    .costum-logo img{
        width: 90%;
    }
    .co-popup-costum-header p {
        text-transform : none;  
        font-size : 22px;
        font-family:'Sharp_sans_bold'
    }
    .co-popup-costum-header {      
        padding-top: 80px;
        text-align: center;
    }
    .co-popup-costum-actions {
        display: flex;
        flex-direction: row;
        padding-top: 50px;
        align-items: center;
        justify-content: center;
        margin: 0;
        padding: 0;
        margin-top: 20px;
    }
    .co-popup-costum-btn-close{
        display: flex;
        flex-direction: row;
        padding-top: 50px;
        align-items: center;
        justify-content: end;
        margin: 0;
        padding: 0;
        bottom: 0;
        position: absolute;
        right: 0;
        margin-top: 20px;
    }
    .co-popup-costum-actions li ,
    .co-popup-costum-btn-close li {
        display: flex;
        margin-bottom: 5px;
        margin-right : 10px;
    }
    .co-popup-costum-actions li a{
        text-decoration: none;
        padding: 10px 20px;
        border-radius: 4px;
        color: #2c3e50;
        font-size: 18px;
    }
    .co-popup-costum-btn-close li a {   
        text-decoration: none;
        padding: 4px 10px;
        border-radius: 4px;
        color: #2c3e50;
        font-size: 15px;
    }
    .card-elm   .icon-elm {
        width: 30px;
        height: 30px;
        z-index: 1;
        position: absolute;
        top: 0;
        left: 0;
        font-size: 17px;
        border-radius: 0px 0 21px;
        border-right: 2px solid #fff;
        border-bottom: 2px solid #fff;
        line-height: 25px;
        text-align: center;    
    }
    .card-elm  .img-container{
        height : 100px;
        width: 100%; 
        z-index: 1;
        -webkit-transition: all .3s;
        -moz-transition: all .3s;
        -ms-transition: all .3s;
        -o-transition: all .3s;
        transition: all .3s;
    }
    .card-elm  .img-container img {
        width: 100%;
        height: 100px;
        object-fit: cover;
    } 
    .card-elm .mc-content {
        box-shadow: 0 2px 20px 0 rgba(0,0,0,0.14), 0 3px 1px -2px rgba(0,0,0,0.12), 0 1px 5px 0 rgba(0,0,0,0.2);
    }
    .card-elm .card-body{
        position: relative;
        height: 0;
        padding-bottom: calc(200px - 16px); 
    }
    .card-elm .card-body .div-img .fa-2x {
        font-size: 5em;
    }
    .card-elm:hover{
        transform: scale(1.1, 1.1);
    }
    .card-elm .card-body .div-img {
        background-color: #eee;
        text-align: center;
        height: 100%;
        display: flex;
        align-items: center;
        justify-content: center;
    }
    /* #element {
        margin-top : 5%;
    } */
    .card-elm .card-name span{
        color: white;
        text-decoration : none;
    }
    .card-elm .card-name {
        height: 60px;
        display: flex;
        align-items: center;
        background: black;
        justify-content: center;
        opacity: 1;
    }
    #modal-list-element {
        position: absolute;
        z-index: 999998;
        width: 100%;
        min-width: 650px; 
        min-height : 100vh; 
        left: 0px;
        /* margin-top: -24%; */
        background-color: #3A3A3A;
        color: white;
        transform: translateX(-100%);
        visibility: hidden;
    }
    #modal-list-element.open {
        transform: translateX(0);
        visibility: visible;
        transition: .3s;
    }
    .modal-list-element-header{
        width: 100%;
        display: flex;
        justify-content: space-between;
        align-items: center;
        margin-bottom : 1%;
    }
    .modal-filter-template{
        width : 100%;
    }
    .modal-list-element-btn{
        display: flex;
        width: 100%;
        justify-content: end;

    }

    .modal-list-element-btn button {
        width: 40px;
        height: 40px;
        border-radius: 100%;
        border: none;
        background-color: #333;
    }
    .modal-list-element-body {
        margin-top : 5%;
    }
    #teste{
        width: 100%;
        height: auto;
        top: 0;
        left: 0;
        background-color: rgba(0, 0, 0, .7);
        display: flex;
        justify-content: center;
        align-items: center;
        color: #2c3e50;
    }
    /* .modal-list-element-list {
        overflow-x: hidden;
        height: 100vh;
        overflow-y: auto;
    } */
    .modal-list-element-header .dropdown{
        margin-bottom: 5px;
        margin-top: 5px;
        margin-right: 10px;
        padding-top: 0px;
        padding-bottom: 0px;
        display: inline-block;
        float: left;
    }
    .modal-list-element-header .dropdown .btn-menu {
        padding: 10px 15px;
        font-size: 16px;
        border: 1px solid #6b6b6b;
        color: #6b6b6b;
        top: 0px;
        position: relative;
        border-radius: 20px;
        text-decoration: none;
        background: white;
        line-height: 20px;
        display: inline-block;
        margin-left: 5px;
    }
    .modal-filter-template .dropdown .dropdown-menu .list-filters {
        max-height: 300px;
        overflow-y: scroll;
    }
    .modal-filter-template .dropdown .dropdown-menu button {
        border: none;
        color: black;
        /* width: 100%; */
        text-align: left;
        font-size: 14px;
        padding-left: 15px;
        padding: 10px 10px;
        padding-left: 15px;
    }
    .modal-filter-template .dropdown .dropdown-menu:before{
        bottom: 100%;
        left: 20px;
        margin-top: -20px;
        border: solid transparent;
        content: " ";
        height: 0px;
        width: 0;
        border-width: 11px;
        position: absolute;
    }
    .modal-filter-template .dropdown .dropdown-menu {
        position: absolute;
        overflow-y: visible !important;
        top: 50px;
        left: 5px;
        width: 250px;
        border-radius: 2px;
        border: 1px solid #6b6b6b;
        padding: 0px;
    }
    .modal-filter-template .dropdown .btn-menu{
        border: 1px solid #9fbd38!important;
    }
    #modalFirstStepCostum{
        z-index: 9999998;
    }
    .no-text-transform {
        text-transform: none !important;
    }
</style>
<script>
    var costumObj = {
        createCostum : function(){           
            $(".popup-costum").html(costumObj.views.createCostum()); 
            $(".initElement").click(function(){
                costumObj.initElement();
            })
            $(".useElement").click(function(){
                costumObj.useExistElt();
            })
        },
        initElement : function(){
            var str = costumObj.views.createElt();
            $(".co-popup-costum-content").html(str);
            $(".return").click(function(){
                var src = costumObj.views.createcstcontent();
                $(".co-popup-costum-content").html(src);
                $(".initElement").click(function(){
                    costumObj.initElement();
                })
                $(".useElement").click(function(){
                    costumObj.useExistElt();
                })
            })
            $(".createElement").click(function(){
                var type = $(this).data("form-type"); 
                costumObj.createElement(type);
            })
        },
        createElement : function(type){
            var dyfElt = {};
            dyfElt.afterSave = function(data){
                dyFObj.commonAfterSave(data, function(){
                    if(notNull(contextData))
                        contextData = null; 
                    costumObj.initCostum(data.map._id.$id, data.map.collection, data.map.slug);
                });	
            }
            dyFObj.openForm(type,null, null,null,dyfElt);
        },
        close : function(){
            $(".co-popup-costum-container").hide();
        },
        initCostum:function(id,type,slug){
            params = {
                id : id,
                type : type,
                slug : slug
            }
            ajaxPost(
                null,
                baseUrl+"/co2/cms/createcostum",
                params,
                function(data){          
                    $("#modalFirstStepCostum").remove();                          
                    $("body").append(data);
                    $("#modalFirstStepCostum").modal("show");
                    coInterface.bindCostumizer();
                    $("#modal-list-element").removeClass("open");
                    $(".co-popup-costum-container").hide();
                    dyFObj.closeForm();
                }
            );


        },
        initCostumExistsElt : function(haveCostum,id,type,slug,name=""){
            if(name == "")
                name = slug;
                if(haveCostum == "true"){
                    bootbox.confirm({
                        message: '<b>'+name+'</b> a déjà un costum, voulez-vous rédiriger vers le costum ?',
                        buttons: {
                            confirm: {
                                label: 'Oui',
                                className: 'btn-success'
                            },
                            cancel: {
                                label: 'Non',
                                className: 'btn btn-secondary btn-default bootbox-cancel'
                            }
                        },
                        callback: function (result) {
                            if (!result) {
                                return;
                            } else {
                                window.open(baseUrl+'/costum/co/index/slug/'+slug+'/edit/true', '_blank');
                            }
                        }
                    });
                }else{
                    bootbox.confirm({
                        message: 'Générer un costum pour <b>'+name+'<b> ?',
                        buttons: {
                            confirm: {
                                label: 'Oui',
                                className: 'btn-success'
                            },
                            cancel: {
                                label: 'Non',
                                className: 'btn btn-secondary btn-default bootbox-cancel'
                            }
                        },
                        callback: function (result) {
                            if (!result) {
                                return;
                            } else {
                                costumObj.initCostum(id, type, slug);
                            }
                        }
                    });




                } 
        },
        views : {
            createCostum : function(){
                var src = "";
                src+=`
                <div class="co-popup-costum-container">
                    <div class="co-popup-costum">
                        <div class="co-popup-costum-content">`
                        src += costumObj.views.createcstcontent()
                        src += `</div>
                    </div>
                </div>`;
                return src;
            },
            createcstcontent : function(){
                var src = "";
                src+=`
                    <div class="costum-logo">
                        <img src="<?php echo Yii::app()->getModule("co2")->assetsUrl; ?>/images/logo_costum.png">
                    </div>
                    <div class="co-popup-costum-header">
                        <p>${tradCms.doyouhavefeature}</p>
                    </div>
                    <div>
                        <ul class="co-popup-costum-actions">
                            <li><a href="javascript:;" class="text-white bg-dark initElement">${tradCms.createelement}</a></li>
                            <li><a href="javascript:;" class="text-white bg-dark useElement">${tradCms.useexistingelement}</a></li>
                        </ul>
                        <ul class="co-popup-costum-btn-close">
                            <li><a href="javascript:;" class="btn btn-secondary btn-default" onClick="costumObj.close()">${trad.Close}</a></li>
                        </ul>
                    </div>`;
                return src;
            },
            createElt : function(){
                var src = "";
                src += `
                    <div class="costum-logo"> 
                        <img src="<?php echo Yii::app()->getModule("co2")->assetsUrl; ?>/images/logo_costum.png">
                    </div>
                    <div class="co-popup-costum-header">
                        <p>Vous voulez utiliser quel type d'élément?</p>
                    </div>
                    <div>
                        <ul class="co-popup-costum-actions">
                            <li><a href="javascript:;" class="text-white bg-dark createElement" data-form-type="organization"> ${trad.organization}</a></li>
                            <li><a href="javascript:;" class="text-white bg-dark createElement" data-form-type="project"> ${trad.project}</a></li>
                            <li><a href="javascript:;" class="text-white bg-dark createElement" data-form-type="event">${trad.events}</a></li>
                        </ul> 
                        <ul class="co-popup-costum-btn-close">
                            <li><a href="javascript:;" class="btn btn-secondary btn-default return"><i class="fa fa-arrow-circle-o-left"></i> ${trad.back}</a></li>
                            <li><a href="javascript:;" class="btn btn-secondary btn-default" onClick="costumObj.close()">${trad.Close}</a></li>
                        </ul>
                </div>`; 
                return src;
            },
            viewElement : function(params){
                var haveCostum =false;
                if(typeof params.costum != "undefined" && typeof params.costum.slug != "undefined")
                    haveCostum =true;
                var id = params._id.$id;
                var type = params.collection;
                var slug = params.slug;
                var name = params.name;
                var str='';
                str +=	
                '<div id="entity_'+params.collection+'_'+params.id+'" class="col-lg-2 col-md-3 col-sm-4 col-xs-12 mt-5  card-elm searchEntityContainer '+params.containerClass+'">'+
                    '<div class=" Light-Green card-body">'+
                        '<div class="mc-content">'+
                            `<div class="icon-elm bg-${params.color}">
                                <span><i class="fa fa-${params.icon} "></i></span>
                            </div>
                            <a href='javascript:;'  onClick='costumObj.initCostumExistsElt("${haveCostum}","${id}","${type}","${slug}","${name.replace(/'/g,"’")}")'><div class="img-container">${params.imageProfilHtml}</div></a>`+
                            `<a href="javascript:;" onClick='costumObj.initCostumExistsElt("${haveCostum}","${id}","${type}","${slug}","${name.replace(/'/g,"’")}")'>`+
                                '<div class="card-name">'+
                                `<span>${params.name}</span>`+
                                '</div>'+
                            '</a>'+
                        '</div>'+
                    '</div>'+
                '</div>';
                return str;
            },
        },
        useExistElt : function(){
            var paramsFilter = {
                container : ".modal-filter-template",
                defaults : {
                    types : ["NGO","LocalBusiness","Group","GovernmentOrganization","Cooperative","projects","events"],
                    fields : ["costum"],
                    filters : {
                        $or : {}
                    },
                    notSourceKey : true
                }, 
                results : {
                    dom : "#modal-element-results",
                    smartGrid : true,
                    renderView : "costumObj.views.viewElement"
                },
                interface : {
                    events : {
                        scroll : true,
                        scrollOne : true
                    }
                },
                header: {
                    dom : "#modal-element-header",
                    options : {
                        left : {
                            classes : 'col-xs-8 elipsis no-padding',
                            group:{
                                count : true
                            }
                        }
                    },
                },
                filters : {
                    text : true,
                    hasCostum : {
                        view : "dropdownList",
                        type : "filters",
                        name : tradCms.havecostum,
                        action : "filters",
                        typeList : "object",
                        event : "exists",
                        list :  {
                            "has" : {
                                label: trad.yes,
                                field : "costum.slug",
                                value : true
                            },
                            "not" : {
                                label: trad.no,
                                field : "costum.slug",
                                value : false
                            }
                        }
                    },
			 		types : {
			 			lists : [
                            "NGO", 
                            "LocalBusiness", 
                            "Group", 
                            "GovernmentOrganization", 
                            "Cooperative" ,
                            "projects", 
                            "events"]
			 		},
                }                
            };
            if(userId){
                paramsFilter.defaults.filters['$or'] = {
                    ["links.attendees."+userId] : {'$exists':true},
                    ["links.members."+userId] : {'$exists':true},
                    ["links.contributors."+userId] :{'$exists':true} 
                }
            }
            $("#btn-hide-modal-list-element").off("click").on("click", function(){
                $("#modal-list-element").removeClass("open")
            })
            $("#modal-element-header").html("")
            $("#modal-list-element").addClass("open")
            filterSearch = searchObj.init(paramsFilter);
            filterSearch.search.init(filterSearch);
        }
    } 
</script>