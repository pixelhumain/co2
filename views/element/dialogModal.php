<!-- Modal -->
<div class="modal modal-custom modal-custom-backdrop fade" id="dialogContent" tabindex="-1" role="dialog" aria-labelledby="dialog-content" aria-hidden="true">
    <div class="modal-dialog modal-custom-dialog">
        <div class="modal-content">
            <div class="close-modal p-2" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            <div class="modal-body" id="dialogContentBody">
            
            </div>
            <div class="p-4 text-center">
                <a href="javascript:" style="font-size: 13px;" type="button" class="" data-dismiss="modal">
                    <i class="fa fa-times"></i> <?= Yii::t("common", "Back") ?>
                </a>
            </div>
        </div>
    </div>
</div>