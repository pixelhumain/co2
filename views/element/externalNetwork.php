<!-- begin loader -->
<style>
    .lds-ring {
        display: inline-block;
        position: relative;
        width: 40px;
        height: 40px;
    }
    .lds-ring div {
        box-sizing: border-box;
        display: block;
        position: absolute;
        width: 50px;
        height: 50px;
        margin: 8px;
        border: 6px solid #9fbd38;
        border-radius: 50%;
        animation: lds-ring 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;
        border-color: #9fbd38 transparent transparent transparent;
    }
    .lds-ring div:nth-child(1) {
        animation-delay: -0.45s;
    }
    .lds-ring div:nth-child(2) {
        animation-delay: -0.3s;
    }
    .lds-ring div:nth-child(3) {
        animation-delay: -0.15s;
    }
    @keyframes lds-ring {
        0% {
            transform: rotate(0deg);
        }
        100% {
            transform: rotate(360deg);
        }
    }

    .external-network-loader{
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        display: none;
        justify-content: center;
        align-items:center;
    }
    .external-network-loader.active{
        display: flex;
    }
</style>
<!-- end loader -->
<style>
    .external-network-container{
        width: 80%;
        margin-left: 10%;
        background-color: white;
        padding: 20px;
        font-family: 'Lato', 'Helvetica Neue', Helvetica, Arial, sans-serif;
        position: relative;
    }

    .external-network-header{
        display: flex;
        align-items: center;
        justify-content: space-between;
    }

    .external-network-filter{
        margin: 0;
        padding: 0;
    }
    .external-network-filter li{
        list-style: none;
        display: inline-block;
    }
    .external-network-filter li a{
        text-decoration: none;
        padding: 5px 10px;
        border-radius: 5px;
        font-size: 16px;
        color: #707070;
        display: block;
    }

    .external-network-filter li a.active{
        font-weight: bold;
        background: #9fbd38;
        color: white;
    }

    .input-finder{
        padding: 10px;
        border: 1px solid #707070;
        border-radius: 5px;
    }

    .input-finder input{
        border: none;
        font-size: 16px;
        width: 300px;
    }
    .input-finder input:focus{
        outline: none;
    }

    .input-finder a{
        text-decoration: none;
        font-size: 18px;
        color: black;
    }

    .external-network-body{
        padding-top: 40px;
        min-height: 300px;
    }
    .external-network-items{
        padding: 10px;
        border-bottom: 1px solid #dadada;
        position: relative;
    }
    .external-network-tags{
        margin-left: 60px;
        animation: slide-bottom 0.5s cubic-bezier(0.250, 0.460, 0.450, 0.940) both;
        animation-duration: calc(0.35s * var(--index));
    }
    .external-network-item{
        display: flex;
        align-items: center;
        justify-content: space-between;
        animation: slide-bottom 0.5s cubic-bezier(0.250, 0.460, 0.450, 0.940) both;
        animation-duration: calc(0.35s * var(--index));
    }
    @keyframes slide-bottom {
        0% {
            opacity: 0;
            -webkit-transform: translateY(-50px);
                    transform: translateY(-50px);
        }
        100% {
            opacity: 1;
            -webkit-transform: translateY(0px);
                    transform: translateY(0px);
        }
    }
    .external-network-item-info{
        display: flex;
    }
    .external-network-item-info img{
        width: 50px;
        height: 50px;
        object-fit: cover;
        border-radius: 5px;
    }

    .external-network-item-info div{
        padding-left: 10px;
    }

    .external-network-item-info p{
        margin: 0;
        font-size: 14px;
        color: #707070;
        margin-bottom: 2px;
    }
    .external-network-item-info p a{
        text-decoration: none;
        color: black;
        font-weight: bold;
        font-size: 16px;
    }
    .external-network-item-info p a:hover{
        text-decoration: underline;
    }

    .external-network-item-action{
        margin:0;
        padding: 0;
    }
    .external-network-item-action li{
        list-style: none;
        display: inline-block;
        margin: 0px 4px;
    }
    .external-network-item-action li a{
        text-decoration: none;
        font-size: 18px;
    }
    .external-network-item-action li a.btn-green{
        color: #9fbd38;
        background-color: transparent !important;
    }
    .external-network-item-action li a.btn-red{
        color: #e6344d;
    }

    @keyframes rotating {
        from {
            transform: rotate(0deg);
        }
        to {
            transform: rotate(360deg);
        }
    }

    .external-network-item-action a.loading{
        cursor:progress;
    }

    .external-network-item-action a.loading i{
        opacity: .6;
        animation: rotating 1s linear infinite;
    }

    .msg-result p{
        font-size: 16px !important;
        color: #707070;
    }
    .msg-result{
        display: flex;
        justify-content: center;
        padding-top: 100px;
    }
</style>

<?php

use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\Config;

if(Preference::isActivitypubActivate($user["preferences"])){ ?>
<div class="external-network-container">
    <div class="external-network-header">
        <ul class="external-network-filter">
            <li><a href="javascript:;" data-type="follows" class="active">Abonnements</a></li>
            <li><a href="javascript:;" data-type="followers">Abonnés</a></li>
        </ul>
        <div class="external-network-finder">
            <div class="input-finder">
                <input id="input-search-external-network" type="text" placeholder="@nom@domaine">
                <a href="javascript:;"><i class="fa fa-search" aria-hidden="true"></i></a>
            </div>
        </div>
    </div>
    <div class="external-network-body">

    </div>
    <div class="external-network-loader">
        <div class="lds-ring"><div></div><div></div><div></div><div></div></div>
    </div>
</div>
<script>
    var AP_HOST = "<?= Config::HOST() ?>";
    /*
        Data structure
        actor:{
            link:{
                follow:{
                    activityId:string,
                    pending: boolead
                },
                follower:{
                    activityId:string,
                    pending: boolead
                }
            },
            data: Object
        }
    */
    async function isInstanceMobilizon(id) {
          var domain = (new URL(id));
          domain = domain.hostname.replace('www.','');
          const response = await fetch('https://instances.joinmobilizon.org/api/v1/instances?start=0&count=10&sort=totalLocalGroups&search='+ domain);
          const data = await response.json();
          return data.total > 0;
    }
    function getActorType(){
        if(contextData!=undefined && contextData.type!=null && contextData.type == "organizations" && contextData.typeElement!=null && contextData.typeElement== "Group"){
            return "group";
        }else{
            return "person"
        }
    }
    function showInvitationBadge(actor,index){
          var isPending = actor.data.link.followers.pending;

          if(isPending){
            return `<span class="badge badge-Info">Invitation à devenir membre</span>`;
          }else{
            return ``;
          }

    }
    var externalNetwork = {
        default:{
            actorIcon:"<?= Yii::$app->getModule('co2')->assetsUrl."/images/avatar.jpg" ?>",
            actionInProgress:false,
            actoryType: getActorType()
        },
        filter:{
            linkType:"follows",
            reset:function(){
                externalNetwork.filter.linkType = "follows"
                $(".external-network-filter a").removeClass("active")
                $(".external-network-filter a[data-type='follows']").addClass("active")
            }
        },
        actions:{
            buttons:{
                undo_request_follow:{
                    id:"undo_request_follow",
                    label:"Annuler la demande",
                    icon:"ban"
                },
                remove_follow:{
                    id:"undo_follow",
                    label:"Ne plus suivre",
                    icon:"user-times",
                },
                accept_follower:{
                    id:"accept_follower",
                    label:"Accepter",
                    icon:"check"
                },
                follow:{
                    id:"follow",
                    label:"Suivre",
                    icon:"user-plus"
                },
                reject_follower:{
                    id:"reject_follower",
                    label:"Réfuser",
                    icon:"user-times"
                },
                remove_follower:{
                    id:"undo_accept",
                    label:"Annuler l'acceptation",
                    icon:"user-times"
                },
                accept_invitation:{
                    id:"accept_invitation",
                    label:"Accepter l'invitation",
                    icon:"check"
                },
                reject_invitation:{
                    id:"reject_invitation",
                    label:"Réfuser l'invitation",
                    icon:"times"
                },
                 remove_invitation:{
                    id:"undo_invitation",
                    label:"Annuler l'acceptation",
                    icon:"times"
                },
            },
            get:function(actor){
                var actions = [],
                    actorId = actor.data["id"],
                    buttons = externalNetwork.actions.buttons;
                var activeFilter= "";
                if(externalNetwork.filter.linkType=="followers"){
                    if(isInstanceMobilizon(actor.data["id"])){
                        activeFilter = "invitation";
                    }else{
                        activeFilter = "followers";
                    }
                }else{
                    activeFilter = "follows";
                }

                if(!isLocalActor(actorId)){
                    if(activeFilter == "follows"){
                        if(actor.link.follows){
                            var isPending = actor.link.follows.pending;
                            if(isPending)
                                actions.push(buttons.undo_request_follow)
                            else
                                actions.push(buttons.remove_follow)
                        }else
                            actions.push(buttons.follow)
                    }else{
                        if(actor.link.followers){
                            var isPending = actor.link.followers.pending;
                            if(isPending){
                                if(isInstanceMobilizon(actor.data["id"])){
                                    actions.push(buttons.accept_invitation)
                                    actions.push(buttons.reject_invitation)
                                }else{
                                    actions.push(buttons.accept_follower)
                                    actions.push(buttons.reject_follower)
                                }
                            }else{
                              //  if(!isInstanceMobilizon(actor.data["id"])){
                                    actions.push(buttons.remove_follower)
                                //}
                            }
                        }
                        if(!actor.link.follows)
                            if(!isInstanceMobilizon(actor.data["id"]))
                                actions.push(buttons.follow)
                    }

                    actions = actions.map(function(action){
                        action.payload = actor.data["id"];
                        return action
                    })
                }

                return actions;
            }
        },
        views:{
            initTotalPendingLink: function(){
                activitypub.services.link.getTotalPending(elementId, "followers", externalNetwork.default.actoryType, function(total){
                    $el = $(".external-network-filter a[data-type='followers']");
                    $el.find("span").remove()
                    if(total > 0)
                        $el.append(`<span class="badge margin-left-5">${total}</span>`)
                })

            },
            renderItem:function($container, actor, index=1,zIndex){
                //  var inviteBadge = showInvitationBadge(externalNetwork,actor,index);
                var actorData = actor.data,
                    actorIcon = (actorData.icon && actorData.icon.url) ? actorData.icon.url : externalNetwork.default.actorIcon,
                    actions = externalNetwork.actions.get(actor),
                    itemLabel = isLocalActor(actorData.id) ? "Utilisateur interne" : `@${actorData.preferredUsername}@${(new URL(actorData.id)).hostname}`,
                    itemUrl = isLocalActor(actorData.id) ? "/#@" + actorData.preferredUsername : actorData.id, tags;
                if (actor.link.follows !== undefined) {
                    tags = actor.link.follows.tags !== undefined ? actor.link.follows.tags : [];
                } else {
                    tags = [];
                }

                var $item = $(`
                    <div class="external-network-items" style="z-index:${zIndex}">
                        <div class="external-network-item" style="--index:${index}">
                            <div class="external-network-item-info">
                                <img src="${actorIcon}" alt="">
                                <div>
                                    <p><a href="${itemUrl}" target="_blank">${actorData.name ? actorData.name : actorData.preferredUsername}</a></p>
                                    <p>${itemLabel}</p>
                                </div>

                            </div>

                            <ul class="external-network-item-action"></ul>
                        </div>

                      <div class="external-network-tags" style="--index:${index};display:${externalNetwork.filter.linkType==="follows" ? 'block': 'none' }">
                          <input-tag
                              value="${tags}"
                              context="${contextData.id}"
                              type="${externalNetwork.default.actoryType}"
                              id="${actorData.id}"
                              name="${contextData.name}" />
                          </div>
                      </div>
                `)

                var $actionsContainer = $item.find(".external-network-item-action");
                actions.forEach(function(action){
                    $actionsContainer.append(`
                        <li>
                            <a href="javascript:;" data-action="${action.id}" data-payload="${action.payload}" title="${action.label}">
                                <i class="fa fa-${action.icon}" aria-hidden="true"></i>
                            </a>
                        </li>
                    `)
                })

                $container.append($item);
            },
            renderItems:function($container, actors){
                $container.html("");
                var zIndex = actors.length;
                actors.forEach(function(actor, index){
                    zIndex -= 1;
                    externalNetwork.views.renderItem($container, actor, index+1,zIndex);
                })
            }
        },
        events:{
            filterType:function(){
                $(".external-network-filter a").off().click(function(){
                    $(".external-network-filter a").removeClass("active")
                    $(this).addClass("active");
                    externalNetwork.filter.linkType = $(this).data("type");
                    externalNetwork.init();
                })
            },
            finder:function(){
                $("#input-search-external-network").off().keyup(function(e){
                    var ENTER_KEY_CODE = 13;
                    if(e.keyCode == ENTER_KEY_CODE){
                        externalNetwork.filter.reset()
                        externalNetwork.utils.setLoader()
                        activitypub.services.search($(this).val(), function(data){
                            externalNetwork.utils.setLoader(false, function(){
                                $(".external-network-body").html("")
                                if(!data.error){
                                    externalNetwork.views.renderItem($(".external-network-body"), data.actor)
                                    externalNetwork.events.init();
                                }else{
                                    displayMessage("Aucun résultat avec cet adresse.")
                                }
                            }, 200)
                        })
                    }
                })
            },
            actorAction:function(){
                $(".external-network-item-action a").off().click(function(){
                    if(!externalNetwork.default.actionInProgress){
                        externalNetwork.default.actionInProgress = true;
                        $(this).addClass("loading").html('<i class="fa fa-circle-o-notch" aria-hidden="true"></i>')
                        activitypub.services.link.update($(this).data("action"), $(this).data("payload"),{
                            id: contextData.id,
                            type: externalNetwork.default.actoryType,
                            name: externalNetwork.default.actoryType == "group" ? contextData.slug : contextData.username
                        }, function(){
                            externalNetwork.default.actionInProgress = false;
                            externalNetwork.init()
                        })
                    }
                })
            },
            init:function(){
                externalNetwork.events.filterType()
                externalNetwork.events.finder()
                externalNetwork.events.actorAction()
            }
        },
        utils:{
            setLoader:function(show=true, callback=false, timeout=false){
                var action = function(){
                    if(show)
                        $(".external-network-loader").addClass("active")
                    else
                        $(".external-network-loader").removeClass("active")
                }

                if(timeout){
                    setTimeout(function(){
                        action()
                        if(typeof callback == 'function') callback();
                    }, timeout)
                }
                action()
            }
        },
        init:function(){
            externalNetwork.views.initTotalPendingLink()
            externalNetwork.utils.setLoader()
            activitypub.services.link.get(elementId, externalNetwork.filter.linkType, externalNetwork.default.actoryType, function(actors){
                externalNetwork.utils.setLoader(false, function(){
                    if(actors.length > 0)
                        externalNetwork.views.renderItems($(".external-network-body"), actors)
                    else{
                        var msg = "";
                        if(externalNetwork.filter.linkType == "follows"){
                            msg = "Vous n'avez aucun abonnement.";
                        }else{
                            msg = "Vous n'avez aucun abonné."
                        }
                        displayMessage(msg)
                    }
                    externalNetwork.events.init();
                }, 200)
            })
        }
    }

    $(function(){
        externalNetwork.init();

        if(activitypubGuide.currentStep.step == activitypubGuide.constants.steps.COMMUNITY) {
            setTimeout(function(){
                activitypubGuide.actions.open(activitypubGuide.constants.steps.SEARCH)
            }, 1000)
        }
    })

    function displayMessage(message){
        $(".external-network-body").html(`
        <div class="msg-result">
            <p>${message}</p>
        </div>
        `);
    }

    function resetExternalNetworkFilter(){
        currentFilter = "";
        $(".external-network-filter a").removeClass("active");
    }

    function isLocalActor(actorId){
        var actorHost = (new URL(actorId)).host;

        return AP_HOST == actorHost;
    }
    // custom element


    class InputTag extends HTMLElement{
        constructor() {
            super();
            this.template = document.createElement('template');
            this.documentClickHandler = this.handleDocumentClick.bind(this);
            document.addEventListener('click', this.documentClickHandler);
            this.template.innerHTML = `
        <style>
        :host{
            display: inline-block;
        }

        .tag-input-container{
            -webkit-writing-mode: horizontal-tb !important;
            text-rendering: auto;
            letter-spacing: normal;
            word-spacing: normal;
            text-transform: none;
            text-indent: 0px;
            text-shadow: none;
            display: inline-block;
            text-align: start;
            -webkit-appearance: textfield;
            -webkit-rtl-ordering: logical;
            cursor: text;
            padding: 1px 2px;
            border-width: 1px;
            border-image: initial;
            z-index: 9999;
        }
        .tag-input-container .data-tag-container{
            display: inline-block;
        }
        .tag-input-container .data-tag{
            display: inline-block;
            border: solid 1px #ccc;
            padding: 1px 5px;
            margin: 1px;
            cursor: pointer;
        }
        .tag-input-container .data-tag:hover{
            background-color: #eee;
        }
        .tag-input-container .data-tag a{
            display:inline-block;
            font-weight: bolder;
            padding: 0 0 0 5px;
            cursor: pointer;
        }

        .data-tag a svg{
            display: inline-block;
            vertical-align: middle;
            margin-top: -1px;
        }


        .tag-input-container input{
            background: transparent;
            font-size: 1em;
            border: 0;
            outline: none;
            margin: 1px;
        }

        .tag-input-container .suggestions-container {
          position: absolute;
          z-index: 1000;
          background-color: #fff;
          border: 1px solid #ccc;
          box-shadow: 0 2px 4px rgba(0, 0, 0, 0.2);
          max-height: 150px;
          overflow-y: auto;
          width: 70%;
        }

        .tag-input-container .suggestion-item {
          padding: 8px 12px;
          cursor: pointer;
        }

        .tag-input-container .suggestion-item:hover {
          background-color: #f0f0f0;
        }

        .tag-input-container .suggestion-item:first-child {
          border-top: none;
        }

        .tag-input-container .suggestion-item:last-child {
          border-bottom: none;
        }
        .tag-input-container  .tag-icon {
          display: inline-block;
          cursor: pointer;
          margin-left: 3px;
          margin-top: 3px;
        }
        .tag-input-container .hidden {
          display: none;
        }

        </style>
      `;
            const el = this;
            this.data = [];
            this.focus = '';
            const shadowRoot = this.attachShadow({mode: 'open'});
            shadowRoot.appendChild(this.template.content.cloneNode(true));

            this.mainContainer = document.createElement('div');
            this.tags = document.createElement('div');
            this.input = document.createElement('input');

            this.mainContainer.setAttribute('class', 'tag-input-container');
            this.tags.setAttribute('class', 'data-tag-container');
            this.input.setAttribute('class', 'tag-input hidden');
            this.input.setAttribute('placeholder', 'Ajouter des tags ...');

            this.tagIcon = document.createElement('div');
            this.tagIcon.setAttribute('class', 'tag-icon');
            this.tagIcon.innerHTML = `<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-tags-fill" viewBox="0 0 16 16" id="IconChangeColor" transform="scale(1,1)"> <path d="M2 2a1 1 0 0 1 1-1h4.586a1 1 0 0 1 .707.293l7 7a1 1 0 0 1 0 1.414l-4.586 4.586a1 1 0 0 1-1.414 0l-7-7A1 1 0 0 1 2 6.586V2zm3.5 4a1.5 1.5 0 1 0 0-3 1.5 1.5 0 0 0 0 3z" id="mainIconPathAttribute"></path> <path d="M1.293 7.793A1 1 0 0 1 1 7.086V2a1 1 0 0 0-1 1v4.586a1 1 0 0 0 .293.707l7 7a1 1 0 0 0 1.414 0l.043-.043-7.457-7.457z" id="mainIconPathAttribute"></path> </svg>`;
            this.mainContainer.appendChild(this.tagIcon);
            this.mainContainer.appendChild(this.input);
            this.mainContainer.appendChild(this.tags);

            shadowRoot.appendChild(this.mainContainer);
            this.mainContainer.addEventListener('click', e =>{
                this.input.focus();
            });

            this.tagIcon.addEventListener('click', () => {
                this.input.classList.remove('hidden');
                this.input.focus();
                this.tagIcon.classList.add('hidden');

            });

            this.tags.addEventListener('click', e =>{
                this.focus = 'tags';
            });

            this.input.addEventListener('focus', e =>{
                el.classList.add('focus');
            });
            this.input.addEventListener('focusout', e =>{
                el.classList.remove('focus');
            });

            this.input.addEventListener("input", async (e) => {
                const value = e.target.value.trim();
                await this.getSuggestions(value).then(suggestions => this.showSuggestions(suggestions)).catch((e) => {
                   // console.log(e);
                });


                if (e.key === 'Enter' && value.length > 0) {
                    e.preventDefault();
                    if (!this.exists(value)) {
                        this.add(value);
                    }
                    e.target.value = '';
                }
            });



            this.input.addEventListener('keydown', e =>{
                const value = e.target.value.trim();

                if ((e.key === 'Enter' || e.key === 'Tab') && value.length > 0) {
                    e.preventDefault();
                    this.add(value, e);
                }

                if(e.key === 'Backspace' && value.length === 0 && this.data.length > 0){
                    this.remove(-1);
                    e.target.focus();
                }
            });
        }
        /**
         * The getSuggestions function returns an array of suggestions based on the input.
         *
         *
         * @param input Filter the predefinedsuggestions array
         *
         * @return An array of suggestions
         */
        async getSuggestions(input) {
            const response = await fetch(`${baseUrl}/api/activitypub/suggestions`);
            const data = await response.json();
            const filteredSuggestions = data.filter(suggestion =>
                suggestion.toLowerCase().includes(input.toLowerCase()) && !this.exists(suggestion) && suggestion.toLowerCase() !== input.toLowerCase()
            );
            return filteredSuggestions;
        }

        handleDocumentClick(event) {
            const isClickedOutside = !this.contains(event.target);
            if (isClickedOutside) {
                // Fermer les suggestions ici
                const suggestionsContainer = this.shadowRoot.querySelector('.suggestions-container');
                if (suggestionsContainer) {
                    suggestionsContainer.remove();
                }
            }
        }

        /**
         * The showSuggestions function is responsible for displaying the suggestions to the user.
         * It does this by creating a new div element with class 'suggestions-container' and appending it to the main container.
         * Then, it iterates through each suggestion in the array of suggestions passed as an argument, creates a new div element with class 'suggestion-item', sets its text content to be equal to that of one of those suggestions, adds an event listener so that when clicked on it will add that suggestion as a tag and clear out any existing tags from view (and from memory), then finally appends this newly created div element into our
         *
         * @param suggestions Pass in the array of suggestions
         */
        showSuggestions(suggestions) {
            var suggestionsContainer = this.mainContainer.querySelector('.suggestions-container');

            if (suggestions.length === 0) {
                if (suggestionsContainer) {
                    suggestionsContainer.remove();
                }
                return;
            }

            if (suggestionsContainer) {
                suggestionsContainer.innerHTML = '';
            } else {
                const newSuggestionsContainer = document.createElement('div');
                newSuggestionsContainer.classList.add('suggestions-container');
                this.mainContainer.appendChild(newSuggestionsContainer);
                suggestionsContainer = newSuggestionsContainer;
            }

            suggestions.forEach(suggestion => {
                const suggestionItem = document.createElement('div');
                suggestionItem.classList.add('suggestion-item');
                suggestionItem.textContent = suggestion;

                suggestionItem.addEventListener('click', () => {
                    this.add(suggestion);
                    this.input.value = '';
                    suggestionsContainer.remove();
                    this.input.focus();
                });

                suggestionsContainer.appendChild(suggestionItem);
            });
        }


        /**
         * The add function adds a new value to the data array.
         *
         *
         * @param value Add a new value to the data array
         * @param e Get the value of the input field
         *
         * @return The value of the data array, which is an array of strings
         */
        add(value, e) {
            if (value && !this.exists(value)) {
                this.data.push(value);
                this.showData();
            } else if (!value && e) {
                const selectedSuggestion = e.target.getAttribute('data-suggestion');
                if (selectedSuggestion && !this.exists(selectedSuggestion) && !this.exists(selectedSuggestion.toLowerCase())) {
                    this.data.push(selectedSuggestion);
                    this.showData();
                }
            }

            if (e) {
                e.target.value = '';
                e.target.focus();
            }
            this.save(this.data);
        }


        /**
         * The value function sets the value of the input element.
         *
         *
         * @param data Set the value of the input element
         *
         * @return The value of the attribute
         */
        set value(data){
            this.setAttribute('value', data);
        }

        /**
         * The value function sets the value of the input element.
         * @return The value of the attribute
         */
        get value(){
            return this.getAttribute('value');
        }

        /**
         * The remove function removes the item at the given index from this.data
         * @param index Identify the index of the item to be removed
         *
         * @return The removed item
         */
        remove(index){
            this.data.splice(index,1);
            this.showData();
            this.save(this.data);
        }

        /**
         * The exists function checks if a value exists in the set.
         * @param value Check if the value is in the array
         * @return A boolean value
         */
        exists(value){
            if(this.data.length === 0) return false;
            const res = this.data.filter(item => {
                return item === value
            });

            return res.length > 0;
        }

        /**
         * The showData function is responsible for displaying the data in the input field.
         * It does this by first clearing out any existing tags and then creating new ones based on the current data array.
         * The function also clears out any existing text in the input field, and appends both it and all of our newly created tags to our main container element.
         *
         */
        showData() {
            const values = this.data.join(',');
            this.value = values;

            this.tags.innerHTML = '';

            this.mainContainer.innerHTML = '';

            this.data.forEach((item, index) => {
                const newTag = this.createTag(index, item);
                this.tags.appendChild(newTag);
            });

            this.mainContainer.appendChild(this.tags);
            this.mainContainer.appendChild(this.input);
            this.mainContainer.appendChild(this.tagIcon);
            this.tags.querySelectorAll('.data-tag a').forEach(item => {
                item.addEventListener('click', this.closeButtonEvent);
            });
        }

        /**
         * The createTag function creates a new tag element and appends it to the DOM.
         *
         *
         * @param index Identify the index of the data array that is being passed in
         * @param text Create the text node that will be displayed in the data tag
         */
        createTag(index, text){
            const dataTag = document.createElement('div');
            const dataTagText = document.createTextNode(text);
            const closeButton = document.createElement('a');
            const icon = document.createElement('div');

            dataTag.setAttribute('data-id', index);
            dataTag.setAttribute('class', 'data-tag');
            closeButton.setAttribute('data-id', index);

            icon.innerHTML = `<svg xmlns="http://www.w3.org/2000/svg" width="12" height="12" viewBox="0 0 18 18" data-id=${index}><path data-id=${index} d="M14.53 4.53l-1.06-1.06L9 7.94 4.53 3.47 3.47 4.53 7.94 9l-4.47 4.47 1.06 1.06L9 10.06l4.47 4.47 1.06-1.06L10.06 9z"/></svg>`;
            closeButton.appendChild(icon);
            closeButton.addEventListener('click', e =>{
                const id = e.target.getAttribute('data-id');
                this.data.splice(id, 1);
                this.showData();
                this.input.focus();
                this.save(this.data);
            });

            dataTag.appendChild(dataTagText);
            dataTag.appendChild(closeButton);

            return dataTag;
        }

        /**
         * The connectedCallback function is called when the element is inserted into the DOM.
         * It checks if there are any values in this.value, and if so, it splits them by commas and stores them in an array at this.data
         * Then it calls showData to display those values on screen as a list of buttons with delete icons next to them
         *
         */
        connectedCallback(){
            if(this.value === null) return false;
            if(this.value !== ''){
                const values = this.value.split(',');
                this.data = [...values];
                this.showData();
            }
        }
        save(value){
            activitypub.services.link.update('add-tag',value,{
                id: this.id,
                name: contextData.name,
                type: contextData.type
            },function(){
             //   console.log('tag added');
            });
        }
        /**
         * The disconnectedCallback function is called when the element is removed from the DOM.
         *
         */
        disconnectedCallback(){
            this.tags.innerHTML = '';
            this.mainContainer.innerHTML = '';
            this.data = [];
            this.input.value = '';
            document.removeEventListener('click', this.documentClickHandler);
        }


    }
    window.customElements.define('input-tag', InputTag);
</script>
<?php }else{ ?>
<div class="external-network-container" style="display: flex; justify-content:center;align-items:center;">
    <p style="font-size: 20px; padding:30px 0px;">Veuillez activer le réseau externe dans le paramètre de votre compte.</p>
</div>
<?php } ?>
