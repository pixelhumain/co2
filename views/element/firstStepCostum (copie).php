<?php 
$assetsUrl = Yii::app()->getModule('costum')->assetsUrl;
HtmlHelper::registerCssAndScriptsFiles(["/css/blockcms/swiper/swiper-bundle.min.css","/js/blockcms/swiper/swiper-bundle.min.js"], $assetsUrl);
 ?>
<style type="text/css">
    #modalFirstStepCostum{
        background-color: white;
        z-index:99999;
        font-size: 18px;
    }
    #modalFirstStepCostum .startfirstStepCostum{
        color: white;
        background-color: #e6344d;
        font-size: 24px;
        border-color: #e6344d;
        font-weight: 700;
        border-radius: 10px;
        padding: 7px 25px;
    }
    #modalFirstStepCostum .startfirstStepCostum:hover{
        color: #e6344d;
        background-color: white;
        font-size: 24px;
        font-weight: 700;
        border-radius: 10px;
        padding: 7px 25px;
    }

    .d-flex {
        display: flex;
    }

    .c-card {
        padding: 20px;
    }

    #modal-blockcms.open {
        transform: translateX(0);
        visibility: visible;
        transition: .3s;
    }
    #modal-blockcms {
        position: absolute;
        z-index: 999998;
        width: 100%;
        min-width: 650px;
        height: calc(100vh - 40px);
        top: 42px;
        left: 0px;
        background-color: #3A3A3A;
        color: white;
        transform: translateX(-100%);
        visibility: hidden;
    }
    .modal-blockcms-header {
        width: 100%;
        display: flex;
        justify-content: space-between;
        align-items: center;
        padding: 20px;
    }
    .modal-blockcms-header {
        background-color: #333333;
        border-top: 1px solid #ababab;
    }

    #category-template-filter {
        padding: 0px;
        height: calc(100vh - 120px);
        background-color: #333333;
    }
    .modal-blockcms-body {
        height: 100%;
    }

    .modal-blockcms-body {
        display: flex;
    }

    .container-filters-menu {
        /* background-color: #FF286B; */
        padding: 0px
        px
        10px;
        margin: 0px;
        display: inline-block;
        width: 100%;
    }

    .panel-group {
        margin-bottom: 20px;
    }

    .modal-blockcms-header .searchBar-filters {
        width: 100%;
    }

    .modal-blockcms-header .searchBar-filters .main-search-bar-addon {
        background-color: #333333!important;
    }

    .modal-blockcms-header .searchBar-filters .search-bar, .modal-blockcms-header .searchBar-filters .main-search-bar-addon {
        margin-top: 0px;
    }

    .modal-blockcms-header button {
        width: 40px;
        height: 40px;
        border-radius: 100%;
        border: none;
        background-color: #333;
    }

    #category-template-filter .verticalList {
        padding: 0px;
        text-align: center;
    }

    #category-template-filter .verticalList .removeAllActiveFilters {
        background: #f4a502;
        border: 0px solid;
        margin: auto;
        padding: 4px 11px;
        border-radius: 5px;
    }

    #category-template-filter .verticalList .btn-filters-select {
        border: 0px;
        width: 100%;
        background-color: #333333;
        text-align: left;
    }
    #category-template-filter .verticalList .btn-filters-select.active {
        background-color: #f4a502;
        color: #333333;
        font-weight: bolder;
    }

    .modal-blockcms-list {
        width: 100%;
        height: calc(100vh - 126px);
        overflow-y: auto;
        display: flex;
        /* display: grid; */
        /* grid-template-columns: repeat(3, 1fr); */
        /* grid-gap: 20px; */
        padding: 0px 20px;
        /* background: white; */
        overflow-x: hidden;
    }
    #modal-template-results {
        width: 100%;
        position: relative;
    }

    #modal-template-results {
        padding-top: 20px;
    }
    #modal-template-results .gallery-masonry-item-wrapper {
        position: relative;
    }
    #modal-template-results .smartgrid-slide-element .tools-kit {
        position: absolute;
        height: 100%;
        width: 100%;
        top: 0px;
        left: 0px;
        bottom: 0px;
        overflow-y: auto;
        overflow-x: hidden;
        background: rgba(0, 0, 0, .8);
        display: none;
        transition: all .5s ease;
        -moz-transition: all .5s ease;
        -webkit-transition: all .5s ease;
        -moz-border-radius: 5px;
        cursor: pointer;
    }
    #modal-template-results .smartgrid-slide-element .tools-kit .btnContent {
        width: 100%;
        height: 100%;
        display: grid;
        align-content: center;
        /* align-self: center; */
        justify-items: center;
    }
    #modal-template-results .smartgrid-slide-element .tools-kit .btn {
        border-radius: 4px;
        width: fit-content;
        border: 1px solid #b1b0b0;
        height: fit-content;
        padding: 10px 25px;
        color: #b1b0b0;
    }
    #modal-template-results .searchEntityContainer .gallery-caption-content {
        font-size: 20px;
        font-weight: 800;
        font-variant: all-small-caps;
    }
    #modal-template-results .searchEntityContainer .gallery-caption-content i {
        line-height: 30px;
    }

    #modal-preview-template.open {
        transform: translateX(0);
        transition: .3s;
        visibility: visible;
    }

    #modal-preview-template {
        position: absolute;
        z-index: 99999999;
        display: flex;
        width: 100%;
        height: 100vh;
        left: 0px;
        background-color: #3a3a3aab;
        color: white;
        transform: translateX(-100%);
        visibility: hidden;
        justify-content: center;
        align-content: center;
    }
    #modal-preview-template .content-shadow, .cmsbuilder-center-content #ajax-modal.template-form-design .modal-content {
        position: relative;
        width: 80%;
        height: 90vh;
        overflow-y: auto;
        overflow-x: hidden;
        margin-top: 5vh;
        background: #333333;
        box-shadow: 0px 0px 41px 3px #565656;
        border-radius: 10px;
    }

    #modal-preview-template .modal-blockcms-header .topsection {
        width: 100%;
        display: inline-flex;
        justify-content: space-between;
    }

    #modal-preview-template .modal-blockcms-header h3 {
        line-height: 40px;
    }

    .modal-blockcms-header h3 {
        margin: 0;
        padding: 0;
        text-transform: none;
        font-weight: normal;
    }

    #modal-preview-template .action-template {
        padding-top: 20px;
    }

    #modal-preview-template .preview-body {
        text-align: center;
        padding-top: 20px;
    }
    #modal-preview-template .swiperElement {
        background-color: #000000;
        margin-bottom: 30px;
    }

    #modal-preview-template .swiperElement {
        display: flex;
        width: 90%;
        margin: auto;
        justify-content: center;
        align-content: center;
    }
    #modal-preview-template .swiperElement .swiper-wrapper {
        width: 100%;
        height: 400px;
        z-index: 1;
        display: flex;
    }
    #modal-preview-template .swiperElement .swiper-template-pagination {
        bottom: -30px;
        left: 0;
        position: absolute;
        width: 100%;
        z-index: 100000;
    }
    #modal-preview-template .swiperElement .swiper-pagination-bullets .swiper-pagination-bullet {
        width: 10px;
        height: 10px;
    }

    #modal-preview-template .swiperElement .swiper-pagination-bullet-active {
        background: #f4a502;
    }
    #modal-preview-template .categoryList {
        padding: 20px;
    }
    #modal-preview-template .categoryList .badge {
        font-size: 15px;
        padding: 7px 12px;
        border-radius: 20px;
        background: #f4a502;
    }
    .modal-filter-template {
        width: 100%;
    }
    #modal-template-results .gallery-masonry-lightbox-link img.centerImgMinHeight {
        width: auto !important;
        min-width: 100%;
        max-width: inherit;
    }

    #modal-template-results .gallery-masonry-lightbox-link img {
        min-height: 175px;
        width: 100%;
        transition-timing-function: ease;
        transition-duration: 1.5s;
        /* transition-delay: 0.581152s; */
    }

    #modalFirstStepCostum .features-label {
        color:#e6344d !important;
    }

</style>
<div class="modal fade portfolio-modal" role="dialog" id="modalFirstStepCostum" style="width: 100%;">
    <div class="modal-dialog modal-lg" style="width: 100%;">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            <div>
                <img src="<?php echo $this->module->getParentAssetsUrl().'/images/logo_costum.png'?>" style="max-height:200px;"/>
            </div>
            <!-- <div class="text-left" style="width: 50%;">
                <input type="checkbox" name="">communauté <br>
                <input type="checkbox" name="">open badges <br>
                <input type="checkbox" name="">coform<br>
                <input type="checkbox" name="">un coevent<br>
                <input type="checkbox" name="">appel à communs<br>                
            </div>
 -->            <div class="modal-body center text-dark">
                <span class="no-margin"> <?php echo Yii::t("cms","You will create the costum of") ?> <b style='color:#e6344d!important;font-size:16px;padding-left: 1px;'><?php echo @$element["name"]?> </b><br/><br/>
               
                    <?php echo Yii::t("cms","What is a COstum") ?><i class="fa fa-question" style="font-size:20px;color:#e6344d;"></i>
                </span>
                <h5><?php echo Yii::t("cms", "Your personalized, contextualized website")?></h5>
                <h5>Quels sont vos objectifs pour votre site web?</h5>

            <div class="d-flex padding-20" style="width:100%;justify-content: center">
                <div class="d-flex" style="width: 70%;justify-content: space-around;">
                    <div class="d-flex c-card" style="box-shadow: rgba(50, 50, 93, 0.25) 0px 2px 5px -1px, rgba(0, 0, 0, 0.3) 0px 1px 3px -1px;justify-content: center;width:45%; flex-direction: column;background-size: cover;border-radius: 10px;">
                        <div style="background-color: white;justify-content: space-around;">                          
                            <input type="checkbox" name="community">
                            <span class="features-label"> Communauté</span>
                            <p>Rassembler et engager des personnes partageant les mêmes intérêts au sein d'une communauté.</p>  
                        </div>
                    </div>
                    <div class="d-flex c-card" style="box-shadow: rgba(50, 50, 93, 0.25) 0px 2px 5px -1px, rgba(0, 0, 0, 0.3) 0px 1px 3px -1px;justify-content: center;width:45%; flex-direction: column;background-size: cover;border-radius: 10px;">
                        <div style="background-color: white;justify-content: space-around;">   
                            <input type="checkbox" name="openBadge">
                            <span class="features-label"> Open Badges</span>
                            <p>Permettez à vos utilisateurs d'afficher des badges sur votre COSTUM pour valoriser leurs contributions et encourager l'engagement dans votre communauté.</p>                            
                        </div>  
                    </div>
                </div>
            </div>

            <div class="d-flex padding-20" style="width:100%;justify-content: center">
                <div class="d-flex" style="width: 70%;justify-content: space-around;">
                    <div class="d-flex c-card" style="box-shadow: rgba(50, 50, 93, 0.25) 0px 2px 5px -1px, rgba(0, 0, 0, 0.3) 0px 1px 3px -1px;justify-content: center;width:45%; flex-direction: column;background-size: cover;border-radius: 10px;">
                        <div style="background-color: white;justify-content: space-around;">
                            <input type="checkbox" name="coForm">
                            <span class="features-label"> Coform</span> 
                            <p>Les formulaires personnalisés.</p>
                        </div>
                    </div>
                    <div class="d-flex c-card" style="box-shadow: rgba(50, 50, 93, 0.25) 0px 2px 5px -1px, rgba(0, 0, 0, 0.3) 0px 1px 3px -1px;justify-content: center;width:45%; flex-direction: column;background-size: cover;border-radius: 10px;">
                        <div style="background-color: white;justify-content: space-around;">   
                            <input type="checkbox" name="coevent">
                            <span class="features-label"> Un coevent</span>  
                            <p>Créer des évènements collaboratifs.</p>
                        </div>  
                    </div>
                </div>
            </div>

            <div class="d-flex padding-20" style="width:100%;justify-content: center">
                <div class="d-flex" style="width: 70%;justify-content: space-around;">
                    <div class="d-flex c-card" style="box-shadow: rgba(50, 50, 93, 0.25) 0px 2px 5px -1px, rgba(0, 0, 0, 0.3) 0px 1px 3px -1px;justify-content: center;width:45%; flex-direction: column;background-size: cover;border-radius: 10px;">
                        <div style="background-color: white;justify-content: space-around;">
                            <input type="checkbox" name="aac">
                            <span class="features-label"> Appel à communs</span> 
                            <p>Contribuez à la création de ressources partagées ! Rejoignez l’appel à communs et participez à la construction d’un patrimoine collectif pour notre communauté.</p>
                        </div>
                    </div>
                    <div class="d-flex c-card" style="box-shadow: rgba(50, 50, 93, 0.25) 0px 2px 5px -1px, rgba(0, 0, 0, 0.3) 0px 1px 3px -1px;justify-content: center;width:45%; flex-direction: column;background-size: cover;border-radius: 10px;">
                        <div style="background-color: white;justify-content: space-around;">   
                            <input type="checkbox" name="cocity">
                            <span class="features-label"> COCity</span>  
                            <p>Un commun térritorial</p>
                        </div>  
                    </div>
                </div>
            </div>
          <!--   <div>
                <button type="button" class="btn btn-primary startWithFeature margin-top-10" data-dismiss="modal">Commencer</button>
            </div> -->
<!-- 
            Aucune de cette liste?

            <?php echo Yii::t("cms","A costum is all the intelligence of CO free software revisited by your magic wand") ?>,<br/><br/> -->
            <?php //echo Yii::t("cms","Are you ready") ?>
            </div>
            <div class="d-flex padding-20" style="width:100%;justify-content: center">
                <div class="d-flex" style="width: 70%;justify-content: space-around;">
                    <div class="d-flex c-card" style="box-shadow: rgba(50, 50, 93, 0.25) 0px 2px 5px -1px, rgba(0, 0, 0, 0.3) 0px 1px 3px -1px;justify-content: center;width:45%; flex-direction: column;background-size: cover;border-radius: 10px;">
                        <div style="background-color: white;justify-content: space-around;">
                            <p>Explorez maintenant notre sélection de modèles soigneusement conçus pour vous inspirer. Chaque modèle offre une base solide pour exprimer votre créativité.</p>
                            <button onclick='firstStepCostum.template.views.initAjax({space:`template`,key:`costum`});' type="button" class="btn btn-primary margin-top-10 startfirstStepCostum uppercase"><?php echo Yii::t("cms","Choose") ?></button>
                        </div>
                    </div>
                    <div class="d-flex c-card" style="box-shadow: rgba(50, 50, 93, 0.25) 0px 2px 5px -1px, rgba(0, 0, 0, 0.3) 0px 1px 3px -1px;justify-content: center;width:45%; flex-direction: column;background-size: cover;border-radius: 10px;">
                    <div style="background-color: white;justify-content: space-around;">   
                        <p>Vous êtes prêt à donner vie à votre vision sans aucune limite, optez pour la création à partir de zéro? Avec cette option, vous avez le contrôle total sur chaque détail de votre COstum.</p>
                        <button type="button" class="btn btn-primary startfirstStepCostum startCostumizer margin-top-10" data-dismiss="modal"><?php echo Yii::t("cms","IT'S ON") ?></button></div>  
                    </div>
                </div>
            </div>
            <a href="https://www.communecter.org/costum/co/index/slug/costumDesCostums#welcome" style="color:#e6344d;text-decoration: underline;" target="_blank"><?php echo Yii::t("cms","Here are some examples of existing costums") ?></a>
        </div>
    </div>
</div>
<div style="position: fixed;z-index: 9999999;top: 0;width: 100%;">
    <div id="modal-blockcms" style="width: 100%;top: 0;height: 100vh;">
        <div class="sp-cms-100 information"></div>
        <div class="modal-blockcms-header">
            <h3></h3>
            <div class="modal-filter-template"></div>
            <button id="btn-hide-modal-blockcms"><i class="fa fa-times" aria-hidden="true"></i></button>
        </div>
        <div class="modal-blockcms-body">
            <div id="category-template-filter" class="col-md-2 col-sm-2"></div>
            <div class="modal-blockcms-list col-md-10 col-sm-10">
                <div id="modal-template-results"></div>
            </div>
        </div>
    </div>
    <div id="modal-preview-template" class="modal">
        <div class="content-shadow">
            <div class="modal-blockcms-header">
                <div class="topsection">
                    <h3 class="title-template"></h3>
                    <button class="close-modal" data-target="#modal-preview-template"><i class="fa fa-times" aria-hidden="true"></i></button>
                </div>
                <div class="action-template text-center col-xs-12">
                    <a href="javascript:;" class="btn btn-default use-this-template" data-id="" data-type=""><i class="fa fa-plus"></i> <?php echo Yii::t("common", "Choose") ?></a>
                </div>

            </div>
            <div class="preview-body">

            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
if(!notNull(contextData))
    contextData = <?= json_encode($element);?>;



firstStepCostum = {
    template: { 
        category :{
            costum : {
                thirdPlace            : tradCategory.thirdPlace, 
                commons               : tradCategory.commons, 
                greenTransition       : tradCategory.greenTransition,
                aap                   : tradCategory.aap,
                territorialConnectivity : tradCategory.territorialConnectivity,
                Cocity                : tradCategory.Cocity, 
                Community             : trad.community,
                citizenAgora          : tradCategory.citizenAgora, 
                researchAndInnovation : tradCategory.researchAndInnovation,
                socialMapping         : tradCategory.socialMapping,
                media                 : "Media",
                ArtsAndMusic          : tradCategory.ArtsAndMusic,
                SportAndActivity      : tradCategory.SportAndActivity,
                coEvent               : "CoEvent",
                businessCorporation   : tradCategory.businessCorporation
            }
        },
        dataTpl:{},
        views : {
            initAjax : function(config){    
                $('#modal-blockcms').addClass('open');
                $('.information').hide();
                $("#btn-hide-modal-blockcms").show()

                var paramsTemplateList = {
                    urlData: baseUrl + "/co2/search/globalautocomplete",
                    container: ".modal-filter-template",
                    loadEvent : {
                        default : "scroll"
                    },
                    scroll : {
                        options : {
                            domTarget:"#modal-template-results",
                            scrollTarget: ".modal-blockcms-list"
                        }
                    },
                    defaults: {
                        notSourceKey: true,
                        types: ["templates"],
                        filters: {
                            type: ["costum"]
                            //shared: true
                        },
                        fields : ["userCounter","shared","blocTplId","source.key"]

                    },
                    results: {
                        dom: "#modal-template-results",
                        smartGrid: true,
                        imgObsCallback : function($this){
                            if($($this).outerHeight()<=175){
                                $($this).addClass("centerImgMinHeight");
                                $($this).css({"margin-left" : -($($this).width()/6)});
                            }
                        },
                        renderView: "firstStepCostum.template.views.templateListHtml",
                        map: {
                            active: false
                        },
                        callBack : function(obj){
                            firstStepCostum.template.events.bind();
                        }
                    },
                    filters: {
                        text: true,
                        category: {
                            view: "verticalList",
                            dom : "#category-template-filter",
                            type: "filters",
                            field: "category",
                            event: "filters",
                            keyValue:false,
                            multiple:true,
                            active : true,
                            list: firstStepCostum.template.category[config.key]
                        }
                    }
                };
                var filterGroupTemplate = searchObj.init(paramsTemplateList);
                filterGroupTemplate.search.init(filterGroupTemplate);
                
            },
            initDocument : function(tplData) {
                var screenShotItem = ""
                var data = {}
                var completedItems = 4;
                ajaxPost(
                    null,
                    baseUrl+"/"+moduleId+"/cms/getlistfile",
                    {costumType : tplData.collection, costumId : tplData.id, doctype : "image"},
                    function (screenData) {
                        completedItems -= screenData.result.length
                        screenShotItem += '<font class="">Screenshot</font><br>'
                        // screenShotItem += '<strong class="">NB: Le nombre maximum de screenshot est limité à 4</strong>'
                        screenShotItem += '<div class="biblio-image" style="border: solid 1px #BDBDBD;">'
                        for (var i = 0; i < 4; i++) {
                            mylog.log("hehe",screenData.result[i] , completedItems)
                            if (typeof screenData.result[i] != "undefined") {
                                screenShotItem += `
                                <div class="img-content">
                                <img src="${screenData.result[i].imagePath}" data-value="${screenData.result[i].imagePath}" class="image-on-bibliotheque">
                                </div>`
                            }else{
                                screenShotItem += `
                                <div class="img-content">
                                <button class="insert-screenshot btn" style="width: 100%;height: 100%;"><i class="fa fa-plus" style="background-color: inherit;font-size: 35px;color: darkgray;"></i></button>
                                </div>`
                            }
                        }
                        screenShotItem += '</div>';
                    },
                    null,
                    "json",
                    {
                        async: false
                    }                                               
                    );
                    data = {html : screenShotItem, remainLength : completedItems}
                return data;
            },
            templateListHtml: function (params) {
                imgSrc=baseUrl+assetPath+'/images/templates/default_directory.png';
                if( typeof params.screenshot != 'undefined' && params.screenshot != '') 
                    imgSrc=baseUrl + '/' + params.screenshot[0];
                else if( typeof params.profilMediumImageUrl != 'undefined' && params.profilMediumImageUrl != '')
                    imgSrc=baseUrl + '/' + params.profilMediumImageUrl;
                var str =  '<div id="entity_'+params.collection+'_'+params.id+'" class="col-md-4 col-sm-6 col-xs-12 searchEntityContainer smartgrid-slide-element start-masonry msr-'+params.id+'">'+
                                '<div class="gallery-masonry-item-wrapper preFade fadeIn" data-animation-role="image" style="overflow: hidden; transition-timing-function: ease; transition-duration: 1.5s; transition-delay: 0.57801s;">'+
                                    '<a href="javascript:;" class="gallery-masonry-lightbox-link" style="height: 100%;">'+
                                        '<img class="img-responsive lzy_img isLoading" data-id="'+params.id+'" data-src="'+imgSrc+'"/>'+
                                    '</a>'+
                                    '<div class="gallery-masonry-lightbox-link tools-kit" style="height: 100%;">'+
                                        '<div class="btnContent">'+
                                            '<a href="javascript:;" class="btn btn-prymary use-this-template margin-bottom-10" data-id="'+((params.type=="blockCms") ? params.blocTplId : params.id)+'" data-type="'+params.type+'"><i class="fa fa-plus"></i> '+tradCms.choose+'</a>'+
                                            `<a href="javascript:;" class="btn btn-prymary see-this-template margin-top-10" data-source="${typeof params.source != 'undefined' ? params.source.key:''}" data-id="${params.id}" data-type="${params.type}" data-title="${params.name}"><i class="fa fa-eye"></i> ${tradBadge.viewDetails}</a>`+
                                        '</div>'+
                                    '</div>'+
                                '</div>'+
                                '<div class="gallery-caption gallery-caption-grid-masonry">'+
                                    '<div class="gallery-caption-wrapper">'+
                                        '<p class="gallery-caption-content preFade fadeIn" style="transition-timing-function: ease; transition-duration: 1.5s; transition-delay: 0.581152s;">'+
                                            params.name;
                                            if(typeof params.shared == "undefined" || !params.shared)
                    str+=                       "<i class='pull-right fa fa-lock tooltips' ata-toggle='tooltip' data-original-title='Lock'></i>";
                    str+=                   '</p>'+
                                    '</div>'+
                                '</div>'+      
                            '</div>';
                return str;
            },
            preview : function(data){
                var str="";
                if(notNull(data.profilImageUrl) || notNull(data.medias) || notNull(data.tplAlbum)){
                    str+=`<div class="swiper swiperElement">
                            <div class="swiper-wrapper">`;
                                if(notEmpty(data.medias))
                                    $.each(data.medias, function(e, v){ 
                                        if (notEmpty(v)) {
                                            str+=`<div class="swiper-slide"><iframe width="560" height="315" src="${v.content.videoLink ? v.content.videoLink : ''}" title="YouTube video player" frameborder="0" allow="autoplay; clipboard-write; encrypted-media;" allowfullscreen></iframe></div>`;
                                        }
                                    });
                                if(notEmpty(data.profilImageUrl))
                    str+=           '<div class="swiper-slide"><img src="'+baseUrl+"/"+data.profilImageUrl+'" /></div>';
                                if(notEmpty(data.tplAlbum)){
                                    $.each(data.tplAlbum, function(e, v){
                    str+=               '<div class="swiper-slide"><img src="'+baseUrl+"/"+v.imagePath+'" /></div>';
                                    });
                                } 
                    str+=   '</div>'+
                            '<div class="swiper-template-pagination"></div>'+
                        '</div>';
                }
                if(notEmpty(data.category)){
                    str+="<div class='categoryList'>";
                    if(typeof data.category == "string")
                        str+="<span class='badge'>"+data.category+"</span>";
                    else{
                        $.each(data.category, function(e, v){
                            str+="<span class='badge'>"+v+"</span>";
                        });
                    }
                    str+="</div>";
                }
                if(notEmpty(data.description))
                    str+="<p class='description'>"+data.description+"</p>";
                $("#modal-preview-template .preview-body").html(str);
            
            }
        },
        events : {
            bind : function(){
                $('.searchEntityContainer').off().on("mouseenter", function () {
                    $(this).find(".tools-kit").show();
                }).on("mouseleave",function(){
                    $(this).find(".tools-kit").hide();  
                });
                $("#btn-hide-modal-blockcms").click(function(){                    
                    firstStepCostum.template.actions.closeModal();
                })
                $(".see-this-template").off().on("click", function(){
                    firstStepCostum.template.actions.openPreview($(this).data("id"), $(this).data("type"), $(this).data("title"),$(this).data("source"));
                });
                $(".use-this-template").off().on("click", function(){
                    var $this = $(this)
                    $this.addClass('disabled');
                    $this.html('<span style=""><i class="fa fa-spin fa-circle-o-notch"></i> <span>'+trad.processing+' ...</span></span>');
                    firstStepCostum.template.actions.use($this.data("id"), $this.data("collection"),$this.data("action"))
                });
                $("#modal-preview-template .close-modal").off().on("click", function(){
                    $("#modal-preview-template").removeClass("open");
                });
            }
        },
        actions: {
            closeModal : function(){
                $("#modal-blockcms").removeClass("open");
                if($("#modal-preview-template").is(":visible")){
                    $("#modal-preview-template").removeClass("open");   
                }
        
            },
            openPreview : function(id, type, title,source=""){
                coInterface.showLoader("#modal-preview-template .preview-body");
                $("#modal-preview-template .title-template").text(title);
                $("#modal-preview-template .action-template .btn").each(function(){
                    $(this).attr("data-id", id);
                    $(this).attr("data-type", type);
                    $(this).attr("data-source", source);
                    if($(this).hasClass("share-this-template"))
                        $(this).hide();
                });
                $("#modal-preview-template").addClass("open").modal("show");
                ajaxPost(
                    null,
                    baseUrl + "/co2/template/get/id/" + id,
                    null,
                    function(data) {
                        firstStepCostum.template.dataTpl=data;
                        firstStepCostum.template.dataTpl.id=id;
                        if(typeof data.blocTplId != "undefined")
                            $("#modal-preview-template .use-this-template").data("id", data.blocTplId);
                        if(typeof data.shared == "undefined" || !data.shared)
                            $("#modal-preview-template .share-this-template").fadeIn(200);
                        firstStepCostum.template.views.preview(data);
                        var swiper = new Swiper(".swiperElement", {
                          effect: "coverflow",
                          grabCursor: true,
                          centeredSlides: true,
                          slidesPerView: "auto",
                          autoplay: {
                            delay : 3000
                          },
                        //  translate3d(29px, 0px, 0px) rotateX(0deg) rotateY(-54.0012deg) scale(1.3)
                          coverflowEffect: {
                            rotate: 70,
                            stretch: 0,
                            depth: 100,
                            scale: 1.4, 
                            modifier: 1,
                            slideShadows: true,
                          },
                          pagination: {
                            el: ".swiper-template-pagination",
                            clickable : true
                          },
                        });
                        firstStepCostum.template.events.bind();
                    },
                    null,
                    "json"
                );
            },
            use : function (tplId, tplCollection, action, callback) {
                tplCtx={};
                tplCtx.id = contextData.id;
                tplCtx.collection = contextData.collection;
                tplCtx.path ="allToRoot";
                tplCtx.value = {
                    costum: {
                        slug : "costumize", 
                        htmlConstruct : { 
                            header : {
                                menuTop :{
                                    activated: true, 
                                    left : { 
                                        buttonList: {logo : true, app : {label : true}}
                                    }
                                }
                            }
                        },
                        language : userConnected.language,
                        css : {
                            menuTop : {
                                left : {
                                    app : {
                                        buttonList : {
                                            fontWeight : "800",
                                            textTransform : "capitalize",
                                            fontSize : "17px",
                                            paddingLeft : "10px"
                                        },
                                        paddingBottom : "5px",
                                        paddingLeft : "5px",
                                        paddingRight : "5px",
                                        paddingTop : "5px"
                                    }
                                }
                            }
                        }
                    }
                };
                dataHelper.path2Value(tplCtx,function(response){
                    $("#modalFirstStepCostum").modal('hide');
                    if(response.result){                        
                        var params = {
                            "id"         : tplId,
                            "action"     : action,
                            "newCostum"  : true,
                            "collection" : tplCollection,
                            "parentId"   : contextData.id,
                            "page"       : "#",
                            "parentSlug" : contextData.slug,
                            "parentType" : contextData.collection
                        };
                        ajaxPost(
                            null,
                            baseUrl + "/co2/template/use",
                            params,
                            function (data) {
                               window.location.href = baseUrl+'/costum/co/index/slug/'+contextData.slug+'/edit/true';
                            },
                            { async: false }
                            );
                    }
                })
            }
            
        }

    }
}
</script>