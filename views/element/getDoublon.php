<?php
$cssAnsScriptFilesModule = array(
    '/plugins/toastr/toastr.js',
    '/plugins/toastr/toastr.min.css',
    '/js/api.js',
    "/js/default/loginRegister.js",
);
HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule);
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Communecter,merge doublon</title>
    <link href="https://code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" rel="stylesheet">
    <link href="<?= $assetsPath ?>/css/element/treeSortable.css" rel="stylesheet" type="text/css" />
    <link href="<?= $assetsPath ?>/css/element/jdd.css" rel="stylesheet" type="text/css" />
    <link href="<?= $assetsPath ?>/css/element/reset.css" rel="stylesheet" type="text/css" />
    <link href="<?= $assetsPath ?>/css/element/throbber.css" rel="stylesheet" type="text/css" />
    <link href="<?= $baseUrl ?>/plugins/toastr/toastr.min.css" rel="stylesheet" type="text/css" />



    <link rel="stylesheet" href="https://mdbcdn.b-cdn.net/wp-content/themes/mdbootstrap4/docs-app/css/dist/mdb5/standard/core.min.css">


    <link rel='stylesheet' id='roboto-subset.css-css' href='https://mdbcdn.b-cdn.net/wp-content/themes/mdbootstrap4/docs-app/css/mdb5/fonts/roboto-subset.css?ver=3.9.0-update.5' type='text/css' media='all' />
    <style>
        .tree-branch>.contents .branch-wrapper {
            max-width: 100%;
        }

        .tree-branch>.contents .branch-wrapper .left-sidebar {
            width: 100%;
            max-width: 100%;
        }

        .border-green {
            border: 1px green solid;
        }

        body {
            font-size: 15px;
        }

        .thumbnail {
            box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.5);
            transition: 0.3s;
            min-width: 40%;
            border-radius: 5px;
        }

        .thumbnail-description {
            min-height: 40px;
        }

        #tree>.tree-branch>.contents>.branch-wrapper:hover {
            cursor: pointer;
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 1);
        }

        element.style {
            width: 300px;
            right: 0px;
            display: block;
        }

        #right-sub-panel-container.active {
            overflow: visible;
        }

        #right-sub-panel-container {
            width: 0;
            height: calc(100vh - 40px);
            position: absolute;
            top: 40px;
            background-color: #313131;
            z-index: 1000000;
            color: white;
            transition: width .2s;
            overflow: hidden;
        }

        .selected-to-fusion {
            border: 1px green solid !important;
            font-size: 15px;
        }

        .leftToPrio {
            background-color: rgba(0, 200, 0, 0.4) !important;
        }

        #titleElementLeft,
        #titleElementRight {
            font-size: 14px;
        }
    </style>
</head>

<body>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1 style="text-align: center;">Liste des doublons</h1>
                <div class="row">
                    <div class="col-md-12">

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="thumbnail">
                                    <div class="caption text-center">
                                        <h4 id="thumbnail-label">Filtrer les résultats :</h4>
                                    </div>
                                    <div class="caption card-footer text-center">
                                        <?php
                                        if (isset($last) && isset($last["name"]) && isset($last["slug"])) { ?>
                                            <p>Dernier modification : <a href="<?= $baseUrl ?>/#@<?= $last["slug"] ?>" target="_blank"><?= $last["name"] ?></a></p>
                                        <?php } ?>

                                        <form class="form-inline">
                                            <!-- <div class="form-group">
                                                <label for="filterType">Type d'element : </label>
                                                <select class="form-control" id="filterType">
                                                    <?php /// foreach ($allElementType as $typeElement) { 
                                                    ?>
                                                        <option <?php // if ($type == $typeElement) { 
                                                                ?> selected="selected" <?php // } 
                                                                                        ?> value="<?php //echo $typeElement 
                                                                                                                                            ?>"><?php //echo $typeElement 
                                                                                                                                                        ?></option>
                                                    <?php //} 
                                                    ?>
                                                </select>
                                            </div> -->
                                            <div class="form-group" style="margin-left:15px">
                                                <label for="filterName"> Nom de l'element : </label>
                                                <input type="text" class="form-control" id="filterName" placeholder="nom de l'element">
                                            </div>
                                            <div class="form-group" style="margin-left:15px">
                                                <label for="filterSource"> Keys Source : </label>
                                                <input type="text" class="form-control" id="filterSource" placeholder="ex : ctenat">
                                            </div>
                                            <div class="form-group" style="margin-left:15px">
                                                <a class="btn btn-primary" role="button" id="btnSearchDoublons"><i class="fa fa-search "> Filtrer</i></a>
                                                <!-- <button type="button" class="btn btn-outline-info" id="btnSearchDoublons"><i class="fa fa-search "> Search</i></button> -->
                                            </div>

                                        </form>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">&nbsp;</div>
                    </div>
                </div>
                <ul id="tree" style="width:100%;"></ul>
            </div>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.1.0.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="<?= $assetsPath ?>/js/treeSortable.js"></script>
    <script src="<?= $assetsPath ?>/js/jsl.format.js"></script>
    <script src="<?= $assetsPath ?>/js/jsl.interaction.js"></script>
    <script src="<?= $assetsPath ?>/js/jsl.parser.js"></script>
    <script src="<?= $assetsPath ?>/js/jdd.js"></script>
    <script src="<?= $assetsPath ?>/js/sweetalert.min.js"></script>
    <script src="<?= $baseUrl ?>/plugins/toastr/toastr.js"></script>
    <script src="<?= $baseUrl ?>/js/api.js"></script>
    <script src="<?= $assetsPath ?>/js/default/loginRegister.js"></script>
    <script src="<?= $assetsPath ?>/js/bootstrap-notify.min.js"></script>

    <script type="text/javascript" src="https://mdbcdn.b-cdn.net/wp-content/themes/mdbootstrap4/docs-app/js/dist/search/search.min.js"></script>
    <?php $outpout = "<script> const data = [";
    $i = 1;
    $doublonsToJsonInJs = "<script> var allDoublons = {};  ";

    foreach ($doublons as $type => $data) {
        $outpout .= "{id:" . $i . ", level:1,parent_id:0,title:`" . $type . "`,count:" . count($data['result']) . "},";
        $i++;
        if (count($data['result']) > 0) {
            $doublonsToJsonInJs .= "allDoublons['" . $type . "'] = {active : 0, max : " . count($data['result']) . "};";
        }
    }

    $doublonsToJsonInJs .=  "</script>";

    $outpout .= "]; var sortable = new TreeSortable({
                treeSelector: '#tree'
            });";

    $outpout .= "const content = data.map(sortable.createBranch);
            $('#tree').html(content);
            sortable.run();</script>";

    echo $outpout;
    echo $doublonsToJsonInJs;
    ?>
    <script>
        var all = {};
        var allToFusion;
        var elementDrag;
        var elementLeftTrueValue = [];
        var elementRightTrueValue = [];
        var idActif = "";
        var intoActif = "";
        var selectedItem = {};

        //Login.openLogin();
        //onClickElementInList();
        //toastr.error("teste");
        eventClickSortableListe(sortable);

        function formateDate(numberLong) {
            const arr = numberLong.split("-");
            var month = "";
            if (arr[1] == 01 || arr[1] == 1) month = "Janvier";
            else if (arr[1] == 02 || arr[1] == 2) month = "Février";
            else if (arr[1] == 03 || arr[1] == 3) month = "Mars";
            else if (arr[1] == 04 || arr[1] == 4) month = "Avril";
            else if (arr[1] == 05 || arr[1] == 5) month = "Mei";
            else if (arr[1] == 06 || arr[1] == 6) month = "Juin";
            else if (arr[1] == 07 || arr[1] == 7) month = "Juillet";
            else if (arr[1] == 08 || arr[1] == 8) month = "Août";
            else if (arr[1] == 09 || arr[1] == 9) month = "Séptembre";
            else if (arr[1] == 10) month = "Octobre";
            else if (arr[1] == 11) month = "Novembre";
            else if (arr[1] == 12) month = "Décembre";

            return arr[2] + " " + month + " " + arr[0];
        }


        function eventClickSortableListe(sortableInstance) {
            sortableInstance.addListener('click', '.branch-wrapper', function(event, instance) {
                event.preventDefault();


                var parent = $(event.target).parent().parent().parent();
                parent = (typeof parent.data("id") != "undefined") ? parent : $(event.target).parent().parent().parent().parent();
                parent = (typeof parent.data("id") != "undefined") ? parent : $(event.target).parent().parent();


                if (parent.data("parent") == 0 && parent.data("level") == 1) {

                    if (typeof $("li[data-parent=" + parent.data("id") + "]").data("id") != "undefined") {
                        const type = $("li[data-id=" + parent.data("id") + "] .left-sidebar .branch-title").html();
                        allDoublons[type]["active"] = 0;

                        forEach($("li[data-parent=" + parent.data("id") + "]"), function(li) {
                            instance.removeBranch($(li));
                        });

                    } else {
                        const type = $("li[data-id=" + parent.data("id") + "] .left-sidebar .branch-title").html();
                        //addChildInList(type, event, instance);
                        const name = $("#filterName").val();

                        var url = "<?php echo $baseUrl ?>/co2/element/getdoublon/type/" + type;

                        params = {
                            type : type,
                        };
                        if (name != "")
                            params["name"] = name;
                        params = cleanEmptyObjectInParams(params);
                        $.ajax({
                            type: "POST",
                            url: url,
                            data : params,
                            type: "GET",
                            url: url,
                            success: function(data) {

                                if (typeof data["doublons"] != "undefined" && typeof data["doublons"]["result"] != "undefined") {
                                    allDoublons[type] = data["doublons"]["result"];
                                    allDoublons[type]["active"] = 0;
                                    allDoublons[type]["max"] = data["doublons"]["result"].length;
                                    addChildInList(type, event, instance);
                                }
                            },
                            error: function(error) {
                                notification(error, "danger");
                            }
                        });

                    }
                } else if (parent.data("parent") != 0 && parent.data("level") == 2) {
                    if (typeof $("li[data-parent=" + parent.data("id") + "]").data("id") != "undefined") {
                        forEach($("li[data-parent=" + parent.data("id") + "]"), function(li) {
                            instance.removeBranch($(li));
                        });

                    } else {
                        if (parent.data("id") == "about") {
                            const type = parent.data("type");
                            addChildInList(type, event, instance, parent);
                        } else {
                            const type = $("li[data-id=" + parent.data("parent") + "] .left-sidebar .branch-title").html();
                            const nameCorrectly = $("li[data-id=" + parent.data("id") + "] .left-sidebar .branch-title").html();
                            var name = nameCorrectly;
                            const source = $("#filterSource").val();
                            var url = "<?= $baseUrl ?>/co2/element/getdoublon";

                            var params = {
                                info: name,
                                type: type,
                            }

                            if (source != "")
                                params["source"] = source;

                            var ajaxOptions = {
                                type: "POST",
                                url: url,
                                success: function(data) {
                                    $.each(data, function(id, element) {
                                        all[id] = element;
                                        var name = element.name;
                                        name += (typeof element.dateUpdate != "undefined") ? " ( Modifié le " + formateDate(element.dateUpdate) + " )" : " ( " + id + " )";

                                        instance.addChildBranch($(event.target), name, id, 0, type);

                                        $("li[data-parent=" + parent.data("id") + "]").addClass("dropzone");
                                        $("li[data-parent=" + parent.data("id") + "] .contents").addClass("dropzone");
                                        $("li[data-parent=" + parent.data("id") + "] .contents .branch-wrapper").addClass("dropzone");
                                        drapAndDropEvent(parent);
                                    });
                                    notification("Veuillez sélectionner l'élément à merger et puis la destinataire");
                                },
                                error: function(xhr, ajaxOptions, thrownError) {
                                    notification(error, "danger");
                                },
                            }
                            params = cleanEmptyObjectInParams(params);
                            ajaxOptions.data = params;
                            $.ajax(ajaxOptions);
                        }
                    }
                } else if (parent.data("parent") != 0 && parent.data("level") == 3) {
                    if ($("li[data-id=" + parent.data("id") + "] .contents").hasClass("selected-to-fusion")) {
                        $("li[data-id=" + parent.data("id") + "] .contents").removeClass("selected-to-fusion");

                    } else {
                        var grand_parent = $("li[data-id=" + parent.data("parent") + "]");

                        const type = $("li[data-id=" + grand_parent.data("parent") + "] .left-sidebar .branch-title").html();
                        $("li[data-id=" + parent.data("id") + "] .contents").addClass("selected-to-fusion");

                        const elementSelected = $("li[data-parent=" + parent.data("parent") + "]");
                        var i = 0;
                        var idToFusion = [];

                        $.each(elementSelected, function(key, elementSelectedOnce) {
                            if (i < 3) {
                                if ($("li[data-id=" + $(elementSelectedOnce).data("id") + "] .contents").hasClass("selected-to-fusion")) {
                                    idToFusion[i] = $(elementSelectedOnce).data("id");
                                    i++;
                                }
                            }

                        });
                        if (i == 1 && typeof selectedItem["first"] == "undefined") {
                            selectedItem["first"] = idToFusion[0];
                            notification("Veuillez sélectionner un autre élément comme destinataire");
                        }

                        if (i == 2) {
                            for (var indexOfidToFusion = 0; indexOfidToFusion < idToFusion.length; indexOfidToFusion++) {
                                if (idToFusion[indexOfidToFusion] != selectedItem["first"]) {
                                    selectedItem["second"] = idToFusion[indexOfidToFusion];
                                }
                            }
                            const id = selectedItem["first"];
                            const into = selectedItem["second"];

                            $.each(elementSelected, function(key, elementSelectedOnce) {
                                if (i < 3) {
                                    if ($("li[data-id=" + $(elementSelectedOnce).data("id") + "] .contents").hasClass("selected-to-fusion")) {
                                        $("li[data-id=" + $(elementSelectedOnce).data("id") + "] .contents").removeClass("selected-to-fusion")
                                    }
                                }

                            });

                            const url = "<?= $baseUrl ?>/co2/element/mergedatadoublon/element/" + parent.data("type") + "/id/" + id + "/into/" + into;
                            selectedItem = {};
                            window.open(url, "_blank" || window.location.replace(url));
                        }
                    }

                }
                //
                //onClickElementInList(event, instance)
            });
        }

        $(".close-modal-modalFusion").click(function() {
            $('#modalFusion').modal("toggle");
        });

        $("#btnSearchDoublons").click(function() {
            filterChange();
        });

        function cleanEmptyObjectInParams(params) {
            for (const [key, value] of Object.entries(params)) {
                if (value === null || (typeof value == "object" && Object.keys(value).length == 0)) {
                    params[key] = "";
                }
            }
            return params;
        }

        function addChildInList(type, event, instance, parent = null) {
            if (typeof allDoublons[type] != "undefined") {

                const maxCountInList = 9;
                var i = 0;
                var addAt = $(event.target);

                if (parent != null) {

                    addAt = $("li[data-id=" + parent.data("parent") + "] .left-sidebar");
                    forEach($("li[data-parent=" + parent.data("parent") + "][data-id=" + parent.data("id") + "]"), function(li) {
                        instance.removeBranch($(li));
                    });
                }

                $.each(allDoublons[type], function(id, element) {
                    if (i <= maxCountInList && allDoublons[type]["active"] <= (allDoublons[type]["max"] - 1)) {
                        if (id >= allDoublons[type]["active"]) {
                            instance.addChildBranch(addAt, element.name, "", element.count, type);
                            allDoublons[type]["active"]++;
                            i++;
                        }
                    }

                    /* if(i == maxCountInList || (allDoublons[type]["active"] >= (allDoublons[type]["max"] - 1)))
                        break; */
                });
                i++;
                if (i > maxCountInList && allDoublons[type]["active"] <= allDoublons[type]["max"]) {
                    instance.addChildBranch(addAt, "Voir plus", "about", 0, type);
                }
                //notification("Veuillez sélectionner l'élément à merger et puis la destinataire");
            }
        }

        function drapAndDropEvent(parent) {
            const dropzone = document.querySelectorAll(".dropzone");
            dropzone.forEach((dropzone) => {
                dropzone.addEventListener("dragstart", (e) => {
                    var parent = $(e.target).parent().parent().parent();
                    parent = (typeof parent.data("id") != "undefined") ? parent : $(event.target).parent().parent().parent().parent();
                    parent = (typeof parent.data("id") != "undefined") ? parent : $(event.target).parent().parent();
                    elementDrag = parent.data("id");
                    $("li[data-id=" + parent.data("id") + "] .contents").css({
                        "border": "3px green solid"
                    });
                });

                dropzone.addEventListener("dragover", (e) => {
                    e.preventDefault();
                });

                dropzone.addEventListener("dragend", (e) => {
                    var parent = $(e.target).parent().parent().parent();
                    parent = (typeof parent.data("id") != "undefined") ? parent : $(event.target).parent().parent().parent().parent();
                    parent = (typeof parent.data("id") != "undefined") ? parent : $(event.target).parent().parent();

                    $("li[data-id=" + parent.data("id") + "] .contents").css({
                        "border": "none"
                    });
                });

                dropzone.addEventListener("dragenter", (e) => {
                    e.preventDefault();

                    var parent = $(e.target).parent().parent().parent();
                    parent = (typeof parent.data("id") != "undefined") ? parent : $(event.target).parent().parent().parent().parent();
                    parent = (typeof parent.data("id") != "undefined") ? parent : $(event.target).parent().parent();

                    $("li[data-id=" + parent.data("id") + "] .contents").css({
                        "border": "3px green solid"
                    });
                });

                dropzone.addEventListener("dragleave", (e) => {
                    e.preventDefault();

                    var parent = $(e.target).parent().parent().parent();
                    parent = (typeof parent.data("id") != "undefined") ? parent : $(event.target).parent().parent().parent().parent();
                    parent = (typeof parent.data("id") != "undefined") ? parent : $(event.target).parent().parent();

                    if (elementDrag != parent.data("id"))
                        $("li[data-id=" + parent.data("id") + "] .contents").css({
                            "border": "none"
                        });
                });

                dropzone.addEventListener("drop", (e) => {
                    e.preventDefault();
                    const data = e.dataTransfer.getData("text/html");
                    var into = "";
                    var id = "";
                    var nameLeft = "";

                    var parentInto = $(event.target).parent().parent().parent();
                    parentInto = (typeof parentInto.data("id") != "undefined") ? parentInto : $(event.target).parent().parent().parent().parent();
                    parentInto = (typeof parentInto.data("id") != "undefined") ? parentInto : $(event.target).parent().parent();

                    const elementDrap = $("li[data-parent=" + parent.data("id") + "]");
                    $.each(elementDrap, function(key, elementDrapOnce) {
                        var name = $("li[data-id=" + $(elementDrapOnce).data("id") + "] .left-sidebar .branch-title").html();

                        if (name == $(data).html()) {
                            if (typeof $(elementDrapOnce).data("id") != "undefined")
                                id = $(elementDrapOnce).data("id");
                            nameLeft = name;
                        }
                        if (name == $(e.target).html() || name == $("li[data-id=" + parentInto.data("id") + "] .left-sidebar .branch-title").html()) {
                            if (typeof $(elementDrapOnce).data("id") != "undefined")
                                into = $(elementDrapOnce).data("id");
                        }

                    });

                    if (into != "" && id != "") {
                        const url = "<?= $baseUrl ?>/co2/element/mergedatadoublon/element/" + parent.data("type") + "/id/" + id + "/into/" + into;

                        window.open(url, "_blank" || window.location.replace(url));
                    }

                });
            });
        }

        function getKeyOrValueInJsonSelected(selected) {
            var element = "";
            element = selected.split(": ")[0];

            element = element.split(",")[0];

            if (typeof element.split('"')[1] != "undefined") {
                element = element.split('"')[1];
            } else {
                var deleteSpace = ""
                for (i = 0; i < element.length; i++) {
                    if (element[i] != " ")
                        deleteSpace += element[i];
                }
                element = Number(deleteSpace);
            }
            return element;
        }

        function removeElementInArray(arr, val) {
            var result = [];
            const index = arr.indexOf(val);
            for (i = 0; i < arr.length; i++) {
                if (index != i) {
                    result.push(arr[i]);
                }

            }
            return result;
        }

        function eventClickJson() {
            $(".leftElementContainer .code").click(function() {
                const selected = $(this).html();
                var element = getKeyOrValueInJsonSelected(selected);

                if ($(this).hasClass("leftToPrio")) {
                    $(this).removeClass("leftToPrio");
                    elementLeftTrueValue = removeElementInArray(elementLeftTrueValue, element);
                } else {
                    $(this).addClass("leftToPrio");
                    elementLeftTrueValue.push(element);
                }
                var left = all[idActif];
                var right = JSON.parse(JSON.stringify(all[intoActif]));
                const resultArray = compareArray(left, right);

                $("#textareaResultat").val(JSON.stringify(resultArray, null, 4));
                refreshResultatFusion();

            });

            $(".rightElementContainer .code").click(function() {
                const selected = $(this).html();
                var element = getKeyOrValueInJsonSelected(selected);

                if ($(this).hasClass("leftToPrio")) {
                    $(this).removeClass("leftToPrio");
                    elementRightTrueValue = removeElementInArray(elementRightTrueValue, element);
                } else {
                    $(this).addClass("leftToPrio");
                    elementRightTrueValue.push(element);
                }
                var left = all[idActif];
                var right = JSON.parse(JSON.stringify(all[intoActif]));
                const resultArray = compareArray(left, right);

                $("#textareaResultat").val(JSON.stringify(resultArray, null, 4));
                refreshResultatFusion();

            });
        }

        function refreshResultatFusion() {
            var resultatFusion = JSON.parse($('#textareaResultat').val());

            var config3 = jdd.createConfig();
            jdd.formatAndDecorate(config3, resultatFusion);
            $(".resultatFusion").text(config3.out);

            formatPRETagsResultat();
        }

        function formatPRETagsResultat() {
            forEach($(".resultContainer pre"), function(pre) {
                var codeBlock = $('<pre class="codeBlock"></pre>');
                var lineNumbers = $('<div class="gutter"></div>');
                codeBlock.append(lineNumbers);

                var codeLines = $('<div></div>');
                codeBlock.append(codeLines);

                var addLine = function(line, index) {
                    var div = $('<div class="codeLine line' + (index + 1) + '"></div>');
                    lineNumbers.append($('<span class="line-number">' + (index + 1) + '.</span>'));
                    var span = $('<span class="code"></span');
                    span.text(line);
                    div.append(span);

                    codeLines.append(div);
                };



                var lines = $(pre).text().split('\n');


                lines.forEach(addLine);

                codeBlock.addClass($(pre).attr('class'));
                codeBlock.attr('id', $(pre).attr('id'));

                $(pre).replaceWith(codeBlock);
            });
        }

        function filterChange() {
            const name = $("#filterName").val();
            const source = $("#filterSource").val();
            var url = "<?= $baseUrl ?>/co2/element/getdoublon";

            params = {};

            if (name != "")
                params["name"] = name;
            if (source != "")
                params["source"] = source;
            filterList(url, params);
        }

        /* function rectifyUrl(nameCorrectly) {
            var name = "";
            for (var i = 0; i < nameCorrectly.length; i++) {
                if (nameCorrectly[i] == " ") name += "___";
                else if (nameCorrectly[i] == '"') name += "àà";
                else if (nameCorrectly[i] == "'") name += "ùù";
                else if (nameCorrectly[i] == "(") name += "èè";
                else if (nameCorrectly[i] == ")") name += "éé";
                else if (nameCorrectly[i] == ":") name += ",,,";
                else if (nameCorrectly[i] == "°") name += "---";
                else if (nameCorrectly[i] == "’") name += "çç";
                else if (nameCorrectly[i] == "–") name += "êêê";
                else name += nameCorrectly[i];
                name.replace("&amp;", "üü");
            }
            return name;
        } */

        function filterList(url, params) {
            params = cleanEmptyObjectInParams(params);
            $.ajax({
                type: "POST",
                url: url,
                data : params,
                success: function(data) {
                    refreshList(data);
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    notification(error, "danger");
                },
            });
        }

        function refreshList(response) {
            allDoublons = {};
            var i = 1;
            var data = [];
            $.each(response, function(index, value) {
                if (typeof value["result"] != "undefined" && value["result"].length > 0) {

                    data[i - 1] = {
                        id: i,
                        level: 1,
                        parent_id: 0,
                        title: index,
                        count: value["result"].length
                    };
                    i++;
                    allDoublons[index] = value["result"];
                    allDoublons[index]["active"] = 0;
                    allDoublons[index]["max"] = value["result"].length;
                }
            });

            if (data.length === 0)
                notification("Aucun élément trouvé!");


            sortable = new TreeSortable({
                treeSelector: '#tree'
            });

            const content = data.map(sortable.createBranch);
            $('#tree').html(content);
            sortable.run();

        }

        function setElementInModalId(keyLeft, keyRight, nameLeft = "", nameRight = "") {
            $("#titleElementLeft").html(nameLeft);
            $("#titleElementRight").html(nameRight);
            idActif = keyLeft;
            intoActif = keyRight;


            var output = `<h2 style="width:100%; text-align:center;">Fusion</h2>
                <p style="width:100%; text-align:center;">L'element de gauche va être fusionner avec l'element de droit</p>
                <div class="row" style="margin-left:5px;"><div class="col-md-4">
                <select class="form-control" id="select-element-to-merge" >
                    <option value="` + keyLeft + `" selected="selected">` + nameLeft + `</option>
                    </select>
                    <a style="margin-top:10px;margin-bottom:25px;" class="btn btn-primary btnModifyName"  role="button" data-id="` + keyLeft + `" > Modifier le nom de l'élément</a> 
                </div>
                    <div class="col-md-2" style="text-align:center;"><a id="btnInverser"><i class="fa fa-exchange" aria-hidden="true">Inverser</i></a></div>
                    <div class="col-md-4">
                        <select class="form-control" id="select-element-merge-into" >
                            <option value="` + keyRight + `" selected="selected">` + nameRight + `</option>
                        </select>
                        <a style="margin-top:10px;margin-bottom:25px;" class="btn btn-primary btnModifyName"  role="button" data-id="` + keyRight + `" > Modifier le nom de l'élément</a> 
                    </div>
                </div>
                <div class="row" id="infomodalmerge" style="margin-left:5px;"></div>`;

            return output;
        }

        function eventChangeSelect(left, right) {
            $("#textarealeft").val(JSON.stringify(left, null, 4));
            $("#textarearight").val(JSON.stringify(right, null, 4));
            const resultat = compareArray(left, right);
            $("#textareaResultat").val(JSON.stringify(resultat, null, 4));

            $("#compare").trigger("click");
            showInformationModal(all);
            eventClickJson();
        }

        function compareArray(merge, into) {

            $.each(merge, function(key, data) {

                if (elementLeftTrueValue.includes(key)) {
                    if (Array.isArray(data) && elementLeftTrueValue.includes(key) && elementRightTrueValue.includes(key)) {
                        if (typeof into[key] != "undefined") {
                            into[key] = copyAllValueTwoArray(data, into[key]);
                        } else
                            into[key] = copyAllValueTwoArray(data, []);
                    } else
                        into[key] = data;
                } else {
                    if ((typeof into == "object" && into.hasOwnProperty(key)) || (Array.isArray(into) && typeof into[key] != "undefined")) {
                        if (typeof data != "object" && !Array.isArray(data)) {

                            if (elementLeftTrueValue.includes(data)) {
                                into[key] = data;
                            } else {
                                if (data != into[key]) {
                                    if (into[key] == "undefined")
                                        into[key] = data;
                                }
                            }
                        } else {
                            if (Array.isArray(data) && key == "tags") {
                                if (typeof into[key] != "undefined") {
                                    into[key] = copyAllValueTwoArray(data, into[key]);
                                } else
                                    into[key] = copyAllValueTwoArray(data, []);
                            } else
                                into[key] = compareArray(data, into[key])
                        }
                    } else {
                        into[key] = data;
                    }
                }
            });
            return into;
        }

        function copyAllValueTwoArray(array1, array2) {
            var tmp = [];
            for (i = 0; i < array2.length; i++) {
                if (typeof array2[i] != "undefined") {
                    if (!tmp.includes(array2[i]))
                        tmp.push(array2[i]);
                }
            }

            for (i = 0; i < array1.length; i++) {
                if (typeof array1[i] != "undefined") {
                    if (!tmp.includes(array1[i]))
                        tmp.push(array1[i]);
                }
            }

            return tmp;
        }

        function notification(message, type = "info") {
            $.notify({
                message: message
            }, {
                type: type,
                placement: {
                    from: "bottom",
                    align: "right"
                }
            });
        }

        function showInformationModal(allElement) {
            $("#infomodalmerge").html("");

            var right = allElement[$("#select-element-merge-into").val()];
            var id = $("#select-element-to-merge").val();
            left = {};

            if (typeof right == "object" && typeof right["costum"] != "undefined")
                $("#infomodalmerge").append(contentInformationInModal("L'élement de droite possède une costum"));

            if (typeof right == "object" && typeof right["oceco"] != "undefined")
                $("#infomodalmerge").append(contentInformationInModal("L'élement de droite possède un oceco"));

            if (id == "all") {
                left = allElement;
                var nombreCostum = 0;
                var nombreOceco = 0;

                $.each(left, function(key, value) {
                    if (typeof value == "object" && typeof value["costum"] != "undefined")
                        nombreCostum++;
                    if (typeof value == "object" && typeof value["oceco"] != "undefined")
                        nombreOceco++;
                });

                if (nombreCostum > 0)
                    $("#infomodalmerge").append(contentInformationInModal("L'élement de gauche possède " + nombreCostum + " costum"));
                if (nombreOceco > 0)
                    $("#infomodalmerge").append(contentInformationInModal("L'élement de gauche possède " + nombreOceco + " oceco"));


            } else {

                left = allElement[$("#select-element-to-merge").val()];

                var numberCostum = 0;
                if (typeof left == "object" && typeof left["costum"] != "undefined")
                    $("#infomodalmerge").append(contentInformationInModal("L'élement de gauche possède une costum"));

                if (typeof left == "object" && typeof left["oceco"] != "undefined")
                    $("#infomodalmerge").append(contentInformationInModal("L'élement de gauche possède une costum"));
            }

            compareAdresse(left, right);
            //infomodalmerge
        }

        function compareAdresse(left, right) {
            var id = $("#select-element-to-merge").val();
            if (id != "all") {


                if (typeof left == "object" && typeof left["address"] != "undefined" && typeof left["address"]["addressLocality"] != "undefined" && typeof left["address"]["streetAddress"] != "undefined") {
                    if (typeof right == "object" && typeof right["address"] != "undefined" && typeof right["address"]["addressLocality"] != "undefined" && typeof right["address"]["streetAddress"] != "undefined") {
                        if (right["address"]["addressLocality"] == left["address"]["addressLocality"] && right["address"]["streetAddress"] == left["address"]["streetAddress"])
                            $("#infomodalmerge").append(contentInformationInModal("Les 2 élements ont la même adresse"));
                        else
                            $("#infomodalmerge").append(contentInformationInModal("Les 2 élements ont une adresse différent"));
                    } else
                        $("#infomodalmerge").append(contentInformationInModal("L'élement de gauche possède une adresse et du droite non"));
                } else {
                    if (typeof right == "object" && typeof right["address"] != "undefined" && typeof right["address"]["addressLocality"] != "undefined" && typeof right["address"]["streetAddress"] != "undefined")
                        $("#infomodalmerge").append(contentInformationInModal("L'élement de droite possède une adresse et du gauche non"));
                    else
                        $("#infomodalmerge").append(contentInformationInModal("Les 2 élements n'ont pas d'adresse"));
                }
            } else {
                var nombreAdresseExiste = 0;
                var adresseDif = 0
                $.each(left, function(key, value) {
                    if (typeof value == "object" && typeof value["address"] != "undefined" && typeof value["address"]["addressLocality"] != "undefined" && typeof value["address"]["streetAddress"] != "undefined") {
                        nombreAdresseExiste++;
                        if (typeof right == "object" && typeof right["address"] != "undefined" && typeof right["address"]["addressLocality"] != "undefined" && typeof right["address"]["streetAddress"] != "undefined") {
                            if (right["address"]["addressLocality"] != value["address"]["addressLocality"] || right["address"]["streetAddress"] != value["address"]["streetAddress"])
                                adresseDif++;
                        } else
                            adresseDif++;
                    }
                });
                if (nombreAdresseExiste > 0) {
                    if (adresseDif > 0) {
                        $("#infomodalmerge").append(contentInformationInModal("Les élements de gauche ont " + adresseDif + " adresse différent de l'élemment de droite"));
                    } else
                        $("#infomodalmerge").append(contentInformationInModal("Les élements de gauche ont la même adresse que celui de droite"));
                } else
                    $("#infomodalmerge").append(contentInformationInModal("Les élements de gauche n'ont pas d'adresse"));
            }
        }

        function contentInformationInModal(message) {
            return '<div class="col-md-3"><p><input id="showEq" type="checkbox" name="checkbox" value="value" checked="true">' + message + '</p></div>';
        }
    </script>


</body>

</html>