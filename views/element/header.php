<section class="col-md-12 col-sm-12 col-xs-12" id="social-header">
	
	<?php 
		echo $this->renderPartial('co2.views.element.banner',
					array(	"element"=>$element,
							"badgesToShow" => $badgesToShow??[],
							"pageConfig"=>$pageConfig,
							"linksBtn"=>$linksBtn,
							"invitedMe"=>@$invitedMe,
							"edit" => @$edit,
							"openEdition" => @$openEdition) 
					); 
	?>
</section>