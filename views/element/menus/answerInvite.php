<?php
if (isset($invitedMe) && !empty($invitedMe)) {
	$inviteRefuse = "Refuse";
	$inviteAccept = "Accept";
	$verb = "Join";
	$labelAdmin = "";
	$option = null;
	$linkValid = Link::IS_INVITING;
	$msgRefuse = Yii::t("common", "Are you sure to refuse this invitation");
	if (@$invitedMe["isAdminInviting"]) {
		$verb = "Administrate";
		$option = "isAdminInviting";
		$labelAdmin = " to administrate";
		$linkValid = Link::IS_ADMIN_INVITING;
		$msgRefuse = Yii::t("common", "Are you sure to refuse to administrate {what}", array("{what}" => Yii::t("common", " this " . Element::getControlerByCollection($element["collection"]))));
	}
	if ($element["collection"] == "citoyens") {
		$labelAdmin = " to become friend";
	}
	$elementId = isset($element["id"]) ? $element["id"] : (string)$element["_id"];
	$labelInvitation = Yii::t("common", "{who} invited you" . $labelAdmin, array("{who}" => "<a href='#page.type." . Person::COLLECTION . ".id." . $invitedMe["invitorId"] . "' class='lbh'>" . $invitedMe["invitorName"] . "</a>"));
	$tooltipAccept = $verb . " this " . Element::getControlerByCollection($element["collection"]);
	if ($element["collection"] == Event::COLLECTION) {
		$inviteRefuse = "Not interested";
		$inviteAccept = "I go";
	}
	if (isset($element['fromActivityPub'])) {
		echo "<div class='no-padding containInvitation'>" .
			"<div class='padding-5'>" .
			$labelInvitation . ": <br/>" .
			'<a class="btn btn-xs tooltips btn-accept" href="javascript:links.acceptInvitationActivityPub(\'' . $element["collection"] . '\',\'' . $elementId . '\',\'' . (string)$element["objectId"] . '\', \'' . Yii::app()->session["userId"] . '\',\'' . Person::COLLECTION . '\',\'' . $linkValid . '\')" data-placement="bottom" data-original-title="' . Yii::t("common", $tooltipAccept) . '">' .
			'<i class="fa fa-check "></i> ' . Yii::t("common", $inviteAccept) .
			'</a>' .
			'<a class="btn btn-xs tooltips btn-refuse margin-left-5" href="javascript:links.refusedInvitationActivityPub(\'' . $element["collection"] . '\',\'' . $elementId . '\',\'' . (string)$element["objectId"] . '\',\'' . Yii::app()->session["userId"] . '\',\'' . Person::COLLECTION . '\',\'' . Element::$connectTypes[$element["collection"]] . '\',null,\'' . $option . '\',\'' . $msgRefuse . '\')" data-placement="bottom" data-original-title="' . Yii::t("common", "Not interested by the invitation") . '">' .
			'<i class="fa fa-remove"></i> ' . Yii::t("common", $inviteRefuse) .
			'</a>' .
			"</div>" .
			"</div>";
	} else {
		echo "<div class='no-padding containInvitation'>" .
			"<div class='padding-5'>" .
			$labelInvitation . ": <br/>" .
			'<a class="btn btn-xs tooltips btn-accept" href="javascript:links.validate(\'' . $element["collection"] . '\',\'' . $elementId . '\', \'' . Yii::app()->session["userId"] . '\',\'' . Person::COLLECTION . '\',\'' . $linkValid . '\')" data-placement="bottom" data-original-title="">' .
			'<i class="fa fa-check "></i> ' . Yii::t("common", $inviteAccept) .
			'</a>' .
			'<a class="btn btn-xs tooltips btn-refuse margin-left-5" href="javascript:links.disconnect(\'' . $element["collection"] . '\',\'' . $elementId . '\',\'' . Yii::app()->session["userId"] . '\',\'' . Person::COLLECTION . '\',\'' . Element::$connectTypes[$element["collection"]] . '\',null,\'' . $option . '\',\'' . $msgRefuse . '\')" data-placement="bottom" data-original-title="' . Yii::t("common", "Not interested by the invitation") . '">' .
			'<i class="fa fa-remove"></i> ' . Yii::t("common", $inviteRefuse) .
			'</a>' .
			"</div>" .
			"</div>";
	}
}
