<?php

use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\utils\Utils;

	$idMenu=(isset($params["id"])) ? $params["id"] : $key;
	$classMenu=(isset($params["class"])) ? $params["class"] : "";
	$classMenu.=(isset($params["addClass"])) ? " ".$params["addClass"] : "";
	$classMenu.=" cosDyn-".$idMenu;
	$keyMenu=(isset($params["key"])) ? $params["key"] : $key; 
	$commonButtonClass= (isset($params["buttonClass"])) ? $params["buttonClass"] : "";
	
	$isFromFediverse = Utils::isFediverseShareLocal($element);
//menu-top-btn-group
?>

<div id="<?php echo $idMenu ?>" class="<?php echo $classMenu ?>">
<?php 

	if(!isset($params["buttonList"])){

		foreach($params as $key => $v){
			if(isset($v["menu"]) && $v["menu"]) {
				echo $this->renderPartial('co2.views.element.menus.construct',array(
		            "params"=>$v,
		            "sub"=>true ,
		            "key"=>$keyMenu,
		            "edit"=>$edit,
		            "element"=>$element,
		            "linksBtn"=>$linksBtn,
					"invitedMe"=>$invitedMe,		
		            "openEdition"=>$openEdition              
	    		));
	    	}
		}
	}else{

		$menuButtonHtml="";
        foreach($params["buttonList"] as $k => $v){
			
            $show=true;
            //ETAT en fonction des droit de l'element
           if(isset($v["openEdition"]) && empty($openEdition)) $show=false;
    		if(isset($v["restricted"])){
	    		if(isset($v["restricted"]["edit"]) && empty($edit)) $show=false;
	    		if(isset($v["restricted"]["members"])) $show=Authorisation::isElementMember((string)$element["_id"],$element["collection"], Yii::app()->session["userId"]);
	    		if(isset($v["restricted"]["admins"])) $show=Authorisation::isElementAdmin((string)$element["_id"],$element["collection"], Yii::app()->session["userId"]);
	    		if(isset($v["restricted"]["costumAdmins"])) $show=Authorisation::isCostumAdmin();
	    		if(isset($v["restricted"]["types"]) && !in_array($element["collection"], $v["restricted"]["types"])) $show=false;
    			if(isset($v["restricted"]["category"]) && (!isset($element["category"]) || !in_array($element["category"],$v["restricted"]["category"]))) $show=false; 
				if(!empty($value["restricted"]["disconnected"]) && !empty(Yii::app()->session["userId"])) $show=false; 
			}
    		//ETAT pour une user CONNEcté
    		if(isset($v["connected"]) && !isset(Yii::app()->session["userId"])) $show=false;
    		
    		//HTML FOR BUTTONS
    		if($show){ 
                if(isset($v["construct"]))
                    $menuButtonHtml .= Menu::{$v["construct"]}($v, $commonButtonClass, $k, $element);
                else if(isset($v["render"])){
                	echo "<div id='".@$v["id"]."' class='".@$v["class"]."'>";
                	 echo $this->renderPartial($v["render"],
						array(
							"element"=>$element,
							"edit" => $edit,
							"linksBtn"=>$linksBtn,
							"invitedMe"=>$invitedMe,
							"openEdition" => $openEdition) 
					);
                	 echo "</div>";
                }
               	else{
					if($isFromFediverse){
						if(in_array($k,['newspaper','detail','community'])){
							$menuButtonHtml.= Menu::elementButtonHtml($v, $commonButtonClass, $k);
						}
					}else{
							$menuButtonHtml.= Menu::elementButtonHtml($v, $commonButtonClass, $k);
						
					}
					
                   
               	}

            }
        }
        echo $menuButtonHtml;
    
	}
?>
</div>
<script type="text/javascript">
	
</script>