<?php
$visibleXsLinks = "";
if (@$linksBtn["followBtn"]) {
	if (@$linksBtn["isFollowing"]) {
		$statusXsMenu = Yii::t("common", "You are following {which}", array("{which}" => Yii::t("common", "this " . Element::getControlerByCollection($element["collection"]))));
		if (isset($element["fromActivityPub"]) && $element["fromActivityPub"] == 1) {
			$visibleXsLinks .= '<li class="text-left visible-xs">' .
				'<a href="javascript:links.disconnect(\'' . $element["collection"] . '\',\'' . (string)$element["_id"] . '\',\'' . Yii::app()->session["userId"] . '\',\'' . Person::COLLECTION . '\',\'followers\')" class="bg-white text-red">' .
				'<i class="fa fa-sign-out"></i> ' . Yii::t("common", "Don't follow this page") .
				'</a>' .
				'</li>';
		} else {
			$visibleXsLinks .= '<li class="text-left visible-xs">' .
				'<a href="javascript:links.unfollowActivityPub(\'' . $element["collection"] . '\',\'' . (string)$element["_id"] . '\',\'' . @$element["objectId"] . '\',\'unfollow_project\')" class="bg-white text-red">' .
				'<i class="fa fa-sign-out"></i> ' . Yii::t("common", "Don't follow this page") .
				'</a>' .
				'</li>';
		}
?>
		<?php if (!@$xsView) { ?>
			<ul class="nav navbar-nav">
				<li class="dropdown">
					<?php if (isset($element["fromActivityPub"]) && $element["fromActivityPub"] == 1) { ?>
						<a href="javascript:;" class="btn-o menu-btn-follow menu-linksBtn" data-toggle="dropdown">
							<i class="fa fa-rss"></i> <?php echo Yii::t("common", "Following") ?> <i class="fa fa-caret-down"></i>
						</a>
						<ul class="dropdown-menu">
							<li class="text-left">
								<a href="javascript:links.unfollowActivityPub('<?php echo @$element["collection"] ?>','<?php echo @$element["_id"] ?>','<?php echo @$element["objectId"] ?>','unfollow_project')" class="bg-white text-red">
									<i class="fa fa-sign-out"></i><?php echo Yii::t("common", "Don't follow this page"); ?>
								</a>
							</li>
						</ul>
					<?php } else { ?>
						<a href="javascript:;" class="btn-o menu-btn-follow menu-linksBtn" data-toggle="dropdown">
							<i class="fa fa-rss"></i> <?php echo Yii::t("common", "Following") ?> <i class="fa fa-caret-down"></i>
						</a>
						<ul class="dropdown-menu">
							<li class="text-left">
								<a href="javascript:links.unfollowActivityPub('<?php echo @$element["collection"] ?>','<?php echo @$element["_id"] ?>','<?php echo @$element["objectId"] ?>','unfollow_project')" class="bg-white text-red">
									<i class="fa fa-sign-out"></i><?php echo Yii::t("common", "Don't follow this page"); ?>
								</a>
							</li>
						</ul>
					<?php } ?>

				</li>
			</ul>
		<?php } ?>
	<?php
	} else {
		if (isset($element["fromActivityPub"]) && $element["fromActivityPub"] == 1) {
			$visibleXsLinks .= '<li class="text-left visible-xs">' .
				'<a  href="javascript:links.followActivityPub(\'' . $element["collection"] . '\',\'' . (string)$element["_id"] . '\',\'' . @$element["objectId"] . '\',\'' . 'follow_project' . '\')" class="bg-white menu-btn-follow">' .
				'<i class="fa fa-rss"></i> ' . Yii::t("common", "Follow this page") .
				'</a>' .
				'</li>';
		} else {
			$visibleXsLinks .= '<li class="text-left visible-xs">' .
				'<a  href="javascript:links.follow(\'' . $element["collection"] . '\',\'' . (string)$element["_id"] . '\',\'' . Yii::app()->session["userId"] . '\',\'' . Person::COLLECTION . '\')" class="bg-white menu-btn-follow">' .
				'<i class="fa fa-rss"></i> ' . Yii::t("common", "Follow this page") .
				'</a>' .
				'</li>';
		}
	?>
		<?php if (!@$xsView) { ?>
			<?php if (isset($element["fromActivityPub"]) && $element["fromActivityPub"] == 1) { ?>
				<a href="javascript:links.followActivityPub('<?php echo @$element["collection"] ?>','<?php echo @$element["_id"] ?>','<?php echo @$element["objectId"] ?>','follow_project')" class="btn-o menu-linksBtn menu-btn-follow"> <i class="fa fa-rss"></i> <?php echo Yii::t("common", "Follow") ?> </a>
			<?php } else { ?>
				<a href="javascript:links.follow('<?php echo $element["collection"] ?>','<?php echo (string)$element["_id"] ?>','<?php echo Yii::app()->session["userId"] ?>','<?php echo Person::COLLECTION ?>')" class="btn-o menu-linksBtn menu-btn-follow"> <i class="fa fa-rss"></i> <?php echo Yii::t("common", "Follow") ?> </a>
			<?php } ?>
		<?php } ?>
		<?php
	}
}

if (@$linksBtn["communityBn"]) {
	if ($linksBtn["isMember"] == false) {
		if (@Yii::app()->session["userId"] && @Yii::app()->session["userId"] != (string)$element["_id"]) {
			if (isset($element["fromActivityPub"]) && $element["fromActivityPub"] == 1) {
				$visibleXsLinks .= '<li class="text-left visible-xs">' .
					'<a href="javascript:links.connectActivityPub(\'' . $element["collection"] . '\',\'' . (string)$element["_id"] . '\',\'' . (string)$element["objectId"] . '\',\'' . Yii::app()->session["userId"] . '\',\'' . Person::COLLECTION . '\',\'' . $linksBtn["connectAs"]  . '\')" class="bg-white">' .
					'<i class="fa fa-link"></i> ' . Yii::t("common", "Be {what}", array("{what}" => Yii::t("common", $linksBtn["connectAs"]))) .
					'</a>' .
					'</li>';
			} else {
				$visibleXsLinks .= '<li class="text-left visible-xs">' .
					'<a href="javascript:links.connect(\'' . $element["collection"] . '\',\'' . (string)$element["_id"] . '\',\'' . Yii::app()->session["userId"] . '\',\'' . Person::COLLECTION . '\',\'' . $linksBtn["connectAs"] . '\')" class="bg-white">' .
					'<i class="fa fa-link"></i> ' . Yii::t("common", "Be {what}", array("{what}" => Yii::t("common", $linksBtn["connectAs"]))) .
					'</a>' .
					'</li>';
			}
		?>
			<?php if (!@$xsView) { ?>
				<?php if (isset($element["fromActivityPub"]) && $element["fromActivityPub"] == 1) { ?>
					<a href="javascript:links.connectActivityPub('<?php echo $element["collection"] ?>','<?php echo (string)$element["_id"] ?>','<?php echo (string)$element["objectId"] ?>','<?php echo Yii::app()->session["userId"] ?>','<?php echo Person::COLLECTION ?>','<?php echo $linksBtn["connectAs"] ?>')" class="btn-o menu-linksBtn">
						<i class="fa fa-link"></i> <?php echo Yii::t("common", "Be {what}", array("{what}" => Yii::t("common", $linksBtn["connectAs"]))); ?>
					</a>
				<?php } else { ?>
					<a href="javascript:links.connect('<?php echo $element["collection"] ?>','<?php echo (string)$element["_id"] ?>','<?php echo Yii::app()->session["userId"] ?>','<?php echo Person::COLLECTION ?>','<?php echo $linksBtn["connectAs"] ?>')" class="btn-o menu-linksBtn">
						<i class="fa fa-link"></i> <?php echo Yii::t("common", "Be {what}", array("{what}" => Yii::t("common", $linksBtn["connectAs"]))); ?>
					</a>
				<?php } ?>
		<?php }
		}; ?>
	<?php
	} else if (@$linksBtn[Link::IS_INVITING]) {
		$statusXsMenu = Yii::t("common", "Your are inviting to join {what}", array("{what}" => Yii::t("common", "the " . Element::getControlerByCollection($element["collection"]))));
	?>
		<?php if (!@$xsView) { ?>
			<a href="javascript:;" class="btn-o menu-linksBtn">
				<i class="fa fa-send"></i> <?php echo Yii::t("common", "Inviting") . "..."; ?>
			</a>
		<?php } ?>
	<?php } else {
		$labelBtn = Yii::t("common", "Already {what}", array("{what}" => Yii::t("common", $linksBtn["connectAs"])));
		$statusXsMenu = Yii::t("common", "You are {what} of {which}", array("{what}" => Yii::t("common", $linksBtn["connectAs"]), "{which}" => Yii::t("common", "this " . Element::getControlerByCollection($element["collection"]))));
		if (@$linksBtn[Link::TO_BE_VALIDATED]) {
			$labelBtn = Yii::t("common", "Waiting");
			$indicateStatus = Yii::t("common", "Waiting an answer to become {what}", array("{what}" => Yii::t("common", $linksBtn["connectAs"])));
			if (@$linksBtn[Link::IS_ADMIN_PENDING])
				$indicateStatus = Yii::t("common", "Waiting an answer to become administrator");
			$statusXsMenu = $indicateStatus;
		} else if (@$linksBtn["isAdmin"] && $linksBtn["isAdmin"] && !@$linksBtn[Link::IS_ADMIN_PENDING]) {
			$labelBtn = Yii::t("common", "Already admin");
			$statusXsMenu = Yii::t("common", "You are {what} of {which}", array("{what}" => Yii::t("common", "admin"), "{which}" => Yii::t("common", "this " . Element::getControlerByCollection($element["collection"]))));
		}
		if (@$linksBtn[Link::IS_ADMIN_PENDING]) {
			$indicateStatus = Yii::t("common", "Waiting an answer to administrate");
			$statusXsMenu = $indicateStatus;
		}
	?>
		<ul class="nav navbar-nav <?php if (@$xsView) echo "hidden"; ?>">
			<li class="dropdown">
				<a href="javascript:;" class="btn-o menu-btn-link menu-linksBtn" data-toggle="dropdown">
					<i class="fa fa-link"></i> <?php echo $labelBtn; ?> <i class="fa fa-caret-down"></i>
				</a>
				<ul class="dropdown-menu">
					<?php if (@$indicateStatus) { ?>
						<li class="text-left noHover padding-10">
							<i><?php echo $indicateStatus; ?></i>
						</li>
					<?php } ?>
					<?php if (!@$linksBtn["isAdmin"]) {
						if (isset($element["fromActivityPub"]) && $element["fromActivityPub"] == 1) {
							$visibleXsLinks .= '<li class="text-left visible-xs">' .
								'<a href="javascript:links.becomeAdministratorActivityPub(\'' . $element["collection"] . '\',\'' . (string)$element["_id"] . '\',\'' .  (string)$element["objectId"] . '\',\'' . Person::COLLECTION . '\',\'admin\')" class="bg-white">' .
								'<i class="fa fa-user-plus"></i> ' . Yii::t("common", "Become administrator") .
								'</a>' .
								'</li>';
						} else {
							$visibleXsLinks .= '<li class="text-left visible-xs">' .
								'<a href="javascript:links.connect(\'' . $element["collection"] . '\',\'' . (string)$element["_id"] . '\',\'' . Yii::app()->session["userId"] . '\',\'' . Person::COLLECTION . '\',\'admin\')" class="bg-white">' .
								'<i class="fa fa-user-plus"></i> ' . Yii::t("common", "Become administrator") .
								'</a>' .
								'</li>';
						}
					?>
						<?php if (!@$xsView && $linksBtn["connectType"] != "friends") { ?>
							<?php if (isset($element["fromActivityPub"]) && $element["fromActivityPub"] == 1) { ?>
								<li class="text-left">
									<a href="javascript:links.becomeAdministratorActivityPub('<?php echo $element["collection"] ?>','<?php echo (string)$element["_id"] ?>','<?php echo (string)$element["objectId"] ?>','<?php echo Person::COLLECTION ?>','admin')" class="bg-white">
										<i class="fa fa-user-plus"></i> <?php echo Yii::t("common", "Become administrator"); ?>
									</a>
								</li>
							<?php } else { ?>
								<li class="text-left">
									<a href="javascript:links.connect('<?php echo $element["collection"] ?>','<?php echo (string)$element["_id"] ?>','<?php echo Yii::app()->session["userId"] ?>','<?php echo Person::COLLECTION ?>','admin')" class="bg-white">
										<i class="fa fa-user-plus"></i> <?php echo Yii::t("common", "Become administrator"); ?>
									</a>
								</li>

							<?php } ?>
						<?php } ?>
					<?php }

					if (isset($element["fromActivityPub"]) && $element["fromActivityPub"] == 1) {
						$visibleXsLinks .= '<li class="text-left visible-xs">' .
							'<a href="javascript:links.quitPageActivityPub(\'' . $element["collection"] . '\',\'' . (string)$element["_id"] . '\',\'' . (string)$element["objectId"] . '\')" class="bg-white text-red">' .
							'<i class="fa fa-sign-out"></i> ' . Yii::t("common", "Leave this page") .
							'</a>' .
							'</li>';
					} else {
						$visibleXsLinks .= '<li class="text-left visible-xs">' .
							'<a href="javascript:links.disconnect(\'' . $element["collection"] . '\',\'' . (string)$element["_id"] . '\',\'' . Yii::app()->session["userId"] . '\',\'' . Person::COLLECTION . '\',\'' . $linksBtn["connectType"] . '\')" class="bg-white text-red">' .
							'<i class="fa fa-sign-out"></i> ' . Yii::t("common", "Leave this page") .
							'</a>' .
							'</li>';
					}
					?>
					<?php if (!@$xsView) {
						if (isset($options) && isset($options["invite"]) && !empty($options["invite"])) {
							echo $this->renderPartial(
								'co2.views.element.menus.inviteBtn',
								array(
									"contextType"      => $element["collection"],
									"contextId"   => (string)$element["_id"],
									"class" => "text-dark",
									"tooltip" => false,
									"separator" => false
								)
							);
						} ?>
						<li class="text-left">
							<?php if (isset($element["fromActivityPub"]) && $element["fromActivityPub"] == 1) { ?>
								<a href="javascript:links.quitPageActivityPub('<?php echo $element["collection"] ?>','<?php echo (string)$element["_id"] ?>','<?php echo (string)$element["objectId"] ?>','<?php echo (string)$element["_id"] ?>')" class="bg-white text-red">
									<?php if ($linksBtn["connectType"] == "friends") { ?>
										<i class="fa fa-remove"></i> <?php echo Yii::t("common", "Remove from the list of friends"); ?>
									<?php } else { ?>
										<i class="fa fa-sign-out"></i> <?php echo Yii::t("common", "Leave this page"); ?>
									<?php } ?>
								</a>
							<?php } else { ?>
								<a href="javascript:links.disconnect('<?php echo $element["collection"] ?>','<?php echo (string)$element["_id"] ?>','<?php echo Yii::app()->session["userId"] ?>','<?php echo Person::COLLECTION ?>','<?php echo $linksBtn["connectType"] ?>')" class="bg-white text-red">
									<?php if ($linksBtn["connectType"] == "friends") { ?>
										<i class="fa fa-remove"></i> <?php echo Yii::t("common", "Remove from the list of friends"); ?>
									<?php } else { ?>
										<i class="fa fa-sign-out"></i> <?php echo Yii::t("common", "Leave this page"); ?>
									<?php } ?>
								</a>
							<?php } ?>
						</li>
					<?php } ?>
				</ul>
			</li>
		</ul>
<?php
	}
}
?>

<?php if ($element["collection"] != Person::COLLECTION && (string)$element["_id"] != Yii::app()->session["userId"]) { ?>
	<a href="javascript:collection.add2fav('<?php echo $element["collection"] ?>','<?php echo (string)$element["_id"] ?>')" class="btn-o menu-linksBtn no-border-right btn-favorite-link <?php if (@$xsView) echo "hidden"; ?> star_<?php echo $element["collection"] . '_' . (string)$element["_id"]; ?>"><i class="fa fa-star-o"></i> <?php echo Yii::t("common", "Favorites"); ?></a>
<?php } ?>
<!-- View in menu params // visible only on xs -->
<?php if (@$xsView) { ?>
	<li role="separator" class="divider visible-xs"></li>
	<?php if (@$statusXsMenu) { ?>
		<li class="text-left noHover visible-xs">
			<span style="font-size: 10px; font-style: italic; padding:3px 20px;"><?php echo $statusXsMenu; ?></span>
		</li>
	<?php } ?>
	<?php echo $visibleXsLinks; ?>
	<li role="separator" class="divider visible-xs"></li>
<?php } ?>
<!-- End of xs generated -->
<script type="text/javascript">
	var elementType = '<?php echo $element["collection"] ?>';
	var elementId = '<?php echo (string)$element["_id"] ?>';
	jQuery(document).ready(function() {
		$('ul.nav li.dropdown').hover(function() {
			$(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
		}, function() {
			$(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
		});
		if (typeof userConnected != "undefined" && userConnected != null &&
			typeof userConnected.collections != "undefined" &&
			typeof userConnected.collections.favorites != "undefined" &&
			typeof userConnected.collections.favorites[elementType] != "undefined" &&
			typeof userConnected.collections.favorites[elementType][elementId] != "undefined" &&
			$(".star_" + elementType + "_" + elementId).length) {
			$(".star_" + elementType + "_" + elementId).addClass("text-yellow");
			$(".star_" + elementType + "_" + elementId).children("i").removeClass("fa-star-o").addClass("fa-star");
		}
	});
</script>