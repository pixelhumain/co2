<style type="text/css">
.visible-xs .link-circle {
    font-size: 25px;
    border-radius: 0px;
    width: auto;
    height: auto;
    position: inherit;
    line-height: 0px;
    background-color: none!important;
}

</style>

<!-- DESCRIPTION MENU TOP HORIZONTAL -->
<!--
<div id=menuTopDesc class="col-md-offset-4 col-sm-offset-4 col-lg-offset-3 col-md-8 col-sm-8 col-lg-9 no-padding menuTopDesc hidden-xs" style="display: none">
	<div class="col-md-12 col-sm-12 col-lg-12 text-left padding-10 hidden-xs shortDescTop text-dark-blue">
		<?php echo @$element["shortDescription"]; ?>
			
	</div>

	<?php if(in_array($type, [Event::COLLECTION, Organization::COLLECTION, Project::COLLECTION])){ 
			if(@$element['parent'] || @$element['organizer'] ){ ?>
				<div class="">
					<?php if($type==Event::COLLECTION || $type==Project::COLLECTION || $type==Organization::COLLECTION){ ?>
						<div class="event-infos-header" style="text-align: left;"></div>
					<?php } ?>
					
			    </div>
			<?php }
	 		} ?>
		
</div>
-->

<!-- END DESCRIPTION MENU TOP HORIZONTAL -->

<button type="button" class="btn btn-default bold menu-left-min visible-xs pull-left" onclick="pageProfil.menuLeftShow();">
		<i class="fa fa-bars"></i>
</button>
 <?php
 	foreach($buttonTop as $key => $v){
 		if($key=="imgProfil"){ ?>		
 			<img class="<?php echo $v["class"] ?>" src="<?php echo $thumbAuthor; ?>" height=45>
 		<?php }
 		else if($key=="nameProfil"){ ?>
 			<div class="identity-min">
	    	  <div class="pastille-type-element bg-<?php echo $iconColor; ?> pull-left"></div>
			  <div class="<?php echo $v["class"] ?> pull-left no-padding">
	    	  	<div class="text-left padding-left-15" id="second-name-element">
					<span id="nameHeader">
						<h5 class="elipsis"><?php echo @$element["name"]; ?></h5>
					</span>	
				</div>
	    	  </div>
    	  </div>
 		<?php } 
 		else if($key=="params" && isset($v["dropdown"])){ 
 			$label=(isset(Yii::app()->session["userId"]) && $edit==true && isset($v["label"])) ? $v["label"] : "";
 			$icon= (isset(Yii::app()->session["userId"]) && $edit==true && isset($v["icon"])) ? $v["icon"] : "chevron-down"; ?>
 			<ul class="nav navbar-nav <?php echo @$v["class"]; ?>" id="paramsMenu">
				<li class="dropdown dropdown-profil-menu-params">
					<button type="button" class="btn btn-default bold <?php  echo @$v["btnClass"] ?>">
						<?php if(@Yii::app()->session["userId"] && $edit==true){ ?>
			  			<span class="<?php  echo @$v["iconClass"] ?>"><i class="fa fa-<?php echo $icon ?>"></i></span> 
			  			<span class="<?php echo @$v["labelClass"]; ?>"><?php echo Yii::t("common", $label); ?>
			  			<?php }else{ ?>
			  			<i class="fa fa-chevron-down"></i>
			  			<?php } ?>
			  			</span>
			  		</button>
			  		<ul class="dropdown-menu arrow_box menu-params">
	                	<?php foreach($v["dropdown"] as $k => $li){
	                		if(!empty($li)){ 
		                		$dataAction=(isset($li["action"])) ? "data-action='".$li["action"]."'" : "";
		                		$dataView=(isset($li["view"])) ? "data-view='".$li["view"]."'" : "";
		                		$show=true;
		                		if(isset($li["edit"]) && empty($edit)) $show=false;
		                		if(isset($li["typeAllow"]) && !in_array($type, $li["typeAllow"])) $show=false;
		                		if($show){ ?>
			                		<li class="text-left">
										<a href="javascript:;" class="bg-white ssmla <?php echo @$li["class"] ?>" <?php echo $dataAction ?> <?php echo $dataView ?>>
											<i class="fa fa-<?php echo $li["icon"] ?>"></i> <?php echo Yii::t("common", $li["label"]); ?>
											</a>
									</li>
	                	<?php 	}
	                		} 
	                	} ?>
	                </ul>
			  	</li>
			 </ul>
 		<?php }
 		else if(!empty($v)){
 			$dataAttr="";
 			$dataAttr.=(@$v["action"]) ? "data-action='".$v["action"]."' " : "";
    		$dataAttr.=(@$v["view"]) ? "data-view='".$v["view"]."' " : "";
    		if(isset($v["dataAttr"])){
    			$dataAttr.=(isset($v["dataAttr"]["dir"])) ? "data-dir='".$v["dataAttr"]["dir"]."' " : "";
    			$dataAttr.=(isset($v["dataAttr"]["toggle"])) ? "data-toggle='".$v["dataAttr"]["toggle"]."' " : "";
    			$dataAttr.=(isset($v["dataAttr"]["target"])) ? "data-target='".$v["dataAttr"]["target"]."' ":"";
    		}
    		$show=true;
    		if(isset($v["openEdition"]) && empty($openEdition)) $show=false;
    		if(isset($v["edit"]) && empty($edit)) $show=false;
    		if(isset($v["onlyMember"])) $show=Authorisation::isElementMember((string)$element["_id"],$type, Yii::app()->session["userId"]);
    		if(isset($v["typeAllow"]) && !in_array($type, $v["typeAllow"])) $show=false;
    		if(isset($v["userConnected"]) && !isset(Yii::app()->session["userId"])) $show=false;
    		if(isset($v["categoryAllow"]) && (!isset($element["category"]) || $element["category"]!=$v["categoryAllow"])) $show=false; 
    		if($show){ ?>
				<a href="javascript:;" class="bg-white ssmla btn btn-default btn-menu-tooltips <?php echo @$v["class"] ?>" <?php echo @$dataAttr ?> data-toggle="tooltip" data-placement="bottom" title="">
					<span class="<?php  echo @$v["iconClass"] ?>" >
						<i class="fa fa-<?php echo $v["icon"] ?>"></i>
					</span>
					<span class="tooltips-top-menu-btn tooltips-visivility" style="display: none;left: auto!important;visibility: hidden;">
						<?php echo Yii::t("common", @$v["label"]);?>
					</span>
					<span class="<?php echo @$v["labelClass"] ?>"><?php echo Yii::t("common", @$v["label"]); ?></span>
				</a>
 		<?php }
 		}
 	}
 	if(isset($openMenuXs) && !empty($openMenuXs)){ ?>
 		<ul class="menu-xs-only-top">
 		<?php foreach($buttonTop as $key => $v){
 			if(isset($v["class"]) && strpos($v["class"], "hidden-xs") !== false && !empty($v) && $v!==true){
	 			$dataAttr="";
	 			$dataAttr.=(@$v["action"]) ? "data-action='".$v["action"]."' " : "";
	    		$dataAttr.=(@$v["view"]) ? "data-view='".$v["view"]."' " : "";
	    		if(isset($v["dataAttr"])){
	    			$dataAttr.=(isset($v["dataAttr"]["dir"])) ? "data-dir='".$v["dataAttr"]["dir"]."' " : "";
	    			$dataAttr.=(isset($v["dataAttr"]["toggle"])) ? "data-toggle='".$v["dataAttr"]["toggle"]."' " : "";
	    			$dataAttr.=(isset($v["dataAttr"]["target"])) ? "data-target='".$v["dataAttr"]["target"]."' ":"";
	    		}
	    		$show=true;
	    		if(isset($v["openEdition"]) && empty($openEdition)) $show=false;
	    		if(isset($v["edit"]) && empty($edit)) $show=false;
	    		if(isset($v["onlyMember"])) $show=Authorisation::isElementMember((string)$element["_id"],$type, Yii::app()->session["userId"]);
	    		if(isset($v["typeAllow"]) && !in_array($type, $v["typeAllow"])) $show=false;
	    		if(isset($v["userConnected"]) && !isset(Yii::app()->session["userId"])) $show=false;
	    		if(isset($v["categoryAllow"]) && (!isset($element["category"]) || $element["category"]!=$v["categoryAllow"])) $show=false; 
	    		if($show){ ?>
	    			<li>
						<a href="javascript:;" class="bg-white ssmla btn btn-default pull-left" <?php echo @$dataAttr ?>>
							<i class="fa fa-<?php echo @$v["icon"] ?>"></i> <span class="<?php @$v["labelClass"] ?>"><?php echo Yii::t("common", @$v["label"]); ?></span>
						</a>
					</li>
				<li>
					<a href="<?php echo Yii::app()->createUrl('/costum/co/index/slug/'.$element["slug"]); ?>
							">
					<i class="fa  fa-heart-o"></i> <?php echo Yii::t("common","Customize") ?>
							</a>
						</li>
		
 		<?php 	}
 			} ?>
 		
 <?php } ?>
 	</ul>
 <?php	}
?>

