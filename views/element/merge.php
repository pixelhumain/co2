<?php
$cssAnsScriptFilesModule = array(
    '/plugins/toastr/toastr.js',
    '/plugins/toastr/toastr.min.css',
    '/js/api.js',
    "/js/default/loginRegister.js",
);
HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule);
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Communecter,merge doublon</title>
    <link href="https://code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" rel="stylesheet">
    <link href="<?= $assetsPath ?>/css/element/treeSortable.css" rel="stylesheet" type="text/css" />
    <link href="<?= $assetsPath ?>/css/element/jdd.css" rel="stylesheet" type="text/css" />
    <link href="<?= $assetsPath ?>/css/element/reset.css" rel="stylesheet" type="text/css" />
    <link href="<?= $assetsPath ?>/css/element/throbber.css" rel="stylesheet" type="text/css" />
    <link href="<?= $baseUrl ?>/plugins/toastr/toastr.min.css" rel="stylesheet" type="text/css" />



    <link rel="stylesheet" href="https://mdbcdn.b-cdn.net/wp-content/themes/mdbootstrap4/docs-app/css/dist/mdb5/standard/core.min.css">


    <link rel='stylesheet' id='roboto-subset.css-css' href='https://mdbcdn.b-cdn.net/wp-content/themes/mdbootstrap4/docs-app/css/mdb5/fonts/roboto-subset.css?ver=3.9.0-update.5' type='text/css' media='all' />
    <style>

        .border-green {
            border: 1px green solid;
        }

        body {
            font-size: 15px;
        }

        .thumbnail {
            box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.5);
            transition: 0.3s;
            min-width: 40%;
            border-radius: 5px;
        }

        .thumbnail-description {
            min-height: 40px;
        }
        .code {
            cursor: pointer;
        }

        element.style {
            width: 300px;
            right: 0px;
            display: block;
        }

        #right-sub-panel-container.active {
            overflow: visible;
        }

        #right-sub-panel-container {
            width: 0;
            height: calc(100vh - 40px);
            position: absolute;
            top: 40px;
            background-color: #313131;
            z-index: 1000000;
            color: white;
            transition: width .2s;
            overflow: hidden;
        }
        .selected-to-fusion {
            border : 1px green solid !important;
            font-size: 15px;
        }
        .leftToPrio{
            background-color : rgba(0, 200, 0, 0.4)!important;
        }

        .resultToDelete{
            color:red!important;
            text-decoration : line-through;
        }

        #titleElementLeft, #titleElementRight {
            font-size: 15px;
            margin-left: 60px;
        }

        #titleElementLeft {
            color : red;
        }
        
        #titleElementRight {
            color : green;
        }

        .leftElementContainer pre {
            border: 1px red solid;
        }

        .rightElementContainer pre {
            border: 1px green solid;
        }

        .resultContainer pre {
            border: 1px blue solid;
        }

        .find {
            color : green!important;
        }
    </style>
</head>

<body>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="modal top" id="modalFusion" tabindex="-1" aria-labelledby="modalFusionLabel" aria-hidden="true" data-mdb-backdrop="true" data-mdb-keyboard="true">
                    <div class="modal-dialog modal-fullscreen">
                        <div class="modal-content">
                            
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-12" style="margin-left:270px;">
                                        <div class="row">
                                            <div class="col-sm-8">
                                                <div class="thumbnail">
                                                    <div class="caption text-center">
                                                        <h2 style="width:100%; text-align:center;">Fusion des doublons</h2>
                                                        <p style="width:100%; text-align:center;">L'élement de gauche va être fusionner avec l'élement de droite</p>
                                                    
                                                        <form class="form-inline">
                                                            <div class="form-group" style="margin-left:15px">
                                                                <input type="text" class="form-control" id="rechercheInput" placeholder="ex : Tags">
                                                            </div>
                                                            <div class="form-group" style="margin-left:15px">
                                                                <a href="#" class="btn btn-primary" role="button" id="btnFiltrerKey"><i class="fa fa-search "> Filtrer</i></a>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-2">&nbsp;</div>
                                    </div>
                                </div>
                                <div id="main">

                                    <div class="titleFusion"></div>

                                    <div class="initContainer" [hidden]="hideInitialStep" style="display:none;">
                                        <div class="left">
                                            <textarea spellcheck="false" id="textarealeft" placeholder="Enter JSON to compare, enter an URL to JSON" tabindex="1" [innerHTML]="leftTree | json"></textarea>
                                            <pre id="errorLeft" class="error"></pre>
                                        </div>

                                        <div class="center">
                                            <button #compare id="compare" tabindex="3">Compare</button>
                                            <div class="throbber-loader"></div>
                                        </div>

                                        <div class="right">
                                            <textarea spellcheck="false" class="right" id="textarearight" placeholder="Enter JSON to compare, enter an URL to JSON" tabindex="2" [innerHTML]="rightTree | json"></textarea>
                                            <pre id="errorRight" class="error"></pre>
                                            <textarea spellcheck="false" class="right" id="textareaResultat" placeholder="Enter JSON to compare, enter an URL to JSON" tabindex="2" [innerHTML]="rightTree | json"></textarea>
                                        </div>

                                    </div>


                                    <div class="diffcontainer row">
                                        <div [hidden]="hideReport" class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="report"></div>

                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 leftElementContainer">
                                            <div  style="margin-top:15px;">
                                                <a target="_blank" href="#" id="titleElementLeft" >Left</a>
                                                <br>
                                            </div>
                                            <pre id="out" class="left codeBlock" style="width:100%!important;"></pre>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 rightElementContainer">
                                            <div style="margin-top:15px;">
                                                <a target="_blank" href="#"  id="titleElementRight" >Right</a>
                                                <br>
                                            </div>
                                            <pre id="out2" class="right codeBlock" style="width:100%!important;border-color:green!important;"></pre>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 resultContainer">
                                            <div style="margin-top:12px;">
                                                <h6 style="color:blue;text-align:center;">Résultat de la fusion</h6>                                        
                                            </div>
                                            <pre id="out2" class="right codeBlock resultatFusion" style="width:100%!important;"></pre>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <ul style="display: none;" id="toolbar" class="toolbar"></ul>
                                            <div style="display: none;" id="reportData"></div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer" style="height: 50px; margin-bottom:50px;">
                                <button type="button" id="btn-fusionner" class="btn btn-primary" style="width:100%; text-align:center;font-size:15px;height:40px; ">
                                    Fusionner
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.1.0.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        /*  var trad = {}; 
        var tradDynForm = {};
        var debug = true;
        var url = "<?= $baseUrl ?>/co2/element/getdoublon/getTrad/" + true;

        $.ajax({
            type: "GET",
            url: url,
            success: function(data) {
                
                tradDynForm = data.split("=");
            },
            error: function(error) {
            }
        }); */
    </script>
    <!-- <script src="<?= $assetsPath ?>/js/co.js"></script> -->
    <script src="<?= $assetsPath ?>/js/treeSortable.js"></script>
    <script src="<?= $assetsPath ?>/js/jsl.format.js"></script>
    <script src="<?= $assetsPath ?>/js/jsl.interaction.js"></script>
    <script src="<?= $assetsPath ?>/js/jsl.parser.js"></script>
    <script src="<?= $assetsPath ?>/js/jdd.js"></script>
    <script src="<?= $assetsPath ?>/js/sweetalert.min.js"></script>
    <script src="<?= $baseUrl ?>/plugins/toastr/toastr.js"></script>
    <script src="<?= $baseUrl ?>/js/api.js"></script>

    <!-- <script src="<?= $baseUrl ?>/plugins/jquery.dynForm.js"></script> -->

    <script src="<?= $assetsPath ?>/js/default/loginRegister.js"></script>
    <script src="<?= $assetsPath ?>/js/bootstrap-notify.min.js"></script>

    <script type="text/javascript" src="https://mdbcdn.b-cdn.net/wp-content/themes/mdbootstrap4/docs-app/js/dist/search/search.min.js"></script>
    <script>
        var all = {};
        var allToFusion;
        var elementDrag;
        var elementLeftTrueValue = [];
        var elementRightTrueValue = [];
        var elementResultToDelete = [];
        var idActif = "";
        var intoActif = "";

        //Login.openLogin();
        //onClickElementInList();
        //toastr.error("teste");
        onLoadMerge();

        $(".close-modal-modalFusion").click(function() {
            $('#modalFusion').modal("toggle");
        });

        $("#btn-fusionner").click(function() {
            confirmBox(idActif, intoActif);
        });

        $("#rechercheInput").change(function(){
            //rechercheKeyInJson($(this).val());
        });

        $("#btnFiltrerKey").click(function(){
            filterKeyFusion($("#rechercheInput").val());
            //rechercheKeyInJson($(this).val());
        });

        function filterKeyFusion(key){
            const allTmp = all;
            var right = allTmp[intoActif];  
            var left = allTmp[idActif];
            if(typeof allTmp[intoActif] != "undefined" && typeof allTmp[intoActif][key] != "undefined" )
                right = allTmp[intoActif][key];
            if(typeof allTmp[idActif] != "undefined" && typeof allTmp[idActif][key] != "undefined" )
                left = allTmp[idActif][key];
            eventChangeSelect(left, right)
        }


        function formateDate(numberLong){
            const arr = numberLong.split("-");
            if(arr.length > 1){
                var month = "";
                if(arr[1] == 01 || arr[1] == 1 ) month = "Janvier"; 
                else if(arr[1] == 02 || arr[1] == 2 ) month = "Février"; 
                else if(arr[1] == 03 || arr[1] == 3 ) month = "Mars";
                else if(arr[1] == 04 || arr[1] == 4 ) month = "Avril";
                else if(arr[1] == 05 || arr[1] == 5 ) month = "Mei";
                else if(arr[1] == 06 || arr[1] == 6 ) month = "Juin";
                else if(arr[1] == 07 || arr[1] == 7 ) month = "Juillet";
                else if(arr[1] == 08 || arr[1] == 8 ) month = "Août";
                else if(arr[1] == 09 || arr[1] == 9 ) month = "Séptembre";
                else if(arr[1] == 10 ) month = "Octobre";
                else if(arr[1] == 11 ) month = "Novembre";
                else if(arr[1] == 12 ) month = "Décembre";

                return arr[2] + " " + month + " " + arr[0] ;
            }
            return numberLong;
        }

        function onLoadMerge(){
            var url = "<?= $baseUrl ?>/co2/element/mergedatadoublon/element/<?= $type ?>/id/<?= $id ?>/into/<?= $into ?>";
            $.ajax({
                type: "GET",
                url: url,
                success: function(data) {
                    var into = "";
                    var id = "";
                    var nameLeft = "";
                    var nameRight = "";
                    var slugLeft = "#";
                    var slugRight = "#";
                    
                    $.each(data, function(elementToDeletetOrToMerge, element) {
                        if(elementToDeletetOrToMerge == "elementToDelete"){
                            into = element._id.$id;
                            nameLeft = element.name + " ( Modifié le "+ formateDate('<?= $dateLeft ?>')+" )";

                            if(typeof element.slug != "undefined")
                                slugLeft = element.slug;
                        }
                        else if(elementToDeletetOrToMerge == "elementToMerge"){
                            id = element._id.$id;
                            nameRight = element.name + " ( Modifié le " +formateDate('<?= $dateRight ?>')+" )";

                            if(typeof element.slug != "undefined")
                                slugRight = element.slug;
                        }
                                
                        all[element._id.$id] = element;
                    });

                    $('#modalFusion .modal-body .titleFusion').html(setElementInModalId(into, id, nameLeft, nameRight, slugLeft, slugRight));
                    eventChangeSelect(all[into], all[id]);

                    $("#modalFusion").modal("show");
                },
                error: function(error) {
                    notification(error, "danger");
                }
            });
        }

        function getKeyOrValueInJsonSelected(selected){
            var element = "";
            element =  selected.split(": ")[0];

            element =  element.split(",")[0];
                    
            if(typeof element.split('"')[1] != "undefined"){
                element =  element.split('"')[1];
            }
            else{
                var deleteSpace = ""
                for(i = 0; i < element.length ; i++){
                    if(element[i] != " ")
                        deleteSpace += element[i];
                }
                element = Number(deleteSpace);
            }                        
            return element;
        }

        function removeElementInArray(arr, val){
            var result = [];
            const index = arr.indexOf(val);
            for(i = 0 ; i < arr.length; i++){
                if(index != i){
                    result.push(arr[i]);
                }
                    
            }
            return result;
        }

        function eventClickElementInResultatJson(){
            $(".resultContainer .code").click(function(){
                const selected =  $(this).html();
                var element = getKeyOrValueInJsonSelected(selected);

                if($(this).hasClass("resultToDelete")){
                    $(this).removeClass("resultToDelete");
                    elementResultToDelete = removeElementInArray(elementResultToDelete, element);
                }
                else{
                    $(this).addClass("resultToDelete");
                    elementResultToDelete.push(element);
                }
                
            });
        }

        function eventClickJson(){
            $(".leftElementContainer .code").click(function(){
                const selected =  $(this).html();
                var element = getKeyOrValueInJsonSelected(selected);

                if($(this).hasClass("leftToPrio")){
                    $(this).removeClass("leftToPrio");
                    elementLeftTrueValue = removeElementInArray(elementLeftTrueValue, element);
                }
                else{
                    $(this).addClass("leftToPrio");
                    elementLeftTrueValue.push(element);
                }
                var left = all[idActif];
                var right = JSON.parse(JSON.stringify(all[intoActif]));
                const resultArray = compareArray(left, right);

                $("#textareaResultat").val(JSON.stringify(resultArray, null, 4));
                refreshResultatFusion();
                
            });

            $(".rightElementContainer .code").click(function(){
                const selected =  $(this).html();
                var element = getKeyOrValueInJsonSelected(selected);

                if($(this).hasClass("leftToPrio")){
                    $(this).removeClass("leftToPrio");
                    elementRightTrueValue = removeElementInArray(elementRightTrueValue, element);
                }
                else{
                    $(this).addClass("leftToPrio");
                    elementRightTrueValue.push(element);
                }
                var left = all[idActif];
                var right = JSON.parse(JSON.stringify(all[intoActif]));
                const resultArray = compareArray(left, right);

                $("#textareaResultat").val(JSON.stringify(resultArray, null, 4));
                refreshResultatFusion();
                
            });

            
            eventClickElementInResultatJson();
            
        }

        function refreshResultatFusion(){
            var resultatFusion = JSON.parse($('#textareaResultat').val());

            var config3 = jdd.createConfig();
            jdd.formatAndDecorate(config3, resultatFusion);
            $(".resultatFusion").text(config3.out);

            formatPRETagsResultat();

            eventClickElementInResultatJson();
        }

        function formatPRETagsResultat(){
            forEach($(".resultContainer pre"), function (pre) {
                var codeBlock = $('<pre class="codeBlock"></pre>');
                var lineNumbers = $('<div class="gutter"></div>');
                codeBlock.append(lineNumbers);

                var codeLines = $('<div></div>');
                codeBlock.append(codeLines);

                var addLine = function (line, index) {
                    var element = getKeyOrValueInJsonSelected(line);
                    var div = $('<div class="codeLine line' + (index + 1) + '" id="'+element+'"></div>');
                    lineNumbers.append($('<span class="line-number">' + (index + 1) + '.</span>'));

                    var span = $('<span class="code"></span');
                    if(elementResultToDelete.includes(getKeyOrValueInJsonSelected(line)))
                        span = $('<span class="code resultToDelete"></span');
                    span.text(line);
                    div.append(span);

                    
                    codeLines.append(div);
                };

            

                var lines = $(pre).text().split('\n');

                
                lines.forEach(addLine);

                codeBlock.addClass($(pre).attr('class'));
                codeBlock.attr('id', $(pre).attr('id'));

                $(pre).replaceWith(codeBlock);
            });
        }

        function mergeData(type, merge, into, deleteAfterMerge = false) {
            const dataToSend = {
                "left" : elementLeftTrueValue,
                "right": elementRightTrueValue,
                "resultDelete" : elementResultToDelete,
            }
            var url = "<?= $baseUrl ?>/co2/element/mergedatadoublon/element/" + type + "/id/" + merge + "/into/" + into + "/merge/" + true;

            if (deleteAfterMerge)
                url += "/deleteAfterMerge/" + deleteAfterMerge;

            $.ajax({
                type: "POST",
                url: url,
                data : dataToSend,
                success: function(data) {
                    elementLeftTrueValue = [];
                    elementRightTrueValue = [];
                    elementResultToDelete = [];
                    notification("fusion en cours");
                    notification("modification en cascade des links");
                    notification("fin de la fusion");
                    var url = "<?= $baseUrl ?>/co2/element/getdoublon/last/" + into+"/lastType/"+type;
                    window.location.replace(url)
                },
                error: function(error) {
                    notification(error, "danger");
                    elementLeftTrueValue = [];
                    elementRightTrueValue = [];
                    elementResultToDelete = [];
                }
            });
        }

        function rechercheKeyInJson(val){
            $("pre .find").removeClass("find");
            $("#btnFiltrerKey").attr("href", "#");

            if(val != "" && typeof $("pre #"+val) != "undefined"  && typeof $("pre #"+val)[0] != "undefined"){

                $("pre #"+val).addClass("find");

                /* document.querySelector("pre #"+val).scrollIntoView({
                    block : "end",
                    behavior : "smooth"
                }); */
            }
        }

        function setElementInModalId(keyLeft, keyRight, nameLeft = "", nameRight = "", slugLeft = "#", slugRigth = "#") {
            $("#titleElementLeft").html(nameLeft);
            $("#titleElementRight").html(nameRight);

            $("#titleElementLeft").attr("href", "<?= $baseUrl ?>/#@"+ slugLeft);
            $("#titleElementRight").attr("href", "<?= $baseUrl ?>/#@"+ slugRigth);

            idActif = keyLeft;
            intoActif = keyRight;
            var output = `
                <div class="row" id="infomodalmerge" style="margin-left:5px;"></div>`;

            return output;
        }

        function eventChangeSelect(left, right) {
            $("#textarealeft").val(JSON.stringify(left, null, 4));
            $("#textarearight").val(JSON.stringify(right, null, 4));
            const resultat = compareArray(left, right);
            $("#textareaResultat").val(JSON.stringify(resultat, null, 4));

            $("#compare").trigger("click");
            jdd.compare();
            showInformationModal(all);
            eventClickJson();
        }

        function compareArray(merge, into) {
            
            $.each(merge, function(key, data) {
                
                if(elementLeftTrueValue.includes(key)){
                    if (Array.isArray(data) && elementLeftTrueValue.includes(key) && elementRightTrueValue.includes(key) ) {
                        if (typeof into[key] != "undefined") {
                            into[key] = copyAllValueTwoArray(data, into[key]);
                        }
                        else
                            into[key] = copyAllValueTwoArray(data, []);
                    } else
                        into[key] = data;
                }
                else{
                    if ((typeof into == "object" && into.hasOwnProperty(key)) || (Array.isArray(into) && typeof into[key] != "undefined")) {
                        if (typeof data != "object" && !Array.isArray(data)) {
                            
                            if(elementLeftTrueValue.includes(data)){
                                into[key] = data;
                            }
                            else{
                                if (data != into[key]) {
                                    if (into[key] == "undefined")
                                        into[key] = data;
                                }
                            }
                        } else {
                            if (Array.isArray(data) && key == "tags") {
                                if (typeof into[key] != "undefined") {
                                    into[key] = copyAllValueTwoArray(data, into[key]);
                                }
                                else
                                    into[key] = copyAllValueTwoArray(data, []);
                            } else
                                into[key] = compareArray(data, into[key])
                        }
                    } else {
                        into[key] = data;
                    }
                }
            });
            return into;
        }

        function copyAllValueTwoArray(array1, array2){
            var tmp = [];
            for (i = 0; i < array2.length; i++) {
                if (typeof array2[i] != "undefined") {
                    if (!tmp.includes(array2[i]))
                        tmp.push(array2[i]);
                }
            }

            for (i = 0; i < array1.length; i++) {
                if (typeof array1[i] != "undefined") {
                    if (!tmp.includes(array1[i]))
                        tmp.push(array1[i]);
                }
            }
        
            return tmp;
        }

        function confirmBox(merge, into) {
            $('#modalFusion').modal("toggle");
            var message = "Vous voulez vraiment fusioner l'élement " + $("#titleElementLeft").html() + " à l'element " + $("#titleElementRight").html() + " ?"; 
            swal({
                title: "Cette action sera ireversible !",
                text: message,
                type: "warning",
                buttons: {
                    confirm: {
                        text: "Oui, fusioner!",
                        className: "btn btn-danger"
                    },
                    cancel: {
                        text: "non, annuler!",
                        visible: true,
                        className: "btn btn-success"
                    }
                }
            }).then((Confirm) => {
                if (Confirm) {
                    swal.close();
                    confirmToDelete();
                } else{
                    swal.close();
                    onLoadMerge();
                }
                    
            });
        }

        function confirmToDelete() {
            var message = "Vous voulez supprimer l'element '" + $("#titleElementLeft").html() + "' après la fusion?";
            swal({
                title: message,
                text: "",
                type: "warning",
                buttons: {
                    confirm: {
                        text: "Oui!",
                        className: "btn btn-success"
                    },
                    cancel: {
                        text: "non!",
                        visible: true,
                        className: "btn btn-danger"
                    }
                }
            }).then((confirm) => {
                if (confirm) {
                    swal.close();

                    sendDataMerge(true);

                } else {
                    swal.close();
                    sendDataMerge(false);
                }
            });
        }

        function sendDataMerge(deleteAfterMerge = false) {
            const type = "<?= $type ?>";
    
            mergeData(type, idActif, intoActif, deleteAfterMerge);
           
            onLoadMerge();
        }

        function notification(message, type = "info") {
            $.notify({
                message: message
            }, {
                type: type,
                placement: {
                    from: "bottom",
                    align: "right"
                }
            });
        }

        function showInformationModal(allElement) {
            $("#infomodalmerge").html("");

            var left = allElement[idActif];
            var right = allElement[intoActif];

            if (typeof right == "object" && typeof right["costum"] != "undefined")
                $("#infomodalmerge").append(contentInformationInModal("L'élement de droite possède une costum"));

            if (typeof right == "object" && typeof right["oceco"] != "undefined")
                $("#infomodalmerge").append(contentInformationInModal("L'élement de droite possède un oceco"));

            if (typeof left == "object" && typeof left["costum"] != "undefined")
                $("#infomodalmerge").append(contentInformationInModal("L'élement de gauche possède une costum"));

            if (typeof left == "object" && typeof left["oceco"] != "undefined")
                $("#infomodalmerge").append(contentInformationInModal("L'élement de gauche possède une costum"));

            compareAdresse(left, right);
            //infomodalmerge
        }

        function compareAdresse(left, right) {
            if (typeof left == "object" && typeof left["address"] != "undefined" && typeof left["address"]["addressLocality"] != "undefined" && typeof left["address"]["streetAddress"] != "undefined") {
                if (typeof right == "object" && typeof right["address"] != "undefined" && typeof right["address"]["addressLocality"] != "undefined" && typeof right["address"]["streetAddress"] != "undefined") {
                    if (right["address"]["addressLocality"] == left["address"]["addressLocality"] && right["address"]["streetAddress"] == left["address"]["streetAddress"])
                        $("#infomodalmerge").append(contentInformationInModal("Les 2 élements ont la même adresse"));
                    else
                        $("#infomodalmerge").append(contentInformationInModal("Les 2 élements ont une adresse différent"));
                } else
                    $("#infomodalmerge").append(contentInformationInModal("L'élement de gauche possède une adresse et du droite non"));
            } else {
                if (typeof right == "object" && typeof right["address"] != "undefined" && typeof right["address"]["addressLocality"] != "undefined" && typeof right["address"]["streetAddress"] != "undefined")
                    $("#infomodalmerge").append(contentInformationInModal("L'élement de droite possède une adresse et du gauche non"));
                else
                    $("#infomodalmerge").append(contentInformationInModal("Les 2 élements n'ont pas d'adresse"));
            }
        }

        function contentInformationInModal(message) {
            return '<div class="col-md-3"><p><input id="showEq" type="checkbox" name="checkbox" value="value" checked="true">' + message + '</p></div>';
        }

    </script>


</body>

</html>