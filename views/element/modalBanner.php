<?php $cssAnsScriptFilesTheme = array(
		"/plugins/jquery-cropbox/jquery.cropbox.css",
		"/plugins/jquery-cropbox/jquery.cropbox.js",
		"/plugins/jquery-cropper/cropper.css",
		"/plugins/jquery-cropper/cropper.js", 
	);

	HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesTheme, Yii::app()->request->baseUrl);
	$idform = $id ?? 'banner_photoAdd';
	$varKey = $id ?? '';
	$bannerElement = !empty($id) ? $id.'-banner_element' : "banner_element";
	$modalCropBanner = !empty($id) ? $id.'-modal-crop-banner' : "modal-crop-banner";
	$colBanner = !empty($id) ? $id.'-col-banner' : "col-banner";
	$bannerChange = !empty($id) ? $id.'-banner_change' : "banner_change";
	$uploadScropResizeAndSaveImage = !empty($id) ? $id.'-uploadScropResizeAndSaveImage' : "uploadScropResizeAndSaveImage";
	$saveBanner = !empty($id) ? $id.'-saveBanner' : "saveBanner";
	$contentBanner = !empty($id) ? $id.'-contentBanner' : "contentBanner";
	$contentHeaderInformation = !empty($id) ? $id.'-contentHeaderInformation' : "contentHeaderInformation";
	$banner_isSubmit = !empty($id) ? $id.'-banner_isSubmit' : "banner_isSubmit";
	$cropImage = !empty($id) ? $id.'-cropImage' : "cropImage";
	$cropContainer = !empty($id) ? $id.'-cropContainer' : "cropContainer";
	$btnColor = !empty($id) ? 'text-white' : ""; 
?>
<form  method="post" id="<?= $idform ?>" enctype="multipart/form-data">
	<?php 
		$editBtnStr = (isset($profilBannerUrl) && !empty($profilBannerUrl)) ? Yii::t("common","Edit banner") : Yii::t("common","Add a banner"); 
		$editBtnIcon = (isset($profilBannerUrl) && !empty($profilBannerUrl)) ? "camera" : "plus";

	if( $edit || ($openEdition && isset(Yii::app()->session["userId"]))){ 

	?>
		<div class="user-image-buttons padding-10" style="position: absolute;z-index: 100;">
			<a class="btn btn-blue btn-file btn-upload fileupload-new btn-sm <?=$btnColor?>" id="<?= $bannerElement ?>" >
				<span class="fileupload-new">
					<i class="fa fa-<?php echo $editBtnIcon ?>"></i> 
					<span class="hidden-xs"> 
					<?php echo $editBtnStr ?>
					</span>
				</span>
				<input type="file" accept=".gif, .jpg, .png" name="banner" id="<?= $bannerChange ?>" class="hide">
				<input class="<?= $banner_isSubmit ?> hidden" value="true"/>
			</a>
		</div>
       
	<?php } ?>
</form>
<div id="<?= $modalCropBanner ?>" class="portfolio-modal modal" tabindex="-1" role="dialog" aria-hidden="true" style="background-color: rgba(255, 255, 255, 0.8);">
	<div class="col-xs-12 no-padding">
        <h3 class="title-comment pull-left"></h3>
        <div class="close-modal" data-dismiss="modal"><div class="lr"><div class="rl"></div></div></div>
    </div>
	<div id="<?= $uploadScropResizeAndSaveImage ?>" style="padding:0px 60px;">
		<?php $imgLogo=(@$this->costum["logo"]) ? $this->costum["logo"] : Yii::app()->theme->baseUrl."/assets/img/LOGOS/CO2/logo-head-search.png";
			$titleBannerCrop=(isset($title)) ? $title : Yii::t("common","Resize and crop your image to render a beautiful banner");
		?>
		<div class="modal-header text-dark">
			<h3 class="modal-title text-center" id="ajax-modal-modal-title">
				<i class="fa fa-crop"></i> <?php echo $titleBannerCrop ?>
			</h3>
		</div>
		<div class="panel-body">
			<div class='' id='<?= $cropContainer ?>'>
				<img src='' id='<?= $cropImage ?>' class='' style=''/>
				<div class='col-xs-12 text-center'>
					<button class='btn btn-success text-white imageCrop <?= $saveBanner ?>'><i class="fa fa-send"></i> <?php echo Yii::t("common","Save") ?></button>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-dialog-centered" style="margin-top: 6%;">
        <div class="modal-content text-center" style="max-width: 500px; max-height: 450px; overflow: hidden;">
            <div class="modal-body">
                <div class="container-copy-paste">
                    <div class="img-area">
                        <div id="switch-image" class="image-container">
                            <i class="fa fa-upload" style="font-size: 48px;"></i>
                            <h3>Upload Image</h3>
                            <p> <?php echo Yii::t("common", "Image size must be less than") ?> <span>2MB</span></p>
                        </div>
                        <input type="text" id="paste-image" class="form-control mt-auto" placeholder="Paste an image from your browser here">
                    </div>
                    <button class="btn select-image"> <?php echo Yii::t("common", "Select Image") ?> </button>
                </div>
            </div>
        </div>
    </div>
</div>




<script>
var contextId<?= $varKey ?> =<?php echo json_encode((string)$element["_id"]); ?>;
var contextType<?= $varKey ?>=<?php echo json_encode($element["collection"]); ?>;
var contextName<?= $varKey ?>=<?php echo json_encode($element["name"]); ?>;
var forcedUnloggued= <?php echo json_encode(@$forcedUnloggued); ?>;
$.fn.cropper;

jQuery(document).ready(function() {
	$("#<?= $colBanner ?>").mouseenter(function(){
		$("#<?= $bannerElement ?>").show();
	}).mouseleave(function(){
		$("#<?= $bannerElement ?>").hide();
	});
	//IMAGE CHANGE//
	$("#<?= $uploadScropResizeAndSaveImage ?> .close-modal").click(function(){
		$.unblockUI();
	});

		var cropper; 

	$("#<?= $bannerElement ?>").click(function(event){
			if (!$(event.target).is('input')) {
					$(this).find("input[name='banner']").trigger('click');
			}
		//$('#'+contentId+'_avatar').trigger("click");		
	});
	
	$('#<?= $bannerChange ?>').off().on('change.bs.fileinput', function () {
		setTimeout(function(){
			var files = document.getElementById("<?= $bannerChange ?>").files;
			if (files[0].size > 5000000)
				toastr.warning("<?php echo Yii::t('common','Size maximum 5Mo') ?>");
			else {
				for (var i = 0; i < files.length; i++) {           
			        var file = files[i];
			       	var imageType = /image.*/;     
			        if (!file.type.match(imageType)) {
			            continue;
			        }           
			        var img=document.getElementById("<?= $cropImage ?>");            
			        img.file = file;    
			        var reader = new FileReader();
			        reader.onload = (function(aImg) { 
			        	var image = new Image();
							image.src = reader.result;
							img.src = reader.result;
							image.onload = function() {
   							// access image size here 
   						 	var imgWidth=this.width;
   						 	var imgHeight=this.height;
   							if(imgWidth>=400 && imgHeight>=150){
   								$("#<?= $modalCropBanner ?>").modal("show");
               					$("#<?= $uploadScropResizeAndSaveImage ?>").parent().css("padding-top", "0px !important");
								setTimeout(function(){
									var setImage={"width":1600,"height":400};
									var parentWidth=$("#<?= $cropContainer ?>").width();
									if(parentWidth > 1200)
										heightCrop=400;
									else if(parentWidth > 800)
										heightCrop=300;
									else if(parentWidth > 600)
										heightCrop=250;
									else if(parentWidth > 400)
										heightCrop=150;
									else 
										heightCrop=60;
									//var parentHeight=setImage.height*(parentWidth/setImage.width);
									var crop = $('#<?= $cropImage ?>').cropbox({
										width: parentWidth,
										height: heightCrop,
										zoomIn:true,
										zoomOut:true}, function() {
											cropResult=this.result;
											
									}).on('cropbox', function(e, crop) {
										cropResult=crop;
						        		
						        
									});
									$(".<?= $saveBanner ?>").click(function(){
								        
								        //var cropResult=cropResult;
								        $(this).prop("disabled",true);
								        $(this).find("i").removeClass("fa-send").addClass("fa-spin fa-spinner");
								        $("#<?= $idform ?>").submit();
									});
									$("#<?= $idform ?>").off().on('submit',(function(e) {
										//alert(moduleId);
										$(".<?= $banner_isSubmit ?>").val("true");
										e.preventDefault();
										
										var fd = new FormData(document.getElementById("<?= $idform ?>"));
										fd.append("parentId", contextId<?= $varKey ?> );
										fd.append("parentType", contextType<?= $varKey ?>);
										fd.append("formOrigin", "banner");
										//fd.append("contentKey", "banner");
										fd.append("cropW", cropResult.cropW);
										fd.append("cropH", cropResult.cropH);
										fd.append("cropX", cropResult.cropX);
										fd.append("cropY", cropResult.cropY);
										extraParamsAjax={
											contentType: false,
											cache: false, 
											processData: false
										};

										extraP=(forcedUnloggued) ? "/forcedUnloggued/true" : "";
				    					ajaxPost(
											null,
										    baseUrl+"/"+moduleId+"/document/upload-save/dir/communecter/folder/"+contextType<?= $varKey ?>+"/ownerId/"+contextId<?= $varKey ?> +"/input/banner/docType/image/contentKey/banner"+extraP,
										    fd,
										    function(data){ 
										     	if(data.result){
										        	$(".<?= $saveBanner ?>").prop("disabled",false);
								        			$(".<?= $saveBanner ?>").find("i").removeClass("fa-spin fa-spinner").addClass("fa-send");
										        	newBanner='<a href="'+baseUrl+data.src.profilRealBannerUrl+'" class="thumb-info" data-title="'+trad.coverImageof+' '+contextName<?= $varKey ?>+'"" data-lightbox="all">'+
										        		'<img class="col-md-12 col-sm-12 col-xs-12 no-padding img-responsive" src="'+baseUrl+data.src.profilBannerUrl+'" style="">'+
										        		'</a>';
										        	$("#<?= $contentBanner ?>").html(newBanner);
										        	$(".<?= $contentHeaderInformation ?>").addClass("backgroundHeaderInformation");
										        	$('#<?= $modalCropBanner ?>').modal("hide");
	    								    	}	
											},
											null,
											null,
											extraParamsAjax
										);
									}));
								}, 300);
							}
   						 	else
   						 		toastr.warning(trad["minsizebanner"]);
						};
			        });
			        reader.readAsDataURL(file);
			    }  
			}
		}, 400);
	});
});
</script>


<style>

:root {
  --light-blue: #B6DBF6;
  --grey: #f2f2f2;
}

.container-copy-paste {
  max-width: 400px;
  width: 100%;
  background: #fff;
  border-radius: 30px;
  margin-right: auto;
  margin-left: auto;
  padding-left: 15px;
  padding-right: 15px;
}

.img-area {
  position: relative;
  width: 100%;
  height: 280px;
  padding: 5px;
  background: var(--grey);
  margin-bottom: 10px;
  border-radius: 15px;
  overflow: hidden;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
}

.img-area h3 {
  font-size: 20px;
  font-weight: 500;
  margin-bottom: 6px;
}

.img-area p {
  color: #999;
}

.img-area p span {
  font-weight: 600;
}

.image-container {
  position: relative;
  width: 100%;
}

.image-container img {
  width: 100%;
  height: auto;
  object-fit: cover;
  object-position: center;
}

.image-container .delete-icon {
  position: absolute;
  top: 15%;
  right: 2%;
  transform: translate(-50%, -50%);
  background-color: red;
  color: white;
  font-size: 16px;
  width: 24px;
  height: 24px;
  line-height: 24px;
  text-align: center;
  border: none;
  cursor: pointer;
  border-radius: 50%;
}

.img-area input[type="text"] {
  position: absolute;
  bottom: 0;
  left: 0;
  right: 0;
  border: none;
  height: 60px;
  text-align: center;
  border-left: 1px solid #ccc;
  border-right: 1px solid #ccc;
  border-bottom: 1px solid #ccc;
  border-bottom-left-radius: 15px;
  border-bottom-right-radius: 15px;
  padding: 5px 0;
}

.select-image{
  display: block;
  width: 100%;
  height: 40px;
  padding: 5px 0;
  border-radius: 20px;
  background-color: #9fbd38;
  color: #fff;
  margin-bottom: 5px;
  font-weight: 500;
  font-size: 16px;
  border: none;
  cursor: pointer;
  transition: background-color 0.3s ease; /* Improved button transition */
}

.select-image:hover{
  background-color: #7daa2d; /* Slightly darker color on hover */
}

</style>

			<!-- // if (!$(event.target).is('input')) {
			// 		$(this).find("input[name='banner']").trigger('click');
			// }
		//$('#'+contentId+'_avatar').trigger("click");	  -->