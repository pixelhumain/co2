<?php
    $cssAnsScriptFilesModule = array(
        //Data helper
        '/js/dataHelpers.js',
        '/js/default/editInPlace.js',
    );
    HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, $this->module->getParentAssetsUrl());
    $elementParams=@Yii::app()->session['paramsConfig']["element"];
    if(isset(Yii::app()->session["costum"])){
        $cssJsCostum=array();
        if(isset($elementParams["js"]) && $elementParams["js"] == "about.js")
            array_push($cssJsCostum, '/js/'.Yii::app()->session["costum"]["slug"].'/about.js');
        if(isset($elementParams["css"]) && $elementParams["js"] == "about.css")
            array_push($cssJsCostum, '/css/'.Yii::app()->session["costum"]["slug"].'/about.css');
        if(!empty($cssJsCostum))
            HtmlHelper::registerCssAndScriptsFiles($cssJsCostum, Yii::app()->getModule( "costum" )->getAssetsUrl());
    }
?>
<style>
.nv-3{
    font-size: 16px;
    margin-top: 20px;
    margin-bottom: 10px;
    text-transform: uppercase;
    font-weight: 700!important;
}
.ibox {
    border-radius: 1px!important;
}
.mg-30{
    margin-left: 25px!important;
}
.card-text {
    padding-top: 10px;
}
.tag-list li a {
    font-size: 12px;
    background-color: white;
    padding: 5px 12px;
    color: #333;
    border-radius: 2px;
    border: 1px solid #e7eaec;
    margin-right: 5px;
    margin-top: 5px;
    display: block;
}
ul.tag-list li {
    list-style: none;
}
.tag-list li {
    float: left;
}
.desc {
    border-bottom: 1px dotted #29b8ee;
    padding: 10px 0;
}
.desc-short {
    padding: 10px 0;
}
.marge {
    width: 60px;
    margin: 0 00px 0 0;
    float: left;
}
.taille{
     width: 45px;
     height: 45px;
     line-height: 45px;
     font-size: 20px;
     border-radius: 50%;
     color: #fff;
}
.couleur-orange{
    background-color: #ff9800;
}
.details-desc {
    float: left;
}
.desc:hover{
    background-color: #fff;
}
.couleur-bleu{
   color: #29b8ee;
}
.couleur-noir{
    color: #2C3E50;
}
.couleur-rouge{
    color: #e6344d;
}
.couleur-vert{
    background-color: #4caf50;
}
.txt-vert{
    color: #4caf50;
}
.txt-orange {
    color: #ff9800;
}
.fs {
    font-size: 20px!important;
}
.badge {
    border-radius: 5px!important;
}

.card.features {
    border: 0;
    border-radius: 3px;
    box-shadow: 0px 5px 7px 0px rgba(0, 0, 0, 0.04);
    transition: all 0.3s ease;
}
.card.features:before {
    content: "";
    position: absolute;
    width: 3px;
    color: #e6344d;
    background: -moz-linear-gradient(top, #e6344d 0%, #e6344d 100%);
    background: -webkit-linear-gradient(top, #e6344d 0%, #e6344d 100%);
    background: linear-gradient(to bottom, #e6344d 0%, #e6344d 100%);
    top: 0;
    bottom: 0;
    left: 0;
}
.gradient-fill:before {
    color: #e6344d;
    background: -moz-linear-gradient(top, #e6344d 0%, #e6344d 100%);
    background: -webkit-linear-gradient(top, #e6344d 0%, #e6344d 100%);
    background: linear-gradient(to bottom, #e6344d 0%, #e6344d 100%);
    -webkit-background-clip: text;
    -webkit-text-fill-color: transparent;
}
.card {
    position: relative;
    display: -ms-flexbox;
    display: flex;
    -ms-flex-direction: column;
    flex-direction: column;
    min-width: 0;
    word-wrap: break-word;
    background-color: #eee;
    background-clip: border-box;
    border: 1px solid rgba(0,0,0,.125);
    border-radius: .25rem;
}
.card-body {
    -ms-flex: 1 1 auto;
    flex: 1 1 auto;
    padding: 1.25rem;
}
.media {
    display: -ms-flexbox;
    display: flex;
    -ms-flex-align: start;
    align-items: flex-start;
}
.media-body {
    -ms-flex: 1;
    flex: 1;
}
.card-title {
    margin-bottom: .75rem;
}
.ttr-4 {
    font-size: 14px;
    font-weight: 500;
    color: #e6344d;
}
.ttr-desc-4 {
    font-size: 14px;
    font-weight: 500;
}
.pad-2{
    padding-top: 2px;
    padding-bottom: 2px;
}
.btn-modif-desc {
    color: #29b8ee;
    background-color: #fff;
    border-color: #29b8ee;
}
.btn-modif-desc:hover {
    color: #fff;
    background-color: #29b8ee;
    border-color: #2590c0;
}
.btn-adress {
     color: #2C3E50;
     background-color: #fff;
     border-color: #2C3E50;
 }
.btn-adress:hover{
    color: #fff;
    background-color: #2C3E50;
    border-color: #222f3d;
}
.btn-modif-info-G {
    color: #d9534f;
    background-color: #fff;
    border-color: #d43f3a;
}
.btn-modif-info-G:hover{
    color: #fff;
    background-color: #d9534f;
    border-color: #d43f3a;
}
.ti-2x {
    font-size: 1.8em;
}
.mr-3, .mx-3 {
    margin-right: 1rem!important;
    width: 25px;
}
.sidebar-widget-area .title {
    display: block;
    font-size: 16px;
    color: #000000;
    margin-bottom: 0;
    margin-right: auto;
    font-weight: 300;
    padding: 12px 0;
    border-bottom: 2px solid #336699;
    padding-left: 30px;
}
.sidebar-widget-area .widget-content {
    padding: 30px 0 30px 30px;
}
.git-color{
    background-color: #444;
}
.fb-color{
    background-color: #3b5998;
}
.linkedin-color{
    background-color: #007bb6;
}
.twitter-color{
    background-color: #55acee;
}
.instagram-color{
    background-color: #c94d82;
}

.history-tl-container ul.tl{
    margin:10px 0;
    padding:0;

}
.history-tl-container ul.tl li{
    list-style: none;
    margin-left: 45px;
    /* min-height: 50px;
      background: rgba(255,255,0,0.1); */
    border-left: 2px dashed #e6344d;
    padding: 0 0 10px 20px;
    position: relative;
}
.history-tl-container ul.tl li:last-child{ border-left:0;}
.history-tl-container ul.tl li::before{
    position: absolute;
    left: -14px;
    top: 0;
    content: " ";
    border: 8px solid rgba(255, 255, 255, 0.74);
    border-radius: 500%;
    background: #e6344d;
    height: 25px;
    width: 25px;
    transition: all 500ms ease-in-out;
}
.history-tl-container ul.tl li:hover::before{
    border-color:  #e6344d;
    transition: all 1000ms ease-in-out;
}
ul.tl li .item-title{
    font-size: 14px;
    font-weight: 500;
    color: #e6344d;
}
ul.tl li .item-detail{
    margin-left: 20px;
    font-size: 15px;
}

.addReadMore.showlesscontent .SecSec,
.addReadMore.showlesscontent .readLess {
    display: none;
}

.addReadMore.showmorecontent .readMore {
    display: none;
}

.addReadMore .readMore,
.addReadMore .readLess {
    font-weight: bold;
    margin-left: 2px;
    color: blue;
    cursor: pointer;
}

.addReadMoreWrapTxt.showmorecontent .SecSec,
.addReadMoreWrapTxt.showmorecontent .readLess {
    display: block;
}
</style>

<div class='col-md-12 margin-bottom-15'>
    <i class="fa fa-info-circle fa-2x"></i><span class='Montserrat' id='name-lbl-title'> <?php echo Yii::t("common","About") ?></span>
</div>
<div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
<div  id="ficheInfo">
    <span class="nv-3 couleur-bleu">
        <i class="fa fa-file-text-o"></i>&nbsp;<?php echo Yii::t("common","Descriptions") ?>
    </span>
    <?php if($edit==true || ( $openEdition==true && Yii::app()->session["userId"] != null ) ){?>
    <button class="btn-update-descriptions btn btn-modif-desc pad-2 pull-right tooltips"
                data-toggle="tooltip" data-placement="top" title="" alt="" data-original-title="<?php echo Yii::t("common","Update description") ?>">
        <i class="fa fa-edit"></i>&nbsp;<?php echo Yii::t("common", "Edit") ?>
    </button>
    <?php } ?>

    <hr class="mt-2" style="margin-top: 10px; border-top: 1px solid #29b7ee!important;">
    <div class="contentInformation" style="margin: 10px 10px;">
        <span id="shortDescriptionAbout" name="shortDescriptionAbout" style="font-size: 18px;" class="bold"><?php echo (@$element["shortDescription"]) ? $element["shortDescription"] : '<i>'.Yii::t("common","Not specified").'</i>'; ?></span>
        <span id="shortDescriptionAboutEdit" name="shortDescriptionAboutEdit"  class="hidden" ><?php echo (!empty($element["shortDescription"])) ? $element["shortDescription"] : ""; ?></span>
    </div>
    <div class="contentInformation" style="margin: 10px 10px">
        <span class="addReadMore showlesscontent" id="descriptionAbout"><?php echo (@$element["description"]) ? $element["description"] : '<i>'.Yii::t("common","Not specified").'</i>'; ?></span>
    </div>
<!--
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12 contentInformation desc-short">
            <div class="marge col-md-2 col-sm-2 col-xs-3" >
                <div class="taille text-center couleur-orange">
                    <span><i class="fa fa-quote-right"></i></span>
                </div>
            </div>
            <div class="col-md-10 col-sm-10 col-xs-9 details-desc">
                <span class="ttr-desc-4 txt-orange bold"> <?php echo Yii::t("common", "Short description") ?></span><br>
                <span id="shortDescriptionAbout" name="shortDescriptionAbout" ><?php echo (@$element["shortDescription"]) ? $element["shortDescription"] : '<i>'.Yii::t("common","Not specified").'</i>'; ?></span>
            </div>
            <span id="shortDescriptionAboutEdit" name="shortDescriptionAboutEdit"  class="hidden" ><?php echo (!empty($element["shortDescription"])) ? $element["shortDescription"] : ""; ?></span>

        </div>

        <div class="col-md-12 col-sm-12 col-xs-12 contentInformation desc-short">
            <div class="marge col-md-2 col-sm-2 col-xs-3" >
                    <div class="taille text-center couleur-vert">
                        <span><i class="fa fa-paragraph"></i></span>
                    </div>
            </div>

                <div class="col-md-10 col-sm-10 col-xs-9  details-desc" style="word-wrap: break-word; overflow:hidden;">
                        <span class="ttr-desc-4 txt-vert bold"> <?php echo Yii::t("common", "Description") ?></span><br>
                    <span id="descriptionAbout"><?php echo (@$element["description"]) ? $element["description"] : '<i>'.Yii::t("common","Not specified").'</i>'; ?></span>
                </div>

        </div>
    </div>
    -->
</div>

    <div class="section light-bg" id="ficheInfo">
        <div class="row">
            <div class="col-md-8 col-sm-8 col-xs-12">
                <span class="nv-3 couleur-rouge">
                    <i class="fa fa-address-card-o"></i> <?php echo Yii::t("common","General information") ?>
                </span>
                <?php if($edit==true || ( $openEdition==true && Yii::app()->session["userId"] != null ) ){?>
                <button class="btn-update-info btn btn-modif-info-G pad-2 pull-right  visible-xs tooltips"
                        data-toggle="tooltip" data-placement="top" title="" alt="" data-original-title="<?php echo Yii::t("common","Update general information") ?>">
                    <i class="fa fa-edit"></i>
                </button>
                <?php } ?>
            </div>
            <?php if($edit==true || ( $openEdition==true && Yii::app()->session["userId"] != null ) ){?>
            <div class=" col-md-4 col-sm-4 col-xs-12 visible-sm visible-md visible-lg">
                <button class="btn-update-info btn btn-modif-info-G pad-2 pull-right tooltips"
                        data-toggle="tooltip" data-placement="top" title="" alt="" data-original-title="<?php echo Yii::t("common","Update general information") ?>">
                    <i class="fa fa-edit"></i>&nbsp;<?php echo Yii::t("common", "Edit") ?>
                </button>
            </div>
            <?php } ?>

        </div>
        <hr class="mt-2" style="margin-top: 10px; border-top: 1px solid #e6344d!important;">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 20px">
                <div class="col-md-6 col-sm-6 col-xs-12" >
                    <div class="card features">
                        <div class="card-body">

                            <div class="media contentInformation">
                                <span class="ti-2x mr-3"><i class="fa fa-pencil gradient-fill"></i></span>
                                <div class="media-body">
                                    <p class="card-text" id="nameAbout"> <?php echo $element["name"]; ?> </p>
                                </div>
                            </div>

                            <?php if($type==Project::COLLECTION){ ?>
                            <div class="media contentInformation">
                                <span class="ti-2x mr-3"><i class="fa fa-pencil gradient-fill"></i></span>
                                <div class="media-body">
                                    <p class="card-text" id="avancementAbout"> <?php echo (@$element["properties"]["avancement"]) ? Yii::t("project",$element["properties"]["avancement"]) : '<i>'.Yii::t("common","Not specified").'</i>' ?> </p>
                                </div>
                            </div>
                            <?php } ?>

                            <?php if($type==Person::COLLECTION) { ?>
                                <div class="media contentInformation">
                                    <span class="ti-2x mr-3"><i class="fa fa-user-secret gradient-fill"></i></span>
                                    <div class="media-body">
                                        <p class="card-text"
                                           id="usernameAbout"><?php echo (@$element["username"]) ? $element["username"] : '<i>' . Yii::t("common", "Not specified") . '</i>' ?></p>
                                    </div>
                                </div>
                                <?php if (Preference::showPreference($element, $type, "birthDate", Yii::app()->session["userId"])) { ?>
                                    <div class="media contentInformation" >
                                        <span class="ti-2x mr-3"><i class="fa fa-birthday-cake gradient-fill"></i></span>
                                        <div class="media-body">
                                            <p class="card-text" id="birthDateAbout"><?php echo (@$element["birthDate"]) ? date("d/m/Y", strtotime($element["birthDate"]))  : '<i>'.Yii::t("common","Not specified").'</i>'; ?></p>
                                        </div>
                                    </div>

                                <?php }
                            }

                            if($type==Organization::COLLECTION || $type==Event::COLLECTION){ ?>
                            <div class="media contentInformation">
                                <span class="ti-2x mr-3"><i class="fa fa-angle-right gradient-fill"></i></span>
                                <div class="media-body">
                                    <p class="card-text"  id="typeAbout">
                                        <?php
                                        if(@$typesList && @$element["type"] && !empty($typesList[$element["type"]]))
                                            $showType=Yii::t( "category",$typesList[$element["type"]]);
                                        else if (@$element["type"])
                                            $showType=Yii::t( "category",$element["type"]);
                                        else
                                            $showType='<i>'.Yii::t("common","Not specified").'</i>';
                                        echo $showType; ?>
                                    </p>
                                </div>
                            </div>
                            <?php }

                            if( (	$type==Person::COLLECTION &&
                                Preference::showPreference($element, $type, "email", Yii::app()->session["userId"]) ) ||
                            in_array($type, [Organization::COLLECTION, Project::COLLECTION, Event::COLLECTION])) { ?>

                            <div class="media contentInformation">
                                <span class="ti-2x mr-3"><i class="fa fa-envelope gradient-fill"></i></span>
                                <div class="media-body">
                                    <p class="card-text" id="emailAbout"><?php echo (@$element["email"]) ? $element["email"]  : '<i>'.Yii::t("common","Not specified").'</i>'; ?></p>
                                </div>
                            </div>
                            <?php } ?>

                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="card features">
                        <div class="card-body">
                            <?php if( $type != Person::COLLECTION /*&& $type != Organization::COLLECTION */ ){ ?>
                            <div class="media contentInformation">
                                <span class="ti-2x mr-3"><i class="fa fa-link gradient-fill"></i></span>
                                <div class="media-body">
                                    <span class="card-title ttr-4"><?php echo Yii::t("common","Carried by"); ?></span>
                                    <p class="card-text" id="parentAbout">
                                        <?php
                                        if(!empty($element["parent"])){
                                            $count=count($element["parent"]);
                                            foreach($element['parent'] as $key =>$v){
                                                $heightImg=($count>1) ? 35 : 25;
                                                $imgPath = (@$v["profilThumbImageUrl"] && !empty($v["profilThumbImageUrl"])) ? Yii::app()->createUrl('/'.$v["profilThumbImageUrl"]) : $this->module->assetsUrl.'/images/thumb/default_'.$v["type"].'.png' ?>
                                                <a href="#page.type.<?php  echo $v['type']; ?>.id.<?php  echo $key; ?>"
                                                   class="lbh tooltips"
                                                    <?php if($count>1) echo 'data-toggle="tooltip" data-placement="left" title="'.$v["name"].'"' ?>>
                                                    <img src="<?php echo $imgPath ?>" class="img-circle" width='<?php echo $heightImg ?>' height='<?php echo $heightImg ?>' />
                                                    <?php  if ($count==1) echo $v['name']; ?>
                                                </a>
                                            <?php 	}
                                        }else
                                            echo '<i>'.Yii::t("common","Not specified").'</i>';
                                        ?>
                                    </p>
                                </div>
                            </div>
                            <?php } ?>

                            <?php if($type == Event::COLLECTION){ ?>
                            <div class="media contentInformation">
                                <span class="ti-2x mr-3"><i class="fa fa-link gradient-fill"></i></span>
                                <div class="media-body">
                                    <span class="card-title ttr-4"><?php echo Yii::t("common","Organized by"); ?></span>
                                    <p class="card-text" id="organizerAbout">
                                        <?php
                                        if(!empty($element["organizer"])){
                                            $count=count($element["organizer"]);
                                            foreach($element['organizer'] as $key =>$v){
                                                $heightImg=($count>1) ? 35 : 25;
                                                $imgPath = (@$v["profilThumbImageUrl"] && !empty($v["profilThumbImageUrl"])) ? Yii::app()->createUrl('/'.$v["profilThumbImageUrl"]) : $this->module->assetsUrl.'/images/thumb/default_'.$v["type"].'.png' ?>
                                                <a href="#page.type.<?php  echo $v['type']; ?>.id.<?php  echo $key; ?>"
                                                   class="lbh tooltips"
                                                    <?php if($count>1) echo 'data-toggle="tooltip" data-placement="left" title="'.$v["name"].'"' ?>>
                                                    <img src="<?php echo $imgPath ?>" class="img-circle" width='<?php echo $heightImg ?>' height='<?php echo $heightImg ?>' />
                                                    <?php  if ($count==1) echo $v['name']; ?>
                                                </a>
                                            <?php 	}
                                        }else
                                            echo '<i>'.Yii::t("common","Not specified").'</i>';
                                        ?>
                                    </p>
                                </div>
                            </div>
                            <?php } ?>

                            <?php if($type!=Poi::COLLECTION){ ?>
                            <div class="media contentInformation">
                                <span class="ti-2x mr-3"><i class="fa fa-desktop gradient-fill"></i></span>
                                <div class="media-body">
                                    <p class="card-text" id="webAbout">
                                        <?php
                                        if(@$element["url"]){
                                            //If there is no http:// in the url
                                            $scheme = ( (!preg_match("~^(?:f|ht)tps?://~i", $element["url"]) ) ? 'http://' : "" ) ;
                                            echo '<a href="'.$scheme.$element['url'].'" target="_blank" id="urlWebAbout" style="cursor:pointer;">'.$element["url"].'</a>';
                                        }else
                                            echo '<i>'.Yii::t("common","Not specified").'</i>'; ?>
                                    </p>
                                </div>
                            </div>
                            <?php } ?>

                            <?php  if($type==Organization::COLLECTION || $type==Person::COLLECTION){ ?>
                            <div class="media contentInformation">
                                <span class="ti-2x mr-3"><i class="fa fa-phone gradient-fill"></i></span>
                                <div class="media-body">
                                    <p class="card-text" id="fixeAbout">
                                        <?php
                                        $fixe = '<i>'.Yii::t("common","Not specified").'</i>';
                                        if( !empty($element["telephone"]["fixe"]))
                                            $fixe = ArrayHelper::arrayToString($element["telephone"]["fixe"]);

                                        echo $fixe;
                                        ?>
                                    </p>
                                </div>
                            </div>


                            <div class="media contentInformation">
                                <span class="ti-2x mr-3"><i class="fa fa-mobile gradient-fill"></i></span>
                                <div class="media-body">
                                    <p class="card-text" id="mobileAbout">
                                        <?php
                                        $mobile = '<i>'.Yii::t("common","Not specified").'</i>';
                                        if( !empty($element["telephone"]["mobile"]))
                                            $mobile = ArrayHelper::arrayToString($element["telephone"]["mobile"]);
                                        echo $mobile;
                                        ?>
                                    </p>
                                </div>
                            </div>

                            <?php } ?>

                        </div>
                    </div>
                </div>

                     <div class="col-md-12 col-sm-12 col-xs-12">
                         <div class="card features visible-lg visible-md visible-sm" style="margin-bottom: 20px">

                         </div>

                        <div class="card features" style="margin-bottom: 20px">
                            <div class="card-body">

                                <div class="media contentInformation">
                                    <span class="ti-2x mr-3"><i class="fa fa-hashtag gradient-fill"></i></span>
                                    <div class="media-body">
                                        <p class="card-text">
                                        <ul class="tag-list" style="padding: 0">
                                            <?php
                                            if(!empty($element["tags"])){
                                                foreach ($element["tags"]  as $key => $tag) {
                                                    echo '<li><a href="#search?text=#'.$tag.'"> <i class="fa fa-tag couleur-rouge"></i>&nbsp;'.$tag.'</a></li>';
                                                }
                                            }else{
                                                echo '<i>'.Yii::t("common","Not specified").'</i>';
                                            } ?>

                                            <!--<li><a href=""><i class="fa fa-tag"></i> tag</a></li>-->
                                        </ul>
                                        </p>
                                    </div>
                                </div>

                            </div>
                        </div >

                    </div>
                </div>
            </div>
    </div>
</div>

<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
    <?php if($type==Event::COLLECTION || $type==Project::COLLECTION){ ?>
    <div class="well well-sm" style="background-color: #eee;border: 1px solid #e6344d!important;">
        <a id="dateTimezone" href="javascript:;" class="tooltips text-dark" data-original-title="" data-toggle="tooltip" data-placement="right">
            <span class="nv-3 text-center">
                <i class="fa fa-clock-o"></i> <?php echo Yii::t("common","When"); ?>
            </span>
        </a>
        <?php if($edit==true || ( $openEdition==true && Yii::app()->session["userId"] != null ) ){?>
        <button class="btn-update-when btn btn-modif-info-G pad-2 pull-right tooltips"
                data-toggle="tooltip" data-placement="top" title="" alt="" data-original-title="<?php echo Yii::t("common","Update date") ?>">
            <i class="fa fa-edit"></i>
        </button>
        <?php } ?>

        <hr class="mt-2" style="margin: 10px 0px; border-top: 1px solid #e6344d!important;">
        <!--
        <span class="ttr-desc-4 txt-vert bold" style="padding-left: 15px; padding-bottom: 10px;">
            <i class="fa fa-calendar"></i> Toutes les semaines
        </span>
        -->
        <div class="history-tl-container">
            <ul class="tl">
                <li class="tl-item contentInformation" id="divStartDate" >
                    <div class="item-title" ><?php echo Yii::t("event","From") ?></div>
                    <div class="item-detail" id="startDateAbout"><?php echo (isset($element["startDate"]) ? $element["startDate"] : "" ); ?></div>
                </li>
                <li class="tl-item contentInformation" id="divEndDate" >
                    <div class="item-title"><?php echo Yii::t("common","To") ?></div>
                    <div class="item-detail" id="endDateAbout"><?php echo (isset($element["endDate"]) ? $element["endDate"] : "" ); ?></div>
                </li>

            </ul>

            <ul class="tl contentInformation" id="divNoDate">
                <span class="ttr-desc-4 txt-vert bold"><i class="fa fa-calendar"></i> Pas de date</span>
            </ul>
        </div>
        <!--
        <span class="ttr-desc-4 txt-vert bold contentInformation" id="divNoDate" style="padding-left: 15px"><i class="fa fa-calendar"></i> Pas de date</span>
        -->
    </div>
    <?php } ?>

    <div class="well no-padding" id="adressesAbout">
        <div class="well no-padding" id="divMapContent" style="border-radius: 3px; background-color: #fff;border: none ; height: 200px; width: 100%; display: block"></div>

        <div  style="padding: 0px 10px 10px 10px; margin-bottom: 10px">
            <span> <i class="fa fa-home"></i>&nbsp;</span>
            <?php if (!empty($element["address"]["codeInsee"]) && ( $edit==true || ( $openEdition==true && Yii::app()->session["userId"] != null ) ) ) { 

            echo '<button class="btn-update-geopos btn btn-modif-desc pad-2 pull-right tooltips"data-toggle="tooltip" data-placement="bottom" title="'.Yii::t("common","Update Locality").'"><span class="couleur-rouge bold"><i class="fa fa-map-marker"></i></span></button>';
            } ?>

            <?php 
                if( ($type == Person::COLLECTION && Preference::showPreference($element, $type, "locality", Yii::app()->session["userId"])) ||  $type!=Person::COLLECTION) {
                    $address = "";
                    $address .= '<span id="detailAddress"> '.
                                    (( @$element["address"]["streetAddress"]) ? 
                                        $element["address"]["streetAddress"]."<br/>": 
                                        ((@$element["address"]["codeInsee"])?"":Yii::t("common","Unknown Locality")));
                    $address .= (( @$element["address"]["postalCode"]) ?
                                     $element["address"]["postalCode"].", " :
                                     "")
                                    ." ".(( @$element["address"]["addressLocality"]) ? 
                                             $element["address"]["addressLocality"] : "") ;
                    $address .= (( @$element["address"]["addressCountry"]) ?
                                     ", ".OpenData::$phCountries[ $element["address"]["addressCountry"] ] 
                                    : "").
                                '</span>';
                    echo $address;
                    if( empty($element["address"]["codeInsee"]) && Yii::app()->session["userId"] == (String) $element["_id"]) { ?>
                        <br><a href="javascript:;" class="cobtn btn btn-danger btn-sm" style="margin: 10px 0px;">
                                <?php echo Yii::t("common", "Connect to your city") ?></a> 
                            <a href="javascript:;" class="whycobtn btn btn-default btn-sm explainLink" style="margin: 10px 0px;" onclick="showDefinition('explainCommunectMe',true)">
                                <?php echo  Yii::t("common", "Why ?") ?></a>
                    <?php }
            }else
                echo '<i>'.Yii::t("common","Not specified").'</i>';
            ?>


            <?php if( !empty($element["addresses"]) ){ ?>
            <div class="labelAbout padding-10">
                <span><i class="fa fa-map"></i></span> <?php echo Yii::t("common", "Others localities") ?>
            </div>
            <div class="col-md-12 col-xs-12 valueAbout no-padding" style="padding-left: 25px !important">
            <?php   foreach ($element["addresses"] as $ix => $p) { ?>           
                <span id="addresses_<?php echo $ix ; ?>">
                    <span>
                    <?php 
                    $address = '<span id="detailAddress_'.$ix.'"> '.
                                    (( @$p["address"]["streetAddress"]) ? 
                                        $p["address"]["streetAddress"]."<br/>": 
                                        ((@$p["address"]["codeInsee"])?"":Yii::t("common","Unknown Locality")));
                    $address .= (( @$p["address"]["postalCode"]) ?
                                     $p["address"]["postalCode"].", " :
                                     "")
                                    ." ".(( @$p["address"]["addressLocality"]) ? 
                                             $p["address"]["addressLocality"] : "") ;
                    $address .= (( @$p["address"]["addressCountry"]) ?
                                     ", ".OpenData::$phCountries[ $p["address"]["addressCountry"] ] 
                                    : "").
                                '</span>';
                    echo $address;
                    ?>



                </span>
                <hr/>
            <?php   } ?>
            </div>
        <?php } ?>

        </div>
        <div class="text-right padding-10">
            <?php if(empty($element["address"]) && $type!=Person::COLLECTION && ($edit==true || ( $openEdition==true && Yii::app()->session["userId"] != null ) )){ ?>
                <b><a href="javascript:;" class="btn-update-geopos btn btn-default letter-blue margin-top-5 addresses">
                    <i class="fa fa-map-marker"></i>
                    <span class="hidden-sm"><?php echo Yii::t("common","Add a primary address") ; ?></span>
                </a></b>
            <?php   }
            //if($type!=Person::COLLECTION && !empty($element["address"]) && ($edit==true || ( $openEdition==true && Yii::app()->session["userId"] != null ) )) { ?>
                <!-- <b><a href='javascript:updateLocalityEntities("<?php //echo count(@$element["addresses"]) ; ?>");' id="btn-add-geopos" class="btn btn-default letter-blue margin-top-5 addresses" style="margin: 10px 0px;">
                    <i class="fa fa-plus" style="margin:0px !important;"></i> 
                    <span class="hidden-sm"><?php //echo Yii::t("common","Add an address"); ?></span>
                </a></b> -->
            <?php //} ?>                        
        </div>
        
        

    </div>

    <?php

    if($type!=Poi::COLLECTION){
    $skype = (!empty($element["socialNetwork"]["skype"])? $element["socialNetwork"]["skype"]:"javascript:;") ;
    $telegram =  (!empty($element["socialNetwork"]["telegram"])? "https://web.telegram.org/#/im?p=@".$element["socialNetwork"]["telegram"]:"javascript:;") ;
    $diaspora =  (!empty($element["socialNetwork"]["diaspora"])? $element["socialNetwork"]["diaspora"]:"javascript:;") ;
    $mastodon =  (!empty($element["socialNetwork"]["mastodon"])? $element["socialNetwork"]["mastodon"]:"javascript:;") ;
    $facebook = (!empty($element["socialNetwork"]["facebook"])? $element["socialNetwork"]["facebook"]:"javascript:;") ;
    $twitter =  (!empty($element["socialNetwork"]["twitter"])? $element["socialNetwork"]["twitter"]:"javascript:;") ;
    $googleplus =  (!empty($element["socialNetwork"]["googleplus"])? $element["socialNetwork"]["googleplus"]:"javascript:;") ;
    $github =  (!empty($element["socialNetwork"]["github"])? $element["socialNetwork"]["github"]:"javascript:;") ;
    $instagram =  (!empty($element["socialNetwork"]["instagram"])? $element["socialNetwork"]["instagram"]:"javascript:;") ;
    ?>
    <div class="panel ibox" style="margin-top: 5px; border-color: #2C3E50; ">
        <div class="panel-heading">
            <span class="nv-3">
                <i class="fa fa-connectdevelop"></i>
                <?php echo Yii::t("common","Socials"); ?>
            </span>
            <?php if($edit==true || ( $openEdition==true && Yii::app()->session["userId"] != null ) ) {?>
            <button class="btn-update-network btn btn-adress pad-2 pull-right tooltips"
                    data-toggle="tooltip" data-placement="top" title="" alt="" data-original-title="<?php echo Yii::t("common","Update network") ?>">
                <i class="fa fa-edit"></i>
            </button>
            <?php } ?>

        </div>
        <div class="panel-body social" style="background-color:#f8f8f8;">
            <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12 desc">
                <div class="col-md-3 col-sm-3 col-xs-3">
                    <span><img src="<?php echo Yii::app()->getModule("co2")->assetsUrl; ?>/images/diaspora_icon.png" width="30px" heigt="30px"></span>
                </div>
                <div class="contentInformation col-md-9 col-sm-9 col-xs-9">
                    <p id="divDiaspora">
                    <?php if ($diaspora != "javascript:;"){ ?>
                        <a href="<?php echo $diaspora ; ?>" target="_blank" id="diasporaAbout" class="socialIcon "><?php echo  $diaspora ; ?></a>
                    <?php } else {
                        echo '<i>'.Yii::t("common","Not specified").'</i>' ;
                    } ?>
                    </p>
                </div>
            </div>

            <div class="col-md-12 col-sm-12 col-xs-12 desc">
                <div class="col-md-3 col-sm-3 col-xs-3">
                    <span><img src="<?php echo Yii::app()->getModule("co2")->assetsUrl; ?>/images/mastodon_icon.png" width="30px" heigt="30px"></i></span>
                </div>
                <div class="contentInformation col-md-9 col-sm-9 col-xs-9">
                    <p id="divMastodon">
                        <?php if ($mastodon != "javascript:;"){ ?>
                            <a href="<?php echo $mastodon ; ?>" target="_blank" id="mastodonAbout" class="socialIcon "><?php echo  $mastodon ; ?></a>
                        <?php } else {
                            echo '<i>'.Yii::t("common","Not specified").'</i>' ;
                        } ?>
                    </p>
                </div>
            </div>

            <div class="col-md-12 col-sm-12 col-xs-12 desc">
                <div class="col-md-3 col-sm-3 col-xs-3">
                    <span class="badge fs fb-color"><i class="fa fa-facebook"></i></span>
                </div>
                <div class="contentInformation col-md-9 col-sm-9 col-xs-9">
                    <p id="divFacebook">
                        <?php if ($facebook != "javascript:;"){ ?>
                            <a href="<?php echo $facebook ; ?>" target="_blank" id="facebookAbout" class="socialIcon "><?php echo  $facebook ; ?></a>
                        <?php } else {
                            echo '<i>'.Yii::t("common","Not specified").'</i>' ;
                        } ?>
                    </p>
                </div>
            </div>

            <div class="col-md-12 col-sm-12 col-xs-12 desc">
                <div class="col-md-3 col-sm-3 col-xs-3">
                    <span class="badge fs twitter-color"><i class="fa fa-twitter"></i></span>
                </div>
                <div class="contentInformation col-md-9 col-sm-9 col-xs-9">
                    <p id="divTwitter">
                        <?php if ($twitter != "javascript:;"){ ?>
                            <a href="<?php echo $twitter ; ?>" target="_blank" id="twitterAbout" class="socialIcon" ><?php echo $twitter ; ?></a>
                        <?php } else {
                            echo '<i>'.Yii::t("common","Not specified").'</i>' ;
                        } ?>
                        </span>
                    </p>
                </div>
            </div>

            <div class="col-md-12 col-sm-12 col-xs-12 desc">
                <div class="col-md-3 col-sm-3 col-xs-3">
                    <span class="badge fs instagram-color"><i class="fa fa-instagram"></i></span>
                </div>
                <div class="contentInformation col-md-9 col-sm-9 col-xs-9">
                    <p id="divInstagram">
                        <?php if ($instagram != "javascript:;"){ ?>
                            <a href="<?php echo $instagram ; ?>" target="_blank" id="instagramAbout" class="socialIcon" ><?php echo $instagram ; ?></a>
                        <?php } else {
                            echo '<i>'.Yii::t("common","Not specified").'</i>' ;
                        } ?>
                    </p>
                </div>
            </div>

            <div class="col-md-12 col-sm-12 col-xs-12 desc">
                <div class="col-md-3 col-sm-3 col-xs-3">
                    <span class="badge fs git-color"><i class="fa fa-github"></i></span>
                </div>
                <div class="contentInformation col-md-9 col-sm-9 col-xs-9">
                    <p id="divGithub">
                        <?php if ($github != "javascript:;"){ ?>
                            <a href="<?php echo $github ; ?>" target="_blank" id="githubAbout" class="socialIcon" ><?php echo $github  ; ?></a>
                        <?php } else {
                            echo '<i>'.Yii::t("common","Not specified").'</i>' ;
                        } ?>
                    </p>
                </div>
            </div>

                <?php if($type==Person::COLLECTION){ ?>
                <div class="col-md-12 col-sm-12 col-xs-12 desc">
                    <div class="col-md-3 col-sm-3 col-xs-3">
                        <span class="badge fs twitter-color"><i class="fa fa-telegram"></i></span>
                    </div>
                    <div class="contentInformation col-md-9 col-sm-9 col-xs-9">
                        <p id="divTelegram">
                            <?php if ($telegram != "javascript:;"){ ?>
                                <a href="<?php echo $telegram ; ?>" target="_blank" id="telegramAbout" class="socialIcon" ><?php echo $telegram ; ?></a>
                            <?php } else {
                                echo '<i>'.Yii::t("common","Not specified").'</i>' ;
                            } ?>
                        </p>
                    </div>
                </div>
                <?php } ?>

    </div>
    </div>
</div>
    <?php } ?>

</div>
<?php echo $this->renderPartial('../pod/whycommunexion',array()); ?>
<script type="text/javascript">
    //Affichage de la map
    $("#divMapContent").show(function () {
        afficheMap();
    });
    var mapAbout = {};
    function afficheMap(){
            mylog.log("afficheMap");

            //alert(contextData.geoPosition.coordinates);
            latitudeMap =0;
            longitudeMap =0;
            if(typeof contextData.address != "undefined"){
                    latitudeMap=contextData.geoPosition.coordinates[0];
                    longitudeMap=contextData.geoPosition.coordinates[1];
            }
            var paramsMapContent = {
                container : "divMapContent",
                latLon : [latitudeMap,longitudeMap],
                activeCluster : false,
                zoom : 16,
                activePopUp : false
            };

        
            mapAbout = mapObj.init(paramsMapContent);

            var composantMap = contextData.type;

            var paramsPointeur = {
                    elt : {
                        id : 0, 
                        type : composantMap,
                        geo : {
                            latitude :  paramsMapContent.latLon[0],
                            longitude : paramsMapContent.latLon[1],
                        }
                    },
                    opt : {
                        draggable: true
                    },
                    center : true
                };

                mapAbout.addMarker(paramsPointeur);
                

    };


    //var paramsPointeur[contextId] = contextData;
    var formatDateView = "DD MMMM YYYY à HH:mm" ;
    var formatDatedynForm = "DD/MM/YYYY HH:mm" ;

    jQuery(document).ready(function() {
        bindDynFormEditable();
        initDate();
        inintDescs();
        //changeHiddenFields();
        bindAboutPodElement();
        bindExplainLinks();

        $("#small_profil").html($("#menu-name").html());
        $("#menu-name").html("");

        $(".cobtn").click(function () {
            communecterUser();
        });

        $(".btn-update-geopos").click(function(){
            updateLocalityEntities();
        });

        $("#btn-add-geopos").click(function(){
            updateLocalityEntities();
        });

        $("#btn-update-organizer").click(function(){
            updateOrganizer();
        });
        $("#btn-add-organizer").click(function(){
            updateOrganizer();
        });

        $("#btn-remove-geopos").click(function(){
            removeAddress();
        });

        $("#btn-update-geopos-admin").click(function(){
            findGeoPosByAddress();
        });


        //buildQRCode(contextData.type,contextData.id.$id);

    });
    function inintDescs() {
        mylog.log("inintDescs");
        if(canEdit == true || openEdition== true)
            descHtmlToMarkdown();
        mylog.log("after");
        mylog.log("inintDescs", $("#descriptionAbout").html());
        var descHtml = "<i>"+trad.notSpecified+"</i>";
        if($("#descriptionAbout").html().length > 0){
            descHtml = dataHelper.markdownToHtml($("#descriptionAbout").html()) ;
        }
        $("#descriptionAbout").html(descHtml);
        //$("#descProfilsocial").html(descHtml);
        mylog.log("descHtml", descHtml);
    }

    function initDate() {//DD/mm/YYYY hh:mm

        formatDateView = "DD MMMM YYYY à HH:mm" ;
        formatDatedynForm = "DD/MM/YYYY HH:mm" ;
        if(typeof contextData.recurrency != "undefined" && contextData.recurrency){
            $("#divNoDate").html(directory.initOpeningHours(contextData, true));
        }
        if( notNull(contextData) && typeof contextData.startDate != "undefined" && contextData.startDate != "" ){
            $("#divStartDate").removeClass("hidden");
            $("#divNoDate").addClass("hidden");
        }
        else{
            $("#divStartDate").addClass("hidden");
            $("#divNoDate").removeClass("hidden");
        }

        if(notNull(contextData) && typeof contextData.endDate != "undefined" && contextData.endDate != "" )
            $("#divEndDate").removeClass("hidden");
        else
            $("#divEndDate").addClass("hidden");
        mylog.log("formatDateView", formatDateView);
        //if($("#startDateAbout").html() != "")
        if(!$("#divStartDate").hasClass("hidden"))
            $("#startDateAbout").html(moment(contextData.startDateDB).local().locale(mainLanguage).format(formatDateView));
        if(!$("#divEndDate").hasClass("hidden"))
            $("#endDateAbout").html(moment( contextData.endDateDB).local().locale(mainLanguage).format(formatDateView));

        if($("#birthDate").html() != "")
            $("#birthDate").html(moment($("#birthDate").html()).local().locale(mainLanguage).format("DD/MM/YYYY"));
        $('#dateTimezone').attr('data-original-title', "Fuseau horaire : GMT " + moment().local().format("Z"));
    }

    function AddReadMore() {
        // Cette limite souhaitez afficher En savoir plus.
        var carLmt = 400;
        // Texte à afficher lorsque le texte est réduit
        var readMoreTxt = " ... Lire la suite";
        // Texte à afficher lorsque le texte est développé
        var readLessTxt = " Lire moins";


        // manipulez la partie HTML pour afficher En savoir plus
        $(".addReadMore").each(function() {
            if ($(this).find(".firstSec").length)
                return;

            var allstr = $(this).text();
            if (allstr.length > carLmt) {
                var firstSet = allstr.substring(0, carLmt);
                var secdHalf = allstr.substring(carLmt, allstr.length);
                var strtoadd = firstSet + "<span class='SecSec'>" + secdHalf + "</span><span class='readMore'  title='Click to Show More'>" + readMoreTxt + "</span><span class='readLess' title='Click to Show Less'>" + readLessTxt + "</span>";
                $(this).html(strtoadd);
            }

        });
        // En savoir plus et en lire moins Cliquez sur Liaison d'événement
        $(document).on("click", ".readMore,.readLess", function() {
            $(this).closest(".addReadMore").toggleClass("showlesscontent showmorecontent");
        });
    }
    $(function() {
        // Appel de la fonction après le chargement de la page
        AddReadMore();
    });
</script>
</script>

