<style type="text/css">
    .blockFontPreview {
        font-size: 14px;
        color: #777;
    }
</style>
<?php
    $cssAnsScriptFilesModule = array(
        '/js/default/preview.js'
    );
    HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, $this->module->getParentAssetsUrl());
    $previewConfig = @$this->appConfig["preview"];
    $auth = Authorisation::canEditItem(@Yii::app()->session["userId"], $type, $element["_id"]) ? true : false;
    $iconColor = (isset($element["type"]) && Element::getColorIcon($element["type"]) !== false) ? Element::getColorIcon($element["type"]) : Element::getColorIcon($element["collection"]);
    $aapUrl = !empty(Yii::app()->getRequest()->getBaseUrl(true)) && isset($element["parentSlug"]) && isset($element["parentConfigSlug"]) && isset($element["type"]) && $element["type"] == "aap" ? Yii::app()->getRequest()->getBaseUrl(true)."/costum/co/index/slug/".$element["parentConfigSlug"]."#proposalAap.context.".$element["parentSlug"].".formid.".(string) $element["_id"] : "";
    function removeSlash($url) {
        $urlSansSlash = ltrim($url, '/');
        return $urlSansSlash;
    }

?>
<div id="preview-elt-<?php echo $type ?>-<?php echo (string)$element["_id"] ?>" class="col-xs-12 no-padding">
    <div class="col-xs-12 padding-10 toolsMenu">
        <?php if (isset($previewConfig["toolBar"]["close"]) && $previewConfig["toolBar"]["close"]) { ?>
            <button class="btn btn-default pull-right btn-close-preview">
                <i class="fa fa-times"></i>
            </button>
        <?php }
            if (isset($previewConfig["toolBar"]["goToItem"]) && $previewConfig["toolBar"]["goToItem"]) { ?>
                <a href="<?= empty($aapUrl) ? '#@'.@$element["slug"] : $aapUrl ?>"
                   <?= !empty($aapUrl) ? 'target="_blank"' : "" ?> class="<?= empty($aapUrl) ? 'lbh' : '' ?> btn btn-primary pull-right margin-right-10"><?php echo Yii::t("common", "Go to the item") ?></a>
            <?php }
            if (isset($previewConfig["toolBar"]["edit"]) && $previewConfig["toolBar"]["edit"] && $auth) { ?>
                <button class="btn btn-default pull-right margin-right-10 btn-edit-preview" data-type="<?php echo $type ?>"
                        data-id="<?php echo $element["_id"] ?>" data-subtype="<?php echo $element["type"] ?>">
                    <i class="fa fa-pencil"></i>
                </button>
            <?php }
            if ($type == Event::COLLECTION) : ?>
                <div id="participate-to-<?= $element["_id"] ?>" class="pull-right margin-right-10"></div>
                <?php if ($auth): ?>
                    <div id="delete-element" class="pull-right margin-right-10" data-id="<?= $element["_id"] ?>">
                        <button class="btn btn-default">
                            <span class="fa fa-trash"></span>
                        </button>
                    </div>
                <?php endif; ?>
            <?php endif ?>
    </div>
    <div class="container-preview col-xs-12 no-padding margin-bottom-20" style="overflow-y: scroll">
        <?php
            if (isset($previewConfig["banner"]) && !empty($previewConfig["banner"])) {
                if (isset($element["profilBannerUrl"]) && !empty($element["profilBannerUrl"])) {
                    $url = Yii::app()->createUrl('/' . $element["profilBannerUrl"]);
                } else {
                    if (
                        !empty($this->costum) && isset($this->costum["htmlConstruct"])
                        && isset($this->costum["htmlConstruct"]["element"])
                        && isset($this->costum["htmlConstruct"]["element"]["banner"]["img"])
                    )
                        $url = Yii::app()->getModule("costum")->assetsUrl . $this->costum["htmlConstruct"]["element"]["banner"]["img"];
                    else
                        $url = Yii::app()->theme->baseUrl . '/assets/img/background-onepage/connexion-lines.jpg';
                }
                ?>
                <div class="col-xs-12 no-padding" id="col-banner" style="border-top: 1px solid #e7e7e7;border-bottom: 1px solid #e7e7e7;">
                    <?php
                        if (isset($previewConfig["banner"]["edit"]) && $previewConfig["banner"]["edit"] && $auth) { ?>
                            <?php echo $this->renderPartial("co2.views.element.modalBanner", array(
                                "type"            => $type,
                                "id"              => (string)$element["_id"],
                                "name"            => $element["name"],
                                "edit"            => $canEdit,
                                "openEdition"     => $openEdition,
                                "profilBannerUrl" => @$element["profilBannerUrl"]
                            ));
                        } ?>
                    <div id="contentBanner" class="col-xs-12 no-padding">
                        <?php $imgHtml = '<img class="col-md-12 col-sm-12 col-xs-12 no-padding img-responsive"
						alt="' . Yii::t("common", "Banner") . '" 
						src="' . $url . '">';
                            if (@$element["profilRealBannerUrl"] && !empty($element["profilRealBannerUrl"])) {
                                $imgHtml = '<a href="' . Yii::app()->createUrl('/' . $element["profilRealBannerUrl"]) . '"
									class="thumb-info"  
									data-title="' . Yii::t("common", "Cover image of") . " " . $element["name"] . '"
									data-lightbox="all">' .
                                    $imgHtml .
                                    '</a>';
                            }
                            echo $imgHtml;
                        ?>
                    </div>
                </div>
                <?php if (isset($previewConfig["banner"]["imgProfil"]) && $previewConfig["banner"]["imgProfil"]) { ?>
                    <div class="content-img-profil-preview col-xs-8 col-xs-offset-2 col-md-6 col-md-offset-3 col-lg-4 col-lg-offset-4">
                        <?php if (isset($element["profilMediumImageUrl"]) && !empty($element["profilMediumImageUrl"])) { ?>
                            <a href="<?php echo Yii::app()->createUrl('/' . $element["profilImageUrl"]) ?>" class="thumb-info"
                               data-title="<?php echo Yii::t("common", "Profil image of") . " " . $element["name"] ?>" data-lightbox="all">
                                <img class="img-responsive" onload='eltImgPreview(this)'
                                     style="box-shadow: 0px 0px 8px 2px rgba(0,0,0,0.5);margin: auto;max-height: 300px;"
                                     src="<?php echo Yii::app()->createUrl('/' . $element["profilMediumImageUrl"]) ?>"/>
                            </a>
                        <?php } else { ?>
                            <img class="img-responsive shadow2 thumbnail" onload='eltImgPreview(this)'
                                 style="margin: auto;box-shadow: 0px 0px 8px 2px rgba(0,0,0,0.5);"
                                 src="<?php echo $this->module->assetsUrl ?>/images/thumb/default_<?php echo $type ?>.png"/>
                        <?php } ?>
                    </div>
                <?php }
            } ?>
        <div class="preview-element-info col-xs-12">
            <?php
                if (isset($previewConfig["body"])) {
                if (is_string($previewConfig["body"])) {
                echo $this->renderPartial($previewConfig["body"], array("type" => $type, "element" => $element, "edit" => $auth));
            } else {
                if (isset($previewConfig["body"]["name"]) && $previewConfig["body"]["name"]) { ?>
                    <h3 class="text-center"><?php echo $element["name"] ?></h3>
                <?php }
                if (isset($previewConfig["body"]["shortDescription"]) && $previewConfig["body"]["shortDescription"]) { ?>
                    <div class="col-xs-10 col-xs-offset-1 margin-top-20">
							<span class="col-xs-12 text-center"
                                  id="shortDescriptionHeader"><?php echo ucfirst(substr(trim(@$element["shortDescription"]), 0, 180)); ?>
							</span>
                    </div>
                <?php }
                if (isset($previewConfig["body"]["type"]) && $previewConfig["body"]["type"]) { ?>
                    <span class="col-xs-12 text-center blockFontPreview margin-top-20">
							<span class="text-<?php echo $iconColor; ?> uppercase"><?php echo Yii::t("common", Element::getControlerByCollection($type)); ?></span>
							<?php if (($type == Organization::COLLECTION || $type == Event::COLLECTION) && @$element["type"]) {
                                if ($type == Organization::COLLECTION)
                                    $typesList = Organization::$types;
                                else
                                    $typesList = Event::$types;
                                $typeCat = (isset($typesList[$element["type"]])) ? $typesList[$element["type"]] : $element["type"];
                                ?>
                                <i class="fa fa-x fa-angle-right margin-left-10"></i>
                                <span class="margin-left-10"><?php
                                        if (isset($typesList[$element["type"]]))
                                            echo Yii::t("category", $typesList[$element["type"]]);
                                    ?></span>
                            <?php } ?>

						</span>
                <?php }
                if (isset($previewConfig["body"]["locality"]) && $previewConfig["body"]["locality"] && !empty($element["address"]["addressLocality"])) { ?>
                    <div class="header-address col-xs-12 text-center blockFontPreview margin-top-20">
                        <i class="fa fa-map-marker"></i>
                        <?php
                            echo !empty($element["address"]["streetAddress"]) ? $element["address"]["streetAddress"] . ", " : "";
                            echo !empty($element["address"]["postalCode"]) ?
                                $element["address"]["postalCode"] . ", " : "";
                            echo $element["address"]["addressLocality"];
                        ?>
                    </div>
                <?php }
                if (isset($previewConfig["body"]["tags"]) && $previewConfig["body"]["tags"]) { ?>
                    <div class="header-tags col-xs-12 text-center blockFontPreview margin-top-20">
                        <?php
                            if (@$element["tags"]) {
                                foreach ($element["tags"] as $key => $tag) { ?>
                                    <a href="javascript:;" class="letter-red" style="vertical-align: top;">#<?php echo $tag; ?></a>
                                <?php }
                            } ?>
                    </div>
                <?php } ?>
        </div>
    <?php if (isset($previewConfig["body"]["eventInfos"]) && $previewConfig["body"]["eventInfos"] && $type == Event::COLLECTION) { ?>
        <div class="event-infos-header text-center margin-top-10 col-xs-12 blockFontPreview margin-top-20"
             style="font-size: 14px;font-weight: none;"></div>
    <?php }
        if (isset($previewConfig["body"]["description"]) && $previewConfig["body"]["description"]) { ?>
            <div class="col-xs-10 col-xs-offset-1 margin-top-20">
				<span class="pull-left text-center" id="descriptionHeader"><?php echo ucfirst(substr(trim(@$element["description"]), 0, 180)); ?>
				</span>
            </div>
        <?php }
        if (isset($previewConfig["body"]["url"]) && $previewConfig["body"]["url"] && isset($element["url"])) { ?>
            <div class="col-xs-10 col-xs-offset-1 margin-top-20 text-center">
                <?php $scheme = ((!preg_match("~^(?:f|ht)tps?://~i", $element["url"])) ? 'http://' : ""); ?>
                <a href="<?php echo $scheme . $element['url'] ?>" target="_blank" id="urlWebAbout"
                   style="cursor:pointer;"><?php echo $element["url"] ?></a>
            </div>
        <?php }
        if (isset($previewConfig["body"]["shareButton"]) && $previewConfig["body"]["shareButton"]) { ?>
            <div class="social-share-button-preview col-xs-12 text-center margin-top-20 margin-bottom-20"></div>
        <?php }

        if (isset($element["collection"]) && in_array($element["collection"], ["citoyens", "organizations", "projects"])) { ?>
            <div class="graph-btn-icon col-xs-12 text-center margin-top-20 margin-bottom-20">
                <a class="lbh btn margin-right-10" href="#@<?= @$element["slug"] ?>.view.conode">
                    <img src="<?= Yii::app()->getModule("co2")->getAssetsUrl() ?>/images/graph/community.png" width="50"> Ouvrir le graph communauté de <?= $element["name"] ?>
                </a>
            </div>
        <?php }
        }
        } ?>
    </div>
</div>

<script type="text/javascript">
    function get_actors(types = ['organizer', 'links.attendees', 'creator', 'links.creator', 'links.organizer', 'organizerName']) {
        var url = `${baseUrl}/costum/coevent/get_events/request/actors/event/<?= (string)$element['_id'] ?>`;
        var parameter = {
            types,
            parent_only : true
        }
        return new Promise(function (resolve) {
            ajaxPost(null, url, parameter, function (events) {
                resolve(events)
            }, null, 'json')
        });
    }

    function get_subevents(type) {
        var url = type ? `${baseUrl}/costum/coevent/get_events/request/subevents/event/${costum['contextId']}/filter/${type}` : `${baseUrl}/costum/coevent/get_events/request/subevents/event/${costum['contextId']}`;
        return new Promise(function (resolve) {
            ajaxPost(null, url, null, function (events) {
                resolve(events)
            }, null, 'json')
        });
    }

    function event_participation(dom) {
        var that = dom;
        let eventData = JSON.parse(JSON.stringify(<?= json_encode($element) ?>));
        thisElement = $(that);
        let isExpAttFull = (typeof eventData != "undefined" && typeof eventData.links.attendees != "undefined" && typeof eventData.expectedAttendees != "undefined" && Object.keys(eventData.links.attendees).length == eventData.expectedAttendees);

        if (isExpAttFull == false) {
            if (userId) {
                subscribeToEvent(thisElement);
            } else {
                $("#modalLogin").on('show.bs.modal', function (e) {
                    if ($("#infoBL").length == 0) {
                        $("#modalLogin .modal-content .container div.row").append(`<div class="col-xs-12" id="infoBL">
                                Vous devez être connecté(e) pour vous inscrire à l’événement. Vous n’avez pas encore de compte ? 
                                <a href="javascript:;" class="bold" data-toggle="modal" data-target="#modalRegister"> Créez-en un </a>
                            </div>`);
                    }
                });
                $("#modalLogin").on('hide.bs.modal', function (e) {
                    $("#infoBL").remove();
                });
                toastr.info("Vous devez être connecté(e) pour vous inscrire à l’événement");
                Login.openLogin();
            }
        } else {
            if (userId) {
                subscribeToEvent(thisElement);
            }
        }

        if (typeof eventData != "undefined" && isExpAttFull && thisElement.data("ownerlink") == "follow") {
            bootbox.dialog({
                message : `<div class="alert-white text-center"><br>
                                <strong>Désolé ! Il n'y a plus de place. Le nombre maximal de participants a été atteint</strong>
                                <br><br>
                                <button class="btn btn-default bootbox-close-button">OK</button>
                            </div>`
            });
        }
    }

    function subscribeToEvent(eventSource, theUserId = null) {
        var labelLink = "";
        var parentId = eventSource.data("id");
        var parentType = eventSource.data("type");
        var childId = (theUserId) ? theUserId : userId;
        var childType = "citoyens";
        var name = eventSource.data("name");
        var id = eventSource.data("id");
        //traduction du type pour le floopDrawer
        eventSource.html("<i class='fa fa-spin fa-circle-o-notch text-azure'></i>");
        var connectType = (parentType == "events") ? "connect" : "follow";

        if (eventSource.data("ownerlink") == "follow") {
            callback = function () {
                labelLink = (parentType == "events") ? "DÉJÀ PARTICIPANT(E)" : trad.alreadyFollow;
                if (eventSource.hasClass("btn-add-to-directory")) {
                    labelLink = "";
                }
                $(`[data-id=${id}]`).each(function () {
                    $(this).html("<small><i class='fa fa-unlink'></i> " + labelLink.toUpperCase() + "</small>");
                    $(this).data("ownerlink", "unfollow");
                    $(this).data("original-title", labelLink);
                });
            }
            if (parentType == "events") {
                links.connectAjax(parentType, parentId, childId, childType, connectType, null, callback);
            } else {
                links.follow(parentType, parentId, childId, childType, callback);
            }
        } else {
            //eventSource.data("ownerlink")=="unfollow"
            connectType = (parentType == "events") ? "attendees" : "followers";
            callback = function () {
                labelLink = (parentType == "events") ? "PARTICIPER" : "DÉJÀ PARTICIPANT(E)";
                if (eventSource.hasClass("btn-add-to-directory")) {
                    labelLink = "";
                }
                $(`[data-id=${id}]`).each(function () {
                    $(this).html("<small><i class='fa fa-chain'></i> " + labelLink.toUpperCase() + "</small>");
                    $(this).data("ownerlink", "follow");
                    $(this).data("original-title", labelLink);
                    $(this).removeClass("text-white");
                });
            };
            links.disconnectAjax(parentType, parentId, childId, childType, connectType, null, callback);
        }
    }

    var eltPreview = <?php echo json_encode($element); ?>;
    var typePreview = <?php echo json_encode($type); ?>;
    var idPreview = <?php echo json_encode($id); ?>;
    var socialBarConfig = <?php echo json_encode(@$previewConfig["body"]["shareButton"]); ?>;
    jQuery(document).ready(function () {
        $('#delete-element').tooltip({
            placement : 'bottom',
            title : trad.delete
        });
        var str = directory.getDateFormated(eltPreview, null, true);
        $(".event-infos-header").html(str);
        $(".social-share-button-preview").html(directory.socialBarHtml({
            "socialBarConfig" : {
                "btnList" : socialBarConfig,
                "btnSize" : 40
            },
            "type" : typePreview,
            "id" : idPreview
        }));
        //coInterface.bindLBHLinks();
        //resizeContainer();
        directory.bindBtnElement();

        // bouton de participation
        get_actors(['links.attendees']).then(function (db_communities) {
            var element = JSON.parse(JSON.stringify(<?= json_encode($element) ?>));
            var id = `<?= (string)$element["_id"] ?>`;
            var communities = db_communities.map(function (map) {
                return map['id']
            });
            var is_following = false;
            var tip = trad['interested'];
            var action_connect = 'follow';
            var label = 'PARTICIPER';
            var icon = 'chain';
            if (userConnected?. ['_id']?. ['$id']) {
                is_following = communities.includes(userConnected?. ['_id']?. ['$id']);
            }

            if (is_following) {
                action_connect = 'unfollow';
                icon = 'unlink';
                tip = "Je ne veux plus participer";
                label = 'DÉJÀ PARTICIPANT(E)'
            }
            $(`#participate-to-${element['_id']['$id']}`).html(`
				<button class="btn btn-default tooltips" onclick="event_participation(this)"
					data-toggle="tooltip" data-placement="left" data-original-title="${tip}"
					data-ownerlink="${action_connect}" data-id="${id}" data-type="${element['collection']}" data-name="${element['name']}"
					data-isFollowed="${is_following}"><i class="fa fa-${icon}"></i> ${label}</button>
				`);
        });
        $('#delete-element').on('click', function () {
            var self = $(this);
            $.confirm({
                title : trad.delete,
                content : 'Procéder à la suppression de l\'événement',
                buttons : {
                    confirm : {
                        text : 'Confirmer',
                        action() {
                            var url = baseUrl + '/' + moduleId + '/element/delete/id/' + self.data('id') + '/type/events';
                            var request_params = {reasons : 'none'};
                            ajaxPost(null, url, request_params, function (__data) {
                                if (__data.result) {
                                    toastr.success(__data.msg);
                                    $(document.body).trigger('co.event.delete');
                                    urlCtrl.closePreview();
                                    urlCtrl.loadByHash(location.hash);
                                } else {toastr.error(__data.msg);}
                            });
                        }
                    },
                    cancel : {
                        text : 'Annuler'
                    }
                }
            })
        })
    });
</script>