<?php
	HtmlHelper::registerCssAndScriptsFiles(array( 
	'/js/form.js',
	'/css/form.css',
	), Yii::app()->getModule( Survey::MODULE )->getAssetsUrl() );

	$cssAndScriptFilesModule = array(
	    '/js/admin/panel.js',
	    '/js/admin/admin_directory.js',

	);
  	HtmlHelper::registerCssAndScriptsFiles($cssAndScriptFilesModule, $this->module->assetsUrl);

  // 	var data={
  //   title : "Promesses de don",
  //   //types : ["crowdfunding"],
  //   table : {
  //     name: {
  //       name : "Nom"
  //     },
  //     amount : {
  //       name : "Montant",
  //       sum : true
  //     },
  //     type : {
  //       name : "type"
  //     },
  //     validateField :{
  //       field : "type",
  //       value : "donation"
  //     }

  //   },  
  //   actions:{
  //      delete : true,
  //      validatePledge : true
  //   },
  //   paramsFilter : {
  //       container : "#filterContainer",
  //       defaults : {
  //         types : [ "crowdfunding" ],
  //         //type : "pledge"
  //         filters : {
  //           type : "pledge"
  //         }
  //       }
  //     }
      
  // };
  // ajaxPost('#central-container', baseUrl+'/'+moduleId+'/admin/directory/type/'+contextData.type+'/id/'+contextData.id, data, function(){},"html");

	$elementParams=@$this->appConfig["element"];
	$cssAnsScriptFilesModule=	array( 
		'/vendor/colorpicker/js/colorpicker.js',
		'/vendor/colorpicker/css/colorpicker.css',
		'/css/default/directory.css',
		'/css/default/settings.css',	
		'/css/profilSocial.css',
		'/css/calendar.css',
	);
	if(isset($elementParams["tplCss"])){
		foreach ($elementParams["tplCss"] as $ressource){
			array_push($cssAnsScriptFilesModule, '/css/'.$ressource);
		}
	}
	
	HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, Yii::app()->theme->baseUrl. '/assets');
	//var_dump($this->module->assetsUrl);
 	$cssAnsScriptFilesModule = array(
    	'/js/default/calendar.js',
	    '/js/default/profilSocial.js',
	    '/js/default/editInPlace.js',
	    '/js/default/settings.js',
		'/js/default/activitypub.js'
	);
	
  	HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, $this->module->assetsUrl);

	$cssAnsScriptFilesTheme = array(
		// SHOWDOWN
		//'/plugins/showdown/showdown.min.js',
		//MARKDOWN
		//'/plugins/to-markdown/to-markdown.js',
		'/plugins/jquery.qrcode/jquery-qrcode.min.js',
		'/plugins/fullcalendar/fullcalendar/fullcalendar.min.js',
        '/plugins/fullcalendar/fullcalendar/fullcalendar.css', 
        '/plugins/fullcalendar/fullcalendar/locale/'.Yii::app()->language.'.js',
        "/plugins/d3/d3.js",
        "/plugins/d3/d3-flextree.js",
        "/plugins/d3/view.mindmap.js",
        "/plugins/d3/view.mindmap.css",
        '/plugins/bootstrap-toggle/css/bootstrap-toggle.min.css',
		'/plugins/bootstrap-toggle/js/bootstrap-toggle.min.js' , 
		'/plugins/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css',
	  	'/plugins/bootstrap-switch/dist/js/bootstrap-switch.min.js' 
	);

	HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesTheme, Yii::app()->request->baseUrl);
		
	if(isset($this->costum)){
		$slugContext=(@$this->costum["assetsSlug"]) ? $this->costum["assetsSlug"] : $this->costum["slug"];
		$cssCostumFile=(!empty($this->costum) && 
                        !empty($this->costum['htmlConstruct']) && 
                        !empty($this->costum['htmlConstruct']['element']) &&
                        !empty($this->costum['htmlConstruct']['element']['css'])) ? $this->costum['htmlConstruct']['element']['css'] : "pageProfil.css"; 
		$jsCostumFile=(!empty($this->costum) && 
                        !empty($this->costum['htmlConstruct']) && 
                        !empty($this->costum['htmlConstruct']['element']) &&
                        !empty($this->costum['htmlConstruct']['element']['js'])) ? $this->costum['htmlConstruct']['element']['js'] : "pageProfil.js"; 
 
		$cssJsCostum=array();
		if(isset($elementParams["js"]))
			array_push($cssJsCostum, '/js/'.$slugContext.'/'.$jsCostumFile);
		if(isset($elementParams["css"]))
			array_push($cssJsCostum, '/css/'.$slugContext.'/'.$cssCostumFile);
		if(!empty($cssJsCostum))
			HtmlHelper::registerCssAndScriptsFiles($cssJsCostum, Yii::app()->getModule( "costum" )->getAssetsUrl());
	}

    if(empty($element["_id"])){ 
    	$what=(@$element["type"]) ? "the ".Element::getControlerByCollection($element["type"]) : "the element";
    	?>
    	<script type="text/javascript">
    		urlCtrl.loadByHash("");
			bootbox.dialog({message:"<div class='alert-danger text-center'><strong><?php echo Yii::t("common","{what}, that you are looking for, has been deleted or doesn't exist", array("{what}"=>ucfirst(Yii::t("common", $what)))) ?></strong></div>"});
		</script>
    <?php 
		exit;
	} 
    if(@Yii::app()->params["front"]) $front = Yii::app()->params["front"];
    $layoutPath = 'webroot.themes.'.Yii::app()->theme->name.'.views.layouts.';
	$auth = Authorisation::canParticipate(Yii::app()->session['userId'], $type, (string)$element["_id"]);
	$element["defaultImg"] = $this->module->assetsUrl.'/images/thumbnail-default.jpg';
	$thumbAuthor =  (isset($element['profilThumbImageUrl']) && !empty($element['profilThumbImageUrl'])) ? Yii::app()->createUrl('/'.@$element['profilThumbImageUrl']) 
                      : $this->module->assetsUrl.'/images/thumbnail-default.jpg';
    $categoryClass="";
    if(isset($element["category"]))
    	$categoryClass=(is_array($element["category"])) ? implode(" ", $element["category"]) : $element["category"];
	?>

<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding <?php echo $element["collection"] ?>-profil <?php echo $element["collection"] ?>-item <?php echo $categoryClass; ?> pageElement">	
    <!-- Header Banner in element -->
    <?php
    echo $this->renderPartial('co2.views.element.header',
		array(	"element"=>$element,
				"badgesToShow" => $badgesToShow,
				"pageConfig"=>$elementParams,
				"linksBtn"=>$linksBtn,
				"invitedMe"=>@$invitedMe,
				"edit" => @$canEdit,
				"openEdition" => @$openEdition) 
		);
	?>
    <!-- END HEADER ELEMENT BANNER -->

    <!-- ///////////////////////// MENUTOP //////////////////////////////////
			//// Containing image profil & group of button /////////// -->
    <?php if(isset($elementParams["menuTop"]) && !empty($elementParams["menuTop"])){
    		 echo $this->renderPartial('co2.views.element.menus.construct',array(
                "params"=>$elementParams["menuTop"],
                "key"=>"top",
	            "edit"=> @$canEdit,
	            "element"=>$element,
	            "linksBtn"=>$linksBtn,
				"invitedMe"=>@$invitedMe,
	            "openEdition"=>@$openEdition                
            ));
        }
     ?>
   <!-- ///////////// END MENU TOP //////////////--> 
	<?php if(isset($elementParams["menuLeft"]) && !empty($elementParams["menuLeft"])){
		echo $this->renderPartial('co2.views.element.menus.construct',array(
                "params"=>$elementParams["menuLeft"],
                "key"=>"top",
	            "edit"=> @$canEdit,
	            "element"=>$element,
	            "linksBtn"=>$linksBtn,
				"invitedMe"=>@$invitedMe,
	            "openEdition"=>@$openEdition                
            )); ?>
	<?php } ?>
	<div class="<?php echo $elementParams["containerClass"]["centralSection"] ?> central-section">
		<div class="col-xs-12 padding-50 links-main-menu" id="div-select-create">
			<div class="col-md-12 col-sm-12 col-xs-12 padding-15 shadow2 bg-white ">
		       
		       	<h4 class="text-center margin-top-15" style="">
			       	<img class="img-circle" src="<?php echo $thumbAuthor; ?>" height=30 width=30 style="margin-top:-10px;">
			       	<a class="btn btn-link pull-right text-dark" id="btn-close-select-create" style="margin-top:-10px;">
			       		<i class="fa fa-times-circle fa-2x"></i>
			       	</a>
			       	<span class="name-header"><?php echo @$element["name"]; ?></span>
			       	<br>
			       	<i class="fa fa-plus-circle"></i> <?php echo Yii::t("form","Create content linked to this page") ?>
			       	<br>
			       	<small><?php echo Yii::t("form","What kind of content will you create ?") ?></small>
		       	</h4>
		        <div class="col-md-12 col-sm-12 col-xs-12 elementCreateButton"><hr></div>
		    </div>
	    </div>

	    <div class="col-xs-12 <?php echo @$elementParams["containerClass"]["centralSectionSub"] ?>" id="central-container" data-active-view="">
		</div>
	</div>
</div>	

<?php 
	//render of modal for coop spaces 
	$params = array(  "element" => @$element, 
                        "type" => @$element["collection"], 
                        "edit" => @$canEdit,
                        "thumbAuthor"=>@$thumbAuthor,
                        "openEdition" => $openEdition
                    );

	echo $this->renderPartial('dda.views.co.pod.modals', $params );

	echo $this->renderPartial('../element/confirmDeleteModal', array("id" =>(String)$element["_id"], "type"=>$element["collection"]));

	if (@$element["status"] == "deletePending" 
		&& Authorisation::isElementAdmin((String)$element["_id"], $element["collection"], Yii::app()->session["userId"])) 
		echo $this->renderPartial('co2.views.element.confirmDeletePendingModal', array(	"element"=>$element));


	echo $this->renderPartial('co2.views.pod.confidentiality',
			array(  "element" => @$element, 
					"type" => @$element["collection"], 
					"edit" => @$canEdit,
					"controller" => $controller,
					"openEdition" => $openEdition,
				) );
	echo $this->renderPartial('co2.views.pod.qrcode',
		array("type" => @$element["collection"],
			"name" => @$element['name'],
			"address" => @$address,
			"address2" => @$address2,
			"email" => @$element['email'],
			"url" => @$element["url"],
			"tel" => @$tel,
			"img"=>(isset($element['profilThumbImageUrl'])) ? Yii::app()->createUrl('/'.@$element['profilThumbImageUrl']): $element['defaultImg'] ));
	echo $this->renderPartial($layoutPath.'forms.'.Yii::app()->params["CO2DomainName"].'.formContact',
		array("element"=>@$element));

	if(!isset($element["costum"]))
		echo $this->renderPartial("co2.views.element.firstStepCostum", array("element"=>$element));
	$contextData = Element::getElementForJS(@$element, @$element["collection"]);
?>

<script type="text/javascript">
	var contextData = <?php echo json_encode( Element::getElementForJS(@$element, @$element["collection"]) ); ?>; 
	mylog.log("init contextData", contextData);

    var params = <?php echo json_encode(@$params); ?>; 
    var canEdit =  <?php echo json_encode(@$canEdit) ?>;
	var canParticipate =  <?php echo json_encode(@$canParticipate)?>;
	var canSee =  <?php echo json_encode(@$canSee) ?>;
	var elementParams =  <?php echo json_encode(@$elementParams) ?>;
	var openEdition = ( ( '<?php echo (@$openEdition == true); ?>' == "1") ? true : false );
    var dateLimit = 0;
    var typeItem = "<?php echo $element["collection"]; ?>";
    var liveScopeType = "";
    var navInSlug=false;
   	var pageConfig=<?php echo json_encode($pageConfig) ?>;
   	var invitedMe = <?php echo json_encode(@$invitedMe) ?>;
	if(typeof contextData.rolesLists != "undefined" && contextData.rolesLists.length == 0)
	    contextData.rolesLists = {};
   	if(typeof contextData.slug != "undefined")
     	navInSlug=true;
   
	var hashUrlPage= ( (typeof contextData.slug != "undefined") ? 
						"#@"+contextData.slug : 
						"#page.type."+contextData.type+".id."+contextData.id);
    
    if(location.hash.indexOf("#page")>=0){
    	strHash="";
    	hashPage=location.hash.split("#page.type."+contextData.type+".id."+contextData.id);
    	if(notEmpty(hashPage[1])){
    		strHash=hashPage[1];
    	}
    	//replaceSlug=true;
    	history.replaceState("#page.type."+contextData.type+".id."+contextData.id, "", hashUrlPage+strHash);

    }
    
    var cropResult;
    var idObjectShared = new Array();

    var personCOLLECTION = "<?php echo Person::COLLECTION; ?>";
    pageProfil.params={
    	view : "<?php echo @$_GET['view']; ?>",
    	subview : "<?php echo @$_GET['subview']; ?>",
    	action : null,
		dir: "<?php echo @$_GET['dir']; ?>",
		key : "<?php echo @$_GET['key']; ?>",
		folderId : "<?php echo @$_GET['folder']; ?>",
	};
	if(notNull(pageConfig) && typeof pageConfig.initView != "undefined" && pageProfil.params.view=="") pageProfil.params.view=pageConfig.initView;
	var roomId = "<?php echo @$_GET['room']; ?>";
	var proposalId = "<?php echo @$_GET['proposal']; ?>";
	var resolutionId = "<?php echo @$_GET['resolution']; ?>";
	var actionId = "<?php echo @$_GET['action']; ?>";
	var isLiveNews = ""; 
	if(typeof contextType != "undefined" && contextType != "poi"){
		var connectTypeElement="<?php echo Element::$connectTypes[$type] ?>";
	}
	if(contextData.type == "citoyens") var currentRoomId = "";
	var contextListFilter={
			classifiedsType:{
				"classifieds":{
					label : tradCategory.market,
					icon: "money",
					count: (typeof contextData.counts != "undefined" && typeof contextData.counts.classifieds != "undefined") ? contextData.counts.classifieds : 0
				},
				"ressources":{
					label : trad.ressources,
					icon : "cubes",
					count: (typeof contextData.counts != "undefined" && typeof contextData.counts.ressources != "undefined") ? contextData.counts.ressources : 0
				},
				"jobs":{
					label : trad.jobs,
					icon : "briefcase",
					count: (typeof contextData.counts != "undefined" && typeof contextData.counts.jobs != "undefined") ? contextData.counts.jobs : 0
				}
			}
		}; 
						
	//var mapCO = {} ;
	$(document).ready(function() {
		directorySocket.init();
		typeObj.buildCreateButton(".elementCreateButton", false, {
			addClass:"col-xs-6 col-sm-6 col-md-4 col-lg-4 uppercase btn-open-form",
			bgIcon:true,
			textColor:true,
			inElement:true,
			allowIn:true,
			contextType: contextData.type,
			bgColor : "white",
			explain:true,
			inline:false
		});
		pageProfil.init();
		
		$(".hide-"+contextData.type).hide();
		
		getContextDataLinks();
		if(typeof contextData.links != "undefined" && typeof rolesList != "undefined")
			pushListRoles(contextData.links);
		var elemSpec = dyFInputs.get("<?php echo $type?>");
		buildQRCode( elemSpec.ctrl ,"<?php echo (string)$element["_id"]?>");
	
		if(typeof networkJson != "undefined" && notNull(networkJson)){
			$(".main-menu-left").hide();
			$("#vertical").remove();
			$("#menuApp").remove();

		}
	});

</script>
