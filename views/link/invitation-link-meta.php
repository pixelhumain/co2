<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?= $title ?></title>
    <meta name="description" content="<?= $description ?>">
    <meta property="og:image" content="<?= $image ?>">
</head>
<body>
    <script>
        window.location.href = "<?= $redirectUrl ?>"
    </script>
</body>
</html>