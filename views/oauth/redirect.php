<?php
use yii\helpers\Json;
use yii\helpers\Url;

/* @var $this \yii\base\View */
/* @var $url string */
/* @var $enforceRedirect bool */
// var_dump(Json::htmlEncode($url));
// var_dump(Json::htmlEncode($enforceRedirect));
// var_dump(Yii::$app->getSession()->hasFlash('errorAuth'));
// exit;
?>
<!DOCTYPE html>
<html>
<head>
    <script>
        function popupWindowRedirect(url, enforceRedirect)
        {
            if (window.opener && !window.opener.closed) {
                window.opener.focus();
                if (enforceRedirect) {
                    window.opener.location.reload();
                }
                window.close();
            } else {
                window.location = url;
            }
        }
        popupWindowRedirect(<?= Json::htmlEncode($url) ?>, <?= Json::htmlEncode($enforceRedirect) ?>);
    </script>
</head>
<body>
<h2 id="title" style="display:none;">Redirecting back to the &quot;<?= Yii::$app->name ?>&quot;...</h2>
<h3 id="link"><a href="<?= $url ?>">Click here to return to the &quot;<?= Yii::$app->name ?>&quot;.</a></h3>
<script type="text/javascript">
    document.getElementById('title').style.display = '';
    document.getElementById('link').style.display = 'none';
</script>
</body>
</html>
