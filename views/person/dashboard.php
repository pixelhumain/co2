<?php
// $cssAnsScriptFilesModule = array(
//     //Data helper
//     '/js/default/calendarObj.js',
// );
// HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, $this->module->getParentAssetsUrl());
$me = isset(Yii::app()->session['userId']) ? Person::getById(Yii::app()->session['userId']) : null;
?>
<style type="text/css">
    .dashboard-account .close-modal .lr,
    .dashboard-account .close-modal .lr .rl {
        height: 50px;
        width: 1px;
        background: white;
    }

    .dashboard-account .close-modal:hover .lr,
    .dashboard-account .close-modal:hover .lr .rl {
        width: 2px;
    }

    .dashboard-account .close-modal .lr {
        margin-left: 28px;
        transform: rotate(45deg);
        -ms-transform: rotate(45deg);
        -webkit-transform: rotate(45deg);
    }

    .dashboard-account .close-modal .lr .rl {
        transform: rotate(90deg);
        -ms-transform: rotate(90deg);
        -webkit-transform: rotate(90deg);

    }

    .dashboard-account .close-modal {
        position: absolute;
        top: 5px;
        right: 5px;
        width: 55px;
        height: 50px;
        z-index: 1;
        cursor: pointer;
    }

    .dashboard-account .profil-img {
        border-radius: 5px;
        border: 2px solid white;
    }

    .dashboard-account .btn-dash-link {
        color: white;
        font-size: 18px;
        padding: 5px 10px;
        border-bottom: 1px solid white;
    }

    .dashboard-account .btn-dash-link:hover {
        color: black;
        background-color: white;
    }

    .dashboard-account {
        padding-bottom: 30px;
    }

    .profile-panel .profile-info {
        padding: 0;
        margin: 0;
        list-style: none;
    }

    .profile-panel .profile-stats-info a {
        font-size: 24px;
        margin-right: 10px;
        margin-top: 10px;
        color: #79859b;
    }

    .profile-panel .user-name {
        margin-top: 10px;
        font-size: 22px;
        margin-bottom: 10px;
    }

    .profile-panel .profile-avatar img {
        border-radius: 50%;
        border: 1px solid #e7e7e2;
        object-fit: contain;
    }

    .profile-panel .profile-avatar {
        border-radius: 50%;
        padding: 10px;
        border: 1px solid #e7e7e2;
        margin: auto;
    }

    @media (min-width:1200px) {
        .profile-panel .profile-avatar {
            width: 210px;
            height: 210px;
        }
    }

    @media (min-width: 992px) and (max-width: 1199px) {
        .profile-panel .profile-avatar {
            padding: 6px;
            width: 145px;
            height: 145px;
        }
    }

    @media (min-width: 768px) and (max-width: 991px) {
        .profile-panel .profile-avatar {
            width: 125px;
            height: 125px;
            padding: 5px;
        }
    }

    @media (max-width: 767px) {
        .profile-panel .profile-avatar {
            width: 125px;
            height: 125px;
            padding: 5px;
        }
    }

    .panel-body {
        padding: 15px;
    }

    .padding-top-15 {
        padding-top: 15px;
    }

    .profile-panel .profile-image {
        border-top-left-radius: 4px;
        border-top-right-radius: 4px;
    }

    .dsb-panel {
        position: relative;
        margin-bottom: 25px;
        border-radius: 4px;
        border: 0;
        box-shadow: none;
    }

    .white-content {
        color: #768399;
        background: #fff;
        background-color: #fff;
    }

    .panel-green {
        background-color: transparent;
        border: 1px solid #9fbd38;
    }

    .panel-green .panel-heading+.panel-collapse>.list-group,
    .panel-green .panel-heading+.panel-collapse>.panel-body {
        border-top: 1px solid #9fbd38;
    }

    .panel-green .panel-heading.label {
        padding: 3px 5px 2px;
        float: right;
        margin: 9px 15px 0 0;
        -webkit-box-shadow: 0 1px 2px rgba(0, 0, 0, 0.3) inset, 0 1px 0 #ffffff;
        box-shadow: 0 1px 2px rgba(0, 0, 0, 0.3) inset, 0 1px 0 #ffffff;
    }

    .panel-green .panel-body ul.nav-tabs {
        border-bottom: 1px solid #9fbd38;
    }

    .panel-green .panel-body ul.nav-tabs>li.active>a,
    .panel-green .panel-body ul.nav-tabs>li.active>a:focus,
    .panel-green .panel-body ul.nav-tabs>li.active>a:hover {
        color: #9fbd38;
        border: 1px solid #9fbd38;
    }

    .panel-green a,
    .panel-green a:hover,
    .panel-green a:focus,
    .panel-green a:active {
        color: #fff;
    }

    .panel-green div#collapseNotif {
        max-height: 560px;
        min-height: 350px;
        overflow-y: scroll;
    }

    .panel-green div#collapseNet {
        height: 560px;
    }

    @media (max-width: 767px) {

        .panel-green div#collapseNotif,
        .panel-green div#collapseEvent {
            height: 400px;
        }

        .panel-green div#collapseNet {
            height: 400px;
        }

        .panel-body div#modalFloopDrawer {
            height: 398px !important;
        }

        .panel-body div#modalFloopDrawer {
            display: block !important;
            width: 99% !important;
        }
    }

    .panel-green h4.panel-title {
        text-transform: initial;
        font-size: 18px;
    }

    #collapseNotif .notificationsElement .pageslide-title {
        display: none;
    }

    #collapseNotif .notificationsElement .pageslide-list {
        padding: 0px !important
    }

    #collapseNotif .notificationsElement .btn-notification-action {
        margin-right: 0px !important;
    }

    #collapseNotif .notifications li,
    #collapseNotif .notificationsElement li {
        background-color: rgba(228, 241, 228, 0.72);
        border-bottom: 1px dashed #d9ded9fa;
    }

    .pan-green .panel-body .list-group {
        background-color: rgba(228, 241, 228, 0.72);
    }

    .panel-green .panel-body .list-group a {
        color: #354535;
        background-color: rgba(228, 241, 228, 0.72);
    }

    .panel-green .panel-body .list-group a:hover {
        color: #fff;
        background-color: transparent;
    }

    .nameLang {
        padding-top: 15px;
        padding-left: 20px;
        text-transform: none;
        font-size: 15px;
    }

    .stats-col {
        margin-bottom: 25px;
    }

    .stats-col .circle {
        display: inline-block;
        width: 75px;
        height: 75px;
        font-weight: 500;
        color: #fff;
        border: 3px solid #9fbd38;
        border-radius: 50%;
        position: relative;
        transition: all .3s ease-out;
    }

    a:hover .circle {
        color: #9fbd38;
        border: 3px solid #fff;
        transform: rotate(360deg);
    }

    .stats-col .circle .stats-no {
        color: #fff;
        width: 30px;
        height: 30px;
        line-height: 25px;
        top: 0px;
        right: -8px;
        font-size: 18px;
        position: absolute;
        border-radius: 50%;
        font-weight: 700;
    }

    .iconDash {
        font-size: 45px;
        line-height: 68px;
    }

    .labelDash {
        padding-top: 10px;
        text-transform: none;
        color: #9fbd38;
    }

    a:hover .labelDash {
        color: #fff;
    }

    .icon-before-name {
        border-radius: 50%;
        width: 28px;
        height: 28px;
        color: white;
        text-align: center;
        line-height: 28px;
        margin-right: 10px;
        display: inline-block;
    }

    .fs-18 {
        font-size: 18px;
    }

    @media (min-width: 768px) and (max-width: 991px) {
        .iconDash {
            font-size: 30px;
            line-height: 52px;
        }

        .stats-col .circle {
            width: 60px;
            height: 60px;
        }
    }

    @media (max-width: 767px) {
        .iconDash {
            font-size: 25px;
            line-height: 50px;
        }

        .stats-col .circle {
            width: 55px;
            height: 55px;
        }

        .labelDash {
            font-size: 13px;
        }

        .stats-col .circle .stats-no {
            width: 25px;
            height: 25px;
            line-height: 18px;
            font-size: 15px;
        }

        .stats-col {
            margin-bottom: 25px;
            padding-left: 5px;
            padding-right: 5px;
        }
    }

    .about-identity {
        text-transform: none;
        font-weight: 500;
        color: white;
        font-size: 15px;
        padding: 6px 10px;
        border-bottom: 1px solid white;
        min-height: 32px;
    }

    a.btn-logout-Dash {
        background-color: white;
        color: red;
        border: 1px solid red;
        text-transform: none;
        font-size: 16px;
        font-weight: 700;
    }

    a.btn-logout-Dash:hover {
        background-color: red;
        color: white;
        border: 1px solid white;
    }

    .panel-body div#modalFloopDrawer {
        display: block !important;
        width: 99%;
        height: 556px;
        top: 42px !important;
        margin-left: 2px;
        margin-right: 2px;
    }

    div#collapseNet #modalFloopDrawer .btn-select-contact {
        min-width: 85% !important;
        width: 85% !important;
        /* border-radius: 0px; */
    }

    .panel-green div#collapseNet #modalFloopDrawer .floopHeader #btnFloopClose {
        display: none;
    }

    .tag-list-identity li a {
        font-size: 12px;
        background-color: white;
        padding: 5px 12px;
        color: #333;
        border-radius: 2px;
        border: 1px solid #9fbd38;
        margin-right: 5px;
        margin-top: 5px;
        display: block;
    }

    ul.tag-list-identity li {
        list-style: none;
        display: inline-block;
        text-align: center;
    }

    /*calendar*/
    .calendar_title {
        border-bottom: 2px solid #E6E9ED;
        padding: 1px 5px 6px;
    }

    .calendar_title h4 {
        text-transform: none;
        font-size: 20px;
        color: #E6E9ED;
    }

    div#event-calendar {
        text-transform: none;
    }

    div#calendar-dash .fc th,
    div#calendar-dash .fc td {
        border-color: #9fbd38 !important;
        color: #fff;
    }

    div#event-calendar .fc-past {
        background: #354535;
    }

    div#event-calendar .fc-button {
        color: #9fbd38 !important;
        line-height: 0px;
        opacity: 1;
        border: 1px solid #9fbd38 !important;
    }

    div#event-calendar .fc-toolbar .fc-center h2 {
        display: inline-block;
        font-size: 22px !important;
        color: #9fbd38;
    }

    div#event-calendar td.fc-today {
        background: #9fbd38 !important;
    }

    /*  fileupload   */

    .profile-avatar #fileuploadContainer {
        position: relative;
        background-color: transparent;
        margin-left: 0px;
        box-shadow: none !important;
    }

    .profile-avatar .fileupload-preview.thumbnail,
    .profile-avatar .fileupload-new.thumbnail {
        border: 0px solid white !important;
    }

    .profile-avatar .user-image .user-image-buttons {
        position: absolute;
        top: auto;
        bottom: 0px;
        display: block;
    }

    .profile-avatar .user-image .user-image-buttons .userLabelBtn {
        display: none;
    }

    .profile-avatar .user-image .user-image-buttons a.fileupload-new {
        border-radius: 50%;
        border: 2px solid #fff;
        background-color: #9fbd38;
        color: #fff;
        width: 35px;
        height: 35px;
        transition: all .3s ease-out;
    }

    .profile-avatar .user-image .user-image-buttons a.fileupload-new:hover {
        border-radius: 50%;
        background-color: white;
        color: #9fbd38;
        border: 2px solid #9fbd38;
        transform: rotate(360deg);
    }

    @media (min-width:1200px) {
        .profile-avatar .user-image .user-image-buttons a.fileupload-new {
            width: 43px;
            height: 43px;
        }

        .profile-avatar .user-image .user-image-buttons a.fileupload-new i {
            font-size: 18px;
            line-height: 30px;
        }

        .profile-avatar .user-image .user-image-buttons {
            left: 140px;
        }

        .profile-avatar .fileupload,
        .profile-avatar .fileupload-preview.thumbnail,
        .profile-avatar .fileupload-new .thumbnail,
        .profile-avatar .fileupload-new .thumbnail img,
        .profile-avatar .fileupload-preview.thumbnail img {
            border-radius: 50%;
            width: 188px;
            height: 188px;
        }
    }

    @media (min-width: 992px) and (max-width: 1199px) {
        .profile-avatar .user-image .user-image-buttons a.fileupload-new {
            width: 39px;
            height: 39px;
        }

        .profile-avatar .user-image .user-image-buttons a.fileupload-new i {
            font-size: 15px;
            line-height: 25px;
        }

        .profile-avatar .user-image .user-image-buttons {
            left: 100px;
        }

        .profile-avatar .fileupload,
        .profile-avatar .fileupload-preview.thumbnail,
        .profile-avatar .fileupload-new .thumbnail,
        .profile-avatar .fileupload-new .thumbnail img,
        .profile-avatar .fileupload-preview.thumbnail img {
            border-radius: 50%;
            width: 131px;
            height: 131px;
        }
    }

    @media (max-width: 991px) {

        .profile-avatar .fileupload,
        .profile-avatar .fileupload-preview.thumbnail,
        .profile-avatar .fileupload-new .thumbnail,
        .profile-avatar .fileupload-new .thumbnail img,
        .profile-avatar .fileupload-preview.thumbnail img {
            border-radius: 50%;
            width: 113px;
            height: 113px;
        }

        .profile-avatar .user-image .user-image-buttons {
            left: 80px;

        }

        .profile-avatar .user-image .user-image-buttons a.fileupload-new i {
            line-height: 23px;
        }
    }

    @media (max-width: 991px) {
        #banner_element {
            display: block !important;
        }
    }

    #fileuploadContainer .thumbnail {
        background-color: #2C3E50 !important;
    }
</style>

<div class='dashboard-account modal co-scroll' style='display:none; overflow-y:scroll;background-color: rgba(0,0,0,0.9);z-index: 100000;position:fixed;'>
    <div class="close-modal" data-dismiss="modal">
        <div class="lr">
            <div class="rl">
            </div>
        </div>
    </div>

    <div class="col-xs-12 padding-top-50">
        <?php if (isset($view) && !empty($view))
        {
            echo $this->renderPartial($view);
        }
        else
        { ?>



            <div class="row">
                <!--<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 padding-top-15">
			    <div class="panel-group  animated bounceInLeft" id="accordion">

                    <div class="dsb-panel panel-green">
                        <div class="panel-heading">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseNet" aria-expanded="false" class="collapsed">
                                <h4 class="panel-title text-green">
                                	<i class="fa fa-users"></i> <?php echo Yii::t("common", "My network") ?>
                            	</h4>
                            </a>
                        </div>
                    	<div id="collapseNet" class="panel-collapse " aria-expanded="true">
                            <div class="panel-body">
                                <div id="modalFloopDrawer" class="floopDrawer" style="display: block!important;"></div>
                            </div>
                        </div>
                    </div>

                    <div class="dsb-panel panel-green visible-xs">
                        <div class="panel-heading">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseNotif" class="collapsed" aria-expanded="false">
                                <h4 class="panel-title text-green">
                                <i class="fa fa-bell"></i> <?php echo Yii::t("common", "My notifications"); ?>
                                <?php $countNotifElement = ActivityStream::countUnseenNotifications(Yii::app()->session["userId"], Person::COLLECTION, Yii::app()->session["userId"]); ?>
                    
                                <span title="" class="pull-right notifications-count 
                                <?php if (!@$countNotifElement || (@$countNotifElement && $countNotifElement == "0"))
                                    echo 'hidden';
                                else echo 'label bg-orange animated tada'; ?>"
                                 data-original-title="<?php echo @$countNotifElement ?> total posts"><?php echo @$countNotifElement ?></span>
                                </h4>
                            </a>
                        </div>
                        <div id="collapseNotif" class="panel-collapse collapse co-scroll" aria-expanded="false" style="height: 0px;">
                            <div class="panel-body no-padding notifications-dash">
                                
                            </div>
                        </div>
                    </div>
                    
                </div>

					
		    </div>-->
                <div class="col-lg-4 col-md-5 col-sm-6 col-xs-12">
                    <div class="dsb-panel panel-default plain profile-panel" style="background-color: transparent;">
                        <div class="panel-body no-padding">
                            <div class="profile-avatar animated bounceInDown">
                                <a href="#@<?php echo Yii::app()->session["user"]["slug"] ?>" class="lbh">
                                    <?php
                                    echo $this->renderPartial(
                                        'co2.views.pod.fileupload',
                                        array(
                                            "podId" => "modalDash",
                                            "element" => $me,
                                            "edit" => true,
                                            "openEdition" => false
                                        )
                                    ); ?>
                                </a>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 animated bounceInUp">
                                <div class="user-name text-center">
                                    <a href="#@<?php echo Yii::app()->session["user"]["slug"] ?>" class="label label-success lbh padding-10">

                                        <span class="elipsis" style="text-transform: none;">
                                            <?php echo Yii::app()->session["user"]["name"]; ?>
                                        </span>

                                    </a>
                                </div>
                                <?php $themeParams = $this->appConfig; ?>
                                <div class="about-identity text-center">
                                    <i class='fa fa-language text-green fs-18'></i>
                                    <div class="dropdown" style="display: inline-block;">
                                        <a href="#" class="dropdown-toggle btn-language padding-5 text-white" style="padding-top: 7px !important;" data-toggle="dropdown" role="button">
                                            <img src="<?php echo Yii::app()->getRequest()->getBaseUrl(true); ?>/images/flags/<?php echo Yii::app()->language ?>.png" width="22" /> <?php echo Yii::t("common", $themeParams["languages"][Yii::app()->language]); ?><span class="caret"></span></a>
                                        <ul class="dropdown-menu arrow_box dropdown-languages-nouser" role="menu" style="">
                                            <?php foreach ($themeParams["languages"] as $lang => $label)
                                            { ?>
                                                <li><a href="javascript:;" onclick="coInterface.setLanguage('<?php echo $lang ?>', true, true)"><img src="<?php echo Yii::app()->getRequest()->getBaseUrl(true); ?>/images/flags/<?php echo $lang ?>.png" width="25" /> <?php echo Yii::t("common", $label) ?></a></li>
                                            <?php } ?>
                                        </ul>
                                    </div>
                                </div>

                                <?php
                                if (!empty($me["address"]["addressLocality"]))
                                {
                                    echo "<div class='text-center about-identity'>";
                                    echo "<i class='fa fa-map-marker text-green fs-18'></i> ";
                                    echo !empty($me["address"]["streetAddress"]) ? $me["address"]["streetAddress"] . ", " : "";
                                    echo !empty($me["address"]["postalCode"]) ? $me["address"]["postalCode"] . ", " : "";
                                    echo $me["address"]["addressLocality"];
                                    echo "</div>";
                                }
                                ?>
                                <?php if (!empty($me["shortDescription"]))
                                {
                                    echo '<div class="text-center about-identity"><i class="fa fa-quote-right text-green fs-18"></i> ' . $me["shortDescription"] . '</div>';
                                } ?>
                                <?php if (!empty($me["tags"]))
                                {
                                    echo "<div class='text-center'><ul class='tag-list-identity text-center no-padding about-identity' style='border-bottom: none;'>";
                                    foreach ($me["tags"]  as $key => $tag)
                                    {
                                        echo '<li class="text-center"><a href="#search?text=#' . $tag . '" class="lbh bold text-green" style="padding:2px 5px;background-color: transparent;"><i class="fa fa-hashtag text-white"></i> ' . $tag . '</a></li>';
                                    };
                                    echo "</ul></div>";
                                } ?>
                                <?php if (Authorisation::isInterfaceAdmin())
                                { ?>
                                    <div class="text-center">
                                        <a href="#admin" class="btn btn-default lbh text-dark bold col-xs-12" style="margin-top: 10px">
                                            <i class="fa fa-cog"></i> <?php echo Yii::t("common", "Administration"); ?>
                                        </a>
                                    </div>
                                <?php } ?>

                                <div class="text-center">
                                    <a href="javascript:;" class="btn btn-logout-Dash col-xs-12 logoutBtn" style="margin-top: 10px">
                                        <i class="fa fa-sign-out"></i> <?php echo Yii::t("common", "Log Out"); ?>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-8 col-md-7 col-sm-6 col-xs-12 padding-top-15 animated bounceInRight">
                    <div class="stats-col text-center col-lg-4 col-md-4 col-sm-4 col-xs-4">
                        <a href="#home" class="btnDash lbh">
                            <div class="circle" data-animation="animated bounce">
                                <i class="fa fa-home iconDash"></i>
                            </div>
                            <div class="labelDash">
                                <?php echo Yii::t("common", "Home"); ?>
                            </div>
                        </a>
                    </div>

                    <div class="stats-col text-center col-lg-4 col-md-4 col-sm-4 col-xs-4">
                        <a href="#@<?php echo Yii::app()->session["user"]["slug"] ?>" class="btnDash lbh">
                            <div class="circle" data-animation="animated bounce">
                                <i class="fa fa-user iconDash"></i>
                            </div>
                            <div class="labelDash">
                                <?php echo Yii::t("common", "My Profile"); ?>
                            </div>
                        </a>
                    </div>

                    <div class="stats-col text-center col-lg-4 col-md-4 col-sm-4 col-xs-4">
                        <a href="javascript:;" onclick='rcObj.loadChat("","citoyens", true, true)' class="btnDash lbh btn-menu-chat">
                            <div class="circle" data-animation="animated bounce">
                                <span class="chatNotifs badge stats-no bg-orange animated bounceIn" data-toggle="counter-up"></span>
                                <i class="fa fa-comments iconDash"></i>
                            </div>
                            <div class="labelDash">
                                <?php echo Yii::t("common", "Chat"); ?>
                            </div>
                        </a>
                    </div>

                    <div class="stats-col text-center col-lg-4 col-md-4 col-sm-4 col-xs-4">
                        <a href="#@<?php echo Yii::app()->session["user"]["slug"] ?>.view.settings" class="btnDash lbh">
                            <div class="circle" data-animation="animated bounce">
                                <i class="fa fa-cogs iconDash"></i>
                            </div>
                            <div class="labelDash">
                                <?php echo Yii::t("common", "Settings"); ?>
                            </div>
                        </a>
                    </div>

                    <div class="stats-col text-center col-lg-4 col-md-4 col-sm-4 col-xs-4">
                        <a href="#docs.page.welcome" class="btnDash lbh">
                            <div class="circle" data-animation="animated bounce">
                                <i class="fa fa-book iconDash"></i>
                            </div>
                            <div class="labelDash">
                                <?php echo Yii::t("common", "Documentation"); ?>
                            </div>
                        </a>
                    </div>

                    <div class="stats-col text-center col-lg-4 col-md-4 col-sm-4 col-xs-4">
                        <a href="#info.p.stats" class="btnDash lbh">
                            <div class="circle" data-animation="animated bounce">
                                <i class="fa fa-bar-chart iconDash"></i>
                            </div>
                            <div class="labelDash">
                                <?php echo Yii::t("common", "Statistics"); ?>
                            </div>
                        </a>
                    </div>


                    <div id="calendar-dash" class="calendar-dash">
                        <div class="calendar_title">
                            <h4><i class="fa fa-calendar"></i> <?php echo Yii::t("common", "My events") ?></h4>
                        </div>
                        <div id="event-calendar" class="no-padding"></div>
                    </div>
                </div>




            </div>



        <?php } ?>
    </div>
</div>

<script type="text/javascript">
    var clickBsDoc = false;

    jQuery(document).ready(function() {
        $(".dashboard-account").css({
            "top": $("#mainNav").outerHeight() + "px",
            "left": $("#menuApp.menuLeft").width() + "px"
        });
        //ajaxPost(".dashboard-cte", baseUrl+"/costum/ctenat/dashboardaccount",{roles:costum.rolesCTE}, function(){ }, "html");
        $(".open-modal-dashboard").off().on('click', function() {
            if (typeof aapObj !== 'undefined' && typeof aapObj.form === 'object' && aapObj.form && typeof aapObj.form.type === 'string' && aapObj.form.type === 'aap' && typeof costum.type === 'string' && costum.type === 'aap' && typeof aapObj.initHTML === 'function') {
                aapObj.initHTML(null);
                $('.modal.dashboard-aap').show(200);
                return;
            }
            localStorage.setItem("dashboardAccoundOpened", true);
            var openDashEvent = true;
            var $win = $(document); // or $box parent container
            var $box = $(".dashboard-account");
            var $lbhBtn = $(".lbh, .lbhp, .close-modal, .btn-dash-link");
            var $preview = $('#modal-preview-coop');
            $(".dashboard-account").show(200);
            if (!clickBsDoc) {
                clickBsDoc = true;
                $win.on("click.Bst", function(event) {
                    if ($lbhBtn.has(event.target).length != 0 //checks if descendants of $box was clicked
                        ||
                        $lbhBtn.is(event.target)) {
                        $(".dashboard-account").hide(200);
                        localStorage.removeItem("dashboardAccoundOpened")
                    } else {
                        if ($preview.has(event.target).length != 0 || $preview.is(event.target)) {
                            $(".dashboard-account").show();
                        } else if (!openDashEvent) {
                            if ($box.has(event.target).length == 0 //checks if descendants of $box was clicked
                                &&
                                !$box.is(event.target) //checks if the $box itself was clicked
                            ) {
                                $(".dashboard-account").hide(200);
                                dashboadAccountModalOpened = false;
                                localStorage.removeItem("dashboardAccoundOpened")
                            }
                        }
                    }
                    openDashEvent = false;
                    //alert("you clicked inside the box");
                });
            }
            /*****************show agenda *****************/
            var domCal = {
                container: "#event-calendar",
                options: {
                    header: {
                        left: 'prev,next',
                        center: 'title',
                        right: 'today'
                    },
                },
                eventBackgroundColor: '#FFA200',
            };
            var dashEvenCalendar = {};
            dashEvenCalendar = calendarObj.init(domCal);
            dashEvenCalendar.options.personalAgenda = true
            dashEvenCalendar.results.add(dashEvenCalendar, myContacts.events);

            $(".dashboard-account .lbh").on('click', function() {
                $(".dashboard-account").hide();
            })

        });
        //afficher la notification
        mylog.log("pageProfil.views.notifications");
        var url = "element/notifications/type/citoyens/id/" + userId;
        ajaxPost('.notifications-dash', baseUrl + '/' + moduleId + '/' + url,
            null,
            function() {});
        initFloopDrawer('#modalFloopDrawer');
        //calendar.init("#event-calendar");

    });
    /* 	ANIMATION
    	$(".circle").hover(function() {
    	  var animation = $(this).attr("data-animation");
    	  $(this).addClass(animation).delay(1000).queue(function(next) {
    	    $(this).removeClass(animation);
    	    next();
    	  });
    	});
    */
    var pageApp = <?php echo json_encode(@$page); ?>;
</script>