<?php if  (!empty($element["address"]) || !empty(Yii::app()->session["userId"])) {?>
    <div class="well no-padding pod-info-Address bg-white" id="pod-info-Address">
        <div class="well no-padding" id="divMapContent" style="border-radius: 3px;background-color: #fff;border: none ;height: 200px;width: 100%;display: block;"></div>

        <div class="margin-bottom-10" style="padding: 10px 10px 0px 10px;">
            <span> <i class="fa fa-home"></i>&nbsp;</span>
            <?php if (!empty($element["address"]["codeInsee"]) && ( $edit==true || ( $openEdition==true && Yii::app()->session["userId"] != null ) ) ) { 

            echo '<button class="btn-update-geopos btn pad-2 pull-right tooltips"data-toggle="tooltip" data-placement="bottom" title="'.Yii::t("common","Update Location").'"><span class="couleur-rouge bold"><i class="fa fa-map-marker"></i></span></button>';
            } ?>

            <?php 
                if( ($type == Person::COLLECTION && Preference::showPreference($element, $type, "locality", Yii::app()->session["userId"])) ||  $type!=Person::COLLECTION) {
                    $address = "";
                    $address .= '<span id="detailAddress"> '.
                                    (( @$element["address"]["streetAddress"]) ? 
                                        $element["address"]["streetAddress"]."<br/>": 
                                        ((@$element["address"]["codeInsee"])?"":Yii::t("common","Unknown Locality")));
                    $address .= (( @$element["address"]["postalCode"]) ?
                                     $element["address"]["postalCode"].", " :
                                     "")
                                    ." ".(( @$element["address"]["addressLocality"]) ? 
                                             $element["address"]["addressLocality"] : "") ;
                    $address .= (( @$element["address"]["addressCountry"]) ?
                                     ", ".OpenData::$phCountries[ $element["address"]["addressCountry"] ] 
                                    : "").
                                '</span>';
                    echo $address;
                    if( empty($element["address"]["codeInsee"]) && Yii::app()->session["userId"] == (String) $element["_id"]) { ?>
                        <br><a href="javascript:;" class="cobtn btn btn-sm" style="margin: 10px 0px;">
                                <?php echo Yii::t("common", "Connect to your city") ?></a> 
                            <a href="javascript:;" class="whycobtn btn btn-default btn-sm explainLink" style="margin: 10px 0px;" onclick="showDefinition('explainCommunectMe',true)">
                                <?php echo  Yii::t("common", "Why ?") ?></a>
                    <?php }
            }else
                echo '<i>'.Yii::t("common","Not specified").'</i>';
            ?>


            <?php if( !empty($element["addresses"]) ){ ?>
            <div class="labelAbout padding-10">
                <span><i class="fa fa-map"></i></span> <?php echo Yii::t("common", "Others localities") ?>
            </div>
            <div class="col-md-12 col-xs-12 valueAbout no-padding" style="padding-left: 25px !important">
            <?php   foreach ($element["addresses"] as $ix => $p) { ?>           
                <span id="addresses_<?php echo $ix ; ?>">
                    <span>
                    <?php 
                    $address = '<span id="detailAddress_'.$ix.'"> '.
                                    (( @$p["address"]["streetAddress"]) ? 
                                        $p["address"]["streetAddress"]."<br/>": 
                                        ((@$p["address"]["codeInsee"])?"":Yii::t("common","Unknown Locality")));
                    $address .= (( @$p["address"]["postalCode"]) ?
                                     $p["address"]["postalCode"].", " :
                                     "")
                                    ." ".(( @$p["address"]["addressLocality"]) ? 
                                             $p["address"]["addressLocality"] : "") ;
                    $address .= (( @$p["address"]["addressCountry"]) ?
                                     ", ".OpenData::$phCountries[ $p["address"]["addressCountry"] ] 
                                    : "").
                                '</span>';
                    echo $address;
                    ?>



                </span>
                <hr/>
            <?php   } ?>
            </div>
        <?php } ?>

        </div>
        <div class="text-right padding-10">
            <?php if(empty($element["address"]) && $type!=Person::COLLECTION && ($edit==true || ( $openEdition==true && Yii::app()->session["userId"] != null ) )){ ?>
                <b><a href="javascript:;" class="btn-update-geopos btn btn-update-orange margin-top-5 addresses">
                    <i class="fa fa-map-marker"></i>
                    <span><?php echo Yii::t("common","Add a primary address") ; ?></span>
                </a></b>
            <?php   }
            //if($type!=Person::COLLECTION && !empty($element["address"]) && ($edit==true || ( $openEdition==true && Yii::app()->session["userId"] != null ) )) { ?>
                <!-- <b><a href='javascript:updateLocalityEntities("<?php //echo count(@$element["addresses"]) ; ?>");' id="btn-add-geopos" class="btn btn-default letter-blue margin-top-5 addresses" style="margin: 10px 0px;">
                    <i class="fa fa-plus" style="margin:0px !important;"></i> 
                    <span class="hidden-sm"><?php //echo Yii::t("common","Add an address"); ?></span>
                </a></b> -->
            <?php //} ?>                        
        </div>
        
        

    </div>

    <?php }?>