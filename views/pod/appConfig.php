<?php 
$tradByApp=array(
	"search"=>array(
		"label"=> Yii::t("common","the search"),
		"title"=> Yii::t("common","Your favorites of search")
	),
	"agenda"=>array(
		"label"=> Yii::t("common","the agenda"),
		"title"=> Yii::t("common","Your favorites on agenda")
	),
	"live"=>array(
		"label"=> Yii::t("common","the live"),
		"title"=> Yii::t("common","Your favorites on live")
	),
	"annonces"=>array(
		"label"=> Yii::t("common","the classifieds"),
		"title"=> Yii::t("common","Your favorites of classifieds")
	),
	"dda"=>array(
		"label"=> Yii::t("common","the agora"),
		"title"=> Yii::t("common","Your favorites of cooperative space")
	)
);
?>
<style type="text/css">
	.title-modal-fav-heading .btn-close-preview{
		border: 0px;
		font-size: 20px;
	}
	.btn-fav-actions{
		padding-top: 0px;
	    border-radius: 0px;
	    padding-bottom: 0px;
	    height: 100%;
	    float: left;
	}
	.title-modal-fav-heading{   
		height: 60px;
	    background-color: rgb(255, 255, 255);
	    color: rgb(73 87 97);
	    padding-top: 10px;
	    padding-bottom: 10px;
	}
	.title-modal-fav-heading h3{
		line-height: 40px;
	}
	.titleFlux{
		font-size: 18px;
   		font-weight: 800;
    	padding-bottom: 5px !important;
	}
	.description-fav{
		font-size: 16px;
	}
	.button-on-air{
		border-top: 1px solid white;
		box-shadow: 0px 1px 2px 1px #cccc;
	}
	.contentFormFav{
		border-bottom: 2px solid #495761;
    	background-color: #4a5761;
	}
	.button-actions-fav{
		position: absolute;
		right: 0px;
		top: 0px;
		bottom: 0px;
		width:fit-content;
	}
	/*.contentFormFav .addFlux{
		color: white;
	    font-size: 20px;
	    font-weight: 800;
	}*/
	.contentFormFav .addFlux i {
		font-size: 21px;
    	padding: 13px 16px !important;
	}
	.contentFormFav .formNewFavorites label{
		color: white;
	    font-weight: bolder;
	    font-size: 16px;
	    padding-left: 0px;
	}
	.badge-filter-fav{
		background-color : #44bc9c;
		margin-right: 5px;
	}
	.needSearch{
		padding: 30px;
	    font-size: 20px;
	    background-color: #9fbd38;
	    color: white;
	}
	.needSearch i{
		border-radius: 50px;
	    margin-top: -42px;
	    margin-left: -20px;
	    color: #9fbd38;
    	border: 4px solid #9fbd38;
	    font-weight: 800;
	    background: white;
	    position: absolute;
	}
	.noFavoritesrecords{
		padding: 30px;
		font-size: 20px; 
	}
</style>
<div class="col-xs-12 no-padding">
	<div class="title-modal-fav-heading col-xs-12">
		<h3 class="elipsis col-xs-10 no padding no-margin"> <?php echo $tradByApp[$appKey]["title"] ?> (<?php echo count($settings) ?>)</h3>
		<button class="btn btn-default pull-right btn-close-preview">
			<i class="fa fa-times"></i>
		</button>
	</div>
	<?php if($activatedF > 0){ ?>
			<div class="contentFormFav col-xs-12 no-padding">
				<a href="javascript:;" class='col-xs-12 addFlux needSearch muted text-center text-white'>
					<i class="fa fa-plus padding-20"></i><br/><?php echo Yii::t("common","Add to your favorites") ?>   
				</a>
			</div>
	<?php }else{ ?>
		<span class="needSearch col-xs-12 muted text-center">
			<i class="fa fa-search padding-20"></i><br/>
			<?php echo Yii::t("common","You need a filtered research to save favorites") ?>
		</span>
	<?php }  ?>
	<div class="col-xs-12 no-padding margin-bottom-15 content-favorites-flux">
        <?php 
        if(!empty($settings)){
	        foreach($settings as $k => $v){ ?>
	            <div class="col-xs-12 button-on-air shadow2 padding-top-10 padding-bottom-10 contentFlux-<?php echo $k ?>" data-sort="<?php echo $v["order"] ?>">
	              <span class= "col-xs-11 titleFlux" for="<?php echo $k ?>"><?php echo $v["name"] ?></span>
	              <div class="button-actions-fav">
	                  <button class="btn btn-primary activate-favorites fluxFav-<?php echo $k ?> btn-fav-actions tooltips" data-url="<?php echo $v["url"] ?>" value="<?php echo $k ?>" data-placement="left" data-title="<?php echo Yii::t("common", "Search") ?>">
	                  		<i class="fa fa-eye"></i>
	                  </button>
	                  <button class="infos-favorites btn btn-success btn-fav-actions tooltips" value="<?php echo $k ?>" data-placement="left" 
	                  		data-title="<?php echo Yii::t("common", "Infos") ?>">
	                  		<i class="fa fa-info-circle"></i>
	                  </button>
	                  <button class="delete-favorites btn bg-red text-white btn-fav-actions tooltips" value="<?php echo $k ?>" data-placement="left" 
	                  		data-title="<?php echo Yii::t("common", "Delete") ?>">
	                  		<i class="fa fa-trash"></i>
	                  	</button>
	              </div>
	            </div>
       	<?php 
     		}
       	}else{ ?>
       		<span class="noFavoritesrecords col-xs-12 muted text-center"><?php echo Yii::t("common", "You still haven't favorites saved on {what}", array("{what}" => Yii::t("common", $tradByApp[$appKey]["label"]))); ?> 
       	<?php } ?>
        
    </div>
</div>
<script type="text/javascript">

var settings=<?php echo json_encode($settings) ?>;
var appKey=<?php echo json_encode($appKey) ?>;
var searchP=<?php echo json_encode($searchP) ?>;
var domFilters=<?php echo json_encode($domFilters) ?>;
/*var urlByKey={
	"search" : "#search?",
	"agenda" : "#agenda?",
	"live": "news/co/index/isLive/true"
}*/
jQuery(document).ready(function() {
	$('.content-favorites-flux').sortDivs();
	/*if($(".searchObjCSS .btn-favorites-filters").length > 0){
		var heightTitle=$(".searchObjCSS .btn-favorites-filters").outerHeight()+"px";
		$(".title-modal-fav-heading").css({
			"height": heightTitle,
			"background-color" : $(".searchObjCSS .btn-favorites-filters").css( "color" ),
			"color" : $(".searchObjCSS .btn-favorites-filters").css( "background-color" )
		});
		$(".title-modal-fav-heading h3").css({
			"line-height": heightTitle
		});
		$(".btn-close-preview").css({
			"height": heightTitle,
			"color" : $(".searchObjCSS .btn-favorites-filters").css( "background-color" ),
			"background-color" : $(".searchObjCSS .btn-favorites-filters").css( "color" )
		});

	}*/
	$(".delete-favorites").off().on("click", function(){
		$(this).find("i").removeClass("fa-trash").addClass("fa-spin fa-spinner");
		var formData={
          	"id":userId,
          	"collection":"citoyens",
          	"path":"preferences."+appKey+"."+$(this).val(),
          	"pull":"preferences."+appKey,
       		"value":null
        };      
        var valueDiv=$(this).val();       
       	dataHelper.path2Value( formData, function(params) {
		    toastr.success("Favoris bien supprimé");
		    reloadPreview();
		});
	});
	$(".activate-favorites").off().on("click", function(){
		if(appKey=="live"){
			newsObj.loadLive($(this).data('url'), $(this).val(), settings);
		}else{
			urlCtrl.loadByHash($(this).data("url"));
		}
		urlCtrl.closePreview();
	});
	$(".infos-favorites").off().on("click", function(){
		var valueFav=$(this).val();
		if($(".contentFlux-"+valueFav+" .info-fav").length > 0){
			if($(".contentFlux-"+valueFav+" .info-fav").is(":visible"))
				$(".contentFlux-"+valueFav+" .info-fav").fadeOut(500);
			else
				$(".contentFlux-"+valueFav+" .info-fav").fadeIn(500);
		}else{
	       	var paramsSet=settings[valueFav];
	       	var str="<div class='info-fav col-xs-12 margin-bottom-5 info-fav-"+valueFav+"'>";
	       		if(typeof paramsSet.description != "undefined")
	       	str+=	"<span class='col-xs-12 no-padding description-fav'>"+paramsSet.description+"</span>";
	       	str+=	"<div class='contentFilters-fav margin-top-10'>";
	       			if(typeof paramsSet.tags != "undefined"){
	       				$.each(paramsSet.tags, function(e,v){
	       	str+=			"<span class='badge badge-filter-fav pull-left text-white'>"+v+"</span>";
			       		});
			       	}
			       	if(typeof paramsSet.type != "undefined"){
			str+=		"<span class='badge badge-filter-fav pull-left text-white'>"+paramsSet.type+"</span>";
			       	}
			       	if(typeof paramsSet.locality != "undefined"){
			       		$.each(paramsSet.locality, function(e,v){
	       	str+=			"<span class='badge badge-filter-fav pull-left text-white'>"+v.name+"</span>";
			       		});
			       	}
	       	str+=	"</div>";
	       	str+="</div>";
	       	$(".contentFlux-"+valueFav+" .button-actions-fav").before(str);
	    }
	});
	$(".addFlux").off().on("click", function(){
		if($(".formNewFavorites").length <= 0){
			var str = '<div class="col-xs-12 formNewFavorites padding-bottom-10 padding-top-10">' ;
				str+=     '<label class="col-xs-12 no-padding" for="awesomeness">'+trad.currentSearch+' : </label>';                       
		          
		           	$(domFilters+" .activeFilters").each(function(){
	            		str+="<span class='badge badge-filter-fav pull-left text-white'>"+$(this).text()+"</span>";
	            	});
		        str+=     '<label class="col-xs-12 padding-top-10" for="awesomeness">'+tradDynForm.name+' : </label><br> ' +                        
		              '<input type="text" id="nameStream" class="nameStream wysiwygInput form-control" value="" style="width: 100%" placeholder="'+tradDynForm.name+'..."/>'+
		              '<label class="col-xs-12 padding-top-10" for="awesomeness">'+tradDynForm.shortDescription+' : </label><br> ' +                        
		              '<textarea id="descStream" class="descStream wysiwygInput form-control" value="" placeholder="'+tradDynForm.shortDescription+'" style="width: 100%"></textarea>'+
		              '<br>'+
		             '<div class="button-ctrl">'+
		             	'<button class="pull-right btn btn-danger closeForm">'+tradDynForm.cancel+'</button>'+
		             	'<button class="pull-right btn btn-success saveFlux margin-right-5">'+tradDynForm.save+'</button>'+
		             '</div>'+
		       '</div>';
		    $(".contentFormFav").append(str);
		    $(".closeForm").click(function(){
		    	$(".formNewFavorites").fadeOut(500);
		    });
	 		$(".saveFlux").click(function(){       	
	            var formData={
	              	"id":userId,
	              	"collection":"citoyens",
	              	"path":"preferences."+appKey,
	              	"arrayForm":true,
	           		"value":	{
	           			"url" : "",
	          	  		"name":$("#nameStream").val(),
	              		"description":$("#descStream").val()
	              	}
	            };
	            if(appKey != "live")
	            	formData.value.url=location.hash;
	            else
	            	formData.value.url="news/co/index/isLive/true";
	            if(typeof searchP.forced != "undefined" && appKey != "live"){
	            	if(typeof searchP.forced.type != "undefined"){
	            		formData.value.url+= (formData.value.url.indexOf("?") >= 1) ? "&" : "?";
	            		formData.value.url+= "type="+searchP.forced.type;
	            	}
	            }
	            if(notEmpty(searchP.tags))
		        	formData.value.tags=searchP.tags;
		        if(notEmpty(getSearchLocalityObject()))
		        	formData.value.locality=getSearchLocalityObject();
		        if(notEmpty(searchP.type))
		        	formData.value.type=searchP.type;
		        if(notEmpty(searchP.category))
		        	formData.value.category=searchP.category;
	             dataHelper.path2Value( formData, function(params) {
	          		toastr.success("favoris bien enregistré");
	          		reloadPreview();
	          	} );
	        });
        }else{
        	if($(".formNewFavorites").is(":visible"))
        		$(".formNewFavorites").fadeOut(500);
        	else
        		$(".formNewFavorites").fadeIn(500);
        }
	});
		
});
function reloadPreview(){
	urlCtrl.openPreview("config/type/citoyens/id/"+userId,{
		appKey:appKey,
		domFilters: domFilters,
		searchP : searchP,
		activatedF : $(domFilters+" .filters-activate").length
	});
}
</script>