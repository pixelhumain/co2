<?php

use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\utils\Utils;

if (@$element["fromActivityPub"] && $element["fromActivityPub"] == true) {
    $activities = Utils::activitypubToElement($element["objectId"]);
    foreach ($activities as $activityKey => $activityValue) {
        $element[$activityKey] = $activityValue;
      }
}
?>


<div class="well well-sm well-DayHours pod-info-DateHours"  id="pod-info-DateHours">
        <a id="dateTimezone" href="javascript:;" class="tooltips text-dark" data-original-title="" data-toggle="tooltip"  data-placement="right">
            <span class="text-nv-3 text-center text-dark-blue">
                <i class="fa fa-clock-o"></i> <?php echo $title;?>
                
            </span>
        </a>
        <?php if($edit==true || ($openEdition==true && Yii::app()->session["userId"] != null) ) { ?>
        <button class="btn-update-when btn btn-update-dark-blue pad-2 pull-right tooltips"
                data-toggle="tooltip" data-editMode="<?php $dataMode ?>" data-placement="top" title="" alt="" data-original-title="<?php echo Yii::t("common","Update date") ?>">
            <i class="fa fa-edit"></i>
        </button>
        <?php } ?>

        <hr class="line-hr text-dark-blue">

        <div class="history-tl-container">
            <?php if((!empty($element["startDate"])) && (!empty($element["startDate"])) ) { ?>
            <ul class="tl">
                <li class="tl-item contentInformation" id="divStartDate" >
                    <div class="item-title bold" ><?php echo Yii::t("event","From") ?></div>
                    <div class="item-detail" id="startDateAbout"><?php echo (isset($element["startDate"]) ? $element["startDate"] : "" ); ?></div>
                </li>
                <li class="tl-item contentInformation" id="divEndDate" >
                    <div class="item-title bold"><?php echo Yii::t("common","To") ?></div>
                    <div class="item-detail" id="endDateAbout"><?php echo (isset($element["endDate"]) ? $element["endDate"] : "" ); ?></div>
                </li>
            </ul>
        <?php } 

            else if(!empty($element["openingHours"])) { ?> 
            <ul class="tl">
                <?php 
                    echo $this->renderPartial('co2.views.pod.openingHours', array("element" => $element));
                ?>
            </ul>
        <?php }

        else { ?>
            <p class="tl contentInformation text-center">
            <!--  -->  
                <span class="ttr-desc-4 text-dark-blue bold"><i class="fa fa-calendar"></i>&nbsp;<?php echo $emptyval ?></span>
            </p>
        <?php } ?>  
        </div>
        <!--
        <span class="ttr-desc-4 txt-vert bold contentInformation" id="divNoDate" style="padding-left: 15px"><i class="fa fa-calendar"></i> Pas de date</span>
        -->
    </div>