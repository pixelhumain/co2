<div class="pod-docs-list">
<?php 
$docType=(isset($docType) && !empty($docType)) ? $docType : "file";
if(@$documents && !empty($documents)){
	foreach($documents as $key => $d){
		if($docType=="file"){
			$ref_ext = array(
				'jpg'=>array("icon"=>"image", "color"=>"dark"),
				'jpeg'=>array("icon"=>"image", "color"=>"dark"),
				'png'=>array("icon"=>"image", "color"=>"dark"),
				'gif'=>array("icon"=>"image", "color"=>"dark"),
				"pdf"=>array("icon"=>"file-pdf-o", "color"=>"red"),
				"xls"=>array("icon"=>"table", "color"=>"green"),
				"xlsx"=>array("icon"=>"table", "color"=>"green"),
				"doc"=>array("icon"=>"file-alt", "color"=>"azure"),
				"docx"=>array("icon"=>"file-alt", "color"=>"azure"),
				"ppt"=>array("icon"=>"file-powerpoint", "color"=>"orange"),
				"pptx"=>array("icon"=>"file-powerpoint", "color"=>"orange"),
				"odt"=>array("icon"=>"file-alt", "color"=>"azure"),
				"ods"=>array("icon"=>"table", "color"=>"green"),
				"odp"=>array("icon"=>"file-powerpoint", "color"=>"orange"),
				"csv"=>array("icon"=>"code", "color"=>"gray"));
			$ext = strtolower(pathinfo($d["name"], PATHINFO_EXTENSION));
		 	$color=(isset($ref_ext[$ext])) ? $ref_ext[$ext]["color"] : "dark";
		 	$icon=(isset($ref_ext[$ext])) ? $ref_ext[$ext]["icon"] : "file"; 
		?>	
			<div class='col-xs-12 <?= ($ref_ext[$ext]["icon"] == "image") ? "col-sm-6 col-md-4": "" ?> padding-5 shadow2 margin-top-5 margin-bottom-5' id='<?php echo $key ?>'>
			<?php if ($ref_ext[$ext]["icon"] == "image") { ?>
				<img src="<?php echo $d["docPath"] ?>" class="img-responsive">	
			<?php } ?>
			<a href='<?php echo $d["docPath"] ?>' target='_blank' class='link-files pull-left'><i class='fa fa-<?php echo $icon ?> text-<?php echo $color ?>'></i> <?php echo $d["name"] ?></a>
				<?php if($edit==true) { ?>
					<a href='javascript:;' class='pull-right text-red btn-remove-document' data-id='<?php echo $key ?>'><i class='fa fa-trash'></i> <?php echo Yii::t("common","Delete") ?></a>
				<?php } ?>
			</div>
		<?php 
		}else{ ?>
			<div class='col-xs-12 col-sm-4 padding-5 shadow2 margin-top-5 margin-bottom-5' id='<?php echo $key ?>'>
				<img src="<?php echo $d["docPath"] ?>" class="img-responsive">
				<span class="col-xs-12 elipsis"><?php echo $d["name"] ?></span>
				<?php if($edit==true) { ?>
					<a href='javascript:;' class='pull-right text-red btn-remove-document' data-id='<?php echo $key ?>'><i class='fa fa-trash'></i> <?php echo Yii::t("common","Delete") ?></a>
				<?php } ?>
			</div>
<?php	}
	} 
}else{ ?>
	<div class="sub-documents col-lg-12 col-md-12 col-sm-12">
		<span>
			<i class="fa fa-file-o margin-left-15"></i> <?php echo (($docType=="file") ? Yii::t("cooperation", "No documents"): Yii::t("common", "No images")) ?>
		</span>
	</div>
<?php 
} ?>
</div>