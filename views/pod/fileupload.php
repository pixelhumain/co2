<?php $fileUploadKey=(isset($podId)) ? $podId :"";
		$fileUploadKey.=(isset($contentId)) ? "_".$contentId : Document::IMG_PROFIL; ?>
<div class="center" id="fileuploadContainer" style="z-index: 10;">
	<form  method="post" id="<?php echo $fileUploadKey ?>_photoAdd" enctype="multipart/form-data">
	<div class="fileupload fileupload-new" data-provides="fileupload" id="<?php echo $fileUploadKey ?>_fileUpload">
		<div class="user-image">
			<div class="fileupload-new thumbnail container-fluid" id="<?php echo $fileUploadKey ?>_imgPreview">
			</div>
			<div class="fileupload-preview fileupload-exists thumbnail container-fluid" id="<?php echo $fileUploadKey ?>_imgNewPreview"></div>
			<?php
			if(@Yii::app()->session["userId"] && (!empty($edit) || 
				!empty($openEdition))){ 
				if(isset($element["profilMediumImageUrl"])) $editBtn=true; else $editBtn=false;
			?>
			<div class="user-image-buttons">
				<a class="btn btn-blue btn-file btn-upload fileupload-new btn-sm" id="<?php echo $fileUploadKey ?>_photoAddBtn" ><span class="fileupload-new">
					<i class="fa fa-<?php if($editBtn) echo "camera"; else echo "plus"?>"></i> <span class="hidden-xs userLabelBtn"><?php if($editBtn) echo Yii::t("common","Edit photo"); else echo Yii::t("common","Add a photo") ?></span></span>
					<input type="file" accept=".gif, .jpg, .png" name="<?php echo $fileUploadKey ?>_avatar" id="<?php echo $fileUploadKey ?>_avatar" data-key="<?php echo $fileUploadKey ?>">

					<input class="<?php echo $fileUploadKey ?>_isSubmit hidden" value="true"/>
				</a>
				<a href="#" class="btn btn-upload fileupload-exists btn-red btn-sm" id="<?php echo $fileUploadKey ?>_photoRemove" data-dismiss="fileupload">
					<i class="fa fa-times"></i>
				</a>
			</div>
			<div class="photoUploading" id="<?php echo $fileUploadKey ?>_photoUploading">
			</div>
			<?php } ?>
		</div>
	</div>
	</form>
</div>
<?php 
	//$defaultImgProfil=$this->module->assetsUrl.'/images/thumbnail-'.$element["collection"].'.png';
$defaultImgProfil=$this->module->assetsUrl.'/images/thumbnail-default.jpg';
 ?>
<script type="text/javascript">
	var elementId = "<?php echo (string)$element["_id"] ?>";
	var editFile = <?php echo ($edit) ? 'true':'false'; ?>;
	var elementType = "<?php echo $element["collection"] ?>";
	var resize = <?php echo (@$resize) ? 'true':'false'; ?>;
	var imageName= "";
	var imageId= "";
	var imagesPath = [];
	var elementData = <?php echo json_encode($element) ?>; 
	var imageUploader = "";
	if(typeof elementData.profilMediumImageUrl != "undefined" && notEmpty(elementData.profilMediumImageUrl))
		imageUploader = { "large" : elementData.profilImageUrl, "medium":elementData.profilMediumImageUrl};
	var itemName="<?php echo addslashes($element["name"]) ?>";
	var contentKey = "<?php echo Document::IMG_PROFIL ?>";
	function isValidUrl(urlString) {
	  var urlPattern = new RegExp(
		"^(https?:\\/\\/)?" +
		"((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|" + 
		"((\\d{1,3}\\.){3}\\d{1,3}))" + 
		"(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*" +
		"(\\?[;&a-z\\d%_.~+=-]*)?" +
		"(\\#[-a-z\\d_]*)?$",
		"i"
	  );
	  return !!urlPattern.test(urlString);
	}
	jQuery(document).ready(function() {
		initProfilImageUpload();
		initBtnUploadAction();
		mylog.log(baseUrl+"/imageTableu:");
	});

	function initBtnUploadAction(){
		var formKey = "<?php echo $fileUploadKey; ?>";
		$("#"+formKey+"_photoAdd").on('submit',function(e) {
			checkIfDashboardAccountModalOpened();

			if(debug)mylog.log("id2", elementId);
			dataSend = new FormData(this);
			console.log("dataSend",dataSend);
			$("."+formKey+"_isSubmit").val("true");
			e.preventDefault();
			updateBtnUpload(true);
			$("#"+formKey+"_imgPreview").addClass("hidden");
			extraParamsAjax={
				contentType: false,
				cache: false, 
				processData: false
			};

			ajaxPost(
		        null,
		        baseUrl+"/"+moduleId+"/document/upload-save/dir/communecter/folder/"+elementType+"/ownerId/"+elementId+"/input/"+formKey+"_avatar/contentKey/profil/docType/image",
		        dataSend,
		        function(data){ 
		            if(data.result){
			        	imagesPath.push(baseUrl+data.src);
						imageId = data.id["$id"];
						setTimeout(function(){
							updateBtnUpload(false);
							mylog.log(typeof(updateSlider));
							if(typeof(updateSlider) != "undefined" && typeof (updateSlider) == "function"){
								updateSlider(data.src, data.id["$id"]);
					  		}
					  		if(typeof(updateSliderImage) !="undefined" && typeof(updateSliderImage) == "function" && "undefined" != typeof events[elementId]){
					  			updateSliderImage(elementId, data.src);
					  		}
						}, 2000);
					    toastr.success(data.msg);
					    //met à jour l'image de myMarker (marker sur MA position)
					    //Sig.initHomeBtn();
					    //met à jour l'image de profil dans le menu principal
					    updateMenuThumbProfil();

					} else{
						updateBtnUpload(false);
						toastr.error(data.msg);
					}
			    },
			    null, null,
			    extraParamsAjax
		    );
		});
		$("#"+formKey+"_photoAddBtn").off().on("click", function(event){
  			if (!$(event.target).is('input')) {
  					$(this).find("#"+formKey+"_avatar").trigger('click');

  			}		
		});

		$('#'+formKey+'_avatar').off().on('change.bs.fileinput', function () {
			domKey=$(this).data("key");
			if($("."+domKey+"_isSubmit").val()== "true" ){
				setTimeout(function(){
					if(resize){
						$(".fileupload-preview img").addClass("img-responsive");//css("height", parseInt($("#"+formKey+"_fileUpload").css("width"))*45/100+"px");
						//$(".fileupload-preview img").css("width", "auto");
					}
					//alert(domKey+'_avatar');
					var file = document.getElementById(domKey+'_avatar').files[0];
					console.log("test",file);
					if(file && file.size < 5000000){
						$("#"+domKey+"_photoAdd").submit();

					}else{

						if(file && file.size > 5000000){
							toastr.error("<?php echo Yii::t('common','Size maximum 5Mo') ?>");
						}
						updateBtnUpload(false);
						$("#"+domKey+"_fileUpload").fileupload("clear");
					}
				}, 200);
			}else{
				setTimeout(function(){
					if(resize){
						$(".fileupload-preview img").css("max-height", "300px"/*parseInt($("#"+formKey+"_fileUpload").css("width"))*45/100+"px"*/);
						$(".fileupload-preview img").css("max-width", "100%");
					}
				}, 200);

				$("."+domKey+"_isSubmit").val("true");
			}
		   
		});

		$("#"+formKey+"_photoRemove").off().on("click", function(e){	
			$("."+formKey+"_isSubmit").val("false");
			//e.preventDefault();
			updateBtnUpload(true);
			$("#"+formKey+"_imgPreview").removeClass("hidden");
			ajaxPost(
		        null,
		        baseUrl+"/"+moduleId+"/document/delete/id/"+imageId,
		        null,
		        function(data){ 
		            if(data.result){
						setTimeout(function(){
							updateBtnUpload(false);
							toastr.success(data.msg);
							//initBtnUploadAction();
						}, 2000);

					}else{
						updateBtnUpload(false);
						toastr.error(data.error)
					}
			    }
		    );
		});
	}
	function urlWithoutSlash(url){
		var urlWithoutSlash = url.replace(/^\/+/, '');
		return urlWithoutSlash;
	};

	function initProfilImageUpload(){
		var formKey = "<?php echo $fileUploadKey; ?>";
		var j = 0;
		
		if(notEmpty(imageUploader)){
			j++;
			if(isValidUrl(imageUploader.medium) && isValidUrl(imageUploader.large)){
				imageHtml='<a href="'+urlWithoutSlash(imageUploader.large)+'" '+
				'class="thumb-info" '+  
				'data-title="<?php echo Yii::t("common","Profil image of") ?> '+elementData.name+'" '+
				'data-lightbox="all">'+
					'<img class="img-responsive" src="'+urlWithoutSlash(imageUploader.medium)+'" />'+
				'</a>';
			}else{
			imageHtml='<a href="'+baseUrl+imageUploader.large+'" '+
						'class="thumb-info" '+  
						'data-title="<?php echo Yii::t("common","Profil image of") ?> '+elementData.name+'" '+
						'data-lightbox="all">'+
							'<img class="img-responsive" src="'+baseUrl+imageUploader.medium+'" />'+
						'</a>';
			}
			$("#"+formKey+"_imgPreview").html(imageHtml);	
		}else {
			imageUrl = '<img class="img-responsive thumbnail" src="<?php echo $defaultImgProfil ?>"/>';
			j++;
			$("#"+formKey+"_imgPreview").html(imageUrl);

		}
		if(j == 0 || resize ){
			if(editFile){
				var textBlock = "<br><?php echo Yii::t('fileUpload','Click on',null,Yii::app()->controller->module->id) ?> <i class='fa fa-plus text-green'></i> <?php echo Yii::t('fileUpload','for share your pictures',null,Yii::app()->controller->module->id) ?>";
				
				var defautText = "<li>" +
									"<div class='center'>"+
										"<i class='fa fa-picture-o fa-5x text-green'></i>"+
										textBlock+
									"</div>"+
								"</li>";
				$("#"+formKey+"_imgPreview").html(defautText);
			}else{
				$("#"+formKey+"_fileUpload").css("visibility", "hidden");
			}
		}
	}
	/*************CHECK FOR DELETE ******************/
	/************* NICO 05/10/2020 *****************/
	/*function saveImage(doc, path){
		mylog.log("----------------saveImage------------------");
		ajaxPost(
	        null,
	        baseUrl+"/"+moduleId+"/document/save",
	        doc,
	        function(data){ 
	            if(data.result){
		        	imagesPath.push(baseUrl+path);
					imageId = data.id["$id"];
					setTimeout(function(){
						updateBtnUpload(false);
						mylog.log(typeof(updateSlider));
						if(typeof(updateSlider) != "undefined" && typeof (updateSlider) == "function"){
							updateSlider(path, data.id["$id"]);
				  		}
				  		if(typeof(updateSliderImage) !="undefined" && typeof(updateSliderImage) == "function" && "undefined" != typeof events[id]){
				  			updateSliderImage(id, path);
				  		}
					}, 2000);
				    toastr.success(data.msg);
				    //met à jour l'image de myMarker (marker sur MA position)
				    Sig.initHomeBtn();
				    //met à jour l'image de profil dans le menu principal
				    updateMenuThumbProfil();

				} else{
					updateBtnUpload(false);
					toastr.error(data.msg);
				}
		    }
	    );
	}*/
	//met à jour l'image de profil dans le menu principal
	function updateMenuThumbProfil(){ 
		var formKey = "<?php echo $fileUploadKey; ?>";
		mylog.log("loading new profil");
		ajaxPost(
	        null,
	        baseUrl+"/"+moduleId+"/element/getthumbpath/type/"+elementType+"/id/"+elementId,
	        null,
	        function(data){ 
	            console.log(data);
		        if(typeof data.profilThumbImageUrl != "undefined"){
		        	
		        	profilThumbImageUrl = baseUrl + data.profilThumbImageUrl;
		        	if(elementType=="citoyens")
		        		$(".menu-name-profil img").attr("src", profilThumbImageUrl);
		        	$(".identity-min img").attr("src", profilThumbImageUrl);
		        	$("#floopItem-"+elementType+"-"+elementId+" a img").attr("src", profilThumbImageUrl);
		        	$(".img-author-news-"+elementId).attr("src", profilThumbImageUrl);
		        	if(typeof contextData.map.data != "undefined" && typeof contextData.map.data[elementId] != "undefined"){
		        		contextData.map.data[elementId].profilImageUrl=data.profilImageUrl;
		        		contextData.map.data[elementId].profilThumbImageUrl=data.profilThumbImageUrl;
		        		contextData.map.data[elementId].profilMediumImageUrl=data.profilMediumImageUrl;
		        		contextData.map.data[elementId].profilMarkerImageUrl=data.profilMarkerImageUrl;
		        	}
		        }
		        if(typeof data.profilImageUrl != "undefined" && !(parseBool(localStorage.getItem("dashboardAccoundOpened")))){
		        	imageHtml='<a href="'+baseUrl + data.profilImageUrl+'" '+
							'class="thumb-info" '+  
							'data-title="<?php echo Yii::t("common","Profil image of") ?> '+elementData.name+'" '+
							'data-lightbox="all">'+
								'<img class="img-responsive" src="'+baseUrl + data.profilImageUrl+'" />'+
							'</a>';
						$("#"+formKey+"_imgNewPreview").html(imageHtml);	
		        }
		    }
	    );
	}

	function updateBtnUpload(addDisable){ 
		var formKey = "<?php echo $fileUploadKey; ?>";
		if(addDisable == true){
			$("#"+formKey+"_fileUpload").css("opacity", "0.4");
			$("#"+formKey+"_photoUploading").css("display", "block");
			$(".btn-upload").addClass("disabled");
		}else if(addDisable == false){
			$("#"+formKey+"_fileUpload").css("opacity", "1");
			$("#"+formKey+"_avatar").attr("name","avatar");
			$("#"+formKey+"_photoUploading").css("display", "none");
			$(".btn").removeClass("disabled");
		}
	}

	function checkIfDashboardAccountModalOpened(){
		if(parseBool(localStorage.getItem("dashboardAccoundOpened"))){
			elementId = userConnected._id.$id;
			elementType = userConnected.collection;
			elementData = userConnected;
		}else{
			elementId = "<?php echo (string)$element["_id"] ?>";
			elementType = "<?php echo $element["collection"] ?>";
			elementData = <?php echo json_encode($element) ?>; 
		}
	}
</script>