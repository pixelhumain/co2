<style>
    /*comment social*/
    .middle {
        text-align: center;
    }

    .btn-network {
        display: inline-block;
        width: 50px;
        height: 50px;
        background: #fff/*#f1f1f1*/;
        margin: 10px;
        border-radius: 30%;
        box-shadow: 0 5px 15px -5px #2C3E50;
        color: #1da1f2;
        overflow: hidden;
        position: relative;
        border: 1px solid #1d3b58;
    }

    a.btn-network:hover {
        transform: scale(1.3);
        color: #f1f1f1;
    }

    .btn-network i {
        line-height: 50px;
        font-size: 25px;
        transition: 0.2s linear;
    }
    .btn-network img {
        margin: 12px;
        width: 25px;
        transition: 0.2s linear;
    }

    .btn-network:hover i, .btn-network:hover img {
        transform: scale(1.3);
        color: #f1f1f1;
    }

    .fa-facebook {
        color: #3c5a99;
    }
    .fa-twitter {
        color: #1da1f2;
    }
    .fa-telegram{
        color: #55acee;
    }
    .fa-instagram {
        color: #c94d82;
    }
    .fa-github {
        color: #444;
    }
    .fa-linkedin {
        color: #1da1f2;
    }

    .btn-network::before {
        content: "";
        position: absolute;
        width: 120%;
        height: 120%;
        background: #1d3b58;
        transform: rotate(45deg);
        left: -110%;
        top: 90%;
    }

    .btn-network:hover::before {
        animation: aaa 0.7s 1;
        top: -10%;
        left: -10%;
    }

    .containerBox{
        box-shadow: 0 2px 5px 0 rgb(63, 78, 88), 0 2px 10px 0 rgb(63, 78, 88);
        padding-bottom: 20px;
        max-height: 500px;
        overflow-y: auto;
    }

    .containerBox::-webkit-scrollbar-thumb {
        border-radius: 10px;
        background-color: #828282;
        background-image: -webkit-linear-gradient(90deg, transparent, rgba(255, 255, 255, 0.4) 50%, transparent, transparent);
    }

    .containerBox::-webkit-scrollbar-thumb:hover {
        background-color: #6d861b;
        background-image: -webkit-linear-gradient(90deg, transparent, rgba(255, 255, 255, 0.4) 50%, transparent, transparent)
    }

    .containerBox::-webkit-scrollbar {
        width: 8px;
        background-color: #F5F5F5;
        transition: width .3s ease-in;
    }

    @keyframes aaa {
        0% {
            left: -110%;
            top: 90%;
        }
        50% {
            left: 10%;
            top: -30%;
        }
        100% {
            left: -10%;
            top: -10%;
        }
    }
</style>
<?php
    if($type!=Poi::COLLECTION){
    $gitlab = (!empty($element["socialNetwork"]["gitlab"])? $element["socialNetwork"]["gitlab"]:"javascript:;") ;
    //$telegram =  (!empty($element["socialNetwork"]["telegram"])? "https://web.telegram.org/#/im?p=@".$element["socialNetwork"]["telegram"]:"javascript:;") ;
    $telegram =  (!empty($element["socialNetwork"]["telegram"])? $element["socialNetwork"]["telegram"]:"javascript:;") ;
    $diaspora =  (!empty($element["socialNetwork"]["diaspora"])? $element["socialNetwork"]["diaspora"]:"javascript:;") ;
    $mastodon =  (!empty($element["socialNetwork"]["mastodon"])? $element["socialNetwork"]["mastodon"]:"javascript:;") ;
    $facebook = (!empty($element["socialNetwork"]["facebook"])? $element["socialNetwork"]["facebook"]:"javascript:;") ;
    $twitter =  (!empty($element["socialNetwork"]["twitter"])? $element["socialNetwork"]["twitter"]:"javascript:;") ;
    $github =  (!empty($element["socialNetwork"]["github"])? $element["socialNetwork"]["github"]:"javascript:;") ;
    $instagram =  (!empty($element["socialNetwork"]["instagram"])? $element["socialNetwork"]["instagram"]:"javascript:;") ;
    $signal =  (!empty($element["socialNetwork"]["signal"])? $element["socialNetwork"]["signal"]:"javascript:;") ;
    $linkedin =  (!empty($element["socialNetwork"]["linkedin"])? $element["socialNetwork"]["linkedin"]:"javascript:;") ;
    ?>

        <div class="well containerBox co-scroll">
            <div class="margin-5" >
                <span class="text-nv-3 tl-network">
                    <i class="fa fa-connectdevelop"></i>
                    <?php echo Yii::t("common","Socials"); ?>
                </span>
                <?php if($edit==true || ( $openEdition==true && Yii::app()->session["userId"] != null ) ) {?>
                <button class="btn-update-network btn btn-update-ig pad-2 pull-right tooltips"
                        data-toggle="tooltip" data-placement="top" title="" alt="" data-original-title="<?php echo Yii::t("common","Update network") ?>">
                    <i class="fa fa-edit"></i>
                </button>
                <?php } ?>
                <hr class="line-hr hr-net" style="margin-bottom: 10px;">

                <div class="container-fluid" id="cont">
                    <div class="row">
                        <div class="col-sm-12 no-padding">
                            <div class="middle">
                                <span id="divDiaspora">
                                    <?php if ($diaspora != "javascript:;"){ ?>
                                        <a href="<?php echo $diaspora ; ?>" target="_blank" id="diasporaAbout" class="contentInformation btn-network tooltips" data-toggle="tooltip" data-placement="top" title="Diaspora">
                                            <img src="<?php echo Yii::app()->getModule("co2")->assetsUrl; ?>/images/diaspora_icon.png">
                                        </a>
                                    <?php } ?>
                                </span>

                                <span id="divMastodon">
                                    <?php if ($mastodon != "javascript:;"){ ?>
                                        <a href="<?php echo $mastodon ; ?>" target="_blank" id="mastodonAbout"  class="contentInformation btn-network tooltips" data-toggle="tooltip" data-placement="top" title="Mastodon">
                                            <img src="<?php echo Yii::app()->getModule("co2")->assetsUrl; ?>/images/mastodon.png">
                                        </a>
                                    <?php } ?>
                                </span>

                                <span id="divFacebook">
                                    <?php if ($facebook != "javascript:;"){ ?>
                                        <a class="contentInformation btn-network tooltips" href="<?php echo $facebook ; ?>" target="_blank" id="facebookAbout" data-toggle="tooltip" data-placement="top" title="Facebook">
                                            <i class="fa fa-facebook"></i>
                                        </a>
                                    <?php } ?>
                                </span>

                                <span id="divTwitter">
                                    <?php if ($twitter != "javascript:;"){ ?>
                                        <a class="contentInformation btn-network tooltips" href="<?php echo $twitter ; ?>" target="_blank" id="twitterAbout" data-toggle="tooltip" data-placement="top" title="Twitter">
                                            <i class="fa fa-twitter"></i>
                                        </a>
                                    <?php } ?>
                                </span>

                                <span id="divInstagram">
                                    <?php if ($instagram != "javascript:;"){ ?>
                                        <a class="contentInformation btn-network tooltips" href="<?php echo $instagram ; ?>" target="_blank" id="instagramAbout" data-toggle="tooltip" data-placement="top" title="Instagram">
                                            <i class="fa fa-instagram"></i>
                                        </a>
                                    <?php } ?>
                                </span>

                                <span id="divSignal">
                                    <?php if ($signal != "javascript:;"){ ?>
                                        <a class="contentInformation btn-network tooltips" href="<?php echo $signal ; ?>" target="_blank" id="signalAbout" data-toggle="tooltip" data-placement="top" title="Signal">
                                            <img src="<?php echo Yii::app()->getModule("co2")->assetsUrl; ?>/images/signal_icon.png" >
                                        </a>
                                    <?php } ?>
                                </span>

                                <span id="divLinkedin">
                                    <?php if ($linkedin != "javascript:;"){ ?>
                                        <a class="contentInformation btn-network tooltips" href="<?php echo $linkedin ; ?>" target="_blank" id="linkedinAbout" data-toggle="tooltip" data-placement="top" title="LinkedIn">
                                        <i class="fa fa-linkedin"></i>
                                        </a>
                                    <?php } ?>
                                </span>

                                <span id="divGithub">
                                    <?php if ($github != "javascript:;"){ ?>
                                        <a class="contentInformation btn-network tooltips" href="<?php echo $github ; ?>" target="_blank" id="githubAbout" data-toggle="tooltip" data-placement="top" title="Github">
                                            <i class="fa fa-github"></i>
                                        </a>
                                    <?php } ?>
                                </span>

                                <span id="divGitlab">
                                    <?php if ($gitlab != "javascript:;"){ ?>
                                        <a href="<?php echo $gitlab ; ?>" class="contentInformation btn-network tooltips"  target="_blank" id="gitlabAbout" data-toggle="tooltip" data-placement="top" title="Gitlab">
                                            <img src="<?php echo Yii::app()->getModule("co2")->assetsUrl; ?>/images/gitlab_icon.png" >
                                        </a>
                                    <?php } ?>
                                </span>

                                <?php //if($type==Person::COLLECTION){ ?>
                                    <span id="divTelegram">
                                        <?php if ($telegram != "javascript:;"){ ?>
                                            <a href="<?php echo $telegram ; ?>" target="_blank" id="telegramAbout" class="contentInformation btn-network tooltips" data-toggle="tooltip" data-placement="top" title="Telegram">
                                                <i class="fa fa-telegram"></i>
                                            </a>
                                        <?php } ?>
                                    </span>
                                <?php //} ?>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
     
<?php } ?>