<?php
/*
sliderGeneric -> slider de n'importe quel élément "carousable"*
- $idCarousel => id unique de la div carousel pour pouvoir en mettre plusieurs sur la meme page
- $nbItem => nb items du carousel pour générer les carousel-indicators (a voir si générable à la volée)

--------------------------
Usage : 
1) dans le code init de la page :
	$this->renderPartial('../pod/sliderGeneric',array("idCarousel"=>"articleCarousel","nbItem"=>count($articles_une))); 
2) on injecte n'importe quel liste d'éléments "carousable"*
	$("#articleCarousel .carousel-inner").html(showResultsDirectoryHtml(articles_une,"article",null,null,null));
3) on active l'animation du carousel :
	start_articleCarousel();  

--------------------------
*Carousable : cad dont le articlePanelHtml gère le fait que l'élèment puisse être de class item et active
	var carousable="";
  	if(typeof params.carousable  != "undefined" && (params.carousable))
    	carousable=" item";

  	var carousel_active="";
  	if(typeof params.carousel_active  != "undefined" && (params.carousel_active))
    	carousel_active=" active";

*/

if(!isset($idCarousel)||(empty($idCarousel))){
	$idCarousel="genericCarousel";
}

if(isset($nbItem)&&($nbItem>0)){
	if(!isset($carousel_indicators_color)||(empty($carousel_indicators_color))){
		$carousel_indicators_color="#999999";
	}
?>

<style type="text/css">
	.carousel-generic > ol > li.active{	  
	  background-color: <?php echo $carousel_indicators_color; ?>;
	  border: 1px solid <?php echo $carousel_indicators_color; ?>;
	}
	.carousel-generic > ol > li{
	  background-color: #cccccc;
	  border: 1px solid <?php echo $carousel_indicators_color; ?>;
	}
	
	.carousel-generic .carousel-indicators{
		bottom : 0px;	
	}
	.carousel-generic{
		overflow-y: auto;
	}
	.carousel-generic .carousel-inner{
		overflow-y: auto;
		margin-bottom : 20px;
	}

</style>

<div id="<?php echo $idCarousel; ?>" class="col-md-12 no-padding carousel carousel-generic slide" data-ride="carousel">
	
	<!-- Wrapper for slides -->
	<div class="carousel-inner">
	</div>

	<!-- Indicators -->
	<ol class="carousel-indicators">
		<?php 		
		for($i=0;$i<$nbItem;$i++){ 
		?>
				 <li data-target="#<?php echo $idCarousel; ?>" data-slide-to="<?php echo $i; ?>" class="<?php if($i==0) echo "active"; ?>"></li>
		<?php 
		} //todo puce
		?>
	</ol>

</div>
<script type="text/javascript">
	function start_<?php echo $idCarousel; ?>() {
		$("#<?php echo $idCarousel; ?>").carousel();
		// Enable Carousel Indicators
		$("#<?php echo $idCarousel; ?> > ol.carousel-indicators > li").click(function(){
		    $("#<?php echo $idCarousel; ?>").carousel($(this).data("slide-to"));
		});
	};
</script>

<?php 
} else {
?>
<script type="text/javascript">
	function start_<?php echo $idCarousel; ?>() {
		//alert("Pas de contenu à afficher");
	};
</script>
<?php
}
?>