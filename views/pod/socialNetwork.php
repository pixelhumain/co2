<style type="text/css">
    .dont-break-out a{
        color: #ff9800;
    }
    
    .dont-break-out a:hover{
        color: #4caf50;
    }
</style>
<!-- ancien ergo -->
<?php
    if($type!=Poi::COLLECTION){
    $gitlab = (!empty($element["socialNetwork"]["gitlab"])? $element["socialNetwork"]["gitlab"]:"javascript:;") ;
    $telegram =  (!empty($element["socialNetwork"]["telegram"])? "https://web.telegram.org/#/im?p=@".$element["socialNetwork"]["telegram"]:"javascript:;") ;
    $diaspora =  (!empty($element["socialNetwork"]["diaspora"])? $element["socialNetwork"]["diaspora"]:"javascript:;") ;
    $mastodon =  (!empty($element["socialNetwork"]["mastodon"])? $element["socialNetwork"]["mastodon"]:"javascript:;") ;
    $facebook = (!empty($element["socialNetwork"]["facebook"])? $element["socialNetwork"]["facebook"]:"javascript:;") ;
    $twitter =  (!empty($element["socialNetwork"]["twitter"])? $element["socialNetwork"]["twitter"]:"javascript:;") ;
    $gitlab =  (!empty($element["socialNetwork"]["gitlab"])? $element["socialNetwork"]["gitlab"]:"javascript:;") ;
    $github =  (!empty($element["socialNetwork"]["github"])? $element["socialNetwork"]["github"]:"javascript:;") ;
    $instagram =  (!empty($element["socialNetwork"]["instagram"])? $element["socialNetwork"]["instagram"]:"javascript:;") ;
    ?>
    <div class="panel ibox no-padding panel-SocialNetwork pod-info-SocialNetwork" id="pod-info-SocialNetwork">
        <div class="panel-heading" style="border-color: #4caf50 ">
            <span class="text-nv-3 text-dark-green">
                <i class="fa fa-connectdevelop"></i>
                <?php echo Yii::t("common","Socials"); ?>
            </span>
            <?php if($edit==true || ( $openEdition==true && Yii::app()->session["userId"] != null ) ) {?>
            <button class="btn-update-network btn btn-update-dark-green pad-2 pull-right tooltips"
                    data-toggle="tooltip" data-placement="top" title="" alt="" data-original-title="<?php echo Yii::t("common","Update network") ?>">
                <i class="fa fa-edit"></i>
            </button>
            <?php } ?>

        </div>
        <div class="panel-body social bg-white" style="padding-top: 0px!important;">
            <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12 desc-network">
                <div class="col-md-2 col-sm-2 col-xs-2">
                    <span><img src="<?php echo Yii::app()->getModule("co2")->assetsUrl; ?>/images/diaspora_icon.png" width="30px" heigt="30px"></span>
                </div>
                <div class="contentInformation col-md-10 col-sm-10 col-xs-10">
                    <span class="dont-break-out" id="divDiaspora">
                        <?php if ($diaspora != "javascript:;"){ ?>
                            <a href="<?php echo $diaspora ; ?>" target="_blank" id="diasporaAbout" class="socialIcon "><?php echo  $diaspora ; ?></a>
                        <?php } else {
                            echo '<i>'.Yii::t("common","Not specified").'</i>' ;
                        } ?>
                    </span>
                </div>
            </div>

            <div class="col-md-12 col-sm-12 col-xs-12 desc-network">
                <div class="col-md-2 col-sm-2 col-xs-2">
                    <span><img src="<?php echo Yii::app()->getModule("co2")->assetsUrl; ?>/images/mastodon_icon.png" width="30px" heigt="30px"></span>
                </div>
                <div class="contentInformation col-md-10 col-sm-10 col-xs-10">
                    <span class="dont-break-out" id="divMastodon" >
                        <?php if ($mastodon != "javascript:;"){ ?>
                            <a href="<?php echo $mastodon ; ?>" target="_blank" id="mastodonAbout" class="socialIcon "><?php echo  $mastodon ; ?></a>
                        <?php } else {
                            echo '<i>'.Yii::t("common","Not specified").'</i>' ;
                        } ?>
                    </span>
                </div>
            </div>

            <div class="col-md-12 col-sm-12 col-xs-12 desc-network">
                <div class="col-md-2 col-sm-2 col-xs-2">
                    <span class="badge font-size-20 fb-color"><i class="fa fa-facebook"></i></span>
                </div>
                <div class="contentInformation col-md-10 col-sm-10 col-xs-10">
                    <span class="dont-break-out"  id="divFacebook">
                        <?php if ($facebook != "javascript:;"){ ?>
                            <a href="<?php echo $facebook ; ?>" target="_blank" id="facebookAbout" class="socialIcon "><?php echo  $facebook ; ?></a>
                        <?php } else {
                            echo '<i>'.Yii::t("common","Not specified").'</i>' ;
                        } ?>
                    </span>
                </div>
            </div>

            <div class="col-md-12 col-sm-12 col-xs-12 desc-network">
                <div class="col-md-2 col-sm-2 col-xs-2">
                    <span class="badge font-size-20 twitter-color"><i class="fa fa-twitter"></i></span>
                </div>
                <div class="contentInformation col-md-10 col-sm-10 col-xs-10">
                    <span class="dont-break-out" id="divTwitter">
                        <?php if ($twitter != "javascript:;"){ ?>
                            <a href="<?php echo $twitter ; ?>" target="_blank" id="twitterAbout" class="socialIcon" ><?php echo $twitter ; ?></a>
                        <?php } else {
                            echo '<i>'.Yii::t("common","Not specified").'</i>' ;
                        } ?>
                    </span>
                </div>
            </div>

            <div class="col-md-12 col-sm-12 col-xs-12 desc-network">
                <div class="ccol-md-2 col-sm-2 col-xs-2">
                    <span class="badge font-size-20 instagram-color"><i class="fa fa-instagram"></i></span>
                </div>
                <div class="contentInformation col-md-10 col-sm-10 col-xs-10">
                    <span class="dont-break-out" id="divInstagram">
                        <?php if ($instagram != "javascript:;"){ ?>
                            <a href="<?php echo $instagram ; ?>" target="_blank" id="instagramAbout" class="socialIcon" ><?php echo $instagram ; ?></a>
                        <?php } else {
                            echo '<i>'.Yii::t("common","Not specified").'</i>' ;
                        } ?>
                    </span>
                </div>
            </div>

            <div class="col-md-12 col-sm-12 col-xs-12 desc-network">
                <div class="col-md-2 col-sm-2 col-xs-2">
                    <span class="badge font-size-20 git-color"><i class="fa fa-github"></i></span>
                </div>
                <div class="contentInformation col-md-10 col-sm-10 col-xs-10">
                    <span class="dont-break-out" id="divGithub">
                        <?php if ($github != "javascript:;"){ ?>
                            <a href="<?php echo $github ; ?>" target="_blank" id="githubAbout" class="socialIcon" ><?php echo $github  ; ?></a>
                        <?php } else {
                            echo '<i>'.Yii::t("common","Not specified").'</i>' ;
                        } ?>
                    </span>
                </div>
            </div>

            <div class="col-md-12 col-sm-12 col-xs-12 desc-network">
                <div class="col-md-2 col-sm-2 col-xs-2">
                    <span><img src="<?php echo Yii::app()->getModule("co2")->assetsUrl; ?>/images/gitlab_icon.png" width="30px" heigt="30px"></span>
                </div>
                <div class="contentInformation col-md-10 col-sm-10 col-xs-10">
                    <span class="dont-break-out" id="divGitlab">
                        <?php if ($gitlab != "javascript:;"){ ?>
                            <a href="<?php echo $gitlab ; ?>" target="_blank" id="gitlabAbout" class="socialIcon" ><?php echo $gitlab  ; ?></a>
                        <?php } else {
                            echo '<i>'.Yii::t("common","Not specified").'</i>' ;
                        } ?>
                    </span>
                </div>
            </div>

                <?php if($type==Person::COLLECTION){ ?>
                <div class="col-md-12 col-sm-12 col-xs-12 desc-network">
                    <div class="col-md-2 col-sm-2 col-xs-2">
                        <span class="badge font-size-20 twitter-color"><i class="fa fa-telegram"></i></span>
                    </div>
                    <div class="contentInformation col-md-10 col-sm-10 col-xs-10">
                        <span class="dont-break-out" id="divTelegram">
                            <?php if ($telegram != "javascript:;"){ ?>
                                <a href="<?php echo $telegram ; ?>" target="_blank" id="telegramAbout" class="socialIcon" ><?php echo $telegram ; ?></a>
                            <?php } else {
                                echo '<i>'.Yii::t("common","Not specified").'</i>' ;
                            } ?>
                        </span>
                    </div>
                </div>
                <?php } ?>

        </div>
    </div>
<?php } ?>