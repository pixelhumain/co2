<?php 

	HtmlHelper::registerCssAndScriptsFiles([
		'/css/coInput/co-input.css',
		'/js/coInput/co-input.js',
		'/js/coInput/co-input-types-css.js'
	], $this->module->getParentAssetsUrl() );

	$categoryTitle = [
		"site" => Yii::t("common","Site"),
		"chat" => Yii::t("common","Chat between members"),
		"agenda" => Yii::t("common","Events calendar"),
		"projectManagement" => Yii::t("common","Project management"),
		"videoPlatform" => Yii::t("common","Video platform"),
		"cloud" => Yii::t("common","Cloud"),
		"annuaire" => Yii::t("common","Directory"),
		"fileSharing" => Yii::t("common","File sharing and online editing"),
		"survey" => Yii::t("common","Poll / survey"),
	]
?>

<style>

    /* modal createevent */
	.modal-edit-tools {
		background-color: #423e3ebe;
		margin-top: 30px;
	}

 
	.modal-edit-tools .modal-body,.modal-footer{
		background-color: rgb(252, 252, 252);
	}
	

	.modal-edit-tools .modal-header {
		display: flex;
		align-items: center;
		justify-content: space-around;
		padding-bottom: 0;
		background-color: #3f4e58;
		position: relative;
		padding-bottom: 15px;
		color: white;
	}

	.modal-edit-tools .modal-header button.close {
		position: absolute;
		right: 15px;
		top: 18px;
		opacity: .75;
	}

	.modal-edit-tools .modal-header::before,
	.modal-edit-tools .modal-header::after {
		content: none;
	}

	.modal-edit-tools .modal-header .tab-item {
		margin: 2px 5px 0px 5px;
		padding-bottom: 10px;
		cursor: pointer;
		text-transform: none;
		text-align: center;
		font-size: 17px;
		text-decoration: none;
	}

	.modal-edit-tools .modal-body .link-to-doc-content {
		display: flex;
		align-items: center;
		justify-content: flex-end;
	}

	.modal-edit-tools .modal-body a.link-to-doc {
		background-color: #1890FF;
		padding: 8px 15px;
		font-weight: bold;
		color: white;
		border-top-right-radius: 10px;
		border-top-left-radius: 10px;
		font-size: 1.38rem;
	}

	.modal-edit-tools .modal-body a.link-to-doc i {
		margin-right: 5px;
	}

	.modal-edit-tools .modal-header a:hover,
	.modal-edit-tools .modal-header a.active {
		border-bottom: 1px solid #f3aa31;
	}

	.modal-edit-tools .btn-submit {
		display: flex;
		align-items: flex-start;
		justify-content: space-between;
	}

	.modal-edit-tools .btn-submit p {
		font-size: 13px;
		text-align: justify;
		font-weight: 400;
		flex: 0.70;
	}

	.modal-edit-tools .btn-submit p a,
	.modal-edit-tools .btn-submit p b {
		font-weight: bold;
	}

	.modal-edit-tools .btn-submit p a {
		color: #1890FF;
	}

	.modal-edit-tools #tabQuestion h5 {
		text-align: center;
		font-size: 14px;
		text-transform: none;
		font-weight: 500;
	}

	.modal-edit-tools #tabQuestion h5 a {
		font-weight: bold;
	}

	.modal-edit-tools .btn-submit button {
		min-width: 100px;
	}

	.modal-edit-tools .form-group textarea {
		height: 150px;
	}

	.modal-edit-tools .form-group .check-value {
		display: flex;
		align-items: center;
		justify-content: flex-start;
	}

	.modal-edit-tools .form-group .check-value .form-check:not(:first-child) {
		margin-left: 10px;
	}

	.modal-personnalise {
		width: 60% !important; 
		height: 80vh; 
	}

	.modal-personnalise-project{
		width: 40%; 
		height: 80vh; 
	}

    @media only screen and (max-width: 768px) {
		.modal-dialog.modal-personnalise.modal-dialog-centered.co-resposnive {
			width: auto !important;
		}

		.modal-dialog.modal-personnalise-project.modal-dialog-centered.co-resposnive {
			width: auto !important;
		}
	}

	.inputContainerCoTools {
		background-color: #3f4e58;
		color : white;
		padding: 10px 20px;
		border-radius: 8px;
		display: flex;
		flex-direction: column;
	}

	.inputContainerCoTools label {
		color: white;
	}

	.inputContainerCoTools  textarea{
		background-color: #3f4e58 ;
		color : white ;
	}

	.inputContainerCoTools button:not(.btn-danger) {
    	background-color: #9fbd38 !important;
	}

	.inputContainerCoTools  input {
		background-color: white ;
		color : black ;
	}

	
	.tools-container {
		display: flex;
		flex-wrap: wrap;
		width: 100%;
		border-bottom: 1px solid #9fbd38;
	}

	.tool-category {
		display: flex;
		font-size: 15px;
		font-weight: bold;
		color: #333;
		justify-content: center;
		align-items: center;
	}

	
	.tool-item .contentInformation{
		background-color:#93C020;
	}

	.tool-item .btn-network{
		margin: 5px;
	}

	.middle ul {
		padding: 0px;
	}
	
    
</style>
<?php
    if($type!=Poi::COLLECTION){?>
		 	<div class="well containerBox">
			 	<div class="margin-5" >
					<span class="text-nv-3 tl-network">
						<i class="fa fa-connectdevelop"></i>
						<?php echo Yii::t("common","Our tools"); ?>
					</span>
					<?php if($edit==true || ( $openEdition==true && Yii::app()->session["userId"] != null ) ) {?>
					<button class="btn-update-tools btn btn-update-ig pad-2 pull-right tooltips"
							data-toggle="tooltip" data-placement="top" title="" alt="" data-original-title="<?php echo Yii::t("common","Update tools") ?>">
						<i class="fa fa-edit"></i>
					</button>
					<?php } ?>
					<hr class="line-hr hr-net" style="margin-bottom: 10px;">

					<div class="container-fluid" id="cont">
						<div class="row">
							<div class="col-sm-12 no-padding">
								<div class="middle">
									<ul id="yourTools" class="tools-container">
										<?php foreach ($element["ourTools"] ?? [] as $category => $tools) : ?>
											
											<li class="tools-container">
												<div class="tool-category"><?= htmlspecialchars($categoryTitle[$category] ?? ucfirst($category)) ?> :</div>
												<?php foreach ($tools as $tool) : ?>
													<div class="tool-item">
														<a href="<?= htmlspecialchars($tool['url'] ?? "") ?>" target="_blank" 
															class="contentInformation btn-network tooltips" 
															data-toggle="tooltip" data-placement="top" 
															title="<?= htmlspecialchars($tool['name']) ?>">
															
															<img src="https://www.google.com/s2/favicons?sz=64&domain=<?= parse_url($tool['url'] ?? "", PHP_URL_HOST) ?>" 
																	alt="Icon" class="tool-icon" 
																	onerror="this.onerror=null; this.style.display='none'; this.insertAdjacentHTML('afterend', '<i class=\'fa fa-link gradient-fill\'></i>');">
														</a>
													</div>
												<?php endforeach; ?>
											</li>
										<?php endforeach; ?>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

            <div class="modal fade modal-edit-tools" id="openModalEditTools" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" style="z-index: 999999;overflow-y: auto;">
				<div class="modal-dialog modal-personnalise modal-dialog-centered co-resposnive" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title"> Specifications des vos outils </h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body col-xs-12">
							<div class="row">
								<div class="project-container col-xs-12 col-md-12 col-lg-12">
									<div id="edit-tools-input"></div>
								</div>
							</div>
						</div>
						<div class="modal-footer col-xs-12">
							<button class="save saveTools" ><?= Yii::t('cms', 'Save') ?></button>
						</div>
					</div>
				</div>
			</div>
     
    <?php } ?>
<script>

	function updateToolsData(toolsData) {
		$("#yourTools").empty();  // Vider le conteneur avant de le remplir

		// Boucle sur les catégories
		$.each(toolsData, function (category, tools) {
			var categoryTitle = <?= json_encode($categoryTitle) ?>;
			var categoryName = categoryTitle[category] ?? category.charAt(0).toUpperCase() + category.slice(1);

			// Création de l'élément de catégorie
			var $categoryItem = $(`
				<li class="tools-container">
					<div class="tool-category">${categoryName} :</div>
				</li>
			`);

			// Boucle sur chaque outil
			$.each(tools, function (_, tool) {
				if (tool.url && isValidUrl(tool.url)) {
					const faviconUrl = `https://www.google.com/s2/favicons?sz=64&domain=${new URL(tool.url).hostname}`;
					var toolItem = `
						<div class="tool-item">
							<a href="${tool.url}" target="_blank" class="contentInformation btn-network tooltips" data-toggle="tooltip" data-placement="top" title="${tool.name}">
								<img src="${faviconUrl}" alt="Icon" class="tool-icon">
							</a>
						</div>
					`;
				} else {
					var toolItem = `
						<div class="tool-item">
							<a href="${tool.url}" target="_blank" class="contentInformation btn-network tooltips" data-toggle="tooltip" data-placement="top" title="${tool.name}">
								<i class="fa fa-link gradient-fill"></i> 
							</a>
						</div>
					`;
				}

				$categoryItem.append(toolItem);
			});

			// Ajouter la liste des outils à la catégorie
			$("#yourTools").append($categoryItem);
		});

		// Activer les tooltips
		$('[data-toggle="tooltip"]').tooltip();
	}

    // Fonction pour valider si l'URL est correcte
    function isValidUrl(url) {
        try {
            new URL(url);  // Essayer de créer une URL
            return true;
        } catch (e) {
            return false;  // Si l'URL n'est pas valide, on retourne false
        }
    }

	var ourToolsValue = {};

	<?php if( isset($element["ourTools"]) ): ?>
		var ourToolsValue = <?= json_encode($element["ourTools"]) ?>;
	<?php endif; ?>

	$(".btn-update-tools").on("click", function () {
		new CoInput({
			container: "#edit-tools-input",
			inputs: [
				<?php 
					$categories = [
						"site" => "Site",
						"chat" => "Chat between members",
						"agenda" => "Events calendar",
						"projectManagement" => "Project management",
						"videoPlatform" => "Video platform",
						"annuaire" => "Directory",
						"fileSharing" => "File sharing and online editing",
						"survey" => "Poll / survey",
						"cloud" => "Cloud"
					];
					foreach ($categories as $key => $label) : 
				?>
				{
					type: "inputMultiple",
					options: {
						name: "<?= $key ?>",
						label: "<?= Yii::t('common', $label) ?>",
						class: "inputContainerCoTools",
						max: 5,
						defaultValue: ourToolsValue["<?= $key ?>"] ?? [],
						inputs: [
							[
								{
									type: "inputSimple",
									options: {
										name: "name",
										label: "Name",
										defaultValue: "",
										icon: "chevron-down",
										class: "inputContainerCoTools"
									}
								},
								{
									type: "inputSimple",
									options: {
										name: "url",
										label: "URL",
										defaultValue: "",
										icon: "chevron-down",
										class: "inputContainerCoTools"
									}
								}
							]
						]
					}
				},
				<?php endforeach; ?>
			],
			onchange: function(name, value, payload) {
				jsonHelper.setValueByPath(ourToolsValue, name, value);
			},
			onblur: function(name, value, payload) {
				jsonHelper.setValueByPath(ourToolsValue, name, value);
			}
		});

   		 $("#openModalEditTools").modal("show");

		$(".saveTools").off("click").on("click", function () {
			if (!contextData["type"] || !contextData["id"]) {
				toastr.error("Erreur : Impossible de sauvegarder les outils, données manquantes.");
				return;
			}

			var editTools = {
				value : ourToolsValue,
				collection: contextData["type"],
				id: contextData["id"],
				path: "ourTools"
			};

			dataHelper.path2Value(editTools, function (params) {
				toastr.success(tradCms.editionSucces);
				updateToolsData(ourToolsValue);
				$("#openModalEditTools").modal("hide");
			});
		});
});



</script>