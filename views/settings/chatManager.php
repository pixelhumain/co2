<style type="text/css">
	.box-title-chat h4 {
	    position:sticky;
	    padding: 10px 20px;
	    margin: 10px 0px;
	    margin-left: -30px;
	    margin-right: -30px;
	    font-size: 25px;
	    line-height:32px;
	    font-weight:bold;
	    color:#fff;
	    background:#0095ff;
	    /* css3 extras */
	    text-shadow:0 1px 1px rgba(0,0,0,0.2);
	    -webkit-box-shadow:0 1px 1px rgba(0,0,0,0.2);
	       -moz-box-shadow:0 1px 1px rgba(0,0,0,0.2);
	            box-shadow:0 1px 1px rgba(0,0,0,0.2);
	    /* See "NOTE 1" */
	    zoom:1;
	}
	.box-icon {
	    background-color: #fff;
	    border-radius: 50%;
	    display: table;
	    height: 70px;
	    margin: 0 auto;
	    width: 70px;
	    margin-top: -35px;
	}
	.box-icon span {
	    color: #03426f;
	    display: table-cell;
	    text-align: center;
	    vertical-align: middle;
	    /*border: 3px solid #3f4e58;*/
	    border-radius: 50%;
	    box-shadow: 0 2px 5px 0 #03426f, 0 2px 10px 0 #03426f;
	}
	@media (max-width: 767px) {
		.box-icon span{
		    font-size: 2em;
		}
		.box-icon {
		    height: 55px;
		    width: 55px;
		    margin-top: -28px;
		}
		.settings-header h3{
			font-size: 20px;
			padding: 0px 10px;	
	    	margin-top: 25px;
		}
	}
	.settings-header {
	    text-align: center;
	    color: #03426f;
	    margin-top: 45px;
	    padding: 0px 10px;
	}
	.box-title-chat h4:before, .box-title-chat h4:after {
	    content: "";
	    position: absolute;
	    z-index: -1;
	    top: 100%;
	    left: 0;
	    border-width: 0 14px 14px 0;
	    border-style: solid;
	    border-color: transparent #03426f;
	}
	.box-title-chat h4:after {
		right: 0;
    	left: auto;
    	border-width: 0 0 14px 14px;
	}
	.destacados{
	    margin-top: -77px;
	}
	.ico-after {
	    width: 26px;
	    height: 26px;
	    font-size: 16px;
	    margin-top: 17px;
	    right: -3px;
	    text-align: center;
	    margin-right: 25px;
	    color: #fff;
	    position: absolute;
	    /* float: right; */
	    padding: 3px;
	    line-height: 0px;
	    background-color: #24284D;
	    border: 1px solid #24284D;
	    border-radius: 50%;
	    -webkit-transition: all .2s ease-in-out;
	    -o-transition: all .2s ease-in-out;
	    transition: all .2s ease-in-out;
	}

	.interne-chat,.externe-chat {
		left: 0;
		-webkit-transition: all 0.5s ease-in-out;
	    -moz-transition: all 0.5s ease-in-out;
	    -o-transition: all 0.5s ease-in-out;
	    transition: all 0.5s ease-in-out;
	}
	.interne-chat .btn-open-chatEl, .externe-chat .btn-open-chatExt{
		width: 92%;
		font-weight: 700;
		margin: 10px;
		padding-left: 25px;
    	padding-right: 25px;
	}
	.interne-chat .chanel-name, .externe-chat .chanel-name {
		overflow-wrap: break-word;
    	word-wrap: break-word;
    	white-space: normal;
	}

	.interne-chat .btn-u-icon, .externe-chat .btn-u-icon { 
		background: #0a93f8;
	    margin-right: 4px;
	    text-align: center;
	    width: 45px;
	    height: 45px;
	    margin-left: -10px;
	    border: 3px solid #24284d;
	    font-size: 29px;
	    line-height: 39px;
	    color: #ffffff;
	    text-transform: uppercase;
	    position: absolute;
	    top: 16px;
	    font-weight: 700;
	}
	.interne-chat .icon-btn-chat, .externe-chat .icon-btn-chat { 
		/*padding: 2px;*/
	    border-radius: 5px;
	    text-align: center;
	    line-height: 22px;
	    min-height: 41px;  
	}
	.interne-chat a.btn-primary, .externe-chat  a.btn-primary {
		background-color: #ffffff;
		color: #0a93f8;
	}
	.interne-chat:hover, .externe-chat:hover {
		left: 15px;
    	padding-left: -15px;
	}
	.interne-chat:hover a.icon-btn-chat, .externe-chat:hover a.icon-btn-chat {
		background-color: #5fad88;
		color: #fff;
	}
	.interne-chat:hover .btn-u-icon, .externe-chat:hover .btn-u-icon {
		background-color: #fff;
		color: #5fad88;
	}
	a.icon-btn-chat:hover span, a.icon-btn-chat:active span, a.icon-btn-chat:hover focus {
		color: #ffffff;
	}

	.button-wrap{
	    padding: 10px 25px;
	    color: #0a93f8;
	    background-color: #fff;
	    border: 1px solid transparent;
	    box-shadow: 0 3px 3px 0 rgba(1,1,1,.55);
	    -webkit-transition: .5s cubic-bezier(.22,.61,.36,1);
	    -moz-transition: .5s cubic-bezier(.22,.61,.36,1);
	    transition: .5s cubic-bezier(.22,.61,.36,1);
	    position: relative;
	    display: inline-block;
	    margin: 15px auto;
	    text-transform: uppercase;
	    border-radius: 5px;
	    font-size: 15px;
	    font-weight: 700
	}

	.button-wrap:hover {
	    -webkit-filter: drop-shadow(3px 3px 20px #fff);
	    filter: drop-shadow(3px 3px 20px #fff);
	    color: #24284D;
	    text-decoration: none;
	}
</style>
			

			<div class="settings-header">
				<?php
					$titleParams=Yii::t("common","Settings chat {what}", array("{what}"=>Yii::t("common","of the ".Element::getControlerByCollection($type))." ".$element["name"]));
				?>
				<div class="box-icon" >
		            <span class="fa fa-3x fa-comments"></span>
		        </div>

				<h3 class="title"></i> <?php echo $titleParams ?></h3>
			</div>
			<div class="modal-body" style="background-color: #24284D;margin-top: 53px">
				
                         <?php 
	if(@Yii::app()->session["userId"] && Yii::app()->params['rocketchatEnabled'] ){
		if( 
			($type==Person::COLLECTION) ||
			//admins can create rooms
			( Authorisation::canEditItem(Yii::app()->session['userId'], $type, $id) ) ||
			//simple members can join only when admins had created
			( Link::isLinked($id,$type,Yii::app()->session["userId"]))  )
			{
				if(@$element["slug"])
				//todo : elements members of
					$loadChat = $element["slug"];
				else
					$createSlugBeforeChat=true;
				//todo : elements members of
				$loadChat = StringHelper::strip_quotes($element["name"]);
				//people have pregenerated rooms so allways available 
				$hasRC = (@$element["hasRC"] || $type == Person::COLLECTION ) ? "true" : "false";
				$canEdit = ( @$openEdition && $openEdition ) ? "true" : "false";
				//Authorisation::canEditItem(Yii::app()->session['userId'], $type, $id) );
				if($type == Person::COLLECTION){
				 	$loadChat = (string)$element["username"];
					if( (string)$element["_id"]==@Yii::app()->session["userId"] )
						$loadChat = "";
				  }
				  $chatColor = (@$element["hasRC"] || $type == Person::COLLECTION ) ? "text-red" : "";
  				if( (@$element['rocketchatMultiEnabled'] || @$this->costum["rocketchatMultiEnabled"]) && $type != Person::COLLECTION ){ ?>

						  		
					  			<?php
					  			if( $hasRC && @$element["tools"]["chat"] )
					  			{ 
						  			if( @$element["tools"]["chat"]["int"] )
		  							{
										  ?>
										  <div class="row destacados">
											<div class="col-xs-12">
												<div class="box-title-chat">
													<h4>Interne</h4>
												</div>
											</div>
										  </div>
										  <div class="row destacados-chat animated bounceInDown">
							  			<?php
							  			foreach (@$element["tools"]["chat"]["int"] as $key => $chat) 
										  { ?>
										  	 
											<div class="col-sm-6 col-xs-12 interne-chat" data-animation="animated bounceIn">
											  	<span class="btn-u-icon img-circle text-primary">  
									          		<?php 
												  		$exist = false;
												  		$str_array = str_split($chat["name"]);
												  		if(in_array("_",$str_array)) $exist = true;
														echo $exist ? explode('_', $chat["name"])[1][0] : "c";
												  	 ?> 
									          	</span>
									          	<?php if( strpos ( $chat["url"] , "/channel/") === false ) 
						                    		echo "<span class='ico-after'><i class='fa fa-lock'></i></span>"; ?>
												<a href="" class="btn-open-chatEl bg-white btn icon-btn-chat btn-primary" data-name-el="<?php echo $element["name"]; ?>" data-username="<?php echo Yii::app()->session['user']['name']; ?>" data-slug="<?php echo $chat["name"]; ?>" data-type-el="<?php echo $type; ?>"  data-open="<?php echo ( strpos ( $chat["url"] , "/channel/") === false ) ? "false" : "true"; ?>"  data-hasRC="true" data-id="<?php echo (string)$element["_id"]; ?>">
													  		
												    <span class="chanel-name">
												        <?php echo $chat["name"]; ?> 
												    </span>
												</a>
											</div>


											
							  			<?php
							  			} 
							  			?>
							  			</div>

							  			<script type="text/javascript">
							  				$(".btn-open-chatEl").click( function(e){
                                                  e.preventDefault();
										    	var nameElo = $(this).data("name-el");
										    	var idEl = $(this).data("id");
										    	var usernameEl = $(this).data("username");
										    	var slugEl = $(this).data("slug");
										    	var typeEl = dyFInputs.get($(this).data("type-el")).col;
										    	var openEl = $(this).data("open");
										    	var hasRCEl = ( $(this).data("hasRC") ) ? true : false;
										    	
										    	var ctxData = {
										    		name : nameElo,
										    		type : typeEl,
										    		id : idEl
										    	}
										    	if(typeEl == "citoyens")
										    		ctxData.username = usernameEl;
										    	else if(slugEl)
                                                    ctxData.slug = slugEl;

                                                // alert(nameElo +" | "+typeEl +" | "+openEl +" | "+hasRCEl);
										    	rcObj.loadChat(nameElo ,typeEl ,openEl ,hasRCEl, ctxData );
										    } );
							  			</script>
							  			<?php
							  		} 

							  		if( @$element["tools"]["chat"]["ext"] )
		  							{
		  								?>
										<div class="row destacados">
											<div class="col-xs-12">
												<div class="box-title-chat">
													<h4>Externe</h4>
												</div>
											</div>
										</div>
										<div class="row destacados-chat animated bounceInDown">
							  			<?php
							  			foreach ($element["tools"]["chat"]["ext"] as $key => $chat) 
										  { ?>
										  	<div class="col-sm-6 col-xs-12 externe-chat" data-animation="animated bounceIn">
											  	<span class="btn-u-icon img-circle text-primary">  
									          		<?php 
												  		$exist = false;
												  		$str_array = str_split($chat["name"]);
												  		if(in_array("_",$str_array)) $exist = true;
														echo $exist ? explode('_', $chat["name"])[1][0] : "c";
												  	 ?> 
									          	</span>
									          	<?php if( strpos ( $chat["url"] , "/channel/") === false ) 
						                    		echo "<span class='ico-after'><i class='fa fa-external-link'></i></span>"; ?>
												<a href="<?php echo $chat["url"]; ?>" target="_blank" class="btn-open-chatExt bg-white btn icon-btn-chat btn-primary">
								                    <span class="chanel-name">
								                    	<?php echo $chat["name"]; ?>
								                    </span>
								                </a>
											</div>
											

							  			<?php
							  			}
                                      } ?>
                                      
									
									  		<div class="row">
											  <div class="col-xs-12 padding-10 text-center">
												  <a href="javascript:dyFObj.openForm('chat','sub')" class="button-wrap">
													  <i class="fa fa-plus-circle"></i> <?php echo Yii::t("common","New Channel") ?>
													</a>
												</div>
											</div>

					  			<?php
					  			} else {
				  	    		?>
									<div class="row">
											  <div class="col-xs-12 padding-10">
												  <a href="javascript:;" onclick="javascript:rcObj.loadChat('<?php echo $loadChat;?>','<?php echo $type?>',<?php echo $canEdit;?>,<?php echo $hasRC;?>, contextData )" class=" <?php echo $chatColor;?> button-wrap" id="open-rocketChat">
										  		<i class="fa fa-plus-circle"></i> <?php echo Yii::t("common","New Channel") ?>
										  </a>
												</div>
											</div>
		  				<?php 	} ?>
					       

				  
	<?php 	} else { ?>
				<button type="button" onclick="javascript:rcObj.loadChat('<?php echo $loadChat;?>','<?php echo $type?>',<?php echo $canEdit;?>,<?php echo $hasRC;?>, contextData )" class="btn btn-default bold hidden-xs <?php echo $chatColor;?>" 
			  		  id="open-rocketChat" style="border-right:0px!important;">
			  		<i class="fa fa-comments elChatNotifs"></i> <?php echo Yii::t("cooperation", "Chat"); 
			  		?>
				</button>
	<?php 	} 
		}
	} 
?>
			</div>		
			</div>

<script type="text/javascript">
var contextId='<?php echo @$id ?>';
var contextType='<?php echo @$type ?>';
jQuery(document).ready(function() {
	//ANIMATION
	$(".interne-chat").focusin(function() {
	  var animation = $(this).attr("data-animation");
	  $(this).addClass(animation).delay(1000).queue(function(next) {
	    $(this).removeClass(animation);
	    next();
	  });
	});

});
</script>