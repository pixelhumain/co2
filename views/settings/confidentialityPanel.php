
<?php if(@$modal && $modal!==false){ ?>
<div class="modal fade" role="dialog" id="modal-confidentiality">
	<div class="modal-dialog">
		<div class="modal-content">
			<?php } ?>
			<div class="settings-header">
			<?php if(@$modal && $modal!==false){ ?>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<?php } ?>	
				<?php if($type==Person::COLLECTION)
					$titleParams=Yii::t("common","Confidentiality of your personal information");
				else
					$titleParams=Yii::t("common","Settings {what}", array("{what}"=>Yii::t("common","of the ".Element::getControlerByCollection($type))." ".$element["name"]));
				?>
				<h3 class="title"><i class="fa fa-ban"></i> <?php echo $titleParams ?></h3>
			</div>
			<div class="modal-body">
				

				<div class="setting-container">
		            <div class="row">
		                <div class="col-sm-6 col-xs-12 confiden setting-cont-1">
		                    <!-- <h3>Login Form 1</h3> -->
		                    <div class="pull-left text-left margin-top-20">
								<?php if ($type==Person::COLLECTION){ ?>
									<strong><i class="fa fa-group"></i> <?php echo Yii::t("common","Public"); ?></strong> : 
									<?php echo Yii::t("common","Visible for everyone."); ?><br/><br/>

									<strong><i class="fa fa-user-secret"></i> <?php echo Yii::t("common","Private"); ?></strong> : 
									<?php echo Yii::t("common","Visible for my contacts."); ?><br/><br/>

									<strong><i class="fa fa-ban"></i> <?php echo Yii::t("common","Mask"); ?></strong> : 
									<?php echo Yii::t("common","Not visible."); ?><br/>	<br/>					
								<?php } ?>

								<strong><i class="fa fa-group"></i> <?php echo Yii::t("common","Open Data"); ?></strong> : 
								<?php echo Yii::t("common","You are providing your data in free access, in order to contribute to the common good."); ?>
								<br/><br/>
								
								<?php if ($type!=Person::COLLECTION){ ?>
									<strong><i class="fa fa-group"></i> <?php echo Yii::t("common","Open Edition") ;?></strong> : 
									<?php echo Yii::t("common","All users have the possibility to participate / modify the information."); ?><br/><br/>
									<?php if ($type!=Organization::COLLECTION){ ?>
									<strong><i class="fa fa-lock"></i> <?php echo Yii::t("common","Private") ;?></strong> : 
									<?php echo Yii::t("common","Only community can access to this page and see its informations."); ?><br/><br/>
								<?php } ?>
								<?php } ?>

                                <strong><i class="fa fa-group"></i> <?php echo Yii::t("common","feedback"); ?></strong> :
                                <?php echo Yii::t("common","feedback"); ?>
                                <br/><br/>
								<strong><i class="fa fa-group"></i> <?php echo Yii::t("common","Request to become admin"); ?></strong> :
                                <?php echo Yii::t("common","Prevent users from asking to become admin"); ?>
                                <br/><br/>
							</div>
		                         
		                </div>
		                <div class="col-sm-6 col-xs-12 confiden setting-cont-2">
		                    <div class="setting-logo">
		                    	<span>
		                    		<i class="fa fa-3x fa-cog"></i>
		                    	</span>
		                    </div>
		                    <!-- <h3>Login Form 2 </h3> -->
		                    <div class="row text-dark panel-btn-confidentiality  margin-top-20">
								<?php if ($type==Person::COLLECTION){ ?>
									
									<div class="no-padding">
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 text-center padding-10 margin-top-10">
										<i class="fa fa-message"></i> <strong><?php echo Yii::t("person","Birth date"); ?> :</strong>
										</div>
										<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 text-center padding-10">
											<div class="btn-group btn-group-birthDate inline-block">
												<button class="btn btn-default confidentialitySettings" type="birthDate" value="public"><i class="fa fa-group"></i> <?php echo Yii::t("common","Public"); ?></button>
												<button class="btn btn-default confidentialitySettings" type="birthDate" value="private"><i class="fa fa-user-secret"></i> <?php echo Yii::t("common","Private"); ?></button>
												<button class="btn btn-default confidentialitySettings" type="birthDate" value="hide"><i class="fa fa-ban"></i> <?php echo Yii::t("common","Mask"); ?></button>
											</div>
										</div>
									</div>

									<div class="no-padding">
										<div class="col-lg-4 col-md-4 col-sm-4 text-center padding-10 margin-top-10">
											<i class="fa fa-message"></i> <strong><?php echo Yii::t("common","My mail"); ?> :</strong>
										</div>
										<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 text-center padding-10">
											<div class="btn-group btn-group-email inline-block">
											<button class="btn btn-default confidentialitySettings" type="email" value="public"><i class="fa fa-group"></i> <?php echo Yii::t("common","Public"); ?></button>
											<button class="btn btn-default confidentialitySettings" type="email" value="private"><i class="fa fa-user-secret"></i> <?php echo Yii::t("common","Private"); ?></button>
											<button class="btn btn-default confidentialitySettings" type="email" value="hide"><i class="fa fa-ban"></i> <?php echo Yii::t("common","Mask"); ?></button>
											</div>
										</div>
									</div>
									<div class=" no-padding">
										<div class="col-lg-4 col-md-4 col-sm-4 text-center padding-10 margin-top-10">
											<i class="fa fa-message"></i> <strong><?php echo Yii::t("common","Locality") ;?> :</strong>
										</div>
										<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 text-center padding-10">
											<div class="btn-group btn-group-locality inline-block">
												<button class="btn btn-default confidentialitySettings" type="locality" value="public" selected><i class="fa fa-group"></i> <?php echo Yii::t("common","Public") ;?></button>
												<button class="btn btn-default confidentialitySettings" type="locality" value="private"><i class="fa fa-user-secret"></i> <?php echo Yii::t("common","Private"); ?></button>
												<button class="btn btn-default confidentialitySettings" type="locality" value="hide"><i class="fa fa-ban"></i> <?php echo Yii::t("common","Mask"); ?></button>
											</div>
										</div>
									</div>
									<div class="no-padding">
										<div class="col-lg-4 col-md-4 col-sm-4 text-center padding-10 margin-top-10">
											<i class="fa fa-message"></i> <strong><?php echo Yii::t("common","My phone"); ?>  :</strong>
										</div>
										<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 text-center padding-10">
											<div class="btn-group btn-group-phone inline-block">
												<button class="btn btn-default confidentialitySettings" type="phone" value="public"><i class="fa fa-group"></i> <?php echo Yii::t("common","Public") ;?></button>
												<button class="btn btn-default confidentialitySettings" type="phone" value="private"><i class="fa fa-user-secret"></i> <?php echo Yii::t("common","Private") ;?></button>
												<button class="btn btn-default confidentialitySettings" type="phone" value="hide"><i class="fa fa-ban"></i> <?php echo Yii::t("common","Mask"); ?></button>
											</div>
										</div>
									</div>
									<div class="no-padding">
										<div class="col-lg-4 col-md-4 col-sm-4 text-center padding-10 margin-top-10">
											<i class="fa fa-message"></i> <strong><?php echo Yii::t("common","My directory"); ?>  :</strong>
										</div>
										<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 text-center padding-10">
											<div class="btn-group btn-group-directory inline-block">
												<button class="btn btn-default confidentialitySettings" type="directory" value="public"><i class="fa fa-group"></i> <?php echo Yii::t("common","Public") ;?></button>
												<button class="btn btn-default confidentialitySettings" type="directory" value="private"><i class="fa fa-user-secret"></i> <?php echo Yii::t("common","Private") ;?></button>
												<button class="btn btn-default confidentialitySettings" type="directory" value="hide"><i class="fa fa-ban"></i> <?php echo Yii::t("common","Mask"); ?></button>
											</div>
										</div>
									</div>
									<div class="no-padding">
										<div class="col-lg-4 col-md-4 col-sm-4 text-center padding-10 margin-top-10">
											<i class="fa fa-message"></i> <strong>ActivityPub :</strong>
										</div>
										<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 text-center padding-10">
											<div class="btn-group btn-group-activitypub inline-block">
												<button class="btn btn-default confidentialitySettings" type="activitypub" value="true"><i class="fa fa-check"></i> <?php echo Yii::t("common","Enable");?></button>
												<button class="btn btn-default confidentialitySettings" type="activitypub" value="false"><i class="fa fa-ban"></i> <?php echo Yii::t("common","Disable");?></button>
											</div>
										</div>
									</div>
								<?php } ?>
								<div class="col-xs-12 no-padding">
									<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 text-right padding-10 margin-top-10">
										<i class="fa fa-message"></i> <strong><?php echo Yii::t("common","Open Data") ;?> :</strong>
									</div>
									<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 padding-10 text-left">
										<div class="btn-group btn-group-isOpenData inline-block">
											<button class="btn btn-default confidentialitySettings" type="isOpenData" value="true">
												<i class="fa fa-group"></i> <?php echo Yii::t("common","Yes"); ?>
											</button>
											<button class="btn btn-default confidentialitySettings" type="isOpenData" value="false">
												<i class="fa fa-user-secret"></i> <?php echo Yii::t("common","No"); ?>
											</button>
											<?php
												$url = Yii::app()->baseUrl.'/api/';
												if($type == Person::COLLECTION)
													$url .= Person::CONTROLLER;
												else if($type == Organization::COLLECTION)
													$url .= Organization::CONTROLLER;
												else if($type == Event::COLLECTION)
													$url .= Event::CONTROLLER;
												else if($type == Project::COLLECTION)
													$url .= Project::CONTROLLER;
												?>
											<a href="<?php echo $url.'/get/id/'.$element['_id'] ;?>" data-toggle="tooltip" 
												title='<?php echo Yii::t("common","View the data"); ?>' id="urlOpenData" 
												class="urlOpenData padding-5 margin-left-10 pull-left" target="_blank">
												<i class="fa fa-eye"></i> <?php echo Yii::t("common","View dataset"); ?>
											</a>
										</div>
									</div>
								</div>
								<!--<div class="col-sm-4 col-xs-4 text-right padding-10 margin-top-10">
									<i class="fa fa-message"></i> <strong><?php echo Yii::t("common","Badge") ;?> :</strong>
								</div>
								<div class="col-sm-8 col-xs-8 text-left padding-10">
									<div class="btn-group btn-group-badge inline-block">
										<button class="btn btn-default confidentialitySettings" type="badge" value="true">
											<i class="fa fa-group"></i> <?php echo Yii::t("common","Yes"); ?>
										</button>
										<button class="btn btn-default confidentialitySettings" type="badge" value="false">
											<i class="fa fa-user-secret"></i> <?php echo Yii::t("common","No"); ?>
										</button>
									</div>
								</div>-->

								<?php if($type != Person::COLLECTION){ ?>
									<div class="col-xs-12 no-padding">
										<div class="col-sm-4 col-xs-4 text-right padding-10 margin-top-10">
											<i class="fa fa-message"></i> <strong><?php echo Yii::t("common","Open Edition") ;?> :</strong>
										</div>
										<div class="col-sm-8 col-xs-8 text-left padding-10">
											<div class="btn-group btn-group-isOpenEdition inline-block">
												<button class="btn btn-default confidentialitySettings" type="isOpenEdition" value="true">
													<i class="fa fa-group"></i> <?php echo Yii::t("common","Yes"); ?></button>
												<button class="btn btn-default confidentialitySettings" type="isOpenEdition" value="false">
													<i class="fa fa-user-secret"></i> <?php echo Yii::t("common","No"); ?></button>
											</div>
										</div>
									</div>
									<div class="col-xs-12 no-padding">
										<div class="col-sm-4 col-xs-4 text-right padding-10 margin-top-10">
											<i class="fa fa-message"></i> <strong><?php echo Yii::t("common","Private") ;?> :</strong>
										</div>
										<div class="col-sm-8 col-xs-8 text-left padding-10">
											<div class="btn-group btn-group-private inline-block">
												<button class="btn btn-default confidentialitySettings" type="private" value="true">
													<i class="fa fa-group"></i> <?php echo Yii::t("common","Yes"); ?></button>
												<button class="btn btn-default confidentialitySettings" type="private" value="false">
													<i class="fa fa-user-secret"></i> <?php echo Yii::t("common","No"); ?></button>
											</div>
										</div>
									</div>
									<?php
									if($type==Project::COLLECTION){ ?>
									<div class="col-xs-12 no-padding">
										<div class="col-sm-4 col-xs-4 text-right padding-10 margin-top-10">
											<i class="fa fa-message"></i> <strong><?php echo Yii::t("common","Crowdfunding") ;?> :</strong>
										</div>
										<div class="col-sm-8 col-xs-8 text-left padding-10">
											<div class="btn-group btn-group-crowdfunding inline-block">
												<button class="btn btn-default confidentialitySettings" type="crowdfunding" value="true">
													<i class="fa fa-group"></i> <?php echo Yii::t("common","Yes"); ?></button>
												<button class="btn btn-default confidentialitySettings" type="crowdfunding" value="false">
													<i class="fa fa-user-secret"></i> <?php echo Yii::t("common","No"); ?></button>
											</div>
										</div>
									</div>
									<?php
									}
                                    if($type==Project::COLLECTION || $type==Organization::COLLECTION){ ?>
                                    <div class="col-xs-12 no-padding">
                                        <div class="col-sm-4 col-xs-4 text-right padding-10 margin-top-10">
                                            <i class="fa fa-message"></i> <strong><?php echo Yii::t("common","Feedback") ;?> :</strong>
                                        </div>
                                        <div class="col-sm-8 col-xs-8 text-left padding-10">
                                            <div class="btn-group btn-group-feedback inline-block">
                                                <button class="btn btn-default confidentialitySettings" type="feedback" value="true">
                                                    <i class="fa fa-group"></i> <?php echo Yii::t("common","Yes"); ?></button>
                                                <button class="btn btn-default confidentialitySettings" type="feedback" value="false">
                                                    <i class="fa fa-user-secret"></i> <?php echo Yii::t("common","No"); ?></button>
                                            </div>
                                        </div>
                                    </div>
									<div class="col-xs-12 no-padding">
                                        <div class="col-sm-4 col-xs-4 text-right padding-10 margin-top-10">
                                            <i class="fa fa-message"></i> <strong><?php echo Yii::t("common","Request to become admin") ;?> :</strong>
                                        </div>
                                        <div class="col-sm-8 col-xs-8 text-left padding-10">
                                            <div class="btn-group btn-group-forbiddenAdminRequest inline-block">
                                                <button class="btn btn-default confidentialitySettings" type="forbiddenAdminRequest" value="true">
                                                    <i class="fa fa-group"></i> <?php echo Yii::t("common","Not allowed"); ?></button>
                                                <button class="btn btn-default confidentialitySettings" type="forbiddenAdminRequest" value="false">
                                                    <i class="fa fa-user-secret"></i> <?php echo Yii::t("common","Allowed"); ?></button>
                                            </div>
                                        </div>
                                    </div>
									<div class="col-xs-12 no-padding">
                                        <div class="col-sm-4 col-xs-4 text-right padding-10 margin-top-10">
                                            <i class="fa fa-message"></i> <strong><?php echo Yii::t("common","Show QR and Join button") ;?> :</strong>
                                        </div>
                                        <div class="col-sm-8 col-xs-8 text-left padding-10">
                                            <div class="btn-group btn-group-showQr inline-block">
                                                <button class="btn btn-default confidentialitySettings" type="showQr" value="true">
                                                    <i class="fa fa-group"></i> <?php echo Yii::t("common","No"); ?></button>
                                                <button class="btn btn-default confidentialitySettings" type="showQr" value="false">
                                                    <i class="fa fa-user-secret"></i> <?php echo Yii::t("common","Yes"); ?></button>
                                            </div>
                                        </div>
                                    </div>
									<?php
									} ?>
							</div>

		                </div>
		            </div>
		        </div>

			    
			        


	

				<?php if(!isset($modal) || empty($modal)){ ?>
										<?php if($type!=Person::COLLECTION){ ?> 
											
				<div class="row">
					<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 sect-infoGe">
			            <div class="box">
			                <div class="box-icon">
			                    <span class="fa fa-3x fa-info"></span>
			                </div>
			                <div class="info">
			                	<h4 class="text-center">
			                		<?php echo Yii::t("common","Settings and general informations") ?>
			                	</h4>
								<div class="block-account text-center link-setting">
									 <a href="javascript:;" onclick="updateSlug();" id="" class="btn btn-success"> <i class="fa fa-pencil"></i> 
									 	<!--<i class="fa fa-id-badge">-->
									 	<?php echo Yii::t("common", "Edit slug"); ?></i> </a>
								</div>
								<div class="text-center link-setting">
									<a href="javascript:;" class="ssmla btn btn-danger" data-action="delete">
										<i class="fa fa-remove"></i>&nbsp;<?php echo Yii::t("common", "Delete element") ?>
									</a>
								</div>
								<div class="text-center link-setting">
									<a href="javascript:;" class="ssmla btn btn-default" data-action="qrCode">
										<i class="fa fa-qrcode"></i>&nbsp;<?php echo Yii::t("common", "QR Code") ?>
									</a>
								</div>
			                	<div class="text-center link-setting">
									<a href="javascript:;" class="ssmla btn btn-default" data-action="chatSettings">
										<i class="fa fa-comments"></i>&nbsp; <?php echo Yii::t("common", "Chat settings") ?>
									</a>
								</div>
			                	<div class="text-center link-setting">
									<a href="javascript:;" class="ssmla btn btn-default" data-view="md">
										<i class="fa fa-file-text-o"></i>&nbsp; <?php echo Yii::t("common", "Markdown version") ?>
									</a>
								</div>
			                	<div class="text-center link-setting">
									<a href="javascript:;" class="ssmla btn btn-default" data-action="mindmap">
										<i class="fa fa-sitemap"></i>&nbsp; <?php echo Yii::t("common", "Mindmap view") ?>
									</a>
								</div>
			                	<div class="text-center link-setting">
									<a href="javascript:;" class="bg-white ssmla btn btn-default" data-action="downloadData">
										<i class="fa fa-download"></i>&nbsp; <?php echo Yii::t("common", "Download data") ?>
									</a>
								</div>
			                	<div class="text-center link-setting">
									<a href="javascript:;" class="bg-white ssmla btn btn-default" data-action="printOut">
										<i class="fa fa-print"></i>&nbsp; <?php echo Yii::t("common", "Print out") ?>
									</a>
								</div>

							</div>
						</div>
					</div>
				<?php } ?>						

					<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 sect-notification notificationsModalSettings">
			            <div class="box">
			                <div class="box-icon">
			                    <span class="fa fa-3x fa-bell"></span>
			                </div>
			                <div class="info">
			                	<h4 class="text-center">
			                		<?php echo Yii::t("common","Notifications settings") ;?><br>
			                		<a href="javascript:;" id="btnSettingsInfos" class="margin-left-10" style="font-size: 14px;color: #e6344d;text-transform: capitalize;"><i class="fa fa-info-circle"></i> <?php echo Yii::t("common", "Info"); ?></a>
			                	</h4>

										
											<div class="text-left padding-10 elementSettingsNotifs notificationsModalSettings">

											</div>
											<?php  echo $this->renderPartial('co2.views.settings.modalExplainCommunitySettings', array()); ?>
							</div>
						</div>
					</div>
				</div>

									<?php } 
									} ?>
				
			</div>
			<?php if(@$modal && $modal!==false){ ?>	
			<div class="modal-footer">
				<button type="button" class="lbh btn btn-success btn-confidentialitySettings" data-dismiss="modal" aria-label="Close" data-hash="#page.type.<?php echo $type ?>.id.<?php echo $element['_id'] ;?>">
				<i class="fa fa-times"></i> <?php echo Yii::t("common","Close") ;?>
				</button>
			</div>
		</div>
	</div>
</div>
<?php }
?>
<script type="text/javascript">
var seePreferences = '<?php echo (@$element["seePreferences"] == true) ? "true" : "false"; ?>';		
var preferences = <?php echo json_encode(@$element["preferences"]) ?>;
var contextId='<?php echo @$id ?>';
var contextType='<?php echo @$type ?>';
var typePreferences=["privateFields", "publicFields"];
var nameFields=["email", "locality", "phone", "directory", "birthDate"];
var typePreferencesBool = ["isOpenData", "isOpenEdition", "private", "crowdfunding", "activitypub" , "feedback", "forbiddenAdminRequest", "showQr"];

jQuery(document).ready(function() {
	$(".open-campaign").off().on( "click", function() {
		
		
	});
	pageProfil.bindButtonMenu();
	settings.bindButtonConfidentiality(preferences);
	settings.bindEventsConfidentiality(contextId, contextType);
	if( typeof myContacts != "undefined" && 
		myContacts != null && 
		typeof myContacts[contextType] != "undefined" && 
		myContacts[contextType] != null && 
		typeof myContacts[contextType][contextId] != "undefined" && 
		myContacts[contextType][contextId] != null ){
		var setNotif = (typeof myContacts[contextType][contextId].notifications != "undefined") ? tradSettings[myContacts[contextType][contextId].notifications] : tradSettings["bydefault"];
		var setMails = (typeof myContacts[contextType][contextId].mails != "undefined") ? tradSettings[myContacts[contextType][contextId].mails] : tradSettings["bydefault"];
			$(".elementSettingsNotifs").html(settings.getToolbarSettingsNotifications(setNotif, setMails, contextType, contextId));
			settings.settingsCommunityEvents();		
	}else{
		$(".notificationsModalSettings").remove();
	}
/*
	var maxHeight = 0;

	$(".confiden").each(function(){
	   if ($(this).height() > maxHeight) { maxHeight = $(this).height(); }
	});

	$(".confiden").height(maxHeight);
*/
	});
</script>