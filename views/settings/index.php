<div id="menu-settings" class="col-xs-12 no-padding shadow2 margin-bottom-20">
	<div class="col-xs-12 no-padding">
		<a href="javascript:" class="link-docs-menu down-menu pull-left btn col-xs-6 elipsis active" data-page="myAccount" data-label="<?php echo Yii::t("settings","My Account"); ?>">
			<i class="fa fa-user-circle"></i> 
			<span class="">
				<?php echo Yii::t("settings","My Account"); ?>
			</span>
		</a>
		<a href="javascript:;" class="link-docs-menu pull-left btn col-xs-6 elipsis" data-page="confidentialityCommunity" data-label="<?php echo Yii::t("settings","My community"); ?>">
			<i class="fa fa-users"></i> 
			<span class="">
				<?php echo Yii::t("common","My community"); ?>
			</span>
		</a>
	</div>
</div>

<div id="container-settings-view" class="col-xs-12 no-padding">
</div>
<script type="text/javascript">
var page="<?php echo @$page ?>";
var searchInCommunity="";
jQuery(document).ready(function() {
	navInSettings("myAccount");
	$(".link-docs-menu").off().on("click",function(){
		$(".link-docs-menu").removeClass("active");
		$(this).addClass("active"); 
		navInSettings($(this).data("page"));
	});
	
});
function navInSettings(page){
	coInterface.simpleScroll(0);
	coInterface.showLoader('#container-settings-view');
	urlToSend="settings/"+page;
	if(page=="confidentiality" || page=="myAccount")
		urlToSend+="/type/citoyens/id/"+contextData.id;
	ajaxPost('#container-settings-view' ,baseUrl+'/'+moduleId+"/"+urlToSend,
			 null,function(){});
}

</script>