<?php
$cs = Yii::app()->getClientScript(); 
// $cssAnsScriptFilesTheme = array(
// 	//SELECT2
// 	'/plugins/bootstrap-toggle/css/bootstrap-toggle.min.css',
// 	'/plugins/bootstrap-toggle/js/bootstrap-toggle.min.js' , 
// 	'/plugins/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css',
//   	'/plugins/bootstrap-switch/dist/js/bootstrap-switch.min.js' ,
// );
//if ($type == Project::COLLECTION)
//	array_push($cssAnsScriptFilesTheme, "/assets/plugins/Chart.js/Chart.min.js");
//HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesTheme, Yii::app()->request->baseUrl);
$cssAnsScriptFiles = array(
     '/assets/css/default/settings.css',
);
HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFiles, Yii::app()->theme->baseUrl); 

    
$cssAnsScriptFilesModule = array(
    '/js/default/settings.js',
);
HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, $this->module->assetsUrl);

?>
<?php
if ( Authorisation::canDeleteElement( (String)$element["_id"], $type, Yii::app()->session["userId"]) &&
//!@$deletePending &&
!empty(Yii::app()->session["userId"]) ) {

$this->renderPartial('../element/confirmDeleteModal', array("id" =>(String)$element["_id"], "type"=>$type));

?>
<div id="myAccount-settings" class="contain-section-params col-xs-12 bg-white shadow2 margin-bottom-20">
	<div class="settings-header">
		<div class="box-icon" >
            <span class="fa fa-3x fa-cogs"></span>
        </div>
		<h3 class="title"></i> <?php echo Yii::t("settings", "Manage your account") ?></h3>
	</div>
	<div class="row">
		<div class="col-sm-6 col-xs-12 div-btn-setting">
			 <a class="btn btn-setting-update icon-btn-update btn-primary" id="btn-update-password-setting" href="javascript:"><span class="btn-u-icon fa fa-unlock-alt img-circle text-primary"></span><?php echo Yii::t("common","Change password"); ?></a>

			 <!--<a href="javascript:;" id="btn-update-password-setting" class="btn btn-success btn-setting-update"> <i class="fa fa-unlock-alt  "></i> <?php echo Yii::t("common","Change password"); ?> </a> -->
		</div>

		<div class="col-sm-6 block-account col-xs-12 div-btn-setting"> 
			<a href="javascript:;" onclick="updateSlug();" id="" class="btn icon-btn-update btn-primary btn-setting-update"><span class="btn-u-icon img-circle text-primary fa fa-id-badge"></span> <?php echo Yii::t("common", "Edit slug"); ?>  </a>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-6 block-account col-xs-12 div-btn-setting">
			<a href="javascript:;" id="downloadProfil-setting" class="btn icon-btn-update btn-primary btn-setting-update"><span class='fa fa-download btn-u-icon img-circle text-primary'></span> <?php echo Yii::t("common", "Download your profile") ?> </a>
		</div>


		<?php 
		/*if ( Authorisation::canDeleteElement( (String)$element["_id"], $type, Yii::app()->session["userId"]) &&
				//!@$deletePending && 
				!empty(Yii::app()->session["userId"]) ) {

				echo $this->renderPartial('../element/confirmDeleteModal', array("id" =>(String)$element["_id"], "type"=>$type));*/

		?>
		<div class="block-account col-sm-6 col-xs-12 div-btn-setting">
			 <a href="javascript:;" id="btn-delete-element-setting" class="btn btn-danger icon-btn-update btn-setting-update"> <span class='fa fa-trash btn-u-icon img-circle'></span> <?php echo Yii::t("common", "Delete my account") ?> </a>
		</div>
		<?php /*}*/ ?>
	</div>
</div>
<div class="col-xs-12 notificationsPod bg-white shadow2 margin-bottom-20">
	<?php echo $this->renderPartial("co2.views.settings.notificationsAccount", array("preferences"=>$element["preferences"]), true); ?>
</div>
<div class="col-xs-12 confidentialityPod bg-white shadow2 margin-bottom-20">
	<?php
    //var_dump($element);
    $params=array(  "element" => @$element,
			"type" => $type,
			"id" => (string)$element["_id"], 
			"edit" => true,
			"openEdition" => false,
			"modal"=>false
		);
    	echo $this->renderPartial("co2.views.settings.confidentialityPanel", $params, true); ?>
</div>
<?php } ?>
<script type="text/javascript">
//contextData = <?php echo json_encode(@$element) ?>;
jQuery(document).ready(function() {
	settings.bindMyAccountSettings(contextData);

	if(activitypubGuide && (activitypubGuide.currentStep.step == activitypubGuide.constants.steps.SETTING)) {

		if(userConnected.preferences && userConnected.preferences.activitypub) {
			activitypubGuide.actions.open(activitypubGuide.constants.steps.COMMUNITY);
		}else {
			var totalWaiting = 0
			var interval = setInterval(function(){
				if($("button[type='activitypub']").length > 0) {
					document.querySelector("button[type='activitypub']").scrollIntoView({ behavior: 'smooth' })
					clearInterval(interval)
				}else if(totalWaiting > 15000) {
					clearInterval(interval)
				}
	
				totalWaiting += 500;
			}, 500)
		}

	}
});
</script>