<style type="text/css">
    #modal-preview-coop {
        left: 0;
        box-shadow: none !important;
    }

    #close-graph-modal {
        border-radius: 50px;
        border: 4px solid #f5f5f5;
        font-size: 16pt;
    }

    #preview-zone .container-preview {
        display: flex;
        flex-wrap: wrap;
        padding: 5px;
        background-color: #f5f5f5;
        border-radius: 10px;
        box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
        margin-bottom: 20px;
    }

    #preview-zone .coherent-data {
        padding: 20px;
        background-color: #e8f7f0;
        border-radius: 10px;
        box-shadow: 0 0 10px rgba(0, 128, 0, 0.2);
        margin-right: 10px;
        flex: 1;
    }

    #preview-zone .incoherent-data {
        padding: 20px;
        background-color: #fff4e6;
        border-radius: 10px;
        box-shadow: 0 0 10px rgba(255, 165, 0, 0.2);
        flex: 1;
        overflow: auto;
        max-height: 715px;
    }

    #preview-zone .search-container {
        margin-bottom: 20px;
        margin-top: 2%;
        display: flex;
        justify-content: flex-end;
        gap: 0;
        width: 100%;
        box-sizing: border-box;
    }

    #preview-zone .search-input {
        border: 1px solid #ccc;
        border-radius: 5px;
        box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1);
        width: 50%;
        padding: 10px;
        font-size: 16px;
        margin-right: auto;
    }

    #btn-merge-data {
        font-size: 15px;
        cursor: pointer;
        white-space: nowrap;
        flex-shrink: 0;
    }

    #btn-merge-data:disabled {
        cursor: not-allowed !important;
    }

    #preview-zone .table-responsive {
        max-height: 900px;
        overflow-y: auto;
    }

    #preview-zone .table {
        width: 100%;
        margin-bottom: 20px;
        border-collapse: collapse;
    }

    #preview-zone .table th,
    #preview-zone .table td {
        padding: 10px;
        text-align: left;
        border: 1px solid #ddd;
        vertical-align: top;
    }

    #preview-zone .table th {
        text-align: center !important;
    }

    #preview-zone .table th {
        background-color: #f2f2f2;
        font-weight: bold;
        color: #333;
    }

    #preview-zone .table td {
        color: #555;
    }

    #preview-zone .table-striped tbody tr:nth-child(odd) {
        background-color: #f9f9f9;
    }

    #preview-zone .table thead tr th {
        border: 0px solid white !important;
    }

    .table-fixed tbody {
        display: block;
        max-height: 550px;
        overflow-y: auto;
    }

    .table-fixed thead,
    .table-fixed tbody tr {
        display: table;
        width: 100%;
        table-layout: fixed;
    }

    .name-width {
        width: 25%;
        font-weight: bold;
    }

    .co-width {
        width: 35%;
    }

    .api-width {
        width: 40%;
    }

    .co-data,
    .api-data {
        padding: 5px;
        margin-bottom: 20px;
        background-color: #fff;
        border-radius: 5px;
        box-shadow: 0 2px 5px rgba(0, 0, 0, 0.1);
        position: relative;
    }

    .co-data p,
    .api-data p {
        display: inline-flex;
        margin: 5px 0;
        line-height: 1.5;
        cursor: pointer;
        width: 90%;
        flex-grow: 1;
    }

    .co-data p {
        color: #00796b;
    }

    .api-data p {
        color: #d84315;
    }

    .city-name i.fa,
    .city-name .myCheckbox {
        position: absolute;
        right: 5px;
        top: 23px;
        transform: translateY(-50%);
        font-size: 20px;
        color: #00796b;
        cursor: pointer;
        transition: color 0.3s, transform 0.5s, box-shadow 0.3s;
    }

    .city-name input[type="checkbox"] {
        width: 20px;
        height: 20px;
        top: 19px !important;
        transition: width 0.2s, height 0.2s;
    }

    .city-name input[type="checkbox"]:hover {
        width: 23px;
        height: 23px;
    }

    .api-data i.fa {
        color: #d84315;
    }

    /* .city-name i.fa-edit {
        right: 30px !important;
        top: 24px !important;
    } */

    .city-name i.fa-save {
        right: 33px !important;
    }

    .co-data .details,
    .api-data .details {
        display: none;
        padding: 10px;
        background-color: #fafafa;
        border: 1px solid #e0e0e0;
        border-radius: 5px;
        box-shadow: inset 0 0 10px rgba(0, 0, 0, 0.05);
        margin-top: 10px;
    }

    .city-name i.fa-edit:hover {
        font-size: 24px;
        color: #0056b3;
        box-shadow: 0px 4px 8px #cdc5c580;
    }

    .city-name i.fa-save:hover {
        font-size: 24px;
    }

    .city-name i.fa-edit:active,
    .city-name i.fa-times:active,
    .city-name i.fa-save:active {
        transform: scale(0.9);
        box-shadow: 0px 2px 4px rgba(0, 0, 0, 0.2);
    }

    /* .city-name i.fa-trash:hover {
        font-size: 24px;
        color: #f44b4b;
        box-shadow: 0px 4px 8px #cdc5c580;
    } */

    .api-data-edited {
        background-color: #d84315;
    }

    .co-data-edited {
        background-color: #00796b;
    }

    .api-data-edited p,
    .co-data-edited p {
        color: white;
    }

    .api-data-edited .details p {
        color: #d84315 !important;
    }

    .co-data-edited .details p {
        color: #00796b !important;
    }

    .co-data-edited .edition {
        display: none !important;
    }

    @keyframes slideDown {
        from {
            max-height: 0;
            opacity: 0;
        }

        to {
            max-height: 200px;
            opacity: 1;
        }
    }

    @keyframes slideUp {
        from {
            max-height: 200px;
            opacity: 1;
        }

        to {
            max-height: 0;
            opacity: 0;
        }
    }

    .details {
        transition: max-height 0.1s ease-out, opacity 0.2s ease-out;
    }

    #preview-zone p {
        font-size: 15px !important;
    }

    .loader-preview {
        position: fixed;
        top: 15%;
        right: 0;
        bottom: 0;
        left: 0%;
        overflow: hidden;
        -webkit-overflow-scrolling: touch;
        outline: 0;
        z-index: 9999999999999999 !important;
        padding-top: 180px !important;
    }

    .loader-preview .content-loader-preview {
        width: auto;
        height: auto;
    }

    .loader-loader .content-loader-preview .processingLoader {
        width: auto !important;
        height: auto !important;
    }

    .backdrop-loader-previews {
        position: fixed;
        opacity: .8;
        background-color : #000;
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
        width: 100%;
        height : 100vh;
        z-index: 999999999 !important;
    }

    #postal-code-data h5 {
        margin-left: 2%;
        background: #cdc5c580;
        padding: 1%;
        border-radius: 4px;
        cursor: pointer;
    }

    .noEdit {
        color: white !important;
    }

    .search-container .selectData {
        border-radius: 5px;
        width: 150px;
        font-size: 15px;
        margin-right: 10px;
    }

    .header_previews_no {
        box-shadow: 4px 5px 9px 4px #d9d0d0;
    }
</style>
<div class="loader-preview hidden" id="loader-preview" role="dialog">
    <div class="content-loader-preview" id="content-loader-preview"></div>
</div>
<div class="backdrop-loader-previews hidden" id="backdrop-loader-previews"></div>
<div id="preview-zone" class="col-xs-12 no-padding">
    <div class="col-xs-12 padding-10 header_previews">
        <span class="pull-left">
            <h4>Cohérence de données</h4>
            <span class="info-coherence-title"></span>
        </span>
        <button id="close-graph-modal" class="btn btn-default pull-right btn-close-preview">
            <i class="fa fa-times"></i>
        </button>
    </div>
    <div class="no_data hidden" id="no_data_table">
        <h4 class="text-center" style="margin-top: 20%;">Aucune ville n'a été trouvée dans la base de données officielle du gouvernement français.</h4>
    </div>
    <div class="container-preview row" id="preview-container">
        <div class="col-sm-6 col-md-6 col-lg-6 coherent-data" id="co-cities-data">
            <span>Tous les données qui se trouve dans CO et aussi dans la base de données du gouvernement Francais.</span>
            <div class="search-container">
                <input type="text" id="searchInput" class="form-control search-input" placeholder="Rechercher par nom de ville...">
                <select class="selectData" id="select-type-filter">
                    <option value="0">Toutes les données</option>
                    <option value="1">Données parfaits</option>
                    <option value="2">Données inparfaits</option>
                </select>
                <button class="btn btn-primary hidden" onclick="mergeEditData('alldata')" id="btn-merge-alldata">Merger</button>
            </div>
            <div class="table-responsive">
                <table class="table table-striped table-fixed" id="coherence_table">
                    <thead>
                        <tr>
                            <th scope="col" class="name-width text-center">Nom de la ville</th>
                            <th scope="col" class="co-width text-center">Données dans CO</th>
                            <th scope="col" class="api-width text-center">Données de l'API</th>
                        </tr>
                    </thead>
                    <tbody id="data-content" class="table-scrollable">

                    </tbody>
                </table>
            </div>
        </div>
        <div class="row col-sm-6 col-md-6 col-lg-6 incoherent-data" id="incoherent-cities-data">
            <span>Les erreurs peuvent provenir de légères variations orthographiques entre la base de données du CO et celle du gouvernement, ou de la présence exclusive de la donnée dans l'une des bases.</span>
            <div class="search-container">
                <input type="text" id="inputSearch" class="form-control search-input" placeholder="Rechercher par nom de ville...">
                <button class="btn btn-primary" onclick="mergeEditData('data')" id="btn-merge-data">Merger </button>
            </div>
            <div id="postal-code-data" class="postal-code-data hidden">
            </div>
        </div>
    </div>
</div>
<script>
    var _params = <?= json_encode($params) ?>;
    var _name = _params.name;
    var _level = _params.level;
    var _zoneId = _params.zoneId;
    var _infoZone = adminZone.zones[_zoneId];

    var citiesCoherent = [];
    var citiesApiCoherent = [];
    var cities1Coherent = [];
    var citiesApi1Coherent = [];
    var allPostalCode = [];
    var editId = [];
    var valueId = [];
    var parfaitCities = [];
    var inparfaitCities = [];
    var editIdTemp = "";
    var valueTemp = "";
    var codeTemp = "";

    function initiVariable() {
        $("#select-type-filter").val('0');
        $("#btn-merge-alldata").addClass('hidden');
        document.getElementById('btn-merge-data').innerText = `Merger`;
        document.getElementById('btn-merge-data').disabled = true;
        parfaitCities = [];
        inparfaitCities = [];
        citiesCoherent = [];
        citiesApiCoherent = [];
        cities1Coherent = [];
        citiesApi1Coherent = [];
        allPostalCode = [];
        editId = [];
        valueId = [];
        editIdTemp = "";
        valueTemp = "";
        codeTemp = "";
    }

    function coherenceData() {
        $('#loader-preview').removeClass('hidden');
        coInterface.showLoader('#content-loader-preview');
        $('#backdrop-loader-previews').removeClass('hidden');
        
        initiVariable();

        ajaxPost(
            null,
            baseUrl + "/co2/zone/getcities",
            _params,
            function(cities) {
                infoZoneAPI().then(citiesApi => {
                    if (citiesApi && citiesApi.length > 0) {
                        allCities = cities;
                        allCitiesApi = citiesApi;
                        renderData(cities, citiesApi);
                    } else {
                        $('#loader-preview').addClass('hidden');
                        $('#backdrop-loader-previews').addClass('hidden');
                        toastr.warning("Aucune ville n'a été trouvée.");
                        $("#preview-container").addClass('hidden');
                        $("#no_data_table").removeClass('hidden');
                        $(".header_previews").addClass("header_previews_no");
                    }
                }).catch(error => {
                    toastr.error("Erreur lors de la récupération des villes", error);
                });

                document.getElementById('searchInput').addEventListener('input', function() {
                    var filter = this.value.toLowerCase();
                    let rows = document.querySelectorAll('#data-content tr');

                    rows.forEach(row => {
                        let cityName = row.cells[0].textContent.toLowerCase();
                        if (cityName.includes(filter)) {
                            row.style.display = '';
                        } else {
                            row.style.display = 'none';
                        }
                    });
                });
            }
        )
    }

    async function infoZoneAPI() {
        try {
            let url = _level == "4" ?
                `https://geo.api.gouv.fr/departements?nom=${encodeURIComponent(_name)}` :
                `https://geo.api.gouv.fr/epcis?nom=${encodeURIComponent(_name)}`;

            let response = await fetch(url);
            let zone = await response.json();

            let cities = [];

            if (zone && zone.length > 0) {
                let newUrl = _level == "4" ?
                    `https://geo.api.gouv.fr/departements/${zone[0].code}/communes?fields=nom,population,codesPostaux,surface,centre,siren,code,contour` :
                    `https://geo.api.gouv.fr/epcis/${zone[0].code}/communes?fields=nom,population,codesPostaux,surface,centre,siren,code,contour`;

                let responses = await fetch(newUrl);
                cities = await responses.json();
            } else {
                toastr.warning("Aucune zone valide n'a été trouvée.");
            }

            return cities;

        } catch (error) {
            toastr.error("Erreur lors de la récupération du zone dans geo.api.gouv.fr", error);
            return [];
        }
    }

    function eventListe() {
        $(".postal-co-data .city-name p").on('click', function() {
            let cityData = $(this).parent().data('city');
            let cohtmlPostal = "";
            if (typeof cityData.postalCodes !== "undefined") {
                (cityData.postalCodes).forEach(postal => {
                    if (postal && typeof postal.postalCode !== "undefined") cohtmlPostal += ` ${postal.postalCode}, `;
                });
            }
            let details = `<div class="details">
                                <p><b>Code postale :</b> ${cohtmlPostal ? cohtmlPostal : ''}</p>  
                                <p><b>Structure :</b> ${cityData.structure ? cityData.structure : ''}</p>
                                <p><b>Population :</b> ${cityData.habitants ? cityData.habitants : ''}</p>
                                <p><b>Superficie :</b> ${cityData.surface ? cityData.surface : ''}</p>
                                <p><b>Densité :</b> ${cityData.densite ? cityData.densite : ''}</p>
                                <p><b>Lat :</b> ${cityData.geo && cityData.geo.latitude ? cityData.geo.latitude : ''}</p>
                                <p><b>Long :</b> ${cityData.geo && cityData.geo.longitude ? cityData.geo.longitude : ''}</p>
                            </div>`;

            let parent = $(this).parent();
            if (parent.find('.details').length > 0) {
                parent.find('.details').slideToggle();
            } else {
                parent.append(details);
                parent.find('.details').slideDown();
            }
        });

        $(".postal-api-data .city-name p").on('click', function() {
            let cityData = $(this).parent().data('city');
            let apihtmlPostal = "";
            if (cityData.codesPostaux) {
                (cityData.codesPostaux).forEach(postal => {
                    apihtmlPostal += ` ${postal}, `;
                });
            }
            let details = `<div class="details">
                                <p><b>Code postale :</b> ${apihtmlPostal ? apihtmlPostal : ''}</p>  
                                <p><b>Population :</b> ${cityData && cityData.population ? cityData.population : ''}</p>
                                <p><b>Superficie :</b> ${cityData && cityData.surface ? cityData.surface : ''}</p>
                                <p><b>Lat :</b> ${cityData && cityData.centre && cityData.centre.coordinates ? cityData.centre.coordinates[1] : ''}</p> 
                                <p><b>Long :</b> ${cityData && cityData.centre && cityData.centre.coordinates ? cityData.centre.coordinates[0] : ''}</p>
                            </div>`;

            let parent = $(this).parent();
            if (parent.find('.details').length > 0) {
                parent.find('.details').slideToggle();
            } else {
                parent.append(details);
                parent.find('.details').slideDown();
            }
        });

        $(".myEdit").on('click', function() {
            let _id = $(this).attr('data-id');
            $(this).parent().addClass("co-data-edited");

            if (valueTemp == "") {
                editId[_id] = {};
                editIdTemp = _id;
                document.getElementById('btn-merge-data').disabled = true;

                document.querySelectorAll('.myEdit').forEach(el => {
                    el.classList.add('hidden');
                });

            } else {
                editId[_id] = valueTemp;
                valueId[codeTemp] = _id;
                codeTemp = "";
                valueTemp = "";
                
                document.getElementById('btn-merge-data').disabled = false;
                document.getElementById('btn-merge-data').innerText = `Merger ( ${Object.keys(editId).length} )`;
                document.querySelectorAll('.myCheckbox').forEach(checkbox => {
                    checkbox.disabled = false;
                });
            }
            $("#no_" + _id).removeClass('hidden');
        });

        $(".myCheckbox").on('click', function() {
            let _city = $(this).data('city');
            let _code = $(this).data('id');
            if (this.checked) {
                $(this).parent().addClass("api-data-edited");
                if (editIdTemp != "") {
                    editId[editIdTemp] = _city;
                    valueId[_code] = editIdTemp;
                    editIdTemp = "";

                    document.getElementById('btn-merge-data').disabled = false;
                    document.getElementById('btn-merge-data').innerText = `Merger ( ${Object.keys(editId).length} )`;
                    document.querySelectorAll('.myEdit').forEach(el => {
                        if (!el.closest('.co-data-edited')) {
                            el.classList.remove('hidden');
                        }
                    });
                } else {
                    valueTemp = _city;
                    valueId[_code] = "";
                    codeTemp = _code;
                    document.getElementById('btn-merge-data').disabled = true;

                    document.querySelectorAll('.myCheckbox').forEach(checkbox => {
                        if (!checkbox.closest('.api-data-edited')) {
                            checkbox.disabled = true;
                        }
                    });
                }
            } else {
                if (valueId[_code] != "") {
                    let idCitie = valueId[_code];
                    delete editId[idCitie];
                    delete valueId[_code];
                    $("#co_" + idCitie).parent().removeClass("co-data-edited");
                    $("#no_" + idCitie).addClass('hidden');
                }

                if (Object.keys(editId).length == 0) {
                    document.getElementById('btn-merge-data').disabled = true;
                    document.getElementById('btn-merge-data').innerText = `Merger`;
                } else {
                    document.getElementById('btn-merge-data').innerText = `Merger ( ${Object.keys(editId).length} )`;
                }

                $(this).parent().removeClass("api-data-edited");
                document.querySelectorAll('.myCheckbox').forEach(checkbox => {
                    checkbox.disabled = false;
                });
                document.querySelectorAll('.myEdit').forEach(el => {
                    if (!el.closest('.co-data-edited')) {
                        el.classList.remove('hidden');
                    }
                });
            }
        });

        $(".noEdit").on('click', function() {
            let _id = $(this).attr('data-id');
            let _code = Object.keys(valueId).find(key => valueId[key] === _id);
            delete editId[_id];
            $("#co_" + _id).parent().removeClass("co-data-edited");
            if (_code) {
                delete valueId[_code];
                $("#api_" + _code).parent().removeClass("api-data-edited");
                document.getElementById('api_' + _code).click();
            }

            $(this).addClass('hidden');
            document.querySelectorAll('.myEdit').forEach(el => {
                if (!el.closest('.co-data-edited')) {
                    el.classList.remove('hidden');
                }
            });

            if (Object.keys(editId).length == 0) {
                document.getElementById('btn-merge-data').disabled = true;
                document.getElementById('btn-merge-data').innerText = `Merger`;
            } else {
                document.getElementById('btn-merge-data').innerText = `Merger ( ${Object.keys(editId).length} )`;
            } 
        });

        $("#select-type-filter").on('change', function() {
            let option = $("#select-type-filter").val();

            if (option == "1") {
                $("#btn-merge-alldata").addClass("hidden");
                $(".parfaitRow").removeClass('hidden');
                $(".inparfaitRow").addClass('hidden');
            } else if (option == "2") {
                $(".inparfaitRow").removeClass('hidden');
                $(".parfaitRow").addClass('hidden');
                $("#btn-merge-alldata").removeClass("hidden");
                document.getElementById('btn-merge-alldata').innerText = `Merger ( ${Object.keys(inparfaitCities).length} ) `;
            } else {
                $(".rowList").removeClass('hidden');
                $("#btn-merge-alldata").addClass("hidden");
            }
        });

        $(".addCities").on('click', function() {
            let cities = $(this).parent().data('city');
            saveCities(cities);
        });

    }

    function renderData(cities, citiesApi) {
        let keys = Object.keys(cities);
        let tableBody = $('#data-content');
        tableBody.empty();

        let zoneLevel = _level == '4' ? ' Département' : ' EPCI';
        let htmlTitle = `Il y a <b>${Object.keys(cities).length}</b> ville(s) dans la base de données de CO et <b>${citiesApi.length}</b> ville(s) trouver dans la base de données du gouvernement Francais rattachée(s) au ${zoneLevel} d'(e) <b>${_name}</b>.`;
        $(".info-coherence-title").html(htmlTitle);

        keys.forEach(key => {
            let value = cities[key];
            let cityName = value.name;
            let indexApi = citiesApi.findIndex(element => element.nom === cityName);
            let citiesApiName = citiesApi[indexApi];

            if (citiesApiName) {

                citiesCoherent.push(value);
                citiesApiCoherent.push(citiesApiName);

                let cohtmlPostal = "";
                if (typeof value.postalCodes !== "undefined") {
                    (value.postalCodes).forEach(postal => {
                        if (postal && typeof postal.postalCode !== "undefined") cohtmlPostal += ` ${postal.postalCode}, `;
                    });
                }

                let apihtmlPostal = "";
                if (citiesApiName.codesPostaux) {
                    (citiesApiName.codesPostaux).forEach(postal => {
                        apihtmlPostal += ` ${postal}, `;
                    });
                }

                let rowClass = "inparfaitRow";
                if (value.habitants == citiesApiName.population && value.surface == citiesApiName.surface && citiesApiName.centre.coordinates[1] == value.geo.latitude && value.geo.longitude == citiesApiName.centre.coordinates[0]) {
                    rowClass = "parfaitRow";
                } else {
                    inparfaitCities[key] = citiesApiName;
                }

                let newRow = `
                            <tr class="rowList ${rowClass}">
                                <td class="cityName name-width">${cityName ? cityName : ''}</td>  
                                <td class="co-width">
                                    <p><b>Code postale :</b> ${cohtmlPostal ? cohtmlPostal : ''}</p>  
                                    <p><b>Structure :</b> ${value.structure ? value.structure : ''}</p>
                                    <p><b>Population :</b> ${value.habitants ? value.habitants : ''}</p>
                                    <p><b>Superficie :</b> ${value.surface ? value.surface : ''}</p>
                                    <p><b>Densité :</b> ${value.densite ? value.densite : ''}</p>
                                    <p><b>Lat :</b> ${value.geo && value.geo.latitude ? value.geo.latitude : ''}, <b>Long :</b> ${value.geo && value.geo.longitude ? value.geo.longitude : ''}</p>
                                </td>
                                <td class="api-width">
                                    <p><b>Code postale :</b> ${apihtmlPostal ? apihtmlPostal : ''}</p>
                                    <p><b>Population :</b> ${citiesApiName && citiesApiName.population ? citiesApiName.population : ''}</p>
                                    <p><b>Superficie :</b> ${citiesApiName && citiesApiName.surface ? citiesApiName.surface : ''}</p>
                                    <p><b>Siren :</b> ${citiesApiName && citiesApiName.siren ? citiesApiName.siren : ''}</p>
                                    <p><b>Lat :</b> ${citiesApiName && citiesApiName.centre && citiesApiName.centre.coordinates ? citiesApiName.centre.coordinates[1] : ''}, <b>Long :</b> ${citiesApiName && citiesApiName.centre && citiesApiName.centre.coordinates ? citiesApiName.centre.coordinates[0] : ''}</p>
                                </td>
                            </tr>`;
                tableBody.append(newRow);

                citiesApi.splice(indexApi, 1);
            } else {
                cities1Coherent.push(value);
                if (Array.isArray(value.postalCodes) && value.postalCodes.length > 0) {
                    value.postalCodes.forEach(postal => {
                        if (postal && typeof (postal?.postalCode) !== "undefined") {
                            if (!allPostalCode.includes(postal.postalCode)) {
                                allPostalCode.push(postal.postalCode);
                            }
                        } 
                    });
                }
            }
        });

        citiesApi.forEach(city => {
            citiesApi1Coherent.push(city);
            if (typeof city.codesPostaux != "undefined") {
                (city.codesPostaux).forEach(postal => {
                    if (!allPostalCode.includes(postal)) allPostalCode.push(postal);
                });
            }
        });

        eventListe();

        $('#inputSearch').on('input', function() {
            let searchText = $(this).val().toLowerCase();

            $('.postal-code-section').each(function() {
                let section = $(this);
                let hasVisibleCity = false;

                section.find('.city-name').each(function() {
                    let cityName = $(this).data('name').toLowerCase();
                    let isVisible = cityName.includes(searchText);
                    $(this).toggle(isVisible);

                    if (isVisible) {
                        hasVisibleCity = true;
                    }
                });

                if (hasVisibleCity) {
                    section.show();
                } else {
                    section.hide();
                }
            });
        });

        setPostalCodeFilter();

        $('#loader-preview').addClass('hidden');
        $('#backdrop-loader-previews').addClass('hidden');
    };

    function setPostalCodeFilter() {
        $("#postal-code-data").removeClass('hidden');
        let container = document.getElementById('postal-code-data');
        $("#postal-code-data").empty();
        allPostalCode.forEach(code => {
            let coDataHTML = '';
            let apiDataHTML = '';

            cities1Coherent.forEach(item => {
                let _id = item._id['$id'];
                let cityName = item.name;
                let valueEncoder = JSON.stringify(item).replace(/'/g, "&#39;");
                if(Array.isArray(item.postalCodes) && item.postalCodes.length > 0) {
                    item.postalCodes.forEach(postal => {
                        if(postal && typeof postal.postalCode !== "undefined") {
                            if (postal.postalCode === code) {
                                coDataHTML += `<div class="city-name co-data" data-name="${cityName}" data-city='${valueEncoder}'>
                                                <p>${cityName}</p>
                                                <i class="fa fa-edit edition myEdit" id="co_${_id}" data-id="${_id}" title="cliquez pour modifier"></i> 
                                                <i class="fa fa-times noEdit hidden" id="no_${_id}" data-id="${_id}"></i>
                                            </div>`;
                            }
                        }
                    })
                }
            });

            citiesApi1Coherent.forEach(item => {
                if(Array.isArray(item.codesPostaux) && item.codesPostaux.length > 0){
                    item.codesPostaux.forEach(postaux => {
                        if (postaux === code) {
                            let cityName = item.nom;
                            let valueEncoder = JSON.stringify(item).replace(/'/g, "&#39;");
                            apiDataHTML += `<div class="city-name api-data" data-name="${cityName}" data-city='${valueEncoder}'>
                                                <p>${cityName}</p>
                                                <i class="fa fa-save addCities" id="add_${item.code}" data-id="${item.code}" title="Cliquez pour Enregistrer l'info dans la base de CO"></i>
                                                <input type="checkbox" id="api_${item.code}" data-id="${item.code}" data-city='${valueEncoder}' class="myCheckbox">
                                            </div>`;
                        }
                    })
                }
            });

            let postalCodeHeader = `<h5>Code Postal : ${code}</h5>`;
            let rowHTML = `<div class="row postal-code-section" data-code="${code}">
                            ${postalCodeHeader}
                            <div class="col-lg-6 col-md-6 postal-co-data">
                                ${coDataHTML}
                            </div>
                            <div class="col-lg-6 col-md-6 postal-api-data">
                                ${apiDataHTML}
                            </div>
                        </div>`;
            container.innerHTML += rowHTML;

            eventListe();
        });
    }

    function mergeEditData(params) {
        let dataEdit = [];
        let upateType = params;
        if (params == 'data') {
            dataEdit = editId;
        } else {
            dataEdit = inparfaitCities;
        }

        $('#loader-preview').removeClass('hidden');
        $('#backdrop-loader-previews').removeClass('hidden');

        if (Object.keys(dataEdit).length > 0) {
            let keys = Object.keys(dataEdit);

            let promises = keys.map(key => {
                let value = dataEdit[key];
                let lat = value.centre.coordinates[1];
                let lng = value.centre.coordinates[0];
                let alterName = (value.nom).replace(/-/g, ' ').toUpperCase();
                let params = {
                    id: key,
                    value: {
                        name: value.nom,
                        alternateName: alterName,
                        habitants: value.population,
                        surface: value.surface,
                        latitude: lat,
                        longitude: lng,
                        postalCodes: value.codesPostaux
                    },
                    updateType: upateType
                };

                return new Promise((resolve, reject) => {
                    ajaxPost(
                        null,
                        baseUrl + "/co2/zone/updatecities",
                        params,
                        function(result) {
                            resolve(result);
                        },
                        function(error) {
                            reject(error);
                        }
                    );
                });
            });

            Promise.all(promises).then(results => {
                toastr.success("City updated successfully");
                coherenceData();
            }).catch(error => {
                toastr.error("City update failed");
            });
        }
    }

    function saveCities(cities) {
        $('#loader-preview').removeClass('hidden');
        $('#backdrop-loader-previews').removeClass('hidden');

        let alterName = (cities.nom).replace(/-/g, ' ').toUpperCase();
        let params = {
            name: cities.nom,
            alternateName: alterName,
            surface: cities.surface,
            habitants: cities.population,
            postalCodes: cities.codesPostaux,
            latitude: cities.centre.coordinates[1],
            longitude: cities.centre.coordinates[0],
            "@context": "schema",
            "@type": "City",
            "country": "FR",
            level1: _infoZone.level1,
            level1Name: _infoZone.level1Name,
            level3: _infoZone.level3,
            level3Name: _infoZone.level3Name
        }

        if (_infoZone.level == ['4']) {
            params.level4 = _infoZone._id['$id'];
            params.level4Name = _infoZone.name;
        } else {
            params.level4 = _infoZone.level4;
            params.level4Name = _infoZone.level4Name;
            params.level5 = _infoZone._id['$id'];
            params.level5Name = _infoZone.name;
        }

        params.geoShape = cities.contour;

        ajaxPost(
            null,
            baseUrl + "/co2/zone/savecities",
            params,
            function(result) {
                toastr.success("Save cities successfully");
                coherenceData();
            }
        )
    }

    $(function() {
        coherenceData();
    })
</script>